﻿using CommonServiceLocator;
using Microsoft.Office.Word.Server.Conversions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.Administration.Backup;
using Microsoft.SharePoint.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using SolrNet;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ViPortal_Utils;
using ViPortal_Utils.Base;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.DMHinhAnh;
using ViPortalData.Lconfig;
using ViPortalData.LichTongCongTy;
using ViPortalData.Models;

namespace VIPORTAL
{
    public class ConvertCheckLich
    {
        public int ID { get; set; }
        public int OldID { get; set; }

        public ConvertCheckLich()
        {

        }
    }
    class Program
    {

        public static void UpdateHinhAnhVideo()
        {
            HttpClient clientGetway = new HttpClient();
            HttpResponseMessage responseLogin = clientGetway.PostAsync("https://portal.evnhanoi.vn/VIPortalAPI/api/Account/UserLogin", new StringContent(
                        Newtonsoft.Json.JsonConvert.SerializeObject(new
                        {
                            UserName = "phuclv",
                            Passward = "123456a@"
                        }), Encoding.UTF8, "application/json")).Result;

            string responseToken = responseLogin.Content.ReadAsStringAsync().Result;
            var TokenJson = JObject.Parse(responseToken);
            if (TokenJson["Status"].ToString() == "Success")
            {


                //add surname
                string token = TokenJson["Message"].ToString();
                clientGetway = new HttpClient();
                clientGetway.DefaultRequestHeaders.Add("Authorization1", token);
                //https://portal.evnhanoi.vn/VIPortalAPI/api/Account/UserLogin
                //lấy token

                //LconfigDA oLconfigDA = new LconfigDA();
                string APIHinhAnhVideo = "";//oLconfigDA.GetListJson(new LconfigQuery() { ConfigType = "APIHinhAnhVideo" }).FirstOrDefault()?.ConfigValue;
                                            //if (string.IsNullOrEmpty(APIHinhAnhVideo))
                APIHinhAnhVideo = "https://apicskh.evnhanoi.com.vn";
                HttpClient client = new HttpClient();
                //var byteArray = Encoding.ASCII.GetBytes("EVNHANOI:Evnhanoi@123");
                //client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
                HttpResponseMessage response = client.GetAsync(APIHinhAnhVideo + "/api/PublicImage/GetCategoryImage?numberOfCategory=100").Result;
                response.EnsureSuccessStatusCode();
                string responseBody = response.Content.ReadAsStringAsync().Result;
                JObject jObject = JObject.Parse(responseBody);

                List<categoryImagesEVN> players = jObject["categoryImagesList"].ToObject<categoryImagesEVN[]>().ToList();


                //video nữa.
                response = client.GetAsync(APIHinhAnhVideo + $"/api/PublicVideo/GetVideo?pageIndex=1&pageSize=40&type=Video").Result;
                response.EnsureSuccessStatusCode();
                responseBody = response.Content.ReadAsStringAsync().Result;
                jObject = JObject.Parse(responseBody);
                int totalPages = jObject["totalPages"].ToObject<int>();
                List<VideoEVN> LstVideo = jObject["videosList"].ToObject<VideoEVN[]>().ToList();
                List<VideoJson> oDataVideo = LstVideo.Select(x => new VideoJson()
                {
                    ImageNews = x.thumbnail,
                    Title = x.name,
                    LinkVideo = x.url,
                    Created = x.createdDate
                }).ToList();

                using (ClientContext context = new ClientContext("https://portal.evnhanoi.vn/noidung"))
                {
                    #region MyRegion
                    context.AuthenticationMode = ClientAuthenticationMode.FormsAuthentication;
                    context.FormsAuthenticationLoginInfo = new FormsAuthenticationLoginInfo("spadmin", "evnhn@123");
                    //
                    // The SharePoint web at the URL.
                    Web web = context.Web;
                    // We want to retrieve the web's properties.
                    context.Load(web);
                    context.Load(web.Webs);
                    // Execute the query to the server.
                    context.ExecuteQuery();
                    List oDMHinhAnh = context.Web.Lists.GetByTitle("DMHinhAnh");
                    context.Load(oDMHinhAnh);

                    List HinhAnh = context.Web.Lists.GetByTitle("HinhAnh");
                    context.Load(HinhAnh);

                    List Video = context.Web.Lists.GetByTitle("Video");
                    context.Load(Video);
                    context.ExecuteQuery();
                    #endregion
                    foreach (var item in players)
                    {
                        int idexit = CheckExitField(oDMHinhAnh, context, item.id.ToString(), "OldID");
                        if (idexit == 0)
                        {
                            ListItemCreationInformation itemCreateInfo = new ListItemCreationInformation();
                            ListItem oListItem = oDMHinhAnh.AddItem(itemCreateInfo);
                            //ListItem oListItem = LUser.GetItemById(IDGroup);
                            //clientContext.Load(oListItem);
                            //clientContext.ExecuteQuery();

                            //ListItem oListItem = oList.AddItem(itemCreateInfo);
                            oListItem["Title"] = item.name;
                            oListItem["ImageNews"] = item.url;
                            oListItem["DMSLAnh"] = item.total;
                            oListItem["TBNgayDang"] = DateTime.Now;
                            oListItem["OldID"] = item.id;
                            oListItem["_ModerationStatus"] = 0;
                            oListItem.Update();
                            context.ExecuteQuery();
                            //save solr


                            HttpResponseMessage responseALL = clientGetway.PostAsync("https://portal.evnhanoi.vn/VIPortalAPI/api/Mobi/UpdateItemSolrData", new StringContent(
                             Newtonsoft.Json.JsonConvert.SerializeObject(new
                             {
                                 ItemID = oListItem.Id,
                                 UrlList = "/noidung/Lists/DMHinhAnh",
                                 TypeDanhMuc = true
                             }), Encoding.UTF8, "application/json")).Result;
                            string responseBodyALL = responseALL.Content.ReadAsStringAsync().Result;
                            Console.WriteLine(responseBodyALL);
                            idexit = oListItem.Id;
                            //update ảnh.

                        }
                        if (idexit > 0)
                        {
                            //update ảnh 
                            response = client.GetAsync(APIHinhAnhVideo + $"/api/PublicImage/GetImage?categoryID={item.id}&pageIndex=1&pageSize=10000").Result;
                            response.EnsureSuccessStatusCode();
                            responseBody = response.Content.ReadAsStringAsync().Result;
                            jObject = JObject.Parse(responseBody);

                            List<ImagesEVN> lstImagesEVN = jObject["imagesList"].ToObject<ImagesEVN[]>().ToList();
                            int STT = 1;
                            foreach (ImagesEVN oImagesEVN in lstImagesEVN)
                            {
                                string imagename = Path.GetFileName(oImagesEVN.url);
                                int idexithinhanh = CheckExitField(HinhAnh, context, imagename, "Title");
                                if (idexithinhanh == 0)
                                {
                                    ListItemCreationInformation itemCreateInfo = new ListItemCreationInformation();
                                    ListItem oListItem = HinhAnh.AddItem(itemCreateInfo);
                                    //ListItem oListItem = LUser.GetItemById(IDGroup);
                                    //clientContext.Load(oListItem);
                                    //clientContext.ExecuteQuery();

                                    //ListItem oListItem = oList.AddItem(itemCreateInfo);
                                    oListItem["Title"] = imagename;
                                    oListItem["STT"] = STT++;
                                    oListItem["ImageNews"] = oImagesEVN.url;
                                    oListItem["NgayDang"] = DateTime.Now;
                                    oListItem["Album"] = idexit;
                                    oListItem["_ModerationStatus"] = 0;
                                    oListItem.Update();
                                    context.ExecuteQuery();

                                    HttpResponseMessage responseALL = clientGetway.PostAsync("https://portal.evnhanoi.vn/VIPortalAPI/api/Mobi/UpdateItemSolrData", new StringContent(
                             Newtonsoft.Json.JsonConvert.SerializeObject(new
                             {
                                 ItemID = oListItem.Id,
                                 UrlList = "/noidung/Lists/HinhAnh",
                                 TypeDanhMuc = false
                             }), Encoding.UTF8, "application/json")).Result;
                                    string responseBodyALL = responseALL.Content.ReadAsStringAsync().Result;
                                    Console.WriteLine(responseBodyALL);
                                }
                            }
                            //oData = players.Select(x => new HinhAnhJson()
                            //{
                            //    ImageNews = x.url,
                            //    Title = Path.GetFileName(x.url)
                            //}).ToList();

                        }
                    }

                    foreach (VideoJson item in oDataVideo)
                    {
                        int idexit = CheckExitField(Video, context, item.Title, "Title");
                        ListItem oListItem = null;

                        if (idexit == 0)
                        {
                            ListItemCreationInformation itemCreateInfo = new ListItemCreationInformation();
                            oListItem = Video.AddItem(itemCreateInfo);

                            //ListItem oListItem = LUser.GetItemById(IDGroup);
                            //clientContext.Load(oListItem);
                            //clientContext.ExecuteQuery();

                            //ListItem oListItem = oList.AddItem(itemCreateInfo);
                            oListItem["Title"] = item.Title;
                            oListItem["DanhMuc"] = 33;
                            oListItem["ImageNews"] = item.ImageNews;
                            oListItem["LinkVideo"] = item.LinkVideo;
                            oListItem["NgayDang"] = item.Created;
                            //oListItem["OldID"] = Path.GetFileName(item.LinkVideo);
                            oListItem["_ModerationStatus"] = 0;
                            oListItem.Update();
                            context.ExecuteQuery();
                            //save solr


                            HttpResponseMessage responseALL = clientGetway.PostAsync("https://portal.evnhanoi.vn/VIPortalAPI/api/Mobi/UpdateItemSolrData", new StringContent(
                             Newtonsoft.Json.JsonConvert.SerializeObject(new
                             {
                                 ItemID = oListItem.Id,
                                 UrlList = "/noidung/Lists/Video",
                                 TypeDanhMuc = false
                             }), Encoding.UTF8, "application/json")).Result;
                            string responseBodyALL = responseALL.Content.ReadAsStringAsync().Result;
                            Console.WriteLine(responseBodyALL);
                            idexit = oListItem.Id;
                        }
                        else
                        {
                            oListItem = Video.GetItemById(idexit);
                            context.Load(oListItem);
                            context.ExecuteQuery();

                            //ListItem oListItem = LUser.GetItemById(IDGroup);
                            //clientContext.Load(oListItem);
                            //clientContext.ExecuteQuery();

                            //ListItem oListItem = oList.AddItem(itemCreateInfo);
                            oListItem["Title"] = item.Title;
                            oListItem["DanhMuc"] = 33;
                            oListItem["ImageNews"] = item.ImageNews;
                            oListItem["LinkVideo"] = item.LinkVideo;
                            oListItem["NgayDang"] = item.Created;
                            //oListItem["OldID"] = Path.GetFileName(item.LinkVideo);
                            oListItem["_ModerationStatus"] = 0;
                            oListItem.Update();
                            context.ExecuteQuery();
                            //save solr


                            HttpResponseMessage responseALL = clientGetway.PostAsync("https://portal.evnhanoi.vn/VIPortalAPI/api/Mobi/UpdateItemSolrData", new StringContent(
                             Newtonsoft.Json.JsonConvert.SerializeObject(new
                             {
                                 ItemID = oListItem.Id,
                                 UrlList = "/noidung/Lists/Video",
                                 TypeDanhMuc = false
                             }), Encoding.UTF8, "application/json")).Result;
                            string responseBodyALL = responseALL.Content.ReadAsStringAsync().Result;
                            Console.WriteLine(responseBodyALL);
                            idexit = oListItem.Id;
                        }
                        //update ảnh.

                    }
                }

            }

        }

        public static void UpdateLichDonvi()
        {
            using (ClientContext context = new ClientContext("https://portal.evnhanoi.vn/noidung"))
            {
                context.AuthenticationMode = ClientAuthenticationMode.FormsAuthentication;
                context.FormsAuthenticationLoginInfo = new FormsAuthenticationLoginInfo("spadmin", "evnhn@123");
                //
                // The SharePoint web at the URL.
                Web web = context.Web;
                // We want to retrieve the web's properties.
                context.Load(web);
                context.Load(web.Webs);
                // Execute the query to the server.
                context.ExecuteQuery();
                List oLichTongCongTy = context.Web.Lists.GetByTitle("LichDonVi");
                context.Load(oLichTongCongTy);
                context.ExecuteQuery();
                //ListItem oListItem1 =  oLichTongCongTy.GetItemById(611);
                //context.Load(oListItem1);
                //context.ExecuteQuery();
                CamlQuery oCaml = new CamlQuery();
                oCaml.DatesInUtc = false;
                oCaml.ViewXml = "<View>" +
                       $"<Query><Where><And><Eq><FieldRef Name=\"_ModerationStatus\" /><Value Type=\"ModStat\">0</Value></Eq><Geq><FieldRef Name=\"Modified\"/><Value IncludeTimeValue='TRUE' Type='DateTime'>2022-06-01T00:00:00</Value></Geq></And></Where><OrderBy><FieldRef Name=\"ID\" Ascending=\"FALSE\"/></OrderBy><RowLimit>0</RowLimit></Query></View>";
                ListItemCollection allItem = oLichTongCongTy.GetItems(oCaml);
                context.Load(allItem);
                // Execute the query to the server.
                context.ExecuteQuery();
                HttpClient clientGetway = new HttpClient();
                var byteArray = Encoding.ASCII.GetBytes("EVNHANOI:EVNHANOI@CuongX16");
                clientGetway.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
                foreach (ListItem oListItem in allItem)
                {
                    string Title = oListItem["Title"].ToString();
                    DateTime LichThoiGianBatDau = Convert.ToDateTime(oListItem["LichThoiGianBatDau"]);
                    DateTime? LichThoiGianKetThuc = null;
                    if (oListItem["LichThoiGianKetThuc"] != null) LichThoiGianKetThuc = Convert.ToDateTime(oListItem["LichThoiGianKetThuc"]);
                    //thanh phan
                    string THANH_PHAN = "";
                    if (oListItem["LichPhongBanThamGia"] != null)
                    {
                        FieldLookupValue[] LichPhongBanThamGia = (FieldLookupValue[])oListItem["LichPhongBanThamGia"];
                        if (LichPhongBanThamGia.Length > 0)
                            THANH_PHAN += (string.Join(", ", LichPhongBanThamGia.Select(x => x.LookupValue)) + ", ");
                    }
                    if (oListItem["LichThanhPhanThamGia"] != null)
                    {
                        FieldLookupValue[] LichThanhPhanThamGia = (FieldLookupValue[])oListItem["LichThanhPhanThamGia"];
                        THANH_PHAN += string.Join(", ", LichThanhPhanThamGia.Select(x => x.LookupValue));
                    }
                    Console.WriteLine(Title);
                    string datajson = Newtonsoft.Json.JsonConvert.SerializeObject(new
                    {
                        ID_VPKG = oListItem.Id.ToString(),
                        Ngay = string.Format("{0:yyyy-MM-dd HH:mm:ss.fff}", LichThoiGianBatDau.Date), // "2021-09-25 00:00:00.000",
                        ThoiGian = LichThoiGianBatDau.Hour, //"8",
                        Phut = LichThoiGianBatDau.Minute, //"30",
                        DenGio = LichThoiGianKetThuc.HasValue ? LichThoiGianKetThuc.Value.Hour.ToString() : "",// "11",
                        DenPhut = LichThoiGianKetThuc.HasValue ? LichThoiGianKetThuc.Value.Minute.ToString() : "", //"30",
                        NoiDung = oListItem["LichNoiDung"].ToString(), // "Họp duyệt TKKT, TKBVTC công trình xây dựng mới trạm 220/110 kV Đại Mỗ và nhánh rẽ 220kV",
                        ThanhPhan = THANH_PHAN, // "Ô Tùng X14, Ô Cường TP X1.8",
                        DonVi = oListItem["CHUAN_BI"] != null ? oListItem["CHUAN_BI"].ToString().Split(new Char[] { ';', '#' }).LastOrDefault() : "", // "X1.8 kết nối Zoom ",
                        DiaDiem = oListItem["LichDiaDiem"] != null ? oListItem["LichDiaDiem"].ToString() : "",//"Zoom Cloud meetings",
                        ChuTri = oListItem["CHU_TRI"] != null ? oListItem["CHU_TRI"].ToString().Split(new Char[] { ';', '#' }).LastOrDefault() : "", //B8
                        NguoiCap = ((FieldLookupValue)oListItem["Author"]).LookupValue, // "huongnh",
                        TgCap = string.Format("{0:yyyy-MM-dd HH:mm:ss.fff}", Convert.ToDateTime(oListItem["Created"])), //"2021-09-24 22:11:49.000",
                        Duyet = "1",
                        TgDuyet = string.Format("{0:yyyy-MM-dd HH:mm:ss.fff}", DateTime.Now), // "2021-09-24 22:11:49.000",
                        Hoan = "3",
                        del = "0",
                        del_user = "",
                        del_time = ""
                    });
                    HttpResponseMessage responseALL = clientGetway.PostAsync("http://42.112.213.225:8074/api/VanPhongKhongGiay/insert_LichTuan_DonVi", new StringContent(
                    datajson, Encoding.UTF8, "application/json")).Result;

                    responseALL.EnsureSuccessStatusCode();
                    string responseBodyALL = responseALL.Content.ReadAsStringAsync().Result;
                    //var root = JsonConvert.DeserializeObject<DanhBaRespon>(responseBodyALL);
                }
            }
        }
        public static void UpdateFieldDMTin()
        {
            using (ClientContext context = new ClientContext($"https://portal.evnhanoi.vn"))
            {
                context.AuthenticationMode = ClientAuthenticationMode.FormsAuthentication;
                context.FormsAuthenticationLoginInfo = new FormsAuthenticationLoginInfo("spadmin", "evnhn@123");
                //
                // The SharePoint web at the URL.
                Web web = context.Web;

                // We want to retrieve the web's properties.
                context.Load(web);
                context.Load(web.Webs);
                context.Load(context.Site.RootWeb);
                // Execute the query to the server.
                context.ExecuteQuery();

                foreach (Web itemweb in web.Webs)
                {
                    try
                    {
                        Web itemwebcms = context.Site.OpenWeb(itemweb.ServerRelativeUrl + "/cms");
                        context.Load(itemwebcms);
                        // Execute the query to the server.
                        context.ExecuteQuery();
                        try
                        {
                            List LUser = itemwebcms.GetList($"{itemwebcms.ServerRelativeUrl}/Lists/DanhMucThongTin");
                            context.Load(LUser);
                            context.Load(LUser.Fields);
                            // Execute the query to the server.
                            context.ExecuteQuery();
                            if (LUser.Fields.FirstOrDefault(x => x.InternalName == "DBPhanLoai") == null)
                            {
                                Field ttduan = context.Site.RootWeb.AvailableFields.GetByTitle("DBPhanLoai");
                                context.Load(ttduan);
                                context.ExecuteQuery();
                                context.ExecuteQuery();
                                LUser.Fields.Add(ttduan);
                                LUser.Update();
                                context.ExecuteQuery();
                            }
                            if (LUser.ItemCount == 0)
                            {
                                //add theem mootj item default cho cai nay.
                                ListItemCreationInformation itemCreateInfo = new ListItemCreationInformation();
                                ListItem oListItem = LUser.AddItem(itemCreateInfo);
                                oListItem["Title"] = "Tin hoạt động";
                                oListItem["UrlList"] = "TinHoatDong";
                                oListItem["TrangThai"] = 0;
                                oListItem["DBPhanLoai"] = 0;
                                oListItem["_ModerationStatus"] = 0;
                                oListItem.Update();
                                context.ExecuteQuery();

                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    catch (Exception exx)
                    {

                    }
                }
            }
        }
        public static void UPdateMenuQuanTriKhaiThac()
        {
            List<string> lstBlock = new List<string>()
            {
                "/cms/Pages/nguoidung.aspx","/cms/Pages/quantriquyen.aspx","/cms/Pages/quantri.aspx",
                "/cms/Pages/Lconfig.aspx","/cms/Pages/DanhSachNgonNgu.aspx","/Pages/LVaiTro.aspx",
                "/noidung/Pages/congthanhphan.aspx","/noidung/Pages/trangthanhphan.aspx","/noidung/Pages/module.aspx","/cms/Pages/LBieuMauNoiDung.aspx",
                "/cms/Pages/NhatKyBackUp.aspx"
            };
            using (ClientContext context = new ClientContext($"http://viportal202150.vn"))
            {
                context.AuthenticationMode = ClientAuthenticationMode.FormsAuthentication;
                context.FormsAuthenticationLoginInfo = new FormsAuthenticationLoginInfo("spadmin", "1");
                //
                // The SharePoint web at the URL.
                Web web = context.Web;

                // We want to retrieve the web's properties.
                context.Load(web);
                context.Load(web.Webs);
                context.Load(context.Site.RootWeb);
                // Execute the query to the server.
                context.ExecuteQuery();

                Web webcmsroot = context.Site.OpenWeb("/cms");
                context.Load(webcmsroot);
                context.ExecuteQuery();
                List MenuQuanTriNguon = webcmsroot.GetList($"/cms/Lists/MenuQuanTri");
                context.Load(MenuQuanTriNguon);
                context.ExecuteQuery();
                ListItemCollection allItemMenu = MenuQuanTriNguon.GetItems(new CamlQuery());
                context.Load(allItemMenu);
                context.ExecuteQuery();
                foreach (Web itemweb in web.Webs)
                {
                    try
                    {
                        Web itemwebcms = context.Site.OpenWeb(itemweb.ServerRelativeUrl + "/cms");
                        context.Load(itemwebcms);

                        // Execute the query to the server.
                        context.ExecuteQuery();
                        Console.WriteLine(itemwebcms.ServerRelativeUrl);
                        try
                        {
                            List MenuQuanTriDich = itemwebcms.GetList($"{itemwebcms.ServerRelativeUrl}/Lists/MenuQuanTri");
                            context.Load(MenuQuanTriDich);
                            context.ExecuteQuery();
                            foreach (var item in allItemMenu)
                            {
                                string UrlLink = "";
                                if (item["MenuLink"] != null)
                                {
                                    UrlLink = item["MenuLink"].ToString();
                                }
                                if (!lstBlock.Contains(UrlLink))
                                {
                                    int IDNew = CheckExitField(MenuQuanTriDich, context, item.Id + "", "OldID");
                                    if (IDNew == 0)
                                    {
                                        ListItemCreationInformation itemCreateInfo = new ListItemCreationInformation();
                                        ListItem oListItem = MenuQuanTriDich.AddItem(itemCreateInfo);
                                        oListItem["Title"] = item["Title"].ToString();
                                        oListItem["MenuLink"] = item["MenuLink"];
                                        oListItem["MenuNewWindow"] = item["MenuNewWindow"];
                                        oListItem["MenuIndex"] = item["MenuIndex"];
                                        oListItem["MenuShow"] = item["MenuShow"];
                                        oListItem["CheckLogin"] = item["CheckLogin"];
                                        oListItem["MenuIcon"] = item["MenuIcon"];
                                        oListItem["Permissions"] = item["Permissions"];
                                        oListItem["MenuPublic"] = item["MenuPublic"];
                                        oListItem["PhanLoaiMenu"] = item["PhanLoaiMenu"];
                                        oListItem["MenuShowSubPortal"] = item["MenuShowSubPortal"];
                                        oListItem["MenuShowPortal"] = item["MenuShowPortal"];
                                        oListItem["OldID"] = item.Id;
                                        oListItem["_ModerationStatus"] = 0;
                                        oListItem.Update();
                                        context.ExecuteQuery();
                                    }
                                    else
                                    {
                                        if (item["MenuParent"] != null)
                                        {
                                            FieldLookupValue MenuParent = (FieldLookupValue)item["MenuParent"];
                                            if (MenuParent.LookupId > 0)
                                            {
                                                int IDParentNew = CheckExitField(MenuQuanTriDich, context, MenuParent.LookupId + "", "OldID");
                                                if (IDParentNew > 0)
                                                {
                                                    ListItem oListItem = MenuQuanTriDich.GetItemById(IDNew);
                                                    oListItem["MenuParent"] = IDParentNew;
                                                    oListItem["_ModerationStatus"] = 0;
                                                    oListItem.Update();
                                                    context.ExecuteQuery();
                                                }
                                            }


                                        }
                                    }
                                }
                            }
                            //add theem mootj item default cho cai nay.


                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    catch (Exception exx)
                    {

                    }
                }
            }
        }

        public static void UploadFile2007(string destinationUrl, byte[] fileData)
        {
            // List of desination Urls, Just one in this example.
            string[] destinationUrls = { Uri.EscapeUriString(destinationUrl) };

            // Empty Field Information. This can be populated but not for this example.
            vn.evnhanoi.portal.FieldInformation information = new
                vn.evnhanoi.portal.FieldInformation();
            vn.evnhanoi.portal.FieldInformation[] info = { information };

            // To receive the result Xml.
            vn.evnhanoi.portal.CopyResult[] result;
            vn.evnhanoi.portal.Copy CopyService2007 = new vn.evnhanoi.portal.Copy();
            //client.cre = ClientAuthenticationMode.FormsAuthentication;
            CopyService2007.Credentials = new NetworkCredential("spadmin", "evnhn@123");
            // Create the Copy web service instance configured from the web.config file.


            CopyService2007.CopyIntoItems(destinationUrl, destinationUrls, info, fileData, out result);

            if (result[0].ErrorCode != vn.evnhanoi.portal.CopyErrorCode.Success)
            {
                // ...
            }
        }
        public static void UpdateMaterage()
        {
            //https://portal.evnhanoi.vn/_catalogs/masterpage/portal_home.html
            //C:\Users\spadmin\Desktop\portal_home.html
            byte[] bytedata = System.IO.File.ReadAllBytes(@"C:\Users\spadmin\Desktop\portal_home.html");
            UploadFile2007("/_catalogs/masterpage/portal_home.html", bytedata);
            //uint result = client.CopyIntoItems(fileName, destinationUrl, fileInfoArray, content, out arrayOfResults);
        }
        public static void TestSolr()
        {
            string temp = "10;#";
            temp.GetListValueFormStringLookup();
            List<string> lstDanhMuc = new List<string>()
            {
                "/noidung/Lists/Banner","/diendan/Lists/BinhLuan","/Lists/ChatHopThoai", "/Lists/ChatMessage", "/diendan/Lists/CommentMXH","/noidung/Lists/CongThanhPhan", "/noidung/Lists/CoQuanBanHanh", "/noidung/Lists/DanhMucQuyChe","/noidung/Lists/DMChucVu", "/cms/Lists/DanhMucThongTin","/Lists/DanhSachNgonNgu", "/noidung/Lists/DMHinhAnh","/noidung/Lists/DMLinhVuc","/noidung/Lists/DMTaiLieu","/noidung/Lists/DMVideo","/diendan/Lists/ForumChuDe","/noidung/Lists/KSBoCauHoi","/noidung/Lists/KSCauTraLoi","/noidung/Lists/KSKetQua","/cms/Lists/Lconfig","/Lists/LGroup","/noidung/Lists/LienKetWebsite","/noidung/Lists/LoaiTaiLieu","/noidung/Lists/LNotification","/Lists/LPermission","/Lists/LUser","/Lists/LVaiTro","/cms/Lists/MailPending","/cms/Lists/MenuQuanTri", "/noidung/Lists/ModuleChucNang",  "/noidung/Lists/TraLoiHoiDap","/noidung/Lists/TrangThanhPhan"
            };
            List<string> lstData = new List<string>()
            {
                "/diendan/Lists/BaiViet","/noidung/Lists/DanhBa","/noidung/Lists/FAQThuongGap","/noidung/Lists/HinhAnh","/noidung/Lists/HoiDap","/noidung/Lists/KSBoChuDe","/noidung/Lists/LichCaNhan","/noidung/Lists/LichDonVi","/noidung/Lists/LichHop","/noidung/Lists/LichPhong","/noidung/Lists/LichTapDoan","/noidung/Lists/LichTongCongTy","/noidung/tintuc/Lists/LTinDiemBao","/diendan/Lists/MangXaHoi","/noidung/Lists/QuyCheNoiBo","/noidung/Lists/ThongBao","/noidung/Lists/ThongBaoKetLuan","/noidung/Lists/TinNoiBat","/noidung/Lists/VanBanTaiLieu","/noidung/Lists/Video","/noidung/Lists/TinXaHoi/","/noidung/Lists/TinTongCongTy"
            };
            //Console.WriteLine("Nhập url list");
            //string UrlList = Console.ReadLine();
            //Console.WriteLine("Loại data:");
            //string intLoai = Console.ReadLine();
            StartupSolr.Init<DanhMucSolr>("http://solrserver2:9000/solr/Portal2021DanhMuc");
            var solrWorkerStaticDM = ServiceLocator.Current.GetInstance<ISolrOperations<DanhMucSolr>>();

            StartupSolr.Init<DataSolr>("http://solrserver2:9000/solr/Portal2021");
            var solrWorkerStatic = ServiceLocator.Current.GetInstance<ISolrOperations<DataSolr>>();



            using (SPSite oSite = new SPSite("http://localhost:6003"))
            {
                //SPWebTemplateCollection WebTemplates = oSite.GetWebTemplates(Convert.ToUInt32(1033));
                //foreach (SPWebTemplate WebTemplate in WebTemplates)
                //{
                //    if (WebTemplate.Name.Contains("templanoidung"))
                //    {
                //        SPWeb spWebCreated = oSite.AllWebs.Add("/tempnoidung", "Template Noi dung", "", oSite.RootWeb.Language, WebTemplate.Name,
                //                                 false, false);
                //    }
                //    Console.WriteLine(WebTemplate.Name);
                //}
                foreach (string UrlList in lstDanhMuc)
                {
                    try
                    {
                        using (SPWeb oweb = oSite.OpenWeb(UrlList))
                        {
                            SPList ListConvert = oweb.GetList(UrlList);
                            foreach (SPListItem item in ListConvert.Items)
                            {
                                DanhMucSolr oDMSolr = new DanhMucSolr(item);
                                solrWorkerStaticDM.Add(oDMSolr);
                                solrWorkerStaticDM.CommitAsync().Wait();
                                Console.WriteLine(oDMSolr.Title);

                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw ex;
                    }
                }
                foreach (string UrlList in lstData)
                {
                    try
                    {
                        using (SPWeb oweb = oSite.OpenWeb(UrlList))
                        {
                            SPList ListConvert = oweb.GetList(UrlList);
                            foreach (SPListItem item in ListConvert.Items)
                            {
                                DataSolr oDMSolr = new DataSolr(item);
                                solrWorkerStatic.Add(oDMSolr);
                                solrWorkerStatic.CommitAsync().Wait();
                                Console.WriteLine(oDMSolr.Title);

                            }

                        }
                    }
                    catch (Exception ex)
                    {

                        Console.WriteLine(ex.Message);
                        throw ex;
                    }
                }
            }
        }

        public static void TestSolrSubSite()
        {
            string temp = "10;#";
            string UrlSite = "";
            Console.WriteLine("Nhập urlsite");
            UrlSite = Console.ReadLine();
            temp.GetListValueFormStringLookup();
            List<string> lstDanhMuc = new List<string>()
            {
                "/noidung/Lists/Banner","/diendan/Lists/BinhLuan","/Lists/ChatHopThoai", "/Lists/ChatMessage", "/diendan/Lists/CommentMXH","/noidung/Lists/CongThanhPhan", "/noidung/Lists/CoQuanBanHanh", "/noidung/Lists/DanhMucQuyChe","/noidung/Lists/DMChucVu", "/cms/Lists/DanhMucThongTin","/Lists/DanhSachNgonNgu", "/noidung/Lists/DMHinhAnh","/noidung/Lists/DMLinhVuc","/noidung/Lists/DMTaiLieu","/noidung/Lists/DMVideo","/diendan/Lists/ForumChuDe","/noidung/Lists/KSBoCauHoi","/noidung/Lists/KSCauTraLoi","/noidung/Lists/KSKetQua","/cms/Lists/Lconfig","/Lists/LGroup","/noidung/Lists/LienKetWebsite","/noidung/Lists/LoaiTaiLieu","/noidung/Lists/LNotification","/Lists/LPermission","/Lists/LUser","/Lists/LVaiTro","/cms/Lists/MailPending","/cms/Lists/MenuQuanTri", "/noidung/Lists/ModuleChucNang",  "/noidung/Lists/TraLoiHoiDap","/noidung/Lists/TrangThanhPhan"
            };
            List<string> lstData = new List<string>()
            {
                "/diendan/Lists/BaiViet","/noidung/Lists/DanhBa",
                "/noidung/Lists/FAQThuongGap","/noidung/Lists/HinhAnh","/noidung/Lists/HoiDap",
                "/noidung/Lists/KSBoChuDe","/noidung/Lists/LichCaNhan","/noidung/Lists/LichDonVi","" +
                "/noidung/Lists/LichHop","/noidung/Lists/LichPhong","/noidung/Lists/LichTapDoan","/noidung/Lists/LichTongCongTy",
                "/noidung/tintuc/Lists/LTinDiemBao","/diendan/Lists/MangXaHoi","/noidung/Lists/QuyCheNoiBo","/noidung/Lists/ThongBao",
                "/noidung/Lists/ThongBaoKetLuan","/noidung/Lists/TinNoiBat","/noidung/Lists/VanBanTaiLieu","/noidung/Lists/Video",
                "/noidung/Lists/TinXaHoi/","/noidung/Lists/TinTongCongTy"
            };
            //Console.WriteLine("Nhập url list");
            //string UrlList = Console.ReadLine();
            //Console.WriteLine("Loại data:");
            //string intLoai = Console.ReadLine();
            StartupSolr.Init<DanhMucSolr>("http://solrserver2:9000/solr/Portal2021DanhMuc");
            var solrWorkerStaticDM = ServiceLocator.Current.GetInstance<ISolrOperations<DanhMucSolr>>();

            StartupSolr.Init<DataSolr>("http://solrserver2:9000/solr/Portal2021");
            var solrWorkerStatic = ServiceLocator.Current.GetInstance<ISolrOperations<DataSolr>>();



            using (SPSite oSite = new SPSite("http://localhost:6003/" + UrlSite))
            {
                using (SPWeb oWeb = oSite.OpenWeb())
                {
                    foreach (string UrlList in lstDanhMuc)
                    {
                        try
                        {

                            SPList ListConvert = oWeb.GetList("/" + UrlSite + UrlList);
                            foreach (SPListItem item in ListConvert.Items)
                            {
                                DanhMucSolr oDMSolr = new DanhMucSolr(item);
                                solrWorkerStaticDM.Add(oDMSolr);
                                solrWorkerStaticDM.CommitAsync().Wait();
                                Console.WriteLine(oDMSolr.Title);

                            }

                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                    foreach (string UrlList in lstData)
                    {
                        try
                        {

                            SPList ListConvert = oWeb.GetList("/" + UrlSite + UrlList);
                            foreach (SPListItem item in ListConvert.Items)
                            {
                                DataSolr oDMSolr = new DataSolr(item);
                                solrWorkerStatic.Add(oDMSolr);
                                solrWorkerStatic.CommitAsync().Wait();
                                Console.WriteLine(oDMSolr.Title);

                            }

                        }
                        catch (Exception ex)
                        {

                            Console.WriteLine(ex.Message);
                        }
                    }
                }
                //SPWebTemplateCollection WebTemplates = oSite.GetWebTemplates(Convert.ToUInt32(1033));
                //foreach (SPWebTemplate WebTemplate in WebTemplates)
                //{
                //    if (WebTemplate.Name.Contains("templanoidung"))
                //    {
                //        SPWeb spWebCreated = oSite.AllWebs.Add("/tempnoidung", "Template Noi dung", "", oSite.RootWeb.Language, WebTemplate.Name,
                //                                 false, false);
                //    }
                //    Console.WriteLine(WebTemplate.Name);
                //}

            }
        }


        private static SPBackupRestoreObject EnsureUniqueValidComponentName(SPBackupRestoreSettings settings, ref Guid operationGUID)
        {
            SPBackupRestoreObjectCollection list = SPBackupRestoreConsole.FindItems(operationGUID, settings.IndividualItem);
            SPBackupRestoreObject component = null;

            if (list.Count <= 0)
            {
                Console.WriteLine("There is no component with that name. Run again with a new name.");
                Console.WriteLine("Press Enter to continue.");
                Console.ReadLine();
            }
            else if (list.Count > 1)  // The component name specified is ambiguous. Prompt user to be more specific.
            {
                Console.WriteLine("More than one component matches the name you entered.");
                Console.WriteLine("Run again with one of the following:");
                for (int i = 0; i < list.Count; i++)
                {
                    Console.WriteLine("\t{0}", list[i].ToString());
                }
                Console.WriteLine("Press Enter to continue.");
                Console.ReadLine();
            }
            else
            {
                component = list[0];
            }

            return component;

        }
        private static Boolean EnsureEnoughDiskSpace(String location, Guid backup, SPBackupRestoreObject node)
        {
            UInt64 backupSize = SPBackupRestoreConsole.DiskSizeRequired(backup, node);
            UInt64 diskFreeSize = 0;
            UInt64 diskSize = 0;
            Boolean hasEnoughSpace = true;

            try
            {
                SPBackupRestoreConsole.DiskSize(location, out diskFreeSize, out diskSize);
            }
            catch
            {
                diskFreeSize = diskSize = UInt64.MaxValue;
            }

            if (backupSize > diskFreeSize)
            {
                // Report through your UI that there is not enough disk space.
                Console.WriteLine("{0} bytes of space is needed but the disk hosting {1} has only {2}.", backupSize, location, diskFreeSize);
                Console.WriteLine("Please try again with a different backup location or a smaller component.");
                hasEnoughSpace = false;
            }
            else if (backupSize == UInt64.MaxValue || diskFreeSize == 0)
            {
                // Report through your UI that it cannot be determined whether there is enough disk space.
                Console.WriteLine("Cannot determine if that location has enough disk space.");
                Console.WriteLine("Please try again with a different backup location or a smaller component.");
                hasEnoughSpace = false;
            }
            return hasEnoughSpace;

        }

        public static void UpDateNhatKyBackup()
        {

            Microsoft.SharePoint.Administration.Backup.SPBackupRestoreHistoryList temp = SPBackupRestoreConsole.GetHistory(@"\\10.10.20.29\c$\SETUP");
            NhatKyBackUpDA nhatKyBackUpDA = new NhatKyBackUpDA();
            for (int i = 0; i < temp.Count; i++)
            {
                SPBackupRestoreHistoryObject sPBackupRestoreHistoryObject = temp[i];
                NhatKyBackUpItem oNhatKyBackUpItem = new NhatKyBackUpItem()
                {
                    BackupID = sPBackupRestoreHistoryObject.SelfId.ToString(),
                    StartTime = sPBackupRestoreHistoryObject.StartTime.AddHours(7),
                    FinishTime = sPBackupRestoreHistoryObject.EndTime.AddHours(7),
                    Directory = sPBackupRestoreHistoryObject.Directory,
                    RequestedBy = sPBackupRestoreHistoryObject.RequestedBy,
                    Method = sPBackupRestoreHistoryObject.BackupMethod.ToString(),
                    Title = sPBackupRestoreHistoryObject.TopComponent,
                    ErrorCount = Convert.ToInt32(sPBackupRestoreHistoryObject.ErrorCount),
                    WarningCount = Convert.ToInt32(sPBackupRestoreHistoryObject.WarningCount)
                };
                int BackupID = nhatKyBackUpDA.CheckExit(0, oNhatKyBackUpItem.BackupID, "BackupID");

                if (nhatKyBackUpDA.CheckExit(0, oNhatKyBackUpItem.BackupID, "BackupID") == 0)
                {
                    nhatKyBackUpDA.UpdateObject<NhatKyBackUpItem>(oNhatKyBackUpItem);
                }
                else
                {
                    //NhatKyBackUpItem oldNhatKyBackUpItem = nhatKyBackUpDA.GetByIdToObjectSelectFields<NhatKyBackUpItem>(BackupID, "FinishTime");
                    //if (!oldNhatKyBackUpItem.FinishTime.HasValue)
                    //{
                    //    oNhatKyBackUpItem.ID = BackupID;
                    //    nhatKyBackUpDA.UpdateObject<NhatKyBackUpItem>(oNhatKyBackUpItem);
                    //}
                }
            }

        }
        public static void BackupJOb()
        {
            NhatKyBackUpDA nhatKyBackUpDA = new NhatKyBackUpDA();

            List<NhatKyBackUpJson> lstNhatKy = nhatKyBackUpDA.GetListJson(new NhatKyBackUpQuery() { ChoBackUp = true });
            foreach (NhatKyBackUpJson oNhatKyBackUpJson in lstNhatKy)
            {

                #region MyRegion
                //Console.Write("Enter full UNC path to the directory where the backup will be stored:");
                String backupLocation = @"\\10.10.20.29\c$\SETUP";
                // Create the backup settings.
                SPBackupSettings settings = SPBackupRestoreSettings.GetBackupSettings(backupLocation, "Full");

                // Identify the content component to backup.
                Console.Write("Enter name of component to backup (default is whole farm):");
                settings.IndividualItem = "SharePoint - 6003";

                // Set optional operation parameters.
                settings.IsVerbose = true;
                settings.UpdateProgress = 10;
                settings.BackupThreads = 10;

                // Create the backup operation and return its ID.
                Guid backup = SPBackupRestoreConsole.CreateBackupRestore(settings);

                // Ensure that user has identified a valid and unique component.
                SPBackupRestoreObject node = EnsureUniqueValidComponentName(settings, ref backup);

                // Ensure that there is enough space.
                Boolean targetHasEnoughSpace = false;
                if (node != null)
                {
                    targetHasEnoughSpace = EnsureEnoughDiskSpace(backupLocation, backup, node);
                }

                // If there is enough space, attempt to run the backup.
                if (targetHasEnoughSpace)
                {
                    // Set the backup as the active job and run it.
                    if (SPBackupRestoreConsole.SetActive(backup) == true)
                    {
                        if (SPBackupRestoreConsole.Run(backup, node) == false)
                        {
                            // Report "error" through your UI.
                            String error = SPBackupRestoreConsole.Get(backup).FailureMessage;
                            Console.WriteLine(error);
                        }
                    }
                    else
                    {
                        // Report through your UI that another backup
                        // or restore operation is underway. 
                        Console.WriteLine("Another backup or restore operation is already underway. Try again when it ends.");
                    }

                    // Clean up the operation.
                    SPBackupRestoreConsole.Remove(backup);
                    //nhatKyBackUpDA.UpdateOneField(oNhatKyBackUpJson.ID, "BackupID", backup.ToString());
                    Microsoft.SharePoint.Administration.Backup.SPBackupRestoreHistoryList temp = SPBackupRestoreConsole.GetHistory(@"\\10.10.20.29\c$\SETUP");
                    if (temp.Count > 0)
                    {
                        SPBackupRestoreHistoryObject sPBackupRestoreHistoryObject = temp[0];
                        NhatKyBackUpItem oNhatKyBackUpItem = new NhatKyBackUpItem()
                        {
                            BackupID = sPBackupRestoreHistoryObject.SelfId.ToString(),
                            StartTime = sPBackupRestoreHistoryObject.StartTime.AddHours(7),
                            FinishTime = sPBackupRestoreHistoryObject.EndTime.AddHours(7),
                            Directory = sPBackupRestoreHistoryObject.Directory,
                            RequestedBy = sPBackupRestoreHistoryObject.RequestedBy,
                            Method = sPBackupRestoreHistoryObject.BackupMethod.ToString(),
                            Title = sPBackupRestoreHistoryObject.TopComponent,
                            ErrorCount = Convert.ToInt32(sPBackupRestoreHistoryObject.ErrorCount),
                            WarningCount = Convert.ToInt32(sPBackupRestoreHistoryObject.WarningCount)
                        };
                        oNhatKyBackUpItem.ID = oNhatKyBackUpJson.ID;
                        nhatKyBackUpDA.UpdateObject<NhatKyBackUpItem>(oNhatKyBackUpItem);
                    }
                    Console.WriteLine("Backup attempt complete. Press Enter to continue.");
                    Console.ReadLine();
                }
                #endregion
            }
        }
        public static void TestAPIEVN()
        {
            var client = new HttpClient();
            //        ServicePointManager.ServerCertificateValidationCallback +=
            //(sender, cert, chain, sslPolicyErrors) => true;
            //        var content = client.GetStringAsync("http://14.238.40.33:1111//sso/serviceValidate?ticket=70d0ce1b-0aef-47ce-93c1-32fe48db9587&appCode=PORTAL").Result;
            //http://14.238.40.33:1111/sso/serviceValidate?ticket=70d0ce1b-0aef-47ce-93c1-32fe48db9587&appCode=PORTAL
            //http://14.238.40.33:1111//sso/serviceValidate?ticket=70d0ce1b-0aef-47ce-93c1-32fe48db9587&appCode=PORTAL
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://14.238.40.33:1111/sso/serviceValidate?ticket=70d0ce1b-0aef-47ce-93c1-32fe48db9587&appCode=PORTAL");
                request.Method = "GET";
                request.ContentType = "application/json";
                WebResponse response = request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            }
            catch (WebException e)
            {
                WebResponse response = e.Response;
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                // TODO: Check whether the status code is one you can handle
                // Get the data from here...
            }
        }


        public static byte[] WordToPdf()
        {

            using (MemoryStream write = new MemoryStream())
            {
                using (SPSite osite = new SPSite("http://localhost:6003"))
                {
                    using (SPWeb oweb = osite.OpenWeb("/htbaocao"))
                    {
                        Stream read = oweb.GetFile("/htbaocao/Lists/KyBaoCao/Attachments/169/MauBCCT169030522221820.xlsx").OpenBinaryStream();
                        string wordAutomationServiceName = "Word Automation Services";
                        SyncConverter sc = new SyncConverter(wordAutomationServiceName);
                        sc.UserToken = osite.UserToken;
                        sc.Settings.UpdateFields = true;
                        sc.Settings.OutputFormat = SaveFormat.PDF;
                        ConversionItemInfo info = sc.Convert(read, write);
                        if (info.Succeeded)
                        {
                            return write.ToArray();
                        }
                    }


                }
            }
            return null;
        }

        public static void UpdateHrmsID()
        {
            ClientContext clientContext = new ClientContext("https://portal.evnhanoi.vn/");
            clientContext.AuthenticationMode = ClientAuthenticationMode.FormsAuthentication;
            clientContext.FormsAuthenticationLoginInfo = new FormsAuthenticationLoginInfo("spadmin", "evnhn@123");
            List oList = clientContext.Web.Lists.GetByTitle("LGroup");
            List LUser = clientContext.Web.Lists.GetByTitle("LUser");
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Basic RVZOSEFOT0k6RVZOSEFOT0lAQ3VvbmdYMTY=");

            HttpResponseMessage response = client.GetAsync("http://42.112.213.225:8074/api/get_HRMS_ThongTinToChuc").Result;

            string content = response.Content.ReadAsStringAsync().Result;

            List<LDonVi> lstDonvi = new List<LDonVi>();
            lstDonvi = Newtonsoft.Json.JsonConvert.DeserializeObject<List<LDonVi>>(content);

            //get tiếp thông tin user theo đơn vị.
            HttpClient clientGetway = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes("EVNHANOI:Evnhanoi@123");
            clientGetway.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));

            //LGroupDA lGroupDA = new LGroupDA();
            foreach (LDonVi oLDonVi in lstDonvi)
            {
                //HttpResponseMessage responseALL = client.PostAsJsonAsync("https://portal.evnhanoi.vn/apigateway/HRMS_DanhBa_NhanVien", new
                //{
                //    MaDonVi = id_donvi,
                //    PhongBan = PhongBan,
                //    TuKhoa = ""
                //}).Result;
                HttpResponseMessage responseALL = clientGetway.PostAsync("https://portal.evnhanoi.vn/apigateway/HRMS_DanhBa_NhanVien", new StringContent(
                       Newtonsoft.Json.JsonConvert.SerializeObject(new
                       {
                           MaDonVi = oLDonVi.orgId,
                           TuKhoa = ""
                       }), Encoding.UTF8, "application/json")).Result;

                response.EnsureSuccessStatusCode();
                string responseBodyALL = responseALL.Content.ReadAsStringAsync().Result;
                var root = JsonConvert.DeserializeObject<DanhBaRespon>(responseBodyALL);

                foreach (var item in root.data)
                {
                    Console.WriteLine(item.SOHIEU_NS);
                    //staffCode
                    int IDUser = CheckExitField(LUser, clientContext, item.SOHIEU_NS, "staffCode");
                    if (IDUser > 0)
                    {
                        //updaet HRMSID
                        ListItem oListItem = LUser.GetItemById(IDUser);
                        clientContext.Load(oListItem);
                        clientContext.ExecuteQuery();

                        //ListItem oListItem = oList.AddItem(itemCreateInfo);
                        oListItem["HRMSID"] = item.NS_ID;
                        oListItem["_ModerationStatus"] = 0;
                        oListItem.Update();
                        clientContext.ExecuteQuery();
                    }

                }
                //List<DanhBaDongBo> lstBirthday = new List<DanhBaDongBo>();

            }
            //List LUser.GetItems(new CamlQuery());


        }

        public static void UpdateUserAnhDaiDien()
        {
            ClientContext clientContext = new ClientContext("https://portal.evnhanoi.vn/");
            clientContext.AuthenticationMode = ClientAuthenticationMode.FormsAuthentication;
            clientContext.FormsAuthenticationLoginInfo = new FormsAuthenticationLoginInfo("spadmin", "evnhn@123");
            List LUser = clientContext.Web.Lists.GetByTitle("LUser");
            ListItemCollection allUser = LUser.GetItems(new CamlQuery());
            clientContext.Load(allUser);
            clientContext.ExecuteQuery();
            foreach (ListItem item in allUser)
            {
                Console.WriteLine(item["Title"].ToString());
                if (item["HRMSID"] != null && !string.IsNullOrEmpty(item["HRMSID"].ToString()))
                {
                    //https://portal.evnhanoi.vn/VIPortalAPI/api/Mobi/UpdateAnhDaiDien
                    HttpClient clientGetway = new HttpClient();
                    clientGetway.DefaultRequestHeaders.Add("Authorization1", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImhvYW5udCIsIm5iZiI6MTY1Mjg4NjA3NCwiZXhwIjoxNjUyOTA3Njc0LCJpYXQiOjE2NTI4ODYwNzR9.XMpWvrllSnWJWtYR-dKb-Cx5l96CwQfv2vE67EWywnA");
                    HttpResponseMessage responseALL = clientGetway.PostAsync("https://portal.evnhanoi.vn/VIPortalAPI/api/Mobi/UpdateAnhDaiDien", new StringContent(
                     Newtonsoft.Json.JsonConvert.SerializeObject(new
                     {
                         ItemID = item.Id,
                         HRMSID = item["HRMSID"].ToString()
                     }), Encoding.UTF8, "application/json")).Result;
                    string responseBodyALL = responseALL.Content.ReadAsStringAsync().Result;
                }
            }
        }

        public static void CheckUpdateUser()
        {
            string testdata = "{\"code\":\"API-000\",\"paramCode\":null,\"message\":\"Success\",\"status\":\"SUCCESS\",\"data\":[{\"vid\":\"DF3843BBFBF0D740E05400144FFBC5CB\",\"nsId\":276000000000455,\"anhNs\":\"iVBORw0KGgoAAAANSUhEUgAAANsAAAEFCAYAAABqwJKrAAAAAXNSR0IArs4c6QAAIABJREFUeF7svQmYpWdVLby+eThjnRq7q4d0d0LGzgAJCcooyBSRwVlxAGfFe1UEAQVBEPCCMqgoghfQ/+pFESV4uYgJYUhCEgJkTsjUSU81n/l883Cftd9zKo3TL4kJpqtOnjxdXV11zqmv3vXtvddee22tLMsS24/tK7B9BR7xK6Btg+0Rv8bbL7B9BeQKbINt+yBsX4FH6Qpsg+1RutDbL7N9BbbBtn0Gtq/Ao3QFtsH2KF3o7ZfZvgLbYNs+A9tX4FG6Attge5Qu9PbLbF+BbbBtn4HtK/AoXYFtsD1KF3r7ZbavwDbYts/A9hV4lK7ANtgepQv9kF9mLKYrNUDjk/DvGlAUGXRdB9V2uVYgSUIsh30keY4ii9GoN2GbNjzdhanrMKDLN5owoBWA/HUi1JMn3n480ldgG2yP9BV+OM9fAkVZCKgmIEtQoJ8N4CABSg2u5cOAKUDSCwMo9TEqARRAaZSApkFDigA5lrobqDkeal4FHgxAvnf78WhcgW2wPRpX+d96DYlSKswUKKEzfMlDAaQscwyQ4Mj6MnK9xNHlY9i9uAhCJ1pZRRjFiOMUqxurmGpOYxgHqLVaaNZqsB0Hlm2jWWkKTituBW6pwddNvhLKvESYDGCaJlyruhnlSnntktD9Vl6Zk/K1t8H2Lfy1CtZKIJfDXSArgXvuvwfHjh3Hl79yA676/OcwyGJkWYZuvwctyVCpVgQorllBqWlAWULLM/h+BZZn4/DyEdiODctxYFgm4iCEZpmSblbqVew+ZS9MS8eBxVNwyfmPx5Of9FRkRYr2RhszrRYqZmUzin4LL81J+dLbYPsW/lozAPd3j+KOI3fibz/8v3DPrfch6Q9gui7yOEVZFIgMDbppYtjvw7J8FHkELdcQ9yNJHR2JYAbyvEBWZDBdE0WSIwgDlLoO29QxvWMe9WYDpmEAugHTMRD1eti5cxGrq8totlp42Y//BC65+GLcdffX8fizHj9OTb+FF+ckfOltsD2Kv9RJJMs0IAj7+PBf/Tk+8KEPYm5qVuqvOEkluo2GfRRFIdGoLAwUBWAZFpIkgW1bKPMCJjRYloUgCOQn4Me6YSBNU6RpDE3XoEkqWsK21J9RFGFmZgaVRh1pFKNa8WA5NhYWd0pd2O12kBYB5mZmcdGTvg0veu6L0PCaipjZfjzsK7ANtod9Cb+JJyiBUCvw8c98An/9V3+J+ZlZXP+la+F5PqIoFsBppY4wDVGr1QQsve4IZakhT3OJYqzlijwXhjEMQwEZAcbaSzN05HkOz/Pk7wQYn6MoE2RpJumobZowLQPhcIS52VnMzs2hOxiiUqnANA006hZ27dqFdrcNvYTUfj/1M7+IfbsOQNcIcuub+IG3v/TEK7ANtkfwPJSK9oCWFSh0YC0Z4H+86504evgBrG905JX1XJeDPhoNsHr8uHyuUqsiCkKkaYJmYwHt9gaSJBIA8cEIZ1mOgImPomD0sqBpukS+KI4EXLquoVKtIoyHAlR+P/8nWPm9vutIZLQNU6JbXhZIghEOnLobQZpg0B9g76n7kacZ+t0envncZ+G7n/UCVHQf0Eig5IAwmtuP/8gV2Abbf+QqPYyvIeDaUQ8v/emXIys1nHLKfiwdXUYSjjAYDNBd25CIo2saDEOlfkL3S1oZIksYiXRoui5p4AQ0pW5IlHNdFzo/ZgU3/nsUhQKmXq+HNI5hmcDMwrz6d8fB2sqqPE9rqiGf4/9ZkcPxPBRZhlariU57Qz5/8OBB3HHHHah4PlBkeNLTLsYznvp0nHPgPBia/TCuzNb71m2wPUK/cxL6x7vLeNVvvwZLh48jCTJEcYZarYksKdEfdMbgMkFw5EmMKOgL8PIsg2Va8H0HaQzYrqvabJomaaJhGIiLEmVZSBvAq9Xg+778W2u6Bdd1BGgEZxTHMLUc/W4XOxZ3CqkShZH04shiDgcDGKaJ3Xv3wrQZHQ30el04jo35+QUkSYxep4sLz78AN3/ta5ibm0ZnuI5fe82rcfDMx6OuVSYtwEfoSp48T7sNtv+s3+VY2cE/YpR4/e//Nr5y7fUIgyFs00OnPYBre0jCWECR5KlEnRI50igREsK2TYlUBQwUTBHzBHleIo5SWOPopTuWUo5okOhEcJIwYZ1mey58z0Ntqolutyv/RhD2ul2M2l32uAVktmUiyVJYlgHLNDEaDBGlCfbu3wffrwr4kjjEcDhEtV7DqNvH3h2LOLZ8DC980aX4xN99HDMLs7j4OU/Fj7/wR7GjOvdgI/0/63qehM+zDbb/rF+q9MuAv7/ik/j7v/8E0n6Ije4IQZpJ45pkRjAMkOcZNJ0NbEgtRIKDvTLWYZsMJFnIskSRFKrp7HsodaaZBuIwkohFsBBIBBz/PY5j+Xu/30ecpvJc+/btk+8hmPvDPrSixKDXF0Kl0WggzTOkMes/S/p9o+EAjmeiNT8H27YF+CvrG2j4VWQBa0YdrYUmPM8RUHfCPk47/0y89hdfixm78Z91JU/a59kG23/Cr5ZBqJeO8DvvfCu+eu2XMRpFsEwXpuWiOxwgTRIBj+O5MDQNwWAo2sYyz5HHiqzIi1zqsjTNUBapkmgZuqqnWNOVit63LRemY6M/HAi4yFqyJc7IlpW5fA2jEf9OoK0cW5Iajf1v27TQbrdhO6a8BxSq5ksyFW0J3DgM4PkO/GZTyBWCvL26hoZbQRyOAKPEU5/5NNxy00248NzzsbaxhnMefw5e9UuvgY7tGu7fO07bYHuYYIuKDJ+57krkZoF3v+Ud6K634bpVhEkmNHmmlZLm8VB3Om0hGVCUMJgKZjmKHBKVCAxGI0kNcwVO/n1CYPBt8nOGbiElmeF78Cu+gCqO1Ndrpi4g7XQ6mCOl3+1KVN23Zy8Mw8I9d90toEKZC9gMTZeIysjI1yKoLUOX12cqydeZmpmFa9kowhiaVsKv+VhZWcb+U06RyHjRJRehUfNx0RMej+c884Xb7OS/c562wfYQwDZpTidagctvvBrv/f3fR2+tDR0pRiEjk46pqRm0N7pIomAzPdQtExkPbVEiTwsBGSOR9MjG5AcPPbJUsYuWrah6HmGp5zw4FV++hyQI00F+vHp8CcFoAOSFvJbh2JiZmxXQjQaBgMrQDezYs4gky7B2bBkW322eCvjyLJHXI+DLkqmtjTRXaW2lXhOmcsqpIc14U9BgmJDvW1xYRLVWw959ezAcdfDmN70Fs9N7H8IV3Rrfsg22h/J7LnKsRj1cf/tNePOb34KK6aG0SgwGbQw2RigKXSKWqVuwDJUGMoJopgGjgEQVx3IlGmVlIYec1L8OxTbyIT0xT/XSChIgFR/NRhP+OEV0fRej0Uiely2E4aCHwUYHQRjK6/E5GfXYj0uiWJjHKInhVStYWFjEkQceQJHl8joEG1+H4KX4mQ9rTMToloHWzDTSXihRkoBjf21qagqjfoDW1DTqzTr2H9iN3Xt245W/8hoZ5NlmTP7lwdoG20MAWy8d4rM3XI0P/On7sbHSQZiWGHTXUPEsWHYFxAsP+ag7RJmnErV4kHO2uPMSWZyAE2YERXN2GmEQsjMtB5TNaUYmdfgTqcnkQfmVbsBxXSFbyizFaDgSIoOt8zLPJAUlABmRSOnzOUzTkecs9QxZQilXCt2wpHHu6Da67Q5MQ8m5pMdXZPI1VKNIpKNI2jAwV23Cr3lob6whL1IB8nRzHuvLa5jfOYcoCXH+4w/iZT/+cjz+govHA3MP4eKexN+yDbZv6pdbIEaBz197Nd717ndjY20NWVGI8sI0PKmnSKtvbGxIhGKdFo4GsAxTRQ3WZ1Ekh741NyMSLVLvbCR7vo/CGKsQpYfGYRcNlmsLe1nxa7ApJDYM6cMZOtDv9eXjTrsttVoOTdQoCft27NflbCvEQrRMBk35J9NXPn+92hASZH1tRd5HHMXwfE/aAExJBYBlgdKw4esFGjMtGdsZdFQPL9d0LMzMIE5GqNWqCMMRfvilP4RffMWvwjW8bU3lPztb22D7JsCWIcMvvfpXcO31NyAdhfCbdYkkjAKOW0OZluh1SJC4ErXILsas2VJ18JlaTk23pB/WXV5TYAxDtOZnEUShRAuCYZJOFlkBx7UxCoYCEBIZ/HrS9az7dNMQkAajkbzeKIhw4MABJHGE1dWVB1lM15FoJ/29JJHXYQ+NxAcBdfYF5+H+u+9BnqhUkwwkwSbprK4h10zUHUMY0/rsNExNR2dtXUXmHXPQ4wJxFsAocjzj2U/Di37gR/GsS57xTVzZrfGl22D7//k9TxwEcuR43osuRTQKMApjzO6Yx/HlZdSmGgjY9xqoBjJHXXj4qRwc9DpSEw36I5iWhd2n7BGGkBR8MBwJ2FzLkfSSNRnVGkzl2EsjMAR4tikyKpIjfC7p1wUBHMuUr2EayTEZgokg5fcnJ7CbRTphKkmquPI9jEpkQvk+JGUsSuw4ZY8AnvUkWUZ+Tm4YjGymCSONYLCnx/fsedLOGA0DkUju3LmAzsY6dszNQjM1POvFL8brf+lV28zkdmT7Ju+iJZBqOS5+2pNw8KyDaK9uSNRIypxtKqmX2ER2zaoiNXRNGsdM7Sh5IgjCMMGZ55yFpaUlqavYzHZMG07NRxIlqg1QFCIQZuThg4eaQ6CFrsGvV+FL66AjEXVlZQW6QdGxLc/HKMXvZXOcyhPRWepkFstNppGCaD4fG258DfbtSJxMWg5kNalKmduxgOOHjyiZF4XPRS5gs8sUGp1MTEPaDtRYDrp9adK7ro1K1Yels9/n41kv/G686ldfDV/f7rudeNq2I9u/gz0CiWPLz3/Jd0HTTQw2+jKU2ZxqICkyqbHiWLF/jt3EsNdDOFApHQ8xm9hM//afegD333tIDjgb0s3pKYkKeZKiMdtCkaQy8pKwf0a631I1Hpk/HnqlLlEGPzKZTWWxbqLCr9V0BIORNMut0kLm6CLzKpMIrqEJAPJUh9uYkp+UZEyZZgKkKI9gmDqKMkMSK5BTG5lXLSDKkSY54jQX5wadL5mrMR2ms9VqFb1uT94Ph1f3HTgFUb8n7/Wipz8Pb3jrb2LOm5a5u+3iTR2ybbD9u2BL8eu/+yZ8+aprkSUZSs1EREZvlCDNc/i1KrobG7BslRYyLYtTVRMxorGeIlEhozBFKUJfEhhSn1ku2HdjPQaO4JQFYgr5swK5YKqEZdoSvSSlNDSpD5kCFrGaX7M1XdhNkyGJDW8D8Jo+Kkz3+NyddXE38b0GSqjB0snzyXtiw73MEJLsyHKpyTgL1NZjpFEmkYzjOwWBR8az1DaVLQTbKBggyyIhcOo0ELIs1Joeao1dePNb3wzdKNBo1nHG7tO/yXTi5PzybbD9W7/XEnjDn74d13z2SolCSKno0JBmhijoCaZGrS49LgJomERUMsnn+X+9XpeD2V9ryytUpxoyIS21VcEQZ6K3vCYyKst15GsYLak2IfCozM9jNdQpcq3xsCjTRsO2EBq5vJ6Ry8ScRERXN1CvVmGzf4YSDHLQOEzqQzcdeQ7RXxKgpoGIzXMCjn103hQApFGEDaaOWY6V1XWE/JoMiHNdmtkZZV1jwoX6TI4QGaYGx2BdaWNmdh7+TBXv+cM/whkLp+CKq67As570DFV/MjJv4cc22P6VX35aFvjd970NX735FgSDAFph4OgDR+XjIIxRq9WFAmfEYa3D1ClPSRiMpH9VrddFqTHsdtFqTmF6bg7tzsamXlHnGEu7LdGJM2sq8gFREG2qTZiGQtdhmXyNEkVaSK3HmsmwLEQWYGkGbPbmdBNV2xWLOlfPUHFZU1mw9RK+5Yj0Smq68eAppVvSYDdtARnfA38GviZ9JwdRiu5oiG6nj9xyZHB0kKVIWKQSkGkqX6vRaiEbweZEgm5i79690jx3WnX8zrveCc/WsWd2N2a9KVGxbPXHNtj+2QnIyhLv+9D7cNknL0O1OYW1pWW017uIglRcp3SHccSSGknYunFvjGoMNpx5+2cK15eZMBfVRl2iQbPWVFR/MEJ3fQ1hrA43R2xEJZIVKDNFt2uGGp8hY+HoGRNAWqvCEgAqaVeCDK5poWp7omdsVKtwbRs1C/C8ikxhG+y8kTChSauhbBJUo9oQYofKFCXRKhAniYCIaeowyaV+7I8CDPICy8urWBv0sTFQpAlZV4KTUTgpAjiGJZPbU62WAL++MIu3/eHvwSpznLf3bF6t7bJtu2Z7EGnjcTR8+Y4b8Ru/8VrEQYIhbeSCCOEoQZFr0k+jooL1F5k8Hthqrb6pmCf4Gq0pdDfawgjy8BEYvlsB2ZEwiRG0u0iySA6qOuglkjyTus0uDVg200SeXUua2r6ewTfUxIDvu0LKkBThfJv8iQKWYY+jmQPbyKVB7VkmHHa+wTEdW4gZMpUEi66bkoomcSY1HImYIk2lPxfnZE9zjJIAfUa4KEJYFDiyvIz1foJer4NwFCAio6qZKI0UhUZrPb4/H1aW4Snf9yL87Ct+AYszs5jSWcuRlZw0UbZufNuObBNH77JApuV48Q9/H1aOd2C4JpIwQq/bh2kwmhgiME7SCGF/QLsrzO9Y2Kx3eIR4cEVKVZaYmpkWbw9GC1H8b2xIb0rkUr4DnXURI1oQy3NZpoGqYfBDmBrHR3X4li6AkZk1ryIiYE/s6SzotgOLH+u6yK0c05Ln8CydbnXyXsRxS9fhSPSzxUeS0quSxIppyL+JAForkGcp4mCELI3E+JUkDvuJQRRjFEfo9YfoDBLct76KI2sDlKwKE9aemVjoea7qAU41mnj5K/87+tEQv/AzP42mVpFop+zPt/ZjG2yT339Z4kOX/2/8+Qc+LHVZ2A2BnGxcjjiORLQbjUJh56iKn5ppSfPXcz1hCWXUhUObaYade3apKDYKUHd9bKyreTJObbcac+ghQZFGqtYpSjgaQaajZnColGkj0GrU4XkEC3t3NhxDQ71aga7lAhDL8mBqVG9pqHr+GHAEogGLzW3ThON4yghIN2VagH8SaCZ7fZwOGEdWGTBNQhR5gjgOEA8jsfKh8n8YBgiiAJ1uhPYgwPF+H3fefQj9AcXNFobxCHmRwLLZdFfmsOc88Uxc+pLnSSr69Cc/FYsLCygCDVO0xWNmMHF+3mIeedtgG4ONMuFLf/QFQKqj0x9gsNGDpflIkwj9fk+azkyTSCq05mYlKsns2MbGpuyKXiFM96hHHA0G2Ll7N4btLnr9jgCkXq+iTIBRGUhEEvKCukqOwEDDlKkikuMY8D0bDpvkmgFTpyyLCzI0uKahTH5MF5aly+dsy4ChlaLBZAppcoLAtKR2kya060ubgWBjzcc+GdNV/kwikhatpSY/K31Q4mEgs2z0n4yTGKNoiI32CJ1RhCgrsLLewU23fF3sG1YHPVBdw6FWRn/6UFoVE7OLc3jmc54lDXj+HGEa4PxLLsFzn/wd2FXbLRMEW83ifBtsTCOLEq98x2tw7+334sjRY8jyUlJI16xhY2VJmDyvVhXBcbXelMPMlIkNZ9IlBB7lVpVaDUF/IH2r6dkZRKMhBv2+0PcS+WgFLg5YmvTIOFPmW7Y0uzlUOuWa8BsVNF1XiA01ma0Lpc7IJ/+z/hKPSEvcjlmWkfbXNZIfrOXUfJxpO7AdT1JH03FFp1mpN+RmwQkCHnWPgON0AftrbDnkBdJwgCjoSeRLggBJkiJKAgxGIboEYVRgrdPF0eUN3HvkKFbCEaK8hO3acGwXa2sbmJ2dljo0K1MZYj3tzDOwsnIUjm9h5+6dqFaaeOOr3wRX87ZUXrmlwaZIkRLLow385E++HP0oAjINw5EyP106clRWLJGCp333pE/FP4f9gZo9Y8+M8irfUwwd/RjHbB2PNPtVwizSCCiLUfeb8LIBbMeCz6Y16yzDEsFxw2UT24Slaai4DhxGLKpAbEX3k20koJiJySiOwX83YFPVzzghrCM1kB4KFDK7ZpEh1U1YtgvL9ZCSiHGdzeY2SZ2Joauo/Jk2j4YS4SjVipNIbiz8v0+zojDFoB9jPQxx15Hj+PKdd6lBU87FlbwOrjCrHDHKMjUixKZ/nIYoGXkdHU/7jqci0TK86VVvgpPbMtu3FR5bGmzS4NULfORjf4XLPvEJ9KIIo26ANC9w/IEH4PNwJophZIqYUGmR5zIiQ7HwiVYGk5k1Rjh+nl4fjTrXOSmjHv676ZgyelJJ+qj7nhxEgssulGdkjYDyHHi2JQCiXyRNdiyLDCKjGk1WFRMptZwwi6pOY0rI3t/k82ROOV1AsOXcXlOpoWCvSyfjaav3Q4DQXZkWCGNXZU6J01YvjUIkcSCza2wHyABsmosz8+pKG4M4xlKvi3t7PVz31a/CcarQTRuDQU8+bk7NYmONtSrn8Eq05qcRDAY46+A56A36WHzcLsyfsgsve/FLMVedhquf/IDb0mBj36yXDPCyn/5JGLaD1c4G2sfWhQQhsDhqwkPKOp5Kezau+cjIKHJ+LFdkBdPEQRKJ45TvuOh3urA1A4ZrIzdy2EaBuutJA5rp3qznoeJaqFUrcGjkY6raq6JZsqXG8ZTbFVlHPhhlheqXSKamuBVJoqLbhFWcfGyalrCOpPsd11N8BBlBip/9ioiRReNoK1Mg6ReyzpOfieSN0mLmKY2AqJsMkXIqnLt2DAP9dhf9YYDRKMTtDxxBOweuvemr0F0Xo36EiuujMTOL9bU17Nm9G+12T1hct+qiVq+LrjKIBzjrovOwuGsvLn3Wc3H6zL6T3n9yS4ONq5Je+7Y34NY770IcRDh8z32ybGJ9fR2AIz4ew8EIju1IGkX7APH4GC+wEFrdUXUPlxRKDZTlouwwKaWyDWgO4Fqk5A34tKWzTUy7LjzTlNdiA1zVXQZqhrIIF0sCTdH5fG6SJqIo4QybrsA3eV2+h0mEIzCVLEr5/rOXZlqK8mcj27Qo2TJlmHQC1Elkm3igSCpKG/IkEv8UXZrfnN6O5Rqw757EqTCvWZqjHYQ43O3hxrvuwu333I88ZfZZotpqCag22kquRvs7t+pj1+7dwt5WanX0shGe/p1Pw3q3g5//nh/FzvpO2YhK49mT8bFlwUZN31v/6J348vVfQl5YouanWWl7rY2ZmWmZouadm4wZZVjCPpJIEAcOVaedeOCpb6Q/v6ub0l9jJLJdA7pBT31TarBGxYPvWaiQnNDIMDqoeK4QHcIklirlZD3H5+BBn/TvuKFGAhQjmq1Iksl7OPFgqjqOkU0XBlJnj3DcW7MdV2wSJkoSlqNMTWW2Ybw3wDQssVwox5GtyGgUm4vdHaNtlimjIvkzSdDu95E6Dr50y2349GevxmDIBnnOsCy9wbMuOA933na7GMLuf9yp6PX7WJifx/TcDNphB7v3nirX/bnf/Uw89/znQNkbnZw9ua0JthK488jX8Z4//gNsLK2jNjuLI4cPS+QglU91Bw9ft8N1TDmyLJeaSHl6mJtOwzx0jASsvWQkJoiEzheKnimcTbVHhnq9hplmFZYGeASd5cA3NanXfK5tYoqoq7qOs20CGLWSQ55fot24QS37sccfT1I/Aks+Hn8997mJqJ91mUv/SkfJwkjNS72m9gTwtRg1J1Z68voEZsGhUdamXCWsUmVGNqaVAspSx2AwUm7NaYBQN/HAehtXXn8Tvn7XEcRRjkE2kH4aV2Cdc/CgDM22+23s379fPm5OTWH/aXsQlynOPOdsfOqf/hG/89rfwRNPO/+kHcnZsmC79djX8bpffy0MzcTsrh1YOr6EpWPHUatUsLG2LilTkZsoilTGTzi5TB0uDx4P6ESJb5ljpjEciRi4YpO251iLB88CmlUTrVYDFdcWLaNpG2hRMCx2IrqM3RBorMckhRtPWzOqqd0Zavqar6OGUyEiYtNQJMdETc+P+ZDPUZ4//vskukkk1BSg5ftZ9+mKYOE8m6SpvLWkrOX4c3M6gFYOdNzixEOGMo3HXpYqVQ3pzhz0MUxLbKQ5rrrlLtx445048kAbsTmAppkYDgLs2rcXFHef9rgDsqTjrLPOkkFYigXOOXg2zn78Qfzjxy7DL7/x1/GMc58C/STd8711wDYWP1LPeMu9t+GDf/ZBHD9yDIN+iCjPhNoPwwCrx45LqjYY8rAYyBPOcSmfRCrXae1GUS9nv9g7o5UBZVIulRlM2aJUTHQaro+KCzSbJlpTTdicYqZHpGOiZnlwKMmiPGssm5ocdqpCxElrHJkUUWIKwBW1b0haOAGhinwKeNQfyhgLlyASrGQwGdU0XQTTXAtsWS4cW3mPMOpIKuqqmwcfsp1U2n50AAN0ppNFKqZE1GVKe2DsDEb7hXgUo7RtdOMItx45jutvuh333L+OpdVj4rnCJj+7Iwt7FoQ4WlxcxJEjR3DK/n3y72WRozEzhYsvuRj/6y8/gg9/6M9w1q5zZZnjyfbYMmCbGKteffMN+LM//zMcPXIEIT3we304bhWtmVllN8DF7qMR4mgo0YRiXTpiSQQxDfHzT1GMI1iJLIml51W1crgaxb8pmraN2eaUKD0aU42xGoSSLA2O56BqqAnrCXjkuSUNpKUB00fWetQwQgBOtf+kpyaRa7xn7UHAqfqNYBPQ6eMhzwlhMhECkyQxFYMpwBLCRP8GNpLRW6IercAY3cYMLHuGua5m9XRNpdKyhpjSLsNApBdYHgxw5Vdvxi13PCB7DmSCIAwwOz+D7nAk4mheE96kmHbHYYKdO3epRn/VxZ49i3j5T/wYTttzKiwySyfZY8uAjb83Ho7rbr0Rv/fudyLNdfR6G+j0ezKnpmsO/LENN+/iZOOYUkVDkiS0lBsPeJY5dFemKFFzfRRZCUPLYesxGraDmmlJyjgzVUfN8+HXbVScCjz200xD6HyCh20AAcYJNnN8BWoHS0kplYiYKSlTR1Hqj1NNRqhJ727SAiDAJg32CZgEfpwSMKkwYURUkZHPxefmDWgy0MkikKYKAAAgAElEQVSaUFJH5JuMK8FGZlIsGbRSnJH5INgkwmmGAIoKlGGRY7nXx9fuvQ9fufEO3HLXUdlU6tS46DFCpdVAzXFEQE2bBdM1EMUpBr0hpptTGHRGeNJTLsbS8UP44z98H+YaCycdJ7llwMbDEaYJLr/+alz2dx/HvYfuRxQF4kgcB7FYBxBsrNeigJbhmYiP0ziVuTTS57Q4YO8qTEIREcuEtG4iizNMeRmmfB+NagVVz0KDTWvKtFw6E1dQdRnNFClB5pEECN8Te3mMJAKerJAGsCHRR6V6hAR1j2QYJ/00ppETQG3WbGNlyeTvUn+xYc2akBIt9sgYtdhqYE0nBZ4a8dkkRvh6IphR0Y5gI6kj6alWbjp+0R5B3nshbgwiyB6WKTpxjOvvvBu33n4I96/0cOzYEbGJaCwsyBd211axsHuPtEaCaAjb5RYeT0aSOE2eRyHOvfBc/Ml7/1ikbIzwJ9NjS4CNB4PpH7IMh5eX8GtveJ0siGAPaDAcSvRiZMs4jRxxKWEgPTVd00VB0pqexjBgv80WuwLLJi0PORA8toZmoVZJMFOtouZ7aLgOmr4r4uGmp8TJlGBR20gdJJ2vxuvWBGyK6GBvjrUVoxtTTNXQ5mwqwSa9szFZwmbbhPrfJElM1YebECUTMDJaMXXk8wtraRqik5TPcyPOWIXCCMW6cDLMKjcA1n60utMKYSg3vSRNdSMQYpLtgKJAPw/RzzLcdOgB3HLbfbj7eBe9QRdrK2swqz6aC3MIVtfhVqoqY4gD0W9ajif9xEEwgJ0XeMKTL8Hln/4MvvT5L2JhesfJhLWtY/jDQ/Gpz38KH/rQR4SyDpJY7TSbaaJzfA1JpJy0guFQ+TZmapI65eJ40udcEJgk8qdjm8p3kbYEBJpbQaOWosWtMq6Jhu+jWamINUHdo/7RlBEYpmSShrFGMx6snTY/N54vY8Qc52tqKcaYWRTwTRQjvOuzG87IOAbsiaSJAoMiSdTsmkodCbTJLBupjgk4Jy2Gya4B+VrWkAIotj5ImGiinjEspTyR1Hz8Ou2wh0GR45b778eXbrgNg8zBPffdh9pUC8eOHUZpWnjc407D0UP3S7M7SRNEFApoFqZnppFxuqLbQTIKcMnTnoxf+oVX4Enn0cb85HlsicimTgXQz/p4+U/+FA7d94CoGarTU1heWUE6iunkIZthBlyPS3U+RcRliVq9geFQ+efzgC0sLGDQaSug0U3KdzBdr6FiRKgydXRt1D0XNdcV6ZZtKgUILeUIoU1AnJAWbqZyZA3H0WtCdEzYSNZpqpemwEpGkFGRwGGdJ2loqZrBkzps0qhmCjz5nEQsWylTaGUw6R3yuTfBKT6WnAQgQ6lIFw6fEnm8DvxYBMwcyxFrPQ/D9jr6RYoH+l1cddOtuOPOwzIpENDRq8rdbgm67R5OP/ssHDl0P+ikwFXEbL1UGjV53qjPhY0FLvz2b8PpZzwOb/y1N5w8SNtKtggs7akaoQ7yeGeIPC6wurYsOsB6o46p+Tm0l5dFPBsGgRwwMVRNcniViozXTM3OYrDRlkZ1VXcxU/dQr9io+T7may489tIcCy4FxeMI6BjlJp0uUWXz/wfTwhPBNjn8k1P2IBGi6H5a00mNpalemgBu07lK/dsk5ZSyjKziuD0wUY5otqrZSMnzRjABmgCcewZkUo3g4/5uLtsopNbk60gdKO0Fai115Ow/wgKKEMudDpZGAW657zC+cs8hESxz1ZVuuej0+jJ869cq8nqSSgahrB/mdVncfwp6/R7izgCGZ+Kqq6+Bw+l1u37SNLm3TGQj2LIywwte9EJUZuexcnQZcZwgHA7USttaBd2VVSRpLBtgJt6PJEd4KKnmp84wHnZlorru11H3LUx5DmqVGqYJNscWRynqHT3bFj2gxQAy7k99YwNaHd7NiMLINf6PbseTCLcJEBZzUsOp5pi0vxmBTgCbTEFLZHsQyASniooPkioEm4B4rFqZREB5f5I8Fpvp4+Q5GZUnwKe5LGutcagWRy6tyDDKMqwOBrj1vqO46tbbsd4dodPvIoch9Vm10cD9d96FmcUdiIYjARsHbWlSy7k3miPdd9fdMgrUWmjhY3/z19g9s/ukEShvGbDxBn/74Tvwvj/4E9x07/3IhoGkQkxlGvUaBmGAqD+Uw1YI1a7AwK+hIJkbXth/q/smpixgfm4Oc9U6fNeC79poVStSl5ljW4MKJ6s5h5aTXRxT5UJUjKVVUgspQmMThHTvIos4rrUm0U32bo9T0M2IN/4+Aas2Vp+Mt5dOQCFAHddlE/XLpOaTVgEJGzo3W46KNjlVn6oxLv20UtnnkavnaM9kXEgMJPk+GfFsS7bnZCGZ3RJhXuK2ew/hylvuxuG1VXR6bcSZIcsfHc8XEiqijUS9KmRUyGtOmVx7HfOLO2UWsNPp4byLnoDfesPrcfDAWSdNKrllwEbQfPhv/wJf/Kcv4J7j6yiSWJa9q8MHma5mCOOBo9aRciKxbatURI3BGTa2AubnfOz0PDQaNdQME42Kj0atKsQIaXoao4qwWKwKFPs4ARgP6aR24uc32cVxncWvn0SZCflwYj+NXy8goJJk/LHy6H9QUTKp2TZBPH79seXjCb06iFCZNwKJZ+Oa0LQMtXFnDDYRH+eJXCP6qwjo2Ssc6zO5i8CwbCS0U5DFdD6Or67i/95wG2675350+wP5vMadBP0RZudmcejrd0nLhaqdaDCSodi15SVZPjK7MC9xmzH6zW97K178nO/aBttj7woU+O33vg1f/sJ1OL42FNu2ycGlFVvQ7aM2PSUMZRKEiq4fRxR+jkBkurl7sYFFHZiabWG22kDNdeC5HGUxRLRLYx5FEpYwC6UCoVWBCHzHrKGkg2PGkYCW1b4i01LRQ+qmsQr/GwiT8UVX6Z4CsXzdmGmcRL8JoPl3ipwlgo5lYfxYfi7uAKBZnvT9Hoy4rMMmQOfUNkXZ4hdCEmZsAcEhVHkeRjhD7YUjO0uyJMxzSR0/89VbcNvScRxbWkeUFjASIKKzs2Ug6g2kLq7PtESxE0ehGNrSX/Pg+edhdXVdGOHv/v7vwdt/6y1KBvfYO3D/4h1vmcjGYv9Vb34drvnitTCsinjs6wVETDsKh9i7dx+OH1sSFb5aNt+B7xlizhpmOVzDwlSjigN7WtjhO5ifbqFVrcIySB7Q35HRRfXIGHXEf5+KDdEoqrSLjWkBHYW9lFHQ/o7p3JgJJJvIhrHsxhmDbZLSbaaPY8qdHiUn0vYCMLpnCanBmk0BYkKoyAaacQorQmSbgFOfm7QHVO9OgY3KFYKMfpKy+ld6bapulKgpsjKSL/wJuey+FLBFeYFep4OrbrsJ19x1D46uDaCZNmJOwI8GyGwT1Wod60sr8v5oy87dBbI3IIhQn24pawfHwbHjx7F8fEUt5zgJHlsGbIf6S3jze34XSw8cR/fohvhjhMMhqtWa+CRyfo1qBoYYTmW7Nl2yuhgFAUx/Gq1GBYuNKnbNedg928BsqyXCYlsDHA54KqSJkt7gEKjhQDe1zWYwTYU0Rr9NZf449eO0pXxOAqMQGRNSQkA5dsA6EXwKRKrm20xRJX9kmsrPqzpQajZpiKtWwWYtJ6mfKQ5c/JwImUX9/2AfUAG5QJYmkhLzPbE/OemrsZVBsLFXR7DJzFsJ2Q3A2vam+w/hU1+6HkfXB+gOQui0fkCOqOAeAlMcvZjGT8/OiuiYzfQ2geW7MsJDhQ7XV33p6mtlp8HkxvJYxtyWABvz/88duh6v/ZVXQbN8TPt1DHoDRMOBTA3zUOVcmNHvw/erMDjE6TkI26somWZ5NdQcEztnGjhn/w6cc/oBUABFKZMvhzKHJ+ycMtyhQoQHZrI6yigKZOJnNZY5ESTmpA3Ary2kv8S5uUkkk9RvTKKotE6Zuk5qMbWDWz02mcsTenSbtd4kQmpjPeTYgpzRje+Xmk/26vgxVSaT75vM7ZVQSz3UVIDqNVK2xsjH2lSN4ihpGPfejOjWFUS4f2UVn7ziKhxud/HARhtRmMBzKAxIUa3U0R30kUWx1MTc90Z2knNufJ5qrSa603POO4i//ujfCthOhuC2JcBGeW2ABC/8kReh1pjB0XsPY3Z6Fr2NdTnAHIwcDlLx22ART08Rft7RcwQpN4pWsdD0sWemgW97wjmYnWlCo0FOqaFKqziTdZolPiayy0zjnuwSlqPU8fyY7+FEql9SszFQ6PvIZfXUPinFhqLwJ0OsE7CpPyfUvwLaBJQSySj4H2+K+efkyySNnLQSpLHNVr5HD0pLZuBs25H2yIk2CzSp5S4APihTo6yLLCXn3HTqOEsNRaxuBATbgBYKoxCH19dx/U1fxxXXXY+hbmNAs6SYVn4cRbIF5L12R8Bbn2rK93NHHdsu9UZDLMt/9hU/jxd816XYv+vAYzmgPXhTLE+kv06KH+lf/yESpPjBn/txrHba0IYZ2p2ucF78BfMQra70UalVYHN/dGsKnbU1sTQwXZrzeJivW3jimftxYM8OsTPwDFN2knFsRqzEaXpn2uK/QWsBUzeRsN4Rpg9SI8n+tvF0N9VWk0qEs2OiPWRkGwNIFjGO0zbpNbN5LOMt4yG08eQ4f1oOt3IOjb6P/HMyuX2iKkRtJFWaSh541lq0tMvZIKeniasck23PkueI4vGeuZJbdAyJSEwl6Y7MZLjkwsUiEwv0IsyE3OGi4SBLEIchltbbuO6Wu3H5VddioygR0mtz0J/EYrmBUa5FIoR77mgFyL7boN1FrVmFlum44JIL8W1Pvhi/9vOvPClO5paIbPxN9coQL/6h78VaewN+wfW7ufxyJRJkXA7hycII9n9Ihw+7HVGNUJq0c8ccFpwS55y2C2eduleWFc41W7A0HTQcKJIIllNTdd/YhptEA+MBBze5OFGa4oYhjXS+JpdZiBKjyIW9jKIQZkZan++2kDaD9MLG6n1mjapWU6Cb1E6T/hgnCsaWJd/Qu/sXdd246c1IVvJGIcwkXZM9sVU3HMXCprIokbYHuUwKkIm1dU6tp0jDSBExfEGmtwk9S+hUyVVuhuysG4QxvnjjnbjymutwZBggzgqxxuODwmgaB0lPbW1dFn7s2LNLFkqOen1pAZAUefbzn4cfedmP4KmP//aTIo/cMmD7P1/4NH7vne/EsZVlzM3uQDgYYn1lDV6jiuX77sfOU8+UKevabAu9DTpCFbKsvtaoo7azhT0G8NTTD2DXbBNuqyn++twGSoEu1fwlXLUM3nbgOL44F/MQjegvSc0iqfexoFjpC8laEuixHFgKcy1KVTSuj4okRZ0YCwlRorLOTZBxtmyTjWSMnEjwhU98UOd4ImM5SVv5J6Mwl82D/TL2/wymwxY0uyIzcAQ6I/PEeFZUNrYm/UkyofRSEcCnsewf0EsNaa7e53A0RJLluO62e/GpK67EfcsdDE1901aBUTrXdFGNLN1/WFoi0/NzAlLWcQQbbdZf8pKX4Lu+/4V42oVPPSmsyrcM2D722cvw/ve+F1lQYD0YwLGo/PDEQnxtZQkzc3sQ9IfYsbgTRw8fUeJbTUdztoVu2MMTF+fx5DP34ZSdc7ArNZlr09mrKmJZcFEaDiyrKryFIWuY1DJ6zmnxTwKRtSDTSKkHqbzIU3imLb6MVL3r4t8oOZpsDpWUlAsMCR+qOajwGC+l+MbsX2ki6RnCOk9UMOPmtxIqP6juF/CNJV6lbogPJP35aUzLCXDoZCZNDKOhLAdhjUbpGl87jwNUPUdkaxRhy9Yc8VApRKtJlQj7eqxTu4OhaCQ//qnP4LajKwjJxHJdMV9T3M5TSR9Zvy0fOy6TAHwwtWTNW/EsvPK1r8SFFz0FTzz3/JPC3m5LgI0H7/6NI/iRH/5BNGszspK3vb4uG1eoy+MvXstd1FtNSS1ZR1AtUWl4iIcjzM20cN6+Hbhg/w4c2DkPw2F9o+ofrpkiq2GaVRRQPiH09kjSTAHPoLe/Kw7JBBsZT37fVK2K0WiopguyVGoxiz4njJIEijgVG9DyHFGpwyiUqmPSBzuRtWRKN4lgnDLXxrXeJJIJ4UFLBwEr1zyNe26mhaQsMBiNRLtIL0hG4lEQibqENd0kDSXoGHWjQVdYw8UdO7F3zx5lIMsbl0PKvmATAFmobO/uPHwMn/7cVfjcjbeiQwt0iw185WHCNJpOZOB7CCO5GbVaLawsLwsAXVvHj/3kT+DHfuJncWBxlwihH+uPLQE2qYLKHE+/9BlsI0stNhoO5ZBwhW2r1USWW2IzvnrkuLQDWE9kSQDfNbCj0cQTTj8FZ+1bwP4ds4r1Myxl502qn8Jel4ZBMW6/4w5Uqk1Mz+5At98Wdy5uqyYAbGdizMPd1byDs2RLZctLtVpB3h3A4bSBx2UbSnmvAKZMgPi1RcLgQ8XKWHkyZi3VLu+xS9a4ppP5s7HbMZ9isrxe1hOwLg2GiFDKaif6QMoSjThHzHqRrslpAqdSkXkJKkSYDftV1nYmyGWyrp2ZnUWlXsNUrYapehOIQ7lR0DnryMoqPnv1DfiHq2/Aaq7JzrsySqWXRkByqkK8VwwLw24PC3t24YH7DslCkKpfwaXf83z8/lvfJXOAYr/3GH9sGbCRfH/K854Gy/RwnGlLvSYRjKTI1EwTowFXJKVKzziOWhoiWGWGMxb34Oy9Czjv7P2Yo38/Dz4PI+/RThODaIgrv/glHDj1bLEIaHd62LFrUdIzWi/0B11hFtM43kwlaZXH8ReD202TFM2puljc+bQU13T4XL0kqaoaa1FjLsrKgCksmcs0UTboDKES6XhTKSbpJvt0SuupnMHUSRWjHvo5cod3kWLIxnVByRSHZU3EnLoeBugtr8hz+VNTQuxUfB9etSops1/3ZSC2wilrh+qRFFUysBQ1kzRybNlVcHxlCV+47mv4u89dg8PDHGbFl2tc9ysil6N3CbfJ2ZamWh62Kb8TvdTRbDWxa88CPnPZP03WVT7GocbEYItQ/3ER4Xt/5PsQRxnuuO3rMH1HWYSXGirNOir+FI4fPiLjKKwfKONy7Aw1S8eFB87A3tkqLrjgTNRsbtQm0IBY19APE3Dhr+vO4PDSuugF4zRGfzAQMHDdU3fQlkhJip7bR7niieQLWwtJMIDvucJvNA0HjmFgZn4aOskHLoOnhR0zPxq1somcJlITlhKxVD1G5lJAORk+NXQBMEEotukEK+ffhC0skZQlgjBijivqmSSh54p8uaTH/eFINI6+X0HVthAFI9C2bs/OHSIvk8hTb4gdAvNFzpUyulNiVXW4+001x8NhiKuuvxkfv+yfcLiToWR3uuZKz7EsVB0pKS7bCGkiRInHlLvXk0mLd7znHXj+My5VrOdJ0NXeMmC79ehd+I3XvArLh5aQ61XoVonOyor0wXh4JZqxuI8TqeFYs5RagZ31Gs7dvRNnnjKHM8/YD9chG8IJZR1xCRxf72FpZR3V2hQGfQ6dQuoyRgI6d/WGXVFJTGosjpUwvdOSIXy/BtdxhYSgVGrad6U2mZ1pyeBkniRSaxWjSBYksvnNFI5RRbUMXOWGNZZnqVt/oYTBY6lXnumi6Ai1DFEYSzpKep4kCHeo9fvdzUNPwDPt7Q9CdNptuenUK1XZQMO9cdkgAGcEOII0OzsrIgDXr8HxGZ8MJe1y1IJ7/ryjbh833HoP/vqy/4tb71uFV2nCmvIQiv+IK9ea15nzf1k4lO+heoS6VKa8Bw6ejis+8WnpYZ4M6sgtA7a8zPCSl/8A0m6EjR43g3YR9gP49RpSWm3HKaaaTQSDoYyUkHbnrNeuZh0Xn7YHZ54yj317d8D21VqmOCmw0e4gNyz0hhEOH13C9PS8ECJczKHStRJOo47ORlvSpzDkOIkjhqfmpMYS/31S7wYalgYrT1CtNcTnRM9y0TAaaQHP5yYbcf8RJpVg0zUX0HJRyzOSCANpkIJX8ilGjiwFokGA0jNlIQbT3NJgW0KpRwg2Ln/keqrhsI/26gpm5xfRmGrK87Vm52GAxEcprCSPfThmVGtkEy0XuZbApRsY1SORWnksU+4lcMsDS/joxz+Jr9x6r9gjFGQ2+Wx0L+PITlGiPt0AokCo/7n5eSGTCNi3veed+J7nvkCslbbB9hjLol/5xlfj9htvQXeQoLO6IYdpanGHzFKRhZyfnUMcjgQocZbCc6vY07Dw7eeejrP2L2BhZlrqqKIwMAhHAoTDx4/L7rYsNRHJKaeWUKVInl/B8npP6G7bNjDod8dsZygkB5UWTAV5sNnTatmGpH8cPGUrgF6TPLgZlzPqtCCnNz/TNS7W0FHklKEQVMoPkj8Pl8lLushdajztJeugGGmeygorRrYozuRmYjpqH3jGcSCmoExX5bmVqoSv7XP7DLeYigu06FoEVLyR2LYrdR4/5g2D2ke+b6KMCxA5JPD1pTb+9hOfxjVfuRkpp4EqNejVugybSgpMW4rmFCIhkwq15piuzVUHr3/bm/CCpz8bpsE6eZuNfMzAjQfiN3/3t/D5y69EUpjYWFmXntD8vr3YWF0REMhKprHHR5IVqDoV7J/1ceGZCzj/9AOYa02h1HiILPS6XaWYp9NWXiDIgEFAyjsXCpv9O0YX22DNEyKn0SmTPllyn6NLrWCSCmnCqEf9Ia3BNT2Xus/j2iguuKCcioeXLlqGBl9U8UppYpm+LI/P84n3CN+9Yi/ZoxMicwy2HClKthV4I+DeN9kUqm4KsouOjWZGXG4+5XorvyJDtBXfEyCyzmQ9S+qehAqvJ/fVTXSTTCnZH+SuA9aQ1XoFUZlgNSrxlx//JC7//LVICg2F5aI2PYfRsK+ULtxPZ7swS7XhlIDbtfcUDjDg6i9+ETbG9guPmZP2b7/RrZNGArjiui/gN175SpjOFNZXV4XlWti9KP6RPHCk52W/tVYgKjJUyxrO3Ovh2x9/AOfu34spr4rC9DFMUzl8bFAXeYI0HtHUHpZT5aYkpCQbcq7MLaGljEqkrTWxyeOSePovDmljnibQMhIhrL9M9IIIqUW35ExWOXGEhXWMlQMGe3ayACBX20ptG8iZ1ClDn8lEgPxJsgQUWLN/p6y+szxUYzRscLMFYehKHUK//bG4mT9P1XPVEkXHkXqN4mrWVK74nRBwJjSbm25SROFQdh6wdpwsilQtBrVGmBF+1B/giqtuwF9cfh02igBJGMO3PWS5Ajr7bapXZyAIuVq4wMGzzkF9dwsf+dM/R5OGP99SfuTB6YqHu8pqy4CNv7Dbj9yFl//oS2H5U1hdWhOWa3Z+B1bXVhVlngl1oEgHiodTD2fva+DZTz2IM3bNoWKzCesJ60f/RBVCOGCZIWGNZ1rwvTpKSd/oDZIj67aF0GCPjCuXuFiQ1GPIVI/2bbnq+XG+jqM52phZJK44hmOXOlLBqnLWYnNb2D6SLIUlh57HQQxWJyM4J/bX2BMkc8rNO3yvXHSYZtIr5GtwyoEeIIzStk2aQ/mK6FxILzsMXNj0vvQIbtqeOzBdW24GvHGwvh32E5FZETxOxRewEUAcKcqjAFd/7Vb8wcc+jQ3OENKr0/PltceXT76PN5CiTGRN1YEDu/Ck5z8Tr3vlGzFtUlnyrUTbNti+6aSCfbb1bITnP/s7YNNDsjuSO7jEkUwTDxJqAulyzJW0snQ+9fGEM3YI2HZPu7KkkLbgmm5vWhck7E/RcIeRyOLuNBNapqGMqSDRYFq0ME8QRwOUmRqjYf8ryFT0jJkCMiXjpHOcosxTWSKvLOW4NNFEZNHgVTlz2Cafk4oSIE84Ma0SR4l0Y6uESSNc2gFKwYxct8T3gw192rDzzzxm5DPEMkHVXfSTJKYUOUTw0+7B4Qpi20HFN+G7rsz3EeyWwcUjbB2UGMZ95SnJjTsWweZI/aoVsRgsvf6P/gJ5pYKA15n2D9Jq4Hyc2jHAAXS6LBBs+w7sxt/+3SewY2qn8sKUqfZvVc22DbZvGmz8hggZzr3oHMxUG0iSXBT6wz4V7JADqNFvwxrrCssC1aKKZzzlLDz94rMxV7WgWWrHme3aIA/BuzOV/jzU3LJJ2RT1jQX9TehlEgaIC00auIwkPOy8R/NwJRJamG4WEiFkVRRV9kwLmWLlSkHCPmA22dPGWo2isPHCQlNzBWwE+omOWhPQTUZsWDuGfJ4sU/N1GoT25/vKmBZqmkS4zmCI0SiQRfODTg++w3SQWsYcB887F3tm6vAaHlqNObgcPWIUz3IMOQkw9nSh3IyEEEHM5nYeD/BAt4dfesM7gNYM+qOA46jQc9VjY79NWZ6nsB1l6z41W8dXr7sZ1KmcTI8tk0ZSNM956Ut/6IUYrXcQdQN4zSbKVEev25NlfUynCjaa2d9ikxkanvcdF+HJFx9Ey3dFJc+GLtsISZRKv4zWPao+MZDHdOhKxEJbSznNnGGgO+j2+tJ7a3eHiKJEHfiylOWIjsvJgRzTUy1p9mbizmWgTDOJZiRfOGjJA08xMndvG1JjFSDYOGTN9JC6S+qpJjYKPKQCNtmcmiFixE4SqTMZ1WhLQE5yFOc4dN+9aO5cRK8b4uprvoSgF+GUU0/F4bvv4nZEPOlpTyY60USMZt3H6WefjZmZWTSnWrDpUcmUMlU/Fzf6iLSKek6mwUaO5STA6/7H+3FsmIpyhTXtRH4l0wwSuTLZf8Cxo2c85+l4/W++CactnnoyYW3rKEiYRrKUes0734ibv/AlWfhQrzeRhAV6vT4Ki5POVM6nSk9oAHPxED/90y/HnoUa6q4Fy+WdtoJSGwnjxhTIsn2pz/QyQTociCKjMwyApECvP8A9R4/iyLGj6Kx2scKoUbA2AbQwwVmnn4b5ndN05EDa7WJ6oYnGzIwcOAJfhknHlDcZStFLTsBYcle3L2p71nvcCMMUjsSHzJaNVxXzxsBHnEP6fRzNYbpMsIejHtbZmK40ce01XydhoHAAACAASURBVMFGP4AzPY21lT5OP/UcpFEfSb+Nc59wAWy/gvz4MtYPH0LD0bH/tH3YfWARrdlZ2KUBg7vexF5iPJE+FlFbVopOnuMPP/oPuObmu5DDwSBjqvygpyWn3GFmSOMQFc/Dpd/zArz59W9Gq9LaBttj8wqovP9d//MP8LnPXI7l48toTk2jQ0OajTZK1mNcC5WotJDpzT4rwa/+yiswUycNH0samee0EWCbYOKWxb5biWzQRRqOZEhybRDhztvvxe233IFRnmHv2efgs1ddjT2nXoDdZ16Ay6+8CrunGyhH63CKAKft2oVGvQJDG6LMh5ianlF7AjSlbCGbSPaTqhIxgpU1U8qCjgarusmhT0fE1moIQReyR6KcEkchKAqsr64p4JGYiSPEoz4Sv4njS0s4fHQFq4MAUemjM+yiXnHRqLg494lPwku+9yVYXVtCcmwJd990Hdr33AtHL7HzlN0449zzMNVoyWQDGUk64U3qRyF5XAuDLMfnv3Yb/vfffwrrmYEhpwI4wsRxG3mP3OBDiolMq4WPfOyjePIFTzopxmpOxMqWSSP5Q8vkcR7hhT/4Ihw7fBy7d+/B0pEl9Nc7MBsV2I6DhOMm1CFqFi6ZcfGDP/GD8CslHFHZOzAKC3k6kkYyt9sQCCzgKDiO0xCrG0Nce82XsXq0Da3eRGv3Imamp3HF5z+P3LVxvNuRjaa53sSORg3f+YwnQh8NceT2W3Dmvv3oBm2UYYBmoyYkhcyNcZ+bDqHgOXSqGsi21Gmi6vB9NchJwx72skIFtIlErEgSDPIIq6trspa4F8SoNVoYDLoypbBwxkHcf98DWF3rQK/NY219CUkyknm1ad/D877vJTiw51T07r4TG0fvVSLKIkJvfR1eo47TTz8d8/OLcBxl/bA5P0fBi+8gyIF7jq3g7e96PwZuBZ3RUGpe9u/GrI5MqRtagsr8NL5y7Y3w2As8yR5bCmz83WVljqc89ykoUzU20uv20VvroLpjRoZJyRwK2ArgiVMOXvpTPwbbp8cHU0wbRkHKTC1yF4NV2gfkCbp9Tn63ccdNX8Pdhw7Dq9RRX9yDvQcPShP42uuvRz8KJcKsrW9gOMxw6s4dcEYDfP8P/QCW7rkTa0cOwfNtMbzh67lcz8upaFoVyKAmp6nV8nlS6wQbazW/WlXKfscTC76sNxI7Pv47U+I0DDAsEvT6fYziCJblIQwi3HbzDWjuOBW5Q8W+hWuvvQ45584cG+7iPOqGCb8Anv3sZwNRiujwEWTxAG61jiAacZYdg1EXdceH35ySDT+NhlpvLJGLsjIDGKY51sIcv/zq30TamEV7OJJVXBNXaDEfCkJYroYbvnoTZqZmxvXwyYW2LQc2JlU/8PLvxxIXawwD2VIz6PZhtmqSqvEQ8JAaSYoXnLEb3/mC56Ha5KJBUugWzNKU8X4Zx+H+tixCHAVYPbqBu245hCyP4U23oPkcvNQRuyYqs3M4fN8h3Pi1G2E2qlJnVW0X/aVlnPW4x2FxcQ8qBpd3OPjyF67Ejh1zMkzKJYxilAwNvuvITgGLW3JcF9X61KYLmDTPebgr3HpaRe/4KuI4VE139g/jCEGeoBcFsDwfURDj3lvvEPlWZuigNLri18Rujs3+B44dQ2VhGlOuj3379qGpWaibJhqUnVVslDozhFwWRNJ+nK0LjsnMzEyj0lDvSwAnrlwFggJYT0v8+ut+C0ltDkdX18UcaPK+afZDSfUgG+GBu46oiHcyiCH/2b1iy4GNP/8b3/0WfPEfr8BoOEKS5mgvr6G2cxb1ak1ExEzz2Pd5+TMvwiUXXyi9LVnLy4NP3rwoZbqYBzwY9RCMhjj09fswHABOzcO+x50qu6TnF3bi9gfulT1l7HfxgLc3NoQV5KBq1fWwc8ci8jjhsgHSJDJgSdpfY48KORqNhvzKaly0aNOSQBeRru1WUa/XN8191EpfA/VaC3F3gPbGmgieOa/HQ9+PA7QD1oSFjBkx2tJRK3EspBGJGMUKcixIGbe6sGidkqXiBs3341crsBlJPRcciSUe6B1p2xU4ri1Ne8q8eNMSd+Wc0wwxIpjYKC288U1vR8eq4r4jy3IteR1EX8l+opHhHe95F178vBeLaet/rYdalPlwe31bEmxv/5N34vJPfEpEuLRsG/VH8GamZJvN2sq6GtIsgbf93A+h0azJ6l7aE3DkhuY9QlfTajsiydDDoN+TUR2r0kSt0ZCRmiRLUcYFuJ6JNgI8WKPeQMZdyH+nri6zarJ9eBBDM5iSFhhy/IQzY7y5cyPOeH82R3Y4vVyp+AI2WjpMdqspt65MvEEWd+7C+pEV9Ls0Rg0Qj4ZCXAR5im40QjAYsR2NkgqPEghkYUYqfT5pplOLySCe67BdCxXPRWt6Co7roDI1DdgkM9S4j8nVVtzOZnpwK4yYA+WlQnaW6TVdy0ge6RraQYr3/+Vf4falLg4fWZaZOm7I0W0ucaR0LcLysSWYmmrO/9d6bIPtIf8+/uofPooP/MkHEHDBAz034hSt2RnZD0bzH7J+FBy/47+9FP6YpJCmMTUd1Bby2ue05k5lGJREhkt1PlUQ1KCMtYpBvyeEAcXGKysrkjaVaS49usBQdnS2x/QthDFeQsh+n8FGNaMGHbhMUw4yHYKZmk0GM/kxyRN6QXI5hczjVVzU6vRRKTHqLGPUWcdw0EPCuo2WehkwigbobQxEI2k6FpI4EZWGmgRnc9wW7xHP58S1SWyJMY+MzXg1pdWUepEEBmVWqnakDR41nkIYiQEtrw+jKBfcjzAKR/jszTfhss9+GeudEYIolP4c9aKapePsc87Bpz/5Kd6yHvLv9ZH5xhMVJJNXeGhqli0Z2bpRD8//rudCy4DhcIiyMDFKEjTnZ2WMpQyGcHIN737dz4kangdqIoESsI230HCEn7jjXV/c9wWICpD8eqoqsjQQBpCDkmmiDiNHX4Y5PT8Sae6SqOHCRJFAymiMGtOhAr/ZmoJXraDmV8ZrmiBSMja5hSihBd3YcUt3XUzP7UaWleiuHkI26st0ApvYEYUp0BHGQ5kYZw+QKhp+zJ7hhEF0mBJ6LhyXXpIW3PGYD9+rSyNX1oYkUBxa+ankmu+DekhONIgJrCwGyZGlGbgAiOztKApw19oGPvjX/wfL7aE8R3c0RKfbwVS9jptvuZ12SWr91SODmof4rNtge4gXTn0bZ5mf/eLvRNAZijo/HNHMxoI/XUdWxii7PdQMHb//G78sd2k2i4XtGy8z5PYbZbBTwOBENV2lDF0OLe/mHIGhjQAnrdN0hJw6SLHtLmXQk7KtLFe6xUmCwucRIQUt5QwdDreW0pq72ZDIRl8SgkwOMgdHqVukZTjbAuKYXCj35kaDCkokow7SYVfMX+MkQUhbdN2RUZ+Ylgm6iSzlEO1QGFhx3hJBMjemerBcumtZskGV0+LicSmbbfjeHnQXEyKEizFoEsQBH44DmdwNkEjU5Owf0hhhMsKxMMEffvhvcP9aD2EcISkyUe7sWdiJL1559cP6nT5y33wi2E68DXzzt4QtGdl4KH7xtf8NX73mOriOh412D5VKE0bdllGP0fIqKoaOd732vysPx/EOtYnnx2SrilgPTDaCaoVEL6ZXjGhxGMghZi2S5ZxXo9BYzaeJ7ZykYeqhSV1mCthomsPtN77vKQ8Tj6p7F864fyXddGowuZmmhHyeDlV8j7kG8QZxnBqyZIR42JHnGIUhBS2wXU8UHnRoZgThICmjM2fwuGCeNwqCTkZeqFM0ANd15D2yPhQt43hQVRZsjK3xZNJcpsRZyxKUjJT0vaT5bIkyiVEWMdq5gbe/789w90pPPClLU4fju/jgH/8pnnDuhY8cXh7WM/9rQuSHJozecmCTYY0S+Mhlf4kPvOe98FwfK2trqPh1VBeaMG0b4eoa7KzAW3/5Z2CJA6vaJz1ho05cYcSVvAox1CrqGI3oOxmowcpxtCBVLhE1G9d8Arjx/jQq4Gl9IPvdDEWZ63TFysQThfUTI6GASmpBDqCpKMKIy6TLHC9bNFlrVaooSwJ+gCIeUPGFmO+FxiEyVa3JOl7Kq/x6QyIjmUuuV0xitalGDXFmUh/yh5/09SYLOLgbgQ8ym5MJceo1+b6FY+ENQctExSKTSBQqjwYYwMHr3/0+HAsyVJsNHDp6GAcO7MdnPvlpakgeFiQeuW/eBtvDvrbdpIfvftELJG3jPNrUzA6s91axuHcRabeH0VoPv/OrPwN3snxQFlAo+ZSoaScLMMbaQ5IfIvLlrNhkJ7bYyqloJm5XMkGdCyg5MS2rcsfLERnZqJifRBYCcAJSApDtBn4vo5j0r8YHne9nEtmY/oGe/F6dxpQIe2vidMwbAiMZvSG5c8CwfdF0hpwaz0pZh8UpAKab/LtE4SISBy+a+Eys/bg1lO+5yEmMqDEjVoLcm8BIxvfBJvrE/4TI43Xhc3CP9kiz8c73fwTZ4i4sLy1hEIzw93/zd9g5tfBfuK/2r9Vs/E1+8yTJlotsE5QmeYrnvPj5CIcBwkGAfaftQRzmqM82sHrkEBqmidf+7I/D4/waVXu0ODAdAQfdtyYjLQSqHK7xqidGDh5WEgrUJFENLykeaW5OSo/BJgOa402gkp4JQJWvo2wF5bAoZ+MmC+5FJK1m02TjND8ez68xwhEElJu5tWmYngVKtDrrKwLsnHN0BCddi1kbWhWURhVpmcLIUjiyqrcU5pMW4jKVQI8uipbHDKNsTOWWuaKA47jK+TnNpFZl70/kYrxxjU1lZZ8brwsUKTQKEuSWi9/9g/fjCCe4wwinnH4q/r8/+p8wNecxC7bJwO5/5O6/ZcHGi/Pnl30UH3jvB6E7Fqy0A9+ex1yrhfbaITQadbziZS8RRbuaaOFIjAOUGXItVkOP0m/jOR7f/ajkIAnCiMYdZlx6/w3/9v/YexNwy6rqWnjsvc/ep7tN3aqCKkqaAAJGhIAiIgoqNsFeQ2KLCiaoQUVN/mj+5JnEJL40L/qS/P9rTEzyEpOYqDHEJM8kdiACtrEBFZEeiurrdqfb/fvGmGudeyhBqqMoXu75voJqzj3N3muuOdeYY46x4qDJAFMPTcHEALVFyTNjwTMYqU6xyYQrqNxnIBDBqQAGMUVwBMMH9lrd6Q6i9hSidlek6IiczX4PxWgoIaBG0kbAwc6kibrZQUHGP4cLZHRoaCplEJSBs6EkC6QSJrKwjQIpqOSxzT/Tl4OUMpbRmpWx50rPhNmOv0zpi6BMHbfxvv//g5g5+bHyW7j0sjfg3DOevDfr9GF6zgNltdXMts835Ibbv4fXv/LVmCEzIsiRtOfQbbXQKIeYmWrjsotfCpoXKWDIuIhI5xohapjBBc9Y/DeBdGaiZqAIpQ94vpKRhLl28vwlvUiXqfhhxR2snGKxIEXO3lQiFLPMpECPyjb3/lKY4ugMZ9zkf20P3/tii6A1PYc6aSEb9hBRioHmGKS28JxHG+AmWShNucyIqZ+mmtA2m2PK0tFPjiNDBnhIudkJwVLSQQJEUWDorJgmdn3GPgLMuG7TCRpWESjT5yX6VYC/+F8fwe27F/Cf3vdfcfqPPfEwzmjacn5gTa1kMtsEVzPbXobdoErxipe+ENXysu3UrbbOLZtmp7BhbgavftmLEAecJq5UBjYp51YWCjaJAQem/GtKVnwOJwAI7/M5tggZaN7CiQHng403SaCLM8wgqMDsQU0PBoN0KzkZrmgizEzNEXOocXiL/onB6J1CO7MzaE6tQcbmNIB8sIhakgwDgRTqA5LMHLX0PnSL4b/RU5sSDPo+GgSlknMLlXMhlfOOSlgrExmI9rDGhZ3RLAvruzq59CowoxApeDHY0MD//vtPYuvCEn77Tz4kWbt9B9D38uYelKetBttBuYzat+ocn/i7v8J1n/w0br/nDoTMCs0Gjp2ZQitL8fKXv0QMDdaR7KFRepsBFUZEBLmz+cloy14ld0L2yTi17XZ0BYdbUp7lrrMak5RzqqHuhtzYSloClzIo1DRBlAipFCuPmwEHVh30Ll0twvFsaocGatAuN2x2Mb3pUSJBV0tLGCzuRM6h1bQ3braTDsb2BL9BFCbIiSTqTBaqGux2p+UgahPotVgyDAqpdblA81MPPnPz87MclnOP9B5qjOpUvuNkqKQl/xzi81dfj29+/Ub84cc/gWh67qDdy4fmhe4/2Phe+2P08R/6zCZJnTrHh973u7jmi1/AzlGOYLaLI4IIR3cTvPilL1ITlwAJF0+DZxbt8NRLtEzAbEHdRp3PmmxM+53eygwGhwdPGGwCGngIDKwNAKpj8cxExgXtq1yQksURxBQXcuAFz2XMdA6FJHMkY5nJloDrBba6UzqTMTtvOP5kLC3OoxwtI11YREnOIl+rKBCTcULn1d4yhqOB/r47PWNS7K2ugTPu7MYWhsx6EBjowx5fsCK/4HVOahjaykwrWXFQFSwy/+2ArjYFBoMCt9x8C77z7e/gha9/I854zosf8ZltX4L8P3Sw6UKVNf7+D38Dt9zxHcwPanzn7h1IqhJPPvXRePaFz0YtaJ9FVq3GrR5OOIe/NcpVgG53RjQslmUSvHH8SJ/V/DCnb2qLJ0kmCDmERSbGPBkcfIwGPcysXY8oaet1OJRJDa1KtklknbAvF6JqhJp5KzijxhLVmdFnvQxHnng8MvrEjRaAwQgRF3tGG+IENR1Sxf5P0V/cgYWd29GZWoOYrjStDkLyHB04Ixlxwfk29a2KgCCIc1E12ssKCjlu2qvNQbcfCgzVSDOCJBVu/+738d1v3YjHnf90PP/N7zyM59buHxzxGpurmW1fthn/3LrGVX/y+9hy503YumMRN+5Yxs7eAM95wqk47/ynSL1KZxMJArleWWYWTr6fpvLKnXmo7WGNXtfAHvuv2OmEP8Pdn8AEA42ZkrSpgna/VaVeHY0+ZtdtkK2V8SxLa3SrdK1QVqFRqKheReNCqiMzwOkb0GzKPWbD0ccgmJlCnQ+RLy5KZYfBmY9S9b0YpGwJBHmO+Z3bsf32W9DqkN1vwVbSU6BJg8RMysgM+oznPJbAsDJZZ86A4IhZV/nrwTOkbULcJHLkVYH+gHbACW7+5rewcM9WbDj1ZDz/Hb+5ErT7c+8e0p+5b7D5IPNvuRps+3rxWR0GBa764P+H4R03Y8f8Eu6NEizVDcxON/C8pz9NftHMQkTi5IDG7JNZv8mDHCId19RgbCp4eNaxWS3OeDnhV1c6ciFacLIv5QCVnM3wDEsL80hmujhiw7FodzpazHauY3+OpWYkgIOVbDI1jbnZWWy9d7NGZNgTYxiUjQDtZhez69Yi6rQwWFpCNuiLiRK2psxYkFmVPM4iR0Rn014fW+64Bcs7t6M9PQvE02i3Gur3ceyHimJ0JiWUz3OqPr/84AKVmGYRZzIMZKR43ig3hTLIMcpJwmYfsYMbvnot1gU15h57Gp74qssBZ2W1r7fuoX/+AwebZfXVpvY+3wOCGjd+4iPoff1LmF9awq2jDFvDCIPBIl7/ilcgpBMb4faqVPOXQ6P8s2f2k9Zk+h9khHRASyg/Z6YFKBTR0bWcHa/KMJaOBRchR1wqDHpLYnmsO/poLXix56laWpMUwulw6vGHgu4D8irrUmpZMtuQ1dVI8D17cxRJDaUwbN7dDAbl1UZbf6Yql5gdQ8uoZKqMej3c9f1bkC0tozu9BkGHwZYYU0TmkDa4SgcffndDIG3wTxQyhzr6MyWfQ58RAjP0AwjkXJPgW1/6Aja1E0yfeALOePWbKVKyz/fs0PzAarAd9OvMS7r121/GHR/7MBYW59E49jh87LNXI08i/PzPvkla/oTc5RfNLDAcqmfmyztC6FxwPE9xKLPJhjH7SupfUXSUXV0/IWDAghZqYUYeVZFj0FtGNhpg7YZNCGc4UtMWV5ISA4Z8ATUHP/sjDJbN4om+3Zq7I0tllKHh+JZsjnFim5bCBFFMB4TgxhBVSJMMuuG0NClAsXHyHLOSgrIjLM8vYcvdt1p2nuuqVO22ZgTMcDDVdnS2O5y3AEIxSNj8Ji9Tn5XmJMx6bHuU4o9gOFxGK+5gVEXYftet2Nhqohe38cRL34YgbB30e3pwXnA12A7OddzjVcrhPL74x+/HcMc2HPX4c/CJT34Gi9kyLn7NazQeojXGcg61GBnc3YXsuX6SGptqmXGItKVzV0wggiDEsI+Gywa2WGl4UYihQTrXYHFBXmnkLIbNDpozs2KRsM1AWTiCLuztseXQGw5QUWORLHxmo6H5cnMglaUkkb+6DNCa6iLudNDpTqHVTgRQmIlGruDkZDUl0/nvOjvGDWWh0WIfO7dvxqC3W9+ZrjrdtetM4kBTAiRKd5xcuOuQcXpdwkfW2PdnN7VCMlYAIdJRz4AV8i5JHcsD9FodnPOaNx/GaORqsD00wVan+Ppf/zeEW7Zi3Smn4Orrr8etd92FV77ujSiLgYjKLPXEnMgoonPfj+HPK95CzMP3HF2hSRldPtXYzSh1zrNSiQZpXxzFSVPJcXe6M6jpS93uokm14zpVe4BBQoCEAj550ECT6lmjAZoNs26i8Cyzo3RLKpZ39FqLJeZDmTmaw5ewWTxSqJhlWXryS3BWjt8pqCNUdYilnYvIqhH6CzuRLi4o2Fqza9DstJGQV0mh2IAKXw01xZk5tckQqU1Mwk5jR5RXzzKJI6UamjUvNl7DjP5zU+tQrN2Ixz77xbaRHZaPHww2//2s2iAyvG+PVehf6H+OG//mjzDafBeOOekx+OotN+K7N92Cn7j4UuT9JakLm60uW2yZSjj/8Gc2ciVNOIHgiVGU+ANlYYwPY5NYm0D0K6fQJZJwSGXlFqogxNTcEQiLFFGZIh0uozcie6NGSp9sMjwqsvSBoN9DJq380DKaVJBNvpuZJml35KpDDytSsCTHkAbI2HMLhmhNEQTh+a2BZtRBHHcxzAsF0YCDp/N0Ty0RtxIxWjozcyolQ54Hndkjy0mV0HS+4VmSZ7kstymHgl4CNgUhQIYoLQWLGhHC1hpsevJ5aK8/Zt9W6yF99gMHm297WK9173eL1WCTliTwtY9/ENWWzThi01FYynrYvnUrHn362cooAkR0yK9csK3cCPaHmcnEJ1Hv29oC/DuLTvOb5p8ZbAQLdGbTRIAtTDaiA7rMlJV0UIpRX8HW7y+hqCJ8/qpr8b8/8yU1hakE9NIXXYCnnPNjaIV0naEDjjFLWKKxgc45NBp9pHmJeGoa2++6G3/6e/8D6aDCGuqKtGs85aLn47iTTsGamS5aDM7OHIJGB1GDimHzGO3aITIzR2ea7S6SzrQAk4BGHA0TitU60xxshJBZzm0k+kzufCtGjN+cqODc7qBuzeKk59K+lzbFhzSC9uHNfnhmsxcyKtrePlaDzQj9uO1rn8Pyzd/F7HQTQ/aFestYs3YDUpZIJYV6cjIXzQtg4j6IjEF5BDJFONNWpuIVyk2UZZujdXHBCX10sDnRQ+ruU0MfJDhLTDVD0gxlTlEOhhimGW6++WbceMN3cMtSiW1btyNpT6FVjvCYk47ByRvncOKPnobuVNt6cXS4IaUs4IR1B72lIW793nfQ27WEJO6iZOnXICXLSsi1mx6FDZuO1J9njzoGnen1mhIfZMsod+7A4q4dek1q8bN/R8fSJG4hahqHUuewgBotzHgW+GxR+LYILbBEwmZLgp+pPYukG2M57OC0C3+C8OjertOH4XkPfmZbaXDvXcCtBhtvYw0s77gDd113FaJqhLjbsbIMEYg1yt2GAEPFzFE+YLCJ0lTxJwwsIG1JipMsN9SrM3FXLVJmPpkHTiFoNJFRkqAqkS1tVTnKn+2lJe7ZtYR0mCMOc8wPSrHw4yTAdGcarbyPHZvvwSmn/ig6U9OidvEsl7RDbN28FaP+CHkZYXbd0TormiVTgLiwUpZnrlY7RDzTxkmnnYU4nkZeZxilPWTzuzCY34V80FOrgWM5zGzczTlEyhEgtTCcQjMDUWe5wtxshMjSPtjLRlD7srUGrU6C7WEHZzyXwbbvvapDF3WrwfaQXGuNyFQpvvrxD2G2G4qUS5m4sgrQF/igoS+Uhel10Ivaj1Yws2mHI3eSpVxUS/rbREorAQpq8pLilaUS3LFgq7VQW9MzaLWmlNkyanbs2I7ewqICh/QqmsvnvZFMCTm4SvYHA7/SJDVBjd0o6gLrNhyNRqeJJGhiVGZYWqKnAIOrlvEFBYbiVluASKsOFPR5OdLkzdkXPBNlo8Z0t20ebukAaVqiv7ALyzu2K2B4XqMFFL8PFWyZ2TTL5ibZ/RwcP4smveXiSgCn0negFuW62fVoJ20cce4FwNR6m0J4SO7owXjR+ykjfTmsKYzVEZv9u8py5a1w7ZUfwkyVYa7bRhnR9SlSGUmxUWpsEAXMs6EQPo9ArgAlNSJph5gsnBrdbAk4cIAkY7L6bde3gKU2/9TsGjeuQ2XhBMViT3qU27dvxY7tW8Xe8OI+OiVQzcud/SgXTsIvuZTSMWGzm0AK2MxOEDCjsrFNF1WiiLLxtWFVPpIkxNy6I2VTtY5S5wQ4KtsU+KF6SwtY5tnNOfsw0NSwZxCTnuWyln7P4Tee2aLaviNtkukKTiKy2DIVjly3EZ0oxLqnvQhoTe/fvTpkP3U/webe276346k66cK9ydKrZaTlGe1Uu7bchp3fvA5TNJcn6kgGPSlTeSoScpky2EaC8nVOc7u6XoHNXPEESWmyh4KusOFQU/1loJHmJdEBdGfmJFtHCW4mRypXxUQXwxBLywvyWtu5cxsyqmOlLGPJu6S8CP3jGHQEbmBqyEksi1yKrxL2lwyD5MHtXMRRHAYJx2vIs2QATnU6WHfkRsTNFqampvR5+e8ULGImpkF9kZdYmt+FMu1rDo6Dpgxoza6R1e+kHTyLJmjGNvnNPiH5N5zX4/urMwAAIABJREFUY4bLCxx39HGIEWNpegOOfeJTDlnY7N8bPXCwGTJkPNU9h0l/2HutBtvE1eGFu/XTH0Y0KpH3FhHOrkFOLfx0JPlxBhuhfXKTucA1Re0EUsVhdBPMPG/xPMQFZ0OlVjoyyMacSlKmIpOH4wJm2aiyit1lZb0EAxKUM2o7LmmY1EAXU2GmEpaMPYRoGiomgIKgTGMFkmbmE2WM3A/C+PTOThoyv5hdY75qLAdbBDncZ6yrXK/PP7OvOBhQW3KEirZOgh9dsLmA03fjdLlSL8WCSvMWZ0OeNXpogkennnSGiNH/vm0XnvpTr33EASTjkneCG7k6qb1/W5kW602f+ghiZTLISqmIad+b6lcgahbPW7kWuYjGXn5ElC7rrwkMmXANlZOp+IRWRkquuzOFsGG9KSpr0fWUj4hT34Z7KiPmJa15yey3JmpZ5AJPMienIJCCkwMOlFBfiyKvHI1xJS2RUg2KksIVRuhMtRVklO/zWigUhLVg4+iPATtkrfB7qKGejUTpkrup10Vx/1f7osGNhxOCpSmGUdaBYkjcbMiprIFjjzsZyWCEzc0uHvPU5x7GsL+vdu67kCYDa197bKp06j1nB/ZzoT6if0wlHO99gW//68cQc6nRq5rgR2TIIAESysOx3tP8mdMXIYXCWO4sDDmMakgjG+EMH2Uy6YTQWteaz9LFjyhjRzjdzmpWllC3n3wPY2H0hv2x/7UmACRdnmsGj+9DRojYKq6cURbhBEJhQc+foQE9nUgVTDEz2rTYJAxGmjlSFFZyCGpU88eNGubn7ZihOc/GElW+2dx0VD7zGvmMWrlJb12elTJaLDdKq3PzqbDpiOORDoc4+oUvRZDwzKbTz2G4dLxO9Q/7aPuOpK4Gm2ogC7ZytITN11+Fks6YlHUjlYk7dJmhGI1QkiFfmbqxn91iZjOeJHmHrmHtWPGC8KVcZc9hYPmMGIRNSRIwQE2LkXoCIbIQonCJzMvMohZEraxBnqVMPRhME7JxyngMUoIwLBPJLCEti0ERMbibaLTMWVWmGa0pO1M2jMtI1on+zM2FUwz8LOyXqfluQet1L0mYpoS6ZWRPyGawucVXrzR6LUtW1BnSmTZqTSMcBjjx4tcgCQ5XAvL9Z7UfDLvVYNu/XdIFGxWEb/ncPyPWTl4gp3/1VFsLj01tQv826DkhI+6EbnJNWltN6YNxfD7zu/dYLYvqwbGCzQROA2UYCqUOqV7sEC7PseRGoAUuJWSgoic1g2sMxhgpmr+YJb17apMyd0mC5tSUARvMXlGEpitfqQrEYOMEgBfvEVWaI0W0e2LWdKYbLCcly0DJPWeyaOpaBohwQ1AAOlaLPqeUxyr15LK6RL+o8cQXvRLJzMa9Ae/2714elJ964CntFcbIarAd0KXm4rjhHz+EDqebhyMMUaK96RhpfDCzaW6ryHWGEUzvgs4Utkz3UfKOWqQTyshews6Jrsq1tEEuZEO9K53nVPpRbNmf71yJ6NSVNYxKlgb7fm4KnAdGBhb/TcRfmhey/9WIZfjBtgF7a802g61hbQOWmSMzkCfNSp5zylDMngRFyCx2IrBqWzDTcXCV7H6zhyI6KkCG50AnMGt8UBWXLksSic0tCDkl0Z3CY1/8Go3oHH5ONXsumweTRFgdHj2gQPM//NmP/Bnm6iGq/iJ27t6NjWc+HrF81ZjZCJCYbe5kKam84Ca3uZhFRyZ7xCGEpDJ5sVWRkIXekY1P6NwQS+7+NMHgz7Jk9Mx5P7Yi2fHKOcSMTO1KKKdz05k8vMsEg4OfZOozbijD12wZcCK/aw7HDtSAH6Z9zHBYNGRm4i8zJ9RR3qE/Iwq+UjFZZ8DEzqTue/j/2zUInOyetT0C12/TeXhuIx574SsOyj166F9kNbM99NdYtU+GL/3Tx7Dlthuxe3ERTz73fFSEJrORaeYToJAsAvFCBpZJ+njyLYNNYjx5LkMMO0P58ROihiVCmlwkIUoOcTo5PJWMlJiTV7fNsbFsZCOZgAqDLWIzPMvkA07VrFKirhFichepdkxzj24HVVogcecsZuFd27cjlAlHLqP5des3SMiVpGO2FQTTE86XjiWHP9kqiFROckB1rN9fFnI8ZfPek6j9eZQSCCaMZOc4RixRzZCiP0ROjzoOj3vmSw/NPTzgd1nNbAd8CffmBTw+pv8XPdxw1SdNe4PlFMvEMney4tag5mCoz2rMXuy/CbEkaufgfDaTBaiQL0jggWUjrXCTpuksRhEGw4Eaz+ZoSmmFFuIG+3Bk2keoshRL87tRLPexNL8oGlZJJgjnOlm65RTgAdauPxJ1HmLXrl0KFp3V2Bpr1IhpDVWUaLcTdNfMYmrtOhT0LWiZB5tnhKhflxfqFRLkGffz5FVnfxbBeFxCEiWyM5tXSFZLgoOuGYMtxJFnnYf1p5y6N7fgMHjOarAdopvg1LQEUTZww+f/CY1BD1VIpI8oYG6G807WjQvaZzZ+QBkc8vzC5rLTHoE7q9U5519iVNQ4GfVBUIVQHf23WZ7RErgcDhQ8QhCTNuqCE9kj7LjjbhTUP6GkNzUq0ZDIUBYWMsZtcfYsNCZI3WyhT/tcZtXQhF55dgsrgjsmJEvUkdB9Mt1Ce90auZsSQOH3MWDGxoao5EcPbwEwVY52gyM8ybj14SUQFFg6w1nDm8GWsoVQRGqQn/Gqn6ZK0SG6hwf6NqvBdqBXcJ9/niXfbd/4PMod21BHGUraIrHpnFPnkVmOY/+OAylSLaXHJ4KNuv1EFp1fAI80NBwkLYtCqikb1K45Tfi/MzWDVpEiUSshkC3w1s33YrDUQ1FR78NUi1mm5mysZ6k0RRIRo8x+l9kllotpjd7ysjQnGURRbHxPpVKvEZKOkEhfpIFmt42puVlUDR3yUMWcNSuRBA01wLWRlCVaVO9qt6wlQd+4Vss16m3EZsxkIRAjqYYGSEk+/acuARqHq7jP5NK4/0DTmXTMg1TxvM/rabXP9kMuGcuqu2/6KgZ3346iHqCKAoSkLHHXJiKnCWxDHQUIsLQjUiiaU6YzmAjLtZvOpgDrkEFm5zMGG7Maf5ZIY7PZQWPYV1m6axtdQ8nAz0R7KhpNWeOyvGSp1u8vI8pLNMiBTJrKPoP+wIjCIe2t7PwVNNkwN0fQqt2UFPiIsgr05WamHo3olohGp4W400JzpqvSlBtD0mpoQJWv6QGhThLJFXVMRHZT295kRJqYGpS1uTaUIZY5WfDqN1IveZ8X6KH/gUm+4w9799VgO+j3ZjC/BXd87Wr5ArAcZAnZkOk7BXQKZSYuLJaAhMcFf0tvhIKmJvnmUT3+3WgwQhVFSNlTE9BSImPfrK4Qk93RTzHs9TDoDd0EOMGLBCNZ5RbIqxpr1641ACav0akTjdhQ/qAYDnH3PXfj2A1HYnrtDEb9gbr1kjegAQeTHPtgFOZpmQspy1RyLQkCRc0YYRxgOOxJ4Wt2dhZT3a5ZW/FXxUFRYGpqenxek1Q5G/bOjZRZnEfVNBsiKEJtSvMIcf5rr1CGf2Q8Hji72eff90DjT61mtge7+9UI3/r0lQjqEUqaFFJqgJw/qm5xVsuVgU3KgFMX35GCNRdG5xgmFTbI81zqwmVeogxDLA9GCBqJRFR7u3ditNxXKRfHs+j3yRQJIWl9CrPWIQaLm7HjnrvR37ELGRvfALqz6xCm5POGuHfLFoyiGkc/5kQs37kV66dnFdBrZmYlNMTNYMOGGXEdm50Otu3aYWRpaqdMt0V43rF9O+KEev9NnH7Wj2Ht3Fp0me2IQJLuFQSY6rbGZaX6c471T71KyeaJQcLsOUJQ5BiyDz93JE678OVCXh8ZjweD/vfve6wG24Pd/TrHV//1I2jSLzq0/lLCZrKUsjjDRilxk+UmQdkIx74PZee5OieZOFXJyACgsE+2PEA9zLFtYSc2370FW+/ejrTVRpeDlnmFteuOsrZBGJswbJpKKyTmrB0zVbsFinsTYbQApzRxhKLK0MAa5Hml7MSmddyMkFB8iOwtlpXyTysxKkaC6JcGi+jNL0oPc2rtWk2DbzjqSExPd9FqN8ZlI8+EnWYLzaS9AoY4DcmwQS+5lqTGKTNOUKQuhuijxvFnPR3rH33mg13pw+jfV4PtYbkZ5LF/+XMfR6MYoOk9oiPzIKMAN3taZLcL5nd0Kg8mMLPp7zmQWRRaxAw2ntny/hBFb4TF3hKGFZvLzJIN5Lt3Ym5uHTqtLpqtKVR5iZmpWQKjJmPQYT8twPLSIqqskLvoYLisa1OlFbJyiEY9JUWs7nQbUZM/E0unn41uZdjRQKUqz5/D4cAsgnkOa8ZoU+WLsH1codttIwjJrSQjpSFeZbfdUaPcdP6N7iXKl7KaWUaRJSJmC2XxEOEpP3kp0CTxeP8ywqG/8T8YbAcKjqyWkXtzF2vg1u9ej/6WO8lUNiIww4zT25TY1qybMwm0fxw3uCXzJnKyjeQQ+KDu/TCzhjf9vHmmobBqVURYM3sE5tbRDMOcP8v+UNB7xrKVPTVKnEfmFcDXJKhCXUYKyNoUAulf7A9SYidA0ooxO9VBf7gkkGM4GAi2z6kTQomGMMRUu4WsTs1xlVPfFHGNbb6OUgwEgIRwkgVDLcpmy8jFLsjUxOd5jrIJ7DmWpkPCYBvUGRZGwHMu+znX6F4NtomhiL1Zff/BnlMD/d69+O6XPodY5hrejcZ6UfUgNTeYiL0lMj7s3zVMyp2+LsSlFMOEisppLmHVHifBea4bjQSvr+nOavxF8mhsIQxHKLISrYiWUBlSlrGE27NS5yEyRySuk6dSGlZ2cdA/BhQIInDBCe0IQRRgav203p+TBUnCs6Ehg/zM3a7N1bEhT4SSgSq9E6Ypqn4VnEKAZM3Z/7NzmQ2oWilN3RbXfC8MmWUWH/A8O7sR5/3ExWO62uG/en5YCbnvwqyT33f1zLYXd7+mGNBnPyatfy0uifzw5F8hGhVa8JwlY5zJIdRxGylyQ9FTP//GtxpS3o2mFqR6kQBCaN2ViHGkSToFBU0v2JRmcDVox1uwhWDCQv2lZaSjoQUc0XV3RvQjLw2O14REEBnsFFqNMb12Vq9LyfFmd1poqWbY6JATrRiF+MtBZkyj4lGRClk8g1LqgGgk+ZZ+JMc2Fm0wamoz49umwDNqWgZ4wstei87ajXtxlQ+Xp/wwJPLAMvNqsO3FPSYH8muf+xgwGsnUnWVUQgoU0cgRM4vNnYVc3I6QrBrdTWZXlQ2bFlS1ytiXC6wHV5aI3diNzxTqTZHJwcWdUU7AhFfTZZPw1jhbWgnwII9llLHs46iOkZk19sIITOmhXaHRpPRciFa7LYKzlYfWgGYmlGnGdFMT2BLs4Yai/pgRn3N9N8cFZXOcA65ujEaWUxqroaQD1b8S0dEI6PSHfQybCV5w2bvNU+oR81gNtofxVskMGLd/+0vYvfl21DkBBfaoTP+f9r6UJDeKUillLJ9hOGJiYj/GpuArjdIRGlGiTEg4nowQb9LhOYc01hAHk3QvZsGMsnN27qPEHHnR1HEkUUyonxuPZiOdqEVNhLCqZHHV7rbkVMOgYOYhYiihVqputThM2pJv9iTPURxP5yaaF9SzZAvDTDKCMkDk5fk4diMNfzd0GkRIqwgFUlHJ8tYcnvOqt8oq+ZHzWA22h/1esWy89p/+GkmQIWnIbl4kXQIkhPbZC5M7aLYSbLb4eexZES71gSF1LmYqN+HMhayYkZS4nYckgeCGMPPUGBli4BcT7qYUceA0QM6zlc3TsS+mkRyCJPT5pr0UTTHatIqiEw0VlCskFFtlRmvYOUvnTAr3kEhN4wACJGzgu2wtgQcFmz3XT4vz9f13Z99uWAVYrgs89UUvx5qNp4zHbh72m7hXH2Ay2PbcJPy/7d/msVpG7tUNsKnj6z/194jzBTSpSxLUAkA0oZ1n4jepxCQB2T3I8uCDgTpWQnZ+1Awa3TLfFCYTQ5YwKzIKDDYGgWbbRk4igZzDrNTP1VK7YnstQjqkkKsxNDTVTXJURJ3HWLIHPMOhxfEYEwOyCW0LasqJjyeQ6YzDQOK5tMHnGKlYbjrMw7UJv+o5LDM1tCo5VjXmiUqOggSbR0O85m2/gjpsPbISmyOY2y1cDba9DI+D/7Tt996CLd/+IomPUr+qUrL1GyhGQzDzcHcnKOJn2whAGFhifTavdsWF7oPN929oTMH7HJLY7LzO+HO+xGSweRkEZjaqJzcoHsRyL4w0EsOSVOc6GyczkR8GV5wYiZlnNAaZEEnqnzgjepddxXcsDcrna7CUtPk2J7vAz8cFyIEG0dSoZWnuqqKv8TMEQA9NHHfWOTjpnGe6ib/9ywQH/w7uzSuuZra9uUoP/XPqHF/4+w+h2wpQhQWq1EzcJR9HWXCWcRPMd83AOXEgwfYuq43/LzFTW4gMBmYJ/Z4SBjwX8VzoMhv7Vnww0CQiJPfREk1nMSxgxaOEDHoWkZxRq2uZ1ctLTZZPDQWczm2azKaokWVEvZebyKbQUUGHVafobIinU/XKLGNTVk9lMaX76kLTELQEGgRNPOvyd6GKOBW+b7ZKD/1NfLB3uL8zm7PrebAffZB/Xy0j9+EC8jbc+e9XY8dd30PIdlNmN0Y7e5YqU7AnZib1JDY6gR4nIeDnvJTVmN1oBexKOqKA9DNT85gN44A+pys/T5CEC5cjOlr4dBulaw3PV67VQCU7BhKdaKjTyGDTRAHLSbKQaX4hj22O6Zi8nspc1zvUc51dL7NlxszpBkQZVNQu0VgRQRudHW0mjp+NeZWpebks0Np0HJ764p+R94EP4n24zA/zU1eD7WG+ARNvX+a45l8/gnY1VFaiYX3YSBBWI1RkkxQ2ojGZ4cZmGtRkHMvAxa5kc2bwRBFLAz401e3Ocl5hi41zX34qS9KDiRohkmrwA588Uxn4xx6bXoNE4yBAiyRqZk3q/fOXIxHz70Qy9jLiLGcZNmwmBvFYuo9/l46c2JDKy1JtCcL/RGCrKEaap+xW4BmvfjOaU5sU9OOz4OFzB+/nkzx0COTkm61mtn1dBHWNqz/1UYT9nWg1qXblJL9ZgEk12QAOr7ylACnz8bmHgeLZGyasY2hfyAgRYcPkBnxgeTFYZk2Vbq6BzWCT0+mE4I9UkSmFQLDFC6iSakX5OvqriVZlNr02FmNnPJKpfbDJpJvvI1WsRMHm35fBJmk7ih4JFKJaskn4pRUwZO+wNY0XXvrzpqj1iOmvrQbbvobBIXt+mfdwzSc/gpiN5KCQhmNRBQiIHhKddJnNM0msXHMqWm6hE2CQ8oI8tnk2M+SPWUKirQ5uX8mQBmbouTQXLKh/Qmk6g+AlW8AzlJM+F1+RUwJJLICEAiXst5FWZgJEISIGGz+bcw21ktJEY/k6WUbIRRKx+k5Dz2pho5vULvYRHfDTJwWtBJ71skswd9RjqKNusniPiMcPU0A+eODOambbr8VQ4upP/z2K+W2Ypm6pDDMaCJgF3OiNLxc9wOF3eQ+O2JSA3WQ9l5lGmh8kFLPnZqWofyj7UcvEoZ2yoHLl5spUtOmhECiRZZPT9vfmHTQxZHfBnwsrtipqCy5f7kmfknJ1po6ghjzPjjyDqslesg2QimjMYGMZyc82agQo4in8xBt+ETXte0OO/jxSgk2F8wOshNVg268QOWg/VANZuYzr/vnDSEY9VV5V0EGZDhETMJ8o7fieKtWcVLcPQisdTeZtMrgIKtikgCkl+4cCj2Tn3Jl3cN2758j/TKigUazUAqAjKMEWp3ilc1kc6sxGhopQQsqru0nr+7yPymBOoxPSZ6OcjqcDUbVkSl9RFdlpaDJDc6nGCY457Vw89qkvGKOcB+16H5IXWg22Q3KZ9/1NeKIJ8aVPfRTVwhbJBZSNBOmADP7SsoLm3ZwnAJ/N8wu5hQRABJW7/tQEl1KB6WybfCD53pqg/JASeT4IneGii0cFG3VGXHvBgx6NmH01BhqJybTitfEZPzZjWXXlPEj5PF/+Uowkz+kNl45VkDnS48nGltGpDxlgFOR43qVvR3Pt0Y8QUGTyrq+e2fY9Bg7RT3hx1iJdwDev+gf0d++wYCpCgfX+bEWLKP59HHopcgsQy2QcsTTpOD+q4rMdSzN/VtPfEc5nLBG2kCxlLSRUsnmKTis7xyWqprcNjfRsEUQsF8kcsdJu3F5gGdmka2lkhh5uasHm1JrIsgEWFxZM85IiRkQ73fvphMePEjQEjrz4Lb9kjJGJjHyIbskBvs1qsB3gBTwUP17hqiv/DFHaE22rGcXIOazm+IwCOaikFaQ2V2Z5S35sROvUQqZhhXiR5vjCx2R5yNFnlob8UQWUG1QlwCEWP0nHzKEyCmDvjdFugAYDig1tRgRPHnQYVZEpdJJTAAba5MUA09Mz1jCvMmVdCQXVFdI+/bVNcJa8T43TOHlyoamiizUw7HTxoje8k4J6h+LCH+T3eDD5Ol69A7e2WgVIDvC2DXbdg6989hMo054mnWka4c9cMj4kchgZ+8PzDLlwudjp6ik2B1FFAiOO4OtBFeGA5FoqfVgWYY3q3Ua9JwCBCAUX34vlKWXsXP+MDjxyU2OvrdXWaA8DnSWlnzIoqxE67S7y3MH6VASLGhikKXJaZTm3HNHHaP1ENWeVohytIeKZID7qWDzlJa91G8oBXtRD9uMPFmRWieh7rgbbIbsrD/hG9G67+h/+EsFo3sAJt/AnEcIUrh1QVmi32ybRLRkDSSjrNqoZPTF4qgXOoJCGSY00G4y9v1VaEpRnpmNuocijk2MwJoejgNEltMwE4DCgyVbhzBoVu6gzOUpTO8/Jw40zbhIVcSRjYOf8bnEsfVlcByXikqwVds19oLWQtNs4+qxzcdxp5x6EJXko7+lqsB3Kq33A78Wd75p/+muEo3nUowHS0jm4OE4jg46zXeqDUVCVw5tCBBtAkY2zHctMncuoqExisfyxV7iRJP3SOMPgdM6rEYhhUcgzkzFAMnEtLbP6wCURRCb2lGXwDXVKILCfJ+Akdr5rVv7yM/Izp9S0pAl96HuBOaj8z8/Jz8WM3E6aSMieacQ44ycvRXv6iEfQ7NqDaUPqZHvA62PyBVbLyAO8nFyiX//Cv2C4+24UvQWZt3tQg9xIBtmwZ/ZRLPH8+EqDysYwIIMBK4Vi54BDDiLPdUVuJZxKGZVtRDKJkISaN9PwKTOgAyyIkaqYJBGZCmCuT8c2AANZgc6s6kAXAh5xqykbYJ8ZNUrjWhf07ab1MZv1HETlKZNFKYOw00jQpF1w3ERQBTj7tW9BEE8fjGrrAO/I3v74arDt7ZU6rJ53w9c+h+H2ezBa3mWDoiWDi9PLxoavs4aCRiKtxhZWRuLiZSlpQUHzKVK2+DOZDabSmMIheyrxdPbiZLeBFwJWdJ6KjMZFmykJ/DipOR3x/HxcLBUuCbM6yXANi5Jo7MpQTRMENp1QMYvKGKRCzD4hmSX8eBVlFmKd/6hTws/MEvSsV11OUcqDng0euhv9YMF2cLOa7t2qgf2B387vfOPzWN56J8regqarR+nA7HuF2jHoDLBgNqJQKwOFSlYEG/wICgPOgodc/tJ5vFmgCKl0bAwGnaYHQscWIfrp2fs6c1GmwBaSz2xqNQSc1uZUtzFWvAyCXFP9JLbLaMZ2sWAOSQerSmU2KnYxsLkxtKamEMQxer0+1q1bhye87HKBJY+c1LYabAe+8g/xK3BBf+OLn0LR341qcReKbCBdRpZj3jIqjFryY8sdD5IZKxdUn0lPhAHC8xfBErFBeFDics+pnmxfSKUehYIYlGyMs0FOWiMzaOBADdezUzZ0/tlqHTj5Bapl0YWH5aYoWS7IKKvgf28oY42Es2wly1L6o3LezgwSCdjIN4DaJfSOSxL0sxzPePUVFmyPmB7barAd4lA58Lfj4r/xq5/DaGE7gv4C0uGy+ILkD1KxWMGRdFBVAfLAMhr/LnfKXAxIsU2I7lFl2JlxKJs5txue10Q2LgzEMEa/yZ3TrYYBYjIFXECUV6CLqDXYGWwKTp65iOi74VYaLzKU9D4wypirUyXvwKBpF5wqYJZliWuQP7NdqzuFsNVEESc6ay4MlvD0V73Dmm6PGHGf1WA78NV/iF+Bi/y2735ZyluN0TLyUc+NnZQaJNWgaEwzwRoFhVZd07pk4zl1g6CFMf/VLCbS3zSnUp3HnPKwej0EQpxOI/mJ7KGRu6iij/0157vGKGOvjWANz10MCIr3NHUSZCVoFC5qSjKjNR3bhGfMRO9TkXCiX8ys3sub0GSLPbVuU1m7HTZRDGqkc2tx1kWXwML7kUI+Xg22QxwqB/h2NU1tMuy65xbc+b0b0K1TDJZ3aXFyqJSLmV5oeRUbWyMsqBqgkpKPRk3/NYP3VXbmNYoowJBiQoTrOYvtmCBibwhQMf4jtSJpWVVy2ltDolTRcpmKhGU5H1pDljNsDBSqTopv4loFeh1Z+VL/UfCMsp93Uo0YnBSfldRdhDCJNcnNLDtFt9KMntxDRMeciMc+68UHeDEP9Y+vBtuhvuL7934MGFTYOr8DH/6rD+HNF78GX/7Cv6FTUtauJ5FSLuIsp60uzQ8jFCVLx1TLndAGPdU4+xbBBjepiMU0QpEdlpiiZ4mmZcKp/CV/a0ftIiLIYAuYvdxQKHtkKhn56RRsrOpcsFFvxAEnbCFYE5suOREa9NVWsHEmj8rLJhhEDiSJzJwSEM+zlSBpNpFwdKfblhp0lgaYPu3xOPZxT9q/a/mw/dRqsD1sl36v39ikE/FXf/cX+G9/8kd4/3vfh9OP+xH8+/VXoVUM0WR26y2hGTekd09JhCog9F/L0DBn4DAcVKpRXsCpKPMDlExuFWrXI/NkZH9+I6eR5SEpXJRQaFDZq8oFsui5bCmwKe5+z+d6KldA3UhqRbJR7XVFOH5D/iTDWIKw/Dy5MltQmUlHpGkB+8UJDckkAAAgAElEQVTPRcif9lCinCHCIAOO//EXYHrtsXt9CQ+fJ/4wJa2D/ylXof99uKY8L21fmscVv3AF7vz+TajqBj7+kSuxptXCXbd8B4v3fB/xaElydIPestIDoYWMBOGSwlOVOYgyW0klmZ7YdH0xkpUPNgaNHhKucqJC7KER6dNMG89q0pUTY18SBh6BVKuhllCPhFr9r6iB2KOXfjDVSSYEmsjmJ3UisWx+R8xiBGzca1P8td1CtzOFYTZSBg5pfVW38ISXvx6IzLPtkfVYDbbD6n4JYNM5q8aO4QJecOELkTQ7KPsLmE8zfPHzX0RUpmpof/3aT+GIBktHyy6jXg/tVoI0YyYKUTXoh5YqgZA6xeY1yb/jcRrpMdbE2Y2p7+bihGiGZIEQuSys5Gu39LlGLEfDFZa/8A/FJEUJzFPAM1AE/QtdtGCmxRQB/WbDXGykXyJ/e++5RijfNEja7Y74lMNBKoHauFWjzgIsFRXOedWbHyF+2XsuLR9sB7+BfX+LeDWzPVhoO2WCtM5x7o8/FWFGMm8D5WiAy9/2Drzk+S9AHJjexm03fxWju76LctBHwowQRti5ex6tViKMrg7N/pYlJQOFdKiU3muauLZAYwaUspXYG8YE8WrKDB6e9QwksXk4Gc0nDZ3BvKQBQX0rCW3GjWUjS1/2yOiaw8/aoGyCg/wJ5PA57L2RX8nfSxqd57cwQqvZ0Swc9VJKthziEEmV6v0W4w6ectHPPthVPEz/fTXYDqsbwyU/LEZ4wWsuQpHmGMzPo6QN7/p1+Nu//jsZa3ABsz+2PFjG96/7JNplH+2sT3ARZTyD/uI2tIIMCFoKHGY+npHKqIGU+o/S8KecArMctTwoU2eB4gNONC8yTpz4DpHCIgSSfEX2js+VWBBIy6IHtjFHBISIX9lCGFEqgeYcpm8poETS5+YN4KcPSHqJSqDV6qDTnUah8ZsauWTLOeYzQJQ00Tj6JJz4hPMRBo/kObbVzPbwBx2h/aDGyy57DXZuuVcjKRxV2bljO379N38Xzz3/OShJ1HXDlKRjDRY249vXfgbTVQ/dRhOphEwDpP0lMyOUdAGFfQiEuN9zCpqCOjRX5O9DIwubB9zKQ+Wgk8ljdmPWSyaekjTblvU4VuNgfT+xTT4lLZ1CqiLXlRghPBRymTHY/IPtA43UULYnjNHpzgro8Sb2fC7bHRRxrdttHHf6uZg+6oRHkGzd5BVdzWwPf5BxAZYV0rrE+c/7caw/cgM23323+IE87ywPBvjMv3waxShHM2noXMTMkBUVBqMBtn3v68h33oWZMEe2sBsBLaAoMTAaYGmph2azJZk5ysCRosUg4hmO2VGZzwm1sm0w5i26AU47a5HZb4hgQlYHcxJLRM2mEYE0lWaWglxOzGDslQVhbKKtDDY5kzoupuNW2mvbICp7c00w61lQW9+Pvh8lok6CstHEvb0U57/g5SgiJ5d3WNy5ffkQhy7YtFGuEpF/8OYw0LK6xCVvuRybt2zVZHM6GGqyutlu4/xnPBNXXHYZWkEg+10mIOMfmtlhY9TDlz59JdbGFTpFH1VUG9VKizpEvzeQhAGpU7zdgvbVWzNZb5ZqfsyFn25S04NnNN04numiEK3apMSVwZz9LjMWm9BCIhsG27N89I+IjjtijbgZNqeC7N/LRFxjRDmpKJxh459V2YoGFrViDKImph91Ao45+QwslQHmKFt+4MoB+xIpB+G5hybYxqNOq8F233umMg41fvrtb8OWu7YgzVIFAc9pnDObX1rEH//PD+DUx5wsKJ0QuOcsKigo7DhMkS7vwD//7Qdw+nHHoS5S9ayYNWj3RKvc4aCHKs2c0aB9Bs28UfBVoyyW8SYfPug8F5JtgakwGZdwlDrgc8bS40pxVpKOWwBqBTh1LS/uQ66ly44MGDbkWRK3iICSOdJoIssJljQ0ac6MuTWPcPbzL8JyVuLdv/ObeP+v/cZBHrU8CLH0oC+xGmwPeokO+hMcJ5FnryoocdElF6OuYmy+eyeSqa407embRtrg0uI8rvzox7BmZlrlmryxEQrCJ/E4DKlAFaPOU/S23Ilr//GvcdqpJ6EoU5TposwmdFIKmjLWYGlJQZ12u6VSTVPSLC8FRyjt6evaxNtKUMq0gg1qZlXq+mvOLTLGSW3EYlPT4pS2aVTy30xSz35VkkoPELtMp4BUW4DPY1laoeT5rQpRJy00p9bIomo0qHHUGeeis+k4ZeT/9BvvxuVv+hmccPTxj1ANkocWIFnNbHtErM8hv/6+38A111yHOqL6VAvDkp5jNg5DWtOR69bhD9//+4LJhfS5CMjTSs1m8gyLKBFI0Q0rbL7hK7j5W1/EkbMtrGknyIcDASNpGWrSuihHOqdl0mP0Ov9GRN4zu01mOo5tlhEDZYVYzK/kfQQIxEjFS+c3o20pYCMLykl2SkJxV2/txFKXhocRpwU4EZ4bajk9g6qOkBYZ8rmjccY5F2A5rTWt/Z/f/zv4xlevw5Uf/cRB3wcf2hdczWwP7fW9n1f3jeuPfvLv8P4/+C+YXb8RaUrDwhgjOrRQC6QuEFcFnnnBM/CWy99sZ7Q9gk3S4XR3iRlyQ4iZkfex+bvfwsIt30C7DjDdilEEmWB4MkpqmEYkm9Z8H/au2BrQAmc5pyxnDx9sGv6kL0DEzFYLoDEI3yx+BZk422CXR1HLqYZzcE6vcqxfWaPpfd08C0XACAM/RtSo0ExiZGGNzuwG3DXfw9nPeznSkghqhelWG7/4q+/Bjd/5Kp7/vBfj8kt/5pDfv/1/w9Vg2/9rtz8/6UjFH//Xf8QHPvgBzEzPYXlI+YIIg3SEUZ+BRqZ8hTId4N2/9Ms490lPGrtzSt3KmRTW8k5LUZJ3xecTYKyBqarAjZ//NDDcjigdqKRrd7saJkVgAjuTIj3elF6wvMtwft5sXJI4uJ5Jly40Qiadt5rXNSHJWMCJQxL5s54d4s9/Yv5XK5ZR5FDq3MeytNVAHPJnKgTdKdyxmOKJF7wIjQ7bAZThoz93C29957uwuLiI67/yZfz5n/4Jzj3jTOc54Ove/bkxh+JnHspgWzHrGEta/EcGSKTLWAd4z/t/D9dcdz2mGi2MOBaTZyiCEkOCI3lEjwgFW52P8Pvvez9OOuEEmwfj8KUrI6n7WBVk7w9lgsgeGS9yVEaSP6Dx/fe/fjUGO+9BKwSGwx4alBpoOEnySRON2mnwV3SOGd6nnPTBpuATwMn0avQqBpsPJgWWG8mJYkMifak4GbhqGdQuKJ2okHp4ZJHEFH8tUYcltg8qPOYZL0R7zSaUwxxxQpTSpO8uveIK7NqxgGang+uu+yyefO4T8O53vROPOflUxJxqcHJ4hyJ89u09VoNt367XXjx7EtOz2DCkb9dgCW9796/h3i07kQ0zTCdTWEoXUYa5CMCjLENYxwgzwucFimyID/3p/8LczAzacaKGt/e/LlPNPYttEtEsg+MvBRdrU4yNMhggKXrYfNM3sXvbrYjbXaA3jzzLkVBmIFZnjPPQyFOO6DCYja5lMngmhyfRVn0D+7+CS2Uj2fw8p8XKZNIVImSPwNxGrbhEQWaIM+8w5JJZjQALz5l0LSV201KAt/rLKNevx+3DCk951nMRR029/qgwlJKPZgi8/ufegfltO+V1kGZDCQh1p6ewe/tuJFMjPPnJT8Hb3nIF1s4cgaazDia44/f+hxae2IsF8hA/5T8MQKIelmhPlRq+eVng9q3b8au//V4s9RaBisbxNP2rxFuspctfot/vS5aA2QolgyDDKO/jyo/+HdZMTyMOQmQM2rEsuIU0A1K8Rs6m1VTHoqk9hXNIzyI3McWd3/wy0ltvQdRu8qVRFSOMRj1Ro8JGC0k+GPMavROpB0usJ2dSCv73LPMss7U1UsP5NiKRRBXliuMCVLqUzcbYfpca/SstAQNOgkas68TGd9lsYnu/wOOe8RxMTa9FmdM6qkKj2RGgWTBjRwEu+4Wfx+6tOzAcjSS/x14jFZ9ZIWw8ci22brsHrWYXW7dvxxPPfiKe95wfx3OfdQG6lMHjeXGiqf4Qr/uH5eX/7w42h5Pzf+wXff2mb+PzV38GX/73r2Pb7p3YeOSPYDRKNXiZprnRleoC/VEPRR4gLANkDBQqHKeE9mM0mkDZqPDXf/bn6HCAkp7TzD2OVa+eNPtkg2VU0nEkOSQHajL4K8RcvEGGNKgwE8S456Zv4N7v3oBWp4FuK0TWX7QeWw602JB2RGR//pJsuOuLKUAq52IjA3s7iyUtlovkQ7K8o8aktQuaZPe72bU6mQi2kGDKivSBfLal819glJW4I2riWRe8GMHMOo3ccANhD8+2LiAvyMEMccWv/DI233kP0ixHlg/0XoPeAM1OG62og21btqnEnJmZdn27BvJ0BwaDAa54y1vw0ue+EC2+/v+lj0d+sE02npzBA43cl0YDfP76L+DjH/8o7tq8XYZ+HA8hslaWGYajAY7csAnzC7vQ7U4rm5n8AOfLMtNeLIEsG4lKVfIcVlD6vsKmE47B//gvv48GqU+NSFmNozc0lRftkeKndSYOpNSyvLc2RXfqHCnFfGQVmKAYZSi23oLbvv0VNBsl6nyAqWZb0gj90dCJqhpFSkHsBF0nG9QrYInJKCgIBe2HqILEifuYrqMnGlO5KwlNWYtZTCUpe3Y55eo4JzdEVjcwLGv86IsuQbs9K7lygj2cJmDJyWTEyXNuZO04wuve/nbcu2Wb3pvBRvMPRnFeMOPGCKsAi/MLZjfc6aKRNMmjwVS7bXzRaiQa3JvfeDme/bRnSnJP3/thCb6Df457xAcbFzMzyz1bNuMzV1+La774ZczvXkJBXQ6KkTapkRgpmNg0puxAu93VQoyCVFocLLfGisNaHDlGw5RTnkYC5qKpGuov1Uix6YRj8Qfv/V10us0x7M++sEZmeGLToNpoHGxey4M0qjrgBHSkURf+py4DNNIl5L0d+PI1n8T66YRQAnMowtqcPZm9TFouQJSYpZOUuNzf+bWoUtWb0Dsxn6JuQCqOgakgy7ObNK+EE9t2SmLJqTNhmEj1GMUSsqpCvzmLM5/+PLSmNyEt2GZIkLDctZdA7kpn8jnbcYhXXf5mzC8sIQs4vTCyYVg57pTWkuCb1TUGi0sqRRuNBO3OFJJWC3VYoRlDZ9ail6JeGOGsc5+Il7zkJTj7jDP0+Q7t4z9asPms5ZANnYtQav7r3u1b8YUvX4vPXfMFzM8vquzqTs9gqU9/Z8Lt5voikz4U6Pd6KNMMcaeNJLEdM2GviRuqU7py5mcaoeEANMV1aERv8nAEG2INUR59wlH4vd/6HbSaBAoMlBCHUW/JGTRKHpBBYgBMFFIOwTiPFQNCz6nV/A4oAJRlaBBkyXfjK9f8M9Z3plGlVLvqg6aE/IBLy4vKJt60g4RiP6Nma5jZxpGNXWYD9RwdQZldbXIbGejy8ZDepAWnUMwokh94mI6w3J9HozuNxz73ZYhnNiCoKDlOsSI7J7IvqFaH512S5BwHeOVll6E/zDAscxRlrgl0fmdKN3gJ9nanjZJn4eVlgTWtaEYbGj0FjtywFkv9BXQbHXSDNlrdBAuL8yqPL3jaM/GKl78UR2/YpLMmWx0PU8rb75g/rDObPpxKJ2Drrp34h098Ah+98iPojQY4+sTj1PyNgykd6JcWB4g7LSz3l2y4kece7qxUH2YpSKk2Srk5Qi5fO6np7kJwwwi/mjZBgNHAXF0YrOmQrjG0u+WCMT2Oo49/FH73vb+FZtyyDKnBTvOOFkTONBesABcq0chxpK4jz21sQPP9mAcZ7BRn5fEq7aNdj3DtZ/8N6zst1MNdiJs8c7Gh3NE5LCgqLC/vGJeVPpN53X7vky3LKGmemNQ4s4cPSpZseQg0gwgtciYpHMuIHmbYvmsH6k3H4UkvughJPQOGmcrLwM6DZJPwtZWYC9Oi5D/zDPfyn369+mzMoBw+ZcBxpyD4EdaBzsWtKVYV/NkCo+FQ5TJ/lhJ76gVyQyEPk9e/4kBsCwUKTHfWYH7+Xn4aXPGmy/G8Zz9X4NQj5THJ+nlYWf/G3JBXp50duJMP+/i3az+HT3/mKuya79uiien6EiLLShn31RXPUSbTlqYj09N3faoGR11GKUY08ZNxRAtJnAg8YCmZZxmarRZPaSv3yw1qUp2KicgCYmV4k4uBlKofPe10/NI734Vpwvae0eFGVbjoNXHNItLzGSd6DoIVJt6STtUFv7eayhWadYVWmONzn/wnzGGEvO6jw/IvCpFEHSsHY/IvU5WT6cjkCmyh2uJL2itG9Jqy1p4VgX02TgFUtPltNeTLHZEBkmUKtt3zy5g+7lSc+fTnIO50UJHcnFN6b8V32y6W3SeKtgpTclnmotdejCFdS920wtguSxTMENlg5O6hDaoSVBos91COMkSxnR/Z0/OjPAy4LM8xNTWjYwB5o4NeHxs3rsPdt92i6fjL3/RmdOk350xCDteEd1gEG4s8nnMKhLj5ztvwN3/3MXzzhm9KMXh2akrk17I0BI4LnR/asoihcJxqNvN3WwbiF1JyoChQUonYKQEnbaoRe56HPZciOfcXbNIaYdVHP7TRYMze8NIEP/aEs/COt16Bmc7UeL5LmYQfQmUrH/fNbD6i9ww2gjkmukqKV6XxnXaDlk0j7Lrh69iy7TZ0Gzzb9ZFo6DNULyxKjOUfaLrbmCcU+BGhxKGSLBk11e3sgBsJS1KWsjw7pgjyClk/RS/PMECAIx99Fk4779koAgaljQE1iIK4cpExxo2EjBFdf/oAUMnLChBc9LrXoDcwFFKyfO5zEXQi8MHzp5c7l+gsX1d00wxFZv3CRmLnR/6spPmUOY1gDZgtFZvocV1gqhVjx/YtePLZT8K7fuGd6DRbRoA+DKXPH/Zg4wdYHg7wwb/6IP77H38AJx5/IubWzCH3c1Yye+CFt7OCaXCEQv0oksMQFUJtBs9CEAkja0aMjVuqEDsQgYib+lIOmqcvWiOhduMPZjZmRDGt+BplNgZPvL7HE550Dt74M5dh/ezc+ODOMBasrtKQTe4JSN41ni0n3Dez6aNXgaTuZGkoDiQHPGvMjgrM774T3/7Gl9AMhphuxmixrCrM1UbopBuTscCz60SQgf9Xc7ykYBBn7CIrVYWWMnMP0V+kgGyE3WmGE896Ek468+moW13Z9co6Q+yTEBGRC/fgGVhIJOlhZvfmdFOA57/8JzEYjUTd0nbjeoAMJo7keJtg9QxFH7Ng4xG2GNiALCcX/FmXwWYsFtLkElRIEDDLc2um+2k50tAu7aru3bwZr3z1q/DTr70E7Ya9/+H0OPTB5sopHrBv33InfuN33itYmCUOWQuciJY8mzON0A1TTW87HSlLXPCyXUqHWlC8iWJWcMcNqMzbUxNaLIsJ7Q4yHvSFCYm7m8hAajlqkwAM9xgNDHLnexJh5IPZ0xvRn/u08/CSF/8Ejn/UsXpPhqwmN53AgMAKNxdmG4EBJQqAgJ/LAkXPVyPOhH98CWo/TwCnQlxUKEZ9fPnz/xtJ0Me6qTYCokETE9YmFOQWr4jRhtzxMzfQ0mdZiZYCWVlgMe0Lit+8bQFPes5LcdSpZyCg+GojkayeTQS48pTnp/vB3z2zTGrNAM559tOxZt0RVmVw9s9NlXOSvdVuIsuor0IV54YLNmO28DrzvspoZGhS7dq4iGDKY875GIRGCKDmZYxMbJo4qDA3ezy2bduK6VlgYWkbfv3dv4Xzzz5fKLRobHZj7PEw9BFWAs3W2CE5s/H7XvW1L+IP/8d/k1E6dzcaBNp6q432RAcWDisGoTTqi35fSBbhY5YT3sHF75yVSL/OfYUGgc7Y3TMtWLZYyekWN1/DlTmC7iUNnps3GQc+WYqm2fh1+HMWKLZI+F5nPflJuPDHn4dTT/lRlWjMVv68xGluu6Du3lpdZ68BZmaWfCvBxkClIJBEj1nymS65UkbUJLAQIu/nmGsW+Oy//D3aQQ/tMjV5Az8O4yUP3MLyYAm/WyPo2ObDzFlXyNgXK3MMiwDbdy3hvJe+FjNHH4vZI9ahSIdIGk2Tywt47jOPN6ky30+aYEyJQeKcUk954pk48aRTrNW9R7DFyYqupRr1KiMb4+uka8b1sLSsc5nO5+2OLANsYtxaHzwWKPsFlEynWpkdKxqRyfAR7aTUOwneX/33a/G0c5+K3/y192C2M6VfThXzkCa9yWDThnvwichcuLbIRmWKT19zDf7sL/4GaLRRhT10OzPIUi70VHeTEgC8kPJSycmgIOuCHywTYqjDszJcpszEkoQ3pBg5jqDUc0rB+9JipOovzygTxFufSfyN49ll0pfM3wGdDQvjII45iSoTWZZl+LEzH48fv/B5OO/MJyEjO8SxOgTFc6FqZsycQA2YsABETUUtKxNXAoKxxfOWBTV3Yp691EdTyWm6/Cz9WkmMr33t04jv+h6mmeFHAzTiNtLGGkQtUsF6en3fk2JWaPZL9cO4wYyKEsMyQzZIESwFOPOlr8TsSY9D1Q7RVCvA9CNt/s2RoLXIf3Bt6hgYuYCvgZ29Hh7/hCfg5NNOtWtGBWhXWZRk3zQ4kGqvIxWGPYKNeyG5mgSK+HOqUFgSJ8xuxg3VRqzPR8TFpNP9puvVnf06qYsaa45Yi21bt+CoIzdguLRTgX32WWfhhS95Hh57wuNcK+ShjbvJ8tHWAP36fuBv9/9DeGLpd277Pv7n//pTbNu5TVPLUdBRD6qsGBAsFQh+GJrIDDHqsz3NkosBaJA4F7Okuif8oXX2ku49A9qk19hXC7jQ8kqBynODv9l7fpPxLvkAXDy9Pg0seKh3xF/SpKTRGFQ45eRT8PwXvQRPetzjEbcTBYvPbL58VJZ0wWby39Z/U4k0IXeggVSBKu6cNXHWk1wdZ+P4o3lmBvdRiS1f/wJ23nMrphybJAgSo4txBMZXTa5sjZXsa6R1hTQK0VvqY1QGuPDCn8LMsSegpL9a0kYiUX+e60JESSwhoHHw319a0yCqndco8fD9u+/ERS/7KcytXycRV34vK5sD9QwD2pW6v9OUOoNNJiBuH2Jh4acC6hrDwQBZf4gwbigDMtiImhJoYYUTN5sr4JRDqrVmtMlSvqFAHc4ijmp0u01UoyFqepe3C0RJgW13bsOmjZvwnnf/Jk76kR9xgrUHv5VwUINtpRTmLhyClhHXfOVafOBPPog1c0egP2BQ5ILmmWnU6LT9zXTv+z2VRIQHuFB9gGih04yCwTk+79gC8La3/qzmg4klXydzhu7On9p7Q09muDEbg8pWCtVJ+Wk7R60Em53/1IcmXFBnOGrjJlz8uktw8rHH44gjjxxD/D7gLJ26+pysD39gZ9fbndN4XvQPAR0e6HGZWAvHEYc5sMprqEValejUFb534/VYuud2BPkAXT/e41xpWGJ5gIQtDk2EF0CvDLGYFbj44ssQTs/pbBdy0pwm9Mxq1K8MOLAa2aIeB+7KduVUFcaBxAHXPM3xz5/7NN75C7+ATccdg+70tIIjzUZ2bTQSsRKxDU4keEaMy6C8WpMDsvz9YGkZSUTtFO5bK+U/r1ecGB0tR4EkpnQfS9KV+xjWEfr1LKKgQBKRcDBEFFprodlqCIklFl1kOeZ3zuNVr3oFXv+6S8bkbDuFHvgR76AF22RPYykf4d+u/jd8+KMfRrc1h6XeENNTMyqjer3eyiJjs9PJctsBmrux7YJiYbjSQztiXSJjc5q4oyuPGKD2HDur+VKPf8ezw/So1G5oeh5tawVwRIalnTvR+3Ncg2cwC/H7JL8fDDaSlPmOpD6V6Ha7uOxNP4vjNj4Kxx9/vN5Doqjus+smOcUqCyQrV1kO6jkuEH3gKZj9zVXJZwvTe1yr9BC0T+qY40gOR7j5G19AvuNuWVRxxIWlOB98tfHCzWskU9PY1UuxKwNedukbMTU1h5GMEu26c0HLlp6RlJh3mwXD5GWxz+SBHd9f49chu/8P//SP8A9XXqkeZszMyNaEMwbZs7LguWkMerjvSndT/9r6Dg7wEpcyZClJvUurEHi9myJbk89ZI5HGiuOCunvAs+4CjR+pMIYK3Zjfj+dWyvk10SantazQiZsYDpbVBrrn7jvwB+/7PZx1xhNsCuEgRNvBC7a6xr9eezX+6K/+HHGLJVYs1xPxcatIaJEWiSsP6DOdjggdE2jgrJhB8yXN1YX2cXTDFq4CqSJyxhkwu/h2jjBUMcuM2eEfXMRZWWF9TcaD/a1HMX0J6ntz/qbq5rElIPDAbrbnHHpEUxsDCpR1jqgxLfpWu93E5W+9ApvWHYnjjjtufKPv84EmMhtvnD6SC3gZ7LrA95uA6JdqzJq2v8ozr1PlFJB5Ldn8j4sQwyIHu0rXferjCHtb0W6YRbCCRGdG43VGZYydyyPctWMXrviV30LY7AJhjIzS4fIDyG3uDW3EZPLTpMNom+OH72HuCek5jjVGowzv+JVfwh233a4Kps5LpFzgU9bb3PNB5okHPXwlb5jJShnHFs7U1JSuwOLOBTMSceK01g6wczCvowZY2YuUl7dOxHL06cfcHs2quCxGQoJZ/QRYo5EgAlFkoRCpph2W4qtIMRot48//5M/V2jlQ8HL/g21iZOUzX/wc/vJjHxMSRABgVAyJiSEoCeMbnCtErSJaVaDfHyGJuAMBo3SoAGPm0n7P0nACjPDBUKaFEErmNk5AS/GJCBlHN5YtW3rVqThJNFXd4cyYUyaWtv1EC8Bnz8mmq+8JKehdsHtQxGdafk4io3GjJcSQ/375W9+CH9n4KKw/4gjMzq0ZnzfGF3fizOYpIz6wfLDpezqNR3mp7bkwRf/izuzMDRWgNepRpn4TEdrpoMKnPvoh1INlNOMSbe9WypKQ1LEqxsKgxJt+6dewxMFW9d1t04oCbmAVwoS7PknKFqyWwe4bIqhfMD4AACAASURBVC5Ru2xil519b1YTu5Z7OPf88zQo2m4Zq8a0TypEnEBnYEixy16T/VC+z3hCYbIN4t7Wb378I2ldlA8UWuvOv6wEOt3W+Fgh8VhHnZM/MQ0ms/4K2BM64IjT6LA+3HjTpecBz4UCXwzgIqLJz/v2t/wMzjn7HLT3051nz2BjFbXXAAl9w/7nX/wRrr7uWsTNDtbMrcVd99yDuE0OHz9oLFJwQVWnIpc4DhnjhLt58avKpp+FOpaFECEzAKxFySF/kSWgEDVvr1S7hqcym/WQ0qGdB4yBwhIjRIsXTE1cp2vvgs1fWOubWVbxAcVS05KOBZvKV0e58sHJ/5vYm00587k/+5Y349gNmzA7PY1jHnW0dl3Z6fpVykWsndYyN//eGxj6toRf2FpYFHnV97GDgrI42dBanSsllj6TsE6z/00yirxW+OyVf4t4uIA6XcaGI9bi3vlFdGbXYlfWwkWXXIbmTNc1tu078LNYb85J1uk9rWE9+ZgMMr+3KWBpRxXS4AO4d/duPPXpT8PaNXMafNVnt6JZ2bXV7WijZKUyGWx+Yd83rO/nTwG0uY6W+xakqkjY+yO7xH0PlpFk1yirO94p+s5qK0DEiQZlMTJw7J6vlMSGBZBsreqmLtGd5hGkQKsB3Lv5bkx1u3j9a16H51/4XLSb5Knu3WPPYGMVZcE2kbn8JV85l1XYubSAd773P2MgehQ5irZb8EzkM4g3Zve1tRaxzmlke7gyTRxfBzy4sxq3ym5tEgAeBWTDmDxFNbqdWOnkmc7vfv6i8c/8WUHwTnuR/5d68LjRbSWpnbGsmTrOOGMu40om0bTMGCCxKQEu/tddeglOOelErF2zHps2bLLGNTORAyc8KumDV5/RtQgme3C67P4aTNw/ltp1RSbLCrDgb9y4bcCyj98rHaAa9bD15ptw5w1fEe2rbibYuOFknHfRRdJ65NgLtQ5i53VtZZwxQTyq6MEP/zEmA01jQmYdN3ba4a5ANs/NW+7BGy97g647N8tJoIPvI25qHKtv6IPNB9oYGZ7QXvHlvv8cnH9L8xzLO3dr8lybhOt5itWT5yIz83uIDia+plHJeO9809xfa7vnrh3DeozT9pSvCI0eZscY8yDPBoX4Cs2QBpINjIYL6pX+9m//Lh593InKnGMBmj2qgR8INOfhEFQViwuuI+6yLAEK5JzfqjNkVY1rrr0eH/yLP0PVmkar09SF5UXUWclBsp4xwAtIOYHJi+aDxRYWs4uBIeqVuZJzJkxE9ym5W4u1UOkQy2DzwezPX76X5LPS5D6zcr5z7prcgdl/U1azzGU9GgtKfs7JBeIDdvL5fkCUnSj20h53+mm46KUvVrCtW7MO7U5T+vkMKAWDo29NngPlCMrv71BG/5l9WWvvZw1hZUUBIxaM/uE3mJyZUCb2wHCph9lOG/feew/CrIer/uWfcOqZT8aFz/8JDBmwQUS9IsmnxxWHSInwWbZUZX4/iLe14lcePthsIXu1L5Z4Kb78nRvwi+/6RaNWORDDsqB9dh9Q1EthMBSONKzSzfkZ+LaAvxb6zrVVEcN+34Zz+yQyJ/e5HnofB56wetT6kMw6y1Qrw/mL19Y2GPtO1nJaKRuZJMZcTFHu3AxhRWUxSrsX0psJgwzL/R7CZhO9xV14+nlPwyUXvwbHbDxGn3cS5HnAYNvR211f95Xr8N3v3YRbb7sVo4yH50TnMf6/vzwSYjMKcgER7HFlFOrMc13A4WA4thOSCwubsRH7VMb5m5RjE3nYNarJw1PviVJvoQEjHACwArFUSSj4egL+n1ykPth8ME9mOf6bLjjBhtSyqwdzrWzMhSxObgT8Gb8B2E3ipsCbyEEPyvAYQ3/Dho244oq3YKo9hY0bNqLT5jmgIZ1/LZhJzoXrWVk/zen568boqtjXEUnQDvdS4yLZl4rGbHC7iW/1odyGSCsn/3MESpCWGIx6uPO27+PRJ5xAMhO++K1v4cILLsSGuSMwKFIESdP8uXXutPJLH8Ejp14izL00A9lnN50WHZPEAyNFXuieX/mZf8Mf/tc/0IT2npugNhdNQQQYpgOVrzNr59z99aintQZUBXAqnsHIMteVWvTvXppfELOk2+6uBJtALbuCDDhmGVU+USCNS0068TM1GlpDut7i1HqBplBrl69BxWdmbSvpzVdctD4yh4JIqmjcZItiiKjRxQDTSOIArcYIS8tb0Y4S/Nzb34bzz3qmK6EdnjAB2WsQma//hne9te52Ogoeyx4tZCIwsAy0WSS6Zo6Cns4gzGaWAQ3OzhyTQ5B8Zex8/29cqN6lxWc7v0P7NgAXcewIx/451AbxIMUk0KFSxJWVk4fsybJycoexM9gKV8+CkBmtVmB5NoovUX1m4/P4Pe39iLCxrg+slKxDvOln34Azf+wMgQJzszPMedYP4k77AMHGc6oFlpMJHweb1/R35x2eK5nhyNJ339dvMjRJpC8bGSLBKMNcq4Vd927GB37ul3Hq6SdjeuM07un3sbnXwxf/+Spc8nNvw4U/+ZNoz6xBq5mInMzxGJZHbuhgsjAY/16Vr9sLPMJrGct+FbT3zQr88d/8Jf72Lz8sxHAy2Px98pmNm5v86AiGJTFoFyzKlupXx0HlrN/Exsr1uGvzFmzbvAU/evrjxH7WfZJk+spZm0HDkFaJmBgSXqS1EkTcik2VTLyyWmNWHO/1xHJloCDU3J84miSwayNixZOrNRKFbYdYe8oGJScM9WQwU25j2NuFE45aj1/95XfjyI2PWtmd3ES7gDIG20//4jusjGSgsRYn68B9cQYbgyJio7AiAdjNjU2wHXywaXiS+KEbibczy0qw7XlXiWZqY6dCk/MY80E6GWzjv3MABIPAs0QmqToeACFsbIFqAePLOf/+vqTwZelkcPpF4l+LwgtpDkTNBG3XOGaZd8Ezn47n8cDc6mLd3BrxCR8o2CxirYT2wWZ5yvXU3GSA7b6evWEuNUw+fiMgn7RoBGjGIbp5hU5a4h//4L/j+5/8LE7YsAmpQAFoAxls3Ybl2TW46e553JUu47c+8hc47wUXYJiTlcIpaVYehhhqgXsPb580fwj5mOuTLA1m/nf/znvxpWuuV7B5BNiC8r69M64N8SKdNRanCSgA5KsJfkcGFwOWYFmVldh17zZs27kVJ55wAlqzM7o2FmyZBXZtOIDO4E76gdpKon0VtLwK0OAoFftRzJVMLoGVmn6TValLQE+App35x307bUpmlbwyiLiyUTJauPHxDJh0IgQcV5rfgf/3F96Fp5573pjQbSWlC7ZXvuOt9SgdCR20NGsAhtC0wiZm+eceaS/uw4je4xrR0spwiJplEkP97MMXushk+2undKx4vo53fuEFjzQBwJxuu5ZvWntE3HY8F5xkEThuHN/PH4J9xhufE9xn4d/7vps/K/jn+Pfy6KBvoGuoss7UVE1aHSxlA9HKmpyAzgu87FWvxPnnnINWI8DadY9C0uQ5i4x2liEr7oQeHPHSAD7g9fdS4LLJAFcXrhyiggqRhmMtGLQpyV63QjdsoLh3K/74538RswsZ1s3NoUklsMTmXgjWsLDd3i/xvTvvQNxq4wvf+wZe+95fxaVvfzuGzQrdiCDRfbc/nclcovHvKT1Jl81UIXhSCGosDge4/IorcMett1pW0RDoilT65Hf1pG6WZmN9kqrCEY86SvJ3UYOIJaccUhRpgbvvuAPD3gDHnvAj6rmRdsZMqGBQya3zxhgMY8+UWYZ0PYnLaibPHj6A/BqwXp8REfg6qnSczokPNI9sCgOQ/qaj3bmM6jdK6a4x0DtdbbaUJFw/M4X5HduRp0O85FnPxmsvfcN4vQYve/tbax84WtTunOHLL51fHD/Rq0URXfRwunYyaj+5sQqWDOMvqarJHR4pQ0DvMordcO7Mjc9wVxXhmB82S++zwIh6+p3SXzz6U3P3myQbTwadDC8mSln29rTTTrAX9gRwfHbjdxoPZIbAVLsrVWTq3JMGRMx07dQMnvnc5+LEY47GYx59PKamjzCQxBkH3l+wkXLlM5QFzg9vmbJHyUfhZ/JkwwusQYXNN92MP/5/3oWzjj8Fu2+6DUc0uyhqa4fwZaMokRArTTson75tfglHn306/v3Gb+L4C87Hpe95j8aLGJwefxEYw/t+n2BbmcZm2ajMoraDATn9PMMrLr4YywsLthgl2uoUlPdAGIWwylPO1olEgEIeUVK04gSt6Q5GWS4yOQOt3Wxhzdq1aDRtcoOgjrITYXq1LvheK5LtTE082oyDzYEvvqrx13vPI4ZfB751oLOsxG6ZeCzLsR/s/96va7WuHNrdYIZtkPNpOEcnYQuswO6d23BEEmDY7+E9v/4ePP70JyB4yVvfbOi/u0CeOc87QXiXb8RalhQnLhru4Hlhkti+FLOs4vh/xQpLYry90OSPUmk83fMgzNrdUZi4o/IC8iArEVU3KLrnWW1yH+b7LS0tiPjKktJnJJ+5/HP593lJFn+lxecRL9uZDGkw8GTlu5C72G5NaXhUWTFsoBeWkn+jA007jPDUZzwTZ595Gk456Xh0p9ZiqjttA5sBQSXHT5xo2E7KNqiZrcXg6FsTvTQfiP+ntvcAkySrzgX/8JGuKsu1menumR7LOLwfGAYvWFiB4K2EEUIChEAPIa0knhB6CJDwA4NHOAESCKFdPT0ZhCwIicUII2YGxjCue9qWr0ofkRkR+/3n3JsZlV09DG+1yTd0d2VWZmTce+455z//+Y9v+JJDgjw8jIYZgsLD5n98Gx/5rd/Elc1DaHT6cHy+l49Oe0tPT7Jd5L1N6OOOkLlVdN0CW26Bu9qncd6jrsZvvOO9aCw0pFYmnTQkIBC1JCWudBDQiwhIpHdI94j8O5dI56nPfKY0msoBx/yGRpdNWP87cU0Fm8r5Nb8vNzSNrtGcwQ9vvRWVMMa+gwckr+t3yf7Q8FEMg0ZvAJ7yIWyNwbKAbIQk719CMcddEWa/C6rL+l2sBW3uPTbcyuskQdfnbaQmWjDCNhoaHc0JEd1zVYvG4yGXjjByXGwduQcHDu1Db/0Y3vKGN8N52i++VN7VUqVYaJabymItyb0mVOPGtt6MQIgNFyco4MTY7Ga3N5anBI0tzDQGFi9oQkZtJ9NZ0TyalW2/E9QoG5q9+VxyNpWy1LB3795xrM0bZcEePu+FZAho0dOGiwJWMOImekUolwpcWSaGy8+mzglZI/pw0GdYkpHOlLFnGAcOno8X/dzz0KjHOHffeaKVoUZE+QH1Yjb0kJyTtLTxTTHz0QzqWuLpjl8S0Ivw93hKpkNUXR9HfnAzPvjiF+BxVz0Ezuk26sMRfLLafWDAiEF6xIxsBFtSePoLshdgKx+h42RYzgZI8xEOPO5R+NW3vgUhxYVYhmMvH0sBJWOTPjvhaepl2c52gixEnTe6HVz7xCdiodk0m9MAHhyxVTq8x8gnP4NyFeZQt/U2/ntmZgbfv+kmzDabmN+7JKktvQRzLz7P18rhLoz+SVSgyLLuV+5NW+sToVn7MG1DfI1NP6yhWtUwhp0WMON9lFotewJL5SF+D3vNdn2lM8PsK4+sKNdHKuQDBcvuvv0o5uYXMbfQRLXfgvOMV7y8EO9lWkosX4eJtMgQ0GsZShWVkxgy8vU29p7UphSJHEPGRvdDjIO0J0pSZ550OMtimBZ/GptIxxljk7D1RxibzQdpHO12W4AQnq78ty1JiFYkSczUtqAh7DA20wtHKbUkQcRBf1GkXlXoVqZz0ixYnx5kRA88lGmc6WCEd7/vOpw6fQ+uvPSBmJ9f0L40MugN79Mu6M68TNFMLfqYEscUC082BYESbnAxIBd3f+8GvP+XXoPLDs2i0e7jgFeDw1agmg8nZqgyQhSqujHvDZE0d1CgXtSRexG6ToF23sfaqI/eqIU7tldw9at/CS971avlvjE3GzFnM+GaeHwKBom2ypnGxibUo6vLePrTno59e/aYTaihooAhNtc0RHNzrsrXtgcwN7fIvVtWj+OiUa8j7Q+0B46FaZl2OmlB4qFs0dHJIW9KB6UktGxsBYtwu4A28jOLThuvLfuHRW6Ob6aXM/XD8eFpiuc2ZC9EhFYPcYa8tHEBZEiHI8Pm9BpGmYfFvYcwz7PvaS9/mXi2cdhmvo3oQ9AILMvBqPJyA1uWhtwIE4ejNGBdmPLMH+xCEW0ixzDJ4Zv3tyGFnJpT2ToR0Omf2YOKxFF7o2lkFgWjd+LvNJtNSbp5sqi3Hmlzokmo7XfloSG0nIhDD7WoLJSxwVBafNi+Yapf6PuF0MH4ffwCaG9t46Of+DhOrRzF5Zc+AEsLSyhEvEd718oADMPRHTmDcCJJrtXcVtU+tJA6Rh6LDKHvSQ1nZeU4fumax+NB5xzAPidFo7aISuaDvco+N4PtBSOFKYww12zId+2ub0nukFMQzo2QFg66yQCnu6fQznOsehl+5vW/g6e/8HmmlUXtihN7eBQRlOCGsTQu0eHUYh/agy7uPnEcv/TyV8ATQhs7CdTY7AYtkwXs9yrnPvZ5rpGsoSnpyL0yE1RtXs77xDTj3h6SmogmoXIzLYhhaX5n+92yFybRXFScPdUdtZ7MtnYxbOf+GwcphkQt35kdCpKXsk6nK9vfTLCxvoHG3BwW9szDeerLXjLO2bSwbFsWKEVtOG0UWWFhgF6H+u6sbYiXo06wdiEz1pWNzC2XUZ7NDAsU7QM1tpDjYg0zwuZ40zdBw1bND2zVf5Jr7Xy1RSD5U4aMvH56qhpZ44ZFIKeO6CjqBE3RuxBj4uAJejztTuAGVaMQwF0XjRs1G2FAGJk69wUQFuxwGOF9H/oglteOYXFuP84/dL7xWCxIT/rZ5H7kKtIzWSGFj62xCc/P1rQM2ssG74rn4cixo/j5J12Lqy+4HHvCGhq9NmpxVeqSsePDd7VFxiPyyoDXD7Ew3xR5ulGni5zePSCsw00EmePdGrXRLnLc1d3ACSfHu//n/8BlV12hnEvu80y1LKeNTdM1dtOP0El6uOGWm/G6174OBaUaWBYxdTALMGjuvfNRNjauk7BKjO5IYCQQJO+ibgzHDBvUWUV3Td42Bb5Yr6XsIRoIkUYpU5fWU9fWhrDlqypfk/ycoTeLaGPkUZ2B5nEEUEzJphQqc4+N6WSy/kQkHPgjF3fcegsOnH8AQS2E8+SX/PyYE6Qho1JaMtJ9So8KjY1GI+KfntBo2KmciGLTBGBg0TBk7cLca35BAizSCSDuXOHfHRuw9Dk2Rxx/QYPe2XzLvtSGsvbftlzAG9VpJ5idbchNsp6Pr2fISQIy809tLtVu8d0fPCGHGJJQ7XOGmaOF5v4IcRzg7e94B6Wh0MsyXHnJA8WLEqwgSCFrtgO2Ly20DSNNHieyc7LGdp41UHVdrJ44jtf88ksxPHEKD5zbi8HqFi7ct0cIv16aIXQ8kSunynIk0n/Muz1UJO+kduYGskFXQtssZ35aRdZPsO2McHJ1BduhgyN5G1f85DPw2298M7xatEPXkvVW+6B3kxySaz4codXr48vf+hre8/a3K5JstF1sjiNrXjI2Gypaw1CQQSMAC83zT+vJNJXI4NcqUgfL+eWYU/qelAesgdgIiygn30vogwYZZchpQTfyKUnbY0FdHxpdWJkKa1jl67SeliQAoSmykM1rKUVhZY/NMFLRywm7eJT7OHXn7YjDHPN7FuE86RdeLJ5tHMYIrE82yNmNTYi1dFjDIQYyYXOiFTJtbPwiNDZSdyigqnqPZ/Y62YXdzdj43DRDgT8rhyqM/8mf43WHQQ3bWySvBqhUqwKAcGEYYvJ30qFq5lujKP/JWpl8nqn+D7McAx+SR7GmwimdZHj85mteg6X5Glr9Pi46fAVm6nXhSHJGtt0Mu52kNmcTTUfz0LxOOxj4/YMsx2+9/KUYrK/B63Vw0IlR7ec4sG9Bwjh/mCNkfadwEDqkiqm6MTfV4ty8hIXd7mlkaUdY73mfkVlVwuDlPMFWb4ATgw1sL86gtziLl73mNXjE1Vcb1spE9JXXYqUf7OEoHc7tDv7mK/+ED19/PWI/wNAYiv2+u33v8vrZ0K38Ovnepp9RohEThfhRLJ6OhQe22wh6bYALWz4i2EZDE+VraSxV0I3Rl+TqJC8zx7WaMCVjsweA9Zz2GihHYR8U+tV01tSDp05n3puAWjAyLUh5mTxCt+Fi0FrHaPUY9u09AOeJP/9zkhfLTGfWwozhEEGzpzNPtCqTdvscYft0KK5eZpSZD7esEELXeXcg4ITkddJqY8CW0iazX7S8EFrrUrC5DNVyGMOOzWk6tpkrsX2HZwonZKZZKrBse3vbIIq58CBZ3OW0TyG+Gg1JoncUS3UNO6TscZkP8RqSYYFhSM9GNNiRmWJ8vPD5L8CVF52H3ItRqddxaP952mFt6DAKDJXCR+nUVrqWPVXlbLVjoKRFxJGD4FMf/yh6yyexcvI4WrfdjgtqTezzqmjKpM8C4bBA7IYMIAX8caMYw/4A3Euk3lHnP8naGGacxAORrqtiBkFe4EToY63fwrFhB8GlhzGYrcLZO4/X/Lc3YW/DR5rzPmox2xKnrVFwk/dHKXrdAf7sC3+JP/7YR4Uxz0hoZy4zOUjsHrJrWQ4H7d9tbdMaCz/PejzxVKwBmvTDDSPdewUHpaicoe3QsNxWS3K2oaX1gCprqF3eQjm0JA1LMKDPo3K0kmJ30MnGm28KlbQHNTVihHcqAyOJhphIYDTCHTfehIOHD8N5ykt/QUBeIZfySxhiLBEn+aKE6oscVZmKpDdBkDIWIQmiGISJH8oiNPeXnwEh2+ZHejrwXeSULIEo5cOhjCxpOKgUl52noRYcbQhSllQgQ4VsD6JAQ4HeVTRVYmnmBeR9Gk8lBVrbU0TOI2HeUj+a3Sb0HNPGxlCeHo4p2KMf+Wg8/9nPEMmEwXCEyy+7Ur7pyGEYqax6Pd/MwxibDv/TmW1yJ6UHjEMLHSSjFH/9D19AyJw1H+Lfvvi3cNY2sLeXYaYA9kVVuZcBgfjeUObEMayqN2cx6PTgxxXM1OqSoybooNtZR5jFmK02RCeG89S2Ch9H8w429tSQzc9ipddF4+A5eOZP/TSe8MhHSjhKJJDfcVSC6u3XGDocGjnEH37uM/jTT39KDg7mK9Lq8iOK9XZjqhFMxGzLkY71WuKhTBpgEUxJJTxtDJZqjHmf6Xqefb1+npYFlNrHw485KcnypbWZcgA7D/Wdboz7icNEpD44mJR0CLiFHPVDqQkDNigyPMKd3/8Bzjn3gOZschJSM1H0PFSywBaKeVPYN1STCr7JzbhZRjpVZkRPYaBVQRGNsXGIhOqMOFIf4aMc9k15YnX/BpGaNjb9uXqJSS4kvexS6WcYw6kv6qnokQOdUlMQvlWUjOEtPY+IyJgCPMNhMhiIpNr3tRvGHCk7PBvrUFU/FBBmpt7AG177G/DiWLohLjhwgeRTtmog71diH7B8Iv1jMpJXETO2yzj+CAEd3gj4yj//i3Znd3pIPAdr7RXc9s1vYuaeDcz3CuwP6gJJSV7oUx04FVZLtbqgno2ujWFm4UqRusgC1LIC9aiGNE/h1ivYWk9xZy1H/rCLcHpjG04Q4fBllyBoNvG8n3we6vWKDMdgnYtsEXtAWu9GSTz2cnzg4x/BX37+86Yxk4jipI5ZXtsy2meBromR6JqXjc3WwsqhJv9u8zk5s6hqzU5+U+eye2O3PWWBMc0RTVuVdJJP5BXY2GkfZxKwd+b09IjWCZSdhIaYLKJrCKwHrtZ8j/3wdpnE6jzhxS8WdYY0Yf9TLl3CkRsic0zhmns8IxnZauznoulHLzhMEh2UIOGQ6XgmHGqQPX4JCRWEiLszTytD3bshV9MLdoZyAN20dfXGkBkmCJIkO17MF4NsiJh1spyE1RSphKiG8CuDAQlqnAmSaI92LqTsnL9v2BMuCdSiYQ+88+1vQq3aNMrOOQ7uOwyvSu3EqfqQBUuMFIIYovGcjl+IfNzXv/H/IOn0xGiqURUJgYCgwOmjd6F38x1Ibz+OQ34NTp7AId2toGoUKxwFmo1FKe5Xgwp6w0RyMyoIk0hbGfqIw0gmwqAe4NabT2Dz4CKiR1+OzU4X80v7Mb//XGwOW7j20U/AA664Ug8nGWqvdTPtP9Cex3Y/kc3EMcnf/upXhaHDOIKHeRmqL3sHu3Z8vzJh2R6w5UO43NSrm1YN0nJgLWkhnpmR9GTU5x60nRP6qeUGUaq6CXhVEuyV/Sr0NhvHKDNED3UFW8re0u5VG02RlSINpqYWp50wBaKqyiiKR3YdqS3HcQV33XqLloMe94IXCbVTvFYxkoWKCXpPGYhFiOQi2QxJFEpQRqMEzI1JgzUPsXrDJiiHDGPPUUqIf5Sx8S2tHLgdyaRMam4IbQa1j1GWIPDZ5JqJAjCRsmjK2MacQDJI2Ph5H41NWlRGOapxhM3NdfzBh65HvTYn3rLd6uDi8+8nxia5tDmEyqGVRR7lkCB5lzIR2RB33H071jZWZaoLI4Z6tWFykRGybgurt9+Bk9/9PvZT96WXIBqMUOFAR0L/nNBTeGh3OjI3rsI6m4YAyDnQYmTUh6ucAT7C8tYQzWc8ASvVXLQkz7/kcmywvNFvY2nxHDz3Wc9Wil5qDE2aVfX+stC91aHmTI7Xvul3ccv3vif3uFrh9WppRT96Z0jJOqY5kctn6BndAfZJC5CU8/yJMZgpq4Z+J6+htmYphC3/nYYgXEzyKkt5vzHL8aGs72+HtVj+5eRyNYc0X8N0douimFFXkwZWo+XC92LOT8/Lg/3kkbtlFoXzmOf9NFNsqS+I6lVWiPqS/aL2wnlTyZi2kmpnIE7jHTzxgK5BHvm7Wn5iqKEXzJPKSs6dzdjKhmm9GGtIgp4a8RgtWvLmsDCtc9K48HIem1loIaeQMhwmKkoY0ipOYAAAIABJREFUgnQotolQgSkMZS60PUHt9xItxSRBNY6xLeGaI4VtjsMlGNDd2MRHPvlBNCpNyVnavR7mq7OIZuYQxiTFMi+YaP/bZRt7Y9bX8gwbGyu48cYb5V7wWsjhpC65HG55gjjLsLZ8Ept33Y3srmOYYfvTsXXUCt4JToKsI+7m2E56mJ1bAFlmRerI4MdRN0XEw4g5WC1At7+JW9MBHvrSXxAWSRBUcP7hS3F62EHa6mJtcwPP/+kXiKYIZfh4WmfSu6U6K4OhAl2nN9bw3377NVg/vSygmhymMtrqLMa2C/psQRCLQpbvj6VMlSmBFjG3e4d/aruUchqtkcs6WuOWjvYJ71E97+6lHmvMNvUpv846Gu5d6/Xo5SipIO1epqanwIohTAfaNc5hLdvrq9ja2oJzzfOeb2mr2iHN09sAGTbREzcubH6m/B4G/QksasMCQvt6etnmUcoA6IlDHUEqK4l0jgHofixvJsVylUig0KcULM2N1Hjd1mz0NE4I2xoIn58vYi/MQ2n8htkitUJDPdPLNvqV5vcCRzvJuTQsagt/c5ih4rOrIAOSFG+//s1o1he0kJrRU/Rw6MJLOPhTjI2J9DT30W4aeqTW1ga+/pWvYGFhXlpJmG8xB2bu63KWwWiAcDREt9/G6vHjiFttnPrqt3BeGqHCWij6GMY+mk4VvXyI5sys5DNJryvrlPVSBI6DMI4wqDrobG7i+OIsDjzpWhxrbeDwwQtRbS5i28/RXl4T73j5FffHIx7+cPGaUio3U3L6xQidbdXi/+GRO/HqX/9VjDg7La5IM2lEhn6JelZGH8sH807wwqQgJYDC8iDFOxjhJssUsvvR/rv8c3uI8feUSqg1XYZ7AsWbTgCuFeutRKinvaFEQ4ZGKClJyVsq6qkIvfzchMSkdfG9PVe5tPRwvE4qdZNdxXJFa3tDJ/k89vk/Y3g4hl2djQTtsroKvFAJxxg+ilQAx9ROIG2SOG1BWeJS0RnRup09mSwSKdp94okmXQI74grzD30d33cCIedE+WSklJ5MlNyWviYhy6rbJlTNzSoeePxepoZokEdZeImrdw6osMiXJdyykCodvaOhtP/IjWRfFXO4JEUlCvHWd70FzfqsesXMQ6vVwvkXXoAoJAWMYZWGL1x0gftZiOZ7pqnE+1/60j/inEYNtXpNCu2cfzASGQZqp7AXkEath9zmygrYk9y9/S5Ub74d0XYHSZogb8xidlRBPxmgHleRDYaoBDG8gESqATi4dCby0c4yrBYFFl/0M1hp9dDud3HowguRVSK0WLjfWENvu4OF5jye/rRnIfcKGdtbYQczyQv8r92XgPLr//FtvP/6d6PV2kRBmcLAxdB1MFtw2OGZNVRrJNZDKDK4U2PFrpfNzfi8rXlNv5ZrRS/F2irf276/7fqQkM0YhR62SvGzHNqJF7TiP3rNen3K+PE8LfHYB2l/LDFN3tdIeBhDJgm5/LBkeCqLtbY2VRns0T/zf4yrQYIS5RliA1nbL8lfkDYYUp1SHfU6DreM9Ss3TUMjPmeVtzRWVi/BfU6ERr/sJDmdNjj7hS3KJK3vpidsgjxZShen3hgghggfP4RkUOPNxqeTtLorBBxyHC15n6UFsSejlD5EGkTDgR2hLD27nEPaxPh7b34jFhqz49NTuo0L4OCBC0BkmYGeFFhZA5R2lknp4js33CiKvItxgGqjDuptWBVnFtLZcWEHhjB03d7YEJk1d30Ty1/4W+xpjzAcpEirM6gPtQ1qptZEEEeoBbGgsyzs524XrjcC3Bgn3BEu+rVX4sjRY8gDD4cvvkTG7G51uugPtrG9vCEH1tOe+pNY2LeIdtKX3jfWInv9RJ5jeeJDH/sovv2Nr2O7tYWchyrR1cBD8z/B2MpGYe//uHht8kELmFgmkByEJj8Zs0rMvpRUwgxX5PtZuUTNL3eGlDuNbTKbju9/hmcz4ap1LIy4rE2UCRPsp2xtbuheu/a5/0UAEplnJShUhgH/LoMwVOeRaKRLviPnq1Dq0TSI8iKYr9gOANmYluJtpoFaI7QGVd68/JkyuSYeTE+YoWgN8gvwRjEU6PUGwlVUUITh2eRGEWWUIgA9L41kwM7lnVC0PSm1TYhSfHoS6SKV2Bxjp72zwVPjcfM6ji3KHfza//kruOTCi8xUTaUYpUkH+/ddIGGV5MHyNvSuGsPz82+77TYhqM7X66hVVP9CoHYOluj3RfdFBz+OkKeE+HUGQUp4fXMD3g03Aj88Ba+TIOk5yBsQ5apmc0nEaQLXQ+zE6K13gGqKSsh6TIzvrBzDpb/+CiwvLyOcrePCSy4V/Y3WyiZ6YYrW6XWsLK/gmsc+GZdccakgmkRzeS2sofKAvPPI3fjuDd/DJz/2UXQ7XUQNUpliFKGDOScaRxvlA9SGcXbTSmphqFpcXxsB2fVQMELX10YC5fzNhpp8juwghuD8maVrSfG931c2vglFLYopJIsp6cNySmm7zS3wYVH06X2rq2oaTPkdjNpAGXXn31lrZgMpox7n8c95jtC17EWpsbEFpjQMggTPnio+7fRKijxZeF9OIqVFTKTGTe1s2ntN3PaZZQElAFOJmIIrpGCl2inM9g0BQ0oFSTFOZl+62YUTR/lzM7Defq4Na603swYuN3+3pjLzi5KHmIGF9poF6MlH+LVXvARXPODBY5a/hNNDAj81LCwsSegt+hycFy3llQwnTh7DyeXjqEd1MYqA7JQwRlypCKVtlA6w3e6K5LDIpPd1XkE+TNDiPRl0ER05hrVvfg/x+ghe6iGp9TEkQZqjifMh0rVtzNRnkWz2kfVXUaQDIJ5B5X6X4eAvvxB3r69g8aLzMLdnj3Z0t7alZLC1sYnOZgd7zz2Aa665VkLJAUWcqE1OuYwR8M3/+A42NzfxkQ+8H8NhKuEl6WIEpWcoDWHvm/EsZU/De2+77LlnrEGUDcCujw3rba5Vzp/4EdawFK7XkLN8oFojLRtbucSgaYr2rJUf1lissVkOp32Nfd7azMSbWYKybZ7m+6vI08baqtoNjc2eNHJ6sPeJs9CMRiBfxHAoSpWIqxuuPKRdf2anvyglSVW5iNrZk2va2CYngJ729n3HJ4jou+Uy7I7GpotAqpXWQOyiaAE+FWRIcsNUIehyk6G8p5Vn0E/SnNDM6N7tIJj+mYQYJvQkuEH44LnPeAoec+0T0Wgo3C7hDT1QOsLBg+chIimYXo0N6ukQGxtrOHbsGBq1OiJuUHq9wMdsfRa1ak1ysN6gh831dRG9kQIqx9BIi8kA/eEI3ZU1DG+6AelNtyNfT9A7sQ1/rm/KFw5qYQ3N2Xnx7OecswfRXAOzbiDTW1ojF+7D7o/jkYf44nNR3bsk5QIkiczE3l5dR2uzhdz18ZhrH489exbRHyVIh7lMj1ld2cCp9VXcc889eO917xSlrhHvLTU9AqDKRn4T6lkjs+G5zZt4ENo0Y2FhQQ5GGt1uD7tHtKl3sudsWMnPoPHyYWt8NmS0FDB7HTSeaXK05VOWc8ryIWzJHfZ5u8b20NX0RhFauT6j0s3PJCagcy1yHDt6RPaIc+1zni3WYt+YrRUpHYeBdO0HuHSTjOIMXWvMNzMs/jIr4N42r/WEZWPTGJdMkBECSTSVKmWHbciXMaV9jyAJXyPxGdWItVmPxWq2+VjmS1mCTQxTep3sw+ROw7N3H+xmbKSoyQkVBQiLHE989EPwjGf/NOrNOamZ8WF1Uwjl12uzUujkAvY6mzh6z11SYK6EkRxmcs/CCuYI2fuklWVI0g66vR6STlf1Pni/B0P0khSzYQ1RnuEzb/odnA8fe5qHUfXqiALmUyNUQg7J8NW7FwOENdYccywOQ8x6s2gFIW7NMzQe80hElx+Cv2ePRABksGTJAJsry9ja7GBjYwOXXHk5Lrv8CnixgkQ8GY7dcwrdpI277z6K1//2azHHLm0WctkV4HpqbFYZujQsYxz9mBzZGpy0RaHAhRdeiONH75ENafMx7sd2p4U4YmNvbOhWerBrKqAHpqpy7azrlb2aNYx725NCAGcnfkh5DsMmkfXk/mKv2sT7lcPE6feUfcapQpRFyJjakGvjYeXYPYiJflpjs0gRY09Kptl/W88kWvo0OBlyYNSMaRaGwnU2dGm3C7LvrTfCzmYj0Eytdlu9V2OzyFP5dCmDJGxfJws/oUybkTeTmL5kWxaRmr4W61GnQxR93Tggkn/JgWDmb5M9TobAFRedj5e9/JVozjYlx7ULK8bV6+HAgfMlVOwkXRy/5x6p5zH/lHtq8oa5uaUxw4HgE4vyvbSLXq+Lgl3LfZ2outnv4ZzZOSS3HcXNH/4Eoll2pXOSaxVNf0ZqXOSHcmg8W4CQkSkPRMMCtbiCuCgwOzeHE6MekgffD/61j0S6sCQCRo00RztJsNbextbaMlrtTUmmr374oxEtLYnw0NrWNvLBAJ2NNemze+1rX4t5eiYWCAgCBSFmKek3NYZrt0PLrgfvAw2bj06/h6uuugonTpyQdhjlSKpOpVKf1KBseDkZIcaOEFPbIvgVhhJiWnCl7ATKe6h8XbsZGz/LfpfyiOBpfMG+jw1JWeZKE7KvhhgWI/Q7fZw8ehT7ztlPY/spKVrJaWOENIuAsD3VjzWmJQxOGpdA0eSp+ToEQzfpRHJNrb4MdkyMlhclpQFTB7HQu/1TNirZ2CoQLNV8nnyclSz6J1J3Ug9o9IGlYs82dmqsiQsXmpgZm3RvR1mJ5cCbZ0MdWVaTa3DWmAA/JRlw0eoVBo0jUC7SFO+6/r1oNsi0J8ikcLC08SQpFhcW0ajM4NjJo6ZsoeOJhGng+yIUFBObN7w6hshctH7aQT/tIecEzv5Q1ma728Gc7+Nf/+CT2LfeQxE46Oc8+atYTF34VHcyhWV6zaQ/FOOLHRc1Mks8dnE30Wl4OL5Qx9KzfwLJ4iIazTnsjWJpPD29sow77rxDdFQ4Nfb+V9wfhy6+DINshLuO34P56gxOHb8LR4/cgzf+/hsRV2fgxb4YG79Pw2XReCfYtXMZJnU1ITrTG8exKJitnF4WWQTWv6r1OiivyIKw7bymwrWmDtqGxP1pH7bYLAZiSgq2vGDFdvncZM76zrID75sIKvskI6hnk6m4YwzOvl47X842KlGHxqjMHqmKo3woas6ba+tY3LsHzsN+8hlyeJDZr9B9AapZqLISNw7zJBWZtARefW6i2W9d/24eYvokkEKxIFET4vFkQYTSLKGFKB2VmgsJmvD6fNj6h8LqrEuRRnW2E+dsNmevtfx75eu3ibFdwPL7yHW4Dlqra/jgRz+Kxeas9HXTw9rvxdf30kQkGditHfkR9UVlMDynu1AkSIjDrKmNOOiD3e8a/6fDBIOkhVFvIK1K/G+71cF80sdNn/kLVLsJhpGD7qiDyI2xn/PxKAPBPJAF5rgKKssl/QyVtJDBf/UgQDWuoeWlWKn5uOBlL8Lg3P0ImvNYmmGhPhLGyw9uuw2rK6eQUlYQDi643/0F6e1wOLznYP3UMdx55z24/v3vgRfE8KNARV59FxG5SLvYGu+hao5MBrFQgsCijmzqpGc+TZQ0COSQZUjJUHx7e3vMFOGaTKhXyvCZBrws8GF/bte0nL5w/ezBb9fVrrMF0oSdQo9J5TFB341xj3m3UzvLdBcwMpR9K4LGOVpb29ja3MSe/fuMsYnsgyJ8vFke2d7JUCIjaYajLIL0AZnin5Gr40aV6SGGKGqZ1ZPLUIPcGQLqyW+b7HYaCXuAUpaVxsZjwwFh5+sVmj/V2EiEVcHZ3U/U3YzFhnsWBSvfcOvdBt2eio+KhuDkvccIFruDhzne9q53Ys/8nDSbcgqTDYttuEMDGGQJOFidsLwfRAgrVamDcXPb3IMHGwv2CrKwubWPbqcj8H7W7mJ7s4NbPv9ZLLXosUJ0MUIr6aDuNrDIwYaRiyDTqZosa4SBhywB/DRDPaqg7nmIq3XUKyGG83W0HnIFRve/FNVDF2B+vgo/qMkx1+518b2bvovI97GxvoKwuoDqbAOVRh3OsIvN06dx8y234OOf+JwoDjMEowYlW61YnxXl4+l9aArL/K40EA0R9XUkS4vKleuIgfH540eOSqMvZRG4V6x8gqxxaWa3LSmUwzwbQdk1nj5M7e/c2yFs94xI14gjmziaCcn9TGMTNJq4BlLBH/jaztY2NtbWcfDweWps/LUy2lPzQ2SDieyYGJH0PRron+GCSEpPBszzPawwir0M69LLSeUkljbJLpkW5AlqNUpOEFub4gbUUDJWhSwD+dPLsfBbDv/sTbUHAGk3PMGS3mDcKr+bF7MnnjUyueaRhs/0sLs9VAgH6G+18ZZ3vhX7l/ZIqCucUpkuqlEBjVm4jhR5JZ2HuoI81ChtHXJ8kUpNlI1dajP0AGkPnW4Hw1Yb7nYPWG7h9i/+DWpJB0XXQYtd8oMRliqziIO2KDe7cgiyN067himL7nQHmI2riBwO/nPRZBd+s4pb9i5h/llPQu3whWjsawpwQwobv/8td92B9vaaMl+8CubmlzDMexgNE2yePIk77rgdf/jZv4QfFOgzAgnp3Rwp+LPfb/rBe8m9wXyV3Qll4yn3LPLvPOD6w0QUui0iPjYqx5mMjkYuYk2au01oVLx/Zck6uyfHmIQhX9hG1elrld9lxETZPGPc9hol5Sixmqxhy/cbTWYJ8vVEldkVs7a8gjzNsXRg78TY7EnAMCigFqZRDrIghTSAWioOya3G2OxmPVuvGjfc7jxI1iG4NUeqOmuMLQzZjJkKR00LlRpOMswoGxu/s63U22u3f9raihwiqWmCNQJA9uZyUWw4Ua7431djk6bE7gCv/o1fxf0vvwKVSiyoXtnYeG/ozTg83XGisbGxRcmlShj72TKDpBm2ing2GlvSRafbRtpqY3BqFa0b7kDvju+jng7gZzHWt1vodxI0ihi1Jn0vCcypiP+wsVIRtADhMEcjqohmiYscs2wPmWlg60ATo2seCtzvElT378PszKx4M37/rX4Xt9x2i6zIwsyidB+nWQfddgfry6fwgx/civ/xhb9BGIRSrpAIgQV3k37wHk8rU9tD1t5/Qvbq4RT0sBuaa8GDi+9HBe3yetnUhe8loZ2p5/IQKT/s63YHvvSVZSOfNjh7KJdDUHmN6fK3r7f7WtIvliJNzih5d6+Hfq8rOijdrS4OXmw8m70R4im4RMMJQ4Ox55g+ZVw/5c6sN7SnVtnLlC/eAiLTX0BmCRhy5yTM5PieCaonw+AzB0kvRaUWjwmg9AUERiRE5f+MAyp7Iptz2VLANBJlSxd8nSUqqyc2V0+JCNHO12klcqqZ006FY1Th6tGPegSe+9znYGamLgRuWSDKRUg8wQkpPK3ZW8ZBEi68gFNcNO/cUQskwCO6g5nQogbdbTG4ZHMbw1Nr+NrH/ggXN2pwB1QRI53Lw/rqKqqcEec7iCuRjq8a9OAxl5GNOAe310Hse4hm63A7fYTVGhrVJtpzGYYPvwzZQx6IaOEgFg7tEzCFRUHe1yOnT6HT6WFhdgZpWqA72Ea33cbq6iq++fWv44tf+ZKQnO2GL8Pvk7xJUxOVmNuZGtj1sdGPPUgV2ChpT+ou1+ZmAdE4skx5owzFSRiehv5tJHVvefx0zmb37BhNtTPXTNhqr1/2kVWSo9iVnW1hxlCLLblAOhig2+9i6/Qm2HVyznkHTc5mTlVuzIiFujaT4x1nxY5/2UF9ljRqP7Ds3WzJwG52voYJr50sY41t56nCVg5KM+hPSa7PBSWddNZyU4lYrBmJy9b9sTDs1Kja6RNrt3+X87bpxSnne7IpLFJFGlDgoRFWcOGFh/HSl70U9boJIxkasHFQkFuihJQNUGOTiScxxyWZk7g0PYZfmdEDhT85rIRTRXvdLYw2toGT6/jWJz6Dc+oRQpPn9dsZutvbcMKqjFTm0PgK1XydDCAwI60eM6gUGRr0FOxi32ohbs4i7xXAuTUUj7oM3tWPgNc8B7UDC6hTXMeE6oPREC0SevMMnXaKdm8TWxsbAlj86Wf/BLceuxuR8FzJzqey1OTuWnCCt0ILvJOUhM/Z+qmNmuyBzedk3xAjcHRCrJia8Rh8HVlFlD6nZCDDSNY1GQ1xfeyBWkYgx0Zk9rhHhS6K2JLAbAbEnK0kcG/7Rw5f08ojgEgpvAxCH62tLbS7bWye3sDM3CxmF+Z2hpGS/DNu77L7did7W+LmMUF5IvlsjYqnSXmzlo3NhhTWy+mXU8+2kwyqGiITY1N9d24U3mih1jB842f9JxnbdKixw+Bk3pqOHCZaa41NFsp3sX+OIZaD17/+v6NajUXrUb6TMTYSkPlNA7bKe1XZlEHESaXmNCnp1ouIAnmd7FxIR2Js3e4mRqtbWP7mjRh++yaEWQ+eS54iWRdAwsmcZKwOEzRnZzjaAxE5oeLZGNfMwEvJ3Pd0TNRWSzycNwrhnTuL5UMNNJ/+ZFQPXgxvoYF6owq2FvHBMG1Q5KJQTGPbbK3KaU3P9omPfAxH108ZY9P8cNq72DXm/bRhns2b+KdFm63HoEcZewnxHppjCphmusCF5U/cV8ozmZSDrP5Jmal0ZhQ1aaGiIVgnUe5suS8H8/RrrBdUuH9SWOe963U7GPT7WD+9jqX9e1Brzk4829hLUV+P1fldsAHmP3zYGsaEZqUIY/mEULKogia8afy3ys1NtPvKOZf9IqReEdqzDXlSlCwcWfRKvYYhx1tJE6BuWA1F9WCwlKByIfO+3ESGkfYEVUGgUiXFaN7b50WZN3PgVnygx2vx8MEPflDqRZHUEVVPsxxWexy4QeVfL5ZrZ/tL4dlxu4Z6ZhSI5fskNKgBeu1VpCtb+PJ1H8KhUYiFiIPj+1JA7rZJEGYfH6dkMjvj7Gcdxsh2RpfTY91YgBGP+S9zcHZuVzibuoqwGmDrwgU0n/t0+HsPIVtootasIKZwkgn5yItkvVCMbWsFm5sbMgn09373DUCD2KMemhMofbJppr1FGYiy62X/FCMzw00kvBORU/JguYd0X5FFwtcNhon2p0kUowMk+Z+GhfdW49N9yz1YVvGy63pf9sn0a+x3sgfHOBIiyWLAOmEfKydXMb9nDmElnhibRXxISWKT5G7XbSlG9kOngYVpwGIMaJgYXJSNxvzKs90Y3rwhEuOiyeQXUJD0Md8TWW6alo7hPVPXxOYAP87NsxLb5ZzNetfyfZCfUXGKHSsVH0W3j2GW4hOf+ASqlYqEcarZoTJm1mv6biCllTCsidHRK9LY+BAvyf+kkq9FUaJXaTJAv7OK9MQGvvyGd+PS+hIqfoah0xVj67WNChmnrhCIKRKdjko1vZzD1yMEvqKQAdXDWBJIhwirEdw8RhB66F+6T4xtOL8X2Z55VOdqItUnIVKeY8DWpSzD1mYPG5vLOHXqJFgS+Y1feTX2XHAeeIjwULTfobzZy8ZWXidB6nq98fQhu9n5cws42Cgp4jisEYVXtRetzMSn52YvJUNCmfgqRrp7F3Z5L1h8QlKmWMeLWbKCHDJMuU1edm85nz2QeK32cJfXy4bJZfw1I4HlEyvYe+4eqa06D372MwrqPCpQwHiWTPLdjU0u1GwIfpitodmL0nxMk2IVWZlw2SYGeu+nj75OeZIyM4BABcMaTnNh7kEIReTJJ59v39ueNPeGQv04RrjbaxlKCrzM3LafIO118eFPfRRz9VnEous4GZ5uC6Ey5cv3UavOyrgohpHWs/EzBGm1w+EZsg4LMbZeex3Z3cdxw8f/DIs5Wf0tOFEKx68g6bCWM5SWqACURM8QM+dxXMSk1vGnbgy/oKdqIeDwEMocEAQbqFZmZ7GK5vOfifR+FyOeW0JzqQk/DkS3nocbOwHI7+tvt3F6fRXHTx5F2uvjvdddh9StSN7EIvRu9/tsxmaLxuXow+Zbk5xfjYYRkd1b8hylLxzN6QKHsxi4NziXvRD1KnaK3NeHrqHmeuXpOlFsUG/Tzzj9fiQo2LiHxs1rkdJDaVunHCTTG6DfT7C5vomlvUtap2SdTRyy6WSVvKifGurTTn10a2z2pkiOZ5A6XpRW9G1zqCEKm07WH9fYVHqOw/cUIeQJx9wvkRqYFn+nF/lsBez7ugD39XXi4TkcsYAgfa/576/FwX37MMNZcWaQhm4SoxnPbDcIEPgVAUgkZ2P6SfoRBziQGiRFbU6vpFARJObvb67i+3/+BeDmo9jnzwC9TWTuQDYVOZDkUeYO+/4q0pJToQeVGeXUuyJZmoAHQYeunP4hCbsZdWZcRAjQ2zOL8NlPQveS81Bd3Ie5pTlEterY2CjImlFro9PDqbUV3Hn3D7G5uoa3v/lNqC+co5/lB2eoak2iCzWaMnhyNmPbGZGcaWxJOhBAhDIElBwIPcoQqLHx1ZyQ++MYG/cuUUJZJxPGiNFLeE+oqjzoxEQpRgVgR/rDnxnNG64n9wYlHnkQs/Ol2+7i3IPnKqjzsCc/TRBoIn528yotSzc5k3oOrpeLYl4hFC5FiWhoqpmvM6w191ETlwsiAzzXRFcfvLDJ0ApuiTMfRsmJ6Nk4THQw6LQRiFSdKy7ZIln/f3mxs4aRpovbZag18hBXHPzyf30VrrrfJSKVQO3K8mazYQo9WhhUpQvA9ZmTcvILD2vlWcrgdN5itmVkOgRjuLGOP3/DW3GBV8e+IkTaaaHwBqDyNMMnoapxthi7CNICoYSULvxhgdBl3Y8WzRSYUoSpUMUo+zDL4Y25i1GtiuJJj0L7iotQOWcP5haWUJtpyD4gWMP8dCjoZ4aTx0/gh0d/iK2VNXzyIx/G0KkTOxUkUHqNpwIW4ZWaUgrFh2z4NikLaD+ZfJah5dlNbMc8CTorhxY9siKICQ94N7ATEuT32S/oUAb/zCnDP/r8pJaIF4ij4PvbvH/HLwp3WOcI0DbMOHg7L1F3tlEQF6eYhCq6AAAgAElEQVRDbzdIsbmyJZ5vfu8CgmoM5+FPeqrcpp0QeFk1WMVZx6FaydisUWmMv9PYpHIvoqQTUcv7YmyMzyVh5olqoHZ+DoERnvp+EGooZljgPyqu/tF3+95fIV7fbKTxbDDFxJBwBlrdxwte+LO45lGPQBT4COz1y0Yz+pQyHM9FxBYZY2xg0ZkE5IzegU2yZChTPyUVY+turWG0to4v/Pe34GAwizkyQljXAetsvDeJMIe8yNfZcP0RyPwZJUPhKNLY3BE951CoW/SAIcMy5Ngzu4DttS2ktYoUtv1HPxTxoX2oNWZRnWkoYdd0UAwcrSWtLq/glrtuwbE778aff+6zGORV2eDC+/GoDVOOaJRkzM1p/5uUfCYz0+2es2GkTQPshB9HODlljUcqWmVCsNZD1n6+7rFpdsd9Wns2RrvKcLFF9unf0ymx+lPqUAp8x/UtvZCpgn0w/KYG6NqJVampLu5fgkuFt0c+8ckk5YjHsA/r2fQTqNWvgq1SzDUqufywcovKbp5NL0hH5doYfsyLFFmbMz0b8zxJkKUwqGRYOQFdDvIw0yBZu4pC0d34cYyt7AXLv2frO5bVoK55AsBYzRXeXG7EPilgIT12Dch6uOZx1+IlL3qhgDd04uXcw3o2AiRBybPR2CyDhJ6Nsxakdy0fif5Ib+MUesdO4l/fcB0OhLOoVyry+tGoK+EJJ6oOi6HkVHEQA4lqx7BMwXHKXuGj4lVkO/o0Bj8BpQVHwxQL1Vl0N1rw9iwieeiViB7zcOT756Uvb2ZxXo1NVCAKdPMUvX5PCLV3n7wbt9xwE77wF3+OThoqZZydF8MUoadz1qwHswe4RSr5b+vtyqCCBU9sx7Siz0bUqSRRwajJzmvn8wL/O1x/asmo0O69sULOZniW/zhN8yq/nh0oduiJNCpbxYKStREJtg+2HfW229g4tS6H6+K+RZGXcB7+pCcX0rhoQkPVzdMkX8jHJf6eFEbHM9t0M54pCaj8Rlt30RurMa+lUZ3dQAT+kF6vcShGT0r2gEH3/HgiTUYD3p0KduatFdTM0IkkmzJnCyeciRq0bQGSQeyTCZnTRinlD/IbKR7kVBAFOS654nL8+qtfpZ7NnUh2q9APP02HafhBBZV6VcI5a2zkE5IFwVlrUj0aciJLgqS9hju//C9Y+dwXMF9ECKManHiIQasri+14uXwH6SVzCkFD0dW2mqCvI3sjl6OZ+4ipQU9ifkiicILZ+jySzTay2QaSyy/CzNOfiH6zimqziUpzBnUpguuYxnY+QGutje3OBja2N/Hdb/87/vav/gKdAYEXMwfNSGGUQ0Vb2LYGqIXrnQNWGHpRgcx6FXvfbUQg4IcpL5BTyfVmCqGzIziARUf7jsnJ9wGNtDujXBcus0nKhwVfaw8DyTVHyRgAk+9nIj5ZY2Ns7JAgiLS9voXW+jbm9s2jMdvUQ+hhT3xSYfXw9UImxiZMEapUjXu6VAbaGoI1pJ1be2Js9uc03rNxJ3f+7tmNzTYFckNKrpiRNVEv9dXde9DAg0NCWxN8eJHWk/gdK1EsgiwSs49neE1628rvTOi7Wq1pC0oRInCGaO5Zwrve9hb1bOT7mZijoG6H6KKw84Hsh9quxibPUZ2JeTOvc5Qg217HF9/6NkS3nMR8nYpZMxj1W+gFQD5gkd10mTO/741EaoGcSE71CXKWEnKEqKBg2ONSMt6Fx5pV6GCpvg+DQRdOtYLVc/bg4KtegHWnQK0+j7hWQaNaUzkHGhuGaJ3aQru1gWOrp3HjD27EX//93yFr983gkEz6wMZMBHNQlXMfu5l5MJcbO6mryd+brtFqD5s9cLUGx/cQxbaiQMw6q0mqy4Zy7ztgaqeZGXD8qfWqZ/t9cSoFm2Spw6OD6uVwLvGsZFIv66vsZewn2FhdR7/dx9KBPaJJ4zGff/jjnyhdjzx/7Wlkv6w97QX5E9epxmZPBd6QsrbjpLNWxyMxjteuao1xOVaXJ5mGc6K9IBMlOOPNjgSWCM5IMggXsdQ3xzDP80MBXKygj2UdiOGYIR6sDXmVSDQeZRLqLtNVtGVIQ5sfdbPLiyDhtFZmRKK73ohRrVbwrre9A3FEJj+DbyprScFCvT8LtDmBjKpMmckp3mpyTjcnu8QTmhxHyBB9pVzccPU4Pv3qX8HhfgXNRowaasj7bfQqjniwPNI54cz1fNKvmAwMR5hhN3cvgUM1LMMd5XVx1rfUo7IU80vnISEVzHXQPXgI9/vtl+OkeMcYlWpdamAK0jmgOOvq8gY6W+tY3VjH17/17/jiP/6dFN5lz5S4j+X7NB3S2ajGGpvAseyUMzSr8hrZuh3vtew39lqa2W2ivzTiWDCdHMtRTUxHpj2SvRYZJ0+6lxkTVvZsFtHeNfwcjxZjqYFwP4e2sObrSnc6wSj2NIp6aWkOhgyn6ffQ7vTQ2W5jaf+SkDFkMum0semmnVavsvU1ZdDbweOKJk1eq8ZmwRSZSKc3wZxU1iAmi0IvSClRNTZLwbJCPGLUNHJDkObnsU7Eeptljgj3zoTLlrMp9k2PzPYLQ+uaPrVEGIhycyWC7H05GXVRddDGoD/E/GxDjP9jf/ARGXKhzXVs09C/58VA5fXYzxao16DsGw1FvGDmSK1KjE3mJ2QyE3xw/G78wYtfjCtnDmAuDsWz0diSuo9RWwcuKmJMxgj1S1IErotapaFiQTk9GWcyJMLPLIoEYUREcxvz8+cCwz6cKMbG/CKufOOrcJoKYKGPuFKTbmkhIBBZA8GRTayvnMR2p41//PI/4x++9E+IvVi8K9fGygJO0OyJ2hUNx0ZDepAatFbkE8uESrtP+LPJVFLeo4SoqBksz/eI2LZkfAscAkCVcZ/c9BoSvJF9N6YH3pdV1k5tIr5irGb/CmrvjGSyEb9rQIFikYlnL6Oh6vGedTvY2tpGj0TupXlhFwXUILHGRl0NCQtT41ZLyra2IZAJ6jjJLTJRAVZEyPShSbepuvyxgdnGu0JPfYlvRZefN4BsvgGGbDMRWFVPISEXG2aFNSoah+3iFX11ntoy51hDDvFqnO5ivoddeFt4n77F9nmZ/lIlI3/no1yzO/PUVE0MQvnsO4tCH5/+9KfEuGlU0n0gc9d4KjP04fAHbsAKanEdXqydwtLTJiN4XdRYYCU41B+hPxiif+IO/NmvvgaHMYNa7KGCAKNBF1lMNSyOH9aDh9e/d2GP6EsGIkSbo1GbkSk44UipYIHoRQzhcm04YqrSBEYJhkmB4qILMf/C/w3LSw1EjRn53UazJhKC3KaJk+HUyRWsLJ9Eq93BX/zNX+Kb3/13GeahKK3M35VDU2D8krCSXUMR9uV0ITk8VZKQUvCuz7qg9Y/Kk+WeGBVD4eFSnU1TFY6SVtKFTJkpQqO8zM+eDHex3QXllRT+Yom3OL3OZ+AHlOaQq2IUl6pcvcyTJ68pQz4kk4meVD07r5iHGcdfy90Y5ej22lg+tSoh+UyjglptVoSRxNg4lcaOOWKCLaFPifZvjY1JqYRyhmmi7Sj8P95sS0SedGZbYZ68oMpTFelwIE2BNukUIIbCq66LATmHJuTjF+Nj8jodhMHwkDdH+4fUyLhhFajZ2RFePmV3CyPtTS6XPHYsUoluVTY8fY0RoBHmRoo48vHu668XdWRuEmH8s5hMZDLricfOUoarVTE2P9br57Wz+EwnZY0NSY5Wu4ft227E37/uTTjfayKo+6jmHobtNpx6VTyXGGahxtaoNqQuFlYCDLZ7iIKK3JsaFYq5OkTt5N5rPSqszMIfpShyH9nBg/Cf+Vh0LzkHbq0hsnqNuRlEnLDK0N8rsLK8jrvvugOtThsf/fhHsby+LjE4Q2l6+DKRocwcsbzJwbALzyM1imEjO7MTjATAMRIXrJGJcK6ML0GaD2SgI4MkHrY82IgG2seElkVjU2k8SQV2kSwQzqVh5O8WLo6BuPGb2/SHHkuNTUagkVhBEnyfs/F0/SxoR7oca8JChk5SkR08fuQE5vcuIqZsYG1GPJ/z0Mc9XnQjxxZuWxXGuZXC4Hyexjb2NKJrQr0/qjjZUNKeUvbKDYSLgeQxMo5XiK4TaQOtuhYYpAM5PaY3vP23RST1OnKZtkn+GYvLknyaJsT7FiTct1eVDXbK72l9yWE9aiho3yte+Uo87AEPEjSQLAE59aTYm0i4xT41z1PPVja2yGN+lKPK5laXI6kcrG9s4cQ3voLvfezTWNoCqvVQC9ftDrJ6BE7v1U/g7OwOGjKrjNNmCFaEyEfK7AgS3SU8ELyIHdw8A1hvq8HpdJEXIYb79qPyrGuxfKAJtzmL2cYsFvfuRb3K8Zfq2TrtPr5/0/fQbnfwgQ+8H+2kgziuodvvyVqEzGI9jUqCgurAIwFNHNeEg0kF+SiC5w5MRMJN7KFPY+cmDViQHkrJgb83YMOw5EGK7Fope1Emk8OVk0cpAMTPpSdS1Nem5nYvK7FCi9Zne8h+Kjg1VFW5bFTG7zJIujrck3J2lN+nt6OxcX09Fd616P84BUpS9NotbC5vIpqpolGPUQlrIhvhPOzaJ4ix2eq9un4VObXUGtbZ5MsQapYwXdWNaGwcc+sZQYB728JZ3oPv1XcUyMVHcD94ZIhRbPPsxrbTs/F0hOjaK0ijkPJk6N194V/eu8Hx/e6ti4CehTQptrtWAh+PueZq/MILXyTXEnpVCddUebAvKktiDG6MSlyH8O9kthql16ghWSASfUJSFQpsrLdx4199Eqf+6RvY20lRcStw/RRBJZZTmvxMThKyQBVBEqZPbFQlrzHwKsj6PbiBclxdslSktKAdAh4G8N0McbWJbH4PBo+8Aq1LDiFc2IswDrG4tAeNZkMI012yVNIc3/7Od7G5voJ3vu3tcEJPtP1liiqHcpi9UWQOIh5AsqiqLyJ5O3NRiVRU49HopwFZpIQ2SVlM/i8Hrh7GVAaQ6EWmwuQSjtvciOCEyD64mlPteBhalR3istPb7rbu6m35fvZBBTRGYnpoyjcVsE9qvyJMuxPXIAopZOpBH5vsQRzl8GueiCyF5G0SxaRnU+UjDQ+5AewUGWuEZWPLMzsfe6exMfdivH02ma+zniw5J0dqh/D4RDJdAp7jjQcsSgiQ5QIEUFOxMdMc52v2vbX3TYeTMzT+X3mcrfA9jjLs8SndJdQhG6Hu+jjv4sN4/W+/TvrRPESitEU0Lyt6SncTLfgIUVhFtaYSCbzXbB3hskVw5QQt0gxbG2381Xt/F9mNt+O8IEYlpYzCELlReiYnM+mazmUBoLhXNXfgtgj9qhibx8HmMnmHP2W3gWpJuqMOfIzgs3bX3IPkUVchf9iVCJb2ScxERLMyPystQ303h5c5+NZ3voMf3vIDfObTf6TzyR3IyONRkmBAGhVCYXfEnMAZxlI7VG/jYeh0MHITZCNuWJ06xKwoYk5UaFlE3lMaXulJTE8dTxCzo2hskufZWekMiTnx1HRP7Fxrrf/a2WzTeZn9t13rkfC8WBxXnqewXcRrGUI+G14pcT8Stvg4hSl/pjimAfmQfZw4fgq1uIpKM8Zsoyl5NA8K58pHPKqoVAgQGP3HHTLdpunOyAHY+JZwv/wdA2QFu4OpQEWEjNokZZd95vigaQPI0UWRk2lOYqkCLCRy8sE6j83HtJCppQcaHE9qblaWAM4whF0QxnKEKu0sJaEd5oz2+bLEwm653ji0FJo+RwdnqMtQrgKf/MSnhEHCkcI0NgF7sgFGzFE4/9mLRR48orHx2wU+qtRslPCLW4cgT471rTb+5CXPR6U7wB64WAybcNyRhFCtQV+vPedE1UBOXVkLlhDkFCb1KwAS1tQIDuTCkSxScvtCaYtxwh4C0TopEO8/jPyaB8F/zMPhz+9B5mYIaxUEMzUlELsOotzHrbfehq999Sv40z/5nMiTExBYCFI0Y180LvtuBZnLGlNP1qQ6syhgQhDU0E5bUpNzvQCnT2yiGAYihnPxlXXpSucsQ/7nQCMVC5TxvBwkiaCklFRkvkmPpmmL4gTcLwRUeFARmmcWzGtmysM9VW77sViDWrACIXJOSUpEtpLK17GbgvvA1tG4N3hd3JfMOWXk89QeCyMf/VZHCBLrp9ZlFHZtvi4dHUR4OTfPeeDVjy3sMDvxHiWPYFE4IoE2XJswnnk2scWDuYCvbng8n3pnOHhvHoayX/SWPAklvCgZG0sA5ViaxiZsA6H+kLUdmevVLy8z0MzjDJSpzAYga1+W31xnqXmU4arw7M7yFayxsQE0YyDu5aixQTNw8YmP/SEyznB2K2J0spB5gmE+0NqhG6HiVxA3Yg3BuCkpRUD6WUHUkAvpYHl1A1969SsRdnuIhyPMNhYw2NhAtTkjDYkyX45bgbPyTAgtMgKDBHG9oY2i9HRxAYdlAC+Am2i+LGz2uI+qCGU4CGbPRXbNA1F5/NXw9u4DZOKNhyJi/iQ4DyoIcc89x/D5z30G//xP/yxKv6GT4byFCNe95fdRrwXI3QC3H7kTw9EAN33vW+hsdXH65Cra2y20Rgn6vQRJmmGUMNeJMUyAzv4e3DTAsM+hhQtIEmUZcS/JwcrCvbBlHJk3MG1sgkx6KqXBtRxSd8WhgBR3CMPPibFxPzCCG5chLOooTH+df80uAO4va2zSjUEswGZmZPRLoU+diBDHDYGfZGhStNg0urWyJcZWX2iAEozMo8XYrnjEowrbk2TDSDKglRzLgp4K0Ng35YZO0p4U98QYuVGlmEXgwtBx2MA41uz/UcGcJraKXCrDOzXhg9WDlLjdqGPxWrIhu5SHCGLVgFddCcLsNlcrz8A0n0/9ezYgcjGFLaUKu2WjlBv7I4xNJ6w6cHweEOQeDtFwfaT5EB/+wIdF74PtHxJGkj9apBgR1WLugxCxV0GlUdFaXeCiElCf34VnPBsvbvn0Kr7wwhdiphIizDLMNxaQbK3J3LUsDrXQQqIwtUooG2AoTYNeD7OLi/CGHEnswK26yAc9+CNW7wlWKEd1VB1Iz5s/9BHU9qF91WHMPOspCA8dRFCJRN8/kRypQODTNDycPr2M915/HW6+6ftwwggLVeBhF5+Ll/3ss7C2dhdSIoJxLKrGQUweZwXu0JEifSvrIc0KESvd3miLcVWqC7hp8zhObS7jhn+/Ea1t6s3w4CFgoo2k/YRTeXL4AaXitLTA9ZMgVDbNSDy1sGmEEsd2JRn9qT1mjBVKh6Z1HgqmMUc0/XIyoYcIoxnwQoK4zJQnCObLHqeHTUcpIi+UmXZ80CtbWT4pN6cjtNpttFa3hLTA/sBqpSpTisRELnvEIwuGI+PWdhOW2SZRvpmdNGKhUzZ2jttsZGaxjbm12p+5PA0ZWv64edPuxiYhpWnDoDwCE2aGIX61In9nva3cjLibefMG9zuqWcjw7WyvobHR45wJ9+tv2AUjeECENUOKuhuA8PZ173gXDu4/KFLgUleiQci9Ym6SCDTNEki9UdM6nO8jqlC6QD2blFjyAssnT+NfXvoKhDnHCjtYmltE3u1Kg2R/2JdG2pEod/lIeD8ImfNXWYv0Qpkv4HARIsrGj+Bz1njGaZ0x0mSEcD4G23CL9ghZXEX3QRdh8X9/CioHL4DbrCOPgSFrZgOyM7TUsrayius/+B7c+v2bBWU9GIzws8+5Go991BVohCHWWluockxVu4uZ2QVpM8kCUphSVBNGJJlI5VEoiQaUjDyRLPQaIb705W/iHZ/6Iop8DkHaw1ak+pGMdsTTmJIP11ioWuPqLmtsuyONlsY1De3vCpYIoKLiqtMItAxMcbV4zXtu9xlfxwhC+g7EoHPJ2VZX1jFodVCdqWN2bladQaTDVZyLH/jgwg6T4xtwoyklaaKbriEa5cRYHyG/UN3rZPNp2EXG+v+KsU1yIzrsSc5WVjq2xsaFT3p9Ud1CyFxPjc3EbHpNpq2wXFexXovUKJKrmQSzmG1DU2t89lrkXuxC8+IpR5TM480fFHBCmpyLasXFz/7cz+PJ1zwZGRN9l1N1SOwqRMBzKMgWGz2raDRqoozseCGc2EODcmw8wZmzZRk27jiBr73y1QJvR/5QdEEiGi6fr89IGMlQphKGorZLhI8FdV6b18/0MOFM7xprD0QfPRQJAQBP5fUiSo2nwPYA4dwedK86F/WnXovmBZdjNF+D3+BMPMjIYJa2GTkcO3USb73ubTh29xG5L3zrl7/4CbjyvHlUOPV0vomC9bHcx+b2Jiq1qgAU3UGCvY0ZGaZJb+8FFSmC8/uzHkky7x99/q/xkX+8ARtOA3GRascHeZBGQ5INo7K52TlhVKPlcHHZzR/uKnlu194KCdnC9tmoWdbYSKviZ9PAI1LEhCGSjcdcSyRkarx8nkpvjJLoSXnYnTpyQkCUmaV5KSnQtoKoopHhJQ96SEGJOatelVNN10RjE0BEp8mIqGrO6Rw5YifCSFoc6LodSR550tLYSLGajH76UWGkQrxs7z8jzyrJipefs8JD3BHkQPI5q54rRmUpWlJbVmnr6Qc9GMVv5HfZwcwDhUaYaEOjDZunf0+GMBjWg0DvjiNoZBS7eNjDHo5Xv/K/YpARrFBal4TgMuIoQZr3EYWzIulHWQUaG8O2KmXJuRgc2DHKceqG2/CN33odIsdH6FBJOEYggko+elSqzoYizb0wP4/NrS2w2ZIHED0aKVrSQ5ZmyCPKAKrUnE/q2qjQEI89hvEI3mYKd6YpBe3gmkegeeX94S/OoGhQaThD3qe4DulRPo6eOoE3vOH16GxsyuDD3uoqXve6n8WDD5+LpUqIzvYaGo0Qab+DNGUoWBEZdaLc5yzOo7tyHKCSdg70Wz3sWeDsNxcb7S6++u2b8cGv3YntYAleug1fit/iaoS65oWh1Mokp5UhGIbEYLpTpteoPBGUoki2IbFMGyx7PhvVcSTvuIwwljknAuqhR9RU1Jd3KliXkU2G9cfvPCrXOLd3FrWwKqO0mL+R2+mcf8VVBTXWGV4pz3DncAQlHpuBfGzmJICRjRAZY+OisL7DDlpJZimtzRLCeFj9fTM2dh2faRE7hT3t82VjA8m/bLqkJrBR7iJOSpSOjZQSgu4SNlpjsws3Bj6MBLeKE52Jpk6cnRlh5DqoFA4qVR9xXMGnPvaH6KRExbTTncZGT0TgYJj3EQYz4lFpbHADhHGEmDxJI0BKguuRr30P33v9G6WQWvGorxGhYjh4o9yXoinzbD4k4Q887czIclT8GPVGA0V/iMQdCNRGsEQga4HcPfgpR2wPRa68vucgtvfMIHjsI1B7xEMRLDSAGe2Ep+IzDxcaMafY/N7vvREJC+socPzfv4qPfvY6POR+F2KZM7r724iqLub27JV7zwSCdCtuxpD1sjSR66PWZCGznVkinkErGeKTn/m/8We3bKPrLyHKunBzjsmSYYByePeTgXRmlKMNPQw13C93HEgU5iqiqc2gnpm5ppRCsmjsfbNtMfb3+V528KaN5qQLpnDBHgdOctoNeJNoispfbBg9tYJqvYa4EaBaqcOlDIay/OGcd79Li1qjKaetLeLavMRelN10Qw6Gd0ZIOSnF01FBpNlo8dtwIs1IKCkw0/gIQlhO5FTRWo1HgQT1EiYnKmlCWAOzX1JeS6M3v8PnqZvBxxg1ki9vEKNdQsGyUStKpTmc8DXN9XOxKBOnP+eIJ6OLQY1EEnwdoutDYQixwyoIVLPxz/7kr7DZa8O3lDPW/oQF0UPmpGJslbgqRsbAsRqzyTMwU0iBMM1x1z9/Aze+493wiwzV2EPkhghZvB3SaH30uz3MmCEUAnhwnAB76YoAvfVNITUz2oCv/WZkpQS+lhtID+PglDzoI6QwUDCLdO8CRg+6HHNPfRzCvfMY1TknPUPe6wvUzZTitnvuwJt//40Y9QnFBwjWV3Di2G245LLzcO7evdg/t4CFc2Zw1ZUPFf3J5lwDlTBAJWbMwJCZhIYuss4WOCOFhZ6hKEe7eMNb3oevnWB/3n5UBy0kIQeLaGsNaVAEtujJGa55otBNehbpX0aBekzTMqRjV/cTveEEPWcpV9FOPqSWZlBFho7dZKAHtPBzNXqp0HMSLGFdk5GXGfgibBGzV+z+KAYDbGxtobPRRn2mjmqjIvtyzB+lvR285OKiUp3BbHNWuV0lhSL7b2sMaUqXTGMbIXQ9CSetNJ3dwDYm5vtYdvYYXZQXTYd0O9keEsKNzvQoZ4SRxtgE8KBnJWBiWPz2BuyWc53pPi0SakyfwxWlq5rNjnb88O7GxgSfHCgaWxy7aG2v4+//9ivojVKdZyekak7ZYX1wINNAyaKpVRnHM4yMUI1ckZtjIX7k5TK88LYv/htufd8HZHZA4NFbVRBR830IpP2RoGBV6rAYhI5IKIdb0GKF2kuQhyFjhTljJuwSFrxZlGWOGw09TkmU4ykK54GFBWxdfAjNn3oKquftg1OLReErS7tI+6RGubjpjpvx1je9UTZPVA/hdCP0sntEC5MqXtvryxROwVxUx/raCTSbVZkvt73RRS/jJi5wzrkO5mfqeMBDrsJFVz4A1T3UPDmA973nwzjRKxB4MwjyBLk/MQqOH9YuLeU/6pmnIq56vJoWGiNJZw/d3T3eRONfjNl0EHPiqKwT6Wmiv2M6Kpim8FocF332Apq5DMLDlUhkgmaTRLBJpv9WF5VaBbWZmoAkjGzI+BcHdt6llxWMJzlAXXIJMwjc9hKVaxMsLIqxZalA+2JsZ1EPkw5aw6kkW3ry+M81Nn4JfpZ4RvMhQr41IjO7Gdf0z6xIq/7cCtFMDgbJS034wXYS69koGMrVp7FVqgGOHzuCr3zp60JYteJI7LymZxtlfQxzTmepi2cjClk4Phq1SH6fhQE2UUSjHN/8/F/j9B/+seh3ViMP9UpVTnSiiq5Hdgp5c7ko7srhSCmFjM/rsD430Jae3BlKqYGHB42Cuz3sdZkAAAoASURBVJaUMI8SeF4Bv882nBijQzX0LziE2Z96CrwLz0dttoG8RRWrPjZbPdl7t9x5K976+29CLpqXLuoeOZhsE2LZJUViRmjlaRe+HyMk20VCwQJu6CL3Rui0tqSxknqULKtWqxQvnZXvkgqAwr2hnks8NnMk0wUt3BgRB1KWiaLFzPMZgShbxaX0utildsZPh5flqajlg1h+V5j9IE1jbGyau4+U1c+6HGd5sOhuBh4q28rUeIdDHD16FNWgKlFQXI8RVSn3B5mvIMZ2/uVXFhQxmZ2d0yF6hLQNV1Lqbcb4FOrXuDdlgu6rjPP0WLRywsixO3wvFgvtl5PNYdRsafXC6JYwVOUQ9Nw6E9CQ9zXTUC0sz59R/4LhnjDoS13W9zoAc8rawkAH64kWpDkgxNRMCCoDIiOCKZNfFCYBGRMU1IErBxXHWH3yk59CFFJ1WHkOcmilAwwTRv2qVFyJG6jWq0IImKmFiJxABABYqo5y4F//6I/R/uxfS/Nn5GWoOhG58DKXbTRkYZbkZo43Ztyt91BOfJNvEhSSnJlQNb0ZNyoHF0qB0YGbt3kaIc4qcPoeksMz6F6wD/7TrsHsZZeJsflDFYptt3ropX18//Zb8KF3vAt99uIxP/YCPXANb1YK8xzK0WcBX6MDG2Ek+QC5p1Nr+TqKtLL47wUNQeqIGZBULvNj6VkEceQBonkSxXLYH8KHJQTb1EYOkoK1U0raWYRch5voBUz2Eu2vrHViuwFYpuFektDShvQM8qXcpIevRcN5m8eTnVieEIkNlY0/euvtqNa5tjV4IQ/oEBHZWXZq6uErriqGwz58P0J9piHafMKgMNr99k/R+nAgsX+r3xrfTDIeykCCNTapZ8gpVCAyM48FkufrDZpHUinZ/HbQwtlYG3aLS65mu7i5deUac4RVKj3tLFDfF482fk1ZqaukyVY2NoarShHig6cfl5/iL0Mz+4w0IQ/vfc/7sG/fPimmMvIXelmSYpSQ2saBEWpsDDUYjNDYQhCYYn5QIBzl+LsPfAj44r9JGaAWOah4sXY38HUME3nakhSb6OB6slQIq487zk0TPEVKCL9Lbxo1YIyx+VTo4ry2xEeUxUgPNdC9eD/8n7gG8UUXot6cEWMr8gxbZIB0W7j1rtvx4Xe+Bwk7xGXwoy8FXpY17IPhVtbrm2GXPET1mZHLwWBDDWHl0NK8e4RIByGyIO6ZVikeHhIm0oPmolJVUEOFBwghd4ouyfw5bUrlxFbm5zL/wQznZLsWlcRk3rs0gWo3Pru7WcxWozZ6kaofOD5YSWtW2J+gH8VXJ86H4bm8V6yHqeyEfgK/EsmIqBN3HpGopbl3gdJLMnve58FrJ51eeNUDioR6FK6PWl1RyXIoaQ2v7Nk4NH4MWMjhOjnyd0NrrCGIFIA5cYSNLoI92raza/1jymL4ZaUlw3hCRRIz8Wi8SWoQP97DenF6JsLU5fBih2cLA2QiqqpznQk8UCSVG0q0NdkpXani137zN3H1gx+qzDMju0DPJjQr0TYsMNNYEICEoj8zDcqlcqNxA+WoZMD/9XtvR/Zv30TNjxC5qRRPCW8I53EUiCgQOwn44GlMBj8l9Hjv2VjLtICMnFHCEJYbLRT5cbdQdI6irYVHdkqMOK+gs+ihf8m58J5+LWYuuRhRfRYRASYnw8rKGtbbG7j7xD348PXvE+STDAqfxmCYRfaOS5OlofaV90HhZkicgewTKfLK5uNmrWgXuyCt7FeUph5hflBKgseV1NgE4SZ/lKGxKwc+y0tce+2jFBc27vS3IpYSBhqPqPtNuy2sDqU0KjG8M03LfBe+nmg7W6VspGX3iOw/ejmuHa/OdIbQNlqbG1g7uYLm/Dxqs+x0V4K1ULW4b0jXomcbDfsS51ZrdTl5WIiT2LQkV6cho8aw9EDWm0kTowEDdtxg2+Q51WdW9kDlkLNsIhaQsW0ukjNpz4a6+74iR3JzhBzKkoMnwwvu60NEZkzsfzYy8473MsPuGMaQQa+GlGrfFsW+fQduGOGZz342Xvic/yKbQcRLiaYlfWm94Abi/igbG3mF/J+C/xlqhYvP/ObvIPruDySsDZ0EsR8hlJOYYY1viMg62F6BAOYVul4SVsmhw7yOB6GG/ywNjI3NaQEcNzyqIhqFaO8JMLjfQYTPeDyqFx5GWG0iSDMEkYvV1RWcWl/Gzbffhk995GNw4xA5Ub2SsdmDUsi7w7JIrR7C1KyksZFKNRmswvtTlbxM5A1lpC69CNmHJDqnGLDhNtR7zVlwEwkOytMT4NDyixTLpctAP3vXNMRQAsdhJEE4U0tTf6kP5r7cZzxUhR1iczKTWkm+ZovcrKEZ+b6Tdx+RQSdhtYJao4qIglJs6I0oky56gnAOX3n/Ik36girSJc80Z3aEklpn4ymuN0F1+CeTY+R9AiNLkGkJQM4Z0+ls/9zNCMrGOZ2wWt1G+2XIAbSvLxecqRPB2xJVaxLG/X99TCOYQu7nQvkqGkOWhzSImjFWFg1jEd+vxbj6sdfgFT//EqkPMY+SXDTlNJiukV/LMTuzhKDCHjUPjSjWRkgxpBz1HPj4L74ajSPHELLLtxghDmqqdszpPpmhpuUMI7lu9F59WVweBCOy/yWB44bhBlbC9jBh6OkiICslCpCLd6J9+xg2PBTn7EX+vJ9A5dBBhI15OSwqAbC+to4jJ4/gu9+/Cf/z83+KEa+Vmpms5wiQoWkES5KUzRCGjhClhwj8WHI6o7ajPEMt+SlxQGhYIwEflPGpKKC0Avmk17EXUOtlokDKfNN0TVtnwPeRfjNmLbuWloRIZfYOjVN3CDtUVHOSYkgT9Dvh/pX4lwuv4zXFgBnCSn28EMFgcTyGysZ7evS2OySimNu/gCQjMBUgpGyhnDaqKOBceP8HFiT1sl4msXMYoDk7t2NMK0MVGp0y8880Nm6CsQSCKQSXQYyzQfBjY2PdoiwLZsEQe6Oth7UAikkGJPyQCaTsDA6lEVMDijOAqF1t0F7XDg9rF2Msfa2/ShDF3njtFtAmR4IREiaw7zPPcO6hQ/iD975fdOcpwCMACbmBzE05UR6OSOHFdQ6N91BnTM/1VZ1dNLICH3nJL2LhxKpMAp0PyO7hYA4Hw1EX0XBGJ7cElHZjwy15hwpkiUSBH6PIU1kPCqdKnUxQykzmkssaktIlo4ZJ8Qqk+Dyo1RC/6mfgHj6Ioj4rcDenYq0sr+AHd9yMb3zn2/jXf/wH5GwFYOOoAC8MfW1/o5LkOKlWxlX5bPsh/5n5kJJ6KdY7TilKxpbysCwLR3m+MO8pViu9iaK0M6kaEaaXJl2ycO6Dsek8N6kfmOZlEz4ayysbKVHRQvoCeb1WTF8Fa+mFRRKx11XHQ5ocX5TlOHr7nUJBq7OtJo7knnONJQI0agL/L4zondVuXxV/AAAAAElFTkSuQmCC\",\"updateTime\":\"2021-11-25 18:52:21\"}]}";

            JObject oResponseUser = JObject.Parse(testdata);
            string status = oResponseUser["status"].Value<string>();
            if (status == "SUCCESS")
            {
                JArray jsonArray = (JArray)oResponseUser["data"];
                List<AvatarUser> lstAvatarUser = jsonArray.ToObject<List<AvatarUser>>();
                //List<AvatarUser> lstAvatarUser = oResponseUser["data"]
                if (lstAvatarUser.Count > 0)
                {
                    string DataAvart = lstAvatarUser[0].anhNs;
                    if (!string.IsNullOrEmpty(DataAvart))
                    {
                        string TypeFile = clsFucUtils.GetFileExtension(DataAvart);
                        string fileServer = "123." + TypeFile;

                        string pathServer = string.Format(@"\{0}\{1}", @"Uploads\ajaxUpload\", fileServer);

                    }
                }
            }
        }

        public static void UpdateLogText()
        {
            using (ClientContext context = new ClientContext("https://portal.evnhanoi.vn/noidung"))
            {
                context.AuthenticationMode = ClientAuthenticationMode.FormsAuthentication;
                context.FormsAuthenticationLoginInfo = new FormsAuthenticationLoginInfo("spadmin", "evnhn@123");
                //
                // The SharePoint web at the URL.
                Web web = context.Web;
                // We want to retrieve the web's properties.
                context.Load(web);
                context.Load(web.Webs);
                // Execute the query to the server.
                context.ExecuteQuery();
                List oLichTongCongTy = context.Web.Lists.GetByTitle("LichTongCongTy");
                context.Load(oLichTongCongTy);
                context.ExecuteQuery();
                //ListItem oListItem1 =  oLichTongCongTy.GetItemById(611);
                //context.Load(oListItem1);
                //context.ExecuteQuery();
                CamlQuery oCaml = new CamlQuery();
                oCaml.ViewXml = "<View>" +
                       "<Query><Where><And><Eq><FieldRef Name=\"_ModerationStatus\" /><Value Type=\"ModStat\">0</Value></Eq><Geq><FieldRef Name=\"LichThoiGianBatDau\"/><Value IncludeTimeValue='TRUE' Type='DateTime'>2022-06-01T00:00:00</Value></Geq></And></Where><OrderBy><FieldRef Name=\"ID\" Ascending=\"FALSE\"/></OrderBy><RowLimit>0</RowLimit></Query></View>";
                ListItemCollection allItem = oLichTongCongTy.GetItems(oCaml);
                context.Load(allItem);
                string outresult = "";
                // Execute the query to the server.
                context.ExecuteQuery();
                foreach (ListItem oListItem in allItem)
                {

                    string OldID = oListItem["OldID"].ToString();
                }
            }
        }

        public static void UpdateLichOld()
        {

            //HttpClient client = new HttpClient();
            ////var byteArray = Encoding.ASCII.GetBytes("username:password1234");
            //client.DefaultRequestHeaders.Add("Authorization", "Basic RVZOSEFOT0k6RVZOSEFOT0lAQ3VvbmdYMTY=");
            ////get lichj theo tuan hieen tai.
            ////firstDay = LayNgayDauVaKhoiTaoGiaTriTuan_Nam();
            //DateTime firstDay = new DateTime(2022, 6, 5);
            ////ip nội bộ
            //HttpResponseMessage response = client.GetAsync("http://42.112.213.225:8074/api/Portal/get_LichTuan_New?TuNgay=" + firstDay.ToString("dd/MM/yyyy") + "&DenNgay=" + firstDay.AddDays(14).ToString("dd/MM/yyyy") + "&Loai_Lich=1").Result;
            ////ip public
            ////HttpResponseMessage response = client.GetAsync("http://42.112.213.225:8074/api/Portal/get_LichTuan?TuNgay=" + firstDay.ToString("dd/MM/yyyy") + "&DenNgay=" + firstDay.AddDays(7).ToString("dd/MM/yyyy") + "&Loai_Lich=1").Result;
            //string content = response.Content.ReadAsStringAsync().Result;
            //Console.WriteLine("Đang tiến hành đồng bộ lịch làm việc...");
            //List<LichTichHop> lichTichHops = Newtonsoft.Json.JsonConvert.DeserializeObject<List<LichTichHop>>(content);



            List<ConvertCheckLich> lstConvertCheckLich = new List<ConvertCheckLich>();
            using (ClientContext context = new ClientContext("https://portal.evnhanoi.vn/noidung"))
            {
                context.AuthenticationMode = ClientAuthenticationMode.FormsAuthentication;
                context.FormsAuthenticationLoginInfo = new FormsAuthenticationLoginInfo("spadmin", "evnhn@123");
                //
                // The SharePoint web at the URL.
                Web web = context.Web;
                // We want to retrieve the web's properties.
                context.Load(web);
                context.Load(web.Webs);
                // Execute the query to the server.
                context.ExecuteQuery();
                List oLichTongCongTy = context.Web.Lists.GetByTitle("LichTongCongTy");
                context.Load(oLichTongCongTy);
                context.ExecuteQuery();
                //ListItem oListItem1 =  oLichTongCongTy.GetItemById(611);
                //context.Load(oListItem1);
                //context.ExecuteQuery();
                CamlQuery oCaml = new CamlQuery();
                oCaml.ViewXml = "<View>" +
                       "<Query><Where><And><Eq><FieldRef Name=\"_ModerationStatus\" /><Value Type=\"ModStat\">0</Value></Eq><Geq><FieldRef Name=\"LichThoiGianBatDau\"/><Value IncludeTimeValue='TRUE' Type='DateTime'>2022-06-01T00:00:00</Value></Geq></And></Where><OrderBy><FieldRef Name=\"ID\" Ascending=\"FALSE\"/></OrderBy><RowLimit>0</RowLimit></Query></View>";
                ListItemCollection allItem = oLichTongCongTy.GetItems(oCaml);
                context.Load(allItem);
                string outresult = "";
                // Execute the query to the server.
                context.ExecuteQuery();
                foreach (ListItem oListItem in allItem)
                {
                    if (oListItem["OldID"] != null)
                    {
                        string OldID = oListItem["OldID"].ToString();
                        lstConvertCheckLich.Add(new ConvertCheckLich()
                        {
                            ID = oListItem.Id,
                            OldID = Convert.ToInt32(OldID)
                        });
                        string Logtxt = "";
                        if (oListItem["LogText"] != null)
                            Logtxt = Convert.ToString(oListItem["LogText"]);
                        try
                        {
                            CamlQuery oCamlSub2 = new CamlQuery();
                            oCaml.ViewXml = "<View> " +
                                   "<Query><Where>" +
                                   "<And><Eq><FieldRef Name=\"_ModerationStatus\" /><Value Type=\"ModStat\">2</Value></Eq><Eq><FieldRef Name='OldID'/><Value Type='Text'>" + OldID + "</Value></Eq></And>" +
                                   "</Where></Query><RowLimit>0</RowLimit></View>";

                            ListItemCollection allitems = oLichTongCongTy.GetItems(oCaml);
                            context.Load(allitems);
                            context.ExecuteQuery();
                            foreach (var item in allitems)
                            {
                                ListItem temp = oLichTongCongTy.GetItemById(item.Id);

                                context.Load(temp);
                                context.ExecuteQuery();
                                temp["OldID"] = "_del" + OldID;
                                temp.Update();
                                context.ExecuteQuery();
                                //if (temp["LogText"] != null)
                                //    Logtxt += Convert.ToString(temp["LogText"]);
                                //Logtxt += $"_OldID{lichTichHop.ID_Cuoc_Hop}_";
                            }
                            //if (allitems.Count > 0)
                            //{
                            //    Logtxt = Logtxt.Replace("_DEL-bs_", "").Replace("_DEL-bt_", "");
                            //    oListItem["LogText"] = Logtxt;
                            //    //temp["OldID"] = lichTichHop.ID_VPKG;
                            //    oListItem["_ModerationStatus"] = 0;
                            //    oListItem.SystemUpdate();
                            //    context.ExecuteQuery();
                            //}
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    else
                    {
                        Console.WriteLine($"{oListItem.Id}-{oListItem["Title"].ToString()}");
                    }
                    //if (Convert.ToInt32(OldID) < 4000)
                    //{
                    //    if (lichTichHops.FirstOrDefault(x => x.ID_Cuoc_Hop == OldID) == null)
                    //    {
                    //        ListItem temp = oLichTongCongTy.GetItemById(oListItem.Id);
                    //        context.Load(temp);
                    //        context.ExecuteQuery();
                    //        string Logtxt = "";
                    //        if (temp["LogText"] != null)
                    //            Logtxt = Convert.ToString(temp["LogText"]);
                    //        Logtxt += $"_HuyDuyetLichCu_";
                    //        Logtxt = Logtxt.Replace("_DEL-bt_", "");
                    //        temp["LogText"] = Logtxt;
                    //        temp["_ModerationStatus"] = 2;
                    //        temp.Update();
                    //        context.ExecuteQuery();
                    //        outresult += $"{oListItem.Id}.{oListItem["Title"].ToString()}\r\n";
                    //    }
                    //    Console.WriteLine($"{oListItem.Id}{oListItem["Title"].ToString()}--{oListItem["LichThoiGianBatDau"].ToString()}");
                    //}

                }


                List<int> LstIDVPKG = lstConvertCheckLich.Select(x => x.OldID).Distinct().ToList();
                foreach (int item in LstIDVPKG)
                {
                    List<int> lstID = lstConvertCheckLich.Where(x => x.OldID == item).Select(y => y.ID).ToList();
                    if (lstID.Count > 1)
                    {
                        int MaxID = lstID.Max();

                        foreach (var itemid in lstID)
                        {
                            if (itemid != MaxID)
                            {
                                ListItem temp = oLichTongCongTy.GetItemById(itemid);

                                context.Load(temp);
                                context.ExecuteQuery();
                                temp["_ModerationStatus"] = 2;
                                temp.Update();
                                context.ExecuteQuery();

                                //https://portal.evnhanoi.vn/VIPortalAPI/api/Mobi/UpdateItemSolrData
                                HttpClient clientGetway = new HttpClient();
                                clientGetway.DefaultRequestHeaders.Add("Authorization1", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImhvYW5udCIsIm5iZiI6MTY1NDc0NDg3MiwiZXhwIjoxNjU0NzY2NDcyLCJpYXQiOjE2NTQ3NDQ4NzJ9.DpV8FNmfYHWlOhMK_2SAqVWigaqviAKXd6bRXZNxP0E");
                                HttpResponseMessage responseALL = clientGetway.PostAsync("https://portal.evnhanoi.vn/VIPortalAPI/api/Mobi/UpdateItemSolrData", new StringContent(
                                 Newtonsoft.Json.JsonConvert.SerializeObject(new
                                 {
                                     ItemID = temp.Id,
                                     UrlList = "/noidung/Lists/LichTongCongTy"
                                 }), Encoding.UTF8, "application/json")).Result;
                                string responseBodyALL = responseALL.Content.ReadAsStringAsync().Result;
                                Console.WriteLine(responseBodyALL);

                            }
                        }
                    }
                }
            }
        }

        public static void UpdateLich()
        {
            using (ClientContext context = new ClientContext("https://portal.evnhanoi.vn/noidung"))
            {
                context.AuthenticationMode = ClientAuthenticationMode.FormsAuthentication;
                context.FormsAuthenticationLoginInfo = new FormsAuthenticationLoginInfo("spadmin", "evnhn@123");
                //
                // The SharePoint web at the URL.
                Web web = context.Web;
                // We want to retrieve the web's properties.
                context.Load(web);
                context.Load(web.Webs);
                // Execute the query to the server.
                context.ExecuteQuery();
                List oLichTongCongTy = context.Web.Lists.GetByTitle("LichTongCongTy");
                HttpClient client = new HttpClient();
                //var byteArray = Encoding.ASCII.GetBytes("username:password1234");
                client.DefaultRequestHeaders.Add("Authorization", "Basic RVZOSEFOT0k6RVZOSEFOT0lAQ3VvbmdYMTY=");
                //get lichj theo tuan hieen tai.
                //firstDay = LayNgayDauVaKhoiTaoGiaTriTuan_Nam();
                DateTime firstDay = new DateTime(2022, 6, 5);
                //ip nội bộ
                HttpResponseMessage response = client.GetAsync("http://42.112.213.225:8074/api/Portal/get_LichTuan_New?TuNgay=" + firstDay.ToString("dd/MM/yyyy") + "&DenNgay=" + firstDay.AddDays(14).ToString("dd/MM/yyyy") + "&Loai_Lich=1").Result;
                //ip public
                //HttpResponseMessage response = client.GetAsync("http://42.112.213.225:8074/api/Portal/get_LichTuan?TuNgay=" + firstDay.ToString("dd/MM/yyyy") + "&DenNgay=" + firstDay.AddDays(7).ToString("dd/MM/yyyy") + "&Loai_Lich=1").Result;
                string content = response.Content.ReadAsStringAsync().Result;
                Console.WriteLine("Đang tiến hành đồng bộ lịch làm việc...");
                List<LichTichHop> lichTichHops = Newtonsoft.Json.JsonConvert.DeserializeObject<List<LichTichHop>>(content);

                foreach (LichTichHop lichTichHop in lichTichHops)
                {

                    try
                    {
                        CamlQuery oCaml = new CamlQuery();
                        oCaml.ViewXml = "<View> " +
                               "<Query><Where>" +
                               "<Eq><FieldRef Name='OldID'/><Value Type='Text'>" + lichTichHop.ID_Cuoc_Hop + "</Value></Eq>" +
                               "</Where></Query><RowLimit>0</RowLimit></View>";

                        ListItemCollection allitems = oLichTongCongTy.GetItems(oCaml);
                        context.Load(allitems);
                        context.ExecuteQuery();
                        foreach (var item in allitems)
                        {
                            ListItem temp = oLichTongCongTy.GetItemById(item.Id);

                            context.Load(temp);
                            context.ExecuteQuery();
                            string Logtxt = "";
                            if (temp["LogText"] != null)
                                Logtxt = Convert.ToString(temp["LogText"]);
                            Logtxt += $"_OldID{lichTichHop.ID_Cuoc_Hop}_";
                            Logtxt = Logtxt.Replace("_DEL-bt_", "");
                            temp["LogText"] = Logtxt.Replace("_DEL-bt_", "");
                            temp["OldID"] = lichTichHop.ID_VPKG;
                            temp["_ModerationStatus"] = 0;
                            temp.SystemUpdate();
                            context.ExecuteQuery();

                        }

                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }

        static void Main(string[] args)
        {
            StartupSolr.Init<ViPortal_Utils.DanhMucSolr>("http://solrserver2:9000/solr/Portal2021DanhMuc");
            //UpdateLichDonvi();
            //UPdateMenuQuanTriKhaiThac();
            //TestSolrSubSite();
            //AddFieldAllSubWeb("OldID", "MenuQuanTri", "/cms");
            //UpdateHinhAnhVideo();
            // Change formats
            //dtfi.ShortDatePattern = "dd/MM/yyyy";
            //dtfi.LongTimePattern = "HH:mm";
            //dtfi.DateSeparator = "/";
            //string temp = $"{DateTime.Now.ToString("dd/MM/yyyy")} 6:00";
            ////DateTime.ParseExact(temp, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
            //DateTime BreackEndTime = DateTime.ParseExact("24/05/2022 06:00", "dd/MM/yyyy H:mm", CultureInfo.InvariantCulture);
            //ConvertLUser();
            //AddField("UrlLink", "LNotification", "/noidung");
            SetAprroveV2("daihoidangbocaccap", "/noidung");
            SetAprroveV2("vanhoadoanhnghiep", "/noidung");
            SetAprroveV2("trienkhaichude", "/noidung");
            SetAprroveV2("tinvanhoadoanhnghiep", "/noidung");
            SetAprroveV2("tinnoibo", "/noidung");
            SetAprroveV2("tindoanthanhnien", "/noidung");
            SetAprroveV2("sanxuatkinhdoanhdonvi", "/noidung");
            SetAprroveV2("tinhoatdongnganhdien", "/noidung");
            SetAprroveV2("tinhoatdongcongtacdang", "/noidung");
            SetAprroveV2("tincongdoan", "/noidung");
            SetAprroveV2("thungo", "/noidung");
            SetAprroveV2("sapxeptaicocau", "/noidung");
            SetAprroveV2("GTSoDoToChuc", "/noidung");
            SetAprroveV2("lichsuphattrien", "/noidung");
            SetAprroveV2("linhvuckinhdoanh", "/noidung");
            SetAprroveV2("cuocdoiHCM", "/noidung");
            //AddField("LichGhiChu", "VanBanTaiLieu", "/noidung");
            //AddFieldAllSubWeb("LichGhiChu", "VanBanTaiLieu", "/noidung");
            //AddField("UrlLink", "tindiembao", "/noidung");
            //AddField("DBPhanLoai", "LichPhong", "/noidung");
            //AddField("DBPhanLoai", "LichDonVi", "/noidung");
            //AddField("DBPhanLoai", "LichCaNhan", "/noidung");
            //AddField("AnhDaiDien", "LienKetWebsite", "/x1");
            //AddField("LichLoaiCuocHop", "LichPhong");
            //AddField("LichLoaiCuocHop", "LichDonVi");
            //AddField("LichLoaiCuocHop", "LichTongCongTy");

            //AddField("UserPhuTrach");
            //AddField("NgayTruc");
            //AddField("LichGhiChu");
            //AddField("CreatedUser");
            //AddField("fldGroup");
            //AddField("ThoiGianBatDauThucTe");
            //AddField("DBPhanLoai");
            //AddField("ThoiGianKetThucThucTe");
            //AddSite("pcchuongmy", "Công ty Điện lực Chương Mỹ");
            //UpdateSTTUser();
            //WordToPdf();
            //GetGroup();
            //ConvertLUser();
            //UpdateMaterage();


            Console.WriteLine("Ok");
            Console.Read();
        }

        public static void UpdateSTTUser(string txtadmin = "spadmin", string txtpass = "evnhn@123")
        {

            string TextExcel = "";
            using (ClientContext context = new ClientContext("https://portal.evnhanoi.vn/"))
            {
                context.AuthenticationMode = ClientAuthenticationMode.FormsAuthentication;
                context.FormsAuthenticationLoginInfo = new FormsAuthenticationLoginInfo(txtadmin, txtpass);
                //
                // The SharePoint web at the URL.
                Web web = context.Web;
                // We want to retrieve the web's properties.
                context.Load(web);
                context.Load(web.Webs);
                // Execute the query to the server.
                context.ExecuteQuery();
                List oListLGroup = context.Web.Lists.GetByTitle("LGroup");
                List oListLUser = context.Web.Lists.GetByTitle("LUser");
                context.Load(oListLGroup);
                // Execute the query to the server.
                context.ExecuteQuery();

                CamlQuery oCaml = new CamlQuery();
                oCaml.ViewXml = "<View> " +
                       "<Query><Where>" +
                       "<Eq><FieldRef Name='GroupIsDonVi'/><Value Type='Text'>True</Value></Eq>" +
                       "</Where></Query><RowLimit>0</RowLimit></View>";
                ListItemCollection allitems = oListLGroup.GetItems(oCaml);
                context.Load(allitems);
                context.ExecuteQuery();
                HttpClient clientGetway = new HttpClient();
                var byteArray = Encoding.ASCII.GetBytes("EVNHANOI:Evnhanoi@123");
                clientGetway.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
                foreach (ListItem item in allitems)
                {
                    if (item["OldID"] != null)
                    {
                        int DonViId = Convert.ToInt32(item["OldID"]);
                        HttpResponseMessage responseALL = clientGetway.PostAsync("https://portal.evnhanoi.vn/apigateway/HRMS_DanhBa_NhanVien", new StringContent(
                      Newtonsoft.Json.JsonConvert.SerializeObject(new
                      {
                          MaDonVi = DonViId,
                          TuKhoa = ""
                      }), Encoding.UTF8, "application/json")).Result;

                        responseALL.EnsureSuccessStatusCode();
                        string responseBodyALL = responseALL.Content.ReadAsStringAsync().Result;
                        var root = JsonConvert.DeserializeObject<DanhBaRespon>(responseBodyALL);

                        foreach (var itemUser in root.data)
                        {
                            Console.WriteLine(itemUser.SOHIEU_NS);
                            //staffCode
                            int IDUser = CheckExitField(oListLUser, context, itemUser.SOHIEU_NS, "staffCode");
                            if (IDUser > 0)
                            {
                                //updaet HRMSID
                                ListItem oListItem = oListLUser.GetItemById(IDUser);
                                context.Load(oListItem);
                                context.ExecuteQuery();

                                //ListItem oListItem = oList.AddItem(itemCreateInfo);
                                oListItem["DMSTT"] = itemUser.STT;
                                oListItem["_ModerationStatus"] = 0;
                                oListItem.Update();
                                context.ExecuteQuery();
                            }

                        }
                    }
                }
            }
        }
        public static void GetGroup(string txtadmin = "spadmin", string txtpass = "evnhn@123")
        {

            string TextExcel = "";
            using (ClientContext context = new ClientContext("https://portal.evnhanoi.vn/"))
            {
                context.AuthenticationMode = ClientAuthenticationMode.FormsAuthentication;
                context.FormsAuthenticationLoginInfo = new FormsAuthenticationLoginInfo(txtadmin, txtpass);
                //
                // The SharePoint web at the URL.
                Web web = context.Web;
                // We want to retrieve the web's properties.
                context.Load(web);
                context.Load(web.Webs);
                // Execute the query to the server.
                context.ExecuteQuery();
                List oListLGroup = context.Web.Lists.GetByTitle("LGroup");
                context.Load(oListLGroup);
                // Execute the query to the server.
                context.ExecuteQuery();

                CamlQuery oCaml = new CamlQuery();
                oCaml.ViewXml = "<View> " +
                       "<Query><Where>" +
                       "<Eq><FieldRef Name='GroupIsDonVi'/><Value Type='Text'>True</Value></Eq>" +
                       "</Where></Query><RowLimit>0</RowLimit></View>";
                ListItemCollection allitems = oListLGroup.GetItems(oCaml);
                context.Load(allitems);
                context.ExecuteQuery();

                foreach (ListItem item in allitems)
                {
                    if (item["UrlSite"] != null)
                    {
                        string UrlSite = Convert.ToString(item["UrlSite"]);
                        if (!string.IsNullOrEmpty(UrlSite))
                        {
                            var subWeb = (from w in web.Webs where w.ServerRelativeUrl == UrlSite select w).SingleOrDefault();
                            if (subWeb == null)
                            {
                                Console.WriteLine($"{item["Title"].ToString()}=={UrlSite}");
                                //string FomartText = "$SiteTitle = \"" + item["Title"].ToString() + "\"" + "\r\n" +
                                //    $"$SiteUrl = \"http://localhost:6003{UrlSite}\"" + "\r\n" +
                                //    "#Templte to be assigned to the subsite" + "\r\n" +
                                //    "$WebTemplate = \"CMSPUBLISHING#0\" #Team Site template" + "\r\n" +
                                //    "New-SPWeb -Name $SiteTitle -Url $SiteUrl -Template $WebTemplate -Language 1033" + "\r\n" +
                                //    $"$SiteUrl = \"http://localhost:6003{UrlSite}/cms\"" + "\r\n" +
                                //    "New-SPWeb -Name \"Quản trị\" -Url $SiteUrl -Template $WebTemplate -Language 1033" + "\r\n" +
                                //    $"$SiteUrl = \"http://localhost:6003{UrlSite}/noidung\"" + "\r\n" +
                                //    "New-SPWeb -Name \"Nội dung\" -Url $SiteUrl -Template $WebTemplate -Language 1033" + "\r\n";

                                #region MyRegion
                                //string FomartText = "\\#Variables for Processing\r\n" +
                                //                                "$WebURL=\"http://localhost:6003\"\r\n" +
                                //                                "$SourceLibrary =\"Pages\"\r\n" +
                                //                                $"$TargetWebURL=\"http://localhost:6003{UrlSite}\"\r\n" +
                                //                                "#Get Objects\r\n" +
                                //                                "$Web = Get-SPWeb $WebURL\r\n" +
                                //                                "$TargetWeb = Get-SPWeb $TargetWebURL\r\n" +
                                //                                "$SourceFolder = $Web.GetFolder($SourceLibrary)\r\n" +
                                //                                "$TargetLibraryUrl = $TargetWeb.ServerRelativeUrl + \"/Pages\";\r\n" +
                                //                                " Write-host $TargetLibraryUrl\r\n" +
                                //                                "$TargetFolder = $TargetWeb.GetFolder($TargetLibraryUrl)\r\n" +
                                //                                "#Call the Function to Copy All Files\r\n" +
                                //                                "Copy-Files $SourceFolder $TargetFolder\r\n" +
                                //                                "#Variables for Processing\r\n" +
                                //                                "$WebURL=\"http://localhost:6003/cms\"\r\n" +
                                //                                "$SourceLibrary =\"Pages\"\r\n" +
                                //                                $"$TargetWebURL=\"http://localhost:6003{UrlSite}/cms\"\r\n" +
                                //                                "#Get Objects\r\n" +
                                //                                "$Web = Get-SPWeb $WebURL\r\n" +
                                //                                "$TargetWeb = Get-SPWeb $TargetWebURL\r\n" +
                                //                                "$SourceFolder = $Web.GetFolder($SourceLibrary)\r\n" +
                                //                                "$TargetLibraryUrl = $TargetWeb.ServerRelativeUrl + \"/Pages\";\r\n" +
                                //                                " Write-host $TargetLibraryUrl\r\n" +
                                //                                "$TargetFolder = $TargetWeb.GetFolder($TargetLibraryUrl)\r\n" +
                                //                                "#Call the Function to Copy All Files\r\n" +
                                //                                "Copy-Files $SourceFolder $TargetFolder\r\n" +
                                //                                "#Variables for Processing\r\n" +
                                //                                "$WebURL=\"http://localhost:6003/noidung\"\r\n" +
                                //                                "$SourceLibrary =\"Pages\"\r\n" +
                                //                                $"$TargetWebURL=\"http://localhost:6003{UrlSite}/noidung\"\r\n" +
                                //                                "#Get Objects\r\n" +
                                //                                "$Web = Get-SPWeb $WebURL\r\n" +
                                //                                "$TargetWeb = Get-SPWeb $TargetWebURL\r\n" +
                                //                                "$SourceFolder = $Web.GetFolder($SourceLibrary)\r\n" +
                                //                                "$TargetLibraryUrl = $TargetWeb.ServerRelativeUrl + \"/Pages\";\r\n" +
                                //                                " Write-host $TargetLibraryUrl\r\n" +
                                //                                "$TargetFolder = $TargetWeb.GetFolder($TargetLibraryUrl)\r\n" +
                                //                                "#Call the Function to Copy All Files\r\n" +
                                //                                "Copy-Files $SourceFolder $TargetFolder\r\n";


                                #endregion
                                string FomartText = $"https://portal.evnhanoi.vn/cms/pages/quanlyfile.aspx?doAction=CreateSite&urlsite={UrlSite}\r\n";
                                FomartText += ("#=========Thêm site " + item["Title"].ToString() + "========\r\n");
                                TextExcel += FomartText;
                            }
                        }
                    }

                }
            }
        }
        public static void AddSite(string Url, string Title, string txtadmin = "spadmin", string txtpass = "evnhn@123")
        {
            string domain = "";
            bool formauthen = true;
            using (ClientContext context = new ClientContext("https://portal.evnhanoi.vn/"))
            {

                if (formauthen)
                {
                    context.AuthenticationMode = ClientAuthenticationMode.FormsAuthentication;
                    context.FormsAuthenticationLoginInfo = new FormsAuthenticationLoginInfo(txtadmin, txtpass);

                }
                else
                {
                    context.Credentials = new NetworkCredential(txtadmin, txtpass, domain);
                }
                //
                // The SharePoint web at the URL.
                Web web = context.Web;

                // We want to retrieve the web's properties.
                context.Load(web);
                context.Load(web.Webs);
                // Execute the query to the server.
                context.ExecuteQuery();
                var subWeb = (from w in web.Webs where w.ServerRelativeUrl == ("/" + Url) select w).SingleOrDefault();
                if (subWeb == null)
                {
                    try
                    {
                        WebCreationInformation creation = new WebCreationInformation();
                        creation.Url = Url;
                        creation.Title = Title;
                        creation.WebTemplate = "CMSPUBLISHING#0";
                        creation.UseSamePermissionsAsParentSite = true;
                        creation.Language = (int)web.Language;
                        Web newWeb = context.Web.Webs.Add(creation);
                        context.ExecutingWebRequest += (object sender, WebRequestEventArgs e) =>
                        {
                            //e.WebRequestExecutor.WebRequest.Headers[HttpRequestHeader.Cookie] = "FedAuth=" + _fedAuth + ";rtFa=" + _rtFa;
                            WebResponse oresult = e.WebRequestExecutor.WebRequest.GetResponse();
                            using (var reader = new StreamReader(oresult.GetResponseStream()))
                            {
                                string result = reader.ReadToEnd(); // do something fun...
                            }
                        };
                        // Retrieve the new web information.
                        context.Load(newWeb, w => w.Title);
                        context.ExecuteQuery();

                    }
                    catch (Microsoft.SharePoint.Client.ClientRequestException e)
                    {

                    }
                    //context.
                    // if found true
                    //return true;
                }


            }
        }

        public static void TestCV()
        {
            DMBaoCaoDA oDMBaoCaoDA = new DMBaoCaoDA();
            DMBaoCaoItem oDMBaoCaoItem = oDMBaoCaoDA.GetByIdToObject<DMBaoCaoItem>(11);


            KyBaoCaoDA kyBaoCaoDA = new KyBaoCaoDA();
            KyBaoCaoItem kyBaoCaoItem = kyBaoCaoDA.GetByIdToObject<KyBaoCaoItem>(165);

            if (oDMBaoCaoItem.LoaiBaoCao == 2)
            {
                //bảo cáo dạng số liệu.
                //lấy shet của file đính kèm + và sheet tổng hợp.

                //đơn vị đầu tiên báo cáo.  nếu không có thì lấy từ mẫu còn ko thì lấy từ kỳ đánh giá root.
                Stream DataFileTH = oDMBaoCaoDA.SpListProcess.ParentWeb.GetFile(oDMBaoCaoItem.UrlBaoCaoTongHop).OpenBinaryStream();
                XSSFWorkbook hssfwbTongHop = new XSSFWorkbook(DataFileTH);
                ISheet sheet = hssfwbTongHop.GetSheetAt(0);

                if (kyBaoCaoItem.idRootKyDanhGia > 0)
                {
                    KyBaoCaoItem kyBaoCaoItemRoot = kyBaoCaoDA.GetByIdToObject<KyBaoCaoItem>(kyBaoCaoItem.idRootKyDanhGia);
                    if (kyBaoCaoItemRoot.ListFileAttach.Count > 0)
                    {
                        DataFileTH = oDMBaoCaoDA.SpListProcess.ParentWeb.GetFile(kyBaoCaoItemRoot.ListFileAttach[0].Url).OpenBinaryStream();
                        hssfwbTongHop = new XSSFWorkbook(DataFileTH);
                        sheet = hssfwbTongHop.GetSheetAt(0);
                    }

                }


                Stream DataFileMauTH = oDMBaoCaoDA.SpListProcess.ParentWeb.GetFile(oDMBaoCaoItem.UrlBaoCaoTongHop).OpenBinaryStream();
                XSSFWorkbook hssfwbMauTongHop = new XSSFWorkbook(DataFileMauTH);
                ISheet sheetMauTH = hssfwbMauTongHop.GetSheetAt(0);

                //oKyBaoCaoItem 
                Stream DataFileCT = kyBaoCaoDA.SpListProcess.ParentWeb.GetFile(kyBaoCaoItem.ListFileAttach[0].Url).OpenBinaryStream();
                XSSFWorkbook hssfwbCT = new XSSFWorkbook(DataFileCT);
                ISheet sheetCT = hssfwbCT.GetSheetAt(0);
                sheetCT.CopyTo(hssfwbTongHop, "VP EVN", true, true);
                int StartRowInsert = 0;

                if (oDMBaoCaoItem.PhanLoaiHinhThucTongHop == 1 && !string.IsNullOrEmpty(oDMBaoCaoItem.JsonConfig)) //loại insert dòng sang bên kia.
                {
                    ConfigExcel configExcel = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigExcel>(oDMBaoCaoItem.JsonConfig);
                    //lấy row bắt đầu inser của bảng tổng hợp đang có.
                    for (int i = configExcel.Type1_DongBatDau; i < sheet.LastRowNum; i++)
                    {
                        StartRowInsert = i;
                        IRow temp = sheet.GetRow(i);
                        if (temp == null)
                            break;
                        else
                        {
                            ICell tempcell = temp.GetCell(0);
                            if (tempcell == null)
                                break;
                            else
                            {
                                if (string.IsNullOrEmpty(tempcell.StringCellValue))
                                {
                                    break;
                                }
                            }
                        }
                    }
                    int RowChiTiet = 0;
                    //insert row từ sheet của đơn vị vào sheet TH
                    for (int i = configExcel.Type1_DongBatDau; i < sheetCT.LastRowNum; i++)
                    {
                        //bắt đầu tính từ row start, cho đến khi gặp row mà giá trị đầu tiên bị null thì bỏ
                        IRow temp = sheetCT.GetRow(i);
                        if (temp == null)
                            break;
                        else
                        {
                            ICell tempcell = temp.GetCell(0);
                            if (tempcell == null)
                                break;
                            else
                            {
                                if (clsFucUtils.GetCellValue(tempcell) == null)
                                {
                                    break;
                                }
                            }
                        }
                        RowChiTiet++;
                    }
                    //số lượng dòng phải chèn vào là bao nhiêu.
                    clsFucUtils.NPOIInsertRows(ref sheet, StartRowInsert + 1, RowChiTiet);
                    WriteToFile(hssfwbTongHop, "tonghop1.xlsx");
                    //
                    for (int i = 0; i < RowChiTiet; i++)
                    {
                        int j = i + configExcel.Type1_DongBatDau;
                        //nếu đủ đk thì chạy tiếp vào hàm insert
                        clsFucUtils.copyFromSourceToDestinationRow(hssfwbTongHop, sheetCT, j, sheet, StartRowInsert, configExcel);
                        StartRowInsert++;
                    }
                    WriteToFile(hssfwbTongHop, "tonghop.xlsx");
                }
                else if (oDMBaoCaoItem.PhanLoaiHinhThucTongHop == 0)
                {
                    for (int i = 0; i < sheetMauTH.LastRowNum; i++)
                    {
                        IRow cellall = sheetMauTH.GetRow(i);
                        for (int j = 0; j < cellall.LastCellNum; j++)
                        {
                            ICell cell = cellall.GetCell(j);
                            object CellValue = clsFucUtils.GetCellValue(cell);
                            if (CellValue != null)
                            {
                                string Function = CellValue.ToString();
                                switch (Function)
                                {
                                    case "SUMALLCELL":
                                    case "'SUMALLCELL":
                                        #region MyRegion
                                        try
                                        {
                                            double SUMALLCELL = 0;
                                            ICell CellTongHop = sheet.GetRow(i).GetCell(j);
                                            //check xem CellTongHop đã có giá trị hay chưa.
                                            if (CellTongHop.CellType == CellType.Numeric)
                                            {
                                                SUMALLCELL = CellTongHop.NumericCellValue;
                                            }

                                            ICell cellTemp = sheetCT.GetRow(i).GetCell(j);
                                            CellTongHop.SetCellValue(SUMALLCELL + cellTemp.NumericCellValue);
                                        }
                                        catch (Exception ex)
                                        {

                                        }
                                        break;
                                    #endregion
                                    case "AVGCALLCELL":
                                    case "'AVGCALLCELL":
                                        #region MyRegion
                                        try
                                        {
                                            double SUMALLCELL = 0;
                                            ICell CellTongHop = sheet.GetRow(i).GetCell(j);

                                            //check xem CellTongHop đã có giá trị hay chưa.
                                            if (CellTongHop.CellType == CellType.Numeric)
                                            {
                                                SUMALLCELL = CellTongHop.NumericCellValue;
                                                SUMALLCELL = (SUMALLCELL * (hssfwbTongHop.NumberOfSheets - 2));
                                            }

                                            ICell cellTemp = sheetCT.GetRow(i).GetCell(j);
                                            CellTongHop.SetCellValue((SUMALLCELL + cellTemp.NumericCellValue) / (hssfwbTongHop.NumberOfSheets - 1));
                                        }
                                        catch (Exception ex)
                                        {

                                        }
                                        break;
                                    #endregion
                                    case "MAXALLCELL":
                                    case "'MAXALLCELL":
                                        #region MyRegion
                                        try
                                        {
                                            double MAXALLCELL = 0;
                                            ICell CellTongHop = sheet.GetRow(i).GetCell(j);
                                            //check xem CellTongHop đã có giá trị hay chưa.
                                            if (CellTongHop.CellType == CellType.Numeric)
                                            {
                                                MAXALLCELL = CellTongHop.NumericCellValue;
                                            }
                                            ICell cellTemp = sheetCT.GetRow(i).GetCell(j);
                                            object ValueCell = clsFucUtils.GetCellValue(cellTemp);
                                            if (ValueCell != null)
                                            {
                                                if ((double)ValueCell > MAXALLCELL)
                                                {
                                                    //set giá trị cho bằng MaxValueDonVi
                                                    CellTongHop.SetCellValue((double)ValueCell);
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {

                                        }
                                        break;
                                    #endregion
                                    case "MINALLCELL":
                                    case "'MINALLCELL":
                                        #region MyRegion
                                        try
                                        {
                                            double MINALLCELL = 999999999;
                                            ICell CellTongHop = sheet.GetRow(i).GetCell(j);
                                            //check xem CellTongHop đã có giá trị hay chưa.
                                            if (CellTongHop.CellType == CellType.Numeric)
                                            {
                                                MINALLCELL = CellTongHop.NumericCellValue;
                                            }
                                            ICell cellTemp = sheetCT.GetRow(i).GetCell(j);
                                            object ValueCell = clsFucUtils.GetCellValue(cellTemp);
                                            if (ValueCell != null)
                                            {
                                                if ((double)ValueCell < MINALLCELL)
                                                {
                                                    //set giá trị cho bằng MaxValueDonVi
                                                    CellTongHop.SetCellValue((double)ValueCell);
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {

                                        }
                                        break;
                                        #endregion
                                }
                            }

                        }

                    }
                    WriteToFile(hssfwbTongHop, "tonghop.xlsx");
                }
                // code to create workbook 
                //using (var exportData = new MemoryStream())
                //{
                //    hssfwbTongHop.Write(exportData);
                //    //string saveAsFileName = string.Format("MembershipExport-{0:d}.xls", DateTime.Now).Replace("/", "-");

                //    byte[] bytes = exportData.ToArray();
                //    System.IO.File.WriteAllBytes(@"C:\Users\spadmin\Downloads\NhatKyMatDien2.xlsx", bytes);


                //    //kyBaoCaoItemRoot.ListFileAttachAdd.Add(new FileAttach() { Name = "BaoCaoTongHop.xlsx", DataFile = bytes });
                //    //SPListItem itembc = oKyBaoCaoDA.SpListProcess.GetItemById(kyBaoCaoItemRoot.ID);
                //    //try
                //    //{
                //    //    itembc.Attachments.RecycleNow("BaoCaoTongHop.xlsx");
                //    //}
                //    //catch (Exception ex)
                //    //{

                //    //}
                //    //itembc.Attachments.AddNow("BaoCaoTongHop.xlsx", bytes);
                //}
            }
        }

        public static void WriteToFile(XSSFWorkbook hssfwbTongHop, string name)
        {
            //Write the stream data of workbook to the root directory
            FileStream file = new FileStream(name, FileMode.Create);
            //ByteArrayOutputStream buffer = new ByteArrayOutputStream(); 
            hssfwbTongHop.Write(file);
            file.Close();
        }
        public static void AddField(string FieldName, string ListName, string siteUrl = "", string txtadmin = "spadmin", string txtpass = "evnhn@123")
        {
            string domain = "";
            bool formauthen = true;
            using (ClientContext context = new ClientContext($"https://portal.evnhanoi.vn{siteUrl}"))
            {
                if (formauthen)
                {
                    context.AuthenticationMode = ClientAuthenticationMode.FormsAuthentication;
                    context.FormsAuthenticationLoginInfo = new FormsAuthenticationLoginInfo(txtadmin, txtpass);

                }
                else
                {
                    context.Credentials = new NetworkCredential(txtadmin, txtpass, domain);
                }
                //
                // The SharePoint web at the URL.
                Web web = context.Web;

                // We want to retrieve the web's properties.
                context.Load(web);
                context.Load(context.Site.RootWeb);
                // Execute the query to the server.
                context.ExecuteQuery();

                Field ttduan = null;
                try
                {
                    List LUser = web.GetList($"{siteUrl}/Lists/" + ListName);
                    context.Load(LUser);

                    ttduan = context.Site.RootWeb.AvailableFields.GetByTitle(FieldName);
                    context.Load(ttduan);
                    context.ExecuteQuery();

                    context.ExecuteQuery();
                    LUser.Fields.Add(ttduan);
                    LUser.Update();
                    // Execute the query to the server.
                    context.ExecuteQuery();
                }
                catch (Exception ex)
                {

                }
            }
        }
        public static void SetAprrove(string ListName, string siteUrl = "", string txtadmin = "spadmin", string txtpass = "evnhn@123")
        {
            string domain = "";
            bool formauthen = true;
            using (ClientContext context = new ClientContext($"https://portal.evnhanoi.vn{siteUrl}"))
            {
                if (formauthen)
                {
                    context.AuthenticationMode = ClientAuthenticationMode.FormsAuthentication;
                    context.FormsAuthenticationLoginInfo = new FormsAuthenticationLoginInfo(txtadmin, txtpass);

                }
                else
                {
                    context.Credentials = new NetworkCredential(txtadmin, txtpass, domain);
                }
                //
                // The SharePoint web at the URL.
                Web web = context.Web;

                // We want to retrieve the web's properties.
                context.Load(web);
                context.Load(context.Site.RootWeb);
                // Execute the query to the server.
                context.ExecuteQuery();

                try
                {
                    List LUser = web.GetList($"{siteUrl}/Lists/" + ListName);
                    context.Load(LUser);

                    context.ExecuteQuery();
                    LUser.EnableModeration = true;
                    LUser.Update();

                    context.ExecuteQuery();
                }
                catch (Exception ex)
                {

                }
            }
        }


        public static void SetAprroveV2(string ListName, string siteUrl = "", string txtadmin = "spadmin", string txtpass = "evnhn@123")
        {
            string domain = "";
            bool formauthen = true;
            using (ClientContext context = new ClientContext($"https://portal.evnhanoi.vn{siteUrl}"))
            {
                if (formauthen)
                {
                    context.AuthenticationMode = ClientAuthenticationMode.FormsAuthentication;
                    context.FormsAuthenticationLoginInfo = new FormsAuthenticationLoginInfo(txtadmin, txtpass);

                }
                else
                {
                    context.Credentials = new NetworkCredential(txtadmin, txtpass, domain);
                }
                //
                // The SharePoint web at the URL.
                Web web = context.Web;

                // We want to retrieve the web's properties.
                context.Load(web);
                context.Load(context.Site.RootWeb);
                // Execute the query to the server.
                context.ExecuteQuery();

                try
                {
                    List LUser = web.GetList($"{siteUrl}/Lists/" + ListName);
                    context.Load(LUser);

                    context.ExecuteQuery();
                    LUser.EnableModeration = true;
                    LUser.DraftVersionVisibility = Microsoft.SharePoint.Client.DraftVisibilityType.Reader;
                    LUser.Update();

                    context.ExecuteQuery();
                }
                catch (Exception ex)
                {

                }
            }
        }


        public static void AddFieldAllSubWeb(string FieldName, string ListName, string siteUrl = "", string txtadmin = "spadmin", string txtpass = "evnhn@123")
        {
            string domain = "";
            bool formauthen = true;
            using (ClientContext context = new ClientContext($"https://portal.evnhanoi.vn"))
            {
                if (formauthen)
                {
                    context.AuthenticationMode = ClientAuthenticationMode.FormsAuthentication;
                    context.FormsAuthenticationLoginInfo = new FormsAuthenticationLoginInfo(txtadmin, txtpass);

                }
                else
                {
                    context.Credentials = new NetworkCredential(txtadmin, txtpass, domain);
                }
                //
                // The SharePoint web at the URL.
                Web web = context.Web;
                // We want to retrieve the web's properties.

                context.Load(web.Webs);
                context.Load(context.Site.RootWeb);
                // Execute the query to the server.
                context.ExecuteQuery();

                Field ttduan = null;
                ttduan = context.Site.RootWeb.AvailableFields.GetByTitle(FieldName);
                context.Load(ttduan);
                context.ExecuteQuery();
                try
                {
                    foreach (var itemsubweb in web.Webs)
                    {
                        try
                        {
                            Console.WriteLine(itemsubweb.ServerRelativeUrl);
                            Web tempweb = context.Site.OpenWeb($"{itemsubweb.ServerRelativeUrl}{siteUrl}");
                            //Web itemwebcms = context.Site.OpenWeb(itemweb.ServerRelativeUrl + "/cms");
                            context.Load(tempweb);
                            context.ExecuteQuery();
                            List LUser = tempweb.GetList($"{tempweb.ServerRelativeUrl}/Lists/" + ListName);
                            context.Load(LUser);
                            context.Load(LUser.Fields);
                            context.ExecuteQuery();
                            if (LUser.Fields.FirstOrDefault(x => x.StaticName == FieldName) == null)
                            {
                                //Console.WriteLine(LUser.DefaultViewUrl);
                                LUser.Fields.Add(ttduan);
                                LUser.Update();
                                // Execute the query to the server.
                                context.ExecuteQuery();

                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }

                }
                catch (Exception ex)
                {

                }
            }
        }

        public static void ConvertLUser()
        {
            ClientContext clientContext = new ClientContext("https://portal.evnhanoi.vn/");
            clientContext.AuthenticationMode = ClientAuthenticationMode.FormsAuthentication;
            clientContext.FormsAuthenticationLoginInfo = new FormsAuthenticationLoginInfo("spadmin", "evnhn@123");
            List oList = clientContext.Web.Lists.GetByTitle("LGroup");
            List LUser = clientContext.Web.Lists.GetByTitle("LUser");
            clientContext.Load(oList);
            clientContext.Load(LUser);
            clientContext.ExecuteQuery();
            LGroupDA lGroupDA = new LGroupDA();
            LUserDA lUserDA = new LUserDA();
            //http://42.112.213.225:8074/api/get_HRMS_ThongTinPhongBan
            HttpClient client = new HttpClient();
            //var byteArray = Encoding.ASCII.GetBytes("username:password1234");
            client.DefaultRequestHeaders.Add("Authorization", "Basic RVZOSEFOT0k6RVZOSEFOT0lAQ3VvbmdYMTY=");

            HttpResponseMessage response = client.GetAsync($"http://42.112.213.225:8074/api/get_HRMS_ThongTinNhanSu_KhongAnh?DonVi_ID=-1&Thang=6&Nam=2022").Result;

            string content = response.Content.ReadAsStringAsync().Result;

            List<LUserEVN> lstNguoiDung = new List<LUserEVN>();
            lstNguoiDung = Newtonsoft.Json.JsonConvert.DeserializeObject<List<LUserEVN>>(content);

            foreach (LUserEVN oLUserEVN in lstNguoiDung)
            {
                Console.WriteLine(oLUserEVN.fullName);

                //int IDGroup = lUserDA.CheckExit(0, oLUserEVN.staffCode, "OldID");
                int IDGroup = CheckExitField(LUser, clientContext, oLUserEVN.staffCode, "staffCode");
                if (IDGroup > 0)
                {
                    if (false)
                    {
                        LUserItem lUserItem = new LUserItem()
                        {
                            staffCode = oLUserEVN.staffCode,
                            HRMSID = oLUserEVN.ns_id,
                            Title = oLUserEVN.fullName,
                            UserEmail = oLUserEVN.email,
                            UserSDT = oLUserEVN.phone,
                            UserChucVu = oLUserEVN.positionName
                        };
                        if (!string.IsNullOrEmpty(oLUserEVN.yearOfBirth))
                        {
                            lUserItem.UserNgaySinh = DateTime.ParseExact(oLUserEVN.yearOfBirth, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        }
                        if (!string.IsNullOrEmpty(oLUserEVN.deptId))
                        {

                            //int UserPhongBan = lGroupDA.CheckExit(0, oLUserEVN.deptId, "OldID");
                            int UserPhongBan = CheckExitField(oList, clientContext, oLUserEVN.deptId, "OldID");
                            if (UserPhongBan > 0)
                            {
                                try
                                {
                                    lUserItem.UserPhongBan = new SPFieldLookupValue(UserPhongBan, "");
                                    int idparent = UserPhongBan;
                                    while (idparent > 0)
                                    {
                                        //LGroupItem temp = lGroupDA.GetByIdToObject<LGroupItem>(idparent);
                                        ListItem temp = oList.GetItemById(idparent);
                                        clientContext.Load(temp);
                                        clientContext.ExecuteQuery();
                                        if (temp["GroupIsDonVi"] != null && Convert.ToBoolean(temp["GroupIsDonVi"]))
                                        {
                                            idparent = temp.Id;
                                            break;
                                        }
                                        else
                                        {
                                            idparent = ((FieldLookupValue)temp["GroupParent"]).LookupId;
                                        }
                                    }
                                    if (idparent > 0)
                                    {
                                        lUserItem.Groups = new SPFieldLookupValue(idparent, "");
                                    }
                                }
                                catch (Exception ex)
                                {

                                }
                            }
                            if (lUserItem.Groups.LookupId > 0)
                            {
                                //lUserDA.UpdateObject<LUserItem>(lUserItem);
                                //ListItemCreationInformation itemCreateInfo = new ListItemCreationInformation();
                                //ListItem oListItem = LUser.AddItem(itemCreateInfo);
                                ListItem oListItem = LUser.GetItemById(IDGroup);
                                clientContext.Load(oListItem);
                                clientContext.ExecuteQuery();

                                //ListItem oListItem = oList.AddItem(itemCreateInfo);
                                oListItem["staffCode"] = oLUserEVN.staffCode;
                                oListItem["HRMSID"] = oLUserEVN.ns_id;
                                if (lUserItem.UserNgaySinh.HasValue)
                                    oListItem["UserNgaySinh"] = lUserItem.UserNgaySinh.Value;
                                oListItem["Title"] = lUserItem.Title;
                                oListItem["UserPhongBan"] = lUserItem.UserPhongBan.LookupId;
                                oListItem["Groups"] = lUserItem.Groups.LookupId;
                                oListItem["UserEmail"] = lUserItem.UserEmail;
                                oListItem["UserSDT"] = lUserItem.UserSDT;
                                oListItem["UserChucVu"] = lUserItem.UserChucVu;
                                oListItem["_ModerationStatus"] = 0;
                                oListItem.Update();
                                clientContext.ExecuteQuery();
                            }

                        }
                    }

                }
                else
                {
                    LUserItem lUserItem = new LUserItem()
                    {
                        staffCode = oLUserEVN.staffCode,
                        HRMSID = oLUserEVN.ns_id,
                        Title = oLUserEVN.fullName,
                        UserEmail = oLUserEVN.email,
                        UserSDT = oLUserEVN.phone,
                        UserChucVu = oLUserEVN.positionName
                    };
                    if (!string.IsNullOrEmpty(oLUserEVN.yearOfBirth))
                    {
                        lUserItem.UserNgaySinh = DateTime.ParseExact(oLUserEVN.yearOfBirth, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    }
                    if (!string.IsNullOrEmpty(oLUserEVN.deptId))
                    {

                        //int UserPhongBan = lGroupDA.CheckExit(0, oLUserEVN.deptId, "OldID");
                        int UserPhongBan = CheckExitField(oList, clientContext, oLUserEVN.deptId, "OldID");
                        if (UserPhongBan > 0)
                        {
                            try
                            {
                                lUserItem.UserPhongBan = new SPFieldLookupValue(UserPhongBan, "");
                                int idparent = UserPhongBan;
                                while (idparent > 0)
                                {
                                    //LGroupItem temp = lGroupDA.GetByIdToObject<LGroupItem>(idparent);
                                    ListItem temp = oList.GetItemById(idparent);
                                    clientContext.Load(temp);
                                    clientContext.ExecuteQuery();
                                    if (temp["GroupIsDonVi"] != null && Convert.ToBoolean(temp["GroupIsDonVi"]))
                                    {
                                        idparent = temp.Id;
                                        break;
                                    }
                                    else
                                    {
                                        idparent = ((FieldLookupValue)temp["GroupParent"]).LookupId;
                                    }
                                }
                                if (idparent > 0)
                                {
                                    lUserItem.Groups = new SPFieldLookupValue(idparent, "");
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                        if (lUserItem.Groups.LookupId > 0)
                        {
                            //lUserDA.UpdateObject<LUserItem>(lUserItem);
                            ListItemCreationInformation itemCreateInfo = new ListItemCreationInformation();
                            ListItem oListItem = LUser.AddItem(itemCreateInfo);
                            //ListItem oListItem = LUser.GetItemById(IDGroup);
                            //clientContext.Load(oListItem);
                            //clientContext.ExecuteQuery();

                            //ListItem oListItem = oList.AddItem(itemCreateInfo);
                            oListItem["staffCode"] = oLUserEVN.staffCode;
                            oListItem["HRMSID"] = oLUserEVN.ns_id;
                            if (lUserItem.UserNgaySinh.HasValue)
                                oListItem["UserNgaySinh"] = lUserItem.UserNgaySinh.Value;
                            oListItem["Title"] = lUserItem.Title;
                            oListItem["UserPhongBan"] = lUserItem.UserPhongBan.LookupId;
                            oListItem["Groups"] = lUserItem.Groups.LookupId;
                            oListItem["UserEmail"] = lUserItem.UserEmail;
                            oListItem["UserSDT"] = lUserItem.UserSDT;
                            oListItem["UserChucVu"] = lUserItem.UserChucVu;
                            oListItem["_ModerationStatus"] = 0;
                            oListItem.Update();
                            clientContext.ExecuteQuery();
                            if (oListItem.Id > 0)
                            {
                                //https://portal.evnhanoi.vn/VIPortalAPI/api/Mobi/UpdateAnhDaiDien
                                HttpClient clientGetway = new HttpClient();
                                clientGetway.DefaultRequestHeaders.Add("Authorization1", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImhvYW5udCIsIm5iZiI6MTY1NDQ5NzQwOCwiZXhwIjoxNjU0NTE5MDA4LCJpYXQiOjE2NTQ0OTc0MDh9.ANfq2MmfkJZw2XxPfPCf3paCcy20rpFQKiNrUbGzq4s");
                                HttpResponseMessage responseALL = clientGetway.PostAsync("https://portal.evnhanoi.vn/VIPortalAPI/api/Mobi/UpdateAnhDaiDien", new StringContent(
                                 Newtonsoft.Json.JsonConvert.SerializeObject(new
                                 {
                                     ItemID = oListItem.Id,
                                     HRMSID = oLUserEVN.ns_id
                                 }), Encoding.UTF8, "application/json")).Result;
                                string responseBodyALL = responseALL.Content.ReadAsStringAsync().Result;
                            }
                        }

                    }

                }
            }

        }



        public static void BackupSite()
        {
            // Get a reference to the Web application publishing
            // Web service.
            SPFarm myFarm = SPFarm.Local;
            SPServiceCollection myServices = myFarm.Services;
            foreach (SPService service in myServices)
            {
                if (service is SPWebService)
                {
                    Console.WriteLine("Web service name:" + service.Name);
                    Console.WriteLine("Web service type:" + service.TypeName);
                    Console.WriteLine("Web service ID:" + service.Id);
                    Console.WriteLine();
                }
            }
            Guid serviceID = new Guid("48ad8ffb-ffcd-4bdd-98f9-663212a868da");
            SPWebService webPubService = (SPWebService)myServices[serviceID];

            // Get a reference to the Web application that hosts the 
            // site collection.
            SPWebApplicationCollection myApps = webPubService.WebApplications;


            foreach (SPWebApplication app in myApps)
            {
                Console.WriteLine("Web application name:" + app.Name);
                Console.WriteLine("Web application type:" + app.TypeName);
                Console.WriteLine("Web application ID:" + app.Id);
                Console.WriteLine();
            }

            Guid appID = new Guid("a915754a-b1f5-4295-b495-f752e20f06d3");
            SPWebApplication myApp = myApps[appID];

            // As alternative to the preceding three lines, you can use
            // the following when you know the URL of the Web application:
            //     SPWebApplication myApp = SPWebApplication.Lookup(url_of_Web_app)

            // Get a reference to the Web application's collection of 
            // site collections. 
            SPSiteCollection mySiteCols = myApp.Sites;

            // Back up a specified site collection. 
            mySiteCols.Backup(@"http://localhost:6003", @"\\10.10.20.29\c$\SETUP", true);


        }
        public static void COpyList()
        {
            using (SPSite oSite = new SPSite("http://localhost:6003"))
            {
                using (SPWeb owebRoot = oSite.OpenWeb())
                {
                    SPFieldCollection allsitecolumn = owebRoot.AvailableFields;
                    using (SPWeb oweb = oSite.OpenWeb("/cms"))
                    {
                        using (SPWeb owebgialam = oSite.OpenWeb("/gialam/cms"))
                        {

                            SPListCollection alllist = oweb.Lists;
                            foreach (SPList olistsubGoc in alllist)
                            {
                                if (olistsubGoc.BaseTemplate == SPListTemplateType.GenericList)
                                {
                                    Console.WriteLine(olistsubGoc.Title + "==" + olistsubGoc.BaseTemplate);
                                    try
                                    {
                                        SPList olistsub = null;
                                        try
                                        {
                                            olistsub = owebgialam.GetList("/gialam/cms/Lists/" + olistsubGoc.RootFolder.Name);
                                        }
                                        catch
                                        {
                                            Guid oGuid = owebgialam.Lists.Add(olistsubGoc.RootFolder.Name, olistsubGoc.Description, SPListTemplateType.GenericList);
                                            olistsub = owebgialam.GetList("/gialam/cms/Lists/" + olistsubGoc.RootFolder.Name);
                                        }
                                        foreach (SPField oField in olistsubGoc.Fields)
                                        {
                                            if (!oField.ReadOnlyField && oField.CanBeDisplayedInEditForm)
                                            {
                                                Console.WriteLine("==" + oField.InternalName);
                                                if (!olistsub.Fields.ContainsFieldWithStaticName(oField.StaticName))
                                                {

                                                    if (allsitecolumn.ContainsFieldWithStaticName(oField.StaticName))
                                                    {
                                                        SPField newField = allsitecolumn.TryGetFieldByStaticName(oField.StaticName);
                                                        if (newField.TypeAsString == oField.TypeAsString)
                                                        {
                                                            olistsub.Fields.Add(newField);
                                                            olistsub.Update();
                                                        }
                                                    }
                                                    else
                                                    {
                                                        #region MyRegion
                                                        Console.WriteLine(oField.StaticName + "==" + oField.TypeAsString);

                                                        string fldName;
                                                        switch (oField.TypeAsString)
                                                        {
                                                            case "Text": //single text
                                                                olistsub.Fields.Add(oField.StaticName, SPFieldType.Text, false);
                                                                break;
                                                            case "Note": //Multiline Text
                                                                SPFieldMultiLineText temp = (SPFieldMultiLineText)oField;
                                                                olistsub.Fields.Add(oField.StaticName, SPFieldType.Note, false);
                                                                SPFieldMultiLineText mul = new SPFieldMultiLineText(olistsub.Fields, oField.StaticName);
                                                                mul.RichText = temp.RichText;
                                                                //mul.RichTextMode = SPRichTextMode.Compatible;
                                                                mul.Update();
                                                                break;
                                                            case "HTML":
                                                                olistsub.Fields.Add(oField.StaticName, SPFieldType.Note, false);
                                                                SPFieldMultiLineText mulhtml = new SPFieldMultiLineText(olistsub.Fields, oField.StaticName);
                                                                mulhtml.RichText = true;
                                                                //mul.RichTextMode = SPRichTextMode.Compatible;
                                                                mulhtml.Update();
                                                                break;
                                                            case "Number":
                                                                fldName = olistsub.Fields.Add(oField.StaticName, SPFieldType.Number, false);
                                                                SPFieldNumber fnumber = new SPFieldNumber(olistsub.Fields, fldName);
                                                                fnumber.Description = oField.Description;
                                                                fnumber.Update();
                                                                break;
                                                            case "Lookup":
                                                            case "LookupMulti":
                                                                try
                                                                {
                                                                    SPFieldLookup templk = (SPFieldLookup)oField;
                                                                    Console.WriteLine(templk.LookupList);
                                                                    SPList olistlk = oweb.Lists.GetList(new Guid(templk.LookupList), false);
                                                                    Console.WriteLine(olistlk.RootFolder.ServerRelativeUrl);
                                                                    try
                                                                    {
                                                                        string urllistLookup = olistlk.RootFolder.ServerRelativeUrl.Split('/').LastOrDefault();

                                                                        SPList newList = owebgialam.Lists.TryGetList(urllistLookup);
                                                                        if (newList != null)
                                                                        {
                                                                            var userNameDn = olistsub.Fields.AddLookup(templk.StaticName, newList.ID, false);
                                                                            SPFieldLookup usernameField = (SPFieldLookup)olistsub.Fields[userNameDn];
                                                                            usernameField.LookupField = newList.Fields[templk.LookupField].StaticName;
                                                                            usernameField.AllowMultipleValues = templk.AllowMultipleValues;
                                                                            usernameField.Update();
                                                                        }
                                                                    }
                                                                    catch (Exception ex)
                                                                    {
                                                                        //file.WriteLine(string.Format("Field: {0}, Eror: {1}", item.StaticName, ex.Message));
                                                                        Console.WriteLine(string.Format("Field: {0}, Eror: {1}", oField.StaticName, ex.Message));
                                                                    }
                                                                }
                                                                catch (Exception ex)
                                                                {

                                                                }
                                                                break;
                                                            case "Boolean":
                                                                fldName = olistsub.Fields.Add(oField.StaticName, SPFieldType.Boolean, false);
                                                                SPFieldBoolean bol = new SPFieldBoolean(olistsub.Fields, fldName);
                                                                bol.DefaultValue = oField.DefaultValue;
                                                                bol.Update();
                                                                break;
                                                            case "DateTime":
                                                                fldName = olistsub.Fields.Add(oField.StaticName, SPFieldType.DateTime, false);
                                                                SPFieldDateTime tem3 = (SPFieldDateTime)oField;
                                                                if (string.IsNullOrEmpty(oField.DefaultValue))
                                                                {
                                                                    SPFieldDateTime fieldDateTime = new SPFieldDateTime(olistsub.Fields, fldName);
                                                                    if (tem3.DisplayFormat == SPDateTimeFieldFormatType.DateOnly)
                                                                        fieldDateTime.DisplayFormat = SPDateTimeFieldFormatType.DateOnly;
                                                                    else fieldDateTime.DisplayFormat = SPDateTimeFieldFormatType.DateTime;
                                                                    fieldDateTime.DefaultValue = tem3.DefaultValue;
                                                                    fieldDateTime.Update();
                                                                }
                                                                break;

                                                        }
                                                        #endregion
                                                    }

                                                }

                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                }
                            }
                        }
                    }
                }

            }
        }

        public static void ConvertPhongBan()
        {
            ClientContext clientContext = new ClientContext("https://portal.evnhanoi.vn/");
            clientContext.AuthenticationMode = ClientAuthenticationMode.FormsAuthentication;
            clientContext.FormsAuthenticationLoginInfo = new FormsAuthenticationLoginInfo("spadmin", "evnhn@123");
            List oList = clientContext.Web.Lists.GetByTitle("LGroup");

            //http://42.112.213.225:8074/api/get_HRMS_ThongTinPhongBan
            HttpClient client = new HttpClient();
            //var byteArray = Encoding.ASCII.GetBytes("username:password1234");
            client.DefaultRequestHeaders.Add("Authorization", "Basic RVZOSEFOT0k6RVZOSEFOT0lAQ3VvbmdYMTY=");

            HttpResponseMessage response = client.GetAsync("http://42.112.213.225:8074/api/get_HRMS_ThongTinPhongBan_Portal").Result;

            string content = response.Content.ReadAsStringAsync().Result;

            List<LPhongBan> lstDonvi = new List<LPhongBan>();
            lstDonvi = Newtonsoft.Json.JsonConvert.DeserializeObject<List<LPhongBan>>(content);
            LGroupDA lGroupDA = new LGroupDA();
            foreach (LPhongBan oLDonVi in lstDonvi)
            {
                Console.WriteLine(oLDonVi.name);
                //if (oLDonVi.deptId == "281000000000941")
                {
                    //int IDGroup = lGroupDA.CheckExit(0, oLDonVi.deptId, "OldID");
                    int IDGroup = CheckExitField(oList, clientContext, oLDonVi.deptId, "OldID");
                    if (IDGroup > 0)
                    {
                        if (!string.IsNullOrEmpty(oLDonVi.DEPT_PARENT_ID))
                        {
                            int IDGroupParent = CheckExitField(oList, clientContext, oLDonVi.DEPT_PARENT_ID, "OldID");
                            if (IDGroupParent > 0)
                            {
                                ListItem oListItem = oList.GetItemById(IDGroup);
                                clientContext.Load(oListItem);
                                oListItem["GroupIsDonVi"] = false;
                                oListItem["UrlSite"] = "";
                                oListItem["GroupParent"] = IDGroupParent;
                                //oListItem["DMSTT"] = oLDonVi.STT;
                                oListItem.Update();
                                clientContext.ExecuteQuery();
                            }
                        }

                        //LGroupItem lGroupItem = new LGroupItem()
                        //{
                        //    OldID = oLDonVi.deptId,
                        //    Title = oLDonVi.name,
                        //    GroupIsDonVi = false
                        //};
                        ////int IDGroupParent = lGroupDA.CheckExit(0, oLDonVi.orgId, "OldID");
                        //int IDGroupParent = CheckExitField(oList, clientContext, oLDonVi.orgId, "OldID");
                        //if (IDGroupParent != 0)
                        //{
                        //    lGroupItem.GroupParent = new SPFieldLookupValue(IDGroupParent, "");
                        //    if (IDGroupParent == 1 && lGroupItem.OldID != "276000000000018")
                        //    {
                        //        lGroupItem.GroupIsDonVi = true;
                        //        lGroupItem.UrlSite = lGroupItem.Title.RemoveVietnameseTone().Replace(" ", "");
                        //    }
                        //    ListItemCreationInformation itemCreateInfo = new ListItemCreationInformation();
                        //    ListItem oListItem = oList.GetItemById(IDGroup);
                        //    clientContext.Load(oListItem);
                        //    clientContext.ExecuteQuery();
                        //    //ListItem oListItem = oList.AddItem(itemCreateInfo);
                        //    oListItem["OldID"] = lGroupItem.OldID;
                        //    oListItem["Title"] = lGroupItem.Title;
                        //    oListItem["GroupParent"] = lGroupItem.GroupParent.LookupId;
                        //    oListItem["MaDinhDanh"] = lGroupItem.MaDinhDanh;
                        //    oListItem["orgCode"] = lGroupItem.orgCode;
                        //    oListItem["GroupIsDonVi"] = lGroupItem.GroupIsDonVi;
                        //    oListItem["UrlSite"] = lGroupItem.UrlSite;
                        //    oListItem.Update();
                        //    clientContext.ExecuteQuery();
                        //    //lGroupDA.UpdateObject<LGroupItem>(lGroupItem);
                        //}
                    }
                    else
                    {

                    }
                }
            }
        }
        public static int CheckExitField(List lstDM, ClientContext context, string _title, string FieldName = "Title")
        {
            CamlQuery oCaml = new CamlQuery();
            oCaml.ViewXml = "<View> " +
                   "<Query><Where>" +
                   "<Eq><FieldRef Name='" + FieldName + "'/><Value Type='Text'>" + _title + "</Value></Eq>" +
                   "</Where></Query><RowLimit>0</RowLimit></View>";

            ListItemCollection allitems = lstDM.GetItems(oCaml);
            context.Load(allitems);
            context.ExecuteQuery();

            if (allitems.Count > 0)
            {
                return allitems[0].Id;
            }
            else return 0;
        }
        public static void ConvertDonVi()
        {
            ClientContext clientContext = new ClientContext("https://portal.evnhanoi.vn/");
            clientContext.AuthenticationMode = ClientAuthenticationMode.FormsAuthentication;
            clientContext.FormsAuthenticationLoginInfo = new FormsAuthenticationLoginInfo("spadmin", "evnhn@123");
            List oList = clientContext.Web.Lists.GetByTitle("LGroup");

            //http://42.112.213.225:8074/api/get_HRMS_ThongTinToChuc
            // ... Use HttpClient.            
            HttpClient client = new HttpClient();

            //var byteArray = Encoding.ASCII.GetBytes("username:password1234");
            client.DefaultRequestHeaders.Add("Authorization", "Basic RVZOSEFOT0k6RVZOSEFOT0lAQ3VvbmdYMTY=");

            HttpResponseMessage response = client.GetAsync("http://42.112.213.225:8074/api/get_HRMS_ThongTinToChuc").Result;

            string content = response.Content.ReadAsStringAsync().Result;

            List<LDonVi> lstDonvi = new List<LDonVi>();
            lstDonvi = Newtonsoft.Json.JsonConvert.DeserializeObject<List<LDonVi>>(content);
            LGroupDA lGroupDA = new LGroupDA();
            foreach (LDonVi oLDonVi in lstDonvi)
            {
                int IDGroup = CheckExitField(oList, clientContext, oLDonVi.orgId, "OldID");

                //int IDGroup = lGroupDA.CheckExit(0, oLDonVi.orgId, "OldID");


                LGroupItem lGroupItem = new LGroupItem()
                {
                    OldID = oLDonVi.orgId,
                    Title = oLDonVi.orgName,
                    MaDinhDanh = oLDonVi.shortName,
                    orgCode = oLDonVi.orgCode,
                    GroupIsDonVi = true,
                    UrlSite = "/" + oLDonVi.shortName.RemoveVietnameseTone()
                };
                lGroupItem.ID = IDGroup;
                if (IDGroup != 0)
                {
                    if (IDGroup != 1)
                    {
                        ListItemCreationInformation itemCreateInfo = new ListItemCreationInformation();
                        ListItem oListItem = oList.GetItemById(IDGroup);
                        clientContext.Load(oListItem);
                        clientContext.ExecuteQuery();
                        oListItem["OldID"] = lGroupItem.OldID;
                        oListItem["GroupParent"] = 1;
                        oListItem["Title"] = lGroupItem.Title;
                        oListItem["MaDinhDanh"] = lGroupItem.MaDinhDanh;
                        oListItem["orgCode"] = lGroupItem.orgCode;
                        oListItem["GroupIsDonVi"] = lGroupItem.GroupIsDonVi;
                        oListItem["UrlSite"] = lGroupItem.UrlSite;
                        oListItem.Update();
                        clientContext.ExecuteQuery();
                    }
                }
                //lGroupDA.UpdateObject<LGroupItem>(lGroupItem);
            }
        }
    }
    public class LDonVi
    {
        public string orgId { get; set; }
        public string orgCode { get; set; }
        public string orgName { get; set; }
        public string parentId { get; set; }
        public string shortName { get; set; }
        public string address { get; set; }
        public int status { get; set; }
    }
    //{
    //    "deptId": "276000000002492",
    //    "DEPT_PARENT_ID": "",
    //    "DEPT_ROOT_ID": "276000000002492",
    //    "DEPT_LEVEL": "1",
    //    "STT": "1",
    //    "orgId": "276",
    //    "name": "Hội đồng thành viên",
    //    "status": "1",
    //    "shortName": "HÐTV"
    //},
    public class LPhongBan
    {
        public string deptId { get; set; }
        public string DEPT_PARENT_ID { get; set; }
        public int STT { get; set; }
        public string orgId { get; set; }
        public string name { get; set; }
        public string status { get; set; }
        public string shortName { get; set; }
    }
    public class LUserEVN
    {
        public string fullName { get; set; }
        public string positionId { get; set; }
        public string ns_id { get; set; }
        public string positionName { get; set; }
        public string deptId { get; set; }
        public string staffCode { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string yearOfBirth { get; set; }
    }
}
