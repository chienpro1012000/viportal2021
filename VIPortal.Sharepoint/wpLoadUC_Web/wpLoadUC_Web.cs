﻿using System;
using System.ComponentModel;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using VIPORTAL.oWeb;

namespace VIPortal.Sharepoint
{
    [ToolboxItemAttribute(false)]
    public class wpLoadUC_Web : WebPart
    {

        #region Các thuộc tính của webpart
        /// <summary>
        /// Tiêu đề của webpart
        /// </summary>
        [Browsable(true),
        WebDisplayName("Địa chỉ vật lý userControls"),
        WebBrowsable(true),
        Category("Cấu hình webpart"),
        Personalizable(PersonalizationScope.Shared)]
        public string UrlControls { get; set; }
        [Browsable(true),
       WebDisplayName("Đường dẫn List"),
       WebBrowsable(true),
       Category("Cấu hình webpart"),
       Personalizable(PersonalizationScope.Shared)]
        public string UrlList { get; set; }
        [Browsable(true),
       WebDisplayName("Đường dẫn"),
       WebBrowsable(true),
       Category("Cấu hình webpart"),
       Personalizable(PersonalizationScope.Shared)]
        public string UrlDeTail { get; set; }

        [Browsable(true),
       WebDisplayName("Title webpart"),
       WebBrowsable(true),
       Category("Cấu hình webpart"),
       Personalizable(PersonalizationScope.Shared)]
        public string TitleWebpart { get; set; }
        [Browsable(true),
       WebDisplayName("Tùy biến cấu hình"),
       WebBrowsable(true),
       Category("Cấu hình webpart"),
       Personalizable(PersonalizationScope.Shared)]
        public string Config { get; set; }
        [Browsable(true),
       WebDisplayName("Số bản nghi hiển thị"),
       WebBrowsable(true),
       Category("Cấu hình webpart"),
       Personalizable(PersonalizationScope.Shared)]
        public int RowsLimit { get; set; }

        [Browsable(true),
        WebDisplayName("Không yêu cầu đăng nhập"),
        WebBrowsable(true),
        Category("Cấu hình webpart"),
        Personalizable(PersonalizationScope.Shared)]
        public bool isNotLogin { get; set; }
        #endregion

        /// <summary>
        /// Hàm load controls
        /// </summary>
        protected override void CreateChildControls()
        {
            if (!string.IsNullOrEmpty(UrlControls))
            {
                try
                {
                    BaseUC_Web control = (BaseUC_Web)Page.LoadControl(UrlControls);
                    control.UrlList = this.UrlList;
                    control.isNotLogin = this.isNotLogin;
                    control.UrlControls = this.UrlControls;
                    control.TitleWebpart = this.TitleWebpart;
                    control.RowsLimit = this.RowsLimit;
                    control.Config = this.Config;
                    this.ChromeType = PartChromeType.None;
                    this.Page.Response.Write(string.Format("<!--\r\n{0}\r\n-->", UrlControls));
                    Controls.Add(control);

                }
                catch (Exception ex)
                {
                    Page.Response.Write(string.Format("{0}", ex.Message));
                }
            }
            else
            {
                string path = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
                System.IO.FileInfo info = new System.IO.FileInfo(path);
                string NameFile = Path.GetFileNameWithoutExtension(path);
                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings[NameFile]))
                {
                    //UC_Quantri.ascx
                    string _ascxPath = System.Configuration.ConfigurationManager.AppSettings[NameFile];
                    //info.Name; 
                    Control control = Page.LoadControl(@"~/UserControls/" + _ascxPath);
                    Page.Response.Write(string.Format("<!--\r\n{0}\r\n-->", @"~/UserControls/" + _ascxPath));
                    this.ChromeType = PartChromeType.None;
                    Controls.Add(control);
                }
                else
                {
                    Page.Response.Write(string.Format("{0}", "Cấu hình webpart và add controls."));
                }
            }
        }
    }
}
