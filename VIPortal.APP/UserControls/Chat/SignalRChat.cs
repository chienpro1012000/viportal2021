﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Owin;
using Microsoft.Owin;


[assembly: OwinStartup(typeof(VIPortal.APP.SignalRChat))]

namespace VIPortal.APP
{
    public class SignalRChat
    {
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
            app.MapSignalR();
            //GlobalHost.DependencyResolver.Register(typeof(IAssemblyLocator), () => new DefaultAssemblyLocator());

        }
    }
}
