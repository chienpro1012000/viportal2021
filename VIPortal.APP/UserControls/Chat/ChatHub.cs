﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using ViPortal_Utils;
using ViPortal_Utils.Base;
using VIPortalAPP;
using ViPortalData;

namespace VIPortal.APP.UserControls.Chat
{
    public class ChatHub : Hub
    {
        public void Hello()
        {
            Clients.All.hello();
        }
        /// <summary>
        /// name = userid
        /// </summary>
        /// <param name="name"></param>
        /// <param name="message"></param>
        public void Send(string userid, string message)
        {
            ChatMessageItem oChatMessage = new ChatMessageItem();
            ChatHopThoaiItem oChatHopThoaiItem = new ChatHopThoaiItem();
            switch (userid)
            {
                case "sendfile":
                    oChatMessage = Newtonsoft.Json.JsonConvert.DeserializeObject<ChatMessageItem>(message, new SPFieldLookupValueConverter());
                    foreach (var item in oChatMessage.ListFileAttachAdd)
                        item.DataFile = clsFucUtils.ReadFile(HttpContext.Current.Server.MapPath(item.Url));
                    if (oChatMessage.UserNhan_Id.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Length == 1)
                    {
                        string _userid = oChatMessage.UserNhan_Id.Replace(",", "");
                        if (!string.IsNullOrEmpty(_userid))
                        {
                            SendMessageChatCaNhan(Convert.ToInt32(_userid), oChatMessage);
                        }
                    }
                    else
                    {
                        SendMessageChatGroup(oChatMessage);
                    }
                    break;
                case "sendgroup":
                    oChatMessage = Newtonsoft.Json.JsonConvert.DeserializeObject<ChatMessageItem>(message, new SPFieldLookupValueConverter());
                    SendMessageChatGroup(oChatMessage);
                    break;
                default:
                    oChatMessage = Newtonsoft.Json.JsonConvert.DeserializeObject<ChatMessageItem>(message, new SPFieldLookupValueConverter());
                    SendMessageChatCaNhan(Convert.ToInt32(userid), oChatMessage);
                    break;
            }
        }
        /// <summary>
        /// hàm gửi theo nhóm.
        /// </summary>
        /// <param name="oChatMessage"></param>
        private void SendMessageChatGroup(ChatMessageItem oChatMessage)
        {
            LUserDA oLUserDA = new LUserDA();
            ChatMessageDA oChatMessageDA = new ChatMessageDA();
            if (!string.IsNullOrEmpty(oChatMessage.UserNhan_Id))
            {
                List<int> lstIDuser = clsFucUtils.GetDanhSachIDsQuaFormPost(oChatMessage.UserNhan_Id);
                List<LUserJson> lstUser = oLUserDA.GetListJson(new LUserQuery() { lstIDget = lstIDuser, isGetBylistID = true }, "ID", "Title", "Connections");
                List<Connection> ListConnections = new List<Connection>();
                foreach (LUserJson oLUserJson in lstUser)
                {
                    if (oLUserJson.ID != oChatMessage.CreatedUser.LookupId)
                    {
                        if (!string.IsNullOrEmpty(oLUserJson.Connections))
                        {
                            ListConnections.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Connection>>(oLUserJson.Connections));

                        }
                    }


                }
                oChatMessage.UserNhan = string.Join(";#", lstUser.Select(x => x.ID + ";#" + x.Title));
                if (oChatMessage.ChatHopThoai_Id.HasValue && oChatMessage.ChatHopThoai_Id.Value > 0)
                {
                    ChatHopThoaiDA oChatHopThoaiDA = new ChatHopThoaiDA();
                    string LastMessage = "";
                    if (!string.IsNullOrEmpty(oChatMessage.NoiDungMessage))
                    {
                        LastMessage = oChatMessage.NoiDungMessage.Length > 20 ? oChatMessage.NoiDungMessage.Substring(0, 25) + "..." : oChatMessage.NoiDungMessage;
                    }
                    else if (oChatMessage.ListFileAttachAdd.Count > 0)
                    {
                        LastMessage = "[File] " + oChatMessage.ListFileAttachAdd[0].Name;
                        LastMessage = LastMessage.Length > 25 ? LastMessage.Substring(0, 25) + "..." : LastMessage;
                    }
                    oChatHopThoaiDA.UpdateOneField(oChatMessage.ChatHopThoai_Id.Value, "LastMessage", LastMessage);
                }
                if (string.IsNullOrEmpty(oChatMessage.NoiDungMessage) & oChatMessage.ListFileAttachAdd.Count > 0)
                {
                    oChatMessage.NoiDungMessage = $"<a target=\"_blank\" href=\"{ oChatMessage.ListFileAttachAdd[0].Url}\">{oChatMessage.ListFileAttachAdd[0].Name}</a>";
                }
                if (lstUser.Count > 0)
                {
                    oChatMessageDA.UpdateObject<ChatMessageItem>(oChatMessage);
                }

                Clients.Clients(ListConnections.Select(x => x.ConnectionID).ToList()).broadcastMessage("func_updateChatHopThoai_Id_B", Newtonsoft.Json.JsonConvert.SerializeObject(new
                {
                    message = oChatMessage.NoiDungMessage,
                    UserID = oChatMessage.CreatedUser.LookupId,
                    UserCreateTitle = oChatMessage.CreatedUser.LookupValue,
                    CreatedUser = new LookupData(oChatMessage.CreatedUser.LookupId, oChatMessage.CreatedUser.LookupValue),
                    ChatHopThoai_Id = oChatMessage.ChatHopThoai_Id.Value,
                    ChatHopThoai = oChatMessage.ChatHopThoai,
                    isGroupChat = true,
                    UserThamGia_Id = oChatMessage.UserNhan_Id,
                    UrAnhDaiDien = oChatMessage.AnhDaiDien,
                })); ;
            }
        }

        private void SendMessageChatCaNhan(int userid, ChatMessageItem oChatMessage)
        {
            //ChatMessageItem oChatMessage = new ChatMessageItem();
            ChatMessageDA oChatMessageDA = new ChatMessageDA();
            ChatHopThoaiItem oChatHopThoaiItem = new ChatHopThoaiItem();
            //oChatMessage = Newtonsoft.Json.JsonConvert.DeserializeObject<ChatMessageItem>(message, new SPFieldLookupValueConverter());


            // Call the broadcastMessage method to update clients.
            LUserDA oLUserDA = new LUserDA();
            if (userid > 0)
            {
                #region MyRegion
                LUserItem temp = oLUserDA.GetByIdToObjectSelectFields<LUserItem>(userid, "ID", "Title", "Connections");
                List<Connection> ListConnections = new List<Connection>();
                if (!string.IsNullOrEmpty(temp.Connections))
                    ListConnections = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Connection>>(temp.Connections);

                #region update thông tin hộp thoại 
                if (!(oChatMessage.ChatHopThoai_Id.HasValue && oChatMessage.ChatHopThoai_Id.Value > 0))
                {
                    #region MyRegion
                    //add thông tin 
                    ChatHopThoaiDA oChatHopThoaiDA = new ChatHopThoaiDA();
                    oChatHopThoaiItem = new ChatHopThoaiItem()
                    {
                        AnhDaiDien = oChatMessage.AnhDaiDien,
                        Title = oChatMessage.ChatHopThoai,
                        UserThamGia_Id = $",{oChatMessage.CreatedUser.LookupId},{temp.ID},",
                        UserThamGia = oChatMessage.CreatedUser.ToString() + $";#{temp.ID};#{temp.Title}",
                        isGroupChat = false,
                        LastMessage = oChatMessage.NoiDungMessage.Length > 20 ? oChatMessage.NoiDungMessage.Substring(0, 20) + "..." : oChatMessage.NoiDungMessage
                    };
                    oChatHopThoaiDA.UpdateObject<ChatHopThoaiItem>(oChatHopThoaiItem);
                    //post về cho những thằng client biết là đang cần phải update lại thông tin hộp thoại.
                    //Context.ConnectionId
                    Clients.Client(Context.ConnectionId).broadcastMessage("func_updateChatHopThoai_Id", oChatHopThoaiItem.ID + "");

                    oChatMessage.ChatHopThoai_Id = oChatHopThoaiItem.ID;
                    oChatMessage.ChatHopThoai = oChatHopThoaiItem.ID + "";
                    #endregion
                }
                else
                {
                    ChatHopThoaiDA oChatHopThoaiDA = new ChatHopThoaiDA();
                    oChatHopThoaiItem = new ChatHopThoaiItem()
                    {
                        Title = oChatMessage.ChatHopThoai,
                        ID = oChatMessage.ChatHopThoai_Id.Value,
                    };
                    string LastMessage = "";
                    if (!string.IsNullOrEmpty(oChatMessage.NoiDungMessage))
                    {
                        LastMessage = oChatMessage.NoiDungMessage.Length > 20 ? oChatMessage.NoiDungMessage.Substring(0, 25) + "..." : oChatMessage.NoiDungMessage;
                    }
                    else if (oChatMessage.ListFileAttachAdd.Count > 0)
                    {
                        LastMessage = "[File] " + oChatMessage.ListFileAttachAdd[0].Name;
                        LastMessage = LastMessage.Length > 25 ? LastMessage.Substring(0, 25) + "..." : LastMessage;
                    }
                    oChatHopThoaiDA.UpdateOneField(oChatMessage.ChatHopThoai_Id.Value, "LastMessage", LastMessage);
                }
                #endregion
                oChatMessage.UserNhan = $"{temp.ID};#{temp.Title}";
                oChatMessage.UserNhan_Id = $",{temp.ID},";
                oChatMessageDA.UpdateObject<ChatMessageItem>(oChatMessage);
                if (string.IsNullOrEmpty(oChatMessage.NoiDungMessage) & oChatMessage.ListFileAttachAdd.Count > 0)
                {
                    oChatMessage.NoiDungMessage = $"<a target=\"_blank\" href=\"{ oChatMessage.ListFileAttachAdd[0].Url}\">{oChatMessage.ListFileAttachAdd[0].Name}</a>";
                }
                foreach (var entity in ListConnections)
                {
                    if (oChatHopThoaiItem.ID > 0)
                    {
                        Clients.Client(entity.ConnectionID).broadcastMessage("func_updateChatHopThoai_Id_B", Newtonsoft.Json.JsonConvert.SerializeObject(new
                        {
                            message = oChatMessage.NoiDungMessage,
                            UserID = oChatMessage.CreatedUser.LookupId,
                            ChatHopThoai_Id = oChatHopThoaiItem.ID,
                            ChatHopThoai = oChatHopThoaiItem.Title,
                            isGroupChat = false,
                            AnhDaiDien = oChatMessage.AnhDaiDien

                        }));
                    }
                    else
                        Clients.Client(entity.ConnectionID).broadcastMessage(temp.Title, oChatMessage.NoiDungMessage);
                }
                #endregion
            }
        }
        public override Task OnConnected()
        {
            var userName = Context.User.Identity.Name;
            if (!string.IsNullOrEmpty(userName))
            {
                if (userName.Contains("|"))
                {
                    userName = userName.Split('|').LastOrDefault();
                }
                //0#.w|simax\maianh
                if (userName.Contains("\\"))
                {
                    userName = userName.Split('\\')[1];
                }
            }
            LUserDA oLUserDA = new LUserDA();
            LUserJson temp = oLUserDA.GetListJson(new LUserQuery()
            {
                TaiKhoanTruyCap = userName
            }).FirstOrDefault();
            if (temp != null && temp.ID > 0)
            {
                if (!string.IsNullOrEmpty(temp.Connections))
                {
                    temp.ListConnections = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Connection>>(temp.Connections);
                }
                temp.ListConnections.Add(new Connection
                {
                    ConnectionID = Context.ConnectionId,
                    UserAgent = Context.Request.Headers["User-Agent"],
                    Connected = true
                });
                oLUserDA.UpdateOneField(temp.ID, "Connections", Newtonsoft.Json.JsonConvert.SerializeObject(temp.ListConnections));
            }
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            string ConnectionId = Context.ConnectionId;
            var userName = Context.User.Identity.Name;
            userName = clsFucUtils.GetUserName(userName);
            LUserDA oLUserDA = new LUserDA();
            LUserJson temp = oLUserDA.GetListJson(new LUserQuery()
            {
                TaiKhoanTruyCap = userName
            }).FirstOrDefault();
            if (temp != null && temp.ID > 0)
            {
                if (!string.IsNullOrEmpty(temp.Connections))
                {
                    temp.ListConnections = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Connection>>(temp.Connections);
                    int sttuser = temp.ListConnections.FindIndex(x => x.ConnectionID == ConnectionId);
                    temp.ListConnections.RemoveAt(sttuser);
                    oLUserDA.UpdateOneField(temp.ID, "Connections", Newtonsoft.Json.JsonConvert.SerializeObject(temp.ListConnections));
                }
            }
            return base.OnDisconnected(stopCalled);
        }
    }
}