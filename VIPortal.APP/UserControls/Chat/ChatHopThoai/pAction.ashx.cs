using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortalAPP.ChatHopThoai
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {

            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            ChatHopThoaiDA oChatHopThoaiDA = new ChatHopThoaiDA();
            ChatHopThoaiItem oChatHopThoaiItem = new ChatHopThoaiItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    var oData = oChatHopThoaiDA.GetListJson(new ChatHopThoaiQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oChatHopThoaiDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    break;
                case "QUERYDATADANHBA":
                    oResult.IsQuery = true;
                    ChatHopThoaiQuery oChatHopThoaiQuery = new ChatHopThoaiQuery(context.Request);
                    oChatHopThoaiQuery.UserThamGia_Id = oChatHopThoaiQuery.ItemIDNotGet;
                    oChatHopThoaiQuery.ItemIDNotGet = 0;
                    var oDatadANHbA = oChatHopThoaiDA.GetListJson(oChatHopThoaiQuery);
                    //xử lý 1 bước nữa đó là loại bỏ người hiện tại.
                    LUserDA oLuserDA = new LUserDA();
                    List<LUserJson> lstUser = oLuserDA.GetListJson(new LUserQuery(context.Request));
                    //oDatadANHbA = oDatadANHbA.Select(c => { if(!c.isGroupChat) c.UserID = c.UserThamGia_Id.Replace($","); return c; }).ToList();
                    foreach (ChatHopThoaiJson item in oDatadANHbA)
                    {
                        if (!item.isGroupChat)
                        {
                            item.UserID = item.UserThamGia_Id.Replace($",{CurentUser.ID},", "").Replace(",", "");
                            item.AnhDaiDien = lstUser.FirstOrDefault(x => x.ID == Convert.ToInt32(item.UserID))?.AnhDaiDien;
                            item.Title = lstUser.FirstOrDefault(x => x.ID == Convert.ToInt32(item.UserID))?.Title;
                        }
                        else
                        {
                            item.AnhDaiDien = item.ListFileAttach.FirstOrDefault()?.Url;
                        }
                    }

                    //xử lý tiếp 1 chỗ này nữa.

                    //lấy ra người dùng mà chưa từng chat trên oChatHopThoaiDA.
                    lstUser = lstUser.Where(x => oDatadANHbA.FindIndex(y => !y.isGroupChat && y.UserThamGia.FindIndex(z => z.ID == x.ID) > -1) == -1).ToList();
                    oDatadANHbA.AddRange(lstUser.Select(x => new ChatHopThoaiJson()
                    {
                        Title = x.Title,
                        UserID = x.ID + "",
                        AnhDaiDien = x.AnhDaiDien
                    }));


                    //xuwr lys doanj nay tiep.
                    oDatadANHbA = oDatadANHbA.Select(c => { c.FullText = c.Title.RemoveVietnameseTone(); return c; }).ToList();
                    if (!string.IsNullOrEmpty(oChatHopThoaiQuery.Keyword)){
                        oDatadANHbA = oDatadANHbA.Where(x => x.FullText.Contains(oChatHopThoaiQuery.Keyword.RemoveVietnameseTone())).ToList();
                    }
                    DataGridRender oGriddANHbA = new DataGridRender(oDatadANHbA, oGridRequest.Draw,
                        oChatHopThoaiDA.TongSoBanGhiSauKhiQuery);



                    oResult.OData = oGriddANHbA;

                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oChatHopThoaiDA.GetListJson(new ChatHopThoaiQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oChatHopThoaiItem = oChatHopThoaiDA.GetByIdToObject<ChatHopThoaiItem>(oGridRequest.ItemID);
                        oChatHopThoaiItem.UpdateObject(context.Request);
                        oChatHopThoaiItem.UpdateObjectOnlyFile(context.Request, "DaiDien");
                        oChatHopThoaiDA.UpdateObject<ChatHopThoaiItem>(oChatHopThoaiItem);
                    }
                    else
                    {
                        oChatHopThoaiItem.UpdateObject(context.Request);
                        oChatHopThoaiItem.UpdateObjectOnlyFile(context.Request, "DaiDien");
                        oChatHopThoaiDA.UpdateObject<ChatHopThoaiItem>(oChatHopThoaiItem);
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    string outb = oChatHopThoaiDA.DeleteObject(oGridRequest.ItemID);
                    if (string.IsNullOrEmpty(outb))
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb;
                    break;
                case "DELETE-MULTI":
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oChatHopThoaiDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oChatHopThoaiDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oChatHopThoaiItem = oChatHopThoaiDA.GetByIdToObjectSelectFields<ChatHopThoaiItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oChatHopThoaiItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}