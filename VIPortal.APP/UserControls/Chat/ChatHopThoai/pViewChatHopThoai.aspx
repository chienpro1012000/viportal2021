<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewChatHopThoai.aspx.cs" Inherits="VIPortalAPP.pViewChatHopThoai" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
               <%=oChatHopThoai.Title%>
            </div>
        </div>
		<div class="form-group">
	<label for="UserThamGia" class="col-sm-2 control-label">UserThamGia</label>
	<div class="col-sm-10">
		<%=oChatHopThoai.UserThamGia%>
	</div>
</div>
<div class="form-group">
	<label for="UserThamGia_Id" class="col-sm-2 control-label">UserThamGia_Id</label>
	<div class="col-sm-10">
		<%=oChatHopThoai.UserThamGia_Id%>
	</div>
</div>

		  <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-10">
               <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oChatHopThoai.Created)%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oChatHopThoai.Modified)%>
            </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-10">
                <%=oChatHopThoai.Author.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-10">
                <%=oChatHopThoai.Editor.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oChatHopThoai.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oChatHopThoai.ListFileAttach[i].Url %>"><%=oChatHopThoai.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>