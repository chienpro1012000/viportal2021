using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public partial class pViewChatHopThoai : pFormBase
    {
        public ChatHopThoaiItem oChatHopThoai = new ChatHopThoaiItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oChatHopThoai = new ChatHopThoaiItem();
            if (ItemID > 0)
            {
                ChatHopThoaiDA oChatHopThoaiDA = new ChatHopThoaiDA();
                oChatHopThoai = oChatHopThoaiDA.GetByIdToObject<ChatHopThoaiItem>(ItemID);

            }
        }
    }
}