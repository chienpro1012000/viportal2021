<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormChatHopThoai.aspx.cs" Inherits="VIPortalAPP.pFormChatHopThoai" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-ChatHopThoai" class="form-horizontal">
	<input type="hidden" name="ItemID" id="ItemID" value="<%=oChatHopThoai.ID %>" />
	<input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oChatHopThoai.Title%>" />
            </div>
        </div>
		<div class="form-group row">
	<label for="UserThamGia" class="col-sm-2 control-label">UserThamGia</label>
	<div class="col-sm-10">
		<textarea  name="UserThamGia" id="UserThamGia" class="form-control"><%:oChatHopThoai.UserThamGia%></textarea>
	</div>
</div>
<div class="form-group row">
	<label for="UserThamGia_Id" class="col-sm-2 control-label">UserThamGia_Id</label>
	<div class="col-sm-10">
		<input type="text" name="UserThamGia_Id" id="UserThamGia_Id" placeholder="Nhập userthamgia_id" value="<%:oChatHopThoai.UserThamGia_Id%>" class="form-control"/>
	</div>
</div>

		<div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
   
    <script type="text/javascript">
        $(document).ready(function () {
			$("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oChatHopThoai.ListFileAttach)%>'
            });
			  $("#Title").focus();
			  $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale:{format: 'DD/MM/YYYY'}
				});
				$(".form-group select").select2({
				dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
				});
			
			$("#frm-ChatHopThoai").validate({
				rules: {
					Title: "required"
				},
				messages: {
					Title: "Vui lòng nhập tiêu đề"
				},
			submitHandler: function(form) {
				$.post("/UserControls/ChatHopThoai/pAction.ashx", $(form).viSerialize()  , function (result) {
						if (result.State == 2) {
							  BootstrapDialog.show({
						        title: "Lỗi",
						        message: result.Message
                              });
						}
						else {
							showMsg(result.Message);
							//$("#btn-find-ChatHopThoai").trigger("click");
						    $(form).closeModal();
                            $('#tblChatHopThoai').DataTable().ajax.reload();
						}
					}).always(function () { });
				}
			});
        });
    </script>
</body>
</html>