<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewChatMessage.aspx.cs" Inherits="VIPortalAPP.pViewChatMessage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
               <%=oChatMessage.Title%>
            </div>
        </div>
		<div class="form-group">
	<label for="ChatHopThoai" class="col-sm-2 control-label">ChatHopThoai</label>
	<div class="col-sm-10">
		<%=oChatMessage.ChatHopThoai%>
	</div>
</div>
<div class="form-group">
	<label for="ChatHopThoai_Id" class="col-sm-2 control-label">ChatHopThoai_Id</label>
	<div class="col-sm-10">
		<%=oChatMessage.ChatHopThoai_Id%>
	</div>
</div>
<div class="form-group">
	<label for="NoiDungMessage" class="col-sm-2 control-label">NoiDungMessage</label>
	<div class="col-sm-10">
		<%=oChatMessage.NoiDungMessage%>
	</div>
</div>
<div class="form-group">
	<label for="CreatedUser" class="col-sm-2 control-label">CreatedUser</label>
	<div class="col-sm-10">
		<%=oChatMessage.CreatedUser%>
	</div>
</div>
<div class="form-group">
	<label for="UserNhan" class="col-sm-2 control-label">UserNhan</label>
	<div class="col-sm-10">
		<%=oChatMessage.UserNhan%>
	</div>
</div>
<div class="form-group">
	<label for="UserNhan_Id" class="col-sm-2 control-label">UserNhan_Id</label>
	<div class="col-sm-10">
		<%=oChatMessage.UserNhan_Id%>
	</div>
</div>

		  <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-10">
               <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oChatMessage.Created)%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oChatMessage.Modified)%>
            </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-10">
                <%=oChatMessage.Author.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-10">
                <%=oChatMessage.Editor.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oChatMessage.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oChatMessage.ListFileAttach[i].Url %>"><%=oChatMessage.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>