<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormChatMessage.aspx.cs" Inherits="VIPortalAPP.pFormChatMessage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-ChatMessage" class="form-horizontal">
	<input type="hidden" name="ItemID" id="ItemID" value="<%=oChatMessage.ID %>" />
	<input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oChatMessage.Title%>" />
            </div>
        </div>
		<div class="form-group row">
	<label for="ChatHopThoai" class="col-sm-2 control-label">ChatHopThoai</label>
	<div class="col-sm-10">
		<input type="text" name="ChatHopThoai" id="ChatHopThoai" placeholder="Nhập chathopthoai" value="<%:oChatMessage.ChatHopThoai%>" class="form-control"/>
	</div>
</div>
<div class="form-group row">
	<label for="ChatHopThoai_Id" class="col-sm-2 control-label">ChatHopThoai_Id</label>
	<div class="col-sm-10">
		<input type="text" name="ChatHopThoai_Id" id="ChatHopThoai_Id" placeholder="Nhập chathopthoai_id" value="<%:oChatMessage.ChatHopThoai_Id%>" class="form-control"/>
	</div>
</div>
<div class="form-group row">
	<label for="NoiDungMessage" class="col-sm-2 control-label">NoiDungMessage</label>
	<div class="col-sm-10">
		<textarea  name="NoiDungMessage" id="NoiDungMessage" class="form-control"><%:oChatMessage.NoiDungMessage%></textarea>
	</div>
</div>
<div class="form-group row">
	<label for="CreatedUser" class="col-sm-2 control-label">CreatedUser</label>
	<div class="col-sm-10">
		<select data-selected="<%:oChatMessage.CreatedUser.LookupId%>" data-url="/UserControls/ChatMessage/pAction.ashx?do=alljson" data-place="Chọn CreatedUser" name="CreatedUser" id="CreatedUser" class="form-control"></select>
	</div>
</div>
<div class="form-group row">
	<label for="UserNhan" class="col-sm-2 control-label">UserNhan</label>
	<div class="col-sm-10">
		<textarea  name="UserNhan" id="UserNhan" class="form-control"><%:oChatMessage.UserNhan%></textarea>
	</div>
</div>
<div class="form-group row">
	<label for="UserNhan_Id" class="col-sm-2 control-label">UserNhan_Id</label>
	<div class="col-sm-10">
		<input type="text" name="UserNhan_Id" id="UserNhan_Id" placeholder="Nhập usernhan_id" value="<%:oChatMessage.UserNhan_Id%>" class="form-control"/>
	</div>
</div>

		<div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
   
    <script type="text/javascript">
        $(document).ready(function () {
			$("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oChatMessage.ListFileAttach)%>'
            });
			  $("#Title").focus();
			  $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale:{format: 'DD/MM/YYYY'}
				});
				$(".form-group select").select2({
				dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
				});
			
			$("#frm-ChatMessage").validate({
				rules: {
					Title: "required"
				},
				messages: {
					Title: "Vui lòng nhập tiêu đề"
				},
			submitHandler: function(form) {
				$.post("/UserControls/ChatMessage/pAction.ashx", $(form).viSerialize()  , function (result) {
						if (result.State == 2) {
							  BootstrapDialog.show({
						        title: "Lỗi",
						        message: result.Message
                              });
						}
						else {
							showMsg(result.Message);
							//$("#btn-find-ChatMessage").trigger("click");
						    $(form).closeModal();
                            $('#tblChatMessage').DataTable().ajax.reload();
						}
					}).always(function () { });
				}
			});
        });
    </script>
</body>
</html>