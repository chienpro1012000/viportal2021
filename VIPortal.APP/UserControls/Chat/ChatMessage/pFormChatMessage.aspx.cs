using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public partial class pFormChatMessage : pFormBase
    {
        public ChatMessageItem oChatMessage {get;set;}
        protected void Page_Load(object sender, EventArgs e)
        {
			oChatMessage = new ChatMessageItem();
            if (ItemID > 0)
            {
                ChatMessageDA oChatMessageDA = new ChatMessageDA();
                oChatMessage = oChatMessageDA.GetByIdToObject<ChatMessageItem>(ItemID);
            }
        }
    }
}