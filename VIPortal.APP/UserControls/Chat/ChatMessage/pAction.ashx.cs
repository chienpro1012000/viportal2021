using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortalAPP.ChatMessage
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : IHttpHandler
    {
        public string doAction = string.Empty;
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            if (!string.IsNullOrEmpty(context.Request["do"]))
                doAction = context.Request["do"].ToUpper();

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            ChatMessageDA oChatMessageDA = new ChatMessageDA();
            ChatMessageItem oChatMessageItem = new ChatMessageItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oChatMessageDA.GetListJson(new ChatMessageQuery(context.Request));
                    //lấy cái url
                    LUserDA oLuserDA = new LUserDA();
                    List<int> lstid = oData.Select(x => x.CreatedUser.ID).Distinct().ToList();
                    List<LUserJson> LstUser =  oLuserDA.GetListJson(new LUserQuery() { isGetBylistID = true, lstIDget = lstid });

                    oData = oData.Select(c => { c.UrAnhDaiDien = LstUser.FirstOrDefault(x=>x.ID == c.CreatedUser.ID)?.AnhDaiDien.GetPathAndQuery(); return c; }).ToList();

                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw, 
                        oChatMessageDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oChatMessageDA.GetListJson(new ChatMessageQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oChatMessageItem = oChatMessageDA.GetByIdToObject<ChatMessageItem>(oGridRequest.ItemID);
                        oChatMessageItem.UpdateObject(context.Request);
                        oChatMessageDA.UpdateObject<ChatMessageItem>(oChatMessageItem);
                    }
                    else
                    {
                        oChatMessageItem.UpdateObject(context.Request);
                        oChatMessageDA.UpdateObject<ChatMessageItem>(oChatMessageItem);
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    string outb = oChatMessageDA.DeleteObject(oGridRequest.ItemID);
                    if (string.IsNullOrEmpty(outb))
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb;
                    break;
                case "DELETE-MULTI":
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oChatMessageDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                     oChatMessageDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oChatMessageItem = oChatMessageDA.GetByIdToObjectSelectFields<ChatMessageItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oChatMessageItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}