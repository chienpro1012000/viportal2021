<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormNhatKyXuLy.aspx.cs" Inherits="VIPortalAPP.pFormNhatKyXuLy" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-NhatKyXuLy" class="form-horizontal">
	<input type="hidden" name="ItemID" id="ItemID" value="<%=oNhatKyXuLy.ID %>" />
	<input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oNhatKyXuLy.Title%>" />
            </div>
        </div>
		<div class="form-group row">
	<label for="CreatedDate" class="col-sm-2 control-label">CreatedDate</label>
	<div class="col-sm-10">
		<input type="text" name="CreatedDate" id="CreatedDate" value="<%:string.Format("{0:dd/MM/yyyy}",oNhatKyXuLy.CreatedDate)%>" class="form-control input-datetime"/>
	</div>
</div>
<div class="form-group row">
	<label for="CreatedUser" class="col-sm-2 control-label">CreatedUser</label>
	<div class="col-sm-10">
		<select data-selected="<%:oNhatKyXuLy.CreatedUser.LookupId%>" data-url="/UserControls/NhatKyXuLy/pAction.ashx?do=alljson" data-place="Chọn CreatedUser" name="CreatedUser" id="CreatedUser" class="form-control"></select>
	</div>
</div>
<div class="form-group row">
	<label for="LogNoiDung" class="col-sm-2 control-label">LogNoiDung</label>
	<div class="col-sm-10">
		<textarea  name="LogNoiDung" id="LogNoiDung" class="form-control"><%:oNhatKyXuLy.LogNoiDung%></textarea>
	</div>
</div>
<div class="form-group row">
	<label for="LogDoiTuong" class="col-sm-2 control-label">LogDoiTuong</label>
	<div class="col-sm-10">
		<input type="text" name="LogDoiTuong" id="LogDoiTuong" placeholder="Nhập logdoituong" value="<%:oNhatKyXuLy.LogDoiTuong%>" class="form-control"/>
	</div>
</div>
<div class="form-group row">
	<label for="LogIDDoiTuong" class="col-sm-2 control-label">LogIDDoiTuong</label>
	<div class="col-sm-10">
		<input type="text" name="LogIDDoiTuong" id="LogIDDoiTuong" placeholder="Nhập logiddoituong" value="<%:oNhatKyXuLy.LogIDDoiTuong%>" class="form-control"/>
	</div>
</div>
<div class="form-group row">
	<label for="LogThaoTac" class="col-sm-2 control-label">LogThaoTac</label>
	<div class="col-sm-10">
		<input type="text" name="LogThaoTac" id="LogThaoTac" placeholder="Nhập logthaotac" value="<%:oNhatKyXuLy.LogThaoTac%>" class="form-control"/>
	</div>
</div>
<div class="form-group row">
	<label for="LogTrangThai" class="col-sm-2 control-label">LogTrangThai</label>
	<div class="col-sm-10">
		<input type="text" name="LogTrangThai" id="LogTrangThai" placeholder="Nhập logtrangthai" value="<%:oNhatKyXuLy.LogTrangThai%>" class="form-control"/>
	</div>
</div>
<div class="form-group row">
	<label for="LogTrangThaiTiepTheo" class="col-sm-2 control-label">LogTrangThaiTiepTheo</label>
	<div class="col-sm-10">
		<input type="text" name="LogTrangThaiTiepTheo" id="LogTrangThaiTiepTheo" placeholder="Nhập logtrangthaitieptheo" value="<%:oNhatKyXuLy.LogTrangThaiTiepTheo%>" class="form-control"/>
	</div>
</div>
<div class="form-group row">
	<label for="UserNhanXuLyText" class="col-sm-2 control-label">UserNhanXuLyText</label>
	<div class="col-sm-10">
		<input type="text" name="UserNhanXuLyText" id="UserNhanXuLyText" placeholder="Nhập usernhanxulytext" value="<%:oNhatKyXuLy.UserNhanXuLyText%>" class="form-control"/>
	</div>
</div>

		<div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
   
    <script type="text/javascript">
        $(document).ready(function () {
			$("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oNhatKyXuLy.ListFileAttach)%>'
            });
			  $("#Title").focus();
			  $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale:{format: 'DD/MM/YYYY'}
				});
				$(".form-group select").select2({
				dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
				});
			
			$("#frm-NhatKyXuLy").validate({
				rules: {
					Title: "required"
				},
				messages: {
					Title: "Vui lòng nhập tiêu đề"
				},
			submitHandler: function(form) {
                $.post("/UserControls/QTQuyTrinhBT/NhatKyXuLy/pAction.ashx", $(form).viSerialize()  , function (result) {
						if (result.State == 2) {
							  BootstrapDialog.show({
						        title: "Lỗi",
						        message: result.Message
                              });
						}
						else {
							showMsg(result.Message);
							//$("#btn-find-NhatKyXuLy").trigger("click");
						    $(form).closeModal();
                            $('#tblNhatKyXuLy').DataTable().ajax.reload();
						}
					}).always(function () { });
				}
			});
        });
    </script>
</body>
</html>