using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public partial class pFormNhatKyXuLy : pFormBase
    {
        public NhatKyXuLyItem oNhatKyXuLy {get;set;}
        protected void Page_Load(object sender, EventArgs e)
        {
			oNhatKyXuLy = new NhatKyXuLyItem();
            if (ItemID > 0)
            {
                NhatKyXuLyDA oNhatKyXuLyDA = new NhatKyXuLyDA(UrlSite);
                oNhatKyXuLy = oNhatKyXuLyDA.GetByIdToObject<NhatKyXuLyItem>(ItemID);
            }
        }
    }
}