using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils;
using ViPortal_Utils.Base;
using ViPortalData.SPListCommon;
using ViPortalData.TinNoiBat;

namespace VIPortalAPP.NhatKyXuLy
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            if (!string.IsNullOrEmpty(context.Request["do"]))
                doAction = context.Request["do"].ToUpper();

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            NhatKyXuLyDA oNhatKyXuLyDA = new NhatKyXuLyDA(UrlSite);
            NhatKyXuLyItem oNhatKyXuLyItem = new NhatKyXuLyItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oNhatKyXuLyDA.GetListJson(new NhatKyXuLyQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oNhatKyXuLyDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oNhatKyXuLyDA.GetListJson(new NhatKyXuLyQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        
                        oNhatKyXuLyItem = oNhatKyXuLyDA.GetByIdToObject<NhatKyXuLyItem>(oGridRequest.ItemID);
                        oNhatKyXuLyItem.UpdateObject(context.Request);
                        oNhatKyXuLyDA.UpdateObject<NhatKyXuLyItem>(oNhatKyXuLyItem);
                    }
                    else
                    {
                        #region MyRegion
                        string NguoiGui = context.Request["LNotiNguoiGui"];
                        oNhatKyXuLyItem.UpdateObject(context.Request);
                        oNhatKyXuLyDA.UpdateObject<NhatKyXuLyItem>(oNhatKyXuLyItem);
                        //update tragnj thái để xử lý các vấn đề.

                        List<string> lstDanhMuc = new List<string>()
                        {
                            "/noidung/Lists/Banner","/diendan/Lists/BinhLuan","/Lists/ChatHopThoai", "/Lists/ChatMessage", "/diendan/Lists/CommentMXH","/noidung/Lists/CongThanhPhan", "/noidung/Lists/CoQuanBanHanh", "/noidung/Lists/DanhMucQuyChe","/noidung/Lists/DMChucVu", "/cms/Lists/DanhMucThongTin","/Lists/DanhSachNgonNgu", "/noidung/Lists/DMHinhAnh","/noidung/Lists/DMLinhVuc","/noidung/Lists/DMTaiLieu","/noidung/Lists/DMVideo","/diendan/Lists/ForumChuDe","/noidung/Lists/KSBoCauHoi","/noidung/Lists/KSCauTraLoi","/noidung/Lists/KSKetQua","/cms/Lists/Lconfig","/Lists/LGroup","/noidung/Lists/LienKetWebsite","/noidung/Lists/LoaiTaiLieu","/noidung/Lists/LNotification","/Lists/LPermission","/Lists/LUser","/Lists/LVaiTro","/cms/Lists/MailPending","/cms/Lists/MenuQuanTri", "/noidung/Lists/ModuleChucNang",  "/noidung/Lists/TraLoiHoiDap","/noidung/Lists/TrangThanhPhan"
                        };
                        ItemContent oItemContent = new ItemContent();
                        if (lstDanhMuc.Contains(UrlSite + oNhatKyXuLyItem.LogDoiTuong))
                        {
                            SPListDA<DanhMucSolr> oSPListDA = new SPListDA<DanhMucSolr>(UrlSite + oNhatKyXuLyItem.LogDoiTuong, true);
                            oItemContent = oSPListDA.GetInfo(oNhatKyXuLyItem.LogIDDoiTuong);
                            Dictionary<string, object> lstUpdate = new Dictionary<string, object>();
                            lstUpdate.Add("WFTrangThai", oNhatKyXuLyItem.LogTrangThaiTiepTheo);
                            lstUpdate.Add("UserPhuTrach", new SPFieldLookupValueCollection(oNhatKyXuLyItem.UserNhanXuLyText));
                            lstUpdate.Add("LogText", oItemContent.LogText + $"|{oNhatKyXuLyItem.TrangThaiParent}-{oNhatKyXuLyItem.CreatedUser.LookupId}-{oNhatKyXuLyItem.LogTrangThaiTiepTheo}|");
                            if (oNhatKyXuLyItem.LogTrangThaiTiepTheo == "999")
                            {
                                lstUpdate.Add("_ModerationStatus", SPModerationStatusType.Approved);
                                oSPListDA.UpdateSPModerationStatus(oNhatKyXuLyItem.LogIDDoiTuong, SPModerationStatusType.Approved);
                            }
                            if (oNhatKyXuLyItem.LogTrangThaiTiepTheo == "000")
                            {
                                lstUpdate.Add("_ModerationStatus", SPModerationStatusType.Pending);
                                oSPListDA.UpdateSPModerationStatus(oNhatKyXuLyItem.LogIDDoiTuong, SPModerationStatusType.Pending);
                            }
                            oResult.Message = oSPListDA.UpdateOneOrMoreField(oNhatKyXuLyItem.LogIDDoiTuong, lstUpdate);
                        }
                        else
                        {
                            SPListDA<DataSolr> oSPListDA = new SPListDA<DataSolr>(UrlSite + oNhatKyXuLyItem.LogDoiTuong, true);
                            oItemContent = oSPListDA.GetInfo(oNhatKyXuLyItem.LogIDDoiTuong);
                            Dictionary<string, object> lstUpdate = new Dictionary<string, object>();
                            lstUpdate.Add("WFTrangThai", oNhatKyXuLyItem.LogTrangThaiTiepTheo);
                            lstUpdate.Add("UserPhuTrach", new SPFieldLookupValueCollection(oNhatKyXuLyItem.UserNhanXuLyText));
                            lstUpdate.Add("LogText", oItemContent.LogText + $"|{oNhatKyXuLyItem.TrangThaiParent}-{oNhatKyXuLyItem.CreatedUser.LookupId}-{oNhatKyXuLyItem.LogTrangThaiTiepTheo}|");
                            if (oNhatKyXuLyItem.LogTrangThaiTiepTheo == "999")
                            {
                                lstUpdate.Add("_ModerationStatus", SPModerationStatusType.Approved);
                                oSPListDA.UpdateSPModerationStatus(oNhatKyXuLyItem.LogIDDoiTuong, SPModerationStatusType.Approved);
                            }
                            if (oNhatKyXuLyItem.LogTrangThaiTiepTheo == "000")
                            {
                                lstUpdate.Add("_ModerationStatus", SPModerationStatusType.Pending);
                                oSPListDA.UpdateSPModerationStatus(oNhatKyXuLyItem.LogIDDoiTuong, SPModerationStatusType.Pending);
                            }
                            oResult.Message = oSPListDA.UpdateOneOrMoreField(oNhatKyXuLyItem.LogIDDoiTuong, lstUpdate);
                        }
                        if (string.IsNullOrEmpty(oResult.Message))
                        {
                            #region
                            //check 1 cái chỗ này nếu là tin được duyejt thì sẽ duyệt in túc.
                            TinNoiBatDA oTinNoiBatDA = new TinNoiBatDA(UrlSite);
                            List<TinNoiBatJson> lstTInNong = oTinNoiBatDA.GetListJson(new TinNoiBatQuery()
                            {
                                ItemTinTuc = oNhatKyXuLyItem.LogIDDoiTuong,
                                UrlListNew = UrlSite + oNhatKyXuLyItem.LogDoiTuong,
                            });
                            if (lstTInNong.Count > 0)
                            {
                                if (oNhatKyXuLyItem.LogTrangThaiTiepTheo == "999")
                                {
                                    oTinNoiBatDA.UpdateSPModerationStatus(lstTInNong[0].ID, SPModerationStatusType.Approved);
                                }
                                else
                                {
                                    oTinNoiBatDA.UpdateSPModerationStatus(lstTInNong[0].ID, SPModerationStatusType.Pending);
                                }
                            }

                            //Tạo mail thông báo cho các ban liên quan. tạo thông báo đây.
                            LNotificationDA oLNotificationDA = new LNotificationDA(UrlSite);
                            LNotificationItem lNotificationItem = new LNotificationItem();
                            lNotificationItem.TitleBaiViet = oItemContent.Title;
                            lNotificationItem.LNotiNguoiGui = NguoiGui;
                            lNotificationItem.LNotiNguoiNhan = oNhatKyXuLyItem.UserNhanXuLyText;
                            lNotificationItem.LNotiNguoiNhan_Id = "," + string.Join(",", new SPFieldLookupValueCollection(oNhatKyXuLyItem.UserNhanXuLyText).Select(x => x.LookupId)) + ",";
                            lNotificationItem.LNotiChuaXem_Id = lNotificationItem.LNotiNguoiNhan_Id;
                            lNotificationItem.DoiTuongTitle = oNhatKyXuLyItem.DoiTuongTitle;
                            lNotificationItem.LNotiData = Newtonsoft.Json.JsonConvert.SerializeObject(oNhatKyXuLyItem);
                            lNotificationItem.Title = $"Bản ghi {lNotificationItem.DoiTuongTitle} cần {"xử lý"} ";
                            lNotificationItem.LNotiMoTa = oItemContent.Title;
                            lNotificationItem.LNotiSentTime = DateTime.Now;
                            if (!string.IsNullOrEmpty(oNhatKyXuLyItem.LogDoiTuong))
                            {
                                string ListTitle = oNhatKyXuLyItem.LogDoiTuong.Split('/').LastOrDefault();
                                switch (ListTitle)
                                {
                                    case "QuyCheNoiBo":
                                        lNotificationItem.UrlLink = $"/noidung/Pages/QuyCheNoiBo.aspx?ItemID={oNhatKyXuLyItem.LogIDDoiTuong}";
                                        break;
                                    case "VanBanTaiLieu":
                                        lNotificationItem.UrlLink = $"/noidung/Pages/VanBanTaiLieu.aspx?ItemID={oNhatKyXuLyItem.LogIDDoiTuong}";
                                        break;
                                    default:
                                        lNotificationItem.UrlLink = $"/cms/Pages/tintuc.aspx?ItemID={oNhatKyXuLyItem.LogIDDoiTuong}";
                                        break;
                                        //boor sung them cac chuc nang khac sau khi xac nhan cac chuc nang thuc hien cau hinh WF
                                }
                            }
                            oLNotificationDA.UpdateObject<LNotificationItem>(lNotificationItem);
                            #endregion;
                            oResult.Message = "Lưu thành công";
                        }
                        else
                        {
                            oResult.State = ActionState.Error;
                        } 
                        #endregion

                    }
                    break;
                case "DELETE":
                    string outb = oNhatKyXuLyDA.DeleteObject(oGridRequest.ItemID);
                    if (string.IsNullOrEmpty(outb))
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb;
                    break;
                case "DELETE-MULTI":
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oNhatKyXuLyDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oNhatKyXuLyDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oNhatKyXuLyItem = oNhatKyXuLyDA.GetByIdToObjectSelectFields<NhatKyXuLyItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oNhatKyXuLyItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}