using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public partial class pViewNhatKyXuLy : pFormBase
    {
        public NhatKyXuLyItem oNhatKyXuLy = new NhatKyXuLyItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oNhatKyXuLy = new NhatKyXuLyItem();
            if (ItemID > 0)
            {
                NhatKyXuLyDA oNhatKyXuLyDA = new NhatKyXuLyDA(UrlSite);
                oNhatKyXuLy = oNhatKyXuLyDA.GetByIdToObject<NhatKyXuLyItem>(ItemID);

            }
        }
    }
}