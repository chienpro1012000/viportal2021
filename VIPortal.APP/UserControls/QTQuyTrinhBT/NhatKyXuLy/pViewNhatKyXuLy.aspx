<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewNhatKyXuLy.aspx.cs" Inherits="VIPortalAPP.pViewNhatKyXuLy" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
               <%=oNhatKyXuLy.Title%>
            </div>
        </div>
		<div class="form-group">
	<label for="CreatedDate" class="col-sm-2 control-label">CreatedDate</label>
	<div class="col-sm-10">
		<%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oNhatKyXuLy.CreatedDate)%>
	</div>
</div>
<div class="form-group">
	<label for="CreatedUser" class="col-sm-2 control-label">CreatedUser</label>
	<div class="col-sm-10">
		<%=oNhatKyXuLy.CreatedUser%>
	</div>
</div>
<div class="form-group">
	<label for="LogNoiDung" class="col-sm-2 control-label">LogNoiDung</label>
	<div class="col-sm-10">
		<%=oNhatKyXuLy.LogNoiDung%>
	</div>
</div>
<div class="form-group">
	<label for="LogDoiTuong" class="col-sm-2 control-label">LogDoiTuong</label>
	<div class="col-sm-10">
		<%=oNhatKyXuLy.LogDoiTuong%>
	</div>
</div>
<div class="form-group">
	<label for="LogIDDoiTuong" class="col-sm-2 control-label">LogIDDoiTuong</label>
	<div class="col-sm-10">
		<%=oNhatKyXuLy.LogIDDoiTuong%>
	</div>
</div>
<div class="form-group">
	<label for="LogThaoTac" class="col-sm-2 control-label">LogThaoTac</label>
	<div class="col-sm-10">
		<%=oNhatKyXuLy.LogThaoTac%>
	</div>
</div>
<div class="form-group">
	<label for="LogTrangThai" class="col-sm-2 control-label">LogTrangThai</label>
	<div class="col-sm-10">
		<%=oNhatKyXuLy.LogTrangThai%>
	</div>
</div>
<div class="form-group">
	<label for="LogTrangThaiTiepTheo" class="col-sm-2 control-label">LogTrangThaiTiepTheo</label>
	<div class="col-sm-10">
		<%=oNhatKyXuLy.LogTrangThaiTiepTheo%>
	</div>
</div>
<div class="form-group">
	<label for="UserNhanXuLyText" class="col-sm-2 control-label">UserNhanXuLyText</label>
	<div class="col-sm-10">
		<%=oNhatKyXuLy.UserNhanXuLyText%>
	</div>
</div>

		  <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-10">
               <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oNhatKyXuLy.Created)%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oNhatKyXuLy.Modified)%>
            </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-10">
                <%=oNhatKyXuLy.Author.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-10">
                <%=oNhatKyXuLy.Editor.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oNhatKyXuLy.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oNhatKyXuLy.ListFileAttach[i].Url %>"><%=oNhatKyXuLy.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>