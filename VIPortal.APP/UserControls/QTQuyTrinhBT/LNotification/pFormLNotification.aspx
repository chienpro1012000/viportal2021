<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormLNotification.aspx.cs" Inherits="VIPortalAPP.pFormLNotification" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-LNotification" class="form-horizontal">
	<input type="hidden" name="ItemID" id="ItemID" value="<%=oLNotification.ID %>" />
	<input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oLNotification.Title%>" />
            </div>
        </div>
		<div class="form-group row">
	<label for="LNotiDaXem" class="col-sm-2 control-label">LNotiDaXem</label>
	<div class="col-sm-10">
		<input type="text" name="LNotiDaXem" id="LNotiDaXem" placeholder="Nhập lnotidaxem" value="<%:oLNotification.LNotiDaXem%>" class="form-control"/>
	</div>
</div>
<div class="form-group row">
	<label for="LNotiIsSentFirebase" class="col-sm-2 control-label">LNotiIsSentFirebase</label>
	<div class="col-sm-10">
		<input type="text" name="LNotiIsSentFirebase" id="LNotiIsSentFirebase" placeholder="Nhập lnotiissentfirebase" value="<%:oLNotification.LNotiIsSentFirebase%>" class="form-control"/>
	</div>
</div>
<div class="form-group row">
	<label for="LNotiIsSentMail" class="col-sm-2 control-label">LNotiIsSentMail</label>
	<div class="col-sm-10">
		<input type="text" name="LNotiIsSentMail" id="LNotiIsSentMail" placeholder="Nhập lnotiissentmail" value="<%:oLNotification.LNotiIsSentMail%>" class="form-control"/>
	</div>
</div>
<div class="form-group row">
	<label for="LNotiMoTa" class="col-sm-2 control-label">LNotiMoTa</label>
	<div class="col-sm-10">
		<textarea  name="LNotiMoTa" id="LNotiMoTa" class="form-control"><%:oLNotification.LNotiMoTa%></textarea>
	</div>
</div>
<div class="form-group row">
	<label for="LNotiNguoiNhan" class="col-sm-2 control-label">LNotiNguoiNhan</label>
	<div class="col-sm-10">
		<textarea  name="LNotiNguoiNhan" id="LNotiNguoiNhan" class="form-control"><%:oLNotification.LNotiNguoiNhan%></textarea>
	</div>
</div>
<div class="form-group row">
	<label for="LNotiNguoiNhan_Id" class="col-sm-2 control-label">LNotiNguoiNhan_Id</label>
	<div class="col-sm-10">
		<input type="text" name="LNotiNguoiNhan_Id" id="LNotiNguoiNhan_Id" placeholder="Nhập lnotinguoinhan_id" value="<%:oLNotification.LNotiNguoiNhan_Id%>" class="form-control"/>
	</div>
</div>
<div class="form-group row">
	<label for="LNotiNoiDung" class="col-sm-2 control-label">LNotiNoiDung</label>
	<div class="col-sm-10">
		<textarea  name="LNotiNoiDung" id="LNotiNoiDung" class="form-control"><%:oLNotification.LNotiNoiDung%></textarea>
	</div>
</div>
<div class="form-group row">
	<label for="LNotiSentTime" class="col-sm-2 control-label">LNotiSentTime</label>
	<div class="col-sm-10">
		<input type="text" name="LNotiSentTime" id="LNotiSentTime" value="<%:string.Format("{0:dd/MM/yyyy}",oLNotification.LNotiSentTime)%>" class="form-control input-datetime"/>
	</div>
</div>

		<div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
   
    <script type="text/javascript">
        $(document).ready(function () {
			$("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oLNotification.ListFileAttach)%>'
            });
			  $("#Title").focus();
			  $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale:{format: 'DD/MM/YYYY'}
				});
				$(".form-group select").select2({
				dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
				});
			
			$("#frm-LNotification").validate({
				rules: {
					Title: "required"
				},
				messages: {
					Title: "Vui lòng nhập tiêu đề"
				},
			submitHandler: function(form) {
				$.post("/UserControls/LNotification/pAction.ashx", $(form).viSerialize()  , function (result) {
						if (result.State == 2) {
							  BootstrapDialog.show({
						        title: "Lỗi",
						        message: result.Message
                              });
						}
						else {
							showMsg(result.Message);
							//$("#btn-find-LNotification").trigger("click");
						    $(form).closeModal();
                            $('#tblLNotification').DataTable().ajax.reload();
						}
					}).always(function () { });
				}
			});
        });
    </script>
</body>
</html>