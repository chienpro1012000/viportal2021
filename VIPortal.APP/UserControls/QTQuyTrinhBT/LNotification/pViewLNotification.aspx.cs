using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public partial class pViewLNotification : pFormBase
    {
        public LNotificationItem oLNotification = new LNotificationItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oLNotification = new LNotificationItem();
            if (ItemID > 0)
            {
                LNotificationDA oLNotificationDA = new LNotificationDA();
                oLNotification = oLNotificationDA.GetByIdToObject<LNotificationItem>(ItemID);

            }
        }
    }
}