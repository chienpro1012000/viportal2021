<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewLNotification.aspx.cs" Inherits="VIPortalAPP.pViewLNotification" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
               <%=oLNotification.Title%>
            </div>
        </div>
		<div class="form-group">
	<label for="LNotiDaXem" class="col-sm-2 control-label">LNotiDaXem</label>
	<div class="col-sm-10">
		<%=oLNotification.LNotiDaXem%>
	</div>
</div>
<div class="form-group">
	<label for="LNotiIsSentFirebase" class="col-sm-2 control-label">LNotiIsSentFirebase</label>
	<div class="col-sm-10">
		<%=oLNotification.LNotiIsSentFirebase%>
	</div>
</div>
<div class="form-group">
	<label for="LNotiIsSentMail" class="col-sm-2 control-label">LNotiIsSentMail</label>
	<div class="col-sm-10">
		<%=oLNotification.LNotiIsSentMail%>
	</div>
</div>
<div class="form-group">
	<label for="LNotiMoTa" class="col-sm-2 control-label">LNotiMoTa</label>
	<div class="col-sm-10">
		<%=oLNotification.LNotiMoTa%>
	</div>
</div>
<div class="form-group">
	<label for="LNotiNguoiNhan" class="col-sm-2 control-label">LNotiNguoiNhan</label>
	<div class="col-sm-10">
		<%=oLNotification.LNotiNguoiNhan%>
	</div>
</div>
<div class="form-group">
	<label for="LNotiNguoiNhan_Id" class="col-sm-2 control-label">LNotiNguoiNhan_Id</label>
	<div class="col-sm-10">
		<%=oLNotification.LNotiNguoiNhan_Id%>
	</div>
</div>
<div class="form-group">
	<label for="LNotiNoiDung" class="col-sm-2 control-label">LNotiNoiDung</label>
	<div class="col-sm-10">
		<%=oLNotification.LNotiNoiDung%>
	</div>
</div>
<div class="form-group">
	<label for="LNotiSentTime" class="col-sm-2 control-label">LNotiSentTime</label>
	<div class="col-sm-10">
		<%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oLNotification.LNotiSentTime)%>
	</div>
</div>

		  <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-10">
               <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oLNotification.Created)%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oLNotification.Modified)%>
            </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-10">
                <%=oLNotification.Author.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-10">
                <%=oLNotification.Editor.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oLNotification.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oLNotification.ListFileAttach[i].Url %>"><%=oLNotification.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>