<%@ page language="C#" autoeventwireup="true" codebehind="pViewLoaiDulieu.aspx.cs" inherits="VIPortalAPP.pViewLoaiDulieu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
        <div class="form-group">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <%=oLoaiDulieu.Title%>
            </div>
        </div>
        <%--<div class="form-group">
            <label for="DMHienThi" class="col-sm-2 control-label">DMHienThi</label>
            <div class="col-sm-10">
                <%=oLoaiDulieu.DMHienThi? "Có" : "Không"%>
            </div>
        </div>--%>
        <div class="form-group">
            <label for="DMMoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <%=oLoaiDulieu.DMMoTa%>
            </div>
        </div>
        <div class="form-group">
            <label for="DMSTT" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-10">
                <%=oLoaiDulieu.DMSTT%>
            </div>
        </div>
        <div class="form-group">
            <label for="DMVietTat" class="col-sm-2 control-label">Viết tắt</label>
            <div class="col-sm-10">
                <%=oLoaiDulieu.DMVietTat%>
            </div>
        </div>
        <div class="form-group">
            <label for="UrlList" class="col-sm-2 control-label">UrlList</label>
            <div class="col-sm-10">
                <%=oLoaiDulieu.UrlList%>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oLoaiDulieu.Created)%>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oLoaiDulieu.Modified)%>
            </div>
        </div>
        <%--<div class="form-group">
            <label class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-10">
                <%=oLoaiDulieu.Author.LookupValue%>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-10">
                <%=oLoaiDulieu.Editor.LookupValue%>
            </div>
        </div>--%>
        <div class="form-group">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oLoaiDulieu.ListFileAttach.Count;i++){ %>
                <div><a href="<%=oLoaiDulieu.ListFileAttach[i].Url %>"><%=oLoaiDulieu.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>
