using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortalAPP
{
    public partial class pFormLoaiDulieu : pFormBase
    {
        public LoaiDulieuItem oLoaiDulieu {get;set;}
        protected void Page_Load(object sender, EventArgs e)
        {
			oLoaiDulieu = new LoaiDulieuItem();
            if (ItemID > 0)
            {
                LoaiDulieuDA oLoaiDulieuDA = new LoaiDulieuDA();
                oLoaiDulieu = oLoaiDulieuDA.GetByIdToObject<LoaiDulieuItem>(ItemID);
            }
        }
    }
}