<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormLoaiDulieu.aspx.cs" Inherits="VIPortalAPP.pFormLoaiDulieu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-LoaiDulieu" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oLoaiDulieu.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oLoaiDulieu.Title%>" />
            </div>
        </div>
        <%--<div class="form-group row">
            <label for="DMHienThi" class="col-sm-2 control-label">Hiển thị</label>
            <div class="col-sm-10">
                <input value="true" type="checkbox" name="DMHienThi" id="DMHienThi" <%=oLoaiDulieu.DMHienThi? "checked" : string.Empty%> />
            </div>
        </div>--%>
        <div class="form-group row">
            <label for="DMMoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <textarea name="DMMoTa" id="DMMoTa" class="form-control"><%:oLoaiDulieu.DMMoTa%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="DMSTT" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-10">
                <input type="text" name="DMSTT" id="DMSTT" placeholder="Nhập dmstt" value="<%:oLoaiDulieu.DMSTT%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="DMVietTat" class="col-sm-2 control-label">Viết tắt</label>
            <div class="col-sm-10">
                <input type="text" name="DMVietTat" id="DMVietTat" placeholder="Nhập dmviettat" value="<%:oLoaiDulieu.DMVietTat%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="UrlList" class="col-sm-2 control-label">UrlList</label>
            <div class="col-sm-10">
                <input type="text" name="UrlList" id="UrlList" placeholder="Nhập urllist" value="<%:oLoaiDulieu.UrlList%>" class="form-control" />
            </div>
        </div>

        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oLoaiDulieu.ListFileAttach)%>'
            });
            $("#Title").focus();
            

            $("#frm-LoaiDulieu").validate({
                rules: {
                    Title: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/QTQuyTrinhBT/LoaiDulieu/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-LoaiDulieu").trigger("click");
                            $(form).closeModal();
                            $('#tblLoaiDulieu').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
        });
    </script>
</body>
</html>
