using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortalAPP.LoaiDulieu
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {

        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            LoaiDulieuDA oLoaiDulieuDA = new LoaiDulieuDA();
            LoaiDulieuItem oLoaiDulieuItem = new LoaiDulieuItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oLoaiDulieuDA.GetListJson(new LoaiDulieuQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw, 
                        oLoaiDulieuDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oLoaiDulieuDA.GetListJson(new LoaiDulieuQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLoaiDulieuItem = oLoaiDulieuDA.GetByIdToObject<LoaiDulieuItem>(oGridRequest.ItemID);
                        oLoaiDulieuItem.UpdateObject(context.Request);
                        oLoaiDulieuDA.UpdateObject<LoaiDulieuItem>(oLoaiDulieuItem);
                    }
                    else
                    {
                        oLoaiDulieuItem.UpdateObject(context.Request);
                        oLoaiDulieuDA.UpdateObject<LoaiDulieuItem>(oLoaiDulieuItem);
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    string outb = oLoaiDulieuDA.DeleteObject(oGridRequest.ItemID);
                    if (string.IsNullOrEmpty(outb))
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb;
                    break;
                case "DELETE-MULTI":
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oLoaiDulieuDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                     oLoaiDulieuDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLoaiDulieuItem = oLoaiDulieuDA.GetByIdToObjectSelectFields<LoaiDulieuItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oLoaiDulieuItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}