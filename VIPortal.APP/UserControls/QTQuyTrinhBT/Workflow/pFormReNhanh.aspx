﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormReNhanh.aspx.cs" Inherits="VIPortal.APP.UserControls.QTQuyTrinhBT.Workflow.pFormReNhanh" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frm-ReNhanh" class="form-horizontal">
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" name="ReNhanhItemId" id="ReNhanhItemId" value="<%=oReNhanhItem.ReNhanhItemId %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Nhãn</label>
            <div class="col-sm-10">
              <input type="text" name="Title" id="Title" class="form-control" value="<%=oReNhanhItem.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="MaTrangThai" class="col-sm-2 control-label">Trạng thái tiếp theo</label>
            <div class="col-sm-10">
                <select data-selected="<%:oReNhanhItem.MaTrangThai%>" data-url="/UserControls/AdminCMS/DMTrangThai/pAction.asp?do=QUERYDATA" data-place="Chọn trạng thái" name="MaTrangThai" id="MaTrangThai" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <label for="MaTrangThaiReturn" class="col-sm-2 control-label">Người dùng trả về</label>
            <div class="col-sm-10">
                <select data-selected="<%:oReNhanhItem.MaTrangThaiReturn%>" data-url="/UserControls/AdminCMS/DMTrangThai/pAction.asp?do=QUERYDATA" data-place="Chọn trạng thái" name="MaTrangThaiReturn" id="MaTrangThaiReturn" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <label for="lstNguoiDung" class="col-sm-2 control-label">Cán bộ xử lý</label>
            <div class="col-sm-10">
                <div class="input-group mb-3">
                    <input type="text" disabled="disabled" value="<%=string.Join(",", oReNhanhItem.lstNguoiDung.Select(x=>x.Title)) %>"  id="UserChoie" class="form-control" />
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary btnfrm" data-do="CHONNGUOI" title="Chọn người dùng" size="1240"  url="/UserControls/AdminCMS/LUser/pFormChoieUsers.aspx" id="btnChonNguoi" type="button">Chọn người dùng</button>
                    </div>
                </div>
                <input type="hidden" name="lstNguoiDung_Value" id="lstNguoiDung_Value" value="<%=string.Join(",", oReNhanhItem.lstNguoiDung.Select(x=>x.ID)) %>" />
                <textarea id="lstNguoiDung" name="lstNguoiDung" class="form-control"><%=Newtonsoft.Json.JsonConvert.SerializeObject(oReNhanhItem.lstNguoiDung) %></textarea>
            </div>
        </div>

    </form>

    <script type="text/javascript">
        $(document).ready(function () {

            //xử lý set giá trị ở đây trước.
            objIndex = dataReNhanh.findIndex((obj => obj.ReNhanhItemId == $("#ReNhanhItemId").val()));
            if (objIndex > -1) {
                $("#frm-ReNhanh #Title").val(dataReNhanh[objIndex].Title);
                $("#frm-ReNhanh #MaTrangThai").attr("data-selected", dataReNhanh[objIndex].MaTrangThai);
                $("#frm-ReNhanh #MaTrangThaiReturn").attr("data-selected", dataReNhanh[objIndex].MaTrangThaiReturn);
                var blkstr = $.map(dataReNhanh[objIndex].lstNguoiDung, function (val, index) {
                    return val.ID;
                }).join(",");
                $("#frm-ReNhanh #lstNguoiDung_Value").val(blkstr);
                var blkstrTitle = $.map(dataReNhanh[objIndex].lstNguoiDung, function (val, index) {
                    return val.Title;
                }).join(",");
                $("#frm-ReNhanh #UserChoie").val(blkstrTitle);
                $("#frm-ReNhanh #lstNguoiDung").val(JSON.stringify(dataReNhanh[objIndex].lstNguoiDung));
            }

            $("#MaTrangThai").smSelect2018V2({
                dropdownParent: "#frm-ReNhanh",
                TemplateValue: '{DMVietTat}'
            });
            $("#MaTrangThaiReturn").smSelect2018V2({
                dropdownParent: "#frm-ReNhanh",
                TemplateValue: '{DMVietTat}'
            });
            $("#frm-ReNhanh").validate({
                rules: {
                    MaTrangThai: "required"
                },
                messages: {
                    MaTrangThai: "Vui lòng chọn trạng thái"
                },
                submitHandler: function (form) {
                    objIndex = dataReNhanh.findIndex((obj => obj.ReNhanhItemId == $("#ReNhanhItemId").val()));
                    if (objIndex == -1) {
                        dataReNhanh.push({
                            Title: $("#frm-ReNhanh #Title").val(),
                            ReNhanhItemId: $("#frm-ReNhanh #ReNhanhItemId").val(),
                            MaTrangThai: $("#frm-ReNhanh #MaTrangThai").val(),
                            MaTrangThaiReturn: $("#frm-ReNhanh #MaTrangThaiReturn").val(),
                            lstNguoiDung: jQuery.parseJSON($("#frm-ReNhanh #lstNguoiDung").val())
                        });
                    } else {
                        dataReNhanh[objIndex] = {
                            Title: $("#frm-ReNhanh #Title").val(),
                            ReNhanhItemId: $("#frm-ReNhanh #ReNhanhItemId").val(),
                            MaTrangThai: $("#frm-ReNhanh #MaTrangThai").val(),
                            MaTrangThaiReturn: $("#frm-ReNhanh #MaTrangThaiReturn").val(),
                            lstNguoiDung: jQuery.parseJSON($("#frm-ReNhanh #lstNguoiDung").val())
                        };
                    }
                    console.log(dataReNhanh);
                    $('#Grid_ReNhanh').DataTable().clear().rows.add(dataReNhanh).draw();
                    //$('#Grid_ReNhanh').DataTable().reload();
                    $(form).closeModal();

                }
            });
            $("#frm-ReNhanh").viForm();
        });
    </script>
</body>
</html>
