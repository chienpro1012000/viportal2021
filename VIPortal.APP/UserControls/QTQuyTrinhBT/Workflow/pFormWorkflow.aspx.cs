using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortalAPP
{
    public partial class pFormWorkflow : pFormBase
    {
        public WorkflowItem oWorkflow { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oWorkflow = new WorkflowItem();
            if (ItemID > 0)
            {
                WorkflowDA oWorkflowDA = new WorkflowDA();
                oWorkflow = oWorkflowDA.GetByIdToObject<WorkflowItem>(ItemID);
            }
            else
            {
                if (!string.IsNullOrEmpty(Request["DoiTuong"]))
                    oWorkflow.DoiTuong = new SPFieldLookupValue(Request["DoiTuong"]);
            }
        }
    }
}