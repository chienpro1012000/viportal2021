﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pCauHinh.aspx.cs" Inherits="VIPortal.APP.UserControls.QTQuyTrinhBT.Workflow.pCauHinh" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script type="text/javascript">
        $(document).ready(function () {

            var $LstTT = jQuery.parseJSON( '<%=JsonTT%>');

            var $tblDanhMucChung = $("#tblWorkflow").viDataTable(
                {
                    "frmSearch": "WorkflowSearch",
                    data: { "DoiTuong": "<%=ItemID%>" },
                    "url": '/UserControls/QTQuyTrinhBT/Workflow/pAction.asp',
                    "aoColumns":
                        [
                            {
                                "mData": function (o) {
                                    return '<a data-id="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                                },
                                "sWidth": "150px",
                                "name": "Title", "sTitle": "Chức năng"
                            },{
                                "mData": null,
                                "sWidth": "90px",
                                "sTitle": "Trạng thái",
                                "mRender": function (o) {
                                    return o.TrangThaiHienTai.Title;
                                },
                            },  {
                                "mData": null,
                                "sTitle": "Trạng thái tiếp theo",
                                "mRender": function (o) {
                                    var $ThongTinChuyenTT = jQuery.parseJSON(o.ThongTinChuyenTT);
                                    var $html = '<ul>';
                                    $.each($ThongTinChuyenTT, function (index, value) {
                                        var titletrangthai = $LstTT.find(x => x.DMVietTat === value.MaTrangThai).Title;
                                        if (value.lstNguoiDung.length > 0) {
                                            $html += `<li>${titletrangthai}: ${$.map(value.lstNguoiDung, function (element, index) { return element.Title }).join(",")}</li>`;
                                        } else {
                                            var titletrangthaiReturn = $LstTT.find(x => x.DMVietTat === value.MaTrangThaiReturn).Title;
                                            $html += `<li>${titletrangthai}: Người dùng thao tác ${titletrangthaiReturn}</li>`;
                                        }
                                    });
                                    return $html;
                                },
                            }, {
                                "mData": null,
                                "bSortable": false,
                                "sWidth": "18px",
                                "className": "all",
                                "mRender": function (o) {
                                    return '<a class="btn default btn-xs purple act-edit" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>';
                                }
                            }, {
                                "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                                "mData": null,
                                "bSortable": false,

                                "className": "all",
                                "sWidth": "18px",
                                "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>';; }
                            }
                        ],
                    "aaSorting": [0, 'asc']
                });

            $("#frm-Workflow_view").validate({
                rules: {
                    Title: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/QTQuyTrinhBT/Workflow/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-Workflow").trigger("click");
                            $(form).closeModal();
                            $('#tblWorkflow').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
        });
    </script>
</head>
<body>
    <form id="frm-Workflow_view" class="form-horizontal">
        <input type="hidden" name="DoiTuong" id="DoiTuong" value="<%=ItemID %>" />
        <input type="hidden" name="do" value="<%=doAction %>" />
        <div class="form-group row">
            <div class="col-sm-12">
                <div role="body-data" data-title="" class="content_wp" data-action="/UserControls/QTQuyTrinhBT/Workflow/pAction.asp" data-form="/UserControls/QTQuyTrinhBT/Workflow/pFormWorkflow.aspx">
                    <div class="clsmanager row">
                        <div class="col-sm-9">
                            <div id="WorkflowSearch" class="form-inline zonesearch">
                                <div class="form-group">
                                    <input type="hidden" name="do" value="QUERYDATA" />
                                    <label for="Keyword">Từ khóa</label>
                                    <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa" />
                                    <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title" />
                                </div>
                                <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <p class="text-right">
                                <button class="btn btn-primary act-add" data-doituong="<%=ItemID %>" type="button">Thêm mới</button>
                            </p>
                        </div>
                    </div>

                    <div class="clsgrid table-responsive">

                        <table id="tblWorkflow" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                        </table>

                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
    </form>
</body>
</html>
