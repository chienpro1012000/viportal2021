﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormThaoTac.aspx.cs" Inherits="VIPortal.APP.UserControls.QTQuyTrinhBT.Workflow.pFormThaoTac" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-ThaoTac" class="form-horizontal">
        <input type="hidden" name="LogIDDoiTuong" id="LogIDDoiTuong" value="<%=ItemID %>" />
        <input type="hidden" name="LogDoiTuong" id="LogDoiTuong" value="<%=UrlListFull %>" />
        <input type="hidden" name="DoiTuongTitle" id="DoiTuongTitle" value="<%=oLoaiDulieuJson.Title %>" />
        <input type="hidden" name="CreatedUser" id="CreatedUser" value="<%=CurentUser.ID %>" />
        <input type="hidden" name="LNotiNguoiGui" id="LNotiNguoiGui" value="<%=CurentUser.Title %>" />
        <input type="hidden" name="TrangThaiParent" id="TrangThaiParent" value="<%=TrangThaiHienTai_x003a_DMVietTat %>" />
        <input type="hidden" name="LogTrangThaiTiepTheo" id="LogTrangThaiTiepTheo" value="" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Trạng thái tiếp theo</label>
            <div class="col-sm-10">
                <%foreach (ViPortalData.ReNhanhItem oReNhanhItem in lstReNhanhItem)
                {%>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="TrangThaiChoice" data-MaTrangThai="<%=oReNhanhItem.MaTrangThai %>" id="TrangThai<%=oReNhanhItem.MaTrangThai %>" value="TrangThai<%=oReNhanhItem.MaTrangThai %>" />
                    <label class="form-check-label" for="TrangThai<%=oReNhanhItem.MaTrangThai %>"><%=oReNhanhItem.Title %></label>
                </div>
                <%} %>
            </div>
        </div>
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Cán bộ xử lý</label>
            <div class="col-sm-10">
                <%foreach (ViPortalData.ReNhanhItem oReNhanhItem in lstReNhanhItem)
                {%>
                <div class="GroupUser" data-group="<%=oReNhanhItem.MaTrangThai %>" style="display:none;">
                    <%foreach (ViPortal_Utils.Base.LookupData oLookupData in oReNhanhItem.lstNguoiDung)
                {%>
                    <div class="form-check form-check-inline">
                        <input data-Value="<%=oLookupData.ID %>;#<%=oLookupData.Title %>" class="form-check-input" type="radio" name="UserChoice" id="User_<%=oReNhanhItem.MaTrangThai %>_<%=oLookupData.ID %>" value="User_<%=oReNhanhItem.MaTrangThai %>_<%=oLookupData.ID %>" />
                        <label class="form-check-label" for="User_<%=oReNhanhItem.MaTrangThai %>_<%=oLookupData.ID %>"><%=oLookupData.Title %></label>
                    </div>
                    <%} %>
                </div>
                <%} %>
            </div>
        </div>
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Ý kiến</label>
            <div class="col-sm-10">
                <textarea name="LogNoiDung" id="LogNoiDung" class="form-control"></textarea>
            </div>
        </div>
        <div class="form-group row" style="display:none;">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
    </form>
    <script type="text/javascript">
        function EventShowUser() {
            var $radioValue = $("input[name='TrangThaiChoice']:checked");
            var MaTrangThai = $radioValue.attr("data-MaTrangThai");
            $("#LogTrangThaiTiepTheo").val(MaTrangThai);
            $(".GroupUser").hide();
            var $groupusershow = $(".GroupUser[data-group='" + MaTrangThai + "']");
            $groupusershow.show();
            $groupusershow.find('input[type=radio]:first').prop('checked', true);
        }
        $(document).ready(function () {
            ///set gia trị default cho các thông tin.
            //TrangThaiChoice
            $("input:radio[name=TrangThaiChoice]:first").prop('checked', true);
            $("input:radio[name=UserChoice]:first").prop('checked', true);
            EventShowUser();
            $('input[type=radio][name=TrangThaiChoice]').change(function () {
                EventShowUser();
            });
            $("#FileAttach").regFileUpload({
                files: '[]'
            });
            $("#LogNoiDung").focus();
            $("#frm-ThaoTac").validate({
                rules: {
                    TrangThaiChoice: { // <- NAME of every radio in the same group
                        required: true
                    },
                    UserChoice: { // <- NAME of every radio in the same group
                        required: true
                    }
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    var DataPost = $(form).siSerializeArray();
                    DataPost["UserNhanXuLyText"] = $("#frm-ThaoTac input[name='UserChoice']:checked").attr("data-Value");
                    $.post("/UserControls/QTQuyTrinhBT/NhatKyXuLy/pAction.asp", DataPost, function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            $(form).closeModal();
                            $('#<%=gridid%>').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-ThaoTac").viForm();
        });
    </script>
</body>
</html>
