<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewWorkflow.aspx.cs" Inherits="VIPortalAPP.pViewWorkflow" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
               <%=oWorkflow.Title%>
            </div>
        </div>
		<div class="form-group">
	<label for="DoiTuong" class="col-sm-2 control-label">DoiTuong</label>
	<div class="col-sm-10">
		<%=oWorkflow.DoiTuong%>
	</div>
</div>
<div class="form-group">
	<label for="TrangThaiHienTai" class="col-sm-2 control-label">TrangThaiHienTai</label>
	<div class="col-sm-10">
		<%=oWorkflow.TrangThaiHienTai%>
	</div>
</div>
<div class="form-group">
	<label for="ThongTinChuyenTT" class="col-sm-2 control-label">ThongTinChuyenTT</label>
	<div class="col-sm-10">
		<%=oWorkflow.ThongTinChuyenTT%>
	</div>
</div>

		  <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-10">
               <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oWorkflow.Created)%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oWorkflow.Modified)%>
            </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-10">
                <%=oWorkflow.Author.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-10">
                <%=oWorkflow.Editor.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oWorkflow.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oWorkflow.ListFileAttach[i].Url %>"><%=oWorkflow.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>