﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils;
using ViPortal_Utils.Base;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.SPListCommon;

namespace VIPortal.APP.UserControls.QTQuyTrinhBT.Workflow
{
    public partial class pFormThaoTac : pFormBase
    {
        public WorkflowItem oWorkflow = new WorkflowItem();
        public string UrlListFull { get; set; }
        public WorkflowJson CurrentWF { get; set; }
        public string gridid { get; set; }
        public List<ReNhanhItem> lstReNhanhItem { get; set; }
        public LoaiDulieuJson oLoaiDulieuJson { get; set; }
        public string TrangThaiHienTai_x003a_DMVietTat { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oLoaiDulieuJson = new LoaiDulieuJson();
            CurrentWF = new WorkflowJson();
            lstReNhanhItem = new List<ReNhanhItem>();
            if (ItemID > 0)
            {
                DMTrangThaiDA oDMTrangThaiDA = new DMTrangThaiDA();
                List<DMTrangThaiJson> lstTrangThai = oDMTrangThaiDA.GetListJson(new DMTrangThaiQuery());
                WorkflowDA oWorkflowDA = new WorkflowDA();
                if (!string.IsNullOrEmpty(Request["UrlListFull"]))
                {
                    UrlListFull = Request["UrlListFull"];
                    //get thông tin đối tượng hiện tại.

                    if (!string.IsNullOrEmpty(Request["WFTrangThai"]))
                        TrangThaiHienTai_x003a_DMVietTat = Request["WFTrangThai"];
                    if (!string.IsNullOrEmpty(Request["gridid"]))
                        gridid = Request["gridid"];
                    LoaiDulieuDA oLoaiDulieuDA = new LoaiDulieuDA();
                    oLoaiDulieuJson = oLoaiDulieuDA.GetListJson(new LoaiDulieuQuery() { UrlList = new List<string>() { UrlListFull } }).FirstOrDefault();
                    if (oLoaiDulieuJson != null)
                        this.Page.Response.Write(string.Format("<!--\r\noLoaiDulieuJson:{0}\r\n-->", Newtonsoft.Json.JsonConvert.SerializeObject(oLoaiDulieuJson)));
                    else this.Page.Response.Write(string.Format("<!--\r\nQueryreturn:{0}\r\n-->", oLoaiDulieuDA.Queryreturn));
                    SPListDA<DanhMucSolr> oSPListDA = new SPListDA<DanhMucSolr>(UrlListFull, true);
                    ItemContent oItemContent = oSPListDA.GetInfo(ItemID);
                    //|-16-999||-16-999||-16-999||-16-999||-16-999||999-16-000||000-16-001||001-16-999||999-16-000||000-27-001|
                    LogTextLstObj oLogTextLstObj = new LogTextLstObj(oItemContent.LogText);
                    this.Page.Response.Write(string.Format("<!--\r\noLogTextLstObj:{0}\r\n-->", Newtonsoft.Json.JsonConvert.SerializeObject(oLogTextLstObj)));
                    //có cấu hình.
                    if (oLoaiDulieuJson != null)
                    {
                        List<WorkflowJson> lstWorkFlow = oWorkflowDA.GetListJson(new WorkflowQuery() { DoiTuong = oLoaiDulieuJson.ID });
                        CurrentWF = lstWorkFlow.FirstOrDefault(x => x.TrangThaiHienTai_x003a_DMVietTat.Title == TrangThaiHienTai_x003a_DMVietTat);
                        if (CurrentWF != null && !string.IsNullOrEmpty(CurrentWF.ThongTinChuyenTT))
                        {
                            lstReNhanhItem = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ReNhanhItem>>(CurrentWF.ThongTinChuyenTT);
                            lstReNhanhItem.Select(c => { c.MaTrangThai_Title = lstTrangThai.FirstOrDefault(x => x.DMVietTat == c.MaTrangThai)?.Title; return c; }).ToList();
                            foreach (var item in lstReNhanhItem)
                            {
                                if (!string.IsNullOrEmpty(item.MaTrangThaiReturn))
                                {
                                    LUserDA oLUserDA = new LUserDA();
                                    this.Page.Response.Write(string.Format("<!--\r\nMaTrangThaiReturn:{0}\r\n-->", item.MaTrangThaiReturn));
                                    int indexbuoc = oLogTextLstObj.LstLog.FindLastIndex(x => x.CurStep == item.MaTrangThaiReturn);
                                    if (indexbuoc > -1)
                                        item.lstNguoiDung = new List<ViPortal_Utils.Base.LookupData>() { new ViPortal_Utils.Base.LookupData() { ID = oLogTextLstObj.LstLog[indexbuoc].UserID, Title = oLUserDA.GetTitleByID(oLogTextLstObj.LstLog[indexbuoc].UserID) } };
                                    else
                                    {
                                        item.lstNguoiDung = new List<ViPortal_Utils.Base.LookupData>() { new ViPortal_Utils.Base.LookupData() { ID = CurentUser.ID, Title = CurentUser.Title } };
                                    }
                                }
                            }
                        }
                    }
                    else oLoaiDulieuJson = new LoaiDulieuJson() { Title = "Dữ liệu" };
                }

                if (lstReNhanhItem.Count == 0)
                {
                    if (TrangThaiHienTai_x003a_DMVietTat == "999")
                    {
                        //add default
                        lstReNhanhItem.Add(new ReNhanhItem()
                        {
                            MaTrangThai = "000",
                            MaTrangThai_Title = "Hủy xuất bản",
                            Title = "Hủy xuất bản",
                            lstNguoiDung = new List<ViPortal_Utils.Base.LookupData>() { new ViPortal_Utils.Base.LookupData() { ID = CurentUser.ID, Title = CurentUser.Title } }
                        });
                    }
                    else
                    {
                        //add default
                        lstReNhanhItem.Add(new ReNhanhItem()
                        {
                            MaTrangThai = "999",
                            MaTrangThai_Title = "Xuất bản",
                            Title = "Xuất bản",
                            lstNguoiDung = new List<ViPortal_Utils.Base.LookupData>() { new ViPortal_Utils.Base.LookupData() { ID = CurentUser.ID, Title = CurentUser.Title } }
                        });
                    }


                }

            }
        }
    }
}