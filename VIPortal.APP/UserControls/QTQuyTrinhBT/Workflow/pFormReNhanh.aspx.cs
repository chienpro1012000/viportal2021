﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData;

namespace VIPortal.APP.UserControls.QTQuyTrinhBT.Workflow
{
    public partial class pFormReNhanh : pFormBase
    {
        public ReNhanhItem oReNhanhItem { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oReNhanhItem = new ReNhanhItem();
            if (!string.IsNullOrEmpty(Request["renhanhitemid"]))
            {
                oReNhanhItem.ReNhanhItemId = Request["renhanhitemid"];
            }
        }
    }
}