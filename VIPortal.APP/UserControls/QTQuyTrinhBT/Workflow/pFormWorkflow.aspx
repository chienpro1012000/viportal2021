<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormWorkflow.aspx.cs" Inherits="VIPortalAPP.pFormWorkflow" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-Workflow" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oWorkflow.ID %>" />
        <input type="hidden" name="DoiTuong" id="DoiTuong" value="<%=oWorkflow.DoiTuong.LookupId %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oWorkflow.Title%>" />
            </div>
        </div>
       
        <div class="form-group row">
            <label for="TrangThaiHienTai" class="col-sm-2 control-label">Trạng thái</label>
            <div class="col-sm-10">
                <select data-selected="<%:oWorkflow.TrangThaiHienTai.LookupId%>" data-url="/UserControls/AdminCMS/DMTrangThai/pAction.asp?do=QUERYDATA" data-place="Chọn trạng thái" name="TrangThaiHienTai" id="TrangThaiHienTai" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <label for="ThongTinChuyenTT" class="col-sm-2 control-label">Rẽ nhánh</label>
            <div class="col-sm-10">
                <textarea name="ThongTinChuyenTT" id="ThongTinChuyenTT" style="display: none;" class="form-control"><%:oWorkflow.ThongTinChuyenTT%></textarea>
                <div role="body-data" data-title="" class="content_wp" data-action="/UserControls/QTQuyTrinhBT/Workflow/pAction.asp" data-form="/UserControls/QTQuyTrinhBT/Workflow/pFormReNhanh.aspx">
                    <div class="card card-default color-palette-box">
                        <div class="card-body">
                            <div class="clsmanager row">
                                <div class="col-sm-9">
                                    <div id="ReNhanhSearch" class="form-inline zonesearch">
                                        <div class="form-group">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <p class="text-right">
                                        <button class="btn btn-primary act-add" type="button">Thêm mới</button>
                                    </p>
                                </div>
                            </div>

                            <div class="clsgrid table-responsive">

                                <table id="Grid_ReNhanh" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <%--<div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>--%>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        var dataReNhanh = [];
        var dataReNhanhData = '<%=oWorkflow.ThongTinChuyenTT%>';
        if (dataReNhanhData != '') {
            dataReNhanh = jQuery.parseJSON(dataReNhanhData);
            console.log(dataReNhanh);
        }
        $(document).ready(function () {
            $("#TrangThaiHienTai").smSelect2018V2({
                dropdownParent: "#frm-Workflow"
            });
            //Grid_ReNhanh
            var oTblReport = $("#Grid_ReNhanh")
            oTblReport.DataTable({
                "data": dataReNhanh,
                searching: false,
                paging: false,
                info: false,
                "aoColumns":
                    [
                        {
                            "mData": function (o) {
                                return o.MaTrangThai;
                            },
                            "name": "MaTrangThai", "sTitle": "Trạng thái"
                        },
                        {
                            "mData": "Title",
                            "name": "Title", "sTitle": "Nhãn"
                        },
                        {
                            "name": "lstNguoiDung", "sTitle": "Cán bộ Phụ trách",
                            "mData": function (o) {
                                if (o.lstNguoiDung.length > 0) {
                                    return $.map(o.lstNguoiDung, function (element, index) { return element.Title }).join(",");
                                } else return '';
                            },
                        },{
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) {
                                return '<a class="btn default btn-xs purple act-edit" data-ItemID="<%=ItemID%>" data-ReNhanhItemId="' + o.ReNhanhItemId + '" href="javascript:;"><i class="fa fa-edit"></i></a>';
                            }
                        }
                    ]
            });
            
            $("#Title").focus();

            $("#frm-Workflow").validate({
                rules: {
                    Title: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    debugger;
                    $("#ThongTinChuyenTT").val(JSON.stringify(dataReNhanh));
                    $.post("/UserControls/QTQuyTrinhBT/Workflow/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-Workflow").trigger("click");
                            $(form).closeModal();
                            $('#tblWorkflow').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
        });
    </script>
</body>
</html>
