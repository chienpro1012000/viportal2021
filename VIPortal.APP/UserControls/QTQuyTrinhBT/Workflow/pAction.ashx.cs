using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortalAPP.Workflow
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {

        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            WorkflowDA oWorkflowDA = new WorkflowDA();
            WorkflowItem oWorkflowItem = new WorkflowItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oWorkflowDA.GetListJson(new WorkflowQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw, 
                        oWorkflowDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    break;
                case "QUERYDATANHANH":
                    List<ReNhanhItem> lstReNhanhItem = new List<ReNhanhItem>();
                    DataGridRender oGridQUERYDATANHANH = new DataGridRender(lstReNhanhItem, oGridRequest.Draw,
                        lstReNhanhItem.Count);
                    oResult.OData = oGridQUERYDATANHANH;
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oWorkflowDA.GetListJson(new WorkflowQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oWorkflowItem = oWorkflowDA.GetByIdToObject<WorkflowItem>(oGridRequest.ItemID);
                        oWorkflowItem.UpdateObject(context.Request);
                        oWorkflowDA.UpdateObject<WorkflowItem>(oWorkflowItem);
                    }
                    else
                    {
                        oWorkflowItem.UpdateObject(context.Request);
                        //xử lý ở đoạn này.

                        oWorkflowDA.UpdateObject<WorkflowItem>(oWorkflowItem);
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    //string outb = oWorkflowDA.DeleteObjectV2(oGridRequest.ItemID);
                    //if (string.IsNullOrEmpty(outb))
                    //    oResult.Message = "Xóa thành công";
                    //else oResult.Message = outb;
                    //break;
                    var outb = oWorkflowDA.DeleteObjectV2(oGridRequest.ItemID);                    
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    break;
                case "DELETE-MULTI":
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oWorkflowDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                     oWorkflowDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oWorkflowItem = oWorkflowDA.GetByIdToObjectSelectFields<WorkflowItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oWorkflowItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}