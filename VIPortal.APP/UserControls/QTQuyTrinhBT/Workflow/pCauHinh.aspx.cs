﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using VIPortalAPP;

namespace VIPortal.APP.UserControls.QTQuyTrinhBT.Workflow
{
    public partial class pCauHinh : pFormBase
    {
        public string JsonTT { get; set; }
        private List<DMTrangThaiJson> lstData;

        protected void Page_Load(object sender, EventArgs e)
        {
            DMTrangThaiDA oDMTrangThaiDA = new DMTrangThaiDA();
            lstData = oDMTrangThaiDA.GetListJson(new DMTrangThaiQuery());
            JsonTT = Newtonsoft.Json.JsonConvert.SerializeObject(lstData);
        }
    }
}