﻿using DevExpress.Spreadsheet;
using DevExpress.Web.Office;
using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using VIPortalAPP;
using ViPortalData;

namespace VIPortal.APP.UserControls
{
    public partial class DevExpressExcel : pFormBase
    {
        public string url = "";
        public string urlGoc = "";
        //public string doAction { get; set; }
        //public int ItemID { get; set; }
        public int KyBaoCao { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Request["do"]))
            {
                doAction = Request["do"];
            }
            if (!string.IsNullOrEmpty(Request["ItemID"]))
            {
                ItemID = Convert.ToInt32(Request["ItemID"]);
            }
            if (!string.IsNullOrEmpty(Request["KyBaoCao"]))
            {
                KyBaoCao = Convert.ToInt32(Request["KyBaoCao"]);
            }
            url = Request["urlfile"];
            
            if (!Page.IsPostBack)
            {
                if (doAction == "UPDATEBIEUMAU")
                {
                    //write biểu mẫu ra file và update.
                    var fullPath = "http://localhost:6003";
                    if (SPContext.Current != null)
                        fullPath = SPContext.Current.Site.Url;

                    using (SPSite cSite = new SPSite(fullPath))
                    {
                        using (SPWeb cWeb = cSite.OpenWeb("/htbaocao"))
                        {
                            urlGoc = url;
                            byte[] FileData = cWeb.GetFile(url).OpenBinary();
                            url = @"/Uploads/ajaxUpload/MauBCCT" + ItemID + string.Format("{0:ddMMyyHHmmss}", DateTime.Now) + ".xlsx";
                            string UrlFileBaoCaoChiTietNhap = Server.MapPath(url);
                            System.IO.File.WriteAllBytes(UrlFileBaoCaoChiTietNhap, FileData);
                            //wreite ra file này.
                        };
                    };
                }
                //string currentUserID = GenerateUniqueId();
                //var user = Users.Register(currentUserID);
                //ASPxSpreadsheet
                var templatePath = Server.MapPath(url);
                var documentId = GenerateUniqueId();
                ASPxSpreadsheet1.Open(templatePath);
                // user.DocumentIDs.Add(documentId);
                //spreadsheet.Open(documentId, DocumentFormat.Xlsx);

            }

        }
        protected void ASPxSpreadsheet1_Saving(object source, DocumentSavingEventArgs e)
        {
            byte[] documentContentAsByteArray = ASPxSpreadsheet1.SaveCopy(DocumentFormat.Xlsx);
            //làm gì đó ở đây nhé.
            if (doAction == "UPDATEKQ")
            {
                if (ItemID > 0)
                {
                    //update kết quả
                    //Thêm mới
                    KyBaoCaoDA kyBaoCaoDA = new KyBaoCaoDA();
                    KyBaoCaoItem kyBaoCaoItem = kyBaoCaoDA.GetByIdToObject<KyBaoCaoItem>(ItemID);
                    kyBaoCaoItem.TitleKetQua = "Báo cáo số liệu File excel";
                    kyBaoCaoItem.TrangThaiBaoCao = 6;
                    kyBaoCaoItem.ListFileRemove = kyBaoCaoItem.ListFileAttach.Select(x => x.Name).ToList();
                    kyBaoCaoItem.ListFileAttachAdd.Add(
                        new ViPortal_Utils.Base.FileAttach() { Name = Path.GetFileName(url), DataFile = documentContentAsByteArray });
                    kyBaoCaoDA.UpdateObject<KyBaoCaoItem>(kyBaoCaoItem);

                    #region Update nhận ký
                    NhatKyXuLyItem nhatKyXuLyItem = new NhatKyXuLyItem()
                    {
                        DoiTuongTitle = "Báo cáo Kỳ",
                        LogDoiTuong = "KyBaoCao",
                        LogIDDoiTuong = ItemID,
                        LogThaoTac = "CapNhatKetQuaBC",
                        TrangThaiParent = kyBaoCaoItem.TrangThaiBaoCao + "",
                        CreatedUser = new SPFieldLookupValue(CurentUser.ID, CurentUser.Title),
                        //LogNoiDung = context.Request["LogNoiDung"]
                    };
                    nhatKyXuLyItem.LogTrangThai = kyBaoCaoItem.TrangThaiBaoCao;
                    NhatKyXuLyDA nhatKyXuLyDA = new NhatKyXuLyDA();
                    nhatKyXuLyDA.UpdateObject<NhatKyXuLyItem>(nhatKyXuLyItem);
                    #endregion
                }
                else
                {

                }
            }
            else if (doAction == "UPDATEBIEUMAU")
            {
                //byte[] documentContentAsByteArray = ASPxSpreadsheet1.SaveCopy(DocumentFormat.Xlsx);
                //save lại file gốc
                var fullPath = "http://localhost:6003";
                if (SPContext.Current != null)
                    fullPath = SPContext.Current.Site.Url;

                using (SPSite cSite = new SPSite(fullPath))
                {
                    using (SPWeb cWeb = cSite.OpenWeb("/htbaocao"))
                    {
                        urlGoc = url;
                        string FileName = Path.GetFileName(urlGoc);
                        cWeb.AllowUnsafeUpdates = true;
                        SPFolder myLibrary = cWeb.Folders["Documents"];
                        SPFile spfile = myLibrary.Files.Add($"{FileName}", documentContentAsByteArray, true);
                        cWeb.AllowUnsafeUpdates = false;
                        //wreite ra file này.
                    };
                };
            }


            // Implement your custom logic to save a document to a custom storage
            e.Handled = false;
        }
        private static string GenerateUniqueId()
        {
            return Guid.NewGuid().ToString();
        }
    }
}