﻿<%@ page language="C#" autoeventwireup="true" codebehind="DevExpressExcel.aspx.cs" inherits="VIPortal.APP.UserControls.DevExpressExcel" %>

<%@ register assembly="DevExpress.Web.ASPxSpreadsheet.v21.1, Version=21.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxSpreadsheet" tagprefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form runat="server">

        <dx:aspxspreadsheet runat="server" id="ASPxSpreadsheet1" clientinstancename="spreadsheet" 
            height="700px" width="100%" OnSaving="ASPxSpreadsheet1_Saving" workdirectory="~/App_Data/WorkDirectory" showconfirmonlosingchanges="false" />
    </form>
</body>
</html>
