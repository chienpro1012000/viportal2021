﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_VanBanTaiLieu.ascx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.VanBanTaiLieu.UC_VanBanTaiLieu" %>
<div role="body-data" data-title="văn bản tài liệu" class="content_wp" data-action="/UserControls/AdminCMS/VanBanTaiLieu/pAction.asp" data-form="/UserControls/AdminCMS/VanBanTaiLieu/pFormVanBanTaiLieu.aspx" data-view="/UserControls/AdminCMS/VanBanTaiLieu/pViewVanBanTaiLieu.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="VanBanTaiLieuSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Titles,DMCoQuan,DMLoaiTaiLieu,VBSoKyHieu" />
                        </div>
                        <div class="form-group" style="display:none;">
                             <select data-selected="" data-url="/UserControls/AdminCMS/DMTaiLieu/pAction.asp?do=QUERYDATA" name="DMLoaiTaiLieu" id="DMLoaiTaiLieuSearch" data-place ="Chọn danh mục" class="form-control">
                </select>
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <button class="btn btn-primary act-add" data-per="01070402" type="button">Thêm mới</button>
                    </p>
                </div>

            </div>
             
            <div class="clsgrid table-responsive">

                <table id="tblVanBanTaiLieu" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        //DMQuyChe
        $("#DMLoaiTaiLieuSearch").smSelect2018V2({
            dropdownParent: ".content_wp"
        });
        var $tagvalue = $(".content_wp");
        var $tblVanBanTaiLieu = $("#tblVanBanTaiLieu").viDataTable(
            {
                "frmSearch": "VanBanTaiLieuSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "sWidth": "20px",
                            "mData": function (o) {
                                return '';
                            },
                            "name": "ID", "sTitle": "STT"
                        },
                        {
                            "sWidth": "70px",
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '" >' + o.VBSoKyHieu + '</a>';
                            },
                            "name": "VBSoKyHieu", "sTitle": "Số kí hiệu"
                        },
                        {
                            "mData": function (o) {
                                if (o.Title != null)
                                    return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                                else
                                    return "";
                            },
                            "name": "Title", "sTitle": "Tên"
                        },
                        {
                            "mData": function (o) {
                                return o.DMCoQuan.Title ;
                            },
                            "name": "DMCoQuan", "sTitle": "Đơn vị phát hành"
                        },
                        {
                            "mData": function (o) {
                                return  o.DMLoaiTaiLieu.Title ;
                            },
                            "name": "DMLoaiTaiLieu", "sTitle": "Loại tài liệu"
                        },
                        {
                            "name": "VBNgayBanHanh", "sTitle": "Ngày ban hành",
                            "mData": function (o) {
                                return formatDate(o.VBNgayBanHanh);
                            },
                            "sWidth": "80px",
                        },
                        {
                            "name": "VBNgayHieuLuc", "sTitle": "Ngày hiệu lực",
                            "mData": function (o) {
                                return formatDate(o.VBNgayHieuLuc);
                            },
                            "sWidth": "80px",
                        },
                        {
                            "name": "WFTrangThaiText", "sTitle": "Trạng thái",
                            "mData": "WFTrangThaiText",
                            "sWidth": "80px",
                        },{
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            //visible: false,
                            "mRender": function (o) {
                                if (o._ModerationStatus == 0)

                                    return '<a data-per="01070404"  title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a data-per="01070404"  title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }
                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" data-WFTrangThai=' + o.WFTrangThai + ' title="Sửa" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        }, {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" data-do="DELETE-MULTI" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE" title="Xóa"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                "order": [5, 'desc']
            });
        $tblVanBanTaiLieu.on('order.dt search.dt', function () {
            $tblVanBanTaiLieu.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        });
        $tblVanBanTaiLieu.on('draw', function () {

            $("a[data-itemid='<%=ItemID%>']").parent().parent().addClass("clsdetail");
        });
    });
</script>
