﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData.VanBanTaiLieu;

namespace VIPortal.APP.UserControls.AdminCMS.VanBanTaiLieu
{

    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {

        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);
             
            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            VanBanTaiLieuDA oVanBanTaiLieuDA = new VanBanTaiLieuDA(UrlSite);
            VanBanTaiLieuItem oVanBanTaiLieuItem = new VanBanTaiLieuItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oVanBanTaiLieuDA.GetListJsonSolr(new VanBanTaiLieuQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oVanBanTaiLieuDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "VanBanTaiLieu", "QUERYDATA", 0, "Xem danh sách văn bản tài liệu");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataVanBanTaiLieu = oVanBanTaiLieuDA.GetListJson(new VanBanTaiLieuQuery(context.Request));
                    treeViewItems = oDataVanBanTaiLieu.Select(x => new TreeViewItem()
                    {
                        key = x.VBCoQuanBanHanh,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;

                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oVanBanTaiLieuDA.GetListJson(new VanBanTaiLieuQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oVanBanTaiLieuItem = oVanBanTaiLieuDA.GetByIdToObject<VanBanTaiLieuItem>(oGridRequest.ItemID);
                        oVanBanTaiLieuItem.UpdateObject(context.Request);
                        if (oVanBanTaiLieuDA.CheckExit(oGridRequest.ItemID, oVanBanTaiLieuItem.Title) == 0)
                        {
                            oVanBanTaiLieuDA.UpdateObject<VanBanTaiLieuItem>(oVanBanTaiLieuItem);
                            AddLog("UPDATE", "VanBanTaiLieu", "UPDATE", oGridRequest.ItemID, $"Cập nhật văn bản tài liệu{oVanBanTaiLieuItem.Title}");
                            oResult.Message = "Cập nhật thành công";
                        }
                        else
                        {
                            oResult = new ResultAction() { State = ActionState.Error, Message = "Bản ghi đã tồn tại" };
                            break;
                        }
                    }
                    else
                    {
                        oVanBanTaiLieuItem.UpdateObject(context.Request);
                        if (!string.IsNullOrEmpty(oVanBanTaiLieuItem.Title) && oVanBanTaiLieuDA.GetCount(new VanBanTaiLieuQuery() { VBSoKyHieu = oVanBanTaiLieuItem.VBSoKyHieu }) > 0 )
                        {
                            oResult = new ResultAction() { State = ActionState.Error, Message = "Bản ghi đã tồn tại" };
                            break;
                        }
                        oVanBanTaiLieuDA.UpdateObject<VanBanTaiLieuItem>(oVanBanTaiLieuItem);
                        AddLog("UPDATE", "VanBanTaiLieu", "ADD", oGridRequest.ItemID, $"Thêm mới văn bản tài liệu{oVanBanTaiLieuItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oVanBanTaiLieuDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "VanBanTaiLieu", "DELETE", oGridRequest.ItemID, $"Xóa văn bản tài liệu{outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    AddLog("DELETE-MULTI", "VanBanTaiLieu", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều văn bản tài liệu");
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oVanBanTaiLieuDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oVanBanTaiLieuDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "VanBanTaiLieu", "APPROVED", oGridRequest.ItemID, $"Duyệt văn bản tài liệu{oVanBanTaiLieuDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oVanBanTaiLieuDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "VanBanTaiLieu", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt văn bản tài liệu{oVanBanTaiLieuDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oVanBanTaiLieuItem = oVanBanTaiLieuDA.GetByIdToObjectSelectFields<VanBanTaiLieuItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oVanBanTaiLieuItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
                case "SENDMAIL":
                    List<string> listMailTo = context.Request["lmailto"].Split(';').Where(x => x.Length > 0).ToList();
                    var link = context.Request["link"];
                    var subject = context.Request["subject"];
                    var content = context.Request["content"];
                    content += "Chi tiết văn bản tài liệu <a href=\"" + link + "\">xem tại đây</a>";
                    var body = content;
                    SendMail(subject, body, listMailTo);
                    oResult.Message = string.Format("Đã chia sẻ văn bản thành công");
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}