﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewVanBanTaiLieu.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.VanBanTaiLieu.pViewVanBanTaiLieu" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên</label>
            <div class="col-sm-10">
                <%=oVanBanTaiLieu.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="VBSoKyHieu" class="col-sm-2 control-label">Số kí hiệu</label>
            <div class="col-sm-4">
                <%=oVanBanTaiLieu.VBSoKyHieu%> 
            </div>
            <label for="VBNguoiKy" class="col-sm-2 control-label">Người kí</label>
            <div class="col-sm-4">
                <%=oVanBanTaiLieu.VBNguoiKy%>
            </div>
        </div>

        <div class="form-group row">
            <label for="VBNgayBanHanh" class="col-sm-2 control-label">Ngày ban hành</label>
            <div class="col-sm-4">
                <%:string.Format("{0:dd-MM-yyyy}", oVanBanTaiLieu.VBNgayBanHanh)%>
            </div>
            <label for="VBNgayHieuLuc" class="col-sm-2 control-label">Ngày hiệu lực</label>
            <div class="col-sm-4">
                <%:string.Format("{0:dd-MM-yyyy}", oVanBanTaiLieu.VBNgayHieuLuc)%>
            </div>
        </div>
        <div class="form-group row">
            <label for="VBTrichYeu" class="col-sm-2 control-label">Trích yếu</label>
            <div class="col-sm-10">
                <%=oVanBanTaiLieu.VBTrichYeu%>
            </div>
        </div>
        <div class="form-group row">
            <label for="DMLoaiTaiLieu" class="col-sm-2 control-label">Loại tài liệu</label>
            <div class="col-sm-4">
                <%:oVanBanTaiLieu.DMLoaiTaiLieu.LookupId%>
            </div>
            <label for="DMCoQuan" class="col-sm-2 control-label">Cơ quan ban hành</label>
            <div class="col-sm-4">
                <%:oVanBanTaiLieu.DMCoQuan.LookupId%>
            </div>
        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oVanBanTaiLieu.ListFileAttach.Count;i++){ %>
                <div>
                    <a href="<%=oVanBanTaiLieu.ListFileAttach[i].Url %>"><%=oVanBanTaiLieu.ListFileAttach[i].Name%></a><%if(oVanBanTaiLieu.ListFileAttach[i].Name.ToLower().EndsWith(".docx")){ %><a target="_blank" class="clslinkedit" style="padding-left:3px;" href="/noidung/_layouts/15/WopiFrame.aspx?sourcedoc=<%=oVanBanTaiLieu.ListFileAttach[i].Url %>&file=<%=oVanBanTaiLieu.ListFileAttach[i].Name%>&action=default"><i class="far fa-edit"></i></a><%} %>
                </div>
                <%} %>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}", oVanBanTaiLieu.Created)%>
            </div>
            <label class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}",oVanBanTaiLieu.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
                <%=oVanBanTaiLieu.Author.LookupValue%>
            </div>
            <label class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
                <%=oVanBanTaiLieu.Editor.LookupValue%>
            </div>
        </div>
    </form>
</body>
</html>
