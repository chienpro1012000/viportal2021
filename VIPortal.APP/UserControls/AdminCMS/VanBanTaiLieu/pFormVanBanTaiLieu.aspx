﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormVanBanTaiLieu.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.VanBanTaiLieu.pFormVanBanTaiLieu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-VanBanTaiLieu" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oVanBanTaiLieu.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên văn bản tài liệu</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" placeholder="Nhập tên văn bản tài liệu" value="<%=oVanBanTaiLieu.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="VBSoKyHieu" class="col-sm-2 control-label">Số kí hiệu</label>
            <div class="col-sm-4">
                <input type="text" name="VBSoKyHieu" id="VBSoKyHieu" class="form-control" placeholder="Nhập số ký hiệu" value="<%=oVanBanTaiLieu.VBSoKyHieu%>" />
            </div>
            <label for="VBNguoiKy" class="col-sm-2 control-label">Người kí</label>
            <div class="col-sm-4"> 
                <input type="text" name="VBNguoiKy" id="VBNguoiKy" class="form-control" placeholder="Nhập người ký" value="<%=oVanBanTaiLieu.VBNguoiKy%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="DMCoQuan" class="col-sm-2 control-label">Đơn vị phát hành</label>
            <div class="col-sm-10">
                <select data-selected="<%:oVanBanTaiLieu.DMCoQuan.LookupId%>" data-url="/UserControls/AdminCMS/CoQuanBanHanh/pAction.asp?do=QUERYDATA" data-place="Chọn cơ quan ban hành " name="DMCoQuan" id="DMCoQuan" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <label for="VBNgayBanHanh" class="col-sm-2 control-label">Ngày ban hành</label>
            <div class="col-sm-4">
                 <input type="text" name="VBNgayBanHanh" id="VBNgayBanHanh" placeholder="Nhập ngày ban hành" value="<%:string.Format("{0:dd/MM/yyyy}", oVanBanTaiLieu.VBNgayBanHanh)%>" class="form-control input-datetime" />
            </div>
            <label for="VBNgayHieuLuc" class="col-sm-2 control-label">Ngày hiệu lực</label>
            <div class="col-sm-4">
                <input type="text" name="VBNgayHieuLuc" id="VBNgayHieuLuc" placeholder="Nhập ngày hiệu lực" value="<%:string.Format("{0:dd/MM/yyyy}", oVanBanTaiLieu.VBNgayHieuLuc)%>" class="form-control input-datetime" />
            </div>
        </div>
        <div class="form-group row">
            <label for="VBTrichYeu" class="col-sm-2 control-label">Trích yếu</label>
            <div class="col-sm-10">
                <textarea name="VBTrichYeu" id="VBTrichYeu" placeholder="Nhập trích yếu" class="form-control"><%=oVanBanTaiLieu.VBTrichYeu%></textarea>
            </div>

        </div>
        <div class="form-group row">
            <label for="DMLoaiTaiLieu" class="col-sm-2 control-label">Loại tài liệu</label>
            <div class="col-sm-10">
                <select data-selected="<%:oVanBanTaiLieu.DMLoaiTaiLieu.LookupId%>" data-url="/UserControls/AdminCMS/LoaiTaiLieu/pAction.asp?do=QUERYDATA" data-place="Chọn loại tài liệu" name="DMLoaiTaiLieu" id="DMLoaiTaiLieu" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <label for="GhiChu" class="col-sm-2 control-label">Ghi chú</label>
            <div class="col-sm-10">
                <textarea class="form-control" name="LichGhiChu" id="LichGhiChu"></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-10">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $("#DMLoaiTaiLieu").smSelect2018V2({
                dropdownParent: "#frm-VanBanTaiLieu"
            });
            $("#DMCoQuan").smSelect2018V2({
                dropdownParent: "#frm-VanBanTaiLieu"
            });

            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oVanBanTaiLieu.ListFileAttach)%>'
             });
            
            //CKEDITOR.replace("ContentNews", {
            //    height: 200
            //});
            $("#btnImageNews").click(function () {
                $elementId = $(this);
                CKFinder.popup({
                    chooseFiles: true,
                    width: 800,
                    height: 600,
                    onInit: function (finder) {
                        finder.on('files:choose', function (evt) {
                            var file = evt.data.files.first();
                            //var output = document.getElementById(elementId);
                            //console.log(file.getUrl());
                            //$("#ImageNews").value(file.getUrl());
                            var urlfileimg = file.getUrl();
                            document.getElementById("ImageNews").value = urlfileimg;
                            document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                        });
                        finder.on('file:choose:resizedImage', function (evt) {
                            //var output = document.getElementById(elementId);
                            console.log(evt.data);
                            var urlfileimg = evt.data.resizedUrl;
                            document.getElementById("ImageNews").value = urlfileimg;
                            document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                        });
                    }
                });
            });
            $("#Title").focus();
            
            $(".form-group select").select2({
                dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
            });

            $("#frm-VanBanTaiLieu").validate({
                rules: {
                    Title: "required",
                    VBSoKyHieu: "required",
                    DMCoQuan: "required",
                    DMLoaiTaiLieu: "required"
                },
                messages: {
                    Title: "Vui lòng nhập Tên văn bản tài liệu",
                    VBSoKyHieu: "Vui lòng nhập số ký hiệu",
                    DMCoQuan: "Vui lòng chọn cơ quan ban hành",
                    DMLoaiTaiLieu: "Vui lòng chọn loại tài liệu",
                },

                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/VanBanTaiLieu/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-VanBanTaiLieu").trigger("click");
                            $(form).closeModal();
                            $('#tblVanBanTaiLieu').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-VanBanTaiLieu").viForm();
        });
    </script>
</body>
</html>

