﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.VanBanTaiLieu;

namespace VIPortal.APP.UserControls.AdminCMS.VanBanTaiLieu
{
    public partial class pFormVanBanTaiLieu : pFormBase
    {
        public VanBanTaiLieuItem oVanBanTaiLieu = new VanBanTaiLieuItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oVanBanTaiLieu = new VanBanTaiLieuItem();
            if (ItemID > 0)
            {
                VanBanTaiLieuDA oVanBanTaiLieuDA = new VanBanTaiLieuDA(UrlSite);
                oVanBanTaiLieu = oVanBanTaiLieuDA.GetByIdToObject<VanBanTaiLieuItem>(ItemID);
            }
        }
    }
}