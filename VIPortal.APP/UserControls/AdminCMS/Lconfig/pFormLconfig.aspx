﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormLconfig.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.Lconfig.pFormLconfig" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-Lconfig" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oLconfig.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" name="ConfigGroup" id="ConfigGroup" value="<%=oLconfig.ConfigGroup %>" />
        <div class="form-group row">
            <label for="ConfigGroup" class="col-sm-2 control-label">Nhóm cấu hình</label>
            <div class="col-sm-10">
                <input type="text" disabled="disabled" name="ConfigGroupValue" id="ConfigGroupValue" placeholder="Nhập nhóm cấu hình" value="<%:oLconfig.ConfigGroup%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" placeholder="Nhập tiêu đề" value="<%=oLconfig.Title%>" />
            </div>
        </div>

        <div class="form-group row">
            <label for="ConfigType" class="col-sm-2 control-label">Cấu hình</label>
            <div class="col-sm-10">
                <input type="text" name="ConfigType" id="ConfigType" placeholder="Nhập kiểu cấu hình" value="<%:oLconfig.ConfigType%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="ConfigValue" class="col-sm-2 control-label">Giá trị cấu hình</label>
            <div class="col-sm-10">
                <input type="text" name="ConfigValue" id="ConfigValue" placeholder="Nhập giá trị cấu hình" value="<%:oLconfig.ConfigValue%>" class="form-control" />
            </div>

        </div>
        <div class="form-group row">
            <label for="ConfigValueMulti" class="col-sm-2 control-label">Cấu hình nội dung</label>
            <div class="col-sm-10">
                <textarea name="ConfigValueMulti" id="ConfigValueMulti" placeholder="Nhập giá trị cấu hình nội dung" class="form-control" ><%:oLconfig.ConfigValueMulti%></textarea>
            </div>
            
        </div>
        
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#LookUp").smSelect2018V2({
                dropdownParent: "#frm-Lconfig"
            });
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oLconfig.ListFileAttach)%>'
            });
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $(".form-group select").select2({
                dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
            });

            $("#frm-Lconfig").validate({
                rules: {
                    Title: "required",
                    ConfigGroup: "required",
                    ConfigType: "required"
                },
                messages: {
                    Title: "Nhập nhập tiêu đề",
                    ConfigGroup: "Nhập nhóm cấu hình",
                    ConfigType: "Nhập kiểu cấu hình",
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/Lconfig/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-Lconfig").trigger("click");
                            $(form).closeModal();
                            $('#tblLconfig').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-Lconfig").viForm();
        });
    </script>
</body>
</html>

