﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using VIPortalAPP;
using ViPortalData.Lconfig;

namespace VIPortal.APP.UserControls.AdminCMS.Lconfig
{
    public partial class pFormCauHinhBC : pFormBase
    {
        public LconfigItem oLconfig { get; set; }
        public ConfigAPI oConfigAPI { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oLconfig = new LconfigItem();
            oConfigAPI = new ConfigAPI();
            if (ItemID > 0)
            {
                MenuQuanTriDA menuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem menuQuanTriItem = menuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(ItemID);
                oLconfig.Title = "Cấu hình báo cáo";
                oLconfig.ConfigType = "BaoCao_" + ItemID;
                oLconfig.ConfigGroup = "CauHinhBaoCao";

                LconfigDA lconfigDA = new LconfigDA();
                LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_" + ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
                if (lconfigJson != null && lconfigJson.ID > 0)
                {
                    oLconfig.ID = lconfigJson.ID;
                }                
                if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
                {
                    oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
                }
            }
        }
    }
}