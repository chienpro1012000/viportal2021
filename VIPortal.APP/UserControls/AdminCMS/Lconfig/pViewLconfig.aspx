﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewLconfig.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.Lconfig.pViewLconfig" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Title</label>
            <div class="col-sm-10">
                <%=oLconfig.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="ConfigGroup" class="col-sm-2 control-label">Config Group</label>
            <div class="col-sm-10">
               <%:oLconfig.ConfigGroup%>
            </div>
        </div>
        <div class="form-group row">
            <label for="ConfigType" class="col-sm-2 control-label">Config Type</label>
            <div class="col-sm-10">
               <%:oLconfig.ConfigType%>
            </div>
        </div>
        <div class="form-group row">
            <label for="ConfigValue" class="col-sm-2 control-label">Config Value</label>
            <div class="col-sm-10">
                <%:oLconfig.ConfigValue%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oLconfig.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oLconfig.ListFileAttach[i].Url %>"><%=oLconfig.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>      
		<div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%=string.Format("{0:dd-MM-yyyy}", oLconfig.Created)%>
            </div>
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}",oLconfig.Modified)%>
            </div>
        </div>

        <div class="form-group row">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
               <%=oLconfig.Author.LookupValue%>
            </div>
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
               <%=oLconfig.Editor.LookupValue%>
            </div>    
        </div>
    </form>
</body>
</html>
