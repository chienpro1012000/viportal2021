﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.Lconfig;

namespace VIPortal.APP.UserControls.AdminCMS.Lconfig
{
    public partial class pFormLconfig : pFormBase
    {
        public LconfigItem oLconfig { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oLconfig = new LconfigItem();
            if (ItemID > 0)
            {
                LconfigDA oLconfigDA = new LconfigDA();
                oLconfig = oLconfigDA.GetByIdToObject<LconfigItem>(ItemID);
            }
            else
            {
                if (!string.IsNullOrEmpty(Request["ConfigGroup"]))
                {
                    oLconfig.ConfigGroup = Request["ConfigGroup"];
                }
            }
        }
    }
}