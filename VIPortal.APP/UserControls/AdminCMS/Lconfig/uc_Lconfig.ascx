﻿<%@ control language="C#" autoeventwireup="true" codebehind="uc_Lconfig.ascx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.Lconfig.uc_Lconfig" %>
<div role="body-data" data-title="Cấu hình" class="content_wp" data-action="/UserControls/AdminCMS/Lconfig/pAction.asp" data-form="/UserControls/AdminCMS/Lconfig/pFormLconfig.aspx" data-view="/UserControls/AdminCMS/Lconfig/pViewLconfig.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-3">
                            <h5>Tham số cấu hình</h5>
                            <div id="treeThamSo"></div>
                        </div>
                <div class="col-sm-9">
                    <div class="clsmanager row">
                        <div class="col-sm-6">
                            <div id="LconfigSearch" class="form-inline zonesearch">
                                <div class="form-group">
                                    <input type="hidden" name="do" id="do" value="QUERYDATA" />
                                    <label for="Keyword">Từ khóa</label>
                                    <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                                    <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title" />
                                    <input type="hidden" name="ConfigGroup" id="ConfigGroup" value="123123" />
                                </div>
                                <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <p class="text-right">
                                <button class="btn btn-primary act-add" data-per="080402" type="button" id="btnAddConfig">Thêm mới</button>
                                <!--<button class="btn btn-primary btnfrmnochek" id="btnTheoDoi" title="Theo dõi sử dụng Cấu hình" url="/UserControls/AdminCMS/LogHeThong/pViewLogByAction.aspx" size="1024" data-per="080102" type="button">Theo dõi</button>
                                <button class="btn btn-primary btnfrmnochek" id="btnGanQuyen" title="Gán Quyền" url="/UserControls/AdminCMS/Lconfig/frmGanQuyen.aspx" size="1024" data-per="080102" type="button">Gán quyền</button>-->
                            </p>
                        </div>
                    </div>

                    <div class="clsgrid table-responsive">

                        <table id="tblLconfig" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#treeThamSo').fancytree({
            //  extensions: ['contextMenu'],
            source: {
                url: "/UserControls/AdminCMS/Lconfig/pAction.asp?do=MENUTREE"
            },
            renderNode: function (event, data) {
                //console.log(data);
                var node = data.node;
                $(node.span).attr('data-id', data.node.key);

            },
            activate: function (event, data) {
                $("#ConfigGroup").val(data.node.key);
                $("#btnAddConfig").data('ConfigGroup', data.node.key);
                $("#btnTheoDoi").data('ConfigGroup', data.node.key);
                $('#tblLconfig').DataTable().ajax.reload();
            },

        });
        $(".fancytree-container").addClass("fancytree-connectors");



        var $tagvalue = $(".content_wp");
        var $tblDanhMucChung = $("#tblLconfig").viDataTable(
            {
                "frmSearch": "LconfigSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "mData": function (o) {
                                if (o.Title != null)
                                    return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                                else
                                    return "";
                            },
                            "name": "Title", "sTitle": "Tiêu đề"
                        },
                        {
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '" >' + o.ConfigType + '</a>';
                            },
                            "name": "ConfigType", "sTitle": "Kiểu cấu hình"
                        },
                        {
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '" >' + o.ConfigValue + '</a>';
                            },
                            "name": "ConfigValue", "sTitle": "Giá trị cấu hình"
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {
                                if (o._ModerationStatus == 0)

                                    return '<a data-per="080405" title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a data-per="080405" title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }
                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" title="Sửa" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        }, {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE" title="Xóa"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                "order": [[0, "asc"]]
            });
    });
</script>

