﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData;
using ViPortalData.Lconfig;

namespace VIPortal.APP.UserControls.AdminCMS.Lconfig
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            LconfigDA oLconfigDA = new LconfigDA();
            LconfigItem oLconfigItem = new LconfigItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oLconfigDA.GetListJson(new LconfigQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oLconfigDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "Lconfig", "QUERYDATA", 0, "Xem danh sách tham số hệ thống");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    List<TreeViewItem> treeViewItemsReturn = new List<TreeViewItem>();
                    string path_to_xml = HttpContext.Current.Server.MapPath("/UserControls/XMLData/ConfigGroup.xml");
                    var xdoc = XDocument.Load(path_to_xml);
                    var map = xdoc.Root.Elements()
                                       .ToDictionary(a => (string)a.Attribute("keyword"),
                                                     a => (string)a.Attribute("replaceWith"));


                    oResult.IsQuery = true;
                    var oDataLconfig = oLconfigDA.GetListJson(new LconfigQuery(context.Request));
                    List<string> GroupConfig = oDataLconfig.Select(x => x.ConfigGroup).Distinct().ToList();
                    var res = from element in oDataLconfig
                              group element by element.ConfigGroup
              into groups
                              select groups.OrderBy(p => p.ID).First();
                    treeViewItems = map.Select(x => new TreeViewItem()
                    {
                        key = x.Key,
                        title = x.Value
                    }).ToList();
                    foreach (var item in res)
                    {
                        if(treeViewItems.FindIndex(x=>x.key == item.ConfigGroup) == -1 && !item.ConfigGroup.Contains("Theme"))
                        {
                            treeViewItems.Add(new TreeViewItem()
                            {
                                key = item.ConfigGroup,
                                title = item.Title
                            });
                        }
                    }
                    foreach (var item in treeViewItems)
                    {
                        bool isAdd = true;
                        var ItemGroup = res.FirstOrDefault(x => x.ConfigGroup == item.key);
                        if(ItemGroup != null && ItemGroup.Permissions != null && ItemGroup.Permissions.Count > 0)
                        {
                            if( CurentUser.Permissions.FindIndex(x=>x.ID == ItemGroup.Permissions.FirstOrDefault().ID) == -1)
                            {
                                isAdd = false;
                            }
                        }
                        if (isAdd)
                        {
                            treeViewItemsReturn.Add(item);
                        }
                    }
                    oResult.OData = treeViewItemsReturn;
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oLconfigDA.GetListJson(new LconfigQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLconfigItem = oLconfigDA.GetByIdToObject<LconfigItem>(oGridRequest.ItemID);
                        oLconfigItem.UpdateObject(context.Request);
                        if (!string.IsNullOrEmpty(oLconfigItem.ConfigType) && oLconfigDA.GetCount(new LconfigQuery() { ConfigType = oLconfigItem.ConfigType, ItemIDNotGet = oLconfigItem.ID }) > 0)
                        {
                            oResult = new ResultAction() { State = ActionState.Error, Message = "Trùng cấu hình" };
                            break;
                        }
                        oLconfigDA.UpdateObject<LconfigItem>(oLconfigItem);
                        AddLog("UPDATE", "Lconfig", "UPDATE", oGridRequest.ItemID, $"Cập nhật tham số hệ thống{oLconfigItem.Title}");
                    }
                    else
                    {
                        oLconfigItem.UpdateObject(context.Request);
                        if (!string.IsNullOrEmpty(oLconfigItem.ConfigType) && oLconfigDA.GetCount(new LconfigQuery() { ConfigType = oLconfigItem.ConfigType }) > 0)
                        {
                            oResult = new ResultAction() { State = ActionState.Error, Message = "Trùng cấu hình" };
                            break;
                        }
                        oLconfigDA.UpdateObject<LconfigItem>(oLconfigItem);
                        AddLog("UPDATE", "Lconfig", "ADD", oGridRequest.ItemID, $"Thêm mới tham số hệ thống {oLconfigItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "CONFIGBC":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLconfigItem = oLconfigDA.GetByIdToObject<LconfigItem>(oGridRequest.ItemID);
                        oLconfigItem.UpdateObject(context.Request);
                        ConfigAPI oConfigAPI = new ConfigAPI();
                        oConfigAPI.UrlAPI = context.Request["UrlAPI"];
                        oConfigAPI.UserName = context.Request["UserName"];
                        oConfigAPI.Password = context.Request["Password"];
                        oLconfigItem.ConfigValueMulti = Newtonsoft.Json.JsonConvert.SerializeObject(oConfigAPI);
                        if (!string.IsNullOrEmpty(oLconfigItem.ConfigType) && oLconfigDA.GetCount(new LconfigQuery() { ConfigType = oLconfigItem.ConfigType, ItemIDNotGet = oLconfigItem.ID }) > 0)
                        {
                           oResult = new ResultAction() { State = ActionState.Error, Message = "Trùng cấu hình" };
                            break;
                        }
                        oLconfigDA.UpdateObject<LconfigItem>(oLconfigItem);
                        AddLog("UPDATE", "Lconfig", "UPDATE", oGridRequest.ItemID, $"Cập nhật tham số hệ thống{oLconfigItem.Title}");
                    }
                    else
                    {
                        oLconfigItem.UpdateObject(context.Request);
                        ConfigAPI oConfigAPI = new ConfigAPI();
                        oConfigAPI.UrlAPI = context.Request["UrlAPI"];
                        oConfigAPI.UserName = context.Request["UserName"];
                        oConfigAPI.Password = context.Request["Password"];
                        oLconfigItem.ConfigValueMulti = Newtonsoft.Json.JsonConvert.SerializeObject(oConfigAPI);
                        if (!string.IsNullOrEmpty(oLconfigItem.ConfigType) && oLconfigDA.GetCount(new LconfigQuery() { ConfigType = oLconfigItem.ConfigType }) > 0)
                        {
                            oResult = new ResultAction() { State = ActionState.Error, Message = "Trùng cấu hình" };
                            break;
                        }
                        oLconfigDA.UpdateObject<LconfigItem>(oLconfigItem);
                        AddLog("UPDATE", "Lconfig", "ADD", oGridRequest.ItemID, $"Thêm mới tham số hệ thống {oLconfigItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oLconfigDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "Lconfig", "DELETE", oGridRequest.ItemID, $"Xóa tham số hệ thống {outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    var outbmulti = "";
                    AddLog("DELETE-MULTI", "Lconfig", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều tham số hệ thống");
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oLconfigDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oLconfigDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "Lconfig", "APPROVED", oGridRequest.ItemID, $"Duyệt tham số hệ thống {oLconfigDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oLconfigDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "Lconfig", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt tham số hệ thống {oLconfigDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLconfigItem = oLconfigDA.GetByIdToObjectSelectFields<LconfigItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oLconfigItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}