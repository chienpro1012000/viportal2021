﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmGanQuyen.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.Lconfig.frmGanQuyen" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frm-LUserQuyen" class="form-horizontal">
        
        <div class="form-group row">
            <label for="Permissions" class="col-sm-2 control-label">Quyền</label>
            <div class="col-sm-10">
                <input type="hidden" name="Permissions" id="Permissions" value="" />
                <div id="treephanquyen" data-source="ajax" class="sampletree" style="max-height: 400px; overflow-y: auto;">
                        </div>
            </div>
        </div>
       
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            var $treePermis = $("#treephanquyen").fancytree({
                source: {
                    url: "/VIPortalAPI/api/LPermissions/QUERYTREE?ListPermissCheck="
                },
                checkbox: true,
                selectMode: 3,
                icon: false,
                beforeSelect: function (event, data) {

                },
                select: function (event, data) {

                },
            });
            $(".fancytree-container").addClass("fancytree-connectors");
           
            $("#frm-LUserQuyen").validate({
                rules: {
                },
                messages: {
                },
                submitHandler: function (form) {
                    var tree = $('#treephanquyen').fancytree('getTree');
                    var selKeys = $.map(tree.getSelectedNodes(), function (element, index) { return element.key }).join(",");
                    $("#Permissions").val(selKeys);
                    $.post("/UserControls/AdminCMS/LUser/pAction.ashx", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-LUser").trigger("click");
                            $(form).closeModal();
                            $('#tblLUser').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
        });
    </script>
</body>
</html>
