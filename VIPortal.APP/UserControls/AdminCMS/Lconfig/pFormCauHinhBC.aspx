﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormCauHinhBC.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.Lconfig.pFormCauHinhBC" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-Lconfig" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oLconfig.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" name="ConfigGroup" id="ConfigGroup" value="<%=oLconfig.ConfigGroup %>" />
        <input type="hidden" name="ConfigType" id="ConfigType" value="<%=oLconfig.ConfigType %>" />
        <input type="hidden" name="Title" id="Title" value="<%=oLconfig.Title %>" />
        
        <div class="form-group row">
            <label for="UrlAPI" class="col-sm-2 control-label">Url API</label>
            <div class="col-sm-10">
                <input type="text" name="UrlAPI" id="UrlAPI" placeholder="Url API Tích hợp" value="<%=oConfigAPI.UrlAPI%>" class="form-control" />
            </div>

        </div>
        <div class="form-group row">
            <label for="UserName" class="col-sm-2 control-label">UserName</label>
            <div class="col-sm-4">
                <input type="text" name="UserName" id="UserName" placeholder="Tài khoản xác thực" value="<%=oConfigAPI.UserName%>" class="form-control" />
            </div>
            <label for="Password" class="col-sm-2 control-label">UserName</label>
            <div class="col-sm-4">
                <input type="text" name="Password" id="Password" placeholder="Mật khẩu" value="<%=oConfigAPI.Password%>" class="form-control" />
            </div>

        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            
            $("#UrlAPI").focus();
            

            $("#frm-Lconfig").validate({
                rules: {
                    UrlAPI: "required",
                    UserName: "required",
                    Password: "required"
                },
                messages: {
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/Lconfig/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-Lconfig").trigger("click");
                            $(form).closeModal();
                            $('#tblLconfig').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-Lconfig").viForm();
        });
    </script>
</body>
</html>

