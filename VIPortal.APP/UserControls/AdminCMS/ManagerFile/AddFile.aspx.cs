﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;

namespace VIPortal.APP.UserControls.AdminCMS.ManagerFile
{
    public partial class AddFile : pFormBase
    {
        public string PathFileUpload { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            PathFileUpload = Request["PathFileUpload"];
        }
    }
}