﻿using CommonServiceLocator;
using Microsoft.SharePoint;
using SolrNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils;

namespace VIPortal.APP.UserControls.AdminCMS.ManagerFile
{
    public partial class UC_ManagerFile : BaseUC
    {
        private void UpdateListField(string UrlList)
        {
            Page.Response.Write(string.Format("UrlList:{0}", UrlList));
            string ListName = UrlList.Split('/').LastOrDefault();
            using (SPSite osite = new SPSite("http://localhost:6003/"))
            {
                string subsitefullpath = UrlList.Split(new[] { "/Lists" }, StringSplitOptions.None)[0];
                using (SPWeb web = osite.OpenWeb(subsitefullpath))
                {
                    SPList ListGoc = web.Lists.TryGetList(ListName);

                    foreach (SPWeb subweb in osite.RootWeb.Webs)
                    {
                        Page.Response.Write(string.Format("subweb.ServerRelativeUrl:{0}", subweb.ServerRelativeUrl));
                        if (subweb.ServerRelativeUrl != "/noidung" && subweb.ServerRelativeUrl != "/cms")
                        {
                            try
                            {
                                using (SPWeb subweb2 = osite.OpenWeb(subweb.ServerRelativeUrl + subsitefullpath))
                                {

                                    subweb2.AllowUnsafeUpdates = true;
                                    SPList NewList = subweb2.Lists.TryGetList(ListName);
                                    if (NewList == null)
                                    {
                                        subweb2.Lists.Add(ListGoc.Title, "", SPListTemplateType.GenericList);
                                        NewList = subweb2.Lists.TryGetList(ListName);
                                    }
                                    NewList.EnableModeration = ListGoc.EnableModeration;
                                    NewList.Update();
                                    foreach (SPField oField in ListGoc.Fields)
                                    {
                                        if (!NewList.Fields.ContainsField(oField.StaticName))
                                        {
                                            Console.WriteLine("==" + oField.StaticName + "==" + oField.TypeAsString);
                                            if (osite.RootWeb.AvailableFields.ContainsField(oField.StaticName))
                                            {
                                                NewList.Fields.Add(osite.RootWeb.AvailableFields.GetField(oField.StaticName));
                                            }
                                            else
                                            {
                                                //add field vao day.
                                                #region MyRegion
                                                //Console.WriteLine(oField.StaticName + "==" + oField.TypeAsString);
                                                string fldName;
                                                switch (oField.TypeAsString)
                                                {
                                                    case "ModStat":
                                                        NewList.EnableModeration = true;
                                                        NewList.Update();
                                                        break;
                                                    case "Text": //single text
                                                        NewList.Fields.Add(oField.StaticName, SPFieldType.Text, false);
                                                        break;
                                                    case "Note": //Multiline Text
                                                        SPFieldMultiLineText temp = (SPFieldMultiLineText)oField;
                                                        NewList.Fields.Add(oField.StaticName, SPFieldType.Note, false);
                                                        SPFieldMultiLineText mul = new SPFieldMultiLineText(NewList.Fields, oField.StaticName);
                                                        mul.RichText = temp.RichText;


                                                        //mul.RichTextMode = SPRichTextMode.Compatible;
                                                        mul.Update();
                                                        break;
                                                    case "HTML":
                                                        NewList.Fields.Add(oField.StaticName, SPFieldType.Note, false);
                                                        SPFieldMultiLineText mulhtml = new SPFieldMultiLineText(NewList.Fields, oField.StaticName);
                                                        mulhtml.RichText = true;
                                                        //mul.RichTextMode = SPRichTextMode.Compatible;
                                                        mulhtml.Update();
                                                        break;
                                                    case "Number":
                                                        fldName = NewList.Fields.Add(oField.StaticName, SPFieldType.Number, false);
                                                        SPFieldNumber fnumber = new SPFieldNumber(NewList.Fields, fldName);
                                                        fnumber.Description = oField.Description;
                                                        fnumber.Update();
                                                        break;
                                                    case "Lookup":
                                                    case "LookupMulti":
                                                        try
                                                        {
                                                            SPFieldLookup templk = (SPFieldLookup)oField;
                                                            Console.WriteLine(templk.LookupList);
                                                            SPList olistlk = ListGoc.ParentWeb.Lists.GetList(new Guid(templk.LookupList), false);
                                                            Console.WriteLine(olistlk.RootFolder.ServerRelativeUrl);
                                                            try
                                                            {
                                                                string urllistLookup = olistlk.RootFolder.ServerRelativeUrl.Split('/').LastOrDefault();

                                                                SPList newListLookup = NewList.ParentWeb.Lists.TryGetList(urllistLookup);
                                                                if (newListLookup != null)
                                                                {
                                                                    var userNameDn = NewList.Fields.AddLookup(templk.StaticName, newListLookup.ID, false);
                                                                    SPFieldLookup usernameField = (SPFieldLookup)NewList.Fields[userNameDn];
                                                                    usernameField.LookupField = newListLookup.Fields[templk.LookupField].StaticName;
                                                                    usernameField.AllowMultipleValues = templk.AllowMultipleValues;
                                                                    usernameField.Update();
                                                                }
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                //file.WriteLine(string.Format("Field: {0}, Eror: {1}", item.StaticName, ex.Message));
                                                                Console.WriteLine(string.Format("Field: {0}, Eror: {1}", oField.StaticName, ex.Message));
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {

                                                        }
                                                        break;
                                                    case "Boolean":
                                                        fldName = NewList.Fields.Add(oField.StaticName, SPFieldType.Boolean, false);
                                                        SPFieldBoolean bol = new SPFieldBoolean(NewList.Fields, fldName);
                                                        bol.DefaultValue = oField.DefaultValue;
                                                        bol.Update();
                                                        break;
                                                    case "DateTime":
                                                        fldName = NewList.Fields.Add(oField.StaticName, SPFieldType.DateTime, false);
                                                        SPFieldDateTime tem3 = (SPFieldDateTime)oField;
                                                        if (string.IsNullOrEmpty(oField.DefaultValue))
                                                        {
                                                            SPFieldDateTime fieldDateTime = new SPFieldDateTime(NewList.Fields, fldName);
                                                            if (tem3.DisplayFormat == SPDateTimeFieldFormatType.DateOnly)
                                                                fieldDateTime.DisplayFormat = SPDateTimeFieldFormatType.DateOnly;
                                                            else fieldDateTime.DisplayFormat = SPDateTimeFieldFormatType.DateTime;
                                                            fieldDateTime.DefaultValue = tem3.DefaultValue;
                                                            fieldDateTime.Update();
                                                        }
                                                        break;
                                                    case "Choice":
                                                        try
                                                        {
                                                            fldName = NewList.Fields.Add(oField.StaticName, SPFieldType.Choice, false);
                                                            SPFieldChoice oChoiceGoc = (SPFieldChoice)oField;
                                                            SPFieldChoice oChoice = (SPFieldChoice)NewList.Fields[oField.StaticName];
                                                            foreach (var itemvalue in oChoiceGoc.Choices)
                                                            {
                                                                oChoice.Choices.Add(itemvalue);
                                                            }
                                                            if (!string.IsNullOrEmpty(oChoiceGoc.DefaultValue))
                                                                oChoice.DefaultValue = oChoiceGoc.DefaultValue;
                                                            oChoice.Update();
                                                        }
                                                        catch (Exception ex)
                                                        {

                                                        }
                                                        break;
                                                }
                                                #endregion
                                            }
                                        }
                                    }
                                    subweb2.AllowUnsafeUpdates = false;

                                }
                            }
                            catch (Exception ex)
                            {
                                Page.Response.Write(string.Format("<!--Error:{0}", ex.Message + "\r\n-->" + ex.StackTrace));
                            }
                        }
                    }

                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request["doAction"]))
            {
                string DoAction = Request["doAction"];
                switch (DoAction)
                {
                    case "UpdateListField":
                        string UrlList = Request["UrlList"];
                        UpdateListField(UrlList);
                        break;
                    case "CreateSite":
                        string urlsite = Request["urlsite"];
                        if (!string.IsNullOrEmpty(urlsite))
                        {
                            //CreateSite(urlsite, urlsite.Substring(1));
                            CreateCopyList("/", urlsite);
                            //using (SPSite osite = new SPSite("http://localhost:6003/"))
                            //{
                            //    using (SPWeb web = osite.OpenWeb(urlsite))
                            //    {
                            //        web.AllowUnsafeUpdates = true;
                            //        SPFolder rootFolder = web.RootFolder;
                            //        rootFolder.WelcomePage = $"Pages/HomePortal.aspx";
                            //        rootFolder.Update();
                            //        web.AllowUnsafeUpdates = false;
                            //    }
                            //    using (SPWeb web = osite.OpenWeb(urlsite + "/cms"))
                            //    {
                            //        web.AllowUnsafeUpdates = true;
                            //        SPFolder rootFolder = web.RootFolder;
                            //        rootFolder.WelcomePage = $"Pages/tintuc.aspx";
                            //        rootFolder.Update();
                            //        web.AllowUnsafeUpdates = false;
                            //    }
                            //}
                            CreateCopyList("/cms", urlsite + "/cms");

                            CreateCopyList("/noidung", urlsite + "/noidung");

                            CreateFieldCopyList("/", urlsite);
                            CreateFieldCopyList("/cms", urlsite + "/cms");
                            CreateFieldCopyList("/noidung", urlsite + "/noidung");
                        }
                        break;
                    case "UpdateSolr":
                        var solrWorkerStatic = ServiceLocator.Current.GetInstance<ISolrOperations<DataSolr>>();
                        string urlList = Request["urlList"];
                        var fullPath = "http://localhost:6003" + urlList;
                        if (SPContext.Current != null)
                            fullPath = SPContext.Current.Site.Url + urlList;

                        using (SPSite cSite = new SPSite(fullPath))
                        {
                            using (SPWeb cWeb = cSite.OpenWeb())
                            {
                                //Su dung ham get GetList 
                                SPList ListConvert = cWeb.GetList(urlList);

                                foreach (SPListItem item in ListConvert.Items)
                                {
                                    DataSolr oDMSolr = new DataSolr(item);
                                    solrWorkerStatic.Add(oDMSolr);
                                    solrWorkerStatic.Commit();
                                    //Console.WriteLine(oDMSolr.Title);

                                }

                            };
                        };
                        break;
                }
            }
        }

        public void CreateSite(string urlSite, string Title)
        {



            Guid siteId = SPContext.Current.Site.ID;

            SPSecurity.RunWithElevatedPrivileges(delegate ()

            {

                using (SPSite spSite = new SPSite(siteId))

                {

                    using (SPWeb objSPWeb = spSite.RootWeb)

                    {

                        objSPWeb.AllowUnsafeUpdates = true;
                        //SPWeb oSPWeb = SPContext.Current.Site.RootWeb;

                        var WebDonvi = objSPWeb.Webs.FirstOrDefault(x => x.ServerRelativeUrl == urlSite);
                        if (WebDonvi == null)
                        {
                            objSPWeb.AllowUnsafeUpdates = true;
                            WebDonvi = objSPWeb.Webs.Add(urlSite.Substring(1), Title, Title, objSPWeb.Language, "CMSPUBLISHING#0", false, false);
                            objSPWeb.AllowUnsafeUpdates = false;
                        }
                        if (WebDonvi != null)
                        {
                            //WebDonvi.AllowUnsafeUpdates = true;
                            ////add thêm site cms và site con nữa.
                            //if (!WebDonvi.Webs.Names.Contains("cms"))
                            //{
                            //    WebDonvi.Webs.Add("cms", "Quản trị nội dung", "Quản trị nội dung", oweb.Language, "CMSPUBLISHING#0", true, false);
                            //}
                            //if (!WebDonvi.Webs.Names.Contains("noidung"))
                            //{
                            //    WebDonvi.Webs.Add("noidung", "Nội dung", "Nội dung", oweb.Language, "CMSPUBLISHING#0", true, false);
                            //}
                            //WebDonvi.AllowUnsafeUpdates = false;
                        }
                        //webApp.FormDigestSettings.Enabled = true;
                        objSPWeb.AllowUnsafeUpdates = false;

                    }

                }

            });
            //SPSecurity.RunWithElevatedPrivileges(delegate ()
            //{
            //Console.WriteLine("Tạo site " + Title);
            //using (SPSite oSite = SPContext.Current.Site.RootWeb.Site)
            //{
            //    using (SPWeb oweb = oSite.OpenWeb())
            //    {
            //oSite.FormDigestSettings.Enabled = false;

            //Microsoft.SharePoint.Administration.SPWebApplication webApp = oweb.Site.WebApplication;
            //webApp.FormDigestSettings.Enabled = false;


            // }
            // }
            //});
        }

        public List<string> LstREmove = new List<string>()
        {
            "appdata","Cache Profiles","Content and Structure Reports","Courses",
            "Device Channel","Long Running Operation Status",
            "Notification List","Quick Deploy Items","Reusable Content",
            "Sharing Links","Suggested Content Browser Locations","SinhVien",
            "TaxonomyHiddenList","User Information List",
            "Variation Labels", "Content type publishing error log","Device Channels","Relationships List","Translation Status",
        };

        public void CreateFieldCopyList(string urlwebNguon, string urlwebDich)
        {
            using (SPSite osite = new SPSite("http://localhost:6003/"))
            {
                using (SPWeb owebGoc = osite.OpenWeb(urlwebNguon))
                {
                    using (SPWeb owebDich = osite.OpenWeb(urlwebDich))
                    {
                        owebDich.AllowUnsafeUpdates = true;
                        foreach (SPList item in owebGoc.Lists)
                        {
                            if (item.BaseType == SPBaseType.GenericList && !LstREmove.Contains(item.Title) && owebDich.Lists.TryGetList(item.Title) != null)
                            {

                                SPList NewList = owebDich.Lists.TryGetList(item.Title);
                                Console.WriteLine(NewList.Title);
                                NewList.EnableModeration = item.EnableModeration;
                                NewList.Update();

                                foreach (SPField oField in item.Fields)
                                {
                                    if (!NewList.Fields.ContainsField(oField.StaticName))
                                    {
                                        Console.WriteLine("==" + oField.StaticName + "==" + oField.TypeAsString);
                                        if (owebGoc.AvailableFields.ContainsField(oField.StaticName))
                                        {
                                            NewList.Fields.Add(owebGoc.AvailableFields.GetField(oField.StaticName));
                                        }
                                        else
                                        {
                                            //add field vao day.
                                            #region MyRegion
                                            //Console.WriteLine(oField.StaticName + "==" + oField.TypeAsString);
                                            string fldName;
                                            switch (oField.TypeAsString)
                                            {
                                                case "ModStat":
                                                    NewList.EnableModeration = true;
                                                    NewList.Update();
                                                    break;
                                                case "Text": //single text
                                                    NewList.Fields.Add(oField.StaticName, SPFieldType.Text, false);
                                                    break;
                                                case "Note": //Multiline Text
                                                    SPFieldMultiLineText temp = (SPFieldMultiLineText)oField;
                                                    NewList.Fields.Add(oField.StaticName, SPFieldType.Note, false);
                                                    SPFieldMultiLineText mul = new SPFieldMultiLineText(NewList.Fields, oField.StaticName);
                                                    mul.RichText = temp.RichText;


                                                    //mul.RichTextMode = SPRichTextMode.Compatible;
                                                    mul.Update();
                                                    break;
                                                case "HTML":
                                                    NewList.Fields.Add(oField.StaticName, SPFieldType.Note, false);
                                                    SPFieldMultiLineText mulhtml = new SPFieldMultiLineText(NewList.Fields, oField.StaticName);
                                                    mulhtml.RichText = true;
                                                    //mul.RichTextMode = SPRichTextMode.Compatible;
                                                    mulhtml.Update();
                                                    break;
                                                case "Number":
                                                    fldName = NewList.Fields.Add(oField.StaticName, SPFieldType.Number, false);
                                                    SPFieldNumber fnumber = new SPFieldNumber(NewList.Fields, fldName);
                                                    fnumber.Description = oField.Description;
                                                    fnumber.Update();
                                                    break;
                                                case "Lookup":
                                                case "LookupMulti":
                                                    try
                                                    {
                                                        SPFieldLookup templk = (SPFieldLookup)oField;
                                                        Console.WriteLine(templk.LookupList);
                                                        SPList olistlk = item.ParentWeb.Lists.GetList(new Guid(templk.LookupList), false);
                                                        Console.WriteLine(olistlk.RootFolder.ServerRelativeUrl);
                                                        try
                                                        {
                                                            string urllistLookup = olistlk.RootFolder.ServerRelativeUrl.Split('/').LastOrDefault();

                                                            SPList newListLookup = NewList.ParentWeb.Lists.TryGetList(urllistLookup);
                                                            if (newListLookup != null)
                                                            {
                                                                var userNameDn = NewList.Fields.AddLookup(templk.StaticName, newListLookup.ID, false);
                                                                SPFieldLookup usernameField = (SPFieldLookup)NewList.Fields[userNameDn];
                                                                usernameField.LookupField = newListLookup.Fields[templk.LookupField].StaticName;
                                                                usernameField.AllowMultipleValues = templk.AllowMultipleValues;
                                                                usernameField.Update();
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            //file.WriteLine(string.Format("Field: {0}, Eror: {1}", item.StaticName, ex.Message));
                                                            Console.WriteLine(string.Format("Field: {0}, Eror: {1}", oField.StaticName, ex.Message));
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {

                                                    }
                                                    break;
                                                case "Boolean":
                                                    fldName = NewList.Fields.Add(oField.StaticName, SPFieldType.Boolean, false);
                                                    SPFieldBoolean bol = new SPFieldBoolean(NewList.Fields, fldName);
                                                    bol.DefaultValue = oField.DefaultValue;
                                                    bol.Update();
                                                    break;
                                                case "DateTime":
                                                    fldName = NewList.Fields.Add(oField.StaticName, SPFieldType.DateTime, false);
                                                    SPFieldDateTime tem3 = (SPFieldDateTime)oField;
                                                    if (string.IsNullOrEmpty(oField.DefaultValue))
                                                    {
                                                        SPFieldDateTime fieldDateTime = new SPFieldDateTime(NewList.Fields, fldName);
                                                        if (tem3.DisplayFormat == SPDateTimeFieldFormatType.DateOnly)
                                                            fieldDateTime.DisplayFormat = SPDateTimeFieldFormatType.DateOnly;
                                                        else fieldDateTime.DisplayFormat = SPDateTimeFieldFormatType.DateTime;
                                                        fieldDateTime.DefaultValue = tem3.DefaultValue;
                                                        fieldDateTime.Update();
                                                    }
                                                    break;
                                                case "Choice":
                                                    try
                                                    {
                                                        fldName = NewList.Fields.Add(oField.StaticName, SPFieldType.Choice, false);
                                                        SPFieldChoice oChoiceGoc = (SPFieldChoice)oField;
                                                        SPFieldChoice oChoice = (SPFieldChoice)NewList.Fields[oField.StaticName];
                                                        foreach (var itemvalue in oChoiceGoc.Choices)
                                                        {
                                                            oChoice.Choices.Add(itemvalue);
                                                        }
                                                        if (!string.IsNullOrEmpty(oChoiceGoc.DefaultValue))
                                                            oChoice.DefaultValue = oChoiceGoc.DefaultValue;
                                                        oChoice.Update();
                                                    }
                                                    catch (Exception ex)
                                                    {

                                                    }
                                                    break;
                                            }
                                            #endregion
                                        }
                                    }
                                }
                            }

                        }
                        owebDich.AllowUnsafeUpdates = false;
                    }
                }
            }
        }

        public void CreateCopyList(string urlwebNguon, string urlwebDich)
        {

            using (SPSite osite = new SPSite("http://localhost:6003/"))
            {
                using (SPWeb owebGoc = osite.OpenWeb(urlwebNguon))
                {
                    using (SPWeb owebDich = osite.OpenWeb(urlwebDich))
                    {
                        owebDich.AllowUnsafeUpdates = true;
                        foreach (SPList item in owebGoc.Lists)
                        {
                            if (item.BaseType == SPBaseType.GenericList && !LstREmove.Contains(item.Title) && owebDich.Lists.TryGetList(item.Title) == null)
                            {

                                Console.WriteLine(item.Title);
                                owebDich.Lists.Add(item.Title, "", SPListTemplateType.GenericList);

                            }

                        }
                        owebDich.AllowUnsafeUpdates = false;
                    }
                }
            }
        }

    }
}