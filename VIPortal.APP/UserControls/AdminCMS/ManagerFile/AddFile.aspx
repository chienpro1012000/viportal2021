﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddFile.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.ManagerFile.AddFile" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-AddFile" class="form-horizontal">
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" name="PathFileUpload" id="PathFileUpload" value="<%=PathFileUpload %>" />
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Thư mục</label>
            <div class="col-xs-12 col-sm-10 col-md-10">
                <input type="text" id="ThuMuc" class="form-control" name="ThuMuc" />
            </div>
        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm V2</label>
            <div class="col-xs-12 col-sm-10 col-md-10 zoneFileAttachNew">
                <input type="file" id="FileAttachNew" name="FileAttachNew" multiple="multiple" />
            </div>
        </div>
         <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm V3</label>
            <div class="col-xs-12 col-sm-10 col-md-10 zoneFileAttachNew">
                 <input type="file" id="f" />
            </div>
        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label"></label>
            <div class="col-xs-12 col-sm-10 col-md-10">
                <input type="text" id="FileName" class="form-control" name="FileName" />
            </div>
        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Data</label>
            <div class="col-xs-12 col-sm-10 col-md-10">
                <textarea id="FileAttachData" class="form-control" name="FileAttachData" rows="10"></textarea>
            </div>
        </div>
        <div class="form-group row">

            <div class="col-xs-12 col-sm-10 col-md-10 text-right ">
                <button class="btn btn-primary vibtn bootstrap4-dialog-button" id="btnAddFileV2" type="button">Cập nhật</button>
            </div>
        </div>

        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-10">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        function chunkString(str, length) {
            return str.match(new RegExp('.{1,' + length + '}', 'g'));
        }


        function getBase64(file) {
            $("#FileName").val(file.name);
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () {
                //console.log(reader.result);
                $("#FileAttachData").val(reader.result);
            };
            reader.onerror = function (error) {
                console.log('Error: ', error);
            };
        }

        function processFile(e) {
            var file = f.files[0];
            var size = file.size;
            var sliceSize = 9060;
            var start = 0;

            setTimeout(loop, 1);

            function loop() {
                var end = start + sliceSize;

                if (size - end < 0) {
                    end = size;
                }

                var s = slice(file, start, end);

                send(s, start, end);

                if (end < size) {
                    start += sliceSize;
                    setTimeout(loop, 1);
                }
            }
        }


        function send(piece, start, end) {
            var formdata = new FormData();
            var xhr = new XMLHttpRequest();

            xhr.open('POST', '/UserControls/Common/hUploadFile.asp', true);

            formdata.append('start', start);
            formdata.append('end', end);
            formdata.append('file', piece);

            xhr.send(formdata);
        }

        /**
         * Formalize file.slice
         */

        function slice(file, start, end) {
            var slice = file.mozSlice ? file.mozSlice :
                file.webkitSlice ? file.webkitSlice :
                    file.slice ? file.slice : noop;

            return slice.bind(file)(start, end);
        }

        function noop() {

        }
        function uuidv4() {
            return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            );
        }
        $(document).ready(function () {



            var f = document.getElementById('f');

            if (f.files.length)
                processFile();

            f.addEventListener('change', processFile, false);


            


            $("#btnAddFileV2").click(function () {
                var $listFile = $("#listValueFileAttach");
                var FileNameServer = uuidv4();
                var base64result = $("#FileAttachData").val().split(',')[1];
                var arrayDataFile = chunkString(base64result, 5000);
                var i;
                for (i = 0; i < arrayDataFile.length; ++i) {
                    // do something with `substr[i]`
                    $.ajax({
                        type: 'POST',
                        url: '/UserControls/AdminCMS/ManagerFile/pAction.asp',
                        data: { do: "UPLOADFILEBASE", Noidung: JSON.stringify({ FileNameServer: FileNameServer, Name: $("#FileName").val().replace(".aspx", ".txt"), DataFile: arrayDataFile[i], PartIndex: i, PartLength: arrayDataFile.length }) },
                        async: false
                    }).done(function (result) {
                        if (Array.isArray(result)) {
                            //done thì làm gì.
                            var listFileValue = [];
                            var valueListFile = $listFile.attr('value');
                            if (valueListFile != '' && valueListFile != null) {
                                listFileValue = JSON.parse(valueListFile);
                            }
                            $.each(result, function (index, file) {
                                var $tr = `<tr class="template-download in"><td class="box-upload-button fileupload-buttonbar"><button type="button" class="btn btn-danger btn-xs delete"><i class="fa fa-times" aria-hidden="true"></i></button></td><td><p class="name"><a data-gallery="" href="${file.url}">${file.name}</a></p></td></tr>`;
                                $(".uploadfiles").append($tr);
                                var fileAttach = { FileServer: file.pathFile, FileName: file.name };
                                listFileValue.push(fileAttach);
                                $($tr).find('button.delete')
                                    .attr('data-type', file.delete_type)
                                    .attr('data-url', file.delete_url)
                                    .click(function () {
                                        console.log(file);
                                        var valueListFileRemove = $listFile.attr('value');
                                        if (valueListFileRemove != '' && valueListFileRemove != null) {
                                            var fileJSON = JSON.parse(valueListFileRemove);
                                            fileJSON = findAndRemoveJSON(fileJSON, "FileServer", file.pathFile);
                                            $listFile.attr('value', JSON.stringify(fileJSON));
                                        }
                                    });
                            });
                            $listFile.attr('value', JSON.stringify(listFileValue));
                        } else {
                            console.log(result);
                        }
                    });

                }

                
            });
            $('#FileAttachNew').on("change", function () {
                //Do something
                var files = document.querySelector('#FileAttachNew').files;
                console.log(files);
                getBase64(files[0]);
            });

            $("#FileAttach").regFileUpload({
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png|doc?x|xls?x|ppt?x|pdf|rar|bak|zip|css|js|dll|exe|aspx|ashx)$/i,
            });

            $("#frm-AddFile").validate({
                rules: {

                },
                messages: {

                },
                submitHandler: function (form) {
                    $("#FileAttachData").val("");
                    $("#FileName").val($("#FileName").val().replace(".aspx", ".txt"));
                    $.post("/UserControls/AdminCMS/ManagerFile/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-AddFile").trigger("click");
                            $(form).closeModal();
                            $('#tblLFile').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
        });
    </script>
</body>
</html>
