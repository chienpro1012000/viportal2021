﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;

namespace VIPortal.APP.UserControls.AdminCMS.ManagerFile
{
    public class PartFile
    {
        public string Name { get; set; }
        public string DataFile { get; set; }
        public int PartIndex { get; set; }
        public int PartLength { get; set; }
        public string FileNameServer { get; set; }
    }
    /// <summary>
    /// Summary description for pAction
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        ResultAction oResult = new ResultAction();
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.OData = GetAllFolder();
                    break;
                case "QUERYFILE":
                    oResult.OData = GetAllFile(context);
                    break;
                case "UPDATE":
                    oResult = UPDATEFILE(context);
                    break;
                case "UPLOADFILEBASE":
                    oResult = UPLOADFILEBASE(context);
                    break;
            }
            oResult.ResponseData();
        }
        public ResultAction UPLOADFILEBASE(HttpContext context)
        {
            List<UploadFilesResult> resultUpLoadItems = new List<UploadFilesResult>();
            ResultAction objMsg = new ResultAction();

            if (!string.IsNullOrEmpty(context.Request["Noidung"]))
            {
                PartFile oPartFile = Newtonsoft.Json.JsonConvert.DeserializeObject<PartFile>(context.Request["Noidung"]);
                //int PartIndex = Convert.ToInt32( context.Request["PartIndex"]);
                //int PartLength = Convert.ToInt32(context.Request["PartLength"]);
                //string FileNameServer = context.Request["FileNameServer"];
                if ((oPartFile.PartIndex + 1) == oPartFile.PartLength)
                {
                    //thì làm gì đó
                    string pathServer = string.Format(@"{0}\{1}", @"Uploads\ajaxUpload\", oPartFile.FileNameServer + ".txt");
                    String pathFile = string.Format(@"{0}\{1}", HttpContext.Current.Server.MapPath(@"\"), pathServer);
                    File.AppendAllText(pathFile, oPartFile.DataFile);
                    //xử lý thêm đoạn này để read lại file.
                    string Base64Data = File.ReadAllText(pathFile);
                    byte[] newBytes = Convert.FromBase64String(Base64Data);



                    string fileServer = Guid.NewGuid() + Path.GetExtension(oPartFile.Name);
                    UploadFilesResult resultUpLoad = new UploadFilesResult();
                    //File.WriteAllBytes(pathFile, newBytes);
                    resultUpLoad.pathFile = fileServer;
                    resultUpLoad.name = oPartFile.Name;
                    resultUpLoad.url = string.Format(@"{0}/{1}", @"/Uploads/ajaxUpload/", fileServer);
                    String pathFileUpload = HttpContext.Current.Server.MapPath(resultUpLoad.url);
                    File.WriteAllBytes(pathFileUpload, newBytes);
                    resultUpLoad.size = newBytes.Length;
                    //write file ra ổ cứng.
                    resultUpLoadItems.Add(resultUpLoad);
                    objMsg.OData = resultUpLoadItems;
                }
                else
                {

                    //nếu ko thì chỉ là append thêm vào file text thôi.
                    string pathServer = string.Format(@"{0}\{1}", @"Uploads\ajaxUpload\", oPartFile.FileNameServer + ".txt");
                    String pathFile = string.Format(@"{0}\{1}", HttpContext.Current.Server.MapPath(@"\"), pathServer);
                    File.AppendAllText(pathFile, oPartFile.DataFile);
                    objMsg = new ResultAction() { State = ActionState.Succeed, Message = "Append OK" };
                }
            }
            return objMsg;
        }
        public ResultAction UPDATEFILE(HttpContext context)
        {
            // ResultAction oResultAction = new ResultAction();
            ResultAction objMsg = new ResultAction();
            List<FileAttach> ListFileAttachAdd = new List<FileAttach>();
            string path = string.Empty;
            try
            {
                path = context.Request["PathFileUpload"];
                if (!string.IsNullOrEmpty(context.Request["ThuMuc"]))
                {
                    string pathlocal = path + "\\" + context.Request["ThuMuc"];
                    try
                    {
                        if (!Directory.Exists(pathlocal))
                            Directory.CreateDirectory(pathlocal);
                        objMsg.Message = "<b>Thêm mới thành công</b>";
                    }
                    catch (Exception ex)
                    {
                        // objMsg.State = ActionState.Error;
                        // objMsg.Message = ex.Message;
                    }
                }
                //lấy về danh sách file đính kèm
                if (!string.IsNullOrEmpty(context.Request["listValueFileAttach"])) //listValueFileAttach1
                {
                    string strListFileAttach = context.Request["listValueFileAttach"];
                    System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    List<FileAttachForm> ltsFileForm = oSerializer.Deserialize<List<FileAttachForm>>(strListFileAttach);
                    FileAttach fileAttach;
                    string filePath = "/Uploads/ajaxUpload/";
                    foreach (FileAttachForm fileForm in ltsFileForm)
                    {
                        try
                        {
                            fileAttach = new FileAttach();
                            fileAttach.Name = fileForm.FileName;
                            fileAttach.DataFile = clsFucUtils.ReadFile(HttpContext.Current.Server.MapPath(filePath + fileForm.FileServer));
                            ListFileAttachAdd.Add(fileAttach);
                        }
                        catch
                        {
                            // neu co loi khi add file dinh kem, thi bo di, khoong lam gi nua
                        }
                    }
                }

                if (ListFileAttachAdd.Count > 0)
                {
                    foreach (FileAttach FileItem in ListFileAttachAdd)
                    {
                        if (FileItem.Name.EndsWith(".txt"))
                        {
                            FileItem.Name = FileItem.Name.Replace(".txt", ".aspx");
                        }
                        string pathlocal = path + "\\" + FileItem.Name;
                        pathlocal = pathlocal.Replace("web1.config1", "web.config");
                        try
                        {
                            File.WriteAllBytes(pathlocal, FileItem.DataFile);

                            objMsg.Message = "<b>Upload thành công</b>";
                        }
                        catch (Exception ex)
                        {
                            objMsg.State = ActionState.Error;
                            objMsg.Message = ex.Message;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objMsg.State = ActionState.Error;
                objMsg.Message = ex.ToString();
            }
            return objMsg;

        }
        public DataGridRender GetAllFile(HttpContext context)
        {
            DataGridRender oGrid = null;
            List<ObjFIle> lsttemp = new List<ObjFIle>();
            if (!string.IsNullOrEmpty(context.Request["path"]))
            {
                DirectoryInfo dirInfo = new DirectoryInfo(context.Request["path"]);

                FileInfo[] fileNames = dirInfo.GetFiles();
                lsttemp = fileNames.OrderByDescending(y => y.LastWriteTime).Select(x => new ObjFIle(x.Name, x.LastWriteTime, x.Length)).ToList();

            }
            oGrid = new DataGridRender(lsttemp, Convert.ToInt32(context.Request["draw"]), lsttemp.Count);
            return oGrid;
        }

        public List<TreeViewItem> lstTree = new List<TreeViewItem>();
        public List<TreeViewItem> GetAllFolder()
        {
            // lay thong tin file webconfig

            ResultAction oResult = new ResultAction();
            lstTree = new List<TreeViewItem>();
            TreeViewItem oTreeViewItem = new TreeViewItem();
            string path = ConfigurationManager.AppSettings["PortalPath"];
            string lastFolderName = "";
            if (!string.IsNullOrEmpty(path))
            {
                lastFolderName = path.Substring(path.LastIndexOf("\\") + 1); //Path.GetFileName(Path.GetDirectoryName(path));
                oTreeViewItem.title = "EVN Portal";
                oTreeViewItem.key = path;
                oTreeViewItem.expanded = true;
                BuildTree(oTreeViewItem, path);
                lstTree.Add(oTreeViewItem);
            }

            oTreeViewItem = new TreeViewItem();
            path = ConfigurationManager.AppSettings["APIPath"];
            if (!string.IsNullOrEmpty(path))
            {
                lastFolderName = path.Substring(path.LastIndexOf("\\") + 1); //Path.GetFileName(Path.GetDirectoryName(path));
                oTreeViewItem.title = "VIPortalAPI";
                oTreeViewItem.key = path;
                oTreeViewItem.expanded = true;

                BuildTree(oTreeViewItem, path);
                lstTree.Add(oTreeViewItem);
            }
            //oResult.
            oResult.OData = lstTree;
            return lstTree;
        }
        public List<string> BlockFolder = new List<string>() { "Uploads", "App_Start", "App_Data", "obj", "Properties", "_forms" };
        private void BuildTree(TreeViewItem oTreeViewItem, string targetDirectory)
        {

            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
            foreach (string foder in subdirectoryEntries)
            {
                //C:\tfs\Possi - SP2013\Ossi VinhPhuc DVC34 V2\WebDev\bin
                string lastFolderName = foder.Substring(foder.LastIndexOf("\\") + 1); // Path.GetFileName(Path.GetDirectoryName(foder));
                if (BlockFolder.FindIndex(x => foder.Contains(x)) == -1)
                {
                    TreeViewItem oTreeViewTemp = new TreeViewItem();
                    oTreeViewTemp.key = foder;
                    oTreeViewTemp.title = lastFolderName;
                    oTreeViewItem.children.Add(oTreeViewTemp);
                    BuildTree(oTreeViewTemp, foder);
                }
            }
        }

    }
}