﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_ManagerFile.ascx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.ManagerFile.UC_ManagerFile" %>
<div role="body-data" data-title="" class="content_wp" data-action="/UserControls/AdminCMS/LGroup/pAction.ashx" data-form="/UserControls/AdminCMS/LGroup/pFormLGroup.aspx" data-view="/UserControls/AdminCMS/LGroup/pViewLGroup.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsgrid table-responsive">
                <div class="container-fluid">
                    <!-- Control the column width, and how they should appear on different devices -->
                    <div class="row">
                        <div class="col-sm-3">
                            <h5>Thư mục</h5>
                            <div id="treeLgroup"></div>
                        </div>

                        <div class="col-sm-9">
                            <div role="body-data" data-title="Người dùng" class="content_wp" data-view="/UserControls/AdminCMS/LUser/pViewLUser.aspx" data-action="/UserControls/AdminCMS/LUser/pAction.ashx" data-form="/UserControls/AdminCMS/ManagerFile/AddFile.aspx">
                                <div class="clsmanager row">
                                    <div class="col-sm-9">
                                        <div id="LFileSearch" class="form-inline zonesearch">
                                            <div class="form-group">
                                                <input type="hidden" name="do" id="do" value="QUERYFILE" />
                                                <input type="hidden" name="path" id="path" value="" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <p class="text-right">
                                            <button class="btn btn-primary act-add" id="pathbt" data-per="080102" type="button">Thêm mới</button>
                                        </p>
                                    </div>
                                </div>

                                <div class="clsgrid table-responsive">

                                    <table id="tblLFile" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function bytesToSize(bytes) {
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes == 0) return '0 Byte';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
    }
    $(document).ready(function () {
        //đăng ký treeview cho danh mục.
        $("#treeLgroup").fancytree({
            source: {
                url: "/VIPortalAPI/api/ManagerFile/DoRequest?do=QUERYDATA"
            },
            click: function (event, data) {

                var urlform = data.node.key;
                $("#path").val(urlform);
                $("#pathbt").data("PathFileUpload", data.node.key);
                $('#tblLFile').DataTable().ajax.reload();
            }
        });

        var $tbltblLFile = $("#tblLFile").viDataTable(
            {
                "frmSearch": "LFileSearch",
                "url": "/VIPortalAPI/api/ManagerFile/DoRequest",
                "aoColumns":
                    [
                        {
                            "name": "TimeEdit", "sTitle": "Thời gian sửa",
                            "mData": function (o) {
                                return formatDateTime(o.TimeEdit);
                            },
                            "sWidth": "200px",
                        }, {
                            "mData": function (o) {
                                return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                            },
                            "name": "Title", "sTitle": "Tên file"
                        }, {
                            "name": "Size", "sTitle": "Dung lượng",
                            "mData": function (o) {
                                return bytesToSize(o.Size);
                            },
                            "sWidth": "100px",
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {

                                return '<a title="Xoas file" data-Name="' + o.Title + '" class="btn default btn-xs black act-deletefix" href="javascript:;"><i class="fas fa-trash-alt"></i></a>';
                            }
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {

                                return '<a title="Tải file" data-Name="' + o.Title + '" class="btn default btn-xs black act-download" href="javascript:;"><i class="fa fa-download" aria-hidden="true"></i></a>';
                            }
                        }
                    ]
            });

        //var table = $('#example').DataTable();

        $tbltblLFile.on('draw', function () {
            //console.log('Redraw occurred at: ' + new Date().getTime());
            $(".act-download").click(function () {
                var dataName = $(this).attr('data-Name');
                window.open('/VIPortalAPI/api/ManagerFile/GenerateFile?Urlfile=' + $("#path").val() + "\\" + dataName, "_blank");
            });
            $(".act-deletefix").click(function () {
                var dataName = $(this).attr('data-Name');
                $.post('/VIPortalAPI/api/ManagerFile/Deletefile?Urlfile=' + $("#path").val() + "\\" + dataName, {}, function (result) {
                    if (result.State == 2) {
                        BootstrapDialog.show({
                            title: "Lỗi",
                            message: result.Message
                        });
                    }
                    else {
                        showMsg(result.Message);
                        //$("#btn-find-AddFile").trigger("click");
                        //$(form).closeModal();
                        $('#tblLFile').DataTable().ajax.reload();
                    }
                }).always(function () { });
                //window.open('/VIPortalAPI/api/ManagerFile/GenerateFile?Urlfile=' + $("#path").val() + "\\" + dataName, "_blank");
            });
        });
    });
</script>
