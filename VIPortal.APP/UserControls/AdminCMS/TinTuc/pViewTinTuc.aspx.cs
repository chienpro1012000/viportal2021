using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP.UserControls.AdminCMS.TinTuc
{
    public partial class pViewTinTuc : pFormBase
    {
        public TinTucItem oTinTuc = new TinTucItem();
        public string UrlListNews { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            UrlListNews = Request["UrlListNews"];
            oTinTuc = new TinTucItem();
            if (ItemID > 0)
            {
                TinTucDA oTinTucDA = new TinTucDA(UrlSite, UrlListNews);
                oTinTuc = oTinTucDA.GetByIdToObject<TinTucItem>(ItemID);

            }
        }
    }
}