using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData;
using ViPortalData.TinNoiBat;

namespace VIPortalAPP.UserControls.AdminCMS.TinTuc
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public string UrlListNew = string.Empty;
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);
            UrlListNew = context.Request["UrlListNews"];
            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            TinTucDA oTinTucDA = new TinTucDA(UrlSite, UrlListNew);
            TinTucItem oTinTucItem = new TinTucItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oTinTucDA.GetListJsonSolr(new TinTucQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oTinTucDA.TongSoBanGhiSauKhiQuery);
                    oGrid.Query = oTinTucDA.Queryreturn;
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "TinTuc", "QUERYDATA", 0, "Xem danh sách tin tức");
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oTinTucDA.GetListJson(new TinTucQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "CHIASETIN":
                    if (oGridRequest.ItemID > 0)
                    {
                        if (!string.IsNullOrEmpty(context.Request["DMDanhMucTin"]))
                        {
                            string DMDanhMucTin = context.Request["DMDanhMucTin"];
                            string UrlSiteChiaSe = context.Request["UrlSiteChiaSe"];
                            TinTucDA oTinTucDichDA = new TinTucDA(UrlSiteChiaSe, DMDanhMucTin);
                            TinTucJson temp = oTinTucDichDA.GetListJson(new TinTucQuery() { LogText = $"|Share_{oTinTucDA.SpListProcess.RootFolder.ServerRelativeUrl}_{oGridRequest.ItemID}|" }).FirstOrDefault();

                            oTinTucItem = oTinTucDA.GetByIdToObject<TinTucItem>(oGridRequest.ItemID);
                            foreach (var item in oTinTucItem.ListFileAttach)
                            {
                                oTinTucItem.ListFileAttachAdd.Add(new FileAttach()
                                {
                                    Name = item.Name,
                                    DataFile = oTinTucDA.SpListProcess.ParentWeb.GetFile(item.Url).OpenBinary()
                                });
                            }
                            if (temp == null)
                                oTinTucItem.ID = 0;
                            else oTinTucItem.ID = temp.ID;
                            oTinTucItem.LogText = $"|Share_{oTinTucDA.SpListProcess.RootFolder.ServerRelativeUrl}_{oGridRequest.ItemID}|";
                            //oTinTucItem.WFTrangThai

                            oTinTucItem.NguoiTao = new Microsoft.SharePoint.SPFieldLookupValue(CurentUser.ID, CurentUser.Title);
                            //lấy ra người có quyền biên tập tin ở site root.
                            LUserDA oLUserDA = new LUserDA();
                            List<LUserJson> LstUserPT = oLUserDA.GetListJson(new LUserQuery() { PerQuery = "1_020202" });
                            oTinTucItem.UserPhuTrach = new Microsoft.SharePoint.SPFieldLookupValueCollection();
                            oTinTucItem.UserPhuTrach.AddRange(LstUserPT.Select(x => new Microsoft.SharePoint.SPFieldLookupValue(x.ID, x.Title)));
                            oTinTucItem.EditUser = new Microsoft.SharePoint.SPFieldLookupValue(CurentUser.ID, CurentUser.Title);
                            oTinTucDichDA.UpdateObject<TinTucItem>(oTinTucItem);
                            oResult.Message = "Chia sẻ thành công thành công";
                        }
                        else
                        {
                            oResult.State = ActionState.Error;
                            oResult.Message = "Không xác định danh mục tin sẽ chia sẻ, hãy chọn danh mục tin chia sẻ";
                        }
                    }
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oTinTucItem = oTinTucDA.GetByIdToObject<TinTucItem>(oGridRequest.ItemID);
                        oTinTucItem.UpdateObject(context.Request);
                        if (string.IsNullOrEmpty(oTinTucItem.Title)) oTinTucItem.Title = oTinTucItem.Titles.Length > 250 ? oTinTucItem.Titles.Substring(0, 250) : oTinTucItem.Titles;
                        oTinTucItem.EditUser = new Microsoft.SharePoint.SPFieldLookupValue(CurentUser.ID, CurentUser.Title);
                        oTinTucDA.UpdateObject<TinTucItem>(oTinTucItem);
                        AddLog("UPDATE", "TinTuc", "UPDATE", oGridRequest.ItemID, $"Cập nhật tin tức{oTinTucItem.Title}");

                        //cập nhật lại tin nội bật nếu có.
                        if (oTinTucItem.isHotNew)
                        {
                            TinNoiBatDA oTinNoiBatDA = new TinNoiBatDA(UrlSite);
                            string Title = context.Request["Title"];

                            List<TinNoiBatJson> lstTInNong = oTinNoiBatDA.GetListJson(new TinNoiBatQuery()
                            {
                                ItemTinTuc = oTinTucItem.ID,
                                UrlListNew = oTinTucDA.FullPathUrlListProcess,
                            });
                            if (lstTInNong.Count > 0)
                            {
                                int IDTinNong = lstTInNong[0].ID;
                                TinNoiBatItem oTinNoiBatItem = oTinNoiBatDA.GetByIdToObject<TinNoiBatItem>(IDTinNong);
                                oTinNoiBatDA.UpdateObject<TinNoiBatItem>(new TinNoiBatItem()
                                {
                                    ID = IDTinNong,
                                    DMSTT = oTinNoiBatItem.DMSTT,
                                    ItemTinTuc = oTinTucItem.ID,
                                    UrlList = oTinTucDA.FullPathUrlListProcess,
                                    Title = oTinTucItem.Title,
                                    DescriptionNews = oTinTucItem.DescriptionNews,
                                    ImageNews = oTinTucItem.ImageNews,
                                    CreatedDate = oTinTucItem.CreatedDate
                                });
                            }
                            else
                            {
                                oTinNoiBatDA.UpdateObject<TinNoiBatItem>(new TinNoiBatItem()
                                {
                                    ItemTinTuc = oTinTucItem.ID,
                                    DMSTT = oTinNoiBatDA.GetLastSTT("DMSTT") + 1,
                                    UrlList = oTinTucDA.FullPathUrlListProcess,
                                    Title = oTinTucItem.Title,
                                    DescriptionNews = oTinTucItem.DescriptionNews,
                                    ImageNews = oTinTucItem.ImageNews,
                                    CreatedDate = oTinTucItem.CreatedDate
                                });
                            }

                        }
                    }
                    else
                    {
                        oTinTucItem.UpdateObject(context.Request);
                        oTinTucItem.NguoiTao = new Microsoft.SharePoint.SPFieldLookupValue(CurentUser.ID, CurentUser.Title);
                        oTinTucItem.UserPhuTrach = new Microsoft.SharePoint.SPFieldLookupValueCollection() { oTinTucItem.NguoiTao };
                        oTinTucItem.EditUser = new Microsoft.SharePoint.SPFieldLookupValue(CurentUser.ID, CurentUser.Title);
                        oTinTucItem.WFTrangThai = "000";
                        if (string.IsNullOrEmpty(oTinTucItem.Title)) oTinTucItem.Title = oTinTucItem.Titles.Length > 250 ? oTinTucItem.Titles.Substring(0, 250) : oTinTucItem.Titles;
                        oTinTucDA.UpdateObject<TinTucItem>(oTinTucItem);
                        if (oTinTucItem.isHotNew)
                        {
                            //add vào hostnew.
                            TinNoiBatDA oTinNoiBatDA = new TinNoiBatDA(UrlSite);

                            oTinNoiBatDA.UpdateObject<TinNoiBatItem>(new TinNoiBatItem()
                            {
                                ItemTinTuc = oTinTucItem.ID,
                                UrlList = oTinTucDA.FullPathUrlListProcess,
                                Title = oTinTucItem.Title,
                                DMSTT = oTinNoiBatDA.GetLastSTT("DMSTT") + 1,
                                DescriptionNews = oTinTucItem.DescriptionNews,
                                ImageNews = oTinTucItem.ImageNews,
                                CreatedDate = oTinTucItem.CreatedDate
                            });

                        }
                        AddLog("UPDATE", "TinTuc", "ADD", oGridRequest.ItemID, $"Thêm mới tin tức{oTinTucItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oTinTucDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "TinTuc", "DELETE", oGridRequest.ItemID, $"Xóa tin tức{outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oTinTucDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    AddLog("DELETE-MULTI", "TinTuc", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều tin tức");
                    break;
                case "ADDHOTNEW":

                    oTinTucItem = oTinTucDA.GetByIdToObject<TinTucItem>(oGridRequest.ItemID);
                    oTinTucItem.UpdateObject(context.Request);
                    if (oTinTucItem.isHotNew)
                    {
                        //thêm mới hoặc cập nhật.
                        TinNoiBatDA oTinNoiBatDA = new TinNoiBatDA(UrlSite);
                        List<TinNoiBatJson> lstTInNong = oTinNoiBatDA.GetListJson(new TinNoiBatQuery()
                        {
                            ItemTinTuc = oTinTucItem.ID,
                            UrlListNew = oTinTucDA.FullPathUrlListProcess,
                        });
                        if (lstTInNong.Count == 0) //nếu chưa có thì add
                        {
                            oTinNoiBatDA.UpdateObject<TinNoiBatItem>(new TinNoiBatItem()
                            {
                                ItemTinTuc = oTinTucItem.ID,
                                DMSTT = oTinNoiBatDA.GetLastSTT("DMSTT") + 1,
                                UrlList = oTinTucDA.FullPathUrlListProcess,
                                Title = oTinTucItem.Title,
                                DescriptionNews = oTinTucItem.DescriptionNews,
                                ImageNews = oTinTucItem.ImageNews,
                                CreatedDate = oTinTucItem.CreatedDate
                            });
                        }
                        oTinTucDA.SystemUpdateOneField(oGridRequest.ItemID, "isHotNew", oTinTucItem.isHotNew);
                        AddLog("ADDHOTNEW", "TinTuc", "ADDHOTNEW", oGridRequest.ItemID, $"Thêm mới tin nóng {oTinTucItem.Title}");
                    }
                    else
                    {
                        //tạm thời ko làm gì. ko xóa tin nóng cũ.
                        oTinTucDA.SystemUpdateOneField(oGridRequest.ItemID, "isHotNew", oTinTucItem.isHotNew);
                        AddLog("ADDHOTNEW", "TinTuc", "ADDHOTNEW", oGridRequest.ItemID, $"Bỏ tin nóng tin nóng {oTinTucItem.Title}");
                        TinNoiBatDA oTinNoiBatDA = new TinNoiBatDA(UrlSite);
                        List<TinNoiBatJson> lstTInNong = oTinNoiBatDA.GetListJson(new TinNoiBatQuery()
                        {
                            ItemTinTuc = oTinTucItem.ID,
                            UrlListNew = oTinTucDA.FullPathUrlListProcess,
                        });
                        if (lstTInNong.Count > 0) //nếu chưa có thì add
                        {
                            oTinNoiBatDA.DeleteObjectV2(lstTInNong[0].ID);
                        }
                    }
                    oResult.Message = "Cập nhật thành công";
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oTinTucDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "TinTuc", "APPROVED", oGridRequest.ItemID, $"Duyệt tin tức{oTinTucDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oTinTucDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "TinTuc", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt tin tức{oTinTucDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oTinTucItem = oTinTucDA.GetByIdToObjectSelectFields<TinTucItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oTinTucItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
                case "SENDMAIL":
                    List<string> listMailTo = context.Request["lmailto"].Split(';').Where(x => x.Length > 0).ToList();
                    var link = context.Request["link"];
                    var subject = context.Request["subject"];
                    var content = context.Request["content"];
                    content += "Chi tiết tin tức <a href=\"" + link + "&l=" + UrlListNew + "\">xem tại đây</a>";
                    var body = content;
                    SendMail(subject, body, listMailTo);
                    oResult.Message = string.Format("Đã gửi bài viết thành công");
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}