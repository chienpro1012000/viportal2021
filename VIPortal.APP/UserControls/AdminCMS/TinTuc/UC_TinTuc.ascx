<%@ control language="C#" autoeventwireup="true" codebehind="UC_TinTuc.aspx.cs" inherits="VIPortalAPP.UserControls.AdminCMS.TinTuc.UC_TinTuc" %>
<div role="body-data" data-title="" class="content_wp" data-size="1040" data-action="/UserControls/AdminCMS/TinTuc/pAction.asp" data-form="/UserControls/AdminCMS/TinTuc/pFormTinTuc.aspx" data-view="/UserControls/AdminCMS/TinTuc/pViewTinTuc.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="TinTucSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="DanhMuc">Danh mục</label>
                            <select name="UrlListNews" id="UrlListNews" class="form-control">
                                <%foreach(var item in lstDanhMuc){%>
                                <option value="<%=item.UrlList %>"><%=item.Title %></option>
                                <%} %>
                            </select>
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Titles" />
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <button class="btn btn-primary act-add" data-per="020202" type="button">Thêm mới</button>
                    </p>
                </div>
            </div>

            <div class="clsgrid table-responsive">
                <table id="tblTinTuc" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var $tagvalue = $(".content_wp");
        var $urlnews = $("#UrlListNews").val();
        var dataUrl = { UrlListNews: $urlnews };
        $tagvalue.data("parameters", dataUrl);
        var $tblDanhMucChung = $("#tblTinTuc").viDataTable(
            {
                "order": [2,"desc"],
                "frmSearch": "TinTucSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "mData": function (o) {
                                var htmlhot = '<i class="fas fa-fire clsred"></i>';
                                if (!o.isHotNew)
                                    htmlhot = '';
                                return '<a data-id="' + o.ID + '" class="act-view-news" href="javascript:;">' + o.Titles + '</a>' + htmlhot;
                            },
                            "name": "Titles", "sTitle": "Tiêu đề"
                        },                        
                        {
                            "mData": function (o) {
                                return o.NguoiTao.Title;
                            },
                            "sWidth": "170px",
                            "name": "NguoiTao", "sTitle": "Người tạo"
                        },
                        {
                            "mData": function (o) {
                                return formatDate(o.Created);
                            },
                            "sWidth": "90px",
                            "name": "Created", "sTitle": "Ngày tạo"
                        }, {
                            "name": "WFTrangThaiText", "sTitle": "Trạng thái",
                            "mData": "WFTrangThaiText",
                            "sWidth": "80px",
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all clschiase",
                            "mRender": function (o) {
                                return `<a title="Chia sẻ tin tức" url="/UserControls/AdminCMS/TinTuc/pChiaSe.aspx" data-UrlListGoc=${o.UrlListFull} size=600 class="default btn-xs purple btnfrmnochek" data-do="CHIASETIN" data-ItemID="${o.ID}" href="javascript:;"><i class="fa fa-share-alt" aria-hidden="true"></i></a>`;
                            }
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            visible: false,
                            "mRender": function (o) {
                                if (o._ModerationStatus == 0)
                                    return '<a title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o)
                            {
                                if (CheckPermiss(o.UserPhuTrach, <%=CurentUser.ID%>))
                                    return `<a Title="Thao tác phê duyệt" data-gridid="tblTinTuc" class="btn default btn-xs purple act-workflow" data-WFTrangThai="${o.WFTrangThai}" data-UrlListFull=${o.UrlListFull} data-ItemID="${o.ID}" href="javascript:;"><i class="fas fa-tasks"></i></i></a>`;
                                else return '';
                            }
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return `<a data-WFTrangThai="${o.WFTrangThai}" class="btn default btn-xs purple act-edit" data-ItemID="` + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        }, {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                
            });
        $tblDanhMucChung.on('draw', function () {
            $tblDanhMucChung.rows().every(function () {
                var d = this.data();
                console.log(this.node());
                if (!CheckPermiss(d.UserPhuTrach, '<%=CurentUser.ID%>')) {
                    console.log($(this.node()).find("a.btn"));
                    $(this.node()).find("a.btn").remove();
                }
            });
            console.log('Redraw occurred at: ' + new Date().getTime());
            $("a[data-itemid='<%=ItemID%>']").parent().parent().addClass("clsdetail");
        });
        console.log($tagvalue.data("parameters"));
        $("#UrlListNews").change(function () {
            var $urlnews = $(this).val();
            var dataUrl = { UrlListNews: $urlnews };
            $tagvalue.data("parameters", dataUrl);
        });
    });
</script>
