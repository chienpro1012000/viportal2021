﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP.UserControls.AdminCMS.TinTuc
{
    public partial class UC_TinTuc : BaseUC
    {
        public List<DanhMucThongTinJson> lstDanhMuc { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request["l"]))
            {
                UrlList = Request["l"];
            }
            DanhMucThongTinDA oDanhMucDA = new DanhMucThongTinDA(UrlSite);
            lstDanhMuc = oDanhMucDA.GetListJson(new DanhMucThongTinQuery() { });

            if (!string.IsNullOrEmpty(UrlList))
            {
                lstDanhMuc = lstDanhMuc.Where(x => x.UrlList == UrlList).ToList();
                if(lstDanhMuc.Count == 0)
                {
                    TinTucDA oLTinTucDA = new TinTucDA(UrlSite, UrlList);
                    lstDanhMuc.Add(new DanhMucThongTinJson()
                    {
                        UrlList = UrlList,
                        Title = oLTinTucDA.SpListProcess.Title
                    });
                }
            }
        }
    }
}