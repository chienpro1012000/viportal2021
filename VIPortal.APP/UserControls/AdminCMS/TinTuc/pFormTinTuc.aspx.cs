using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP.UserControls.AdminCMS.TinTuc
{
    public partial class pFormTinTuc : pFormBase
    {
        public TinTucItem oTinTuc {get;set;}
        public DanhMucThongTinJson oDanhMucThongTinJson { get; set; }
        public  string UrlListNews { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            UrlListNews = Request["UrlListNews"];
            oTinTuc = new TinTucItem();
            oDanhMucThongTinJson = new DanhMucThongTinJson();
            if (ItemID > 0)
            {
                TinTucDA oTinTucDA = new TinTucDA(UrlSite,UrlListNews);
                oTinTuc = oTinTucDA.GetByIdToObject<TinTucItem>(ItemID);
            }
            DanhMucThongTinDA danhMucThongTinDA = new DanhMucThongTinDA(UrlSite);
            oDanhMucThongTinJson = danhMucThongTinDA.GetListJson(new DanhMucThongTinQuery() { UrlList = UrlListNews }).FirstOrDefault();
            if (oDanhMucThongTinJson == null)
                oDanhMucThongTinJson = new DanhMucThongTinJson();

        }
    }
}