<%@ page language="C#" autoeventwireup="true" codebehind="pFormTinTuc.aspx.cs" inherits="VIPortalAPP.UserControls.AdminCMS.TinTuc.pFormTinTuc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-TinTuc" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oTinTuc.ID %>" />
        <input type="hidden" name="Title" id="Title" value="" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" name="UrlListNews" id="UrlListNews" value="<%=UrlListNews %>" />
        <div class="form-group row">
            <label for="Titles" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Titles" id="Titles" class="form-control" placeholder="Nhập tiêu đề tin tức" value="<%=oTinTuc.Titles%>" />
            </div>
        </div>
        <%if(oDanhMucThongTinJson.DBPhanLoai == 2){ %>
        <div class="form-group row">
            <label for="UrlView" class="col-sm-2 control-label">Liên kết</label>
            <div class="col-sm-10">
                <input type="text" name="UrlView" id="UrlView" class="form-control" placeholder="Nhập liên kết" value="<%=oTinTuc.UrlView%>" />
            </div>
        </div>
        <%}%>
        <div class="form-group row clsTinTuc">
            <label for="DescriptionNews" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <textarea name="DescriptionNews" id="DescriptionNews" placeholder="Nhập mô tả" class="form-control"><%:oTinTuc.DescriptionNews%></textarea>
            </div>
        </div>

        <div class="form-group row">
            <label for="ImageNews" class="col-sm-2 control-label">Ảnh đại diện</label>
            <div class="col-sm-10" data-role="groupImageDD">
                <input type="hidden" name="ImageNews" id="ImageNews" value="<%:oTinTuc.ImageNews%>" />
                <img title="Ảnh đại diện" id="srcImageNews" src="<%=oTinTuc.ImageNews%>" />
                <button id="btnImageNews" type="button" class="btn  btn-success">Ảnh đại diện</button>
                <button id="btnXoaImageNews" type="button" class="btn btn-danger">Xóa ảnh</button>
            </div>
        </div>

        <div class="form-group row clsTinTuc">
            <label for="ContentNews" class="col-sm-2 control-label">Nội dung</label>
            <div class="col-sm-10">
                <textarea name="ContentNews" id="ContentNews" placeholder="Nhập nội dung" class="form-control"><%=oTinTuc.ContentNews%></textarea>
            </div>
        </div>
        <div class="form-group row clsTinTuc">
            <label for="isHotNew" class="col-sm-2 control-label">Tin nổi bật</label>
            <div class="col-sm-1">
                <input value="true" type="checkbox" name="isHotNew" id="isHotNew" <%=oTinTuc.isHotNew? "checked" : string.Empty%> />
            </div>
        </div>
        <div class="form-group row">
            <label for="CreatedDate" class="col-sm-2 control-label">Ngày đăng</label>
            <div class="col-sm-2">
                <input type="text" name="CreatedDate" id="CreatedDate" value="<%:string.Format("{0:dd/MM/yyyy}",oTinTuc.CreatedDate)%>" class="form-control input-datetime" />
            </div>
            <label for="TacGia" class="col-sm-2 control-label">Tác giả</label>
            <div class="col-sm-3">
                <input type="text" name="TacGia" id="TacGia" placeholder="Nhập tác giả" value="<%:oTinTuc.TacGia%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-10">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            if (<%=oDanhMucThongTinJson.DBPhanLoai%> == 2) {
                $(".clsTinTuc").hide();
            }
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oTinTuc.ListFileAttach)%>'
            });
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            CKEDITOR.replace("ContentNews", {

            });
            $("#btnImageNews").click(function () {
                $elementId = $(this);
                CKFinder.popup({
                    chooseFiles: true,
                    width: 800,
                    height: 600,
                    resourceType: 'Images',
                    onInit: function (finder) {
                        finder.on('files:choose', function (evt) {
                            var file = evt.data.files.first();
                            //var output = document.getElementById(elementId);
                            //console.log(file.getUrl());
                            //$("#ImageNews").value(file.getUrl());
                            var urlfileimg = file.getUrl();
                            document.getElementById("ImageNews").value = urlfileimg;
                            document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                        });
                        finder.on('file:choose:resizedImage', function (evt) {
                            //var output = document.getElementById(elementId);
                            console.log(evt.data);
                            var urlfileimg = evt.data.resizedUrl;
                            document.getElementById("ImageNews").value = urlfileimg;
                            document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                        });
                    }
                });
            });
            $("#btnXoaImageNews").click(function () {
                document.getElementById("ImageNews").value = "";
                document.getElementById("srcImageNews").setAttribute("src", "");
            });
            $("#frm-TinTuc").validate({
                //ignore: [],
                rules: {
                    Titles: "required",
                    //ContentNews: "required",
                    //ImageNews: "required"
                },
                messages: {
                    Titles: "Vui lòng nhập tiêu đề bài viết",
                    //ContentNews: "Vui lòng nhập nội dung",
                    //ImageNews: "Vui lòng chọn ảnh đại diện"
                },
                submitHandler: function (form) {
                    $("Title").val($("Titles").val());
                    $.post("/UserControls/AdminCMS/TinTuc/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-TinTuc").trigger("click");
                            $(form).closeModal();
                            $('#tblTinTuc').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-TinTuc").viForm();
        });
    </script>
</body>
</html>
