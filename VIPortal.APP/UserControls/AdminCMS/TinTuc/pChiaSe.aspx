﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pChiaSe.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.TinTuc.pChiaSe" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frmChiaSe" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=ItemID %>" />
        <input type="hidden" name="UrlListGoc" id="UrlListGoc" value="<%=UrlListGoc %>" />
        <input type="hidden" name="UrlListNews" id="UrlListNews" value="<%=UrlListNews %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" name="UrlSiteChiaSe" id="UrlSiteChiaSe" value="" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Chọn danh mục chia sẻ</label>
            <div class="col-sm-10">
                 <select data-selected="" data-url="/UserControls/AdminCMS/DanhMucThongTin/pAction.asp?do=QUERYDATA&UrlSiteFix=" data-place="Chọn Danh mục chia sẻ" name="DMDanhMucTin" id="DMDanhMucTin" class="form-control"></select>
            </div>
        </div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#DMDanhMucTin").smSelect2018V2({
                dropdownParent: "#frmChiaSe",
                TemplateValue: "{UrlList}"
            });
            $("#frmChiaSe").validate({
                rules: {
                },
                messages: {
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/TinTuc/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            $(form).closeModal();
                            $('#tblTinTuc').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frmChiaSe").viForm();
        });
    </script>
</body>
</html>

