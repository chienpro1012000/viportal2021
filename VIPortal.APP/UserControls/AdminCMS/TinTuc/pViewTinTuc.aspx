<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewTinTuc.aspx.cs" Inherits="VIPortalAPP.UserControls.AdminCMS.TinTuc.pViewTinTuc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
    <style>
        #img_contents img{
            width:100% !important;
            max-height: 900px;
        }
    </style>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
        <div class="form-group row">
            <label for="Titles" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <%=oTinTuc.Titles%>
            </div>
        </div>
        <div class="form-group row">
            <label for="DescriptionNews" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <%=oTinTuc.DescriptionNews%>
            </div>
        </div>
        <div class="form-group row">
            <label for="CreatedDate" class="col-sm-2 control-label">Ngày đăng</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oTinTuc.CreatedDate)%>
            </div>
        </div>
        <div class="form-group row">
            <label for="ImageNews" class="col-sm-2 control-label">Ảnh đại diện</label>
            <div class="col-sm-10">
                <img src="<%=oTinTuc.ImageNews%>" style="width:200px !important; height:120px !important" alt="Alternate Text" />
            </div>
        </div>
        <div class="form-group row">
            <label for="TacGia" class="col-sm-2 control-label">Tác giả</label>
            <div class="col-sm-4">
                <%=oTinTuc.TacGia%>
            </div>
            <%--<label for="isHotNew" class="col-sm-2 control-label">Tin nổi bật</label>
            <div class="col-sm-4">
                <%=oTinTuc.isHotNew? "Có" : "Không"%>
            </div>--%>
        </div>
        <div class="form-group row" id="img_contents">
            <label for="ContentNews" class="col-sm-2 control-label">Nội dung</label>
            <div class="col-sm-10">
                <%=oTinTuc.ContentNews%>
            </div>
        </div>
        
        <%--<div class="form-group">
            <label for="NguoiTao" class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-10">
                
            </div>
        </div>--%>
        <div class="form-group row">
            <label class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oTinTuc.Created)%>
            </div>
            <label class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oTinTuc.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
                <%=oTinTuc.NguoiTao.LookupValue%>
            </div>
            <label class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
                <%=oTinTuc.EditUser.LookupValue%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oTinTuc.ListFileAttach.Count;i++){ %>
                <div><a href="<%=oTinTuc.ListFileAttach[i].Url %>"><%=oTinTuc.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>
