﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;

namespace VIPortal.APP.UserControls.AdminCMS.TinTuc
{
    
    public partial class pChiaSe : pFormBase
    {
        public string UrlListGoc { get; set; }
        public string UrlListNews { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request["UrlListGoc"]))
            {
                UrlListGoc = Request["UrlListGoc"];
                UrlListNews = UrlListGoc.Split('/').LastOrDefault();
            }
        }
    }
}