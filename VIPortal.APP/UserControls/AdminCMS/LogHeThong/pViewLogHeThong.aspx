﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewLogHeThong.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.LogHeThong.pViewLogHeThong" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên hệ thống</label>
            <div class="col-sm-10">
               <%=oLogHeThong.Title%>
            </div>
        </div>
          <div class="form-group row">
            <label for="CreatedUser" class="col-sm-2 control-label">User</label>
            <div class="col-sm-10">
                <%:oLogHeThong.CreatedUser.LookupValue%>
            </div>
        </div>
        <div class="form-group row">
            <label for="LogDoiTuong" class="col-sm-2 control-label">Đối tượng</label>
            <div class="col-sm-10">
               <%:oLogHeThong.LogDoiTuong%>
            </div>
        </div>
        <div class="form-group row">
            <label for="LogThaoTac" class="col-sm-2 control-label">Thao tác </label>
            <div class="col-sm-10">
                <%:oLogHeThong.LogThaoTac%>
            </div>
        </div>
          <div class="form-group row">
            <label for="fldGroup" class="col-sm-2 control-label">Group</label>
            <div class="col-sm-10">
                <%:oLogHeThong.fldGroup.LookupValue%>
            </div>
        </div>
        <div class="form-group row">
            <label for="LogFunction" class="col-sm-2 control-label">Chức năng </label>
            <div class="col-sm-4">
                <%:oLogHeThong.LogFunction%>
            </div>
             <label for="LogTrangThai" class="col-sm-2 control-label">Trạng thái</label>
            <div class="col-sm-4">
                <div class="">
                     <%:(oLogHeThong!= null &&  oLogHeThong.LogTrangThai>0)?oLogHeThong.LogTrangThai+"": "" %>
                </div>
            </div>
        </div>
        <div class="form-group row">
             <label for="LogNoiDung" class="col-sm-2 control-label">Nội dung </label>
             <div class="col-sm-10">
                  <%=oLogHeThong.LogNoiDung%>
             </div>
        </div>
        <div class="form-group row">
             <label class="col-sm-2 control-label">File đính kèm</label>
             <div class="col-sm-10">
                <%for(int i=0; i< oLogHeThong.ListFileAttach.Count;i++){ %>
                <div><a href="<%=oLogHeThong.ListFileAttach[i].Url %>"><%=oLogHeThong.ListFileAttach[i].Name%></a></div>
                    <%} %>
             </div>
       </div>
       <div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%=string.Format("{0:dd-MM-yyyy}", oLogHeThong.Created)%>
            </div>
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}",oLogHeThong.Modified)%>
            </div>
       </div>
       <div class="form-group row">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
               <%=oLogHeThong.Author.LookupValue%>
            </div>
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
               <%=oLogHeThong.Editor.LookupValue%>
            </div>    
        </div>
    </form>
</body>
</html>
