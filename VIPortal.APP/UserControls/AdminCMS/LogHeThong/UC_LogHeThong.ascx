﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_LogHeThong.ascx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.LogHeThong.UC_LogHeThong" %>
<div role="body-data" data-title="log hệ thống" class="content_wp" data-action="/UserControls/AdminCMS/LogHeThong/pAction.asp" data-form="/UserControls/AdminCMS/LogHeThong/pFormLogHeThong.aspx" data-view="/UserControls/AdminCMS/LogHeThong/pViewLogHeThong.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="LogHeThongSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title,CreatedUser,LogDoiTuong,LogNoiDung" />
                             <select data-place="Chọn thao tác" name="LogThaoTac" id="LogThaoTac" class="form-control mr-2">
                                  <option value="UPDATELICHDONVIDONBO">UPDATELICHDONVIDONBO</option>
                                  <option value="UPDATELICHDONVIDONBO">UPDATELICHDONVIDONBO</option>
                                 <option value="QUERYDATA">QUERYDATA</option>
                                 <option value="serviceValidate">serviceValidate</option>
                                 <option value="UPDATE">Cập nhật</option>
                                 <option value="ADD">Thêm mới</option>
                                 <option value="DELETE">Xóa</option>
                                 <option value="DELETE-MULTI">Xóa nhiều</option>
                                 <option value="APPROVED">Duyệt</option>
                                 <option value="PENDDING">Hủy duyệt</option>
                             </select>
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <!--<button class="btn btn-primary act-add" type="button">Thêm mới</button>-->
                    </p>
                </div>

            </div>

            <div class="clsgrid table-responsive">

                <table id="tblLogHeThong" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">  
    $(document).ready(function () {

        var $tagvalue = $(".content_wp");
        var $tblDanhMucChung = $("#tblLogHeThong").viDataTable(
            {
                "frmSearch": "LogHeThongSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "name": "Created", "sTitle": "Thời gian ",
                            "mData": function (o) {
                                return formatDateTime(o.Created);
                            },
                            "sWidth": "100px",
                        },
                        {
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '" >' + o.CreatedUser.Title + '</a>';
                            },
                            "name": "CreatedUser", "sTitle": "Người dùng",
                            "sWidth": "150px",
                        }, {
                            "mData": function (o) {
                                if (o.Title != null)
                                    return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.sTitle + '</a>';
                                else
                                    return "";
                            },
                            "name": "Title", "sTitle": "Thao tác"
                        },
                        {
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '" >' + o.LogDoiTuong + '</a>';
                            },
                            "name": "LogDoiTuong", "sTitle": "Đối tượng"
                        }
                    ],
                "aaSorting": [0, 'asc'],
                "order": [[0, "desc"]]
            });
    });
</script>
