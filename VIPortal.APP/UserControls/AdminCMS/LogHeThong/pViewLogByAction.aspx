﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewLogByAction.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.LogHeThong.pViewLogByAction" %>

<div role="body-data" data-title="log hệ thống" class="content_wp" data-action="/UserControls/AdminCMS/LogHeThong/pAction.ashx" data-form="/UserControls/AdminCMS/LogHeThong/pFormLogHeThong.aspx" data-view="/UserControls/AdminCMS/LogHeThong/pViewLogHeThong.aspx">
    <div class="clsmanager row">
        <div class="col-sm-9">
            <div id="LogHeThongSearch" class="form-inline zonesearch">
                <div class="form-group">
                    <input type="hidden" name="do" id="do" value="QUERYDATA" />
                    <label for="Keyword">Từ khóa</label>
                    <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                    <input type="hidden"  name="LogThaoTac" value="<%=ConfigGroup %>" id="LogThaoTac"/>
                    <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title,LogNoiDung,LogThaoTac" />
                </div>
                <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
            </div>
        </div>
    </div>

    <div class="clsgrid table-responsive">

        <table id="tblLogHeThong" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
        </table>

    </div>
</div>
<script type="text/javascript">  
    $(document).ready(function () {
        var $tblDanhMucChung = $("#tblLogHeThong").viDataTable(
            {
                "frmSearch": "LogHeThongSearch",
                "url": "/UserControls/AdminCMS/LogHeThong/pAction.ashx",
                "aoColumns":
                    [
                        {
                            "name": "Created", "sTitle": "Thời gian ",
                            "mData": function (o) {
                                return formatDateTime(o.Created);
                            },
                            "sWidth": "100px",
                        },
                        {
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '" >' + o.CreatedUser.Title + '</a>';
                            },
                            "name": "CreatedUser", "sTitle": "Người dùng",
                            "sWidth": "150px",
                        }, {
                            "mData": function (o) {
                                if (o.Title != null)
                                    return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.LogNoiDung + '</a>';
                                else
                                    return "";
                            },
                            "name": "LogNoiDung", "sTitle": "Thao tác"
                        },
                        {
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '" >' + o.LogDoiTuong + '</a>';
                            },
                            "name": "LogDoiTuong", "sTitle": "Đối tượng"
                        },
                        {
                            "sWidth": "100px",
                            "mData": function (o) {
                                if (o.LogTrangThai == 1) {
                                    return "Lỗi";
                                } else
                                return "Thành Công" 
                            },
                            "name": "LogDoiTuong", "sTitle": "Trạng Thái"
                        }
                    ],
                "aaSorting": [0, 'asc']
            });
    });
</script>
