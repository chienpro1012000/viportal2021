﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormLogHeThong.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.LogHeThong.pFormLogHeThong" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-LogHeThong" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oLogHeThong.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên hệ thống</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oLogHeThong.Title%>" />
            </div>
        </div>
          <div class="form-group row">
            <label for="CreatedUser" class="col-sm-2 control-label">User</label>
            <div class="col-sm-10">
                <select data-selected="<%:oLogHeThong.CreatedUser.LookupId%>" data-url="/UserControls/AdminCMS/LUser/pAction.asp?do=QUERYDATA" data-place="Chọn user" name="CreatedUser" id="CreatedUser" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <label for="LogDoiTuong" class="col-sm-2 control-label">Đối tượng</label>
            <div class="col-sm-10">
                <input type="text" name="LogDoiTuong" id="LogDoiTuong" placeholder="Nhập đối tượng" value="<%:oLogHeThong.LogDoiTuong%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="LogThaoTac" class="col-sm-2 control-label">Thao tác </label>
            <div class="col-sm-10">
                <input type="text" name="LogThaoTac" id="LogThaoTac" placeholder="Nhập thao tác" value="<%:oLogHeThong.LogThaoTac%>" class="form-control" />
            </div>
        </div>
          <div class="form-group row">
            <label for="fldGroup" class="col-sm-2 control-label">Group</label>
            <div class="col-sm-10">
                <select data-selected="<%:oLogHeThong.fldGroup.LookupId%>" data-url="/UserControls/AdminCMS/LGroup/pAction.asp?do=QUERYDATA" data-place="Chọn Group" name="fldGroup" id="fldGroup" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <label for="LogFunction" class="col-sm-2 control-label">Chức năng </label>
            <div class="col-sm-4">
                <input type="text" name="LogFunction" id="LogFunction" placeholder="Nhập chức năng" value="<%:oLogHeThong.LogFunction%>" class="form-control" />
            </div>
             <label for="LogTrangThai" class="col-sm-1 control-label">Trạng thái</label>
            <div class="col-sm-5">
                <div class="">
                     <input type="text" name="LogTrangThai" id="LogTrangThai" placeholder="Nhập trạng thái" value="<%:(oLogHeThong!= null &&  oLogHeThong.LogTrangThai>0)?oLogHeThong.LogTrangThai+"": "" %>" class="form-control" />
                </div>
            </div>
            <div class="form-group row">
            <label for="LogNoiDung" class="col-sm-2 control-label">Nội dung </label>
            <div class="col-sm-10">
                <textarea name="LogNoiDung" id="LogNoiDung" class="form-control"><%=oLogHeThong.LogNoiDung%></textarea>
            </div>
        </div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#CreatedUser").smSelect2018V2({
                dropdownParent: "#frm-LogHeThong"
            });
            CKEDITOR.replace("LogNoiDung", {
                height: 300
            });
            $("#fldGroup").smSelect2018V2({
                dropdownParent: "#frm-LogHeThong"
            });
           
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $("#frm-LogHeThong").validate({
                rules: {
                    Title: "required",
                    FAQLuotXem: "required",
                  
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề",
                    FAQLuotXem: "Vui lòng nhập số",
                },
               
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/LogHeThong/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-LogHeThong").trigger("click");
                            $(form).closeModal();
                            $('#tblLogHeThong').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
        });
    </script>
</body>
</html>

