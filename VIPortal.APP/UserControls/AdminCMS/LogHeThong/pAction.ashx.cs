﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData.LogHeThong;

namespace VIPortal.APP.UserControls.AdminCMS.LogHeThong
{

    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            LogHeThongDA oLogHeThongDA = new LogHeThongDA(UrlSite);
            LogHeThongItem oLogHeThongItem = new LogHeThongItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oLogHeThongDA.GetListJson(new LogHeThongQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oLogHeThongDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "LogHeThong", "QUERYDATA", 0, "Xem danh sách nhật ký hệ thống");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataLogHeThong = oLogHeThongDA.GetListJson(new LogHeThongQuery(context.Request));
                    treeViewItems = oDataLogHeThong.Select(x => new TreeViewItem()
                    {
                        key = x.LogDoiTuong,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;

                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oLogHeThongDA.GetListJson(new LogHeThongQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLogHeThongItem = oLogHeThongDA.GetByIdToObject<LogHeThongItem>(oGridRequest.ItemID);
                        oLogHeThongItem.UpdateObject(context.Request);
                        oLogHeThongDA.UpdateObject<LogHeThongItem>(oLogHeThongItem);
                        AddLog("UPDATE", "LogHeThong", "UPDATE", oGridRequest.ItemID, $"Cập nhật nhật ký hệ thống{oLogHeThongItem.Title}");
                    }
                    else
                    {
                        oLogHeThongItem.UpdateObject(context.Request);
                        oLogHeThongDA.UpdateObject<LogHeThongItem>(oLogHeThongItem);
                        AddLog("UPDATE", "LogHeThong", "ADD", oGridRequest.ItemID, $"Thêm mới nhật ký hệ thống{oLogHeThongItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oLogHeThongDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "LogHeThong", "DELETE", oGridRequest.ItemID, $"Xóa nhật ký hệ thống{outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    AddLog("DELETE-MULTI", "LogHeThong", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều nhật ký hệ thống");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oLogHeThongDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "LogHeThong", "APPROVED", oGridRequest.ItemID, $"Duyệt nhật ký hệ thống{oLogHeThongDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oLogHeThongDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "LogHeThong", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt nhật ký hệ thống{oLogHeThongDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLogHeThongItem = oLogHeThongDA.GetByIdToObjectSelectFields<LogHeThongItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oLogHeThongItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}