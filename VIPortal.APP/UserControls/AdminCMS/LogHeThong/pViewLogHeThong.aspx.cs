﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.LogHeThong;

namespace VIPortal.APP.UserControls.AdminCMS.LogHeThong
{
    public partial class pViewLogHeThong : pFormBase
    {
        public LogHeThongItem oLogHeThong { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oLogHeThong = new LogHeThongItem();
            if (ItemID > 0)
            {
                LogHeThongDA oLogHeThongDA = new LogHeThongDA(UrlSite);
                oLogHeThong = oLogHeThongDA.GetByIdToObject<LogHeThongItem>(ItemID);
            }
        }
    }
}