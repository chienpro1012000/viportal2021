﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;

namespace VIPortal.APP.UserControls.AdminCMS.LogHeThong
{
    public partial class pViewLogByAction : pFormBase
    {
        public string ConfigGroup { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request["ConfigGroup"]))
            {
                ConfigGroup = Request["ConfigGroup"];
            }
        }
    }
}