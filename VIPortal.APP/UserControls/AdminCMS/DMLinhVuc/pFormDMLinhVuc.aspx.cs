﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData.DMLinhVuc;

namespace VIPortal.APP.UserControls.AdminCMS.DMLinhVuc
{
    public partial class pFormDMLinhVuc : pFormBase
    {
        public DMLinhVucItem oDMLinhVuc { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oDMLinhVuc = new DMLinhVucItem();
            if (ItemID > 0)
            {
                DMLinhVucDA oDMLinhVucDA = new DMLinhVucDA(UrlSite);
                oDMLinhVuc = oDMLinhVucDA.GetByIdToObject<DMLinhVucItem>(ItemID);
            }
        }
    }
}