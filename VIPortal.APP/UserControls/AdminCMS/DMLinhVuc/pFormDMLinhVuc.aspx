﻿<%@ page language="C#" autoeventwireup="true" codebehind="pFormDMLinhVuc.aspx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.DMLinhVuc.pFormDMLinhVuc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-DMLinhVuc" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oDMLinhVuc.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />

        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Lĩnh Vực Câu Hỏi</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" placeholder="Nhập tiêu đề lĩnh vực" value="<%=oDMLinhVuc.Title%>" />
            </div>
        </div>
        <%--<div class="form-group row">
            <label for="HoTen" class="col-sm-2 control-label">Số Thứ Tự</label>
            <div class="col-sm-4">
                <input type="number" name="DMSTT" id="DMSTT" min="0" placeholder="Nhập số thứ tự" oninput="validity.valid||(value='');" value="<%:(oDMLinhVuc!= null &&  oDMLinhVuc.DMSTT>0)?oDMLinhVuc.DMSTT+"": "" %>" class="form-control" />
            </div>
            <label for="DMVietTat" class="col-sm-2 control-label">Viết tắt</label>
            <div class="col-sm-4">
                <input type="text" name="DMVietTat" id="DMVietTat" placeholder="Nhập tên viết tắt" value="<%:oDMLinhVuc.DMVietTat%>" class="form-control" />
            </div>
        </div>--%>
        <div class="form-group row">
            <label for="DMMoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <textarea name="DMMoTa" id="DMMoTa" placeholder="Nhập mô tả lĩnh vực" class="form-control"><%=oDMLinhVuc.DMMoTa%></textarea>
            </div>
        </div>
        
        
    </form>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#KSBoChuDe").smSelect2018V2({
                dropdownParent: "#frm-DMLinhVuc"
            });
            
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $(".form-group select").select2({
                dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
            });

            $("#frm-DMLinhVuc").validate({
                rules: {
                    Title: "required",
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề lĩnh vực"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/DMLinhVuc/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-DMLinhVuc").trigger("click");
                            $(form).closeModal();
                            $('#tblDanhMucLinhVuc').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-DMLinhVuc").viForm();
        });
    </script>
</body>
</html>
