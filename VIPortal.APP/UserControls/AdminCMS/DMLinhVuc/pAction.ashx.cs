﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData.DMLinhVuc;
using ViPortalData.FAQThuongGap;

namespace VIPortal.APP.UserControls.AdminCMS.DMLinhVuc
{
    /// <summary>
    /// Summary description for pAction
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            DMLinhVucDA oDMLinhVucDA = new DMLinhVucDA(UrlSite);
            DMLinhVucItem oDMLinhVucItem = new DMLinhVucItem();
            FAQThuongGapDA oFAQThuongGapDA = new FAQThuongGapDA(UrlSite, true);
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    var oData = oDMLinhVucDA.GetListJsonSolr(new DMLinhVucQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oDMLinhVucDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "DMLinhVuc", "QUERYDATA", 0, "Xem danh sách danh mục lĩnh vực");
                    break;
                //case "MENUTREE":
                //    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                //    oResult.IsQuery = true;
                //    var oDataDMLinhVuc = oDMLinhVucDA.GetListJson(new DMLinhVucQuery(context.Request));
                //    treeViewItems = oDataDMLinhVuc.Select(x => new TreeViewItem()
                //    {
                //        key = x.DMDescription,
                //        title = x.Title
                //    }).ToList();
                //    oResult.OData = treeViewItems;
                //    break;

                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oDMLinhVucDA.GetListJson(new DMLinhVucQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDMLinhVucItem = oDMLinhVucDA.GetByIdToObject<DMLinhVucItem>(oGridRequest.ItemID);
                        oDMLinhVucItem.UpdateObject(context.Request);
                        oDMLinhVucDA.UpdateObject<DMLinhVucItem>(oDMLinhVucItem);
                        AddLog("UPDATE", "DMLinhVuc", "UPDATE", oGridRequest.ItemID, $"Cập nhật danh mục lĩnh vực{oDMLinhVucItem.Title}");
                    }
                    else
                    {
                        oDMLinhVucItem.UpdateObject(context.Request);
                        oDMLinhVucDA.UpdateObject<DMLinhVucItem>(oDMLinhVucItem);
                        AddLog("UPDATE", "DMLinhVuc", "ADD", oGridRequest.ItemID, $"Thêm mới danh mục lĩnh vực{oDMLinhVucItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oDMLinhVucDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "DMLinhVuc", "DELETE", oGridRequest.ItemID, $"Xóa danh mục lĩnh vực{outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    AddLog("DELETE-MULTI", "DMLinhVuc", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều danh mục lĩnh vực");
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oDMLinhVucDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oDMLinhVucDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "DMLinhVuc", "APPROVED", oGridRequest.ItemID, $"Duyệt danh mục lĩnh vực{oDMLinhVucDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oDMLinhVucDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "DMLinhVuc", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt danh mục lĩnh vực{oDMLinhVucDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDMLinhVucItem = oDMLinhVucDA.GetByIdToObjectSelectFields<DMLinhVucItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oDMLinhVucItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}