﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.LichCaNhan;

namespace VIPortal.APP.UserControls.AdminCMS.LichCaNhan
{
    public partial class pViewLichCaNhan : pFormBase
    {
        public LichCaNhanItem oLichCaNhan = new LichCaNhanItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oLichCaNhan = new LichCaNhanItem();
            if (ItemID > 0)
            {
                LichCaNhanDA oLichCaNhanDA = new LichCaNhanDA();
                oLichCaNhan = oLichCaNhanDA.GetByIdToObject<LichCaNhanItem>(ItemID);
            }
        }
    }
}