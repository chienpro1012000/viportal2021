﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData.LichCaNhan;

namespace VIPortal.APP.UserControls.AdminCMS.LichCaNhan
{

    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);
            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            LichCaNhanDA oLichCaNhanDA = new LichCaNhanDA();
            LichCaNhanItem oLichCaNhanItem = new LichCaNhanItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    var oData = oLichCaNhanDA.GetListJsonSolr(new LichCaNhanQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oLichCaNhanDA.TongSoBanGhiSauKhiQuery);
                    AddLog("QUERYDATA", "LichCaNhan", "QUERYDATA", 0, "Xem danh sách lịch cá nhân");
                    oResult.OData = oGrid;
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataLichCaNhan = oLichCaNhanDA.GetListJson(new LichCaNhanQuery(context.Request));
                    treeViewItems = oDataLichCaNhan.Select(x => new TreeViewItem()
                    {
                        key = x.LichLanhDaoChuTri.Title,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;

                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oLichCaNhanDA.GetListJson(new LichCaNhanQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLichCaNhanItem = oLichCaNhanDA.GetByIdToObject<LichCaNhanItem>(oGridRequest.ItemID);
                        oLichCaNhanItem.UpdateObject(context.Request);
                        oLichCaNhanDA.UpdateObject<LichCaNhanItem>(oLichCaNhanItem);
                        oLichCaNhanDA.UpdateSPModerationStatus(oLichCaNhanItem.ID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                        AddLog("UPDATE", "LichCaNhan", "UPDATE", oGridRequest.ItemID, $"Cập nhật lịch cá nhân{oLichCaNhanItem.Title}");
                    }
                    else
                    {
                        oLichCaNhanItem.UpdateObject(context.Request);
                        oLichCaNhanDA.UpdateObject<LichCaNhanItem>(oLichCaNhanItem);
                        oLichCaNhanDA.UpdateSPModerationStatus(oLichCaNhanItem.ID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                        AddLog("UPDATE", "LichCaNhan", "ADD", oGridRequest.ItemID, $"Thêm mới lịch cá nhân {oLichCaNhanItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oLichCaNhanDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "LichCaNhan", "DELETE", oGridRequest.ItemID, $"Xóa lịch cá nhân {outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";                    
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oLichCaNhanDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    AddLog("DELETE-MULTI", "LichCaNhan", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều lịch cá nhân");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oLichCaNhanDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "LichCaNhan", "APPROVED", oGridRequest.ItemID, $"Duyệt lịch cá nhân {oLichCaNhanDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oLichCaNhanDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "LichCaNhan", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt lịch cá nhân {oLichCaNhanDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLichCaNhanItem = oLichCaNhanDA.GetByIdToObjectSelectFields<LichCaNhanItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oLichCaNhanItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}