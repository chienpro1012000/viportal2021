﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewLichCaNhan.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.LichCaNhan.pViewLichCaNhan" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên</label>
            <div class="col-sm-10">
                <%=oLichCaNhan.Title%>
            </div>
        </div>
        <div class="form-group row">
	        <label for="fldGroup" class="col-sm-2 control-label">Group</label>
            <div class="col-sm-4">
                <%:oLichCaNhan.fldGroup.LookupId%>
            </div>
            <label for="CreatedUser" class="col-sm-2 control-label">User</label>
            <div class="col-sm-4">
                <%:oLichCaNhan.CreatedUser.LookupId%>
            </div>
        </div>
        <div class="form-group row">
	        <label for="LichThoiGianBatDau" class="col-sm-2 control-label">Ngày bắt đầu</label>
            <div class="col-sm-4">
               <%:string.Format("{0:yyyy-MM-dd}",oLichCaNhan.LichThoiGianBatDau)%>
            </div>
            <label for="LichThoiGianKetThuc" class="col-sm-2 control-label">Ngày kết thúc</label>
            <div class="col-sm-4">
               <%:string.Format("{0:yyyy-MM-dd}",oLichCaNhan.LichThoiGianKetThuc)%>
            </div>
        </div>
        <div class="form-group row">
            <label for="LichLanhDaoChuTri" class="col-sm-2 control-label">Lãnh đạo chủ trì</label>
            <div class="col-sm-4">
               <%=oLichCaNhan.LichLanhDaoChuTri.LookupValue%>
            </div>
            <label for="LichTrangThai" class="col-sm-2 control-label">Trạng thái</label>
            <div class="col-sm-4">
               <%=oLichCaNhan.LichTrangThai%>
            </div>
        </div>
        <div class="form-group row">
	        <label for="LichDiaDiem" class="col-sm-2 control-label">Địa điểm</label>
	        <div class="col-sm-10">
		       <%=oLichCaNhan.LichDiaDiem%>
	        </div>
        </div>
        <div class="form-group row">
	        <label for="LichNoiDung" class="col-sm-2 control-label">Nội dung</label>
	        <div class="col-sm-10">
		       <%=oLichCaNhan.LichNoiDung%>
	        </div>
        </div>
        
        <div class="form-group row">
	        <label for="LichThanhPhanThamGia" class="col-sm-2 control-label">Thành phần tham gia</label>
	        <div class="col-sm-10">
		        <%=oLichCaNhan.LichThanhPhanThamGia%>
	        </div>
        </div>
        <div class="form-group row">
	        <label for="LichGhiChu" class="col-sm-2 control-label">Ghi chú</label>
	        <div class="col-sm-10">
		        <%=oLichCaNhan.LichGhiChu%>
	        </div>
        </div>
         <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oLichCaNhan.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oLichCaNhan.ListFileAttach[i].Url %>"><%=oLichCaNhan.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%=string.Format("{0:dd-MM-yyyy}", oLichCaNhan.Created)%>
            </div>
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}",oLichCaNhan.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
               <%=oLichCaNhan.Author.LookupValue%>
            </div>
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
               <%=oLichCaNhan.Editor.LookupValue%>
            </div>    
        </div>
    </form>
</body>
</html>
