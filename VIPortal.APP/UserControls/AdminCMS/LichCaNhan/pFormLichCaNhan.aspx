﻿<%@ page language="C#" autoeventwireup="true" codebehind="pFormLichCaNhan.aspx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.LichCaNhan.pFormLichCaNhan" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-LichCaNhan" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oLichCaNhan.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" name="CreatedUser" id="CreatedUser" value="<%=CurentUser.ID %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề lịch</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" placeholder="Nhập tên cuộc họp" value="<%=oLichCaNhan.Title%>" />
            </div>
        </div>

        <div class="form-group row">
            <label for="LichThoiGianBatDau" class="col-sm-2 control-label">Ngày bắt đầu</label>
            <div class="col-sm-4">
                <input type="text" name="LichThoiGianBatDau" id="LichThoiGianBatDau" value="<%:string.Format("{0:dd/MM/yyyy HH:mm}",oLichCaNhan.LichThoiGianBatDau)%>" class="form-control datetime" />
            </div>
            <label for="LichThoiGianKetThuc" class="col-sm-2 control-label">Ngày kết thúc</label>
            <div class="col-sm-4">
                <input type="text" name="LichThoiGianKetThuc" id="LichThoiGianKetThuc" value="<%:string.Format("{0:dd/MM/yyyy HH:mm}",oLichCaNhan.LichThoiGianKetThuc)%>" class="form-control datetime" />
            </div>
        </div>
        <!--<div class="form-group row">
            <label for="LichLanhDaoChuTri" class="col-sm-2 control-label">Lãnh đạo chủ trì</label>
            <div class="col-sm-10">
                <select data-selected="" data-url="/UserControls/AdminCMS/LUser/pAction.asp?do=QUERYDATA" data-place="Chọn người" name="LichLanhDaoChuTri" id="LichLanhDaoChuTri" class="form-control"></select>
            </div>
        </div>-->
        <div class="form-group row">
            <label for="LichDiaDiem" class="col-sm-2 control-label">Địa điểm</label>
            <div class="col-sm-10">
                <input type="text" name="LichDiaDiem" id="LichDiaDiem" class="form-control" value="<%=oLichCaNhan.LichDiaDiem%>" placeholder="Nhập địa điểm" />
            </div>
        </div>
        <div class="form-group row">
            <label for="LichNoiDung" class="col-sm-2 control-label">Nội dung</label>
            <div class="col-sm-10">
                <textarea name="LichNoiDung" id="LichNoiDung" class="form-control" placeholder="Nhập nội dung cuộc họp"><%=oLichCaNhan.LichNoiDung%></textarea>
            </div>
        </div>


        <div class="form-group row">
            <label for="LichGhiChu" class="col-sm-2 control-label">Ghi chú</label>
            <div class="col-sm-10">
                <textarea name="LichGhiChu" id="LichGhiChu" class="form-control" placeholder="Nhập ghi chú"><%=oLichCaNhan.LichGhiChu%></textarea>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $(document).ready(function () {

            //$("#LichLanhDaoChuTri").smSelect2018V2({
            //    dropdownParent: "#frm-LichCaNhan",
            //    Ajax: true
            //});
            $("#fldGroup").smSelect2018V2({
                dropdownParent: "#frm-LichCaNhan"
            });

            $("#Title").focus();
            $(".datetime").daterangepicker({
                timePicker: true,
                singleDatePicker: true,
                timePicker24Hour: true,
                locale: { format: 'DD/MM/YYYY HH:mm' }
            });

            $("#frm-LichCaNhan").validate({
                rules: {
                    Title: "required",
                    LichThoiGianBatDau: "required",
                    LichThoiGianKetThuc: "required",
                    LichDiaDiem: "required",
                    //LichThanhPhanThamGia: "required",
                },
                messages: {
                    Title: "Vui lòng nhập tên cuộc họp",
                    LichThoiGianBatDau: "Vui lòng nhập thời gian bắt đầu",
                    LichThoiGianKetThuc: "Vui lòng nhập thời gian kết thúc",
                    LichDiaDiem: "Vui lòng nhập địa điểm",
                    //LichThanhPhanThamGia: "Vui lòng nhập thành phần tham gia",
                },

                submitHandler: function (form) {
                    
                    var from = $("#LichThoiGianBatDau").val();
                    var to = $("#LichThoiGianKetThuc").val();

                    if (Date.parse(from) > Date.parse(to)) {
                        BootstrapDialog.show({
                            title: "Lỗi",
                            message: "Dữ liệu ngày bắt đầu nhỏ hơn ngày kết thúc"
                        });
                    }
                    else {
                        loading();
                        $.post("/UserControls/AdminCMS/LichCaNhan/pAction.asp", $(form).viSerialize(), function (result) {
                            Endloading();
                            if (result.State == 2) {
                                BootstrapDialog.show({
                                    title: "Lỗi",
                                    message: result.Message
                                });
                            }
                            else {
                                showMsg(result.Message);
                                //$("#btn-find-LichCaNhan").trigger("click");
                                $(form).closeModal();
                                $("#lichCaNhan_div_parent").data("smGrid2018").RefreshGrid();
                            }
                        }).always(function () { });
                    }
                }
            });
            $("#frm-LichCaNhan").viForm();
        });
    </script>
</body>
</html>

