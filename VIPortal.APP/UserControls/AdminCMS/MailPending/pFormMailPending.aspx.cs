﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.MailPending;

namespace VIPortal.APP.UserControls.AdminCMS.MailPending
{
    public partial class pFormMailPending : pFormBase
    {
        public MailPendingItem oMailPending { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oMailPending = new MailPendingItem();
            if (ItemID > 0)
            {
                MailPendingDA oMailPendingDA = new MailPendingDA();
                oMailPending = oMailPendingDA.GetByIdToObject<MailPendingItem>(ItemID);
            }
        }
    }
}