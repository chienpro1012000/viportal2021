﻿<%@ control language="C#" autoeventwireup="true" codebehind="uc_MailPending.ascx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.MailPending.uc_MailPending" %>
<div role="body-data" data-title="" class="content_wp" data-action="/UserControls/AdminCMS/MailPending/pAction.ashx" data-form="/UserControls/AdminCMS/MailPending/pFormMailPending.aspx" data-view="/UserControls/AdminCMS/MailPending/pViewMailPending.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="MailPendingSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title" />
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <button class="btn btn-primary act-add" type="button">Thêm mới</button>
                    </p>
                </div>
            </div>

            <div class="clsgrid table-responsive">

                <table id="tblMailPending" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function formatDate(strValue) {
        if (strValue == null) return "";
        //var date = new Date(strValue);
        var d = new Date(strValue);
        var month = d.getMonth() + 1;
        var day = d.getDate();
        var output = //(('' + hour).length < 2 ? '0' : '') + hour + ':' +
            //(('' + minute).length < 2 ? '0' : '') + minute + " ngày " +
            (('' + day).length < 2 ? '0' : '') + day + '/' +
            (('' + month).length < 2 ? '0' : '') + month + '/' +
            d.getFullYear();
        return output;
    }
    $(document).ready(function () {

        var $tagvalue = $(".content_wp");
        var $tblDanhMucChung = $("#tblMailPending").viDataTable(
            {
                "frmSearch": "MailPendingSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "mData": function (o) {
                                if (o.Title != null)
                                    return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                                else
                                    return "";
                            },
                            "name": "Title", "sTitle": "Title"
                        },
                        {
                            "mData": function (o) {
                                var gt = "";
                                if (o.EmailDaGui)
                                    gt = "Yes";
                                else gt = "No";
                                return '<a data-id="' + o.ID + '" class="act-view" href="javascript:;">' + gt + '</a>';
                            },
                            "name": "EmailDaGui", "sTitle": "Tình trạng gửi Email"
                        },
                        {
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '" class="act-view" href="javascript:;">' + o.EmailNhan + '</a>';
                            },
                            "name": "EmailNhan", "sTitle": "Email người nhận"
                        },
                        {
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '" class="act-view" href="javascript:;">' + o.EmailNoiDung + '</a>';
                            },
                            "name": "EmailNoiDung", "sTitle": "Nội dung Email"
                        },
                        {
                            "mData": function (o) {

                                return '<a data-id="' + o.ID + '" class="act-view" href="javascript:;">' + formatDate(o.EmailThoiGianGui) + '</a>';
                            },
                            "name": "EmailThoiGianGui", "sTitle": "Thời gian gửi Email"
                        },
                        {
                            "mData": function (o) {
                                var gt = "";
                                if (o.SMSDaGui)
                                    gt = "Yes";
                                else gt = "No";
                                return '<a data-id="' + o.ID + '" class="act-view" href="javascript:;">' + gt + '</a>';
                            },
                            "name": "SMSDaGui", "sTitle": "Tình trạng gửi SMS"
                        },
                        {
                            "mData": function (o) {

                                return '<a data-id="' + o.ID + '" class="act-view" href="javascript:;">' + formatDate(o.SMSThoiGianGui) + '</a>';
                            },
                            "name": "SMSThoiGianGui", "sTitle": "Thời gian gửi SMS"
                        },
                        {
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '" class="act-view" href="javascript:;">' + o.SMSSDT + '</a>';
                            },
                            "name": "SMSSDT", "sTitle": "Số điện thoại người nhận"
                        },

                        {
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '" class="act-view" href="javascript:;">' + o.SmsNoiDung + '</a>';
                            },
                            "name": "SMSNoiDung", "sTitle": "Nội dung SMS"
                        },





                        {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" title="Sửa" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        }, {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE" title="Xóa"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                "aaSorting": [0, 'asc']
            });
    });
</script>

