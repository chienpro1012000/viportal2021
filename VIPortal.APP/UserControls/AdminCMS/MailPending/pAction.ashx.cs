﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData.MailPending;

namespace VIPortal.APP.UserControls.AdminCMS.MailPending
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);


            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            MailPendingDA oMailPendingDA = new MailPendingDA();
            MailPendingItem oMailPendingItem = new MailPendingItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oMailPendingDA.GetListJson(new MailPendingQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oMailPendingDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "LichTapDoan", "QUERYDATA", 0, "Xem danh sách MailPending");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataMailPending = oMailPendingDA.GetListJson(new MailPendingQuery(context.Request));
                    treeViewItems = oDataMailPending.Select(x => new TreeViewItem()
                    {
                        key = x.Title,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;
                case "ALLJSON":

                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oMailPendingDA.GetListJson(new MailPendingQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;


                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oMailPendingItem = oMailPendingDA.GetByIdToObject<MailPendingItem>(oGridRequest.ItemID);
                        oMailPendingItem.UpdateObject(context.Request);
                        oMailPendingDA.UpdateObject<MailPendingItem>(oMailPendingItem);
                        AddLog("UPDATE", "MailPending", "UPDATE", oGridRequest.ItemID, $"Cập nhật MailPending{oMailPendingItem.Title}");
                    }
                    else
                    {
                        oMailPendingItem.UpdateObject(context.Request);
                        oMailPendingDA.UpdateObject<MailPendingItem>(oMailPendingItem);
                        AddLog("UPDATE", "MailPending", "ADD", oGridRequest.ItemID, $"Thêm mới MailPending{oMailPendingItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oMailPendingDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "MailPending", "DELETE", oGridRequest.ItemID, $"Xóa MailPending{outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    AddLog("DELETE-MULTI", "MailPending", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều MailPending");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oMailPendingDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "MailPending", "APPROVED", oGridRequest.ItemID, $"Duyệt MailPending{oMailPendingDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oMailPendingDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "MailPending", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt MailPending{oMailPendingDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oMailPendingItem = oMailPendingDA.GetByIdToObjectSelectFields<MailPendingItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oMailPendingItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}