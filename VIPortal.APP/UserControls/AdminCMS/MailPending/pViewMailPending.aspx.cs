﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.MailPending;

namespace VIPortal.APP.UserControls.AdminCMS.MailPending
{
    public partial class pViewMailPending : pFormBase
    {
        public MailPendingItem oMailPending = new MailPendingItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oMailPending = new MailPendingItem();
            if (ItemID > 0)
            {
                MailPendingDA OMailPendingDA = new MailPendingDA();
                oMailPending = OMailPendingDA.GetByIdToObject<MailPendingItem>(ItemID);

            }
        }
    }
}