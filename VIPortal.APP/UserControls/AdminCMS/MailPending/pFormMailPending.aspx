﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormMailPending.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.MailPending.pFormMailPending" %>



<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-MailPending" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oMailPending.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />

        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" placeholder="Nhập vào tiêu đề (bắt buộc)" id="Title" class="form-control" value="<%=oMailPending.Title%>" />
        </div>
</div>
        <div>
            <label for="EmailDaGui">Tình trạng gửi Email</label>

             <select  style="margin-left: 5%; margin-block-start: -9px;" name="EmailDaGui" id="EmailDaGui">
             <option value="true">Yes</option>
             <option value="false">No</option>       
            </select>
        </div>

        <div class="form-group row">
            <label for="EmailNhan" class="col-sm-2 control-label" style="   margin-block-start: -1px;">Email người nhận</label>
            <div class="col-sm-10"     style="margin-block-start: 5px;">
                <input type="text" name="EmailNhan" id="EmailNhan" placeholder="Nhập Email người nhận" value="<%:oMailPending.EmailNhan%>" class="form-control" />
            </div>
        </div>

        <div class="form-group row">
            <label for="EmailNoiDung" class="col-sm-2 control-label" style=" margin-block-start: -3px;">Nội dung Email</label>
            <div class="col-sm-10" style="margin-block-start: -9px;">
                <input type="text" name="EmailNoiDung" placeholder="Nhập nội dung Email" id="EmailNoiDung" value="<%:oMailPending.EmailNoiDung%>" class="form-control" />
            </div>
        </div>

            <div class="form-group row">
                 <label for="EmailThoiGianGui" style="margin-block-start: -7px;" class="col-sm-2 control-label">Thời gian gửi Email</label>
                <div class="col-sm-10" style="margin-block-start: -45px; margin-left: 130px;">
                <input type="date" id="EmailThoiGianGui" name="EmailThoiGianGui" value="<%:oMailPending.EmailThoiGianGui%>" />
                 </div>
            </div>

        <div >
            <label for="SMSDaGui" style="margin-block-start: -32px;">Tình trạng gửi SMS</label>

             <select  style="margin-left: 5%; margin-block-start: -42px;" name="SMSDaGui" id="SMSDaGui">
             <option value="true">Yes</option>
             <option value="false">No</option>       
            </select>
        </div>

        
        <div class="form-group row">
                 <label for="SMSThoiGianGui" style="margin-block-start: -9px;" class="col-sm-2 control-label">Thời gian gửi SMS</label>
                <div class="col-sm-10" style="margin-block-start: -5px;">
                <input type="date" id="SMSThoiGianGui" name="SMSThoiGianGui" value="<%:oMailPending.SMSThoiGianGui%>" />
                 </div>
            </div>

         <div class="form-group row">
            <label for="SMSSDT" class="col-sm-2 control-label" style="margin-block-start: -21px;">Số điện thoại người nhận</label>
            <div class="col-sm-10" style="margin-block-start: -18px;">
                <input type="text" name="SMSSDT" id="SMSSDT" placeholder="Nhập sô người nhận " value="<%:oMailPending.SMSSDT%>" class="form-control" />
            </div>
        </div>

         <div class="form-group row">
            <label for="SmsNoiDung" style="margin-block-start: -4px;" class="col-sm-2 control-label">Nội dung SMS</label>
            <div class="col-sm-10" style="margin-block-start: -11px;">
                <input type="text" name="SmsNoiDung" id="SmsNoiDung" placeholder="Nhập nội dung sms" value="<%:oMailPending.SmsNoiDung%>" class="form-control" />
            </div>
        </div>



   <%--    
        <div class="form-group row">
            <label class="control-label col-sm-2">Giới Tính:</label>
            <div class="col-sm-4">
                <div class="checkbox">
                    <input type="" id="nam" name="GioiTinh" value="True"  <%:oMailPending.GioiTinh?"checked":"" %>/>
                    <label for="nam" style="padding-left: 1%; padding-right: 10%;">Nam</label>
                    <input type="radio" id="nu" name="GioiTinh" value="False" <%:!oMailPending.GioiTinh?"checked":"" %> />
                    <label for="nu" style="padding-left: 1%;">Nữ</label>
                </div>
            </div>
        </div>--%>

  </form>
 

    <script type="text/javascript">
        $(document).ready(function () {
            $("#submit").click(function () {
                    $.post("/UserControls/AdminCMS/MailPending/pAction.ashx", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-MailPending").trigger("click");
                            $(form).closeModal();
                            $('#tblMailPending').DataTable().ajax.reload();
                        }
                    }).always(function () { });
            })
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oMailPending.ListFileAttach)%>'
            });
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $(".form-group select").select2({
                dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
            });

            $("#frm-MailPending").validate({
                rules: {
                    Title: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/MailPending/pAction.ashx", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-MailPending").trigger("click");
                            $(form).closeModal();
                            $('#tblMailPending').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
        });
    </script>
</body>
</html>

