﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewMailPending.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.MailPending.pViewMailPending" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <%=oMailPending.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="UrlList" class="col-sm-2 control-label">Nội dung mail</label>
            <div class="col-sm-10">
                <%=oMailPending.EmailNoiDung%>
            </div>
        </div>
        <div class="form-group row">
            <label for="TrangThai" class="col-sm-2 control-label">Người nhận</label>
            <div class="col-sm-4">
                <%=oMailPending.NguoiNhan%>
            </div>
            <label for="TrangThai" class="col-sm-2 control-label">Email nhận</label>
            <div class="col-sm-4">
                <%=oMailPending.EmailNhan%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">Người gửi</label>
            <div class="col-sm-4">
                <%=oMailPending.NguoiGui%>
            </div>
            <label class="col-sm-2 control-label">Thời gian gửi</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oMailPending.EmailThoiGianGui)%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">Tình trạng gửi mail</label>
            <div class="col-sm-4">
                <%=oMailPending.EmailDaGui ? "Đã gửi" : "Chưa gửi"%>
            </div>
            <label class="col-sm-2 control-label">Tình trạng gửi sms</label>
            <div class="col-sm-4">
                <%=oMailPending.SMSDaGui ? "Đã gửi" : "Chưa gửi"%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">Số điện thoại</label>
            <div class="col-sm-10">
                <%=oMailPending.SMSSDT%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">Nội dung sms</label>
            <div class="col-sm-10">
                <%=oMailPending.SmsNoiDung%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oMailPending.Created)%>
            </div>
            <label class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oMailPending.Modified)%>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>
