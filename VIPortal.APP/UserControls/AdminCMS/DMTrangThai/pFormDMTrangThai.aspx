<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormDMTrangThai.aspx.cs" Inherits="VIPortalAPP.pFormDMTrangThai" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-DMTrangThai" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oDMTrangThai.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oDMTrangThai.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="DMVietTat" class="col-sm-2 control-label">Mã trạng thái</label>
            <div class="col-sm-10">
                <input type="text" name="DMVietTat" id="DMVietTat" placeholder="Nhập mã trạng thái" value="<%:oDMTrangThai.DMVietTat%>" class="form-control" />
            </div>
        </div>

        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oDMTrangThai.ListFileAttach)%>'
            });
            $("#Title").focus();
            
            $("#frm-DMTrangThai").validate({
                rules: {
                    Title: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/DMTrangThai/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-DMTrangThai").trigger("click");
                            $(form).closeModal();
                            $('#tblDMTrangThai').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
        });
    </script>
</body>
</html>
