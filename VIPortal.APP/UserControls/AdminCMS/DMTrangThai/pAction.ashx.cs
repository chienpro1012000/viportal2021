using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ViPortal_Utils.Base;

namespace VIPortalAPP.DMTrangThai
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : IHttpHandler
    {
        public string doAction = string.Empty;
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            if (!string.IsNullOrEmpty(context.Request["do"]))
                doAction = context.Request["do"].ToUpper();

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            DMTrangThaiDA oDMTrangThaiDA = new DMTrangThaiDA();
            DMTrangThaiItem oDMTrangThaiItem = new DMTrangThaiItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oDMTrangThaiDA.GetListJson(new DMTrangThaiQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw, 
                        oDMTrangThaiDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oDMTrangThaiDA.GetListJson(new DMTrangThaiQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDMTrangThaiItem = oDMTrangThaiDA.GetByIdToObject<DMTrangThaiItem>(oGridRequest.ItemID);
                        oDMTrangThaiItem.UpdateObject(context.Request);
                        oDMTrangThaiDA.UpdateObject<DMTrangThaiItem>(oDMTrangThaiItem);
                    }
                    else
                    {
                        oDMTrangThaiItem.UpdateObject(context.Request);
                        oDMTrangThaiDA.UpdateObject<DMTrangThaiItem>(oDMTrangThaiItem);
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    string outb = oDMTrangThaiDA.DeleteObject(oGridRequest.ItemID);
                    if (string.IsNullOrEmpty(outb))
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb;
                    break;
                case "DELETE-MULTI":
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oDMTrangThaiDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                     oDMTrangThaiDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDMTrangThaiItem = oDMTrangThaiDA.GetByIdToObjectSelectFields<DMTrangThaiItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oDMTrangThaiItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}