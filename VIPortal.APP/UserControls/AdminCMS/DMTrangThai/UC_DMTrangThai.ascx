<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_DMTrangThai.aspx.cs" Inherits="VIPortalAPP.UC_DMTrangThai" %>
<div role="body-data" data-title="Trạng thái" class="content_wp" data-action="/UserControls/AdminCMS/DMTrangThai/pAction.asp" data-form="/UserControls/AdminCMS/DMTrangThai/pFormDMTrangThai.aspx">
    <div class="clsmanager row">
        <div class="col-sm-9">
            <div id="DMTrangThaiSearch" class="form-inline zonesearch">
                <div class="form-group">
                    <input type="hidden" name="do" id="do" value="QUERYDATA" />
                    <label for="Keyword">Từ khóa</label>
                    <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                    <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title" />
                </div>
                <button  type="button" class="btn btn-default act-search">Tìm kiếm</button>
            </div>
        </div>
        <div class="col-sm-3">
            <p class="text-right">
                <button class="btn btn-primary act-add" data-per="100302" type="button">Thêm mới</button>
            </p>
        </div>
    </div>
    <div class="clsgrid table-responsive">
        <table id="tblDMTrangThai" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
        </table>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        var $tagvalue = $(".content_wp");
        var $tblDanhMucChung = $("#tblDMTrangThai").viDataTable(
             {
                 "frmSearch": "DMTrangThaiSearch",
                 "url": $tagvalue.data("action"),
                 "aoColumns":
                  [
                     {
                         "mData": function (o) {
                             return '<a data-id="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                         },
                         "name": "Title", "sTitle": "Tiêu đề"
                     },
                     {
                         "data": "DMVietTat",
                         "name": "DMVietTat", "sTitle": "Mã trạng thái"
                     }, {
                         "mData": null,
                         "bSortable": false,
                         "className": "all",
                         "sWidth": "18px",
                         "mRender": function (o) {
                             if (o._ModerationStatus == 0)

                                 return '<a data-per="100305" title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                             else
                                 return '<a data-per="100305" title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                         }
                     },{
                         "mData": null,
                         "bSortable": false,
                         "sWidth": "18px",
                         "className": "all",
                        "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" data-itemid="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                     },{
                        "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="fa fa-trash-o"></i></a>',
                        "mData": null,
                        "bSortable": false,

                        "className": "all",
                        "sWidth": "18px",
                        "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" href="javascript:;"><i class="fa fa-trash-o"></i></a>';}
                    },{
                        "sTitle": '<input class="checked-all" type="checkbox" />',
                        "mData": null,
                        "bSortable": false,
                        "sWidth": "18px",
                        "className": "all",
                        "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                    }
                  ],
                 "aaSorting": [0, 'asc']
             });
    });
</script>
