using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public partial class pFormDMTrangThai : pFormBase
    {
        public DMTrangThaiItem oDMTrangThai {get;set;}
        protected void Page_Load(object sender, EventArgs e)
        {
			oDMTrangThai = new DMTrangThaiItem();
            if (ItemID > 0)
            {
                DMTrangThaiDA oDMTrangThaiDA = new DMTrangThaiDA();
                oDMTrangThai = oDMTrangThaiDA.GetByIdToObject<DMTrangThaiItem>(ItemID);
            }
        }
    }
}