﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.LTinDiemBao;

namespace VIPortal.APP.UserControls.AdminCMS.LTinDiemBao
{
    public partial class pFormLTinDiemBao : pFormBase
    {
        public LTinDiemBaoItem oLTinDiemBao { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oLTinDiemBao = new LTinDiemBaoItem();
            if (ItemID > 0)
            {
                LTinDiemBaoDA oLTinDiemBaoDA = new LTinDiemBaoDA();
                oLTinDiemBao = oLTinDiemBaoDA.GetByIdToObject<LTinDiemBaoItem>(ItemID);
            }
        }
    }
}