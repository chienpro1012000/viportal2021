﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormLTinDiemBao.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.LTinDiemBao.pFormLTinDiemBao" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-LTinDiemBao" class="form-horizontal" >
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oLTinDiemBao.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên tin</label>
            <div class="col-sm-4">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oLTinDiemBao.Title%>" />
            </div>
              <label for="CreatedDate" class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
                 <input type="date" name="CreatedDate" id="CreatedDate" value="<%:string.Format("{0:yyyy-MM-dd}", oLTinDiemBao.CreatedDate)%>" class="form-control" />   
            </div>
        </div>		
        <div class="form-group row">
	        <label for="CreatedUser" class="col-sm-2 control-label">User</label>
            <div class="col-sm-4">
                 <select data-selected="<%:oLTinDiemBao.CreatedUser.LookupId%>" data-url="/UserControls/AdminCMS/LUser/pAction.ashx?do=QUERYDATA" data-place="Chọn người hỏi" name="CreatedUser" id="CreatedUser" class="form-control"></select>
            </div>
            <label for="ReadCount" class="col-sm-2 control-label">Lượt đọc</label>
	        <div class="col-sm-4">
		        <input type="text" name="ReadCount" id="ReadCount" placeholder="Nhập số lượt đọc" value="<%:(oLTinDiemBao!= null &&  oLTinDiemBao.ReadCount>0)?oLTinDiemBao.ReadCount+"": "" %>" class="form-control" />                   
	        </div>
        </div>
        <div class="form-group row">
	        <label for="DescriptionNews" class="col-sm-2 control-label">Mô tả tin tức</label>
	        <div class="col-sm-10">
		        <textarea  name="DescriptionNews" id="DescriptionNews" class="form-control"><%:oLTinDiemBao.DescriptionNews%></textarea>
	        </div>
        </div>
        <div class="form-group row">
	        <label for="SourceNews" class="col-sm-2 control-label">Nguồn tin</label>
	        <div class="col-sm-10">
		        <textarea name="SourceNews" id="SourceNews" class="form-control"><%=oLTinDiemBao.SourceNews%></textarea>
	        </div>
        </div>
         <div class="form-group row">
              <label for="ImageNews" class="col-sm-2 control-label">Ảnh đại diện</label>
               <div class="col-sm-10" data-role="groupImageDD">

                     <input type="hidden" name="ImageNews" id="ImageNews" value="<%:oLTinDiemBao.ImageNews%>" />
                     <img title="Ảnh đại diện" id="srcImageNews" src="<%=oLTinDiemBao.ImageNews%>" />
                     <button id="btnImageNews" type="button" class="btn  btn-success">Ảnh đại diện</button>
                     <button id="btnXoaImageNews" type="button" class="btn btn-danger">Xóa ảnh</button>
                </div>
         </div>
         <div class="form-group row">
              <label for="SourceNews" class="col-sm-2 control-label">Nội dung tin tức</label>
               <div class="col-sm-10">
                     <textarea name="ContentNews" id="ContentNews" class="form-control"><%=oLTinDiemBao.ContentNews%></textarea>
               </div>
         </div>
    </form>
<script type="text/javascript">
        $(document).ready(function () {
            $("#CreatedUser").smSelect2018V2({
                dropdownParent: "#frm-LTinDiemBao"
            });
            CKEDITOR.replace("ContentNews", {
                height: 50
            });
            $("#btnImageNews").click(function () {
                $elementId = $(this);
                CKFinder.popup({
                    chooseFiles: true,
                    width: 800,
                    height: 600,
                    onInit: function (finder) {
                        finder.on('files:choose', function (evt) {
                            var file = evt.data.files.first();
                            //var output = document.getElementById(elementId);
                            //console.log(file.getUrl());
                            //$("#ImageNews").value(file.getUrl());
                            var urlfileimg = file.getUrl();
                            document.getElementById("ImageNews").value = urlfileimg;
                            document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                        });
                        finder.on('file:choose:resizedImage', function (evt) {
                            //var output = document.getElementById(elementId);
                            console.log(evt.data);
                            var urlfileimg = evt.data.resizedUrl;
                            document.getElementById("ImageNews").value = urlfileimg;
                            document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                        });
                    }
                });
            });
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                timePicker: true,
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY HH:mm' }
            });
            $(".form-group select").select2({
                dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
            });

            $("#frm-LTinDiemBao").validate({
                rules: {
                    Title: "required",
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề",
                },
               
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/LTinDiemBao/pAction.ashx", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-LTinDiemBao").trigger("click");
                            $(form).closeModal();
                            $('#tblLTinDiemBao').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
        });
    </script>
</body>
</html>

