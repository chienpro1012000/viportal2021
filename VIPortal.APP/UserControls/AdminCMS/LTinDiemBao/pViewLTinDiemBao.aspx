﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewLTinDiemBao.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.LTinDiemBao.pViewLTinDiemBao" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên tin</label>
            <div class="col-sm-4">
               <%=oLTinDiemBao.Title%>
            </div>
              <label for="CreatedDate" class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
                <%:string.Format("{0:dd-MM-yyyy}", oLTinDiemBao.CreatedDate)%>
            </div>
        </div>		
        <div class="form-group row">
	        <label for="CreatedUser" class="col-sm-2 control-label">User</label>
            <div class="col-sm-4">
                 <%:oLTinDiemBao.CreatedUser.LookupId%>
            </div>
            <label for="ReadCount" class="col-sm-2 control-label">Lượt đọc</label>
	        <div class="col-sm-4">
		        <%:(oLTinDiemBao!= null &&  oLTinDiemBao.ReadCount>0)?oLTinDiemBao.ReadCount+"": "" %>
	        </div>
        </div>
        <div class="form-group row">
	        <label for="DescriptionNews" class="col-sm-2 control-label">Mô tả tin tức</label>
	        <div class="col-sm-10">
		        <%:oLTinDiemBao.DescriptionNews%>
	        </div>
        </div>
        <div class="form-group row">
	        <label for="SourceNews" class="col-sm-2 control-label">Nguồn tin</label>
	        <div class="col-sm-10">
		      <%=oLTinDiemBao.SourceNews%>
	        </div>
        </div>
         <div class="form-group row">
              <label for="ImageNews" class="col-sm-2 control-label">Ảnh đại diện</label>
               <div class="col-sm-10" data-role="groupImageDD">
                   <img src="<%=oLTinDiemBao.ImageNews%>" />                     
                </div>
         </div>
         <div class="form-group row">
              <label for="SourceNews" class="col-sm-2 control-label">Nội dung tin tức</label>
               <div class="col-sm-10">
                    <%=oLTinDiemBao.ContentNews%>
               </div>
         </div>
         <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oLTinDiemBao.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oLTinDiemBao.ListFileAttach[i].Url %>"><%=oLTinDiemBao.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
       
		<div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%=string.Format("{0:dd-MM-yyyy}", oLTinDiemBao.Created)%>
            </div>
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}",oLTinDiemBao.Modified)%>
            </div>
        </div>

        <div class="form-group row">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
               <%=oLTinDiemBao.Author.LookupValue%>
            </div>
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
               <%=oLTinDiemBao.Editor.LookupValue%>
            </div>    
        </div>
    </form>
</body>
</html>
