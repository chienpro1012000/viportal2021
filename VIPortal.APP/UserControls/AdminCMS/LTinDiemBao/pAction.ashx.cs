﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData.LTinDiemBao;

namespace VIPortal.APP.UserControls.AdminCMS.LTinDiemBao
{

    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            LTinDiemBaoDA oLTinDiemBaoDA = new LTinDiemBaoDA();
            LTinDiemBaoItem oLTinDiemBaoItem = new LTinDiemBaoItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    var oData = oLTinDiemBaoDA.GetListJson(new LTinDiemBaoQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oLTinDiemBaoDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "LTinDiemBao", "QUERYDATA", 0, "Xem danh sách tin điểm báo");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataLTinDiemBao = oLTinDiemBaoDA.GetListJson(new LTinDiemBaoQuery(context.Request));
                    treeViewItems = oDataLTinDiemBao.Select(x => new TreeViewItem()
                    {
                        key = x.ContentNews,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;

                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oLTinDiemBaoDA.GetListJson(new LTinDiemBaoQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLTinDiemBaoItem = oLTinDiemBaoDA.GetByIdToObject<LTinDiemBaoItem>(oGridRequest.ItemID);
                        oLTinDiemBaoItem.UpdateObject(context.Request);
                        oLTinDiemBaoDA.UpdateObject<LTinDiemBaoItem>(oLTinDiemBaoItem);
                        AddLog("UPDATE", "LTinDiemBao", "UPDATE", oGridRequest.ItemID, $"Cập nhật tin điểm báo{oLTinDiemBaoItem.Title}");
                    }
                    else
                    {
                        oLTinDiemBaoItem.UpdateObject(context.Request);
                        oLTinDiemBaoDA.UpdateObject<LTinDiemBaoItem>(oLTinDiemBaoItem);
                        AddLog("UPDATE", "LTinDiemBao", "ADD", oGridRequest.ItemID, $"Thêm mới tin điểm báo{oLTinDiemBaoItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oLTinDiemBaoDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "LTinDiemBao", "DELETE", oGridRequest.ItemID, $"Xóa tin điểm báo{outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    AddLog("DELETE-MULTI", "LTinDiemBao", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều tin điểm báo");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oLTinDiemBaoDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "LTinDiemBao", "APPROVED", oGridRequest.ItemID, $"Duyệt tin điểm báo{oLTinDiemBaoDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oLTinDiemBaoDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "LTinDiemBao", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt tin điểm báo{oLTinDiemBaoDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLTinDiemBaoItem = oLTinDiemBaoDA.GetByIdToObjectSelectFields<LTinDiemBaoItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oLTinDiemBaoItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}