﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormKSKetQua.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.KSKetQua.pFormKSKetQua" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-KSKetQua" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oKSKetQua.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Câu trả lời</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oKSKetQua.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="KSCauTraLoiNoiDung" class="col-sm-2 control-label">Nội dung trả lời</label>
            <div class="col-sm-10">
                <input type="text" name="KSCauTraLoiNoiDung" id="KSCauTraLoiNoiDung" placeholder="Nhập nội dung câu trả lời" value="<%:oKSKetQua.KSCauTraLoiNoiDung%>" class="form-control" />
            </div>
        </div>
         <div class="form-group row">
            <label for="LookUp" class="col-sm-2 control-label">Bộ chủ đề</label>
            <div class="col-sm-10">
                <select data-selected="<%:oKSKetQua.KSBoChuDe.LookupId%>" data-url="/UserControls/AdminCMS/KSBoCauHoi/pAction.asp?do=QUERYDATA" data-place="Chọn bộ chủ đề" name="KSBoChuDe" id="KSBoChuDe" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <label for="LookUp" class="col-sm-2 control-label">Bộ câu hỏi</label>
            <div class="col-sm-10">
                <select data-selected="<%:oKSKetQua.KSCauHoi.LookupId%>" data-url="/UserControls/AdminCMS/KSBoCauHoi/pAction.asp?do=QUERYDATA" data-place="Chọn bộ câu hỏi" name="KSCauHoi" id="KSCauHoi" class="form-control"></select>
            </div>
        </div>
         <div class="form-group row">
            <label for="LookUp" class="col-sm-2 control-label">Bộ câu trả lời</label>
            <div class="col-sm-10">
                <select data-selected="<%:oKSKetQua.KSCauTraLoi.LookupId%>" data-url="/UserControls/AdminCMS/KSCauTraLoi/pAction.asp?do=QUERYDATA" data-place="Chọn bộ câu trả lời" name="KSCauTraLoi" id="KSCauTraLoi" class="form-control"></select>
            </div>
        </div>
         <div class="form-group row">
            <label for="LookUp" class="col-sm-2 control-label">Người trả lời</label>
            <div class="col-sm-10">
                <select data-selected="<%:oKSKetQua.FQNguoiTraLoi.LookupId%>" data-url="/UserControls/AdminCMS/LUser/pAction.asp?do=QUERYDATA" data-place="Chọn người trả lời" name="FQNguoiTraLoi" id="FQNguoiTraLoi" class="form-control"></select>
            </div>
        </div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#KSBoChuDe").smSelect2018V2({
                dropdownParent: "#frm-KSKetQua"
            });
            $("#KSCauHoi").smSelect2018V2({
                dropdownParent: "#frm-KSKetQua"
            });
            $("#KSCauTraLoi").smSelect2018V2({
                dropdownParent: "#frm-KSKetQua"
            });
            $("#FQNguoiTraLoi").smSelect2018V2({
                dropdownParent: "#frm-KSKetQua"
            });
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oKSKetQua.ListFileAttach)%>'
            });
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $("#frm-KSKetQua").validate({
                rules: {
                    Title: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/KSKetQua/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-KSKetQua").trigger("click");
                            $(form).closeModal();
                            $('#tblKSKetQua').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-KSKetQua").viForm();
        });
    </script>
</body>
</html>


