﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.KSKetQua;

namespace VIPortal.APP.UserControls.AdminCMS.KSKetQua
{

    public partial class pFormKSKetQua : pFormBase
    {
        public KSKetQuaItem oKSKetQua { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oKSKetQua = new KSKetQuaItem();
            if (ItemID > 0)
            {
                KSKetQuaDA oKSKetQuaDA = new KSKetQuaDA(UrlSite);
                oKSKetQua = oKSKetQuaDA.GetByIdToObject<KSKetQuaItem>(ItemID);
            }
        }
    }
}