﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pListKetQua.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.KSKetQua.pListKetQua" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        a {
            cursor: pointer;
        }
        .tab1{margin-left:50px}
        .tab2{margin-left:100px}
    </style>
</head>
<body>
    <%if(IdChuDe > 0){ %>
    <div id="Total" style="float: right;"></div>
    <input type="hidden" id="soluotbinhchon" value="<%=Total %>" />
    <table class="table table-bordered">
        <thead>
            <tr>
                <th scope="col">Tiêu đề</th>
                <th scope="col">Số lượt bình chọn</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="font-weight:600">Chủ đề: <%=oBoChuDe.Title %></td>
                <td></td>
            </tr>
            <%int i = 1; %>
            <%foreach(var item in lstCauHoi){ %>
            <tr>
                <td style="font-weight:600"><span class="tab1"></span> Câu hỏi <%=i %>: <%=item.Title %></td>
                <td></td>
            </tr>
                <%foreach (var itemCauTraLoi in GetCauTraLoiByID(item.ID))
                        {%>
                <tr>
                    <td><span class="tab2"></span> Câu trả lời: <%=itemCauTraLoi.Title %></td>
                    <td><%=itemCauTraLoi.SoLuotBinhChon %></td>
                </tr>
                <%} %>
            <%i++; %>
            <%} %>
        </tbody>
    </table>
    <% }else{ %>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th scope="col">Danh sách chủ đề</th>
                <th scope="col">Số câu hỏi thuộc chủ đề</th>
            </tr>
        </thead>
        <tbody>
            <%foreach(var item in lstBoChuDe){ %>
            <tr>
                <td><a id="<%=item.ID %>" class="TenChuDe"><%=item.Title %> </a></td>
                <td style="text-align: center;"><%=GetSoCauHoi(item.ID) %></td>
            </tr>
            <%} %>
        </tbody>
    </table>
    <%} %>
</body>
</html>
<script>
    $(document).ready(function () {
        $("#Total").text("Tổng số lượt bình chọn: " + $("#soluotbinhchon").val());
        $("a.TenChuDe").click(function () {
            var idChuDe = $(this).attr("id");
            loadAjaxContent("/UserControls/AdminCMS/KSKetQua/pListKetQua.aspx", "#tblKSKetQua", { IdChuDe: idChuDe });
        });
    });
</script>
