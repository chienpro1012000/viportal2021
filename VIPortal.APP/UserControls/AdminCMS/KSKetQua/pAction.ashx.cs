﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData.KSKetQua;
namespace VIPortal.APP.UserControls.AdminCMS.KSKetQua
{/// <summary>
 /// Summary description for pAcion
 /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            KSKetQuaDA oKSKetQuaDA = new KSKetQuaDA(UrlSite);
            KSKetQuaItem oKSKetQuaItem = new KSKetQuaItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    var oData = oKSKetQuaDA.GetListJson(new KSKetQuaQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oKSKetQuaDA.TongSoBanGhiSauKhiQuery);
                    AddLog("QUERYDATA", "KSKetQuaDA", "QUERYDATA", 0, "Xem danh sách khảo sát kết quả");
                    oResult.OData = oGrid;
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataKSKetQua = oKSKetQuaDA.GetListJson(new KSKetQuaQuery(context.Request));
                    treeViewItems = oDataKSKetQua.Select(x => new TreeViewItem()
                    {
                        key = x.KSCauTraLoiNoiDung,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;

                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oKSKetQuaDA.GetListJson(new KSKetQuaQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oKSKetQuaItem = oKSKetQuaDA.GetByIdToObject<KSKetQuaItem>(oGridRequest.ItemID);
                        oKSKetQuaItem.UpdateObject(context.Request);
                        oKSKetQuaDA.UpdateObject<KSKetQuaItem>(oKSKetQuaItem);
                        AddLog("UPDATE", "KSKetQuaDA", "UPDATE", oGridRequest.ItemID, $"Cập nhật khảo sát kết quả{oKSKetQuaItem.Title}");
                    }
                    else
                    {
                        oKSKetQuaItem.UpdateObject(context.Request);
                        oKSKetQuaDA.UpdateObject<KSKetQuaItem>(oKSKetQuaItem);
                        AddLog("UPDATE", "KSKetQuaDA", "ADD", oGridRequest.ItemID, $"Thêm mới khảo sát kết quả {oKSKetQuaItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oKSKetQuaDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "KSKetQuaDA", "DELETE", oGridRequest.ItemID, $"Xóa khảo sát kết quả {outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oKSKetQuaDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    AddLog("DELETE-MULTI", "KSKetQuaDA", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều khảo sát kết quả");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oKSKetQuaDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "KSKetQuaDA", "APPROVED", oGridRequest.ItemID, $"Duyệt khảo sát kết quả {oKSKetQuaDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oKSKetQuaDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "KSKetQuaDA", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt khảo sát kết quả {oKSKetQuaDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oKSKetQuaItem = oKSKetQuaDA.GetByIdToObjectSelectFields<KSKetQuaItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oKSKetQuaItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}