﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData.KSBoCauHoi;
using ViPortalData.KSBoChuDe;
using ViPortalData.KSCauTraLoi;

namespace VIPortal.APP.UserControls.AdminCMS.KSKetQua
{
    //get danh sách chủ đề
    //get danh sách câu hỏi thuộc chủ đề
    //get câu trả lời thuộc câu hỏi    
    public partial class pListKetQua : pFormBase
    {
        public int IdChuDe { get; set; }
        public int Total { get; set; }
        public List<KSBoChuDeJson> lstBoChuDe { get; set; }
        public KSBoChuDeItem oBoChuDe { get; set; }
        public List<KSBoCauHoiJson> lstCauHoi = new List<KSBoCauHoiJson>();
        protected void Page_Load(object sender, EventArgs e)
        {
            KSBoChuDeDA oBoChuDeDA = new KSBoChuDeDA(UrlSite);
            KSBoCauHoiDA CauHoiDA = new KSBoCauHoiDA(UrlSite);
            IdChuDe = Convert.ToInt32(Request["IdChuDe"]);
            if (IdChuDe > 0)
            {
                oBoChuDe = oBoChuDeDA.GetByIdToObject<KSBoChuDeItem>(IdChuDe);
                lstCauHoi = CauHoiDA.GetListJson(new KSBoCauHoiQuery() { IdChuDe = IdChuDe });
                foreach (var item in lstCauHoi)
                {
                    Total += GetLuotBinhChon(item.ID);
                }
            }
            else
            {
                lstBoChuDe = oBoChuDeDA.GetListJson(new KSBoChuDeQuery());
            } 
        }
        public  int GetSoCauHoi(int ChuDeID)
        {
            List<KSBoCauHoiJson> lstCauHoi = new List<KSBoCauHoiJson>();
            KSBoCauHoiDA CauHoiDA = new KSBoCauHoiDA(UrlSite);
            lstCauHoi = CauHoiDA.GetListJson(new KSBoCauHoiQuery() { IdChuDe = ChuDeID });
            return lstCauHoi.Count();
        }
        public  List<KSCauTraLoiJson> GetCauTraLoiByID(int ItemID)
        {
            List<KSCauTraLoiJson> lstCauTraLoi = new List<KSCauTraLoiJson>();
            KSCauTraLoiDA objCauTraLoi = new KSCauTraLoiDA(UrlSite);
            lstCauTraLoi = objCauTraLoi.GetListJson(new KSCauTraLoiQuery() { IDCauHoi = ItemID });
            return lstCauTraLoi;
        }
        public int GetLuotBinhChon(int ItemID)
        {
            int luotBinhChon = 0;
            List<KSCauTraLoiJson> lstCauTraLoi = new List<KSCauTraLoiJson>();
            KSCauTraLoiDA objCauTraLoi = new KSCauTraLoiDA(UrlSite);
            lstCauTraLoi = objCauTraLoi.GetListJson(new KSCauTraLoiQuery() { IDCauHoi = ItemID });
            foreach (var item in lstCauTraLoi)
            {
                luotBinhChon += item.SoLuotBinhChon;
            }
            return luotBinhChon;
        }
    }
}