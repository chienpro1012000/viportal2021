﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData.KSBoChuDe;

namespace VIPortal.APP.UserControls.AdminCMS.KSKetQua
{
    public partial class UC_KSKetQua : BaseUC
    {
        public List<KSBoChuDeJson> lstBoChuDe { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            KSBoChuDeDA oBoChuDeDA = new KSBoChuDeDA(UrlSite);
            lstBoChuDe = oBoChuDeDA.GetListJson(new KSBoChuDeQuery());
        }
    }
}