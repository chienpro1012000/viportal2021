﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_KSKetQua.ascx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.KSKetQua.UC_KSKetQua" %>
<div role="body-data" data-title="kết quả khảo sát" class="content_wp" data-action="/UserControls/AdminCMS/KSKetQua/pAction.asp" data-form="/UserControls/AdminCMS/KSKetQua/pFormKSKetQua.aspx" data-view="/UserControls/AdminCMS/KSKetQua/pViewKSKetQua.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row" style="margin-bottom:15px">
                <div class="col-sm-9">
                    <div id="KSKetQuaSearch" class="form-inline zonesearch">
                        <div class="form-group" style="padding-right:15px">
                            <select name="IdChuDe" id="IdChuDe" class="form-control">
                                <option value="0">Chọn chủ đề</option>
                                <%foreach(var item in lstBoChuDe){%>
                                <option value="<%=item.ID %>"><%=item.Title %></option>
                                <%} %>
                            </select>
                        </div>
                        <%--<button type="button" class="btn btn-default act-search">Tìm kiếm</button>--%>
                        <button type="button" class="btn btn-primary" id="btnthongke">Thống kê</button>
                    </div>
                </div>
            </div>

            <div class="clsgrid table-responsive">
                <div id="tblKSKetQua"></div>
                <%--<table  class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>--%>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function LoadFormThongKe() {
        var idChuDe = $("#IdChuDe").val();
        loadAjaxContent("/UserControls/AdminCMS/KSKetQua/pListKetQua.aspx", "#tblKSKetQua", { IdChuDe : idChuDe});
    }
    $(document).ready(function () {
        LoadFormThongKe();
        $("#btnthongke").click(function () {
            LoadFormThongKe();
        });
        
    });
</script>


