﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewKSKetQua.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.KSKetQua.pViewKSKetQua" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		  <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Câu trả lời</label>
            <div class="col-sm-10">
                <%=oKSKetQua.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="KSCauTraLoiNoiDung" class="col-sm-2 control-label">Nội dung trả lời</label>
            <div class="col-sm-10">
                <%:oKSKetQua.KSCauTraLoiNoiDung%>
            </div>
        </div>
         <div class="form-group row">
            <label for="LookUp" class="col-sm-2 control-label">Bộ chủ đề</label>
            <div class="col-sm-10">
                <%:oKSKetQua.KSBoChuDe.LookupValue%>
            </div>
        </div>
        <div class="form-group row">
            <label for="LookUp" class="col-sm-2 control-label">Bộ câu hỏi</label>
            <div class="col-sm-10">
                <%:oKSKetQua.KSCauHoi.LookupValue%>
            </div>
        </div>
         <div class="form-group row">
            <label for="LookUp" class="col-sm-2 control-label">Bộ câu trả lời</label>
            <div class="col-sm-10">
                <%:oKSKetQua.KSCauTraLoi.LookupValue%>
             </div>
        </div>
         <div class="form-group row">
            <label for="LookUp" class="col-sm-2 control-label">Người trả lời</label>
            <div class="col-sm-10">
                <%:oKSKetQua.FQNguoiTraLoi.LookupValue%>
            </div>
        </div>
       <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oKSKetQua.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oKSKetQua.ListFileAttach[i].Url %>"><%=oKSKetQua.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
		  <div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%=string.Format("{0:dd-MM-yyyy}", oKSKetQua.Created)%>
            </div>
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}",oKSKetQua.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
               <%=oKSKetQua.Author.LookupValue%>
            </div>
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
               <%=oKSKetQua.Editor.LookupValue%>
            </div>    
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>