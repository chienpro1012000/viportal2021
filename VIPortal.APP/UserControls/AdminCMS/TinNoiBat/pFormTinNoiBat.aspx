﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormTinNoiBat.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.TinNoiBat.pFormTinNoiBat" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-TinNoiBat" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oTinNoiBat.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề tin tức</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title"placeholder="Nhập tiêu đề tin tức" class="form-control" value="<%=oTinNoiBat.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="DMSTT" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-4">
               <input type="text" name="DMSTT" id="DMSTT" placeholder="Nhập số thứ tự" value="<%:(oTinNoiBat!= null &&  oTinNoiBat.DMSTT>0)?oTinNoiBat.DMSTT+"": "" %>" class="form-control" />
            </div>
              <label for="CreatedDate" class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
                <input type="date" name="CreatedDate" id="CreatedDate" value="<%:string.Format("{0:yyyy-MM-dd}",oTinNoiBat.CreatedDate)%>" class="form-control"/>
            </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="DescriptionNews" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <textarea name="DescriptionNews" id="DescriptionNews" class="form-control" placeholder="Nhập mô tả"><%:oTinNoiBat.DescriptionNews%></textarea>
                
            </div>
        </div>
            
         <div class="form-group row">
            <label for="ImageNews" class="col-sm-2 control-label">Ảnh đại diện</label>
            <div class="col-sm-10" data-role="groupImageDD">

                <input type="hidden" name="ImageNews" id="ImageNews" value="<%:oTinNoiBat.ImageNews%>" />
                <img title="Ảnh đại diện" id="srcImageNews" src="<%=oTinNoiBat.ImageNews%>" />
                <button id="btnImageNews" type="button" class="btn  btn-success">Ảnh đại diện</button>
                    <button id="btnXoaImageNews" type="button" class="btn btn-danger">Xóa ảnh</button>
            </div>
        </div>
        <div class="form-group row">
            <label for="ContentNews" class="col-sm-2 control-label">Nội dung</label>
            <div class="col-sm-10">
                <textarea name="ContentNews" id="ContentNews" class="form-control" placeholder="Nhập nội dung"><%:oTinNoiBat.ContentNews%></textarea>
                
            </div>
        </div>
        <%--<div class="form-group row">
            <label for="AnhDaiDien" class="col-sm-2 control-label">Ảnh mới</label>
            <div class="col-sm-10">
                <div id="ImageNews" >
                    <div class="include-library">
                        <%=oTinNoiBat.ImageNews%>
                    </div>
                </div>
                <button id="btnImageNews" type="button" class="btn  btn-success">Chọn ảnh</button>
                <button id="btnXoaImageNews" type="button" class="btn btn-danger">Xóa ảnh</button>
            </div>
        </div>--%>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            CKEDITOR.replace("ContentNews", {

            });
            $("#btnImageNews").click(function () {
                $elementId = $(this);
                CKFinder.popup({
                    chooseFiles: true,
                    width: 800,
                    height: 600,
                    onInit: function (finder) {
                        finder.on('files:choose', function (evt) {
                            var file = evt.data.files.first();
                            //var output = document.getElementById(elementId);
                            //console.log(file.getUrl());
                            //$("#ImageNews").value(file.getUrl());
                            var urlfileimg = file.getUrl();
                            document.getElementById("ImageNews").value = urlfileimg;
                            document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                        });
                        finder.on('file:choose:resizedImage', function (evt) {
                            //var output = document.getElementById(elementId);
                            console.log(evt.data);
                            var urlfileimg = evt.data.resizedUrl;
                            document.getElementById("ImageNews").value = urlfileimg;
                            document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                        });
                    }
                });
            });
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                timePicker: true,
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY HH:mm' }
            });
            $(".form-group select").select2({
                dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
            });

            $("#frm-TinNoiBat").validate({
                rules: {
                    Title: "required",
                    DMSTT: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề",
                    DMSTT: "Vui lòng nhập số"
                },
               
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/TinNoiBat/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-TinNoiBat").trigger("click");
                            $(form).closeModal();
                            $('#tblTinNoiBat').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-TinNoiBat").viForm();
        });
    </script>
</body>
</html>
