﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormAddTinNoiBatFormTin.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.TinNoiBat.pFormAddTinNoiBatFormTin" %>

<style type="text/css">
    .modal-footer {
        display:none;
    }
</style>
<div role="body-data" data-title="" id="GridTinTuc" class="content_wp" data-size="1040" data-action="/UserControls/AdminCMS/TinTuc/pAction.asp" data-form="/UserControls/AdminCMS/TinTuc/pFormTinTuc.aspx" data-view="/UserControls/AdminCMS/TinTuc/pViewTinTuc.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="TinTucSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="DanhMuc">Danh mục</label>
                            <select name="UrlListNews" id="UrlListNews" class="form-control">
                                <%foreach(var item in lstDanhMuc){%>
                                <option value="<%=item.UrlList %>"><%=item.Title %></option>
                                <%} %>
                            </select>
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Titles" />
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
            </div>

            <div class="clsgrid table-responsive">

                <table id="tblTinTuc" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var $tagvalue = $("#GridTinTuc.content_wp");
        var $urlnews = $("#UrlListNews").val();
        var dataUrl = { UrlListNews: $urlnews };
        $tagvalue.data("parameters", dataUrl);
        var $tblDanhMucChung = $("#tblTinTuc").viDataTable(
            {
                "order": [2,"desc"],
                "frmSearch": "TinTucSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "mData": function (o) {
                                var htmlhot = '<i class="fas fa-fire clsred"></i>';
                                if (!o.isHotNew)
                                    htmlhot = '';
                                return '<a data-id="' + o.ID + '" class="act-view-news" href="javascript:;">' + o.Titles + '</a>' + htmlhot;
                            },
                            "name": "Titles", "sTitle": "Tiêu đề"
                        },                        
                        {
                            "mData": function (o) {
                                if (o.ImageNews != null) {
                                    return ' <img  data-id="' + o.ID + '" class="act-view"style="width:100px;height:60px"  src="' + o.ImageNews + '" alt="" />';
                                } else {
                                    return '';
                                }

                            },
                            "name": "ImageNews", "sTitle": "Ảnh"
                        },
                        {
                            "mData": function (o) {
                                return formatDate(o.Created);
                            },
                            "sWidth": "90px",
                            "name": "Created", "sTitle": "Ngày tạo"
                        }, {
                            "name": "WFTrangThaiText", "sTitle": "Trạng thái",
                            "mData": "WFTrangThaiText",
                            "sWidth": "80px",
                        },{
                            "mData": null,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {
                                if (o.isHotNew) {
                                    return '<a title="Hủy tin nổi bật" data-do="ADDHOTNEW" data-isHotNew="false" data-ItemID="' + o.ID + '" class="btn default btn-xs black btnHotNew" href="javascript:;"><i class="fas fa-fire clsblue"></i></a>';
                                }
                                else {
                                    return '<a title="Thêm tin nổi bật" data-do="ADDHOTNEW" data-isHotNew="true" data-ItemID="' + o.ID + '" class="btn default btn-xs blue btnHotNew" href="javascript:;"><i class="fas fa-fire"></i></a>';
                                }
                            }
                        }
                    ],
                
            });
        $tblDanhMucChung.on('draw', function () {
            $(".btnHotNew").click(function () {
                var dataPost = $(this).data();
                dataPost["UrlListNews"] = $("#UrlListNews").val();
                $.post("/UserControls/AdminCMS/TinTuc/pAction.asp", dataPost).done(function (result) {
                    if (result.State == 2) {
                        BootstrapDialog.show({
                            title: "Lỗi",
                            message: result.Message
                        });
                    }
                    else {
                        showMsg(result.Message);
                        $('#tblTinTuc').DataTable().ajax.reload();
                        $('#tblTinNoiBat').DataTable().ajax.reload();
                    }
                });
            });
        });
        console.log($tagvalue.data("parameters"));
        $("#UrlListNews").change(function () {
            var $urlnews = $(this).val();
            var dataUrl = { UrlListNews: $urlnews };
            $tagvalue.data("parameters", dataUrl);
        });
    });
</script>
