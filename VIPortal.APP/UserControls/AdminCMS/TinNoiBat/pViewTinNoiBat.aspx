﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewTinNoiBat.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.TinNoiBat.pViewTinNoiBat" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề tin tức</label>
            <div class="col-sm-10">
              <%=oTinNoiBat.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="DMSTT" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-4">
              <%:(oTinNoiBat!= null &&  oTinNoiBat.DMSTT>0)?oTinNoiBat.DMSTT+"": "" %>
            </div>
              <label for="CreatedDate" class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%:string.Format("{0:dd-MM-yyyy}",oTinNoiBat.CreatedDate)%>
            </div>
        </div>
        <div class="form-group row">
            <label for="DescriptionNews" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
               <%:oTinNoiBat.DescriptionNews%>
                
            </div>
        </div>           
         <div class="form-group row" data-role="groupImageDD">
            <label for="ImageNews" class="col-sm-2 control-label">Ảnh đại diện</label>
            <div class="col-sm-10">
                 <img src="<%=oTinNoiBat.ImageNews%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="ContentNews" class="col-sm-2 control-label">Nội dung</label>
            <div class="col-sm-10">
               <%=oTinNoiBat.ContentNews%>
                
            </div>
        </div> 
       <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oTinNoiBat.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oTinNoiBat.ListFileAttach[i].Url %>"><%=oTinNoiBat.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
		<div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%=string.Format("{0:dd-MM-yyyy}", oTinNoiBat.Created)%>
            </div>
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}",oTinNoiBat.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
               <%=oTinNoiBat.Author.LookupValue%>
            </div>
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
               <%=oTinNoiBat.Editor.LookupValue%>
            </div>    
        </div>
    </form>
</body>
</html>
