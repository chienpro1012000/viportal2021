﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.TinNoiBat;

namespace VIPortal.APP.UserControls.AdminCMS.TinNoiBat
{
    public partial class pFormTinNoiBat : pFormBase
    {
        public TinNoiBatItem oTinNoiBat { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oTinNoiBat = new TinNoiBatItem();
            if (ItemID > 0)
            {
                TinNoiBatDA oTinNoiBatDA = new TinNoiBatDA(UrlSite);
                oTinNoiBat = oTinNoiBatDA.GetByIdToObject<TinNoiBatItem>(ItemID);
            }
        }
    }
}