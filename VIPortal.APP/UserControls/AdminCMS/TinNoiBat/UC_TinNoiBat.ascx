﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_TinNoiBat.ascx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.TinNoiBat.UC_TinNoiBat" %>

<div role="body-data" data-title="tin nổi bật" class="content_wp" data-size="1300" data-action="/UserControls/AdminCMS/TinNoiBat/pAction.asp" data-form="/UserControls/AdminCMS/TinNoiBat/pFormAddTinNoiBatFormTin.aspx" data-view="/UserControls/AdminCMS/TinNoiBat/pViewTinNoiBat.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="TinNoiBatSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title" />
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <button class="btn btn-primary act-add" type="button">Thêm mới</button>
                    </p>
                </div>

            </div>

            <div class="clsgrid table-responsive">

                <table id="tblTinNoiBat" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function UpTinNoiBatTop(ItemID, $DMSTT) {
        $.post("/UserControls/AdminCMS/TinNoiBat/pAction.asp", { do: "UPTINNOIBAT", ItemID: ItemID, DMSTT: -1 }, function (result) {
            if (result.State == 2) {
                BootstrapDialog.show({
                    title: "Lỗi",
                    message: result.Message
                });
            }
            else {
                showMsg(result.Message);
                $('#tblTinNoiBat').DataTable().ajax.reload();
            }
        }).always(function () { });
    }
    function UpTinNoiBat(ItemID, $DMSTT) {
        $.post("/UserControls/AdminCMS/TinNoiBat/pAction.asp", { do: "UPTINNOIBAT", ItemID: ItemID, DMSTT: $DMSTT }, function (result) {
            if (result.State == 2) {
                BootstrapDialog.show({
                    title: "Lỗi",
                    message: result.Message
                });
            }
            else {
                showMsg(result.Message);
                $('#tblTinNoiBat').DataTable().ajax.reload();
            }
        }).always(function () { });
    }

    function DownTinNoiBat(ItemID, $DMSTT) {
        $.post("/UserControls/AdminCMS/TinNoiBat/pAction.asp", { do: "DOWNTINNOIBAT", ItemID: ItemID, DMSTT: $DMSTT }, function (result) {
            if (result.State == 2) {
                BootstrapDialog.show({
                    title: "Lỗi",
                    message: result.Message
                });
            }
            else {
                showMsg(result.Message);
                $('#tblTinNoiBat').DataTable().ajax.reload();
            }
        }).always(function () { });
    }

    $(document).ready(function () {
        
        var $tagvalue = $(".content_wp");
        var $tblDanhMucChung = $("#tblTinNoiBat").viDataTable(
            {
                "frmSearch": "TinNoiBatSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "sWidth": "30px",
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '" >' + o.DMSTT + '</a>';
                            },
                            "name": "DMSTT", "sTitle": "STT"
                        },
                        {
                            "mData": function (o) {
                                if (o.Title != null)
                                    return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                                else
                                    return "";
                            },
                            "name": "Title", "sTitle": "Tên tin tức"
                        },
                        {
                            "mData": function (o) {
                                if (o.ImageNews != null) {
                                    return ' <img  data-id="' + o.ID + '" class="act-view"style="width:100px;height:60px"  src="' + o.ImageNews + '" alt="" />';
                                } else {
                                    return '';
                                }

                            },
                            "name": "ImageNews", "sTitle": "Ảnh"
                        },
                        {
                            "name": "CreatedDate", "sTitle": "Ngày tạo",
                            "mData": function (o) {
                                return formatDate(o.CreatedDate);
                            },
                            "sWidth": "80px",
                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) {
                                return '<span class="groupbtn"><a title="Tăng" data-id="' +
                                    o.ID + '" class="btn default btn-xs black act-Up" href="javascript:UpTinNoiBat(' + o.ID + ',' + o.DMSTT + ');"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>' +
                                    '<a title="Tăng lên đầu" data-id="' +
                                    o.ID + '" class="btn default btn-xs black act-Up" href="javascript:UpTinNoiBatTop(' + o.ID + ',' + o.DMSTT + ');"><i class="fa fa-fire" aria-hidden="true"></i></a></span>' +
                                    '<a title="Giảm" data-id="' +
                                    o.ID + '" class="btn default btn-xs black act-Down" href="javascript:DownTinNoiBat(' + o.ID + ',' + o.DMSTT + ');"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>';

                            }
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {

                                if (o._ModerationStatus == 0)
                                    return '<a title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }

                        },
                        {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE" title="Xóa"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                "order": [[0, 'desc']]
            });
    });
</script>
