﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData.TinNoiBat;

namespace VIPortal.APP.UserControls.AdminCMS.TinNoiBat


{

    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {

        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            TinNoiBatDA oTinNoiBatDA = new TinNoiBatDA(UrlSite);
            TinNoiBatItem oTinNoiBatItem = new TinNoiBatItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    var oData = oTinNoiBatDA.GetListJson(new TinNoiBatQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw, oTinNoiBatDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "TinNoiBat", "QUERYDATA", 0, "Xem danh sách tin nổi bật");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataTinNoiBat = oTinNoiBatDA.GetListJson(new TinNoiBatQuery(context.Request));
                    treeViewItems = oDataTinNoiBat.Select(x => new TreeViewItem()
                    {
                        key = x.DescriptionNews,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;

                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oTinNoiBatDA.GetListJson(new TinNoiBatQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "DOWNTINNOIBAT":
                    if (oGridRequest.ItemID > 0)
                    {
                        int DMSTT = Convert.ToInt32(context.Request["DMSTT"]);
                        TinNoiBatJson lstTinNoiBat = oTinNoiBatDA.GetListJson(new TinNoiBatQuery()
                        {
                            MaxSTT = DMSTT,
                            FieldOrder = "DMSTT",
                            Ascending = false,
                            Length = 1
                        }).FirstOrDefault();
                        if(lstTinNoiBat != null)
                        {
                            int NewDMSTT = lstTinNoiBat.DMSTT;
                            oTinNoiBatDA.SystemUpdateOneField(oGridRequest.ItemID, "DMSTT", NewDMSTT);
                            //update thằng sau đó lên trên.
                            oTinNoiBatDA.SystemUpdateOneField(lstTinNoiBat.ID, "DMSTT", DMSTT);
                            oResult = new ResultAction()
                            {
                                State = ActionState.Succeed,
                                Message = "Cập nhật thành công"
                            };
                        }
                        else
                        {
                            oResult = new ResultAction()
                            {
                                State = ActionState.Error,
                                Message = "Tin đang ở cuối danh sách"
                            };
                        }

                    }
                    else
                    {
                        oResult = new ResultAction()
                        {
                            State = ActionState.Error,
                            Message = "Không xác định ItemID"
                        };
                    }
                    break;
                case "UPTINNOIBAT":
                    if (oGridRequest.ItemID > 0)
                    {
                        int DMSTT = Convert.ToInt32(context.Request["DMSTT"]);
                        if (DMSTT == -1)
                        {
                            //đẩy lên top cho xong
                            TinNoiBatJson lstTinNoiBat = oTinNoiBatDA.GetListJson(new TinNoiBatQuery()
                            {
                                FieldOrder = "DMSTT",
                                Ascending = false,
                                Length = 1
                            }).FirstOrDefault();
                            if (lstTinNoiBat != null)
                            {
                                int NewDMSTT = lstTinNoiBat.DMSTT + 1;
                                //oTinNoiBatDA.SystemUpdateOneField(oGridRequest.ItemID, "DMSTT", NewDMSTT);
                                oTinNoiBatDA.SystemUpdateOneField(oGridRequest.ItemID, "DMSTT", NewDMSTT);
                                //update thằng sau đó lên trên.
                                //oTinNoiBatDA.SystemUpdateOneField(lstTinNoiBat.ID, "DMSTT", DMSTT);
                                oResult = new ResultAction()
                                {
                                    State = ActionState.Succeed,
                                    Message = "Cập nhật thành công"
                                };
                            }
                            else
                            {
                                oResult = new ResultAction()
                                {
                                    State = ActionState.Error,
                                    Message = "Tin đang ở đầu danh sách"
                                };
                            }
                        }
                        else
                        {
                            TinNoiBatJson lstTinNoiBat = oTinNoiBatDA.GetListJson(new TinNoiBatQuery()
                            {
                                MinSTT = DMSTT,
                                FieldOrder = "DMSTT",
                                Ascending = true,
                                Length = 1
                            }).FirstOrDefault();
                            if (lstTinNoiBat != null)
                            {
                                int NewDMSTT = lstTinNoiBat.DMSTT;
                                //oTinNoiBatDA.SystemUpdateOneField(oGridRequest.ItemID, "DMSTT", NewDMSTT);
                                oTinNoiBatDA.SystemUpdateOneField(oGridRequest.ItemID, "DMSTT", NewDMSTT);
                                //update thằng sau đó lên trên.
                                oTinNoiBatDA.SystemUpdateOneField(lstTinNoiBat.ID, "DMSTT", DMSTT);
                                oResult = new ResultAction()
                                {
                                    State = ActionState.Succeed,
                                    Message = "Cập nhật thành công"
                                };
                            }
                            else
                            {
                                oResult = new ResultAction()
                                {
                                    State = ActionState.Error,
                                    Message = "Tin đang ở đầu danh sách"
                                };
                            }
                        }
                    }
                    else
                    {
                        oResult = new ResultAction()
                        {
                            State = ActionState.Error,
                            Message = "Không xác định ItemID"
                        };
                    }
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {

                        oTinNoiBatItem = oTinNoiBatDA.GetByIdToObject<TinNoiBatItem>(oGridRequest.ItemID);
                        oTinNoiBatItem.UpdateObject(context.Request);
                        oTinNoiBatDA.UpdateObject<TinNoiBatItem>(oTinNoiBatItem);
                        AddLog("UPDATE", "TinNoiBat", "UPDATE", oGridRequest.ItemID, $"Cập nhật tin nổi bật{oTinNoiBatItem.Title}");
                    }
                    else
                    {
                        oTinNoiBatItem.UpdateObject(context.Request);
                        oTinNoiBatItem.CreatedDate = DateTime.Now;
                        oTinNoiBatDA.UpdateObject<TinNoiBatItem>(oTinNoiBatItem);
                        AddLog("UPDATE", "TinNoiBat", "ADD", oGridRequest.ItemID, $"Thêm mới tin nổi bật{oTinNoiBatItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oTinNoiBatDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "TinNoiBat", "DELETE", oGridRequest.ItemID, $"Xóa tin nổi bật{outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oTinNoiBatDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    AddLog("DELETE-MULTI", "TinNoiBat", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều tin nổi bật");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oTinNoiBatDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "TinNoiBat", "APPROVED", oGridRequest.ItemID, $"Duyệt tin nổi bật{oTinNoiBatDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oTinNoiBatDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "TinNoiBat", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt tin nổi bật{oTinNoiBatDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oTinNoiBatItem = oTinNoiBatDA.GetByIdToObjectSelectFields<TinNoiBatItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oTinNoiBatItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}