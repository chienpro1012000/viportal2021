﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using VIPortalAPP;

namespace VIPortal.APP.UserControls.AdminCMS.TinNoiBat
{
    public partial class pFormAddTinNoiBatFormTin : pFormBase
    {
        public List<DanhMucThongTinJson> lstDanhMuc { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {

            DanhMucThongTinDA oDanhMucDA = new DanhMucThongTinDA(UrlSite);
            lstDanhMuc = oDanhMucDA.GetListJson(new DanhMucThongTinQuery() { });


        }
    }
}