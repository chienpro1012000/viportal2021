﻿<%@ page language="C#" autoeventwireup="true" codebehind="pFormFAQThuongGap.aspx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.FAQThuongGap.pFormFAQThuongGap" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-FAQThuongGap" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oFAQThuongGap.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" placeholder="Nhập tiêu đề câu hỏi" class="form-control" value="<%=oFAQThuongGap.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="FAQCauHoi" class="col-sm-2 control-label">Câu hỏi</label>
            <div class="col-sm-10">
                <textarea name="FAQCauHoi" id="FAQCauHoi" placeholder="Nhập nội dung câu hỏi" class="form-control"><%:oFAQThuongGap.FAQCauHoi%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="FAQNoiDungTraLoi" class="col-sm-2 control-label">Nội dung trả lời</label>
            <div class="col-sm-10">
                <textarea name="FAQNoiDungTraLoi" id="FAQNoiDungTraLoi" placeholder="Nhập nội dung câu trả lời" class="form-control"><%:oFAQThuongGap.FAQNoiDungTraLoi%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="FQNguoiHoi" class="col-sm-2 control-label">Người hỏi</label>
            <div class="col-sm-4">
                <select data-selected="<%:oFAQThuongGap.FQNguoiHoi.LookupId%>" data-url="/UserControls/AdminCMS/LUser/pAction.asp?do=QUERYDATA" data-place="Chọn người hỏi" name="FQNguoiHoi" id="FQNguoiHoi" class="form-control"></select>
            </div>
            <label for="FQNguoiTraLoi" class="col-sm-2 control-label">Người trả lời</label>
            <div class="col-sm-4">
                <select data-selected="<%:oFAQThuongGap.FQNguoiTraLoi.LookupId%>" data-url="/UserControls/AdminCMS/LUser/pAction.asp?do=QUERYDATA" data-place="Chọn người trả lời" name="FQNguoiTraLoi" id="FQNguoiTraLoi" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <label for="FAQNgayHoi" class="col-sm-2 control-label">Ngày hỏi</label>
            <div class="col-sm-4">
                <div class="">
                    <input type="date" name="FAQNgayHoi" id="FAQNgayHoi" value="<%:string.Format("{0:yyyy-MM-dd}", oFAQThuongGap.FAQNgayHoi)%>" class="form-control" />
                </div>
            </div>
            <label for="FAQNgayTraLoi" class="col-sm-2 control-label">Ngày trả lời</label>
            <div class="col-sm-4">
                <div class="">
                    <input type="date" name="FAQNgayTraLoi" id="FAQNgayTraLoi" value="<%:string.Format("{0:yyyy-MM-dd}", oFAQThuongGap.FAQNgayTraLoi)%>" class="form-control" />
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="FAQSTT" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-4">
                <div class="">
                    <input type="text" name="FAQSTT" id="FAQSTT" placeholder="Nhập số thứ tự" value="<%:(oFAQThuongGap!= null &&  oFAQThuongGap.FAQSTT>0)?oFAQThuongGap.FAQSTT+"": "" %>" class="form-control" />
                </div>
            </div>
            <label for="FAQLuotXem" class="col-sm-2 control-label">Lượt xem</label>
            <div class="col-sm-4">
                <input type="text" name="FAQLuotXem" id="FAQLuotXem" placeholder="Nhập lượt xem" value="<%=(oFAQThuongGap!= null &&  oFAQThuongGap.FAQLuotXem>0)?oFAQThuongGap.FAQLuotXem+"": "" %>" class="form-control" />
            </div>
        </div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#FQNguoiHoi").smSelect2018V2({
                dropdownParent: "#frm-FAQThuongGap",
                Ajax: true
            });
            $("#FQNguoiTraLoi").smSelect2018V2({
                dropdownParent: "#frm-FAQThuongGap",
                Ajax: true
            });

            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $("#FAQNoiDungTraLoi").viCustomFck();
            $("#FAQCauHoi").viCustomFck();
            $("#frm-FAQThuongGap").validate({
                rules: {
                    Title: "required",
                    FAQCauHoi: "required",

                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề",
                    FAQCauHoi: "Vui lòng nhập câu hỏi",
                },

                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/FAQThuongGap/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-FAQThuongGap").trigger("click");
                            $(form).closeModal();
                            $('#tblFAQThuongGap').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-FAQThuongGap").viForm();
        });
    </script>
</body>
</html>

