﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_FAQThuongGap.ascx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.FAQThuongGap.UC_FAQThuongGap" %>
<div role="body-data" data-title="FAQ" class="content_wp" data-action="/UserControls/AdminCMS/FAQThuongGap/pAction.asp" data-form="/UserControls/AdminCMS/FAQThuongGap/pFormFAQThuongGap.aspx" data-view="/UserControls/AdminCMS/FAQThuongGap/pViewFAQThuongGap.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="FAQThuongGapSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Titles" />
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <button class="btn btn-primary act-add" data-per="01030302" type="button">Thêm mới</button>
                    </p>
                </div>

            </div>

            <div class="clsgrid table-responsive">

                <table id="tblFAQThuongGap" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function formatDate(strValue) {
        if (strValue == null) return "";
        //var date = new Date(strValue);
        var d = new Date(strValue);
        var month = d.getMonth() + 1;
        var day = d.getDate();
        var output = //(('' + hour).length < 2 ? '0' : '') + hour + ':' +
            //(('' + minute).length < 2 ? '0' : '') + minute + " ngày " +
            (('' + day).length < 2 ? '0' : '') + day + '/' +
            (('' + month).length < 2 ? '0' : '') + month + '/' +
            d.getFullYear();
        return output;
    }

    $(document).ready(function () {

        var $tagvalue = $(".content_wp");
        var $tblDanhMucChung = $("#tblFAQThuongGap").viDataTable(
            {
                "frmSearch": "FAQThuongGapSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "mData": "ID",
                            "name": "ID",
                            "visible": false,
                            "sTitle": "Tiêu đề"
                        },{
                            "mData": function (o) {
                                if (o.Title != null)
                                    return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                                else
                                    return "";
                            },
                            "name": "Title", "sTitle": "Tiêu đề"
                        }, {
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '" >' + o.FQNguoiTraLoi.Title + '</a>';
                            },
                            "name": "FQNguoiTraLoi", "sTitle": "Người trả lời",
                            "sWidth": "150px",
                        },
                        {
                            "mData": function (o) {

                                return '<a data-id="' + o.ID + '" >' + formatDate(o.FAQNgayHoi) + '</a>';
                            },
                            "name": "FAQNgayHoi", "sTitle": "Ngày hỏi",
                            "sWidth": "80px",
                        },
                        {
                            "mData": function (o) {

                                return '<a data-id="' + o.ID + '" >' + formatDate(o.FAQNgayTraLoi) + '</a>';
                            },
                            "name": "FAQNgayTraLoi", "sTitle": "Ngày trả lời",
                            "sWidth": "80px",
                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {

                                if (o._ModerationStatus == 0)

                                    return '<a data-per="01030305" title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a data-per="01030305" title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }

                        },
                        //{
                        //    "mData": null,
                        //    "bSortable": false,
                        //    "sWidth": "18px",
                        //    "className": "all",
                        //    "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" title="Sửa" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        //}, 
                        {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE" title="Xóa"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                "aaSorting": [0, 'desc']
            });
    });
</script>
