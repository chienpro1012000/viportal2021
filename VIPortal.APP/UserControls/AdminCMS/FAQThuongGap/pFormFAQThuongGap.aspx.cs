﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.FAQThuongGap;

namespace VIPortal.APP.UserControls.AdminCMS.FAQThuongGap
{
    public partial class pFormFAQThuongGap : pFormBase
    {
        public FAQThuongGapItem oFAQThuongGap { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oFAQThuongGap = new FAQThuongGapItem();
            if (ItemID > 0)
            {
                FAQThuongGapDA oFAQThuongGapDA = new FAQThuongGapDA(UrlSite);
                oFAQThuongGap = oFAQThuongGapDA.GetByIdToObject<FAQThuongGapItem>(ItemID);
            }
        }
    }
}