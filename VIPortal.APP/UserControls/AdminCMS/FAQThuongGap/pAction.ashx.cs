﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData.FAQThuongGap;

namespace VIPortal.APP.UserControls.AdminCMS.FAQThuongGap
{

    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);
            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            FAQThuongGapDA oFAQThuongGapDA = new FAQThuongGapDA(UrlSite,true);
            FAQThuongGapItem oFAQThuongGapItem = new FAQThuongGapItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oFAQThuongGapDA.GetListJsonSolr(new FAQThuongGapQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oFAQThuongGapDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "FAQThuongGap", "QUERYDATA", 0, "Xem danh sách FAQ thường gặp");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataFAQThuongGap = oFAQThuongGapDA.GetListJson(new FAQThuongGapQuery(context.Request));
                    treeViewItems = oDataFAQThuongGap.Select(x => new TreeViewItem()
                    {
                        key = x.FAQCauHoi,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;  
                    break;

                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oFAQThuongGapDA.GetListJson(new FAQThuongGapQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oFAQThuongGapItem = oFAQThuongGapDA.GetByIdToObject<FAQThuongGapItem>(oGridRequest.ItemID);
                        oFAQThuongGapItem.UpdateObject(context.Request);
                        if (!string.IsNullOrEmpty(oFAQThuongGapItem.Title) && oFAQThuongGapDA.CheckExit( oFAQThuongGapItem.ID, oFAQThuongGapItem.Title) > 0)
                        {
                            oResult = new ResultAction() { State = ActionState.Error, Message = "Trùng tiêu đề câu hỏi thường gặp" };
                            break;
                        }
                        oFAQThuongGapDA.UpdateObject<FAQThuongGapItem>(oFAQThuongGapItem);
                        AddLog("UPDATE", "FAQThuongGap", "UPDATE", oGridRequest.ItemID, $"Cập nhật FAQ thường gặp{oFAQThuongGapItem.Title}");
                    }
                    else
                    {
                        oFAQThuongGapItem.UpdateObject(context.Request);
                        if (!string.IsNullOrEmpty(oFAQThuongGapItem.Title) && oFAQThuongGapDA.CheckExit(oFAQThuongGapItem.ID, oFAQThuongGapItem.Title) > 0)
                        {
                            oResult = new ResultAction() { State = ActionState.Error, Message = "Trùng tiêu đề câu hỏi thường gặp" };
                            break;
                        }
                        oFAQThuongGapDA.UpdateObject<FAQThuongGapItem>(oFAQThuongGapItem);
                        AddLog("UPDATE", "FAQThuongGap", "ADD", oGridRequest.ItemID, $"Thêm mới FAQ thường gặp{oFAQThuongGapItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oFAQThuongGapDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "FAQThuongGap", "DELETE", oGridRequest.ItemID, $"Xóa FAQ thường gặp {outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    AddLog("DELETE-MULTI", "FAQThuongGap", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều loại tài liệu");
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oFAQThuongGapDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oFAQThuongGapDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "FAQThuongGap", "APPROVED", oGridRequest.ItemID, $"Duyệt FAQ thường gặp{oFAQThuongGapDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oFAQThuongGapDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "FAQThuongGap", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt FAQ thường gặp{oFAQThuongGapDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oFAQThuongGapItem = oFAQThuongGapDA.GetByIdToObjectSelectFields<FAQThuongGapItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oFAQThuongGapItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}