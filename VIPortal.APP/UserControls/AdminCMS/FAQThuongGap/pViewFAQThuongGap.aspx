﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewFAQThuongGap.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.FAQThuongGap.pViewFAQThuongGap" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <%=oFAQThuongGap.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="FAQCauHoi" class="col-sm-2 control-label">Câu hỏi</label>
            <div class="col-sm-10">
               <%=oFAQThuongGap.FAQCauHoi%>
            </div>
        </div>
        <div class="form-group row">
            <label for="FAQNoiDungTraLoi" class="col-sm-2 control-label">Nội dung trả lời</label>
            <div class="col-sm-10">
                <%=oFAQThuongGap.FAQNoiDungTraLoi%>
            </div>
        </div>
          <div class="form-group row">
            <label for="FQNguoiHoi" class="col-sm-2 control-label">Người hỏi</label>
            <div class="col-sm-4">
                <%:oFAQThuongGap.FQNguoiHoi.LookupId%>
            </div>
              <label for="FQNguoiTraLoi" class="col-sm-2 control-label">Người trả lời</label>
            <div class="col-sm-4">
                <%:oFAQThuongGap.FQNguoiTraLoi.LookupId%>
            </div>
        </div>
            <div class="form-group row">
                <label for="FAQNgayHoi" class="col-sm-2 control-label">Ngày hỏi</label>
                <div class="col-sm-4">
                    <%:string.Format("{0:dd-MM-yyyy}", oFAQThuongGap.FAQNgayHoi)%>                
                </div>
                <label for="FAQNgayTraLoi" class="col-sm-2 control-label">Ngày trả lời</label>
                <div class="col-sm-4">
                   <%:string.Format("{0:dd-MM-yyyy}", oFAQThuongGap.FAQNgayTraLoi)%>                  
                </div>

            </div>
        <div class="form-group row">
             <label for="FAQSTT" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-4">
                <div class="">
                     <%:(oFAQThuongGap!= null &&  oFAQThuongGap.FAQSTT>0)?oFAQThuongGap.FAQSTT+"": "" %>
                </div>
            </div>
            <label for="FAQLuotXem" class="col-sm-2 control-label">Lượt xem</label>
            <div class="col-sm-4">
                <%=(oFAQThuongGap!= null &&  oFAQThuongGap.FAQLuotXem>0)?oFAQThuongGap.FAQLuotXem+"": "" %>      
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oFAQThuongGap.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oFAQThuongGap.ListFileAttach[i].Url %>"><%=oFAQThuongGap.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
		  <div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%=string.Format("{0:dd-MM-yyyy}", oFAQThuongGap.Created)%>
            </div>
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}",oFAQThuongGap.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
                <%=oFAQThuongGap.Author.LookupValue%>
            </div>
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
                <%=oFAQThuongGap.Editor.LookupValue%>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>