﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.FAQThuongGap;

namespace VIPortal.APP.UserControls.AdminCMS.FAQThuongGap
{
    public partial class pViewFAQThuongGap : pFormBase
    {
        public FAQThuongGapItem oFAQThuongGap = new FAQThuongGapItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oFAQThuongGap = new FAQThuongGapItem();
            if (ItemID > 0)
            {
                FAQThuongGapDA oFQAThuongGapDA = new FAQThuongGapDA(UrlSite);
                oFAQThuongGap = oFQAThuongGapDA.GetByIdToObject<FAQThuongGapItem>(ItemID);
            }
        }
    }
}