﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;

namespace VIPortal.APP.UserControls.AdminCMS.MenuAdmin
{
    public partial class CMS_Header : BaseUC
    {
        public string TitlePage = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            TitlePage = SPContext.Current.Item["Title"].ToString();

        }
    }
}