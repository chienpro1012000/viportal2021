﻿<%@ control language="C#" autoeventwireup="true" codebehind="CMS_Header.ascx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.MenuAdmin.CMS_Header" %>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4 class="m-0">  <i class="fas fa-tag"></i>&nbsp;&nbsp;<%=TitlePage%>
                </h4>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item">
                        <a href="#">Home
                        </a>
                    </li>
                    <li class="breadcrumb-item active">Dashboard v2
                    </li>
                </ol>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
