﻿<%@ control language="C#" autoeventwireup="true" codebehind="AdminMenuLeft.ascx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.MenuAdmin.AdminMenuLeft" %>
<style>
    .elevation-3 {
        box-shadow: none !important;
    }

    .brand-link {
        background-color: #fff !important;
        justify-content: center;
        border-bottom: 1px solid #f5f5f5 !important;
        padding: 5px 0;
    }

        .brand-link img {
            margin-left: 2.2rem !important;
            margin-right: .5rem !important;
            max-height: 50px !important;
            width: 70% !important;
        }
</style>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="/Content/themev1/img/logo/logo_xanh_ngang.png" alt="AdminLTE Logo" class="brand-image elevation-3" style="opacity: 1" />

    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="pull-left image">
                <%if(CurentUser.AnhDaiDien != null) {%>
                <img src="<%=CurentUser.AnhDaiDien %>" alt="" style="display: block; width: 50px; height: 50px; border-radius: 50%">
                <%}else {%>
                <img src="/Content/themeV1/img/avatar/avatar_nam.jpg" alt="" style="display: block; width: 50px; height: 50px; border-radius: 50%">
                <%} %>
            </div>
            <div class=" pull-left info">
                <p>
                    <%=CurentUser.Title %>
                </p>
                <div class="dropdown">
                    <a href="#" class="dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-circle text-success w3-button"></i>Online</a>
                    <ul class="dropdown-menu">
                        <li><a id="btnlogout" href="/_layouts/closeConnection.aspx?loginasanotheruser=true">Đăng xuất</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- SidebarSearch Form -->
        <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search" />
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false" id="cmsmenuleft">
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>

<script type="text/javascript">
    // A $( document ).ready() block.
    $(document).ready(function () {
        console.log("AdminMenuLeft!");
        $.getJSON("/VIPortalAPI/api/MenuQuanTri/GETMENULEFTCMS", { "MenuPublic": 2 }, function (data) {
            $("#cmsmenuleft").html(data);
            //get first item all
            active_menu();

        });
    });
    function active_menu() {
        var url = window.location.href;
        var linkmenu = $('#cmsmenuleft .nav-item a').filter(function () {
            return this.href == url;
        });
        if (linkmenu.length > 0) {
            linkmenu.addClass('selected');
            linkmenu.closest(".nav-treeview").css("display", "block");
            var $parentDiv = $('.os-viewport-native-scrollbars-invisible');
            var $top = $parentDiv.scrollTop() + linkmenu.position().top;
            console.log($top);
            //$parentDiv.scrollTop($top);\
            $parentDiv.animate({ // animate your right div
                scrollTop: $top // to the position of the target
            }, 400);
        } else {
            if (url.indexOf("quanlyfile.aspx") == -1) {
                var defaultlink = $('#cmsmenuleft .nav-item a:first-child').attr('href');
                if (defaultlink != undefined) {
                    window.location.href = defaultlink;
                } else {
                    var newurl = url;
                    if (url.indexOf("/cms") > -1)
                        newurl = url.substring(0, url.indexOf("/cms"));
                    else if (url.indexOf("/noidung") > -1) {
                        newurl = url.substring(0, url.indexOf("/noidung"));
                    }
                    window.location.href = newurl;
                }
            }
        }
    }
</script>
