﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using VIPortalAPP;

namespace VIPortal.APP.UserControls.AdminCMS.MenuAdmin
{
    public partial class AdminHeader : BaseUC
    {
        public List<LNotificationJson> lstLNotification { get; set; }
        public int TotalNotifi { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            
            LNotificationDA oLNotificationDA = new LNotificationDA();
            lstLNotification = oLNotificationDA.GetListJson(new LNotificationQuery() { _ModerationStatus = 1, FieldOrder = "Created", Ascending = false, LNotiNguoiNhan_Id = CurentUser.ID });
            TotalNotifi = oLNotificationDA.TongSoBanGhiSauKhiQuery;
        }
    }
}