﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormLVaiTro.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.LVaiTro.pFormLVaiTro" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-LVaiTro" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oLVaiTro.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" name="fldGroup" id="fldGroup" value="<%=CurentUser.fldGroup %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề vai trò</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" placeholder="Nhập Tiêu đề vai trò" class="form-control" value="<%=oLVaiTro.Title%>" />
            </div>
        </div>
         
        <div class="form-group row">
            <label for="Permission" class="col-sm-2 control-label">Quyền</label>
            <div class="col-sm-10">
                <input type="hidden" name="Permission" id="Permission" value="" />
                <div id="treephanquyen" data-source="ajax" class="sampletree" style="max-height: 400px; overflow-y: auto;">
                        </div>
            </div>
        </div>
       
       <div class="form-group row">
	<label for="DMMoTa" class="col-sm-2 control-label">Mô tả</label>
	<div class="col-sm-10">
		<textarea name="DMMoTa" id="DMMoTa" placeholder="Nhập mô tả" class="form-control"><%=oLVaiTro.DMMoTa%></textarea>
	</div>
</div>
        
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            var $treePermis =  $("#treephanquyen").fancytree({
                source: {
                    url: "/VIPortalAPI/api/LPermissions/QUERYTREE?ListPermissCheck=<%=string.Join(",", oLVaiTro.Permission.Select(x => x.LookupId)) %>"
                },
                checkbox: true,
                selectMode: 3,
                icon: false,
                beforeSelect: function (event, data) {
                },
                select: function (event, data) {
                    var tree = $('#treephanquyen').fancytree('getTree');
                    var selKeys = $.map(tree.getSelectedNodes(), function (element, index) { return element.key }).join(",");
                    $("#Permission").val(selKeys);
                },
            });
            $(".fancytree-container").addClass("fancytree-connectors");
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oLVaiTro.ListFileAttach)%>'
            });
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });

            $("#frm-LVaiTro").validate({
                ignore: [],
                rules: {
                    Title: "required",
                    Permission:"required"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề vai trò",
                    Permission:"Vui lòng chọn quyền"
                },
                submitHandler: function (form) {
                    var tree = $('#treephanquyen').fancytree('getTree');
                    var selKeys = $.map(tree.getSelectedNodes(), function (element, index) { return element.key }).join(",");
                    $("#Permission").val(selKeys);
                    $.post("/UserControls/AdminCMS/LVaiTro/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-LVaiTro").trigger("click");
                            $(form).closeModal();
                            $('#tblLVaiTro').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-LVaiTro").viForm();
        });
    </script>
</body>
</html>

