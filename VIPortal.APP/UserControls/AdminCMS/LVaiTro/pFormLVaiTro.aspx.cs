﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData.LVaiTro;

namespace VIPortal.APP.UserControls.AdminCMS.LVaiTro
{
    public partial class pFormLVaiTro : pFormBase
    {
        public LVaiTroItem oLVaiTro { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            
            oLVaiTro = new LVaiTroItem();
            if (ItemID > 0)
            {
                LVaiTroDA oLVaiTroDA = new LVaiTroDA();
                oLVaiTro = oLVaiTroDA.GetByIdToObject<LVaiTroItem>(ItemID);
            }
        }
    }
}