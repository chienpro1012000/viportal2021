﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData.LVaiTro;

namespace VIPortal.APP.UserControls.AdminCMS.LVaiTro
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);


            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            LVaiTroDA oLVaiTroDA = new LVaiTroDA();
            LVaiTroItem oLVaiTroItem = new LVaiTroItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oLVaiTroDA.GetListJson(new LVaiTroQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oLVaiTroDA.TongSoBanGhiSauKhiQuery);
                    AddLog("QUERYDATA", "LVaiTro", "QUERYDATA", 0, "Xem danh sách vai trò");
                    oResult.OData = oGrid;
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataLVaiTro = oLVaiTroDA.GetListJson(new LVaiTroQuery(context.Request));
                    treeViewItems = oDataLVaiTro.Select(x => new TreeViewItem()
                    {
                        key = x.DMMoTa,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oLVaiTroDA.GetListJson(new LVaiTroQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLVaiTroItem = oLVaiTroDA.GetByIdToObject<LVaiTroItem>(oGridRequest.ItemID);
                        oLVaiTroItem.UpdateObject(context.Request);
                        oLVaiTroDA.UpdateObject<LVaiTroItem>(oLVaiTroItem);
                        AddLog("UPDATE", "LVaiTro", "UPDATE", oGridRequest.ItemID, $"Cập nhật vai trò{oLVaiTroItem.Title}");
                    }
                    else
                    {
                        oLVaiTroItem.UpdateObject(context.Request);
                        oLVaiTroDA.UpdateObject<LVaiTroItem>(oLVaiTroItem);
                        AddLog("UPDATE", "LVaiTro", "ADD", oGridRequest.ItemID, $"Thêm mới vai trò{oLVaiTroItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oLVaiTroDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "LVaiTro", "DELETE", oGridRequest.ItemID, $"Xóa vai trò{outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    AddLog("DELETE-MULTI", "LVaiTro", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều vai trò");
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oLVaiTroDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oLVaiTroDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "LVaiTro", "APPROVED", oGridRequest.ItemID, $"Duyệt vai trò{oLVaiTroDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oLVaiTroDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "LVaiTro", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt vai trò{oLVaiTroDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLVaiTroItem = oLVaiTroDA.GetByIdToObjectSelectFields<LVaiTroItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oLVaiTroItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}