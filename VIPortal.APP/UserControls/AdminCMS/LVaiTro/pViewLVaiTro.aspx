﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewLVaiTro.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.LVaiTro.pViewLVaiTro" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Title</label>
            <div class="col-sm-10">
                <%=oLVaiTro.Title%>
            </div>
        </div>
         <div class="form-group row">
            <label for="fldGroup" class="col-sm-2 control-label">Group</label>
            <div class="col-sm-10">
               <%:oLVaiTro.fldGroup.LookupId%>
            </div>
        </div>
        <div class="form-group row">
            <label for="Permission" class="col-sm-2 control-label">Quyền</label>
            <div class="col-sm-10">
               <%:string.Join(",", oLVaiTro.Permission.Select(x => x.LookupId)) %>
            </div>
        </div>
       
       <div class="form-group row">
	        <label for="DMMoTa" class="col-sm-2 control-label">Mô tả</label>
	        <div class="col-sm-10">
		        <%=oLVaiTro.DMMoTa%>
	    </div>
        </div>
       <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oLVaiTro.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oLVaiTro.ListFileAttach[i].Url %>"><%=oLVaiTro.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>      
		<div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%=string.Format("{0:dd-MM-yyyy}", oLVaiTro.Created)%>
            </div>
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}",oLVaiTro.Modified)%>
            </div>
        </div>

        <div class="form-group row">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
               <%=oLVaiTro.Author.LookupValue%>
            </div>
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
               <%=oLVaiTro.Editor.LookupValue%>
            </div>    
        </div>
    </form>
</body>
</html>
