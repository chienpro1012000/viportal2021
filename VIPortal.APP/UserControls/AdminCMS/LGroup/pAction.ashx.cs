using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortalAPP.LGroup
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            LGroupDA oLGroupDA = new LGroupDA();
            LGroupItem oLGroupItem = new LGroupItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    LGroupQuery oLGroupQuery = new LGroupQuery(context.Request);

                    var oData = oLGroupDA.GetListJson(new LGroupQuery(context.Request));
                    if (oLGroupQuery.isGetParent)
                    {
                        oLGroupItem = oLGroupDA.GetByIdToObject<LGroupItem>(oLGroupQuery.GroupParent);
                        oData.Insert(0, new LGroupJson()
                        {
                            ID = oLGroupItem.ID,
                            Title = oLGroupItem.Title
                        });
                    }
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oLGroupDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "LGroup", "QUERYDATA", 0, "Xem danh sách LGroup");
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oLGroupDA.GetListJson(new LGroupQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLGroupItem = oLGroupDA.GetByIdToObject<LGroupItem>(oGridRequest.ItemID);
                        oLGroupItem.UpdateObject(context.Request);
                        oLGroupDA.UpdateObject<LGroupItem>(oLGroupItem);
                        AddLog("UPDATE", "LGroup", "UPDATE", oGridRequest.ItemID, $"Cập nhật LGroup{oLGroupItem.Title}");
                    }
                    else
                    {
                        oLGroupItem.UpdateObject(context.Request);
                        oLGroupDA.UpdateObject<LGroupItem>(oLGroupItem);
                        AddLog("UPDATE", "LGroup", "ADD", oGridRequest.ItemID, $"Thêm mới LGroup {oLGroupItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oLGroupDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "LGroup", "DELETE", oGridRequest.ItemID, $"Xóa LGroup {outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    AddLog("DELETE-MULTI", "LGroup", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều LGroup");
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oLGroupDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oLGroupDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "LGroup", "APPROVED", oGridRequest.ItemID, $"Duyệt LGroup {oLGroupDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oLGroupDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "LGroup", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt LGroup {oLGroupDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLGroupItem = oLGroupDA.GetByIdToObjectSelectFields<LGroupItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oLGroupItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}