<%@ control language="C#" autoeventwireup="true" codebehind="UC_LGroup.aspx.cs" inherits="VIPortalAPP.UC_LGroup" %>
<style>
    .fancytree-title {
        white-space: normal !important;
        display: unset !important;
    }
</style>
<div role="body-data" data-title="" class="content_wp" data-action="/UserControls/AdminCMS/LGroup/pAction.asp" data-form="/UserControls/AdminCMS/LGroup/pFormLGroup.aspx" data-view="/UserControls/AdminCMS/LGroup/pViewLGroup.aspx">
    <div class="clsmanager row">
    </div>
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsgrid table-responsive">
                <div class="container-fluid">
                    <!-- Control the column width, and how they should appear on different devices -->
                    <div class="row">
                        <div class="col-sm-3">
                            <h5>Cơ cấu tổ chức</h5>
                            <div id="treeLgroup"></div>
                        </div>

                        <div class="col-sm-9">
                            <div role="body-data" data-title="Người dùng" class="content_wp" data-view="/UserControls/AdminCMS/LUser/pViewLUser.aspx" data-action="/UserControls/AdminCMS/LUser/pAction.asp" data-form="/UserControls/AdminCMS/LUser/pFormLUser.aspx">
                                <div class="clsmanager row">
                                    <div class="col-sm-9">
                                        <div id="LUserSearch" class="form-inline zonesearch">
                                            <div class="form-group">
                                                <input type="hidden" name="do" id="do" value="QUERYDATA" />
                                                <input type="hidden" name="UserPhongBan" id="UserPhongBan" value="0" />
                                                <input type="hidden" name="GetUserPhongBan" id="GetUserPhongBan" value="true" />
                                                <label for="Keyword">Từ khóa</label>
                                                <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                                                <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title,TaiKhoanTruyCap" />
                                            </div>
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input" name="SearchAll" id="SearchAll">
                                                <label class="form-check-label" for="SearchAll">Toàn bộ</label>
                                            </div>
                                            <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <p class="text-right">
                                            <button class="btn btn-primary" id="btnAddUser" data-per="080102" type="button">Thêm mới</button>
                                        </p>
                                    </div>
                                </div>

                                <div class="clsgrid table-responsive">

                                    <table id="tblLUser" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        $("#btnAddUser").click(function () {
            var $UserPhongBan = $("#btnAddUser").data('UserPhongBan');
            if ($UserPhongBan != undefined && $UserPhongBan != 0) {

                var $tagData = getRoleData($(this));
                var datapost = $tagData.data("parameters");
                if (datapost == undefined || datapost == 'undefined') {
                    datapost = {}
                }

                var datapostSub = $(this).data();
                if (datapostSub == undefined || datapostSub == 'undefined') {
                    datapostSub = {}
                }

                $.extend(datapost, datapostSub);
                console.log(datapost);
                openDialog("Thêm mới " + $tagData.data("title"), $tagData.data("form"), datapost, $tagData.data("size"));
                event.stopPropagation();
            } else {
                BootstrapDialog.show({
                    title: "Thông báo",
                    message: "Chưa chọn phòng ban"
                });
            }
        });

        $('#treeLgroup').fancytree({
            //  extensions: ['contextMenu'],
            source: {
                url: "/VIPortalAPI/api/Lgroup/QUERYTREE?DefaultGetLgroup=1"
            },
            renderNode: function (event, data) {
                //console.log(data);
                var node = data.node;
                $(node.span).attr('data-id', data.node.key);

            },
            activate: function (event, data) {
                $('#UserPhongBan').val(data.node.key);
                //$("#btnAddUser").attr("data-UserPhongBan", data.node.key);
                $("#btnAddUser").data('UserPhongBan', data.node.key);
                $('#tblLUser').DataTable().ajax.reload();
            },

        });

        $.contextMenu({
            selector: '#treeLgroup .fancytree-node',
            callback: function (key, options) {
                //console.log($(this));
                switch (key) {
                    case "add":
                        openDialog("Thêm mới Đơn vị", "/UserControls/AdminCMS/LGroup/pFormLGroup.aspx", { GroupParent: $(this).attr("data-id"), "GroupIsDonVi": true }, 1024);
                        break;
                    case "addpb":
                        openDialog("Thêm mới phòng", "/UserControls/AdminCMS/LGroup/pFormLGroup.aspx", { GroupParent: $(this).attr("data-id"), "GroupIsDonVi": false }, 1024);
                        break;
                    case "edit":
                        openDialog("Sửa Đơn vị/Phòng ban", "/UserControls/AdminCMS/LGroup/pFormLGroup.aspx", { ItemID: $(this).attr("data-id"), do: "UPDATE" }, 1024);
                        break;
                    case "delete":
                        DeleteFromDialogFunc("/UserControls/AdminCMS/LGroup/pAction.asp", { ItemID: $(this).attr("data-id"), do: "DELETE" }, "quyền", function () {
                            var tree = $("#treeLgroup").fancytree("getTree");
                            tree.reload().done(function () {
                            });
                        });
                        break;
                }

            },
            events: {
            },
            items: {
                "add": {
                    name: "Tạo Đơn vị", icon: function (opt, $itemElement, itemKey, item) {
                        // Set the content to the menu trigger selector and add an bootstrap icon to the item.
                        $itemElement.html('<i class="fa fa-plus" aria-hidden="true"></i> ' + 'Thêm mới Đơn vị');
                        // Add the context-menu-icon-updated class to the item
                        //return 'context-menu-icon-add';
                    }
                },
                "addpb": {
                    name: "Tạo Phòng ban", icon: function (opt, $itemElement, itemKey, item) {
                        // Set the content to the menu trigger selector and add an bootstrap icon to the item.
                        $itemElement.html('<i class="fa fa-plus" aria-hidden="true"></i> ' + 'Thêm mới phòng ban');
                        // Add the context-menu-icon-updated class to the item
                        //return 'context-menu-icon-add';
                    }
                },
                "edit": {
                    name: "Sửa Đơn vị/Phòng ban", icon: function (opt, $itemElement, itemKey, item) {
                        $itemElement.html('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> ' + 'Sửa Đơn vị/Phòng ban');
                        //return 'context-menu-icon-edit';
                    }
                },
                "delete": {
                    name: "Xóa Đơn vị/Phòng ban", icon: function (opt, $itemElement, itemKey, item) {
                        $itemElement.html('<i class="fa fa-trash-o" aria-hidden="true"></i> ' + 'Xóa Đơn vị/Phòng ban');
                        //return 'context-menu-icon-delete';
                    }
                },

            },
        });
        $(".fancytree-container").addClass("fancytree-connectors");


        var $tblDanhMucChung = $("#tblLUser").viDataTable(
            {
                "frmSearch": "LUserSearch",
                "url": "/UserControls/AdminCMS/LUser/pAction.asp",
                parameterPlus: function (odata) {
                    console.log(odata);
                    if ($('#SearchAll').is(":checked")) {
                        delete odata['GetUserPhongBan'];
                        delete odata['UserPhongBan'];
                    }
                },
                "aoColumns":
                    [
                        {
                            "mData": function (o) {
                                return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                            },
                            "name": "Title", "sTitle": "Họ và tên"
                        },
                        {
                            "data": "TaiKhoanTruyCap",
                            "name": "TaiKhoanTruyCap", "sTitle": "Tài khoản",
                            "sWidth": "90px",
                        }, {
                            "data": "UserChucVu",
                            "name": "UserChucVu", "sTitle": "Chức vụ",
                            "sWidth": "90px",
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {
                                if (o._ModerationStatus == 0)
                                    return '<a data-per="080105" title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a data-per="080105" title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "60px",
                            "className": "all",
                            "mRender": function (o) {
                                return '<a title="Gán quyền người dùng" url="/UserControls/AdminCMS/LUser/pFormGanQuyen.aspx" class="btn default btn-xs purple btnfrm" data-do="UPDATE" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fas fa-user-shield"></i></a>' +
                                    '<a title="Nhật ký" url="/UserControls/AdminCMS/LogHeThong/pViewLogByUser.aspx" size=1240 class="btn default btn-xs purple btnfrm" data-do="VIEWNHATKY" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fas fa-calendar-alt"></i></a>';
                            }
                        }, {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" data-do="DELETE-MULTI" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {
                                return '<a data-id="' + o.ID + '" data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-ItemID ="' + o.ID + '" data-do="DELETE" href = "javascript:;" > <i class="far fa-trash-alt"></i></a > ';
                            }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                "order": [[0, "asc"]]
            });
    });

</script>
