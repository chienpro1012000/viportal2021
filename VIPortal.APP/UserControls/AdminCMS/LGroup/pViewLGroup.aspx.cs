using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortalAPP
{
    public partial class pViewLGroup : pFormBase
    {
        public LGroupItem oLGroup = new LGroupItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oLGroup = new LGroupItem();
            if (ItemID > 0)
            {
                LGroupDA oLGroupDA = new LGroupDA();
                oLGroup = oLGroupDA.GetByIdToObject<LGroupItem>(ItemID);

            }
        }
    }
}