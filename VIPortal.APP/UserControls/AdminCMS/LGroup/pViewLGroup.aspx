<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewLGroup.aspx.cs" Inherits="VIPortalAPP.pViewLGroup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group">
            <label for="Title" class="col-sm-2 control-label">Tên</label>
            <div class="col-sm-10">
               <%=oLGroup.Title%>
            </div>
        </div>
		<div class="form-group">
	<label for="MaDinhDanh" class="col-sm-2 control-label">Mã định danh</label>
	<div class="col-sm-10">
		<%=oLGroup.MaDinhDanh%>
	</div>
</div>
<div class="form-group">
	<label for="GroupParent" class="col-sm-2 control-label">Đơn vị cha</label>
	<div class="col-sm-10">
		<%=oLGroup.GroupParent%>
	</div>
</div>
<div class="form-group">
	<label for="GroupIsDonVi" class="col-sm-2 control-label">Đơn vị</label>
	<div class="col-sm-10">
		<%=oLGroup.GroupIsDonVi? "Có" : "Không"%>
	</div>
</div>
<div class="form-group">
	<label for="GroupEmail" class="col-sm-2 control-label">Email phòng</label>
	<div class="col-sm-10">
		<%=oLGroup.GroupEmail%>
	</div>
</div>
<div class="form-group">
	<label for="UrlSite" class="col-sm-2 control-label">Url</label>
	<div class="col-sm-10">
		<%=oLGroup.UrlSite%>
	</div>
</div>
<div class="form-group">
	<label for="GroupSDT" class="col-sm-2 control-label">Số điện thoại phòng</label>
	<div class="col-sm-10">
		<%=oLGroup.GroupSDT%>
	</div>
</div>
<div class="form-group">
	<label for="GroupTreeID" class="col-sm-2 control-label">GroupTreeID</label>
	<div class="col-sm-10">
		<%=oLGroup.GroupTreeID%>
	</div>
</div>

		  <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-10">
               <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oLGroup.Created)%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oLGroup.Modified)%>
            </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-10">
                <%=oLGroup.Author.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-10">
                <%=oLGroup.Editor.LookupValue%>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>