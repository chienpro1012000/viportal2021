<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormLGroup.aspx.cs" Inherits="VIPortalAPP.pFormLGroup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
    <style>
        .error {
            color: red;
        }
    </style>
</head>
<body>
    <form id="frm-LGroup" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oLGroup.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" placeholder="Nhập tên" class="form-control" value="<%=oLGroup.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="MaDinhDanh" class="col-sm-2 control-label">Mã định danh</label>
            <div class="col-sm-10">
                <input type="text" name="MaDinhDanh" id="MaDinhDanh"placeholder="Nhập mã định danh" value="<%:oLGroup.MaDinhDanh%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="GroupParent" class="col-sm-2 control-label">Đơn vị cha</label>
            <div class="col-sm-10">
                <select data-selected="<%:oLGroup.GroupParent.LookupId%>"  data-url="/UserControls/AdminCMS/LGroup/pAction.asp?do=QUERYDATA&GroupIsDonVi=1" data-place="Chọn GroupParent" name="GroupParent" id="GroupParent" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <label for="GroupIsDonVi" class="col-sm-2 control-label">Đơn vị</label>
            <div class="col-sm-4">
                <input value="true" type="checkbox" name="GroupIsDonVi" id="GroupIsDonVi" <%=oLGroup.GroupIsDonVi? "checked" : string.Empty%> />
            </div>
            <label for="DMSTT" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-4">
                <input type="text" name="DMSTT" id="DMSTT" placeholder="Nhập STT" class="form-control" value="<%=oLGroup.DMSTT%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="GroupEmail" class="col-sm-2 control-label">Email phòng</label>
            <div class="col-sm-4">
                <input type="text" name="GroupEmail" id="GroupEmail" placeholder="Nhập email" value="<%:oLGroup.GroupEmail%>" class="form-control" />
            </div>
            <label for="GroupSDT" class="col-sm-2 control-label">Số điện thoại phòng</label>
            <div class="col-sm-4">
                <input type="text" name="GroupSDT" id="GroupSDT" placeholder="Nhập số điện thoại" value="<%:oLGroup.GroupSDT%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="UrlSite" class="col-sm-2 control-label">Đường dẫn</label>
            <div class="col-sm-10">
                <input type="text" name="UrlSite" id="UrlSite" placeholder="Nhập url" value="<%:oLGroup.UrlSite%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="GroupTreeID" class="col-sm-2 control-label">GroupTreeID</label>
            <div class="col-sm-4">
                <input type="text" name="GroupTreeID" id="GroupTreeID" placeholder="Nhập grouptreeid" value="<%:oLGroup.GroupTreeID%>" class="form-control" />
            </div>
            <label for="KieuDonVi" class="col-sm-2 control-label">Kiểu đơn vị</label>
            <div class="col-sm-4">
                <select id="KieuDonVi" name="KieuDonVi" class="form-control" >
                    <option value="0">Chọn phân loại đơn vị</option>
                    <option value="1">Công ty sản xuất</option>
                    <option value="2">Ban/đơn vị tham mưu</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="AnhDaiDien" class="col-sm-2 control-label">Logo Đơn vị</label>
            <div class="col-sm-10" data-role="groupImageDD">
                <input type="hidden" name="UrlLogo" id="UrlLogo" value="<%=oLGroup.UrlLogo%>" />
                <img title="Ảnh đại diện" id="srcImageNews" src="<%=oLGroup.UrlLogo%>" />
                <button id="btnImageNews" type="button" class="btn  btn-success">Ảnh Logo</button>
                <button id="btnXoaImageNews" type="button" class="btn btn-danger">Xóa ảnh</button>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#KieuDonVi").val('<%=oLGroup.KieuDonVi%>');
            $("#btnImageNews").click(function () {
                $elementId = $(this);
                CKFinder.popup({
                    chooseFiles: true,
                    width: 800,
                    resourceType: 'Images',
                    height: 600,
                    onInit: function (finder) {
                        finder.on('files:choose', function (evt) {
                            var file = evt.data.files.first();
                            //var output = document.getElementById(elementId);
                            //console.log(file.getUrl());
                            //$("#ImageNews").value(file.getUrl());
                            var urlfileimg = file.getUrl();
                            document.getElementById("UrlLogo").value = urlfileimg;
                            document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                        });
                        finder.on('file:choose:resizedImage', function (evt) {
                            //var output = document.getElementById(elementId);
                            console.log(evt.data);
                            var urlfileimg = evt.data.resizedUrl;
                            document.getElementById("UrlLogo").value = urlfileimg;
                            document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                        });
                    }
                });
            });
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oLGroup.ListFileAttach)%>'
            });
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $("#GroupParent").smSelect2018V2({
                dropdownParent: "#frm-LGroup"
            });
                $("#frm-LGroup").validate({
                    rules: {
                        Title: "required",
                        MaDinhDanh: "required",
                        GroupParent: "required",
                    },
                    messages: {
                        Title: "Nhập tên đơn vị",
                        MaDinhDanh: "Nhập mã định danh",
                        GroupParent: "Nhập đơn vị cha"
                    },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/LGroup/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-LGroup").trigger("click");
                            $(form).closeModal();
                            var tree = $("#treeLgroup").fancytree("getTree");
                            tree.reload().done(function () {
                            });
                        }
                    }).always(function () { });
                }
                });
            $("#frm-LGroup").viForm();
        });
    </script>
</body>
</html>
