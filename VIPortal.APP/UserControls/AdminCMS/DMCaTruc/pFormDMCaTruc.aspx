<%@ page language="C#" autoeventwireup="true" codebehind="pFormDMCaTruc.aspx.cs" inherits="VIPortalAPP.pFormDMCaTruc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-DMCaTruc" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oDMCaTruc.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <!--<input type="hidden" name="fldGroup" id="fldGroup" value="<%=oDMCaTruc.fldGroup %>" />-->
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oDMCaTruc.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="ThoiGianBatDau" class="col-sm-2 control-label">Thời gian bắt đầu</label>
            <div class="col-sm-10">
                <input type="text" name="ThoiGianBatDau" id="ThoiGianBatDau" placeholder="Nhập thoigianbatdau" value="<%:oDMCaTruc.ThoiGianBatDau%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="ThoiGianKetThuc" class="col-sm-2 control-label">Thời gian kết thúc</label>
            <div class="col-sm-10">
                <input type="text" name="ThoiGianKetThuc" id="ThoiGianKetThuc" placeholder="Nhập thoigianketthuc" value="<%:oDMCaTruc.ThoiGianKetThuc%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="ThoiGianKetThuc" class="col-sm-2 control-label">Phân loại ca trực</label>
            <div class="col-sm-10">
                <select id="LoaiCaTruc" name="LoaiCaTruc" class="form-control">
                    <option value="0">Ngày thường</option>
                    <option value="1">Ngày nghỉ</option>
                    <option value="2">Ngày Lễ</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="ThoiGianKetThuc" class="col-sm-2 control-label">Đơn vị/Phòng ban</label>
            <div class="col-sm-10">
              <select data-selected="<%=oDMCaTruc.fldGroup.LookupId %>" data-url="/UserControls/AdminCMS/LGroup/pAction.asp?do=QUERYDATA&isGetParent=true&GroupParent=<%=CurentUser.Groups.ID %>&isGetByParent=true" data-place="Chọn Phòng ban" name="fldGroup" id="fldGroup" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#LoaiCaTruc").val('<%=oDMCaTruc.LoaiCaTruc%>');
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oDMCaTruc.ListFileAttach)%>'
            });
            $("#Title").focus();
            //fldGroup
            $("#fldGroup").smSelect2018V2({
                dropdownParent: "#frm-DMCaTruc",
                parameterPlus: function (odata) {
                    odata["FieldOrder"] = "DMSTT";
                    odata["Ascending"] = true;

                }
            });

            $("#frm-DMCaTruc").validate({
                rules: {
                    Title: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/DMCaTruc/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-DMCaTruc").trigger("click");
                            $(form).closeModal();
                            $('#tblDMCaTruc').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
        });
    </script>
</body>
</html>
