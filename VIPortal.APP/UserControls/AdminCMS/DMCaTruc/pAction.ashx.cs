using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;

namespace VIPortalAPP.DMCaTruc
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);
            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            DMCaTrucDA oDMCaTrucDA = new DMCaTrucDA(UrlSite);
            DMCaTrucItem oDMCaTrucItem = new DMCaTrucItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oDMCaTrucDA.GetListJson(new DMCaTrucQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw, 
                        oDMCaTrucDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oDMCaTrucDA.GetListJson(new DMCaTrucQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDMCaTrucItem = oDMCaTrucDA.GetByIdToObject<DMCaTrucItem>(oGridRequest.ItemID);
                        oDMCaTrucItem.UpdateObject(context.Request);
                        oDMCaTrucDA.UpdateObject<DMCaTrucItem>(oDMCaTrucItem);
                    }
                    else
                    {
                        oDMCaTrucItem.UpdateObject(context.Request);
                        oDMCaTrucDA.UpdateObject<DMCaTrucItem>(oDMCaTrucItem);
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    string outb = oDMCaTrucDA.DeleteObject(oGridRequest.ItemID);
                    if (string.IsNullOrEmpty(outb))
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb;
                    break;
                case "DELETE-MULTI":
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oDMCaTrucDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                     oDMCaTrucDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDMCaTrucItem = oDMCaTrucDA.GetByIdToObjectSelectFields<DMCaTrucItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oDMCaTrucItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}