<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewDMCaTruc.aspx.cs" Inherits="VIPortalAPP.pViewDMCaTruc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
               <%=oDMCaTruc.Title%>
            </div>
        </div>
		<div class="form-group">
	<label for="ThoiGianBatDau" class="col-sm-2 control-label">ThoiGianBatDau</label>
	<div class="col-sm-10">
		<%=oDMCaTruc.ThoiGianBatDau%>
	</div>
</div>
<div class="form-group">
	<label for="ThoiGianKetThuc" class="col-sm-2 control-label">ThoiGianKetThuc</label>
	<div class="col-sm-10">
		<%=oDMCaTruc.ThoiGianKetThuc%>
	</div>
</div>

		  <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-10">
               <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oDMCaTruc.Created)%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oDMCaTruc.Modified)%>
            </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-10">
                <%=oDMCaTruc.Author.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-10">
                <%=oDMCaTruc.Editor.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oDMCaTruc.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oDMCaTruc.ListFileAttach[i].Url %>"><%=oDMCaTruc.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>