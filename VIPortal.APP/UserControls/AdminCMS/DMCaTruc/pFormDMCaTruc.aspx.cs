using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public partial class pFormDMCaTruc : pFormBase
    {
        public DMCaTrucItem oDMCaTruc {get;set;}
        protected void Page_Load(object sender, EventArgs e)
        {
			oDMCaTruc = new DMCaTrucItem();
            if (ItemID > 0)
            {
                DMCaTrucDA oDMCaTrucDA = new DMCaTrucDA(UrlSite);
                oDMCaTruc = oDMCaTrucDA.GetByIdToObject<DMCaTrucItem>(ItemID);
            }
        }
    }
}