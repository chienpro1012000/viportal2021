using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public partial class pViewDMCaTruc : pFormBase
    {
        public DMCaTrucItem oDMCaTruc = new DMCaTrucItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oDMCaTruc = new DMCaTrucItem();
            if (ItemID > 0)
            {
                DMCaTrucDA oDMCaTrucDA = new DMCaTrucDA();
                oDMCaTruc = oDMCaTrucDA.GetByIdToObject<DMCaTrucItem>(ItemID);

            }
        }
    }
}