using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;

namespace VIPortalAPP.UserControls.AdminCMS.TrangThanhPhan
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction :  hActionBase,IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);
            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            TrangThanhPhanDA oTrangThanhPhanDA = new TrangThanhPhanDA();
            TrangThanhPhanItem oTrangThanhPhanItem = new TrangThanhPhanItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oTrangThanhPhanDA.GetListJson(new TrangThanhPhanQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw, 
                        oTrangThanhPhanDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "TrangThanhPhan", "QUERYDATA", 0, "Xem danh sách trang thành phần");
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oTrangThanhPhanDA.GetListJson(new TrangThanhPhanQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oTrangThanhPhanItem = oTrangThanhPhanDA.GetByIdToObject<TrangThanhPhanItem>(oGridRequest.ItemID);
                        oTrangThanhPhanItem.UpdateObject(context.Request);
                        oTrangThanhPhanDA.UpdateObject<TrangThanhPhanItem>(oTrangThanhPhanItem);
                        AddLog("UPDATE", "TrangThanhPhan", "UPDATE", oGridRequest.ItemID, $"Cập nhật trang thành phần{oTrangThanhPhanItem.Title}");
                    }
                    else
                    {
                        oTrangThanhPhanItem.UpdateObject(context.Request);
                        oTrangThanhPhanDA.UpdateObject<TrangThanhPhanItem>(oTrangThanhPhanItem);
                        AddLog("UPDATE", "TrangThanhPhan", "ADD", oGridRequest.ItemID, $"Thêm mới trang thành phần{oTrangThanhPhanItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oTrangThanhPhanDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "TrangThanhPhan", "DELETE", oGridRequest.ItemID, $"Xóa trang thành phần{outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    break;
                case "DELETE-MULTI":
                    AddLog("DELETE-MULTI", "TrangThanhPhan", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều trang thành phần");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oTrangThanhPhanDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "TrangThanhPhan", "APPROVED", oGridRequest.ItemID, $"Duyệt trang thành phần{oTrangThanhPhanDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                     oTrangThanhPhanDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "TrangThanhPhan", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt trang thành phần{oTrangThanhPhanDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oTrangThanhPhanItem = oTrangThanhPhanDA.GetByIdToObjectSelectFields<TrangThanhPhanItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oTrangThanhPhanItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
                
            }
            oResult.ResponseData();

        }

    }
}