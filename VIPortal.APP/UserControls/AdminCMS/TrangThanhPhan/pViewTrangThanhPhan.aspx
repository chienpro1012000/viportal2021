<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewTrangThanhPhan.aspx.cs" Inherits="VIPortalAPP.UserControls.AdminCMS.TrangThanhPhan.pViewTrangThanhPhan" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
        <div class="form-group row">
            <label for="STT" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-10">
                <%=oTrangThanhPhan.STT%>
            </div>
        </div>
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên trang</label>
            <div class="col-sm-10">
                <%=oTrangThanhPhan.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="UrlView" class="col-sm-2 control-label">Đường dẫn khai thác</label>
            <div class="col-sm-10">
                <%=oTrangThanhPhan.UrlView%>
            </div>
        </div>
        <div class="form-group row">
            <label for="UrlQuanTri" class="col-sm-2 control-label">Đường dẫn quản trị</label>
            <div class="col-sm-10">
                <%=oTrangThanhPhan.UrlQuanTri%>
            </div>
        </div>
        <div class="form-group row">
            <label for="MoTa" class="col-sm-2 control-label">MoTa</label>
            <div class="col-sm-10">
                <%=oTrangThanhPhan.MoTa%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oTrangThanhPhan.Created)%>
            </div>
            <label class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oTrangThanhPhan.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
                <%=oTrangThanhPhan.Author.LookupValue%>
            </div>
            <label class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
                <%=oTrangThanhPhan.Editor.LookupValue%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oTrangThanhPhan.ListFileAttach.Count;i++){ %>
                <div><a href="<%=oTrangThanhPhan.ListFileAttach[i].Url %>"><%=oTrangThanhPhan.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>
