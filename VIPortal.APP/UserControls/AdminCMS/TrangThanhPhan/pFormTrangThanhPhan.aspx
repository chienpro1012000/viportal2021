<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormTrangThanhPhan.aspx.cs" Inherits="VIPortalAPP.UserControls.AdminCMS.TrangThanhPhan.pFormTrangThanhPhan" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-TrangThanhPhan" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oTrangThanhPhan.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên trang</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oTrangThanhPhan.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="STT" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-10">
                <input type="text" name="STT" id="STT" placeholder="Nhập stt" value="<%:oTrangThanhPhan.STT%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="UrlView" class="col-sm-2 control-label">Đường dẫn khai thác</label>
            <div class="col-sm-10">
                <textarea name="UrlView" id="UrlView" class="form-control"><%:oTrangThanhPhan.UrlView%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="UrlQuanTri" class="col-sm-2 control-label">Đường dẫn quản trị</label>
            <div class="col-sm-10">
                <textarea name="UrlQuanTri" id="UrlQuanTri" class="form-control"><%:oTrangThanhPhan.UrlQuanTri%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="MoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <textarea name="MoTa" id="MoTa" class="form-control"><%:oTrangThanhPhan.MoTa%></textarea>
            </div>
        </div>

        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oTrangThanhPhan.ListFileAttach)%>'
            });
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $(".form-group select").select2({
                dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
            });

            $("#frm-TrangThanhPhan").validate({
                rules: {
                    Title: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/TrangThanhPhan/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-TrangThanhPhan").trigger("click");
                            $(form).closeModal();
                            $('#tblTrangThanhPhan').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-TrangThanhPhan").viForm();
        });
    </script>
</body>
</html>
