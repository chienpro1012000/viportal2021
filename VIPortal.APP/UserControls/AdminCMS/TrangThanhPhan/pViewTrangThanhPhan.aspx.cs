using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP.UserControls.AdminCMS.TrangThanhPhan
{
    public partial class pViewTrangThanhPhan : pFormBase
    {
        public TrangThanhPhanItem oTrangThanhPhan = new TrangThanhPhanItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oTrangThanhPhan = new TrangThanhPhanItem();
            if (ItemID > 0)
            {
                TrangThanhPhanDA oTrangThanhPhanDA = new TrangThanhPhanDA();
                oTrangThanhPhan = oTrangThanhPhanDA.GetByIdToObject<TrangThanhPhanItem>(ItemID);

            }
        }
    }
}