using Microsoft.SharePoint;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData;
using ViPortalData.LichCaNhan;
using ViPortalData.LichDonVi;
using ViPortalData.LichPhong;

namespace VIPortalAPP.UserControls.AdminCMS.LichHop
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public string contentMail { get; set; }
        public List<string> mailto { get; set; }
        SPFieldLookupValueCollection lstThamGia = new SPFieldLookupValueCollection();
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);
            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            LichHopDA oLichHopDA = new LichHopDA(UrlSite);
            LichHopItem oLichHopItem = new LichHopItem();
            LUserDA oLUserDA = new LUserDA();
            LNotificationDA oLNotificationDA = new LNotificationDA(UrlSite);
            LNotificationItem lNotificationItem = new LNotificationItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oLichHopDA.GetListJsonSolr(new LichHopQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oLichHopDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    //AddLog("QUERYDATA", "LichHop", "QUERYDATA", 0, "Xem danh sách lịch họp");
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oLichHopDA.GetListJson(new LichHopQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "DKLICH":
                    if (!string.IsNullOrEmpty(context.Request["UrlList"]))
                    {
                        string JsonKhachMoi = "";
                        if (!string.IsNullOrEmpty(context.Request["JsonKhachMoi"]))
                        {
                            JsonKhachMoi = context.Request["JsonKhachMoi"];
                        }
                        string _LichSoNguoiThamgia = "";
                        if (!string.IsNullOrEmpty(context.Request["LichSoNguoiThamgia"]))
                        {
                            _LichSoNguoiThamgia = context.Request["LichSoNguoiThamgia"];
                        }
                        GiaoLichDonVi giaoLichDonVi = new GiaoLichDonVi()
                        {
                            LichLanhDaoChuTri = new SPFieldLookupValue(),
                            LichPhongBanThamGia = new SPFieldLookupValueCollection(),
                            LichThanhPhanThamGia = new SPFieldLookupValueCollection(),
                            JsonKhachMoi = Newtonsoft.Json.JsonConvert.DeserializeObject<List<KhachMoi>>(JsonKhachMoi),
                            LichGhiChuGiao = "",
                            LichSoNguoiThamgia = _LichSoNguoiThamgia,
                            ListFileAttach = new List<BaseFileAttach>()
                        };
                        string UrlList = context.Request["UrlList"];
                        if (UrlList == "/noidung/Lists/LichDonVi")
                        {
                            LichDonViDA lichDonViDA = new LichDonViDA();
                            LichDonViItem lichDonViItem = new LichDonViItem();
                            if (oGridRequest.ItemID > 0)
                                lichDonViItem = lichDonViDA.GetByIdToObject<LichDonViItem>(oGridRequest.ItemID);
                            lichDonViItem.UpdateObject(context.Request);
                            lichDonViItem.LogNoiDung = Newtonsoft.Json.JsonConvert.SerializeObject(giaoLichDonVi);
                            //xử lý chỗ khách mời
                            if (string.IsNullOrEmpty(lichDonViItem.LichNoiDung)) lichDonViItem.LichNoiDung = lichDonViItem.Title;
                            lichDonViItem.fldGroup = new SPFieldLookupValue(CurentUser.Groups.ID, CurentUser.Groups.Title);
                            LGroupDA lGroupDA = new LGroupDA();
                            //pb chủ trì chọn 1.
                            //get thông tin phòng ban đơn vị ở chỗ này để lúc sau hiển thị.
                            if (!string.IsNullOrEmpty(lichDonViItem.CHU_TRI))
                            {
                                int IDChu_tri = Convert.ToInt32(lichDonViItem.CHU_TRI);
                                LGroupItem oLGroupItem = lGroupDA.GetByIdToObject<LGroupItem>(IDChu_tri);
                                lichDonViItem.CHU_TRI = oLGroupItem.ID + ";#" + oLGroupItem.Title;
                            }
                            else if (!string.IsNullOrEmpty(context.Request["CHU_TRIKhac"]))
                            {
                                string CHU_TRIKhac = context.Request["CHU_TRIKhac"];
                                lichDonViItem.CHU_TRI = "-1;#" + CHU_TRIKhac;
                            }
                            if (!string.IsNullOrEmpty(lichDonViItem.CHUAN_BI))
                            {
                                List<int> lstChuanBi = clsFucUtils.GetDanhSachIDsQuaFormPost(lichDonViItem.CHUAN_BI);
                                List<LGroupJson> LstLGroupJson = lGroupDA.GetListJson(new LGroupQuery() { isGetBylistID = true, lstIDget = lstChuanBi });
                                lichDonViItem.CHUAN_BI = string.Join(";#", LstLGroupJson.Select(x => x.ID + ";#" + x.Title));
                            }
                            else if (!string.IsNullOrEmpty(context.Request["CHUAN_BI_Khac"]))
                            {
                                string CHUAN_BI_Khac = context.Request["CHUAN_BI_Khac"];
                                lichDonViItem.CHUAN_BI = "-1;#" + CHUAN_BI_Khac;
                            }
                            if (lichDonViItem.LichTrangThai == 1) //chỉ bằng 1 mới update thông tin này.
                                lichDonViItem.LogText += $"_CreatedUser{CurentUser.ID}_PBCreated{CurentUser.UserPhongBan.ID}_";
                            if (lichDonViItem.LichTrangThai == 2) //chỉ bằng 1 mới update thông tin này.
                                lichDonViItem.LogText += $"_Approve{CurentUser.ID}_";
                            lichDonViDA.UpdateObject<LichDonViItem>(lichDonViItem);
                            lichDonViDA.UpdateSPModerationStatus(lichDonViItem.ID, SPModerationStatusType.Approved);
                        }
                        else if (UrlList == "/noidung/Lists/LichPhong")
                        {
                            LichPhongDA lichCaNhanDA = new LichPhongDA();
                            LichPhongItem oLichPhongItem = new LichPhongItem();
                            if (oGridRequest.ItemID > 0)
                                oLichPhongItem = lichCaNhanDA.GetByIdToObject<LichPhongItem>(oGridRequest.ItemID);
                            oLichPhongItem.UpdateObject(context.Request);
                            oLichPhongItem.LogNoiDung = Newtonsoft.Json.JsonConvert.SerializeObject(giaoLichDonVi);
                            oLichPhongItem.LogText += $"_CreatedUser{CurentUser.ID}_PBCreated{CurentUser.UserPhongBan.ID}_";
                            oLichPhongItem.fldGroup = new SPFieldLookupValue(CurentUser.UserPhongBan.ID, CurentUser.UserPhongBan.Title);
                            
                            if (!string.IsNullOrEmpty(context.Request["CHU_TRIUser"]))
                            {
                                int IDChu_tri = Convert.ToInt32(context.Request["CHU_TRIUser"]);
                                LUserItem oLUserItem = oLUserDA.GetByIdToObject<LUserItem>(IDChu_tri);
                                oLichPhongItem.CHU_TRI = oLUserItem.ID + ";#" + oLUserItem.Title;
                            }
                            else if (!string.IsNullOrEmpty(context.Request["CHU_TRIUserKhac"]))
                            {
                                string CHU_TRIKhac = context.Request["CHU_TRIUserKhac"];
                                oLichPhongItem.CHU_TRI = "-1;#" + CHU_TRIKhac;
                            }


                            if (!string.IsNullOrEmpty(context.Request["CHUAN_BIUser"]))
                            {
                                string CHUAN_BIUser = context.Request["CHUAN_BIUser"];
                                List<int> lstChuanBi = clsFucUtils.GetDanhSachIDsQuaFormPost(CHUAN_BIUser);
                                List<LUserJson> LstLUserJson = oLUserDA.GetListJson(new LUserQuery() { isGetBylistID = true, lstIDget = lstChuanBi });
                                oLichPhongItem.CHUAN_BI = string.Join(";#", LstLUserJson.Select(x => x.ID + ";#" + x.Title));
                            }
                            else if (!string.IsNullOrEmpty(context.Request["CHUAN_BIUser_Khac"]))
                            {
                                string CHUAN_BIUser_Khac = context.Request["CHUAN_BIUser_Khac"];
                                oLichPhongItem.CHUAN_BI = "-1;#" + CHUAN_BIUser_Khac;
                            }

                            if (string.IsNullOrEmpty(oLichPhongItem.LichNoiDung)) oLichPhongItem.LichNoiDung = oLichPhongItem.Title;
                            lichCaNhanDA.UpdateObject<LichPhongItem>(oLichPhongItem);
                            lichCaNhanDA.UpdateSPModerationStatus(oLichPhongItem.ID, SPModerationStatusType.Approved);
                        }
                        oResult.Message = "Lưu thành công";
                    }
                    else
                    {
                        oResult = new ResultAction()
                        {
                            Message = "Không xác định loại lịch cần đăng ký",
                            State = ActionState.Error
                        };
                    }
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLichHopItem = oLichHopDA.GetByIdToObject<LichHopItem>(oGridRequest.ItemID);
                        oLichHopItem.UpdateObject(context.Request);
                        mailto = new List<string>();
                        if (oLichHopItem.LichLanhDaoChuTri.LookupId > 0 && oLichHopItem.LichThanhPhanThamGia.FindIndex(x => x == oLichHopItem.LichLanhDaoChuTri) == -1)
                            lstThamGia.Add(oLichHopItem.LichLanhDaoChuTri);

                        lstThamGia.AddRange(oLichHopItem.LichThanhPhanThamGia);

                        foreach (var item in lstThamGia)
                        {
                            var LUser = oLUserDA.GetByIdToObject<LUserItem>(item.LookupId);
                            if (!string.IsNullOrEmpty(LUser.UserEmail))
                            {
                                mailto.Add(LUser.UserEmail);
                            }
                        }
                        //gửi mail
                        contentMail = "Kính gửi ông/bà,";
                        contentMail += "<div> Ông bà đã nhận được thông báo về cuộc họp: " + oLichHopItem.Title + " vào hồi " + oLichHopItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy") + " tại " + oLichHopItem.LichDiaDiem + "</div>";
                        contentMail += "<div>Nhận được mail này mong quý vị sắp xếp công việc và thời gian tham gia cuộc họp.</div>";
                        contentMail += "<div>Trân trọng cảm ơn.</div>";
                        if (mailto.Count > 0)
                        {
                            SendMail("[Email thông báo lịch họp]", contentMail, mailto);
                        }

                        #region Lưu Notifi
                        string NguoiTao = context.Request["NguoiTao"];
                        lNotificationItem.TitleBaiViet = oLichHopItem.Title;
                        lNotificationItem.LNotiNguoiGui = NguoiTao;
                        if (lstThamGia.Count > 0)
                        {
                            lNotificationItem.LNotiNguoiNhan_Id = "," + string.Join(",", lstThamGia) + ",";
                            lNotificationItem.LNotiChuaXem_Id = lNotificationItem.LNotiNguoiNhan_Id;
                        }
                        lNotificationItem.LNotiData = Newtonsoft.Json.JsonConvert.SerializeObject(new SPEntity() { ID = oLichHopItem.ID, Title = oLichHopItem.Title });
                        lNotificationItem.DoiTuongTitle = "Lịch họp";
                        lNotificationItem.Title = $"Lịch họp:  {oLichHopItem.Title} ngày {oLichHopItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy")} ";
                        lNotificationItem.LNotiMoTa = $"Lịch họp:  {oLichHopItem.Title} ngày {oLichHopItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy")} ";
                        lNotificationItem.LNotiIsSentMail = 1;
                        lNotificationItem.UrlLink = "/Pages/lichhop.aspx?FromDate=" + string.Format("{0:dd/MM/yyyy}", oLichHopItem.LichThoiGianBatDau) + "&ToDate=" + string.Format("{0:dd/MM/yyyy}", oLichHopItem.LichThoiGianKetThuc) + $"&ItemID={oLichHopItem.ID}";
                        lNotificationItem.LNotiSentTime = DateTime.Now;
                        oLNotificationDA.UpdateObject<LNotificationItem>(lNotificationItem);

                        #endregion
                        oLichHopDA.UpdateObject<LichHopItem>(oLichHopItem);
                        AddLog("UPDATE", "LichHop", "UPDATE", oGridRequest.ItemID, $"Cập nhật lịch họp{oLichHopItem.Title}");
                    }
                    else
                    {
                        oLichHopItem.UpdateObject(context.Request);
                        mailto = new List<string>();
                        if (oLichHopItem.LichLanhDaoChuTri.LookupId > 0 && oLichHopItem.LichThanhPhanThamGia.FindIndex(x => x == oLichHopItem.LichLanhDaoChuTri) == -1)
                            lstThamGia.Add(oLichHopItem.LichLanhDaoChuTri);
                        lstThamGia.AddRange(oLichHopItem.LichThanhPhanThamGia);

                        foreach (var item in lstThamGia)
                        {
                            var LUser = oLUserDA.GetByIdToObject<LUserItem>(item.LookupId);
                            if (!string.IsNullOrEmpty(LUser.UserEmail))
                            {
                                mailto.Add(LUser.UserEmail);
                            }
                        }
                        oLichHopDA.UpdateObject<LichHopItem>(oLichHopItem);
                        //gửi mail
                        contentMail = "Kính gửi ông/bà,";
                        contentMail += "<div> Ông bà đã nhận được thông báo về cuộc họp: " + oLichHopItem.Title + " vào hồi " + oLichHopItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy") + " tại " + oLichHopItem.LichDiaDiem + "</div>";
                        contentMail += "<div>Nhận được mail này mong quý vị sắp xếp công việc và thời gian tham gia cuộc họp.</div>";
                        contentMail += "<div>Trân trọng cảm ơn.</div>";
                        if (mailto.Count > 0)
                        {
                            SendMail("[Email thông báo lịch họp]", contentMail, mailto);
                        }

                        #region Lưu Notifi
                        string NguoiTao = context.Request["NguoiTao"];
                        lNotificationItem.TitleBaiViet = oLichHopItem.Title;
                        lNotificationItem.LNotiNguoiGui = NguoiTao;
                        if (lstThamGia.Count > 0)
                        {
                            lNotificationItem.LNotiNguoiNhan_Id = "," + string.Join(",", lstThamGia) + ",";
                            lNotificationItem.LNotiChuaXem_Id = lNotificationItem.LNotiNguoiNhan_Id;
                        }
                        lNotificationItem.LNotiData = Newtonsoft.Json.JsonConvert.SerializeObject(new SPEntity() { ID = oLichHopItem.ID, Title = oLichHopItem.Title });
                        lNotificationItem.DoiTuongTitle = "Lịch họp";
                        lNotificationItem.Title = $"Lịch họp:  {oLichHopItem.Title} ngày {oLichHopItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy")} ";
                        lNotificationItem.LNotiMoTa = $"Lịch họp:  {oLichHopItem.Title} ngày {oLichHopItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy")} ";
                        lNotificationItem.LNotiIsSentMail = 1;
                        lNotificationItem.LNotiSentTime = DateTime.Now;
                        lNotificationItem.UrlLink = "/Pages/lichhop.aspx?FromDate=" + string.Format("{0:dd/MM/yyyy}", oLichHopItem.LichThoiGianBatDau) + "&ToDate=" + string.Format("{0:dd/MM/yyyy}", oLichHopItem.LichThoiGianKetThuc) + $"&ItemID={oLichHopItem.ID}";
                        oLNotificationDA.UpdateObject<LNotificationItem>(lNotificationItem);
                        #endregion

                        AddLog("UPDATE", "LichHop", "ADD", oGridRequest.ItemID, $"Thêm mới lịch họp {oLichHopItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oLichHopDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "LichHop", "DELETE", oGridRequest.ItemID, $"Xóa lịch họp {outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oLichHopDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    AddLog("DELETE-MULTI", "LichHop", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều lịch họp");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oLichHopDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "LichHop", "APPROVED", oGridRequest.ItemID, $"Duyệt lịch họp {oLichHopDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oLichHopDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "LichHop", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt lịch họp {oLichHopDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLichHopItem = oLichHopDA.GetByIdToObjectSelectFields<LichHopItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oLichHopItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}