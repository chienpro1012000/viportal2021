<%@ control language="C#" autoeventwireup="true" codebehind="UC_LichHop.aspx.cs" inherits="VIPortalAPP.UserControls.AdminCMS.LichHop.UC_LichHop" %>
<div role="body-data" data-title="" class="content_wp" data-action="/UserControls/AdminCMS/LichHop/pAction.asp" data-form="/UserControls/AdminCMS/LichHop/pFormLichHop.aspx" data-view="/UserControls/AdminCMS/LichHop/pViewLichHop.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="LichHopSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title,LichDiaDiem,FullText" />
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <button class="btn btn-primary act-add" data-per="01010602" type="button">Thêm mới</button>
                    </p>
                </div>
            </div>

            <div class="clsgrid table-responsive">

                <table id="tblLichHop" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        var $tagvalue = $(".content_wp");
        var $tblDanhMucChung = $("#tblLichHop").viDataTable(
            {
                "frmSearch": "LichHopSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "mData": "ID",
                            "name": "ID", "visible": false, "sTitle": "Số thứ tự"
                        },{
                            "name": "LichThoiGianBatDau", "sTitle": "Thời gian bắt đầu",
                            "mData": function (o) {
                                return formatDateTime(o.LichThoiGianBatDau);
                            },
                            "sWidth": "120px",
                        },
                        {
                            "mData": function (o) {
                                if (o.Title != null)
                                    return '<a data-Itemid="' + o.ID + '" class="act-view">' + o.Title + '</a>';
                                else
                                    return "";
                            },
                            "name": "Title", "sTitle": "Tên"
                        },
                        {
                            "mData": function (o) {
                                return o.LichLanhDaoChuTri.Title;
                            },
                            "name": "LichLanhDaoChuTri", "sTitle": "Lãnh đạo chủ trì"
                        },
                        {
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '" >' + o.LichDiaDiem + '</a>';
                            },
                            "name": "LichDiaDiem", "sTitle": "Địa điểm"
                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {
                                if (o._ModerationStatus == 0)

                                    return '<a data-per="01010605" title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a data-per="01010605" title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }
                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" title="Sửa" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        }, {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE" title="Xóa"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                "aaSorting": [0, 'desc']
            });
    });
</script>
