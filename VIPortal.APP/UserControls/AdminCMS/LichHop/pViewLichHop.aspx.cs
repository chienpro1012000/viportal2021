using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP.UserControls.AdminCMS.LichHop
{
    public partial class pViewLichHop : pFormBase
    {
        public LichHopItem oLichHop = new LichHopItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oLichHop = new LichHopItem();
            if (ItemID > 0)
            {
                LichHopDA oLichHopDA = new LichHopDA();
                oLichHop = oLichHopDA.GetByIdToObject<LichHopItem>(ItemID);

            }
        }
    }
}