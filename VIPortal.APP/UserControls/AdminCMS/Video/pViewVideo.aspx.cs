using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP.UserControls.AdminCMS.Video
{
    public partial class pViewVideo : pFormBase
    {
        public VideoItem oVideo = new VideoItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oVideo = new VideoItem();
            if (ItemID > 0)
            {
                VideoDA oVideoDA = new VideoDA(UrlSite);
                oVideo = oVideoDA.GetByIdToObject<VideoItem>(ItemID);

            }
        }
    }
}