<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewVideo.aspx.cs" Inherits="VIPortalAPP.UserControls.AdminCMS.Video.pViewVideo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">

        <div class="form-group row">
            <label for="STT" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-10">
                <%=oVideo.STT%>
            </div>
        </div>
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <%=oVideo.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="DanhMuc" class="col-sm-2 control-label">Danh mục</label>
            <div class="col-sm-10">
                <%=oVideo.DanhMuc.LookupValue%>
            </div>
        </div>
        <div class="form-group row">
            <label for="LinkVideo" class="col-sm-2 control-label">LinkVideo</label>
            <div class="col-sm-10">
                <%=oVideo.LinkVideo%>
            </div>
        </div>
        <div class="form-group row">
            <label for="NgayDang" class="col-sm-2 control-label">Ngày đăng</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy}", oVideo.NgayDang)%>
            </div>
        </div>
        <div class="form-group row">
            <label for="ImageNews" class="col-sm-2 control-label">Ảnh đại diện</label>
            <div class="col-sm-10">
                <img style="width:100%;" src="<%=oVideo.ImageNews%>" alt="Alternate Text" />
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oVideo.Created)%>
            </div>
            <label class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oVideo.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
                <%=oVideo.Author.LookupValue%>
            </div>
            <label class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
                <%=oVideo.Editor.LookupValue%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oVideo.ListFileAttach.Count;i++){ %>
                <div><a href="<%=oVideo.ListFileAttach[i].Url %>"><%=oVideo.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>
