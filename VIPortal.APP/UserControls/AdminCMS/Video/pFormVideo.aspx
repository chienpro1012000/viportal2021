<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormVideo.aspx.cs" Inherits="VIPortalAPP.UserControls.AdminCMS.Video.pFormVideo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-Video" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oVideo.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên video</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" placeholder="Nhập tiêu đề video" class="form-control" value="<%=oVideo.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="DanhMuc" class="col-sm-2 control-label">Danh mục video</label>
            <div class="col-sm-10">
                <select data-selected="<%:oVideo.DanhMuc.LookupId%>" data-url="/UserControls/AdminCMS/DMVideo/pAction.asp?do=QUERYDATA" data-place="Chọn danh mục video" name="DanhMuc" id="DanhMuc" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <label for="LinkVideo" class="col-sm-2 control-label">LinkVideo</label>
            <div class="col-sm-10">
                <textarea name="LinkVideo" id="LinkVideo" placeholder="Link video nhúng" class="form-control"><%:oVideo.LinkVideo%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="STT" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-4">
                <input type="text" name="STT" id="STT" placeholder="Nhập STT" value="<%:oVideo.STT%>" class="form-control" />
            </div>
            <label for="NgayDang" class="col-sm-2 control-label">Ngày đăng</label>
            <div class="col-sm-4">
                <input type="text" name="NgayDang" id="NgayDang" value="<%:string.Format("{0:dd/MM/yyyy}",oVideo.NgayDang)%>" class="form-control input-datetime" />
            </div>
        </div>
        <div class="form-group row">
            <label for="ImageNews" class="col-sm-2 control-label">Ảnh đại diện</label>
            <div class="col-sm-10" data-role="groupImageDD">
                <div id="imgImageNews" style="float: left; padding-right: 10px;">
                    <div class="include-library" style="display: inline-block; clear: both;">
                        <input type="hidden" name="ImageNews" id="ImageNews" value="<%:oVideo.ImageNews%>" />
                        <img title="Ảnh đại diện" id="srcImageNews" src="<%=oVideo.ImageNews%>" />
                    </div>
                </div>
                <button id="btnImageNews" type="button" class="btn  btn-success">Ảnh đại diện</button>
                <button id="btnXoaImageNews" type="button" class="btn btn-danger">Xóa ảnh</button>
            </div>

        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-10">
                <input type="file" id="FileAttach" accept="video/mp4,video/x-m4v,video/*" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oVideo.ListFileAttach)%>'
            });
            $("#DanhMuc").smSelect2018V2({
                dropdownParent: "#frm-Video"
            });
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $(".form-group select").select2({
                dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
            });
            $("#btnImageNews").click(function () {
                $elementId = $(this);
                CKFinder.popup({
                    chooseFiles: true,
                    width: 800,
                    height: 600,
                    onInit: function (finder) {
                        finder.on('files:choose', function (evt) {
                            var file = evt.data.files.first();
                            var urlfileimg = file.getUrl();
                            document.getElementById("ImageNews").value = urlfileimg;
                            document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                        });
                        finder.on('file:choose:resizedImage', function (evt) {
                            //var output = document.getElementById(elementId);
                            console.log(evt.data);
                            var urlfileimg = evt.data.resizedUrl;
                            document.getElementById("ImageNews").value = urlfileimg;
                            document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                        });
                    }
                });
            });
            DeleteImage('btnXoaImageNews', 'srcImageNews', 'ImageNews');
            $("#frm-Video").validate({
                ignore: [],
                rules: {
                    Title: "required",
                    DanhMuc: "required",
                    DMSTT: {
                        numberchar: true, required: true
                    },
                    ImageNews: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tên video",
                    DanhMuc: "Chọn danh mục video",                    
                    ImageNews: "Vui lòng chọn ảnh đại diện",
                },
                submitHandler: function (form) {

                    $.post("/UserControls/AdminCMS/Video/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-Video").trigger("click");
                            $(form).closeModal();
                            $('#tblVideo').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-Video").viForm();
        });
    </script>
</body>
</html>
