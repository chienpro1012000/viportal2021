using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;

namespace VIPortalAPP.UserControls.AdminCMS.Video
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);
            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            VideoDA oVideoDA = new VideoDA(UrlSite);
            VideoItem oVideoItem = new VideoItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oVideoDA.GetListJsonSolr(new VideoQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw, 
                        oVideoDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "Video", "QUERYDATA", 0, "Xem danh sách video");
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oVideoDA.GetListJson(new VideoQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oVideoItem = oVideoDA.GetByIdToObject<VideoItem>(oGridRequest.ItemID);
                        oVideoItem.UpdateObject(context.Request);
                        if(string.IsNullOrEmpty( oVideoItem.LinkVideo) && (oVideoItem.ListFileAttach.Count + oVideoItem.ListFileAttachAdd.Count) == 0)
                        {
                            oResult.Message = "Bản ghi chưa có thông tin Video";
                            oResult.State = ActionState.Error;
                            break;
                        } 
                            

                        if (oVideoDA.CheckExit(oGridRequest.ItemID, oVideoItem.Title) > 0)
                        {
                            oResult = new ResultAction()
                            {
                                State = ActionState.Error,
                                Message = $"Đã tồn tại video có tiêu đề {oVideoItem.Title}"
                            };
                            break;
                        }
                        oVideoDA.UpdateObject<VideoItem>(oVideoItem);
                        AddLog("UPDATE", "Video", "UPDATE", oGridRequest.ItemID, $"Cập nhật video{oVideoItem.Title}");
                    }
                    else
                    {
                        oVideoItem.UpdateObject(context.Request);
                        if (string.IsNullOrEmpty(oVideoItem.LinkVideo) && (oVideoItem.ListFileAttach.Count + oVideoItem.ListFileAttachAdd.Count) == 0)
                        {
                            oResult.Message = "Bản ghi chưa có thông tin Video";
                            oResult.State = ActionState.Error;
                            break;
                        }
                        if (oVideoDA.CheckExit(oGridRequest.ItemID, oVideoItem.Title) > 0)
                        {
                            oResult = new ResultAction()
                            {
                                State = ActionState.Error,
                                Message = $"Đã tồn tại video có tiêu đề {oVideoItem.Title}"
                            };
                            break;
                        }
                        oVideoDA.UpdateObject<VideoItem>(oVideoItem);
                        AddLog("UPDATE", "Video", "ADD", oGridRequest.ItemID, $"Thêm mới video{oVideoItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oVideoDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "Video", "DELETE", oGridRequest.ItemID, $"Xóa video{outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    AddLog("DELETE-MULTI", "Video", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều video");
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oVideoDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oVideoDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "Video", "APPROVED", oGridRequest.ItemID, $"Duyệt video{oVideoDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                     oVideoDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "Video", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt video{oVideoDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oVideoItem = oVideoDA.GetByIdToObjectSelectFields<VideoItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oVideoItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}