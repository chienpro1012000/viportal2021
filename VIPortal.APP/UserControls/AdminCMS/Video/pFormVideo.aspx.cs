using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP.UserControls.AdminCMS.Video
{
    public partial class pFormVideo : pFormBase
    {
        public VideoItem oVideo {get;set;}
        protected void Page_Load(object sender, EventArgs e)
        {
			oVideo = new VideoItem();
            VideoDA oVideoDA = new VideoDA(UrlSite);
            if (ItemID > 0)
            {
                oVideo = oVideoDA.GetByIdToObject<VideoItem>(ItemID);
            }
            else
            {
                oVideo.STT = oVideoDA.GetLastSTT("STT");
            }
        }
    }
}