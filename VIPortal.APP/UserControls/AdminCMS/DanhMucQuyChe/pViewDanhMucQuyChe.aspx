﻿<%@ page language="C#" autoeventwireup="true" codebehind="pViewDanhMucQuyChe.aspx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.DanhMucQuyChe.pViewDanhMucQuyChe" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		         <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Quy chế</label>
            <div class="col-sm-10">
               <%=oDanhMucQuyChe.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="HoTen" class="col-sm-2 control-label">Số thứ tự</label>
            <div class="col-sm-4">
                 <%:(oDanhMucQuyChe!= null &&  oDanhMucQuyChe.DMSTT>0)?oDanhMucQuyChe.DMSTT+"": "" %>
            </div>
               <label class="control-label col-sm-2">Hiển thị</label>
            <div class="col-sm-4">
                <%=oDanhMucQuyChe.DMHienThi? "Có hiển thị" : "Không hiển thị"%>
                </div>
        </div>
       <div class="form-group row">
            <label for="DMMoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
               <%:oDanhMucQuyChe.DMMoTa%>
            </div>
        </div>
        <div class="form-group row">
            <label for="DMVietTat" class="col-sm-2 control-label">Viết tắt</label>
            <div class="col-sm-4">
                <%:oDanhMucQuyChe.DMVietTat%>
            </div>
        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <%for(int i=0; i< oDanhMucQuyChe.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oDanhMucQuyChe.ListFileAttach[i].Url %>"><%=oDanhMucQuyChe.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%=string.Format("{0:dd-MM-yyyy}", oDanhMucQuyChe.Created)%>
            </div>
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}",oDanhMucQuyChe.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
                <%=oDanhMucQuyChe.Author.LookupValue%>
            </div>
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
                <%=oDanhMucQuyChe.Editor.LookupValue%>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>