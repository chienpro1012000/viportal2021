﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using ViPortal_Utils;
using ViPortalData.QuyCheNoiBo;

namespace VIPortal.APP.UserControls.AdminCMS.DanhMucQuyChe
{
    public class Worker
    {
        public Worker(int _ItemID)
        {
            ItemID = _ItemID;
        }
        public int ItemID { get; set; }
        SimpleLogger oSimpleLogger = new SimpleLogger();
        
        public void StartProcessing(CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                oSimpleLogger.Info("StartProcessing");
                oSimpleLogger.Info($"DMQuyChe_Id:,{ItemID},");
                QuyCheNoiBoDA oQuyCheNoiBoDA = new QuyCheNoiBoDA(true);
                var allresult = oQuyCheNoiBoDA.solrWorkerDM.Query($"DMQuyChe_Id:,{ItemID},");
                foreach (var item in allresult)
                {
                    oQuyCheNoiBoDA.SaveSolr(item.SP_ID);
                }

            }
            catch (Exception ex)
            {
                var line = Environment.NewLine + Environment.NewLine;

                string ErrorlineNo = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
                string  Errormsg = ex.GetType().Name.ToString();
                string extype = ex.GetType().ToString();
                string ErrorLocation = ex.Message.ToString();
                oSimpleLogger.Error(ErrorlineNo);
                oSimpleLogger.Error(Errormsg);
                oSimpleLogger.Error(extype);
                oSimpleLogger.Error(ErrorLocation);
                ProcessCancellation();
                //File.AppendAllText(filePath, "Error Occured : " + ex.GetType().ToString() + " : " + ex.Message);
            }
        }
        

        private void ProcessCancellation()
        {
            Thread.Sleep(1000);

            //File.AppendAllText(filePath, "Process Cancelled");
        }
    }
}