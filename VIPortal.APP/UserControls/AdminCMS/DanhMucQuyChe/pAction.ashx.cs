﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData.DanhMucQuyChe;
using ViPortalData.QuyCheNoiBo;

namespace VIPortal.APP.UserControls.AdminCMS.DanhMucQuyChe
{

    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {

        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            DanhMucQuyCheDA oDanhMucQuyCheDA = new DanhMucQuyCheDA(UrlSite);
            DanhMucQuyCheItem oDanhMucQuyCheItem = new DanhMucQuyCheItem();
            QuyCheNoiBoDA oQueCheDA = new QuyCheNoiBoDA();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    var oData = oDanhMucQuyCheDA.GetListJson(new DanhMucQuyCheQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oDanhMucQuyCheDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "DanhMucQuyChe", "QUERYDATA", 0, "Xem danh sách danh mục quy chế");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataDanhMucQuyChe = oDanhMucQuyCheDA.GetListJson(new DanhMucQuyCheQuery(context.Request));
                    treeViewItems = oDataDanhMucQuyChe.Select(x => new TreeViewItem()
                    {
                        key = x.DMDescription,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;

                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oDanhMucQuyCheDA.GetListJson(new DanhMucQuyCheQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oDanhMucQuyCheDA.CheckExit(oGridRequest.ItemID, context.Request["Title"]) == 0)
                    {
                        if (oGridRequest.ItemID > 0)
                        {

                            oDanhMucQuyCheItem = oDanhMucQuyCheDA.GetByIdToObject<DanhMucQuyCheItem>(oGridRequest.ItemID);
                            oDanhMucQuyCheItem.UpdateObject(context.Request);
                            oDanhMucQuyCheDA.UpdateObject<DanhMucQuyCheItem>(oDanhMucQuyCheItem);
                            AddLog("UPDATE", "DanhMucQuyChe", "UPDATE", oGridRequest.ItemID, $"Cập nhật danh mục quy chế{oDanhMucQuyCheItem.Title}");

                            HostingEnvironment.QueueBackgroundWorkItem(cancellationToken => new Worker(oGridRequest.ItemID).StartProcessing(cancellationToken));
                        }
                        else
                        {
                            oDanhMucQuyCheItem.UpdateObject(context.Request);
                            oDanhMucQuyCheDA.UpdateObject<DanhMucQuyCheItem>(oDanhMucQuyCheItem);
                            AddLog("UPDATE", "DanhMucQuyChe", "ADD", oGridRequest.ItemID, $"Thêm mới danh mục quy chế{oDanhMucQuyCheItem.Title}");
                        }
                        oResult.Message = "Lưu thành công";
                    }
                    else
                    {
                        oResult = new ResultAction() { State = ActionState.Error, Message = $"Tiêu đề {context.Request["Title"]} đã tồn tại trong hệ thống" };
                    }
                    
                    break;
                case "DELETE":
                    if (oQueCheDA.GetListJson(new QuyCheNoiBoQuery() { QC_DMQuyChe = oGridRequest.ItemID, _ModerationStatus = 1 }).Count > 0 )
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Danh mục chứa quy chế đang hoạt động";
                    }
                    else
                    {
                        var outb = oDanhMucQuyCheDA.DeleteObjectV2(oGridRequest.ItemID);
                        AddLog("DELETE", "DanhMucQuyChe", "DELETE", oGridRequest.ItemID, $"Xóa danh mục quy chế {outb.Message}");
                        if (outb.State == ActionState.Succeed)
                            oResult.Message = "Xóa thành công";
                        HostingEnvironment.QueueBackgroundWorkItem(cancellationToken => new Worker(oGridRequest.ItemID).StartProcessing(cancellationToken));
                    }
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oDanhMucQuyCheDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    AddLog("DELETE-MULTI", "DanhMucQuyChe", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều danh mục quy chế");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oDanhMucQuyCheDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "DanhMucQuyChe", "APPROVED", oGridRequest.ItemID, $"Duyệt danh mục quy chế{oDanhMucQuyCheDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oDanhMucQuyCheDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "DanhMucQuyChe", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt danh mục quy chế{oDanhMucQuyCheDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDanhMucQuyCheItem = oDanhMucQuyCheDA.GetByIdToObjectSelectFields<DanhMucQuyCheItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oDanhMucQuyCheItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}