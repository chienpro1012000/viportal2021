﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.DanhMucQuyChe;

namespace VIPortal.APP.UserControls.AdminCMS.DanhMucQuyChe
{
    public partial class pFormDanhMucQuyChe : pFormBase
    {
        public DanhMucQuyCheItem oDanhMucQuyChe { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oDanhMucQuyChe = new DanhMucQuyCheItem();
            if (ItemID > 0)
            {
                DanhMucQuyCheDA oDanhMucQuyCheDA = new DanhMucQuyCheDA(UrlSite );
                oDanhMucQuyChe = oDanhMucQuyCheDA.GetByIdToObject<DanhMucQuyCheItem>(ItemID);
            }
        }
    }
}