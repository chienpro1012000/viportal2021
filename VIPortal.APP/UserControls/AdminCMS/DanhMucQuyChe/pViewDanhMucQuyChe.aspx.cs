﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ViPortalData.DanhMucQuyChe;
using ViPortal_Utils.Base;
using VIPORTAL.oWeb;

namespace VIPortal.APP.UserControls.AdminCMS.DanhMucQuyChe
{
    public partial class pViewDanhMucQuyChe : pFormBase
    {
        public DanhMucQuyCheItem oDanhMucQuyChe = new DanhMucQuyCheItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oDanhMucQuyChe = new DanhMucQuyCheItem();
            if (ItemID > 0)
            {
                DanhMucQuyCheDA oDanhMucQuyCheDA = new DanhMucQuyCheDA(UrlSite);
                oDanhMucQuyChe = oDanhMucQuyCheDA.GetByIdToObject<DanhMucQuyCheItem>(ItemID);

            }
        }
    }
}