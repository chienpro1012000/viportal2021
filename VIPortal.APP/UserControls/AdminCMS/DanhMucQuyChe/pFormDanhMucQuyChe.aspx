﻿<%@ page language="C#" autoeventwireup="true" codebehind="pFormDanhMucQuyChe.aspx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.DanhMucQuyChe.pFormDanhMucQuyChe" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
    <style>
        .error {
            color: red;
        }
    </style>
</head>
<body>
    <form id="frm-DanhMucQuyChe" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oDanhMucQuyChe.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />

        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên danh mục</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" placeholder="Nhập tên danh mục" class="form-control" value="<%=oDanhMucQuyChe.Title%>" />
            </div>
        </div>
        
        <div class="form-group row">
            <label for="DMMoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <textarea name="DMMoTa" id="DMMoTa" placeholder="Nhập mô tả" class="form-control"><%:oDanhMucQuyChe.DMMoTa%></textarea>
            </div>
        </div>
       
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            
            $("#Title").focus();
            

            $("#frm-DanhMucQuyChe").validate({
                rules: {
                    Title: "required",
                },
                messages: {
                    Title: "Vui lòng nhập tên danh mục",
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/DanhMucQuyChe/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-DanhMucQuyChe").trigger("click");
                            $(form).closeModal();
                            $('#tblDanhMucQuyChe').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-DanhMucQuyChe").viForm();
        });
    </script>
</body>
</html>
