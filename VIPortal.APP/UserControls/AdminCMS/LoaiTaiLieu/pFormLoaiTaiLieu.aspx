﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormLoaiTaiLieu.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.LoaiTaiLieu.pFormLoaiTaiLieu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-LoaiTaiLieu" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oLoaiTaiLieu.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Loại tài liệu</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" placeholder="Nhập loại tài liệu" class="form-control" value="<%=oLoaiTaiLieu.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <%--<label for="HoTen" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-4">
                 <input type="text" name="DMSTT" id="DMSTT" placeholder="Nhập số thứ tự" value="<%:(oLoaiTaiLieu!= null &&  oLoaiTaiLieu.DMSTT>0)?oLoaiTaiLieu.DMSTT+"": "" %>" class="form-control" />
            </div>--%>
            <label for="DMVietTat" class="col-sm-2 control-label">Viết tắt</label>
            <div class="col-sm-10">
                <input type="text" name="DMVietTat" id="DMVietTat" placeholder="Nhập tên viết tắt" value="<%:oLoaiTaiLieu.DMVietTat%>" class="form-control" />
            </div>
        </div>
       <div class="form-group row">
            <label for="MaSV" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <textarea name="DMMoTa" id="DMMoTa" placeholder="Mô tả tài liệu" class="form-control"><%:oLoaiTaiLieu.DMMoTa%></textarea>
            </div>
        </div>
        <div class="form-group row">
            
        </div>
        <%--<div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>--%>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#KSBoChuDe").smSelect2018V2({
                dropdownParent: "#frm-LoaiTaiLieu"
            });
            <%--$("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oLoaiTaiLieu.ListFileAttach)%>'
            });--%>
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $(".form-group select").select2({
                dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
            });

            $("#frm-LoaiTaiLieu").validate({
                rules: {
                    DMVietTat: "required",
                    Title: "required",                   
                },
                messages: {
                    Title: "Vui lòng nhập tên loại tài liệu",
                    DMVietTat:"Vui lòng nhập tên viết tắt"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/LoaiTaiLieu/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-LoaiTaiLieu").trigger("click");
                            $(form).closeModal();
                            $('#tblLoaiTaiLieu').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-LoaiTaiLieu").viForm();
        });
    </script>
</body>
</html>

