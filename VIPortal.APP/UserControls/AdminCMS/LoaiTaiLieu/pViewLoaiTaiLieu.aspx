﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewLoaiTaiLieu.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.LoaiTaiLieu.pViewLoaiTaiLieu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Loại tài liệu</label>
            <div class="col-sm-10">
               <%=oLoaiTaiLieu.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="HoTen" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-4">
                 <%:(oLoaiTaiLieu!= null &&  oLoaiTaiLieu.DMSTT>0)?oLoaiTaiLieu.DMSTT+"": "" %>
            </div>
            <label class="control-label col-sm-2">Hiển thị</label>
            <div class="col-sm-4">
                <div class="checkbox">
                    <%:oLoaiTaiLieu.DMHienThi?"Có":"Không" %>                  
                </div>
            </div>
        </div>
       <div class="form-group row">
            <label for="MaSV" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
               <%:oLoaiTaiLieu.DMMoTa%>
            </div>
        </div>
        <div class="form-group row">
            <label for="MaSV" class="col-sm-2 control-label">Viết tắt</label>
            <div class="col-sm-4">
               <%:oLoaiTaiLieu.DMVietTat%>
            </div>
        </div>
        <%--<div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oLoaiTaiLieu.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oLoaiTaiLieu.ListFileAttach[i].Url %>"><%=oLoaiTaiLieu.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>--%>
		  <div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%=string.Format("{0:dd-MM-yyyy}", oLoaiTaiLieu.Created)%>
            </div>
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}",oLoaiTaiLieu.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
               <%=oLoaiTaiLieu.Author.LookupValue%>
            </div>
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
               <%=oLoaiTaiLieu.Editor.LookupValue%>
            </div>    
        </div>
         
        <div class="clearfix"></div>
    </form>
</body>
</html>
