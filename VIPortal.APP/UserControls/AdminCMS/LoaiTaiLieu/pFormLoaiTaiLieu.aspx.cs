﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.LoaiTaiLieu;

namespace VIPortal.APP.UserControls.AdminCMS.LoaiTaiLieu
{
    public partial class pFormLoaiTaiLieu : pFormBase
    {
        public LoaiTaiLieuItem oLoaiTaiLieu { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oLoaiTaiLieu = new LoaiTaiLieuItem();
            if (ItemID > 0)
            {
                LoaiTaiLieuDA oLoaiTaiLieuDA = new LoaiTaiLieuDA(UrlSite);
                oLoaiTaiLieu = oLoaiTaiLieuDA.GetByIdToObject<LoaiTaiLieuItem>(ItemID);
            }
        }
    }
}