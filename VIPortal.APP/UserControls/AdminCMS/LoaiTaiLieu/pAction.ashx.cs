﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData.LoaiTaiLieu;

namespace VIPortal.APP.UserControls.AdminCMS.LoaiTaiLieu
{

    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);
            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            LoaiTaiLieuDA oLoaiTaiLieuDA = new LoaiTaiLieuDA(UrlSite);
            LoaiTaiLieuItem oLoaiTaiLieuItem = new LoaiTaiLieuItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    var oData = oLoaiTaiLieuDA.GetListJson(new LoaiTaiLieuQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oLoaiTaiLieuDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "LoaiTaiLieu", "QUERYDATA", 0, "Xem danh sách loại tài liệu");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataLoaiTaiLieu = oLoaiTaiLieuDA.GetListJson(new LoaiTaiLieuQuery(context.Request));
                    treeViewItems = oDataLoaiTaiLieu.Select(x => new TreeViewItem()
                    {
                        key = x.DMDescription,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;

                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oLoaiTaiLieuDA.GetListJson(new LoaiTaiLieuQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLoaiTaiLieuItem = oLoaiTaiLieuDA.GetByIdToObject<LoaiTaiLieuItem>(oGridRequest.ItemID);
                        oLoaiTaiLieuItem.UpdateObject(context.Request);
                        if (oLoaiTaiLieuDA.CheckExit(oGridRequest.ItemID, oLoaiTaiLieuItem.Title) > 0)
                        {
                            oResult = new ResultAction() { State = ActionState.Error, Message = "Trùng tên loại tài liệu" };
                            break;
                        }
                        oLoaiTaiLieuDA.UpdateObject<LoaiTaiLieuItem>(oLoaiTaiLieuItem);
                        AddLog("UPDATE", "LoaiTaiLieu", "UPDATE", oGridRequest.ItemID, $"Cập nhật loại tài liệu{oLoaiTaiLieuItem.Title}");
                    }
                    else
                    {
                        oLoaiTaiLieuItem.UpdateObject(context.Request);
                        if (oLoaiTaiLieuDA.CheckExit(oGridRequest.ItemID, oLoaiTaiLieuItem.Title) > 0)
                        {
                            oResult = new ResultAction() { State = ActionState.Error, Message = "Trùng tên loại tài liệu" };
                            break;
                        }
                        oLoaiTaiLieuDA.UpdateObject<LoaiTaiLieuItem>(oLoaiTaiLieuItem);
                        AddLog("UPDATE", "LoaiTaiLieu", "ADD", oGridRequest.ItemID, $"Thêm mới loại tài liệu {oLoaiTaiLieuItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oLoaiTaiLieuDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "LoaiTaiLieu", "DELETE", oGridRequest.ItemID, $"Xóa loại tài liệu {outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    AddLog("DELETE-MULTI", "LoaiTaiLieu", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều loại tài liệu");
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oLoaiTaiLieuDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oLoaiTaiLieuDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "LoaiTaiLieu", "APPROVED", oGridRequest.ItemID, $"Duyệt loại tài liệu{oLoaiTaiLieuDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oLoaiTaiLieuDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "LoaiTaiLieu", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt loại tài liệu {oLoaiTaiLieuDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLoaiTaiLieuItem = oLoaiTaiLieuDA.GetByIdToObjectSelectFields<LoaiTaiLieuItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oLoaiTaiLieuItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}