﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.DMTaiLieu;

namespace VIPortal.APP.UserControls.AdminCMS.DMTaiLieu
{
    public partial class pFormDMTaiLieu : pFormBase
    {
        public DMTaiLieuItem oDMTaiLieu { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oDMTaiLieu = new DMTaiLieuItem();
            DMTaiLieuDA oDMTaiLieuDA = new DMTaiLieuDA(UrlSite);
            if (ItemID > 0)
            {               
                oDMTaiLieu = oDMTaiLieuDA.GetByIdToObject<DMTaiLieuItem>(ItemID);
            }
            else
            {
                oDMTaiLieu.DMSTT = oDMTaiLieuDA.GetLastSTT("DMSTT");
            }
        }
    }
}