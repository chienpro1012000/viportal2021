﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.DMTaiLieu;

namespace VIPortal.APP.UserControls.AdminCMS.DMTaiLieu
{
    public partial class pViewDMTaiLieu : pFormBase
    {
        public DMTaiLieuItem oDMTaiLieu = new DMTaiLieuItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oDMTaiLieu = new DMTaiLieuItem();
            if (ItemID > 0)
            {
                DMTaiLieuDA oDMTaiLieuDA = new DMTaiLieuDA(UrlSite);
                oDMTaiLieu = oDMTaiLieuDA.GetByIdToObject<DMTaiLieuItem>(ItemID);

            }
        }
    }
}