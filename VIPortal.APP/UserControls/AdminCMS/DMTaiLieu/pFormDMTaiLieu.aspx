﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormDMTaiLieu.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.DMTaiLieu.pFormDMTaiLieu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-DMTaiLieu" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oDMTaiLieu.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên danh mục</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" placeholder="  Nhập tên danh mục" class="form-control" value="<%=oDMTaiLieu.Title%>" />
            </div>
        </div>
       <div class="form-group row">
            <label for="MaSV" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <textarea name="DMMoTa" id="DMMoTa" class="form-control" placeholder="Nhập mô tả"><%:oDMTaiLieu.DMMoTa%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="MaSV" class="col-sm-2 control-label">Viết tắt</label>
            <div class="col-sm-10">
                <input type="text" name="DMVietTat" id="DMVietTat" placeholder="Nhập tên viết tắt" value="<%:oDMTaiLieu.DMVietTat%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#KSBoChuDe").smSelect2018V2({
                dropdownParent: "#frm-DMTaiLieu"
            });
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oDMTaiLieu.ListFileAttach)%>'
            });
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $(".form-group select").select2({
                dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
            });

            $("#frm-DMTaiLieu").validate({
                rules: {
                    Title: "required",
                    DMSTT: {
                        numberchar: true, required:true
                    },
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề danh mục",
                    
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/DMTaiLieu/pAction.ashx", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-DMTaiLieu").trigger("click");
                            $(form).closeModal();
                            $('#tblDMTaiLieu').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-DMTaiLieu").viForm();
        });
    </script>
</body>
</html>

