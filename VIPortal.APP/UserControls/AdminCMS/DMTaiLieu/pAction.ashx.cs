﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData.DMTaiLieu;
using ViPortalData.VanBanTaiLieu;

namespace VIPortal.APP.UserControls.AdminCMS.DMTaiLieu
{

    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);
            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            DMTaiLieuDA oDMTaiLieuDA = new DMTaiLieuDA(UrlSite);
            DMTaiLieuItem oDMTaiLieuItem = new DMTaiLieuItem();
            VanBanTaiLieuDA oVanBanTaiLieuDA = new VanBanTaiLieuDA(UrlSite);
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    var oData = oDMTaiLieuDA.GetListJson(new DMTaiLieuQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oDMTaiLieuDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "DMLinhVuc", "QUERYDATA", 0, "Xem danh sách danh mục lĩnh vực");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataDMTaiLieu = oDMTaiLieuDA.GetListJson(new DMTaiLieuQuery(context.Request));
                    treeViewItems = oDataDMTaiLieu.Select(x => new TreeViewItem()
                    {
                        key = x.DMDescription,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;

                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oDMTaiLieuDA.GetListJson(new DMTaiLieuQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDMTaiLieuItem = oDMTaiLieuDA.GetByIdToObject<DMTaiLieuItem>(oGridRequest.ItemID);
                        oDMTaiLieuItem.UpdateObject(context.Request);
                        if (oDMTaiLieuDA.CheckExit(oGridRequest.ItemID, oDMTaiLieuItem.Title) == 0)
                        {
                            oDMTaiLieuDA.UpdateObject<DMTaiLieuItem>(oDMTaiLieuItem);
                            AddLog("UPDATE", "DMTaiLieu", "UPDATE", oGridRequest.ItemID, $"Cập nhật danh mục tài liệu {oDMTaiLieuItem.Title}");
                            oResult.Message = "Cập nhật thành công";
                        }
                        else
                        {
                            oResult = new ResultAction() { State = ActionState.Error, Message = "Tên danh mục tài liệu đã tồn tại" };
                        }
                    }
                    else
                    {
                        oDMTaiLieuItem.UpdateObject(context.Request);
                        if (oDMTaiLieuDA.CheckExit(oGridRequest.ItemID, oDMTaiLieuItem.Title) == 0)
                        {
                            oDMTaiLieuDA.UpdateObject<DMTaiLieuItem>(oDMTaiLieuItem);
                            AddLog("UPDATE", "DMTaiLieu", "UPDATE", oGridRequest.ItemID, $"Cập nhật danh mục tài liệu {oDMTaiLieuItem.Title}");
                            oResult.Message = "Cập nhật thành công";
                        }
                        else
                        {
                            oResult = new ResultAction() { State = ActionState.Error, Message = "Tên danh mục tài liệu đã tồn tại" };
                        }
                    }
                    break;
                case "DELETE":
                    if (oVanBanTaiLieuDA.GetListJson(new VanBanTaiLieuQuery() { DMLoaiTaiLieu = oGridRequest.ItemID, _ModerationStatus = 1 }).Count  > 0)
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Danh mục chứa văn bản đang họat động";
                    }
                    else
                    {
                        var outb = oDMTaiLieuDA.DeleteObjectV2(oGridRequest.ItemID);
                        AddLog("DELETE", "DMLinhVuc", "DELETE", oGridRequest.ItemID, $"Xóa danh mục lĩnh vực{outb.Message}");
                        if (outb.State == ActionState.Succeed)
                            oResult.Message = "Xóa thành công";
                    }
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oDMTaiLieuDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    AddLog("DELETE-MULTI", "DMTaiLieu", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều danh mục tài liệu");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oDMTaiLieuDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "DMTaiLieu", "APPROVED", oGridRequest.ItemID, $"Duyệt danh mục tài liệu {oDMTaiLieuDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oDMTaiLieuDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "DMTaiLieu", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt danh mục tài liệu {oDMTaiLieuDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDMTaiLieuItem = oDMTaiLieuDA.GetByIdToObjectSelectFields<DMTaiLieuItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oDMTaiLieuItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}