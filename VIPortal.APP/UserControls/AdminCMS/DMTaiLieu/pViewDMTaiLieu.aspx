﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewDMTaiLieu.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.DMTaiLieu.pViewDMTaiLieu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên Danh mục</label>
            <div class="col-sm-10">
                <%=oDMTaiLieu.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="HoTen" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-4">
                <%:(oDMTaiLieu!= null &&  oDMTaiLieu.DMSTT>0)?oDMTaiLieu.DMSTT+"": "" %>
            </div>
            <label class="control-label col-sm-2">Hiển thị</label>
            <div class="col-sm-4">
                <div class="checkbox">
                    <%:oDMTaiLieu.DMHienThi?"Có":"Không" %>
                </div>
            </div>
        </div>
       <div class="form-group row">
            <label for="DMMoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <%:oDMTaiLieu.DMMoTa%>
            </div>
        </div>
        <div class="form-group row">
            <label for="MaSV" class="col-sm-2 control-label">Viết tắt</label>
            <div class="col-sm-10">
                <%:oDMTaiLieu.DMVietTat%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oDMTaiLieu.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oDMTaiLieu.ListFileAttach[i].Url %>"><%=oDMTaiLieu.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%=string.Format("{0:dd-MM-yyyy}", oDMTaiLieu.Created)%>
            </div>
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}",oDMTaiLieu.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
                <%=oDMTaiLieu.Author.LookupValue%>
            </div>
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
                <%=oDMTaiLieu.Editor.LookupValue%>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>