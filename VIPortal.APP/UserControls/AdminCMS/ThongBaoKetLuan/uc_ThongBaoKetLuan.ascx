﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uc_ThongBaoKetLuan.ascx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.ThongBaoKetLuan.uc_ThongBaoKetLuan" %>

<div role="body-data" data-title="thông báo kết luận" data-size="1040" class="content_wp" data-action="/UserControls/AdminCMS/ThongBaoKetLuan/pAction.asp" data-form="/UserControls/AdminCMS/ThongBaoKetLuan/pFormThongBaoKetLuan.aspx" data-view="/UserControls/AdminCMS/ThongBaoKetLuan/pViewThongBaoKetLuan.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="ThongBaoKetLuanSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title" />
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <button class="btn btn-primary act-add" data-per="010202" type="button">Thêm mới</button>
                    </p>
                </div>
            </div>

            <div class="clsgrid table-responsive">

                <table id="tblThongBaoKetLuan" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function () {

        var $tagvalue = $(".content_wp");
        var $tblDanhMucChung = $("#tblThongBaoKetLuan").viDataTable(
            {
                "frmSearch": "ThongBaoKetLuanSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [{
                        "mData": function (o) {
                            return '<a data-id="' + o.ID + '" >' + o.TBSoKyHieu + '</a>';
                        },
                        "name": "TBSoKyHieu", "sTitle": "Số kí hiệu",
                        "sWidth": "180px",
                    },
                    {
                        "mData": function (o) {
                            if (o.Title != null)
                                return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                            else
                                return "";
                        },
                        "name": "Title", "sTitle": "Tiêu đề"
                    },

                    {
                        "mData": function (o) {
                            return '<a data-id="' + o.ID + '">' + (o.TBLanhDaoKetLuan != null ? o.TBLanhDaoKetLuan : "") + '</a>';
                        },
                        "name": "TBLanhDaoKetLuan", "sTitle": "Lãnh đạo kết luận",
                        "sWidth": "150px",
                    },
                    {
                        "mData": function (o) {
                            return '<a data-id="' + o.ID + '" >' + formatDate(o.TBNgayDang) + '</a>';
                        },
                        "name": "TBNgayDang", "sTitle": "Ngày đăng",
                        "sWidth": "80px",
                    }, {
                        "mData": null,
                        "bSortable": false,
                        "className": "all",
                        "sWidth": "18px",
                        "mRender": function (o) {
                            if (o._ModerationStatus == 0)

                                return '<a data-per="010205" title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                            else
                                return '<a data-per="010205" title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "sWidth": "18px",
                        "className": "all",
                        "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" title="Sửa" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                    }, {
                        "mData": null,
                        "bSortable": false,
                        "sWidth": "18px",
                        "className": "all",
                        "mRender": function (o) { return '<a data-id="' + o.ID + '" title="Gửi mail/thông báo" url="/UserControls/AdminCMS/ThongBaoKetLuan/pFormSendMail.aspx" size=800 class="btn default btn-xs purple btnfrm" data-do="SENDMAIL" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fas fa-paper-plane"></i></a>'; }
                    }, {
                        "sTitle": '<a class="btn default btn-xs origan act-delete-multi" data-do="DELETE-MULTI" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                        "mData": null,
                        "bSortable": false,

                        "className": "all",
                        "sWidth": "18px",
                        "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE" title="Xóa"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                    }, {
                        "sTitle": '<input class="checked-all" type="checkbox" />',
                        "mData": null,
                        "bSortable": false,
                        "sWidth": "18px",
                        "className": "all",
                        "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                    }
                    ],
                //"aaSorting": [0, 'asc'],
                "order": [3, 'desc']
            });
    });
</script>

