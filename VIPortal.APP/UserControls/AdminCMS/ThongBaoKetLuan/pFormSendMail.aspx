﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormSendMail.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.ThongBaoKetLuan.pFormSendMail" %>



<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-ThongBaoKetLuan" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oThongBaoKetLuan.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />

        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Thông báo kết luận</label>
            <div class="col-sm-10">
                <input type="text" name="Title" placeholder="" disabled="disabled" id="Title" class="form-control" value="<%=oThongBaoKetLuan.Title%>" />
            </div>
        </div>
        
        <div class="form-group row">
            <label for="TBTrichYeu" class="col-sm-2 control-label">Người nhận</label>
            <div class="col-sm-10">
                <select multiple="multiple" data-selected="" data-url="/UserControls/AdminCMS/LUser/pAction.ashx?do=QUERYDATA" data-place="Chọn người dùng" name="LstMaiLSend" id="LstMaiLSend" class="form-control"></select>
            </div>
        </div>

       
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#LstMaiLSend").smSelect2018V2({
                dropdownParent: "#frm-ThongBaoKetLuan",
                TemplateText: "{Title}({UserEmail})"
            });
            $("#frm-ThongBaoKetLuan").validate({
                rules: {
                    LstMaiLSend: "required",
                    
                },
                messages: {
                    Title: "Vui lòng nhập người nhận"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/ThongBaoKetLuan/pAction.ashx", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-ThongBaoKetLuan").trigger("click");
                            $(form).closeModal();
                            $('#tblThongBaoKetLuan').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-ThongBaoKetLuan").viForm();
        });
    </script>
</body>
</html>

