﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.ThongBaoKetLuan;

namespace VIPortal.APP.UserControls.AdminCMS.ThongBaoKetLuan
{
    public partial class pFormThongBaoKetLuan : pFormBase
    {
        public ThongBaoKetLuanItem oThongBaoKetLuan { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oThongBaoKetLuan = new ThongBaoKetLuanItem();
            if (ItemID > 0)
            {
                ThongBaoKetLuanDA oThongBaoKetLuanDA = new ThongBaoKetLuanDA(UrlSite);
                oThongBaoKetLuan = oThongBaoKetLuanDA.GetByIdToObject<ThongBaoKetLuanItem>(ItemID);
            }
        }
    }
}