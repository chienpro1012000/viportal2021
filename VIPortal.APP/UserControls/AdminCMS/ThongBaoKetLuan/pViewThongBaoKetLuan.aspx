﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewThongBaoKetLuan.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.ThongBaoKetLuan.pViewThongBaoKetLuan" %>



<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <%=oThongBaoKetLuan.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="TBLanhDaoKetLuan" class="col-sm-2 control-label">Lãnh đạo kết luận</label>
            <div class="col-sm-4">
                <%:oThongBaoKetLuan.TBLanhDaoKetLuan%>
            </div>
            <label for="TBSoKyHieu" class="col-sm-2 control-label">Số kí hiệu</label>
            <div class="col-sm-4">
               <%:oThongBaoKetLuan.TBSoKyHieu%>
            </div>
        </div>

        <div class="form-group row">
            <label for="TBNgayDang" class="col-sm-2 control-label">Ngày đăng</label>
            <div class="col-sm-4">
                <%:string.Format("{0:dd-MM-yyyy}",oThongBaoKetLuan.TBNgayDang)%>           
            </div>
             <label for="TBNgayRaThongBao" class="col-sm-2 control-label">Ngày ra thông báo</label>
            <div class="col-sm-4">
                <%:string.Format("{0:dd-MM-yyyy}",oThongBaoKetLuan.TBNgayRaThongBao)%>
               
            </div>
        </div>
         <div class="form-group row">
            
        </div>  
         <div class="form-group row">
            <label for="TBTrichYeu" class="col-sm-2 control-label">Trích yếu</label>

            <div class="col-sm-10">
               <%:oThongBaoKetLuan.TBTrichYeu%>
            </div>
        </div> 
         
        <div class="form-group row">
	        <label for="TBNoiDung" class="col-sm-2 control-label">Nội dung</label>
	        <div class="col-sm-10">
		       <%=oThongBaoKetLuan.TBNoiDung%>
	        </div>
        </div>
         <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oThongBaoKetLuan.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oThongBaoKetLuan.ListFileAttach[i].Url %>"><%=oThongBaoKetLuan.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%=string.Format("{0:dd-MM-yyyy}", oThongBaoKetLuan.Created)%>
            </div>
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}",oThongBaoKetLuan.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
               <%=oThongBaoKetLuan.Author.LookupValue%>
            </div>
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
               <%=oThongBaoKetLuan.Editor.LookupValue%>
            </div>    
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>
