﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using VIPORTAL;
using ViPortal_Utils;
using ViPortal_Utils.Base;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.Lconfig;
using ViPortalData.ThongBaoKetLuan;

namespace VIPortal.APP.UserControls.AdminCMS.ThongBaoKetLuan
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {

        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            ThongBaoKetLuanDA oThongBaoKetLuanDA = new ThongBaoKetLuanDA(UrlSite);
            ThongBaoKetLuanItem oThongBaoKetLuanItem = new ThongBaoKetLuanItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oThongBaoKetLuanDA.GetListJsonSolr(new ThongBaoKetLuanQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oThongBaoKetLuanDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "ThongBaoKetLuan", "QUERYDATA", 0, "Xem danh sách thông báo kết luận");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataThongBaoKetLuan = oThongBaoKetLuanDA.GetListJson(new ThongBaoKetLuanQuery(context.Request));
                    treeViewItems = oDataThongBaoKetLuan.Select(x => new TreeViewItem()
                    {
                        key = x.Title,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;
                case "TICHHOPEOFFICE":

                    oThongBaoKetLuanItem.UpdateObject(context.Request);
                    LconfigDA oLconfigDA = new LconfigDA();
                    string APIEVNDOFFICE = oLconfigDA.GetListJson(new LconfigQuery() { ConfigType = "APIEVNDOFFICE" }).FirstOrDefault()?.ConfigValue;
                    if (string.IsNullOrEmpty(APIEVNDOFFICE))
                        APIEVNDOFFICE = "https://portal.evnhanoi.vn/apigateway";
                    //lấy dữ liệu theo đơn vị.
                    HttpClient client = new HttpClient();
                    var byteArray = Encoding.ASCII.GetBytes("EVNHANOI:Evnhanoi@123");
                    client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
                    HttpResponseMessage response = client.PostAsync(APIEVNDOFFICE + "/DOFFICE_GET_VANBAN", new StringContent(
                        Newtonsoft.Json.JsonConvert.SerializeObject(new
                        {
                            SoVanBan = oThongBaoKetLuanItem.TBSoKyHieu,
                            NgayVanBan = string.Format("{0:dd/MM/yyyy}", oThongBaoKetLuanItem.TBNgayRaThongBao),
                        }), Encoding.UTF8, "application/json")).Result;

                    //HttpResponseMessage response = client.PostAsJsonAsync(APIEVNDanhBa + "/HRMS_DanhBa_NhanVien").Result;
                    response.EnsureSuccessStatusCode();
                    string responseBody = response.Content.ReadAsStringAsync().Result;
                    try
                    {

                        ThongBaoKetLuanRespon oThongBaoKetLuanRespon = Newtonsoft.Json.JsonConvert.DeserializeObject<ThongBaoKetLuanRespon>(responseBody);
                        if(oThongBaoKetLuanRespon.suc)
                        {
                            List<VanBanEVN> lstVB = Newtonsoft.Json.JsonConvert.DeserializeObject<List<VanBanEVN>>(oThongBaoKetLuanRespon.data);
                            if (lstVB.Count > 0)
                            {
                                VanBanEVN oVanBanEVN = lstVB[0];
                                //oVanBanEVN.file_base64 = oThongBaoKetLuanRespon.file_base64;
                                string TypeFile = clsFucUtils.GetFileExtension(oThongBaoKetLuanRespon.file_base64);
                                string FileName = lstVB[0].KY_HIEU.RemoveVietnameseTone() + "." + TypeFile;
                                string fileServer = Guid.NewGuid() + "." + TypeFile;

                                string pathServer = string.Format(@"\{0}\{1}", @"Uploads\ajaxUpload\", fileServer);
                                string  pathFile = string.Format(@"{0}\{1}", HttpContext.Current.Server.MapPath(@"\"), pathServer);
                                //write file ra thư mục upload.
                                byte[] bytes = System.Convert.FromBase64String(oThongBaoKetLuanRespon.file_base64);
                                File.WriteAllBytes(pathFile, bytes);
                                oVanBanEVN.ListFileAttachAdd = new List<UploadFilesResult>()
                                {
                                    new UploadFilesResult()
                                    {
                                        name = FileName,
                                        pathFile = fileServer,
                                        url = pathServer
                                    }
                                };
                                oResult.OData = oVanBanEVN;
                                //write file thư mục upload

                            }
                            else
                            {
                                oResult.State = ActionState.Error;
                                oResult.Message = $"Không tìm thấy thông tin văn bản";
                            }
                        }
                        else
                        {
                            oResult.State = ActionState.Error;
                            oResult.Message = $"Thông báo tích hợp không thành công.  " + oThongBaoKetLuanRespon.msg;
                        }
                       

                    }
                    catch (Exception ex)
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = $"Thông báo tích hợp không thành công.  " + ex.Message;
                    }
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oThongBaoKetLuanDA.GetListJson(new ThongBaoKetLuanQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":

                    if (oGridRequest.ItemID > 0)
                    {
                        oThongBaoKetLuanItem = oThongBaoKetLuanDA.GetByIdToObject<ThongBaoKetLuanItem>(oGridRequest.ItemID);
                        oThongBaoKetLuanItem.UpdateObject(context.Request);
                        oThongBaoKetLuanItem.Title = oThongBaoKetLuanItem.TBTrichYeu;
                        if (oThongBaoKetLuanDA.CheckExit(oGridRequest.ItemID, oThongBaoKetLuanItem.Title) == 0)
                        {
                            oThongBaoKetLuanDA.UpdateObject<ThongBaoKetLuanItem>(oThongBaoKetLuanItem);
                            AddLog("UPDATE", "ThongBaoKetLuan", "UPDATE", oGridRequest.ItemID, $"Cập nhật thông báo kết luận {oThongBaoKetLuanItem.Title}");
                            oResult.Message = "Lưu thành công";
                        }
                        else
                        {
                            oResult.State = ActionState.Error;
                            oResult.Message = $"Thông báo {oThongBaoKetLuanItem.Title} đã tồn tại";
                        }
                    }
                    else
                    {
                        oThongBaoKetLuanItem.UpdateObject(context.Request);
                        oThongBaoKetLuanItem.Title = oThongBaoKetLuanItem.TBTrichYeu;
                        if (oThongBaoKetLuanDA.CheckExit(oGridRequest.ItemID, oThongBaoKetLuanItem.Title) == 0)
                        {
                            oThongBaoKetLuanDA.UpdateObject<ThongBaoKetLuanItem>(oThongBaoKetLuanItem);
                            AddLog("UPDATE", "ThongBaoKetLuan", "ADD", oGridRequest.ItemID, $"Thêm mới thông báo kết luận{oThongBaoKetLuanItem.Title}");
                            oResult.Message = "Lưu thành công";
                        }
                        else
                        {
                            oResult.State = ActionState.Error;
                            oResult.Message = $"Thông báo {oThongBaoKetLuanItem.Title} đã tồn tại";
                        }
                    }
                    break;
                case "DELETE":
                    var outb = oThongBaoKetLuanDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "ThongBaoKetLuan", "DELETE", oGridRequest.ItemID, $"Xóa thông báo kết luận{outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oThongBaoKetLuanDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    AddLog("DELETE-MULTI", "ThongBaoKetLuan", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều thông báo kết luận");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oThongBaoKetLuanDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "ThongBaoKetLuan", "APPROVED", oGridRequest.ItemID, $"Duyệt thông báo kết luận{oThongBaoKetLuanDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oThongBaoKetLuanDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "ThongBaoKetLuan", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt thông báo kết luận{oThongBaoKetLuanDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oThongBaoKetLuanItem = oThongBaoKetLuanDA.GetByIdToObjectSelectFields<ThongBaoKetLuanItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oThongBaoKetLuanItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
                case "SENDMAIL":
                    if (oGridRequest.ItemID > 0)
                    {
                        oThongBaoKetLuanItem = oThongBaoKetLuanDA.GetByIdToObject<ThongBaoKetLuanItem>(oGridRequest.ItemID);
                        oThongBaoKetLuanItem.UpdateObject(context.Request);
                        if (oThongBaoKetLuanItem.LstMaiLSend.Count > 0)
                        {
                            LUserDA oLUserDA = new LUserDA();
                            List<LUserJson> lstUser = oLUserDA.GetListJson(new LUserQuery()
                            {
                                isGetBylistID = true,
                                lstIDget = oThongBaoKetLuanItem.LstMaiLSend.Select(x => x.LookupId).ToList()
                            });
                            List<string> LstEmail = lstUser.Where(y => !string.IsNullOrEmpty(y.UserEmail)).Select(x => x.UserEmail).ToList();

                            LNotificationDA oLNotificationDA = new LNotificationDA(UrlSite);
                            LNotificationItem lNotificationItem = new LNotificationItem();
                            lNotificationItem.TitleBaiViet = oThongBaoKetLuanItem.Title;
                            lNotificationItem.LNotiNguoiGui = CurentUser.Title;
                            lNotificationItem.LNotiNguoiNhan = string.Join(";#", lstUser.Select(x => x.ID + ";#" + x.Title));
                            lNotificationItem.LNotiNguoiNhan_Id = "," + string.Join(",", new SPFieldLookupValueCollection(lNotificationItem.LNotiNguoiNhan).Select(x => x.LookupId)) + ",";
                            lNotificationItem.LNotiChuaXem_Id = lNotificationItem.LNotiNguoiNhan_Id;
                            lNotificationItem.DoiTuongTitle = "Thông báo kết luận";
                            lNotificationItem.LNotiData = Newtonsoft.Json.JsonConvert.SerializeObject(oThongBaoKetLuanItem);
                            lNotificationItem.Title = $"Thông báo kết luận: {lNotificationItem.TitleBaiViet} mới";
                            lNotificationItem.LNotiMoTa = oThongBaoKetLuanItem.Title;
                            lNotificationItem.LNotiSentTime = DateTime.Now;
                            lNotificationItem.UrlLink = $"/Pages/ketluangiaoban.aspx?ItemID={oGridRequest.ItemID}";
                            oLNotificationDA.UpdateObject<LNotificationItem>(lNotificationItem);
                        }
                        oResult.Message = "Gửi thông báo thành công";
                    }
                    break;
            }
            oResult.ResponseData();

        }

    }
}