﻿<%@ page language="C#" autoeventwireup="true" codebehind="pFormThongBaoKetLuan.aspx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.ThongBaoKetLuan.pFormThongBaoKetLuan" %>




<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-ThongBaoKetLuan" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oThongBaoKetLuan.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />

        <div class="form-group row" style="display:none;">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" placeholder="Nhập vào tiêu đề (bắt buộc)" id="Title" class="form-control" value="<%=oThongBaoKetLuan.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="TBLanhDaoKetLuan" class="col-sm-2 control-label">Lãnh đạo kết luận</label>
            <div class="col-sm-4">
                <input type="text" name="TBLanhDaoKetLuan" id="TBLanhDaoKetLuan" value="<%:oThongBaoKetLuan.TBLanhDaoKetLuan%>" class="form-control" />
            </div>
            <label for="TBSoKyHieu" class="col-sm-2 control-label">Số kí hiệu</label>

            <div class="col-sm-4">
                <input type="text" name="TBSoKyHieu" id="TBSoKyHieu" value="<%:oThongBaoKetLuan.TBSoKyHieu%>" class="form-control" />
            </div>
        </div>

        <div class="form-group row">
            <label for="TBNgayDang" class="col-sm-2 control-label">Ngày đăng</label>
            <div class="col-sm-4">
                <input type="text" name="TBNgayDang" id="TBNgayDang" value="<%:string.Format("{0:dd/MM/yyyy}",oThongBaoKetLuan.TBNgayDang)%>" class="form-control input-datetime" />
            </div>
            <label for="TBNgayRaThongBao" class="col-sm-2 control-label">Ngày ra thông báo</label>
            <div class="col-sm-4">
                <input type="text" name="TBNgayRaThongBao" id="TBNgayRaThongBao" value="<%:string.Format("{0:dd/MM/yyyy}",oThongBaoKetLuan.TBNgayRaThongBao)%>" class="form-control input-datetime" />

            </div>
        </div>
        <div class="form-group row">
            <label for="TBTrichYeu" class="col-sm-2 control-label">Trích yếu</label>

            <div class="col-sm-10">
                <textarea name="TBTrichYeu" class="form-control" id="TBTrichYeu"><%=oThongBaoKetLuan.TBTrichYeu%></textarea>
            </div>
        </div>

        <div class="form-group row">
            <label for="TBNoiDung" class="col-sm-2 control-label">Nội dung</label>
            <div class="col-sm-10">
                <textarea name="TBNoiDung" id="TBNoiDung" class="form-control"><%=oThongBaoKetLuan.TBNoiDung%></textarea>
            </div>
        </div>


        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-10">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {

            CKEDITOR.replace("TBNoiDung", {
                height: 200
            });
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oThongBaoKetLuan.ListFileAttach)%>'
            });
            $("#TBSoKyHieu").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            //TBNgayRaThongBao
            $("#TBNgayRaThongBao").change(function () {
                //alert("Handler for .change() called.");
                $.post("/UserControls/AdminCMS/ThongBaoKetLuan/pAction.asp", { "do": "TICHHOPEOFFICE", "TBNgayRaThongBao": $("#TBNgayRaThongBao").val(), "TBSoKyHieu": $("#TBSoKyHieu").val(), }, function (result) {
                    if (result.State == 2) {
                        BootstrapDialog.show({
                            title: "Lỗi",
                            message: result.Message
                        });
                    }
                    else {
                        $("#TBLanhDaoKetLuan").val(result.NGUOI_KY);
                        $("#TBTrichYeu").val(result.TRICH_YEU);
                        var listFileValue = [];
                        var $listFile = $("#listValueFileAttach");

                        var valueListFile = $listFile.attr('value');
                        if (valueListFile != '' && valueListFile != null) {
                            listFileValue = JSON.parse(valueListFile);
                        }
                        result.ListFileAttachAdd.forEach(function (file, index) {
                            //console.log(item, index);
                            var row = $('<tr class="template-download">' +
                                '<td class="box-upload-button fileupload-buttonbar"><button type="button" class="btn btn-danger btn-xs delete"><i class="fa fa-times" aria-hidden="true"></i></button></td>' +
                                '<td><p class="name"></p>' +
                                '</td>' +
                                '</tr>');
                            var fileAttach = { FileServer: file.pathFile, FileName: file.name };
                            listFileValue.push(fileAttach);
                            row.data("filesv", file.pathFile);
                            row.find('.name').append($('<a></a>').text(file.name));
                            row.find('a')
                                .attr('data-gallery', '')
                                .prop('href', file.url);
                            row.find('button.delete')
                                .attr('data-type', file.delete_type)
                                .attr('data-url', file.delete_url)
                                .click(function () {
                                    console.log(file);
                                    var valueListFileRemove = $listFile.attr('value');
                                    if (valueListFileRemove != '' && valueListFileRemove != null) {
                                        var fileJSON = JSON.parse(valueListFileRemove);
                                        fileJSON = findAndRemoveJSON(fileJSON, "FileServer", file.pathFile);
                                        $listFile.attr('value', JSON.stringify(fileJSON));
                                    }
                                });
                            $("table.uploadfiles").append(row);
                        });
                        $listFile.attr('value', JSON.stringify(listFileValue));
                        //$("#TBTrichYeu").val(result.TRICH_YEU);

                    }
                }).always(function () { });
            });

            $("#frm-ThongBaoKetLuan").validate({
                rules: {
                    //Title: "required",
                    TBSoKyHieu: "required",
                    TBNgayDang: "required",
                    TBNgayRaThongBao: "required",
                },
                messages: {
                    //Title: "Vui lòng nhập tiêu đề",
                    TBSoKyHieu: "Vui lòng nhập số ký hiệu",
                    TBNgayDang: "Vui lòng nhập ngày đăng thông báo",
                    TBNgayRaThongBao: "Vui lòng nhập ngày ra thông báo"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/ThongBaoKetLuan/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-ThongBaoKetLuan").trigger("click");
                            $(form).closeModal();
                            $('#tblThongBaoKetLuan').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-ThongBaoKetLuan").viForm();
        });
    </script>
</body>
</html>

