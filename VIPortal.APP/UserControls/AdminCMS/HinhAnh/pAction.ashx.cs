using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData;
using ViPortalData.DMHinhAnh;

namespace VIPortalAPP.UserControls.AdminCMS.HinhAnh
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public int NewID { get; set; }
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);
            if (!string.IsNullOrEmpty(context.Request["doaction"]))
                doAction = context.Request["doaction"].ToUpper();

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            HinhAnhDA oHinhAnhDA = new HinhAnhDA(UrlSite);
            HinhAnhItem oHinhAnhItem = new HinhAnhItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    var oData = oHinhAnhDA.GetListJsonSolr(new HinhAnhQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw, 
                        oHinhAnhDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "HinhAnh", "QUERYDATA", 0, "Xem danh sách hình ảnh");
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oHinhAnhDA.GetListJson(new HinhAnhQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oHinhAnhItem = oHinhAnhDA.GetByIdToObject<HinhAnhItem>(oGridRequest.ItemID);
                        oHinhAnhItem.UpdateObject(context.Request);
                        oHinhAnhDA.UpdateObject<HinhAnhItem>(oHinhAnhItem);
                        
                        //thiếu trường hợp update mà đổi dm anhe.
                        AddLog("UPDATE", "HinhAnh", "UPDATE", oGridRequest.ItemID, $"Cập nhật hình ảnh {oHinhAnhItem.Title}");
                    }
                    else
                    {
                        oHinhAnhItem.UpdateObject(context.Request);
                        oHinhAnhDA.UpdateObject<HinhAnhItem>(oHinhAnhItem);
                        NewID = oHinhAnhItem.ID;
                        //update tăng số của dmhinhf ảnh lên ở đây.
                        if (oHinhAnhItem.Album.LookupId > 0)
                        {
                            DMHinhAnhDA oDMHinhAnhDA = new DMHinhAnhDA(UrlSite);
                            DMHinhAnhItem oDMHinhAnhItem = oDMHinhAnhDA.GetByIdToObject<DMHinhAnhItem>(oHinhAnhItem.Album.LookupId);
                            oDMHinhAnhDA.SystemUpdateOneField(oDMHinhAnhItem.ID, "DMSLAnh", oDMHinhAnhItem.DMSLAnh + 1);
                        }
                        AddLog("UPDATE", "HinhAnh", "ADD", oGridRequest.ItemID, $"Thêm mới hình ảnh {oHinhAnhItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    oResult.ID = NewID;
                    break;
                case "DELETE":
                    //trương khi delete sẽ phải giảm số lượng ảnh.
                    //update tăng số của dmhinhf ảnh lên ở đây.
                    oHinhAnhItem = oHinhAnhDA.GetByIdToObject<HinhAnhItem>(oGridRequest.ItemID);
                    if (oHinhAnhItem.Album.LookupId > 0)
                    {
                        DMHinhAnhDA oDMHinhAnhDA2 = new DMHinhAnhDA(UrlSite);
                        DMHinhAnhItem oDMHinhAnhItem2 = oDMHinhAnhDA2.GetByIdToObject<DMHinhAnhItem>(oHinhAnhItem.Album.LookupId);
                        oDMHinhAnhDA2.SystemUpdateOneField(oDMHinhAnhItem2.ID, "DMSLAnh", oDMHinhAnhItem2.DMSLAnh - 1);
                    }
                    var outb = oHinhAnhDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "oHinhAnhDA", "DELETE", oGridRequest.ItemID, $"Xóa hình ảnh {outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    AddLog("DELETE-MULTI", "HinhAnh", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều hình ảnh");
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oHinhAnhDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oHinhAnhDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "HinhAnh", "APPROVED", oGridRequest.ItemID, $"Duyệt hình ảnh {oHinhAnhDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                     oHinhAnhDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "HinhAnh", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt hình ảnh {oHinhAnhDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oHinhAnhItem = oHinhAnhDA.GetByIdToObjectSelectFields<HinhAnhItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oHinhAnhItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}