<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormHinhAnh.aspx.cs" Inherits="VIPortalAPP.UserControls.AdminCMS.HinhAnh.pFormHinhAnh" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
    
</head>
<body>
    <form id="frm-HinhAnh" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oHinhAnh.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên ảnh</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" placeholder="Nhập tên ảnh" class="form-control" value="<%=oHinhAnh.Title%>" />
            </div>
        </div>
        <%--<div class="form-group row">
            <label for="STT" class="col-sm-2 control-label">Số thứ tự</label>
            <div class="col-sm-4">
                <input type="number" name="STT" id="STT" placeholder="Nhập số thứ tự" value="<%:(oHinhAnh!= null &&  oHinhAnh.STT>0)?oHinhAnh.STT+"": "" %>" min="0" oninput="validity.valid||(value='');" class="form-control" />
            </div>
        </div>--%>
        <div class="form-group row">
            <label for="DMMoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <textarea name="MoTa" id="MoTa" placeholder="Nhập mô tả" class="form-control"><%:oHinhAnh.MoTa%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="ImageNews" class="col-sm-2 control-label">Hình ảnh</label>
            <div class="col-sm-10" data-role="groupImageDD">

                <input type="hidden" name="ImageNews" id="ImageNewsAlbum" value="<%:oHinhAnh.ImageNews%>" />
                <img title="Ảnh đại diện" id="srcImageNewsAlbum" src="<%=oHinhAnh.ImageNews%>" />
                <button id="btnImageNewsAlbum" type="button" class="btn  btn-success">Hình ảnh</button>
                <button id="btnXoaImageNews1" type="button" class="btn btn-danger">Xóa ảnh</button>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#Album").smSelect2018V2({
                dropdownParent: "#frm-HinhAnh"
            });
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            
            
            $("#btnImageNewsAlbum").click(function () {
                $elementId = $(this);
                CKFinder.popup({
                    chooseFiles: true,
                    width: 800,
                    height: 600,
                    onInit: function (finder) {
                        finder.on('files:choose', function (evt) {
                            var file = evt.data.files.first();
                            var urlfileimg = file.getUrl();
                            $("#frm-HinhAnh #ImageNewsAlbum").val(urlfileimg);
                            $("#frm-HinhAnh #srcImageNewsAlbum").attr("src", urlfileimg);
                        });
                        finder.on('file:choose:resizedImage', function (evt) {
                            //var output = document.getElementById(elementId);
                            console.log(evt.data);
                            var urlfileimg = evt.data.resizedUrl;
                            $("#frm-HinhAnh #ImageNewsAlbum").val(urlfileimg);
                            $("#frm-HinhAnh #srcImageNewsAlbum").attr("src", urlfileimg);
                        });
                    }
                });
            });
            $("#btnXoaImageNews1").click(function () {
                //document.getElementById("ImageNews").value = "";
                //document.getElementById("srcImageNewsAlbum").setAttribute("src", "");
                $("#frm-HinhAnh #ImageNewsAlbum").val("");
                $("#frm-HinhAnh #srcImageNewsAlbum").attr("src", "");
            });
            $("#frm-HinhAnh").validate({
                ignore: [],
                rules: {
                    Title: "required",
                    ImageNewsAlbum: "required",
                    ImageNews: "required"
                },
                messages: {
                    Title: "Vui lòng nhập Tên ảnh",
                    ImageNews: "Vui lòng chọn ảnh"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/HinhAnh/pAction.ashx", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            debugger;
                            var lstID = $("#IdNewsSave").val();
                            if (lstID != "")
                                lstID += "," + result.ID;
                            else
                                lstID = result.ID;
                            $("#IdNews").val(lstID);
                            $("#IdNewsSave").val(lstID);
                            showMsg(result.Message);
                            $(form).closeModal();
                            $('#tblHinhAnh2').DataTable().ajax.reload();
                            $(window).trigger('resize');
                        }
                    }).always(function () { });
                }
            });
            $("#frm-HinhAnh").viForm();
        });
    </script>
</body>
</html>
