using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortalAPP.UserControls.AdminCMS.HinhAnh
{
    public partial class pFormHinhAnh : pFormBase
    {
        public HinhAnhItem oHinhAnh {get;set;}
        protected void Page_Load(object sender, EventArgs e)
        {
			oHinhAnh = new HinhAnhItem();
            if (ItemID > 0)
            {
                HinhAnhDA oHinhAnhDA = new HinhAnhDA(UrlSite);
                oHinhAnh = oHinhAnhDA.GetByIdToObject<HinhAnhItem>(ItemID);
            }
        }
    }
}