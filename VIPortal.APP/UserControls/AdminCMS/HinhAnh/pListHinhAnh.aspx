﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pListHinhAnh.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.pListHinhAnh" %>

<style>
    .clsmanager1 {
        /*display:none !important;*/
    }

    #tblHinhAnh_length {
        display: none !important;
    }

    #tblHinhAnh_info {
        display: none !important;
    }

    #tblHinhAnh_paginate {
        display: none !important;
    }

    .content_wp_hinhanh {
        width: 100%;
    }
</style>
<div id="frm-hinhanh" role="body-data" data-title="" class="content_wp_hinhanh" data-action="/UserControls/AdminCMS/HinhAnh/pAction.asp" data-form="/UserControls/AdminCMS/HinhAnh/pFormHinhAnh.aspx">
    <div class="clsmanager1 row">
        <div class="col-sm-9">
            <div id="HinhAnhSearch" class="form-inline zonesearch">
                <div class="form-group">
                    <h3 style="color: #007BFF;">Danh sách hình ảnh</h3>
                    <input type="hidden" name="do" id="do" value="QUERYDATA" />
                    <input type="hidden" name="lstIdGet" id="IdNewsSave" value="<%=lstIdGet %>" />
                    <input type="hidden" name="IdNewsRemove" id="IdNewsRemove" value="" />
                    <input type="hidden" name="isGetBylistID" id="isGetBylistID" value="True" />
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <p class="text-right">
                <button class="btn btn-primary act-add" type="button">Thêm mới</button>
            </p>
        </div>
    </div>

    <div class="clsgrid table-responsive">

        <table id="tblHinhAnh2" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
        </table>

    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var $tagvalue = $(".content_wp_hinhanh");
        var $tblDanhMucChung = $("#tblHinhAnh2").viDataTable(
            {
                "frmSearch": "HinhAnhSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                            },
                            "name": "Title", "sWidth": "300px", "sTitle": "Tiêu đề"
                        }, {
                            "mData": function (o) {
                                if (o.ImageNews != null)
                                    return ' <img  data-id="' + o.ID + '" class="act-view" style="width:100px;height:60px"  src="' + o.ImageNews + '" alt="ImageNews" />'
                                else
                                    return ''
                            },
                            "sWidth": "110px",
                            "name": "ImageNews", "sTitle": "Ảnh"
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {

                                if (o._ModerationStatus == 0)

                                    return '<a title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }

                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        }, {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-deletefix" data-do="DELETE" title="Xóa"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                "aaSorting": [0, 'asc']
            });
        $tblDanhMucChung.on('draw', function () {
            $(".act-deletefix").click(function () {
                var $ItemID = $(this).attr("data-id");
                var IdNewsRemove = $("#IdNewsRemove").val();
                IdNewsRemove += `,${$ItemID}`;
                $("#IdNewsRemove").val(IdNewsRemove);
                var myArr = $("#IdNewsSave").val().split(",");
                myArr = jQuery.grep(myArr, function (a) {
                    return a !== $ItemID;
                });
                $("#IdNewsSave").val(myArr.join(","));
                $("#IdNews").val(myArr.join(","));
                $('#tblHinhAnh2').DataTable().ajax.reload();
                
            });
        });
    });
</script>

