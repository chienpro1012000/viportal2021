<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormHinhAnhOnly.aspx.cs" Inherits="VIPortalAPP.UserControls.AdminCMS.HinhAnh.pFormHinhAnhOnly" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
    
</head>
<body>
    <form id="frm-HinhAnh" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oHinhAnh.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên ảnh</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" placeholder="Nhập tên ảnh" class="form-control" value="<%=oHinhAnh.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="Album" class="col-sm-2 control-label">Danh mục hình ảnh</label>
            <div class="col-sm-10">
                <select data-selected="<%:oHinhAnh.Album.LookupId%>" data-url="/UserControls/AdminCMS/DMHinhAnh/pAction.asp?do=QUERYDATA&moder=1" data-place="Chọn danh mục" name="Album" id="Album" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <label for="STT" class="col-sm-2 control-label">Số thứ tự</label>
            <div class="col-sm-4">
                <input type="number" name="STT" id="STT" placeholder="Nhập số thứ tự" value="<%:(oHinhAnh!= null &&  oHinhAnh.STT>0)?oHinhAnh.STT+"": "" %>" min="0" oninput="validity.valid||(value='');" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="DMMoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <textarea name="MoTa" id="MoTa" placeholder="Nhập mô tả" class="form-control"><%:oHinhAnh.MoTa%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="ImageNews" class="col-sm-2 control-label">Hình ảnh</label>
            <div class="col-sm-10" data-role="groupImageDD">

                <input type="hidden" name="ImageNews" id="ImageNewsAlbum" value="<%:oHinhAnh.ImageNews%>" />
                <img title="Ảnh đại diện" id="srcImageNewsAlbum" src="<%=oHinhAnh.ImageNews%>" />
                <button id="btnImageNewsAlbum" type="button" class="btn  btn-success">Hình ảnh</button>
                <button id="btnXoaImageNews" type="button" class="btn btn-danger">Xóa ảnh</button>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#btnXoaImageNews").click(function () {
                $("#ImageNewsAlbum").val("");
                $("#srcImageNewsAlbum").attr("src", "");
            });
            $("#Album").smSelect2018V2({
                dropdownParent: "#frm-HinhAnh"
            });
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $(".form-group select").select2({
                dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
            });
            $("#btnImageNewsAlbum").click(function () {
                $elementId = $(this);
                CKFinder.popup({
                    chooseFiles: true,
                    width: 800,
                    height: 600,
                    onInit: function (finder) {
                        finder.on('files:choose', function (evt) {
                            var file = evt.data.files.first();
                            var urlfileimg = file.getUrl();
                            document.getElementById("ImageNewsAlbum").value = urlfileimg;
                            document.getElementById("srcImageNewsAlbum").setAttribute("src", urlfileimg);
                        });
                        finder.on('file:choose:resizedImage', function (evt) {
                            //var output = document.getElementById(elementId);
                            console.log(evt.data);
                            var urlfileimg = evt.data.resizedUrl;
                            document.getElementById("ImageNewsAlbum").value = urlfileimg;
                            document.getElementById("srcImageNewsAlbum").setAttribute("src", urlfileimg);
                        });
                    }
                });
            });
            $("#frm-HinhAnh").validate({
                ignore: [],
                rules: {
                    Title: "required",
                    Album: "required",
                    ImageNewsAlbum: "required",
                    ImageNews: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tên ảnh",
                    ImageNews: "Vui lòng chọn ảnh"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/HinhAnh/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            $(form).closeModal();
                            $('#tblHinhAnh').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-HinhAnh").viForm();
        });
    </script>
</body>
</html>
