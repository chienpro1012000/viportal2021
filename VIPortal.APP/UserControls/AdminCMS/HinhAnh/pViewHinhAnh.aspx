<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewHinhAnh.aspx.cs" Inherits="VIPortalAPP.UserControls.AdminCMS.HinhAnh.pViewHinhAnh" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <%=oHinhAnh.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="Album" class="col-sm-2 control-label">Album ảnh</label>
            <div class="col-sm-10">
                <%=oHinhAnh.Album.LookupValue%>
            </div>
        </div>
        <div class="form-group row">
            <label for="STT" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-10">
                <%=oHinhAnh.STT%>
            </div>
        </div>
        <div class="form-group row">
            <label for="ImageNews" class="col-sm-2 control-label">Ảnh</label>
            <div class="col-sm-10">
                <img style="max-width: 100%;" src="<%=oHinhAnh.ImageNews%>"/>
            </div>
        </div>
        <div class="form-group row">
            <label for="NgayDang" class="col-sm-2 control-label">Ngày đăng</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy}", oHinhAnh.NgayDang)%>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oHinhAnh.Created)%>
            </div>
            <label class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oHinhAnh.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
                <%=oHinhAnh.Author.LookupValue%>
            </div>
            <label class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
                <%=oHinhAnh.Editor.LookupValue%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oHinhAnh.ListFileAttach.Count;i++){ %>
                <div><a href="<%=oHinhAnh.ListFileAttach[i].Url %>"><%=oHinhAnh.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>
