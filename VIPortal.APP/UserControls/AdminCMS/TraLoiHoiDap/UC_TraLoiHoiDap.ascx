﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_TraLoiHoiDap.ascx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.TraLoiHoiDap.UC_TraLoiHoiDap" %>
<div role="body-data" data-title="câu trả lời" class="content_wp" data-action="/UserControls/AdminCMS/TraLoiHoiDap/pAction.asp" data-form="/UserControls/AdminCMS/TraLoiHoiDap/pFormTraLoiHoiDap.aspx" data-view="/UserControls/AdminCMS/TraLoiHoiDap/pViewTraLoiHoiDap.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="TraLoiHoiDapSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Titles,FullText" />
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                    </p>
                </div>

            </div>

            <div class="clsgrid table-responsive">

                <table id="tblTraLoiHoiDap" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        var $tagvalue = $(".content_wp");
        var $tblDanhMucChung = $("#tblTraLoiHoiDap").viDataTable(
            {
                "frmSearch": "TraLoiHoiDapSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "name": "FAQNgayTraLoi", "sTitle": "Thời gian trả lời",
                            "mData": function (o) {
                                return formatDate(o.FAQNgayTraLoi);
                            },
                            "sWidth": "100px",
                        }, {
                            "mData": function (o) {
                                return o.IDHoiDap.Title;
                            },
                            "sWidth": "170px",
                            "name": "IDHoiDap", "sTitle": "Câu hỏi"
                        },{
                            "mData": function (o) {
                                if (o.Title != null)
                                    return '<a data-Itemid="' + o.ID + '" class="act-view">' + o.Title + '</a>';
                                else
                                    return "";
                            },
                            "name": "Title", "sTitle": "Câu trả lời"
                        },
                        {
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '">' + o.FQNguoiTraLoi.Title + '</a>';
                            },
                            "name": "FQNguoiTraLoi", "sTitle": "Người trả lời",
                            "sWidth": "120px",
                        },
                        
                        {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {
                                if (o._ModerationStatus == 0)

                                    return '<a data-per="01030205" title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a data-per="01030205" title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }
                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" title="Sửa" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        }, {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE" title="Xóa"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                "order": [[0, "desc"]]
            });
    });
</script>
