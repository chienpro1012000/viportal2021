﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.HoiDap;
using ViPortalData.TraLoiHoiDap;

namespace VIPortal.APP.UserControls.AdminCMS.TraLoiHoiDap
{
    public partial class pViewTraLoiHoiDap : pFormBase
    {
        public TraLoiHoiDapItem oTraLoiHoiDap = new TraLoiHoiDapItem();
        public HoiDapItem oHoiDap { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oTraLoiHoiDap = new TraLoiHoiDapItem();
            oHoiDap = new HoiDapItem();
            if (ItemID > 0)
            {
                TraLoiHoiDapDA OTraLoiHoiDapDA = new TraLoiHoiDapDA(UrlSite);
                oTraLoiHoiDap = OTraLoiHoiDapDA.GetByIdToObject<TraLoiHoiDapItem>(ItemID);
                if(oTraLoiHoiDap.IDHoiDap.LookupId > 0)
                {
                    HoiDapDA oHoiDapDA = new HoiDapDA();
                    oHoiDap = oHoiDapDA.GetByIdToObject<HoiDapItem>(oTraLoiHoiDap.IDHoiDap.LookupId);
                }
            }
        }
    }
}