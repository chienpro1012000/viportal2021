﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormTraLoiHoiDap.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.TraLoiHoiDap.pFormTraLoiHoiDap" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-TraLoiHoiDap" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oTraLoiHoiDap.ID %>" />
        <input type="hidden" name="IDHoiDap" id="IDHoiDap" value="<%=oHoiDap.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <%if(oHoiDap.ID > 0){ %>

        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Câu hỏi</label>
            <div class="col-sm-10">
                <%=oHoiDap.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="FAQSTT" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-4">
                <%:(oHoiDap!= null &&  oHoiDap.FAQSTT>0)?oHoiDap.FAQSTT+"": "" %>
            </div>
            <label for="FAQLuotXem" class="col-sm-2 control-label">Lượt xem</label>
            <div class="col-sm-4">
                <div class="">
                    <%=(oHoiDap!= null &&  oHoiDap.FAQLuotXem>0)?oHoiDap.FAQLuotXem+"": "" %>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="FQNguoiHoi" class="col-sm-2 control-label">Người hỏi</label>
            <div class="col-sm-4">
                <%:oHoiDap.FQNguoiHoi.LookupId%>
            </div>
            <label for="FAQNgayHoi" class="col-sm-2 control-label">Ngày hỏi</label>
            <div class="col-sm-4">
                <div class="">
                    <%:string.Format("{0:dd-MM-yyyy}", oHoiDap.FAQNgayHoi)%>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="FAQCauHoi" class="col-sm-2 control-label">Nội dung câu hỏi</label>
            <div class="col-sm-10">
                <%=oHoiDap.FAQCauHoi%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oHoiDap.ListFileAttach.Count;i++){ %>
                <div><a href="<%=oHoiDap.ListFileAttach[i].Url %>"><%=oHoiDap.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <hr />
        <%} %>
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Câu trả lời</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oTraLoiHoiDap.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="FQNguoiTraLoi" class="col-sm-2 control-label">Người trả lời</label>
            <div class="col-sm-4">
                <select data-selected="<%:oTraLoiHoiDap.FQNguoiTraLoi.LookupId%>" data-url="/UserControls/AdminCMS/LUser/pAction.asp?do=QUERYDATA" data-place="Chọn người trả lời" name="FQNguoiTraLoi" id="FQNguoiTraLoi" class="form-control"></select>
            </div>
            <label for="FAQNgayTraLoi" class="col-sm-2 control-label">Ngày trả lời</label>
            <div class="col-sm-4">
                <div class=""> 
                    <input type="date" name="FAQNgayTraLoi" id="FAQNgayTraLoi" value="<%:string.Format("{0:yyyy-MM-dd}", oTraLoiHoiDap.FAQNgayTraLoi)%>" class="form-control" />
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="FAQCauHoi" class="col-sm-2 control-label">Nội dung trả lời</label>
            <div class="col-sm-10">
                <textarea name="FAQNoiDungTraLoi" id="FAQNoiDungTraLoi" class="form-control"><%=oTraLoiHoiDap.FAQNoiDungTraLoi%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-10">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oTraLoiHoiDap.ListFileAttach)%>'
            });
            $("#FQNguoiTraLoi").smSelect2018V2({
                dropdownParent: "#frm-TraLoiHoiDap",
                Ajax: true
            });
            $("#FAQNoiDungTraLoi").viCustomFck();
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                timePicker: true,
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY HH:mm' }
            });

            $("#frm-TraLoiHoiDap").validate({
                rules: {
                    Title: "required",
                    FAQLuotXem: "required",

                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề",
                    FAQLuotXem: "Vui lòng nhập số",
                },

                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/TraLoiHoiDap/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-TraLoiHoiDap").trigger("click");
                            $(form).closeModal();
                            $('#tblTraLoiHoiDap').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-TraLoiHoiDap").viForm();
        });
    </script>
</body>
</html>

