﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.HoiDap;
using ViPortalData.TraLoiHoiDap;

namespace VIPortal.APP.UserControls.AdminCMS.TraLoiHoiDap
{
    public partial class pFormTraLoiHoiDap : pFormBase
    {
        public TraLoiHoiDapItem oTraLoiHoiDap { get; set; }
        public HoiDapItem oHoiDap;
        protected void Page_Load(object sender, EventArgs e)
        {
            oTraLoiHoiDap = new TraLoiHoiDapItem();
            oHoiDap = new HoiDapItem();
            TraLoiHoiDapDA oTraLoiHoiDapDA = new TraLoiHoiDapDA(UrlSite);
            if (ItemID > 0)
            {
                oTraLoiHoiDap = oTraLoiHoiDapDA.GetByIdToObject<TraLoiHoiDapItem>(ItemID);

                HoiDapDA oFQAThuongGapDA = new HoiDapDA(UrlSite, false);
                oHoiDap = oFQAThuongGapDA.GetByIdToObject<HoiDapItem>(oTraLoiHoiDap.IDHoiDap.LookupId);

            }
            else if(!string.IsNullOrEmpty(Request["IDHoiDap"]))
            {
                HoiDapDA oFQAThuongGapDA = new HoiDapDA(UrlSite,false);
                oHoiDap = oFQAThuongGapDA.GetByIdToObject<HoiDapItem>(Convert.ToInt32(Request["IDHoiDap"]));

                oTraLoiHoiDap = oTraLoiHoiDapDA.GetListObj(new TraLoiHoiDapQuery() { IDHoiDap = Convert.ToInt32(Request["IDHoiDap"]) }).FirstOrDefault();
                if (oTraLoiHoiDap == null) oTraLoiHoiDap = new TraLoiHoiDapItem();
            }
        }
    }
}