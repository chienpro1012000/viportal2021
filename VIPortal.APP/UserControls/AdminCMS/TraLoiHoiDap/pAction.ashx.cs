﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData.TraLoiHoiDap;

namespace VIPortal.APP.UserControls.AdminCMS.TraLoiHoiDap
{

    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {

        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);
            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            TraLoiHoiDapDA oTraLoiHoiDapDA = new TraLoiHoiDapDA(UrlSite);
            TraLoiHoiDapItem oTraLoiHoiDapItem = new TraLoiHoiDapItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oTraLoiHoiDapDA.GetListJsonSolr(new TraLoiHoiDapQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oTraLoiHoiDapDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "TraLoiHoiDap", "QUERYDATA", 0, "Xem danh sách trả lời hỏi đáp");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataTraLoiHoiDap = oTraLoiHoiDapDA.GetListJson(new TraLoiHoiDapQuery(context.Request));
                    treeViewItems = oDataTraLoiHoiDap.Select(x => new TreeViewItem()
                    {
                        key = x.FAQNoiDungTraLoi,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;

                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oTraLoiHoiDapDA.GetListJson(new TraLoiHoiDapQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oTraLoiHoiDapItem = oTraLoiHoiDapDA.GetByIdToObject<TraLoiHoiDapItem>(oGridRequest.ItemID);
                        oTraLoiHoiDapItem.UpdateObject(context.Request);
                        oTraLoiHoiDapDA.UpdateObject<TraLoiHoiDapItem>(oTraLoiHoiDapItem);
                        AddLog("UPDATE", "TraLoiHoiDap", "UPDATE", oGridRequest.ItemID, $"Cập nhật trả lời hỏi đáp{oTraLoiHoiDapItem.Title}");
                    }
                    else
                    {
                        oTraLoiHoiDapItem.UpdateObject(context.Request);
                        oTraLoiHoiDapDA.UpdateObject<TraLoiHoiDapItem>(oTraLoiHoiDapItem);
                        AddLog("UPDATE", "TraLoiHoiDap", "ADD", oGridRequest.ItemID, $"Thêm mới trả lời hỏi đáp{oTraLoiHoiDapItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oTraLoiHoiDapDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "TraLoiHoiDap", "DELETE", oGridRequest.ItemID, $"Xóa trả lời hỏi đáp{outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oTraLoiHoiDapDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    AddLog("DELETE-MULTI", "TraLoiHoiDap", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều trả lời hỏi đáp");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oTraLoiHoiDapDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "TraLoiHoiDap", "APPROVED", oGridRequest.ItemID, $"Duyệt trả lời hỏi đáp{oTraLoiHoiDapDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oTraLoiHoiDapDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "TraLoiHoiDap", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt trả lời hỏi đáp{oTraLoiHoiDapDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oTraLoiHoiDapItem = oTraLoiHoiDapDA.GetByIdToObjectSelectFields<TraLoiHoiDapItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oTraLoiHoiDapItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}