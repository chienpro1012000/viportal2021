using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.BinhLuan;

namespace VIPortalAPP.UserControls.AdminCMS.BinhLuan
{
    public partial class pViewBinhLuan : pFormBase
    {
        public BinhLuanItem oBinhLuan = new BinhLuanItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oBinhLuan = new BinhLuanItem();
            if (ItemID > 0)
            {
                BinhLuanDA oBinhLuanDA = new BinhLuanDA(UrlSite);
                oBinhLuan = oBinhLuanDA.GetByIdToObject<BinhLuanItem>(ItemID);

            }
        }
    }
}