using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData.BinhLuan;

namespace VIPortalAPP.BinhLuan
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            BinhLuanDA oBinhLuanDA = new BinhLuanDA(UrlSite);
            BinhLuanItem oBinhLuanItem = new BinhLuanItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oBinhLuanDA.GetListJson(new BinhLuanQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw, 
                        oBinhLuanDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oBinhLuanDA.GetListJson(new BinhLuanQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oBinhLuanItem = oBinhLuanDA.GetByIdToObject<BinhLuanItem>(oGridRequest.ItemID);
                        oBinhLuanItem.UpdateObject(context.Request);
                        oBinhLuanDA.UpdateObject<BinhLuanItem>(oBinhLuanItem);
                    }
                    else
                    {
                        oBinhLuanItem.UpdateObject(context.Request);
                        oBinhLuanDA.UpdateObject<BinhLuanItem>(oBinhLuanItem);
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    string outb = oBinhLuanDA.DeleteObject(oGridRequest.ItemID);
                    if (string.IsNullOrEmpty(outb))
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb;
                    break;
                case "DELETE-MULTI":
                    var outbmulti = "";
                    AddLog("DELETE-MULTI", "BinhLuan", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều bình luận");
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oBinhLuanDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oBinhLuanDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                     oBinhLuanDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oBinhLuanItem = oBinhLuanDA.GetByIdToObjectSelectFields<BinhLuanItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oBinhLuanItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}