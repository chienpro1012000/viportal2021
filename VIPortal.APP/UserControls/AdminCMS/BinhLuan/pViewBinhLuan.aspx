<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewBinhLuan.aspx.cs" Inherits="VIPortalAPP.UserControls.AdminCMS.BinhLuan.pViewBinhLuan" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
               <%=oBinhLuan.Title%>
            </div>
        </div>
		<div class="form-group row">
	<label for="BaiViet" class="col-sm-2 control-label">BaiViet</label>
	<div class="col-sm-10">
		<%=oBinhLuan.BaiViet%>
	</div>
</div>
<div class="form-group row">
	<label for="CreatedUser" class="col-sm-2 control-label">CreatedUser</label>
	<div class="col-sm-10">
		<%=oBinhLuan.CreatedUser.LookupValue%>
	</div>
</div>

		  <div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oBinhLuan.Created)%>
            </div>
              <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oBinhLuan.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
                <%=oBinhLuan.Author.LookupValue%>
            </div>
             <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
                <%=oBinhLuan.Editor.LookupValue%>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>