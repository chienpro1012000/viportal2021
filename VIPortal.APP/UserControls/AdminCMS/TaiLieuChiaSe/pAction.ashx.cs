using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortalAPP.TaiLieuChiaSe
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : IHttpHandler
    {
        public string doAction = string.Empty;
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            if (!string.IsNullOrEmpty(context.Request["do"]))
                doAction = context.Request["do"].ToUpper();

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            TaiLieuChiaSeDA oTaiLieuChiaSeDA = new TaiLieuChiaSeDA();
            TaiLieuChiaSeItem oTaiLieuChiaSeItem = new TaiLieuChiaSeItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oTaiLieuChiaSeDA.GetListJson(new TaiLieuChiaSeQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw, 
                        oTaiLieuChiaSeDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oTaiLieuChiaSeDA.GetListJson(new TaiLieuChiaSeQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oTaiLieuChiaSeItem = oTaiLieuChiaSeDA.GetByIdToObject<TaiLieuChiaSeItem>(oGridRequest.ItemID);
                        oTaiLieuChiaSeItem.UpdateObject(context.Request);
                        oTaiLieuChiaSeDA.UpdateObject<TaiLieuChiaSeItem>(oTaiLieuChiaSeItem);
                    }
                    else
                    {
                        oTaiLieuChiaSeItem.UpdateObject(context.Request);
                        oTaiLieuChiaSeDA.UpdateObject<TaiLieuChiaSeItem>(oTaiLieuChiaSeItem);
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    string outb = oTaiLieuChiaSeDA.DeleteObject(oGridRequest.ItemID);
                    if (string.IsNullOrEmpty(outb))
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb;
                    break;
                case "DELETE-MULTI":
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oTaiLieuChiaSeDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                     oTaiLieuChiaSeDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oTaiLieuChiaSeItem = oTaiLieuChiaSeDA.GetByIdToObjectSelectFields<TaiLieuChiaSeItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oTaiLieuChiaSeItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}