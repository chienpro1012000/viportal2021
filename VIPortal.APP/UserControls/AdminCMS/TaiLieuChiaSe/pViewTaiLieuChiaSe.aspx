<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewTaiLieuChiaSe.aspx.cs" Inherits="VIPortalAPP.pViewTaiLieuChiaSe" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
               <%=oTaiLieuChiaSe.Title%>
            </div>
        </div>
		<div class="form-group">
	<label for="CreatedUser" class="col-sm-2 control-label">CreatedUser</label>
	<div class="col-sm-10">
		<%=oTaiLieuChiaSe.CreatedUser%>
	</div>
</div>
<div class="form-group">
	<label for="fldGroup" class="col-sm-2 control-label">fldGroup</label>
	<div class="col-sm-10">
		<%=oTaiLieuChiaSe.fldGroup%>
	</div>
</div>
<div class="form-group">
	<label for="DMTaiLieu" class="col-sm-2 control-label">DMTaiLieu</label>
	<div class="col-sm-10">
		<%=oTaiLieuChiaSe.DMTaiLieu%>
	</div>
</div>

		  <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-10">
               <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oTaiLieuChiaSe.Created)%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oTaiLieuChiaSe.Modified)%>
            </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-10">
                <%=oTaiLieuChiaSe.Author.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-10">
                <%=oTaiLieuChiaSe.Editor.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oTaiLieuChiaSe.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oTaiLieuChiaSe.ListFileAttach[i].Url %>"><%=oTaiLieuChiaSe.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>