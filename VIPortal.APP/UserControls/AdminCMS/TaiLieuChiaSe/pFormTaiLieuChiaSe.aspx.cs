using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortalAPP
{
    public partial class pFormTaiLieuChiaSe : pFormBase
    {
        public TaiLieuChiaSeItem oTaiLieuChiaSe { get; set; }
        public List<LGroupItem> lstGroupItem { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            lstGroupItem = new List<LGroupItem>();
            oTaiLieuChiaSe = new TaiLieuChiaSeItem();
            if (ItemID > 0)
            {
                TaiLieuChiaSeDA oTaiLieuChiaSeDA = new TaiLieuChiaSeDA();
                oTaiLieuChiaSe = oTaiLieuChiaSeDA.GetByIdToObject<TaiLieuChiaSeItem>(ItemID);

            }
            LGroupDA lGroupDA = new LGroupDA();
            int PhongBanId = CurentUser.UserPhongBan.ID;
            while (PhongBanId > 0)
            {
                LGroupItem oLGroupItem = lGroupDA.GetByIdToObject<LGroupItem>(PhongBanId);
                lstGroupItem.Add(oLGroupItem);
                if (oLGroupItem.GroupParent != null)
                {
                    PhongBanId = oLGroupItem.GroupParent.LookupId;
                }
                else
                {
                    PhongBanId = 0;
                }
            }

        }
    }
}