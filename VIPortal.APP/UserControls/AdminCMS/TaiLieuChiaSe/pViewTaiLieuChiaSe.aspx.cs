using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortalAPP
{
    public partial class pViewTaiLieuChiaSe : pFormBase
    {
        public TaiLieuChiaSeItem oTaiLieuChiaSe = new TaiLieuChiaSeItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oTaiLieuChiaSe = new TaiLieuChiaSeItem();
            if (ItemID > 0)
            {
                TaiLieuChiaSeDA oTaiLieuChiaSeDA = new TaiLieuChiaSeDA();
                oTaiLieuChiaSe = oTaiLieuChiaSeDA.GetByIdToObject<TaiLieuChiaSeItem>(ItemID);

            }
        }
    }
}