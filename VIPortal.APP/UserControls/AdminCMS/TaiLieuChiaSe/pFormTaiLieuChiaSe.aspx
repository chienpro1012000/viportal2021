<%@ page language="C#" autoeventwireup="true" codebehind="pFormTaiLieuChiaSe.aspx.cs" inherits="VIPortalAPP.pFormTaiLieuChiaSe" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-TaiLieuChiaSe" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oTaiLieuChiaSe.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" name="CreatedUser" id="CreatedUser" value="<%=CurentUser.ID %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oTaiLieuChiaSe.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="fldGroup" class="col-sm-2 control-label">Phạm vi chia sẻ</label>
            <div class="col-sm-10">
                <select id="fldGroup" name="fldGroup" class="form-control">
                <%foreach (ViPortalData.LGroupItem item in lstGroupItem)
                    {%>
                        <option value="<%=item.ID %>"><%=item.Title %></option>
                    <%} %>
                    </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="DMTaiLieu" class="col-sm-2 control-label">Loại dữ liệu</label>
            <div class="col-sm-10">
                <select data-selected="<%:oTaiLieuChiaSe.DMTaiLieu.LookupId%>" data-url="/UserControls/AdminCMS/DanhMucChiaSeTL/pAction.asp?do=QUERYDATA" data-place="Chọn Danh mục" name="DMTaiLieu" id="DMTaiLieu" class="form-control"></select>
            </div>
        </div>

        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label" for="FileAttach" >Tệp đính kèm<span class="clslable">(<span class="clsred">*</span>)</span></label>
            <div class="col-xs-12 col-sm-10 col-md-10">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oTaiLieuChiaSe.ListFileAttach)%>'
            });
            $("#fldGroup").val("<%=oTaiLieuChiaSe.fldGroup.LookupId%>");
            $("#Title").focus();
            $("#DMTaiLieu").smSelect2018V2({
                dropdownParent: "#frm-TaiLieuChiaSe"
            });
            $("#fldGroup").select2({
                placeholder: "Chọn đơn vị",
            });
            $("#frm-TaiLieuChiaSe").validate({
                rules: {
                    Title: "required",
                    fldGroup: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề",
                    FileAttach: "Đính kèm tài liệu"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/TaiLieuChiaSe/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-TaiLieuChiaSe").trigger("click");
                            $(form).closeModal();
                            $("#lstvbtl").data("smGrid2018").RefreshGrid();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-TaiLieuChiaSe").viForm();
        });
    </script>
</body>
</html>
