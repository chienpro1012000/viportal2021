<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewThongBao.aspx.cs" Inherits="VIPortalAPP.UserControls.AdminCMS.ThongBao.pViewThongBao" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">

        <div class="form-group row">
            <label for="Titles" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <%=oThongBao.Titles%>
            </div>
        </div>
        <div class="form-group row">
            <label for="NoiDung" class="col-sm-2 control-label">Nội dung</label>
            <div class="col-sm-10">
                <%=oThongBao.NoiDung%>
            </div>
        </div>
        <div class="form-group row">
            <label for="NgayHieuLuc" class="col-sm-2 control-label">Ngày có hiệu lực</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy}", oThongBao.NgayHieuLuc)%>
            </div>
            <label for="NgayHetHieuLuc" class="col-sm-2 control-label">Ngày hết hiệu lực</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy}", oThongBao.NgayHetHieuLuc)%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oThongBao.Created)%>
            </div>
            <label class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oThongBao.Modified)%>
            </div>
        </div>

<%--        <div class="form-group row">
            <label class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
                <%=oThongBao.Author.LookupValue%>
            </div>
            <label class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
                <%=oThongBao.Editor.LookupValue%>
            </div>
        </div>--%>

        <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oThongBao.ListFileAttach.Count;i++){ %>
                <div><a href="<%=oThongBao.ListFileAttach[i].Url %>"><%=oThongBao.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>
