<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormThongBao.aspx.cs" Inherits="VIPortalAPP.UserControls.AdminCMS.ThongBao.pFormThongBao" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-ThongBao" class="form-horizontal">
        <input type="hidden" name="Title" id="Title" value="" />
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oThongBao.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <%--<div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oThongBao.Title%>" />
            </div>
        </div>--%>
        <div class="form-group row">
            <label for="Titles" class="col-sm-2 control-label">Tiêu đề thông báo</label>
            <div class="col-sm-10">
                <textarea name="Titles" id="Titles" class="form-control"><%:oThongBao.Titles%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="MoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <textarea name="MoTa" id="MoTa" class="form-control"><%:oThongBao.MoTa%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="ImageNews" class="col-sm-2 control-label">Ảnh đại diện</label>
            <div class="col-sm-10" data-role="groupImageDD">

                <input type="hidden" name="ImageNews" id="ImageNews" value="<%:oThongBao.ImageNews%>" />
                <img title="Ảnh đại diện" id="srcImageNews" src="<%=oThongBao.ImageNews%>" />
                <button id="btnImageNews" type="button" class="btn  btn-success">Ảnh đại diện</button>
                <button id="btnXoaImageNews" type="button" class="btn btn-danger">Xóa ảnh</button>
            </div>
        </div>
        <div class="form-group row">
            <label for="NgayHieuLuc" class="col-sm-2 control-label">Ngày có hiệu lực</label>
            <div class="col-sm-4">
                <input type="text" name="NgayHieuLuc" id="NgayHieuLuc" value="<%:string.Format("{0:dd/MM/yyyy}",oThongBao.NgayHieuLuc)%>" class="form-control input-datetime" />
            </div>
            <label for="NgayHetHieuLuc" class="col-sm-2 control-label">Ngày hết hiệu lực</label>
            <div class="col-sm-4">
                <input type="text" name="NgayHetHieuLuc" id="NgayHetHieuLuc" value="<%:string.Format("{0:dd/MM/yyyy}",oThongBao.NgayHetHieuLuc)%>" class="form-control input-datetime" />
            </div>
        </div>
        <div class="form-group row">
            <label for="NoiDung" class="col-sm-2 control-label">Nội dung</label>
            <div class="col-sm-10">
                <textarea name="NoiDung" id="NoiDung" class="form-control"><%=oThongBao.NoiDung%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oThongBao.ListFileAttach)%>'
            });
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $(".form-group select").select2({
                dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
            });
            CKEDITOR.replace("NoiDung", {
                height: 200
            });
            $("#btnImageNews").click(function () {
                $elementId = $(this);
                CKFinder.popup({
                    chooseFiles: true,
                    width: 800,
                    height: 600,
                    onInit: function (finder) {
                        finder.on('files:choose', function (evt) {
                            var file = evt.data.files.first();
                            //var output = document.getElementById(elementId);
                            //console.log(file.getUrl());
                            //$("#ImageNews").value(file.getUrl());
                            var urlfileimg = file.getUrl();
                            document.getElementById("ImageNews").value = urlfileimg;
                            document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                        });
                        finder.on('file:choose:resizedImage', function (evt) {
                            //var output = document.getElementById(elementId);
                            console.log(evt.data);
                            var urlfileimg = evt.data.resizedUrl;
                            document.getElementById("ImageNews").value = urlfileimg;
                            document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                        });
                    }
                });
            });
            $("#frm-ThongBao").validate({
                ignore: [],
                rules: {
                    Titles: "required",
                    NgayHieuLuc: "required",
                    NgayHetHieuLuc: "required",
                },
                messages: {
                    Titles: "Vui lòng nhập tiêu đề",
                    NgayHieuLuc: "Vui lòng nhập ngày có hiệu lực",
                    NgayHetHieuLuc: "Vui lòng nhập ngày hết hiệu lực",
                    NoiDung: "Vui lòng nhập nội dung",
                    ImageNews: "Vui lòng chọn ảnh đại diện",
                },
                submitHandler: function (form) {
                    $("#Title").val($("#Titles").val());
                    $.post("/UserControls/AdminCMS/ThongBao/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-ThongBao").trigger("click");
                            $(form).closeModal();
                            $('#tblThongBao').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-ThongBao").viForm();
        });
    </script>
</body>
</html>
