<%@ control language="C#" autoeventwireup="true" codebehind="UC_ThongBao.aspx.cs" inherits="VIPortalAPP.UserControls.AdminCMS.ThongBao.UC_ThongBao" %>
<div role="body-data" data-title="Thông báo" class="content_wp" data-action="/UserControls/AdminCMS/ThongBao/pAction.asp" data-form="/UserControls/AdminCMS/ThongBao/pFormThongBao.aspx" data-view="/UserControls/AdminCMS/ThongBao/pViewThongBao.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="ThongBaoSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title" />
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <button class="btn btn-primary act-add" data-per="010802" type="button">Thêm mới</button>
                    </p>
                </div>
            </div>

            <div class="clsgrid table-responsive">

                <table id="tblThongBao" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        var $tagvalue = $(".content_wp");
        var $tblDanhMucChung = $("#tblThongBao").viDataTable(
            {
                "frmSearch": "ThongBaoSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "mData": function (o) {
                                return '<a data-ItemID="' + o.ID + '" class="act-view" href="javascript:;">' + o.Titles + '</a>';
                            },
                            "name": "Titles", "sTitle": "Tiêu đề"
                        },
                        {
                            "mData": function (d) {
                                return formatDate(d.NgayHieuLuc);
                            },
                            "sWidth": "90px",
                            "name": "NgayHieuLuc", "sTitle": "Ngày có hiệu lực"
                        },
                        {
                            "mData": function (d) {
                                return formatDate(d.NgayHetHieuLuc);
                            },
                            "sWidth": "90px",
                            "name": "NgayHetHieuLuc", "sTitle": "Ngày hết hiệu lực"
                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {

                                if (o._ModerationStatus == 0)

                                    return '<a data-per="010805" title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a data-per="010805" title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",

                            "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        }, {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                "order": [[1, "desc"]],
            });
    });
</script>
