using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;

namespace VIPortalAPP.UserControls.AdminCMS.ThongBao
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            ThongBaoDA oThongBaoDA = new ThongBaoDA(UrlSite,true);
            ThongBaoItem oThongBaoItem = new ThongBaoItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oThongBaoDA.GetListJsonSolr(new ThongBaoQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw, 
                        oThongBaoDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "ThongBao", "QUERYDATA", 0, "Xem danh sách thông báo");
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oThongBaoDA.GetListJson(new ThongBaoQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oThongBaoItem = oThongBaoDA.GetByIdToObject<ThongBaoItem>(oGridRequest.ItemID);
                        oThongBaoItem.UpdateObject(context.Request);
                        oThongBaoDA.UpdateObject<ThongBaoItem>(oThongBaoItem);
                        AddLog("UPDATE", "ThongBao", "UPDATE", oGridRequest.ItemID, $"Cập nhật thông báo{oThongBaoItem.Title}");
                    }
                    else
                    {
                        oThongBaoItem.UpdateObject(context.Request);
                        oThongBaoItem.CreatedUser = new Microsoft.SharePoint.SPFieldLookupValue(CurentUser.ID,CurentUser.Title);
                        oThongBaoDA.UpdateObject<ThongBaoItem>(oThongBaoItem);
                        AddLog("UPDATE", "ThongBao", "ADD", oGridRequest.ItemID, $"Thêm mới thông báo{oThongBaoItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oThongBaoDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "ThongBao", "DELETE", oGridRequest.ItemID, $"Xóa thông báo{outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oThongBaoDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    AddLog("DELETE-MULTI", "ThongBao", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều thông báo");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oThongBaoDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "ThongBao", "APPROVED", oGridRequest.ItemID, $"Duyệt thông báo{oThongBaoDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                     oThongBaoDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "ThongBao", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt thông báo{oThongBaoDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oThongBaoItem = oThongBaoDA.GetByIdToObjectSelectFields<ThongBaoItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oThongBaoItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}