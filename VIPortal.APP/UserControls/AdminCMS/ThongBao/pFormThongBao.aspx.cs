using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP.UserControls.AdminCMS.ThongBao
{
    public partial class pFormThongBao : pFormBase
    {
        public ThongBaoItem oThongBao {get;set;}
        protected void Page_Load(object sender, EventArgs e)
        {
			oThongBao = new ThongBaoItem();
            if (ItemID > 0)
            {
                ThongBaoDA oThongBaoDA = new ThongBaoDA(UrlSite);
                oThongBao = oThongBaoDA.GetByIdToObject<ThongBaoItem>(ItemID);
            }
        }
    }
}