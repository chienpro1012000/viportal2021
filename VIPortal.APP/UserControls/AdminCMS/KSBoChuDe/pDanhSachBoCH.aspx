﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pDanhSachBoCH.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.KSBoChuDe.pDanhSachBoCH" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">

        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <%=oKSBoChuDe.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="HoTen" class="col-sm-2 control-label">Chi tiết</label>
            <div class="col-sm-10">
                <%:oKSBoChuDe.DMDescription%>
            </div>
        </div>
        <div class="form-group row">
            <label for="KSTuNgay" class="col-sm-2 control-label">Khảo sát từ</label>
            <div class="col-sm-4">
                <div class="">
                    <%=string.Format("{0:dd-MM-yyyy}", oKSBoChuDe.KSTuNgay)%>
                </div>
            </div>
            <label for="KSDenNgay" class="col-sm-2 control-label">Khảo sát đến</label>
            <div class="col-sm-4">
                <div class="">
                    <%=string.Format("{0:dd-MM-yyyy}", oKSBoChuDe.KSDenNgay)%>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="HoTen" class="col-sm-6 control-label">Danh sách Câu hỏi</label>
            <div class="col-sm-12">
                <div id="tablecauhoi"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
<script type="text/javascript">
    loadAjaxContent("/UserControls/AdminCMS/KSBoCauHoi/pListKSCauHoi.aspx?ItemID=" + <%=ItemID %>, "#tablecauhoi", {});
</script>
</html>
