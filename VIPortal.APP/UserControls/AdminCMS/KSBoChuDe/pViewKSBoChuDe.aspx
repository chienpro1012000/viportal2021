﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewKSBoChuDe.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.KSBoChuDe.pViewKSBoChuDe" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">

        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <%=oKSBoChuDe.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="MaSV" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-4">
                <%:(oKSBoChuDe!= null &&  oKSBoChuDe.DMSTT>0)?oKSBoChuDe.DMSTT+"": "" %>
            </div>
            <label class="control-label col-sm-2">Hiển thị</label>
            <div class="col-sm-4">
                <div class="checkbox">
                    <%:oKSBoChuDe.DMHienThi?"Có":"Không" %>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="HoTen" class="col-sm-2 control-label">Chi tiết</label>
            <div class="col-sm-10">
                <%:oKSBoChuDe.DMDescription%>
            </div>
        </div>
        <div class="form-group row">
            <label for="HoTen" class="col-sm-2 control-label">Câu hỏi</label>
            <div class="col-sm-10">
                <div id="tablecauhoi"></div>
            </div>
        </div>
        <div class="form-group row">
            <label for="KSTuNgay" class="col-sm-2 control-label">Khảo sát từ</label>
            <div class="col-sm-4">
                <div class="">
                    <%=string.Format("{0:dd-MM-yyyy}", oKSBoChuDe.KSTuNgay)%>
                </div>
            </div>
            <label for="KSDenNgay" class="col-sm-2 control-label">Khảo sát đến</label>
            <div class="col-sm-4">
                <div class="">
                    <%=string.Format("{0:dd-MM-yyyy}", oKSBoChuDe.KSDenNgay)%>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}", oKSBoChuDe.Created)%>
            </div>
            <label class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}",oKSBoChuDe.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
                <%=oKSBoChuDe.Author.LookupValue%>
            </div>
            <label class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
                <%=oKSBoChuDe.Editor.LookupValue%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oKSBoChuDe.ListFileAttach.Count;i++){ %>
                <div><a href="<%=oKSBoChuDe.ListFileAttach[i].Url %>"><%=oKSBoChuDe.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
<script type="text/javascript">
    loadAjaxContent("/UserControls/AdminCMS/KSBoCauHoi/pListKSCauHoi.aspx?ItemID=" + <%=ItemID %>, "#tablecauhoi", {});
</script>
</html>
