﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData.KSBoChuDe;


namespace VIPortal.APP.UserControls.AdminCMS.KSBoChuDe
{/// <summary>
 /// Summary description for pAcion
 /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            KSBoChuDeDA oKSBoChuDeDA = new KSBoChuDeDA(UrlSite);
            KSBoChuDeItem oKSBoChuDeItem = new KSBoChuDeItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oKSBoChuDeDA.GetListJsonSolr(new KSBoChuDeQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oKSBoChuDeDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "KSBoChuDe", "QUERYDATA", 0, "Xem danh sách chủ đề khảo sát");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataKSBoChuDe = oKSBoChuDeDA.GetListJson(new KSBoChuDeQuery(context.Request));
                    treeViewItems = oDataKSBoChuDe.Select(x => new TreeViewItem()
                    {
                        key = x.DMDescription,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oKSBoChuDeDA.GetListJson(new KSBoChuDeQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oKSBoChuDeItem = oKSBoChuDeDA.GetByIdToObject<KSBoChuDeItem>(oGridRequest.ItemID);
                        oKSBoChuDeItem.UpdateObject(context.Request);
                        if (oKSBoChuDeDA.CheckExit(oGridRequest.ItemID, oKSBoChuDeItem.Title) > 0)
                        {
                            oResult = new ResultAction()
                            {
                                State = ActionState.Error,
                                Message = $"Đã tồn tại bộ chủ đề {oKSBoChuDeItem.Title}"
                            };
                            break;
                        }
                        oKSBoChuDeDA.UpdateObject<KSBoChuDeItem>(oKSBoChuDeItem);
                        AddLog("UPDATE", "KSBoChuDe", "UPDATE", oGridRequest.ItemID, $"Cập nhật danh mục video{oKSBoChuDeItem.Title}");
                    }
                    else
                    {
                        oKSBoChuDeItem.UpdateObject(context.Request);
                        if (oKSBoChuDeDA.CheckExit(oGridRequest.ItemID, oKSBoChuDeItem.Title) > 0)
                        {
                            oResult = new ResultAction()
                            {
                                State = ActionState.Error,
                                Message = $"Đã tồn tại bộ chủ đề {oKSBoChuDeItem.Title}"
                            };
                            break;
                        }
                        oKSBoChuDeDA.UpdateObject<KSBoChuDeItem>(oKSBoChuDeItem);
                        AddLog("UPDATE", "KSBoChuDe", "ADD", oGridRequest.ItemID, $"Thêm mới khảo sát bộ chủ đề{oKSBoChuDeItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oKSBoChuDeDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "KSBoChuDe", "DELETE", oGridRequest.ItemID, $"Xóa khảo sát bộ chủ đề{outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oKSBoChuDeDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    AddLog("DELETE-MULTI", "KSBoChuDe", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều danh mục video");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oKSBoChuDeDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "KSBoChuDe", "APPROVED", oGridRequest.ItemID, $"Duyệt khảo sát bộ chủ đề{oKSBoChuDeDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oKSBoChuDeDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "KSBoChuDe", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt khảo sát bộ chủ đề{oKSBoChuDeDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = " Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oKSBoChuDeItem = oKSBoChuDeDA.GetByIdToObjectSelectFields<KSBoChuDeItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oKSBoChuDeItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}