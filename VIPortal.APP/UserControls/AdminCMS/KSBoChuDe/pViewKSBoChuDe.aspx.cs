﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.KSBoCauHoi;
using ViPortalData.KSBoChuDe;

namespace VIPortal.APP.UserControls.AdminCMS.KSBoChuDe
{
    public partial class pViewKSBoChuDe : pFormBase
    {
        public KSBoChuDeItem oKSBoChuDe { get; set; }
        public List<KSBoCauHoiJson> lstKSBoCauHoi { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oKSBoChuDe = new KSBoChuDeItem();
            lstKSBoCauHoi = new List<KSBoCauHoiJson>();
            if (ItemID > 0)
            {
                KSBoChuDeDA oKSBoChuDeDA = new KSBoChuDeDA(UrlSite);
                oKSBoChuDe = oKSBoChuDeDA.GetByIdToObject<KSBoChuDeItem>(ItemID);
                KSBoCauHoiDA oKSBoCauHoiDA = new KSBoCauHoiDA(UrlSite);
                lstKSBoCauHoi = oKSBoCauHoiDA.GetListJson(new KSBoCauHoiQuery() { IdChuDe = ItemID });
            }
        }
    }
}