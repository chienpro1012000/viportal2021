﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.KSBoChuDe;

namespace VIPortal.APP.UserControls.AdminCMS.KSBoChuDe
{
    public partial class pFormKSBoChuDe : pFormBase
    {
        public KSBoChuDeItem oKSBoChuDe { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oKSBoChuDe = new KSBoChuDeItem();
            if (ItemID > 0)
            {
                KSBoChuDeDA oKSBoChuDeDA = new KSBoChuDeDA(UrlSite);
                oKSBoChuDe = oKSBoChuDeDA.GetByIdToObject<KSBoChuDeItem>(ItemID);
            }
        }
    }
}