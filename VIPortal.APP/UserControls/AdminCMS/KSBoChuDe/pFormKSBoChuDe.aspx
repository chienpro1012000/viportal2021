﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormKSBoChuDe.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.KSBoChuDe.pFormKSBoChuDe" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-KSBoChuDe" class="form-horizontal">
       <input type="hidden" name="ItemID" id="ItemID" value="<%=oKSBoChuDe.ID%>" />
       <input type="hidden" name="do" id="do" value="<%=doAction %>" />       
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên chủ đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" placeholder="Nhập tên chủ đề" value="<%=oKSBoChuDe.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="HoTen" class="col-sm-2 control-label">Chi tiết</label>
            <div class="col-sm-10">
                <textarea name="DMDescription" id="DMDescription" placeholder="Chi tiết" class="form-control"><%:oKSBoChuDe.DMDescription%></textarea>
            </div>
        </div>
       <div class="form-group row">
            <label for="KSTuNgay" class="col-sm-2 control-label">Khảo sát từ</label>
            <div class="col-sm-4">
                <div class="">
                    <input type="text" name="KSTuNgay" id="KSTuNgay" value="<%:string.Format("{0:dd/MM/yyyy}", oKSBoChuDe.KSTuNgay)%>" class="form-control input-datetime" />
                </div>
            </div>
           <label for="KSDenNgay" class="col-sm-2 control-label">Khảo sát đến</label>
            <div class="col-sm-4">
                <div class="">
                     <input type="text" name="KSDenNgay" id="KSDenNgay" value="<%:string.Format("{0:dd/MM/yyyy}", oKSBoChuDe.KSDenNgay)%>" class="form-control input-datetime" />
                </div>
            </div>
      </div> 
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oKSBoChuDe.ListFileAttach)%>'
            });
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $(".form-group select").select2({
                dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
            });

            $("#frm-KSBoChuDe").validate({
                rules: {
                    Title: "required",
                    KSTuNgay: "required",
                    KSDenNgay: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tên chủ đề",
                    KSTuNgay: "Chọn ngày bắt đầu",
                    KSDenNgay: "Chọn ngày bắt đầu"
                },
                submitHandler: function (form) {
                    debugger;
                    var from = $("#KSTuNgay").val();
                    var to = $("#KSDenNgay").val();

                    if (Date.parse(from) > Date.parse(to)) {
                        //showMsgwarning("Dữ liệu ngày bắt đầu nhỏ hơn ngày kết thúc");
                        BootstrapDialog.show({
                            title: "Lỗi",
                            message: "Dữ liệu ngày bắt đầu nhỏ hơn ngày kết thúc"
                        });
                    }
                    else {
                        $.post("/UserControls/AdminCMS/KSBoChuDe/pAction.asp", $(form).viSerialize(), function (result) {
                            if (result.State == 2) {
                                BootstrapDialog.show({
                                    title: "Lỗi",
                                    message: result.Message
                                });
                            }
                            else {
                                showMsg(result.Message);
                                //$("#btn-find-KSBoChuDe").trigger("click");
                                $(form).closeModal();
                                $('#tblKSBoChuDe').DataTable().ajax.reload();
                            }
                        }).always(function () { });
                    }
                }
            });
            $("#frm-KSBoChuDe").viForm();
        });
    </script>
</body>
</html>
