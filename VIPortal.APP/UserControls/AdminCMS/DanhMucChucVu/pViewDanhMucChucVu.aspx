﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewDanhMucChucVu.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.DanhMucChucVu.pViewDanhMucChucVu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		         <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Chức Vụ</label>
            <div class="col-sm-10">
               <%=oDanhMucChucVu.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="HoTen" class="col-sm-2 control-label">Số thứ tự</label>
            <div class="col-sm-4">
                 <%:(oDanhMucChucVu!= null &&  oDanhMucChucVu.DMSTT>0)?oDanhMucChucVu.DMSTT+"": "" %>
            </div>
               <label class="control-label col-sm-2">Hiển thị</label>
            <div class="col-sm-4">
                <%=oDanhMucChucVu.DMHienThi? "Có" : "Không"%>
                </div>
        </div>
       <div class="form-group row">
            <label for="DMMoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
               <%:oDanhMucChucVu.DMMoTa%>
            </div>
        </div>
        <div class="form-group row">
            <label for="DMVietTat" class="col-sm-2 control-label">Viết tắt</label>
            <div class="col-sm-4">
                <%:oDanhMucChucVu.DMVietTat%>
            </div>
        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <%for(int i=0; i< oDanhMucChucVu.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oDanhMucChucVu.ListFileAttach[i].Url %>"><%=oDanhMucChucVu.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%=string.Format("{0:dd-MM-yyyy}", oDanhMucChucVu.Created)%>
            </div>
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}",oDanhMucChucVu.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
                <%=oDanhMucChucVu.Author.LookupValue%>
            </div>
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
                <%=oDanhMucChucVu.Editor.LookupValue%>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>