﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData.DanhMucChucVu;

namespace VIPortal.APP.UserControls.AdminCMS.DanhMucChucVu
{
    public partial class pFormDanhMucChucVu : pFormBase
    {
        public DanhMucChucVuItem oDanhMucChucVu { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oDanhMucChucVu = new DanhMucChucVuItem();
            if(ItemID > 0)
            {
                DanhMucChucVuDA oDanhMucChucVuDA = new DanhMucChucVuDA(UrlSite);
                oDanhMucChucVu = oDanhMucChucVuDA.GetByIdToObject<DanhMucChucVuItem>(ItemID);
            }
        }
    }
}