﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormDanhMucChucVu.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.DanhMucChucVu.pFormDanhMucChucVu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-DanhMucChucVu" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oDanhMucChucVu.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
          
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Chức Vụ</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oDanhMucChucVu.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="HoTen" class="col-sm-2 control-label">Số</label>
            <div class="col-sm-4">
                 <input type="number" name="DMSTT" id="DMSTT" min = "0" placeholder="Nhập số thứ tự" oninput="validity.valid||(value='');" value="<%:(oDanhMucChucVu!= null &&  oDanhMucChucVu.DMSTT>0)?oDanhMucChucVu.DMSTT+"": "" %>" class="form-control" />
            </div>
               <label class="control-label col-sm-2">Hiển thị</label>
            <div class="col-sm-4">
                <div class="checkbox">
                    <input type="radio" id="co" name="DMHienThi" value="True"  <%:oDanhMucChucVu.DMHienThi?"checked":"" %>/>
                    <label for="co" style="padding-left: 1%; padding-right: 10%;">Có </label>
                    <input type="radio" id="khong" name="DMHienThi" value="False" <%:!oDanhMucChucVu.DMHienThi?"checked":"" %> />
                    <label for="khong" style="padding-left: 1%;">Không</label>
                </div>
            </div>
        </div>
       <div class="form-group row">
            <label for="DMMoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <textarea name="DMMoTa" id="DMMoTa" class="form-control"><%:oDanhMucChucVu.DMMoTa%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="DMVietTat" class="col-sm-2 control-label">Viết tắt</label>
            <div class="col-sm-4">
                <input type="text" name="DMVietTat" id="DMVietTat" placeholder="Nhập tên viết tắt" value="<%:oDanhMucChucVu.DMVietTat%>" class="form-control" />
            </div>
        </div>
        	<div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $(document).ready(function () {
           $("#KSBoChuDe").smSelect2018V2({
                dropdownParent: "#frm-DanhMucChucVu"
             });
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oDanhMucChucVu.ListFileAttach)%>'
            });
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $(".form-group select").select2({
                dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
            });

            $("#frm-DanhMucChucVu").validate({
                rules: {
                    Title: "required",
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/DanhMucChucVu/pAction.ashx", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-DanhMucChucVu").trigger("click");
                            $(form).closeModal();
                            $('#tblDanhMucChucVu').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-DanhMucChucVu").viForm();
        });
    </script>
</body>
</html>