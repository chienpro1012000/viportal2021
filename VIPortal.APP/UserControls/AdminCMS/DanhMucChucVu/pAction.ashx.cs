﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData.DanhMucChucVu;

namespace VIPortal.APP.UserControls.AdminCMS.DanhMucChucVu
{
    /// <summary>
    /// Summary description for pAction
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {

        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            DanhMucChucVuDA oDanhMucChucVuDA = new DanhMucChucVuDA(UrlSite);
            DanhMucChucVuItem oDanhMucChucVuItem = new DanhMucChucVuItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    var oData = oDanhMucChucVuDA.GetListJson(new DanhMucChucVuQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oDanhMucChucVuDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "DanhMucChucVu", "QUERYDATA", 0, "Xem danh sách danh mục chức vụ");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataDanhMucChucVu = oDanhMucChucVuDA.GetListJson(new DanhMucChucVuQuery(context.Request));
                    treeViewItems = oDataDanhMucChucVu.Select(x => new TreeViewItem()
                    {
                        key = x.DMDescription,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;

                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oDanhMucChucVuDA.GetListJson(new DanhMucChucVuQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDanhMucChucVuItem = oDanhMucChucVuDA.GetByIdToObject<DanhMucChucVuItem>(oGridRequest.ItemID);
                        oDanhMucChucVuItem.UpdateObject(context.Request);
                        oDanhMucChucVuDA.UpdateObject<DanhMucChucVuItem>(oDanhMucChucVuItem);
                        AddLog("UPDATE", "DanhMucChucVu", "UPDATE", oGridRequest.ItemID, $"Cập nhật danh mục chức vụ{oDanhMucChucVuItem.Title}");
                    }
                    else
                    {
                        oDanhMucChucVuItem.UpdateObject(context.Request);
                        oDanhMucChucVuDA.UpdateObject<DanhMucChucVuItem>(oDanhMucChucVuItem);
                        AddLog("UPDATE", "DanhMucChucVu", "ADD", oGridRequest.ItemID, $"Thêm mới danh mục chức vụ{oDanhMucChucVuItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oDanhMucChucVuDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "DanhMucChucVu", "DELETE", oGridRequest.ItemID, $"Xóa danh mục chức vụ {outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oDanhMucChucVuDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    AddLog("DELETE-MULTI", "DanhMucChucVu", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều danh mục chức vụ");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oDanhMucChucVuDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "DanhMucChucVu", "APPROVED", oGridRequest.ItemID, $"Duyệt danh mục chức vụ {oDanhMucChucVuDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oDanhMucChucVuDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "DanhMucChucVu", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt danh mục chức vụ{oDanhMucChucVuDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDanhMucChucVuItem = oDanhMucChucVuDA.GetByIdToObjectSelectFields<DanhMucChucVuItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oDanhMucChucVuItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
                    oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}