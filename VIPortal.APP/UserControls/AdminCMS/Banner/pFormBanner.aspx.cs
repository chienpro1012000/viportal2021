using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public partial class pFormBanner : pFormBase
    {
        public BannerItem oBanner {get;set;}
        protected void Page_Load(object sender, EventArgs e)
        {
			oBanner = new BannerItem();
            if (ItemID > 0)
            {
                BannerDA oBannerDA = new BannerDA();
                oBanner = oBannerDA.GetByIdToObject<BannerItem>(ItemID);
            }
        }
    }
}