using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;

namespace VIPortalAPP.Banner
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);
            if (!string.IsNullOrEmpty(context.Request["do"]))
                doAction = context.Request["do"].ToUpper();

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            BannerDA oBannerDA = new BannerDA();
            BannerItem oBannerItem = new BannerItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oBannerDA.GetListJson(new BannerQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oBannerDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "Banner", "QUERYDATA", 0, "Xem danh sách banner");
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oBannerDA.GetListJson(new BannerQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oBannerItem = oBannerDA.GetByIdToObject<BannerItem>(oGridRequest.ItemID);
                        oBannerItem.UpdateObject(context.Request);
                        oBannerDA.UpdateObject<BannerItem>(oBannerItem);
                        AddLog("UPDATE", "Banner", "UPDATE", oGridRequest.ItemID, $"Cập nhật banner {oBannerItem.Title}");
                    }
                    else
                    {
                        oBannerItem.UpdateObject(context.Request);
                        oBannerDA.UpdateObject<BannerItem>(oBannerItem);
                        AddLog("UPDATE", "Banner", "ADD", oGridRequest.ItemID, $"Thêm mới banner {oBannerItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oBannerDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "Banner", "DELETE", oGridRequest.ItemID, $"Xóa banner {outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    var outbmulti = "";
                    AddLog("DELETE-MULTI", "Banner", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều banner");
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oBannerDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oBannerDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "Banner", "APPROVED", oGridRequest.ItemID, $"Duyệt banner {oBannerDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oBannerDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "Banner", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt banner {oBannerDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oBannerItem = oBannerDA.GetByIdToObjectSelectFields<BannerItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oBannerItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}