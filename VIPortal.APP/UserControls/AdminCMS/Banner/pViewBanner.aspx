<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewBanner.aspx.cs" Inherits="VIPortalAPP.pViewBanner" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
               <%=oBanner.Title%>
            </div>
        </div>
		<div class="form-group">
	<label for="ImageNews" class="col-sm-2 control-label">ImageNews</label>
	<div class="col-sm-10">
		<%=oBanner.ImageNews%>
	</div>
</div>
<div class="form-group">
	<label for="DMVietTat" class="col-sm-2 control-label">DMVietTat</label>
	<div class="col-sm-10">
		<%=oBanner.DMVietTat%>
	</div>
</div>

		  <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-10">
               <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oBanner.Created)%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oBanner.Modified)%>
            </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-10">
                <%=oBanner.Author.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-10">
                <%=oBanner.Editor.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oBanner.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oBanner.ListFileAttach[i].Url %>"><%=oBanner.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>