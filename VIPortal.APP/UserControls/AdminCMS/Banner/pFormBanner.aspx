<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormBanner.aspx.cs" Inherits="VIPortalAPP.pFormBanner" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-Banner" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oBanner.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oBanner.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="ColumnImage" class="col-sm-2 control-label">Ảnh đại diện</label>
            <div class="col-sm-10" data-role="groupImageDD">
                <div id="ColumnImageZone" style="float: left; padding-right: 10px;">
                    <div class="include-library" style="display: inline-block; clear: both;">
                        <input type="hidden" name="ColumnImage" id="ColumnImage" value="<%:oBanner.ColumnImage%>" />
                        <img title="Ảnh đại diện" id="srcColumnImage" src="<%=oBanner.ColumnImageSrc%>" />
                    </div>
                </div>
                <button id="btnColumnImage" type="button" class="btn  btn-success">Ảnh đại diện</button>
                <button id="btnXoaColumnImage" type="button" class="btn btn-danger">Xóa ảnh</button>
            </div>
        </div>
        <div class="form-group row">
            <label for="DMVietTat" class="col-sm-2 control-label">Phân loại</label>
            <div class="col-sm-10">
                <select class="form-control" >
                    <option value="1">Banner Slide</option>
                </select>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $("#btnColumnImage").click(function () {
                $elementId = $(this);
                CKFinder.popup({
                    chooseFiles: true,
                    width: 800,
                    height: 600,
                    onInit: function (finder) {
                        finder.on('files:choose', function (evt) {
                            var file = evt.data.files.first();
                            //var output = document.getElementById(elementId);
                            //console.log(file.getUrl());
                            //$("#ColumnImage").value(file.getUrl());
                            var urlfileimg = file.getUrl();
                            document.getElementById("ColumnImage").value = urlfileimg;
                            document.getElementById("srcColumnImage").setAttribute("src", urlfileimg);
                        });
                        finder.on('file:choose:resizedImage', function (evt) {
                            //var output = document.getElementById(elementId);
                            console.log(evt.data);
                            var urlfileimg = evt.data.resizedUrl;
                            document.getElementById("ColumnImage").value = urlfileimg;
                            document.getElementById("srcColumnImage").setAttribute("src", urlfileimg);
                        });
                    }
                });
            });
            DeleteImage('btnXoaColumnImage', 'imgColumnImage', 'ColumnImage');
            $("#frm-Banner").validate({
                rules: {
                    Title: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/Banner/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-Banner").trigger("click");
                            $(form).closeModal();
                            $('#tblBanner').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
        });
    </script>
</body>
</html>
