<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewLichCaTruc.aspx.cs" Inherits="VIPortalAPP.pViewLichCaTruc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
               <%=oLichCaTruc.Title%>
            </div>
        </div>
		<div class="form-group">
	<label for="CaTruc" class="col-sm-2 control-label">CaTruc</label>
	<div class="col-sm-10">
		<%=oLichCaTruc.CaTruc%>
	</div>
</div>
<div class="form-group">
	<label for="CaTruc_x003a_ThoiGianBatDau" class="col-sm-2 control-label">CaTruc_x003a_ThoiGianBatDau</label>
	<div class="col-sm-10">
		<%=oLichCaTruc.CaTruc_x003a_ThoiGianBatDau%>
	</div>
</div>
<div class="form-group">
	<label for="CaTruc_x003a_ThoiGianKetThuc" class="col-sm-2 control-label">CaTruc_x003a_ThoiGianKetThuc</label>
	<div class="col-sm-10">
		<%=oLichCaTruc.CaTruc_x003a_ThoiGianKetThuc%>
	</div>
</div>
<div class="form-group">
	<label for="UserPhuTrach" class="col-sm-2 control-label">UserPhuTrach</label>
	<div class="col-sm-10">
		<%=oLichCaTruc.UserPhuTrach%>
	</div>
</div>
<div class="form-group">
	<label for="NgayTruc" class="col-sm-2 control-label">NgayTruc</label>
	<div class="col-sm-10">
		<%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oLichCaTruc.NgayTruc)%>
	</div>
</div>
<div class="form-group">
	<label for="LichGhiChu" class="col-sm-2 control-label">LichGhiChu</label>
	<div class="col-sm-10">
		<%=oLichCaTruc.LichGhiChu%>
	</div>
</div>
<div class="form-group">
	<label for="fldGroup" class="col-sm-2 control-label">fldGroup</label>
	<div class="col-sm-10">
		<%=oLichCaTruc.fldGroup%>
	</div>
</div>

		  <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-10">
               <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oLichCaTruc.Created)%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oLichCaTruc.Modified)%>
            </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-10">
                <%=oLichCaTruc.Author.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-10">
                <%=oLichCaTruc.Editor.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oLichCaTruc.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oLichCaTruc.ListFileAttach[i].Url %>"><%=oLichCaTruc.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>