﻿<%@ page language="C#" autoeventwireup="true" codebehind="pFormLichCaTruc2.aspx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.LichCaTruc.pFormLichCaTruc2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
    <style type="text/css">
        .clslichcatruc, .lichheader {
            width: 100%;
            border-collapse: collapse;
        }

        table.clslichcatruc, table.clslichcatruc td, table.clslichcatruc th {
            border: 1px solid;
            padding: 5px;
        }
        .selectuserchinh {
            width:100%;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            //alert(123);
            //console.log($("#selectuserchinhcachinh2"));
            //$('.js-example-basic-multiple').select2();
            //$('#selectuserchinhcachinh2').select2();
            //$("#selectuserchinhcachinh2").select2();
            $('.selectuserchinh').each(function (i, obj) {
                //test
                var $selected = $(obj).attr('data-selected');
                var $selectid = $(obj).attr('id');
                const myArray = $selected.split(",");
                $.each(myArray, function (index, ItemID) {
                    if (ItemID != '') {
                        $('#' + $selectid + ' option[value=' + ItemID + ']').attr('selected', 'selected');
                    }
                });
            });
            $('.selectuserphu').each(function (i, obj) {
                //test
                var $selected = $(obj).attr('data-selected');
                var $selectid = $(obj).attr('id');
                const myArray = $selected.split(",");
                $.each(myArray, function (index, ItemID) {
                    if (ItemID != '') {
                        $('#' + $selectid + ' option[value=' + ItemID + ']').attr('selected', 'selected');
                    }
                });
            });
            $(".selectuserchinh").select2({
                allowClear: true,
                placeholder: "Chọn cán bộ trực chính"
            });
            $(".selectuserphu").select2({
                placeholder: "Chọn cán bộ trực phụ"
            });
            $("#frm-LichCaTruc").validate({
                rules: {
                },
                messages: {
                    //Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    loading();
                    $.post("/UserControls/AdminCMS/LichCaTruc/pAction.asp", $(form).viSerialize(), function (result) {
                        Endloading();
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-LichCaTruc").trigger("click");
                            $(form).closeModal();
                            $("#lich_div_parent").data("smGrid2018").RefreshGrid();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-LichCaTruc").viForm();
        });
    </script>
</head>
<body>
    <form id="frm-LichCaTruc" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oLichCaTruc.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" name="fldGroup" id="fldGroup" value="<%=oLichCaTruc.fldGroup.LookupId %>" />
        <input type="hidden" name="SearchTuNgay" id="SearchTuNgay" value="<%=Request["SearchTuNgay"] %>" />
        <input type="hidden" name="SearchDenNgay" id="SearchDenNgay" value="<%=Request["SearchDenNgay"] %>" />
        
        <div class="form-group row">
            <div class="col-sm-12">
                <table class="clslichcatruc">
                    <tr>
                        <th style="width: 100px;">Ngày</th>

                        <th colspan="3">
                            <table class="lichheader">
                                <tr>
                                    <td style="width: 130px">Ca trực</td>
                                    <td style="width: 30%;">Trực chính</td>
                                    <td>Chực vụ</td>

                                </tr>
                            </table>
                        </th>
                    </tr>
                    <% foreach (VIPortalAPP.LichHienThi item in lstLichHienThi)
                    { %>
                    <tr>
                        <td><%=item.Thu %><br />
                            <%=item.Ngay %></td>
                        <td colspan="3">
                            <%=item.Content %>
                        </td>
                    </tr>
                    <%} %>
                </table>
            </div>
        </div>
        <div class="form-group row">
            <label for="LichGhiChu" class="col-sm-2 control-label">Ghi chú</label>
            <div class="col-sm-10">
                <textarea name="LichGhiChu" id="LichGhiChu" class="form-control"><%:oLichCaTruc.LichGhiChu%></textarea>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>


</body>
</html>
