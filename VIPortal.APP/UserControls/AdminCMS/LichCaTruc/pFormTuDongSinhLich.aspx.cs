﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData.Lconfig;

namespace VIPortal.APP.UserControls.AdminCMS.LichCaTruc
{
    public partial class pFormTuDongSinhLich : pFormBase
    {
        public int fldGroup { get; set; }
        public List<LconfigJson> LstToNhom { get; set; }
        public DateTime TuNgay { get; set; }
        public DateTime DenNgay { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request["fldGroup"]))
            {
                fldGroup = Convert.ToInt32(Request["fldGroup"]);
            }
            LconfigDA oLconfigDA = new LconfigDA();
            LstToNhom =  oLconfigDA.GetListJson(new LconfigQuery() { ConfigGroup = "CauhinhToPhong" });
            LstToNhom = LstToNhom.Where(x => x.ConfigType.StartsWith($"GroupTo_{fldGroup}_")).ToList();
            
        }
    }
}