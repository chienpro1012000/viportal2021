using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public partial class pViewLichCaTruc : pFormBase
    {
        public LichCaTrucItem oLichCaTruc = new LichCaTrucItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oLichCaTruc = new LichCaTrucItem();
            if (ItemID > 0)
            {
                LichCaTrucDA oLichCaTrucDA = new LichCaTrucDA();
                oLichCaTruc = oLichCaTrucDA.GetByIdToObject<LichCaTrucItem>(ItemID);

            }
        }
    }
}