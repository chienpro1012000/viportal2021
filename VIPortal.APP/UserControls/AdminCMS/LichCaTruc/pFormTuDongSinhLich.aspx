﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormTuDongSinhLich.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.LichCaTruc.pFormTuDongSinhLich" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-LichCaTruc" class="form-horizontal">
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" name="fldGroup" id="fldGroup" value="<%=fldGroup %>" />
        <div class="form-group row">
            <label for="NgayTruc" class="col-sm-2 control-label">Từ ngày</label>
            <div class="col-sm-4">
                <input type="text" name="SearchTuNgay" id="NgayTruc" value="" class="form-control input-datetime" />
            </div>
            <label for="CaTruc" class="col-sm-2 control-label">Đến ngày</label>
            <div class="col-sm-4">
               <input type="text" name="SearchDenNgay" id="DenNgay" value="" class="form-control input-datetime" />
            </div>
        </div>
        <div class="form-group row">
                <label for="LichThanhPhanThamGia" class="col-sm-2 control-label requPhong">Chọn tổ nhóm</label>
                <div class="col-sm-10">
                    <select id="NhomUser" name="NhomUser" class="form-control">
                        <option value="">Chọn tổ/nhóm</option>
                    <%foreach (ViPortalData.Lconfig.LconfigJson item in LstToNhom)
                    {%>
                        <option value="<%=item.ConfigValue %>"><%=item.Title%></option>
                    <%} %>
                         </select>
                </div>
            </div>
        <div class="form-group row">
                <label for="LichThanhPhanThamGia" class="col-sm-2 control-label requPhong">Cán bộ tham gia</label>
                <div class="col-sm-10">
                    <div class="input-group mb-3">
                        <input type="text" disabled="disabled" value="" placeholder="Thành phần tham gia" id="UserChoie" class="form-control" />
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" data-idselected="" data-do="CHONNGUOI" title="Chọn người dùng" size="1240" url="/UserControls/AdminCMS/LichDonVi/pFormChooseThamGia.aspx" id="btnChonNguoi" type="button">Chọn người dùng</button>
                        </div>
                    </div>
                    <input type="hidden" name="lstNguoiDung_Value" id="lstNguoiDung_Value" value="" />
                    <input type="hidden" name="LichThanhPhanThamGia" id="lstLichThamGia" value="" />
                    <textarea id="LichThanhPhanThamGia" name="" disabled="disabled" placeholder="Thành phần tham gia" class="form-control"></textarea>
                </div>
            </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#btnChonNguoi").click(function () {
                var idSelected = $(this).attr("data-idSelected");
                openDialog("Chọn cán bộ", "/UserControls/AdminCMS/LichDonVi/pFormChooseThamGia.aspx", { "do": "CHONNGUOI", "idSelected": idSelected }, 1024);
            });
            $("#frm-LichCaTruc").validate({
                rules: {
                    NgayTruc: "required",
                    DenNgay: "required",
                    //CreatedUser: "required",
                    //ThanhPhanThamGia: "required"
                },
                messages: {
                    //Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    loading();
                    $.post("/UserControls/AdminCMS/LichCaTruc/pAction.asp", $(form).viSerialize(), function (result) {
                        Endloading();
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-LichCaTruc").trigger("click");
                            $(form).closeModal();
                            $("#lich_div_parent").data("smGrid2018").RefreshGrid();
                            $("#lichDonVi_div_parent").data("smGrid2018").RefreshGrid();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-LichCaTruc").viForm();
        });
    </script>
</body>
</html>
