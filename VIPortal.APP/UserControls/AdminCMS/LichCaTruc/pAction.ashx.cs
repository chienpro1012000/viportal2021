using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortalAPP.LichCaTruc
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        //public string doAction = string.Empty;
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            LichCaTrucDA oLichCaTrucDA = new LichCaTrucDA(UrlSite);
            LichCaTrucItem oLichCaTrucItem = new LichCaTrucItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oLichCaTrucDA.GetListJson(new LichCaTrucQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oLichCaTrucDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oLichCaTrucDA.GetListJson(new LichCaTrucQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "IMPORTEXCEL":
                    oLichCaTrucItem.UpdateObject(context.Request);
                    if (oLichCaTrucItem.ListFileAttachAdd.Count > 0)
                    {
                        #region MyRegion
                        DMCaTrucDA oDMCaTrucDA = new DMCaTrucDA(UrlSite);
                        List<DMCaTrucJson> lstCaTruc = oDMCaTrucDA.GetListJson(new DMCaTrucQuery() { fldGroup = oLichCaTrucItem.fldGroup.LookupId });

                        LUserDA oLuserDA = new LUserDA();
                        List<LUserJson> lstUser = new List<LUserJson>();
                        if (CurentUser.Groups.ID == oLichCaTrucItem.fldGroup.LookupId)
                        {
                            lstUser = oLuserDA.GetListJson(new LUserQuery() { Groups = oLichCaTrucItem.fldGroup.LookupId });
                        }
                        else
                        {
                            lstUser = oLuserDA.GetListJson(new LUserQuery() { UserPhongBan = CurentUser.UserPhongBan.ID, GetUserPhongBan = true });
                        }

                        Stream stream = new MemoryStream(oLichCaTrucItem.ListFileAttachAdd[0].DataFile);

                        XSSFWorkbook hssfworkbook = new XSSFWorkbook(stream);
                        ISheet oSheet = hssfworkbook.GetSheetAt(0);
                        int CountSecces = 0;
                        DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
                        dtfi.ShortDatePattern = "dd/MM/yyyy";
                        dtfi.DateSeparator = "/";
                        #endregion
                        for (int i = 4; i < oSheet.LastRowNum; i++)
                        {
                            LichCaTrucItem lichCaTrucItem = new LichCaTrucItem();
                            lichCaTrucItem.fldGroup = new Microsoft.SharePoint.SPFieldLookupValue(oLichCaTrucItem.fldGroup.LookupId, "");
                            IRow oRow = oSheet.GetRow(i);
                            ICell oCellNgayTruc = oRow.GetCell(0);
                            if (oCellNgayTruc.CellType == CellType.String)
                            {
                                string NgayTruc = oCellNgayTruc.StringCellValue;
                                lichCaTrucItem.NgayTruc = Convert.ToDateTime(NgayTruc, dtfi);
                                //lichCaTrucItem.NgayTruc = oCellNgayTruc.DateCellValue;
                            }
                            if (oCellNgayTruc.CellType == CellType.Numeric)
                            {
                                lichCaTrucItem.NgayTruc = oCellNgayTruc.DateCellValue;
                            }



                            ICell oCellChucVu = oRow.GetCell(4);
                            string TitleChucVu = oCellChucVu.StringCellValue;
                            if (TitleChucVu == "Trực phụ")
                            {
                                lichCaTrucItem.DBPhanLoai = 0;
                            }
                            else lichCaTrucItem.DBPhanLoai = 1;

                            ICell oCellCaTruc = oRow.GetCell(1);
                            string TitleCaTruc = oCellCaTruc.StringCellValue.Trim();
                            DMCaTrucJson oDMCaTrucJson = lstCaTruc.FirstOrDefault(x => x.Title.Contains(TitleCaTruc));
                            if (oDMCaTrucJson != null)
                            {
                                lichCaTrucItem.CaTruc = new Microsoft.SharePoint.SPFieldLookupValue(oDMCaTrucJson.ID, oDMCaTrucJson.Title);
                            }
                            ICell oCellCanBo = oRow.GetCell(2);
                            string TitleCb = oCellCanBo.StringCellValue;
                            LUserJson oLUserJson = lstUser.FirstOrDefault(x => x.Title == TitleCb);
                            if (oLUserJson != null)
                            {
                                lichCaTrucItem.CreatedUser = new Microsoft.SharePoint.SPFieldLookupValue(oLUserJson.ID, oLUserJson.Title);
                            }
                            if (lichCaTrucItem.CreatedUser.LookupId > 0 && lichCaTrucItem.CaTruc.LookupId > 0 && lichCaTrucItem.NgayTruc.HasValue)
                            {

                                //checkexit nếu cso rồi thi bỏ qua.
                                int Checkexit = oLichCaTrucDA.GetCount(new LichCaTrucQuery()
                                {
                                    NgayTruc = lichCaTrucItem.NgayTruc,
                                    CreatedUser = lichCaTrucItem.CreatedUser.LookupId,
                                    CaTruc = lichCaTrucItem.CaTruc.LookupId
                                });
                                if (Checkexit == 0)
                                {
                                    lichCaTrucItem.ThoiGianBatDauThucTe = DateTime.ParseExact($"{lichCaTrucItem.NgayTruc.Value.ToString("dd/MM/yyyy")} {oDMCaTrucJson.ThoiGianBatDau}", "dd/MM/yyyy H:mm", CultureInfo.InvariantCulture);
                                    lichCaTrucItem.ThoiGianKetThucThucTe = DateTime.ParseExact($"{lichCaTrucItem.NgayTruc.Value.ToString("dd/MM/yyyy")} {oDMCaTrucJson.ThoiGianKetThuc}", "dd/MM/yyyy H:mm", CultureInfo.InvariantCulture);
                                    oLichCaTrucDA.UpdateObject<LichCaTrucItem>(lichCaTrucItem);
                                    CountSecces++;
                                }
                            }
                        }
                        oResult = new ResultAction() { State = ActionState.Succeed, Message = "Import thành công " + CountSecces + " bản ghi trên tổng số " + (oSheet.LastRowNum - 3) };
                    }
                    break;
                case "CREATEMULTIV2":
                    DMCaTrucDA dMCaTrucDA2 = new DMCaTrucDA(UrlSite);
                    List<DMCaTrucJson> lstDMCaTruc2 = dMCaTrucDA2.GetListJson(new DMCaTrucQuery() { FieldOrder = "Title", Ascending = true, });
                    LichCaTrucQuery oLichCaTrucQuery2 = new LichCaTrucQuery(context.Request);
                    int SONgayFix2 = oLichCaTrucQuery2.DenNgay.Value.Subtract(oLichCaTrucQuery2.TuNgay.Value).Days + 1;
                    oLichCaTrucQuery2.Length = 0;
                    lstDMCaTruc2 = lstDMCaTruc2.Where(x => x.fldGroup != null && x.fldGroup.ID == oLichCaTrucQuery2.fldGroup).ToList();

                    for (int i = 0; i < SONgayFix2; i++)
                    {
                        DateTime curdate = oLichCaTrucQuery2.TuNgay.Value.AddDays(i);
                        int DayOfWeek = (int)curdate.DayOfWeek;
                        foreach (DMCaTrucJson oDMCaTrucJson in lstDMCaTruc2)
                        {
                            //xóa lịch ngày + ca 

                            bool isCaCuaNgay = false;
                            // curdate.DayOfWeek
                            if ((DayOfWeek == 0 || DayOfWeek == 6) && oDMCaTrucJson.LoaiCaTruc == 1)//nghỉ t7 cn
                            {
                                //là ngày nghỉ.
                                isCaCuaNgay = true;
                            }
                            if ((DayOfWeek != 0 && DayOfWeek != 6) && oDMCaTrucJson.LoaiCaTruc == 0)//nghỉ t7 cn
                            {
                                isCaCuaNgay = true;
                            }
                            if (isCaCuaNgay)
                            {
                                if(!string.IsNullOrEmpty(context.Request[$"selectuserchinh{oDMCaTrucJson.ID}_{string.Format("{0:ddMMyyyy}", curdate)}"]))
                                {
                                    //lịch chính, 
                                    oLichCaTrucItem = new LichCaTrucItem();
                                    oLichCaTrucItem.CaTruc = new Microsoft.SharePoint.SPFieldLookupValue(oDMCaTrucJson.ID, "");
                                    oLichCaTrucItem.fldGroup = new Microsoft.SharePoint.SPFieldLookupValue(oLichCaTrucQuery2.fldGroup, "");
                                    oLichCaTrucItem.CreatedUser = new Microsoft.SharePoint.SPFieldLookupValue(
                                        Convert.ToInt32(context.Request[$"selectuserchinh{oDMCaTrucJson.ID}_{string.Format("{0:ddMMyyyy}", curdate)}"]), "");
                                    oLichCaTrucItem.NgayTruc = curdate;
                                    oLichCaTrucItem.DBPhanLoai = 1;

                                    if (oLichCaTrucItem.CreatedUser.LookupId > 0 && oLichCaTrucItem.CaTruc.LookupId > 0 && oLichCaTrucItem.NgayTruc.HasValue)
                                    {

                                        //checkexit nếu cso rồi thi bỏ qua.
                                        int Checkexit = oLichCaTrucDA.GetCount(new LichCaTrucQuery()
                                        {
                                            NgayTruc = oLichCaTrucItem.NgayTruc,
                                            CreatedUser = oLichCaTrucItem.CreatedUser.LookupId,
                                            CaTruc = oLichCaTrucItem.CaTruc.LookupId
                                        });
                                        if (Checkexit == 0)
                                        {
                                            oLichCaTrucItem.ThoiGianBatDauThucTe = DateTime.ParseExact($"{oLichCaTrucItem.NgayTruc.Value.ToString("dd/MM/yyyy")} {oDMCaTrucJson.ThoiGianBatDau}", "dd/MM/yyyy H:mm", CultureInfo.InvariantCulture);
                                            oLichCaTrucItem.ThoiGianKetThucThucTe = DateTime.ParseExact($"{oLichCaTrucItem.NgayTruc.Value.ToString("dd/MM/yyyy")} {oDMCaTrucJson.ThoiGianKetThuc}", "dd/MM/yyyy H:mm", CultureInfo.InvariantCulture);
                                            oLichCaTrucDA.UpdateObject<LichCaTrucItem>(oLichCaTrucItem);
                                            //CountSecces++;
                                        }
                                    }
                                }


                                if (!string.IsNullOrEmpty(context.Request[$"selectusercaphu{oDMCaTrucJson.ID}_{string.Format("{0:ddMMyyyy}", curdate)}"]))
                                {
                                    List<int> lstUserPhu = clsFucUtils.GetDanhSachIDsQuaFormPost(context.Request[$"selectusercaphu{oDMCaTrucJson.ID}_{string.Format("{0:ddMMyyyy}", curdate)}"]);
                                    foreach (var itemiduser in lstUserPhu)
                                    {
                                        //lịch phu, 
                                        oLichCaTrucItem = new LichCaTrucItem();
                                        oLichCaTrucItem.CaTruc = new Microsoft.SharePoint.SPFieldLookupValue(oDMCaTrucJson.ID, "");
                                        oLichCaTrucItem.fldGroup = new Microsoft.SharePoint.SPFieldLookupValue(oLichCaTrucQuery2.fldGroup, "");
                                        oLichCaTrucItem.CreatedUser = new Microsoft.SharePoint.SPFieldLookupValue(itemiduser, "");
                                        oLichCaTrucItem.NgayTruc = curdate;
                                        oLichCaTrucItem.DBPhanLoai = 0;

                                        if (oLichCaTrucItem.CreatedUser.LookupId > 0 && oLichCaTrucItem.CaTruc.LookupId > 0 && oLichCaTrucItem.NgayTruc.HasValue)
                                        {

                                            //checkexit nếu cso rồi thi bỏ qua.
                                            int Checkexit = oLichCaTrucDA.GetCount(new LichCaTrucQuery()
                                            {
                                                NgayTruc = oLichCaTrucItem.NgayTruc,
                                                CreatedUser = oLichCaTrucItem.CreatedUser.LookupId,
                                                CaTruc = oLichCaTrucItem.CaTruc.LookupId
                                            });
                                            if (Checkexit == 0)
                                            {
                                                oLichCaTrucItem.ThoiGianBatDauThucTe = DateTime.ParseExact($"{oLichCaTrucItem.NgayTruc.Value.ToString("dd/MM/yyyy")} {oDMCaTrucJson.ThoiGianBatDau}", "dd/MM/yyyy H:mm", CultureInfo.InvariantCulture);
                                                oLichCaTrucItem.ThoiGianKetThucThucTe = DateTime.ParseExact($"{oLichCaTrucItem.NgayTruc.Value.ToString("dd/MM/yyyy")} {oDMCaTrucJson.ThoiGianKetThuc}", "dd/MM/yyyy H:mm", CultureInfo.InvariantCulture);
                                                oLichCaTrucDA.UpdateObject<LichCaTrucItem>(oLichCaTrucItem);
                                                //CountSecces++;
                                            }
                                        }
                                    }
                                    
                                }
                            }
                        }
                    }
                    oResult = new ResultAction() { State = ActionState.Succeed, Message = "Tạo lịch thành công" };
                    break;
                case "CREATEMULTIV3":
                    LichCaTrucQuery oLichCaTrucQuery3 = new LichCaTrucQuery(context.Request);
                    int SONgayFix3 = oLichCaTrucQuery3.DenNgay.Value.Subtract(oLichCaTrucQuery3.TuNgay.Value).Days + 1;
                    oLichCaTrucQuery3.Length = 0;
                    DMCaTrucDA dMCaTrucDA3 = new DMCaTrucDA(UrlSite);
                    List<DMCaTrucJson>  lstDMCaTruc3 = dMCaTrucDA3.GetListJson(new DMCaTrucQuery() { FieldOrder = "Title", Ascending = true, fldGroup = oLichCaTrucQuery3.fldGroup });
                    oLichCaTrucItem.UpdateObject(context.Request);

                    for (int i = 0; i < SONgayFix3; i++)
                    {
                        DateTime curdate = oLichCaTrucQuery3.TuNgay.Value.AddDays(i);
                        int DayOfWeek = (int)curdate.DayOfWeek;
                        foreach (DMCaTrucJson oDMCaTrucJson in lstDMCaTruc3)
                        {
                            oLichCaTrucItem.ID = 0;
                            string NameInput = string.Format("{0:ddMMyyyy}", curdate) + "_" + oDMCaTrucJson.ID;
                            string valueINput = context.Request[NameInput];
                            if (!string.IsNullOrEmpty(context.Request[NameInput]) && context.Request[NameInput] == "on")
                            {
                                oLichCaTrucItem.NgayTruc = curdate;
                                oLichCaTrucItem.CaTruc = new Microsoft.SharePoint.SPFieldLookupValue(oDMCaTrucJson.ID, "");
                                if (oLichCaTrucItem.CreatedUser.LookupId > 0 && oLichCaTrucItem.CaTruc.LookupId > 0 && oLichCaTrucItem.NgayTruc.HasValue)
                                {

                                    //checkexit nếu cso rồi thi bỏ qua.
                                    int Checkexit = oLichCaTrucDA.GetCount(new LichCaTrucQuery()
                                    {
                                        NgayTruc = oLichCaTrucItem.NgayTruc,
                                        CreatedUser = oLichCaTrucItem.CreatedUser.LookupId,
                                        CaTruc = oLichCaTrucItem.CaTruc.LookupId
                                    });
                                    if (Checkexit == 0)
                                    {
                                        oLichCaTrucItem.ThoiGianBatDauThucTe = DateTime.ParseExact($"{oLichCaTrucItem.NgayTruc.Value.ToString("dd/MM/yyyy")} {oDMCaTrucJson.ThoiGianBatDau}", "dd/MM/yyyy H:mm", CultureInfo.InvariantCulture);
                                        oLichCaTrucItem.ThoiGianKetThucThucTe = DateTime.ParseExact($"{oLichCaTrucItem.NgayTruc.Value.ToString("dd/MM/yyyy")} {oDMCaTrucJson.ThoiGianKetThuc}", "dd/MM/yyyy H:mm", CultureInfo.InvariantCulture);
                                        oLichCaTrucDA.UpdateObject<LichCaTrucItem>(oLichCaTrucItem);
                                        //CountSecces++;
                                    }
                                }
                            }
                        }
                    }
                    oResult = new ResultAction() { State = ActionState.Succeed, Message = "Tạo lịch thành công" };
                    break;
                case "CREATEMULTI":
                    if (!string.IsNullOrEmpty(context.Request["LichThanhPhanThamGia"]) || !string.IsNullOrEmpty(context.Request["NhomUser"]))
                    {
                        int sttuser = -1;
                        List<int> lstUserTruc = new List<int>();
                        if (!string.IsNullOrEmpty(context.Request["LichThanhPhanThamGia"]))
                            lstUserTruc = clsFucUtils.GetDanhSachIDsQuaFormPost(context.Request["LichThanhPhanThamGia"]);
                        if (!string.IsNullOrEmpty(context.Request["NhomUser"]))
                            lstUserTruc = clsFucUtils.GetDanhSachIDsQuaFormPost(context.Request["NhomUser"]);
                        //oLichCaTrucItem.UpdateObject(context.Request);
                        LichCaTrucQuery oLichCaTrucQuery = new LichCaTrucQuery(context.Request);
                        int SONgayFix = oLichCaTrucQuery.DenNgay.Value.Subtract(oLichCaTrucQuery.TuNgay.Value).Days + 1;
                        DMCaTrucDA odMCaTrucDA = new DMCaTrucDA(UrlSite);
                        List<DMCaTrucJson> lstDMCaTruc = odMCaTrucDA.GetListJson(new DMCaTrucQuery() { FieldOrder = "Title", Ascending = true, fldGroup = oLichCaTrucQuery.fldGroup });


                        for (int i = 0; i < SONgayFix; i++)
                        {
                            DateTime curdate = oLichCaTrucQuery.TuNgay.Value.AddDays(i);
                            //check xem nay là ngày gì.
                            int DayOfWeek = (int)curdate.DayOfWeek;
                            List<DMCaTrucJson> lstDMCaTrucByday = new List<DMCaTrucJson>();
                            if ((DayOfWeek == 0 || DayOfWeek == 6))//nghỉ t7 cn
                            {
                                lstDMCaTrucByday = lstDMCaTruc.Where(x => x.LoaiCaTruc == 1).ToList();
                            }
                            if ((DayOfWeek != 0 && DayOfWeek != 6))//nghỉ t7 cn
                            {
                                lstDMCaTrucByday = lstDMCaTruc.Where(x => x.LoaiCaTruc == 0).ToList();
                            }
                            foreach (DMCaTrucJson oDMCaTrucJson in lstDMCaTrucByday)
                            {
                                try
                                {
                                    //tạo 1 ca trục vs 1 nguoi,
                                    sttuser++;
                                    if (sttuser >= lstUserTruc.Count)
                                        sttuser = 0;
                                    oLichCaTrucItem = new LichCaTrucItem()
                                    {
                                        fldGroup = new Microsoft.SharePoint.SPFieldLookupValue(oLichCaTrucQuery.fldGroup, ""),
                                        CreatedUser = new Microsoft.SharePoint.SPFieldLookupValue(lstUserTruc[sttuser], ""),
                                        CaTruc = new Microsoft.SharePoint.SPFieldLookupValue(oDMCaTrucJson.ID, ""),
                                        NgayTruc = curdate,
                                        DBPhanLoai = 0
                                    };
                                    oLichCaTrucItem.ThoiGianBatDauThucTe = DateTime.ParseExact($"{oLichCaTrucItem.NgayTruc.Value.ToString("dd/MM/yyyy")} {oDMCaTrucJson.ThoiGianBatDau}", "dd/MM/yyyy H:mm", CultureInfo.InvariantCulture);
                                    oLichCaTrucItem.ThoiGianKetThucThucTe = DateTime.ParseExact($"{oLichCaTrucItem.NgayTruc.Value.ToString("dd/MM/yyyy")} {oDMCaTrucJson.ThoiGianKetThuc}", "dd/MM/yyyy H:mm", CultureInfo.InvariantCulture);
                                    oLichCaTrucDA.UpdateObject<LichCaTrucItem>(oLichCaTrucItem);
                                }
                                catch (Exception ex)
                                {
                                    throw new Exception("Message + " + sttuser + "." + lstUserTruc.Count, ex);
                                }
                            }
                        }
                        oResult.Message = "Khởi tạo thành công";
                    }
                    else
                    {
                        oResult = new ResultAction() { State = ActionState.Error, Message = "Không xác định người dùng" };
                    }
                    break;
                case "UPDATE":
                    DMCaTrucDA dMCaTrucDA = new DMCaTrucDA(UrlSite);
                    if (oGridRequest.ItemID > 0)
                    {
                        oLichCaTrucItem = oLichCaTrucDA.GetByIdToObject<LichCaTrucItem>(oGridRequest.ItemID);
                        oLichCaTrucItem.UpdateObject(context.Request);
                        DMCaTrucItem oDMCaTrucItem = dMCaTrucDA.GetByIdToObject<DMCaTrucItem>(oLichCaTrucItem.CaTruc.LookupId);
                        oLichCaTrucItem.ThoiGianBatDauThucTe = DateTime.ParseExact($"{oLichCaTrucItem.NgayTruc.Value.ToString("dd/MM/yyyy")} {oDMCaTrucItem.ThoiGianBatDau}", "dd/MM/yyyy H:mm", CultureInfo.InvariantCulture);
                        oLichCaTrucItem.ThoiGianKetThucThucTe = DateTime.ParseExact($"{oLichCaTrucItem.NgayTruc.Value.ToString("dd/MM/yyyy")} {oDMCaTrucItem.ThoiGianKetThuc}", "dd/MM/yyyy H:mm", CultureInfo.InvariantCulture);
                        oLichCaTrucDA.UpdateObject<LichCaTrucItem>(oLichCaTrucItem);
                    }
                    else
                    {
                        oLichCaTrucItem.UpdateObject(context.Request);
                        List<int> lstUserTruc = new List<int>();
                        DMCaTrucItem oDMCaTrucItem = dMCaTrucDA.GetByIdToObject<DMCaTrucItem>(oLichCaTrucItem.CaTruc.LookupId);
                        if (!string.IsNullOrEmpty(context.Request["ThanhPhanThamGia"]))
                        {
                            lstUserTruc = clsFucUtils.GetDanhSachIDsQuaFormPost(context.Request["ThanhPhanThamGia"]);
                        }
                        int LichChinh = oLichCaTrucItem.CreatedUser.LookupId;
                        if (LichChinh > 0 && !lstUserTruc.Contains(LichChinh))
                            lstUserTruc.Add(LichChinh);

                        foreach (int Userid in lstUserTruc)
                        {
                            oLichCaTrucItem.ID = 0;


                            oLichCaTrucItem.CreatedUser = new Microsoft.SharePoint.SPFieldLookupValue(Userid, "");
                            if (LichChinh == oLichCaTrucItem.CreatedUser.LookupId)
                                oLichCaTrucItem.DBPhanLoai = 1;
                            else oLichCaTrucItem.DBPhanLoai = 0;
                            oLichCaTrucItem.ThoiGianBatDauThucTe = DateTime.ParseExact($"{oLichCaTrucItem.NgayTruc.Value.ToString("dd/MM/yyyy")} {oDMCaTrucItem.ThoiGianBatDau}", "dd/MM/yyyy H:mm", CultureInfo.InvariantCulture);
                            oLichCaTrucItem.ThoiGianKetThucThucTe = DateTime.ParseExact($"{oLichCaTrucItem.NgayTruc.Value.ToString("dd/MM/yyyy")} {oDMCaTrucItem.ThoiGianKetThuc}", "dd/MM/yyyy H:mm", CultureInfo.InvariantCulture);
                            //string formattedDate = date.ToString("yyyy-MM-dd HH:mm:ss")

                            oLichCaTrucDA.UpdateObject<LichCaTrucItem>(oLichCaTrucItem);
                        }
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    string outb = oLichCaTrucDA.DeleteObject(oGridRequest.ItemID);
                    if (string.IsNullOrEmpty(outb))
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb;
                    break;
                case "DELETE-MULTI":
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oLichCaTrucDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oLichCaTrucDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLichCaTrucItem = oLichCaTrucDA.GetByIdToObjectSelectFields<LichCaTrucItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oLichCaTrucItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}