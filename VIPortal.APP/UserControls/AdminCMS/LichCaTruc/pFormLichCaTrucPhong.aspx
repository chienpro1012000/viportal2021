<%@ page language="C#" autoeventwireup="true" codebehind="pFormLichCaTrucPhong.aspx.cs" inherits="VIPortalAPP.pFormLichCaTrucPhong" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-LichCaTruc" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oLichCaTruc.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" name="fldGroup" id="fldGroup" value="<%=oLichCaTruc.fldGroup.LookupId %>" />
        <div class="form-group row">
            <label for="NgayTruc" class="col-sm-2 control-label">Ngày trực</label>
            <div class="col-sm-10">
                <input type="text" name="NgayTruc" id="NgayTruc" value="<%:string.Format("{0:dd/MM/yyyy}",oLichCaTruc.NgayTruc)%>" class="form-control input-datetime" />
            </div>
        </div>
        <div class="form-group row">
            <label for="CaTruc" class="col-sm-2 control-label">Ca trực</label>
            <div class="col-sm-10">
                <select data-selected="<%:oLichCaTruc.CaTruc.LookupId%>" data-url="/UserControls/AdminCMS/DMCaTruc/pAction.asp?do=QUERYDATA&fldGroup=<%=oLichCaTruc.fldGroup.LookupId %>" data-place="Chọn ca trực" name="CaTruc" id="CaTruc" class="form-control"></select>
            </div>
        </div>
        <%if(ItemID == 0){ %>
        <div class="form-group row">
            <label for="CreatedUser" class="col-sm-2 control-label">Cán bộ trực chính</label>
            <div class="col-sm-10">
                <select data-selected="" data-url="/UserControls/AdminCMS/LUser/pAction.asp?do=QUERYDATA&&Groups=<%=CurentUser.Groups.ID %>&UserPhongBan=<%=CurentUser.UserPhongBan.ID %>&GetUserPhongBan=true" data-place="Chọn cán bộ tham gia" name="CreatedUser" id="CreatedUser" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <label for="ThanhPhanThamGia" class="col-sm-2 control-label">Cán bộ trực phụ</label>
            <div class="col-sm-10">
                <select data-selected="" multiple="multiple" data-url="/UserControls/AdminCMS/LUser/pAction.asp?do=QUERYDATA&&Groups=<%=CurentUser.Groups.ID %>&UserPhongBan=<%=CurentUser.UserPhongBan.ID %>&GetUserPhongBan=true" data-place="Chọn cán bộ tham gia" name="ThanhPhanThamGia" id="ThanhPhanThamGia" class="form-control"></select>
            </div>
        </div>
        <%}else{ %>
        <div class="form-group row">
            <label for="CreatedUser" class="col-sm-2 control-label">Cán bộ Trực</label>
            <div class="col-sm-10">
                <select data-selected="<%:oLichCaTruc.CreatedUser.LookupId%>" data-url="/UserControls/AdminCMS/LUser/pAction.asp?do=QUERYDATA&&Groups=<%=CurentUser.Groups.ID %>&UserPhongBan=<%=CurentUser.UserPhongBan.ID %>&GetUserPhongBan=true" data-place="Chọn cán bộ tham gia" name="CreatedUser" id="CreatedUserUpdate" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <label for="DBPhanLoai" class="col-sm-2 control-label">Chức vụ</label>
            <div class="col-sm-10">
                <select data-selected="<%:oLichCaTruc.DBPhanLoai%>" name="DBPhanLoai" id="DBPhanLoai" class="form-control">
                    <option value="1">Trực chính</option>
                    <option value="0">Trực Phụ</option>
                </select>
            </div>
        </div>
        <%} %>
        <div class="form-group row">
            <label for="LichGhiChu" class="col-sm-2 control-label">Ghi chú</label>
            <div class="col-sm-10">
                <textarea name="LichGhiChu" id="LichGhiChu" class="form-control"><%:oLichCaTruc.LichGhiChu%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            if ($("#DBPhanLoai").length)
                $("#DBPhanLoai").val('<%:oLichCaTruc.DBPhanLoai%>');
                $("#DBPhanLoai").select2();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oLichCaTruc.ListFileAttach)%>'
            });
            
            //$("#NgayTruc").focus();
            $("#CaTruc").smSelect2018V2({
                dropdownParent: "#frm-LichCaTruc"
            });
            //CreatedUserUpdate
            $("#CreatedUserUpdate").smSelect2018V2({
                dropdownParent: "#frm-LichCaTruc"
            });
            $("#ThanhPhanThamGia").smSelect2018V2({
                dropdownParent: "#frm-LichCaTruc"
            });
            //ThanhPhanThamGiaChinh
            $("#CreatedUser").smSelect2018V2({
                dropdownParent: "#frm-LichCaTruc"
            });
            $("#frm-LichCaTruc").validate({
                rules: {
                    NgayTruc: "required",
                    CaTruc: "required",
                    //CreatedUser: "required",
                    //ThanhPhanThamGia: "required"
                },
                messages: {
                    //Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/LichCaTruc/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-LichCaTruc").trigger("click");
                            $(form).closeModal();
                            $("#lichDonVi_div_parent").data("smGrid2018").RefreshGrid();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-LichCaTruc").viForm();
        });
    </script>
</body>
</html>
