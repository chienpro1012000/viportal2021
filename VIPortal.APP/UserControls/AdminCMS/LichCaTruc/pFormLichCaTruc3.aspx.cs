﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils;
using VIPortalAPP;
using ViPortalData;

namespace VIPortal.APP.UserControls.AdminCMS.LichCaTruc
{
    public partial class pFormLichCaTruc3 : pFormBase
    {
        public LichCaTrucItem oLichCaTruc { get; set; }
        public List<DMCaTrucJson> lstDMCaTruc { get; private set; }
        public List<LichHienThi> lstLichHienThi { get; private set; }

        public List<LUserJson> lstUser = new List<LUserJson>();
        protected void Page_Load(object sender, EventArgs e)
        {
            oLichCaTruc = new LichCaTrucItem();
            lstLichHienThi = new List<LichHienThi>();
            lstDMCaTruc = new List<DMCaTrucJson>();
            if (!string.IsNullOrEmpty(Request["fldGroup"]))
            {
                oLichCaTruc.fldGroup = new Microsoft.SharePoint.SPFieldLookupValue(Convert.ToInt32(Request["fldGroup"]), "");
            }
            LUserDA lUserDA = new LUserDA();
            if (oLichCaTruc.fldGroup.LookupId == CurentUser.Groups.ID)
            {
                lstUser = lUserDA.GetListJson(new LUserQuery() { Groups = CurentUser.Groups.ID, Length = 0, FieldOrder = "DMSTT", Ascending = true });
            }
            else
            {
                lstUser = lUserDA.GetListJson(new LUserQuery() { UserPhongBan = CurentUser.UserPhongBan.ID, GetUserPhongBan = true, FieldOrder = "DMSTT", Ascending = true });
            }
            DMCaTrucDA dMCaTrucDA = new DMCaTrucDA(UrlSite);
            lstDMCaTruc = dMCaTrucDA.GetListJson(new DMCaTrucQuery() { FieldOrder = "Title", Ascending = true, fldGroup = oLichCaTruc.fldGroup.LookupId });
            LichCaTrucQuery oLichCaTrucQuery = new LichCaTrucQuery(HttpContext.Current.Request);
            oLichCaTrucQuery.Length = 0;
            lstDMCaTruc = lstDMCaTruc.Where(x => x.fldGroup != null && x.fldGroup.ID == oLichCaTrucQuery.fldGroup).ToList();
            //DataGridRender dataGridRender = new DataGridRender();
            LichCaTrucDA lichCaTrucDA = new LichCaTrucDA(UrlSite);
            List<LichCaTrucJson> lstLichCaTruc = lichCaTrucDA.GetListJson(oLichCaTrucQuery);
            int SONgayFix = oLichCaTrucQuery.DenNgay.Value.Subtract(oLichCaTrucQuery.TuNgay.Value).Days + 1;
            for (int i = 0; i < SONgayFix; i++)
            {

                DateTime curdate = oLichCaTrucQuery.TuNgay.Value.AddDays(i);
                LichHienThi lichHienThi = new LichHienThi()
                {
                    Thu = curdate.GetThuByNgay(),
                    Ngay = string.Format("{0:dd/MM/yyyy}", curdate)
                };
                lstLichHienThi.Add(lichHienThi);
            }
        }
    }
}