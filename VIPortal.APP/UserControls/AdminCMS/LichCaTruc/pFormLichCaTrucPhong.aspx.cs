using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public partial class pFormLichCaTrucPhong : pFormBase
    {
        
        public LichCaTrucItem oLichCaTruc {get;set;}
        protected void Page_Load(object sender, EventArgs e)
        {
            
			oLichCaTruc = new LichCaTrucItem();
            if (ItemID > 0)
            {
                LichCaTrucDA oLichCaTrucDA = new LichCaTrucDA(UrlSite);
                oLichCaTruc = oLichCaTrucDA.GetByIdToObject<LichCaTrucItem>(ItemID);
            }
            else
            {
                if (!string.IsNullOrEmpty(Request["fldGroup"]))
                {
                    oLichCaTruc.fldGroup = new Microsoft.SharePoint.SPFieldLookupValue(Convert.ToInt32(Request["fldGroup"]), "");
                }
            }
        }
    }
}