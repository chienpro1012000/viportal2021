﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils;
using VIPortalAPP;
using ViPortalData;

namespace VIPortal.APP.UserControls.AdminCMS.LichCaTruc
{
    public partial class pFormLichCaTruc2 : pFormBase
    {
        public LichCaTrucItem oLichCaTruc { get; set; }
        public  List<LichHienThi> lstLichHienThi = new List<LichHienThi>();
        protected void Page_Load(object sender, EventArgs e)
        {
            lstLichHienThi = new List<LichHienThi>();
            oLichCaTruc = new LichCaTrucItem();
            if (!string.IsNullOrEmpty(Request["fldGroup"]))
            {
                oLichCaTruc.fldGroup = new Microsoft.SharePoint.SPFieldLookupValue(Convert.ToInt32(Request["fldGroup"]), "");
            }
            List<LUserJson> lstUser = new List<LUserJson>();
            LUserDA lUserDA = new LUserDA();
            if(oLichCaTruc.fldGroup.LookupId == CurentUser.Groups.ID)
            {
                lstUser = lUserDA.GetListJson(new LUserQuery() { Groups = CurentUser.Groups.ID, Length = 0 });
            }
            else
            {
                lstUser = lUserDA.GetListJson(new LUserQuery() { UserPhongBan = CurentUser.UserPhongBan.ID, GetUserPhongBan = true });
            }
            //string optionSelectChinh = "<select  class=\"form-control\"></select>";
            string optionUser = string.Join("\r\n", lstUser.Select(x => $"<option value=\"{x.ID}\">{x.Title}-{x.UserPhongBan.Title}</option>"));

            //LichCaTrucQuery oLichCaTrucQuery = new LichCaTrucQuery(this.Request);
            DMCaTrucDA dMCaTrucDA = new DMCaTrucDA(UrlSite);
            List<DMCaTrucJson> lstDMCaTruc = dMCaTrucDA.GetListJson(new DMCaTrucQuery() { FieldOrder = "Title", Ascending = true, });

            LichCaTrucQuery oLichCaTrucQuery = new LichCaTrucQuery(HttpContext.Current.Request);
            oLichCaTrucQuery.Length = 0;
            lstDMCaTruc = lstDMCaTruc.Where(x => x.fldGroup != null && x.fldGroup.ID == oLichCaTrucQuery.fldGroup).ToList();
            //DataGridRender dataGridRender = new DataGridRender();
            LichCaTrucDA lichCaTrucDA = new LichCaTrucDA(UrlSite);
            List<LichCaTrucJson> lstLichCaTruc = lichCaTrucDA.GetListJson(oLichCaTrucQuery);
            int SONgayFix = oLichCaTrucQuery.DenNgay.Value.Subtract(oLichCaTrucQuery.TuNgay.Value).Days + 1;
            for (int i = 0; i < SONgayFix; i++)
            {
               
                DateTime curdate = oLichCaTrucQuery.TuNgay.Value.AddDays(i);
                LichHienThi lichHienThi = new LichHienThi()
                {
                    Thu = curdate.GetThuByNgay(),
                    Ngay = string.Format("{0:dd/MM/yyyy}", curdate)
                };
                int DayOfWeek = (int)curdate.DayOfWeek;
                string strKetQua = "";
                strKetQua = strKetQua + "<table class='tblChiTietLich' cellpadding='0' cellspacing='0'>";
                foreach (DMCaTrucJson oDMCaTrucJson in lstDMCaTruc)
                {
                    bool isCaCuaNgay = false;
                    // curdate.DayOfWeek
                    if ((DayOfWeek == 0 || DayOfWeek == 6) && oDMCaTrucJson.LoaiCaTruc == 1)//nghỉ t7 cn
                    {
                        //là ngày nghỉ.
                        isCaCuaNgay = true;
                    }
                    if ((DayOfWeek != 0 && DayOfWeek != 6) && oDMCaTrucJson.LoaiCaTruc == 0)//nghỉ t7 cn
                    {
                        isCaCuaNgay = true;
                    }
                    if (isCaCuaNgay)
                    {
                        string LichTrucChinhSlected = string.Join(",", lstLichCaTruc.Where(x => x.DBPhanLoai == 1 && x.CaTruc.ID == oDMCaTrucJson.ID && x.NgayTruc == curdate).Select(y=>y.CreatedUser.ID).Distinct());
                        string LichTrucPhuSlected = string.Join(",", lstLichCaTruc.Where(x => x.DBPhanLoai == 0 && x.CaTruc.ID == oDMCaTrucJson.ID && x.NgayTruc == curdate).Select(y => y.CreatedUser.ID).Distinct());
                        strKetQua += ("<tr>" +
                            $"<td style=\"width: 130px\">{oDMCaTrucJson.Title}</td>" +
                            "<td style=\"width:30%;\">" +
                            $"<select data-selected=\"{LichTrucChinhSlected}\" class=\"selectuserchinh\" name=\"selectuserchinh{oDMCaTrucJson.ID}_{string.Format("{0:ddMMyyyy}", curdate)}\" id=\"selectusercachinh{oDMCaTrucJson.ID}_{string.Format("{0:ddMMyyyy}", curdate)}\" class=\"form-control\">" + "<option></option>" + optionUser + "</select>" + 
                            "</td>" +
                            "<td>" +
                            $"<select data-selected=\"{LichTrucPhuSlected}\" class=\"selectuserphu\" name=\"selectusercaphu{oDMCaTrucJson.ID}_{string.Format("{0:ddMMyyyy}", curdate)}\" multiple=\"\" id=\"selectusercaphu{oDMCaTrucJson.ID}_{string.Format("{0:ddMMyyyy}", curdate)}\" class=\"form-control\">" + optionUser + "</select>" + "</td>" +
                            "</tr>");
                    }
                }
                strKetQua = strKetQua + "</table>";
                lichHienThi.Content = strKetQua;
                lstLichHienThi.Add(lichHienThi);

                
            }
        }
    }
}