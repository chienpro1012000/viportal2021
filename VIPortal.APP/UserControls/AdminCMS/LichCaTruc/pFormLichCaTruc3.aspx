﻿<%@ page language="C#" autoeventwireup="true" codebehind="pFormLichCaTruc3.aspx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.LichCaTruc.pFormLichCaTruc3" %>

<style type="text/css">
    .clslichcatruc, .lichheader {
        width: 100%;
        border-collapse: collapse;
    }

    table.clslichcatruc, table.clslichcatruc td, table.clslichcatruc th {
        border: 1px solid;
        padding: 5px;
    }

    .selectuserchinh {
        width: 100%;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {

        $("#CreatedUser").select2({
            allowClear: true,
            placeholder: "Chọn cán bộ"
        });
        $("#frm-LichCaTruc").validate({
            rules: {
                CreatedUser: "required"
            },
            messages: {
                //Title: "Vui lòng nhập tiêu đề"
            },
            submitHandler: function (form) {
                loading();
                $.post("/UserControls/AdminCMS/LichCaTruc/pAction.asp", $(form).viSerialize(), function (result) {
                    Endloading();
                    if (result.State == 2) {
                        BootstrapDialog.show({
                            title: "Lỗi",
                            message: result.Message
                        });
                    }
                    else {
                        showMsg(result.Message);
                        //$("#btn-find-LichCaTruc").trigger("click");
                        $(form).closeModal();
                        $("#lich_div_parent").data("smGrid2018").RefreshGrid();
                    }
                }).always(function () { });
            }
        });
        $("#frm-LichCaTruc").viForm();
    });
</script>
<body>
    <form id="frm-LichCaTruc" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oLichCaTruc.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" name="fldGroup" id="fldGroup" value="<%=oLichCaTruc.fldGroup.LookupId %>" />
        <input type="hidden" name="SearchTuNgay" id="SearchTuNgay" value="<%=Request["SearchTuNgay"] %>" />
        <input type="hidden" name="SearchDenNgay" id="SearchDenNgay" value="<%=Request["SearchDenNgay"] %>" />
        <div class="form-group row">
            <label for="CreatedUser" class="col-sm-2 control-label">Nhân sự</label>
            <div class="col-sm-10">
                <select id="CreatedUser" name="CreatedUser" class="form-control">
                    <% foreach (ViPortalData.LUserJson item in lstUser)
                    { %>
                    <option value="<%=item.ID %>"><%=item.Title %></option>
                    <%} %>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="DBPhanLoai" class="col-sm-2 control-label">Vai trò</label>
            <div class="col-sm-10">
                <div class="form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" value="1" checked="checked" name="DBPhanLoai">Trực chính
                    </label>
                </div>
                <div class="form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" value="0" name="DBPhanLoai">Trực phụ
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-12">
                <table class="clslichcatruc">
                    <tr>
                        <th style="width: 200px;">Ngày</th>
                        <% foreach (VIPortalAPP.DMCaTrucJson item in lstDMCaTruc)
                    { %>
                        <th><%=item.Title %></th>
                        <%} %>
                    </tr>
                    <% foreach (VIPortalAPP.LichHienThi item in lstLichHienThi)
                    { %>
                    <tr>
                        <td><%=item.Thu %><br />
                            <%=item.Ngay %></td>

                        <% foreach (VIPortalAPP.DMCaTrucJson itemca in lstDMCaTruc)
                    { %>
                        <td>
                            <input type="checkbox" id="<%=item.Ngay.Replace("/","") %>_<%=itemca.ID %>" name="<%=item.Ngay.Replace("/","") %>_<%=itemca.ID %>" />
                        </td>
                        <%} %>
                    </tr>
                    <%} %>
                </table>
            </div>
        </div>

        <div class="clearfix"></div>
    </form>


</body>
