﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewCoQuanBanHanh.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.CoQuanBanHanh.pViewCoQuanBanHanh" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		         <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên Cơ Quan</label>
            <div class="col-sm-10">
               <%=oCoQuanBanHanh.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="HoTen" class="col-sm-2 control-label">Số thứ tự</label>
            <div class="col-sm-4">
                 <%:(oCoQuanBanHanh!= null &&  oCoQuanBanHanh.DMSTT>0)?oCoQuanBanHanh.DMSTT+"": "" %>
            </div>
               <label class="control-label col-sm-2">Hiển thị</label>
            <div class="col-sm-4">
                <%=oCoQuanBanHanh.DMHienThi? "Có" : "Không"%>
                </div>
        </div>
       <div class="form-group row">
            <label for="DMMoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
               <%:oCoQuanBanHanh.DMMoTa%>
            </div>
        </div>
        <div class="form-group row">
            <label for="DMVietTat" class="col-sm-2 control-label">Viết tắt</label>
            <div class="col-sm-4">
                <%:oCoQuanBanHanh.DMVietTat%>
            </div>
        </div>
        <%--<div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <%for(int i=0; i< oCoQuanBanHanh.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oCoQuanBanHanh.ListFileAttach[i].Url %>"><%=oCoQuanBanHanh.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>--%>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%=string.Format("{0:dd-MM-yyyy}", oCoQuanBanHanh.Created)%>
            </div>
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}",oCoQuanBanHanh.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
                <%=oCoQuanBanHanh.Author.LookupValue%>
            </div>
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
                <%=oCoQuanBanHanh.Editor.LookupValue%>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>