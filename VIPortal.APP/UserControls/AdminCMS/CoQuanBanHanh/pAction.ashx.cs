﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData.CoQuanBanHanh;

namespace VIPortal.APP.UserControls.AdminCMS.CoQuanBanHanh
{
    /// <summary>
    /// Summary description for pAction
    /// </summary>
    public class pAction : hActionBase, IHttpHandler 
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            CoQuanBanHanhDA oCoQuanBanHanhDA = new CoQuanBanHanhDA(UrlSite);
            CoQuanBanHanhItem oCoQuanBanHanhItem = new CoQuanBanHanhItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    var oData = oCoQuanBanHanhDA.GetListJson(new CoQuanBanHanhQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oCoQuanBanHanhDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "CoQuanBanHanh", "QUERYDATA", 0, "Xem danh sách cơ quan ban hành");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataDanhMucChucVu = oCoQuanBanHanhDA.GetListJson(new CoQuanBanHanhQuery(context.Request));
                    treeViewItems = oDataDanhMucChucVu.Select(x => new TreeViewItem()
                    {
                        key = x.DMDescription,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oCoQuanBanHanhDA.GetListJson(new CoQuanBanHanhQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oCoQuanBanHanhItem = oCoQuanBanHanhDA.GetByIdToObject<CoQuanBanHanhItem>(oGridRequest.ItemID);
                        oCoQuanBanHanhItem.UpdateObject(context.Request);
                        if (oCoQuanBanHanhDA.CheckExit(oGridRequest.ItemID, oCoQuanBanHanhItem.Title) == 0)
                        {
                            oCoQuanBanHanhDA.UpdateObject<CoQuanBanHanhItem>(oCoQuanBanHanhItem);
                            AddLog("UPDATE", "CoQuanBanHanh", "UPDATE", oGridRequest.ItemID, $"Cập nhật cơ quan ban hành {oCoQuanBanHanhItem.Title}");
                            oResult.Message = "Cập nhật thành công";
                        }
                        else
                        {
                            oResult = new ResultAction() { State = ActionState.Error, Message = "Tên cơ quan ban hành đã tồn tại" };
                        }
                    }
                    else
                    {
                        oCoQuanBanHanhItem.UpdateObject(context.Request);
                        if (oCoQuanBanHanhDA.CheckExit(oGridRequest.ItemID, oCoQuanBanHanhItem.Title) == 0)
                        {
                            oCoQuanBanHanhDA.UpdateObject<CoQuanBanHanhItem>(oCoQuanBanHanhItem);
                            AddLog("UPDATE", "CoQuanBanHanh", "ADD", oGridRequest.ItemID, $"Thêm mới cơ quan ban hành {oCoQuanBanHanhItem.Title}");
                            oResult.Message = "Thêm mới thành công";
                        }
                        else
                        {
                            oResult = new ResultAction() { State = ActionState.Error, Message = "Tên cơ quan ban hành đã tồn tại" };
                        }
                    }
                   
                    break;
                case "DELETE":
                    var outb = oCoQuanBanHanhDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "CoQuanBanHanh", "DELETE", oGridRequest.ItemID, $"Xóa cơ quan ban hành {outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oCoQuanBanHanhDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    AddLog("DELETE-MULTI", "CoQuanBanHanh", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều cơ quan ban hành");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oCoQuanBanHanhDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "CoQuanBanHanh", "APPROVED", oGridRequest.ItemID, $"Duyệt cơ quan ban hành{oCoQuanBanHanhDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oCoQuanBanHanhDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "CoQuanBanHanh", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt cơ quan ban hành{oCoQuanBanHanhDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oCoQuanBanHanhItem = oCoQuanBanHanhDA.GetByIdToObjectSelectFields<CoQuanBanHanhItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oCoQuanBanHanhItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}