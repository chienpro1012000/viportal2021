﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_CoQuanBanHanh.ascx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.CoQuanBanHanh.UC_CoQuanBanHanh" %>
<div role="body-data" data-title="cơ quan ban hành" class="content_wp" data-action="/UserControls/AdminCMS/CoQuanBanHanh/pAction.asp" data-form="/UserControls/AdminCMS/CoQuanBanHanh/pFormCoQuanBanHanh.aspx" data-view="/UserControls/AdminCMS/CoQuanBanHanh/pViewCoQuanBanHanh.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="CoQuanBanHanhSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title,DMSTT,DMVietTat" />
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <button class="btn btn-primary act-add" data-per="01070302" type="button">Thêm mới</button>
                    </p>
                </div>
            </div>

            <div class="clsgrid table-responsive">

                <table id="tblCoQuanBanHanh" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>

            </div>
        </div>

    </div>
</div>
<script type="text/javascript">
  
    $(document).ready(function () {

        var $tagvalue = $(".content_wp");
        var $tblCoQuanBanHanh = $("#tblCoQuanBanHanh").viDataTable(
            {
                "frmSearch": "CoQuanBanHanhSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "sWidth": "20px",
                            "mData": function (o) {
                                return '';
                            },
                            "name": "STT", "sTitle": "STT", className: "txtcenter"
                        },
                        {
                            "mData": "ID",
                            "name": "ID", "visible": false, "sTitle": "ID",
                            "sWidth": "30px",
                        }, {
                            "mData": function (o) {

                                return '<a data-id="' + o.ID + '" >' + o.DMVietTat + '</a>';
                            },
                            "name": "DMVietTat", "sTitle": "Danh mục viết tắt",
                            "sWidth": "120px",
                        },
                        {
                            "mData": function (o) {
                                if (o.Title != null)
                                    return '<a data-ItemID="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                                else
                                    return "";
                            },
                            "name": "Title", "sTitle": "Tên danh mục "
                        },
                        //{
                        //    "mData": "DMMoTa",
                        //    "name": "DMMoTa", "sTitle": "Mô tả"
                        //},
                        {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {
                                if (o._ModerationStatus == 0)

                                    return '<a data-per="01070305" title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a data-per="01070305" title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }

                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" title="Sửa" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        }, {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE" title="Xóa"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                "order": [1, 'desc']
            });
        $tblCoQuanBanHanh.on('order.dt search.dt', function () {
            $tblCoQuanBanHanh.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    });
</script>
