﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData.CoQuanBanHanh;

namespace VIPortal.APP.UserControls.AdminCMS.CoQuanBanHanh
{
    public partial class pFormCoQuanBanHanh : pFormBase
    {
        public CoQuanBanHanhItem oCoQuanBanHanh { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oCoQuanBanHanh = new CoQuanBanHanhItem();
            if (ItemID > 0)
            {
                CoQuanBanHanhDA oCoQuanBanHanhDA = new CoQuanBanHanhDA(UrlSite);
                oCoQuanBanHanh = oCoQuanBanHanhDA.GetByIdToObject<CoQuanBanHanhItem>(ItemID);
            }
        }
    }
}