﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormDanhBa.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.DanhBa.pFormDanhBa" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-DanhBa" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oDanhBa.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
             <label for="IsDonVi" class="col-sm-2 control-label">Đơn vị, tổ chức</label>
            <div class="col-sm-4">
                <input type="checkbox" name="IsDonVi" id="IsDonVi" value="true" <%=(oDanhBa.isOffice== "1") ? " checked" : ""%> />
            </div>
            <%--<label for="DMSTT" class="col-sm-2 control-label">Số thứ tự</label>
            <div class="col-sm-4">
                <input type="text" name="DMSTT" id="DMSTT" placeholder="Nhập số thứ tự" value="<%:oDanhBa.DMSTT%>" class="form-control" />
            </div>
           --%>
        </div>
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên đơn vị/cá nhân</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" placeholder="Nhập tên đơn vị/cá nhân" class="form-control" value="<%=oDanhBa.Title%>" />
            </div>
        </div>
        
        <div class="form-group row">
            <label for="DBDiaChi" class="col-sm-2 control-label">Địa chỉ</label>
            <div class="col-sm-10">
                <input type="text" name="DBDiaChi" id="DBDiaChi" placeholder="Nhập địa chỉ" value="<%:oDanhBa.DBDiaChi%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="DBEmail" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-4">
                <input type="text" name="DBEmail" id="DBEmail" placeholder="Nhập email" value="<%:oDanhBa.DBEmail%>" class="form-control" />
            </div>
            <label for="DBFax" class="col-sm-1 control-label">Fax</label>
            <div class="col-sm-5">
                <input type="text" name="DBFax" id="DBFax" placeholder="Nhập fax" value="<%:oDanhBa.DBFax%>" class="form-control" />
            </div>
        </div>        
        <div class="form-group row">
            <label for="DBSDT" class="col-sm-2 control-label">Số điện thoại</label>
            <div class="col-sm-4">
                <input type="text" name="DBSDT" id="DBSDT" placeholder="Nhập số điện thoại" value="<%:oDanhBa.DBSDT%>" class="form-control" />
            </div>
            <label for="DBWebsite" class="col-sm-1 control-label">Website</label>
            <div class="col-sm-5">
                <input type="text" name="DBWebsite" id="DBWebsite" placeholder="Nhập website" value="<%:oDanhBa.DBWebsite%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="ImageNews" class="col-sm-2 control-label">Ảnh đại diện</label>
            <div class="col-sm-10" data-role="groupImageDD">
                <div id="imgImageNews" style="float: left; padding-right: 10px;">
                    <div class="include-library" style="display: inline-block; clear: both;">
                        <input type="hidden" name="ImageNews" id="ImageNews" value="<%:oDanhBa.ImageNews%>" />
                        <img title="Ảnh đại diện" id="srcImageNews" src="<%=oDanhBa.ImageNews%>" />
                    </div>
                </div>
                <button id="btnImageNews" type="button" class="btn  btn-success">Ảnh đại diện</button>
                <button id="btnXoaImageNews" type="button" class="btn btn-danger">Xóa ảnh</button>
            </div>
        </div>
        <%--<div class="form-group row">
            <label for="DBPhanLoai" class="col-sm-2 control-label">Phân loại</label>
            <div class="col-sm-4">
                <input type="text" name="DBPhanLoai" id="DBPhanLoai" placeholder="Nhập phân loại" value="<%:oDanhBa.DBPhanLoai%>" class="form-control" />
            </div>
        </div>--%>
        <div class="form-group row">
            <label for="LookUp" class="col-sm-2 control-label">Đơn vị cha</label>
            <div class="col-sm-10">
                <select data-selected="<%:oDanhBa.DBParentPB.LookupId%>" data-url="/UserControls/AdminCMS/DanhBa/pAction.asp?do=QUERYDATA&isOffice=1" data-place="Chọn đơn vị cha" name="DBParentPB" id="DBParentPB" class="form-control"></select>
            </div>
        </div>
        
        
        <div class="clearfix"></div>
    </form>
    <script type="text/javascript">
        $(document).ready(function () {
            
            $("#DBParentPB").smSelect2018V2({        
                dropdownParent: "#frm-DanhBa"
            });
            $("#DBChucVu").smSelect2018V2({
                dropdownParent: "#frm-DanhBa"
            });
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oDanhBa.ListFileAttach)%>'
           });
           $("#Title").focus();
           $(".input-datetime").daterangepicker({
               singleDatePicker: true,
               locale: { format: 'DD/MM/YYYY' }
           });
           $(".form-group select").select2({
               dropdownParent: $(".modal-body"),
               placeholder: "Chọn"
           });
           $("#btnImageNews").click(function () {
                $elementId = $(this);
                CKFinder.popup({
                    chooseFiles: true,
                    width: 800,
                    height: 600,
                    onInit: function (finder) {
                        finder.on('files:choose', function (evt) {
                            var file = evt.data.files.first();
                            //var output = document.getElementById(elementId);
                            //console.log(file.getUrl());
                            //$("#ImageNews").value(file.getUrl());
                            var urlfileimg = file.getUrl();
                            document.getElementById("ImageNews").value = urlfileimg;
                            document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                        });
                        finder.on('file:choose:resizedImage', function (evt) {
                            //var output = document.getElementById(elementId);
                            console.log(evt.data);
                            var urlfileimg = evt.data.resizedUrl;
                            document.getElementById("ImageNews").value = urlfileimg;
                            document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                        });
                    }
                });
            });
            $("#btnXoaImageNews").click(function () {
                document.getElementById("ImageNews").value = "";
                document.getElementById("srcImageNews").setAttribute("src", "");
            });

           $("#frm-DanhBa").validate({
               ignore: [],
               rules: {
                   Title: "required",
                   DBEmail: "required",
                   DBSDT: {
                       phoneNumber: true, required: true,
                   }
               },
               messages: {
                   Title: "Vui lòng nhập tên đơn vị/cá nhân",
                   DBEmail: "Vui lòng nhập email đơn vị/cá nhân",
                   DBSDT: "Vui lòng nhập số điện thoại",                   
               },
               submitHandler: function (form) {
                   $.post("/UserControls/AdminCMS/DanhBa/pAction.asp", $(form).viSerialize(), function (result) {
                       if (result.State == 2) {
                           BootstrapDialog.show({
                               title: "Lỗi",
                               message: result.Message
                           });
                       }
                       else {
                           showMsg(result.Message);
                           //$("#btn-find-DanhBa").trigger("click");
                           $(form).closeModal();
                           //$('#tblDanhBa').DataTable().ajax.reload();
                           loadAjaxContent('/UserControls/AdminCMS/DanhBa/pListDanhBa.aspx', "#tblDanhBa");
                       }
                   }).always(function () { });
               }
           });
            $("#frm-DanhBa").viForm();
       });
    </script>
</body>
</html>


