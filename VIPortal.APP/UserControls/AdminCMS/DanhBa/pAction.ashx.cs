﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils;
using ViPortal_Utils.Base;
using ViPortalData;
namespace VIPortal.APP.UserControls.AdminCMS.DanhBa
{

    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {

        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            DanhBaDA oDanhBaDA = new DanhBaDA();
            DanhBaItem oDanhBaItem = new DanhBaItem();
            List<DanhBaJson> lstDanhBa = new List<DanhBaJson>();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    var oData = oDanhBaDA.GetListJson(new DanhBaQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oDanhBaDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "DanhBa", "QUERYDATA", 0, "Xem danh sách danh bạ");
                    break;
                case "QUERYTREEROOT":
                    lstDanhBa = new List<DanhBaJson>();
                    lstDanhBa = oDanhBaDA.GetListJson(new DanhBaQuery(context.Request));
                    List<TreeViewItemV1> lstTreeRoot = new List<TreeViewItemV1>();
                    if (Convert.ToInt32(context.Request["ItemID"]) > 0)
                    {
                        List<int> lstIDCha = new List<int>();
                        lstIDCha = GetAllIDCapCha(Convert.ToInt32(context.Request["ItemID"]), oDanhBaDA);
                        lstTreeRoot = buildTreeJson2(lstIDCha, lstDanhBa, oDanhBaDA);
                    }
                    else
                        lstTreeRoot = buildTreeJson(Convert.ToInt32(context.Request["IDParent"]), lstDanhBa);
                    oResult.OData = lstTreeRoot;
                    AddLog("QUERYDATA", "DanhBa", "QUERYDATA", 0, "Xem danh sách danh bạ");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataDanhBa = oDanhBaDA.GetListJson(new DanhBaQuery(context.Request));
                    treeViewItems = oDataDanhBa.Select(x => new TreeViewItem()
                    {
                        key = x.DBNhom,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;

                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oDanhBaDA.GetListJson(new DanhBaQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oDanhBaDA.CheckExit(oGridRequest.ItemID, context.Request["DBEmail"], "DBEmail") == 0) //check trùng DBEmail
                    {
                        if (oDanhBaDA.CheckExit(oGridRequest.ItemID, context.Request["DBSDT"], "DBSDT") == 0) //check trùng DBEmail
                        {
                            if (oGridRequest.ItemID > 0)
                            {
                                oDanhBaItem = oDanhBaDA.GetByIdToObject<DanhBaItem>(oGridRequest.ItemID);
                                oDanhBaItem.UpdateObject(context.Request);
                                if (oDanhBaItem.IsDonVi)
                                    oDanhBaItem.isOffice = "1";
                                else
                                    oDanhBaItem.isOffice = "";
                                oDanhBaDA.UpdateObject<DanhBaItem>(oDanhBaItem);
                                AddLog("UPDATE", "DanhBa", "UPDATE", oGridRequest.ItemID, $"Cập nhật danh bạ {oDanhBaItem.Title}");
                            }
                            else
                            {
                                oDanhBaItem.UpdateObject(context.Request);
                                if (oDanhBaItem.IsDonVi)
                                    oDanhBaItem.isOffice = "1";
                                oDanhBaDA.UpdateObject<DanhBaItem>(oDanhBaItem);
                                AddLog("UPDATE", "DanhBa", "ADD", oGridRequest.ItemID, $"Thêm mới danh bạ {oDanhBaItem.Title}");
                            }
                            oResult.Message = "Lưu thành công";
                        }
                        else
                        {
                            oResult = new ResultAction() { State = ActionState.Error, Message = $"Số điện thoại {oDanhBaItem.DBSDT} đã tồn tại trong hệ thống" };
                        }
                    }
                    else
                    {
                        oResult = new ResultAction() { State = ActionState.Error, Message = $"Email {oDanhBaItem.DBEmail} đã tồn tại trong hệ thống" };
                    }
                    break;
                case "DELETE":
                    var outb = oDanhBaDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "DanhBa", "DELETE", oGridRequest.ItemID, $"Xóa danh bạ {outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    AddLog("DELETE-MULTI", "DanhBa", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều danh bạ");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oDanhBaDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "DanhBa", "APPROVED", oGridRequest.ItemID, $"Duyệt danh bạ{oDanhBaDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";                   
                    oResult.ID = oGridRequest.ItemID;
                    break;
                case "PENDDING":
                    oDanhBaDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "DanhBa", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt danh bạ{oDanhBaDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    oResult.ID = oGridRequest.ItemID;
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDanhBaItem = oDanhBaDA.GetByIdToObjectSelectFields<DanhBaItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oDanhBaItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        public List<int> GetAllIDCapCha(int id, DanhBaDA danhBaDA)
        {
            List<int> lstIDCha = new List<int>();
            DanhBaItem oDanhBa = new DanhBaItem();
            oDanhBa = danhBaDA.GetByIdToObject<DanhBaItem>(id);
            if (oDanhBa.DBParentPB.LookupId > 0)
            {
                lstIDCha.Add(oDanhBa.DBParentPB.LookupId);
                lstIDCha.AddRange(GetAllIDCapCha(oDanhBa.DBParentPB.LookupId, danhBaDA));
            }
            return lstIDCha;
        }
        private List<TreeViewItemV1> buildTreeJson2(List<int> lstidparrent, List<DanhBaJson> lstDanhBa, DanhBaDA danhBaDA)
        {
            TreeViewItemV1 oTreeViewItem = new TreeViewItemV1();
            List<TreeViewItemV1> lstEntityJson = new List<TreeViewItemV1>();
            foreach (var item in lstDanhBa)
            {
                oTreeViewItem = new TreeViewItemV1();
                oTreeViewItem.key = Convert.ToString(item.ID);
                oTreeViewItem.title = item.Title;
                if (item.isOffice == "1")
                {
                    oTreeViewItem.folder = true;
                    oTreeViewItem.lazy = true;
                }
                oTreeViewItem.expanded = false;
                oTreeViewItem.email = item.DBEmail;
                oTreeViewItem.diachi = item.DBDiaChi;
                oTreeViewItem.dienthoai = item.DBFax;
                if (lstidparrent.Contains(item.ID))
                {
                    List<DanhBaJson> lstTemp = new List<DanhBaJson>();
                    lstTemp = danhBaDA.GetListJson(new DanhBaQuery()
                    {
                        Length = 0,
                        FieldOrder = "STT",
                        Ascending = true,
                        IDParent = item.ID,
                    });
                    if (lstTemp.Count > 0)
                    {
                        oTreeViewItem.children = new List<TreeViewItemV1>();
                        oTreeViewItem.children.AddRange(buildTreeJson2(lstidparrent, lstTemp, danhBaDA));
                    }
                    oTreeViewItem.expanded = true;
                }
                oTreeViewItem.duyet = (item._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? true : false;
                lstEntityJson.Add(oTreeViewItem);
            }
            return lstEntityJson;
        }
        private List<TreeViewItemV1> buildTreeJson(int idparrent, List<DanhBaJson> lstDanhBa)
        {
            TreeViewItemV1 oTreeViewItem ;
            List<TreeViewItemV1> lstEntityJson = new List<TreeViewItemV1>();
            foreach (var item in lstDanhBa)
            {
                oTreeViewItem = new TreeViewItemV1();
                oTreeViewItem.key = Convert.ToString(item.ID);
                oTreeViewItem.title = item.Title;
                if (item.isOffice == "1")
                {
                    oTreeViewItem.folder = true;
                    oTreeViewItem.lazy = true;
                }
                oTreeViewItem.expanded = false;
                oTreeViewItem.email = item.DBEmail;
                oTreeViewItem.diachi = item.DBDiaChi;
                oTreeViewItem.dienthoai = item.DBSDT;
                oTreeViewItem.duyet = (item._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? true : false;
                lstEntityJson.Add(oTreeViewItem);
            }
            return lstEntityJson;
        }
    }
}