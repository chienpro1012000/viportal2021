﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_DanhBa.ascx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.DanhBa.UC_DanhBa" %>
<div id="box-frm-DanhBa" role="body-data" data-title="danh bạ" class="content_wp" data-action="/UserControls/AdminCMS/DanhBa/pAction.asp" data-form="/UserControls/AdminCMS/DanhBa/pFormDanhBa.aspx" data-list="/UserControls/AdminCMS/DanhBa/pListDanhBa.aspx" data-view="/UserControls/AdminCMS/DanhBa/pViewDanhBa.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="card card-default color-palette-box">
                <div class="card-body">
                    <div class="clsmanager row">
                        <div class="col-sm-9">
                            <div id="DanhBaSearch" class="form-inline zonesearch">
                                <div class="form-group">
                                    <input type="hidden" name="do" id="do" value="QUERYDATA" />
                                    <label for="Keyword">Từ khóa</label>
                                    <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                                    <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title,DBSDT,DBEmail" />
                                </div>
                                <button type="button" id="btnSearch" class="btn btn-default act-search">Tìm kiếm</button>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <p class="text-right">
                                <button class="btn btn-primary act-add" data-per="010602" type="button">Thêm mới</button>
                            </p>
                        </div>
                    </div>
                    <div id="tblDanhBa">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        var dataUrl = { <%--UrlList: '<%=UrlList%>'--%> };
        var $tagvalue = $("#box-frm-DanhBa");
        $tagvalue.data("parameters", dataUrl);       
        loadAjaxContent($tagvalue.data("list"), "#tblDanhBa", dataUrl);
    });
    function SearchGridVBDH() {
        var $tagvalue = $("#box-frm-DanhBa");
        loadAjaxContent($tagvalue.data("list"), "#tblDanhBa", { Keyword: $("#Keyword").val(), SearchInAdvance: $("#SearchInAdvance").val() });
    }
    $("#btnSearch").click(function () {
        debugger;
        SearchGridVBDH();
    });
    $('#Keyword').keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            debugger;
            SearchGridVBDH();
            event.preventDefault();
        }
    });
</script>
