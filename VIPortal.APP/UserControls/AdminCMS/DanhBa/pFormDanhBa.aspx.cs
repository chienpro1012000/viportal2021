﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortal.APP.UserControls.AdminCMS.DanhBa
{
    public partial class pFormDanhBa : pFormBase
    {
        public DanhBaItem oDanhBa { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oDanhBa = new DanhBaItem();
            if (ItemID > 0)
            {
                DanhBaDA oDanhBaDA = new DanhBaDA();
                oDanhBa = oDanhBaDA.GetByIdToObject<DanhBaItem>(ItemID);
            }
            else
            {
                oDanhBa.DBParentPB = new Microsoft.SharePoint.SPFieldLookupValue(Convert.ToInt32(Request["IDParent"]), "");
            }
        }
    }
}