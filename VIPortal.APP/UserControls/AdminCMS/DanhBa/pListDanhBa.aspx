﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pListDanhBa.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.DanhBa.pListDanhBa" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        ul.fancytree-container {
            border: none !important;
        }

        table#tree td, table#tree td {
            padding: 5px;
        }

        .ui-dialog {
            z-index: 1;
        }

            .ui-dialog .ui-dialog-buttonpane button {
                padding: 0.4em 1em;
            }

            .ui-dialog .ui-dialog-titlebar-close {
                display: none;
            }
    </style>
</head>
<body>
   
    <script type="text/javascript">
        $(function () {
            $('#tree').fancytree({
                
                extensions: ['contextMenu', 'childcounter', 'table'],
                 source: {
                    <%if(!checkkey){%>
                     url: "/UserControls/AdminCMS/DanhBa/pAction.asp?do=QUERYTREEROOT&isOffice=&ItemID=<%=itemid%>&cap=1&length=0"
                    <%}else if(checkkey){%>
                        url: "/UserControls/AdminCMS/DanhBa/pAction.asp?do=QUERYTREEROOT&keyword=<%=oQuery.Keyword%>&SearchInAdvance=<%=searchIn%>"
                    <%}else if(!checkParent) {%>
                        url: "/UserControls/AdminCMS/DanhBa/pAction.asp?do=QUERYTREEROOT&IDParent=<%=oQuery.IDParent%>&length=0"
                    <%}%>                    
                },
                lazyLoad: function (event, data) {
                    var id = data.node.key;
                    data.result = {
                        url: "/UserControls/AdminCMS/DanhBa/pAction.asp?do=QUERYTREEROOT&IDParent=" + id + "&length=0",
                      //dataType: "json"
                    };  
                },
                table: {
                    indentation: 20,      // indent 20px per node level
                    nodeColumnIdx: 0,     // render the node title into the 2nd column
                    checkboxColumnIdx: 0  // render the checkboxes into the 1st column
                },
                renderColumns: function (event, data) {
                    var node = data.node,
                        $tdList = $(node.tr).find(">td");
                    $tdList.eq(1).text(node.data.diachi ?? "").addClass("alignRight");
                    $tdList.eq(2).text(node.data.dienthoai ?? "");
                    $tdList.eq(3).text(node.data.email ?? "");
                    $tdList.eq(4).html("<a href=\"javascript:View('" + node.key + "');\" class=\"act-view\" style=\"color:#000 !important\" data-ItemID=" + node.key + ">  <i class=\"fa fa-tasks\" aria-hidden=\"true\"></i>  </a>");
                    if (node.data.duyet == true) {
                        $tdList.eq(5).html("<a data-per=\"010605\" style=\"color:#000 !important\" class=\"act-penddingDanhBa\" data-id=" + node.key + ">  <i  class=\"fa fa-toggle-on\" style=\"color: green\"></i></a>");
                    } else {
                        $tdList.eq(5).html("<a data-per=\"010605\" class=\"act-approvedDanhBa\" style=\"color:#000 !important\" data-id=" + node.key + "> <i class=\"fa fa-toggle-off\"></i></a>");
                    }
                    $tdList.eq(6).html("<a class=\"act-edit\" data-ItemID=" + node.key + " style=\"color:#000 !important\">  <i class=\"fa fa-edit\"></i></a>");
                    $tdList.eq(7).html("<a class=\"act-deleteDanhBa\" data-do=\"DELETE\" data-ItemID=" + node.key +" style=\"color:#000 !important\"> <i class=\"fa fa-trash\"  aria-hidden=\"true\"></i></a>");
                },
                childcounter: {
                    deep: true,
                    hideZeros: true,
                    hideExpanded: true
                },
                contextMenu: {
                    menu: {
                        'add': { 'name': 'Thêm mới danh bạ', 'icon': 'add' },
                        'edit': { 'name': 'Sửa danh bạ', 'icon': 'edit' },
                        'delete': { 'name': 'Xóa danh bạ', 'icon': 'delete' },
                    },
                    
                    actions: function (node, action) {
                        switch (action) {
                            case "add":
                                openDialog("Thêm mới danh bạ", "/UserControls/AdminCMS/DanhBa/pFormDanhBa.aspx", { IDParent: node.key, do: "UPDATE" }, 1024);
                                break;
                            case "edit":
                                openDialog("Sửa danh bạ", "/UserControls/AdminCMS/DanhBa/pFormDanhBa.aspx", { ItemID: node.key, do: "UPDATE" }, 1024);
                                break;
                            case "delete":
                                DeleteFromDialog("/UserControls/AdminCMS/DanhBa/pAction.asp", { ItemID: node.key, do: "DELETE" },"/UserControls/AdminCMS/DanhBa/pListDanhBa.aspx");
                                break;
                        }
                    }
                }
            });
        });
        function EditDanhBa(arrID, TreeID) {
            $.post(encodeURI(urlForm), { "Action": "Save", "ItemID": "" + arrID + "" }, function (data) {
                $("#dialog-form").html(data).dialog(
                    { title: "Cập nhật nội dung", height: formHeight, width: formWidth }
                ).dialog("open");
            });
        }
    </script>

    <table id="tree" style="width: 100%">
        <thead>
            <tr>
                <th></th>
                <th>Địa chỉ</th>
                <th>Điện thoại</th>
                <th>Email</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <!-- Optionally define a row that serves as template, when new nodes are created: -->
        <tbody>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td style="text-align:center"></td>
                <td style="text-align:center"></td>
                <td style="text-align:center"></td>
                <td style="text-align:center"></td>
            </tr>
        </tbody>
    </table>
</body>
</html>
