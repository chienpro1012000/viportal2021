﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData;

namespace VIPortal.APP.UserControls.AdminCMS.DanhBa
{
    public partial class pViewDanhBa : pFormBase
    {
        public DanhBaItem oDanhBa { get; set; } 
        public int ItemID { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oDanhBa = new DanhBaItem();
            ItemID = Convert.ToInt32(Request["ItemID"]);
            var oDanhBaDA = new DanhBaDA();
            if (ItemID > 0)
                oDanhBa = oDanhBaDA.GetByIdToObject<DanhBaItem>(ItemID);
        }
    }
}