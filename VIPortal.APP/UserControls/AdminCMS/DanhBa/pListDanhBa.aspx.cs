﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortal.APP.UserControls.AdminCMS.DanhBa
{
    public partial class pListDanhBa : System.Web.UI.Page
    {
        public int itemid { get; set; }
        public string searchIn { get; set; }
        public bool checkkey = false;
        public bool checkParent = false;
        public DanhBaQuery oQuery { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            var queryR = new QuerySearch(Request);
            oQuery = new DanhBaQuery(this.Request);
            searchIn = Request["SearchInAdvance"];
            if (!string.IsNullOrEmpty(oQuery.Keyword))
                checkkey = true;
            else if (oQuery.IDParent > 0)
                checkParent = true;
            itemid = queryR.ItemID;
        }
    }
}