<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewCongThanhPhan.aspx.cs" Inherits="VIPortalAPP.UserControls.AdminCMS.CongThanhPhan.pViewCongThanhPhan" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <%=oCongThanhPhan.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="UrlSite" class="col-sm-2 control-label">UrlSite</label>
            <div class="col-sm-10">
                <%=oCongThanhPhan.UrlSite%>
            </div>
        </div>
        <div class="form-group row">
            <label for="MoTa" class="col-sm-2 control-label">MoTa</label>
            <div class="col-sm-10">
                <%=oCongThanhPhan.MoTa%>
            </div>
        </div>
        <div class="form-group row">
            <label for="Public" class="col-sm-2 control-label">Public</label>
            <div class="col-sm-10">
                <%=oCongThanhPhan.Public? "Có" : "Không"%>
            </div>
        </div>
        <div class="form-group row">
            <label for="STT" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-10">
                <%=oCongThanhPhan.STT%>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oCongThanhPhan.Created)%>
            </div>
            <label class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oCongThanhPhan.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
                <%=oCongThanhPhan.Author.LookupValue%>
            </div>
            <label class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
                <%=oCongThanhPhan.Editor.LookupValue%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oCongThanhPhan.ListFileAttach.Count;i++){ %>
                <div><a href="<%=oCongThanhPhan.ListFileAttach[i].Url %>"><%=oCongThanhPhan.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>
