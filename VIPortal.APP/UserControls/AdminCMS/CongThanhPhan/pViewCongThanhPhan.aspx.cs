using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP.UserControls.AdminCMS.CongThanhPhan
{
    public partial class pViewCongThanhPhan : pFormBase
    {
        public CongThanhPhanItem oCongThanhPhan = new CongThanhPhanItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oCongThanhPhan = new CongThanhPhanItem();
            if (ItemID > 0)
            {
                CongThanhPhanDA oCongThanhPhanDA = new CongThanhPhanDA();
                oCongThanhPhan = oCongThanhPhanDA.GetByIdToObject<CongThanhPhanItem>(ItemID);

            }
        }
    }
}