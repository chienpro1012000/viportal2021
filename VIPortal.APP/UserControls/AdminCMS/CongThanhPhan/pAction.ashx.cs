using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;

namespace VIPortalAPP.UserControls.AdminCMS.CongThanhPhan
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);
            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            CongThanhPhanDA oCongThanhPhanDA = new CongThanhPhanDA();
            CongThanhPhanItem oCongThanhPhanItem = new CongThanhPhanItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oCongThanhPhanDA.GetListJson(new CongThanhPhanQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oCongThanhPhanDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "CongThanhPhan", "QUERYDATA", 0, "Xem danh sách cổng thành phần");
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oCongThanhPhanDA.GetListJson(new CongThanhPhanQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oCongThanhPhanItem = oCongThanhPhanDA.GetByIdToObject<CongThanhPhanItem>(oGridRequest.ItemID);
                        oCongThanhPhanItem.UpdateObject(context.Request);
                        oCongThanhPhanDA.UpdateObject<CongThanhPhanItem>(oCongThanhPhanItem);
                        AddLog("UPDATE", "CongThanhPhan", "UPDATE", oGridRequest.ItemID, $"Cập nhật cổng thành phần{oCongThanhPhanItem.Title}");
                        oResult.Message = "Lưu thành công";
                    }
                    else
                    {
                        oCongThanhPhanItem.UpdateObject(context.Request);
                        ///taoj site.
                        ///
                        oCongThanhPhanDA.UpdateObject<CongThanhPhanItem>(oCongThanhPhanItem);
                        #region Template
                        try
                        {
                            Task.Run(() =>
                            {
                                SPSecurity.RunWithElevatedPrivileges(delegate ()
                                {
                                    using (SPSite site = new SPSite("http://localhost:6003"))
                                    {
                                        using (SPWeb web1 = site.OpenWeb())
                                        {
                                            //SPWeb web = site.OpenWeb();
                                            site.AllowUnsafeUpdates = true;
                                            web1.AllowUnsafeUpdates = true;
                                            SPWebTemplate ThanhPhanRoot = null;
                                            SPWebTemplate ThanhPhanCMS = null;
                                            SPWebTemplate ThanhPhanNoiDung = null;                                            
                                            ///get sitetemplate
                                            SPWebTemplateCollection oWebtTemplateCollection = web1.GetAvailableWebTemplates((uint)1033);
                                            foreach (SPWebTemplate wt in oWebtTemplateCollection)
                                            {
                                                if (wt.Title == "RootTemp_X02")
                                                {
                                                    ThanhPhanRoot = wt;
                                                }
                                                else if (wt.Title == "NoiDungTemp_X02")
                                                {
                                                    ThanhPhanNoiDung = wt;
                                                }
                                                else if (wt.Title == "CMSTemp_X02")
                                                {
                                                    ThanhPhanCMS = wt;
                                                }                                                
                                            }
                                            ///create sitetemplate
                                            if (ThanhPhanRoot != null)
                                            {
                                                string UrlRoot = oCongThanhPhanItem.UrlSite;
                                                string UrlRootTitle = oCongThanhPhanItem.Title;
                                                string UrlRootDescription = oCongThanhPhanItem.MoTa;
                                                SPWeb newWeb = web1.Webs.Add(UrlRoot, UrlRootTitle, UrlRootDescription, (uint)1033, ThanhPhanRoot, false, false);
                                                web1.AllowUnsafeUpdates = false;
                                                site.AllowUnsafeUpdates = false;
                                                using (SPSite subsite = new SPSite("http://localhost:6003/" + UrlRoot))
                                                {
                                                    SPWeb subweb = subsite.OpenWeb();
                                                    if (ThanhPhanNoiDung != null)
                                                    {
                                                        string UrlQuanTri = "noidung";
                                                        string UrlQuanTriTitle = "Nội dung";
                                                        string UrlQuanTriDescription = "Nội dung";
                                                        SPWeb newsubWeb = subweb.Webs.Add(UrlQuanTri, UrlQuanTriTitle, UrlQuanTriDescription, (uint)1033, ThanhPhanNoiDung, false, false);
                                                    }
                                                    if (ThanhPhanCMS != null)
                                                    {
                                                        string UrlNoiDung = "cms";
                                                        string UrlNoiDungTitle = "CMS";
                                                        string UrlNoiDungDescription = "CMS";
                                                        SPWeb newsubWeb = subweb.Webs.Add(UrlNoiDung, UrlNoiDungTitle, UrlNoiDungDescription, (uint)1033, ThanhPhanCMS, false, false);                                                        
                                                    }
                                                }
                                            }
                                        }
                                    }
                                });
                            });
                        }
                        catch (Exception exx)
                        {
                            throw;
                        }
                        #endregion
                        AddLog("UPDATE", "CongThanhPhan", "ADD", oGridRequest.ItemID, $"Thêm mới cổng thành phần {oCongThanhPhanItem.Title}");
                        oResult.Message = "Cổng thành phần đang được tạo lập. Vui lòng chờ đợi trong vài phút";
                    }                    
                    break;
                case "DELETE":
                    var outb = oCongThanhPhanDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "CongThanhPhan", "DELETE", oGridRequest.ItemID, $"Xóa cổng thành phần {outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    AddLog("DELETE-MULTI", "CongThanhPhan", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều cổng thành phần");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oCongThanhPhanDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "CongThanhPhan", "APPROVED", oGridRequest.ItemID, $"Duyệt cổng thành phần {oCongThanhPhanDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oCongThanhPhanDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "CongThanhPhan", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt cổng thành phần {oCongThanhPhanDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oCongThanhPhanItem = oCongThanhPhanDA.GetByIdToObjectSelectFields<CongThanhPhanItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oCongThanhPhanItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
                case "DONGBO":
                    string urlsite = SPContext.Current.Site.RootWeb.Url;
                    SPWeb web = SPContext.Current.Web;
                    List<string> lstSiteRemove = new List<string>()
                    {
                        "/noidung","/cms","/login","/portalchat", "/htbaocao", "/diendan"
                    };
                    SPSecurity.RunWithElevatedPrivileges(delegate ()
                    {
                        using (SPSite ElevatedSite = new SPSite(urlsite))
                        {
                            using (SPWeb ElevatedWeb = ElevatedSite.OpenWeb())
                            {
                                // Code Using the SPWeb Object Goes Here
                                foreach (SPWeb oSPWeb in ElevatedWeb.Webs)
                                {
                                    if (!lstSiteRemove.Contains(oSPWeb.ServerRelativeUrl.ToLower()))
                                    {
                                        if (oCongThanhPhanDA.CheckExit(0, oSPWeb.Title) == 0)
                                        {
                                            oCongThanhPhanDA.UpdateObject<CongThanhPhanItem>(new CongThanhPhanItem()
                                            {
                                                Title = oSPWeb.Title,
                                                UrlSite = oSPWeb.ServerRelativeUrl.ToLower()
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    });
                    oResult.Message = "Đồng bộ thành công";
                    break;
            }
            oResult.ResponseData();

        }

        public string CreateWeb(CongThanhPhanItem oCongThanhPhanItem)
        {
            SPSecurity.RunWithElevatedPrivileges(delegate
            {
                using (var curSite = new SPSite("http://localhost:6003"))
                {
                    using (var curWebRoot = curSite.OpenWeb())
                    {
                        SPWebCollection newWeb = curWebRoot.Webs;
                        SPWeb spWebCreated = newWeb.Add(oCongThanhPhanItem.UrlSite, oCongThanhPhanItem.Title, "", curWebRoot.Language, "CMSPUBLISHING#0",
                                                  false, false);
                      
                            SPWebCollection newWebSUb = spWebCreated.Webs;
                            SPWeb spWebCreatedSubCMS = newWebSUb.Add(oCongThanhPhanItem.UrlSite, oCongThanhPhanItem.Title, "", curWebRoot.Language, "CMSPUBLISHING#0",
                                                      false, false);



                    }
                }
            });
            return "";
        }
    }
}