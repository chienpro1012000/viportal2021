<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormCongThanhPhan.aspx.cs" Inherits="VIPortalAPP.UserControls.AdminCMS.CongThanhPhan.pFormCongThanhPhan" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-CongThanhPhan" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oCongThanhPhan.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oCongThanhPhan.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <%--<label for="STT" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-4">
                <input type="text" name="STT" id="STT" placeholder="Nhập stt" value="<%:oCongThanhPhan.STT%>" class="form-control" />
            </div>--%>
            <%--<label for="Public" class="col-sm-2 control-label">Public</label>
            <div class="col-sm-4">
                <input value="true" type="checkbox" name="Public" id="Public" <%=oCongThanhPhan.Public? "checked" : string.Empty%> />
            </div>--%>
        </div>
        <div class="form-group row">
            <label for="UrlSite" class="col-sm-2 control-label">Url</label>
            <div class="col-sm-10">
                <input type="text" name="UrlSite" id="UrlSite" placeholder="Nhập urlsite (ví dụ: caugiay)" value="<%:oCongThanhPhan.UrlSite%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="MoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <textarea name="MoTa" id="MoTa" class="form-control"><%:oCongThanhPhan.MoTa%></textarea>
            </div>
        </div>
        

        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oCongThanhPhan.ListFileAttach)%>'
            });
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $(".form-group select").select2({
                dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
            });

            $("#frm-CongThanhPhan").validate({
                rules: {
                    Title: "required",
                    UrlSite: "required",
                    MoTa: "required",
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề",
                    UrlSite: "Vui lòng nhập UrlSite",
                    MoTa: "Vui lòng nhập mô tả",
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/CongThanhPhan/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-CongThanhPhan").trigger("click");
                            $(form).closeModal();
                            $('#tblCongThanhPhan').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-CongThanhPhan").viForm();
        });
    </script>
</body>
</html>
