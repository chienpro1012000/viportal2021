<%@ control language="C#" autoeventwireup="true" codebehind="UC_CongThanhPhan.aspx.cs" inherits="VIPortalAPP.UserControls.AdminCMS.CongThanhPhan.UC_CongThanhPhan" %>
<div role="body-data" data-title="" class="content_wp" data-action="/UserControls/AdminCMS/CongThanhPhan/pAction.asp" data-form="/UserControls/AdminCMS/CongThanhPhan/pFormCongThanhPhan.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="CongThanhPhanSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title" />
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <button class="btn btn-primary act-add" data-per="081002" type="button">Thêm mới</button>
                        <button class="btn btn-primary" id="btnDongBoMaNguon" data-per="080802" type="button">Đồng bộ mã nguồn</button>
                    </p>
                </div>
            </div>

            <div class="clsgrid table-responsive">

                <table id="tblCongThanhPhan" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#btnDongBoMaNguon").click(function () {
            $.post("/UserControls/AdminCMS/CongThanhPhan/pAction.asp", { do: "DONGBO" }, function (result) {
                if (result.State == 2) {
                    BootstrapDialog.show({
                        title: "Lỗi",
                        message: result.Message
                    });
                }
                else {
                    showMsg(result.Message);
                    $('#tblCongThanhPhan').DataTable().ajax.reload();
                }
            }).always(function () { });
        });
        var $tagvalue = $(".content_wp");
        var $tblDanhMucChung = $("#tblCongThanhPhan").viDataTable(
            {
                "frmSearch": "CongThanhPhanSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "data": "STT",
                            "name": "STT", "sWidth": "50px", "sTitle": "STT"
                        }, {
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                            },
                            "name": "Title", "sTitle": "Tiêu đề"
                        },
                        {
                            "data": "UrlSite",
                            "name": "UrlSite", "sTitle": "Đường dẫn cổng thành phần"
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {
                                if (o._ModerationStatus == 0)
                                    return '<a data-per="081005" title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a data-per="081005" title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" title="Sửa" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) {
                                return '<a title="Khởi tạo Menu chức năng" data-UrlSiteFix="' + o.UrlSite + '" url="/UserControls/AdminCMS/CongThanhPhan/pMenuForm.aspx" class="btn default btn-xs purple btnfrm" size="1244" data-do="UPDATE" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fas fa-bars"></i></a>';
                            }
                        }, {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-ItemID="' + o.ID + '" />'; }
                        }
                    ],
                "aaSorting": [0, 'asc']
            });
    });
</script>
