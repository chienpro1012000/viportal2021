﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormLichPhong.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.LichPhong.pFormLichPhong" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-LichPhong" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oLichPhong.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" name="NguoiTao" id="NguoiTao" value="<%=CurentUser.Title %>" />
        <input type="hidden" name="fldGroup" id="fldGroup" value="<%=CurentUser.UserPhongBan.ID %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" placeholder="Nhập tên cuộc họp" value="<%=oLichPhong.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="LichThoiGianBatDau" class="col-sm-2 control-label">Ngày bắt đầu</label>
            <div class="col-sm-4">
                <input type="text" name="LichThoiGianBatDau" id="LichThoiGianBatDau" value="<%:string.Format("{0:dd/MM/yyyy HH:mm}", oLichPhong.LichThoiGianBatDau)%>" class="form-control datetime" />
            </div>
            <label for="LichThoiGianKetThuc" class="col-sm-2 control-label">Ngày kết thúc</label>
            <div class="col-sm-4">
                <input type="text" name="LichThoiGianKetThuc" id="LichThoiGianKetThuc" value="<%:string.Format("{0:dd/MM/yyyy HH:mm}",oLichPhong.LichThoiGianKetThuc)%>" class="form-control datetime" />
            </div>
        </div>
        <div class="form-group row">
            <label for="LichLanhDaoChuTri" class="col-sm-2 control-label">Lãnh đạo chủ trì</label>
            <div class="col-sm-4">
                <select data-selected="<%:oLichPhong.LichLanhDaoChuTri.LookupId%>" data-url="/UserControls/AdminCMS/LUser/pAction.asp?do=QUERYDATA" data-place="Chọn người" name="LichLanhDaoChuTri" id="LichLanhDaoChuTri" class="form-control"></select>
            </div> 
            <label for="LichTrangThai" class="col-sm-2 control-label">Trạng thái</label>
            <div class="col-sm-4">
                <input type="text" name="LichTrangThai" id="LichTrangThai" class="form-control" value="<%=oLichPhong.LichTrangThai%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="LichDiaDiem" class="col-sm-2 control-label">Địa điểm</label>
            <div class="col-sm-10">
                <input type="text" name="LichDiaDiem" id="LichDiaDiem" placeholder="Nhập địa điểm" class="form-control" value="<%=oLichPhong.LichDiaDiem%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="NoiDungChuanBi" class="col-sm-2 control-label">Nội dung chuẩn bị</label>
            <div class="col-sm-10">
                <textarea name="NoiDungChuanBi" id="NoiDungChuanBi" placeholder="Nhập nội dung chuẩn bị" class="form-control"><%=oLichPhong.NoiDungChuanBi%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="LichNoiDung" class="col-sm-2 control-label">Nội dung</label>
            <div class="col-sm-10">
                <textarea name="LichNoiDung" id="LichNoiDung" placeholder="Nhập nội dung" class="form-control"><%=oLichPhong.LichNoiDung%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="LichThanhPhanThamGia" class="col-sm-2 control-label">Thành phần tham gia</label>
            <div class="col-sm-10">
                <div class="input-group mb-3">
                    <input type="text" disabled="disabled" value="" placeholder="Thành phần tham gia" id="UserChoie" class="form-control" />
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary btnfrm" data-do="CHONNGUOI" title="Chọn người dùng" size="1240"  url="/UserControls/AdminCMS/LichDonVi/pFormChooseThamGia.aspx" id="btnChonNguoi" type="button">Chọn người dùng</button>
                    </div>
                </div>
                <input type="hidden" name="lstNguoiDung_Value" id="lstNguoiDung_Value" value="<%=string.Join(",", oLichPhong.LichThanhPhanThamGia.Select(x=>x.LookupId)) %>" />
                <input type="hidden" name="LichThanhPhanThamGia" id="lstLichThamGia" value="<%=string.Join(",", oLichPhong.LichThanhPhanThamGia.Select(x=>x.LookupId)) %>" />
                <textarea id="LichThanhPhanThamGia" name="" placeholder="Thành phần tham gia" class="form-control"><%=string.Join(",", oLichPhong.LichThanhPhanThamGia.Select(x=>x.LookupValue)) %></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="LichGhiChu" class="col-sm-2 control-label">Ghi chú</label>
            <div class="col-sm-10">
                <textarea name="LichGhiChu" id="LichGhiChu" placeholder="Nhập ghi chú" class="form-control"><%=oLichPhong.LichGhiChu%></textarea>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#LichLanhDaoChuTri").smSelect2018V2({
                dropdownParent: "#frm-LichPhong"
            });
            
            $("#Title").focus();
            $(".datetime").daterangepicker({
                timePicker: true,
                singleDatePicker: true,
                timePicker24Hour: true,
                locale: { format: 'DD/MM/YYYY HH:mm' }
            });
            $(".form-group select").select2({
                dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
            });

            $("#frm-LichPhong").validate({
                rules: {
                    Title: "required",
                    LichLanhDaoChuTri: "required",
                    LichThoiGianBatDau: "required",
                    LichThoiGianKetThuc: "required",
                    LichDiaDiem: "required",
                    LichNoiDung: "required",
                },
                messages: {
                    Title: "Vui lòng nhập tên cuộc họp",
                    LichLanhDaoChuTri: "Vui lòng nhập lãnh đạo chủ trì",
                    LichThoiGianBatDau: "Vui lòng nhập ngày bắt đầu",
                    LichThoiGianKetThuc: "Vui lòng nhập ngày kết thúc",
                    LichDiaDiem: "Vui lòng nhập địa điểm",
                    LichNoiDung: "Vui lòng nhập nội dung",
                },

                submitHandler: function (form) {
                    var from = $("#LichThoiGianBatDau").val();
                    var to = $("#LichThoiGianKetThuc").val();

                    if (Date.parse(from) > Date.parse(to)) {
                        BootstrapDialog.show({
                            title: "Lỗi",
                            message: "Dữ liệu ngày bắt đầu nhỏ hơn ngày kết thúc"
                        });
                    }
                    else {
                        $.post("/UserControls/AdminCMS/LichPhong/pAction.asp", $(form).viSerialize(), function (result) {
                            if (result.State == 2) {
                                BootstrapDialog.show({
                                    title: "Lỗi",
                                    message: result.Message
                                });
                            }
                            else {
                                showMsg(result.Message);
                                //$("#btn-find-LichPhong").trigger("click");
                                $(form).closeModal();
                                $('#tblLichPhong').DataTable().ajax.reload();
                            }
                        }).always(function () { });
                    }
                }
            });
            $("#frm-LichPhong").viForm();
        });
    </script>
</body>
</html>

