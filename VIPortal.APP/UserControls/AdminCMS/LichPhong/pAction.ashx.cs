﻿using Microsoft.SharePoint;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.LichCaNhan;
using ViPortalData.LichDonVi;
using ViPortalData.LichPhong;

namespace VIPortal.APP.UserControls.AdminCMS.LichPhong
{

    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public string contentMail { get; set; }
        public List<string> mailto { get; set; }
        SPFieldLookupValueCollection lstThamGia = new SPFieldLookupValueCollection();
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);
            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            LichPhongDA oLichPhongDA = new LichPhongDA();
            LichPhongItem oLichPhongItem = new LichPhongItem();
            LUserDA oLUserDA = new LUserDA();
            LNotificationDA oLNotificationDA = new LNotificationDA(UrlSite);
            LNotificationItem lNotificationItem = new LNotificationItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    var oData = oLichPhongDA.GetListJsonSolr(new LichPhongQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oLichPhongDA.TongSoBanGhiSauKhiQuery);
                    oGrid.Query = oLichPhongDA.Queryreturn;
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "LichPhong", "QUERYDATA", 0, "Xem danh sách lịch phòng");
                    break;
                case "HUYDUYETLICH":
                    oLichPhongItem = oLichPhongDA.GetByIdToObject<LichPhongItem>(oGridRequest.ItemID);
                    //update lại thông tin cho lịch cũ đối các thông tin về như cũ.
                    oLichPhongDA.SystemUpdateOneField(oGridRequest.ItemID, "LichTrangThai", 1);
                    oLichPhongDA.SaveSolr(oGridRequest.ItemID);

                    LichCaNhanDA lichCaNhanDAFix = new LichCaNhanDA();
                    List<LichCaNhanJson> LstLichCaNhan = lichCaNhanDAFix.GetListJson(new LichCaNhanQuery() { LogText = $"_LichPhong-{oGridRequest.ItemID}_", _ModerationStatus = 1 }); // lấy ra lịch xuất phát từ lịch cha này và đang được duyệt.
                    foreach (LichCaNhanJson oLichCaNhanJson in LstLichCaNhan)
                    {
                        lichCaNhanDAFix.UpdateOneOrMoreField(oLichCaNhanJson.ID, new Dictionary<string, object>() {
                                { "LogText", oLichCaNhanJson.LogText + "_HuyDuyet_" },
                                {"Title", oLichCaNhanJson.Title + "(Hủy duyệt)" }
                            });
                        lichCaNhanDAFix.SaveSolr(oLichCaNhanJson.ID);
                    }
                    oResult.Message = "Hủy Duyệt thành công";
                    break;
                case "DUYETLICH":
                    if (oGridRequest.ItemID > 0)
                    {
                        //LogText = oLichDonViItem.LogText + $"_LichDonVi-{oLichDonViItem.ID}_", //xác định được logtxt khi query
                        oLichPhongItem = oLichPhongDA.GetByIdToObject<LichPhongItem>(oGridRequest.ItemID);
                        if (string.IsNullOrEmpty(context.Request["LoaiDuyet"]))
                        {
                            //UPDATE TRẠNG THÁI CHO LỊCH NÀY
                            oLichPhongDA.SystemUpdateOneField(oGridRequest.ItemID, "LichTrangThai", 0);
                            oLichPhongDA.SaveSolr(oGridRequest.ItemID);
                            //lịch cá nhân nếu có.
                            LichCaNhanDA lichCaNhanDA = new LichCaNhanDA();
                            //taoj lichj ca nhan khi giao ve cho nguoi dung.
                            if (oLichPhongItem.LichLanhDaoChuTri.LookupId > 0)
                            {

                                LichCaNhanItem lichCaNhan = new LichCaNhanItem()
                                {
                                    CreatedUser = new SPFieldLookupValue(oLichPhongItem.LichLanhDaoChuTri.LookupId, ""),
                                    OldID = oLichPhongItem.ID.ToString(),
                                    Title = oLichPhongItem.Title,
                                    LichDiaDiem = oLichPhongItem.LichDiaDiem,
                                    DBPhanLoai = oLichPhongItem.DBPhanLoai,
                                    LogNoiDung = oLichPhongItem.LogNoiDung,
                                    LogText = oLichPhongItem.LogText + $"_LichPhong-{oLichPhongItem.ID}_", //xác định được logtxt khi query
                                    LichGhiChu = oLichPhongItem.LichGhiChu,
                                    LichNoiDung = oLichPhongItem.LichNoiDung,
                                    LichThoiGianBatDau = oLichPhongItem.LichThoiGianBatDau,
                                    LichThoiGianKetThuc = oLichPhongItem.LichThoiGianKetThuc,
                                    NoiDungChuanBi = oLichPhongItem.NoiDungChuanBi,
                                    LichLanhDaoChuTri = oLichPhongItem.LichLanhDaoChuTri, //chỗ này là khi chọn đon vị.
                                    LichPhongBanThamGia = oLichPhongItem.LichPhongBanThamGia, //chỗ này là khi chọn đon vị.
                                    LichThanhPhanThamGia = oLichPhongItem.LichThanhPhanThamGia, //lấy từ thông tin giao
                                    CHUAN_BI = oLichPhongItem.CHUAN_BI,
                                    THANH_PHAN = oLichPhongItem.THANH_PHAN,
                                    CHU_TRI = oLichPhongItem.CHU_TRI
                                };
                                lichCaNhanDA.UpdateObject<LichCaNhanItem>(lichCaNhan);
                                lichCaNhanDA.UpdateSPModerationStatus(lichCaNhan.ID, SPModerationStatusType.Approved); //duyệt luôn.
                            }
                            SPFieldLookupValueCollection ALLUserThamGIa = new SPFieldLookupValueCollection();
                            ALLUserThamGIa.AddRange(oLichPhongItem.LichThanhPhanThamGia);
                            if (!string.IsNullOrEmpty(oLichPhongItem.CHU_TRI))
                            {
                                LookupData CHU_TRI = clsFucUtils.GetLoookupByStringFiledData(oLichPhongItem.CHU_TRI);
                                if (CHU_TRI.ID > 0)
                                {
                                    ALLUserThamGIa.Add(new SPFieldLookupValue(CHU_TRI.ID, CHU_TRI.Title));
                                }
                            }
                            if (!string.IsNullOrEmpty(oLichPhongItem.CHUAN_BI))
                            {
                                List<LookupData> CHUAN_BI = clsFucUtils.GetDanhSachMutilLoookupByStringFiledData(oLichPhongItem.CHUAN_BI);
                                foreach (LookupData item in CHUAN_BI)
                                {
                                    if (ALLUserThamGIa.FindIndex(x => x.LookupId == item.ID) == -1) ALLUserThamGIa.Add(new SPFieldLookupValue(item.ID, item.Title));
                                }
                            }
                            //cá nhân tham gia thì cũng phải add vào.
                            if (ALLUserThamGIa.Count > 0)
                            {
                                foreach (var cbthamgia in ALLUserThamGIa)
                                {
                                    LichCaNhanItem lichCaNhan = new LichCaNhanItem()
                                    {
                                        CreatedUser = new SPFieldLookupValue(cbthamgia.LookupId, ""),
                                        OldID = oLichPhongItem.ID.ToString(),
                                        Title = oLichPhongItem.Title,
                                        LichDiaDiem = oLichPhongItem.LichDiaDiem,
                                        LogNoiDung = oLichPhongItem.LogNoiDung,
                                        LogText = oLichPhongItem.LogText + $"_LichPhong-{oLichPhongItem.ID}_", //xác định được logtxt khi query
                                        LichGhiChu = oLichPhongItem.LichGhiChu,
                                        LichNoiDung = oLichPhongItem.LichNoiDung,
                                        LichThoiGianBatDau = oLichPhongItem.LichThoiGianBatDau,
                                        LichThoiGianKetThuc = oLichPhongItem.LichThoiGianKetThuc,
                                        NoiDungChuanBi = oLichPhongItem.NoiDungChuanBi,
                                        LichLanhDaoChuTri = oLichPhongItem.LichLanhDaoChuTri, //chỗ này là khi chọn đon vị.
                                        LichPhongBanThamGia = oLichPhongItem.LichPhongBanThamGia, //chỗ này là khi chọn đon vị.
                                        LichThanhPhanThamGia = oLichPhongItem.LichThanhPhanThamGia, //lấy từ thông tin giao
                                        CHUAN_BI = oLichPhongItem.CHUAN_BI,
                                        THANH_PHAN = oLichPhongItem.THANH_PHAN,
                                        CHU_TRI = oLichPhongItem.CHU_TRI
                                    };
                                    lichCaNhanDA.UpdateObject<LichCaNhanItem>(lichCaNhan);
                                    lichCaNhanDA.UpdateSPModerationStatus(lichCaNhan.ID, SPModerationStatusType.Approved); //duyệt luôn.
                                }
                            }
                            //tạo lịch cho các phòng ban khác trong đươn vị nếu có.
                            oResult.Message = "Duyệt thành công";
                        }
                        else
                        {
                            oLichPhongDA.UpdateSPModerationStatus(oGridRequest.ItemID, SPModerationStatusType.Pending);
                            oResult.Message = "Hủy Duyệt thành công";
                        }
                    }
                    break;
                case "GIAOLICHDONVI":
                    if (!string.IsNullOrEmpty(context.Request["ItemIDs"]))
                    {
                        LichCaNhanDA lichCaNhanDA = new LichCaNhanDA();
                        SPFieldLookupValueCollection LichThanhPhanThamGia = new SPFieldLookupValueCollection();
                        List<int> ItemIDs = context.Request["ItemIDs"].Split(',').Select(x => Convert.ToInt32(x)).ToList();
                        List<LUserJson> lUserJsons = new List<LUserJson>();
                        string TBNoiDung = context.Request["TBNoiDung"];
                        if (!string.IsNullOrEmpty(context.Request["LichThanhPhanThamGia"]))
                        {
                            //các phòng tham gia họp nếu có.
                            LichThanhPhanThamGia = clsFucUtils.StringToLookup(context.Request["LichThanhPhanThamGia"]);
                            LUserDA lUserDA = new LUserDA();
                            lUserJsons = lUserDA.GetListJson(new LUserQuery() { isGetBylistID = true, lstIDget = LichThanhPhanThamGia.Select(x => x.LookupId).ToList() });

                        }
                        foreach (int itemid in ItemIDs)
                        {

                            LichPhongItem lichPhongItem = oLichPhongDA.GetByIdToObject<LichPhongItem>(itemid);
                            lichPhongItem.LogText += $"_${CurentUser.UserPhongBan.ID}$_";
                            lichPhongItem.LichThanhPhanThamGia.AddRange(LichThanhPhanThamGia);
                            lichPhongItem.TBNoiDung = TBNoiDung;
                            //lichPhongItem.LichThanhPhanThamGia = LichThanhPhanThamGia;
                            oLichPhongDA.UpdateOneOrMoreField(itemid, new Dictionary<string, object>() { { "LogText", lichPhongItem.LogText }, { "LichThanhPhanThamGia", lichPhongItem.LichThanhPhanThamGia }, { "TBNoiDung", TBNoiDung } }, true);
                            oLichPhongDA.SaveSolr(itemid);
                            //update ngược lại lịch đươn vị để biết cán bộ sẽ tham gia họp.
                            if (!string.IsNullOrEmpty(lichPhongItem.OldID))
                            {
                                int OldID = Convert.ToInt32(lichPhongItem.OldID);
                                LichDonViDA lichDonViDA = new LichDonViDA();
                                LichDonViItem lichDonViItem = lichDonViDA.GetByIdToObject<LichDonViItem>(OldID);
                                lichDonViItem.LichThanhPhanThamGia.AddRange(LichThanhPhanThamGia);
                                lichDonViDA.UpdateOneOrMoreField(OldID, new Dictionary<string, object>() { { "LichThanhPhanThamGia", lichPhongItem.LichThanhPhanThamGia } }, true);
                                lichDonViDA.SaveSolr(OldID);
                            }
                            foreach (SPFieldLookupValue oSPFieldLookupValue in LichThanhPhanThamGia)
                            {
                                LichCaNhanItem lichCaNhan = new LichCaNhanItem()
                                {
                                    CreatedUser = new SPFieldLookupValue(oSPFieldLookupValue.LookupId, ""),
                                    OldID = lichPhongItem.ID.ToString(),
                                    Title = lichPhongItem.Title,
                                    LichDiaDiem = lichPhongItem.LichDiaDiem,
                                    LogText = oLichPhongItem.LogText + $"_LichPhong-{oLichPhongItem.ID}_", //xác định được logtxt khi query
                                    TBNoiDung = lichPhongItem.TBNoiDung,
                                    LichGhiChu = lichPhongItem.LichGhiChu,
                                    LichNoiDung = lichPhongItem.LichNoiDung,
                                    LogNoiDung = lichPhongItem.LogNoiDung,
                                    DBPhanLoai = oLichPhongItem.DBPhanLoai,
                                    LichThoiGianBatDau = lichPhongItem.LichThoiGianBatDau,
                                    LichThoiGianKetThuc = lichPhongItem.LichThoiGianKetThuc,
                                    LichThanhPhanThamGia = lichPhongItem.LichThanhPhanThamGia,
                                    LichPhongBanThamGia = lichPhongItem.LichPhongBanThamGia,
                                    NoiDungChuanBi = lichPhongItem.NoiDungChuanBi,
                                    LichLanhDaoChuTri = lichPhongItem.LichLanhDaoChuTri, //chỗ này là khi chọn đon vị.
                                    // LichPhongBanThamGia = LichPhongBanThamGia, //chỗ này là khi chọn đon vị.
                                    //LichThanhPhanThamGia = string.Join(", ", lUserJsons.Select(x=>x.Title)), //lấy từ thông tin giao
                                    CHUAN_BI = lichPhongItem.CHUAN_BI,
                                    THANH_PHAN = lichPhongItem.THANH_PHAN,
                                    CHU_TRI = lichPhongItem.CHU_TRI
                                };
                                lichCaNhanDA.UpdateObject<LichCaNhanItem>(lichCaNhan);
                                lichCaNhanDA.UpdateSPModerationStatus(lichCaNhan.ID, SPModerationStatusType.Approved); //duyệt luôn.
                            }
                        }
                        oResult.Message = "Giao thành công";
                    }
                    break;
                case "SUAGIAOLICHDONVI":
                    if (!string.IsNullOrEmpty(context.Request["ItemIDs"]))
                    {
                        string TBNoiDung = context.Request["TBNoiDung"];
                        SPFieldLookupValueCollection LichThanhPhanThamGia = new SPFieldLookupValueCollection();
                        if (!string.IsNullOrEmpty(context.Request["LichThanhPhanThamGia"]))
                        {

                            //các phòng tham gia họp nếu có.
                            LichThanhPhanThamGia = clsFucUtils.StringToLookup(context.Request["LichThanhPhanThamGia"]);
                            // LUserDA lUserDA = new LUserDA();
                            //lUserJsons = lUserDA.GetListJson(new LUserQuery() { isGetBylistID = true, lstIDget = LichThanhPhanThamGia.Select(x => x.LookupId).ToList() });

                        }
                        List<int> ItemIDs = context.Request["ItemIDs"].Split(',').Select(x => Convert.ToInt32(x)).ToList();
                        LichCaNhanDA lichCaNhanDA = new LichCaNhanDA();
                        foreach (int ItemID in ItemIDs)
                        {
                            string TPRemove = context.Request["TPRemove"];
                            if (!string.IsNullOrEmpty(TPRemove))
                            {
                                //xóa người dùng cũ ở trong lịch nếu có.
                                oLichPhongItem = oLichPhongDA.GetByIdToObject<LichPhongItem>(ItemID);
                                //lichPhongItem.LichThanhPhanThamGia.remo
                                List<int> UserREmove = TPRemove.Split(',').Select(x => Convert.ToInt32(x)).ToList();
                                var listTam = oLichPhongItem.LichThanhPhanThamGia.Where(x => !UserREmove.Contains(x.LookupId)).ToList();
                                oLichPhongItem.LichThanhPhanThamGia = new SPFieldLookupValueCollection();
                                oLichPhongItem.LichThanhPhanThamGia.AddRange(listTam);
                                oLichPhongDA.SystemUpdateOneField(ItemID, "LichThanhPhanThamGia", oLichPhongItem.LichThanhPhanThamGia);
                                LichCaNhanDA lichCaNhanDAFix2 = new LichCaNhanDA();
                                List<LichCaNhanJson> LstLichCaNhan2 = lichCaNhanDAFix2.GetListJson(new LichCaNhanQuery() { LogText = $"_LichPhong-{oGridRequest.ItemID}_", _ModerationStatus = 1 }); // lấy ra lịch xuất phát từ lịch cha này và đang được duyệt.
                                //xóa các lịch liên quan nếu có.
                                foreach (var item in UserREmove)
                                {
                                    LichCaNhanJson oLichCaNhanJson = LstLichCaNhan2.FirstOrDefault(x => x.CreatedUser.ID == item);

                                    if (oLichCaNhanJson != null)
                                    {
                                        lichCaNhanDAFix2.UpdateOneOrMoreField(oLichCaNhanJson.ID, new Dictionary<string, object>() {
                                            { "LogText", oLichCaNhanJson.LogText + "_HuyDuyet_" },
                                            {"Title", oLichCaNhanJson.Title + "(Hủy duyệt)" }
                                        });
                                        lichCaNhanDAFix2.SaveSolr(oLichCaNhanJson.ID);
                                    }

                                }

                            }
                            oLichPhongItem = oLichPhongDA.GetByIdToObject<LichPhongItem>(ItemID);
                            //lichPhongItem.LogText += $"_${CurentUser.UserPhongBan.ID}$_";
                            oLichPhongItem.LichThanhPhanThamGia.AddRange(LichThanhPhanThamGia);
                            oLichPhongItem.TBNoiDung = TBNoiDung;
                            //lichPhongItem.LichThanhPhanThamGia = LichThanhPhanThamGia;
                            oLichPhongDA.UpdateOneOrMoreField(ItemID, new Dictionary<string, object>() { { "LogText", oLichPhongItem.LogText }, { "LichThanhPhanThamGia", oLichPhongItem.LichThanhPhanThamGia }, { "TBNoiDung", TBNoiDung } }, true);
                            oLichPhongDA.SaveSolr(ItemID);
                            //update ngược lại lịch đươn vị để biết cán bộ sẽ tham gia họp.
                            if (!string.IsNullOrEmpty(oLichPhongItem.OldID))
                            {
                                int OldID = Convert.ToInt32(oLichPhongItem.OldID);
                                LichDonViDA lichDonViDA = new LichDonViDA();
                                LichDonViItem lichDonViItem = lichDonViDA.GetByIdToObject<LichDonViItem>(OldID);
                                lichDonViItem.LichThanhPhanThamGia.AddRange(LichThanhPhanThamGia);
                                lichDonViDA.UpdateOneOrMoreField(OldID, new Dictionary<string, object>() { { "LichThanhPhanThamGia", oLichPhongItem.LichThanhPhanThamGia } }, true);
                                lichDonViDA.SaveSolr(OldID);
                            }
                            foreach (SPFieldLookupValue oSPFieldLookupValue in LichThanhPhanThamGia)
                            {
                                LichCaNhanItem lichCaNhan = new LichCaNhanItem()
                                {
                                    CreatedUser = new SPFieldLookupValue(oSPFieldLookupValue.LookupId, ""),
                                    OldID = oLichPhongItem.ID.ToString(),
                                    Title = oLichPhongItem.Title,
                                    LichDiaDiem = oLichPhongItem.LichDiaDiem,
                                    LogText = oLichPhongItem.LogText + $"_LichPhong-{oLichPhongItem.ID}_", //xác định được logtxt khi query
                                    TBNoiDung = oLichPhongItem.TBNoiDung,
                                    LichGhiChu = oLichPhongItem.LichGhiChu,
                                    LichNoiDung = oLichPhongItem.LichNoiDung,
                                    DBPhanLoai = oLichPhongItem.DBPhanLoai,
                                    LogNoiDung = oLichPhongItem.LogNoiDung,
                                    LichThoiGianBatDau = oLichPhongItem.LichThoiGianBatDau,
                                    LichThoiGianKetThuc = oLichPhongItem.LichThoiGianKetThuc,
                                    LichThanhPhanThamGia = oLichPhongItem.LichThanhPhanThamGia,
                                    LichPhongBanThamGia = oLichPhongItem.LichPhongBanThamGia,
                                    NoiDungChuanBi = oLichPhongItem.NoiDungChuanBi,
                                    LichLanhDaoChuTri = oLichPhongItem.LichLanhDaoChuTri, //chỗ này là khi chọn đon vị.
                                    // LichPhongBanThamGia = LichPhongBanThamGia, //chỗ này là khi chọn đon vị.
                                    //LichThanhPhanThamGia = string.Join(", ", lUserJsons.Select(x=>x.Title)), //lấy từ thông tin giao
                                    CHUAN_BI = oLichPhongItem.CHUAN_BI,
                                    THANH_PHAN = oLichPhongItem.THANH_PHAN,
                                    CHU_TRI = oLichPhongItem.CHU_TRI
                                };
                                lichCaNhanDA.UpdateObject<LichCaNhanItem>(lichCaNhan);
                                lichCaNhanDA.UpdateSPModerationStatus(lichCaNhan.ID, SPModerationStatusType.Approved); //duyệt luôn.
                            }
                        }
                    }
                    break;
                case "XACNHANDAXEM":
                    if (!string.IsNullOrEmpty(context.Request["ItemIDs"]))
                    {
                        List<int> ItemIDs = context.Request["ItemIDs"].Split(',').Select(x => Convert.ToInt32(x)).ToList();
                        foreach (var itemid in ItemIDs)
                        {
                            oLichPhongItem = oLichPhongDA.GetByIdToObject<LichPhongItem>(itemid);
                            oLichPhongItem.LogText += $"_${CurentUser.UserPhongBan.ID}$_";
                            oLichPhongDA.SystemUpdateOneField(itemid, "LogText", oLichPhongItem.LogText);
                            oLichPhongDA.SaveSolr(itemid);
                        }
                    }
                    oResult.Message = "Đã đánh dấu";
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataLichPhong = oLichPhongDA.GetListJson(new LichPhongQuery(context.Request));
                    treeViewItems = oDataLichPhong.Select(x => new TreeViewItem()
                    {
                        key = x.LichLanhDaoChuTri.Title,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;

                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oLichPhongDA.GetListJson(new LichPhongQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLichPhongItem = oLichPhongDA.GetByIdToObject<LichPhongItem>(oGridRequest.ItemID);
                        oLichPhongItem.UpdateObject(context.Request);
                        mailto = new List<string>();
                        if (oLichPhongItem.LichLanhDaoChuTri.LookupId > 0 && oLichPhongItem.LichThanhPhanThamGia.FindIndex(x => x == oLichPhongItem.LichLanhDaoChuTri) == -1)
                            lstThamGia.Add(oLichPhongItem.LichLanhDaoChuTri);
                        lstThamGia.AddRange(oLichPhongItem.LichThanhPhanThamGia);
                        foreach (var item in lstThamGia)
                        {
                            var LUser = oLUserDA.GetByIdToObject<LUserItem>(item.LookupId);
                            if (!string.IsNullOrEmpty(LUser.UserEmail))
                            {
                                mailto.Add(LUser.UserEmail);
                            }
                        }
                        //gửi mail
                        oLichPhongDA.UpdateObject<LichPhongItem>(oLichPhongItem);
                        contentMail = "Kính gửi ông/bà,";
                        contentMail += "<div> Ông bà đã nhận được thông báo về cuộc họp: " + oLichPhongItem.Title + " vào hồi " + oLichPhongItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy") + " tại " + oLichPhongItem.LichDiaDiem + "</div>";
                        contentMail += "<div>Nhận được mail này mong quý vị sắp xếp công việc và thời gian tham gia cuộc họp.</div>";
                        contentMail += "<div>Trân trọng cảm ơn.</div>";
                        if (mailto.Count > 0)
                        {
                            SendMail("[Email thông báo lịch họp phòng]", contentMail, mailto);
                        }
                        #region Lưu Notifi
                        string NguoiTao = context.Request["NguoiTao"];
                        lNotificationItem.TitleBaiViet = oLichPhongItem.Title;
                        lNotificationItem.LNotiNguoiGui = NguoiTao;
                        if (lstThamGia.Count > 0)
                        {
                            lNotificationItem.LNotiNguoiNhan_Id = "," + string.Join(",", lstThamGia) + ",";
                            lNotificationItem.LNotiChuaXem_Id = lNotificationItem.LNotiNguoiNhan_Id;
                        }
                        lNotificationItem.DoiTuongTitle = "Lịch họp phòng";
                        lNotificationItem.LNotiData = Newtonsoft.Json.JsonConvert.SerializeObject(new SPEntity() { ID = oLichPhongItem.ID, Title = oLichPhongItem.Title });
                        lNotificationItem.Title = $"Lịch họp:  {oLichPhongItem.Title} ngày {oLichPhongItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy")} ";
                        lNotificationItem.LNotiMoTa = $"Lịch họp:  {oLichPhongItem.Title} ngày {oLichPhongItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy")} ";
                        lNotificationItem.LNotiIsSentMail = 1;
                        lNotificationItem.LNotiSentTime = DateTime.Now;
                        lNotificationItem.UrlLink = "/Pages/meeting.aspx?Lich=Phong" + $"&ItemID={oLichPhongItem.ID}";
                        oLNotificationDA.UpdateObject<LNotificationItem>(lNotificationItem);

                        #endregion

                        AddLog("UPDATE", "LichPhong", "UPDATE", oGridRequest.ItemID, $"Cập nhật lịch phòng{oLichPhongItem.Title}");
                    }
                    else
                    {
                        oLichPhongItem.UpdateObject(context.Request);
                        mailto = new List<string>();

                        if (oLichPhongItem.LichLanhDaoChuTri.LookupId > 0 && oLichPhongItem.LichThanhPhanThamGia.FindIndex(x => x == oLichPhongItem.LichLanhDaoChuTri) == -1)
                            lstThamGia.Add(oLichPhongItem.LichLanhDaoChuTri);
                        lstThamGia.AddRange(oLichPhongItem.LichThanhPhanThamGia);
                        foreach (var item in lstThamGia)
                        {
                            var LUser = oLUserDA.GetByIdToObject<LUserItem>(item.LookupId);
                            if (!string.IsNullOrEmpty(LUser.UserEmail))
                            {
                                mailto.Add(LUser.UserEmail);
                            }
                        }
                        oLNotificationDA.UpdateObject<LNotificationItem>(lNotificationItem);
                        //gửi mail
                        contentMail = "Kính gửi ông/bà,";
                        contentMail += "<div> Ông bà đã nhận được thông báo về cuộc họp: " + oLichPhongItem.Title + " vào hồi " + oLichPhongItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy") + " tại " + oLichPhongItem.LichDiaDiem + "</div>";
                        contentMail += "<div>Nhận được mail này mong quý vị sắp xếp công việc và thời gian tham gia cuộc họp.</div>";
                        contentMail += "<div>Trân trọng cảm ơn.</div>";
                        if (mailto.Count > 0)
                        {
                            try
                            {
                                SendMail("[Email thông báo lịch họp phòng]", contentMail, mailto);
                            }
                            catch (Exception ex)
                            {
                                AddLog("SENDMAIL", "LichPhong", "SENDMAIL", oGridRequest.ItemID, $"Gửi mail lịch phòng {oLichPhongItem.Title} lỗi");
                            }
                        }
                        #region Lưu Notifi
                        string NguoiTao = context.Request["NguoiTao"];
                        lNotificationItem.TitleBaiViet = oLichPhongItem.Title;
                        lNotificationItem.LNotiNguoiGui = NguoiTao;
                        if (lstThamGia.Count > 0)
                        {
                            lNotificationItem.LNotiNguoiNhan_Id = "," + string.Join(",", lstThamGia) + ",";
                            lNotificationItem.LNotiChuaXem_Id = lNotificationItem.LNotiNguoiNhan_Id;
                        }
                        lNotificationItem.DoiTuongTitle = "Lịch họp phòng";
                        lNotificationItem.LNotiData = Newtonsoft.Json.JsonConvert.SerializeObject(new SPEntity() { ID = oLichPhongItem.ID, Title = oLichPhongItem.Title });
                        lNotificationItem.Title = $"Lịch họp:  {oLichPhongItem.Title} ngày {oLichPhongItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy")} ";
                        lNotificationItem.LNotiMoTa = $"Lịch họp:  {oLichPhongItem.Title} ngày {oLichPhongItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy")} ";
                        lNotificationItem.LNotiIsSentMail = 1;
                        lNotificationItem.LNotiSentTime = DateTime.Now;
                        lNotificationItem.UrlLink = "/Pages/meeting.aspx?Lich=Phong" + $"&ItemID={oLichPhongItem.ID}";

                        #endregion
                        oLichPhongDA.UpdateObject<LichPhongItem>(oLichPhongItem);
                        AddLog("UPDATE", "LichPhong", "ADD", oGridRequest.ItemID, $"Thêm mới lịch phòng {oLichPhongItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oLichPhongDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "LichPhong", "DELETE", oGridRequest.ItemID, $"Xóa lịch phòng {outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oLichPhongDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    AddLog("DELETE-MULTI", "LichPhong", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều lịch phòng");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oLichPhongDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "LichPhong", "APPROVED", oGridRequest.ItemID, $"Duyệt lịch phòng {oLichPhongDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oLichPhongDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "LichPhong", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt lịch phòng {oLichPhongDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLichPhongItem = oLichPhongDA.GetByIdToObjectSelectFields<LichPhongItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oLichPhongItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}