﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.LichPhong;

namespace VIPortal.APP.UserControls.AdminCMS.LichPhong
{
    public partial class pFormLichPhong : pFormBase
    {
        public LichPhongItem oLichPhong { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oLichPhong = new LichPhongItem();
            if (ItemID > 0)
            {
                LichPhongDA oLichPhongDA = new LichPhongDA();
                oLichPhong = oLichPhongDA.GetByIdToObject<LichPhongItem>(ItemID);
            }
        }
    }
}