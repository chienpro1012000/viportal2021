<%@ control language="C#" autoeventwireup="true" codebehind="UC_MangXaHoi.aspx.cs" inherits="VIPortalAPP.UserControls.AdminCMS.MangXaHoi.UC_MangXaHoi" %>
<div role="body-data" data-title="" class="content_wp" data-action="/UserControls/AdminCMS/MangXaHoi/pAction.asp" data-view="/UserControls/AdminCMS/MangXaHoi/pViewMangXaHoi.aspx" data-form="/UserControls/AdminCMS/MangXaHoi/pFormMangXaHoi.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="MangXaHoiSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa" style="margin-right: 15px;">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="ThreadNoiDung" />
                            <select data-url="/UserControls/AdminCMS/LUser/pAction.asp?do=QUERYDATA" style="width:300px;" data-place="Chọn người tạo" name="CreatedUser" id="CreatedUser" class="form-control mr-2"></select>
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                    </p>
                </div>
            </div>

            <div class="clsgrid table-responsive">

                <table id="tblMangXaHoi" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#CreatedUser").smSelect2018V2({
            dropdownParent: "#MangXaHoiSearch",
            Ajax: true
        });
        var $tagvalue = $(".content_wp");
        var $tblDanhMucChung = $("#tblMangXaHoi").viDataTable(
            {
                "frmSearch": "MangXaHoiSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "name": "Created", "sTitle": "Thời gian",
                            "mData": function (o) {
                                return formatDateTime(o.Created);
                            },
                            "sWidth": "100px",
                        }, {
                            "mData": function (o) {
                                if (o.ThreadNoiDung.length > 500) {
                                    return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.ThreadNoiDung.substr(0, 500) + '...' + '</a>';
                                }
                                else {
                                    return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.ThreadNoiDung + '</a>';
                                }
                            },
                            "name": "Title", "sTitle": "Bài viết",
                        }, {
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '">' + o.DanhMuc.Title + '</a>';
                            },
                            "name": "DanhMuc", "sTitle": "Danh mục",
                            "sWidth": "120px",
                        }, {
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '">' + o.CreatedUser.Title + '</a>';
                            },
                            "name": "CreatedUser", "sTitle": "Người tạo",
                            "sWidth": "120px",
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {
                                if (o._ModerationStatus == 0)
                                    return '<a title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }
                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        },
                        {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE" title="Xóa"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                "aaSorting": [0, 'asc']
            });
    });
</script>
