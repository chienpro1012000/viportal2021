<%@ page language="C#" autoeventwireup="true" codebehind="pViewMangXaHoi.aspx.cs" inherits="VIPortalAPP.UserControls.AdminCMS.MangXaHoi.pViewMangXaHoi" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <%=oMangXaHoi.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="ThreadNoiDung" class="col-sm-2 control-label">Nội dung</label>
            <div class="col-sm-10">
                <%=oMangXaHoi.ThreadNoiDung%>
            </div>
        </div>
        <div class="form-group row">
            <label for="CreatedUser" class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-10">
                <%=oMangXaHoi.CreatedUser.LookupValue%>
            </div>
        </div>
        <div class="form-group row">
            <label for="SoLuotXem" class="col-sm-2 control-label">Số lượt xem</label>
            <div class="col-sm-4">
                <%=oMangXaHoi.SoLuotXem%>
            </div>
            <label for="SoLuotComment" class="col-sm-2 control-label">Số lượt Comment</label>
            <div class="col-sm-4">
                <%=oMangXaHoi.SoLuotComment%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oMangXaHoi.ListFileAttach.Count;i++){ %>
                <div><a href="<%=oMangXaHoi.ListFileAttach[i].Url %>"><%=oMangXaHoi.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="form-group row">
            <label for="AnhDaiDien" class="col-sm-2 control-label">Ảnh đại diện</label>
            <div class="col-sm-10">
                <img style="max-width:200px;" src="<%=oMangXaHoi.AnhDaiDien%>" />
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oMangXaHoi.Created)%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oMangXaHoi.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-10">
                <%=oMangXaHoi.Author.LookupValue%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-10">
                <%=oMangXaHoi.Editor.LookupValue%>
            </div>
        </div>

        <div class="clearfix"></div>
    </form>
</body>
</html>
