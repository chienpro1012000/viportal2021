using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;

namespace VIPortalAPP.MangXaHoi
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            MangXaHoiDA oMangXaHoiDA = new MangXaHoiDA(UrlSite);
            MangXaHoiItem oMangXaHoiItem = new MangXaHoiItem();
            CommentMXHDA oCommentMXHDA = new CommentMXHDA(UrlSite);            
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oMangXaHoiDA.GetListJson(new MangXaHoiQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw, 
                        oMangXaHoiDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oMangXaHoiDA.GetListJson(new MangXaHoiQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oMangXaHoiItem = oMangXaHoiDA.GetByIdToObject<MangXaHoiItem>(oGridRequest.ItemID);
                        oMangXaHoiItem.UpdateObject(context.Request);
                        oMangXaHoiDA.UpdateObject<MangXaHoiItem>(oMangXaHoiItem);
                        //oMangXaHoiDA.UpdateSPModerationStatus(oMangXaHoiItem.ID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    }
                    else
                    {
                        oMangXaHoiItem.UpdateObject(context.Request);
                        if (oMangXaHoiItem.ThreadNoiDung.Length > 255)
                            oMangXaHoiItem.Title = oMangXaHoiItem.ThreadNoiDung.Substring(0, 250);
                        else
                            oMangXaHoiItem.Title = oMangXaHoiItem.ThreadNoiDung;
                        oMangXaHoiItem.UpdateObjectOnlyFile(context.Request, "DaiDien");
                        oMangXaHoiDA.UpdateObject<MangXaHoiItem>(oMangXaHoiItem);
                    }
                    oResult.Message = "Cập nhật bài đăng thành công";
                    break;
                case "DELETE":
                    string outb = oMangXaHoiDA.DeleteObject(oGridRequest.ItemID);
                    List<CommentMXHJson> lstComment = oCommentMXHDA.GetListJson(new CommentMXHQuery() { BaiViet = oGridRequest.ItemID });
                    if (lstComment.Count > 0)
                    {
                        foreach (var item in lstComment)
                        {
                            oCommentMXHDA.DeleteObject(item.ID);
                        }
                    }
                    if (string.IsNullOrEmpty(outb))
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb;
                    break;
                case "DELETE-MULTI":
                    var outbmulti = "";
                    AddLog("DELETE-MULTI", "Lconfig", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều tham số hệ thống");
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oMangXaHoiDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oMangXaHoiDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                     oMangXaHoiDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oMangXaHoiItem = oMangXaHoiDA.GetByIdToObjectSelectFields<MangXaHoiItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oMangXaHoiItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}