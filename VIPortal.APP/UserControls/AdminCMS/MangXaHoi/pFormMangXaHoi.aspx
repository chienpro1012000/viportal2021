﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormMangXaHoi.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.MangXaHoi.pFormMangXaHoi" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-MangXaHoi" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oMangXaHoi.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
       <div class="form-group row">
            <label for="MaSV" class="col-sm-2 control-label">Nội dung</label>
            <div class="col-sm-10">
                <textarea name="ThreadNoiDung" id="ThreadNoiDung" placeholder="Nội dung" class="form-control"><%:oMangXaHoi.ThreadNoiDung%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="MenuParent" class="col-sm-2 control-label">Danh mục</label>
            <div class="col-sm-10">
                <select data-selected="<%:oMangXaHoi.DanhMuc.LookupId%>" data-url="/UserControls/AdminCMS/DanhMucMXH/pAction.asp?do=QUERYDATA&moder=1" data-place="Chọn danh mục" name="DanhMuc" id="DanhMuc" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttachDaiDien" multiple="multiple" />
            </div>
        </div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oMangXaHoi.ListFileAttach)%>',
                preview: true
            });
            $("#Title").focus();
            $("#DanhMuc").smSelect2018V2({
                dropdownParent: "#frm-MangXaHoi"
            });

            $("#frm-MangXaHoi").validate({
                rules: {
                    ThreadNoiDung: "required",
                },
                messages: {
                    ThreadNoiDung: "Vui lòng nhập tên loại tài liệu"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/MangXaHoi/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-MangXaHoi").trigger("click");
                            $(form).closeModal();
                            $('#tblMangXaHoi').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-MangXaHoi").viForm();
        });
    </script>
</body>
</html>

