using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP.UserControls.AdminCMS.MangXaHoi
{
    public partial class pViewMangXaHoi : pFormBase
    {
        public MangXaHoiItem oMangXaHoi = new MangXaHoiItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oMangXaHoi = new MangXaHoiItem();
            if (ItemID > 0)
            {
                MangXaHoiDA oMangXaHoiDA = new MangXaHoiDA(UrlSite);
                oMangXaHoi = oMangXaHoiDA.GetByIdToObject<MangXaHoiItem>(ItemID);

            }
        }
    }
}