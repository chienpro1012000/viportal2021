using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;

namespace VIPortalAPP.UserControls.AdminCMS.LienKetWebsite
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            LienKetWebsiteDA oLienKetWebsiteDA = new LienKetWebsiteDA(UrlSite);
            LienKetWebsiteItem oLienKetWebsiteItem = new LienKetWebsiteItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oLienKetWebsiteDA.GetListJson(new LienKetWebsiteQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw, 
                        oLienKetWebsiteDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "LienKetWebsite", "QUERYDATA", 0, "Xem danh sách liên kết website");
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oLienKetWebsiteDA.GetListJson(new LienKetWebsiteQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLienKetWebsiteItem = oLienKetWebsiteDA.GetByIdToObject<LienKetWebsiteItem>(oGridRequest.ItemID);
                        oLienKetWebsiteItem.UpdateObject(context.Request);
                        oLienKetWebsiteDA.UpdateObject<LienKetWebsiteItem>(oLienKetWebsiteItem);
                        AddLog("UPDATE", "LienKetWebsite", "UPDATE", oGridRequest.ItemID, $"Cập nhật liên kết website{oLienKetWebsiteItem.Title}");
                    }
                    else
                    {
                        oLienKetWebsiteItem.UpdateObject(context.Request);
                        oLienKetWebsiteDA.UpdateObject<LienKetWebsiteItem>(oLienKetWebsiteItem);
                        AddLog("UPDATE", "LienKetWebsite", "ADD", oGridRequest.ItemID, $"Thêm mới liên kết website {oLienKetWebsiteItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oLienKetWebsiteDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "LienKetWebsite", "DELETE", oGridRequest.ItemID, $"Xóa liên kết website {outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    AddLog("DELETE-MULTI", "LienKetWebsite", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều liên kết website");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oLienKetWebsiteDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "LienKetWebsite", "APPROVED", oGridRequest.ItemID, $"Duyệt liên kết website {oLienKetWebsiteDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                     oLienKetWebsiteDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "LienKetWebsite", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt liên kết website {oLienKetWebsiteDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLienKetWebsiteItem = oLienKetWebsiteDA.GetByIdToObjectSelectFields<LienKetWebsiteItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oLienKetWebsiteItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}