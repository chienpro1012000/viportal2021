<%@ control language="C#" autoeventwireup="true" codebehind="UC_LienKetWebsite.aspx.cs" inherits="VIPortalAPP.UserControls.AdminCMS.LienKetWebsite.UC_LienKetWebsite" %>
<div role="body-data" data-title="" class="content_wp" data-action="/UserControls/AdminCMS/LienKetWebsite/pAction.asp" data-form="/UserControls/AdminCMS/LienKetWebsite/pFormLienKetWebsite.aspx" data-view="/UserControls/AdminCMS/LienKetWebsite/pViewLienKetWebsite.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="LienKetWebsiteSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title" />
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <button class="btn btn-primary act-add" data-per="010902" type="button">Thêm mới</button>
                    </p>
                </div>
            </div>
            <div class="clsgrid table-responsive">
                <table id="tblLienKetWebsite" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        var $tagvalue = $(".content_wp");
        var $tblDanhMucChung = $("#tblLienKetWebsite").viDataTable(
            {
                "frmSearch": "LienKetWebsiteSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "mData": function (o) {
                                return o.ParentLienKet != null ? o.ParentLienKet.Title : "";
                            },
                            "name": "ParentLienKet", "sTitle": "Phân nhóm"
                        },{
                            "data": "STT",
                            "sWidth": "20px",
                            "name": "STT", "sTitle": "STT"
                        },
                        {
                            "mData": function (o) {
                                return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                            },
                            "name": "Title", "sTitle": "Tiêu đề"
                        },
                        {
                            "data": "Link",
                            "name": "Link", "sTitle": "Liên kết"
                        }, {
                            "mData": function (o) {
                                if (o.AnhDaiDien != null && o.AnhDaiDien != '')
                                    return ' <img  data-id="' + o.ID + '" style="width:100px;height:60px"  src="' + GetPathUrlString(o.AnhDaiDien) + '" alt="ImageNews" />'
                                else
                                    return ''
                            },
                            "name": "ImageNews", "sTitle": "Ảnh"
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {
                                if (o._ModerationStatus == 0)

                                    return '<a data-per="010905" title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a data-per="010905" title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }
                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" title="Sửa" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        }, {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE" title="Xóa"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                "aaSorting": [0, 'asc']
            });
    });
</script>
