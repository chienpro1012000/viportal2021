<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormLienKetWebsite.aspx.cs" Inherits="VIPortalAPP.UserControls.AdminCMS.LienKetWebsite.pFormLienKetWebsite" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-LienKetWebsite" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oLienKetWebsite.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="ParentLienKet" class="col-sm-2 control-label">Phân loại</label>
            <div class="col-sm-10">
                 <select data-selected="<%:oLienKetWebsite.ParentLienKet.LookupId%>" data-url="/UserControls/AdminCMS/LienKetWebsite/pAction.asp?do=QUERYDATA&NotHasLink=True" data-place="Chọn Phân loại" name="ParentLienKet" id="ParentLienKet" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <label for="STT" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-10">
                <input type="text" name="STT" id="STT" placeholder="Nhập stt" value="<%:oLienKetWebsite.STT%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu liên kết</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oLienKetWebsite.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="Link" class="col-sm-2 control-label">Địa chỉ liên kết</label>
            <div class="col-sm-10">
                <input name="Link" id="Link" class="form-control" value="<%:oLienKetWebsite.Link%>" />
            </div>
        </div>
        <!--AnhDaiDien-->
        <div class="form-group row">
            <label for="AnhDaiDien" class="col-sm-2 control-label">Ảnh đại diện</label>
            <div class="col-sm-10" data-role="groupImageDD">
                <input type="hidden" name="AnhDaiDien" id="AnhDaiDien" value="<%=oLienKetWebsite.AnhDaiDien%>" />
                <img title="Ảnh đại diện" id="srcImageNews" src="<%=oLienKetWebsite.AnhDaiDien%>" />
                <button id="btnImageNews" type="button" class="btn  btn-success">Ảnh đại diện</button>
                <button id="btnXoaImageNews" type="button" class="btn btn-danger">Xóa ảnh</button>
            </div>
        </div>
        <div class="form-group row">
            <label for="MoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <textarea name="MoTa" id="MoTa" class="form-control"><%:oLienKetWebsite.MoTa%></textarea>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            //ParentLienKet
            $("#ParentLienKet").smSelect2018V2({
                dropdownParent: "#frm-LienKetWebsite"
            });
            $("#btnImageNews").click(function () {
                $elementId = $(this);
                CKFinder.popup({
                    chooseFiles: true,
                    width: 800,
                    resourceType: 'Images',
                    height: 600,
                    onInit: function (finder) {
                        finder.on('files:choose', function (evt) {
                            var file = evt.data.files.first();
                            //var output = document.getElementById(elementId);
                            //console.log(file.getUrl());
                            //$("#ImageNews").value(file.getUrl());
                            var urlfileimg = file.getUrl();
                            document.getElementById("AnhDaiDien").value = urlfileimg;
                            document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                        });
                        finder.on('file:choose:resizedImage', function (evt) {
                            //var output = document.getElementById(elementId);
                            console.log(evt.data);
                            var urlfileimg = evt.data.resizedUrl;
                            document.getElementById("AnhDaiDien").value = urlfileimg;
                            document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                        });
                    }
                });
            });
            //DeleteImage('btnXoaImageNews', 'srcImageNews', 'AnhDaiDien');
            $("#btnXoaImageNews").click(function () {
                document.getElementById("AnhDaiDien").value = "";
                document.getElementById("srcImageNews").setAttribute("src", "");
            });
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oLienKetWebsite.ListFileAttach)%>'
            });
            $("#STT").focus();

            $("#frm-LienKetWebsite").validate({
                rules: {
                    Title: "required",
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/LienKetWebsite/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-LienKetWebsite").trigger("click");
                            $(form).closeModal();
                            $('#tblLienKetWebsite').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-LienKetWebsite").viForm();
        });
    </script>
</body>
</html>
