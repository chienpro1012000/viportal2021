using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP.UserControls.AdminCMS.LienKetWebsite
{
    public partial class pFormLienKetWebsite : pFormBase
    {
        public LienKetWebsiteItem oLienKetWebsite {get;set;}
        protected void Page_Load(object sender, EventArgs e)
        {
			oLienKetWebsite = new LienKetWebsiteItem();
            if (ItemID > 0)
            {
                LienKetWebsiteDA oLienKetWebsiteDA = new LienKetWebsiteDA(UrlSite);
                oLienKetWebsite = oLienKetWebsiteDA.GetByIdToObject<LienKetWebsiteItem>(ItemID);
            }
        }
    }
}