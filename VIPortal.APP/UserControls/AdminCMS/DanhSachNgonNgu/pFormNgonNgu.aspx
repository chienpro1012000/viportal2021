﻿<%@ page language="C#" autoeventwireup="true" codebehind="pFormNgonNgu.aspx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.DanhSachNgonNgu.pFormNgonNgu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
    <style type="text/css">
        .ace_editor {
            height: 300px;
        }
    </style>
</head>
<body>
    <form id="frm-DanhSachNgonNgu" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oDanhSachNgonNgu.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" name="Title" id="Title" value="<%=oDanhSachNgonNgu.Title %>" />
        <div class="form-group row">
            <label for="DMVietTat" class="col-sm-1 control-label">Viết tắt</label>
            <div class="col-sm-5">
                <%=oDanhSachNgonNgu.DMVietTat%>
            </div>
        </div>
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên ngôn ngữ</label>
            <div class="col-sm-10">
                <%=oDanhSachNgonNgu.Title%>
            </div>
        </div>

        <div class="form-group row">
            <label for="DMMoTa" class="col-sm-2 control-label">Ngôn ngữ</label>
            <div class="col-sm-10">
                <textarea name="LanguageValue" id="LanguageValue" placeholder="Nhập nội dung mô tả" class="form-control"><%=oDanhSachNgonNgu.Language%></textarea>
            </div>
        </div>

        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="btnUpdate">Cập nhật</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            
        </div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#btnUpdate").click(function () {
                CK_jQ();
                $("#frm-DanhSachNgonNgu").submit();
            });
            var editor = ace.edit("LanguageValue");
            editor.session.setMode("ace/mode/json");
            editor.setTheme("ace/theme/tomorrow");
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oDanhSachNgonNgu.ListFileAttach)%>'
            });
            $("#Title").focus();

            $("#frm-DanhSachNgonNgu").validate({
                ignore: [],
                rules: {
                },
                messages: {
                },
                submitHandler: function (form) {
                    var dataPost = $(form).serializeArray();
                    var code = editor.getValue();
                    dataPost.push({ name: "Language", value: code });
                    $.post("/UserControls/AdminCMS/DanhSachNgonNgu/pAction.asp", dataPost, function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-DanhSachNgonNgu").trigger("click");
                            $(form).closeModal();
                            $('#tblDanhSachNgonNgu').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-DanhSachNgonNgu").viForm();
        });
    </script>
</body>
</html>

