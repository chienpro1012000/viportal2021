﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_DanhSachNgonNgu.ascx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.DanhSachNgonNgu.UC_DanhSachNgonNgu" %>

<script type="text/javascript" src="/Content/plugins/ace-master/src-min-noconflict/ace.js"></script>
<div role="body-data" data-size="1040" data-title="ngôn ngữ" class="content_wp" data-action="/UserControls/AdminCMS/DanhSachNgonNgu/pAction.asp" data-form="/UserControls/AdminCMS/DanhSachNgonNgu/pFormDanhSachNgonNgu.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="DanhSachNgonNguSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title" />
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <button class="btn btn-primary act-add" data-per="080602" type="button">Thêm mới</button>
                    </p>
                </div>
            </div>

            <div class="clsgrid table-responsive">

                <table id="tblDanhSachNgonNgu" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">


    $(document).ready(function () {

        var $tagvalue = $(".content_wp");
        var $tblDanhMucChung = $("#tblDanhSachNgonNgu").viDataTable(
            {
                "frmSearch": "DanhSachNgonNguSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "sWidth": "30px",
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '" >' + o.DMSTT + '</a>';
                            },
                            "name": "DMSTT", "sTitle": "STT"
                        }, {
                            "mData": function (o) {
                                if (o.ColumnImageSrc != null && o.ColumnImageSrc != '' && o.ColumnImageSrc != "")
                                    return ' <img  data-id="' + o.ID + '" style="height:40px"  src="' + o.ColumnImageSrc + '" alt="ImageNews" />'
                                else
                                    return ''
                            },
                            "name": "ColumnImageSrc", "sTitle": "Ảnh"
                        },
                        {
                            "mData": function (o) {
                                if (o.Title != null)
                                    return '<a data-id="' + o.ID + '" >' + o.Title + '</a>';
                                else
                                    return "";
                            },
                            "name": "Title", "sTitle": "Tên ngôn ngữ "
                        },

                        {
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '" >' + o.DMVietTat + '</a>';
                            },
                            "name": "DMDescription", "sTitle": "Tên viết tắt"
                        },
                        {
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '" >' + o.URLWeb + '</a>';
                            },
                            "name": "DMMoTa", "sTitle": "URLWeb"
                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) {
                                return '<a title="Cấu hình ngôn ngữ" url="/UserControls/AdminCMS/DanhSachNgonNgu/pFormNgonNgu.aspx" class="btn default btn-xs purple btnfrmnocheknobutton" data-do="UPDATE" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fas fa-sliders-h"></i></a>';
                            }
                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {
                                if (o._ModerationStatus == 0)
                                    return '<a data-Per="080605" title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a data-Per="080605" title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }
                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" title="Sửa" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        }, {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE" title="Xóa"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                "aaSorting": [0, 'asc']
            });
    });
</script>
