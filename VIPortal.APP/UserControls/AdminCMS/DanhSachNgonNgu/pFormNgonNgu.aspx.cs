﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData.DanhSachNgonNgu;

namespace VIPortal.APP.UserControls.AdminCMS.DanhSachNgonNgu
{
    public partial class pFormNgonNgu : pFormBase
    {
        public DanhSachNgonNguItem oDanhSachNgonNgu { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oDanhSachNgonNgu = new DanhSachNgonNguItem();
            if (ItemID > 0)
            {
                DanhSachNgonNguDA oDanhSachNgonNguDA = new DanhSachNgonNguDA();
                oDanhSachNgonNgu = oDanhSachNgonNguDA.GetByIdToObject<DanhSachNgonNguItem>(ItemID);
            }
        }
    }
}