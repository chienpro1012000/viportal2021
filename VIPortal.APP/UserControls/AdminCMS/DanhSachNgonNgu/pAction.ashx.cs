﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData.DanhSachNgonNgu;

namespace VIPortal.APP.UserControls.AdminCMS.DanhSachNgonNgu
{

    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);
            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            DanhSachNgonNguDA oDanhSachNgonNguDA = new DanhSachNgonNguDA();
            DanhSachNgonNguItem oDanhSachNgonNguItem = new DanhSachNgonNguItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    var oData = oDanhSachNgonNguDA.GetListJson(new DanhSachNgonNguQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oDanhSachNgonNguDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "DanhSachNgonNgu", "QUERYDATA", 0, "Xem danh sách danh sách ngôn ngũ");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataDanhSachNgonNgu = oDanhSachNgonNguDA.GetListJson(new DanhSachNgonNguQuery(context.Request));
                    treeViewItems = oDataDanhSachNgonNgu.Select(x => new TreeViewItem()
                    {
                        key = x.DMDescription,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;

                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oDanhSachNgonNguDA.GetListJson(new DanhSachNgonNguQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oDanhSachNgonNguDA.CheckExit(oGridRequest.ItemID, context.Request["Title"]) == 0)
                    {
                        if (oGridRequest.ItemID > 0)
                        {
                            oDanhSachNgonNguItem = oDanhSachNgonNguDA.GetByIdToObject<DanhSachNgonNguItem>(oGridRequest.ItemID);
                            oDanhSachNgonNguItem.UpdateObject(context.Request);
                            
                            oDanhSachNgonNguDA.UpdateObject<DanhSachNgonNguItem>(oDanhSachNgonNguItem);
                            oDanhSachNgonNguDA.UpdateSPModerationStatus(oGridRequest.ItemID, oDanhSachNgonNguItem._ModerationStatus);
                            AddLog("UPDATE", "DanhSachNgonNgu", "UPDATE", oGridRequest.ItemID, $"Cập nhật danh sách ngôn ngũ{oDanhSachNgonNguItem.Title}");
                        }
                        else
                        {
                            oDanhSachNgonNguItem.UpdateObject(context.Request);
                            oDanhSachNgonNguDA.UpdateObject<DanhSachNgonNguItem>(oDanhSachNgonNguItem);
                            AddLog("UPDATE", "DanhSachNgonNgu", "ADD", oGridRequest.ItemID, $"Thêm mới danh sách ngôn ngũ {oDanhSachNgonNguItem.Title}");
                        }
                        oResult.Message = "Lưu thành công";
                    }
                    else
                    {
                        oResult = new ResultAction() { State = ActionState.Error, Message = $"Tiêu đề {context.Request["Title"]} đã tồn tại trong hệ thống" };
                    }
                    break;
                case "DELETE":
                    var outb = oDanhSachNgonNguDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "DanhSachNgonNgu", "DELETE", oGridRequest.ItemID, $"Xóa danh sách ngôn ngũ {outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    AddLog("DELETE-MULTI", "DanhSachNgonNgu", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều danh sách ngôn ngũ");
                    var outbmulti = "";
                    AddLog("DELETE-MULTI", "DanhSachNgonNgu", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều danh sách ngôn ngũ");
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oDanhSachNgonNguDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oDanhSachNgonNguDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "DanhSachNgonNgu", "APPROVED", oGridRequest.ItemID, $"Duyệt danh sách ngôn ngũ {oDanhSachNgonNguDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oDanhSachNgonNguDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "DanhSachNgonNgu", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt danh sách ngôn ngũ {oDanhSachNgonNguDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDanhSachNgonNguItem = oDanhSachNgonNguDA.GetByIdToObjectSelectFields<DanhSachNgonNguItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oDanhSachNgonNguItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}