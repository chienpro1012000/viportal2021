﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewDanhSachNgonNgu.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.DanhSachNgonNgu.pViewDanhSachNgonNgu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
        <div class="form-group row">
            <label for="DMSTT" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-4">
                <%:(oDanhSachNgonNgu!= null &&  oDanhSachNgonNgu.DMSTT>0)?oDanhSachNgonNgu.DMSTT+"": "" %>
            </div>
              <label for="DMVietTat" class="col-sm-1 control-label">Viết tắt</label>
            <div class="col-sm-5">
                <%:oDanhSachNgonNgu.DMVietTat%>"
        </div>
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên ngôn ngữ</label>
            <div class="col-sm-10">
                <%=oDanhSachNgonNgu.Title%>
        </div>
         <div class="form-group row">
	<label for="DMMoTa" class="col-sm-2 control-label">Mô tả</label>
	<div class="col-sm-10">
		<%=oDanhSachNgonNgu.DMMoTa%>
	</div>
</div>
         <div class="form-group row">
	<label for="DMDMDescriptionMoTa" class="col-sm-2 control-label">Chi tiết</label>
	<div class="col-sm-10">
		<%=oDanhSachNgonNgu.DMDescription%>
	</div>
</div>
        
        <div class="form-group row">
            <label for="URLWeb" class="col-sm-2 control-label">URL Web</label>
            <div class="col-sm-10">
                <%=oDanhSachNgonNgu.URLWeb%>
            </div>
        </div>
         <div class="form-group row">
            <label class="control-label col-sm-2">Hiển thị</label>
            <div class="col-sm-4">
                <div class="checkbox">
                    <%:oDanhSachNgonNgu.DMHienThi?"Có":"Không" %>/>
                </div>
            </div>
        </div>

            <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oDanhSachNgonNgu.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oDanhSachNgonNgu.ListFileAttach[i].Url %>"><%=oDanhSachNgonNgu.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
         <div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%=string.Format("{0:dd/MM/yyyy}", oDanhSachNgonNgu.Created)%>
            </div>
              <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy}",oDanhSachNgonNgu.Modified)%>
            </div>
          </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
                <%=oDanhSachNgonNgu.Author.LookupValue%>
            </div>
             <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
                <%=oDanhSachNgonNgu.Editor.LookupValue%>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>
