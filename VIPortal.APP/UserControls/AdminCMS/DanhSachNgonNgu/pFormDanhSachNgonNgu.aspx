﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormDanhSachNgonNgu.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.DanhSachNgonNgu.pFormDanhSachNgonNgu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-DanhSachNgonNgu" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oDanhSachNgonNgu.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="DMSTT" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-4">
                <input type="text" name="DMSTT" id="DMSTT" placeholder="Nhập số thứ tự" value="<%:oDanhSachNgonNgu.DMSTT%>" class="form-control" />
            </div>
            <label for="DMVietTat" class="col-sm-1 control-label">Viết tắt</label>
            <div class="col-sm-5">
                <input type="text" name="DMVietTat" id="DMVietTat" placeholder="Nhập tên viết tắt" value="<%:oDanhSachNgonNgu.DMVietTat%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên ngôn ngữ</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" placeholder="Nhập tên ngôn ngữ" value="<%=oDanhSachNgonNgu.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="ColumnImage" class="col-sm-2 control-label">Ảnh Icon</label>
            <div class="col-sm-10" data-role="groupImageDD">
                <div id="ColumnImageZone" style="float: left; padding-right: 10px;">
                    <div class="include-library" style="display: inline-block; clear: both;">
                        <input type="hidden" name="ColumnImage" id="ColumnImage" value="<%:oDanhSachNgonNgu.ColumnImage%>" />
                        <img title="Ảnh Icon" id="srcColumnImage" src="<%=oDanhSachNgonNgu.ColumnImageSrc%>" />
                    </div>
                </div>
                <button id="btnColumnImage" type="button" class="btn  btn-success">Ảnh đại diện</button>
                <button id="btnXoaColumnImage" type="button" class="btn btn-danger">Xóa ảnh</button>
            </div>
        </div>
        <div class="form-group row">
            <label for="DMMoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <textarea name="DMMoTa" id="DMMoTa" placeholder="Nhập nội dung mô tả" class="form-control"><%=oDanhSachNgonNgu.DMMoTa%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="DMDMDescriptionMoTa" class="col-sm-2 control-label">Chi tiết</label>
            <div class="col-sm-10">
                <textarea name="DMDescription" placeholder="Nhập nội dung Chi tiết" id="DMDescription" class="form-control"><%=oDanhSachNgonNgu.DMDescription%></textarea>
            </div>
        </div>

        <div class="form-group row">
            <label for="URLWeb" class="col-sm-2 control-label">URL Web</label>
            <div class="col-sm-10">
                <input type="text" name="URLWeb" id="URLWeb" class="form-control" placeholder="Nhập link" value="<%=oDanhSachNgonNgu.URLWeb%>" />
            </div>
        </div>
        <div class="form-group row">
            <label class="control-label col-sm-2">Hiển thị</label>
            <div class="col-sm-4">
                <div class="checkbox">
                    <input type="radio" id="co" name="DMHienThi" value="True" <%:oDanhSachNgonNgu.DMHienThi?"checked":"" %> />
                    <label for="co" style="padding-left: 1%; padding-right: 10%;">Có </label>
                    <input type="radio" id="khong" name="DMHienThi" value="False" <%:!oDanhSachNgonNgu.DMHienThi?"checked":"" %> />
                    <label for="khong" style="padding-left: 1%;">Không</label>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#btnColumnImage").click(function () {
                $elementId = $(this);
                CKFinder.popup({
                    chooseFiles: true,
                    width: 800,
                    height: 600,
                    onInit: function (finder) {
                        finder.on('files:choose', function (evt) {
                            var file = evt.data.files.first();
                            //var output = document.getElementById(elementId);
                            //console.log(file.getUrl());
                            //$("#ColumnImage").value(file.getUrl());
                            var urlfileimg = file.getUrl();
                            document.getElementById("ColumnImage").value = urlfileimg;
                            document.getElementById("srcColumnImage").setAttribute("src", urlfileimg);
                        });
                        finder.on('file:choose:resizedImage', function (evt) {
                            //var output = document.getElementById(elementId);
                            console.log(evt.data);
                            var urlfileimg = evt.data.resizedUrl;
                            document.getElementById("ColumnImage").value = urlfileimg;
                            document.getElementById("srcColumnImage").setAttribute("src", urlfileimg);
                        });
                    }
                });
            });
            //DeleteImage('btnXoaColumnImage', 'imgColumnImage', 'ColumnImage');
            $("#btnXoaColumnImage").click(function () {
                document.getElementById("ColumnImage").value = "";
                document.getElementById("srcColumnImage").setAttribute("src", "");
            });

            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oDanhSachNgonNgu.ListFileAttach)%>'
            });
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });

            $("#frm-DanhSachNgonNgu").validate({
                ignore: [],
                rules: {
                    Title: "required",
                    DMSTT: {
                        numberchar: true, required: true
                    },
                    URLWeb: "required",
                    ColumnImage: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề ngôn ngữ",
                    URLWeb: "Vui lòng nhập địa chỉ trang",
                    ColumnImage: "Vui lòng chọn ảnh icon"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/DanhSachNgonNgu/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-DanhSachNgonNgu").trigger("click");
                            $(form).closeModal();
                            $('#tblDanhSachNgonNgu').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-DanhSachNgonNgu").viForm();
        });
    </script>
</body>
</html>

