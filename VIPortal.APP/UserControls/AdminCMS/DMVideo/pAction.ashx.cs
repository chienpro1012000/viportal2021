﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using VIPortalAPP;
using ViPortalData.DMVideo;

namespace VIPortal.APP.UserControls.AdminCMS.DMVideo
{

    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            DMVideoDA oDMVideoDA = new DMVideoDA(UrlSite);
            DMVideoItem oDMVideoItem = new DMVideoItem();
            VideoDA oVideoDA = new VideoDA();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    var oData = oDMVideoDA.GetListJsonSolr(new DMVideoQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oDMVideoDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "DMVideo", "QUERYDATA", 0, "Xem danh sách danh mục video");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataDMVideo = oDMVideoDA.GetListJson(new DMVideoQuery(context.Request));
                    treeViewItems = oDataDMVideo.Select(x => new TreeViewItem()
                    {
                        key = x.DMMoTa,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;

                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oDMVideoDA.GetListJson(new DMVideoQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDMVideoItem = oDMVideoDA.GetByIdToObject<DMVideoItem>(oGridRequest.ItemID);
                        oDMVideoItem.UpdateObject(context.Request);
                        if (oDMVideoDA.CheckExit(oGridRequest.ItemID, oDMVideoItem.Title) > 0)
                        {
                            oResult = new ResultAction()
                            {
                                State = ActionState.Error,
                                Message = $"Đã tồn tại danh mục video có tiêu đề {oDMVideoItem.Title}"
                            };
                            break;
                        }
                        oDMVideoDA.UpdateObject<DMVideoItem>(oDMVideoItem);
                        AddLog("UPDATE", "DMVideo", "UPDATE", oGridRequest.ItemID, $"Cập nhật danh mục video{oDMVideoItem.Title}");
                    }
                    else
                    {
                        oDMVideoItem.UpdateObject(context.Request);
                        if (oDMVideoDA.CheckExit(oGridRequest.ItemID, oDMVideoItem.Title) > 0)
                        {
                            oResult = new ResultAction()
                            {
                                State = ActionState.Error,
                                Message = $"Đã tồn tại danh mục video có tiêu đề {oDMVideoItem.Title}"
                            };
                            break;
                        }
                        oDMVideoDA.UpdateObject<DMVideoItem>(oDMVideoItem);
                        AddLog("UPDATE", "DMVideo", "ADD", oGridRequest.ItemID, $"Thêm mới danh mục video {oDMVideoItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    if (oVideoDA.GetListJson(new VideoQuery() {DanhMuc = oGridRequest.ItemID,_ModerationStatus = 1 }).Count > 0)
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Danh mục chứa video đang hoạt động";
                    }
                    else
                    {
                        var outb = oDMVideoDA.DeleteObjectV2(oGridRequest.ItemID);
                        AddLog("DELETE", "DMVideo", "DELETE", oGridRequest.ItemID, $"Xóa danh mục video {outb.Message}");
                        if (outb.State == ActionState.Succeed)
                            oResult.Message = "Xóa thành công";
                    }
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    AddLog("DELETE-MULTI", "DMVideo", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều danh mục video");
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oDMVideoDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oDMVideoDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "DMVideo", "APPROVED", oGridRequest.ItemID, $"Duyệt danh mục video {oDMVideoDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oDMVideoDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "DMVideo", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt danh mục video {oDMVideoDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDMVideoItem = oDMVideoDA.GetByIdToObjectSelectFields<DMVideoItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oDMVideoItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}