﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_DMVideo.ascx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.DMVideo.UC_DMVideo" %>
<style>
    #tblDMVideo tbody td img {
        width: 200px !important;
    }
</style>
<div role="body-data" data-title="danh mục video" class="content_wp" data-action="/UserControls/AdminCMS/DMVideo/pAction.asp" data-form="/UserControls/AdminCMS/DMVideo/pFormDMVideo.aspx" data-view="/UserControls/AdminCMS/DMVideo/pViewDMVideo.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="DMVideoSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Titles" />
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <button class="btn btn-primary act-add" data-per="010502" type="button">Thêm mới</button>
                    </p>
                </div>

            </div>

            <div class="clsgrid table-responsive">

                <table id="tblDMVideo" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        function formatDate(strValue) {
            if (strValue == null) return "";
            //var date = new Date(strValue);
            var d = new Date(strValue);
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var output = //(('' + hour).length < 2 ? '0' : '') + hour + ':' +
                //(('' + minute).length < 2 ? '0' : '') + minute + " ngày " +
                (('' + day).length < 2 ? '0' : '') + day + '/' +
                (('' + month).length < 2 ? '0' : '') + month + '/' +
                d.getFullYear();
            return output;
        }

        var $tagvalue = $(".content_wp");
        var $tblDanhMucChung = $("#tblDMVideo").viDataTable(
            {
                "frmSearch": "DMVideoSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "mData": "ID",
                            "name": "ID", "sTitle": "ID",
                            "sWidth": "30px",
                        },
                        {
                            "mData": function (o) {
                                if (o.Title != null)
                                    return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                                else
                                    return "";
                            },
                            "name": "Title", "sTitle": "Tên danh mục"
                        }, {
                            "mData": function (o) {
                                if (o.ImageNews != null && o.ImageNews != "")
                                    return ' <img  data-id="' + o.ID + '" class="act-view" style="width:100px;height:90px"  src="' + o.ImageNews + '" alt="ImageNews" />'
                                else
                                    return ''
                            },
                            "name": "ImageNews", "sTitle": "Ảnh"
                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {

                                if (o._ModerationStatus == 0)

                                    return '<a data-per="010505" title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a data-per="010505" title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }
                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" title="Sửa" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        },
                        {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE" title="Xóa"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ]
            });
    });
</script>
