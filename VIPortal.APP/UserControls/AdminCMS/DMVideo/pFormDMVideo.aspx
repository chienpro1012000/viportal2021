﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormDMVideo.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.DMVideo.pFormDMVideo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-DMVideo" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oDMVideo.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên danh mục</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" placeholder="  Nhập tên danh mục video" class="form-control" value="<%=oDMVideo.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <%--<label for="DMSTT" class="col-sm-2 control-label">Số thứ tự</label>
            <div class="col-sm-4">
                <input type="number" name="DMSTT" id="DMSTT" min = "0" oninput="validity.valid||(value='');" placeholder="Nhập số thứ tự" value="<%:(oDMVideo!= null &&  oDMVideo.DMSTT>0)?oDMVideo.DMSTT+"": "" %>" class="form-control" />
            </div>--%>
            <label for="TBNgayDang" class="col-sm-2 control-label">Ngày đăng</label>
            <div class="col-sm-10">
                <input type="text" name="TBNgayDang" id="TBNgayDang" value="<%:string.Format("{0:dd/MM/yyyy}",oDMVideo.TBNgayDang)%>" class="form-control input-datetime" />
            </div>
        </div>
        <div class="form-group row">
            <label for="DMMoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <textarea name="DMMoTa" id="DMMoTa" placeholder="Nhập mô tả" class="form-control"><%:oDMVideo.DMMoTa%></textarea>
            </div>
        </div>


        <div class="form-group row">
            <label for="ImageNews" class="col-sm-2 control-label">Ảnh đại diện</label>
            <div class="col-sm-10" data-role="groupImageDD">

                <input type="hidden" name="ImageNews" id="ImageNews" value="<%:oDMVideo.ImageNews%>" />
                <img title="Ảnh đại diện" id="srcImageNews" src="<%=oDMVideo.ImageNews%>" />
                <button id="btnImageNews" type="button" class="btn  btn-success">Ảnh đại diện</button>
                <button id="btnXoaImageNews" type="button" class="btn btn-danger">Xóa ảnh</button>
            </div>
        </div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {


            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $(".form-group select").select2({
                dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
            });
            $("#btnImageNews").click(function () {
                $elementId = $(this);
                CKFinder.popup({
                    chooseFiles: true,
                    width: 800,
                    height: 600,
                    onInit: function (finder) {
                        finder.on('files:choose', function (evt) {
                            var file = evt.data.files.first();
                            //var output = document.getElementById(elementId);
                            //console.log(file.getUrl());
                            //$("#ImageNews").value(file.getUrl());
                            var urlfileimg = file.getUrl();
                            document.getElementById("ImageNews").value = urlfileimg;
                            document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                        });
                        finder.on('file:choose:resizedImage', function (evt) {
                            //var output = document.getElementById(elementId);
                            console.log(evt.data);
                            var urlfileimg = evt.data.resizedUrl;
                            document.getElementById("ImageNews").value = urlfileimg;
                            document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                        });
                    }
                });
            });
            $("#btnXoaImageNews").click(function () {
                document.getElementById("ImageNews").value = "";
                document.getElementById("srcImageNews").setAttribute("src", "");
            });
            $("#frm-DMVideo").validate({
                ignore: [],
                rules: {
                    Title: "required",                    
                    DMSTT: {
                        numberchar: true, required: true
                    }
                },
                messages: {
                    Title: "Vui lòng nhập tên danh mục video",
                    DMSTT: "Vui lòng nhập số thứ tự",
                    ImageNews: "Vui lòng chọn ảnh đại diện",
                },

                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/DMVideo/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-DMVideo").trigger("click");
                            $(form).closeModal();
                            $('#tblDMVideo').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-DMVideo").viForm();
        });
    </script>
</body>
</html>
