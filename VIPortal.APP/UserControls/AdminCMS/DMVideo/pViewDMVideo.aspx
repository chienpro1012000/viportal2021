﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewDMVideo.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.DMVideo.pViewDMVideo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên danh mục video</label>
            <div class="col-sm-10">
               <%=oDMVideo.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="DMSTT" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-4">
              <%:(oDMVideo!= null &&  oDMVideo.DMSTT>0)?oDMVideo.DMSTT+"": "" %>
            </div>
            <label for="TBNgayDang" class="col-sm-2 control-label">Ngày đăng</label>
            <div class="col-sm-4">
                <div class="">
                    <%:string.Format("{0:dd-MM-yyyy}", oDMVideo.TBNgayDang)%>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="DMMoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
               <%:oDMVideo.DMMoTa%>
            </div>
        </div>
         <div class="form-group row">
            <label for="ImageNews" class="col-sm-2 control-label">Ảnh đại diện</label>
            <div class="col-sm-10" data-role="groupImageDD">
                <img src="<%=oDMVideo.ImageNews%>" />
            </div>
        </div>
        <%--<div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oDMVideo.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oDMVideo.ListFileAttach[i].Url %>"><%=oDMVideo.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>--%>
		  <div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%=string.Format("{0:dd/MM/yyyy}", oDMVideo.Created)%>
            </div>
              <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy}",oDMVideo.Modified)%>
            </div>
          </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
                <%=oDMVideo.Author.LookupValue%>
            </div>
             <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
                <%=oDMVideo.Editor.LookupValue%>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>