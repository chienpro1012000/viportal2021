﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.DMVideo;

namespace VIPortal.APP.UserControls.AdminCMS.DMVideo
{
    public partial class pViewDMVideo :pFormBase
    {
        public DMVideoItem oDMVideo = new DMVideoItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oDMVideo = new DMVideoItem();
            if(ItemID > 0)
            {
                DMVideoDA oDMVideoDA = new DMVideoDA(UrlSite);
                oDMVideo = oDMVideoDA.GetByIdToObject<DMVideoItem>(ItemID);
            }
        }
    }
}