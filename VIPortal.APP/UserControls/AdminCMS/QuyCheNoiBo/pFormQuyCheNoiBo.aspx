﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormQuyCheNoiBo.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.QuyCheNoiBo.pFormQuyCheNoiBo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
    <style>
        .error {
            color: red;
        }
    </style>
</head>
<body>
    <form id="frm-QuyCheNoiBo" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oQuyCheNoiBo.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Quy chế</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" placeholder="Nhập quy chế" class="form-control" value="<%=oQuyCheNoiBo.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <%--<label for="STT" class="col-sm-2 control-label">Số thứ tự</label>
            <div class="col-sm-4">
                <input type="text" name="STT" id="STT" class="form-control" value="<%=oQuyCheNoiBo.STT%>" />
            </div>--%>
            <label for="DMQuyChe" class="col-sm-2 control-label">Danh mục </label>
            <div class="col-sm-4">
                <select data-selected="<%:oQuyCheNoiBo.DMQuyChe.LookupId%>" data-url="/UserControls/AdminCMS/DanhMucQuyChe/pAction.asp?do=QUERYDATA" name="DMQuyChe" id="DMQuyChe" data-place ="Chọn danh mục" class="form-control">
                </select>
            </div>
        </div>
          <div class="form-group row">
            <label for="QCNgayDang" class="col-sm-2 control-label">Ngày đăng</label>
            <div class="col-sm-4">
                <div class="">
                     <input type="text" name="QCNgayDang" id="QCNgayDang" value="<%:string.Format("{0:dd/MM/yyyy}", oQuyCheNoiBo.QCNgayDang)%>" class="form-control input-datetime" />
                </div>
            </div>
                <label for="QCNgayVanBan" class="col-sm-2 control-label">Ngày ban hành</label>
            <div class="col-sm-4">
                 <input type="text" name="QCNgayVanBan" id="QCNgayVanBan" value="<%:string.Format("{0:dd/MM/yyyy}", oQuyCheNoiBo.QCNgayVanBan)%>" class="form-control input-datetime" />
            </div>
       
        </div>  
        <div class="form-group row">
            <label for="QCSoKyHieu" class="col-sm-2 control-label">Số ký hiệu</label>
            <div class="col-sm-4">
                <div class="">
                     <input type="text" name="QCSoKyHieu" id="QCSoKyHieu" <%-- min = "0" oninput="validity.valid||(value='');" --%> placeholder="Nhập số ký hiệu" value="<%:oQuyCheNoiBo.QCSoKyHieu%>" class="form-control"  />
                </div>
            </div>
                <label for="QCHieuLuc" class="col-sm-2 control-label">Hiệu lực</label>
            <div class="col-sm-4">
                <select class="form-control"  id="QCHieuLuc" name="QCHieuLuc">
                    <option value="1"  <%:oQuyCheNoiBo.QCHieuLuc == 1 ? "selected":"" %>>Có hiệu lực</option>
                    <option value="2" <%:oQuyCheNoiBo.QCHieuLuc == 2 ? "selected":"" %>>Hết hiệu lực</option>
                </select>
            </div>
       
        </div>  
       
        <div class="form-group row">
            <label for="QCTrichYeu" class="col-sm-2 control-label">Trích yếu</label>
            <div class="col-sm-10">
                <textarea name="QCTrichYeu" id="QCTrichYeu" placeholder="Nhập trích yếu" class="form-control"><%:oQuyCheNoiBo.QCTrichYeu%></textarea>
            </div>
        </div>
          <div class="form-group row">
            <label for="QCToanVan" class="col-sm-2 control-label">Toàn văn</label>
            <div class="col-sm-10">
                 <textarea name="QCToanVan" id="QCToanVan" placeholder="Nhập toàn văn" class="form-control"><%=oQuyCheNoiBo.QCToanVan%></textarea>
            </div>
        </div>
          <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-10">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>

    </form>
    
    <script type="text/javascript">

        $(document).ready(function () {
            $("#DMQuyChe").smSelect2018V2({
                dropdownParent: "#frm-QuyCheNoiBo"
            });
            CKEDITOR.replace("QCToanVan", {
                height: 300
            });
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oQuyCheNoiBo.ListFileAttach)%>'
            });
            $("#frm-QuyCheNoiBo").validate({
                rules: {
                    Title: {
                        required: true,
                        maxlength: 254
                    },
                    QCSoKyHieu: "required",
                    QCNgayVanBan: "required",
                    QCTrichYeu: "required",
                    DMQuyChe: "required",
                },
                messages: {
                    Title: {
                        required: "Vui lòng nhập tiêu đề quy chế"
                    },
                    QCSoKyHieu: "Vui lòng nhập số ký hiệu",
                    QCNgayVanBan: "Vui lòng chọn ngày ban hành",
                    QCTrichYeu: "Vui lòng nhập trích yếu",
                    DMQuyChe: "Vui lòng chọn danh mục",
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/QuyCheNoiBo/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-QuyCheNoiBo").trigger("click");
                            $(form).closeModal();
                            $('#tblQuyCheNoiBo').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-QuyCheNoiBo").viForm();
        });
    </script>
</body>
</html>

