﻿<%@ page language="C#" autoeventwireup="true" codebehind="pViewQuyCheNoiBo.aspx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.QuyCheNoiBo.pViewQuyCheNoiBo" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <%=oQuyCheNoiBo.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="QCNgayDang" class="col-sm-2 control-label">QC Ngày đăng</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy}", oQuyCheNoiBo.QCNgayDang)%>
            </div>
            <label for="QCNgayVanBan" class="col-sm-2 control-label">QC Ngày ban hành</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy}", oQuyCheNoiBo.QCNgayVanBan)%>
            </div> 
        </div>
        <div class="form-group row">
            <label for="QCTrichYeu" class="col-sm-2 control-label">QC Trích yếu</label>
            <div class="col-sm-10">
                <%=oQuyCheNoiBo.QCTrichYeu%>
            </div>
        </div>
        <div class="form-group row">
            <label for="QCToanVan" class="col-sm-2 control-label">QC Toàn văn</label>
            <div class="col-sm-10">
                <%=oQuyCheNoiBo.QCToanVan%>
            </div>
        </div>
        <div class="form-group row">
            <label for="QCSoKyHieu" class="col-sm-2 control-label">QC Số ký hiệu</label>
            <div class="col-sm-4">
                <%=oQuyCheNoiBo.QCSoKyHieu%>
            </div>
            <label for="QCHieuLuc" class="col-sm-2 control-label">QC Hiệu lực</label>
            <div class="col-sm-4">
                <%=oQuyCheNoiBo.QCHieuLuc%>
            </div>
        </div>
        <div class="form-group row">
            <label for="DMQuyChe" class="col-sm-2 control-label">DM Quy chế</label>
            <div class="col-sm-10">
                <%=oQuyCheNoiBo.DMQuyChe.LookupValue%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oQuyCheNoiBo.ListFileAttach.Count;i++){ %>
                <div><a href="<%=oQuyCheNoiBo.ListFileAttach[i].Url %>"><%=oQuyCheNoiBo.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy}", oQuyCheNoiBo.Created)%>
            </div>
            <label class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy}",oQuyCheNoiBo.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
                <%=oQuyCheNoiBo.Author.LookupValue%>
            </div>
            <label class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
                <%=oQuyCheNoiBo.Editor.LookupValue%>
            </div>
        </div>
        
        <div class="clearfix"></div>
    </form>
</body>
</html>
