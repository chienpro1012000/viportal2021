﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData.QuyCheNoiBo;

namespace VIPortal.APP.UserControls.AdminCMS.QuyCheNoiBo
{

    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {

        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);
            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            QuyCheNoiBoDA oQuyCheNoiBoDA = new QuyCheNoiBoDA(UrlSite,true);
            QuyCheNoiBoItem oQuyCheNoiBoItem = new QuyCheNoiBoItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oQuyCheNoiBoDA.GetListJsonSolr(new QuyCheNoiBoQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oQuyCheNoiBoDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "QuyCheNoiBo", "QUERYDATA", 0, "Xem danh sách quy chế nội bộ");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataQuyCheNoiBo = oQuyCheNoiBoDA.GetListJson(new QuyCheNoiBoQuery(context.Request));
                    treeViewItems = oDataQuyCheNoiBo.Select(x => new TreeViewItem()
                    {
                        key = x.QCSoKyHieu,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;

                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oQuyCheNoiBoDA.GetListJson(new QuyCheNoiBoQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oQuyCheNoiBoItem = oQuyCheNoiBoDA.GetByIdToObject<QuyCheNoiBoItem>(oGridRequest.ItemID);
                        oQuyCheNoiBoItem.UpdateObject(context.Request);
                        if (oQuyCheNoiBoDA.CheckExit(oGridRequest.ItemID, oQuyCheNoiBoItem.Title) == 0)
                        {
                            oQuyCheNoiBoDA.UpdateObject<QuyCheNoiBoItem>(oQuyCheNoiBoItem);
                            AddLog("UPDATE", "QuyCheNoiBo", "UPDATE", oGridRequest.ItemID, $"Cập nhật quy chế nội bộ{oQuyCheNoiBoItem.Title}");
                            oResult.Message = "Cập nhật thành công";
                        }
                        else
                        {
                            oResult = new ResultAction() { State = ActionState.Error, Message = "Tên quy chế nội bộ đã tồn tại" };
                        }
                    }
                    else
                    {
                        oQuyCheNoiBoItem.UpdateObject(context.Request);
                        if (oQuyCheNoiBoDA.CheckExit(oGridRequest.ItemID, oQuyCheNoiBoItem.Title) == 0)
                        {
                            oQuyCheNoiBoDA.UpdateObject<QuyCheNoiBoItem>(oQuyCheNoiBoItem);
                            AddLog("UPDATE", "QuyCheNoiBo", "ADD", oGridRequest.ItemID, $"Thêm mới quy chế nội bộ{oQuyCheNoiBoItem.Title}");
                            oResult.Message = "Thêm mới thành công";
                        }
                        else
                        {
                            oResult = new ResultAction() { State = ActionState.Error, Message = "Tên quy chế nội bộ đã tồn tại" };
                        }
                    }
                    break;
                case "DELETE":
                    var outb = oQuyCheNoiBoDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "QuyCheNoiBo", "DELETE", oGridRequest.ItemID, $"Xóa quy chế nội bộ{outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oQuyCheNoiBoDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    AddLog("DELETE-MULTI", "QuyCheNoiBo", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều quy chế nội bộ");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oQuyCheNoiBoDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "QuyCheNoiBo", "APPROVED", oGridRequest.ItemID, $"Duyệt quy chế nội bộ{oQuyCheNoiBoDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oQuyCheNoiBoDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "QuyCheNoiBo", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt quy chế nội bộ{oQuyCheNoiBoDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oQuyCheNoiBoItem = oQuyCheNoiBoDA.GetByIdToObjectSelectFields<QuyCheNoiBoItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oQuyCheNoiBoItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}