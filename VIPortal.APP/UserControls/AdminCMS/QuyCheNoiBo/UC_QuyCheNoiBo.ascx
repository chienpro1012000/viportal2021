﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_QuyCheNoiBo.ascx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.QuyCheNoiBo.UC_QuyCheNoiBo" %>
<div role="body-data" data-title="quy chế nội bộ" class="content_wp" data-action="/UserControls/AdminCMS/QuyCheNoiBo/pAction.asp" data-form="/UserControls/AdminCMS/QuyCheNoiBo/pFormQuyCheNoiBo.aspx" data-view="/UserControls/AdminCMS/QuyCheNoiBo/pViewQuyCheNoiBo.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="QuyCheNoiBoSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <input type="text" class="form-control" id="QCSoKyHieu" name="QCSoKyHieu" placeholder="Số ký hiệu" style="padding:0px 10px" />
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Titles,QCSoKyHieu" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                        </div>
                        
                        <div class="form-group">
                             <select data-selected="" data-url="/UserControls/AdminCMS/DanhMucQuyChe/pAction.asp?do=QUERYDATA" name="QC_DMQuyChe" id="DMQuyCheSearch" data-place ="Chọn danh mục" class="form-control">
                </select>
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <button class="btn btn-primary act-add" data-per="050102" type="button">Thêm mới</button>
                    </p>
                </div>

            </div>

            <div class="clsgrid table-responsive">

                <table id="tblQuyCheNoiBo" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    

    $(document).ready(function () {

        //DMQuyChe
        $("#DMQuyCheSearch").smSelect2018V2({
            dropdownParent: ".content_wp"
        });
        var $tagvalue = $(".content_wp");
        var $tblQuyCheNoiBo = $("#tblQuyCheNoiBo").viDataTable(
            {
                "frmSearch": "QuyCheNoiBoSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [{
                        "sWidth": "20px",
                        "mData": function (o) {
                            return '';
                        },
                        "name": "ID", "sTitle": "STT", className: "txtcenter"
                    },
                    {
                        "mData": "ID",
                        "name": "ID", "visible": false, "sTitle": "ID",
                        "sWidth": "30px",
                    }, {
                        "sWidth": "70px",
                        "mData": function (o) {
                            return '<a data-id="' + o.ID + '">' + o.QCSoKyHieu + '</a>';
                        },
                        "name": "QCSoKyHieu", "sTitle": "Số ký hiệu"
                    },
                    {
                        "mData": function (o) {
                            if (o.Title != null)
                                return '<a data-ItemID="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                            else
                                return "";
                        },
                        "name": "Title", "sTitle": "Tên quy chế"
                    }, {
                        "mData": function (o) {
                            return o.DMQuyChe.Title;
                        },
                        "name": "DMQuyChe", "sTitle": "Danh mục quy chế"
                    },
                    {
                        "name": "QCNgayVanBan", "sTitle": "Ngày ban hành",
                        "mData": function (o) {
                            return formatDate(o.QCNgayVanBan);
                        },
                        "sWidth": "100px",
                    }, {
                        "name": "QCNgayDang", "sTitle": "Ngày đăng",
                        "mData": function (o) {
                            return formatDate(o.QCNgayDang);
                        },
                        "sWidth": "80px",
                    },
                    {
                        "sWidth": "30px",
                        "mData": function (o) {
                            if (o.QCHieuLuc == 1) {
                                return '<a data-id="' + o.ID + '">' + "Có hiệu lực" + '</a>';
                            } else return '<a data-id="' + o.ID + '">' + "Hết hiệu lực" + '</a>';

                        },
                        "name": "QCHieuLuc", "sTitle": "Hiệu lực",
                        "sWidth": "80px",
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "className": "all",
                        "sWidth": "18px",
                        "mRender": function (o) {
                            if (o._ModerationStatus == 0)

                                return '<a title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                            else
                                return '<a title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "sWidth": "18px",
                        "className": "all",
                        "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" title="Sửa" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                    }, {
                        "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                        "mData": null,
                        "bSortable": false,
                        "className": "all",
                        "sWidth": "18px",
                        "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE" title="Xóa"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                    }, {
                        "sTitle": '<input class="checked-all" type="checkbox" />',
                        "mData": null,
                        "bSortable": false,
                        "sWidth": "18px",
                        "className": "all",
                        "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                    }
                    ],
                "aaSorting": [0, 'desc']
            });
        $tblQuyCheNoiBo.on('order.dt search.dt', function () {
            $tblQuyCheNoiBo.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        });
        $tblQuyCheNoiBo.on('draw', function () {
            
            $("a[data-itemid='<%=ItemID%>']").parent().parent().addClass("clsdetail");
        });
    });
</script>
