﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.QuyCheNoiBo;

namespace VIPortal.APP.UserControls.AdminCMS.QuyCheNoiBo
{
    public partial class pFormQuyCheNoiBo : pFormBase
    {
        public QuyCheNoiBoItem oQuyCheNoiBo { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oQuyCheNoiBo = new QuyCheNoiBoItem();
            QuyCheNoiBoDA oQuyCheNoiBoDA = new QuyCheNoiBoDA(UrlSite);
            if (ItemID > 0)
            {
                
                oQuyCheNoiBo = oQuyCheNoiBoDA.GetByIdToObject<QuyCheNoiBoItem>(ItemID);
            }
            else
            {
                oQuyCheNoiBo.STT = oQuyCheNoiBoDA.GetLastSTT("STT");
            }
        }
    }
}