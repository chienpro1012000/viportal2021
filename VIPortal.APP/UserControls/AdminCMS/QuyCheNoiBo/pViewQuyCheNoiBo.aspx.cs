﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.QuyCheNoiBo;

namespace VIPortal.APP.UserControls.AdminCMS.QuyCheNoiBo
{
    public partial class pViewQuyCheNoiBo : pFormBase
    {
        public QuyCheNoiBoItem oQuyCheNoiBo = new QuyCheNoiBoItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oQuyCheNoiBo = new QuyCheNoiBoItem();
            if (ItemID > 0)
            {
                QuyCheNoiBoDA OQuyCheNoiBoDA = new QuyCheNoiBoDA(UrlSite );
                oQuyCheNoiBo = OQuyCheNoiBoDA.GetByIdToObject<QuyCheNoiBoItem>(ItemID);

            }
        }
    }
}