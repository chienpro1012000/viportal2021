<%@ page language="C#" autoeventwireup="true" codebehind="pFormMenuQuanTri.aspx.cs" inherits="VIPortalAPP.pFormMenuQuanTri" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-MenuQuanTri" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oMenuQuanTri.ID %>" />
        <input type="hidden" name="UrlSiteFix" value="<%=UrlSite %>" id="UrlSiteFix" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="PhanLoaiMenu" class="col-sm-2 control-label">Loại menu</label>
            <div class="col-sm-10">
                <select id="PhanLoaiMenu" name="PhanLoaiMenu" class="form-control">
                    <option value="0">Menu quản trị</option>
                    <option value="1">Menu khai thác</option>
                    <option value="2">Danh mục báo cáo</option>
                    <option value="3">Liên kết</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oMenuQuanTri.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="MenuLink" class="col-sm-2 control-label">MenuLink</label>
            <div class="col-sm-10">
                <input type="text" name="MenuLink" id="MenuLink" placeholder="Nhập menulink" value="<%:oMenuQuanTri.MenuLink%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="MenuNewWindow" class="col-sm-2 control-label">Mở cửa sổ mới</label>
            <div class="col-sm-4">
                <input value="true" type="checkbox" name="MenuNewWindow" id="MenuNewWindow" <%=oMenuQuanTri.MenuNewWindow? "checked" : string.Empty%> />
            </div>
            <label for="MenuParent" class="col-sm-2 control-label">Menu cha</label>
            <div class="col-sm-4">
                <select data-selected="<%:oMenuQuanTri.MenuParent.LookupId%>" data-url="/UserControls/AdminCMS/MenuQuanTri/pAction.asp?do=QUERYDATA&UrlSiteFix=<%=UrlSite %>&PhanLoaiMenu=<%=oMenuQuanTri.PhanLoaiMenu %>" data-place="Chọn MenuParent" name="MenuParent" id="MenuParent" class="form-control"></select>
            </div>
            <label for="MenuIndex" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-4">
                <input type="text" name="MenuIndex" id="MenuIndex" placeholder="Nhập STT" value="<%:oMenuQuanTri.MenuIndex%>" class="form-control" />
            </div>
        </div>
        <!--MenuShowSubPortal-->
        <div class="form-group row">
            <label for="MenuShow" class="col-sm-2 control-label">Sử dụng cổng thành phần</label>
            <div class="col-sm-4">
                <input value="true" type="checkbox" name="MenuShowSubPortal" id="MenuShowSubPortal" <%=oMenuQuanTri.MenuShowSubPortal? "checked" : string.Empty%> />
            </div>
            <label for="MenuShow" class="col-sm-2 control-label">Sử dụng Cổng TCY</label>
            <div class="col-sm-4">
                <input value="true" type="checkbox" name="MenuShowPortal" id="MenuShowPortal" <%=oMenuQuanTri.MenuShowPortal? "checked" : string.Empty%> />
            </div>
        </div>
        <div class="form-group row">
            <label for="MenuShow" class="col-sm-2 control-label">Hiển thị</label>
            <div class="col-sm-4">
                <input value="true" type="checkbox" name="MenuShow" id="MenuShow" <%=oMenuQuanTri.MenuShow? "checked" : string.Empty%> />
            </div>
            <label for="CheckLogin" class="col-sm-2 control-label">Yêu cầu đăng nhập</label>
            <div class="col-sm-4">
                <input value="true" type="checkbox" name="CheckLogin" id="CheckLogin" <%=oMenuQuanTri.CheckLogin? "checked" : string.Empty%> />
            </div>
        </div>
        <div class="form-group row">
            <label for="MenuIcon" class="col-sm-2 control-label">Icon</label>
            <div class="col-sm-10">
                <div class="input-group mb-3">
                    <input type="text" name="MenuIcon" id="MenuIcon" placeholder="Nhập menuicon" value="<%:oMenuQuanTri.MenuIcon%>" class="form-control" />
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" data-do="CHONNGUOI" title="Chọn ảnh từ thư viện" id="btnChoiceImage" type="button">Chọn ảnh</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="Permissions" class="col-sm-2 control-label">Quyền truy cập</label>
            <div class="col-sm-10">
                <input type="hidden" name="Permissions" id="Permissions" value="" />
                <div id="treephanquyen" data-source="ajax" class="sampletree" style="max-height: 400px; overflow-y: auto;">
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#btnChoiceImage").click(function () {
                $elementId = $(this);
                CKFinder.popup({
                    chooseFiles: true,
                    width: 800,
                    height: 600,
                    resourceType: 'Images',
                    onInit: function (finder) {
                        finder.on('files:choose', function (evt) {
                            var file = evt.data.files.first();
                            //var output = document.getElementById(elementId);
                            //console.log(file.getUrl());
                            //$("#ImageNews").value(file.getUrl());
                            var urlfileimg = file.getUrl();
                            document.getElementById("MenuIcon").value = urlfileimg;
                        });
                        finder.on('file:choose:resizedImage', function (evt) {
                            //var output = document.getElementById(elementId);
                            console.log(evt.data);
                            var urlfileimg = evt.data.resizedUrl;
                            document.getElementById("MenuIcon").value = urlfileimg;
                        });
                    }
                });
            });
            $('#PhanLoaiMenu option[value="<%=oMenuQuanTri.PhanLoaiMenu%>"]').attr('selected', 'selected');
            var $treePermis = $("#treephanquyen").fancytree({
                source: {
                    url: "/VIPortalAPI/api/LPermissions/QUERYTREE?ListPermissCheck=<%=string.Join(",", oMenuQuanTri.Permissions.Select(x => x.LookupId)) %>"
                },
                checkbox: true,
                selectMode: 2,
                icon: false,
                beforeSelect: function (event, data) {
                },
                select: function (event, data) {

                },
            });
            $(".fancytree-container").addClass("fancytree-connectors");
            $("#MenuParent").smSelect2018V2({
                dropdownParent: "#frm-MenuQuanTri"
            });
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oMenuQuanTri.ListFileAttach)%>'
            });
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $(".form-group row select").select2({
                dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
            });

            $("#frm-MenuQuanTri").validate({
                rules: {
                    Title: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    var tree = $('#treephanquyen').fancytree('getTree');
                    var selKeys = $.map(tree.getSelectedNodes(), function (element, index) { return element.key }).join(",");
                    $("#Permissions").val(selKeys);
                    //NProgress.start();
                    $.post("/UserControls/AdminCMS/MenuQuanTri/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-MenuQuanTri").trigger("click");
                            $(form).closeModal();
                            $('#tblMenuQuanTri').dataTable().fnDraw(false);
                        }
                    }).always(function () {
                        //NProgress.done(); 
                    });
                }
            });
        });
    </script>
</body>
</html>
