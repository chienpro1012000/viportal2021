using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public partial class pViewMenuQuanTri : pFormBase
    {
        public MenuQuanTriItem oMenuQuanTri = new MenuQuanTriItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oMenuQuanTri = new MenuQuanTriItem();
            if (ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                oMenuQuanTri = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(ItemID);

            }
        }
    }
}