<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewMenuQuanTri.aspx.cs" Inherits="VIPortalAPP.pViewMenuQuanTri" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group row">
            <label for="MenuShow" class="col-sm-2 control-label">Loại menu</label>
            <div class="col-sm-10">
               <%=oMenuQuanTri.MenuPublic? "Menu khai thác" : "Menu quản trị"%>
            </div>
        </div>
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
               <%=oMenuQuanTri.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="MenuLink" class="col-sm-2 control-label">MenuLink</label>
            <div class="col-sm-10">
                <%:oMenuQuanTri.MenuLink%>
            </div>
        </div>
        <div class="form-group row">
            <label for="MenuNewWindow" class="col-sm-2 control-label">MenuNewWindow</label>
            <div class="col-sm-10">
                <%=oMenuQuanTri.MenuNewWindow? "checked" : string.Empty%>
            </div>
        </div>
        <div class="form-group row">
            <label for="MenuIndex" class="col-sm-2 control-label">MenuIndex</label>
            <div class="col-sm-10">
                <%:oMenuQuanTri.MenuIndex%>
            </div>
        </div>
        <div class="form-group row">
            <label for="MenuShow" class="col-sm-2 control-label">MenuShow</label>
            <div class="col-sm-10">
                <%=oMenuQuanTri.MenuShow? "checked" : string.Empty%>
            </div>
        </div>
        <div class="form-group row">
            <label for="CheckLogin" class="col-sm-2 control-label">CheckLogin</label>
            <div class="col-sm-10">
                <%=oMenuQuanTri.CheckLogin? "checked" : string.Empty%>
            </div>
        </div>
        <div class="form-group row">
            <label for="MenuParent" class="col-sm-2 control-label">MenuParent</label>
            <div class="col-sm-10">
                <%:oMenuQuanTri.MenuParent.LookupId%>
            </div>
        </div>
        <div class="form-group row">
            <label for="MenuIcon" class="col-sm-2 control-label">MenuIcon</label>
            <div class="col-sm-10">
               <%:oMenuQuanTri.MenuIcon%>
            </div>
        </div>
        <div class="form-group row">
            <label for="Permissions" class="col-sm-2 control-label">Permissions</label>
            <div class="col-sm-10">
            </div>
        </div

		  <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oMenuQuanTri.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oMenuQuanTri.ListFileAttach[i].Url %>"><%=oMenuQuanTri.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>      
		<div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%=string.Format("{0:dd-MM-yyyy}", oMenuQuanTri.Created)%>
            </div>
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}",oMenuQuanTri.Modified)%>
            </div>
        </div>

        <div class="form-group row">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
               <%=oMenuQuanTri.Author.LookupValue%>
            </div>
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
               <%=oMenuQuanTri.Editor.LookupValue%>
            </div>    
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>