using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using VIPORTAL;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace VIPortalAPP

{
    /// <summary> 
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
     
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);
            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA(UrlSite);
            MenuQuanTriItem oMenuQuanTriItem = new MenuQuanTriItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    var oData = oMenuQuanTriDA.GetListJson(new MenuQuanTriQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw, 
                        oMenuQuanTriDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "MenuQuanTri", "QUERYDATA", 0, "Xem danh sách menu quản trị");
                    break;
                
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oMenuQuanTriDA.GetListJson(new MenuQuanTriQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "GETMENULEFTCMS":
                    oHtmlMenu = new StringBuilder();
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJsonMenu = oMenuQuanTriDA.GetListJson(new MenuQuanTriQuery(context.Request));
                    BuildMenuDaCap(oDataJsonMenu, 0);
                    oResult.OData = oHtmlMenu.ToString();
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oGridRequest.ItemID);
                        oMenuQuanTriItem.UpdateObject(context.Request);
                        oMenuQuanTriDA.UpdateObject<MenuQuanTriItem>(oMenuQuanTriItem);
                        AddLog("UPDATE", "MenuQuanTri", "UPDATE", oGridRequest.ItemID, $"Cập nhật menu quản trị{oMenuQuanTriItem.Title}");
                    }
                    else
                    {
                        oMenuQuanTriItem.UpdateObject(context.Request);
                        oMenuQuanTriDA.UpdateObject<MenuQuanTriItem>(oMenuQuanTriItem);
                        AddLog("UPDATE", "MenuQuanTri", "ADD", oGridRequest.ItemID, $"Thêm mới menu quản trị{oMenuQuanTriItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oMenuQuanTriDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "MenuQuanTri", "DELETE", oGridRequest.ItemID, $"Xóa menu quản trị{outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    break;
                case "DELETE-MULTI":
                    AddLog("DELETE-MULTI", "MenuQuanTri", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều menu quản trị");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oMenuQuanTriDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "MenuQuanTri", "APPROVED", oGridRequest.ItemID, $"Duyệt menu quản trị{oMenuQuanTriDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                     oMenuQuanTriDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "MenuQuanTri", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt menu quản trị{oMenuQuanTriDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObjectSelectFields<MenuQuanTriItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oMenuQuanTriItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }
        private StringBuilder oHtmlMenu;
        private void BuildMenuDaCap(List<MenuQuanTriJson> lstMenuQuanTriJson, int rootId = 0)
        {
            IEnumerable<MenuQuanTriJson> tempMenu = lstMenuQuanTriJson.Where(x=>x.MenuParent.ID == rootId);
            foreach (MenuQuanTriJson oMenuQuanTriJson in tempMenu)
            {
                if(oMenuQuanTriJson.MenuParent.ID == 0)
                {
                    //<li class="nav-header">EXAMPLES</li>
                    oHtmlMenu.AppendFormat("<li class=\"nav-header\">{0}</li>", oMenuQuanTriJson.Title);
                }
                else if(string.IsNullOrEmpty(oMenuQuanTriJson.MenuLink))
                {
                    oHtmlMenu.AppendFormat("<li class=\"nav-item\"><a href=\"#\" class=\"nav-link\"><i class=\"nav-icon fas fa-copy\"></i><p>{0}<i class=\"fas fa-angle-left right\"></i><span class=\"badge badge-info right\">6</span></p></a>", oMenuQuanTriJson.Title);
                    //check neu có menu con thì gọi tiếp đệ quy ở đây rồi đóng form.
                    if(lstMenuQuanTriJson.Count(x => x.MenuParent.ID == oMenuQuanTriJson.ID) > 0)
                    {
                        oHtmlMenu.Append("<ul class=\"nav nav-treeview\">");
                        BuildMenuDaCap(lstMenuQuanTriJson, oMenuQuanTriJson.ID);
                        oHtmlMenu.Append("</ul>");
                    }
                    oHtmlMenu.Append("</li>");
                }
                else
                {
                    oHtmlMenu.AppendFormat("<li class=\"nav-item\"><a href=\"{0}\" class=\"nav-link\"><i class=\"far fa-circle nav-icon\"></i><p>{1}</p></a></li>", oMenuQuanTriJson.MenuLink, oMenuQuanTriJson.Title);
                }
            }
        }
        
    }
}