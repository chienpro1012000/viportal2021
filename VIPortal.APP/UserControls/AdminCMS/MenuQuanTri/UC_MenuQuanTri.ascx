<%@ control language="C#" autoeventwireup="true" codebehind="UC_MenuQuanTri.aspx.cs" inherits="VIPortalAPP.UC_MenuQuanTri" %>
<div role="body-data" data-title="Menu quản trị" class="content_wp" data-action="/UserControls/AdminCMS/MenuQuanTri/pAction.asp" data-form="/UserControls/AdminCMS/MenuQuanTri/pFormMenuQuanTri.aspx" data-view="/UserControls/AdminCMS/MenuQuanTri/pViewMenuQuanTri.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="MenuQuanTriSearch" class="form-inline zonesearch">
                        <div class="form-group mb-2">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                        </div>
                        <div class="form-group mx-sm-3 mb-2">
                            <select class="form-control" aria-label="Default select example" name="PhanLoaiMenu" id="PhanLoaiMenu">
                                <option selected value="0">Menu quản trị</option>
                                <option value="1">Menu khai thác</option>
                                <option value="2">Danh mục báo cáo</option>
                                <option value="3">Liên kết website</option>
                            </select>
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title" /> 
                        </div>
                        <button type="button" class="btn btn-default act-search mb-2">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <button class="btn btn-primary act-add" id="btnAddMenu" data-PhanLoaiMenu="0" data-per="080302" type="button">Thêm mới</button>
                    </p>
                </div>
            </div>
            <div class="clsgrid table-responsive">

                <table id="tblMenuQuanTri" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#PhanLoaiMenu").change(function () {
            $("#btnAddMenu").attr("data-PhanLoaiMenu", $(this).val());
        });
        var $tagvalue = $(".content_wp");
        var $tblDanhMucChung = $("#tblMenuQuanTri").viDataTable(
            {
                "frmSearch": "MenuQuanTriSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "mData": function (d) {
                                return d.MenuIndex;
                            },
                            "sWidth": "55px",
                            "name": "MenuIndex", "sTitle": "STT"
                        },{
                            "mData": function (d) {
                                return d.MenuIcon;
                            },
                            "sWidth": "35px",
                            "name": "MenuIcon", "sTitle": "Icon"
                        }, {
                            "mData": function (o) {
                                return '<a data-ItemID="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                            },
                            "name": "Title", "sTitle": "Tiêu đề",
                            "sWidth": "235px",
                        },
                        {
                            "mData": function (d) {
                                return d.MenuLink;
                            },
                            "name": "MenuLink", "sTitle": "Link Quản trị"
                        }, {
                            "mData": function (d) {
                                return d.Permissions.map(function (st) { return st.Title });
                            },
                            "name": "Permissions", "sTitle": "Quyền"
                        }, {
                            "mData": function (d) {
                                if (d.MenuShowSubPortal) return "Có";
                                else return "Không";
                            },
                            "sWidth": "55px",
                            "sTitle": "Sub Portal"
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {
                                if (o.PhanLoaiMenu == 2)
                                    return '<a title="Cấu hình tích hợp báo cáo" url="/UserControls/AdminCMS/Lconfig/pFormCauHinhBC.aspx" class="btn default btn-xs purple btnfrm" data-do="CONFIGBC" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-cogs"></i></a>'
                                else
                                    return '';
                            }
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {
                                if (o._ModerationStatus == 0)

                                    return '<a title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" title="Sửa" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        }, {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                "aaSorting": [0, 'asc']
            });
    });
</script>
