﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DanhMucTinThong.ascx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.News.DanhMucTinThong" %>

<div role="body-data" data-title="" class="content_wp" data-action="/UserControls/AdminCMS/LPermission/pAction.ashx" data-form="/UserControls/AdminCMS/LPermission/pFormLPermission.aspx">
    <div class="clsmanager row">
        <div class="col-sm-9">
            <div id="LPermissionSearch" class="form-inline zonesearch">
                <div class="form-group">
                    <input type="hidden" name="do" id="do" value="QUERYDATA" />
                    <label for="Keyword">Từ khóa</label>
                    <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                    <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title" />
                </div>
                <button  type="button" class="btn btn-default act-search">Tìm kiếm</button>
            </div>
        </div>
        <div class="col-sm-3">
            <p class="text-right">
                <button class="btn btn-primary act-add" type="button">Thêm mới</button>
            </p>
        </div>
    </div>

    <div class="clsgrid table-responsive">
        <div id="treeDanhMuc"></div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#treeDanhMuc').fancytree({
            //  extensions: ['contextMenu'],
            source: {
                url: "/UserControls/AdminCMS/DanhMucThongTin/pAction.ashx?do=MENUTREE"
            },
            renderNode: function (event, data) {
                //console.log(data);
                var node = data.node;
                $(node.span).attr('data-id', data.node.key);
            },
            activate: function (event, data) {
                // A node was activated: display its title:
                var node = data.node;
                console.log(node);
                var ListName = node.key;
                window.location.replace("/cms/Pages/thongtin.aspx?ListName=" + ListName);

            }
        });
        $(".fancytree-container").addClass("fancytree-connectors");

        ///cms/Pages/thongtin.aspx
    });
</script>
