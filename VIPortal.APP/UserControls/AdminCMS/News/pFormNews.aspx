<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormNews.aspx.cs" Inherits="VIPortalAPP.pFormNews" %>


    <form id="frm-News" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oNews.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oNews.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="DescriptionNews" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <textarea name="DescriptionNews" id="DescriptionNews" class="form-control"><%:oNews.DescriptionNews%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="CreatedDate" class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
                <input type="date" name="CreatedDate" id="CreatedDate" value="<%:string.Format("{0:yyyy-MM-dd}",oNews.CreatedDate)%>" class="form-control" />
            </div>
            <label for="TacGia" class="col-sm-2 control-label">Tác giả</label>
            <div class="col-sm-4">
                <input type="text" name="TacGia" id="TacGia" placeholder="Nhập tác giả" value="<%:oNews.TacGia%>" class="form-control" />
            </div>
        </div>

         <div class="form-group row">
            <label for="ImageNews" class="col-sm-2 control-label">Ảnh đại diện</label>
            <div class="col-sm-10" data-role="groupImageDD">

                <input type="hidden" name="ImageNews" id="ImageNews" value="<%:oNews.ImageNews%>" />
                <img title="Ảnh đại diện" id="srcImageNews" src="<%=oNews.ImageNews%>" />
                <button id="btnImageNews" type="button" class="btn  btn-success">Ảnh đại diện</button>
                    <button id="btnXoaImageNews" type="button" class="btn btn-danger">Xóa ảnh</button>
            </div>
        </div>
        <div class="form-group row">
            <label for="SourceNews" class="col-sm-2 control-label">Nội dung</label>
            <div class="col-sm-10">
                <textarea name="ContentNews" id="ContentNews" class="form-control"><%=oNews.ContentNews%></textarea>
            </div>
        </div>
       
       <%-- <div class="form-group row">
            <label for="isHotNew" class="col-sm-2 control-label">isHotNew</label>
            <div class="col-sm-10">
                <input value="true" type="checkbox" name="isHotNew" id="isHotNew" <%=oNews.isHotNew? "checked" : string.Empty%> />
            </div>
        </div>--%>

        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
    </form>



<script type="text/javascript">
    $(document).ready(function () {
        $("#btnImageNews").click(function () {
            $elementId = $(this);
            CKFinder.popup({
                chooseFiles: true,
                width: 800,
                height: 600,
                onInit: function (finder) {
                    finder.on('files:choose', function (evt) {
                        var file = evt.data.files.first();
                        //var output = document.getElementById(elementId);
                        //console.log(file.getUrl());
                        //$("#ImageNews").value(file.getUrl());
                        var urlfileimg = file.getUrl();
                        document.getElementById("ImageNews").value = urlfileimg;
                        document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                    });
                    finder.on('file:choose:resizedImage', function (evt) {
                        //var output = document.getElementById(elementId);
                        console.log(evt.data);
                        var urlfileimg = evt.data.resizedUrl;
                        document.getElementById("ImageNews").value = urlfileimg;
                        document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                    });
                }
            });
        });
        CKEDITOR.replace("ContentNews", {
            height: 300
        });
        $("#FileAttach").regFileUpload({
            files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oNews.ListFileAttach)%>'
            });
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                timePicker: true,
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY HH:mm' }
            });
            $(".form-group select").select2({
                dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
            });

            $("#frm-News").validate({
                rules: {
                    Title: "required",
                    CreatedDate: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/News/pAction.ashx", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-News").trigger("click");
                            $(form).closeModal();
                            $('#tblNews').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
        $("#frm-News").viForm();
        });
</script>