using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public partial class pFormNews : pFormBase
    {
        public NewsItem oNews {get;set;}
        protected void Page_Load(object sender, EventArgs e)
        {
			oNews = new NewsItem();
            if (ItemID > 0)
            {
                NewsDA oNewsDA = new NewsDA();
                oNews = oNewsDA.GetByIdToObject<NewsItem>(ItemID);
            }
        }
    }
}