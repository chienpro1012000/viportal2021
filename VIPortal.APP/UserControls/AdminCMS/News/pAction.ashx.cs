using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;

namespace VIPortalAPP.News
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {

        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            NewsDA oNewsDA = new NewsDA();
            NewsItem oNewsItem = new NewsItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oNewsDA.GetListJson(new NewsQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw, 
                        oNewsDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "News", "QUERYDATA", 0, "Xem danh sách thông tin");
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oNewsDA.GetListJson(new NewsQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oNewsItem = oNewsDA.GetByIdToObject<NewsItem>(oGridRequest.ItemID);
                        oNewsItem.UpdateObject(context.Request);
                        oNewsDA.UpdateObject<NewsItem>(oNewsItem);
                        AddLog("UPDATE", "News", "UPDATE", oGridRequest.ItemID, $"Cập nhật thông tin{oNewsItem.Title}");
                    }
                    else
                    {
                        oNewsItem.UpdateObject(context.Request);
                        oNewsDA.UpdateObject<NewsItem>(oNewsItem);
                        AddLog("UPDATE", "News", "ADD", oGridRequest.ItemID, $"Thêm mới thông tin{oNewsItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oNewsDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "News", "DELETE", oGridRequest.ItemID, $"Xóa thông tin{outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    break;
                case "DELETE-MULTI":
                    AddLog("DELETE-MULTI", "News", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều thông tin");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oNewsDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "News", "APPROVED", oGridRequest.ItemID, $"Duyệt thông tin{oNewsDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                     oNewsDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "News", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt thông tin{oNewsDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oNewsItem = oNewsDA.GetByIdToObjectSelectFields<NewsItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oNewsItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}