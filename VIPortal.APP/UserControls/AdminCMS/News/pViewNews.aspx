<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewNews.aspx.cs" Inherits="VIPortalAPP.pViewNews" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		<div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
               <%=oNews.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="DescriptionNews" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
              <%:oNews.DescriptionNews%>
            </div>
        </div>
        <div class="form-group row">
            <label for="CreatedDate" class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%:string.Format("{0:dd-MM-yyyy}",oNews.CreatedDate)%>
            </div>
            <label for="TacGia" class="col-sm-2 control-label">Tác giả</label>
            <div class="col-sm-4">
                <%:oNews.TacGia%>
            </div>
        </div>

         <div class="form-group row" data-role="groupImageDD">
            <label for="ImageNews" class="col-sm-2 control-label">Ảnh đại diện</label>
            <div class="col-sm-10">
               <img src="<%=oNews.ImageNews%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="SourceNews" class="col-sm-2 control-label">Nội dung</label>
            <div class="col-sm-10">
               <%=oNews.ContentNews%>
            </div>
        </div>
       

		   <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oNews.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oNews.ListFileAttach[i].Url %>"><%=oNews.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
		<div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%=string.Format("{0:dd-MM-yyyy}", oNews.Created)%>
            </div>
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}",oNews.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
               <%=oNews.Author.LookupValue%>
            </div>
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
               <%=oNews.Editor.LookupValue%>
            </div>    
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>