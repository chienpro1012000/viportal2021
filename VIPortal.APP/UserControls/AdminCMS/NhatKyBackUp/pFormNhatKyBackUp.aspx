<%@ page language="C#" autoeventwireup="true" codebehind="pFormNhatKyBackUp.aspx.cs" inherits="VIPortalAPP.pFormNhatKyBackUp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-NhatKyBackUp" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oNhatKyBackUp.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="GhiChu" class="col-sm-2 control-label">Ghi chú</label>
            <div class="col-sm-10">
                <textarea  name="GhiChu" id="GhiChu" class="form-control"><%=oNhatKyBackUp.GhiChu%></textarea>
            </div>
        </div>
       
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            

            $("#frm-NhatKyBackUp").validate({
                rules: {
                    GhiChu: "required"
                },
                messages: {
                    GhiChu: "Vui lòng nhập ghi chú"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/NhatKyBackUp/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-NhatKyBackUp").trigger("click");
                            $(form).closeModal();
                            $('#tblNhatKyBackUp').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
        });
    </script>
</body>
</html>
