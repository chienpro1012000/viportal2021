<%@ control language="C#" autoeventwireup="true" codebehind="UC_NhatKyBackUp.aspx.cs" inherits="VIPortalAPP.UC_NhatKyBackUp" %>
<div role="body-data" data-title="Backup" class="content_wp" data-action="/UserControls/AdminCMS/NhatKyBackUp/pAction.asp" data-form="/UserControls/AdminCMS/NhatKyBackUp/pFormNhatKyBackUp.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="NhatKyBackUpSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title" />
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <button class="btn btn-primary act-add" type="button">Backup</button>
                    </p>
                </div>
            </div>

            <div class="clsgrid table-responsive">

                <table id="tblNhatKyBackUp" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        var $tagvalue = $(".content_wp");
        var $tblDanhMucChung = $("#tblNhatKyBackUp").viDataTable(
            {
                "frmSearch": "NhatKyBackUpSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "sWidth": "130px",
                            "name": "StartTime", "sTitle": "Thời gian",
                            "mData": function (o) {
                                return formatDateTime(o.StartTime)
                            }
                        },{
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                            },
                            "name": "Title", "sTitle": "Tiêu đề"
                        }, {
                            "data": "Directory",
                            "name": "Directory", "sTitle": "Thư mục"
                        }, {
                            "sWidth": "130px",
                            "name": "FinishTime", "sTitle": "Hoàn thành",
                            "mData": function (o) {
                                return formatDateTime(o.FinishTime)
                            }
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" data-id="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        }, {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="fa fa-trash-o"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn - xs origan act-delete" data-do="DELETE"  data-ItemID="' + o.ID + '" href="javascript:; "><i class="far fa-trash-alt"></i></a>'; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                "aaSorting": [0, 'asc']
            });
    });
</script>
