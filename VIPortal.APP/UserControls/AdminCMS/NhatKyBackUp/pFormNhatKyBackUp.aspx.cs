using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public partial class pFormNhatKyBackUp : pFormBase
    {
        public NhatKyBackUpItem oNhatKyBackUp {get;set;}
        protected void Page_Load(object sender, EventArgs e)
        {
			oNhatKyBackUp = new NhatKyBackUpItem();
            if (ItemID > 0)
            {
                NhatKyBackUpDA oNhatKyBackUpDA = new NhatKyBackUpDA();
                oNhatKyBackUp = oNhatKyBackUpDA.GetByIdToObject<NhatKyBackUpItem>(ItemID);
            }
        }
    }
}