<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewNhatKyBackUp.aspx.cs" Inherits="VIPortalAPP.pViewNhatKyBackUp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
               <%=oNhatKyBackUp.Title%>
            </div>
        </div>
		<div class="form-group">
	<label for="RequestedBy" class="col-sm-2 control-label">RequestedBy</label>
	<div class="col-sm-10">
		<%=oNhatKyBackUp.RequestedBy%>
	</div>
</div>
<div class="form-group">
	<label for="StartTime" class="col-sm-2 control-label">StartTime</label>
	<div class="col-sm-10">
		<%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oNhatKyBackUp.StartTime)%>
	</div>
</div>
<div class="form-group">
	<label for="FinishTime" class="col-sm-2 control-label">FinishTime</label>
	<div class="col-sm-10">
		<%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oNhatKyBackUp.FinishTime)%>
	</div>
</div>
<div class="form-group">
	<label for="Directory" class="col-sm-2 control-label">Directory</label>
	<div class="col-sm-10">
		<%=oNhatKyBackUp.Directory%>
	</div>
</div>
<div class="form-group">
	<label for="BackupID" class="col-sm-2 control-label">BackupID</label>
	<div class="col-sm-10">
		<%=oNhatKyBackUp.BackupID%>
	</div>
</div>
<div class="form-group">
	<label for="WarningCount" class="col-sm-2 control-label">WarningCount</label>
	<div class="col-sm-10">
		<%=oNhatKyBackUp.WarningCount%>
	</div>
</div>
<div class="form-group">
	<label for="ErrorCount" class="col-sm-2 control-label">ErrorCount</label>
	<div class="col-sm-10">
		<%=oNhatKyBackUp.ErrorCount%>
	</div>
</div>
<div class="form-group">
	<label for="Method" class="col-sm-2 control-label">Method</label>
	<div class="col-sm-10">
		<%=oNhatKyBackUp.Method%>
	</div>
</div>
<div class="form-group">
	<label for="TypeBackUpRestore" class="col-sm-2 control-label">TypeBackUpRestore</label>
	<div class="col-sm-10">
		<%=oNhatKyBackUp.TypeBackUpRestore%>
	</div>
</div>

		  <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-10">
               <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oNhatKyBackUp.Created)%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oNhatKyBackUp.Modified)%>
            </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-10">
                <%=oNhatKyBackUp.Author.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-10">
                <%=oNhatKyBackUp.Editor.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oNhatKyBackUp.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oNhatKyBackUp.ListFileAttach[i].Url %>"><%=oNhatKyBackUp.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>