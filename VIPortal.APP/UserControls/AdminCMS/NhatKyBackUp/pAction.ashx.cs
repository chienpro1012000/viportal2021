using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration.Backup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ViPortal_Utils.Base;

namespace VIPortalAPP.NhatKyBackUp
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : IHttpHandler
    {
        public string doAction = string.Empty;
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            if (!string.IsNullOrEmpty(context.Request["do"]))
                doAction = context.Request["do"].ToUpper();

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            NhatKyBackUpDA oNhatKyBackUpDA = new NhatKyBackUpDA();
            NhatKyBackUpItem oNhatKyBackUpItem = new NhatKyBackUpItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    var oData = oNhatKyBackUpDA.GetListJson(new NhatKyBackUpQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oNhatKyBackUpDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oNhatKyBackUpDA.GetListJson(new NhatKyBackUpQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oNhatKyBackUpItem = oNhatKyBackUpDA.GetByIdToObject<NhatKyBackUpItem>(oGridRequest.ItemID);
                        oNhatKyBackUpItem.UpdateObject(context.Request);
                        oNhatKyBackUpDA.UpdateObject<NhatKyBackUpItem>(oNhatKyBackUpItem);
                    }
                    else
                    {
                        oNhatKyBackUpItem.UpdateObject(context.Request);
                        oNhatKyBackUpItem.StartTime = DateTime.Now;
                        oNhatKyBackUpItem.Title = @"Farm\Microsoft SharePoint Foundation Web Application\SharePoint - 6003";
                        oNhatKyBackUpDA.UpdateObject<NhatKyBackUpItem>(oNhatKyBackUpItem);
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    string outb = oNhatKyBackUpDA.DeleteObject(oGridRequest.ItemID);
                    if (string.IsNullOrEmpty(outb))
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb;
                    break;
                case "DELETE-MULTI":
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oNhatKyBackUpDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oNhatKyBackUpDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oNhatKyBackUpItem = oNhatKyBackUpDA.GetByIdToObjectSelectFields<NhatKyBackUpItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oNhatKyBackUpItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}