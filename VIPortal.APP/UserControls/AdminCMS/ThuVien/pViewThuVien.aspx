﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewThuVien.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.ThuVien.pViewThuVien" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
               <%=oThuVien.Title%>
            </div>
        </div>
		<div class="form-group">
	<label for="UrlList" class="col-sm-2 control-label">UrlList</label>
	<div class="col-sm-10">
		<%=oThuVien.UrlList%>
	</div>
</div>
<div class="form-group">
	<label for="TrangThai" class="col-sm-2 control-label">TrangThai</label>
	<div class="col-sm-10">
		<%=oThuVien.TrangThai%>
	</div>
</div>

		  <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-10">
               <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oThuVien.Created)%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oThuVien.Modified)%>
            </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-10">
                <%=oThuVien.Author.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-10">
                <%=oThuVien.Editor.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oThuVien.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oThuVien.ListFileAttach[i].Url %>"><%=oThuVien.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>