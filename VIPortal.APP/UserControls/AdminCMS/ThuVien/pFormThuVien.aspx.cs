﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.ThuVien;

namespace VIPortal.APP.UserControls.AdminCMS.ThuVien
{
    public partial class pFormThuVien : pFormBase
    {
        public ThuVienItem oThuVien { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oThuVien = new ThuVienItem();
            if (ItemID > 0)
            {
                ThuVienDA oThuVienDA = new ThuVienDA();
                oThuVien = oThuVienDA.GetByIdToObject<ThuVienItem>(ItemID);
               
            }
        }
    }
}