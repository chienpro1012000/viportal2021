﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData.ThuVien;

namespace VIPortal.APP.UserControls.AdminCMS.ThuVien
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {

        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);
            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            ThuVienDA oThuVienDA = new ThuVienDA();
            ThuVienItem oThuVienItem = new ThuVienItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oThuVienDA.GetListJson(new ThuVienQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oThuVienDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "ThuVien", "QUERYDATA", 0, "Xem danh sách thư viện");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataThuVien = oThuVienDA.GetListJson(new ThuVienQuery(context.Request));
                    treeViewItems = oDataThuVien.Select(x => new TreeViewItem()
                    {
                        key = x.MaSach,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oThuVienDA.GetListJson(new ThuVienQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oThuVienItem = oThuVienDA.GetByIdToObject<ThuVienItem>(oGridRequest.ItemID);
                        oThuVienItem.UpdateObject(context.Request);
                        oThuVienDA.UpdateObject<ThuVienItem>(oThuVienItem);
                        AddLog("UPDATE", "ThuVien", "UPDATE", oGridRequest.ItemID, $"Cập nhật thư viện{oThuVienItem.Title}");
                    }
                    else
                    {
                        oThuVienItem.UpdateObject(context.Request);
                        oThuVienDA.UpdateObject<ThuVienItem>(oThuVienItem);
                        AddLog("UPDATE", "ThuVien", "ADD", oGridRequest.ItemID, $"Thêm mới thư viện{oThuVienItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oThuVienDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "ThuVien", "DELETE", oGridRequest.ItemID, $"Xóa thư viện{outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    break;
                case "DELETE-MULTI":
                    AddLog("DELETE-MULTI", "ThuVien", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều thư viện");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oThuVienDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "ThuVien", "APPROVED", oGridRequest.ItemID, $"Duyệt thư viện{oThuVienDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oThuVienDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "ThuVien", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt thư viện{oThuVienDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oThuVienItem = oThuVienDA.GetByIdToObjectSelectFields<ThuVienItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oThuVienItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}