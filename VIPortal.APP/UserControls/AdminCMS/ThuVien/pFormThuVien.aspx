﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormThuVien.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.ThuVien.pFormThuVien" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-ThuVien" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oThuVien.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oThuVien.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="MaSach" class="col-sm-2 control-label">Mã sách</label>
            <div class="col-sm-10">
                <input type="text" name="MaSach" id="MaSach" placeholder="Nhập mã sách" value="<%:oThuVien.MaSach%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="NhaXB" class="col-sm-2 control-label">Nhà xuất bản</label>
            <div class="col-sm-10">
                <input type="text" name="NhaXB" id="NhaXB" placeholder="Nhập tên nhà xuất bản" value="<%:oThuVien.NhaXB%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="NguoiMuon" class="col-sm-2 control-label">Người mượn</label>
            <div class="col-sm-10">
                <select data-selected="<%:string.Join(",", oThuVien.NguoiMuon.Select(x => x.LookupId)) %>" data-url="/UserControls/AdminCMS/QuanLySinhVien/pAction.ashx?do=QUERYDATA" data-place="Chọn người mượn" name="NguoiMuon" id="NguoiMuon" class="form-control" multiple></select>
            </div>
        </div>
        <div class="form-group row">
            <label for="NgayMuon" class="col-sm-2 control-label">Ngày Mượn</label>
            <div class="col-sm-10">
                <input type="date" name="NgayMuon" id="NgayMuon" value="<%:oThuVien.NgayMuon%>" class="form-control" />
            </div>
        </div>
         <div class="form-group row">
            <label for="NgayTra" class="col-sm-2 control-label">Ngày Trả</label>
            <div class="col-sm-10">
                <input type="date" name="NgayTra" id="NgayTra" value="<%:oThuVien.NgayTra%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="SoDT" class="col-sm-2 control-label">SĐT</label>
            <div class="col-sm-10">
                <input type="text" name="SoDT" id="SoDT" placeholder="Nhập số điện thoại" value="<%:oThuVien.SoDT%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="GhiChu" class="col-sm-2 control-label">Ghi chú</label>
            <div class="col-sm-10">
                <input type="text" name="GhiChu" id="GhiChu" placeholder="Nhập ghi chú" value="<%:oThuVien.GhiChu%>" class="form-control" />
            </div>
        </div>
       <div class="form-group row">
	<label for="TrangThai" class="col-sm-2 control-label">Trạng thái</label>
	<div class="col-sm-10">
		<input type="text" name="TrangThai" id="TrangThai" placeholder="Nhập trangthai" value="<%:oThuVien.TrangThai%>" class="form-control"/>
	</div>
</div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#NguoiMuon").smSelect2018V2({
                dropdownParent: "#frm-ThuVien"
            });
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oThuVien.ListFileAttach)%>'
            });
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $(".form-group select").select2({
                dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
            });

            $("#frm-ThuVien").validate({
                rules: {
                    Title: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/ThuVien/pAction.ashx", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-ThuVien").trigger("click");
                            $(form).closeModal();
                            $('#tblThuVien').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
        });
    </script>
</body>
</html>

