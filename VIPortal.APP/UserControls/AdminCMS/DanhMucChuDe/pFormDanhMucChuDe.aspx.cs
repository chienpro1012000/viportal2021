using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public partial class pFormDanhMucChuDe : pFormBase
    {
        public DanhMucChuDeItem oDanhMucChuDe {get;set;}
        protected void Page_Load(object sender, EventArgs e)
        {
			oDanhMucChuDe = new DanhMucChuDeItem();
            if (ItemID > 0)
            {
                DanhMucChuDeDA oDanhMucChuDeDA = new DanhMucChuDeDA();
                oDanhMucChuDe = oDanhMucChuDeDA.GetByIdToObject<DanhMucChuDeItem>(ItemID);
            }
        }
    }
}