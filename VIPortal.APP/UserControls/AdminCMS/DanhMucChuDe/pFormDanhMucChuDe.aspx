<%@ page language="C#" autoeventwireup="true" codebehind="pFormDanhMucChuDe.aspx.cs" inherits="VIPortalAPP.pFormDanhMucChuDe" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-DanhMucChuDe" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oDanhMucChuDe.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oDanhMucChuDe.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="NoiDung" class="col-sm-2 control-label">NoiDung</label>
            <div class="col-sm-10">
                <textarea name="NoiDung" id="NoiDung" class="form-control"><%:oDanhMucChuDe.NoiDung%></textarea>
            </div>
        </div>
     
        </div>
           <div class="form-group row">
            <label for="ImageNews" class="col-sm-2 control-label">Hình ảnh</label>
            <div class="col-sm-10" data-role="groupImageDD">

                <input type="hidden" name="AnhDaiDien" id="ImageNewsAlbum" value="<%:oDanhMucChuDe.AnhDaiDien%>" />
                <img title="Ảnh đại diện" id="srcImageNewsAlbum" src="<%=oDanhMucChuDe.AnhDaiDien%>" />
                <button id="btnImageNewsAlbum" type="button" class="btn  btn-success">Hình ảnh</button>
                <button id="btnXoaImageNews1" type="button" class="btn btn-danger">Xóa ảnh</button>
            </div>
        </div>
        <div class="form-group row">
            <label for="TrangThai" class="col-sm-2 control-label">TrangThai</label>
            <div class="col-sm-10">
                <input value="true" type="checkbox" name="TrangThai" id="TrangThai" <%=oDanhMucChuDe.TrangThai? "checked" : string.Empty%> />
            </div>
        </div>
        <div class="form-group row">
            <label for="UrlList" class="col-sm-2 control-label">UrlList</label>
            <div class="col-sm-10">
                <input type="text" name="UrlList" id="UrlList" placeholder="Nhập urllist" value="<%:oDanhMucChuDe.UrlList%>" class="form-control" />
            </div>
        </div>

        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oDanhMucChuDe.ListFileAttach)%>'
            });
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $(".form-group select").select2({
                dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
            });
            CKEDITOR.replace("NoiDung", {
                height: 200
            });
            $("#btnImageNewsAlbum").click(function () {
                $elementId = $(this);
                CKFinder.popup({
                    chooseFiles: true,
                    width: 800,
                    height: 600,
                    onInit: function (finder) {
                        finder.on('files:choose', function (evt) {
                            var file = evt.data.files.first();
                            var urlfileimg = file.getUrl();
                            $("#frm-DanhMucChuDe #ImageNewsAlbum").val(urlfileimg);
                            $("#frm-DanhMucChuDe #srcImageNewsAlbum").attr("src", urlfileimg);
                        });
                        finder.on('file:choose:resizedImage', function (evt) {
                            //var output = document.getElementById(elementId);
                            console.log(evt.data);
                            var urlfileimg = evt.data.resizedUrl;
                            $("#frm-DanhMucChuDe #ImageNewsAlbum").val(urlfileimg);
                            $("#frm-DanhMucChuDe #srcImageNewsAlbum").attr("src", urlfileimg);
                        });
                    }
                });
            });
            $("#btnXoaImageNews1").click(function () {
                //document.getElementById("ImageNews").value = "";
                //document.getElementById("srcImageNewsAlbum").setAttribute("src", "");
                $("#frm-DanhMucChuDe #ImageNewsAlbum").val("");
                $("#frm-DanhMucChuDe #srcImageNewsAlbum").attr("src", "");
            });
            $("#frm-DanhMucChuDe").validate({
                rules: {
                    Title: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/DanhMucChuDe/pAction.ashx", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-DanhMucChuDe").trigger("click");
                            $(form).closeModal();
                            $('#tblDanhMucChuDe').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
        });
    </script>
</body>
</html>
