<%@ control language="C#" autoeventwireup="true" codebehind="UC_DanhMucChuDe.aspx.cs" inherits="VIPortalAPP.UC_DanhMucChuDe" %>
<div role="body-data" data-title="" class="content_wp" data-action="/UserControls/AdminCMS/DanhMucChuDe/pAction.ashx" data-form="/UserControls/AdminCMS/DanhMucChuDe/pFormDanhMucChuDe.aspx">
    <div class="clsmanager row">
        <div class="col-sm-9">
            <div id="DanhMucChuDeSearch" class="form-inline zonesearch">
                <div class="form-group">
                    <input type="hidden" name="do" id="do" value="QUERYDATA" />
                    <label for="Keyword">Từ khóa</label>
                    <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                    <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title" />
                </div>
                <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
            </div>
        </div>
        <div class="col-sm-3">
            <p class="text-right">
                <button class="btn btn-primary act-add" type="button">Thêm mới</button>
            </p>
        </div>
    </div>

    <div class="clsgrid table-responsive">

        <table id="tblDanhMucChuDe" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
        </table>

    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        var $tagvalue = $(".content_wp");
        var $tblDanhMucChung = $("#tblDanhMucChuDe").viDataTable(
            {
                "frmSearch": "DanhMucChuDeSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                            },
                            "name": "Title", "sTitle": "Tiêu đề"
                        },
                        {
                            "data": "NoiDung",
                            "name": "NoiDung", "sTitle": "Noi dung"
                        },
                        {
                            "mData": function (o) {
                                if (o.AnhDaiDien != null && o.AnhDaiDien != "")
                                    return ' <img  data-id="' + o.ID + '" class="act-view" style="width:150px;height:100px"  src="' + o.AnhDaiDien + '" alt="AnhDaiDien" />'
                                else
                                    return ''
                            },
                            "name": "AnhDaiDien", "sTitle": "Ảnh"
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {
                                if (o._ModerationStatus == 0)
                                    return '<a title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                        <%-- "visible": '<%=oUserPermission.EditItem ? "true" : "flase"%>' == "true",--%>
                            "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        }, {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="fa fa-trash"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                       <%-- "visible": '<%=oUserPermission.DeleteItem? "true" : "flase"%>' == "true",--%>
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" href="javascript:;"><i class="fa fa-trash"></i></a>'; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                       <%-- "visible": '<%=(oUserPermission.AddItem||oUserPermission.EditItem || oUserPermission.DeleteItem)? "true" : "flase"%>' == "true",--%>
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                "aaSorting": [0, 'asc']
            });
    });
</script>
