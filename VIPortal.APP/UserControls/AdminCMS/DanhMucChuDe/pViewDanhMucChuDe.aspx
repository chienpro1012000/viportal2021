<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewDanhMucChuDe.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.DanhMucChuDe.pViewDanhMucChuDe" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
               <%=oDanhMucChuDe.Title%>
            </div>
        </div>
		<div class="form-group">
	<label for="NoiDung" class="col-sm-2 control-label">NoiDung</label>
	<div class="col-sm-10">
		<%=oDanhMucChuDe.NoiDung%>
	</div>
</div>
<div class="form-group">
	<label for="AnhDaiDien" class="col-sm-2 control-label">AnhDaiDien</label>
	<div class="col-sm-10">
		<%=oDanhMucChuDe.AnhDaiDien%>
	</div>
</div>
<div class="form-group">
	<label for="TrangThai" class="col-sm-2 control-label">TrangThai</label>
	<div class="col-sm-10">
		<%=oDanhMucChuDe.TrangThai? "Có" : "Không"%>
	</div>
</div>
<div class="form-group">
	<label for="UrlList" class="col-sm-2 control-label">UrlList</label>
	<div class="col-sm-10">
		<%=oDanhMucChuDe.UrlList%>
	</div>
</div>

		  <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-10">
               <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oDanhMucChuDe.Created)%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oDanhMucChuDe.Modified)%>
            </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-10">
                <%=oDanhMucChuDe.Author.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-10">
                <%=oDanhMucChuDe.Editor.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oDanhMucChuDe.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oDanhMucChuDe.ListFileAttach[i].Url %>"><%=oDanhMucChuDe.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>