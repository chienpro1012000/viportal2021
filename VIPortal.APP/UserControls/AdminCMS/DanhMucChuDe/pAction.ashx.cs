using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;

namespace VIPortalAPP.UserControls.AdminCMS.DanhMucChuDe

{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public string doAction = string.Empty;
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            if (!string.IsNullOrEmpty(context.Request["do"]))
                doAction = context.Request["do"].ToUpper();

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            DanhMucChuDeDA oDanhMucChuDeDA = new DanhMucChuDeDA();
            DanhMucChuDeItem oDanhMucChuDeItem = new DanhMucChuDeItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    var oData = oDanhMucChuDeDA.GetListJson(new DanhMucChuDeQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw, 
                        oDanhMucChuDeDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oDanhMucChuDeDA.GetListJson(new DanhMucChuDeQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDanhMucChuDeItem = oDanhMucChuDeDA.GetByIdToObject<DanhMucChuDeItem>(oGridRequest.ItemID);
                        oDanhMucChuDeItem.UpdateObject(context.Request);
                        oDanhMucChuDeDA.UpdateObject<DanhMucChuDeItem>(oDanhMucChuDeItem);
                    }
                    else
                    {
                        oDanhMucChuDeItem.UpdateObject(context.Request);
                        oDanhMucChuDeDA.UpdateObject<DanhMucChuDeItem>(oDanhMucChuDeItem);
                        //AddLog("UPDATE", "DanhMucChuDe", "ADD", oGridRequest.ItemID, $"Thêm mới danh muc chu de{oDanhMucChuDeItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    string outb = oDanhMucChuDeDA.DeleteObject(oGridRequest.ItemID);
                    if (string.IsNullOrEmpty(outb))
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb;
                    break;
                case "DELETE-MULTI":
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oDanhMucChuDeDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    //AddLog("APPROVED", "DanhMucChuDe", "APPROVED", oGridRequest.ItemID, $"Duyệt tin nổi bật{oDanhMucChuDeDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oDanhMucChuDeDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                   // AddLog("PENDDING", "DanhMucChuDe", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt chu de{oDanhMucChuDeDA.GetTitleByID(oGridRequest.ItemID)}");

                    oResult.Message = "Huy duyet thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDanhMucChuDeItem = oDanhMucChuDeDA.GetByIdToObjectSelectFields<DanhMucChuDeItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oDanhMucChuDeItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}