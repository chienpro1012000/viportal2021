﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewKSCauTraLoi.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.KSCauTraLoi.pViewKSCauTraLoi" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		  <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Câu trả lời</label>
            <div class="col-sm-10">
               <%=oKSCauTraLoi.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="DMDescription" class="col-sm-2 control-label">Chi tiết</label>
            <div class="col-sm-10">
               <%:oKSCauTraLoi.DMDescription%>
            </div>
        </div>
        <div class="form-group row">
            <label for="LookUp" class="col-sm-2 control-label">Bộ câu hỏi</label>
            <div class="col-sm-10">
                <%:oKSCauTraLoi.KSCauHoi.LookupValue%>
            </div>
        </div>
        <%--<div class="form-group row">
            <label for="LookUp" class="col-sm-2 control-label">Bộ chủ đề</label>
            <div class="col-sm-10">
               <%:oKSCauTraLoi.KSBoChuDe.LookupValue%>
            </div>
        </div>--%>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%=string.Format("{0:dd-MM-yyyy}", oKSCauTraLoi.Created)%>
            </div>
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}",oKSCauTraLoi.Modified)%>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>