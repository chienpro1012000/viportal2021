﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_KSCauTraLoi.ascx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.KSCauTraLoi.UC_KSCauTraLoi" %>
<div role="body-data" data-title="câu trả lời" class="content_wp1" data-action="/UserControls/AdminCMS/KSCauTraLoi/pAction.ashx" data-form="/UserControls/AdminCMS/KSCauTraLoi/pFormKSCauTraLoi.aspx" data-view="/UserControls/AdminCMS/KSCauTraLoi/pViewKSCauTraLoi.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="KSCauTraLoiSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title" />
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <button class="btn btn-primary act-add" type="button">Thêm mới</button>
                    </p>
                </div>
            </div>

            <div class="clsgrid table-responsive">

                <table id="tblKSCauTraLoi" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function formatDate(strValue) {
        if (strValue == null) return "";
        //var date = new Date(strValue);
        var d = new Date(strValue);
        var month = d.getMonth() + 1;
        var day = d.getDate();
        var output = //(('' + hour).length < 2 ? '0' : '') + hour + ':' +
            //(('' + minute).length < 2 ? '0' : '') + minute + " ngày " +
            (('' + day).length < 2 ? '0' : '') + day + '/' +
            (('' + month).length < 2 ? '0' : '') + month + '/' +
            d.getFullYear();
        return output;
    }
    $(document).ready(function () {

        var $tagvalue = $(".content_wp1");
        var $tblDanhMucChung = $("#tblKSCauTraLoi").viDataTable(
            {
                "frmSearch": "KSCauTraLoiSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "mData": function (o) {

                                return '<a data-id="' + o.ID + '" >' + o.DMSTT + '</a>';
                            },
                            "name": "DMSTT", "sTitle": "STT",
                            "sWidth": "30px",
                        }, {
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '" >' + o.KSBoChuDe.Title + '</a>';
                            },
                            "name": "KSBoChuDe", "sTitle": "Bộ chủ đề",
                            "sWidth": "250px"
                        },
                        {
                            "mData": function (o) {
                                if (o.Title != null)
                                    return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                                else
                                    return "";
                            },
                            "name": "Title", "sTitle": "Câu trả lời"
                        },


                        {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {
                                if (o._ModerationStatus == 0)

                                    return '<a title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }

                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" title="Sửa" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        }, {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE" title="Xóa"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                "aaSorting": [0, 'asc']
            });
    });
</script>


