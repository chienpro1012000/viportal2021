﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormKSCauTraLoi.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.KSCauTraLoi.pFormKSCauTraLoi" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-KSCauTraLoi" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oKSCauTraLoi.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
          
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Câu trả lời</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oKSCauTraLoi.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="DMDescription" class="col-sm-2 control-label">Chi tiết</label>
            <div class="col-sm-10">
                <textarea name="DMDescription" id="DMDescription" placeholder="Nhập chi tiết câu trả lời" class="form-control"><%:oKSCauTraLoi.DMDescription%></textarea>
            </div>
        </div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            
            $("#frm-KSCauTraLoi").validate({
                rules: {
                    Title: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/KSCauTraLoi/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            var lstID = $("#IdNewsSave").val();
                            if (lstID != "")
                                lstID += "," + result.ID;
                            else
                                lstID = result.ID;
                            $("#IdNews").val(lstID);
                            $("#IdNewsSave").val(lstID);
                            showMsg(result.Message);
                            $(form).closeModal();
                            $('#tblKSCauTraLoi').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-KSCauTraLoi").viForm();
        });
    </script>
</body>
</html>


