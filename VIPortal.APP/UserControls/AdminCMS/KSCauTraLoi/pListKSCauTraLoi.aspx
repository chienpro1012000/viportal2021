﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pListKSCauTraLoi.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.KSCauTraLoi.pListKSCauTraLoi" %>

<style>
    #tblKSCauTraLoi_length {
        display: none !important;
    }

    #tblKSCauTraLoi_info {
        display: none !important;
    }

    #tblKSCauTraLoi_paginate {
        display: none !important;
    }

</style>
<div id="box-frm-CauTraLoi" data-title="câu trả lời" role="body-data" data-size="WIDE" data-form="/UserControls/AdminCMS/KSCauTraLoi/pFormKSCauTraLoi.aspx" data-action="/UserControls/AdminCMS/KSCauTraLoi/pAction.asp" data-view="/UserControls/AdminCMS/KSCauTraLoi/pViewKSCauTraLoi.aspx">
    <h4 style="text-align: center">Câu trả lời</h4>
    <div class="row">
        <div class="col-sm-9">
            <div id="KSCauTraLoiSearch" class="form-inline zonesearch">
                <div class="form-group">
                    <input type="hidden" name="do" id="do" value="QUERYDATA" />
                    <input type="hidden" name="lstIdGet" id="IdNewsSave" value="<%=lstIdGet %>" />
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <p class="text-right">
                <button class="btn btn-primary act-add" type="button">Thêm mới</button>
            </p>
        </div>
    </div>
    <%--<div class="text-right">
        <button type="button" id="btnCauTraLoi" class="btn btn-primary">
            Thêm câu trả lời</button>
    </div>--%>
    <div class="clsgrid table-responsive">

        <table id="tblKSCauTraLoi" data-form="" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
        </table>

    </div>
</div>
<script type="text/javascript">

    $(document).ready(function () {
        $("#btnCauTraLoi").on("click", function (event) {
            var data = {};
            openDialog("Câu trả lời", "/UserControls/AdminCMS/KSCauTraLoi/pFormKSCauTraLoi.aspx");
            event.stopPropagation();
        });
        var $tagvalue = $("#box-frm-CauTraLoi");
        $("#tblKSCauTraLoi").viDataTable(
            {
                "frmSearch": "KSCauTraLoiSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [

                        {
                            "mData": function (o) {
                                if (o.Title != null)
                                    return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                                else
                                    return "";
                            },
                            "name": "Title", "sTitle": "Tiêu đề câu trả lời"
                        },
                        {
                            //"sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE" title="Xóa"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }
                    ],
                "aaSorting": [0, 'asc']
            });

    });
</script>
