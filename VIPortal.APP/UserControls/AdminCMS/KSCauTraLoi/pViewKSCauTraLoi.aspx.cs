﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.KSCauTraLoi;

namespace VIPortal.APP.UserControls.AdminCMS.KSCauTraLoi
{
    public partial class pViewKSCauTraLoi : pFormBase
    {
        public KSCauTraLoiItem oKSCauTraLoi { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oKSCauTraLoi = new KSCauTraLoiItem();
            if (ItemID > 0)
            {
                KSCauTraLoiDA oKSCauTraLoiDA = new KSCauTraLoiDA(UrlSite);
                oKSCauTraLoi = oKSCauTraLoiDA.GetByIdToObject<KSCauTraLoiItem>(ItemID);
            }
        }
    }
}