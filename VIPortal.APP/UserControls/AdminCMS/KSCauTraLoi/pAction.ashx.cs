﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData.KSBoChuDe;
using ViPortalData.KSCauTraLoi;
namespace VIPortal.APP.UserControls.AdminCMS.KSCauTraLoi
{/// <summary>
 /// Summary description for pAcion
 /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public int NewID { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);
            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            KSCauTraLoiDA oKSCauTraLoiDA = new KSCauTraLoiDA(UrlSite);
            KSCauTraLoiItem oKSCauTraLoiItem = new KSCauTraLoiItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    var oData = oKSCauTraLoiDA.GetListJson(new KSCauTraLoiQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oKSCauTraLoiDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "KSCauTraLoiDA", "QUERYDATA", 0, "Xem danh sách khảo sát câu trả lời");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataKSCauTraLoi = oKSCauTraLoiDA.GetListJson(new KSCauTraLoiQuery(context.Request));
                    treeViewItems = oDataKSCauTraLoi.Select(x => new TreeViewItem()
                    {
                        key = x.DMDescription,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;

                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oKSCauTraLoiDA.GetListJson(new KSCauTraLoiQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oKSCauTraLoiItem = oKSCauTraLoiDA.GetByIdToObject<KSCauTraLoiItem>(oGridRequest.ItemID);
                        oKSCauTraLoiItem.UpdateObject(context.Request);
                        oKSCauTraLoiDA.UpdateObject<KSCauTraLoiItem>(oKSCauTraLoiItem);
                        AddLog("UPDATE", "KSCauTraLoiDA", "UPDATE", oGridRequest.ItemID, $"Cập nhật khảo sát câu trả lời{oKSCauTraLoiItem.Title}");
                    }
                    else
                    {
                        oKSCauTraLoiItem.UpdateObject(context.Request);
                        oKSCauTraLoiDA.UpdateObject<KSCauTraLoiItem>(oKSCauTraLoiItem);
                        NewID = oKSCauTraLoiItem.ID;
                        AddLog("UPDATE", "KSCauTraLoiDA", "ADD", oGridRequest.ItemID, $"Thêm mới khảo sát câu trả lời {oKSCauTraLoiItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    oResult.ID = NewID;
                    break;
                case "UPDATEBINHCHON":
                    if (!string.IsNullOrEmpty(context.Request["BoChuDe"]))
                    {
                        KSBoChuDeDA oKSBoChuDeDA = new KSBoChuDeDA(UrlSite);
                        KSBoChuDeItem oKSBoChuDeItem = oKSBoChuDeDA.GetByIdToObject<KSBoChuDeItem>(Convert.ToInt32(context.Request["BoChuDe"]));
                        if (oKSBoChuDeItem.UCThamGia.Contains($",{CurentUser.ID},"))
                        {
                            oResult.State = ActionState.Error;
                            oResult.Message = "Bạn đã bình chọn trước đó, không thể bình chọn lại";
                            break;
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(oKSBoChuDeItem.UCThamGia)) oKSBoChuDeItem.UCThamGia = ",";
                            oKSBoChuDeDA.SystemUpdateOneField(Convert.ToInt32(context.Request["BoChuDe"]), "UCThamGia", oKSBoChuDeItem.UCThamGia + "," + CurentUser.ID + ",");
                            var lstidcautraloi = context.Request["DapAn"].Split(',');
                            if (lstidcautraloi.Length > 0)
                            {
                                for (int i = 0; i < lstidcautraloi.Length; i++)
                                {
                                    if (!string.IsNullOrEmpty(lstidcautraloi[i]))
                                    {
                                        int idcautraloi = 0;
                                        Int32.TryParse(lstidcautraloi[i], out idcautraloi);
                                        if (idcautraloi > 0)
                                        {
                                            oKSCauTraLoiItem = oKSCauTraLoiDA.GetByIdToObject<KSCauTraLoiItem>(idcautraloi);
                                            oKSCauTraLoiItem.SoLuotBinhChon += 1;
                                            oKSCauTraLoiDA.UpdateObject<KSCauTraLoiItem>(oKSCauTraLoiItem);
                                        }
                                    }
                                }
                            }
                            oResult.Message = "Bạn đã gửi bình chọn thành công";
                        }
                    }
                    break;
                case "DELETE":
                    var outb = oKSCauTraLoiDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "KSCauTraLoiDA", "DELETE", oGridRequest.ItemID, $"Xóa khảo sát câu trả lời {outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    AddLog("DELETE-MULTI", "KSCauTraLoiDA", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều khảo sát câu trả lời");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oKSCauTraLoiDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "KSCauTraLoiDA", "APPROVED", oGridRequest.ItemID, $"Duyệt khảo sát câu trả lời {oKSCauTraLoiDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oKSCauTraLoiDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "KSCauTraLoiDA", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt khảo sát câu trả lời {oKSCauTraLoiDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oKSCauTraLoiItem = oKSCauTraLoiDA.GetByIdToObjectSelectFields<KSCauTraLoiItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oKSCauTraLoiItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}