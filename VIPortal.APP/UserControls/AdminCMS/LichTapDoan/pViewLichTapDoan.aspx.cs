﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.LichTapDoan;

namespace VIPortal.APP.UserControls.AdminCMS.LichTapDoan
{
    public partial class pViewLichTapDoan : pFormBase
    {
        public LichTapDoanItem oLichTapDoan { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oLichTapDoan = new LichTapDoanItem();
            if (ItemID > 0)
            {
                LichTapDoanDA oLichTapDoanDA = new LichTapDoanDA(UrlSite);
                oLichTapDoan = oLichTapDoanDA.GetByIdToObject<LichTapDoanItem>(ItemID);
            }
        }
    }
}