﻿using Microsoft.SharePoint;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.LichTapDoan;

namespace VIPortal.APP.UserControls.AdminCMS.LichTapDoan
{

    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public string contentMail { get; set; }
        public List<string> mailto { get; set; }
        SPFieldLookupValueCollection lstThamGia = new SPFieldLookupValueCollection();
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            LichTapDoanDA oLichTapDoanDA = new LichTapDoanDA(UrlSite);
            LichTapDoanItem oLichTapDoanItem = new LichTapDoanItem();
            LUserDA oLUserDA = new LUserDA();
            LNotificationDA oLNotificationDA = new LNotificationDA(UrlSite);
            LNotificationItem lNotificationItem = new LNotificationItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    var oData = oLichTapDoanDA.GetListJsonSolr(new LichTapDoanQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oLichTapDoanDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "LichTapDoan", "QUERYDATA", 0, "Xem danh sách lịch tập đoàn");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataLichTapDoan = oLichTapDoanDA.GetListJson(new LichTapDoanQuery(context.Request));
                    treeViewItems = oDataLichTapDoan.Select(x => new TreeViewItem()
                    {
                        key = x.LichLanhDaoChuTri.Title,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;

                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oLichTapDoanDA.GetListJson(new LichTapDoanQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLichTapDoanItem = oLichTapDoanDA.GetByIdToObject<LichTapDoanItem>(oGridRequest.ItemID);
                        oLichTapDoanItem.UpdateObject(context.Request);
                        mailto = new List<string>();
                        if (oLichTapDoanItem.LichLanhDaoChuTri.LookupId > 0 && oLichTapDoanItem.LichThanhPhanThamGia.FindIndex(x => x == oLichTapDoanItem.LichLanhDaoChuTri) == -1)
                            lstThamGia.Add(oLichTapDoanItem.LichLanhDaoChuTri);
                        lstThamGia.AddRange(oLichTapDoanItem.LichThanhPhanThamGia);
                        foreach (var item in lstThamGia)
                        {
                            var LUser = oLUserDA.GetByIdToObject<LUserItem>(item.LookupId);
                            if (!string.IsNullOrEmpty(LUser.UserEmail))
                            {
                                mailto.Add(LUser.UserEmail);
                            }
                        }
                        //gửi mail
                        contentMail = "Kính gửi ông/bà,";
                        contentMail += "<div> Ông bà đã nhận được thông báo về cuộc họp: " + oLichTapDoanItem.Title + " vào hồi " + oLichTapDoanItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy") + " tại " + oLichTapDoanItem.LichDiaDiem + "</div>";
                        contentMail += "<div>Nhận được mail này mong quý vị sắp xếp công việc và thời gian tham gia cuộc họp.</div>";
                        contentMail += "<div>Trân trọng cảm ơn.</div>";
                        if (mailto.Count > 0)
                        {
                            SendMail("[Email thông báo lịch họp tập đoàn]", contentMail, mailto);
                        }
                        #region Lưu Notifi
                        string NguoiTao = context.Request["NguoiTao"];
                        lNotificationItem.TitleBaiViet = oLichTapDoanItem.Title;
                        lNotificationItem.LNotiNguoiGui = NguoiTao;
                        if (lstThamGia.Count > 0)
                        {
                            lNotificationItem.LNotiNguoiNhan_Id = "," + string.Join(",", lstThamGia) + ",";
                            lNotificationItem.LNotiChuaXem_Id = lNotificationItem.LNotiNguoiNhan_Id;
                        }
                        lNotificationItem.DoiTuongTitle = "Lịch họp tập đoàn";
                        lNotificationItem.Title = $"Lịch họp:  {oLichTapDoanItem.Title} ngày {oLichTapDoanItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy")} ";
                        lNotificationItem.LNotiMoTa = $"Lịch họp:  {oLichTapDoanItem.Title} ngày {oLichTapDoanItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy")} ";
                        lNotificationItem.LNotiIsSentMail = 1;
                        lNotificationItem.UrlLink = "/Pages/meeting.aspx?Lich=TapDoan";
                        lNotificationItem.LNotiSentTime = DateTime.Now;
                        oLNotificationDA.UpdateObject<LNotificationItem>(lNotificationItem);
                        #endregion
                        oLichTapDoanDA.UpdateObject<LichTapDoanItem>(oLichTapDoanItem);
                        AddLog("UPDATE", "LichTapDoan", "UPDATE", oGridRequest.ItemID, $"Cập nhật lịch tập đoàn{oLichTapDoanItem.Title}");
                    }
                    else
                    {
                        oLichTapDoanItem.UpdateObject(context.Request);
                        mailto = new List<string>();

                        if (oLichTapDoanItem.LichLanhDaoChuTri.LookupId > 0 && oLichTapDoanItem.LichThanhPhanThamGia.FindIndex(x => x == oLichTapDoanItem.LichLanhDaoChuTri) == -1)
                            lstThamGia.Add(oLichTapDoanItem.LichLanhDaoChuTri);
                        lstThamGia.AddRange(oLichTapDoanItem.LichThanhPhanThamGia);
                        foreach (var item in lstThamGia)
                        {
                            var LUser = oLUserDA.GetByIdToObject<LUserItem>(item.LookupId);
                            if (!string.IsNullOrEmpty(LUser.UserEmail))
                            {
                                mailto.Add(LUser.UserEmail);
                            }
                        }
                        //gửi mail
                        contentMail = "Kính gửi ông/bà,";
                        contentMail += "<div> Ông bà đã nhận được thông báo về cuộc họp: " + oLichTapDoanItem.Title + " vào hồi " + oLichTapDoanItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy") + " tại " + oLichTapDoanItem.LichDiaDiem + "</div>";
                        contentMail += "<div>Nhận được mail này mong quý vị sắp xếp công việc và thời gian tham gia cuộc họp.</div>";
                        contentMail += "<div>Trân trọng cảm ơn.</div>";
                        if (mailto.Count > 0)
                        {
                            SendMail("[Email thông báo lịch họp tập đoàn]", contentMail, mailto);
                        }

                        #region Lưu Notifi
                        string NguoiTao = context.Request["NguoiTao"];
                        lNotificationItem.TitleBaiViet = oLichTapDoanItem.Title;
                        lNotificationItem.LNotiNguoiGui = NguoiTao;
                        if (lstThamGia.Count > 0)
                        {
                            lNotificationItem.LNotiNguoiNhan_Id = string.Join(",", lstThamGia);
                            lNotificationItem.LNotiChuaXem_Id = lNotificationItem.LNotiNguoiNhan_Id;
                        }
                        lNotificationItem.DoiTuongTitle = "Lịch họp tập đoàn";
                        lNotificationItem.Title = $"Lịch họp:  {oLichTapDoanItem.Title} ngày {oLichTapDoanItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy")} ";
                        lNotificationItem.LNotiMoTa = $"Lịch họp:  {oLichTapDoanItem.Title} ngày {oLichTapDoanItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy")} ";
                        lNotificationItem.LNotiIsSentMail = 1;
                        lNotificationItem.UrlLink = "/Pages/meeting.aspx?Lich=TapDoan";
                        lNotificationItem.LNotiSentTime = DateTime.Now;
                        oLNotificationDA.UpdateObject<LNotificationItem>(lNotificationItem);
                        #endregion
                        oLichTapDoanDA.UpdateObject<LichTapDoanItem>(oLichTapDoanItem);
                        AddLog("UPDATE", "LichTapDoan", "ADD", oGridRequest.ItemID, $"Thêm mới lịch tập đoàn{oLichTapDoanItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oLichTapDoanDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "LichTapDoan", "DELETE", oGridRequest.ItemID, $"Xóa lịch tập đoàn{outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oLichTapDoanDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    AddLog("DELETE-MULTI", "LichTapDoan", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều lịch tập đoàn");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oLichTapDoanDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "LichTapDoan", "APPROVED", oGridRequest.ItemID, $"Duyệt lịch tập đoàn{oLichTapDoanDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oLichTapDoanDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "LichTapDoan", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt lịch tập đoàn{oLichTapDoanDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLichTapDoanItem = oLichTapDoanDA.GetByIdToObjectSelectFields<LichTapDoanItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oLichTapDoanItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}