<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormLPermission.aspx.cs" Inherits="VIPortalAPP.pFormLPermission" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-LPermission" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oLPermission.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" name="UrlList" id="UrlList" value="<%=UrlList %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" placeholder="Nhập tiêu đề" id="Title" class="form-control" value="<%=oLPermission.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="MaQuyen" class="col-sm-2 control-label">Mã quyền</label>
            <div class="col-sm-10">
                <input type="number" name="MaQuyen" id="MaQuyen" min = "0" oninput="validity.valid||(value='');" placeholder="Nhập mã quyền" value="<%:oLPermission.MaQuyen%>" class="form-control" />
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oLPermission.ListFileAttach)%>'
            });
            $("#Title").focus();
            

            $("#frm-LPermission").validate({
                rules: {
                    Title: "required",
                    MaQuyen: "required"
                },
                messages: {
                    Title: "Nhập tiêu đề",
                    MaQuyen:"Nhập mã quyền"
                },
                submitHandler: function (form) {                 
                    $.post("/UserControls/AdminCMS/LPermission/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-LPermission").trigger("click");
                            $(form).closeModal();
                            //$('#tblLPermission').DataTable().ajax.reload();
                            var tree = $("#treePermission").fancytree("getTree");
                            tree.reload().done(function () {
                            });
                        }
                    }).always(function () {  });
                }
            });
            $("#frm-LPermission").viForm();
        });
    </script>
</body>
</html>
