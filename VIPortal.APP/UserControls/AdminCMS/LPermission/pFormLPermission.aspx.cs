using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortalAPP
{
    public partial class pFormLPermission : pFormBase
    {
        public LPermissionItem oLPermission {get;set;}
        protected void Page_Load(object sender, EventArgs e)
        {
			oLPermission = new LPermissionItem();
            LPermissionDA oLPermissionDA = new LPermissionDA();
            if (ItemID > 0)
            {
                
                oLPermission = oLPermissionDA.GetByIdToObject<LPermissionItem>(ItemID);
            }
            else
            {
                if (!string.IsNullOrEmpty(Request["idparent"]))
                {
                    int idparent = Convert.ToInt32(Request["idparent"]);
                    LPermissionItem oLPermissionsItemParent = oLPermissionDA.GetByIdToObject<LPermissionItem>(idparent);
                    List<LPermissionJson> lstPer = oLPermissionDA.GetListJson(new LPermissionQuery()
                    {
                        MaQuyen = oLPermissionsItemParent.MaQuyen,
                        FieldOrder = "MaQuyen",
                        Ascending = false,
                        Start = 0
                    });
                    if (lstPer.Count > 0)
                    {
                        LPermissionJson temp = lstPer[0];
                        if (temp.MaQuyen == oLPermissionsItemParent.MaQuyen)
                            oLPermission.MaQuyen = oLPermissionsItemParent.MaQuyen + "01";
                        else
                        {
                            //new string('0', 3 - STTSend.ToString().Length) + STTSend;
                            string lastMaPer = temp.MaQuyen.Substring(temp.MaQuyen.Length - 2);
                            int lastSTT = Convert.ToInt32(lastMaPer) + 1;
                            oLPermission.MaQuyen = oLPermissionsItemParent.MaQuyen + (new string('0', 2 - lastSTT.ToString().Length) + lastSTT);
                        }
                    }
                }
            }
        }
    }
}