using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortalAPP
{
    public partial class pViewLPermission : pFormBase
    {
        public LPermissionItem oLPermission = new LPermissionItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oLPermission = new LPermissionItem();
            if (ItemID > 0)
            {
                LPermissionDA oLPermissionDA = new LPermissionDA();
                oLPermission = oLPermissionDA.GetByIdToObject<LPermissionItem>(ItemID);

            }
        }
    }
}