using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortalAPP.LPermission
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);
            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            LPermissionDA oLPermissionDA = new LPermissionDA();
            LPermissionItem oLPermissionItem = new LPermissionItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oLPermissionDA.GetListJson(new LPermissionQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oLPermissionDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "LPermission", "QUERYDATA", 0, "Xem danh sách quyền");
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oLPermissionDA.GetListJson(new LPermissionQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLPermissionItem = oLPermissionDA.GetByIdToObject<LPermissionItem>(oGridRequest.ItemID);
                        oLPermissionItem.UpdateObject(context.Request);
                        oLPermissionDA.UpdateObject<LPermissionItem>(oLPermissionItem);
                        AddLog("UPDATE", "LPermission", "UPDATE", oGridRequest.ItemID, $"Cập nhật quyền{oLPermissionItem.Title}");
                    }
                    else
                    {

                        oLPermissionItem.UpdateObject(context.Request);
                        //check exit permsion
                        if (oLPermissionDA.GetCount(new LPermissionQuery() { MaQuyen = oLPermissionItem.MaQuyen }) == 0)
                            oLPermissionDA.UpdateObject<LPermissionItem>(oLPermissionItem);
                        else
                        {
                            oResult = new ResultAction()
                            {
                                State = ActionState.Error,
                                Message = "Trùng mã quyền"
                            };
                            AddLog("UPDATE", "LPermission", "ADD", oGridRequest.ItemID, $"Thêm mới quyền{oLPermissionItem.Title}");
                            break;
                        }
                        AddLog("UPDATE", "LPermission", "ADD", oGridRequest.ItemID, $"Thêm mới quyền{oLPermissionItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oLPermissionDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "LPermission", "DELETE", oGridRequest.ItemID, $"Xóa quyền{outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    AddLog("DELETE-MULTI", "LPermission", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều quyền");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oLPermissionDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "LPermission", "APPROVED", oGridRequest.ItemID, $"Duyệt quyền{oLPermissionDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oLPermissionDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "LPermission", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt quyền{oLPermissionDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLPermissionItem = oLPermissionDA.GetByIdToObjectSelectFields<LPermissionItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oLPermissionItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}