<%@ control language="C#" autoeventwireup="true" codebehind="UC_LPermission.aspx.cs" inherits="VIPortalAPP.UC_LPermission" %>
<div role="body-data" id="box-frm-Per" data-title="" class="content_wp" data-action="/UserControls/AdminCMS/LPermission/pAction.asp" data-form="/UserControls/AdminCMS/LPermission/pFormLPermission.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="LPermissionSearch" class="form-inline zonesearch">
                        <div class="form-group mb-2">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />                            
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title" />
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <button class="btn btn-primary act-add" id="btnAddMenu" data-per="080202" type="button">Thêm mới</button>
                    </p>
                </div>
            </div>

            <div class="clsgrid table-responsive">
                <div id="treePermission"></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var dataUrl = { UrlList: '<%=UrlList%>' };
        var $tagvalue = $("#box-frm-Per");
        $tagvalue.data("parameters", dataUrl);
        $('#treePermission').fancytree({
            //  extensions: ['contextMenu'],
            source: {
                url: "/VIPortalAPI/api/LPermissions/QUERYTREE?UrlList=<%=UrlList%>"
            },
            renderNode: function (event, data) {
                //console.log(data);
                var node = data.node;
                $(node.span).attr('data-id', data.node.key);

            }
        });

        $.contextMenu({
            selector: '#treePermission .fancytree-node',
            callback: function (key, options) {
                //console.log($(this));
                switch (key) {
                    case "add":
                        openDialog("Thêm mới quyền", "/UserControls/AdminCMS/LPermission/pFormLPermission.aspx", { idparent: $(this).attr("data-id"), UrlList : '<%=UrlList%>' }, 800);
                        break;
                    case "edit":
                        openDialog("Sửa quyền", "/UserControls/AdminCMS/LPermission/pFormLPermission.aspx", { ItemID: $(this).attr("data-id"), do: "UPDATE", UrlList : '<%=UrlList%>' }, 800);
                        break;
                    case "delete":
                        DeleteFromDialogFunc("/UserControls/AdminCMS/LPermission/pAction.asp", { ItemID: $(this).attr("data-id"), do: "DELETE", UrlList : '<%=UrlList%>' }, "quyền", function () {
                            var tree = $("#treePermission").fancytree("getTree");
                            tree.reload().done(function () {
                            });
                        });
                        break;
                }
            },
            events: {
            },
            items: {
                "add": {
                    name: "Tạo Quyền con", icon: function (opt, $itemElement, itemKey, item) {
                        // Set the content to the menu trigger selector and add an bootstrap icon to the item.
                        $itemElement.html('<i class="fa fa-plus" aria-hidden="true"></i> ' + 'Tạo Quyền con');
                        // Add the context-menu-icon-updated class to the item
                        //return 'context-menu-icon-add';
                    }
                },
                "edit": {
                    name: "Sửa quyền", icon: function (opt, $itemElement, itemKey, item) {
                        $itemElement.html('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> ' + 'Sửa quyền');
                        //return 'context-menu-icon-edit';
                    }
                },
                "delete": {
                    name: "Xóa quyền", icon: function (opt, $itemElement, itemKey, item) {
                        $itemElement.html('<i class="fa fa-trash-o" aria-hidden="true"></i> ' + 'Xóa quyền');
                        //return 'context-menu-icon-delete';
                    }
                },

            },
        });
        $(".fancytree-container").addClass("fancytree-connectors");
    });
</script>
