﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData.DanhMucMXH;
using ViPortalData.QuyCheNoiBo;

namespace VIPortal.APP.UserControls.AdminCMS.DanhMucMXH
{

    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {

        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            DanhMucMXHDA oDanhMucMXHDA = new DanhMucMXHDA(UrlSite);
            DanhMucMXHItem oDanhMucMXHItem = new DanhMucMXHItem();
            QuyCheNoiBoDA oQueCheDA = new QuyCheNoiBoDA();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    var oData = oDanhMucMXHDA.GetListJsonSolr(new DanhMucMXHQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oDanhMucMXHDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "DanhMucMXH", "QUERYDATA", 0, "Xem danh sách danh mục mạng xã hội ");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataDanhMucMXH = oDanhMucMXHDA.GetListJson(new DanhMucMXHQuery(context.Request));
                    treeViewItems = oDataDanhMucMXH.Select(x => new TreeViewItem()
                    {
                        key = x.DMDescription,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;

                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oDanhMucMXHDA.GetListJson(new DanhMucMXHQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oDanhMucMXHDA.CheckExit(oGridRequest.ItemID, context.Request["Title"]) == 0)
                    {
                        if (oGridRequest.ItemID > 0)
                        {

                            oDanhMucMXHItem = oDanhMucMXHDA.GetByIdToObject<DanhMucMXHItem>(oGridRequest.ItemID);
                            oDanhMucMXHItem.UpdateObject(context.Request);
                            oDanhMucMXHDA.UpdateObject<DanhMucMXHItem>(oDanhMucMXHItem);
                            AddLog("UPDATE", "DanhMucXNH", "UPDATE", oGridRequest.ItemID, $"Cập nhật danh mục mạng xã hội {oDanhMucMXHItem.Title}");
                        }
                        else
                        {
                            oDanhMucMXHItem.UpdateObject(context.Request);
                            oDanhMucMXHDA.UpdateObject<DanhMucMXHItem>(oDanhMucMXHItem);
                            AddLog("UPDATE", "DanhMucXNH", "ADD", oGridRequest.ItemID, $"Thêm mới danh mục mạng xã hội {oDanhMucMXHItem.Title}");
                        }
                        oResult.Message = "Lưu thành công";
                    }
                    else
                    {
                        oResult = new ResultAction() { State = ActionState.Error, Message = $"Tiêu đề {context.Request["Title"]} đã tồn tại trong hệ thống" };
                    }
                    
                    break;
                case "DELETE":
                    
                        var outb = oDanhMucMXHDA.DeleteObjectV2(oGridRequest.ItemID);
                        AddLog("DELETE", "DanhMucXNH", "DELETE", oGridRequest.ItemID, $"Xóa danh mục mạng xã hội  {outb.Message}");
                        if (outb.State == ActionState.Succeed)
                            oResult.Message = "Xóa thành công";
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oDanhMucMXHDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    AddLog("DELETE-MULTI", "DanhMucXNH", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều danh mục mạng xã hội ");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oDanhMucMXHDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "DanhMucXNH", "APPROVED", oGridRequest.ItemID, $"Duyệt danh mục mạng xã hội {oDanhMucMXHDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oDanhMucMXHDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "DanhMucXNH", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt danh mục mạng xã hội {oDanhMucMXHDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDanhMucMXHItem = oDanhMucMXHDA.GetByIdToObjectSelectFields<DanhMucMXHItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oDanhMucMXHItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}