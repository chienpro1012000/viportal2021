﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.DanhMucMXH;

namespace VIPortal.APP.UserControls.AdminCMS.DanhMucMXH
{
    public partial class pFormDanhMucMXH : pFormBase
    {
        public DanhMucMXHItem oDanhMucMXH { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oDanhMucMXH = new DanhMucMXHItem();
            if (ItemID > 0)
            {
                DanhMucMXHDA oDanhMucMXHDA = new DanhMucMXHDA(UrlSite);
                oDanhMucMXH = oDanhMucMXHDA.GetByIdToObject<DanhMucMXHItem>(ItemID);
            }
        }
    }
}