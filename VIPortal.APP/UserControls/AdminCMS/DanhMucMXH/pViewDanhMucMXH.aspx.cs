﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ViPortalData.DanhMucMXH;
using ViPortal_Utils.Base;
using VIPORTAL.oWeb;

namespace VIPortal.APP.UserControls.AdminCMS.DanhMucMXH
{
    public partial class pViewDanhMucMXH : pFormBase
    {
        public DanhMucMXHItem oDanhMucMXH = new DanhMucMXHItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oDanhMucMXH = new DanhMucMXHItem();
            if (ItemID > 0)
            {
                DanhMucMXHDA oDanhMucMXHDA = new DanhMucMXHDA(UrlSite);
                oDanhMucMXH = oDanhMucMXHDA.GetByIdToObject<DanhMucMXHItem>(ItemID);

            }
        }
    }
}