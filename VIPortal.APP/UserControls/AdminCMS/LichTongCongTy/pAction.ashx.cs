﻿using Microsoft.SharePoint;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using VIPORTAL;
using ViPortal_Utils;
using ViPortal_Utils.Base;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.Lconfig;
using ViPortalData.LichCaNhan;
using ViPortalData.LichDonVi;
using ViPortalData.LichPhong;
using ViPortalData.LichTongCongTy;

namespace VIPortal.APP.UserControls.AdminCMS.LichTongCongTy
{
    
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public string contentMail { get; set; }
        public List<string> mailto { get; set; }
        SPFieldLookupValueCollection lstThamGia = new SPFieldLookupValueCollection();

        private ResultAction GiaoLichTongCTY(HttpContext _context, GiaoLichDonVi giaoLichDonViold, List<int> ItemIDs)
        {
            ResultAction oResult = new ResultAction();
            LichTongCongTyDA oLichTongCongTyDA = new LichTongCongTyDA();
            if (ItemIDs.Count > 0)
            {
                #region groups lại
                //List<int> ItemIDs = _context.Request["ItemIDs"].Split(',').Select(x => Convert.ToInt32(x)).ToList();
                LichDonViDA lichDonViDA = new LichDonViDA();
                SPFieldLookupValue LichLanhDaoChuTri = new SPFieldLookupValue();
                SPFieldLookupValueCollection LichPhongBanThamGia = new SPFieldLookupValueCollection();
                SPFieldLookupValueCollection LichThanhPhanThamGia = new SPFieldLookupValueCollection();
                //SPFieldLookupValue LichPhongChuTri = new SPFieldLookupValue();
                string LichGhiChuGiao = "";
                string JsonKhachMoi = "";
                string CHUAN_BI = "";
                string LichDiaDiem = "";
                if (!string.IsNullOrEmpty(_context.Request["LichDiaDiem"]))
                {
                    LichDiaDiem = _context.Request["LichDiaDiem"];
                }
                if (!string.IsNullOrEmpty(_context.Request["CHUAN_BI"]))
                {
                    CHUAN_BI = _context.Request["CHUAN_BI"];
                }
                if (!string.IsNullOrEmpty(_context.Request["LichLanhDaoChuTri"]))
                {
                    LichLanhDaoChuTri = new SPFieldLookupValue(_context.Request["LichLanhDaoChuTri"]);
                }
                //if (!string.IsNullOrEmpty(context.Request["LichPhongChuTri"]))
                //{
                //    LichPhongChuTri = new SPFieldLookupValue(context.Request["LichPhongChuTri"]);
                //}
                if (!string.IsNullOrEmpty(_context.Request["LichPhongBanThamGia"]))
                {
                    //các phòng tham gia họp nếu có.
                    LichPhongBanThamGia = clsFucUtils.StringToLookup(_context.Request["LichPhongBanThamGia"]);
                }
                else if (!string.IsNullOrEmpty(_context.Request["LichPhongBanThamGia[]"]))
                {
                    //các phòng tham gia họp nếu có.
                    LichPhongBanThamGia = clsFucUtils.StringToLookup(_context.Request["LichPhongBanThamGia[]"]);
                }

                if (!string.IsNullOrEmpty(_context.Request["LichThanhPhanThamGia"]))
                {
                    //các phòng tham gia họp nếu có.
                    LichThanhPhanThamGia = clsFucUtils.StringToLookup(_context.Request["LichThanhPhanThamGia"]);
                }
                else if (!string.IsNullOrEmpty(_context.Request["LichThanhPhanThamGia[]"]))
                {
                    //các phòng tham gia họp nếu có.
                    LichThanhPhanThamGia = clsFucUtils.StringToLookup(_context.Request["LichThanhPhanThamGia[]"]);
                }
                //còn khác nhiều mục khác.
                if (!string.IsNullOrEmpty(_context.Request["LichGhiChuGiao"]))
                {
                    //các phòng tham gia họp nếu có.
                    LichGhiChuGiao = _context.Request["LichGhiChuGiao"];
                }
                //JsonKhachMoi
                if (!string.IsNullOrEmpty(_context.Request["JsonKhachMoi"]))
                {
                    //các phòng tham gia họp nếu có.
                    JsonKhachMoi = _context.Request["JsonKhachMoi"];
                }
                //file đính kèm thì add lịch vào.
                //NhatKyXuLy
                NhatKyXuLyDA nhatKyXuLyDA = new NhatKyXuLyDA();

                foreach (var item in ItemIDs)
                {
                    #region Nhật ký xử lý
                    NhatKyXuLyItem nhatKyXuLyItem = new NhatKyXuLyItem();
                    nhatKyXuLyItem.UpdateObject(_context.Request);
                    if (nhatKyXuLyItem.ListFileAttachAdd.Count > 0)
                    {
                        nhatKyXuLyItem.Title = "Thông tin giao lịch đơn vị";
                        nhatKyXuLyItem.CreatedDate = DateTime.Now;
                        nhatKyXuLyItem.LogDoiTuong = "LichDonVi";
                        nhatKyXuLyItem.LogIDDoiTuong = item;
                        nhatKyXuLyItem.LogThaoTac = "GiaoLich";
                        nhatKyXuLyItem.CreatedUser = new SPFieldLookupValue(CurentUser.ID, CurentUser.Title);
                        nhatKyXuLyDA.UpdateObject<NhatKyXuLyItem>(nhatKyXuLyItem);
                        nhatKyXuLyItem = nhatKyXuLyDA.GetByIdToObject<NhatKyXuLyItem>(nhatKyXuLyItem.ID);
                    }
                    #endregion
                    //Luu lại thong tin và getbyid nó ra để lấy file đính kèm lưu vào bản ghi đơn vị.
                    LichCaNhanDA lichCaNhanDA = new LichCaNhanDA();
                    LichTongCongTyItem lichTongCongTyItem = oLichTongCongTyDA.GetByIdToObject<LichTongCongTyItem>(item);
                    ///check trungf lich cua nguoi dung vs cacs thoong tin lich ca nhan cu.
                    ///
                    if (LichLanhDaoChuTri.LookupId > 0 && (string.IsNullOrEmpty(_context.Request["NoCheck"]) && doAction == "DONGBO"))
                    {

                        List<LichCaNhanJson> lstLich = lichCaNhanDA.GetListJsonSolr(new LichCaNhanQuery() { checkTrungLich = true, LichThoiGianBatDau = lichTongCongTyItem.LichThoiGianBatDau.Value.AddHours(-7), LichThoiGianKetThuc = lichTongCongTyItem.LichThoiGianKetThuc.Value.AddHours(-7), CreatedUser = LichLanhDaoChuTri.LookupId, FieldOrder = "Created", Ascending = true });
                        if (lstLich.Count > 0)
                        {
                            //bij trung lich. 
                            oResult.Message = $"Đã có lịch dành cho Lãnh đạo {lstLich.FirstOrDefault()?.CreatedUser.Title} trong không thời gian {string.Format("{0:HH:mm dd/MM/yyyy}", lichTongCongTyItem.LichThoiGianBatDau.Value)} đến {string.Format("{0:HH:mm dd/MM/yyyy}", lichTongCongTyItem.LichThoiGianKetThuc.Value)}";
                            oResult.State = ActionState.Warning;
                            break;
                        }
                    }
                    #region MyRegion
                    //update trường đã giao cho lịch tổng cty.
                    //LogText _groupid_
                    //<i class="far fa-check-circle"></i>

                    lichTongCongTyItem.LogText += $"_${CurentUser.Groups.ID}$_";
                    oLichTongCongTyDA.SystemUpdateOneField(item, "LogText", lichTongCongTyItem.LogText);
                    oLichTongCongTyDA.SaveSolr(item);
                    //get lịch về cho đơn vị.
                    LichDonViItem lichDonViItem = new LichDonViItem()
                    {
                        fldGroup = new SPFieldLookupValue(CurentUser.fldGroup, ""),
                        OldID = lichTongCongTyItem.ID.ToString(),
                        Title = lichTongCongTyItem.Title,
                        LogText = lichTongCongTyItem.LogText + $"_LichTongCongTy-{lichTongCongTyItem.ID}_",
                        LichDiaDiem = LichDiaDiem,
                        LichGhiChu = lichTongCongTyItem.LichGhiChu,
                        LichNoiDung = lichTongCongTyItem.LichNoiDung,
                        LichThoiGianBatDau = lichTongCongTyItem.LichThoiGianBatDau,
                        LichThoiGianKetThuc = lichTongCongTyItem.LichThoiGianKetThuc,
                        NoiDungChuanBi = lichTongCongTyItem.NoiDungChuanBi,
                        LichLanhDaoChuTri = LichLanhDaoChuTri, //chỗ này là khi chọn đon vị.
                        LichPhongBanThamGia = LichPhongBanThamGia, //chỗ này là khi chọn đon vị.
                        LichThanhPhanThamGia = LichThanhPhanThamGia, //lấy từ thông tin giao
                        CHUAN_BI = CHUAN_BI,
                        THANH_PHAN = lichTongCongTyItem.THANH_PHAN,
                        CHU_TRI = lichTongCongTyItem.CHU_TRI
                    };
                    lichDonViDA.UpdateObject<LichDonViItem>(lichDonViItem);
                    
                    GiaoLichDonVi giaoLichDonVi = new GiaoLichDonVi()
                    {
                        LichLanhDaoChuTri = lichDonViItem.LichLanhDaoChuTri,
                        LichPhongBanThamGia = lichDonViItem.LichPhongBanThamGia,
                        LichThanhPhanThamGia = lichDonViItem.LichThanhPhanThamGia,
                        JsonKhachMoi = Newtonsoft.Json.JsonConvert.DeserializeObject<List<KhachMoi>>(JsonKhachMoi),
                        LichGhiChuGiao = LichGhiChuGiao,
                        ListFileAttach = nhatKyXuLyItem.ListFileAttach.Select(x => new BaseFileAttach()
                        {
                            Name = x.Name,
                            Url = x.Url
                        }).ToList()
                    };
                    //Lấy về danh sách file xóa
                    if (!string.IsNullOrEmpty(_context.Request["listValueRemoveFileAttach"]))
                    {
                        string[] tempLinkList = _context.Request["listValueRemoveFileAttach"].Split('#');
                        for (int indexLink = 0; indexLink < tempLinkList.Length; indexLink++)
                        {
                            if (!string.IsNullOrEmpty(tempLinkList[indexLink]))
                            {
                                int indexfileremove = giaoLichDonViold.ListFileAttach.FindIndex(x => x.Name == tempLinkList[indexLink]);
                                giaoLichDonViold.ListFileAttach.RemoveAt(indexfileremove);
                            }
                        }
                    }
                    giaoLichDonVi.ListFileAttach.AddRange(giaoLichDonViold.ListFileAttach);
                    lichDonViDA.UpdateOneField(lichDonViItem.ID, "LogNoiDung", Newtonsoft.Json.JsonConvert.SerializeObject(giaoLichDonVi));
                    lichDonViDA.UpdateSPModerationStatus(lichDonViItem.ID, SPModerationStatusType.Approved); //duyệt luôn.
                    lichDonViItem = lichDonViDA.GetByIdToObject<LichDonViItem>(lichDonViItem.ID);
                    AddNotifiLich(lichDonViItem);                                                                               //check trùng người vào thời gian họp nhé.
                    //LichPhongDA lichPhongDA = new LichPhongDA();
                    if (LichPhongBanThamGia.Count > 0)
                    {
                        HostingEnvironment.QueueBackgroundWorkItem(cancellationToken => new WorkerAddLichPhong(LichPhongBanThamGia, lichDonViItem).StartProcessing(cancellationToken));
                        //xử lý với phòng ban tham gia.
                        //foreach (var PbHop in LichPhongBanThamGia)
                        //{
                        //    LichPhongItem lichPhong = new LichPhongItem()
                        //    {
                        //        fldGroup = new SPFieldLookupValue(PbHop.LookupId, ""),
                        //        OldID = lichDonViItem.ID.ToString(),
                        //        Title = lichTongCongTyItem.Title,
                        //        LogText = lichTongCongTyItem.LogText + $"_LichTongCongTy-{lichTongCongTyItem.ID}_",
                        //        LichDiaDiem = lichDonViItem.LichDiaDiem,
                        //        LichGhiChu = lichTongCongTyItem.LichGhiChu,
                        //        LichNoiDung = lichTongCongTyItem.LichNoiDung,
                        //        LichThoiGianBatDau = lichTongCongTyItem.LichThoiGianBatDau,
                        //        LichThoiGianKetThuc = lichTongCongTyItem.LichThoiGianKetThuc,
                        //        NoiDungChuanBi = lichTongCongTyItem.NoiDungChuanBi,
                        //        LichLanhDaoChuTri = LichLanhDaoChuTri, //chỗ này là khi chọn đon vị.
                        //        LichPhongBanThamGia = LichPhongBanThamGia, //chỗ này là khi chọn đon vị.
                        //        LichThanhPhanThamGia = LichThanhPhanThamGia, //lấy từ thông tin giao
                        //        CHUAN_BI = lichDonViItem.CHUAN_BI,
                        //        THANH_PHAN = lichTongCongTyItem.THANH_PHAN,
                        //        CHU_TRI = lichTongCongTyItem.CHU_TRI,
                        //        LogNoiDung = Newtonsoft.Json.JsonConvert.SerializeObject(giaoLichDonVi)
                        //    };

                        //    lichPhongDA.UpdateObject<LichPhongItem>(lichPhong);
                        //    lichPhongDA.UpdateSPModerationStatus(lichPhong.ID, SPModerationStatusType.Approved); //duyệt luôn.
                        //}
                       
                    }
                    #endregion
                    //taoj lichj ca nhan khi giao ve cho nguoi dung.
                    if (LichLanhDaoChuTri.LookupId > 0)
                    {

                        LichCaNhanItem lichCaNhan = new LichCaNhanItem()
                        {
                            CreatedUser = new SPFieldLookupValue(LichLanhDaoChuTri.LookupId, ""),
                            OldID = lichDonViItem.ID.ToString(),
                            Title = lichTongCongTyItem.Title,
                            LogText = lichTongCongTyItem.LogText + $"_LichTongCongTy-{lichTongCongTyItem.ID}_",
                            LichDiaDiem = lichDonViItem.LichDiaDiem,
                            LichGhiChu = lichTongCongTyItem.LichGhiChu,
                            LichNoiDung = lichTongCongTyItem.LichNoiDung,
                            LichThoiGianBatDau = lichTongCongTyItem.LichThoiGianBatDau,
                            LichThoiGianKetThuc = lichTongCongTyItem.LichThoiGianKetThuc,
                            LichLanhDaoChuTri = LichLanhDaoChuTri, //chỗ này là khi chọn đon vị.
                            LichPhongBanThamGia = LichPhongBanThamGia, //chỗ này là khi chọn đon vị.
                            LichThanhPhanThamGia = LichThanhPhanThamGia, //lấy từ thông tin giao
                            NoiDungChuanBi = lichTongCongTyItem.NoiDungChuanBi,
                            // LichPhongBanThamGia = LichPhongBanThamGia, //chỗ này là khi chọn đon vị.
                            //LichThanhPhanThamGia = string.Join(", ", lUserJsons.Select(x=>x.Title)), //lấy từ thông tin giao
                            CHUAN_BI = lichDonViItem.CHUAN_BI,
                            THANH_PHAN = lichTongCongTyItem.THANH_PHAN,
                            CHU_TRI = lichTongCongTyItem.CHU_TRI,
                            LogNoiDung = Newtonsoft.Json.JsonConvert.SerializeObject(giaoLichDonVi)
                        };
                        lichCaNhanDA.UpdateObject<LichCaNhanItem>(lichCaNhan);
                        lichCaNhanDA.UpdateSPModerationStatus(lichCaNhan.ID, SPModerationStatusType.Approved); //duyệt luôn.
                    }
                    //cá nhân tham gia thì cũng phải add vào.
                    if (LichThanhPhanThamGia.Count > 0)
                    {
                        foreach (var cbthamgia in LichThanhPhanThamGia)
                        {
                            LichCaNhanItem lichCaNhan = new LichCaNhanItem()
                            {
                                CreatedUser = new SPFieldLookupValue(cbthamgia.LookupId, ""),
                                OldID = lichDonViItem.ID.ToString(),
                                Title = lichTongCongTyItem.Title,
                                LichDiaDiem = lichDonViItem.LichDiaDiem,
                                LichGhiChu = lichTongCongTyItem.LichGhiChu,
                                LichNoiDung = lichTongCongTyItem.LichNoiDung,
                                LichThoiGianBatDau = lichTongCongTyItem.LichThoiGianBatDau,
                                LichThoiGianKetThuc = lichTongCongTyItem.LichThoiGianKetThuc,
                                LichLanhDaoChuTri = LichLanhDaoChuTri, //chỗ này là khi chọn đon vị.
                                LichPhongBanThamGia = LichPhongBanThamGia, //chỗ này là khi chọn đon vị.
                                LichThanhPhanThamGia = LichThanhPhanThamGia, //lấy từ thông tin giao
                                NoiDungChuanBi = lichTongCongTyItem.NoiDungChuanBi,
                                // LichPhongBanThamGia = LichPhongBanThamGia, //chỗ này là khi chọn đon vị.
                                //LichThanhPhanThamGia = string.Join(", ", lUserJsons.Select(x=>x.Title)), //lấy từ thông tin giao
                                CHUAN_BI = lichDonViItem.CHUAN_BI,
                                THANH_PHAN = lichTongCongTyItem.THANH_PHAN,
                                CHU_TRI = lichTongCongTyItem.CHU_TRI,
                                LogNoiDung = Newtonsoft.Json.JsonConvert.SerializeObject(giaoLichDonVi)
                            };
                            lichCaNhanDA.UpdateObject<LichCaNhanItem>(lichCaNhan);
                            lichCaNhanDA.UpdateSPModerationStatus(lichCaNhan.ID, SPModerationStatusType.Approved); //duyệt luôn.
                        }
                    }
                }
                if (oResult.State != ActionState.Error && oResult.State != ActionState.Warning)
                    oResult.Message = "Phân công lịch thành công";
                #endregion
            }
            else
            {
                oResult = new ResultAction()
                {
                    State = ActionState.Error,
                    Message = "Không xác định bản ghi lịch cần giao"
                };
            }
            return oResult;
        }
        private void AddNotifiLich(LichDonViItem oLichDonViItem)
        {
            if (oLichDonViItem.LichLanhDaoChuTri.LookupId > 0 && oLichDonViItem.LichThanhPhanThamGia.FindIndex(x => x == oLichDonViItem.LichLanhDaoChuTri) == -1)
                lstThamGia.Add(oLichDonViItem.LichLanhDaoChuTri);
            lstThamGia.AddRange(oLichDonViItem.LichThanhPhanThamGia);
            contentMail = "Kính gửi các Anh/Chị!";
            contentMail += "<div> Ông bà đã nhận được thông báo về cuộc họp: " + oLichDonViItem.Title + " vào hồi " + oLichDonViItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy") + " tại " +
                oLichDonViItem.LichDiaDiem + "</div>";
            contentMail += "<div>Nhận được mail này mong quý vị sắp xếp công việc và thời gian tham gia cuộc họp.</div>";
            contentMail += "<div>Trân trọng cảm ơn.</div>";

            LconfigDA lConfigDA = new LconfigDA();
            string ContentMailTemp = lConfigDA.GetValueMultilConfigByType("EmailTBLich");
            if (!string.IsNullOrEmpty(ContentMailTemp))
            {
                Dictionary<string, string> lstContent = new Dictionary<string, string>();
                string TrangThaiTxt = "Bình thường";
                if (!string.IsNullOrEmpty(oLichDonViItem.LogText))
                {
                    if (oLichDonViItem.LogText.Contains("_TT-ho_"))
                    {
                        TrangThaiTxt = "Hoãn";
                    }
                    else if (oLichDonViItem.LogText.Contains("_TT-bs_"))
                    {
                        TrangThaiTxt = "Bổ sung";
                    }
                    else if (oLichDonViItem.LogText.Contains("_TT-dt_"))
                    {
                        TrangThaiTxt = "Thay đổi";
                    }
                    //dt
                    else
                    {
                        if (oLichDonViItem.DBPhanLoai == 1)
                        {
                            TrangThaiTxt = "Bổ sung";
                        }
                        else if (oLichDonViItem.DBPhanLoai == 2)
                        {
                            TrangThaiTxt = "Hoãn";
                        }
                        else if (oLichDonViItem.DBPhanLoai == 3)
                        {
                            TrangThaiTxt = "Thay đổi";
                        }
                    }
                }
                lstContent.Add("TrangThai", TrangThaiTxt);
                lstContent.Add("NgayBatDau", string.Format("{0:HHhmm Ngày dd/MM/yyyy}", oLichDonViItem.LichThoiGianBatDau));
                lstContent.Add("Diadiem", oLichDonViItem.LichDiaDiem);
                lstContent.Add("Noidung", oLichDonViItem.LichNoiDung);
                lstContent.Add("LanhDao", oLichDonViItem.LichLanhDaoChuTri != null ? oLichDonViItem.LichLanhDaoChuTri.LookupValue : "");
                string Chu_tri_txt = "";
                if (!string.IsNullOrEmpty(oLichDonViItem.CHU_TRI) && oLichDonViItem.CHU_TRI.Contains(";#"))
                {
                    Chu_tri_txt = new SPFieldLookupValue(oLichDonViItem.CHU_TRI).LookupValue;
                }
                else Chu_tri_txt = oLichDonViItem.CHU_TRI;
                lstContent.Add("ChuTri", Chu_tri_txt);

                string CHUAN_BI_txt = "";
                if (!string.IsNullOrEmpty(oLichDonViItem.CHUAN_BI) && oLichDonViItem.CHUAN_BI.Contains(";#"))
                {
                    CHUAN_BI_txt = string.Join(", ", new SPFieldLookupValueCollection(oLichDonViItem.CHUAN_BI).Select(x => x.LookupValue));
                }
                else CHUAN_BI_txt = oLichDonViItem.CHUAN_BI;
                lstContent.Add("ChuanBi", CHUAN_BI_txt);

                string ThanhPhanTxt = "";
                if (!string.IsNullOrEmpty(oLichDonViItem.THANH_PHAN))
                {
                    ThanhPhanTxt += oLichDonViItem.THANH_PHAN;
                }
                if (oLichDonViItem.LichPhongBanThamGia != null && oLichDonViItem.LichPhongBanThamGia.Count > 0)
                {
                    ThanhPhanTxt += ((!string.IsNullOrEmpty(ThanhPhanTxt) ? ", " : "") + string.Join(", ", oLichDonViItem.LichPhongBanThamGia.Select(x => x.LookupValue)));
                }
                if (oLichDonViItem.LichThanhPhanThamGia != null && oLichDonViItem.LichThanhPhanThamGia.Count > 0)
                {
                    ThanhPhanTxt += ((!string.IsNullOrEmpty(ThanhPhanTxt) ? ", " : "") + string.Join(", ", oLichDonViItem.LichThanhPhanThamGia.Select(x => x.LookupValue)));
                }
                lstContent.Add("ThanhPhan", ThanhPhanTxt);
                //lstContent.Add("TrangThai", "Bình thường");
                foreach (Match m in Regex.Matches(ContentMailTemp, @"@[^>]*@"))
                {
                    string key = m.Value.Replace("@", "");
                    if (lstContent.ContainsKey(key))
                    {
                        //Console.WriteLine(m.Value);
                        string values = lstContent.FirstOrDefault(x => x.Key == m.Value.Replace("@", "")).Value;
                        ContentMailTemp = ContentMailTemp.Replace(m.Value, values);
                    }
                    else
                        ContentMailTemp = ContentMailTemp.Replace(m.Value, "");
                }
                contentMail = ContentMailTemp;
            }
            LNotificationDA oLNotificationDA = new LNotificationDA(UrlSite);
            LNotificationItem lNotificationItem = new LNotificationItem();
            #region Lưu Notifi
            string NguoiTao = CurentUser.Title;
            lNotificationItem.TitleBaiViet = oLichDonViItem.Title;
            lNotificationItem.LNotiNguoiGui = NguoiTao;
            if (lstThamGia.Count > 0)
            {
                lNotificationItem.LNotiNguoiNhan_Id = "," + string.Join(",", lstThamGia.Select(x => x.LookupId)) + ",";
                lNotificationItem.LNotiNguoiNhan = lstThamGia.ToString();
                lNotificationItem.LNotiChuaXem_Id = lNotificationItem.LNotiNguoiNhan_Id;
            }
            lNotificationItem.DoiTuongTitle = "Lịch họp đơn vị";
            lNotificationItem.Title = $"Lịch họp:  {oLichDonViItem.Title} ngày {oLichDonViItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy")} ";
            lNotificationItem.LNotiMoTa = $"Lịch họp:  {oLichDonViItem.Title} ngày {oLichDonViItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy")} ";
            lNotificationItem.LNotiIsSentMail = 0;
            lNotificationItem.LNotiNoiDung = contentMail;
            lNotificationItem.LNotiSentTime = DateTime.Now;
            lNotificationItem.UrlLink = "/Pages/meeting.aspx?Lich=LichDonVi&ItemID=" + oLichDonViItem.ID;
            oLNotificationDA.UpdateObject<LNotificationItem>(lNotificationItem);
            #endregion
        }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            LichTongCongTyDA oLichTongCongTyDA = new LichTongCongTyDA();
            LichTongCongTyItem oLichTongCongTyItem = new LichTongCongTyItem();
            LUserDA oLUserDA = new LUserDA();
            LNotificationDA oLNotificationDA = new LNotificationDA(UrlSite);
            LNotificationItem lNotificationItem = new LNotificationItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    var oData = oLichTongCongTyDA.GetListJsonSolr(new LichTongCongTyQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oLichTongCongTyDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "LichTongCongTy", "QUERYDATA", 0, "Xem danh sách lịch tổng công ty");
                    break;
                case "DONGBO":
                    #region MyRegion
                    LichTongCongTyQuery oLichTongCongTyQuery = new LichTongCongTyQuery(context.Request);
                    LconfigDA oLconfigDA = new LconfigDA();
                    string APILichDongBo = oLconfigDA.GetListJson(new LconfigQuery() { ConfigType = "APILichDongBo" }).FirstOrDefault()?.ConfigValue;
                    if (string.IsNullOrEmpty(APILichDongBo))
                        APILichDongBo = "http://42.112.213.225:8074";
                    HttpClient client = new HttpClient();
                    //var byteArray = Encoding.ASCII.GetBytes("username:password1234");
                    client.DefaultRequestHeaders.Add("Authorization", "Basic RVZOSEFOT0k6RVZOSEFOT0lAQ3VvbmdYMTY=");
                    //get lichj theo tuan hieen tai.
                    DateTime firstDay = DateTime.Now.StartOfWeek(DayOfWeek.Sunday);
                    DateTime toDay = firstDay.AddDays(14);
                    if (oLichTongCongTyQuery.TuNgay.HasValue)
                        firstDay = oLichTongCongTyQuery.TuNgay.Value;
                    if (oLichTongCongTyQuery.DenNgay.HasValue)
                        toDay = oLichTongCongTyQuery.DenNgay.Value;
                    else toDay = firstDay.AddDays(14);
                    //HttpResponseMessage response = client.GetAsync("http://10.9.125.13:8074/api/Portal/get_LichTuan_New?TuNgay=" + firstDay.ToString("dd/MM/yyyy") + "&DenNgay=" + firstDay.AddDays(7).ToString("dd/MM/yyyy") + "&Loai_Lich=1").Result;
                    HttpResponseMessage response = client.GetAsync(APILichDongBo + "/api/Portal/get_LichTuan_New?TuNgay=" + firstDay.ToString("dd/MM/yyyy") + "&DenNgay=" + toDay.ToString("dd/MM/yyyy") + "&Loai_Lich=1").Result;
                    string content = response.Content.ReadAsStringAsync().Result;
                    Console.WriteLine("Đang tiến hành đồng bộ lịch làm việc...");
                    List<LichTichHop> lichTichHops = Newtonsoft.Json.JsonConvert.DeserializeObject<List<LichTichHop>>(content);
                    LichDonViDA lichDonViDA1 = new LichDonViDA();
                    LichPhongDA oLichPhongDA1 = new LichPhongDA();
                    LichCaNhanDA oLichCaNhanDA1 = new LichCaNhanDA();
                    foreach (LichTichHop lichTichHop in lichTichHops)
                    {

                        try
                        {
                            int IDItem = oLichTongCongTyDA.CheckExit(0, lichTichHop.ID_VPKG, "OldID");
                            string[] objTG = lichTichHop.THOI_GIAN.Split('-');
                            DateTime NGAY_THANG;
                            if (DateTime.TryParseExact(lichTichHop.NGAY_THANG,
                                                        "d/M/yyyy",
                                                        CultureInfo.InvariantCulture,
                                                        DateTimeStyles.None,
                                out NGAY_THANG))
                            {
                                #region update cho lịch cũ
                                LichTongCongTyItem lichTongCongTyItem = new LichTongCongTyItem();
                                if (IDItem > 0)
                                {
                                    lichTongCongTyItem = oLichTongCongTyDA.GetByIdToObject<LichTongCongTyItem>(IDItem);
                                }

                                lichTongCongTyItem.ID = IDItem;
                                lichTongCongTyItem.Title = lichTichHop.NOI_DUNG.GetTitle255();
                                lichTongCongTyItem.LichNoiDung = lichTichHop.NOI_DUNG;
                                lichTongCongTyItem.LichDiaDiem = lichTichHop.DIA_DIEM;
                                lichTongCongTyItem.THANH_PHAN = lichTichHop.THANH_PHAN;
                                lichTongCongTyItem.CHU_TRI = lichTichHop.CHU_TRI;
                                lichTongCongTyItem.CHUAN_BI = lichTichHop.CHUAN_BI;
                                lichTongCongTyItem.LogText = lichTongCongTyItem.LogText.Replace("_TT-", "_DEL-").Replace("_TT-bt_", "");
                                lichTongCongTyItem.LogText += $"_TT-{lichTichHop.TT}_";
                                lichTongCongTyItem.OldID = lichTichHop.ID_VPKG;
                                lichTongCongTyItem.ID = IDItem;
                                if (objTG.Length == 2)
                                {
                                    string[] TimeBatdat = objTG[0].Split('h');

                                    lichTongCongTyItem.LichThoiGianBatDau = new DateTime(NGAY_THANG.Year, NGAY_THANG.Month, NGAY_THANG.Day, Convert.ToInt32(TimeBatdat[0]), Convert.ToInt32(TimeBatdat[1]), 0);

                                    string[] TimeKetThuc = objTG[1].Split('h');

                                    lichTongCongTyItem.LichThoiGianKetThuc = new DateTime(NGAY_THANG.Year, NGAY_THANG.Month, NGAY_THANG.Day, Convert.ToInt32(TimeKetThuc[0]), Convert.ToInt32(TimeKetThuc[1]), 0);
                                    if (lichTichHop.TT != "bt" || IDItem == 0)
                                    {
                                        oLichTongCongTyDA.UpdateObject<LichTongCongTyItem>(lichTongCongTyItem);
                                        AddLog("UPDATELICHDONVIDONBO", "LichTongCongTy", "UPDATELICHDONVIDONBO", lichTongCongTyItem.ID, $"update lich tcy {lichTongCongTyItem.LichNoiDung}");
                                        oLichTongCongTyDA.UpdateSPModerationStatus(lichTongCongTyItem.ID, SPModerationStatusType.Approved);
                                    }
                                }
                                if (IDItem > 0)
                                {
                                    if (lichTichHop.TT != "bt")
                                    {
                                        //LogText = lichTongCongTyItem.LogText + $"_LichTongCongTy-{lichTongCongTyItem.ID}_",
                                        List<LichDonViJson> lstLichDonvi = lichDonViDA1.GetListJson(new LichDonViQuery()
                                        {
                                            LogText = $"_LichTongCongTy-{IDItem}_",
                                            _ModerationStatus = 1
                                        });
                                        //AddLog("UPDATELICHDONVIDONBO", "LichDonVi", "UPDATELICHDONVIDONBO", IDItem, $"Timf lich don vi {Newtonsoft.Json.JsonConvert.SerializeObject(lstLichDonvi)}");
                                        if (lstLichDonvi.Count < 2)
                                        {
                                            foreach (var itemldv in lstLichDonvi)
                                            {
                                                //add log

                                                itemldv.LogText = itemldv.LogText.Replace("_TT-", "_DEL-").Replace("_DEL-bs_", "").Replace("_DEL-bt_", "");
                                                string outtb = lichDonViDA1.UpdateOneOrMoreField(itemldv.ID, new Dictionary<string, object>()
                                            {
                                                {  "LichNoiDung", lichTichHop.NOI_DUNG },
                                                {  "LichDiaDiem", lichTongCongTyItem.LichDiaDiem },
                                                {  "THANH_PHAN", lichTongCongTyItem.THANH_PHAN },
                                                {  "LichThoiGianKetThuc", lichTongCongTyItem.LichThoiGianKetThuc },
                                                {  "LichThoiGianBatDau", lichTongCongTyItem.LichThoiGianBatDau },
                                                {  "LogText", itemldv.LogText + $"_TT-{lichTichHop.TT}_" },
                                                {  "CHU_TRI", lichTongCongTyItem.CHU_TRI },
                                                {  "CHUAN_BI", lichTongCongTyItem.CHUAN_BI }
                                            }, true);
                                                AddLog("UPDATELICHDONVIDONBO", "LichDonVi", "UPDATELICHDONVIDONBO", itemldv.ID, $"update lich don vi {itemldv.LichNoiDung}, error: {outtb}");
                                                lichDonViDA1.SaveSolr(itemldv.ID);
                                                //gửi mail lịch này nhé.
                                                LichDonViItem oLichDonViItem = lichDonViDA1.GetByIdToObject<LichDonViItem>(itemldv.ID);
                                                AddNotifiLich(oLichDonViItem);
                                            }
                                        }
                                        //update cho lịch phòng. và lịch cá nhân được giao trực tiếp từ lịch đơn vị.
                                        List<LichPhongJson> LstLichPHong = oLichPhongDA1.GetListJson(new LichPhongQuery()
                                        {
                                            LogText = $"_LichTongCongTy-{IDItem}_",
                                            _ModerationStatus = 1
                                        });
                                        foreach (var itemlphong in LstLichPHong)
                                        {
                                            //add log

                                            itemlphong.LogText = itemlphong.LogText.Replace("_TT-", "_DEL-").Replace("_DEL-bs_", "").Replace("_DEL-bt_", "");
                                            string outtb = oLichPhongDA1.UpdateOneOrMoreField(itemlphong.ID, new Dictionary<string, object>()
                                            {
                                                {  "LichNoiDung", lichTichHop.NOI_DUNG },
                                                {  "LichDiaDiem", lichTongCongTyItem.LichDiaDiem },
                                                {  "THANH_PHAN", lichTongCongTyItem.THANH_PHAN },
                                                {  "LichThoiGianKetThuc", lichTongCongTyItem.LichThoiGianKetThuc },
                                                {  "LichThoiGianBatDau", lichTongCongTyItem.LichThoiGianBatDau },
                                                {  "LogText", itemlphong.LogText + $"_TT-{lichTichHop.TT}_" },
                                                {  "CHU_TRI", lichTongCongTyItem.CHU_TRI },
                                                {  "CHUAN_BI", lichTongCongTyItem.CHUAN_BI }
                                            }, true);
                                            AddLog("UPDATELICHDONVIDONBO", "LichPhong", "UPDATELICHDONVIDONBO", itemlphong.ID, $"update lich phong {itemlphong.LichNoiDung}, error: {outtb}");
                                            oLichPhongDA1.SaveSolr(itemlphong.ID);

                                            //lịch cá nhân.
                                            //check xem phòng đã giao lịch hay chưa.
                                            //để check lại xem cần ko có lẽ ko cần.
                                            //List<LichCaNhanJson> LstLichCaNhanSub = oLichCaNhanDA1.GetListJson(new LichCaNhanQuery()
                                            //{
                                            //    LogText = $"_LichPhong-{itemlphong.ID}_",
                                            //    _ModerationStatus = 1
                                            //});
                                            //foreach (var itemlcanhan in LstLichCaNhanSub)
                                            //{
                                            //    //add log
                                            //    itemlcanhan.LogText = itemlcanhan.LogText.Replace("_TT-", "_DEL-");
                                            //    string outtbsub = oLichCaNhanDA1.UpdateOneOrMoreField(itemlcanhan.ID, new Dictionary<string, object>()
                                            //{
                                            //    {  "LichNoiDung", lichTichHop.NOI_DUNG },
                                            //    {  "LichDiaDiem", lichTongCongTyItem.LichDiaDiem },
                                            //    {  "THANH_PHAN", lichTongCongTyItem.THANH_PHAN },
                                            //    {  "LichThoiGianKetThuc", lichTongCongTyItem.LichThoiGianKetThuc },
                                            //    {  "LichThoiGianBatDau", lichTongCongTyItem.LichThoiGianBatDau },
                                            //    {  "LogText", itemlcanhan.LogText + $"_TT-{lichTichHop.TT}_" },
                                            //    {  "CHU_TRI", lichTongCongTyItem.CHU_TRI },
                                            //    {  "CHUAN_BI", lichTongCongTyItem.CHUAN_BI }
                                            //}, true);
                                            //    //AddLog("UPDATELICHDONVIDONBO", "LichDonVi", "UPDATELICHDONVIDONBO", itemldv.ID, $"update lich don vi {itemldv.LichNoiDung}, error: {outtb}");
                                            //    oLichPhongDA1.SaveSolr(itemlcanhan.ID);
                                            //    //lịch cá nhân.
                                            //}
                                        }

                                        List<LichCaNhanJson> LstLichCaNhan = oLichCaNhanDA1.GetListJson(new LichCaNhanQuery()
                                        {
                                            LogText = $"_LichTongCongTy-{IDItem}_",
                                            _ModerationStatus = 1
                                        });
                                        foreach (var itemlcanhan in LstLichCaNhan)
                                        {
                                            //add log
                                            itemlcanhan.LogText = itemlcanhan.LogText.Replace("_TT-", "_DEL-").Replace("_DEL-bs_", "").Replace("_DEL-bt_", "");
                                            string outtb = oLichCaNhanDA1.UpdateOneOrMoreField(itemlcanhan.ID, new Dictionary<string, object>()
                                            {
                                                {  "LichNoiDung", lichTichHop.NOI_DUNG },
                                                {  "LichDiaDiem", lichTongCongTyItem.LichDiaDiem },
                                                {  "THANH_PHAN", lichTongCongTyItem.THANH_PHAN },
                                                {  "LichThoiGianKetThuc", lichTongCongTyItem.LichThoiGianKetThuc },
                                                {  "LichThoiGianBatDau", lichTongCongTyItem.LichThoiGianBatDau },
                                                {  "LogText", itemlcanhan.LogText + $"_TT-{lichTichHop.TT}_" },
                                                {  "CHU_TRI", lichTongCongTyItem.CHU_TRI },
                                                {  "CHUAN_BI", lichTongCongTyItem.CHUAN_BI }
                                            }, true);
                                            AddLog("UPDATELICHDONVIDONBO", "LichCaNhan", "UPDATELICHDONVIDONBO", itemlcanhan.ID, $"update lich don vi {itemlcanhan.LichNoiDung}, error: {outtb}");
                                            oLichPhongDA1.SaveSolr(itemlcanhan.ID);
                                            //lịch cá nhân.
                                        }
                                    }
                                }

                                #endregion
                            }
                            else
                            {
                                //invalid date
                            }
                        }
                        catch (Exception ex)
                        {
                            oResult = new ResultAction()
                            {
                                State = ActionState.Error,
                                Message = ex.Message + "." + ex.StackTrace
                            };
                            //Console.WriteLine(ex.Message + "." + ex.StackTrace);
                        }

                    }
                    if (oResult.State == ActionState.Succeed)
                        oResult.Message = "Đồng bộ thành công";
                    #endregion
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataLichTongCongTy = oLichTongCongTyDA.GetListJson(new LichTongCongTyQuery(context.Request));
                    treeViewItems = oDataLichTongCongTy.Select(x => new TreeViewItem()
                    {
                        key = x.LichLanhDaoChuTri.Title,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;
                case "XACNHANDAXEM":
                    if (!string.IsNullOrEmpty(context.Request["ItemIDs"]))
                    {
                        List<int> ItemIDs = context.Request["ItemIDs"].Split(',').Select(x => Convert.ToInt32(x)).ToList();
                        foreach (var itemid in ItemIDs)
                        {
                            LichTongCongTyItem lichTongCongTyItem = oLichTongCongTyDA.GetByIdToObject<LichTongCongTyItem>(itemid);
                            lichTongCongTyItem.LogText += $"_${CurentUser.Groups.ID}$_";
                            oLichTongCongTyDA.SystemUpdateOneField(itemid, "LogText", lichTongCongTyItem.LogText);
                            oLichTongCongTyDA.SaveSolr(itemid);
                        }
                    }
                    oResult.Message = "Đã đánh dấu";
                    break;
                case "SUAGIAOLICHDONVI":
                    //xóa đi các lịch đã tạo cũ nếu có.
                    LichDonViDA lichDonViDA = new LichDonViDA();
                    GiaoLichDonVi oGiaoLichDonvi = new GiaoLichDonVi();
                    LichPhongDA lichPhongDA = new LichPhongDA();
                    LichCaNhanDA lichCaNhanDA = new LichCaNhanDA();
                    List<int> lstLichTongCTY = new List<int>();
                    if (!string.IsNullOrEmpty(context.Request["ItemIDs"]))
                    {
                        List<int> ItemIDs = context.Request["ItemIDs"].Split(',').Select(x => Convert.ToInt32(x)).ToList();
                        foreach (var itemid in ItemIDs)
                        {
                            LichDonViItem lichDonViJson = lichDonViDA.GetByIdToObject<LichDonViItem>(itemid); //GetListJson(new LichDonViQuery() { OldID = itemid }).FirstOrDefault();
                            if (lichDonViJson.ID > 0)
                            {
                                //lấy ra file đính kèm cũ là thông tin cầu lưu lại nếu có.
                                if (!string.IsNullOrEmpty(lichDonViJson.LogNoiDung))
                                {
                                    oGiaoLichDonvi = Newtonsoft.Json.JsonConvert.DeserializeObject<GiaoLichDonVi>(lichDonViJson.LogNoiDung);
                                }
                                //query ra tiếp lịch phòng và lịch đơn vị để xóa nó đi nếu có.
                                List<LichPhongJson> lstLichPhong = lichPhongDA.GetListJson(new LichPhongQuery() { LogText = $"_LichTongCongTy-{lichDonViJson.OldID}_", _ModerationStatus = 1 });

                                foreach (LichPhongJson item in lstLichPhong)
                                {
                                    lichPhongDA.UpdateSPModerationStatus(item.ID, SPModerationStatusType.Pending);
                                }

                                List<LichCaNhanJson> lstLichCaNhan = lichCaNhanDA.GetListJson(new LichCaNhanQuery() { LogText = $"_LichTongCongTy-{lichDonViJson.OldID}_", _ModerationStatus = 1 });
                                foreach (LichCaNhanJson item in lstLichCaNhan)
                                {
                                    lichCaNhanDA.UpdateSPModerationStatus(item.ID, SPModerationStatusType.Pending);
                                }
                                //sau đó thì xóa lịch đơn vị này đi để chạy lại.
                                lichDonViDA.UpdateSPModerationStatus(lichDonViJson.ID, SPModerationStatusType.Pending);
                                lstLichTongCTY.Add(Convert.ToInt32(lichDonViJson.OldID));
                                AddLog("UPDATE", "LichDonVi", "UPDATE", oGridRequest.ItemID, $"Giao lại lịch đơn vị {lichDonViJson.Title}");
                            }

                        }
                        oResult = GiaoLichTongCTY(context, oGiaoLichDonvi, lstLichTongCTY);
                    }
                    break;
                case "GIAOLICHDONVI":
                    GiaoLichDonVi oGiaoLichDonvi2 = new GiaoLichDonVi();
                    List<int> ItemIDs2 = context.Request["ItemIDs"].Split(',').Select(x => Convert.ToInt32(x)).ToList();
                    oResult = GiaoLichTongCTY(context, oGiaoLichDonvi2, ItemIDs2);
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oLichTongCongTyDA.GetListJson(new LichTongCongTyQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLichTongCongTyItem = oLichTongCongTyDA.GetByIdToObject<LichTongCongTyItem>(oGridRequest.ItemID);
                        oLichTongCongTyItem.UpdateObject(context.Request);
                        mailto = new List<string>();

                        if (oLichTongCongTyItem.LichLanhDaoChuTri.LookupId > 0 && oLichTongCongTyItem.LichThanhPhanThamGia.FindIndex(x => x == oLichTongCongTyItem.LichLanhDaoChuTri) == -1)
                            lstThamGia.Add(oLichTongCongTyItem.LichLanhDaoChuTri);
                        lstThamGia.AddRange(oLichTongCongTyItem.LichThanhPhanThamGia);
                        foreach (var item in lstThamGia)
                        {
                            var LUser = oLUserDA.GetByIdToObject<LUserItem>(item.LookupId);
                            if (!string.IsNullOrEmpty(LUser.UserEmail))
                            {
                                mailto.Add(LUser.UserEmail);
                            }
                        }
                        //gửi mail
                        contentMail = "Kính gửi ông/bà,";
                        contentMail += "<div> Ông bà đã nhận được thông báo về cuộc họp: " + oLichTongCongTyItem.Title + " vào hồi " + oLichTongCongTyItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy") + " tại " + oLichTongCongTyItem.LichDiaDiem + "</div>";
                        contentMail += "<div>Nhận được mail này mong quý vị sắp xếp công việc và thời gian tham gia cuộc họp.</div>";
                        contentMail += "<div>Trân trọng cảm ơn.</div>";
                        if (mailto.Count > 0)
                        {
                            SendMail("[Email thông báo lịch họp tổng công ty]", contentMail, mailto);
                        }
                        #region Lưu Notifi
                        string NguoiTao = context.Request["NguoiTao"];
                        lNotificationItem.TitleBaiViet = oLichTongCongTyItem.Title;
                        lNotificationItem.LNotiNguoiGui = NguoiTao;
                        if (lstThamGia.Count > 0)
                        {
                            lNotificationItem.LNotiNguoiNhan_Id = "," + string.Join(",", lstThamGia) + ",";
                            lNotificationItem.LNotiChuaXem_Id = lNotificationItem.LNotiNguoiNhan_Id;
                        }
                        lNotificationItem.DoiTuongTitle = "Lịch họp tổng công ty";
                        lNotificationItem.Title = $"Lịch họp:  {oLichTongCongTyItem.Title} ngày {oLichTongCongTyItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy")} ";
                        lNotificationItem.LNotiMoTa = $"Lịch họp:  {oLichTongCongTyItem.Title} ngày {oLichTongCongTyItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy")} ";
                        lNotificationItem.LNotiIsSentMail = 1;
                        lNotificationItem.LNotiSentTime = DateTime.Now;
                        lNotificationItem.UrlLink = "/Pages/meeting.aspx?Lich=TongCty" + $"&ItemID={oLichTongCongTyItem.ID}";
                        oLNotificationDA.UpdateObject<LNotificationItem>(lNotificationItem);
                        #endregion
                        oLichTongCongTyDA.UpdateObject<LichTongCongTyItem>(oLichTongCongTyItem);
                        AddLog("UPDATE", "LichTongCongTy", "UPDATE", oGridRequest.ItemID, $"Cập nhật lịch tổng công ty{oLichTongCongTyItem.Title}");
                    }
                    else
                    {
                        oLichTongCongTyItem.UpdateObject(context.Request);
                        mailto = new List<string>();

                        if (oLichTongCongTyItem.LichLanhDaoChuTri.LookupId > 0 && oLichTongCongTyItem.LichThanhPhanThamGia.FindIndex(x => x == oLichTongCongTyItem.LichLanhDaoChuTri) == -1)
                            lstThamGia.Add(oLichTongCongTyItem.LichLanhDaoChuTri);
                        lstThamGia.AddRange(oLichTongCongTyItem.LichThanhPhanThamGia);
                        foreach (var item in lstThamGia)
                        {
                            var LUser = oLUserDA.GetByIdToObject<LUserItem>(item.LookupId);
                            if (!string.IsNullOrEmpty(LUser.UserEmail))
                            {
                                mailto.Add(LUser.UserEmail);
                            }
                        }

                        oLNotificationDA.UpdateObject<LNotificationItem>(lNotificationItem);
                        //gửi mail
                        contentMail = "Kính gửi ông/bà,";
                        contentMail += "<div> Ông bà đã nhận được thông báo về cuộc họp: " + oLichTongCongTyItem.Title + " vào hồi " + oLichTongCongTyItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy") + " tại " + oLichTongCongTyItem.LichDiaDiem + "</div>";
                        contentMail += "<div>Nhận được mail này mong quý vị sắp xếp công việc và thời gian tham gia cuộc họp.</div>";
                        contentMail += "<div>Trân trọng cảm ơn.</div>";
                        if (mailto.Count > 0)
                        {
                            SendMail("[Email thông báo lịch họp tổng công ty]", contentMail, mailto);
                        }
                        #region Lưu Notifi
                        string NguoiTao = context.Request["NguoiTao"];
                        lNotificationItem.TitleBaiViet = oLichTongCongTyItem.Title;
                        lNotificationItem.LNotiNguoiGui = NguoiTao;
                        if (lstThamGia.Count > 0)
                        {
                            lNotificationItem.LNotiNguoiNhan_Id = "," + string.Join(",", lstThamGia) + ",";
                            lNotificationItem.LNotiChuaXem_Id = lNotificationItem.LNotiNguoiNhan_Id;
                        }
                        lNotificationItem.DoiTuongTitle = "Lịch họp tổng công ty";
                        lNotificationItem.Title = $"Lịch họp:  {oLichTongCongTyItem.Title} ngày {oLichTongCongTyItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy")} ";
                        lNotificationItem.LNotiMoTa = $"Lịch họp:  {oLichTongCongTyItem.Title} ngày {oLichTongCongTyItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy")} ";
                        lNotificationItem.LNotiIsSentMail = 1;
                        lNotificationItem.LNotiSentTime = DateTime.Now;
                        lNotificationItem.UrlLink = "/Pages/meeting.aspx?Lich=TongCty" + $"&ItemID={oLichTongCongTyItem.ID}";

                        #endregion
                        oLichTongCongTyDA.UpdateObject<LichTongCongTyItem>(oLichTongCongTyItem);
                        AddLog("UPDATE", "LichTongCongTy", "ADD", oGridRequest.ItemID, $"Thêm mới lịch tổng công ty {oLichTongCongTyItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oLichTongCongTyDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "LichTongCongTy", "DELETE", oGridRequest.ItemID, $"Xóa lịch tổng công ty {outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oLichTongCongTyDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    AddLog("DELETE-MULTI", "LichTongCongTy", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều lịch tổng công ty");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oLichTongCongTyDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "LichTongCongTy", "APPROVED", oGridRequest.ItemID, $"Duyệt lịch tổng công ty {oLichTongCongTyDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oLichTongCongTyDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "LichTongCongTy", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt lịch tổng công ty {oLichTongCongTyDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLichTongCongTyItem = oLichTongCongTyDA.GetByIdToObjectSelectFields<LichTongCongTyItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oLichTongCongTyItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

    }
}