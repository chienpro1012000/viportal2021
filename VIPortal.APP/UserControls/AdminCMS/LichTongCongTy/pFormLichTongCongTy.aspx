﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormLichTongCongTy.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.LichTongCongTy.pFormLichTongCongTy" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-LichTongCongTy" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oLichTongCongTy.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" name="NguoiTao" id="NguoiTao" value="<%=CurentUser.Title %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" placeholder="Nhập tên cuộc họp" value="<%=oLichTongCongTy.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="LichThoiGianBatDau" class="col-sm-2 control-label">Ngày bắt đầu</label>
            <div class="col-sm-4">
                <input type="text" name="LichThoiGianBatDau" id="LichThoiGianBatDau" value="<%:string.Format("{0:dd/MM/yyyy HH:mm}", oLichTongCongTy.LichThoiGianBatDau)%>" class="form-control datetime" />
            </div>
            <label for="LichThoiGianKetThuc" class="col-sm-2 control-label">Ngày kết thúc</label>
            <div class="col-sm-4">
                <input type="text" name="LichThoiGianKetThuc" id="LichThoiGianKetThuc" value="<%:string.Format("{0:dd/MM/yyyy HH:mm}",oLichTongCongTy.LichThoiGianKetThuc)%>" class="form-control datetime" />
            </div>
        </div>
        <div class="form-group row">
            <label for="LichLanhDaoChuTri" class="col-sm-2 control-label">Lãnh đạo chủ trì</label>
            <div class="col-sm-4">
                <select data-selected="<%:oLichTongCongTy.LichLanhDaoChuTri.LookupId%>" data-url="/UserControls/AdminCMS/LUser/pAction.ashx?do=QUERYDATA" data-place="Chọn người" name="LichLanhDaoChuTri" id="LichLanhDaoChuTri" class="form-control"></select>
            </div>
            <label for="LichTrangThai" class="col-sm-2 control-label">Trạng thái</label>
            <div class="col-sm-4">
                <input type="text" name="LichTrangThai" id="LichTrangThai" class="form-control" value="<%=oLichTongCongTy.LichTrangThai%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="LichDiaDiem" class="col-sm-2 control-label">Địa điểm</label>
            <div class="col-sm-10">
                <textarea type="text" name="LichDiaDiem" id="LichDiaDiem" placeholder="Nhập địa điểm" class="form-control"><%=oLichTongCongTy.LichDiaDiem%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="NoiDungChuanBi" class="col-sm-2 control-label">Nội dung chuẩn bị</label>
            <div class="col-sm-10">
                <textarea name="NoiDungChuanBi" id="NoiDungChuanBi" placeholder="Nhập nội dung chuẩn bị" class="form-control"><%=oLichTongCongTy.NoiDungChuanBi%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="LichNoiDung" class="col-sm-2 control-label">Nội dung</label>
            <div class="col-sm-10">
                <textarea name="LichNoiDung" id="LichNoiDung" placeholder="Nhập nội dung" class="form-control"><%=oLichTongCongTy.LichNoiDung%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="LichThanhPhanThamGia" class="col-sm-2 control-label">Thành phần tham gia</label>
            <div class="col-sm-10">
                <div class="input-group mb-3">
                    <input type="text" disabled="disabled" placeholder="Thành phần tham gia" value="" id="UserChoie" class="form-control" />
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary btnfrm" data-do="CHONNGUOI" title="Chọn người dùng" size="1240"  url="/UserControls/AdminCMS/LichDonVi/pFormChooseThamGia.aspx" id="btnChonNguoi" type="button">Chọn người dùng</button>
                    </div>
                </div>
                <input type="hidden" name="LichThanhPhanThamGia" id="lstLichThamGia" value="<%=string.Join(",", oLichTongCongTy.LichThanhPhanThamGia.Select(x=>x.LookupId)) %>" />
                <textarea id="LichThanhPhanThamGia" name="" placeholder="Thành phần tham gia" class="form-control"><%=string.Join(",", oLichTongCongTy.LichThanhPhanThamGia.Select(x=>x.LookupValue)) %></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="LichGhiChu" class="col-sm-2 control-label">Ghi chú</label>
            <div class="col-sm-10">
                <textarea name="LichGhiChu" id="LichGhiChu" placeholder="Nhập ghi chú" class="form-control"><%=oLichTongCongTy.LichGhiChu%></textarea>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#LichLanhDaoChuTri").smSelect2018V2({
                dropdownParent: "#frm-LichTongCongTy"
            });
            
            $("#Title").focus();
            $(".datetime").daterangepicker({
                timePicker: true,
                singleDatePicker: true,
                timePicker24Hour: true,
                locale: { format: 'DD/MM/YYYY HH:mm' }
            });
            $(".form-group select").select2({
                dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
            });

            $("#frm-LichTongCongTy").validate({
                rules: {
                    Title: "required",
                    LichLanhDaoChuTri: "required",
                    LichThoiGianBatDau: "required",
                    LichThoiGianKetThuc: "required",
                    LichDiaDiem: "required",
                    LichNoiDung: "required",
                },
                messages: {
                    Title: "Vui lòng nhập tên cuộc họp",
                    LichLanhDaoChuTri: "Vui lòng nhập lãnh đạo chủ trì",
                    LichThoiGianBatDau: "Vui lòng nhập ngày bắt đầu",
                    LichThoiGianKetThuc: "Vui lòng nhập ngày kết thúc",
                    LichDiaDiem: "Vui lòng nhập địa điểm",
                    LichNoiDung: "Vui lòng nhập nội dung",
                },

                submitHandler: function (form) {
                    var from = $("#LichThoiGianBatDau").val();
                    var to = $("#LichThoiGianKetThuc").val();

                    if (Date.parse(from) > Date.parse(to)) {
                        BootstrapDialog.show({
                            title: "Lỗi",
                            message: "Dữ liệu ngày bắt đầu nhỏ hơn ngày kết thúc"
                        });
                    }
                    else {
                        $.post("/UserControls/AdminCMS/LichTongCongTy/pAction.ashx", $(form).viSerialize(), function (result) {
                            if (result.State == 2) {
                                BootstrapDialog.show({
                                    title: "Lỗi",
                                    message: result.Message
                                });
                            }
                            else {
                                showMsg(result.Message);
                                //$("#btn-find-LichTongCongTy").trigger("click");
                                $(form).closeModal();
                                $('#tblLichTongCongTy').DataTable().ajax.reload();
                            }
                        }).always(function () { });
                    }
                }
            });
            $("#frm-LichTongCongTy").viForm();
        });
    </script>
</body>
</html>

