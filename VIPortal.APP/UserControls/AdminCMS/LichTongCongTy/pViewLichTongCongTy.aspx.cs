﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.LichTongCongTy;

namespace VIPortal.APP.UserControls.AdminCMS.LichTongCongTy
{
    public partial class pViewLichTongCongTy : pFormBase
    {
        public LichTongCongTyItem oLichTongCongTy { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oLichTongCongTy = new LichTongCongTyItem();
            if (ItemID > 0)
            {
                LichTongCongTyDA oLichTongCongTyDA = new LichTongCongTyDA(UrlSite);
                oLichTongCongTy = oLichTongCongTyDA.GetByIdToObject<LichTongCongTyItem>(ItemID);
            }
        }
    }
}