﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewLichTongCongTy.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.LichTongCongTy.pViewLichTongCongTy" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên</label>
            <div class="col-sm-10">
                <%=oLichTongCongTy.Title%>
            </div>
        </div>
        <div class="form-group row">
	        <label for="LichThoiGianBatDau" class="col-sm-2 control-label">Ngày bắt đầu</label>
            <div class="col-sm-4">
                <%:string.Format("{0:dd-MM-yyyy}", oLichTongCongTy.LichThoiGianBatDau)%>
            </div>
            <label for="LichThoiGianKetThuc" class="col-sm-2 control-label">Ngày kết thúc</label>
            <div class="col-sm-4">
                <%:string.Format("{0:dd-MM-yyyy}",oLichTongCongTy.LichThoiGianKetThuc)%>
            </div>
        </div>
        <div class="form-group row">
            <label for="LichLanhDaoChuTri" class="col-sm-2 control-label">Lãnh đạo chủ trì</label>
            <div class="col-sm-4">
                <%=oLichTongCongTy.LichLanhDaoChuTri.LookupValue%>
            </div>
            <label for="LichTrangThai" class="col-sm-2 control-label">Trạng thái</label>
            <div class="col-sm-4">
                <%=oLichTongCongTy.LichTrangThai%>
            </div>
        </div>
        <div class="form-group row">
	        <label for="LichDiaDiem" class="col-sm-2 control-label">Địa điểm</label>
	        <div class="col-sm-10">
		        <%=oLichTongCongTy.LichDiaDiem%>
	        </div>
        </div>
        <div class="form-group row">
	        <label for="LichNoiDung" class="col-sm-2 control-label">Nội dung</label>
	        <div class="col-sm-10">
		       <%=oLichTongCongTy.LichNoiDung%>
	        </div>
        </div>
        <div class="form-group row">
	        <label for="LichThanhPhanThamGia" class="col-sm-2 control-label">Thành phần tham gia</label>
	        <div class="col-sm-10">
		        <%=oLichTongCongTy.LichThanhPhanThamGia%>
	        </div>
        </div>
         <div class="form-group row">
	        <label for="LichGhiChu" class="col-sm-2 control-label">Ghi chú</label>
	        <div class="col-sm-10">
		        <%=oLichTongCongTy.LichGhiChu%>
	        </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oLichTongCongTy.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oLichTongCongTy.ListFileAttach[i].Url %>"><%=oLichTongCongTy.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%=string.Format("{0:dd-MM-yyyy}", oLichTongCongTy.Created)%>
            </div>
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}",oLichTongCongTy.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
               <%=oLichTongCongTy.Author.LookupValue%>
            </div>
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
               <%=oLichTongCongTy.Editor.LookupValue%>
            </div>    
        </div>
    </form>
</body>
</html>
