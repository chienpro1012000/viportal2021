﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.HoiDap;

namespace VIPortal.APP.UserControls.AdminCMS.HoiDap
{
    public partial class pViewHoiDap :pFormBase
    {
        public HoiDapItem oHoiDap = new HoiDapItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oHoiDap = new HoiDapItem();
            if (ItemID > 0)
            {
                HoiDapDA oFQAThuongGapDA = new HoiDapDA(UrlSite,false);
                oHoiDap = oFQAThuongGapDA.GetByIdToObject<HoiDapItem>(ItemID);
            }
        }
    }
}