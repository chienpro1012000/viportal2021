﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData.HoiDap;

namespace VIPortal.APP.UserControls.AdminCMS.HoiDap
{

    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            HoiDapDA oHoiDapDA = new HoiDapDA(UrlSite,true);
            HoiDapItem oHoiDapItem = new HoiDapItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oHoiDapDA.GetListJsonSolr(new HoiDapQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oHoiDapDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "HoiDap", "QUERYDATA", 0, "Xem danh sách hỏi đáp");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataHoiDap = oHoiDapDA.GetListJson(new HoiDapQuery(context.Request));
                    treeViewItems = oDataHoiDap.Select(x => new TreeViewItem()
                    {
                        key = x.FAQCauHoi,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;

                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oHoiDapDA.GetListJson(new HoiDapQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oHoiDapItem = oHoiDapDA.GetByIdToObject<HoiDapItem>(oGridRequest.ItemID);
                        oHoiDapItem.UpdateObject(context.Request);
                        oHoiDapDA.UpdateObject<HoiDapItem>(oHoiDapItem);
                        AddLog("UPDATE", "HoiDap", "UPDATE", oGridRequest.ItemID, $"Cập nhật hỏi đáp {oHoiDapItem.Title}");
                    }
                    else
                    {
                        oHoiDapItem.UpdateObject(context.Request);
                        oHoiDapDA.UpdateObject<HoiDapItem>(oHoiDapItem);
                        AddLog("UPDATE", "HoiDap", "ADD", oGridRequest.ItemID, $"Thêm mới hỏi đáp {oHoiDapItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oHoiDapDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "HoiDap", "DELETE", oGridRequest.ItemID, $"Xóa hỏi đáp {outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                   // AddLog("DELETE-MULTI", "HoiDap", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều hỏi đáp");
                    string outbmulti = "";
                    AddLog("DELETE-MULTI", "HoiDap", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều hỏi đáp");
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oHoiDapDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oHoiDapDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "HoiDap", "APPROVED", oGridRequest.ItemID, $"Duyệt hỏi đáp {oHoiDapDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oHoiDapDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "HoiDap", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt hỏi đáp {oHoiDapDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oHoiDapItem = oHoiDapDA.GetByIdToObjectSelectFields<HoiDapItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oHoiDapItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}