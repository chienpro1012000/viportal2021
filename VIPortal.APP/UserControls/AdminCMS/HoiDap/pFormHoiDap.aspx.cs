﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.HoiDap;
using ViPortalData.TraLoiHoiDap;

namespace VIPortal.APP.UserControls.AdminCMS.HoiDap
{
    public partial class pFormHoiDap : pFormBase
    {
        public HoiDapItem oHoiDap { get; set; }
        public TraLoiHoiDapItem oTraLoiHoiDap { get;set;}
        public bool isTraLoi { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            isTraLoi = false;
            oHoiDap = new HoiDapItem();
            HoiDapDA oHoiDapDA = new HoiDapDA(UrlSite,false);
            oTraLoiHoiDap = new TraLoiHoiDapItem();
            if (ItemID > 0)
            {
                oHoiDap = oHoiDapDA.GetByIdToObject<HoiDapItem>(ItemID);
                


            }
            if (!string.IsNullOrEmpty(Context.Request["TraLoi"]))
            {
                isTraLoi = true;
            }
        }
    }
}