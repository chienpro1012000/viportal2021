﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_HoiDap.ascx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.HoiDap.UC_HoiDap" %>
<div role="body-data" data-title="câu hỏi" class="content_wp" data-action="/UserControls/AdminCMS/HoiDap/pAction.asp" data-form="/UserControls/AdminCMS/HoiDap/pFormHoiDap.aspx" data-view="/UserControls/AdminCMS/HoiDap/pViewHoiDap.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="HoiDapSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title,FAQCauHoi" />
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <button class="btn btn-primary act-add" data-per="01030102" type="button">Thêm mới</button>
                    </p>
                </div>

            </div>

            <div class="clsgrid table-responsive">

                <table id="tblHoiDap" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function formatDate(strValue) {
        if (strValue == null) return "";
        //var date = new Date(strValue);
        var d = new Date(strValue);
        var month = d.getMonth() + 1;
        var day = d.getDate();
        var output = //(('' + hour).length < 2 ? '0' : '') + hour + ':' +
            //(('' + minute).length < 2 ? '0' : '') + minute + " ngày " +
            (('' + day).length < 2 ? '0' : '') + day + '/' +
            (('' + month).length < 2 ? '0' : '') + month + '/' +
            d.getFullYear();
        return output;
    }
    function TraLoi(itemid) {
        openDialog("Trả lời câu hỏi", "/UserControls/AdminCMS/HoiDap/pFormHoiDap.aspx", { ItemID: itemid, TraLoi: 1 }, 1024);
    }
    $(document).ready(function () {

        var $tagvalue = $(".content_wp");
        var $tblHoiDap = $("#tblHoiDap").viDataTable(
            {
                "frmSearch": "HoiDapSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "sWidth": "20px",
                            "mData": function (o) {
                                return '';
                            },
                            "name": "STT", "sTitle": "STT", className: "txtcenter"
                        },
                        {
                            "mData": "ID",
                            "name": "ID", "visible": false, "sTitle": "ID",
                            "sWidth": "30px",
                        },{
                            "name": "FAQNgayHoi", "sTitle": "Thời gian hỏi",
                            "mData": function (o) {
                                return formatDate(o.FAQNgayHoi);
                            },
                            "sWidth": "100px",
                        },{
                            "mData": function (o) {
                                return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.FAQCauHoi + '</a>';
                            },
                            "name": "FAQCauHoi", "sTitle": "Câu hỏi"
                        },
                        {
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '" >' + o.FQNguoiHoi.Title + '</a>';
                            },
                            "name": "FQNguoiHoi", "sTitle": "Người hỏi"
                        },
                        {
                            "mData": function (d) {
                                return d.FAQLinhVuc.Title;
                            },
                            "sWidth": "150px",
                            "name": "FAQLinhVuc", "sTitle": "Lĩnh vực"
                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {
                                if (o._ModerationStatus == 0)

                                    return '<a data-per="01030105" title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a data-per="01030105" title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }
                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",

                            "mRender": function (o) { return '<a url="/UserControls/AdminCMS/TraLoiHoiDap/pFormTraLoiHoiDap.aspx" class="btn default btn-xs purple btnfrm" title="Trả lời" data-IDHoiDap="' + o.ID + '" href="javascript:;"><i class="fa fa-reply" aria-hidden="true"></i></a>'; }
                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) {
                                return '<a class="btn default btn-xs purple act-edit" title="Sửa" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>';
                            }
                        }, {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE" title="Xóa"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                "order": [1, 'desc']
            });
        $tblHoiDap.on('order.dt search.dt', function () {
            $tblHoiDap.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    });
</script>
