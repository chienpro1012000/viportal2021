﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormHoiDap.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.HoiDap.pFormHoiDap" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-HoiDap" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oHoiDap.ID %>" />

        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Câu hỏi</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control"  value="<%=oHoiDap.Title%>" placeholder="Nhập tiêu đề câu hỏi" />
            </div>
        </div>
        <div class="form-group row">            
            <label for="FAQLinhVuc" class="col-sm-2 control-label">Lĩnh vực</label>
            <div class="col-sm-10">
                <select data-selected="<%:oHoiDap.FAQLinhVuc.LookupId%>" data-url="/UserControls/AdminCMS/DMLinhVuc/pAction.asp?do=QUERYDATA&moder=1" data-place="Chọn lĩnh vực" name="FAQLinhVuc" id="FAQLinhVuc" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <label for="FQNguoiHoi" class="col-sm-2 control-label">Người hỏi</label>
            <div class="col-sm-4">
                <select data-selected="<%:oHoiDap.FQNguoiHoi.LookupId%>"  data-url="/UserControls/AdminCMS/LUser/pAction.asp?do=QUERYDATA" data-place="Chọn người hỏi" name="FQNguoiHoi" id="FQNguoiHoi" class="form-control"></select>
            </div>
            <label for="FAQNgayHoi" class="col-sm-2 control-label">Ngày hỏi</label>
            <div class="col-sm-4">
                <div class="">
                    <input type="date" name="FAQNgayHoi" id="FAQNgayHoi" value="<%:string.Format("{0:yyyy-MM-dd}", oHoiDap.FAQNgayHoi)%>" class="form-control" />
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="FAQCauHoi" class="col-sm-2 control-label">Nội dung câu hỏi</label>
            <div class="col-sm-10">

                <textarea name="FAQCauHoi" id="FAQCauHoi"  class="form-control"><%=oHoiDap.FAQCauHoi%></textarea>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#FQNguoiHoi").smSelect2018V2({
                dropdownParent: "#frm-HoiDap",
                Ajax: true
            });
            $("#FAQLinhVuc").smSelect2018V2({
                dropdownParent: "#frm-HoiDap"
            });
            $("#FAQCauHoi").viCustomFck();
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                timePicker: true,
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY HH:mm' }
            });
            

            $("#frm-HoiDap").validate({
                rules: {
                    Title: "required",
                    TraLoiHoiDap_Title: "required",
                    FQNguoiHoi: "required",
                    FAQCauHoi: "required",
                    FAQLinhVuc: "required",

                },
                messages: {
                    Title: "Vui lòng nhập câu hỏi",
                    TraLoiHoiDap_Title: "Vui lòng nhập tiêu đề",
                    FQNguoiHoi: "Vui lòng chọn người hỏi",
                    FAQCauHoi: "Vui lòng nhập nội dung câu hỏi",
                    FAQLinhVuc: "Chọn lĩnh vực",
                },

                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/HoiDap/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-HoiDap").trigger("click");
                            $(form).closeModal();
                            $('#tblHoiDap').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-HoiDap").viForm();
        });
    </script>
</body>
</html>

