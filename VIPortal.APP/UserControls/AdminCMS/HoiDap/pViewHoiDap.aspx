﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewHoiDap.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.HoiDap.pViewHoiDap" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Câu hỏi</label>
            <div class="col-sm-10">
                <%=oHoiDap.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="FAQSTT" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-4">
                   <%:(oHoiDap!= null &&  oHoiDap.FAQSTT>0)?oHoiDap.FAQSTT+"": "" %>
            </div>
            <label for="FAQLuotXem" class="col-sm-2 control-label">Lượt xem</label>
            <div class="col-sm-4">
                <div class="">
                    <%=(oHoiDap!= null &&  oHoiDap.FAQLuotXem>0)?oHoiDap.FAQLuotXem+"": "" %>
                </div>
            </div>
        </div>
        
         <div class="form-group row">
            <label for="FQNguoiHoi" class="col-sm-2 control-label">Người hỏi</label>
            <div class="col-sm-4">
               <%:oHoiDap.FQNguoiHoi.LookupId%>
            </div>
            <label for="FAQNgayHoi" class="col-sm-2 control-label">Ngày hỏi</label>
            <div class="col-sm-4">
                <div class="">
                     <%:string.Format("{0:dd-MM-yyyy}", oHoiDap.FAQNgayHoi)%>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="FAQCauHoi" class="col-sm-2 control-label">Nội dung câu hỏi</label>
            <div class="col-sm-10">
                <%=oHoiDap.FAQCauHoi%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oHoiDap.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oHoiDap.ListFileAttach[i].Url %>"><%=oHoiDap.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%=string.Format("{0:dd-MM-yyyy}", oHoiDap.Created)%>
            </div>
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}",oHoiDap.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
                <%=oHoiDap.Author.LookupValue%>
            </div>
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
                <%=oHoiDap.Editor.LookupValue%>
            </div>
        </div>        
        <div class="clearfix"></div>
    </form>
</body>
</html>