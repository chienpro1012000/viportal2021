﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pListKSCauHoi.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.KSBoCauHoi.pListKSCauHoi" %>

<style>
    #tblKSBoCauHoi_length {
        display: none !important;
    }

    #tblKSBoCauHoi_info {
        display: none !important;
    }

    #tblKSBoCauHoi_paginate {
        display: none !important;
    }

</style>
<div role="body-data" data-title="bộ câu hỏi" class="content_wp1" data-action="/UserControls/AdminCMS/KSBoCauHoi/pAction.asp" data-form="/UserControls/AdminCMS/KSBoCauHoi/pFormKSBoCauHoi.aspx" data-view="/UserControls/AdminCMS/KSBoCauHoi/pViewKSBoCauHoi.aspx">
    <div class="clsmanager row">
        <div class="col-sm-9">
            <div id="KSBoCauHoiSearch" class="form-inline zonesearch">
                <div class="form-group">
                    <input type="hidden" name="do" id="do" value="QUERYDATA" />
                    <input type="hidden" name="IdChuDe" id="IdChuDe" value="<%=ItemID %>" />
                    <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title" />
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <p class="text-right">
                <button class="btn btn-primary" id="btnThemMoi" type="button">Thêm mới</button>
            </p>
        </div>
    </div>

    <div class="clsgrid table-responsive">

        <table id="tblKSBoCauHoi" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
            
        </table>

    </div>
</div>
<script type="text/javascript">
    $("#btnThemMoi").on("click", function (event) {
        var data = {
            do: "UPDATE",
            ItemChuDe: <%=ItemID %>
            //UrlSite: $urlSite
        };
        openDialog("Thêm mới câu hỏi", "/UserControls/AdminCMS/KSBoCauHoi/pFormKSBoCauHoi.aspx", data, 798);
        event.stopPropagation();
    });
    $(document).ready(function () {

        var $tagvalue = $(".content_wp1");
        var $tblDanhMucChung = $("#tblKSBoCauHoi").viDataTable(
            {
                "frmSearch": "KSBoCauHoiSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        //{
                        //    "mData": function (o) {
                        //        return '<a data-id="' + o.ID + '" class="act-view" href="javascript:;">' + o.DMSTT + '</a>';
                        //    },
                        //    "name": "DMSTT", "sTitle": "Số thứ tự"
                        //},
                        {
                            "mData": function (o) {
                                return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                            },
                            "name": "Title", "sTitle": "Tiêu đề câu hỏi"
                        }, 
                        {
                            "mData": function (o) {
                                if(o.LoaiTraLoi = 1)
                                    return 'Chọn một';
                                else
                                    return 'Chọn nhiều'
                            },
                            "name": "LoaiTraLoi", "sTitle": "Loại trả lời"
                        }, 
                        {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" title="Sửa" data-ItemID="' + o.ID + '" data-ItemChuDe="' + <%=ItemID%> + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        }, {
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE" title="Xóa"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }
                    ],
                "aaSorting": [0, 'asc']
            });
    });
</script>
