﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.KSBoCauHoi;
using ViPortalData.KSBoChuDe;
using ViPortalData.KSCauTraLoi;

namespace VIPortal.APP.UserControls.AdminCMS.KSBoCauHoi
{
    public partial class pFormKSBoCauHoi : pFormBase
    {
        public KSBoCauHoiItem oKSBoCauHoi { get; set; }
        public KSBoChuDeItem oKSBoChuDe { get; set; }
        public List<KSCauTraLoiJson> lstCTL { get; set; }
        public int ItemChuDe { get; set; }
        public string lstIDGet { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            ItemChuDe = Convert.ToInt32(Request["ItemChuDe"]);
            if (ItemChuDe > 0)
            {
                KSBoChuDeDA oKSBoChuDeDA = new KSBoChuDeDA(UrlSite);
                oKSBoChuDe = oKSBoChuDeDA.GetByIdToObject<KSBoChuDeItem>(ItemChuDe);
            }
            oKSBoCauHoi = new KSBoCauHoiItem();
            if (ItemID > 0)
            {
                KSBoCauHoiDA oKSBoCauHoiDA = new KSBoCauHoiDA(UrlSite);
                oKSBoCauHoi = oKSBoCauHoiDA.GetByIdToObject<KSBoCauHoiItem>(ItemID);
                KSCauTraLoiDA oKSCauTraLoiDA = new KSCauTraLoiDA(UrlSite);
                lstCTL = oKSCauTraLoiDA.GetListJson(new KSCauTraLoiQuery() { IDCauHoi = ItemID });
                lstIDGet = "";
                foreach (var item in lstCTL)
                {
                    if (lstIDGet == "")
                    {
                        lstIDGet = Convert.ToString(item.ID);
                    }
                    else
                    {
                        lstIDGet += "," + Convert.ToString(item.ID);
                    }
                }
            }
        }
    }
}