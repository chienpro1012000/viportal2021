﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormKSBoCauHoi.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.KSBoCauHoi.pFormKSBoCauHoi" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-KSBoCauHoi" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oKSBoCauHoi.ID %>" />
        <input type="hidden" name="do" id="doaction" value="<%=doAction %>" />
        <input type="hidden" name="IdNews" id="IdNews" value="" />
        <%if(ItemChuDe > 0){%>
        <input type="hidden" name="ChuDeId" id="ChuDeId" value="<%=oKSBoChuDe.ID %>" />
        <div class="form-group row">
            <label for="KSBoChuDe" class="col-sm-2 control-label">Bộ chủ đề</label>
            <div class="col-sm-10">
                <%=oKSBoChuDe.Title %>
            </div>
        </div>
        <%}else{%>
        <%--<input type="hidden" name="ChuDeId" id="ChuDeId" value="<%=oKSBoCauHoi.KSBoChuDe.LookupId %>" />--%>
        <div class="form-group row">
            <label for="KSBoChuDe" class="col-sm-2 control-label">Bộ chủ đề</label>
            <div class="col-sm-10">
                <select data-selected="<%:oKSBoCauHoi.KSBoChuDe.LookupId%>" data-url="/UserControls/AdminCMS/KSBoChuDe/pAction.asp?do=QUERYDATA" data-place="Chọn chủ đề" name="KSBoChuDe" id="KSBoChuDe" class="form-control"></select>
            </div>
        </div>
        <%} %>
        <div class="form-group row">
            <%--<label for="HoTen" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-4">
                <input type="text" name="DMSTT" id="DMSTT" placeholder="Nhập số thứ tự" value="<%:(oKSBoCauHoi!= null &&  oKSBoCauHoi.DMSTT>0)?oKSBoCauHoi.DMSTT+"": "" %>" class="form-control" />
            </div>--%>
            <label for="LoaiTraLoi" class="col-sm-2 control-label">Loại trả lời</label>
            <div class="col-sm-10">
                <select class="form-control" id="LoaiTraLoi" name="LoaiTraLoi">
                    <option value="1">Chọn loại trả lời</option>
                    <option value="1" <%:oKSBoCauHoi.LoaiTraLoi == 1 ? "selected" : "" %>>Chọn một</option>
                    <option value="2" <%:oKSBoCauHoi.LoaiTraLoi == 2 ? "selected" : "" %>>Chọn nhiều</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Câu hỏi</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" placeholder="Nhập câu hỏi" class="form-control" value="<%=oKSBoCauHoi.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="MaSV" class="col-sm-2 control-label">Chi tiết</label>
            <div class="col-sm-10">
                <textarea name="DMDescription" id="DMDescription" placeholder="Nhập chi tiết câu hỏi" class="form-control"><%:oKSBoCauHoi.DMDescription%></textarea>

            </div>
        </div>
        <div class="form-group row">            
        </div>
        <div class="form-group row">
            <div id="frmCauTraloi" style="width: 100%"></div>
        </div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            var lstget;
            if ($("#IdNews").val() != "")
                lstget = $("#IdNews").val();
            else lstget = 0;
            //loadAjaxContent("/UserControls/AdminCMS/KSCauTraLoi/pListKSCauTraLoi.aspx?lstIDGet=" + $("#IdNews").val(), "#frmCauTraloi", {});
            if ( <%=ItemID %> > 0) {
                loadAjaxContent("/UserControls/AdminCMS/KSCauTraLoi/pListKSCauTraLoi.aspx?lstIdGet=" + '<%=lstIDGet%>', "#frmCauTraloi", {});
            } else {
                loadAjaxContent("/UserControls/AdminCMS/KSCauTraLoi/pListKSCauTraLoi.aspx?lstIdGet=" + lstget, "#frmCauTraloi", {});
            }
            $("#KSBoChuDe").smSelect2018V2({
                dropdownParent: "#frm-KSBoCauHoi"
            });
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oKSBoCauHoi.ListFileAttach)%>'
            });
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $(".form-group select").select2({
                dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
            });
            $("#frm-KSBoCauHoi").validate({
                rules: {
                    Title: "required",
                    LoaiTraLoi: "required",
                    KSBoChuDe: "required"
                },
                messages: {
                    Title: "Vui lòng nhập câu hỏi",
                    LoaiTraLoi: "Vui lòng chọn loại trả lời",
                    KSBoChuDe: "Vui lòng lựa chọn chủ đề",
                },
                submitHandler: function (form) {
                    var idChuDe;
                    if ( <%=ItemChuDe %> > 0) {
                        idChuDe = $("#ChuDeId").val();
                    } else {
                        idChuDe = $("#KSBoChuDe").val();
                    }
                    var idNewsSave = $("#IdNewsSave").val();
                    debugger;
                    if (idNewsSave != "0") {
                        $.post("/UserControls/AdminCMS/KSBoCauHoi/pAction.asp", "&do=" + $("#doaction").val() + "&lstIDGet=" + $("#IdNewsSave").val() + "&ItemID=" + $("#ItemID").val() + "&KSBoChuDe=" + idChuDe  + "&Title=" + $("#Title").val() + "&DMDescription=" + $("#DMDescription").val() + "&LoaiTraLoi=" + $("#LoaiTraLoi").val(), function (result) {
                            if (result.State == 2) {
                                BootstrapDialog.show({
                                    title: "Lỗi",
                                    message: result.Message
                                });
                            }
                            else {
                                showMsg(result.Message);
                                $("#btn-find-KSBoCauHoi").trigger("click");
                                $(form).closeModal();
                                $('#tblKSBoCauHoi').DataTable().ajax.reload();
                            }
                        }).always(function () { });
                    } else {
                        alert("Vui lòng nhập câu trả lời!");
                    }
                    
                }
            });
            $("#frm-KSBoCauHoi").viForm();
        });
    </script>
</body>
</html>

