﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.KSBoCauHoi;
using ViPortalData.KSBoChuDe;

namespace VIPortal.APP.UserControls.AdminCMS.KSBoCauHoi
{
    public partial class pViewKSBoCauHoi : pFormBase
    {
        public KSBoCauHoiItem oKSBoCauHoi = new KSBoCauHoiItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oKSBoCauHoi = new KSBoCauHoiItem();
            if (ItemID > 0)
            {
                KSBoCauHoiDA oFQAThuongGapDA = new KSBoCauHoiDA(UrlSite);
                oKSBoCauHoi = oFQAThuongGapDA.GetByIdToObject<KSBoCauHoiItem>(ItemID);
            }
        }
    }
}