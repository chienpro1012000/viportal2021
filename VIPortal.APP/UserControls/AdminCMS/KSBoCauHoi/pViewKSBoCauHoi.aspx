﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewKSBoCauHoi.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.KSBoCauHoi.pViewKSBoCauHoi" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group row">
            <label for="LookUp" class="col-sm-2 control-label">Bộ chủ đề</label>
            <div class="col-sm-10">
                <%:oKSBoCauHoi.KSBoChuDe.LookupValue%>
            </div>
        </div>
          <div class="form-group row">
            <label for="HoTen" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-4">
                  <%:oKSBoCauHoi.DMSTT %>
            </div>
              <label class="control-label col-sm-2">Hiển thị</label>
               <div class="col-sm-4">
                    <%:oKSBoCauHoi.DMHienThi?"Có":"Không" %>
                </div>
        </div>
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Câu hỏi</label>
            <div class="col-sm-10">
                <%=oKSBoCauHoi.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="MaSV" class="col-sm-2 control-label">Chi tiết</label>
            <div class="col-sm-10">
                <%:oKSBoCauHoi.DMDescription%>
               
            </div>
        </div>
        <div class="form-group row">
            <label for="MaLop" class="col-sm-2 control-label">Hình thức trả lời</label>
            <div class="col-sm-10">
                
                <%:(oKSBoCauHoi!= null &&  oKSBoCauHoi.KSHinhThucTraLoi>0)?oKSBoCauHoi.KSHinhThucTraLoi+"": "" %>
            </div>
        </div>
         <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oKSBoCauHoi.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oKSBoCauHoi.ListFileAttach[i].Url %>"><%=oKSBoCauHoi.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
		  <div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%=string.Format("{0:dd-MM-yyyy}", oKSBoCauHoi.Created)%>
            </div>
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}",oKSBoCauHoi.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
                <%=oKSBoCauHoi.Author.LookupValue%>
            </div>
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
                <%=oKSBoCauHoi.Editor.LookupValue%>
            </div>
        </div>     
        <div class="clearfix"></div>
    </form>
</body>
</html>