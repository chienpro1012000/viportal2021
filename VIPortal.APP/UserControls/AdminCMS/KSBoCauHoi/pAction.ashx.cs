﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData.KSBoCauHoi;
using ViPortalData.KSCauTraLoi;

namespace VIPortal.APP.UserControls.AdminCMS.KSBoCauHoi
{

    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);
            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            KSCauTraLoiQuery oQueryCTL = new KSCauTraLoiQuery();
            KSBoCauHoiDA oKSBoCauHoiDA = new KSBoCauHoiDA(UrlSite);
            KSCauTraLoiDA oKSCauTraLoiDA = new KSCauTraLoiDA(UrlSite);
            KSBoCauHoiItem oKSBoCauHoiItem = new KSBoCauHoiItem();
            KSCauTraLoiItem oKSCauTraLoiItem = new KSCauTraLoiItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    var oData = oKSBoCauHoiDA.GetListJson(new KSBoCauHoiQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oKSBoCauHoiDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "KSBoCauHoi", "QUERYDATA", 0, "Xem danh sách danh mục video");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataKSBoCauHoi = oKSBoCauHoiDA.GetListJson(new KSBoCauHoiQuery(context.Request));
                    treeViewItems = oDataKSBoCauHoi.Select(x => new TreeViewItem()
                    {
                        key = x.DMDescription,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;

                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oKSBoCauHoiDA.GetListJson(new KSBoCauHoiQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        string lstIdNews = context.Request["IdNews"];
                        if (!string.IsNullOrEmpty(context.Request["ChuDeId"]))
                        {
                            int ChuDeId = Convert.ToInt32(context.Request["ChuDeId"]);
                            oKSBoCauHoiItem.KSBoChuDe = new Microsoft.SharePoint.SPFieldLookupValue(ChuDeId, "");
                        }  
                        List<KSCauTraLoiJson> lstCauTraLoi = new List<KSCauTraLoiJson>();
                        oQueryCTL.lstIdGet = GetDanhSachIDsQuaFormPost(lstIdNews);
                        lstCauTraLoi = oKSCauTraLoiDA.GetListJson(oQueryCTL);
                        ///luu cau hoi
                        oKSBoCauHoiItem = oKSBoCauHoiDA.GetByIdToObject<KSBoCauHoiItem>(oGridRequest.ItemID);
                        oKSBoCauHoiItem.UpdateObject(context.Request);
                        oKSBoCauHoiDA.UpdateObject<KSBoCauHoiItem>(oKSBoCauHoiItem);
                        AddLog("UPDATE", "KSBoCauHoi", "UPDATE", oGridRequest.ItemID, $"Cập nhật danh mục video{oKSBoCauHoiItem.Title}");
                        ///cap nhat id cau hoi
                        foreach (var item in lstCauTraLoi)
                        {
                            oKSCauTraLoiItem = oKSCauTraLoiDA.GetByIdToObject<KSCauTraLoiItem>(item.ID);
                            oKSCauTraLoiItem.KSCauHoi = new Microsoft.SharePoint.SPFieldLookupValue(oKSBoCauHoiItem.ID, "");
                            oKSCauTraLoiDA.UpdateObject<KSCauTraLoiItem>(oKSCauTraLoiItem);
                        }                       
                    }
                    else
                    {
                        string lstIdNews = context.Request["lstIDGet"];
                        if (!string.IsNullOrEmpty(context.Request["ChuDeId"]))
                        {
                            int ChuDeId = Convert.ToInt32(context.Request["ChuDeId"]);
                            oKSBoCauHoiItem.KSBoChuDe = new Microsoft.SharePoint.SPFieldLookupValue(ChuDeId, "");
                        }
                        List<KSCauTraLoiJson> lstCauTraLoi = new List<KSCauTraLoiJson>();
                        oQueryCTL.lstIdGet = GetDanhSachIDsQuaFormPost(lstIdNews);
                        lstCauTraLoi = oKSCauTraLoiDA.GetListJson(oQueryCTL);
                        ///luu cau hỏi
                        oKSBoCauHoiItem.UpdateObject(context.Request);                        
                        oKSBoCauHoiDA.UpdateObject<KSBoCauHoiItem>(oKSBoCauHoiItem);
                        AddLog("UPDATE", "KSBoCauHoi", "ADD", oGridRequest.ItemID, $"Thêm mới khảo sát bộ câu hỏi{oKSBoCauHoiItem.Title}");
                        ///cap nhat id cau hoi
                        foreach (var item in lstCauTraLoi)
                        {
                            oKSCauTraLoiItem = oKSCauTraLoiDA.GetByIdToObject<KSCauTraLoiItem>(item.ID);
                            oKSCauTraLoiItem.KSCauHoi = new Microsoft.SharePoint.SPFieldLookupValue(oKSBoCauHoiItem.ID, "");
                            oKSCauTraLoiDA.UpdateObject<KSCauTraLoiItem>(oKSCauTraLoiItem);
                        }
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oKSBoCauHoiDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "KSBoCauHoi", "DELETE", oGridRequest.ItemID, $"Xóa khảo sát bộ câu hỏi{outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oKSBoCauHoiDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    AddLog("DELETE-MULTI", "KSBoCauHoi", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều danh mục video");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oKSBoCauHoiDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "KSBoCauHoi", "APPROVED", oGridRequest.ItemID, $"Duyệt khảo sát bộ câu hỏi{oKSBoCauHoiDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oKSBoCauHoiDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "KSBoCauHoi", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt khảo sát bộ câu hỏi{oKSBoCauHoiDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oKSBoCauHoiItem = oKSBoCauHoiDA.GetByIdToObjectSelectFields<KSBoCauHoiItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oKSBoCauHoiItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        public static List<int> GetDanhSachIDsQuaFormPost(string arrID)
        {
            List<int> dsID = new List<int>();
            if (!string.IsNullOrEmpty(arrID))
            {
                string[] tempIDs = arrID.Split(',');
                foreach (string idConvert in tempIDs)
                {
                    int _id = 0;
                    if (int.TryParse(idConvert, out _id))
                    {
                        //if (_id > 0)
                        if (!dsID.Contains(_id))
                            dsID.Add(_id);
                    }
                }
            }
            return dsID;
        }
    }
}