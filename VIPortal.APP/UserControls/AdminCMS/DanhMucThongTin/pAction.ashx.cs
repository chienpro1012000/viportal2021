using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;

namespace VIPortalAPP.DanhMucThongTin
{
    /// <summary> 
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public string fullPath { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);
            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            DanhMucThongTinDA oDanhMucThongTinDA = new DanhMucThongTinDA(UrlSite);
            DanhMucThongTinItem oDanhMucThongTinItem = new DanhMucThongTinItem();
            fullPath = SPContext.Current.Site.Url;
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oDanhMucThongTinDA.GetListJsonSolr(new DanhMucThongTinQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw, 
                        oDanhMucThongTinDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "DanhMucThongTin", "QUERYDATA", 0, "Xem danh sách danh mục thông tin");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataDanhMuc = oDanhMucThongTinDA.GetListJson(new DanhMucThongTinQuery(context.Request));
                    treeViewItems = oDataDanhMuc.Select(x => new TreeViewItem()
                    {
                        key = x.UrlList,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oDanhMucThongTinDA.GetListJson(new DanhMucThongTinQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDanhMucThongTinItem = oDanhMucThongTinDA.GetByIdToObject<DanhMucThongTinItem>(oGridRequest.ItemID);
                        oDanhMucThongTinItem.UpdateObject(context.Request);
                        if (!string.IsNullOrEmpty(oDanhMucThongTinItem.Title) && oDanhMucThongTinDA.GetCount(new DanhMucThongTinQuery() { Title = oDanhMucThongTinItem.Title,  ItemIDNotGet = oDanhMucThongTinItem.ID }) > 0)
                        {
                            oResult = new ResultAction() { State = ActionState.Error, Message = "Bản ghi Tiêu đề đã tồn tại" };
                            break;
                        }
                        if (!string.IsNullOrEmpty(oDanhMucThongTinItem.Title) && oDanhMucThongTinDA.GetCount(new DanhMucThongTinQuery() { UrlList = oDanhMucThongTinItem.UrlList, ItemIDNotGet = oDanhMucThongTinItem.ID }) > 0)
                        {
                            oResult = new ResultAction() { State = ActionState.Error, Message = "Bản ghi UrlList đã tồn tại" };
                            break;
                        }
                        EditListToSharepoint(oDanhMucThongTinItem.Title, oDanhMucThongTinItem.UrlList, UrlSite);
                        oDanhMucThongTinDA.UpdateObject<DanhMucThongTinItem>(oDanhMucThongTinItem);
                        AddLog("UPDATE", "DanhMucThongTin", "UPDATE", oGridRequest.ItemID, $"Cập nhật danh mục thông tin{oDanhMucThongTinItem.Title}");

                    }
                    else
                    {
                        oDanhMucThongTinItem.UpdateObject(context.Request);
                        //int a = oDanhMucThongTinDA.GetCount(new DanhMucThongTinQuery() { Title = oDanhMucThongTinItem.Title });
                        if (!string.IsNullOrEmpty(oDanhMucThongTinItem.Title) && oDanhMucThongTinDA.GetCount(new DanhMucThongTinQuery() { Title = oDanhMucThongTinItem.Title}) > 0 || oDanhMucThongTinDA.GetCount(new DanhMucThongTinQuery() {UrlList = oDanhMucThongTinItem.UrlList }) > 0)
                        {
                            oResult = new ResultAction() { State = ActionState.Error, Message = "Bản ghi đã tồn tại" };
                            break;
                        }
                        oDanhMucThongTinDA.UpdateObject<DanhMucThongTinItem>(oDanhMucThongTinItem);
                        CreateListToSharepoint(oDanhMucThongTinItem.Title, oDanhMucThongTinItem.UrlList, "TinTuc", UrlSite);
                        AddLog("UPDATE", "DanhMucThongTin", "ADD", oGridRequest.ItemID, $"Thêm mới danh mục thông tin{oDanhMucThongTinItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oDanhMucThongTinDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "DanhMucThongTin", "DELETE", oGridRequest.ItemID, $"Xóa danh mục thông tin {outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    var outbmulti = "";
                    AddLog("DELETE-MULTI", "DanhMucThongTin", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều danh mục thông tin");
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oDanhMucThongTinDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oDanhMucThongTinDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "DanhMucThongTin", "APPROVED", oGridRequest.ItemID, $"Duyệt danh mục thông tin{oDanhMucThongTinDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                     oDanhMucThongTinDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "DanhMucThongTin", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt danh mục thông tin{oDanhMucThongTinDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDanhMucThongTinItem = oDanhMucThongTinDA.GetByIdToObjectSelectFields<DanhMucThongTinItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oDanhMucThongTinItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        /// <summary>
        /// Tạo List với teamplate Tin tức 
        /// </summary>
        /// <param name="urllist"></param>
        /// <param name="contentType"></param>
        private void CreateListToSharepoint(string title, string titleRemove, string contentType, string UrlSite = null)
        {
            SPSecurity.RunWithElevatedPrivileges(delegate
            {
                using (var curSite = new SPSite(fullPath))
                {
                    using (var curWebRoot = curSite.OpenWeb())
                    {
                        SPListTemplateCollection listTemplates = curWebRoot.Site.GetCustomListTemplates(curWebRoot);
                        SPListTemplate template = listTemplates[contentType];

                        //HttpContext.Current.Request.Url.AbsolutePath.Split()
                        UrlSite = !string.IsNullOrEmpty(UrlSite) ? UrlSite : "";

                        using (var curWebDonVi = curSite.OpenWeb(GetFullURL(UrlSite + "/noidung")))
                        {
                            try
                            {
                                List<string> lstTitle = checkLists(curWebDonVi);

                                bool hasName = false;
                                int index = 1;
                                while (hasName)
                                {
                                    hasName = lstTitle.Contains(titleRemove);
                                    index++;
                                }

                                #region tạo list
                                curWebDonVi.AllowUnsafeUpdates = true;
                                if (hasName == false)
                                {
                                    Guid g = curWebDonVi.Lists.Add(titleRemove, string.Empty, template);
                                    SPList listDataType = curWebDonVi.Lists[g];
                                    listDataType.Title = title;
                                    listDataType.Update();
                                }
                                else
                                {
                                    Guid g = curWebDonVi.Lists.Add(titleRemove + index, string.Empty, template);
                                    SPList listDataType = curWebDonVi.Lists[g];
                                    listDataType.Title = title;
                                    listDataType.Update();
                                }
                                #endregion
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                    }
                }
            });
        }

        /// <summary>
        /// Sửa tiêu đề của list dữ liệu
        /// </summary>
        /// <param name="urllist"></param>
        /// <param name="contentType"></param>
        private void EditListToSharepoint(string title, string titleRemove, string UrlSite = "")
        {
            string urlsite = SPContext.Current.Site.Url;
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                using (var curSite = new SPSite(urlsite))
                {
                    using (var curWebDonVi = curSite.OpenWeb(GetFullURL(UrlSite + "/noidung")))
                    {
                        try
                        {
                            curWebDonVi.AllowUnsafeUpdates = true;
                            SPList list = curWebDonVi.GetList(GetFullURL(UrlSite + "/noidung/Lists/") + titleRemove);
                            list.Title = title;
                            list.Update();
                        }
                        catch (Exception ex)
                        {
                            //htRespon.Write(ex.Message);
                        }
                    }
                }
            });
        }
        private List<string> checkLists(SPWeb spweb)
        {
            List<string> tList = new List<string>();
            string titleList;
            foreach (SPList list in spweb.Lists)
            {
                titleList = list.Title;
                tList.Add(titleList);
            }

            return tList;
        }
    }
}