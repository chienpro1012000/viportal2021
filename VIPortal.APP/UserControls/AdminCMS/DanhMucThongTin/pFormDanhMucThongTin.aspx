<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormDanhMucThongTin.aspx.cs" Inherits="VIPortalAPP.pFormDanhMucThongTin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-DanhMucThongTin" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oDanhMucThongTin.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề danh mục</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" placeholder="Nhập tiêu đề" value="<%=oDanhMucThongTin.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="UrlList" class="col-sm-2 control-label">Tên bảng dữ liệu</label>
            <div class="col-sm-10">
                <input type="text" name="UrlList" id="UrlList" placeholder="Nhập Tên bảng, viết liền không dấu" value="<%:oDanhMucThongTin.UrlList%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="DBPhanLoai" class="col-sm-2 control-label">Phân loại dữ liệu</label>
            <div class="col-sm-10">
                <select name="DBPhanLoai" id="DBPhanLoai" class="form-control">
                    <option value="2">Tin điểm báo</option>
                    <option value="1">Giới thiệu</option>
                    <option value="0">Tin tức sự kiện</option>
                </select>
            </div>
        </div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#DBPhanLoai").val('<%:oDanhMucThongTin.DBPhanLoai%>');
            $("#Title").focus();
          
            $("#frm-DanhMucThongTin").validate({
                rules: {
                    Title: "required",
                    UrlList: { 
                        specialChars: true, required: true,
                    },
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề",
                    UrlList: "Vui lòng nhập đường dẫn dữ liệu không chứa dấu cách và ký tự đặc biệt"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/DanhMucThongTin/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-DanhMucThongTin").trigger("click");
                            $(form).closeModal();
                            $('#tblDanhMucThongTin').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-DanhMucThongTin").viForm();
        });
    </script>
</body>
</html>
