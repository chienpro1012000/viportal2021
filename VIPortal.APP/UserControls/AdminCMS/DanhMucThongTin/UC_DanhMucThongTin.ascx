<%@ control language="C#" autoeventwireup="true" codebehind="UC_DanhMucThongTin.aspx.cs" inherits="VIPortalAPP.UC_DanhMucThongTin" %>
<style>
    .all{
        vertical-align: middle !important;
    }
    .table thead th{
        vertical-align: middle !important;
    }
</style>
<div id="GridDanhMucThongTin" role="body-data" data-title="danh mục tin tức" class="content_wp" data-action="/UserControls/AdminCMS/DanhMucThongTin/pAction.asp" data-form="/UserControls/AdminCMS/DanhMucThongTin/pFormDanhMucThongTin.aspx" data-view="/UserControls/AdminCMS/DanhMucThongTin/pViewDanhMucThongTin.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body"> 
            <div class="clsmanager row">
                <div class="col-sm-9">

                    <div id="DanhMucThongTinSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa" id="TimKiem">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Titles,UrlList" />
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <button class="btn btn-primary act-add" data-per="020102" type="button">Thêm mới</button>
                    </p>
                </div>
            </div>
            <div class="clsgrid table-responsive">
                <table id="tblDanhMucThongTin" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>
            </div>
        </div>

    </div>
</div>
<script type="text/javascript">

    $(document).ready(function () {
        var $tagvalue = $(".content_wp");
        var $tblDanhMucChung = $("#tblDanhMucThongTin").viDataTable(
            {
                "frmSearch": "DanhMucThongTinSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "mData": function (o) {
                                return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                            },
                            "name": "Title", "sTitle": "Tiêu đề"
                        },
                        {
                            "data": "UrlList",
                            "name": "UrlList", "sTitle": "Đường dẫn dữ liệu",
                            "sWidth": "150px",
                        }, 
                        {
                            "mData": function (o) {
                                if (o.DBPhanLoai == null || o.DBPhanLoai == 0)
                                    return 'Tin tức';
                                else if (o.DBPhanLoai == 1)
                                    return 'Giới thiệu';
                                else if (o.DBPhanLoai == 2)
                                    return 'Tin điểm báo';
                                else return 'Tin tức';
                            },
                            "name": "DBPhanLoai", "sTitle": "Nhóm Dữ liệu"
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {
                                //console.log(o);
                                if (o._ModerationStatus == 0)

                                    return '<a data-per="020105" title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a data-per="020105" title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        }, {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" data-do="DELETE-MULTI" href="javascript:;">  <i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete"data-do="DELETE" data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></i></a>';; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                "aaSorting": [0, 'asc']
            });

        $("#GridDanhMucThongTin").CheckPer();
    });
</script>
