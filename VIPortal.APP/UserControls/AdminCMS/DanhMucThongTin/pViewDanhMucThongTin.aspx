<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewDanhMucThongTin.aspx.cs" Inherits="VIPortalAPP.pViewDanhMucThongTin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
               <%=oDanhMucThongTin.Title%>
            </div>
        </div> 
		<div class="form-group row">
	        <label for="UrlList" class="col-sm-2 control-label">Đường dẫn dữ liệu</label>
	        <div class="col-sm-10">
		        <%=oDanhMucThongTin.UrlList%>
	        </div>
        </div>
            <div class="form-group row">
	            <label for="TrangThai" class="col-sm-2 control-label">Trạng thái</label>
	            <div class="col-sm-10">                    
		            <%if(oDanhMucThongTin._ModerationStatus == 0) {%>Đã duyệt <%}else{%>Chưa duyệt <%}%>
	            </div>
            </div>

		  <div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%=string.Format("{0:dd/MM/yyyy}", oDanhMucThongTin.Created)%>
            </div>
              <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy}",oDanhMucThongTin.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
                <%=oDanhMucThongTin.Author.LookupValue%>
            </div>
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
                <%=oDanhMucThongTin.Editor.LookupValue%>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>