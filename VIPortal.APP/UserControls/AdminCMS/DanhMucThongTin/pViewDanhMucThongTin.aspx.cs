using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public partial class pViewDanhMucThongTin : pFormBase
    {
        public DanhMucThongTinItem oDanhMucThongTin = new DanhMucThongTinItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oDanhMucThongTin = new DanhMucThongTinItem();
            if (ItemID > 0)
            {
                DanhMucThongTinDA oDanhMucThongTinDA = new DanhMucThongTinDA(UrlSite);
                oDanhMucThongTin = oDanhMucThongTinDA.GetByIdToObject<DanhMucThongTinItem>(ItemID);

            }
        }
    }
}