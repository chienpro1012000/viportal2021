<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewCommentMXH.aspx.cs" Inherits="VIPortalAPP.CommentMXH.pViewCommentMXH" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
               <%=oCommentMXH.Title%>
            </div>
        </div>
		<div class="form-group">
	<label for="BaiViet" class="col-sm-2 control-label">BaiViet</label>
	<div class="col-sm-10">
		<%=oCommentMXH.BaiViet%>
	</div>
</div>
<div class="form-group">
	<label for="CreatedUser" class="col-sm-2 control-label">CreatedUser</label>
	<div class="col-sm-10">
		<%=oCommentMXH.CreatedUser%>
	</div>
</div>

		  <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-10">
               <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oCommentMXH.Created)%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oCommentMXH.Modified)%>
            </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-10">
                <%=oCommentMXH.Author.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-10">
                <%=oCommentMXH.Editor.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oCommentMXH.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oCommentMXH.ListFileAttach[i].Url %>"><%=oCommentMXH.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>