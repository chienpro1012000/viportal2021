<%@ page language="C#" autoeventwireup="true" codebehind="pFormCommentMXH.aspx.cs" inherits="VIPortalAPP.CommentMXH.pFormCommentMXH" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-CommentMXH" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oCommentMXH.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Titles" id="Titles" class="form-control" value="<%=oCommentMXH.Titles%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="BaiViet" class="col-sm-2 control-label">BaiViet</label>
            <div class="col-sm-10">
                <select data-selected="<%:oCommentMXH.BaiViet.LookupId%>" data-url="/UserControls/AdminCMS/MangXaHoi/pAction.asp?do=QUERYDATA" data-place="Chọn BaiViet" name="BaiViet" id="BaiViet" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <label for="CreatedUser" class="col-sm-2 control-label">CreatedUser</label>
            <div class="col-sm-10">
                <select data-selected="<%:oCommentMXH.CreatedUser.LookupId%>" data-url="/UserControls/AdminCMS/LUser/pAction.asp?do=QUERYDATA" data-place="Chọn CreatedUser" name="CreatedUser" id="CreatedUser" class="form-control"></select>
            </div>
        </div>

        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oCommentMXH.ListFileAttach)%>'
            });
            $("#CreatedUser").smSelect2018V2({
                dropdownParent: "#frm-CommentMXH",
                Ajax: true
            });
            $("#BaiViet").smSelect2018V2({
                dropdownParent: "#frm-CommentMXH"
            });
            $("#Title").focus();

            $("#frm-CommentMXH").validate({
                rules: {
                    Title: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/CommentMXH/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-CommentMXH").trigger("click");
                            $(form).closeModal();
                            $('#tblCommentMXH').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
        });
    </script>
</body>
</html>
