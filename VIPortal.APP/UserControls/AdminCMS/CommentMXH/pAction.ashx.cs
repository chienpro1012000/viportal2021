using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;

namespace VIPortalAPP.CommentMXH
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);
            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            CommentMXHDA oCommentMXHDA = new CommentMXHDA();
            CommentMXHItem oCommentMXHItem = new CommentMXHItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oCommentMXHDA.GetListJsonSolr(new CommentMXHQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw, 
                        oCommentMXHDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oCommentMXHDA.GetListJson(new CommentMXHQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oCommentMXHItem = oCommentMXHDA.GetByIdToObject<CommentMXHItem>(oGridRequest.ItemID);
                        oCommentMXHItem.UpdateObject(context.Request);
                        oCommentMXHDA.UpdateObject<CommentMXHItem>(oCommentMXHItem);
                    }
                    else
                    {
                        oCommentMXHItem.UpdateObject(context.Request);
                        oCommentMXHDA.UpdateObject<CommentMXHItem>(oCommentMXHItem);
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    string outb = oCommentMXHDA.DeleteObject(oGridRequest.ItemID);
                    if (string.IsNullOrEmpty(outb))
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb;
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oCommentMXHDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    AddLog("DELETE-MULTI", "CoQuanBanHanh", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều cơ quan ban hành");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oCommentMXHDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                     oCommentMXHDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oCommentMXHItem = oCommentMXHDA.GetByIdToObjectSelectFields<CommentMXHItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oCommentMXHItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

    }
}