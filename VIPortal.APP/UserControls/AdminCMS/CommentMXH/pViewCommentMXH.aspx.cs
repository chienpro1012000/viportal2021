using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP.CommentMXH
{
    public partial class pViewCommentMXH : pFormBase
    {
        public CommentMXHItem oCommentMXH = new CommentMXHItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oCommentMXH = new CommentMXHItem();
            if (ItemID > 0)
            {
                CommentMXHDA oCommentMXHDA = new CommentMXHDA();
                oCommentMXH = oCommentMXHDA.GetByIdToObject<CommentMXHItem>(ItemID);

            }
        }
    }
}