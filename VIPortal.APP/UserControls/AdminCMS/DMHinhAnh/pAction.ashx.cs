﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ViPortal_Utils.Base;
using ViPortal_Utils;
using ViPortalData;
using ViPortalData.DMHinhAnh;
using VIPORTAL;

namespace VIPortal.APP.UserControls.AdminCMS.DMHinhAnh
{

    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            DMHinhAnhDA oDMHinhAnhDA = new DMHinhAnhDA(UrlSite);
            DMHinhAnhItem oDMHinhAnhItem = new DMHinhAnhItem();
            HinhAnhQuery oQueryHA = new HinhAnhQuery();
            HinhAnhDA oHinhAnhDA = new HinhAnhDA(UrlSite);
            HinhAnhItem oHinhAnhItem = new HinhAnhItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    var oData = oDMHinhAnhDA.GetListJsonSolr(new DMHinhAnhQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oDMHinhAnhDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "DMHinhAnh", "QUERYDATA", 0, "Xem danh sách danh mục hình ảnh");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataDMHinhAnh = oDMHinhAnhDA.GetListJson(new DMHinhAnhQuery(context.Request));
                    treeViewItems = oDataDMHinhAnh.Select(x => new TreeViewItem()
                    {
                        key = x.DMMoTa,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;  
                    break;

                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oDMHinhAnhDA.GetListJson(new DMHinhAnhQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        //get danh sach album anh con
                        string lstIdNews = context.Request["lstIDGet"];
                        List<HinhAnhJson> lstAlAnh = new List<HinhAnhJson>();
                        oQueryHA.lstIdGet = GetDanhSachIDsQuaFormPost(lstIdNews);
                        oQueryHA.isGetBylistID = true;
                        lstAlAnh = oHinhAnhDA.GetListJson(oQueryHA);

                        oDMHinhAnhItem = oDMHinhAnhDA.GetByIdToObject<DMHinhAnhItem>(oGridRequest.ItemID);
                        oDMHinhAnhItem.UpdateObject(context.Request);
                        oDMHinhAnhItem.DMSLAnh = lstAlAnh.Count + oDMHinhAnhItem.ListFileAttach.Count + oDMHinhAnhItem.ListFileAttachAdd.Count - oDMHinhAnhItem.ListFileRemove.Count;
                        oDMHinhAnhDA.UpdateObject<DMHinhAnhItem>(oDMHinhAnhItem);
                        foreach (var item in lstAlAnh)
                        {
                            oHinhAnhItem = oHinhAnhDA.GetByIdToObject<HinhAnhItem>(item.ID);
                            oHinhAnhItem.Album = new Microsoft.SharePoint.SPFieldLookupValue(oDMHinhAnhItem.ID, "");
                            oHinhAnhDA.UpdateObject<HinhAnhItem>(oHinhAnhItem);
                            //oHinhAnhDA.SystemUpdateOneField(item.ID, "Album", oHinhAnhItem.Album.LookupId);
                        }
                        //revove thông tin. IdNewsRemove
                        string IdNewsRemove = context.Request["IdNewsRemove"];
                        List<int> lstIDRemove =  GetDanhSachIDsQuaFormPost(IdNewsRemove);
                        foreach (int itemidremove in lstIDRemove)
                        {
                            oHinhAnhDA.DeleteObject(itemidremove);
                        }
                        AddLog("UPDATE", "DMHinhAnh", "UPDATE", oGridRequest.ItemID, $"Cập nhật danh mục hình ảnh{oDMHinhAnhItem.Title}");
                    }
                    else
                    {
                        //get danh sach album anh con
                        string lstIdNews = context.Request["lstIDGet"];
                        List<HinhAnhJson> lstAlAnh = new List<HinhAnhJson>();
                        oQueryHA.lstIdGet = GetDanhSachIDsQuaFormPost(lstIdNews);
                        oQueryHA.isGetBylistID = true;
                        lstAlAnh = oHinhAnhDA.GetListJson(oQueryHA);
                        oDMHinhAnhItem.UpdateObject(context.Request);
                        oDMHinhAnhItem.DMSLAnh = lstAlAnh.Count + oDMHinhAnhItem.ListFileAttach.Count + oDMHinhAnhItem.ListFileAttachAdd.Count - oDMHinhAnhItem.ListFileRemove.Count;
                        oDMHinhAnhItem.CreatedUser = new Microsoft.SharePoint.SPFieldLookupValue(CurentUser.ID, CurentUser.Title);
                        oDMHinhAnhDA.UpdateObject<DMHinhAnhItem>(oDMHinhAnhItem);
                        foreach (var item in lstAlAnh)
                        {
                            oHinhAnhItem = oHinhAnhDA.GetByIdToObject<HinhAnhItem>(item.ID);
                            oHinhAnhItem.Album = new Microsoft.SharePoint.SPFieldLookupValue(oDMHinhAnhItem.ID, "");
                            oHinhAnhDA.SystemUpdateOneField(item.ID, "Album", oHinhAnhItem.Album.LookupId);
                        }
                        AddLog("UPDATE", "DMHinhAnh", "ADD", oGridRequest.ItemID, $"Thêm mới danh mục hình ảnh {oDMHinhAnhItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    if (oHinhAnhDA.GetListJson(new HinhAnhQuery() { Album = oGridRequest.ItemID ,_ModerationStatus = 1}).Count > 0)
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Danh mục chứa hình ảnh đang hoạt động";
                    }
                    else
                    {
                        var outb = oDMHinhAnhDA.DeleteObjectV2(oGridRequest.ItemID);
                        AddLog("DELETE", "DMHinhAnh", "DELETE", oGridRequest.ItemID, $"Xóa danh mục hình ảnh {outb.Message}");
                        if (outb.State == ActionState.Succeed)
                            oResult.Message = "Xóa thành công";
                    }
                    break;
                case "DELETE-MULTI":
                    var outbmulti = "";
                    AddLog("DELETE-MULTI", "DMHinhAnh", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều danh mục hình ảnh");
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oDMHinhAnhDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oDMHinhAnhDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "DMHinhAnh", "APPROVED", oGridRequest.ItemID, $"Duyệt danh mục hình ảnh {oDMHinhAnhDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oDMHinhAnhDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "DMHinhAnh", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt danh mục hình ảnh {oDMHinhAnhDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDMHinhAnhItem = oDMHinhAnhDA.GetByIdToObjectSelectFields<DMHinhAnhItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oDMHinhAnhItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        public static List<int> GetDanhSachIDsQuaFormPost(string arrID)
        {
            List<int> dsID = new List<int>();
            if (!string.IsNullOrEmpty(arrID))
            {
                string[] tempIDs = arrID.Split(',');
                foreach (string idConvert in tempIDs)
                {
                    int _id = 0;
                    if (int.TryParse(idConvert, out _id))
                    {
                        //if (_id > 0)
                        if (!dsID.Contains(_id))
                            dsID.Add(_id);
                    }
                }
            }
            return dsID;
        }
    }
}