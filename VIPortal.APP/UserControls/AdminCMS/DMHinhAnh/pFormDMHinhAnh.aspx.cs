﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData;
using ViPortalData.DMHinhAnh;

namespace VIPortal.APP.UserControls.AdminCMS.DMHinhAnh
{
    public partial class pFormDMHinhAnh : pFormBase
    {
        public DMHinhAnhItem oDMHinhAnh { get; set; }
        public List<HinhAnhJson> lstAnh { get; set; }
        public string lstIDGet { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            
            oDMHinhAnh = new DMHinhAnhItem();
            if (ItemID > 0)
            {
                DMHinhAnhDA oDMHinhAnhDA = new DMHinhAnhDA(UrlSite);
                HinhAnhDA oHinhAnhDA = new HinhAnhDA(UrlSite);
                oDMHinhAnh = oDMHinhAnhDA.GetByIdToObject<DMHinhAnhItem>(ItemID);
                lstAnh = oHinhAnhDA.GetListJson(new HinhAnhQuery() { Album = ItemID });
                lstIDGet = "";
                foreach (var item in lstAnh)
                {
                    if (lstIDGet == "")
                    {
                        lstIDGet = Convert.ToString(item.ID);
                    }
                    else
                    {
                        lstIDGet += "," + Convert.ToString(item.ID);
                    }
                }
            }
        }
    }
}