﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormDMHinhAnh.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.DMHinhAnh.pFormDMHinhAnh" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
    <style type="text/css">
        .modal-body {
            max-height: calc(100vh - 200px);
            overflow-y: auto;
        }
    </style>
</head>
<body>
    <form id="frm-DMHinhAnh" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oDMHinhAnh.ID %>" />
        <input type="hidden" name="do" id="doaction" value="<%=doAction %>" />
        <input type="hidden" name="CreatedUser" id="CreatedUser" value="<%=CurentUser.ID %>" />
        <input type="hidden" name="IdNews" id="IdNews" value="" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên danh mục</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" placeholder="Nhập danh mục hình ảnh" class="form-control" value="<%=oDMHinhAnh.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <!--<label for="DMSTT" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-4">
                <input type="text" name="DMSTT" id="DMSTT" placeholder="Nhập số thứ tự" value="<%:(oDMHinhAnh!= null &&  oDMHinhAnh.DMSTT>0)?oDMHinhAnh.DMSTT+"": "" %>" class="form-control" />
            </div>-->
            <label for="TBNgayDang" class="col-sm-2 control-label">Ngày đăng</label>
            <div class="col-sm-10">
                <div class="">
                    <input type="text" name="TBNgayDang" id="TBNgayDang" value="<%:string.Format("{0:dd/MM/yyyy}", oDMHinhAnh.TBNgayDang)%>" class="form-control input-datetime" />
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="DMMoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <textarea name="DMMoTa" id="DMMoTa" placeholder="Nhập mô tả" class="form-control"><%:oDMHinhAnh.DMMoTa%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="ImageNews" class="col-sm-2 control-label">Ảnh đại diện</label>
            <div class="col-sm-10" data-role="groupImageDD">

                <input type="hidden" name="ImageNews" id="ImageNews" value="<%:oDMHinhAnh.ImageNews%>" />
                <img title="Ảnh đại diện" id="srcImageNews" src="<%=oDMHinhAnh.ImageNews%>" />
                <button id="btnImageNews" type="button" class="btn  btn-success">Ảnh đại diện</button>
                <button id="btnXoaImageNews" type="button" class="btn btn-danger">Xóa ảnh</button>
            </div>
        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-10">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>

        <div class="form-group row" >
            <div class="col-sm-12" id="listAnh">

            </div>
        </div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oDMHinhAnh.ListFileAttach)%>',
                preview: true
            });
            var lstget;
            if ($("#IdNews").val() != "")
                lstget = $("#IdNews").val();
            else lstget = 0;
            if ( <%=ItemID %> > 0) {
                loadAjaxContent("/UserControls/AdminCMS/HinhAnh/pListHinhAnh.aspx?lstIdGet=" + '<%=lstIDGet%>', "#listAnh", {});
            } else {
                loadAjaxContent("/UserControls/AdminCMS/HinhAnh/pListHinhAnh.aspx?lstIdGet=" + lstget, "#listAnh", {});
            }
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $(".form-group select").select2({
                dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
            });
            $("#btnImageNews").click(function () {
                $elementId = $(this);
                CKFinder.popup({
                    chooseFiles: true,
                    width: 800,
                    height: 600,
                    onInit: function (finder) {
                        finder.on('files:choose', function (evt) {
                            var file = evt.data.files.first();
                            //var output = document.getElementById(elementId);
                            //console.log(file.getUrl());
                            //$("#ImageNews").value(file.getUrl());
                            var urlfileimg = file.getUrl();
                            //document.getElementById("ImageNews").value = urlfileimg;
                            //document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                            $("#frm-DMHinhAnh #ImageNews").val(urlfileimg);
                            $("#frm-DMHinhAnh #srcImageNews").attr("src", urlfileimg);
                        });
                        finder.on('file:choose:resizedImage', function (evt) {
                            //var output = document.getElementById(elementId);
                            console.log(evt.data);
                            var urlfileimg = evt.data.resizedUrl;
                            //document.getElementById("ImageNews").value = urlfileimg;
                            //document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                            $("#frm-DMHinhAnh #ImageNews").val(urlfileimg);
                            $("#frm-DMHinhAnh #srcImageNews").attr("src", urlfileimg);
                        });
                    }
                });
            });
            $("#btnXoaImageNews").click(function () {
                //document.getElementById("ImageNews").value = "";
                //document.getElementById("srcImageNews").setAttribute("src", "");
                $("#frm-DMHinhAnh #ImageNews").val("");
                $("#frm-DMHinhAnh #srcImageNews").attr("src", "");
            });
            $("#frm-DMHinhAnh").validate({
                ignore: [],
                rules: {
                    Title: "required",
                    ImageNews: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tên danh mục",
                    ImageNews: "Vui lòng chọn ảnh đại diện",
                },

                submitHandler: function (form) {
                    var forms = $("#frm-DMHinhAnh").viSerialize();
                    $.post("/UserControls/AdminCMS/DMHinhAnh/pAction.asp", "&do=" + $("#doaction").val() + "&lstIDGet=" + $("#IdNews").val() + "&IdNewsRemove=" + $("#IdNewsRemove").val() + "&ItemID=" + $("#ItemID").val() + "&Title=" + $("#Title").val() + "&TBNgayDang=" + $("#TBNgayDang").val() + "&DMMoTa=" + $("#DMMoTa").val() + "&ImageNews=" + $("#ImageNews").val() + "&listValueFileAttach=" + $("#listValueFileAttach").val() + "&listValueRemoveFileAttach=" + $("#listValueRemoveFileAttach").val(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            $("#btn-find-DMHinhAnh").trigger("click");
                            $(form).closeModal();
                            $('#tblDMHinhAnh').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-DMHinhAnh").viForm();
        });
    </script>
</body>
</html>
