﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.DMHinhAnh;

namespace VIPortal.APP.UserControls.AdminCMS.DMHinhAnh
{
    public partial class pViewDMHinhAnh : pFormBase
    {
        public DMHinhAnhItem oDMHinhAnh = new DMHinhAnhItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oDMHinhAnh = new DMHinhAnhItem();
            if (ItemID > 0)
            {
                DMHinhAnhDA oDMHinhAnhDA = new DMHinhAnhDA(UrlSite);
                oDMHinhAnh = oDMHinhAnhDA.GetByIdToObject<DMHinhAnhItem>(ItemID);

            }
        }
    }
}