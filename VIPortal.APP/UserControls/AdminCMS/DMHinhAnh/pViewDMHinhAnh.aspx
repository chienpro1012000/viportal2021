﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewDMHinhAnh.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.DMHinhAnh.pViewDMHinhAnh" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên ảnh</label>
            <div class="col-sm-10">
                 <%=oDMHinhAnh.Title%>
            </div>
        </div>
         <div class="form-group row">
            <label for="DMSTT" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-4">
               <%:(oDMHinhAnh!= null &&  oDMHinhAnh.DMSTT>0)?oDMHinhAnh.DMSTT+"": "" %>
            </div>
             <label for="TBNgayDang" class="col-sm-2 control-label">Ngày đăng</label>
            <div class="col-sm-4">
                <div class="">
                   <%:string.Format("{0:dd-MM-yyyy}", oDMHinhAnh.TBNgayDang)%>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="ImageNews" class="col-sm-2 control-label">Ảnh đại diện</label>
            <div class="col-sm-4" data-role="groupImageDD">
                <img style="width:100%;" src="<%=oDMHinhAnh.ImageNews%>" />
            </div>
            <label for="DMSLAnh" class="col-sm-2 control-label">Số lượng ảnh</label>
            <div class="col-sm-4">
               <%:oDMHinhAnh.DMSLAnh%>
            </div>
        </div>
        <div class="form-group row">
            <label for="DMMoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
               <%:oDMHinhAnh.DMMoTa%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oDMHinhAnh.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oDMHinhAnh.ListFileAttach[i].Url %>"><%=oDMHinhAnh.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
         <div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%=string.Format("{0:dd/MM/yyyy}", oDMHinhAnh.Created)%>
            </div>
              <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy}",oDMHinhAnh.Modified)%>
            </div>
          </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
                <%=oDMHinhAnh.Author.LookupValue%>
            </div>
             <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
                <%=oDMHinhAnh.Editor.LookupValue%>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>