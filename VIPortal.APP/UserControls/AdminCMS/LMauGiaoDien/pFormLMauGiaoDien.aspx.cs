using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public partial class pFormLMauGiaoDien : pFormBase
    {
        public LMauGiaoDienItem oLMauGiaoDien {get;set;}
        protected void Page_Load(object sender, EventArgs e)
        {
			oLMauGiaoDien = new LMauGiaoDienItem();
            if (ItemID > 0)
            {
                LMauGiaoDienDA oLMauGiaoDienDA = new LMauGiaoDienDA();
                oLMauGiaoDien = oLMauGiaoDienDA.GetByIdToObject<LMauGiaoDienItem>(ItemID);
            }
        }
    }
}