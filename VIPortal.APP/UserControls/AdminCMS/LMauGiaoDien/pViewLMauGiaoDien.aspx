<%@ page language="C#" autoeventwireup="true" codebehind="pViewLMauGiaoDien.aspx.cs" inherits="VIPortalAPP.pViewLMauGiaoDien" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <%=oLMauGiaoDien.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="TemplateHtml" class="col-sm-2 control-label">TemplateHtml</label>
            <div class="col-sm-10" id="TemplateHtmlEditor">
                <%=oLMauGiaoDien.TemplateHtml%>
            </div>
        </div>
        <div class="form-group row">
            <label for="Module" class="col-sm-2 control-label">Module chức năng</label>
            <div class="col-sm-10">
                <%=oLMauGiaoDien.Module.LookupValue%>
            </div>
        </div>
        <div class="form-group row">
            <label for="DMMoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <%=oLMauGiaoDien.DMMoTa%>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oLMauGiaoDien.Created)%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oLMauGiaoDien.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oLMauGiaoDien.ListFileAttach.Count;i++){ %>
                <div><a href="<%=oLMauGiaoDien.ListFileAttach[i].Url %>"><%=oLMauGiaoDien.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>
<script type="text/javascript">
    $(document).ready(function () {
        var embedded_editor = ace.edit("TemplateHtmlEditor");
        embedded_editor.container.style.opacity = "";
        embedded_editor.session.setMode("ace/mode/html");
        embedded_editor.setAutoScrollEditorIntoView(true);
        embedded_editor.setOption("maxLines", 40);
    });
</script>
