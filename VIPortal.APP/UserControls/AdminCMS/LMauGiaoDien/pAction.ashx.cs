using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ViPortal_Utils.Base;

namespace VIPortalAPP.LMauGiaoDien
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : IHttpHandler
    {
        public string doAction = string.Empty;
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            if (!string.IsNullOrEmpty(context.Request["do"]))
                doAction = context.Request["do"].ToUpper();

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            LMauGiaoDienDA oLMauGiaoDienDA = new LMauGiaoDienDA();
            LMauGiaoDienItem oLMauGiaoDienItem = new LMauGiaoDienItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oLMauGiaoDienDA.GetListJson(new LMauGiaoDienQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw, 
                        oLMauGiaoDienDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oLMauGiaoDienDA.GetListJson(new LMauGiaoDienQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLMauGiaoDienItem = oLMauGiaoDienDA.GetByIdToObject<LMauGiaoDienItem>(oGridRequest.ItemID);
                        oLMauGiaoDienItem.UpdateObject(context.Request);
                        oLMauGiaoDienDA.UpdateObject<LMauGiaoDienItem>(oLMauGiaoDienItem);
                    }
                    else
                    {
                        oLMauGiaoDienItem.UpdateObject(context.Request);
                        oLMauGiaoDienDA.UpdateObject<LMauGiaoDienItem>(oLMauGiaoDienItem);
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    string outb = oLMauGiaoDienDA.DeleteObject(oGridRequest.ItemID);
                    if (string.IsNullOrEmpty(outb))
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb;
                    break;
                case "DELETE-MULTI":
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oLMauGiaoDienDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                     oLMauGiaoDienDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLMauGiaoDienItem = oLMauGiaoDienDA.GetByIdToObjectSelectFields<LMauGiaoDienItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oLMauGiaoDienItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}