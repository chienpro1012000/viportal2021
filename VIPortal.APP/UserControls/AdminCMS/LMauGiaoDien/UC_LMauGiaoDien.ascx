<%@ control language="C#" autoeventwireup="true" codebehind="UC_LMauGiaoDien.aspx.cs" inherits="VIPortalAPP.UC_LMauGiaoDien" %>
<script type="text/javascript" src="/Content/plugins/ace-master/src-min-noconflict/ace.js"></script>
<div role="body-data" data-title="" class="content_wp" data-action="/UserControls/AdminCMS/LMauGiaoDien/pAction.ashx" data-form="/UserControls/AdminCMS/LMauGiaoDien/pFormLMauGiaoDien.aspx" data-view="/UserControls/AdminCMS/LMauGiaoDien/pViewLMauGiaoDien.aspx">

    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="LMauGiaoDienSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title" />
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <button class="btn btn-primary act-addfix" type="button">Thêm mới</button>
                    </p>
                </div>
            </div>

            <div class="clsgrid table-responsive">

                <table id="tblLMauGiaoDien" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $(".act-addfix").click(function () {
            openDialogView("Thêm mới Mẫu giao diện", "/UserControls/AdminCMS/LMauGiaoDien/pFormLMauGiaoDien.aspx", {}, 1244);
        });
        var $tagvalue = $(".content_wp");
        var $tblDanhMucChung = $("#tblLMauGiaoDien").viDataTable(
            {
                "frmSearch": "LMauGiaoDienSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "name": "Created", "sTitle": "Thời gian tạo",
                            "mData": function (o) {
                                return formatDateTime(o.Created);
                            },
                            "sWidth": "150px",
                        }, {
                            "name": "Module", "sTitle": "Module",
                            "mData": function (o) {
                                return o.Module.Title;
                            },
                            "sWidth": "250px",
                        }, {
                            "mData": function (o) {
                                return '<a data-ItemID="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                            },
                            "name": "Title", "sTitle": "Tiêu đề"
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {
                                if (o._ModerationStatus == 0)

                                    return '<a title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<a url="/UserControls/AdminCMS/LMauGiaoDien/pFormLMauGiaoDien.aspx" size="1244" title="Sửa thông tin mẫu giao diện" class="btn default btn-xs purple btnfrmnocheknobutton" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        }, {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="fa fa-trash-o"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE" title="Xóa"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                "aaSorting": [0, 'asc']
            });
    });
</script>
