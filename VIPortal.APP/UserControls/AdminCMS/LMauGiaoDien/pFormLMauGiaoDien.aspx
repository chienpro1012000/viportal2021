<%@ page language="C#" autoeventwireup="true" codebehind="pFormLMauGiaoDien.aspx.cs" inherits="VIPortalAPP.pFormLMauGiaoDien" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
   <style type="text/css">
        .ace_editor {
            height: 300px;
        }
    </style>
</head>
<body>
    <form id="frm-LMauGiaoDien" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oLMauGiaoDien.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oLMauGiaoDien.Title%>" />
            </div>
        </div>
        
        <div class="form-group row">
            <label for="Module" class="col-sm-2 control-label">Chức năng</label>
            <div class="col-sm-10">
                <select data-selected="<%:oLMauGiaoDien.Module.LookupId%>" data-url="/UserControls/AdminCMS/ModuleChucNang/pAction.ashx?do=QUERYDATA" data-place="Chọn Module" name="Module" id="Module" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <label for="DMSTT" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-10">
                <input type="text" name="DMSTT" id="DMSTT" placeholder="Nhập dmstt" value="<%:oLMauGiaoDien.DMSTT%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="DMMoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <textarea name="DMMoTa" id="DMMoTa" class="form-control"><%:oLMauGiaoDien.DMMoTa%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="TemplateHtml" class="col-sm-2 control-label">TemplateHtml</label>
            <div class="col-sm-10">
                <textarea name="TemplateHtmlEditor"  id="TemplateHtmlEditor" class="form-control"><%:oLMauGiaoDien.TemplateHtml%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="btnUpdate">Cập nhật</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            
        </div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#btnUpdate").click(function () {
                CK_jQ();
                $("#frm-LMauGiaoDien").submit();
            });
           
            var  embedded_editor = ace.edit("TemplateHtmlEditor");
            embedded_editor.container.style.opacity = "";
            embedded_editor.session.setMode("ace/mode/html");
            embedded_editor.setAutoScrollEditorIntoView(true);
            embedded_editor.setOption("maxLines", 40);

            $("#Module").smSelect2018V2({
                dropdownParent: "#frm-LMauGiaoDien"
            });
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oLMauGiaoDien.ListFileAttach)%>'
            });
            $("#Title").focus();
            

            $("#frm-LMauGiaoDien").validate({
                rules: {
                    Title: "required",
                    Module: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    var dataPost = $(form).serializeArray();
                    var code = embedded_editor.getValue();
                    dataPost.push({ name: "TemplateHtml", value: code });
                    $.post("/UserControls/AdminCMS/LMauGiaoDien/pAction.ashx", dataPost, function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-LMauGiaoDien").trigger("click");
                            $(form).closeModal();
                            $('#tblLMauGiaoDien').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-LMauGiaoDien").viForm();
        });
    </script>
</body>
</html>
