using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData.Lconfig;

namespace VIPortalAPP.UserControls.AdminCMS.ModuleChucNang
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {

            ProcessRequestBase(context);
            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            ModuleChucNangDA oModuleChucNangDA = new ModuleChucNangDA(UrlSite);
            ModuleChucNangItem oModuleChucNangItem = new ModuleChucNangItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oModuleChucNangDA.GetListJson(new ModuleChucNangQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oModuleChucNangDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "ModuleChucNang", "QUERYDATA", 0, "Xem danh sách Module chức năng");
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oModuleChucNangDA.GetListJson(new ModuleChucNangQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "DONGBO":
                    List<ModuleChucNangJson> lstModule = oModuleChucNangDA.GetListJson(new ModuleChucNangQuery() { });

                    string UrlPath = @"C:\tfs2021\ThucTap\VIPortal2021\VIPortal.APP\UserControls";
                    LconfigDA lconfigDA = new LconfigDA();
                    UrlPath = lconfigDA.GetValueConfigByType("PathUserControls");
                    if(string.IsNullOrEmpty(UrlPath))
                        UrlPath = @"C:\tfs2021\ThucTap\VIPortal2021\VIPortal.APP\UserControls";
                    string[] allfiles = Directory.GetFiles(UrlPath, "*.ascx", SearchOption.AllDirectories);
                    foreach (var file in allfiles)
                    {
                        FileInfo info = new FileInfo(file);
                        string UrlControl = (@"\UserControls" + file.Replace(UrlPath, "")).Replace("\\", "/");
                        if (lstModule.FindIndex(x => x.UrlControl == UrlControl) == -1)
                        {
                            oModuleChucNangDA.UpdateObject<ModuleChucNangItem>(new ModuleChucNangItem()
                            {
                                UrlControl = UrlControl,
                                Title = Path.GetFileNameWithoutExtension(UrlControl),
                                NgayTao = info.CreationTime,
                                LastModified = info.LastWriteTime
                            });
                        }
                    }
                    oResult.Message = "Lưu thành công";

                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oModuleChucNangItem = oModuleChucNangDA.GetByIdToObject<ModuleChucNangItem>(oGridRequest.ItemID);
                        oModuleChucNangItem.UpdateObject(context.Request);
                        oModuleChucNangDA.UpdateObject<ModuleChucNangItem>(oModuleChucNangItem);
                        AddLog("UPDATE", "ModuleChucNang", "UPDATE", oGridRequest.ItemID, $"Cập nhật  Module chức năng{oModuleChucNangItem.Title}");
                    }
                    else
                    {
                        oModuleChucNangItem.UpdateObject(context.Request);
                        oModuleChucNangDA.UpdateObject<ModuleChucNangItem>(oModuleChucNangItem);
                        AddLog("UPDATE", "ModuleChucNang", "ADD", oGridRequest.ItemID, $"Thêm mới  Module chức năng{oModuleChucNangItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oModuleChucNangDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "ModuleChucNang", "DELETE", oGridRequest.ItemID, $"Xóa  Module chức năng{outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    break;
                case "DELETE-MULTI":
                    AddLog("DELETE-MULTI", "ModuleChucNang", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều  Module chức năng");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oModuleChucNangDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "ModuleChucNang", "APPROVED", oGridRequest.ItemID, $"Duyệt  Module chức năng{oModuleChucNangDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oModuleChucNangDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "ModuleChucNang", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt  Module chức năng{oModuleChucNangDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oModuleChucNangItem = oModuleChucNangDA.GetByIdToObjectSelectFields<ModuleChucNangItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oModuleChucNangItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
                case "GETCONFIG":
                    if (oGridRequest.ItemID > 0)
                    {
                        oModuleChucNangItem = oModuleChucNangDA.GetByIdToObject<ModuleChucNangItem>(oGridRequest.ItemID);
                        List<ConfigNgonngu> lstConfig = new List<ConfigNgonngu>();
                        if (!string.IsNullOrEmpty(oModuleChucNangItem.ConfigNgonNgu))
                        {
                            lstConfig = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ConfigNgonngu>>(oModuleChucNangItem.ConfigNgonNgu);
                            if (!string.IsNullOrEmpty(context.Request["NgonNgu"]))
                            {
                                ConfigNgonngu oConfigNgonngu = lstConfig.FirstOrDefault(x => x.IDNgonNgu == Convert.ToInt32(context.Request["NgonNgu"]));
                                if (oConfigNgonngu != null)
                                    oResult.OData = oConfigNgonngu.ConfigKeyValue;
                                else oResult.OData = new Dictionary<string, string>();
                            }
                            else oResult.OData = new Dictionary<string, string>();
                        }
                        else
                        {
                            oResult.OData = new Dictionary<string, string>();
                        }
                    }
                    break;
                case "CONFIGNGONNGU":
                    if (oGridRequest.ItemID > 0)
                    {
                        oModuleChucNangItem = oModuleChucNangDA.GetByIdToObject<ModuleChucNangItem>(oGridRequest.ItemID);
                        List<ConfigNgonngu> lstConfig = new List<ConfigNgonngu>();
                        if (!string.IsNullOrEmpty(oModuleChucNangItem.ConfigNgonNgu))
                        {
                            lstConfig = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ConfigNgonngu>>(oModuleChucNangItem.ConfigNgonNgu);
                        }
                        if (!string.IsNullOrEmpty(context.Request["NgonNgu"]))
                        {
                            ConfigNgonngu oConfigNgonngu = lstConfig.FirstOrDefault(x => x.IDNgonNgu == Convert.ToInt32(context.Request["NgonNgu"]));
                            if (oConfigNgonngu == null)
                            {
                                oConfigNgonngu = new ConfigNgonngu()
                                {
                                    IDNgonNgu = Convert.ToInt32(context.Request["NgonNgu"]),
                                    TitleNgonNgu = context.Request["NgonNgu"],
                                    ConfigKeyValue = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(context.Request["Config"])
                                };
                            }
                            else
                            {
                                oConfigNgonngu.ConfigKeyValue = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(context.Request["Config"]);
                            }
                            int indexconfig = lstConfig.FindIndex(x => x.IDNgonNgu == Convert.ToInt32(context.Request["NgonNgu"]));
                            if (indexconfig == -1)
                                lstConfig.Add(oConfigNgonngu);
                            else lstConfig[indexconfig] = oConfigNgonngu;
                            oModuleChucNangDA.UpdateOneField(oGridRequest.ItemID, "ConfigNgonNgu", Newtonsoft.Json.JsonConvert.SerializeObject(lstConfig));
                        }
                        oResult.Message = "Lưu thành công";
                        //oModuleChucNangDA.UpdateObject<ModuleChucNangItem>(oModuleChucNangItem);
                    }
                    break;
            }
            oResult.ResponseData();

        }
    }
}