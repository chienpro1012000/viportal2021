<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormModuleChucNang.aspx.cs" Inherits="VIPortalAPP.UserControls.AdminCMS.ModuleChucNang.pFormModuleChucNang" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-ModuleChucNang" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oModuleChucNang.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên modules</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oModuleChucNang.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="UrlControl" class="col-sm-2 control-label">Url UserControl</label>
            <div class="col-sm-10">
                <input name="UrlControl" id="UrlControl" class="form-control" value="<%:oModuleChucNang.UrlControl%>"></input>
            </div>
        </div>
        <div class="form-group row">
            <label for="UrlCMS" class="col-sm-2 control-label">Đường dẫn quản trị</label>
            <div class="col-sm-10">
                <input name="UrlCMS" id="UrlCMS" class="form-control" value="<%:oModuleChucNang.UrlCMS%>"></input>
            </div>
        </div>
        <div class="form-group row">
            <label for="UrlPortal" class="col-sm-2 control-label">Đường dẫn khai thác</label>
            <div class="col-sm-10">
                <input name="UrlCMS" id="UrlCMS" class="form-control" value="<%:oModuleChucNang.UrlPortal%>"></input>
            </div>
        </div>
        <div class="form-group row">
            <label for="ImageNews" class="col-sm-2 control-label">Banner</label>
            <div class="col-sm-10" data-role="groupImageDD">

                <input type="hidden" name="ImageNews" id="ImageNews" value="<%:oModuleChucNang.ImageNews%>" />
                <img title="Ảnh đại diện" id="srcImageNews" src="<%=oModuleChucNang.ImageNews%>" />
                <button id="btnImageNews" type="button" class="btn  btn-success">Ảnh đại diện</button>
                    <button id="btnXoaImageNews" type="button" class="btn btn-danger">Xóa ảnh</button>
            </div>
        </div>
        <div class="form-group row">
            <label for="MoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <textarea name="MoTa" id="MoTa" class="form-control"><%:oModuleChucNang.MoTa%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="NgayTao" class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
                <input type="text" name="NgayTao" id="NgayTao" value="<%:string.Format("{0:dd/MM/yyyy}",oModuleChucNang.NgayTao)%>" class="form-control input-datetime" />
            </div>
            <label for="LastModified" class="col-sm-2 control-label">Ngày sửa</label>
            <div class="col-sm-4">
                <input type="text" name="LastModified" id="LastModified" value="<%:string.Format("{0:dd/MM/yyyy}",oModuleChucNang.LastModified)%>" class="form-control input-datetime" />
            </div>
        </div>

        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oModuleChucNang.ListFileAttach)%>'
            });
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $(".form-group select").select2({
                dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
            });
            $("#btnImageNews").click(function () {
                $elementId = $(this);
                CKFinder.popup({
                    chooseFiles: true,
                    width: 800,
                    height: 600,
                    onInit: function (finder) {
                        finder.on('files:choose', function (evt) {
                            var file = evt.data.files.first();
                            //var output = document.getElementById(elementId);
                            //console.log(file.getUrl());
                            //$("#ImageNews").value(file.getUrl());
                            var urlfileimg = file.getUrl();
                            document.getElementById("ImageNews").value = urlfileimg;
                            document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                        });
                        finder.on('file:choose:resizedImage', function (evt) {
                            //var output = document.getElementById(elementId);
                            console.log(evt.data);
                            var urlfileimg = evt.data.resizedUrl;
                            document.getElementById("ImageNews").value = urlfileimg;
                            document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                        });
                    }
                });
            });
            $("#frm-ModuleChucNang").validate({
                rules: {
                    Title: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/ModuleChucNang/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-ModuleChucNang").trigger("click");
                            $(form).closeModal();
                            $('#tblModuleChucNang').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-ModuleChucNang").viForm();
        });
    </script>
</body>
</html>
