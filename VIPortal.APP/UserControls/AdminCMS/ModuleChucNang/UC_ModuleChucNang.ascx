<%@ control language="C#" autoeventwireup="true" codebehind="UC_ModuleChucNang.aspx.cs" inherits="VIPortalAPP.UserControls.AdminCMS.ModuleChucNang.UC_ModuleChucNang" %>
<div role="body-data" data-title="Chức năng" class="content_wp" data-action="/UserControls/AdminCMS/ModuleChucNang/pAction.asp" data-form="/UserControls/AdminCMS/ModuleChucNang/pFormModuleChucNang.aspx" 
    data-view="/UserControls/AdminCMS/ModuleChucNang/pViewModuleChucNang.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="ModuleChucNangSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title,UrlControl" />
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <button class="btn btn-primary" id="btnDongBoMaNguon" type="button">Đồng bộ mã nguồn</button>
                        <button class="btn btn-primary act-add" data-per="081102" title="" type="button">Thêm mới</button>
                    </p>
                </div>
            </div>
            <div class="clsgrid table-responsive">
                <table id="tblModuleChucNang" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#btnDongBoMaNguon").click(function () { 
            $.post("/UserControls/AdminCMS/ModuleChucNang/pAction.asp", { do: "DONGBO" }, function (result) {
                if (result.State == 2) {
                    BootstrapDialog.show({
                        title: "Lỗi",
                        message: result.Message
                    });
                }
                else {
                    showMsg(result.Message);
                    $('#tblModuleChucNang').DataTable().ajax.reload();
                }
            }).always(function () { });
        });
        var $tagvalue = $(".content_wp");
        var $tblDanhMucChung = $("#tblModuleChucNang").viDataTable(
            {
                "frmSearch": "ModuleChucNangSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                            },
                            "name": "Title", "sTitle": "Tiêu đề",
                            "sWidth": "250px",
                        }, {
                            "data": "UrlControl",
                            "name": "UrlControl", "sTitle": "Url UserControl",
                            "sWidth": "250px",
                        },
                        {
                            "data": "MoTa",
                            "name": "MoTa", "sTitle": "Mô tả"
                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) {
                                return '<a title="Cấu hình ngôn ngữ" url="/UserControls/AdminCMS/ModuleChucNang/pFormNgonNgu.aspx" class="btn default btn-xs purple btnfrm" data-do="UPDATE" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fas fa-sliders-h"></i></a>';
                            }
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {
                                if (o._ModerationStatus == 0)

                                    return '<a data-per="081105" title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a data-per="081105" title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        }, {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE" title="Xóa"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                "aaSorting": [0, 'asc']
            });
    });
</script>
