using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP.UserControls.AdminCMS.ModuleChucNang
{
    public partial class pFormModuleChucNang : pFormBase
    {
        public ModuleChucNangItem oModuleChucNang {get;set;}
        protected void Page_Load(object sender, EventArgs e)
        {
			oModuleChucNang = new ModuleChucNangItem();
            if (ItemID > 0)
            {
                ModuleChucNangDA oModuleChucNangDA = new ModuleChucNangDA(UrlSite);
                oModuleChucNang = oModuleChucNangDA.GetByIdToObject<ModuleChucNangItem>(ItemID);
            }
        }
    }
}