<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewModuleChucNang.aspx.cs" Inherits="VIPortalAPP.UserControls.AdminCMS.ModuleChucNang.pViewModuleChucNang" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên modules</label>
            <div class="col-sm-10">
                <%=oModuleChucNang.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="UrlCMS" class="col-sm-2 control-label">Đường dẫn quản trị</label>
            <div class="col-sm-10">
                <%=oModuleChucNang.UrlCMS%>
            </div>
        </div>
        <div class="form-group row">
            <label for="UrlPortal" class="col-sm-2 control-label">Đường dẫn khai thác</label>
            <div class="col-sm-10">
                <%=oModuleChucNang.UrlPortal%>
            </div>
        </div>
        <div class="form-group row">
            <label for="ImageNews" class="col-sm-2 control-label">Banner</label>
            <div class="col-sm-10">
                <img src="<%=oModuleChucNang.ImageNews%>" alt="Alternate Text" />
            </div>
        </div>
        <div class="form-group row">
            <label for="MoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <%=oModuleChucNang.MoTa%>
            </div>
        </div>
        <div class="form-group row">
            <label for="NgayTao" class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oModuleChucNang.NgayTao)%>
            </div>
            <label class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oModuleChucNang.Modified)%>
            </div>
        </div>

       
        <div class="form-group row">
            <label class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
                <%=oModuleChucNang.Author.LookupValue%>
            </div>
            <label class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
                <%=oModuleChucNang.Editor.LookupValue%>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oModuleChucNang.ListFileAttach.Count;i++){ %>
                <div><a href="<%=oModuleChucNang.ListFileAttach[i].Url %>"><%=oModuleChucNang.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>
