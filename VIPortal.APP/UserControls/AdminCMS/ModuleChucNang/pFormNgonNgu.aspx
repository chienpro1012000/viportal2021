﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormNgonNgu.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.ModuleChucNang.pFormNgonNgu" %>

<style>
    form {
        margin: 20px 0;
    }

        form input, button {
            padding: 5px;
        }

    table {
        width: 100%;
        margin-bottom: 20px;
        border-collapse: collapse;
    }

    table, th, td {
        border: 1px solid #cdcdcd;
    }

        table th, table td {
            padding: 10px;
            text-align: left;
        }
</style>
<script>
    $(document).ready(function () {
        $("#frm-pFormNgonNgu .add-row").click(function () {
            var $KeyInPut = $("#KeyInPut").val().trim();
            var $ValueInput = $("#ValueInput").val().trim();
            var markup = `<tr><td><i class="far fa-trash-alt"></i></td><td><input type="text" name="Key" class="form-control clsKey" placeholder="Key" value="${$KeyInPut}"></td><td><input type="text" name="Value" class="form-control clsValue" placeholder="Value" value="${$ValueInput}"></td></tr>`;
            $("#frm-pFormNgonNgu table tbody").append(markup);
        });
        $("#NgonNgu").smSelect2018V2({
            dropdownParent: "#frm-pFormNgonNgu",
            dataBound: function (dataItems) {
                //$("#NgonNgu").val(dataItems[0].ID).trigger();
                $("#NgonNgu option:eq(1)").attr('selected', 'selected');
                $("#NgonNgu").trigger('change');

            }
        });
        // Find and remove selected table rows
        $("#frm-pFormNgonNgu  .delete-row").click(function () {
            $("#frm-pFormNgonNgu table tbody").find('input[name="record"]').each(function () {
                if ($(this).is(":checked")) {
                    $(this).parents("tr").remove();
                }
            });
        });

        $("#NgonNgu").change(function () {
            $.post("/UserControls/AdminCMS/ModuleChucNang/pAction.asp", { do: "GETCONFIG", ItemID: "<%=ItemID%>", NgonNgu: $("#NgonNgu").val() }, function (result) {
                if (result.State == 2) {
                    BootstrapDialog.show({
                        title: "Lỗi",
                        message: result.Message
                    });
                }
                else {
                    $("#frm-pFormNgonNgu table tbody").html("");
                    for (var key in result) {
                        //console.log("key " + key + " has value " + myArray[key]);
                        var markup = `<tr><td><i class="far fa-trash-alt"></i></td><td><input type="text" name="Key" class="form-control clsKey" placeholder="Key" value="${key}"></td><td><input type="text" name="Value" class="form-control clsValue" placeholder="Value" value="${result[key]}"></td></tr>`;
                         $("#frm-pFormNgonNgu table tbody").append(markup);
                    }
                    
                }
            }).always(function () { });
        });

        $("#frm-pFormNgonNgu").validate({
            rules: {
                NgonNgu: "required",
            },
            messages: {
                NgonNgu: "Vui lòng chọn ngôn ngữ"
            },
            submitHandler: function (form) {
                var odata = {};
                //xử lý chỗ này để lấy ra json.
                $('#frm-pFormNgonNgu tbody tr').each(function () {
                    var value = $(this).find('.clsKey').val();
                    var text = $(this).find('.clsValue').val();
                    odata[value] = text;
                });
                console.log(odata);

                $.post("/UserControls/AdminCMS/ModuleChucNang/pAction.asp", { do: "ConfigNgonNgu", ItemID: "<%=ItemID%>", NgonNgu: $("#NgonNgu").val(), Config: JSON.stringify(odata) }, function (result) {
                    if (result.State == 2) {
                        BootstrapDialog.show({
                            title: "Lỗi",
                            message: result.Message
                        });
                    }
                    else {
                        showMsg(result.Message);
                            //$("#btn-find-FAQThuongGap").trigger("click");
                            $(form).closeModal();
                    }
                }).always(function () { });
            }
        });
    });
</script>
<body>
    <form id="frm-pFormNgonNgu" class="form-horizontal">
        <div class="form-group row">
            <div class="col">
                <select data-selected="" data-url="/UserControls/AdminCMS/DanhSachNgonNgu/pAction.asp?do=QUERYDATA" data-place="Chọn ngôn ngữ" name="NgonNgu" id="NgonNgu" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <div class="col">
                <input type="text" class="form-control" name="KeyInPut" id="KeyInPut" placeholder="Key">
            </div>
            <div class="col">
                <input type="text" class="form-control" id="ValueInput" placeholder="Value">
            </div>
            <button class="btn btn-primary add-row" title="" type="button">Thêm mới</button>
        </div>



        <table>
            <thead>
                <tr>
                    <th style="width: 10%;">Xóa</th>
                    <th>Key</th>
                    <th>Value</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </form>
</body>
