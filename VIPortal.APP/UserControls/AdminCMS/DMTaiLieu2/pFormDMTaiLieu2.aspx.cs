using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using VIPortalData.DMTaiLieu2;

namespace VIPortal.APP.UserControls.AdminCMS.DMTaiLieu2
{
    public partial class pFormDMTaiLieu2 : pFormBase
    {
        public DMTaiLieu2Item oDMTaiLieu2 { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oDMTaiLieu2 = new DMTaiLieu2Item();
            DMTaiLieu2DA oDMTaiLieuDA = new DMTaiLieu2DA();
            if (ItemID > 0)
            {
                oDMTaiLieu2 = oDMTaiLieuDA.GetByIdToObject<DMTaiLieu2Item>(ItemID);
            }
            else
            {
                oDMTaiLieu2.DMSTT = oDMTaiLieuDA.GetLastSTT("DMSTT");
            }
        }
    }
}