<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_DMTaiLieu2.ascx.cs" Inherits="VIPortalAPP.UC_DMTaiLieu2" %>
<div role="body-data" data-title="danh mục tài liệu 2 " class="content_wp" data-action="/UserControls/AdminCMS/DMTaiLieu2/pAction.ashx" data-form="/UserControls/AdminCMS/DMTaiLieu2/pFormDMTaiLieu2.aspx" data-view="/UserControls/AdminCMS/DMTaiLieu2/pViewDMTaiLieu2.aspx">
    <div class="clsmanager row">
        <div class="col-sm-9">
            <div id="DMTaiLieu2Search" class="form-inline zonesearch">
                <div class="form-group">
                    <input type="hidden" name="do" id="do" value="QUERYDATA" />
                    <label for="Keyword">Từ khóa</label>
                    <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                    <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title" />
                </div>
                <button  type="button" class="btn btn-default act-search">Tìm kiếm</button>
            </div>
        </div>
        <div class="col-sm-3">
            <p class="text-right">
                <button class="btn btn-primary act-add" type="button">Thêm mới</button>
            </p>
        </div>
    </div>

    <div class="clsgrid table-responsive">

        <table id="tblDMTaiLieu2" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
            
        </table>

    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var $tagvalue = $(".content_wp");
        var $tblDanhMucTaiLieu2 = $("#tblDMTaiLieu2").viDataTable(
             {
                 "frmSearch": "DMTaiLieu2Search",
                 "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "sWidth": "20px",
                            "mData": function (o) {
                                return '';
                            },
                            "name": "STT", "sTitle": "STT", className: "txtcenter"
                        },
                        {
                            "mData": "ID",
                            "name": "ID", "visible": false, "sTitle": "ID",
                            "sWidth": "30px",
                        },
                        {
                            "mData": function (o) {
                                if (o.Title != null)
                                    return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                                else
                                    return "";
                            },
                            "name": "Title", "sTitle": "Tên danh mục "
                        },
                        {
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '">' + o.DMMoTa + '</a>';
                            },
                            "name": "DMMoTa", "sTitle": "Mô tả tài liệu"
                        },
                        {
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '">' + o.DMVietTat + '</a>';
                            },
                            "name": "DMVietTat", "sTitle": "Tên Viết Tắt"
                        },

                        {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {
                                if (o._ModerationStatus == 0)

                                    return '<a title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }

                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" title="Sửa" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        }, {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE" title="Xóa"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                 "aaSorting": [0, 'asc']
             });
    });
</script>
