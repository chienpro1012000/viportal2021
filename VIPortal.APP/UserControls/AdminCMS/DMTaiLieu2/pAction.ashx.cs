using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using VIPortalData.DMTaiLieu2;
namespace VIPortal.APP.UserControls.AdminCMS.DMTaiLieu2
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public string doAction = string.Empty;
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            if (!string.IsNullOrEmpty(context.Request["do"])) 
                doAction = context.Request["do"].ToUpper();

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            DMTaiLieu2DA oDMTaiLieu2DA = new DMTaiLieu2DA();
            DMTaiLieu2Item oDMTaiLieu2Item = new DMTaiLieu2Item();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oDMTaiLieu2DA.GetListJson(new DMTaiLieu2Query(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw, 
                        oDMTaiLieu2DA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oDMTaiLieu2DA.GetListJson(new DMTaiLieu2Query(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDMTaiLieu2Item = oDMTaiLieu2DA.GetByIdToObject<DMTaiLieu2Item>(oGridRequest.ItemID);
                        oDMTaiLieu2Item.UpdateObject(context.Request);
                        oDMTaiLieu2DA.UpdateObject<DMTaiLieu2Item>(oDMTaiLieu2Item);
                    }
                    else
                    {
                        oDMTaiLieu2Item.UpdateObject(context.Request);
                        oDMTaiLieu2DA.UpdateObject<DMTaiLieu2Item>(oDMTaiLieu2Item);
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    string outb = oDMTaiLieu2DA.DeleteObject(oGridRequest.ItemID);
                    if (string.IsNullOrEmpty(outb))
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb;
                    break;
                case "DELETE-MULTI":
                    var outbmulti = "";
                    AddLog("DELETE-MULTI", "DMTaiLieu2", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều danh mục thông tin");
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oDMTaiLieu2DA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oDMTaiLieu2DA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                   // AddLog("APPROVED", "DMTaiLieu2", "APPROVED", oGridRequest.ItemID, $"Duyệt danh mục tài liệu{oDMTaiLieu2DA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oDMTaiLieu2DA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    //AddLog("PENDDING", "DMTaiLieu2", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt mục tài liệu{oDMTaiLieu2DA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDMTaiLieu2Item = oDMTaiLieu2DA.GetByIdToObjectSelectFields<DMTaiLieu2Item>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oDMTaiLieu2Item._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}