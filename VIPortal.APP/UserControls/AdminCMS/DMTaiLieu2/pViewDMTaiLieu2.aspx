<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewDMTaiLieu2.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.DMTaiLieu2.pViewDMTaiLieu2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
               <%=oDMTaiLieu2.Title%>
            </div>
        </div>
		<div class="form-group">
	<label for="DMSTT" class="col-sm-2 control-label">DMSTT</label>
	<div class="col-sm-10">
		<%=oDMTaiLieu2.DMSTT%>
	</div>
</div>
<div class="form-group">
	<label for="DMMoTa" class="col-sm-2 control-label">DMMoTa</label>
	<div class="col-sm-10">
		<%=oDMTaiLieu2.DMMoTa%>
	</div>
</div>
<div class="form-group">
	<label for="DMNgayDang" class="col-sm-2 control-label">DMNgayDang</label>
	<div class="col-sm-10">
		<%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oDMTaiLieu2.DMNgayDang)%>
	</div>
</div>
<div class="form-group">
	<label for="DMAnh" class="col-sm-2 control-label">DMAnh</label>
	<div class="col-sm-10">
		<%=oDMTaiLieu2.DMAnh%>
	</div>
</div>
<div class="form-group">
	<label for="DMVietTat" class="col-sm-2 control-label">DMVietTat</label>
	<div class="col-sm-10">
		<%=oDMTaiLieu2.DMVietTat%>
	</div>
</div>

		  <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-10">
               <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oDMTaiLieu2.Created)%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oDMTaiLieu2.Modified)%>
            </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-10">
                <%=oDMTaiLieu2.Author.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-10">
                <%=oDMTaiLieu2.Editor.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oDMTaiLieu2.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oDMTaiLieu2.ListFileAttach[i].Url %>"><%=oDMTaiLieu2.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>