<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormDMTaiLieu2.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.DMTaiLieu2.pFormDMTaiLieu2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-DMTaiLieu2" class="form-horizontal">
	<input type="hidden" name="ItemID" id="ItemID" value="<%=oDMTaiLieu2.ID  %>" />
	<input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oDMTaiLieu2.Title%>" />
            </div>
        </div>
		<div class="form-group row">
	<label for="DMSTT" class="col-sm-2 control-label">DMSTT</label>
	<div class="col-sm-10">
		<input type="text" name="DMSTT" id="DMSTT" placeholder="Nhập STT" value="<%:oDMTaiLieu2.DMSTT%>" class="form-control"/>
	</div>
</div>
<div class="form-group row">
	<label for="DMMoTa" class="col-sm-2 control-label">DMMoTa</label>
	<div class="col-sm-10">
		<input type="text" name="DMMoTa" id="DMMoTa" placeholder="Nhập Mô Tả" value="<%:oDMTaiLieu2.DMMoTa%>" class="form-control"/>
	</div>
</div>
<div class="form-group row">
	<label for="DMNgayDang" class="col-sm-2 control-label">DMNgayDang</label>
	<div class="col-sm-10">
		<input type="text" name="DMNgayDang" id="DMNgayDang" value="<%:string.Format("{0:dd/MM/yyyy}",oDMTaiLieu2.DMNgayDang)%>" class="form-control input-datetime"/>
	</div>
</div>

<div class="form-group row">
	<label for="DMVietTat" class="col-sm-2 control-label">DMVietTat</label>
	<div class="col-sm-10">
		<input type="text" name="DMVietTat" id="DMVietTat" placeholder="Nhập Tên Viết Tắt" value="<%:oDMTaiLieu2.DMVietTat%>" class="form-control"/>
	</div>
</div>
        <div class="clearfix"></div>
    </form>
   
    <script type="text/javascript">
        $(document).ready(function () {	
			  $("#Title").focus();
			  $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale:{format: 'DD/MM/YYYY'}
				});
				$(".form-group select").select2({
				dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
				});
			
			$("#frm-DMTaiLieu2").validate({
				rules: {
					Title: "required"
				},
				messages: {
					Title: "Vui lòng nhập tiêu đề"
				},
			submitHandler: function(form) {
                $.post("/UserControls/AdminCMS/DMTaiLieu2/pAction.ashx", $(form).viSerialize()  , function (result) {
						if (result.State == 2) {
							  BootstrapDialog.show({
						        title: "Lỗi",
						        message: result.Message
                              });
						}
						else {
							showMsg(result.Message);
							//$("#btn-find-DMTaiLieu2").trigger("click");
						    $(form).closeModal();
                            $('#tblDMTaiLieu2').DataTable().ajax.reload();
						}
					}).always(function () { });
				}
			});
        });
    </script>
</body>
</html>