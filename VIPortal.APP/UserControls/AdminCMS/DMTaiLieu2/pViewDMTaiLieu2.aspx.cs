using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ViPortal_Utils.Base;
using VIPORTAL.oWeb;
using VIPortalData.DMTaiLieu2;
namespace VIPortal.APP.UserControls.AdminCMS.DMTaiLieu2
{
    public partial class pViewDMTaiLieu2 : pFormBase
    {
        public DMTaiLieu2Item oDMTaiLieu2 = new DMTaiLieu2Item();
        protected void Page_Load(object sender, EventArgs e)
        { 
            oDMTaiLieu2 = new DMTaiLieu2Item();
            if (ItemID > 0)
            {
                DMTaiLieu2DA oDMTaiLieu2DA = new DMTaiLieu2DA();
                oDMTaiLieu2 = oDMTaiLieu2DA.GetByIdToObject<DMTaiLieu2Item>(ItemID);

            }
        }
    }
}