﻿using Microsoft.SharePoint;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Hosting;
using VIPORTAL;
using ViPortal_Utils;
using ViPortal_Utils.Base;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.Lconfig;
using ViPortalData.LichCaNhan;
using ViPortalData.LichDonVi;
using ViPortalData.LichPhong;

namespace VIPortal.APP.UserControls.AdminCMS.LichDonVi
{

    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public string contentMail { get; set; }
        public List<string> mailto { get; set; }
        SPFieldLookupValueCollection lstThamGia = new SPFieldLookupValueCollection();
        private void AddNotifiLich(LichDonViItem oLichDonViItem)
        {
            if (oLichDonViItem.LichLanhDaoChuTri.LookupId > 0 && oLichDonViItem.LichThanhPhanThamGia.FindIndex(x => x == oLichDonViItem.LichLanhDaoChuTri) == -1)
                lstThamGia.Add(oLichDonViItem.LichLanhDaoChuTri);
            lstThamGia.AddRange(oLichDonViItem.LichThanhPhanThamGia);
            contentMail = "Kính gửi các Anh/Chị!";
            contentMail += "<div> Ông bà đã nhận được thông báo về cuộc họp: " + oLichDonViItem.Title + " vào hồi " + oLichDonViItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy") + " tại " +
                oLichDonViItem.LichDiaDiem + "</div>";
            contentMail += "<div>Nhận được mail này mong quý vị sắp xếp công việc và thời gian tham gia cuộc họp.</div>";
            contentMail += "<div>Trân trọng cảm ơn.</div>";

            LconfigDA lConfigDA = new LconfigDA();
            string ContentMailTemp = lConfigDA.GetValueMultilConfigByType("EmailTBLich");
            if (!string.IsNullOrEmpty(ContentMailTemp))
            {
                Dictionary<string, string> lstContent = new Dictionary<string, string>();
                string TrangThaiTxt = "Bình thường";
                if (!string.IsNullOrEmpty(oLichDonViItem.LogText))
                {
                    if (oLichDonViItem.LogText.Contains("_TT-ho_"))
                    {
                        TrangThaiTxt = "Hoãn";
                    }
                    else if (oLichDonViItem.LogText.Contains("_TT-bs_"))
                    {
                        TrangThaiTxt = "Bổ sung";
                    }
                    else if (oLichDonViItem.LogText.Contains("_TT-dt_"))
                    {
                        TrangThaiTxt = "Thay đổi";
                    }
                    //dt
                    else
                    {
                        if (oLichDonViItem.DBPhanLoai == 1)
                        {
                            TrangThaiTxt = "Bổ sung";
                        }
                        else if (oLichDonViItem.DBPhanLoai == 2)
                        {
                            TrangThaiTxt = "Hoãn";
                        }
                        else if (oLichDonViItem.DBPhanLoai == 3)
                        {
                            TrangThaiTxt = "Thay đổi";
                        }
                    }
                }
                lstContent.Add("TrangThai", TrangThaiTxt);
                lstContent.Add("NgayBatDau", string.Format("{0:HHhmm Ngày dd/MM/yyyy}", oLichDonViItem.LichThoiGianBatDau));
                lstContent.Add("Diadiem", oLichDonViItem.LichDiaDiem);
                lstContent.Add("Noidung", oLichDonViItem.LichNoiDung);
                lstContent.Add("LanhDao", oLichDonViItem.LichLanhDaoChuTri != null ? oLichDonViItem.LichLanhDaoChuTri.LookupValue : "");
                string Chu_tri_txt = "";
                if (!string.IsNullOrEmpty(oLichDonViItem.CHU_TRI) && oLichDonViItem.CHU_TRI.Contains(";#"))
                {
                    Chu_tri_txt = new SPFieldLookupValue(oLichDonViItem.CHU_TRI).LookupValue;
                }
                else Chu_tri_txt = oLichDonViItem.CHU_TRI;
                lstContent.Add("ChuTri", Chu_tri_txt);

                string CHUAN_BI_txt = "";
                if (!string.IsNullOrEmpty(oLichDonViItem.CHUAN_BI) && oLichDonViItem.CHUAN_BI.Contains(";#"))
                {
                    CHUAN_BI_txt = string.Join(", ", new SPFieldLookupValueCollection(oLichDonViItem.CHUAN_BI).Select(x => x.LookupValue));
                }
                else CHUAN_BI_txt = oLichDonViItem.CHUAN_BI;
                lstContent.Add("ChuanBi", CHUAN_BI_txt);

                string ThanhPhanTxt = "";
                if (!string.IsNullOrEmpty(oLichDonViItem.THANH_PHAN))
                {
                    ThanhPhanTxt += oLichDonViItem.THANH_PHAN;
                }
                if (oLichDonViItem.LichPhongBanThamGia != null && oLichDonViItem.LichPhongBanThamGia.Count > 0)
                {
                    ThanhPhanTxt += ((!string.IsNullOrEmpty(ThanhPhanTxt) ? ", " : "") + string.Join(", ", oLichDonViItem.LichPhongBanThamGia.Select(x => x.LookupValue)));
                }
                if (oLichDonViItem.LichThanhPhanThamGia != null && oLichDonViItem.LichThanhPhanThamGia.Count > 0)
                {
                    ThanhPhanTxt += ((!string.IsNullOrEmpty(ThanhPhanTxt) ? ", " : "") + string.Join(", ", oLichDonViItem.LichThanhPhanThamGia.Select(x => x.LookupValue)));
                }
                lstContent.Add("ThanhPhan", ThanhPhanTxt);
                //lstContent.Add("TrangThai", "Bình thường");
                foreach (Match m in Regex.Matches(ContentMailTemp, @"@[^>]*@"))
                {
                    string key = m.Value.Replace("@", "");
                    if (lstContent.ContainsKey(key))
                    {
                        //Console.WriteLine(m.Value);
                        string values = lstContent.FirstOrDefault(x => x.Key == m.Value.Replace("@", "")).Value;
                        ContentMailTemp = ContentMailTemp.Replace(m.Value, values);
                    }
                    else
                        ContentMailTemp = ContentMailTemp.Replace(m.Value, "");
                }
                contentMail = ContentMailTemp;
            }
            LNotificationDA oLNotificationDA = new LNotificationDA(UrlSite);
            LNotificationItem lNotificationItem = new LNotificationItem();
            #region Lưu Notifi
            string NguoiTao = CurentUser.Title;
            lNotificationItem.TitleBaiViet = oLichDonViItem.Title;
            lNotificationItem.LNotiNguoiGui = NguoiTao;
            if (lstThamGia.Count > 0)
            {
                lNotificationItem.LNotiNguoiNhan_Id = "," + string.Join(",", lstThamGia.Select(x => x.LookupId)) + ",";
                lNotificationItem.LNotiNguoiNhan = lstThamGia.ToString();
                lNotificationItem.LNotiChuaXem_Id = lNotificationItem.LNotiNguoiNhan_Id;
            }
            lNotificationItem.DoiTuongTitle = "Lịch họp đơn vị";
            lNotificationItem.Title = $"Lịch họp:  {oLichDonViItem.Title} ngày {oLichDonViItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy")} ";
            lNotificationItem.LNotiMoTa = $"Lịch họp:  {oLichDonViItem.Title} ngày {oLichDonViItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy")} ";
            lNotificationItem.LNotiIsSentMail = 0;
            lNotificationItem.LNotiNoiDung = contentMail;
            lNotificationItem.LNotiSentTime = DateTime.Now;
            lNotificationItem.UrlLink = "/Pages/meeting.aspx?Lich=LichDonVi&ItemID=" + oLichDonViItem.ID;
            oLNotificationDA.UpdateObject<LNotificationItem>(lNotificationItem);
            #endregion
        }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            LichDonViDA oLichDonViDA = new LichDonViDA();
            LichDonViItem oLichDonViItem = new LichDonViItem();
            LUserDA oLUserDA = new LUserDA();
            LNotificationDA oLNotificationDA = new LNotificationDA(UrlSite);
            LNotificationItem lNotificationItem = new LNotificationItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    var oData = oLichDonViDA.GetListJsonSolr(new LichDonViQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oLichDonViDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "LichDonVi", "QUERYDATA", 0, "Xem danh sách lịch đơn vị");
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataLichDonVi = oLichDonViDA.GetListJson(new LichDonViQuery(context.Request));
                    treeViewItems = oDataLichDonVi.Select(x => new TreeViewItem()
                    {
                        key = x.LichLanhDaoChuTri.Title,
                        title = x.Title
                    }).ToList();
                    oResult.OData = treeViewItems;
                    break;
                case "HUYDUYETLICH":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLichDonViItem = oLichDonViDA.GetByIdToObject<LichDonViItem>(oGridRequest.ItemID);
                        //update lại thông tin cho lịch cũ đối các thông tin về như cũ.
                        oLichDonViDA.SystemUpdateOneField(oGridRequest.ItemID, "LichTrangThai", 1);
                        oLichDonViDA.SaveSolr(oGridRequest.ItemID);
                        LichPhongDA lichPhongDA = new LichPhongDA();
                        List<LichPhongJson> LstLichPhong = lichPhongDA.GetListJson(new LichPhongQuery() { LogText = $"_LichDonVi-{oGridRequest.ItemID}_", _ModerationStatus = 1 }); // lấy ra lịch xuất phát từ lịch cha này và đang được duyệt.
                        foreach (LichPhongJson oLichPhongJson in LstLichPhong)
                        {
                            lichPhongDA.UpdateOneOrMoreField(oLichPhongJson.ID, new Dictionary<string, object>() {
                                { "LogText", oLichPhongJson.LogText + "_HuyDuyet_" },
                                {"Title", oLichPhongJson.Title + "(Hủy duyệt)" }
                            });
                            lichPhongDA.SaveSolr(oLichPhongJson.ID);
                        }
                        LichCaNhanDA lichCaNhanDA = new LichCaNhanDA();
                        List<LichCaNhanJson> LstLichCaNhan = lichCaNhanDA.GetListJson(new LichCaNhanQuery() { LogText = $"_LichDonVi-{oGridRequest.ItemID}_", _ModerationStatus = 1 }); // lấy ra lịch xuất phát từ lịch cha này và đang được duyệt.
                        foreach (LichCaNhanJson oLichCaNhanJson in LstLichCaNhan)
                        {
                            lichCaNhanDA.UpdateOneOrMoreField(oLichCaNhanJson.ID, new Dictionary<string, object>() {
                                { "LogText", oLichCaNhanJson.LogText + "_HuyDuyet_" },
                                {"Title", oLichCaNhanJson.Title + "(Hủy duyệt)" }
                            });
                            lichCaNhanDA.SaveSolr(oLichCaNhanJson.ID);
                        }
                        oResult.Message = "Hủy duyệt thành công";
                    }
                    else
                    {
                        oResult = new ResultAction()
                        {
                            State = ActionState.Error,
                            Message = "Không xác định bản ghi hủy duyệt"
                        };
                    }
                    break;
                case "DUYETLICH":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLichDonViItem = oLichDonViDA.GetByIdToObject<LichDonViItem>(oGridRequest.ItemID);

                        //goi api cho sang bên kia.


                        if (string.IsNullOrEmpty(context.Request["LoaiDuyet"]))
                        {
                            //UPDATE TRẠNG THÁI CHO LỊCH NÀY
                            oLichDonViDA.SystemUpdateOneField(oGridRequest.ItemID, "LichTrangThai", 0);
                            oLichDonViDA.SaveSolr(oGridRequest.ItemID);
                            SPFieldLookupValueCollection ALLPhongThamGIa = new SPFieldLookupValueCollection();

                            if (!string.IsNullOrEmpty(oLichDonViItem.CHU_TRI))
                            {
                                LookupData CHU_TRI = clsFucUtils.GetLoookupByStringFiledData(oLichDonViItem.CHU_TRI);
                                if (CHU_TRI.ID > 0)
                                {
                                    ALLPhongThamGIa.Add(new SPFieldLookupValue(CHU_TRI.ID, CHU_TRI.Title));
                                }
                            }
                            if (!string.IsNullOrEmpty(oLichDonViItem.CHUAN_BI))
                            {
                                List<LookupData> CHUAN_BI = clsFucUtils.GetDanhSachMutilLoookupByStringFiledData(oLichDonViItem.CHUAN_BI);
                                foreach (LookupData item in CHUAN_BI)
                                {
                                    if (ALLPhongThamGIa.FindIndex(x => x.LookupId == item.ID) == -1) ALLPhongThamGIa.Add(new SPFieldLookupValue(item.ID, item.Title));
                                }
                            }
                            foreach (var item in oLichDonViItem.LichPhongBanThamGia)
                            {
                                if (ALLPhongThamGIa.FindIndex(x => x.LookupId == item.LookupId) == -1) ALLPhongThamGIa.Add(item);
                            }
                            LichPhongDA lichPhongDA = new LichPhongDA();
                            //tạo lịch cho các phòng ban khác trong đươn vị nếu có.
                            HostingEnvironment.QueueBackgroundWorkItem(cancellationToken => new WorkerAddLichPhong(ALLPhongThamGIa, oLichDonViItem).StartProcessing(cancellationToken));
                            //foreach (var PbHop in ALLPhongThamGIa)
                            //{
                            //    LichPhongItem lichPhong = new LichPhongItem()
                            //    {
                            //        fldGroup = new SPFieldLookupValue(PbHop.LookupId, ""),
                            //        OldID = oLichDonViItem.ID.ToString(),
                            //        Title = oLichDonViItem.Title,
                            //        LichDiaDiem = oLichDonViItem.LichDiaDiem,
                            //        LogNoiDung = oLichDonViItem.LogNoiDung,
                            //        LogText = oLichDonViItem.LogText + $"_LichDonVi-{oLichDonViItem.ID}_",
                            //        LichGhiChu = oLichDonViItem.LichGhiChu,
                            //        LichNoiDung = oLichDonViItem.LichNoiDung,
                            //        LichThoiGianBatDau = oLichDonViItem.LichThoiGianBatDau,
                            //        LichThoiGianKetThuc = oLichDonViItem.LichThoiGianKetThuc,
                            //        NoiDungChuanBi = oLichDonViItem.NoiDungChuanBi,
                            //        LichLanhDaoChuTri = oLichDonViItem.LichLanhDaoChuTri, //chỗ này là khi chọn đon vị.
                            //        LichPhongBanThamGia = oLichDonViItem.LichPhongBanThamGia, //chỗ này là khi chọn đon vị.
                            //        LichThanhPhanThamGia = oLichDonViItem.LichThanhPhanThamGia, //lấy từ thông tin giao
                            //        CHUAN_BI = oLichDonViItem.CHUAN_BI,
                            //        THANH_PHAN = oLichDonViItem.THANH_PHAN,
                            //        CHU_TRI = oLichDonViItem.CHU_TRI
                            //    };
                            //    lichPhongDA.UpdateObject<LichPhongItem>(lichPhong);
                            //    lichPhongDA.UpdateSPModerationStatus(lichPhong.ID, SPModerationStatusType.Approved); //duyệt luôn.
                            //}
                            //lịch cá nhân nếu có.
                            LichCaNhanDA lichCaNhanDA = new LichCaNhanDA();
                            //taoj lichj ca nhan khi giao ve cho nguoi dung.
                            if (oLichDonViItem.LichLanhDaoChuTri.LookupId > 0)
                            {
                                LichCaNhanItem lichCaNhan = new LichCaNhanItem()
                                {
                                    CreatedUser = new SPFieldLookupValue(oLichDonViItem.LichLanhDaoChuTri.LookupId, ""),
                                    OldID = oLichDonViItem.ID.ToString(),
                                    Title = oLichDonViItem.Title,
                                    LichDiaDiem = oLichDonViItem.LichDiaDiem,
                                    LogNoiDung = oLichDonViItem.LogNoiDung,
                                    LogText = oLichDonViItem.LogText + $"_LichDonVi-{oLichDonViItem.ID}_", //xác định được logtxt khi query
                                    LichGhiChu = oLichDonViItem.LichGhiChu,
                                    LichNoiDung = oLichDonViItem.LichNoiDung,
                                    DBPhanLoai = oLichDonViItem.DBPhanLoai,
                                    LichThoiGianBatDau = oLichDonViItem.LichThoiGianBatDau,
                                    LichThoiGianKetThuc = oLichDonViItem.LichThoiGianKetThuc,
                                    NoiDungChuanBi = oLichDonViItem.NoiDungChuanBi,
                                    LichLanhDaoChuTri = oLichDonViItem.LichLanhDaoChuTri, //chỗ này là khi chọn đon vị.
                                    LichPhongBanThamGia = oLichDonViItem.LichPhongBanThamGia, //chỗ này là khi chọn đon vị.
                                    LichThanhPhanThamGia = oLichDonViItem.LichThanhPhanThamGia, //lấy từ thông tin giao
                                    CHUAN_BI = oLichDonViItem.CHUAN_BI,
                                    THANH_PHAN = oLichDonViItem.THANH_PHAN,
                                    CHU_TRI = oLichDonViItem.CHU_TRI
                                };
                                lichCaNhanDA.UpdateObject<LichCaNhanItem>(lichCaNhan);
                                lichCaNhanDA.UpdateSPModerationStatus(lichCaNhan.ID, SPModerationStatusType.Approved); //duyệt luôn.
                            }
                            //cá nhân tham gia thì cũng phải add vào.
                            if (oLichDonViItem.LichThanhPhanThamGia.Count > 0)
                            {
                                HostingEnvironment.QueueBackgroundWorkItem(cancellationToken => new WorkerAddLichCaNhan(oLichDonViItem.LichThanhPhanThamGia, oLichDonViItem).StartProcessing(cancellationToken));
                            }
                            AddNotifiLich(oLichDonViItem);
                            oResult.Message = "Duyệt thành công";
                        }
                        else
                        {
                            //oLichDonViDA.UpdateSPModerationStatus(oGridRequest.ItemID, SPModerationStatusType.Pending);
                            oLichDonViDA.SystemUpdateOneField(oGridRequest.ItemID, "LichTrangThai", 4); //Từ chối
                            oLichDonViDA.SaveSolr(oGridRequest.ItemID);
                            oResult.Message = "Hủy Duyệt thành công";
                        }
                    }
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oLichDonViDA.GetListJson(new LichDonViQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLichDonViItem = oLichDonViDA.GetByIdToObject<LichDonViItem>(oGridRequest.ItemID);
                        oLichDonViItem.UpdateObject(context.Request);
                        if (string.IsNullOrEmpty(oLichDonViItem.Title)) oLichDonViItem.Title = oLichDonViItem.LichNoiDung.GetTitle255();
                        mailto = new List<string>();
                        if (oLichDonViItem.LichLanhDaoChuTri.LookupId > 0 && oLichDonViItem.LichThanhPhanThamGia.FindIndex(x => x == oLichDonViItem.LichLanhDaoChuTri) == -1)
                            lstThamGia.Add(oLichDonViItem.LichLanhDaoChuTri);
                        //if (oLichDonViItem.LichThanhPhanThamGia.Count > 0)
                        //{
                        lstThamGia.AddRange(oLichDonViItem.LichThanhPhanThamGia);
                        foreach (var item in lstThamGia)
                        {
                            var LUser = oLUserDA.GetByIdToObject<LUserItem>(item.LookupId);
                            if (!string.IsNullOrEmpty(LUser.UserEmail))
                            {
                                mailto.Add(LUser.UserEmail);
                            }
                        }
                        //gửi mail
                        contentMail = "Kính gửi ông/bà,";
                        contentMail += "<div> Ông bà đã nhận được thông báo về cuộc họp: " + oLichDonViItem.Title + " vào hồi " + oLichDonViItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy") + " tại " + oLichDonViItem.LichDiaDiem + "</div>";
                        contentMail += "<div>Nhận được mail này mong quý vị sắp xếp công việc và thời gian tham gia cuộc họp.</div>";
                        contentMail += "<div>Trân trọng cảm ơn.</div>";
                        if (mailto.Count > 0)
                        {
                            SendMail("[Email thông báo lịch họp đơn vị]", contentMail, mailto);
                        }
                        //}

                        #region Lưu Notifi
                        string NguoiTao = context.Request["NguoiTao"];
                        lNotificationItem.TitleBaiViet = oLichDonViItem.Title;
                        if (lstThamGia.Count > 0)
                        {
                            lNotificationItem.LNotiNguoiNhan_Id = "," + string.Join(",", lstThamGia) + ",";
                            lNotificationItem.LNotiChuaXem_Id = lNotificationItem.LNotiNguoiNhan_Id;
                        }
                        lNotificationItem.LNotiNguoiGui = NguoiTao;
                        lNotificationItem.UrlLink = "/Pages/meeting.aspx?Lich=DonVi";
                        lNotificationItem.DoiTuongTitle = "Lịch họp đơn vị";
                        lNotificationItem.Title = $"Lịch họp:  {oLichDonViItem.Title} ngày {oLichDonViItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy")} ";
                        lNotificationItem.LNotiMoTa = $"Lịch họp:  {oLichDonViItem.Title} ngày {oLichDonViItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy")} ";
                        lNotificationItem.LNotiIsSentMail = 1;
                        lNotificationItem.LNotiSentTime = DateTime.Now;
                        oLNotificationDA.UpdateObject<LNotificationItem>(lNotificationItem);
                        #endregion
                        oLichDonViDA.UpdateObject<LichDonViItem>(oLichDonViItem);
                        AddLog("UPDATE", "LichDonVi", "UPDATE", oGridRequest.ItemID, $"Cập nhật lịch đơn vị{oLichDonViItem.Title}");
                    }
                    else
                    {
                        oLichDonViItem.UpdateObject(context.Request);
                        if (string.IsNullOrEmpty(oLichDonViItem.Title)) oLichDonViItem.Title = oLichDonViItem.LichNoiDung.GetTitle255();
                        mailto = new List<string>();
                        if (oLichDonViItem.LichLanhDaoChuTri.LookupId > 0 && oLichDonViItem.LichThanhPhanThamGia.FindIndex(x => x == oLichDonViItem.LichLanhDaoChuTri) == -1)
                            lstThamGia.Add(oLichDonViItem.LichLanhDaoChuTri);
                        //if (oLichDonViItem.LichThanhPhanThamGia.Count > 0)
                        //{
                        lstThamGia.AddRange(oLichDonViItem.LichThanhPhanThamGia);

                        foreach (var item in lstThamGia)
                        {
                            var LUser = oLUserDA.GetByIdToObject<LUserItem>(item.LookupId);
                            if (!string.IsNullOrEmpty(LUser.UserEmail))
                            {
                                mailto.Add(LUser.UserEmail);
                            }
                        }
                        //gửi mail
                        contentMail = "Kính gửi ông/bà,";
                        contentMail += "<div> Ông bà đã nhận được thông báo về cuộc họp: " + oLichDonViItem.Title + " vào hồi " + oLichDonViItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy") + " tại " + oLichDonViItem.LichDiaDiem + "</div>";
                        contentMail += "<div>Nhận được mail này mong quý vị sắp xếp công việc và thời gian tham gia cuộc họp.</div>";
                        contentMail += "<div>Trân trọng cảm ơn.</div>";
                        if (mailto.Count > 0)
                        {
                            SendMail("[Email thông báo lịch họp đơn vị]", contentMail, mailto);
                        }
                        //}
                        #region Lưu Notifi
                        string NguoiTao = context.Request["NguoiTao"];
                        lNotificationItem.TitleBaiViet = oLichDonViItem.Title;
                        lNotificationItem.LNotiNguoiGui = NguoiTao;
                        if (lstThamGia.Count > 0)
                        {
                            lNotificationItem.LNotiNguoiNhan_Id = "," + string.Join(",", lstThamGia) + ",";
                            lNotificationItem.LNotiChuaXem_Id = lNotificationItem.LNotiNguoiNhan_Id;
                        }
                        lNotificationItem.DoiTuongTitle = "Lịch họp đơn vị";
                        lNotificationItem.Title = $"Lịch họp:  {oLichDonViItem.Title} ngày {oLichDonViItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy")} ";
                        lNotificationItem.LNotiMoTa = $"Lịch họp:  {oLichDonViItem.Title} ngày {oLichDonViItem.LichThoiGianBatDau.Value.ToString("hh:mm dd/MM/yyyy")} ";
                        lNotificationItem.LNotiIsSentMail = 1;
                        lNotificationItem.LNotiSentTime = DateTime.Now;
                        lNotificationItem.UrlLink = "/Pages/meeting.aspx?Lich=DonVi";
                        oLNotificationDA.UpdateObject<LNotificationItem>(lNotificationItem);
                        #endregion
                        oLichDonViDA.UpdateObject<LichDonViItem>(oLichDonViItem);
                        AddLog("UPDATE", "LichDonVi", "ADD", oGridRequest.ItemID, $"Thêm mới lịch đơn vị {oLichDonViItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oLichDonViDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "LichDonVi", "DELETE", oGridRequest.ItemID, $"Xóa lịch đơn vị {outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oLichDonViDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    AddLog("DELETE-MULTI", "LichDonVi", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều lịch đơn vị");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oLichDonViDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "LichDonVi", "APPROVED", oGridRequest.ItemID, $"Duyệt lịch đơn vị {oLichDonViDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oLichDonViDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "LichDonVi", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt lịch đơn vị {oLichDonViDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLichDonViItem = oLichDonViDA.GetByIdToObjectSelectFields<LichDonViItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oLichDonViItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
}