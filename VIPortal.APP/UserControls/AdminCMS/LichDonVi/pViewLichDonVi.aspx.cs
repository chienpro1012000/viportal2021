﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.LichDonVi;

namespace VIPortal.APP.UserControls.AdminCMS.LichDonVi
{
    public partial class pViewLichDonVi : pFormBase
    {
        public LichDonViItem oLichDonVi { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oLichDonVi = new LichDonViItem();
            if (ItemID > 0)
            {
                LichDonViDA oLichDonViDA = new LichDonViDA(UrlSite);
                oLichDonVi = oLichDonViDA.GetByIdToObject<LichDonViItem>(ItemID);
            }
        }
    }
}