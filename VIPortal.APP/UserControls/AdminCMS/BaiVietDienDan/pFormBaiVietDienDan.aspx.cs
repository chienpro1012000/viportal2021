﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using VIPortalAPP;

namespace VIPortal.APP.UserControls.AdminCMS.BaiVietDienDan
{
    public partial class pFormBaiVietDienDan : pFormBase
    {
        public BaiVietItem oBaiViet = new BaiVietItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oBaiViet = new BaiVietItem();
            if (ItemID > 0)
            {
                BaiVietDA oMangXaHoiDA = new BaiVietDA();
                oBaiViet = oMangXaHoiDA.GetByIdToObject<BaiVietItem>(ItemID);

            }
        }
    }
}