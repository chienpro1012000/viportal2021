<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewBaiViet.aspx.cs" Inherits="VIPortalAPP.UserControls.AdminCMS.BaiVietDienDan.pViewBaiViet" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
               <%=oBaiViet.Title%>
            </div>
        </div>
		<div class="form-group row">
	<label for="ThreadNoiDung" class="col-sm-2 control-label">Nội dung</label>
	<div class="col-sm-10">
		<%=oBaiViet.ThreadNoiDung%>
	</div>
</div>
<div class="form-group row">
	<label for="CreatedUser" class="col-sm-2 control-label">Người tạo</label>
	<div class="col-sm-10">
		<%=oBaiViet.CreatedUser%>
	</div>
</div>
<div class="form-group row">
	<label for="ChuDe" class="col-sm-2 control-label">Chủ đề</label>
	<div class="col-sm-10">
		<%=oBaiViet.ChuDe.LookupValue%>
	</div>
</div>
<%--<div class="form-group">
	<label for="LienKetChuDe" class="col-sm-2 control-label">LienKetChuDe</label>
	<div class="col-sm-10">
		<%=oBaiViet.LienKetChuDe%>
	</div>
</div>--%>
<div class="form-group row">
	<label for="SoLuotXem" class="col-sm-2 control-label">Số lượt xem</label>
	<div class="col-sm-4">
		<%=oBaiViet.SoLuotXem%>
	</div>
    <label for="SoLuotComment" class="col-sm-2 control-label">Số lượt comment</label>
	<div class="col-sm-4">
		<%=oBaiViet.SoLuotComment%>
	</div>
</div>

		  <div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}", oBaiViet.Created)%>
            </div>
               <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy}",oBaiViet.Modified)%>
            </div>
        </div>
       <%-- <div class="form-group row">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
                <%=oBaiViet.Author.LookupValue%>
            </div>
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
                <%=oBaiViet.Editor.LookupValue%>
            </div>
        </div>--%>
        <div class="clearfix"></div>
    </form>
</body>
</html>