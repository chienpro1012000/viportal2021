using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP.UserControls.AdminCMS.BaiVietDienDan
{
    public partial class pViewBaiViet : pFormBase
    {
        public BaiVietItem oBaiViet = new BaiVietItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oBaiViet = new BaiVietItem();
            if (ItemID > 0)
            {
                BaiVietDA oBaiVietDA = new BaiVietDA(UrlSite);
                oBaiViet = oBaiVietDA.GetByIdToObject<BaiVietItem>(ItemID);

            }
        }
    }
}