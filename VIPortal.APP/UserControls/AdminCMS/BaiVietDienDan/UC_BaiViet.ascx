<%@ control language="C#" autoeventwireup="true" codebehind="UC_BaiViet.aspx.cs" inherits="VIPortalAPP.UserControls.AdminCMS.BaiVietDienDan.UC_BaiViet" %>
<div role="body-data" data-title="bài viết" class="content_wp" data-action="/UserControls/AdminCMS/BaiVietDienDan/pAction.asp" data-form="/UserControls/AdminCMS/BaiVietDienDan/pFormBaiVietDienDan.aspx" data-view="/UserControls/AdminCMS/BaiVietDienDan/pViewBaiViet.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="BaiVietSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title" />
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <button class="btn btn-primary act-add"  data-per="110102" type="button">Thêm mới</button>
                    </p>
                </div>
            </div>
            <div class="clsgrid table-responsive">

                <table id="tblBaiViet" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        function formatDate(strValue) {
            if (strValue == null) return "";
            //var date = new Date(strValue);
            var d = new Date(strValue);
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var output = //(('' + hour).length < 2 ? '0' : '') + hour + ':' +
                //(('' + minute).length < 2 ? '0' : '') + minute + " ngày " +
                (('' + day).length < 2 ? '0' : '') + day + '/' +
                (('' + month).length < 2 ? '0' : '') + month + '/' +
                d.getFullYear();
            return output;
        }

        var $tagvalue = $(".content_wp");
        var $tblDanhMucChung = $("#tblBaiViet").viDataTable(
            {
                "frmSearch": "BaiVietSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "name": "Created", "sTitle": "Thời gian",
                            "mData": function (o) {
                                return formatDateTime(o.Created);
                            },
                            "sWidth": "100px",
                        },{
                            "mData": function (o) {
                                return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                            },
                            "name": "Title", "sTitle": "Tiêu đề bài viết",
                        }, {
                            "mData": function (o) {
                                return '<a data-id="' + o.ID + '">' + o.CreatedUser.Title + '</a>';
                            },
                            "name": "CreatedUser", "sTitle": "Người tạo",
                            "sWidth": "120px",
                        }, {
                            "mData": function (d) {
                                return '<a data-id="' + d.ID + '" >' + d.ChuDe.Title + '</a>';
                            },
                            "name": "ChuDe", "sTitle": "Danh mục",
                            "sWidth": "150px",
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {
                                if (o._ModerationStatus == 0)

                                    return '<a title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" title="Sửa" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        }, {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE" title="Xóa"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                "order": [[0, "desc"]]
            });
    });
</script>
