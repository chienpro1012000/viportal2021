using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;


namespace VIPortalAPP.BaiVietDienDan
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase,IHttpHandler
    {

        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            BaiVietDA oBaiVietDA = new BaiVietDA(UrlSite);
            BaiVietItem oBaiVietItem = new BaiVietItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oBaiVietDA.GetListJson(new BaiVietQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw, 
                        oBaiVietDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oBaiVietDA.GetListJson(new BaiVietQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oBaiVietItem = oBaiVietDA.GetByIdToObject<BaiVietItem>(oGridRequest.ItemID);
                        oBaiVietItem.UpdateObject(context.Request);
                        //if (oBaiVietItem.ThreadNoiDung.Length > 255)
                        //    oBaiVietItem.Title = oBaiVietItem.ThreadNoiDung.Substring(0, 250);
                        //else
                        //    oBaiVietItem.Title = oBaiVietItem.ThreadNoiDung;
                        oBaiVietDA.UpdateObject<BaiVietItem>(oBaiVietItem);
                    }
                    else
                    {
                        oBaiVietItem.UpdateObject(context.Request);
                        //if (oBaiVietItem.ThreadNoiDung.Length > 255)
                        //    oBaiVietItem.Title = oBaiVietItem.ThreadNoiDung.Substring(0, 250);
                        //else
                        //    oBaiVietItem.Title = oBaiVietItem.ThreadNoiDung;
                        oBaiVietDA.UpdateObject<BaiVietItem>(oBaiVietItem);
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    string outb = oBaiVietDA.DeleteObject(oGridRequest.ItemID);
                    if (string.IsNullOrEmpty(outb))
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb;
                    break;
                case "DELETE-MULTI":
                    var outbmulti = "";
                    AddLog("DELETE-MULTI", "BaiVietDienDan", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều bài viết");
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oBaiVietDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oBaiVietDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                     oBaiVietDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oBaiVietItem = oBaiVietDA.GetByIdToObjectSelectFields<BaiVietItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oBaiVietItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}