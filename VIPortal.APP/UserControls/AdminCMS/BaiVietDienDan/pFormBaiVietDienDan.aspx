﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormBaiVietDienDan.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.BaiVietDienDan.pFormBaiVietDienDan" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-MangXaHoi" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oBaiViet.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" name="CreatedUser" id="CreatedUser" value="<%=CurentUser.ID %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề bài viết</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oBaiViet.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="ChuDe" class="col-sm-2 control-label">Danh mục</label>
            <div class="col-sm-10">
                <select data-selected="<%:oBaiViet.ChuDe.LookupId%>" data-url="/UserControls/Forum/ForumChuDe/pAction.asp?do=QUERYDATA&moder=1" data-place="Chọn danh mục" name="ChuDe" id="ChuDe" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <label for="AnhDaiDien" class="col-sm-2 control-label">Ảnh đại diện</label>
            <div class="col-sm-10" data-role="groupImageDD">

                <input type="hidden" name="AnhDaiDien" id="AnhDaiDien" value="<%:oBaiViet.AnhDaiDien%>" />
                <img title="Ảnh đại diện" id="srcImageNews" src="<%=oBaiViet.AnhDaiDien%>" />
                <button id="btnImageNews" type="button" class="btn  btn-success">Ảnh đại diện</button>
                <button id="btnXoaImageNews" type="button" class="btn btn-danger">Xóa ảnh</button>
            </div>
        </div>
       <div class="form-group row">
            <label for="ThreadNoiDung" class="col-sm-2 control-label">Nội dung</label>
            <div class="col-sm-10">
                <textarea name="ThreadNoiDung" id="ThreadNoiDung" placeholder="Nội dung" class="form-control"><%:oBaiViet.ThreadNoiDung%></textarea>
            </div>
        </div>
        
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#ChuDe").smSelect2018V2({
                dropdownParent: "#frm-MangXaHoi"
            });
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oBaiViet.ListFileAttach)%>',
                preview: true
            });
            $("#Title").focus();
            $("#btnImageNews").click(function () {
                $elementId = $(this);
                CKFinder.popup({
                    chooseFiles: true,
                    width: 800,
                    height: 600,
                    onInit: function (finder) {
                        finder.on('files:choose', function (evt) {
                            var file = evt.data.files.first();
                            //var output = document.getElementById(elementId);
                            //console.log(file.getUrl());
                            //$("#ImageNews").value(file.getUrl());
                            var urlfileimg = file.getUrl();
                            //document.getElementById("ImageNews").value = urlfileimg;
                            //document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                            $("#frm-MangXaHoi #AnhDaiDien").val(urlfileimg);
                            $("#frm-MangXaHoi #srcImageNews").attr("src", urlfileimg);
                        });
                        finder.on('file:choose:resizedImage', function (evt) {
                            //var output = document.getElementById(elementId);
                            console.log(evt.data);
                            var urlfileimg = evt.data.resizedUrl;
                            //document.getElementById("ImageNews").value = urlfileimg;
                            //document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                            $("#frm-MangXaHoi #AnhDaiDien").val(urlfileimg);
                            $("#frm-MangXaHoi #srcImageNews").attr("src", urlfileimg);
                        });
                    }
                });
            });
            $("#btnXoaImageNews").click(function () {
                //document.getElementById("ImageNews").value = "";
                //document.getElementById("srcImageNews").setAttribute("src", "");
                $("#frm-MangXaHoi #AnhDaiDien").val("");
                $("#frm-MangXaHoi #srcImageNews").attr("src", "");
            });

            $("#frm-MangXaHoi").validate({
                ignore: [],
                rules: {                    
                    Title: {
                        maxlength: 255, required: false,
                    },
                    ThreadNoiDung: "required",
                    AnhDaiDien: "required",
                    ChuDe: "required",
                },
                messages: {
                    Title: {
                        required: "Vui lòng nhập tên chủ đề",
                        maxlength: "Tối đa 255 ký tự"
                    } ,
                    ThreadNoiDung: "Vui lòng nhập nội dung",
                    AnhDaiDien: "Vui lòng chọn ảnh đại diện",
                    ChuDe: "Vui lòng chọn danh mục",
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/BaiVietDienDan/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-MangXaHoi").trigger("click");
                            $(form).closeModal();
                            $('#tblBaiViet').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-MangXaHoi").viForm();
        });
    </script>
</body>
</html>

