﻿<%@ page language="C#" autoeventwireup="true" codebehind="pFormGanQuyen.aspx.cs" inherits="VIPortal.APP.UserControls.AdminCMS.LUser.pFormGanQuyen" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frm-LUserQuyen" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oLUserItem.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" name="Action" id="Action" value="GanQuyen" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Họ tên</label>
            <div class="col-sm-4">
                <p class="form-control-static"><%=oLUserItem.Title%></p>
            </div>
            <label for="TaiKhoanTruyCap" class="col-sm-2 control-label">Tài khoản</label>
            <div class="col-sm-4">
                <p class="form-control-static"><%=oLUserItem.TaiKhoanTruyCap%></p>
            </div>
        </div>
        <div class="form-group row">
            <label for="Roles" class="col-sm-2 control-label">Đơn vị</label>
            <div class="col-sm-10">
                <select data-selected="" data-place="Chọn Vai trò" name="PerfldGroup" id="PerfldGroup" class="form-control">
                    <% foreach (ViPortalData.LGroupItem item in LstGroups)
                {%>
                    <option value="<%=item.ID %>"><%=item.Title %></option>
                    <%} %>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="Roles" class="col-sm-2 control-label">Vai trò</label>
            <div class="col-sm-10">
                <select multiple="multiple" data-selected="<%=oPermisonGroup.Roles != null ? string.Join(",", oPermisonGroup.Roles.Select(x=>x.ID)) : "" %>" data-url="/UserControls/AdminCMS/LVaiTro/pAction.asp?do=QUERYDATA&_moder=1" data-place="Chọn Vai trò" name="Roles" id="Roles" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <label for="Permissions" class="col-sm-2 control-label">Quyền hệ thống</label>
            <div class="col-sm-10">
                <input type="hidden" name="Permissions" id="Permissions" value="" />
                <div id="treephanquyen" data-source="ajax" class="sampletree" style="max-height: 400px; overflow-y: auto;">
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#PerfldGroup").val('<%=PerfldGroup%>');
            $("#PerfldGroup").change(function () {
                var PerfldGroup = $("#PerfldGroup").val();
                $("#frm-LUserQuyen").closeModal();
                openDialog("Gán quyền người dùng", "/UserControls/AdminCMS/LUser/pFormGanQuyen.aspx", { "PerfldGroup": PerfldGroup, "itemid": '<%=oLUserItem.ID%>' }, 800);
            });
            var $treePermis = $("#treephanquyen").fancytree({
                source: {
                    url: "/VIPortalAPI/api/LPermissions/QUERYTREE?ListPermissCheck=<%=string.Join(",", oPermisonGroup.Permissions.Select(x => x.ID)) %>"
                },
                checkbox: true,
                selectMode: 3,
                icon: false,
                beforeSelect: function (event, data) {

                },
                select: function (event, data) {

                },
            });
            $(".fancytree-container").addClass("fancytree-connectors");
            $("#Roles").smSelect2018V2({
                dropdownParent: "#frm-LUserQuyen"
            });

            $("#frm-LUserQuyen").validate({
                rules: {
                },
                messages: {
                },
                submitHandler: function (form) {
                    var tree = $('#treephanquyen').fancytree('getTree');
                    var selKeys = $.map(tree.getSelectedNodes(), function (element, index) { return element.key }).join(",");
                    $("#Permissions").val(selKeys);
                    $.post("/UserControls/AdminCMS/LUser/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-LUser").trigger("click");
                            //$(form).closeModal();
                            $('#tblLUser').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
        });
    </script>
</body>
</html>
