using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortalAPP
{
    public partial class pFormLUser : pFormBase
    {
        public LUserItem oLUser {get;set;}
        protected void Page_Load(object sender, EventArgs e)
        {
			oLUser = new LUserItem();
            if (ItemID > 0)
            {
                LUserDA oLUserDA = new LUserDA();
                oLUser = oLUserDA.GetByIdToObject<LUserItem>(ItemID);
            }
            else
            {
                if (!string.IsNullOrEmpty(Request["userphongban"]))
                {
                    oLUser.UserPhongBan = new Microsoft.SharePoint.SPFieldLookupValue(Convert.ToInt32(Request["userphongban"]), "");
                    LGroupDA oLGroupDA = new LGroupDA();
                    oLUser.Groups = new Microsoft.SharePoint.SPFieldLookupValue( oLGroupDA.GetDonByPhongBan(oLUser.UserPhongBan.LookupId).ID,"");

                }
            }
        }
    }
}