using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData;
using ViPortalData.Lconfig;
using ViPortalData.LVaiTro;

namespace VIPortalAPP.LUser
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);


            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            LUserDA oLUserDA = new LUserDA();
            LUserItem oLUserItem = new LUserItem();
            switch (doAction)
            {
                case "UPDATEROLE":
                    if (!string.IsNullOrEmpty(context.Request["Role"]))
                    {
                        int Role = Convert.ToInt32(context.Request["Role"]);
                        if (oGridRequest.LstItemID.Count > 0)
                        {
                            foreach (int item in oGridRequest.LstItemID)
                            {
                                oLUserItem = oLUserDA.GetByIdToObject<LUserItem>(item);
                                oLUserItem.Roles.Add(new SPFieldLookupValue(Role, ""));
                                oLUserDA.SystemUpdateOneField(item, "Roles", oLUserItem.Roles);
                            }
                            oResult.Message = "Gán vai trò thành công";
                        }
                        else
                        {
                            oResult.Message = "Chưa chọn người dùng";
                            oResult.State = ActionState.Error;
                        }
                    }
                    else
                    {
                        oResult.Message = "Không xác định Vai trò được gán";
                        oResult.State = ActionState.Error;
                    }
                    break;
                case "QUERYDATA":
                    oResult.IsQuery = true;
                    LUserQuery oLUserQuery = new LUserQuery(context.Request);
                    //fix riêng với user default là load ra theo stt.
                    oLUserQuery.Ascending = context.Request["order[0][dir]"] == "desc" ? false : true;
                    string indexCol = context.Request["order[0][column]"];
                    if (indexCol != null && indexCol.Length > 0)
                    {
                        oLUserQuery.FieldOrder = context.Request[string.Format("columns[{0}][name]", indexCol)];
                        if (oLUserQuery.FieldOrder == null || oLUserQuery.FieldOrder.Length == 0)
                            oLUserQuery.FieldOrder = context.Request[string.Format("columns[{0}][data]", indexCol)];
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(context.Request["FieldOrder"]))
                        {
                            oLUserQuery.FieldOrder = context.Request["FieldOrder"];
                        }
                        else oLUserQuery.FieldOrder = "DMSTT";
                        if (!string.IsNullOrEmpty(context.Request["Ascending"]))
                        {
                            oLUserQuery.Ascending = Convert.ToBoolean(context.Request["Ascending"]);
                        }
                        else oLUserQuery.Ascending = true;
                    }
                    var oData = oLUserDA.GetListJson(oLUserQuery);

                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oLUserDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "LUser", "QUERYDATA", 0, "Xem danh sách người dùng");
                    break;
                case "GETBYID":
                    if (oGridRequest.ItemID > 0)
                    {
                        LUserItem temp = oLUserDA.GetByIdToObjectSelectFields<LUserItem>(oGridRequest.ItemID, "ID", "Title", "Connections");
                        oResult.OData = temp;
                    }
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oLUserDA.GetListJson(new LUserQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                      
                        oLUserItem = oLUserDA.GetByIdToObject<LUserItem>(oGridRequest.ItemID);
                        string oldName = oLUserItem.TaiKhoanTruyCap;
                        oLUserItem.UpdateObject(context.Request);
                        
                        if (string.IsNullOrEmpty(context.Request["Action"]))
                        {
                            #region check trùng và update
                            if (oLUserDA.GetCount(new LUserQuery() { TaiKhoanTruyCap = oLUserItem.TaiKhoanTruyCap, ItemIDNotGet = oLUserItem.ID }) > 0)
                            {
                                oResult = new ResultAction() { State = ActionState.Error, Message = "Trùng tên truy cập" };
                                break;
                            }
                            if (!string.IsNullOrEmpty(oLUserItem.UserEmail) && oLUserDA.GetCount(new LUserQuery() { UserEmail = oLUserItem.UserEmail, ItemIDNotGet = oLUserItem.ID }) > 0)
                            {
                                oResult = new ResultAction() { State = ActionState.Error, Message = "Trùng Email" };
                                break;
                            }
                            if (!string.IsNullOrEmpty(oLUserItem.UserSDT) && oLUserDA.GetCount(new LUserQuery() { UserSDT = oLUserItem.UserSDT, ItemIDNotGet = oLUserItem.ID }) > 0)
                            {
                                oResult = new ResultAction() { State = ActionState.Error, Message = "Trùng Số điện thoại" };
                                break;
                            }
                            #endregion
                            oLUserDA.UpdateObject<LUserItem>(oLUserItem);
                            if (oldName != oLUserItem.TaiKhoanTruyCap)
                            {
                                string passwordQuestion = "";
                                string passwordAnswer = "";
                                MembershipCreateStatus status;
                                if (Membership.RequiresQuestionAndAnswer)
                                {
                                    passwordQuestion = "evn";
                                    passwordAnswer = "evn";
                                }
                                try
                                {
                                    MembershipUser newUser = Membership.CreateUser(oLUserItem.TaiKhoanTruyCap, "123456a@",
                                                                                   $"{oLUserItem.TaiKhoanTruyCap}@evnhn.vn", passwordQuestion,
                                                                                   passwordAnswer, true, out status);
                                    if (newUser == null && status != MembershipCreateStatus.DuplicateUserName)
                                    {
                                        //Msg.Text = GetErrorMessage(status);
                                        oResult.Message = GetErrorMessage(status);
                                        oResult.State = ActionState.Error;
                                    }
                                }
                                catch (Exception ex)
                                {

                                }
                            }
                            //SPUser newUser = newWeb.EnsureUser(@"domain\username");
                            SPSite site = SPContext.Current.Site;
                            SPWeb web = SPContext.Current.Web;
                            LconfigDA oLconfigDA = new LconfigDA();
                            string FomatNameLogin = oLconfigDA.GetListJson(new LconfigQuery() { ConfigType = "AuthenticationProviders" }).FirstOrDefault()?.ConfigValue;
                            FomatNameLogin = string.Format(FomatNameLogin, oLUserItem.TaiKhoanTruyCap);
                            SPSecurity.RunWithElevatedPrivileges(delegate ()
                            {
                                using (SPSite ElevatedSite = new SPSite(site.ID))
                                {
                                    using (SPWeb ElevatedWeb = ElevatedSite.OpenWeb())
                                    {
                                        // Code Using the SPWeb Object Goes Here
                                        ElevatedWeb.AllowUnsafeUpdates = true;
                                        SPUser oSPUser = ElevatedWeb.EnsureUser(FomatNameLogin);
                                        oSPUser.Name = oLUserItem.Title;
                                        oSPUser.Update();
                                        ElevatedWeb.AllowUnsafeUpdates = false;
                                        ElevatedWeb.Update();
                                    }
                                }
                            });
                            oResult.Message = "Cập nhật thành công";

                            AddLog("UPDATE", "LUser", "UPDATE", oGridRequest.ItemID, $"Cập nhật người dùng{oLUserItem.Title}");
                        }
                        else if (context.Request["Action"] == "GanQuyen")
                        {
                            //update role và quyền cho tài khoản.
                            LPermissionDA lPermissionDA = new LPermissionDA();
                            List<LookupData> LstPerALL = new List<LookupData>();
                            List<LPermissionJson> lstPermissionJson = new List<LPermissionJson>();
                            PermisonGroup permisonGroup = new PermisonGroup();
                            if (!string.IsNullOrEmpty(context.Request["PerfldGroup"]))
                            {
                                permisonGroup.fldGroup = Convert.ToInt32(context.Request["PerfldGroup"]);
                            }
                            if (!string.IsNullOrEmpty(context.Request["Roles"]))
                            {
                                permisonGroup.Roles = clsFucUtils.GetDanhSachIDsQuaFormPost(context.Request["Roles"]).Select(x => new LookupData(x, "")).ToList();
                                LVaiTroDA lVaiTroDA = new LVaiTroDA();
                                List<LVaiTroJson> lstVaiTroJson = lVaiTroDA.GetListJson(new LVaiTroQuery() { isGetBylistID = true, lstIDget = permisonGroup.Roles.Select(x => x.ID).ToList() });
                                foreach (LVaiTroJson itemrole in lstVaiTroJson)
                                {
                                    LstPerALL.AddRange(itemrole.Permission);
                                }
                                permisonGroup.Roles = lstVaiTroJson.Select(x => new LookupData(x.ID, x.Title)).ToList();
                            }
                            if (!string.IsNullOrEmpty(context.Request["Permissions"]))
                            {
                                permisonGroup.Permissions = clsFucUtils.GetDanhSachIDsQuaFormPost(context.Request["Permissions"]).Select(x => new LookupData(x, "")).ToList();
                                List<LPermissionJson> lPermissionJsons = lPermissionDA.GetListJson(new LPermissionQuery() { isGetBylistID = true, lstIDget = permisonGroup.Permissions.Select(x => x.ID).ToList() });
                                LstPerALL.AddRange(lPermissionJsons.Select(x => new LookupData(x.ID, x.Title)));
                                permisonGroup.Permissions = lPermissionJsons.Select(x => new LookupData(x.ID, x.Title)).ToList();
                            }
                            List<PermisonGroup> permisonGroups = new List<PermisonGroup>();
                            if (!string.IsNullOrEmpty(oLUserItem.PerJson))
                            {
                                permisonGroups = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PermisonGroup>>(oLUserItem.PerJson);
                            }
                            //tim nhóm phân quyền.
                            int indexper = permisonGroups.FindIndex(x => x.fldGroup == permisonGroup.fldGroup);
                            if (indexper > -1)
                            {
                                permisonGroups[0] = permisonGroup;
                            }
                            else
                            {
                                permisonGroups.Add(permisonGroup);
                            }
                            oLUserItem.PerJson = Newtonsoft.Json.JsonConvert.SerializeObject(permisonGroups);
                            //tính toán cho perquery
                            if (LstPerALL.Count > 0)
                            {
                                List<int> ListIDPer = LstPerALL.Select(x => x.ID).Distinct().ToList();
                                List<LPermissionJson> lPermissionJsons = lPermissionDA.GetListJson(new LPermissionQuery() { isGetBylistID = true, lstIDget = ListIDPer });
                                //oLUserItem.PerQuery  == |1_001|2_003|;
                                LstPerQuery lstPerQuery = new LstPerQuery();
                                if (!string.IsNullOrEmpty(oLUserItem.PerQuery))
                                {
                                    lstPerQuery = new LstPerQuery(oLUserItem.PerQuery);
                                }
                                lstPerQuery.perQueries.RemoveAll(x => x.fldGroup == permisonGroup.fldGroup);
                                lstPerQuery.perQueries.AddRange(lPermissionJsons.Select(x => new PerQuery(permisonGroup.fldGroup, x.MaQuyen)));
                                if(lstPerQuery.perQueries.Count > 0)
                                {
                                    oLUserItem.PerQuery = "|" + string.Join("|", lstPerQuery.perQueries.Select(x => x.fldGroup + "_" + x.MaQuyen)) + "|";
                                }
                            }
                            //update với 2 field kể trên thôi.
                            oLUserDA.UpdateOneOrMoreField(oLUserItem.ID, new Dictionary<string, object>() {
                                {"PerQuery", oLUserItem.PerQuery},
                                {"PerJson", oLUserItem.PerJson},
                                {"Roles", oLUserItem.Roles},
                                {"Permissions", oLUserItem.Permissions}
                            }, true);
                            oResult.Message = "Phân quyền thành công";
                        }
                    }
                    else
                    {
                        oLUserItem.UpdateObject(context.Request);
                        #region MyRegion
                        if (oLUserDA.GetCount(new LUserQuery() { TaiKhoanTruyCap = oLUserItem.TaiKhoanTruyCap }) > 0)
                        {
                            oResult = new ResultAction() { State = ActionState.Error, Message = "Trùng tên truy cập" };
                            break;
                        }
                        if (!string.IsNullOrEmpty(oLUserItem.UserEmail) && oLUserDA.GetCount(new LUserQuery() { UserEmail = oLUserItem.UserEmail }) > 0)
                        {
                            oResult = new ResultAction() { State = ActionState.Error, Message = "Trùng Email" };
                            break;
                        }
                        if (!string.IsNullOrEmpty(oLUserItem.UserSDT) && oLUserDA.GetCount(new LUserQuery() { UserSDT = oLUserItem.UserSDT }) > 0)
                        {
                            oResult = new ResultAction() { State = ActionState.Error, Message = "Trùng Số điện thoại" };
                            break;
                        }
                        #endregion


                        MembershipCreateStatus status;
                        string passwordQuestion = "";
                        string passwordAnswer = "";

                        if (Membership.RequiresQuestionAndAnswer)
                        {
                            passwordQuestion = "evn";
                            passwordAnswer = "evn";
                        }
                        try
                        {
                            MembershipUser newUser = Membership.CreateUser(oLUserItem.TaiKhoanTruyCap, "123456a@",
                                                                           $"{oLUserItem.TaiKhoanTruyCap}@evnhn.vn", passwordQuestion,
                                                                           passwordAnswer, true, out status);
                            if (newUser == null && status != MembershipCreateStatus.DuplicateUserName)
                            {
                                //Msg.Text = GetErrorMessage(status);
                                oResult.Message = GetErrorMessage(status);
                                oResult.State = ActionState.Error;
                            }
                            else
                            {
                                oLUserDA.UpdateObject<LUserItem>(oLUserItem);
                                oResult.Message = "Lưu thành công";
                                //Response.Redirect("login.aspx");
                            }
                        }
                        catch (Exception ex)
                        {
                            oResult.Message = ex.Message;
                            oResult.State = ActionState.Error;
                            ///Msg.Text = "An exception occurred creating the user.";
                        }


                        AddLog("UPDATE", "LUser", "ADD", oGridRequest.ItemID, $"Thêm mới người dùng{oLUserItem.Title}");
                    }

                    break;
                case "DELETE":
                    var outb = oLUserDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "LUser", "DELETE", oGridRequest.ItemID, $"Xóa người dùng{outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb.Message;
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    AddLog("DELETE-MULTI", "LUser", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều người dùng");
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oLUserDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oLUserDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    //check user có thông tin.
                    oLUserItem = oLUserDA.GetByIdToObject<LUserItem>(oGridRequest.ItemID);
                    try
                    {
                        //MembershipUser newUser = Membership.GetUser(oLUserItem.TaiKhoanTruyCap);
                        //bool isResult =  newUser.UnlockUser();
                        LconfigDA oLconfigDA = new LconfigDA();
                        string ConnectionStrings = oLconfigDA.GetListJson(new LconfigQuery() { ConfigType = "ConnectionStrings" }).FirstOrDefault()?.ConfigValue;
                        if (!string.IsNullOrEmpty(ConnectionStrings))
                        {
                            SqlConnection dbConnection = new SqlConnection(ConnectionStrings);
                            {
                                string updateUserSql = $"UPDATE aspnet_Membership SET IsLockedOut = 0 WHERE LoweredEmail like '{oLUserItem.TaiKhoanTruyCap.ToLower()}@%'";
                                dbConnection.Open();
                                SqlCommand comd = new SqlCommand(updateUserSql, dbConnection);
                                int CountItem = comd.ExecuteNonQuery();
                                dbConnection.Close();
                            }

                        }
                        //Membership.UpdateUser(newUser);
                        AddLog("APPROVED", "LUser", "APPROVED", oGridRequest.ItemID, $"Duyệt người dùng{oLUserDA.GetTitleByID(oGridRequest.ItemID)}");
                        oResult.Message = "Duyệt thành công.";
                    }
                    catch (Exception ex)
                    {
                        oResult.Message = ex.Message;
                        oResult.State = ActionState.Error;
                    }


                    break;
                case "PENDDING":
                    oLUserDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "LUser", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt người dùng{oLUserDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oLUserItem = oLUserDA.GetByIdToObjectSelectFields<LUserItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oLUserItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }


        public string GetErrorMessage(MembershipCreateStatus status)
        {
            switch (status)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "Username already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A username for that email address already exists. Please enter a different email address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The email address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
    }
}