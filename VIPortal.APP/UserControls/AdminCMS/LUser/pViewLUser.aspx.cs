using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortalAPP
{
    public partial class pViewLUser : pFormBase
    {
        public LUserItem oLUser = new LUserItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oLUser = new LUserItem();
            if (ItemID > 0)
            {
                LUserDA oLUserDA = new LUserDA();
                oLUser = oLUserDA.GetByIdToObject<LUserItem>(ItemID);

            }
        }
    }
}