<%@ page language="C#" autoeventwireup="true" codebehind="pViewLUser.aspx.cs" inherits="VIPortalAPP.pViewLUser" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
    <script type="text/javascript">
        //btnDongBoAnh
        $(document).ready(function () {
            $("#btnDongBoAnh").click(function () {
                $.post("/VIPortalAPI/api/LUser/DONGBOANH", { ItemID: "<%=oLUser.ID%>", staffCode : "<%=oLUser.HRMSID%>"}, function (result) {
                    if (result.State == 2) {
                        BootstrapDialog.show({
                            title: "Lỗi",
                            message: result.Message
                        });
                    }
                    else {
                        showMsg(result.Message);
                        //$("#btn-find-LUser").trigger("click");
                        $("#ViewUser").closeModal();
                        $('#tblLUser').DataTable().ajax.reload();
                    }
                }).always(function () { });
            });
        });
    </script>
</head>
<body>
    <form class="form-horizontal form-viewdetail" id="ViewUser">
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <%=oLUser.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Ảnh đại diện</label>
            <div class="col-sm-10">
                <img src="<%=oLUser.AnhDaiDien%>" title="Ảnh đại diện" style="width: 150px" />
                 <button class="btn btn-primary" id="btnDongBoAnh" type="button">Đồng bộ ảnh</button>
            </div>
        </div>
        <div class="form-group row">
            <label for="Groups" class="col-sm-2 control-label">Groups</label>
            <div class="col-sm-10">
                <%=oLUser.Groups.LookupValue%>
            </div>
        </div>
        <div class="form-group row">
            <label for="UserPhongBan" class="col-sm-2 control-label">Phòng ban</label>
            <div class="col-sm-10">
                <%=oLUser.UserPhongBan.LookupValue%>
            </div>
        </div>
        <div class="form-group row">
            <label for="TaiKhoanTruyCap" class="col-sm-2 control-label">Tài khoản truy cập</label>
            <div class="col-sm-10">
                <%=oLUser.TaiKhoanTruyCap%>
            </div>
        </div>
        <div class="form-group row">
            <label for="UserEmail" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-4">
                <%=oLUser.UserEmail%>
            </div>
            <label for="UserSDT" class="col-sm-2 control-label">Số điện thoại</label>
            <div class="col-sm-4">
                <%=oLUser.UserSDT%>
            </div>
        </div>

        <div class="form-group row">
            <label for="UserChucVu" class="col-sm-2 control-label">Chức vụ</label>
            <div class="col-sm-4">
                <%=oLUser.UserChucVu%>
            </div>
            <label for="UserNgaySinh" class="col-sm-2 control-label">Ngày sinh</label>
            <div class="col-sm-4">
                <%=oLUser.UserNgaySinh%>
            </div>
        </div>
        <div class="form-group row">
            <label for="Permissions" class="col-sm-2 control-label">Permissions</label>
            <div class="col-sm-4">
                <%=oLUser.Permissions%>
            </div>
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-4">
                <%for(int i=0; i< oLUser.ListFileAttach.Count;i++){ %>
                <div><a href="<%=oLUser.ListFileAttach[i].Url %>"><%=oLUser.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oLUser.Created)%>
            </div>
            <label class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oLUser.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
                <%=oLUser.Author.LookupValue%>
            </div>
            <label class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
                <%=oLUser.Editor.LookupValue%>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>
