<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormLUser.aspx.cs" Inherits="VIPortalAPP.pFormLUser" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
    <style>
        .error {
            color: red;
        }
    </style>
</head>
<body>

    <form id="frm-LUser" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oLUser.ID %>" />
        
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Họ tên</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" placeholder="Nhập họ tên" class="form-control" value="<%=oLUser.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="TaiKhoanTruyCap" class="col-sm-2 control-label">Tài khoản</label>
            <div class="col-sm-4">
                <input type="text" name="TaiKhoanTruyCap" id="TaiKhoanTruyCap" placeholder="Nhập tài khoản truy cập" value="<%:oLUser.TaiKhoanTruyCap%>" class="form-control" />
            </div>
             <label for="TaiKhoanTruyCap" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-4">
                <input type="text" name="DMSTT" id="DMSTT" placeholder="STT" value="<%:oLUser.DMSTT%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="UserEmail" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-4">
                <input type="text" name="UserEmail" id="UserEmail" placeholder="Nhập email" value="<%:oLUser.UserEmail%>" class="form-control" />
            </div>
            <label for="UserSDT" class="col-sm-2 control-label">Số điện thoại</label>
            <div class="col-sm-4">
                <input type="text" name="UserSDT" id="UserSDT" placeholder="Nhập số điện thoại" value="<%:oLUser.UserSDT%>" class="form-control" />
            </div>
        </div>

        <div class="form-group row">
            <label for="Groups" class="col-sm-2 control-label">Đơn vị</label>
            <div class="col-sm-4">
                <select data-selected="<%=oLUser.Groups.LookupId %>" data-url="/UserControls/AdminCMS/LGroup/pAction.asp?do=QUERYDATA&GroupIsDonVi=1" data-place="Chọn đơn vị" name="Groups" id="Groups" class="form-control"></select>
            </div>
            <label for="UserPhongBan" class="col-sm-2 control-label">Phòng ban</label>
            <div class="col-sm-4">
                 <select data-selected="<%=oLUser.UserPhongBan.LookupId %>" data-url="/UserControls/AdminCMS/LGroup/pAction.asp?do=QUERYDATA" data-place="Chọn phòng ban" name="UserPhongBan" id="UserPhongBanForm" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <label for="UserChucVu" class="col-sm-2 control-label">Chức vụ</label>
            <div class="col-sm-4">
                <input type="text" name="UserChucVu" id="UserChucVu" placeholder="Nhập chức vụ" value="<%:oLUser.UserChucVu%>" class="form-control" />
            </div>
            <label for="UserNgaySinh" class="col-sm-2 control-label">Ngày sinh</label>
            <div class="col-sm-4">
                <input type="text" name="UserNgaySinh" id="UserNgaySinh" placeholder="Nhập ngày sinh" value="<%:string.Format("{0:dd/MM/yyyy}",oLUser.UserNgaySinh)%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="Roles" class="col-sm-2 control-label">Vai trò</label>
            <div class="col-sm-10">
                <select multiple="multiple" data-selected="<%=string.Join(",", oLUser.Roles.Select(x=>x.LookupId)) %>" data-url="/UserControls/AdminCMS/LVaiTro/pAction.asp?do=QUERYDATA" data-place="Chọn Vai trò" name="Roles" id="Roles" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <label for="AnhDaiDien" class="col-sm-2 control-label">Ảnh đại diện</label>
            <div class="col-sm-10" data-role="groupImageDD">
                <input type="hidden" name="AnhDaiDien" id="AnhDaiDien" value="<%=oLUser.AnhDaiDien%>" />
                <img title="Ảnh đại diện" id="srcImageNews" src="<%=oLUser.AnhDaiDien%>" />
                <button id="btnImageNews" type="button" class="btn  btn-success">Ảnh đại diện</button>
                <button id="btnXoaImageNews" type="button" class="btn btn-danger">Xóa ảnh</button>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            if (<%=ItemID%> > 0) {
                $("#TaiKhoanTruyCap").prop('disabled', true);
            }
            //Groups
            $("#Groups").smSelect2018V2({
                dropdownParent: "#frm-LUser"
            });
            //UserPhongBan
            $("#UserPhongBanForm").smSelect2018V2({
                dropdownParent: "#frm-LUser"
            });
            $("#Roles").smSelect2018V2({
                dropdownParent: "#frm-LUser"
            });
            //UserNgaySinh
            $("#UserNgaySinh").daterangepicker({
                autoUpdateInput: false,
                singleDatePicker: true,
                locale: vidatalocale,
                maxDate: new Date(),
                useCurrent: false
            });
            $('#UserNgaySinh').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY')).change();
            });
            $("#btnImageNews").click(function () {
                $elementId = $(this);
                CKFinder.popup({
                    chooseFiles: true,
                    width: 800,
                    resourceType: 'Images',
                    height: 600,
                    onInit: function (finder) {
                        finder.on('files:choose', function (evt) {
                            var file = evt.data.files.first();
                            //var output = document.getElementById(elementId);
                            //console.log(file.getUrl());
                            //$("#ImageNews").value(file.getUrl());
                            var urlfileimg = file.getUrl();
                            document.getElementById("AnhDaiDien").value = urlfileimg;
                            document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                        });
                        finder.on('file:choose:resizedImage', function (evt) {
                            //var output = document.getElementById(elementId);
                            console.log(evt.data);
                            var urlfileimg = evt.data.resizedUrl;
                            document.getElementById("AnhDaiDien").value = urlfileimg;
                            document.getElementById("srcImageNews").setAttribute("src", urlfileimg);
                        });
                    }
                });
            });
            //DeleteImage('btnXoaImageNews', 'srcImageNews', 'AnhDaiDien');
            $("#btnXoaImageNews").click(function () {
                document.getElementById("AnhDaiDien").value = "";
                document.getElementById("srcImageNews").setAttribute("src", "");
            });
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oLUser.ListFileAttach)%>'
            });
            $("#Title").focus();



            $("#frm-LUser").validate({
                ignore: [],
                rules: {
                    Title: "required",
                    TaiKhoanTruyCap: "required",
                    Roles: "required",
                    AnhDaiDien: "required",
                    UserNgaySinh: {
                        LeqToDay: true,
                        required: true
                    },
                    UserSDT: {
                        phoneNumber: true, required: false,
                    },
                    UserEmail: {
                        email: true, required: false,
                    },
                },
                messages: {
                    Title: "Vui lòng nhập họ tên",
                    TaiKhoanTruyCap: "Vui lòng nhập tài khoản",
                    Roles: "Vui lòng chọn vai trò",
                    UserNgaySinh: "Vui lòng nhập ngày sinh",
                    AnhDaiDien: "Vui lòng chọn ảnh đại diện",
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/LUser/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-LUser").trigger("click");
                            $(form).closeModal();
                            $('#tblLUser').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-LUser").viForm();
        });
    </script>

</body>
</html>
