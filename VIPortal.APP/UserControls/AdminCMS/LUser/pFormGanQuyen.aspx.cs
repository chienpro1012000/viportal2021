﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData;

namespace VIPortal.APP.UserControls.AdminCMS.LUser
{
    public partial class pFormGanQuyen : pFormBase
    {
        public LUserItem oLUserItem = new LUserItem();
        public List<LGroupItem> LstGroups = new List<LGroupItem>();
        public int PerfldGroup { get; set; }
        public PermisonGroup oPermisonGroup { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oPermisonGroup = new PermisonGroup();
            if (ItemID > 0)
            {
                LUserDA oLuserDA = new LUserDA();
                oLUserItem = oLuserDA.GetByIdToObject<LUserItem>(ItemID);
                LGroupDA lGroupDA = new LGroupDA();
                int IDGroup = oLUserItem.Groups.LookupId;
                while (IDGroup > 0)
                {
                    LGroupItem temp = lGroupDA.GetByIdToObjectSelectFields<LGroupItem>(IDGroup, "ID", "Title", "GroupParent");
                    LstGroups.Add(temp);
                    if (temp.GroupParent != null && temp.GroupParent.LookupId > 0)
                        IDGroup = temp.GroupParent.LookupId;
                    else IDGroup = 0;
                }

                //add thêm một các loại có kiểu bẳng 9.
                List<LGroupJson> lstSiteKhac = lGroupDA.GetListJson(new LGroupQuery() { KieuDonVi = 9 });
                LstGroups.AddRange(lstSiteKhac.Select(x => new LGroupItem()
                {
                    ID = x.ID,
                    Title = x.Title
                }));
                if (!string.IsNullOrEmpty(Request["PerfldGroup"]))
                {
                    PerfldGroup = Convert.ToInt32(Request["PerfldGroup"]);
                }
                else PerfldGroup = LstGroups.FirstOrDefault().ID;
                if (!string.IsNullOrEmpty(oLUserItem.PerJson))
                {
                    List<PermisonGroup> permisonGroups = new List<PermisonGroup>();
                    permisonGroups = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PermisonGroup>>(oLUserItem.PerJson);
                    
                    oPermisonGroup = permisonGroups.FirstOrDefault(x => x.fldGroup == PerfldGroup);
                    if (oPermisonGroup == null) oPermisonGroup = new PermisonGroup();


                    //string.Join(",", oPermisonGroup.Roles?.Select(x => x.ID));
                }
                

            }
        }
    }
}