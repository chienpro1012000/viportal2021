﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormChoieUsers.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.LUser.pFormChoieUsers" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        $(document).ready(function () {


            $('#treeLgroup').fancytree({
                //  extensions: ['contextMenu'],
                source: {
                    url: "/VIPortalAPI/api/Lgroup/QUERYTREE?"
                },
                renderNode: function (event, data) {
                    //console.log(data);
                    var node = data.node;
                    $(node.span).attr('data-id', data.node.key);

                },
                activate: function (event, data) {
                    // A node was activated: display its title:
                    //var node = data.node;
                    //var tree = $('#treePhongBan').fancytree('getTree');
                    //tree.reload({
                    //    url: "/VIPortalAPI/api/Lgroup/QUERYTREE?isGetParent=true&isGetByParent=true&GroupIsDonVi=2&GroupParent=" + node.key
                    //});
                    $('#UserPhongBan').val(data.node.key);
                    //$("#btnAddUser").attr("data-UserPhongBan", data.node.key);
                    $("#btnAddUser").data('UserPhongBan', data.node.key);
                    $('#tblLUser').DataTable().ajax.reload();
                },

            });
            //tblLUserChoie
            var $tblLUserChoie = $("#tblLUserChoie").viDataTable(
                {
                    "frmSearch": "LUserSearchChoie",
                    "paging": false,
                    "length": 100,
                    "url": "/UserControls/AdminCMS/LUser/pAction.asp",
                    "aoColumns":
                        [
                            {
                                "mData": function (o) {
                                    return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                                },
                                "name": "Title", "sTitle": "Họ và tên"
                            }, {
                                "sTitle": '<input class="checked-all" type="checkbox" />',
                                "mData": null,
                                "bSortable": false,
                                "sWidth": "18px",
                                "className": "all",
                                "mRender": function (o) { return '<a data-id="' + o.ID + '" data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-ItemID ="' + o.ID + '" data-do="DELETE" href = "javascript:;" > <i class="far fa-trash-alt"></i></a > '; }
                            }
                        ],
                    "order": [[0, "asc"]]
                });

            var $tblLUser = $("#tblLUser").viDataTable(
                {
                    "frmSearch": "LUserSearch",
                    "paging": false,
                    "length": 100,
                    "url": "/UserControls/AdminCMS/LUser/pAction.asp",
                    "aoColumns":
                        [
                            {
                                "mData": function (o) {
                                    return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                                },
                                "name": "Title", "sTitle": "Họ và tên"
                            }, {
                                "sTitle": '<input class="checked-alluser" type="checkbox" />',
                                "mData": null,
                                "bSortable": false,
                                "sWidth": "18px",
                                "className": "all",
                                "mRender": function (o) { return '<input class="check-item" type="checkbox" data-title="' + o.Title + '" data-id="' + o.ID + '" />'; }
                            }
                        ],
                    "order": [[0, "asc"]]
                });
            //alert('Table redrawn');
            $tblLUser.on('draw', function () {
                //đăng ký sự kiện cho checkbox
                $('#tblLUser .check-item').click(function () {
                    if ($(this).is(':checked')) {
                        var $itemid = $(this).attr("data-id");
                        var $lstIDget = $("#lstIDget").val();
                        if ($lstIDget.indexOf("," + $itemid + ",") == -1) {
                            $lstIDget += ($itemid + ',');
                            $("#lstIDget").val($lstIDget);
                        }

                    } else {
                        //unchecked
                        var $itemid = $(this).attr("data-id");
                        var $lstIDget = $("#lstIDget").val();
                        $lstIDget = $lstIDget.replace("," + $itemid + ",", ",");
                        $("#lstIDget").val($lstIDget);
                    }
                    //refresh lại grid.
                    $('#tblLUserChoie').DataTable().ajax.reload();
                });
                
            });
            //init.dt
            $tblLUser.on('init.dt', function () {
                //checked-alluser
                $('#tblLUser .checked-alluser').click(function () {
                    $('#tblLUser .check-item').prop('checked', this.checked);
                    if ($(this).is(':checked')) {
                        var allcheckbox = $('#tblLUser .check-item');
                        allcheckbox.each(function (i, obj) {
                            var $itemid = $(obj).attr("data-id");
                            var $lstIDget = $("#lstIDget").val();
                            if ($lstIDget.indexOf("," + $itemid + ",") == -1) {
                                $lstIDget += ($itemid + ',');
                                $("#lstIDget").val($lstIDget);
                            }
                        });
                    } else {
                        var allcheckbox = $('#tblLUser .check-item');
                        allcheckbox.each(function (i, obj) {
                            var $itemid = $(obj).attr("data-id");
                            var $lstIDget = $("#lstIDget").val();
                            $lstIDget = $lstIDget.replace("," + $itemid + ",", ",");
                            $("#lstIDget").val($lstIDget);
                        });
                    }
                    $('#tblLUserChoie').DataTable().ajax.reload();
                });

            });
            $("#frm-pFormChoieUsers").validate({
                submitHandler: function (form) {
                    //xử lý ở đoạn này.
                    //alert(23123);
                    const UsersChoie = [];
                    var alltitles = $("#tblLUserChoie .act-view");
                    alltitles.each(function (i, obj) {
                        //test
                        UsersChoie.push({ "ID": $(obj).attr("data-itemid"), "Title": $(obj).text() });
                    });
                    var TitleChoie = $.map(UsersChoie, function (element, index) { return element.Title });

                    $("#UserChoie").val(TitleChoie.join(","));
                    $("#lstNguoiDung").val(JSON.stringify(UsersChoie));
                    //set id va obj
                    $(form).closeModal();
                }
            });
        });
    </script>
</head>
<body>
    <form id="frm-pFormChoieUsers" class="form-horizontal">

        <div class="container-fluid">
            <!-- Control the column width, and how they should appear on different devices -->
            <div class="row">
                <div class="col-sm-4">
                    <h5>Cơ cấu tổ chức</h5>
                    <div id="treeLgroup"></div>
                </div>

                <div class="col-sm-4">
                    <div role="body-data" data-title="Người dùng" class="content_wp" data-view="/UserControls/AdminCMS/LUser/pViewLUser.aspx" data-action="/UserControls/AdminCMS/LUser/pAction.asp" data-form="/UserControls/AdminCMS/LUser/pFormLUser.aspx">
                        <div class="clsmanager row">
                            <div class="col-sm-9">
                                <div id="LUserSearch" class="form-inline zonesearch">
                                    <div class="form-group">
                                        <input type="hidden" name="do" id="do" value="QUERYDATA" />
                                        <input type="hidden" name="UserPhongBan" id="UserPhongBan" value="0" />
                                        <input type="hidden" name="GetUserPhongBan" id="GetUserPhongBan" value="true" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clsgrid table-responsive">
                            <table id="tblLUser" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                            </table>

                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <h5>Người được chọn</h5>
                    <input type="hidden" value="" id="TitleUserChoie" />
                    <div role="body-data" data-title="Người dùng" class="content_wp" data-view="/UserControls/AdminCMS/LUser/pViewLUser.aspx" data-action="/UserControls/AdminCMS/LUser/pAction.asp" data-form="/UserControls/AdminCMS/LUser/pFormLUser.aspx">
                        <div class="clsmanager row">

                            <div class="col-sm-9">
                                <div id="LUserSearchChoie" class="form-inline zonesearch">
                                    <div class="form-group">
                                        <input type="hidden" name="do" value="QUERYDATA" />
                                        <input type="hidden" name="lstIDget" id="lstIDget" value="," />
                                        <input type="hidden" name="isGetBylistID" id="isGetBylistID" value="true" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clsgrid table-responsive">
                            <table id="tblLUserChoie" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>


    </form>
</body>
</html>
