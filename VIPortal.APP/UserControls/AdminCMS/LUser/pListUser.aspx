﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pListUser.aspx.cs" Inherits="VIPortal.APP.UserControls.AdminCMS.LUser.pListUser" %>

<div role="body-data" data-title="" class="content_wp" data-action="/UserControls/LUser/pAction.ashx" data-form="/UserControls/LUser/pFormLUser.aspx" data-view="/UserControls/AdminCMS/LUser/pViewLUser.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="LUserSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title" />
                            <input type="hidden" name="Roles" id="Roles" value="<%=ItemID %>" />
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <button class="btn btn-primary btnfrmnochek" title="Thêm mới người dùng" url="/UserControls/AdminCMS/LUser/pGanVaiTroNguoi.aspx" data-ItemID="<%=ItemID %>" size="1024" data-per="080102" type="button">Thêm mới</button>
                    </p>
                </div>
            </div>

            <div class="clsgrid table-responsive">

                <table id="tblLUser" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        var $tagvalue = $(".content_wp");
        var $tblDanhMucChung = $("#tblLUser").viDataTable(
            {
                "frmSearch": "LUserSearch",
                "url": "/UserControls/AdminCMS/LUser/pAction.ashx",
                "aoColumns":
                    [
                        {
                            "mData": function (o) {
                                return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                            },
                            "name": "Title", "sTitle": "Họ và tên"
                        },
                        {
                            "data": "TaiKhoanTruyCap",
                            "name": "TaiKhoanTruyCap", "sTitle": "Tài khoản",
                            "sWidth": "90px",
                        }, {
                            "data": "UserChucVu",
                            "name": "UserChucVu", "sTitle": "Chức vụ",
                            "sWidth": "90px",
                        }, {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE" title="Xóa"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }, {
                             "sTitle": '<input class="checked-all" type="checkbox" />',
                             "mData": null,
                             "bSortable": false,
                             "sWidth": "18px",
                             "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                "aaSorting": [0, 'asc']
            });
    });
</script>
