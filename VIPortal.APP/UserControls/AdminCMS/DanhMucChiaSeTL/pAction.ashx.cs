using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortalAPP.DanhMucChiaSeTL
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {        
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);
            if (!string.IsNullOrEmpty(context.Request["do"]))
                doAction = context.Request["do"].ToUpper();

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            DanhMucChiaSeTLDA oDanhMucChiaSeTLDA = new DanhMucChiaSeTLDA();
            DanhMucChiaSeTLItem oDanhMucChiaSeTLItem = new DanhMucChiaSeTLItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oDanhMucChiaSeTLDA.GetListJson(new DanhMucChiaSeTLQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw, 
                        oDanhMucChiaSeTLDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oDanhMucChiaSeTLDA.GetListJson(new DanhMucChiaSeTLQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDanhMucChiaSeTLItem = oDanhMucChiaSeTLDA.GetByIdToObject<DanhMucChiaSeTLItem>(oGridRequest.ItemID);
                        oDanhMucChiaSeTLItem.UpdateObject(context.Request);
                        oDanhMucChiaSeTLDA.UpdateObject<DanhMucChiaSeTLItem>(oDanhMucChiaSeTLItem);
                    }
                    else
                    {
                        oDanhMucChiaSeTLItem.UpdateObject(context.Request);
                        oDanhMucChiaSeTLDA.UpdateObject<DanhMucChiaSeTLItem>(oDanhMucChiaSeTLItem);
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    string outb = oDanhMucChiaSeTLDA.DeleteObject(oGridRequest.ItemID);
                    if (string.IsNullOrEmpty(outb))
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb;
                    break;
                case "DELETE-MULTI":
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oDanhMucChiaSeTLDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                     oDanhMucChiaSeTLDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDanhMucChiaSeTLItem = oDanhMucChiaSeTLDA.GetByIdToObjectSelectFields<DanhMucChiaSeTLItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oDanhMucChiaSeTLItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}