<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewDanhMucChiaSeTL.aspx.cs" Inherits="VIPortalAPP.pViewDanhMucChiaSeTL" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
               <%=oDanhMucChiaSeTL.Title%>
            </div>
        </div>
		<div class="form-group">
	<label for="DMDescription" class="col-sm-2 control-label">DMDescription</label>
	<div class="col-sm-10">
		<%=oDanhMucChiaSeTL.DMDescription%>
	</div>
</div>
<div class="form-group">
	<label for="DMHienThi" class="col-sm-2 control-label">DMHienThi</label>
	<div class="col-sm-10">
		<%=oDanhMucChiaSeTL.DMHienThi? "Có" : "Không"%>
	</div>
</div>
<div class="form-group">
	<label for="DMSTT" class="col-sm-2 control-label">DMSTT</label>
	<div class="col-sm-10">
		<%=oDanhMucChiaSeTL.DMSTT%>
	</div>
</div>
<div class="form-group">
	<label for="DMVietTat" class="col-sm-2 control-label">DMVietTat</label>
	<div class="col-sm-10">
		<%=oDanhMucChiaSeTL.DMVietTat%>
	</div>
</div>

		  <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-10">
               <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oDanhMucChiaSeTL.Created)%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oDanhMucChiaSeTL.Modified)%>
            </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-10">
                <%=oDanhMucChiaSeTL.Author.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-10">
                <%=oDanhMucChiaSeTL.Editor.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oDanhMucChiaSeTL.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oDanhMucChiaSeTL.ListFileAttach[i].Url %>"><%=oDanhMucChiaSeTL.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>