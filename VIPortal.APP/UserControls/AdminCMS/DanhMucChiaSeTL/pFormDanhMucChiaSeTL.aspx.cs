using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortalAPP
{
    public partial class pFormDanhMucChiaSeTL : pFormBase
    {
        public DanhMucChiaSeTLItem oDanhMucChiaSeTL {get;set;}
        protected void Page_Load(object sender, EventArgs e)
        {
			oDanhMucChiaSeTL = new DanhMucChiaSeTLItem();
            if (ItemID > 0)
            {
                DanhMucChiaSeTLDA oDanhMucChiaSeTLDA = new DanhMucChiaSeTLDA(UrlSite);
                oDanhMucChiaSeTL = oDanhMucChiaSeTLDA.GetByIdToObject<DanhMucChiaSeTLItem>(ItemID);
            }
        }
    }
}