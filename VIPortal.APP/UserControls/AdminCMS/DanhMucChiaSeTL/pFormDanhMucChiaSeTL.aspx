<%@ page language="C#" autoeventwireup="true" codebehind="pFormDanhMucChiaSeTL.aspx.cs" inherits="VIPortalAPP.pFormDanhMucChiaSeTL" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-DanhMucChiaSeTL" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oDanhMucChiaSeTL.ID %>" />
        <input type="hidden" name="fldGroup" id="fldGroup" value="<%=oDanhMucChiaSeTL.fldGroup.LookupId %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oDanhMucChiaSeTL.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="DMDescription" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <textarea name="DMDescription" id="DMDescription" class="form-control"><%:oDanhMucChiaSeTL.DMDescription%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="DMSTT" class="col-sm-2 control-label">STT</label>
            <div class="col-sm-4">
                <input type="text" name="DMSTT" id="DMSTT" placeholder="Nhập dmstt" value="<%:oDanhMucChiaSeTL.DMSTT%>" class="form-control" />
            </div>
            <label for="DMVietTat" class="col-sm-2 control-label">Viết tắt</label>
            <div class="col-sm-4">
                <input type="text" name="DMVietTat" id="DMVietTat" placeholder="Nhập dmviettat" value="<%:oDanhMucChiaSeTL.DMVietTat%>" class="form-control" />
            </div>
        </div>

        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oDanhMucChiaSeTL.ListFileAttach)%>'
            });
            $("#Title").focus();

            $("#frm-DanhMucChiaSeTL").validate({
                rules: {
                    Title: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    $.post("/UserControls//AdminCMS/DanhMucChiaSeTL/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-DanhMucChiaSeTL").trigger("click");
                            $(form).closeModal();
                            $('#tblDanhMucChiaSeTL').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
        });
    </script>
</body>
</html>
