using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ViPortal_Utils.Base;

namespace VIPortalAPP.OssiDataType
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : IHttpHandler
    {
        public string doAction = string.Empty;
        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            if (!string.IsNullOrEmpty(context.Request["do"]))
                doAction = context.Request["do"].ToUpper();

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            OssiDataTypeDA oOssiDataTypeDA = new OssiDataTypeDA();
            OssiDataTypeItem oOssiDataTypeItem = new OssiDataTypeItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oOssiDataTypeDA.GetListJson(new OssiDataTypeQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw, 
                        oOssiDataTypeDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oOssiDataTypeDA.GetListJson(new OssiDataTypeQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oOssiDataTypeItem = oOssiDataTypeDA.GetByIdToObject<OssiDataTypeItem>(oGridRequest.ItemID);
                        oOssiDataTypeItem.UpdateObject(context.Request);
                        oOssiDataTypeDA.UpdateObject<OssiDataTypeItem>(oOssiDataTypeItem);
                    }
                    else
                    {
                        oOssiDataTypeItem.UpdateObject(context.Request);
                        oOssiDataTypeDA.UpdateObject<OssiDataTypeItem>(oOssiDataTypeItem);
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    string outb = oOssiDataTypeDA.DeleteObject(oGridRequest.ItemID);
                    if (string.IsNullOrEmpty(outb))
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb;
                    break;
                case "DELETE-MULTI":
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oOssiDataTypeDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                     oOssiDataTypeDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oOssiDataTypeItem = oOssiDataTypeDA.GetByIdToObjectSelectFields<OssiDataTypeItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oOssiDataTypeItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}