﻿using System;
using System.IO;
using System.Web.UI;
using DevExpress.Spreadsheet;
using DevExpress.Web;
using DevExpress.Web.ASPxSpreadsheet;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData;
using ViPortal_Utils.Base;
using System.Collections.Generic;

namespace VIPortal.APP.UserControls.BaoCao.OssiDataType
{

    public partial class pFormCauHinhMauBaoCaoV2 : pFormBase
    {
        //public DMBaoCaoItem oDMBaoCao { get; set; }
        public DMBaoCaoItem oDMBaoCao { get; set; }
        public ConfigExcel JsonConfig { get; set; }
        public List<FileAttach> LstFileBCChiTiet { get; set; }
        public List<FileAttach> LstFileBCTongHop { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //string currentUserID = GenerateUniqueId();
                //var user = Users.Register(currentUserID);
                //ASPxSpreadsheet
                //var templatePath = Server.MapPath("~/App_Data/WorkDirectory/BaoCaoBoPhanTuan14.xlsx");
                //var documentId = GenerateUniqueId();
                //user.DocumentIDs.Add(documentId);
                //ASPxSpreadsheet1.Open(documentId, DocumentFormat.Xlsx);
            }
            oDMBaoCao = new DMBaoCaoItem();
            LstFileBCChiTiet = new List<FileAttach>();
            LstFileBCTongHop = new List<FileAttach>();
            JsonConfig = new ConfigExcel();
            if (ItemID > 0)
            {
                DMBaoCaoDA oDMBaoCaoDA = new DMBaoCaoDA();
                oDMBaoCao = oDMBaoCaoDA.GetByIdToObject<DMBaoCaoItem>(ItemID);
                if (!string.IsNullOrEmpty(oDMBaoCao.JsonConfig))
                {
                    JsonConfig = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigExcel>(oDMBaoCao.JsonConfig);
                }
                if (!string.IsNullOrEmpty(oDMBaoCao.UrlFileBCChiTiet))
                {
                    //write file ra ổ tạm để xử lý nếu cần.
                    LstFileBCChiTiet = new List<FileAttach>()
                    {
                        new FileAttach()
                        {
                            Url = oDMBaoCao.UrlFileBCChiTiet,
                            Name = Path.GetFileName(oDMBaoCao.UrlFileBCChiTiet)
                        }
                    };
                    LstFileBCTongHop = new List<FileAttach>()
                    {
                        new FileAttach()
                        {
                            Url = oDMBaoCao.UrlBaoCaoTongHop,
                            Name = Path.GetFileName(oDMBaoCao.UrlBaoCaoTongHop)
                        }
                    };
                }

            }
        }
        private static string GenerateUniqueId()
        {
            return Guid.NewGuid().ToString();
        }

    }
}