<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewOssiDataType.aspx.cs" Inherits="VIPortalAPP.pViewOssiDataType" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
               <%=oOssiDataType.Title%>
            </div>
        </div>
		<div class="form-group">
	<label for="dataName" class="col-sm-2 control-label">dataName</label>
	<div class="col-sm-10">
		<%=oOssiDataType.dataName%>
	</div>
</div>
<div class="form-group">
	<label for="dataKieuDuLieu" class="col-sm-2 control-label">dataKieuDuLieu</label>
	<div class="col-sm-10">
		<%=oOssiDataType.dataKieuDuLieu%>
	</div>
</div>
<div class="form-group">
	<label for="dataDefaultValue" class="col-sm-2 control-label">dataDefaultValue</label>
	<div class="col-sm-10">
		<%=oOssiDataType.dataDefaultValue%>
	</div>
</div>
<div class="form-group">
	<label for="dataSTT" class="col-sm-2 control-label">dataSTT</label>
	<div class="col-sm-10">
		<%=oOssiDataType.dataSTT%>
	</div>
</div>
<div class="form-group">
	<label for="dataDescription" class="col-sm-2 control-label">dataDescription</label>
	<div class="col-sm-10">
		<%=oOssiDataType.dataDescription%>
	</div>
</div>
<div class="form-group">
	<label for="dataReadOnly" class="col-sm-2 control-label">dataReadOnly</label>
	<div class="col-sm-10">
		<%=oOssiDataType.dataReadOnly? "Có" : "Không"%>
	</div>
</div>
<div class="form-group">
	<label for="dataRequired" class="col-sm-2 control-label">dataRequired</label>
	<div class="col-sm-10">
		<%=oOssiDataType.dataRequired? "Có" : "Không"%>
	</div>
</div>
<div class="form-group">
	<label for="dataFormat" class="col-sm-2 control-label">dataFormat</label>
	<div class="col-sm-10">
		<%=oOssiDataType.dataFormat%>
	</div>
</div>
<div class="form-group">
	<label for="dataIsDateOnly" class="col-sm-2 control-label">dataIsDateOnly</label>
	<div class="col-sm-10">
		<%=oOssiDataType.dataIsDateOnly? "Có" : "Không"%>
	</div>
</div>
<div class="form-group">
	<label for="dataEnum" class="col-sm-2 control-label">dataEnum</label>
	<div class="col-sm-10">
		<%=oOssiDataType.dataEnum%>
	</div>
</div>
<div class="form-group">
	<label for="dataMinLength" class="col-sm-2 control-label">dataMinLength</label>
	<div class="col-sm-10">
		<%=oOssiDataType.dataMinLength%>
	</div>
</div>
<div class="form-group">
	<label for="dataMaxLength" class="col-sm-2 control-label">dataMaxLength</label>
	<div class="col-sm-10">
		<%=oOssiDataType.dataMaxLength%>
	</div>
</div>
<div class="form-group">
	<label for="dataMinimum" class="col-sm-2 control-label">dataMinimum</label>
	<div class="col-sm-10">
		<%=oOssiDataType.dataMinimum%>
	</div>
</div>
<div class="form-group">
	<label for="dataMaximum" class="col-sm-2 control-label">dataMaximum</label>
	<div class="col-sm-10">
		<%=oOssiDataType.dataMaximum%>
	</div>
</div>
<div class="form-group">
	<label for="dataListLookupName" class="col-sm-2 control-label">dataListLookupName</label>
	<div class="col-sm-10">
		<%=oOssiDataType.dataListLookupName%>
	</div>
</div>
<div class="form-group">
	<label for="dataListLookupUrl" class="col-sm-2 control-label">dataListLookupUrl</label>
	<div class="col-sm-10">
		<%=oOssiDataType.dataListLookupUrl%>
	</div>
</div>
<div class="form-group">
	<label for="BaoCaoDanhMuc" class="col-sm-2 control-label">BaoCaoDanhMuc</label>
	<div class="col-sm-10">
		<%=oOssiDataType.BaoCaoDanhMuc%>
	</div>
</div>
<div class="form-group">
	<label for="dataIsMultiLookup" class="col-sm-2 control-label">dataIsMultiLookup</label>
	<div class="col-sm-10">
		<%=oOssiDataType.dataIsMultiLookup? "Có" : "Không"%>
	</div>
</div>
<div class="form-group">
	<label for="dataRowMulti" class="col-sm-2 control-label">dataRowMulti</label>
	<div class="col-sm-10">
		<%=oOssiDataType.dataRowMulti%>
	</div>
</div>
<div class="form-group">
	<label for="dataTitleRows" class="col-sm-2 control-label">dataTitleRows</label>
	<div class="col-sm-10">
		<%=oOssiDataType.dataTitleRows%>
	</div>
</div>
<div class="form-group">
	<label for="dataTitleColumns" class="col-sm-2 control-label">dataTitleColumns</label>
	<div class="col-sm-10">
		<%=oOssiDataType.dataTitleColumns%>
	</div>
</div>
<div class="form-group">
	<label for="ControlHTML" class="col-sm-2 control-label">ControlHTML</label>
	<div class="col-sm-10">
		<%=oOssiDataType.ControlHTML%>
	</div>
</div>

		  <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-10">
               <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oOssiDataType.Created)%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oOssiDataType.Modified)%>
            </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-10">
                <%=oOssiDataType.Author.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-10">
                <%=oOssiDataType.Editor.LookupValue%>
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oOssiDataType.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oOssiDataType.ListFileAttach[i].Url %>"><%=oOssiDataType.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>