﻿using System;
using System.IO;
using System.Web.UI;
using DevExpress.Spreadsheet;
using DevExpress.Web;
using DevExpress.Web.ASPxSpreadsheet;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData;
using ViPortal_Utils.Base;

namespace VIPortal.APP.UserControls.BaoCao.OssiDataType
{

    public partial class pFormCauHinhMauBaoCao : pFormBase
    {
        //public DMBaoCaoItem oDMBaoCao { get; set; }
        public DMBaoCaoItem oDMBaoCao { get; set; }
        public ConfigExcel JsonConfig { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oDMBaoCao = new DMBaoCaoItem();
            JsonConfig = new ConfigExcel();
            if (ItemID > 0)
            {
                DMBaoCaoDA oDMBaoCaoDA = new DMBaoCaoDA();
                oDMBaoCao = oDMBaoCaoDA.GetByIdToObject<DMBaoCaoItem>(ItemID);
                if (!string.IsNullOrEmpty(oDMBaoCao.JsonConfig))
                {
                    JsonConfig = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigExcel>(oDMBaoCao.JsonConfig);
                }
            }
        }

    }
}