﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormCongThuc.aspx.cs" Inherits="VIPortal.APP.UserControls.BaoCao.OssiDataType.pFormCongThuc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#frm-CongThuc").submit(function (event) {
                event.preventDefault();
                var objData = { KeyCell: $("#KeyCell").val(), CongThuc: $("#CongThuc").val() };
                console.log(objData);
                var dataConfig = $("#DataJsonCell").data("ConfigCell");
                if (dataConfig == undefined) dataConfig = [];
                var added = false;
                $.map(dataConfig, function (elementOfArray, indexInArray) {
                    if (elementOfArray.KeyCell == objData.KeyCell) {
                        dataConfig[indexInArray] = objData;
                        added = true;
                    }
                });
                if (!added) {
                    dataConfig.push(objData);
                }
                console.log(dataConfig);
                $("#DataJsonCell").data("ConfigCell", dataConfig);
                $("#frm-CongThuc").closeModal();
            });
            $("#frm-CongThuc").closest('.bootstrap-dialog').on('hidden.bs.modal', function (event) {
                console.log(123);
                if ($('.modal:visible').length) {
                    $('body').addClass('modal-open');
                }
            });
        });
    </script>
</head>
<body>
    <form id="frm-CongThuc" class="form-horizontal">
        <input type="hidden" name="KeyCell" id="KeyCell" value="<%=KeyCell %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Công thức</label>
            <div class="col-sm-10">
                <input type="text" name="CongThuc" id="CongThuc" class="form-control" value="" />
            </div>
        </div>
    </form>
</body>
</html>
