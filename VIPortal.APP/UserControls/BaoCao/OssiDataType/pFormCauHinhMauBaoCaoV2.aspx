﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormCauHinhMauBaoCaoV2.aspx.cs" Inherits="VIPortal.APP.UserControls.BaoCao.OssiDataType.pFormCauHinhMauBaoCaoV2" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpreadsheet.v21.1, Version=21.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpreadsheet" TagPrefix="dx" %>


<form id="frmOssiDataType" class="form-horizontal">
    <input type="hidden" name="ItemID" id="ItemID" value="<%=ItemID %>" />
    <input type="hidden" name="do" id="do" value="<%=doAction %>" />
    
    <div class="form-group row">
        <label for="Title" class="col-sm-2 control-label">Phân loại tổng hợp</label>
        <div class="col-sm-10">
            <select id="PhanLoaiHinhThucTongHop" name="PhanLoaiHinhThucTongHop" class="form-control">
                <option value="0">Tính công thức tức Báo cáo chi tiết</option>
                <option value="1">Báo cáo gộp dòng từ BC chi tiết</option>
                <option value="3">.....</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="Type1_DongBatDau" class="col-sm-2 control-label">Dòng bắt đầu</label>
        <div class="col-sm-4">
            <input type="text" name="Type1_DongBatDau" id="Type1_DongBatDau" class="form-control" value="<%=JsonConfig.Type1_DongBatDau %>" />
        </div>
        <label for="Type1_DongKetThuc" class="col-sm-2 control-label">Dòng kết thúc</label>
        <div class="col-sm-4">
            <input type="text" name="Type1_DongKetThuc" id="Type1_DongKetThuc" class="form-control" value="<%=JsonConfig.Type1_DongKetThuc %>" />
        </div>
    </div>
     <div class="form-group row">
        <label for="Type1_DongBatDau" class="col-sm-2 control-label">Dòng bắt đầu Fotter</label>
        <div class="col-sm-4">
            <input type="text" name="Type1_DongBatDauFotter" id="Type1_DongBatDauFotter" class="form-control" value="<%=JsonConfig.Type1_DongBatDauFotter %>" />
        </div>
        <label for="Type1_DongKetThuc" class="col-sm-2 control-label">Dòng kết thúc Fotter</label>
        <div class="col-sm-4">
            <input type="text" name="Type1_DongKetThucFotter" id="Type1_DongKetThucFotter" class="form-control" value="<%=JsonConfig.Type1_DongKetThucFotter %>" />
        </div>
    </div>
     <div class="form-group row">
         <label for="Type1_HasSTT" class="col-sm-2 control-label">STT Tự động</label>
        <div class="col-sm-4">
            <input type="checkbox" name="Type1_HasSTT" id="Type1_HasSTT" <%=JsonConfig.Type1_HasSTT ? "checked" : string.Empty%> value="True" />
        </div>
        <label for="Type1_STT" class="col-sm-2 control-label">Có cột STT</label>
        <div class="col-sm-4">
            <input type="text" name="Type1_STT" id="Type1_STT" class="form-control" value="<%=JsonConfig.Type1_STT %>" />
        </div>
    </div>
    <div class="form-group row">
        <label for="Title" class="col-sm-2 control-label">Biểu mẫu chi tiết</label>
        <div class="col-sm-10">
             <input type="file" id="FileAttachCT" name="FileAttachCT" multiple="multiple" />
        </div>
    </div>
    
    <div class="form-group row">
        <label for="Title" class="col-sm-2 control-label">Biểu mẫu Tổng hợp</label>
        <div class="col-sm-10">
             <input type="file" id="FileAttachTH" name="FileAttachTH" multiple="multiple" />
        </div>
    </div>
    
</form>


<script type="text/javascript">
    function EditDevExpress(urlfile) {
        //alert(urlfile);
        //http://viportal202150.vn/UserControls/DevExpressExcel.aspx?urlfile=/Uploads/ajaxUpload//39c38877-e922-4c34-bf67-87f78dfdd4c0.xlsx
        window.open(`/UserControls/DevExpressExcel.aspx?urlfile=${urlfile}`, '_blank');

    }
    function EditDevExpressOld(urlfile) {
        //alert(urlfile);
        //http://viportal202150.vn/UserControls/DevExpressExcel.aspx?urlfile=/Uploads/ajaxUpload//39c38877-e922-4c34-bf67-87f78dfdd4c0.xlsx
        window.open(`/UserControls/DevExpressExcel.aspx?do=UPDATEBIEUMAU&urlfile=${urlfile}`, '_blank');

    }
    $(function () {
        $("#PhanLoaiHinhThucTongHop").val("<%=oDMBaoCao.PhanLoaiHinhThucTongHop%>").change();
        $("#FileAttachCT").regFileUpload({
            dataBound: function ($row, value) {
                $row.find(".name").append('&nbsp;&nbsp;&nbsp;<a href="javascript:EditDevExpress(\'' + value.url + '\');" href="' + value.url + '" ><i class="far fa-edit"></i></a>');
                return $row;
            },
            files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(LstFileBCChiTiet)%>'
        });
        $("#FileAttachTH").regFileUpload({
            dataBound: function ($row, value) {
                $row.find(".name").append('&nbsp;&nbsp;&nbsp;<a href="javascript:EditDevExpress(\'' + value.url + '\');" href="' + value.url + '" ><i class="far fa-edit"></i></a>');
                return $row;
            },
            files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(LstFileBCTongHop)%>'
        });
        $("p.name").each(function () {
            var hreffile = $(this).find('a').attr('href');
            $(this).append('&nbsp;&nbsp;&nbsp;<a href="javascript:EditDevExpressOld(\'' + hreffile + '\');" href="' + hreffile + '" ><i class="far fa-edit"></i></a>');
        });
        //$("p.name").append('&nbsp;&nbsp;&nbsp;<a href="javascript:EditDevExpress(\'' + value.url + '\');" href="' + value.url + '" ><i class="far fa-edit"></i></a>');


        $("#frmOssiDataType").validate({
            rules: {
                Title: "required"
            },
            messages: {
                Title: "Vui lòng nhập tiêu đề"
            },
            submitHandler: function (form) {
                $.post("/UserControls/BaoCao/BCTongHop/DMBaoCao/pAction.asp", $(form).viSerialize(), function (result) {
                    if (result.State == 2) {
                        BootstrapDialog.show({
                            title: "Lỗi",
                            message: result.Message
                        });
                    }
                    else {
                        showMsg(result.Message);
                        //$("#btn-find-DMBaoCao").trigger("click");
                        $(form).closeModal();
                        $('#tblDMBaoCao').DataTable().ajax.reload();
                    }
                }).always(function () { });
            }
        });
    });
</script>
