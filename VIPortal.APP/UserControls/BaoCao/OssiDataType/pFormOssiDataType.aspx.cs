using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public partial class pFormOssiDataType : pFormBase
    {
        public OssiDataTypeItem oOssiDataType {get;set;}
        protected void Page_Load(object sender, EventArgs e)
        {
			oOssiDataType = new OssiDataTypeItem();
            if (ItemID > 0)
            {
                OssiDataTypeDA oOssiDataTypeDA = new OssiDataTypeDA();
                oOssiDataType = oOssiDataTypeDA.GetByIdToObject<OssiDataTypeItem>(ItemID);
            }
        }
    }
}