﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;

namespace VIPortal.APP.UserControls.BaoCao.OssiDataType
{
    public partial class pFormCongThuc : pFormBase
    {
        public string KeyCell { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            KeyCell = Request["KeyCell"];
        }
    }
}