<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormOssiDataType.aspx.cs" Inherits="VIPortalAPP.pFormOssiDataType" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-OssiDataType" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oOssiDataType.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oOssiDataType.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="dataName" class="col-sm-2 control-label">dataName</label>
            <div class="col-sm-10">
                <input type="text" name="dataName" id="dataName" placeholder="Nhập dataname" value="<%:oOssiDataType.dataName%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="dataKieuDuLieu" class="col-sm-2 control-label">dataKieuDuLieu</label>
            <div class="col-sm-10">
                <input type="text" name="dataKieuDuLieu" id="dataKieuDuLieu" placeholder="Nhập datakieudulieu" value="<%:oOssiDataType.dataKieuDuLieu%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="dataDefaultValue" class="col-sm-2 control-label">dataDefaultValue</label>
            <div class="col-sm-10">
                <input type="text" name="dataDefaultValue" id="dataDefaultValue" placeholder="Nhập datadefaultvalue" value="<%:oOssiDataType.dataDefaultValue%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="dataSTT" class="col-sm-2 control-label">dataSTT</label>
            <div class="col-sm-10">
                <input type="text" name="dataSTT" id="dataSTT" placeholder="Nhập datastt" value="<%:oOssiDataType.dataSTT%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="dataDescription" class="col-sm-2 control-label">dataDescription</label>
            <div class="col-sm-10">
                <textarea name="dataDescription" id="dataDescription" class="form-control"><%:oOssiDataType.dataDescription%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="dataReadOnly" class="col-sm-2 control-label">dataReadOnly</label>
            <div class="col-sm-10">
                <input value="true" type="checkbox" name="dataReadOnly" id="dataReadOnly" <%=oOssiDataType.dataReadOnly? "checked" : string.Empty%> />
            </div>
        </div>
        <div class="form-group row">
            <label for="dataRequired" class="col-sm-2 control-label">dataRequired</label>
            <div class="col-sm-10">
                <input value="true" type="checkbox" name="dataRequired" id="dataRequired" <%=oOssiDataType.dataRequired? "checked" : string.Empty%> />
            </div>
        </div>
        <div class="form-group row">
            <label for="dataFormat" class="col-sm-2 control-label">dataFormat</label>
            <div class="col-sm-10">
                <input type="text" name="dataFormat" id="dataFormat" placeholder="Nhập dataformat" value="<%:oOssiDataType.dataFormat%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="dataIsDateOnly" class="col-sm-2 control-label">dataIsDateOnly</label>
            <div class="col-sm-10">
                <input value="true" type="checkbox" name="dataIsDateOnly" id="dataIsDateOnly" <%=oOssiDataType.dataIsDateOnly? "checked" : string.Empty%> />
            </div>
        </div>
        <div class="form-group row">
            <label for="dataEnum" class="col-sm-2 control-label">dataEnum</label>
            <div class="col-sm-10">
                <textarea name="dataEnum" id="dataEnum" class="form-control"><%:oOssiDataType.dataEnum%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="dataMinLength" class="col-sm-2 control-label">dataMinLength</label>
            <div class="col-sm-10">
                <input type="text" name="dataMinLength" id="dataMinLength" placeholder="Nhập dataminlength" value="<%:oOssiDataType.dataMinLength%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="dataMaxLength" class="col-sm-2 control-label">dataMaxLength</label>
            <div class="col-sm-10">
                <input type="text" name="dataMaxLength" id="dataMaxLength" placeholder="Nhập datamaxlength" value="<%:oOssiDataType.dataMaxLength%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="dataMinimum" class="col-sm-2 control-label">dataMinimum</label>
            <div class="col-sm-10">
                <input type="text" name="dataMinimum" id="dataMinimum" placeholder="Nhập dataminimum" value="<%:oOssiDataType.dataMinimum%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="dataMaximum" class="col-sm-2 control-label">dataMaximum</label>
            <div class="col-sm-10">
                <input type="text" name="dataMaximum" id="dataMaximum" placeholder="Nhập datamaximum" value="<%:oOssiDataType.dataMaximum%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="dataListLookupName" class="col-sm-2 control-label">dataListLookupName</label>
            <div class="col-sm-10">
                <input type="text" name="dataListLookupName" id="dataListLookupName" placeholder="Nhập datalistlookupname" value="<%:oOssiDataType.dataListLookupName%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="dataListLookupUrl" class="col-sm-2 control-label">dataListLookupUrl</label>
            <div class="col-sm-10">
                <input type="text" name="dataListLookupUrl" id="dataListLookupUrl" placeholder="Nhập datalistlookupurl" value="<%:oOssiDataType.dataListLookupUrl%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="BaoCaoDanhMuc" class="col-sm-2 control-label">BaoCaoDanhMuc</label>
            <div class="col-sm-10">
                <input type="text" name="BaoCaoDanhMuc" id="BaoCaoDanhMuc" placeholder="Nhập baocaodanhmuc" value="<%:oOssiDataType.BaoCaoDanhMuc%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="dataIsMultiLookup" class="col-sm-2 control-label">dataIsMultiLookup</label>
            <div class="col-sm-10">
                <input value="true" type="checkbox" name="dataIsMultiLookup" id="dataIsMultiLookup" <%=oOssiDataType.dataIsMultiLookup? "checked" : string.Empty%> />
            </div>
        </div>
        <div class="form-group row">
            <label for="dataRowMulti" class="col-sm-2 control-label">dataRowMulti</label>
            <div class="col-sm-10">
                <input type="text" name="dataRowMulti" id="dataRowMulti" placeholder="Nhập datarowmulti" value="<%:oOssiDataType.dataRowMulti%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="dataTitleRows" class="col-sm-2 control-label">dataTitleRows</label>
            <div class="col-sm-10">
                <textarea name="dataTitleRows" id="dataTitleRows" class="form-control"><%:oOssiDataType.dataTitleRows%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="dataTitleColumns" class="col-sm-2 control-label">dataTitleColumns</label>
            <div class="col-sm-10">
                <textarea name="dataTitleColumns" id="dataTitleColumns" class="form-control"><%:oOssiDataType.dataTitleColumns%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="ControlHTML" class="col-sm-2 control-label">ControlHTML</label>
            <div class="col-sm-10">
                <textarea name="ControlHTML" id="ControlHTML" class="form-control"><%:oOssiDataType.ControlHTML%></textarea>
            </div>
        </div>

        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oOssiDataType.ListFileAttach)%>'
            });
            $("#Title").focus();
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
            $(".form-group select").select2({
                dropdownParent: $(".modal-body"),
                placeholder: "Chọn"
            });

            $("#frm-OssiDataType").validate({
                rules: {
                    Title: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/OssiDataType/pAction.ashx", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-OssiDataType").trigger("click");
                            $(form).closeModal();
                            $('#tblOssiDataType').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
        });
    </script>
</body>
</html>
