﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormCauHinhMauBaoCao.aspx.cs" Inherits="VIPortal.APP.UserControls.BaoCao.OssiDataType.pFormCauHinhMauBaoCao" %>


<form id="frm-OssiDataType" class="form-horizontal">
    <input type="hidden" name="ItemID" id="ItemID" value="<%=ItemID %>" />
    <input type="hidden" name="do" id="do" value="<%=doAction %>" />
    
    <div class="form-group row">
        <label for="Title" class="col-sm-2 control-label">Phân loại tổng hợp</label>
        <div class="col-sm-10">
            <select id="PhanLoaiHinhThucTongHop" name="PhanLoaiHinhThucTongHop" class="form-control">
                <option value="0">Tính công thức tức Báo cáo chi tiết</option>
                <option value="1">Báo cáo gộp dòng từ BC chi tiết</option>
                <option value="3">.....</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="Type1_DongBatDau" class="col-sm-2 control-label">Dòng bắt đầu</label>
        <div class="col-sm-4">
            <input type="text" name="Type1_DongBatDau" id="Type1_DongBatDau" class="form-control" value="<%=JsonConfig.Type1_DongBatDau %>" />
        </div>
        <label for="Type1_DongKetThuc" class="col-sm-2 control-label">Dòng kết thúc</label>
        <div class="col-sm-4">
            <input type="text" name="Type1_DongKetThuc" id="Type1_DongKetThuc" class="form-control" value="<%=JsonConfig.Type1_DongKetThuc %>" />
        </div>
    </div>
     <div class="form-group row">
        <label for="Type1_DongBatDau" class="col-sm-2 control-label">Dòng bắt đầu Fotter</label>
        <div class="col-sm-4">
            <input type="text" name="Type1_DongBatDauFotter" id="Type1_DongBatDauFotter" class="form-control" value="<%=JsonConfig.Type1_DongBatDauFotter %>" />
        </div>
        <label for="Type1_DongKetThuc" class="col-sm-2 control-label">Dòng kết thúc Fotter</label>
        <div class="col-sm-4">
            <input type="text" name="Type1_DongKetThucFotter" id="Type1_DongKetThucFotter" class="form-control" value="<%=JsonConfig.Type1_DongKetThucFotter %>" />
        </div>
    </div>
    <div class="form-group row">
        <label for="Title" class="col-sm-2 control-label">Biểu mẫu chi tiết</label>
        <div class="col-sm-10">
            <input type="file" name="files" id="fileForUpload" accept=".xlsx" />
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-12">
            <div id="spreadsheet" style="width: 100%;"></div>
        </div>
    </div>
    <div class="form-group row">
        <input type="hidden" name="DataJsonCell" id="DataJsonCell" />
        <button type="button" id="btnConfigCell" title="Cấu hình công thức" url="/UserControls/BaoCao/OssiDataType/pFormCongThuc.aspx" size="550" data-KeyCell="" class="btn btn-primary mb-2 btnfrmnochek">Công Thức</button>
    </div>
    <div class="form-group row">
        <label for="Title" class="col-sm-2 control-label">Biểu mẫu Tổng hợp</label>
        <div class="col-sm-10">
            <input type="file" name="files" id="fileForUploadTH" accept=".xlsx" />
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-12">
            <div id="spreadsheetTH" style="width: 100%;"></div>
        </div>
    </div>
</form>


<script type="text/javascript">
    $(function () {
        $("#PhanLoaiHinhThucTongHop").val("<%=oDMBaoCao.PhanLoaiHinhThucTongHop%>").change();

        $("#frm-OssiDataType").submit(function (event) {
            event.preventDefault();
            var spreadsheet = $("#spreadsheet").data("kendoSpreadsheet");
            var spreadsheetTH = $("#spreadsheetTH").data("kendoSpreadsheet");
            //console.log(spreadsheet._workbook);
            new kendo.ooxml.Workbook(spreadsheet._workbook).toDataURLAsync().then(function (dataURL) {
                //var base64ChiTiet = dataURL.split(";base64,")[1];

                new kendo.ooxml.Workbook(spreadsheetTH._workbook).toDataURLAsync().then(function (dataURL) {
                    var base64TH = dataURL.split(";base64,")[1];
                    // Post the base64 encoded content to the server which can save it.
                    var dataCT = spreadsheet.toJSON();
                    var jsonCT = JSON.stringify(dataCT, null, 2);

                    var dataTH = spreadsheetTH.toJSON();
                    var jsonTH = JSON.stringify(dataTH, null, 2);
                    var dataConfig = $("#DataJsonCell").data("ConfigCell");
                    if (dataConfig == undefined) dataConfig = [];
                    $.post("/UserControls/BaoCao/BCTongHop/DMBaoCao/pAction.asp", {
                        Type1_DongBatDauFotter: $("#Type1_DongBatDauFotter").val(),
                        Type1_DongKetThucFotter: $("#Type1_DongKetThucFotter").val(),
                        Type1_DongBatDau: $("#Type1_DongBatDau").val(),
                        Type1_DongKetThuc: $("#Type1_DongKetThuc").val(),
                        ConfigCell: JSON.stringify(dataConfig),
                        PhanLoaiHinhThucTongHop: $("#PhanLoaiHinhThucTongHop").val(),
                        //base64TH: base64TH,
                        jsonBCCT: jsonCT,
                        jsonBCTH: jsonTH,
                        do: "UPDATEEXCEL",
                        ItemID: "<%=ItemID%>",
                        fileName: "ExcelExport.xlsx"
                    }, function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-DMLinhVuc").trigger("click");
                            $("#frm-OssiDataType").closeModal();
                        }
                    }).always(function () { });
                });


            });
        });

        $("input#fileForUploadTH").change(function () {
            if ($("input#fileForUploadTH").val() == "") {
                return;
            } else {
                var file = document.getElementById("fileForUploadTH").files[0];

                if (file) {
                    var workbook = new kendo.spreadsheet.Workbook({});

                    workbook.fromFile(file).then(function () {
                        var jsonContent = workbook.toJSON();
                        var spreadsheet = $("#spreadsheetTH").getKendoSpreadsheet();
                        spreadsheet.fromJSON(jsonContent);
                        console.log(jsonContent);
                    })
                }
            }
            // your ajax submit
            $("input#fileForUploadTH").val("");
        });
        $("input#fileForUpload").change(function () {
            if ($("input#fileForUpload").val() == "") {
                return;
            } else {
                var file = document.getElementById("fileForUpload").files[0];

                if (file) {
                    var workbook = new kendo.spreadsheet.Workbook({});

                    workbook.fromFile(file).then(function () {
                        var jsonContent = workbook.toJSON();
                        var spreadsheet = $("#spreadsheet").getKendoSpreadsheet();
                        spreadsheet.fromJSON(jsonContent);
                        console.log(jsonContent);
                    })
                }
            }
            // your ajax submit
            $(".attachmentsUpload input.file").val("");
        });

        $("#spreadsheet").kendoSpreadsheet({
            excelExport: function (e) {
                // Prevent the default behavior which will prompt the user to save the generated file.
                e.preventDefault();

                // Get the Excel file as a data URL.
                //var dataURL = new kendo.ooxml.Workbook(e.workbook).toDataURLAsync();
                new kendo.ooxml.Workbook(e.workbook).toDataURLAsync().then(function (dataURL) {
                    var base64 = dataURL.split(";base64,")[1];

                    // Post the base64 encoded content to the server which can save it.
                    $.post("/UserControls/BaoCao/BCTongHop/KyBaoCao/pAction.asp", {
                        base64: base64,
                        do: "UPDATEEXCEL",
                        fileName: "ExcelExport.xlsx"
                    });
                });
                // Strip the data URL prologue.

            }
        });
        //spreadsheetTH
        $("#spreadsheetTH").kendoSpreadsheet({
            select: function (e) {
                console.log(e.range._ref);
                $("#btnConfigCell").data('KeyCell', `Cell_${e.range._ref.col}_${e.range._ref.row}`);
            },
            excelExport: function (e) {

                // Prevent the default behavior which will prompt the user to save the generated file.
                e.preventDefault();

                // Get the Excel file as a data URL.
                //var dataURL = new kendo.ooxml.Workbook(e.workbook).toDataURLAsync();
                new kendo.ooxml.Workbook(e.workbook).toDataURLAsync().then(function (dataURL) {
                    var base64 = dataURL.split(";base64,")[1];

                    // Post the base64 encoded content to the server which can save it.
                    $.post("/UserControls/BaoCao/BCTongHop/KyBaoCao/pAction.asp", {
                        base64: base64,
                        do: "UPDATEEXCEL",
                        fileName: "ExcelExport.xlsx"
                    });
                });
                // Strip the data URL prologue.

            }
        });


        if (<%=oDMBaoCao.LoaiBaoCao%> == 2) {
            $.post("/UserControls/BaoCao/BCTongHop/DMBaoCao/pAction.asp?", { "ItemID": "<%=oDMBaoCao.ID%>", "do": "GETJSONBCCT" }, function (result) {
                if (result.State != 2) {
                    var spreadsheet = $("#spreadsheet").data("kendoSpreadsheet");
                    spreadsheet.fromJSON(result);
                }
            });
            $.post("/UserControls/BaoCao/BCTongHop/DMBaoCao/pAction.asp?", { "ItemID": "<%=oDMBaoCao.ID%>", "do": "GETJSONBCTH" }, function (result) {
                if (result.State != 2) {
                    var spreadsheetTH = $("#spreadsheetTH").data("kendoSpreadsheet");
                    spreadsheetTH.fromJSON(result);
                }
            });
        }
    });
</script>
