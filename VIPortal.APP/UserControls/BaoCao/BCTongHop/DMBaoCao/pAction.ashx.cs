using Microsoft.SharePoint;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortalAPP.DMBaoCao
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {

        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            DMBaoCaoDA oDMBaoCaoDA = new DMBaoCaoDA();
            DMBaoCaoItem oDMBaoCaoItem = new DMBaoCaoItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oDMBaoCaoDA.GetListJson(new DMBaoCaoQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oDMBaoCaoDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "DMBaoCao", "QUERYDATA", 0, "Xem danh sách danh mục báo cáo");
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oDMBaoCaoDA.GetListJson(new DMBaoCaoQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "GETJSONBCCT":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDMBaoCaoItem = oDMBaoCaoDA.GetByIdToObject<DMBaoCaoItem>(oGridRequest.ItemID);
                        if (!string.IsNullOrEmpty(oDMBaoCaoItem.jsonBCCT))
                        {
                            JObject json = JObject.Parse(oDMBaoCaoItem.jsonBCCT);
                            oResult.OData = json;
                        }
                        else oResult = new ResultAction() { State = ActionState.Error, Message = "Không xác định cấu trúc báo cáo" };
                    }
                    else
                        oResult.Message = "Không xác định bản ghi cần lưu trữ.";
                    break;
                case "GETJSONBCTH":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDMBaoCaoItem = oDMBaoCaoDA.GetByIdToObject<DMBaoCaoItem>(oGridRequest.ItemID);
                        if (!string.IsNullOrEmpty(oDMBaoCaoItem.jsonBCTH))
                        {
                            JObject json = JObject.Parse(oDMBaoCaoItem.jsonBCTH);
                            oResult.OData = json;
                        }
                        else oResult = new ResultAction() { State = ActionState.Error, Message = "Không xác định cấu trúc báo cáo" };
                    }
                    else
                        oResult.Message = "Không xác định bản ghi cần lưu trữ.";
                    break;
                case "UPDATEEXCEL":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDMBaoCaoItem = oDMBaoCaoDA.GetByIdToObject<DMBaoCaoItem>(oGridRequest.ItemID);
                        oDMBaoCaoItem.UpdateObject(context.Request);
                        if (!string.IsNullOrEmpty(context.Request["listValueFileAttachCT"]))
                        {
                            List<FileAttach>  ListFileAttachAdd = new List<FileAttach>();
                            string strListFileAttach = context.Request["listValueFileAttachCT"];

                            List<FileAttachForm> ltsFileForm = Newtonsoft.Json.JsonConvert.DeserializeObject<List<FileAttachForm>>(strListFileAttach);
                            FileAttach fileAttach;
                            string filePath = "/Uploads/ajaxUpload/";
                            foreach (FileAttachForm fileForm in ltsFileForm)
                            {
                                fileAttach = new FileAttach();
                                fileAttach.Name = fileForm.FileName;
                                fileAttach.Url = filePath + fileForm.FileServer;
                                fileAttach.DataFile = clsFucUtils.ReadFile(HttpContext.Current.Server.MapPath(filePath + fileForm.FileServer));
                                ListFileAttachAdd.Add(fileAttach);
                                SPFolder myLibrary = oDMBaoCaoDA.SpListProcess.ParentWeb.Folders["Documents"];
                                SPFile spfile = myLibrary.Files.Add($"{oGridRequest.ItemID}_BCChiTiet.xlsx", fileAttach.DataFile, true);
                                oDMBaoCaoItem.UrlFileBCChiTiet = spfile.ServerRelativeUrl;
                            }
                            //string base64ChiTiet = context.Request["base64ChiTiet"];
                            //byte[] Database64ChiTiet = Convert.FromBase64String(base64ChiTiet);
                            //SPFolder myLibrary = oDMBaoCaoDA.SpListProcess.ParentWeb.Folders["Documents"];
                            //SPFile spfile = myLibrary.Files.Add($"{oGridRequest.ItemID}_BCChiTiet.xlsx", Database64ChiTiet, true);
                            //myLibrary.Update();
                            
                            // Commit 
                            //oDMBaoCaoItem.jsonBCCT = context.Request["jsonBCCT"];
                            //JObject jsonBCCT = JObject.Parse(oDMBaoCaoItem.jsonBCCT);
                            //JArray sheetsCT = (JArray)jsonBCCT["sheets"];
                            //JArray RowTongHop = (JArray)sheetsCT[0]["rows"];
                            //JArray sorted = new JArray(RowTongHop.OrderBy(obj => (int)obj["index"]));
                            //sheetsCT[0]["rows"] = sorted;
                            //oDMBaoCaoItem.jsonBCCT = Newtonsoft.Json.JsonConvert.SerializeObject(jsonBCCT, Formatting.Indented);
                        }

                        if (!string.IsNullOrEmpty(context.Request["listValueFileAttachTH"]))
                        {
                            List<FileAttach> ListFileAttachAdd = new List<FileAttach>();
                            string strListFileAttach = context.Request["listValueFileAttachTH"];

                            List<FileAttachForm> ltsFileForm = Newtonsoft.Json.JsonConvert.DeserializeObject<List<FileAttachForm>>(strListFileAttach);
                            FileAttach fileAttach;
                            string filePath = "/Uploads/ajaxUpload/";
                            foreach (FileAttachForm fileForm in ltsFileForm)
                            {
                                fileAttach = new FileAttach();
                                fileAttach.Name = fileForm.FileName;
                                fileAttach.Url = filePath + fileForm.FileServer;
                                fileAttach.DataFile = clsFucUtils.ReadFile(HttpContext.Current.Server.MapPath(filePath + fileForm.FileServer));
                                ListFileAttachAdd.Add(fileAttach);
                                SPFolder myLibrary = oDMBaoCaoDA.SpListProcess.ParentWeb.Folders["Documents"];
                                SPFile spfile = myLibrary.Files.Add($"{oGridRequest.ItemID}_BCTongHop.xlsx", fileAttach.DataFile, true);
                                oDMBaoCaoItem.UrlBaoCaoTongHop = spfile.ServerRelativeUrl;
                            }
                            //string base64TH = context.Request["base64TH"];
                            //byte[] Database64TH = Convert.FromBase64String(base64TH);
                            //SPFolder myLibrary = oDMBaoCaoDA.SpListProcess.ParentWeb.Folders["Documents"];
                            //SPFile spfile = myLibrary.Files.Add($"{oGridRequest.ItemID}_BCTongHop.xlsx", Database64TH, true);
                            //myLibrary.Update();
                            //oDMBaoCaoItem.UrlBaoCaoTongHop = spfile.ServerRelativeUrl;
                            //oDMBaoCaoItem.jsonBCTH = context.Request["jsonBCTH"];

                            //JObject jsonBCTH = JObject.Parse(oDMBaoCaoItem.jsonBCTH);
                            //JArray sheetsCT = (JArray)jsonBCTH["sheets"];
                            //JArray RowTongHop = (JArray)sheetsCT[0]["rows"];
                            //JArray sorted = new JArray(RowTongHop.OrderBy(obj => (int)obj["index"]));
                            //sheetsCT[0]["rows"] = sorted;
                            //oDMBaoCaoItem.jsonBCTH = Newtonsoft.Json.JsonConvert.SerializeObject(jsonBCTH, Formatting.Indented);

                            // Commit 

                        }
                        ConfigExcel configExcel = new ConfigExcel();
                        configExcel.UpdateObject(context.Request);
                        oDMBaoCaoItem.JsonConfig = Newtonsoft.Json.JsonConvert.SerializeObject(configExcel, Formatting.Indented);
                        oDMBaoCaoDA.UpdateOneOrMoreField(oGridRequest.ItemID, new Dictionary<string, object>() {
                            {"UrlBaoCaoTongHop",  oDMBaoCaoItem.UrlBaoCaoTongHop},
                            {"UrlFileBCChiTiet",  oDMBaoCaoItem.UrlFileBCChiTiet},
                            {"jsonBCTH",  oDMBaoCaoItem.jsonBCTH},
                            {"jsonBCCT",  oDMBaoCaoItem.jsonBCCT},
                            {"JsonConfig",  oDMBaoCaoItem.JsonConfig},
                            {"PhanLoaiHinhThucTongHop",  oDMBaoCaoItem.PhanLoaiHinhThucTongHop}
                        }, true);
                        oResult.Message = "Lưu thành công";
                    }
                    else
                        oResult.Message = "Không xác định bản ghi cần lưu trữ.";
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDMBaoCaoItem = oDMBaoCaoDA.GetByIdToObject<DMBaoCaoItem>(oGridRequest.ItemID);
                        oDMBaoCaoItem.UpdateObject(context.Request);
                        oDMBaoCaoItem.DonViSuDung = "," + oDMBaoCaoItem.DonViSuDung + ",";
                        oDMBaoCaoDA.UpdateObject<DMBaoCaoItem>(oDMBaoCaoItem);
                        AddLog("UPDATE", "DMBaoCao", "UPDATE", oGridRequest.ItemID, $"Cập nhật danh mục báo cáo{oDMBaoCaoItem.Title}");
                    }
                    else
                    {
                        oDMBaoCaoItem.UpdateObject(context.Request);
                        oDMBaoCaoDA.UpdateObject<DMBaoCaoItem>(oDMBaoCaoItem);
                        oDMBaoCaoItem.DonViSuDung = "," + oDMBaoCaoItem.DonViSuDung + ",";
                        AddLog("UPDATE", "DMBaoCao", "ADD", oGridRequest.ItemID, $"Thêm mới danh mục báo cáo{oDMBaoCaoItem.Title}");
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oDMBaoCaoDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "DMBaoCao", "DELETE", oGridRequest.ItemID, $"Xóa danh mục báo cáo{outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    break;
                case "DELETE-MULTI":
                    string outbmulti = "";
                    AddLog("DELETE-MULTI", "DMBaoCao", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều danh mục báo cáo");
                    if (oGridRequest.LstItemID.Count > 0)
                    {
                        foreach (int itemid in oGridRequest.LstItemID)
                        {
                            outbmulti = oDMBaoCaoDA.DeleteObject(itemid);
                            if (!string.IsNullOrEmpty(outbmulti))
                                oResult.Message += $"{itemid},";
                        }
                    }
                    if (!string.IsNullOrEmpty(oResult.Message))
                    {
                        oResult.State = ActionState.Error;
                        oResult.Message = "Xóa không thành công các bản ghi: " + oResult.Message;
                    }
                    else oResult.Message = "Xóa thành công";
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oDMBaoCaoDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "DMBaoCao", "APPROVED", oGridRequest.ItemID, $"Duyệt danh mục báo cáo{oDMBaoCaoDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oDMBaoCaoDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "DMBaoCao", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt danh mục báo cáo{oDMBaoCaoDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy Duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oDMBaoCaoItem = oDMBaoCaoDA.GetByIdToObjectSelectFields<DMBaoCaoItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oDMBaoCaoItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}