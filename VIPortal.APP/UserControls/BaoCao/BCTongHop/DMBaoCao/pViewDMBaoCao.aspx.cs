using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortalAPP
{
    public partial class pViewDMBaoCao : pFormBase
    {
        public DMBaoCaoItem oDMBaoCao = new DMBaoCaoItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oDMBaoCao = new DMBaoCaoItem();
            if (ItemID > 0)
            {
                DMBaoCaoDA oDMBaoCaoDA = new DMBaoCaoDA();
                oDMBaoCao = oDMBaoCaoDA.GetByIdToObject<DMBaoCaoItem>(ItemID);

            }
        }
    }
}