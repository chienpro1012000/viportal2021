<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewDMBaoCao.aspx.cs" Inherits="VIPortalAPP.pViewDMBaoCao" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
		 <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <%=oDMBaoCao.Title%>
            </div>
        </div>
        
        <div class="form-group row">            
            <label for="DMSTT" class="col-sm-2 control-label">DM STT</label>
            <div class="col-sm-4">
               <%:oDMBaoCao.DMSTT%>
            </div>
            <label for="DMHienThi" class="col-sm-2 control-label">DM hiển thị</label>
            <div class="col-sm-4">
               <%=oDMBaoCao.DMHienThi? "checked" : string.Empty%>
            </div>
        </div>
        <div class="form-group row">
            <label for="DMMoTa" class="col-sm-2 control-label">DM Mô tả</label>
            <div class="col-sm-10">
                <%:oDMBaoCao.DMMoTa%>
            </div>
        </div>
        <div class="form-group row">
            <label for="DMVietTat" class="col-sm-2 control-label">DM Viết tắt</label>
            <div class="col-sm-4">
                <%:oDMBaoCao.DMVietTat%>
            </div>
        </div>
        <div class="form-group row">
            <label for="UrlList" class="col-sm-2 control-label">Url List</label>
            <div class="col-sm-10">
               <%:oDMBaoCao.UrlList%>
            </div>
        </div>
         <div class="form-group row">
            <label for="UrlList" class="col-sm-2 control-label">File BC chi tiết</label>
            <div class="col-sm-10">
             <a href="<%=oDMBaoCao.UrlFileBCChiTiet%>" target="_blank"><%=System.IO.Path.GetFileName(oDMBaoCao.UrlFileBCChiTiet)%></a>
            </div>
        </div>
         <div class="form-group row">
            <label for="UrlList" class="col-sm-2 control-label">File BC tổng hợp</label>
            <div class="col-sm-10">
                <a href="<%=oDMBaoCao.UrlBaoCaoTongHop%>" target="_blank"><%=System.IO.Path.GetFileName(oDMBaoCao.UrlBaoCaoTongHop)%></a>
            </div>
        </div>
         <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oDMBaoCao.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=oDMBaoCao.ListFileAttach[i].Url %>"><%=oDMBaoCao.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
         <div class="form-group row">
            <label  class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
               <%=string.Format("{0:dd-MM-yyyy}", oDMBaoCao.Created)%>
            </div>
            <label  class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}",oDMBaoCao.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
               <%=oDMBaoCao.Author.LookupValue%>
            </div>
            <label  class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
               <%=oDMBaoCao.Editor.LookupValue%>
            </div>    
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>