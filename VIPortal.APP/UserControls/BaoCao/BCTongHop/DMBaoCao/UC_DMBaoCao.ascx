<%@ control language="C#" autoeventwireup="true" codebehind="UC_DMBaoCao.aspx.cs" inherits="VIPortalAPP.UC_DMBaoCao" %>
<link rel="stylesheet" href="/Content/plugins/kendoui/styles/kendo.common.min.css" />
<link rel="stylesheet" href="/Content/plugins/kendoui/styles/kendo.default.min.css" />

<script type="text/javascript" src="/Content/plugins/kendoui/js/jszip.min.js"></script>
<script type="text/javascript" src="/Content/plugins/kendoui/js/kendo.all.min.js"></script>

<div role="body-data" data-title="danh mục báo cáo" class="content_wp" data-action="/UserControls/BaoCao/BCTongHop/DMBaoCao/pAction.asp" data-form="/UserControls/BaoCao/BCTongHop/DMBaoCao/pFormDMBaoCao.aspx" data-view="/UserControls/BaoCao/BCTongHop/DMBaoCao/pViewDMBaoCao.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="DMBaoCaoSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title" />
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <button class="btn btn-primary act-add" type="button">Thêm mới</button>
                    </p>
                </div>
            </div>

            <div class="clsgrid table-responsive">

                <table id="tblDMBaoCao" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        var $tagvalue = $(".content_wp");
        var $tblDanhMucChung = $("#tblDMBaoCao").viDataTable(
            {
                "frmSearch": "DMBaoCaoSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "data": "DMSTT",
                            "name": "DMSTT",
                            "sTitle": "STT",
                            "sWidth": "30px",
                        }, {
                            "data": "DMVietTat",
                            "name": "DMVietTat",
                            "sTitle": "Viết tắt",
                            "sWidth": "90px",
                        }, {
                            "mData": function (o) {
                                return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>';
                            },
                            "name": "Title", "sTitle": "Tiêu đề"
                        },
                        {
                            "mData": function (o) {
                                if (o.LoaiBaoCao == 2) {
                                    return 'Báo cáo số liệu';
                                } else return 'Báo cáo file';
                            },
                            "name": "LoaiBaoCao", "sTitle": "Loại báo cáo"
                        }, {
                            "data": "UrlFileBCChiTiet",
                            "name": "UrlFileBCChiTiet",
                            "sTitle": "Mẫu chi tiết",
                            "sWidth": "190px",
                        }, {
                            "data": "UrlBaoCaoTongHop",
                            "name": "UrlBaoCaoTongHop",
                            "sTitle": "Mẫu tổng hợp",
                            "sWidth": "190px",
                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {
                                if (o._ModerationStatus == 0)
                                    return '<a title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" title="Sửa" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        }, {
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) {
                                return '<a size="1024" title="Cấu hình trường dữ liệu" url="/UserControls/BaoCao/OssiDataType/pFormCauHinhMauBaoCaoV2.aspx" class="btn default btn-xs purple btnfrm" data-do="UPDATEEXCEL" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-cogs" aria-hidden="true"></i></a>';
                            }
                        }, {

                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" data-do="DELETE" title="Xóa"  data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-trash-alt"></i></a>'; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ],
                "aaSorting": [0, 'asc']
            });
    });
</script>
