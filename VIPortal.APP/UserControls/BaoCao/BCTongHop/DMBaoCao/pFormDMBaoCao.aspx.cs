using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortalAPP
{
    public partial class pFormDMBaoCao : pFormBase
    {
        public DMBaoCaoItem oDMBaoCao {get;set;}
        public ConfigExcel JsonConfig { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
			oDMBaoCao = new DMBaoCaoItem();
            JsonConfig = new ConfigExcel();
            if (ItemID > 0)
            {
                DMBaoCaoDA oDMBaoCaoDA = new DMBaoCaoDA();
                oDMBaoCao = oDMBaoCaoDA.GetByIdToObject<DMBaoCaoItem>(ItemID);
                if (!string.IsNullOrEmpty(oDMBaoCao.JsonConfig))
                {
                    JsonConfig = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigExcel>(oDMBaoCao.JsonConfig);
                }
            }
        }
    }
}