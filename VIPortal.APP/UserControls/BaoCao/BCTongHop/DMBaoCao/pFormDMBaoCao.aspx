<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormDMBaoCao.aspx.cs" Inherits="VIPortalAPP.pFormDMBaoCao" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-DMBaoCao" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oDMBaoCao.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oDMBaoCao.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="DonViSuDung" class="col-sm-2 control-label">Đơn vị</label>
            <div class="col-sm-10">
               <div class="input-group mb-3">
                    <select data-selected="<%=oDMBaoCao.DonViSuDung%>" data-url="/UserControls/AdminCMS/LGroup/pAction.asp?do=QUERYDATA&GroupIsDonVi=1" data-place="Chọn Đơn vị sử dụng" name="DonViSuDung" id="DonViThamGiaBC" class="form-control" multiple="multiple"></select>
                    <div class="input-group-append">
                         <button class="btn btn-outline-secondary btnfrm" data-do="CHONDONVI" title="Chọn đơn vị" size="900" url="/UserControls/BaoCao/BCTongHop/KyBaoCao/pFormChonDonVi.aspx" id="btnChonNguoi" type="button">Chọn đơn vị</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="DMSTT" class="col-sm-2 control-label">Số thứ tự</label>
            <div class="col-sm-4">
                <input type="text" name="DMSTT" id="DMSTT" placeholder="Nhập số thứ tự" value="<%:oDMBaoCao.DMSTT%>" class="form-control" />
            </div>
            <label for="DMHienThi" class="col-sm-2 control-label">DM hiển thị</label>
            <div class="col-sm-4">
                <input value="true" type="checkbox" name="DMHienThi" id="DMHienThi" <%=oDMBaoCao.DMHienThi? "checked" : string.Empty%> />
            </div>
        </div>
        <div class="form-group row">
            <label for="DMMoTa" class="col-sm-2 control-label">DM Mô tả</label>
            <div class="col-sm-10">
                <textarea name="DMMoTa" id="DMMoTa" class="form-control"><%:oDMBaoCao.DMMoTa%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="DMVietTat" class="col-sm-2 control-label">DM Viết tắt</label>
            <div class="col-sm-4">
                <input type="text" name="DMVietTat" id="DMVietTat" placeholder="Nhập danh mục viết tắt" value="<%:oDMBaoCao.DMVietTat%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="LoaiBaoCao" class="col-sm-2 control-label">Loại báo cáo</label>
            <div class="col-sm-4">
                <label style="font-style:normal;" class="radio-inline">
                    <input type="radio" name="LoaiBaoCao" value="1" />Báo cáo file
                </label>
                <label style="font-style:normal;" class="radio-inline">
                    <input type="radio" name="LoaiBaoCao" value="2" />Báo cáo số liệu
                </label>
               
            </div>
            <label for="UrlList" class="col-sm-2 control-label">Url List</label>
            <div class="col-sm-4">
                <input type="text" name="UrlList" id="UrlList" placeholder="Nhập url list" value="<%:oDMBaoCao.UrlList%>" class="form-control" />
            </div>
        </div>

        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $('input:radio[name="LoaiBaoCao"]').filter('[value="<%=oDMBaoCao.LoaiBaoCao%>"]').attr('checked', true);
            $("#DonViThamGiaBC").smSelect2018V2({
                dropdownParent: "#frm-DMBaoCao"
            });
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oDMBaoCao.ListFileAttach)%>'
            });
            $("#Title").focus();
           

            $("#frm-DMBaoCao").validate({
                rules: {
                    Title: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/BaoCao/BCTongHop/DMBaoCao/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-DMBaoCao").trigger("click");
                            $(form).closeModal();
                            $('#tblDMBaoCao').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
        });
    </script>
</body>
</html>
