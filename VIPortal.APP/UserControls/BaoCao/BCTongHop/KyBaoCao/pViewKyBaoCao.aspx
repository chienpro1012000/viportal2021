<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewKyBaoCao.aspx.cs" Inherits="VIPortalAPP.pViewKyBaoCao" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
    <style type="text/css">
        #frmViewBaoCao fieldset.scheduler-border {
            border: solid 1px #DDD !important;
            padding: 0 10px 10px 10px;
            border-bottom: none;
            margin-bottom: 15px;
        }

        #frmViewBaoCao legend.scheduler-border {
            width: auto !important;
            border: none;
            font-size: 18px;
            padding: 0px 10px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            var $tblDanhMucChung = $("#tblNhatKyXuLy").viDataTable(
                {
                    "frmSearch": "NhatKyXuLySearch",
                    "url": '/UserControls/QTQuyTrinhBT/NhatKyXuLy/pAction.asp',
                    "aoColumns":
                        [
                            {
                                "name": "CreatedDate", "sTitle": "Thời gian",
                                "mData": function (o) {
                                    return formatDateTime(o.CreatedDate);
                                },
                                "sWidth": "120px",
                            }, {
                                "data": "LogThaoTacTitle",
                                "name": "LogThaoTacTitle", "sTitle": "Thao tác",
                                "sWidth": "120px",
                            },
                            {
                                "data": "CreatedUser.Title",
                                "name": "CreatedUser", "sTitle": "Cán bộ",
                                "sWidth": "120px",
                            }, {
                                "name": "LogNoiDung", "sTitle": "Ghi chú/Nội dung",
                                "mData": function (o) {
                                    var htmlattach = '';
                                    if (o.ListFileAttach.length > 0) {
                                        $.each(o.ListFileAttach, function (index, value) {
                                            htmlattach += `<a class="clslink" href="${value.Url}" target="_blank">${value.Name}</a>`;
                                        });
                                    }
                                    return o.LogNoiDung + '	&nbsp;&nbsp;' + htmlattach;
                                }
                            },
                        ],
                    "aaSorting": [0, 'asc']
                });
        });
    </script>
</head>
<body>
    <form class="form-horizontal form-viewdetail" id="frmViewBaoCao">
        <div class="form-group row">
            <div class="col-sm-12 text-right">
                <%if(oKyBaoCao.DoiTuongThucHien == 1 && oKyBaoCao.TrangThaiBaoCao == 3){  %>
                <button type="button" title="Chuyển báo cáo" url="/UserControls/BaoCao/BCTongHop/KyBaoCao/pFormGiaoBaoCaoDonVi.aspx?ItemID=<%=oKyBaoCao.ID %>&chuyenbc=1&do=CHUYENBC" size="800" class="btn btn-primary mb-2 btnfrm">Chuyển báo cáo</button>
                
                <%}else if(oKyBaoCao.TrangThaiBaoCao == 2){  %>
                <button type="button" title="Tiếp nhận báo cáo" url="/UserControls/BaoCao/BCTongHop/KyBaoCao/pFormTiepNhan.aspx?ItemID=<%=oKyBaoCao.ID %>&do=TIEPNHAN" size="800" class="btn btn-primary mb-2 btnfrm">Tiếp nhận</button>
                <%}else if(oKyBaoCao.TrangThaiBaoCao == 4){  %>
                <button type="button" title="Cập nhật kết quả" url="/UserControls/BaoCao/BCTongHop/KyBaoCao/pCapNhatKQ.aspx?ItemID=<%=oKyBaoCao.ID %>&do=UPDATEKETQUA" size="1024" class="btn btn-primary mb-2 btnfrm">Cập nhật kết quả</button>
                <%}else if(oKyBaoCao.TrangThaiBaoCao == 5){  %>
                <button type="button" title="Cập nhật kết quả" url="/UserControls/BaoCao/BCTongHop/KyBaoCao/pCapNhatKQ.aspx?ItemID=<%=oKyBaoCao.ID %>&do=UPDATEKETQUA" size="1024" class="btn btn-primary mb-2 btnfrm">Cập nhật kết quả</button>
                <%}else if((oKyBaoCao.DoiTuongThucHien == 2 && oKyBaoCao.TrangThaiBaoCao == 3) || oKyBaoCao.TrangThaiBaoCao == 6 || oKyBaoCao.TrangThaiBaoCao == 9){  %>
                <button type="button" title="Cập nhật kết quả" url="/UserControls/BaoCao/BCTongHop/KyBaoCao/pCapNhatKQ.aspx?ItemID=<%=oKyBaoCao.ID %>&do=UPDATEKETQUA" size="1024" class="btn btn-primary mb-2 btnfrm">Cập nhật kết quả</button>
                <%if(oKyBaoCao.isLDDonViDuyet == 1){ %>
                <button type="button" title="Trình duyệt kết quả báo cáo" url="/UserControls/BaoCao/BCTongHop/KyBaoCao/pFormGiaoBaoCaoDonVi.aspx?ItemID=<%=oKyBaoCao.ID %>&do=CHUYENBC" size="800" class="btn btn-primary mb-2 btnfrm">Trình duyệt</button>
                <%}else{ %>
                <!--Truong hop don vi ko can duyet, chuyen vien xu ly xong là xong-->
                <button type="button" title="Xác nhận hoàn thành" url="/UserControls/BaoCao/BCTongHop/KyBaoCao/pFormTiepNhan.aspx?TrangThaiTiepTheo=10&ItemID=<%=oKyBaoCao.ID %>&do=TIEPNHAN" size="800" class="btn btn-primary mb-2 btnfrm">Hoàn thành</button>
                <%} %>
                <%}else if(oKyBaoCao.TrangThaiBaoCao == 7){  %>
                <button type="button" title="Cập nhật kết quả" url="/UserControls/BaoCao/BCTongHop/KyBaoCao/pFormDuyetBaoCao.aspx?ItemID=<%=oKyBaoCao.ID %>&do=CHUYENBC" size="1024" class="btn btn-primary mb-2 btnfrm">Phê duyệt</button>
                <%}else if(oKyBaoCao.TrangThaiBaoCao == 8){  %>
                <button type="button" title="Xác nhận hoàn thành" url="/UserControls/BaoCao/BCTongHop/KyBaoCao/pFormTiepNhan.aspx?ItemID=<%=oKyBaoCao.ID %>&do=TIEPNHAN" size="800" class="btn btn-primary mb-2 btnfrm">Hoàn thành</button>
                <%} %>
                <%if(isThuHoi){  %>
                <button type="button" title="Thu hồi" url="/UserControls/BaoCao/BCTongHop/KyBaoCao/pFormTiepNhan.aspx?ItemID=<%=oKyBaoCao.ID %>&do=THUHOi" size="800" class="btn btn-primary mb-2 btnfrm">Thu hồi</button>
                <%} %>
            </div>
        </div>
        <fieldset class="scheduler-border">
            <legend class="scheduler-border">Thông tin kỳ báo cáo</legend>
            <div class="form-group row">
                <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
                <div class="col-sm-10">
                    <%=oKyBaoCao.Title%>
                </div>
            </div>
            <div class="form-group row">
                <label for="DMMoTa" class="col-sm-2 control-label">Mô tả</label>
                <div class="col-sm-10">
                    <%=oKyBaoCao.DMMoTa%>
                </div>
            </div>
            <div class="form-group row">
                <label for="DMMoTa" class="col-sm-2 control-label">Đơn vị</label>
                <div class="col-sm-10">
                    <%=RootKyDanhGi.fldGroup.LookupValue%>
                </div>
            </div>
            <div class="form-group row">
                <label for="LoaiBaoCao" class="col-sm-2 control-label">Loại báo cáo</label>
                <div class="col-sm-4">
                    <%=oKyBaoCao.LoaiBaoCao.LookupValue%>
                </div>
                <label for="DMVietTat" class="col-sm-2 control-label">DM viết tắt</label>
                <div class="col-sm-4">
                    <%:oKyBaoCao.DMVietTat%>
                </div>
            </div>
            <%if(!string.IsNullOrEmpty(oDMBaoCaoItem.UrlFileBCChiTiet)){ %>
            <div class="form-group row">
                <label for="UrlFileBCChiTiet" class="col-sm-2 control-label">File BC chi tiết</label>
                <div class="col-sm-10">
                    <a href="<%=oDMBaoCaoItem.UrlFileBCChiTiet%>" target="_blank"><%=System.IO.Path.GetFileName(oDMBaoCaoItem.UrlFileBCChiTiet)%></a>
                </div>
            </div>
            <%} %>
            <div class="form-group row">
                <label for="BCTuNgay" class="col-sm-2 control-label">Từ ngày</label>
                <div class="col-sm-4">
                    <div class="">
                        <%:string.Format("{0:dd-MM-yyyy}", oKyBaoCao.BCTuNgayTemp)%>
                    </div>
                </div>
                <label for="QCNgayVanBan" class="col-sm-2 control-label">Hạn dến ngày</label>
                <div class="col-sm-4">
                    <%:string.Format("{0:dd-MM-yyyy}", oKyBaoCao.BCDenNgayTemp)%>
                </div>
            </div>
            <div class="form-group row">
                <label for="BCTuNgay" class="col-sm-2 control-label">Đơn vị tham gia</label>
                <div class="col-sm-10">
                    <div class="">
                        <%=string.Join(", ", oKyBaoCao.DonViThamGiaBC.Select(x=>x.LookupValue)) %>
                    </div>
                </div>

            </div>
            <div class="form-group row">
                <label class="col-sm-2 control-label">File đính kèm</label>
                <div class="col-sm-10">
                    <%for(int i=0; i< RootKyDanhGi.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=RootKyDanhGi.ListFileAttach[i].Url %>"><%=RootKyDanhGi.ListFileAttach[i].Name%></a></div>
                    <%} %>
                </div>
            </div>
        </fieldset>

        <fieldset class="scheduler-border">
            <legend class="scheduler-border">Kết quả báo cáo</legend>
            <div class="form-group row">
                <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
                <div class="col-sm-10">
                    <%=oKyBaoCao.TitleKetQua%>
                </div>
            </div>
            <div class="form-group row">
                <label for="Title" class="col-sm-2 control-label">Mô tả</label>
                <div class="col-sm-10">
                    <%=oKyBaoCao.MoTaKetQua%>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 control-label">File đính kèm</label>
                <div class="col-sm-10">
                    <%for(int i=0; i< oKyBaoCao.ListFileAttach.Count;i++){ %>
                    <div>
                        <a href="<%=oKyBaoCao.ListFileAttach[i].Url %>">
                            <%=oKyBaoCao.ListFileAttach[i].Name%></a><%if(oKyBaoCao.ListFileAttach[i].Name.ToLower().EndsWith(".docx")){ %><a target="_blank" class="clslinkedit" style="padding-left: 3px;" href="/htbaocao/_layouts/15/WopiFrame.aspx?sourcedoc=<%=oKyBaoCao.ListFileAttach[i].Url %>&file=<%=oKyBaoCao.ListFileAttach[i].Name%>&action=default"><i class="far fa-window-restore"></i></a><%} %>
                    </div>
                    <%} %>
                </div>
            </div>
        </fieldset>
        <div class="form-group row">
            <div class="col-sm-12">

                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Nhật ký xử lý</a>
                    </li>

                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                        <div role="body-data" data-title="" class="content_wp" data-action="/UserControls/NhatKyXuLy/pAction.asp" data-form="/UserControls/NhatKyXuLy/pFormNhatKyXuLy.aspx">
                            <div class="clsmanager row">
                                <div class="col-sm-12 text-right">
                                    <div id="NhatKyXuLySearch" class="form-inline zonesearch float-right">
                                        <div class="form-group">
                                            <input type="hidden" name="LogDoiTuong" id="LogDoiTuong" value="KyBaoCao" />
                                            <input type="hidden" name="LogIDDoiTuong" id="LogIDDoiTuong" value="<%=ItemID %>" />
                                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="clsgrid table-responsive">

                                <table id="tblNhatKyXuLy" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                </table>

                            </div>
                        </div>

                    </div>
                </div>



            </div>
        </div>

        <div class="clearfix"></div>
    </form>
</body>

</html>
