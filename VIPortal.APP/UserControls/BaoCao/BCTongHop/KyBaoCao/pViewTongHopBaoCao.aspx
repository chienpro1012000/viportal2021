﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewTongHopBaoCao.aspx.cs" Inherits="VIPortal.APP.UserControls.BaoCao.BCTongHop.KyBaoCao.pViewTongHopBaoCao" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        $(function () {
            $.post("/UserControls/BaoCao/BCTongHop/KyBaoCao/pAction.asp?", { "ItemID": "<%=ItemID%>", "do": "GETJSONBCCT" }, function (result) {
                $("#spreadsheetTH").kendoSpreadsheet();
                var spreadsheet = $("#spreadsheetTH").data("kendoSpreadsheet");
                spreadsheet.fromJSON(result);
            });
        });
    </script>
</head>
<body>
    <div class="form-horizontal form-viewdetail" id="frmViewBaoCao">

        <div class="form-group row">
            <div class="col-sm-12">
                <div id="spreadsheetTH" style="width: 100%;"></div>
            </div>
        </div>
    </div>
</body>
</html>
