using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortalAPP
{
    public partial class pViewKyBaoCao : pFormBase
    {
        public KyBaoCaoItem oKyBaoCao = new KyBaoCaoItem();
        public KyBaoCaoItem RootKyDanhGi = new KyBaoCaoItem();
        public DMBaoCaoItem oDMBaoCaoItem = new DMBaoCaoItem();
        public bool isThuHoi = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            oKyBaoCao = new KyBaoCaoItem();
            if (ItemID > 0)
            {
                KyBaoCaoDA oKyBaoCaoDA = new KyBaoCaoDA();
                oKyBaoCao = oKyBaoCaoDA.GetByIdToObject<KyBaoCaoItem>(ItemID);
                if(oKyBaoCao.idRootKyDanhGia > 0)
                {
                    RootKyDanhGi = oKyBaoCaoDA.GetByIdToObject<KyBaoCaoItem>(oKyBaoCao.idRootKyDanhGia);
                   int indexkq =  RootKyDanhGi.ListFileAttach.FindIndex(x => x.Name == "BaoCaoTongHop.xlsx");
                    if(indexkq > -1)
                    {
                        RootKyDanhGi.ListFileAttach.RemoveAt(indexkq);
                    }    
                }
                //
                //checked thu hooif |30_3_30|

                if (!string.IsNullOrEmpty(oKyBaoCao.LogText))
                {
                     string lastLog = oKyBaoCao.LogText.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault();
                    if (lastLog.StartsWith($"{CurentUser.ID}_"))
                    {
                        isThuHoi = true;
                    }
                }
                if(oKyBaoCao.LoaiBaoCao.LookupId > 0)
                {
                    DMBaoCaoDA dMBaoCaoDA = new DMBaoCaoDA();
                    oDMBaoCaoItem = dMBaoCaoDA.GetByIdToObject<DMBaoCaoItem>(oKyBaoCao.LoaiBaoCao.LookupId);
                    
                }
            }
        }
    }
}