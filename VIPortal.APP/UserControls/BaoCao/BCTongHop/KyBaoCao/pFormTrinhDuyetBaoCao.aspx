﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormTrinhDuyetBaoCao.aspx.cs" Inherits="VIPortal.APP.UserControls.BaoCao.BCTongHop.KyBaoCao.pFormTrinhDuyetBaoCao" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-FormGiaoBaoCao" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oKyBaoCao.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" id="TrangThaiBaoCao" name="TrangThaiBaoCao" value="<%=oKyBaoCao.TrangThaiBaoCao + 1 %>" />

        <%--<div class="form-group row">
            <label for="LogNoiDung" class="col-sm-2 control-label">Ý kiến</label>
            <div class="col-sm-10">
                <textarea name="LogNoiDung" id="LogNoiDung" class="form-control"></textarea>
            </div>
        </div>--%>
       <%-- <div class="form-group row">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-10">
                <span style="float: left; margin-top: 10px;">                   
                    <input type="radio" value="<%=oKyBaoCao.TrangThaiBaoCao + 1 %>" id="isDuyetVaPhatHanhKyHT" name="TrangThaiBaoCao"/>&nbsp;Duyệt &amp; Trình ký&nbsp;&nbsp;&nbsp;
                    <input type="radio" value="<%=oKyBaoCao.TrangThaiBaoCao - 1 %>" id="btnTraVe" name="TrangThaiBaoCao"/>&nbsp;Trả về  &nbsp;&nbsp;&nbsp; 
                </span>
            </div>
        </div>--%>
        <div class="form-group row">
            <label for="UserPhuTrach" class="col-sm-2 control-label">Cán bộ</label>
            <div class="col-sm-10">
                <select data-selected="" data-url="/UserControls/AdminCMS/LUser/pAction.asp?do=QUERYDATA&Groups=<%=CurentUser.Groups.ID %>" data-place="Chọn cán bộ" name="UserPhuTrach" id="UserPhuTrach" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#FileAttach").regFileUpload({
                files: '[]'
            });
            //UserPhuTrach
            $("#UserPhuTrach").smSelect2018V2({
                dropdownParent: "#frm-FormGiaoBaoCao",
                parameterPlus: function (para) {
                    // Hàm post dữ liệu.
                    para["Groups"] = '<%=oKyBaoCao.fldGroup.LookupId%>'
                },
            });
            $("#frm-FormGiaoBaoCao").validate({
                rules: {
                    LogNoiDung: "required",
                    UserPhuTrach: "required",
                    TrangThaiBaoCao: "required",
                },
                messages: {
                    LogNoiDung: "Vui lòng nhập nội dung"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/BaoCao/BCTongHop/KyBaoCao/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-DMLinhVuc").trigger("click");
                            $(form).closeModal();
                            $('#tblKyBaoCao').DataTable().ajax.reload();
                            $('#tblKyBaoCaoCD').DataTable().ajax.reload();
                            $('#tblKyBaoCaoCK').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-FormGiaoBaoCao").viForm();
        });
    </script>
</body>
</html>
