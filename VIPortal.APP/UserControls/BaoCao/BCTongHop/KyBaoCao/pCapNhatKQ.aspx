﻿<%@ page language="C#" autoeventwireup="true" codebehind="pCapNhatKQ.aspx.cs" inherits="VIPortal.APP.UserControls.BaoCao.BCTongHop.KyBaoCao.pCapNhatKQ" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-KETQUABAOCAO" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oKyBaoCao.ID %>" />
        <input type="hidden" name="TrangThaiBaoCao" id="TrangThaiBaoCao" value="6" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="TitleKetQua" id="TitleKetQua" placeholder="Tiêu đề kết quả" class="form-control" value="<%=oKyBaoCao.TitleKetQua%>" />
            </div>
        </div>

        <div class="form-group row">
            <label for="MaSV" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <textarea rows="6" name="MoTaKetQua" id="MoTaKetQua" class="form-control" placeholder="Nhập mô tả"><%=oKyBaoCao.MoTaKetQua%></textarea>
            </div>
        </div>
        <div class="form-group row" id="divfile">
           <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label" for="FileAttach" >Tệp đính kèm<span class="clslable">(<span class="clsred">*</span>)</span></label>
            <div class="col-xs-8 col-sm-8 col-md-8">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>

        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Biểu mẫu</label>
            <div class="col-xs-8 col-sm-8 col-md-8">
                <button type="button" class="btn btn-primary" id="btnNhapTuBieuMau">Nhập từ biểu mẫu</button>
            </div>
        </div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#btnNhapTuBieuMau").click(function () {
                var urlfile = '<%=urlfile%>';
                window.open(`/UserControls/DevExpressExcel.aspx?urlfile=${urlfile}&do=UPDATEKQ&ItemID=<%=oKyBaoCao.ID%>`, '_blank');
                $("#frm-KETQUABAOCAO").closeModal();
            });
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oKyBaoCao.ListFileAttach)%>'
            });
            $("#Title").focus();
            //if ("<%=UrlFileBaoCaoChiTietNhap%>" != "") {
            //    //bao cáo tổng hợp thì sẽ close form và open tab khác.
            //    var urlfile = '<%=urlfile%>';
            //    //alert(urlfile);
            //    
            //    $("#frm-KETQUABAOCAO").closeModal();
            //}

            //$("#divfile").hide();
            //ko check validate
            $("#frm-KETQUABAOCAO").validate({
                rules: {
                    Title: "required",
                    FileAttach: "Attachments"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề kết quả",
                    FileAttach: "Đính kèm tài liệu"
                },
                submitHandler: function (form) {
                    var odata = $("#frm-KETQUABAOCAO").siSerializeArray();
                    $.post("/UserControls/BaoCao/BCTongHop/KyBaoCao/pAction.asp?", odata, function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            $("#frm-KETQUABAOCAO").closeModal();
                            $("#frmViewBaoCao").closeModal();
                            //reload lại grid vừa thao tác.
                            var idtable = $('#contentbc div.tab-pane.active table.dataTable').attr('id');
                            $('#' + idtable).DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            
            $("#frm-KETQUABAOCAO").viForm();
        });
    </script>
</body>
</html>
