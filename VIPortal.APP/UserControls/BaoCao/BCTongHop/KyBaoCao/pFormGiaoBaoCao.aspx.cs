﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData;

namespace VIPortal.APP.UserControls.BaoCao.BCTongHop.KyBaoCao
{
    public partial class pFormGiaoBaoCao : pFormBase
    {
        public KyBaoCaoItem oKyBaoCao { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oKyBaoCao = new KyBaoCaoItem();
            if (ItemID > 0)
            {
                KyBaoCaoDA oKyBaoCaoDA = new KyBaoCaoDA();
                oKyBaoCao = oKyBaoCaoDA.GetByIdToObject<KyBaoCaoItem>(ItemID);
            }
        }
    }
}