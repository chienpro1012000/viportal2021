﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData;

namespace VIPortal.APP.UserControls.BaoCao.BCTongHop.KyBaoCao
{
    public partial class pFormTiepNhan : pFormBase
    {
        public int TrangThaiTiepTheo = 3;
        public KyBaoCaoItem oKyBaoCao { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oKyBaoCao = new KyBaoCaoItem();
            if (ItemID > 0)
            {
                KyBaoCaoDA oKyBaoCaoDA = new KyBaoCaoDA();
                oKyBaoCao = oKyBaoCaoDA.GetByIdToObject<KyBaoCaoItem>(ItemID);
                if (oKyBaoCao.idRoot != 0 && oKyBaoCao.idRootKyDanhGia != 0)
                {
                    if (oKyBaoCao.TrangThaiBaoCao == 2)
                    {
                        TrangThaiTiepTheo = 3;
                    }
                    else if (oKyBaoCao.TrangThaiBaoCao == 8)
                    {
                        TrangThaiTiepTheo = 10;
                    }
                }
                else
                {
                    if (oKyBaoCao.TrangThaiBaoCao == 2 || oKyBaoCao.TrangThaiBaoCao == 1) //thao tác xác nhận thông tin.
                    {
                        TrangThaiTiepTheo = 99; //xác nhận kết thúc.
                    }
                }
                if (!string.IsNullOrEmpty(Request["TrangThaiTiepTheo"]))
                {
                    TrangThaiTiepTheo = Convert.ToInt32(Request["TrangThaiTiepTheo"]);
                }
            }
        }
    }
}