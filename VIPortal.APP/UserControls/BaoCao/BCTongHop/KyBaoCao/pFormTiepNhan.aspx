﻿<%@ page language="C#" autoeventwireup="true" codebehind="pFormTiepNhan.aspx.cs" inherits="VIPortal.APP.UserControls.BaoCao.BCTongHop.KyBaoCao.pFormTiepNhan" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-FormGiaoBaoCao" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oKyBaoCao.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" name="TrangThaiBaoCao" id="TrangThaiBaoCao" value="<%=TrangThaiTiepTheo %>" />
        <input type="hidden" name="DoiTuongTitle" id="DoiTuongTitle" value="Báo cáo Kỳ" />
        <input type="hidden" name="LogDoiTuong" id="LogDoiTuong" value="KyBaoCao" />
        <input type="hidden" name="LogIDDoiTuong" id="LogIDDoiTuong" value="<%=oKyBaoCao.ID %>" />
        <input type="hidden" name="LogThaoTac" id="LogThaoTac" value="TiepNhanBC" />
        <input type="hidden" name="LogTrangThai" id="LogTrangThai" value="<%=TrangThaiTiepTheo %>" />
        <input type="hidden" name="TrangThaiParent" id="TrangThaiParent" value="<%=oKyBaoCao.TrangThaiBaoCao %>" />
        <input type="hidden" name="CreatedUser" id="CreatedUser" value="<%=CurentUser.ID %>" />
        <div class="form-group row">
            <label for="LogNoiDung" class="col-sm-2 control-label">Ý kiến</label>
            <div class="col-sm-10">
                <textarea name="LogNoiDung" id="LogNoiDung" class="form-control"></textarea>
            </div>
        </div>

        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-10">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#FileAttach").regFileUpload({
                files: '[]'
            });
            //UserPhuTrach
            $("#UserPhuTrach").smSelect2018V2({
                dropdownParent: "#frm-FormGiaoBaoCao",
                parameterPlus: function (para) {
                    // Hàm post dữ liệu.
                    para["Groups"] = '<%=oKyBaoCao.fldGroup.LookupId%>'
                },
            });
            $("#frm-FormGiaoBaoCao").validate({
                rules: {
                    LogNoiDung: "required",
                },
                messages: {
                    LogNoiDung: "Vui lòng nhập nội dung"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/BaoCao/BCTongHop/KyBaoCao/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-DMLinhVuc").trigger("click");
                            $(form).closeModal();
                            $("#frmViewBaoCao").closeModal();
                            var idtable = $('#contentbc div.tab-pane.active table.dataTable').attr('id');
                            console.log(idtable);
                            $('#' + idtable).DataTable().ajax.reload();
                            //$('#tblKyBaoCaoDCNKQ').DataTable().ajax.reload();
                            //$('#tblKyBaoCaoXL').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-FormGiaoBaoCao").viForm();
        });
    </script>
</body>
</html>
