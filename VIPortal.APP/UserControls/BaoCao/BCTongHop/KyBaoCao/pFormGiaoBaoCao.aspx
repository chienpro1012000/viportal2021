﻿<%@ page language="C#" autoeventwireup="true" codebehind="pFormGiaoBaoCao.aspx.cs" inherits="VIPortal.APP.UserControls.BaoCao.BCTongHop.KyBaoCao.pFormGiaoBaoCao" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
    <style type="text/css">
        .select2-dropdown.select2-dropdown {
            z-index: 1071;
        }
    </style>
</head>
<body>
    <form id="frm-FormGiaoBaoCao" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oKyBaoCao.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />

        <div class="form-group row">
            <label for="LogNoiDung" class="col-sm-2 control-label">Ý kiến</label>
            <div class="col-sm-10">
                <textarea name="LogNoiDung" id="LogNoiDung" class="form-control"></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Đối tượng thực hiện</label>
            <div class="col-sm-10">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="DoiTuongThucHien" id="inlineCheckbox2" checked="checked" value="1" />
                    <label class="form-check-label" for="inlineCheckbox2">Đơn vị thực hiện</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="DoiTuongThucHien" id="inlineCheckbox1" value="2" />
                    <label class="form-check-label" for="DoiTuongThucHien">Người thực hiện</label>
                </div>

            </div>
        </div>
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Đơn vị thực hiện</label>
            <div class="col-sm-10">
                <div class="input-group mb-3">
                    <select data-selected="<%=string.Join(",", oKyBaoCao.DonViThamGiaBC.Select(x=>x.LookupId)) %>" data-url="/UserControls/AdminCMS/LGroup/pAction.asp?do=QUERYDATA&GroupIsDonVi=1" data-place="Chọn Đơn vị tham gia" name="DonViThamGiaBC" id="DonViThamGiaBC" class="form-control" multiple="multiple"></select>
                    <div class="input-group-append">
                         <button class="btn btn-outline-secondary btnfrm" data-do="CHONDONVI" title="Chọn đơn vị" size="900" url="/UserControls/BaoCao/BCTongHop/KyBaoCao/pFormChonDonVi.aspx" id="btnChonNguoi" type="button">Chọn đơn vị</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Người dùng thực hiện</label>
            <div class="col-sm-10">
                <div class="input-group mb-3">
                    <select data-selected="<%=string.Join(",", oKyBaoCao.UserThamGia.Select(x=>x.LookupId)) %>" data-url="/UserControls/AdminCMS/LUser/pAction.asp?do=QUERYDATA" data-place="Chọn Người dùng tham gia" name="UserThamGia" id="UserThamGiaForm" class="form-control" multiple="multiple"></select>
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button">Chọn</button>
                    </div>
                </div>

            </div>
        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $(document).ready(function () {
            
            $("input[name=DoiTuongThucHien][value=<%:oKyBaoCao.DoiTuongThucHien%>]").attr('checked', 'checked');

            $("#DonViThamGiaBC").smSelect2018V2({
                dropdownParent: "#frm-FormGiaoBaoCao"
            });
            $("#UserThamGiaForm").smSelect2018V2({
                dropdownParent: "#frm-FormGiaoBaoCao",
                Ajax: true
            });
            $("#FileAttach").regFileUpload({
                files: '[]'
            });
            $("#frm-FormGiaoBaoCao").validate({
                rules: {
                    LogNoiDung: "required",
                },
                messages: {
                    LogNoiDung: "Vui lòng nhập nội dung"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/BaoCao/BCTongHop/KyBaoCao/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-DMLinhVuc").trigger("click");
                            $(form).closeModal();
                            $("#frmViewBaoCao").closeModal();
                            $('#tblKyBaoCao').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-FormGiaoBaoCao").viForm();
        });
    </script>
</body>
</html>
