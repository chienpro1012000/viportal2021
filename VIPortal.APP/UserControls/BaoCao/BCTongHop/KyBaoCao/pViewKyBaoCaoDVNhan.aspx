<%@ page language="C#" autoeventwireup="true" codebehind="pViewKyBaoCaoDVNhan.aspx.cs" inherits="VIPortalAPP.pViewKyBaoCaoDVNhan" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
        <div class="form-group row">
            <div class="col-sm-12">
                <button type="button" class="btn btn-primary mb-2">Cập nhật Báo cáo</button>
            </div>
        </div>
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <%=oKyBaoCao.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="DMHienThi" class="col-sm-2 control-label">Hiển thị</label>
            <div class="col-sm-4">
                <%=oKyBaoCao.DMHienThi? "checked" : string.Empty%>
            </div>
            <label for="DMSTT" class="col-sm-2 control-label">DMSTT</label>
            <div class="col-sm-4">
                <%:oKyBaoCao.DMSTT%>
            </div>
        </div>
        <div class="form-group row">
            <label for="DMMoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <%=oKyBaoCao.DMMoTa%>
            </div>
        </div>
        <div class="form-group row">
            <label for="LoaiBaoCao" class="col-sm-2 control-label">Loại báo cáo</label>
            <div class="col-sm-4">
                <%:oKyBaoCao.LoaiBaoCao.LookupId%>
            </div>
            <label for="DMVietTat" class="col-sm-2 control-label">DM viết tắt</label>
            <div class="col-sm-4">
                <%:oKyBaoCao.DMVietTat%>
            </div>
        </div>
        <div class="form-group row">
            <label for="BCLoai" class="col-sm-2 control-label">BCLoai</label>
            <div class="col-sm-10">
                <%=oKyBaoCao.BCLoai%>
            </div>
        </div>
        <div class="form-group row">
            <label for="BCTuNgay" class="col-sm-2 control-label">Từ ngày</label>
            <div class="col-sm-4">
                <div class="">
                    <%:string.Format("{0:dd-MM-yyyy}", oKyBaoCao.BCTuNgay)%>
                </div>
            </div>
            <label for="QCNgayVanBan" class="col-sm-2 control-label">Đến ngày</label>
            <div class="col-sm-4">
                <%:string.Format("{0:dd-MM-yyyy}", oKyBaoCao.BCDenNgay)%>
            </div>
        </div>
        <div class="form-group row">
            <label for="BCTuNgay" class="col-sm-2 control-label">Đơn vị tham gia</label>
            <div class="col-sm-10">
                <div class="">
                    <%=string.Join(", ", oKyBaoCao.DonViThamGiaBC.Select(x=>x.LookupValue)) %>
                </div>
            </div>

        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oKyBaoCao.ListFileAttach.Count;i++){ %>
                <div><a href="<%=oKyBaoCao.ListFileAttach[i].Url %>"><%=oKyBaoCao.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}", oKyBaoCao.Created)%>
            </div>
            <label class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd-MM-yyyy}",oKyBaoCao.Modified)%>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>
