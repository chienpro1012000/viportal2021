<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormKyBaoCao.aspx.cs" Inherits="VIPortalAPP.pFormKyBaoCao" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-KyBaoCao" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oKyBaoCao.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" name="fldGroup" id="fldGroup" value="<%:oKyBaoCao.fldGroup.LookupId%>" />
        <input type="hidden" name="TrangThaiPheDuyet" id="TrangThaiPheDuyet" value="<%=oKyBaoCao.TrangThaiPheDuyet %>" />
        <input type="hidden" name="NgayThu" id="NgayThu" value="<%: oKyBaoCao.NgayThu%>" />
        <input type="hidden" name="ThangBaoBao" id="ThangBaoBao" value="<%: oKyBaoCao.ThangBaoBao%>" class="form-control" />
        <input type="hidden" name="TuanThu" id="TuanThu" value="<%=oKyBaoCao.TuanThu %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" value="<%=oKyBaoCao.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="DMMoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <textarea name="DMMoTa" id="DMMoTa" class="form-control"><%=oKyBaoCao.DMMoTa%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="LoaiBaoCao" class="col-sm-2 control-label">Loại báo cáo</label>
            <div class="col-sm-10">
                <select data-selected="<%:oKyBaoCao.LoaiBaoCao.LookupId%>" data-url="/UserControls/BaoCao/BCTongHop/DMBaoCao/pAction.asp?do=QUERYDATA" data-place="Chọn LoaiBaoCao" name="LoaiBaoCao" id="LoaiBaoCao" class="form-control"></select>
            </div>

        </div>
        <div class="form-group row">

            <label for="BCLoai" class="col-sm-2 control-label">
                Kỳ báo cáo</label>
            <div class="col-sm-4">
                <select id="BCLoai" name="BCLoai" class="form-control">
                    <option value="">Chọn loại báo cáo</option>
                    <option value="1">Báo cáo từ ngày đến ngày</option>
                    <option value="3">Báo cáo theo ngày</option>
                    <option value="4">Báo cáo theo tuần</option>
                    <option value="5">Báo cáo theo tháng</option>
                </select>
            </div>
            <label for="DMVietTat" class="col-sm-2 control-label">Viết tắt</label>
            <div class="col-sm-4">
                <input type="text" name="DMVietTat" id="DMVietTat" placeholder="Viết tắt" value="<%:oKyBaoCao.DMVietTat%>" class="form-control" />
            </div>
        </div>
        <div class="form-group row">
            <label for="BCTuNgay" class="col-sm-2 control-label">Từ ngày</label>
            <div class="col-sm-4">
                <div class="">
                    <input type="text" name="BCTuNgay" id="BCTuNgay" value="<%:string.Format("{0:dd/MM/yyyy HH:mm}", oKyBaoCao.BCTuNgay)%>" class="form-control" />
                </div>
            </div>
            <label for="BCDenNgay" class="col-sm-2 control-label">Đến ngày</label>
            <div class="col-sm-4">
                <input type="text" name="BCDenNgay" id="BCDenNgay" value="<%:string.Format("{0:dd/MM/yyyy HH:mm}", oKyBaoCao.BCDenNgay)%>" class="form-control" />
            </div>
        </div>
        <!--ThoiGianLapLai-->
        <div class="form-group row customchoie" for="ThoiGianLapLai" style="display: none;">
            <label for="ThoiGianLapLai" class="col-sm-2 control-label">Thời gian lặp</label>
            <div class="col-sm-3">
                <input type="text" name="ThoiGianLapLai" id="ThoiGianLapLai" value="<%: oKyBaoCao.ThoiGianLapLai%>" class="form-control" />
            </div>
            <div for="ThoiGianLapLai" class="col-sm-1"><span id="ForThoiGianLapLai">Ngày</span></div>
        </div>
        <div class="form-group row divNgayThu customchoie" for="NgayThu" style="display: none;">
            <label for="NgayThu" class="col-sm-2 control-label">Chọn thứ</label>
            <div class="col-sm-10">

                <div class="checkbox-group" id="checkboxes">
                    <ul>
                        <li>
                            <input type="checkbox" id="2" data-title="Thứ 2" />
                            <label for="2">Thứ 2</label>
                        </li>
                        <li>
                            <input type="checkbox" id="3" data-title="Thứ 3" />
                            <label for="3">Thứ 3</label>
                        </li>
                        <li>
                            <input type="checkbox" id="4" data-title="Thứ 4" />
                            <label for="4">Thứ 4</label>
                        </li>
                        <li>
                            <input type="checkbox" id="5" data-title="Thứ 5" />
                            <label for="5">Thứ 5</label>
                        </li>
                        <li>
                            <input type="checkbox" id="6" data-title="Thứ 6" />
                            <label for="6">Thứ 6</label>
                        </li>
                        <li>
                            <input type="checkbox" id="7" data-title="Thứ 7" />
                            <label for="7">Thứ 7</label>
                        </li>
                        <li>
                            <input type="checkbox" id="1" data-title="Chủ nhật" />
                            <label for="1">Chủ nhật</label>
                        </li>
                    </ul>
                </div>
                <span id="allChecked" style="display: block; width: 425px; text-align: center; color: #999999;">(Chưa chọn ngày)
                </span>
            </div>
        </div>
        <div class="form-group row divThangBaoBao customchoie" for="ThangBaoBao" style="display: none;">
            <label for="ThangBaoBao" class="col-sm-2 control-label">Chọn tháng</label>
            <div class="col-sm-10">

                <div class="checkbox-group" id="checkboxesThangBaoBao">
                    <ul>
                        <li>
                            <input type="checkbox" id="thang1" data-id="1" data-title="Tháng 1" />
                            <label for="thang1">Tháng 1</label>
                        </li>
                        <li>
                            <input type="checkbox" id="thang2" data-id="2" data-title="Tháng 2" />
                            <label for="thang2">Tháng 2</label>
                        </li>
                        <li>
                            <input type="checkbox" id="thang3" data-id="3" data-title="Tháng 3" />
                            <label for="thang3">Tháng 3</label>
                        </li>
                        <li>
                            <input type="checkbox" id="thang4" data-id="4" data-title="Tháng 4" />
                            <label for="thang4">Tháng 4</label>
                        </li>
                        <li>
                            <input type="checkbox" id="thang5" data-id="5" data-title="Tháng 5" />
                            <label for="thang5">Tháng 5</label>
                        </li>
                        <li>
                            <input type="checkbox" id="thang6" data-id="6" data-title="Tháng 6" />
                            <label for="thang6">Tháng 6</label>
                        </li>
                        <li>
                            <input type="checkbox" id="thang7" data-id="7" data-title="Tháng 7" />
                            <label for="thang7">Tháng 7</label>
                        </li>
                        <li>
                            <input type="checkbox" id="thang8" data-id="8" data-title="Tháng 8" />
                            <label for="thang8">Tháng 8</label>
                        </li>
                        <li>
                            <input type="checkbox" id="thang9" data-id="9" data-title="Tháng 9" />
                            <label for="thang9">Tháng 9</label>
                        </li>
                        <li>
                            <input type="checkbox" id="thang10" data-id="10" data-title="Tháng 10" />
                            <label for="thang10">Tháng 10</label>
                        </li>
                        <li>
                            <input type="checkbox" id="thang11" data-id="11" data-title="Tháng 11" />
                            <label for="thang11">Tháng 11</label>
                        </li>
                        <li>
                            <input type="checkbox" id="thang12" data-id="12" data-title="Tháng 12" />
                            <label for="thang12">Tháng 12</label>
                        </li>

                    </ul>
                </div>
                <span id="allChecked_ThangBaoBao" style="display: block; width: 425px; text-align: center; color: #999999;">(Chưa chọn tháng)
                </span>
            </div>
        </div>

        <div class="form-group divThangBaoBao customchoie row" for="ThangBaoBao">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label"></label>
            <div class="col-sm-10">
                <div class="row">
                    <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">
                        <input type="radio" name="PhanLoaiThang" value="ThaoNgay" />
                        Ngày</label>
                    <div class="col-sm-10">

                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="1" />1
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="2" />2
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="3" />3
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="4" />4
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="5" />5
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="6" />6
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="7" />7
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="8" />8
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="9" />9
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="10" />10
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="11" />11
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="12" />12
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="13" />13
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="14" />14
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="15" />15
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="16" />16
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="17" />17
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="18" />18
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="19" />19
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="20" />20
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="21" />21
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="22" />22
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="23" />23
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="24" />24
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="25" />25
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="26" />26
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="27" />27
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="28" />28
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="29" />29
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="30" />30
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="31" />31
                        </label>
                        <label class="checkbox-inline">
                            <input name="cbk_dayofmonth" type="checkbox" value="Last" />Cuối
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group divThangBaoBao customchoie row" for="ThangBaoBao">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label"></label>
            <div class="col-sm-10">
                <div class="row">
                    <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">
                        <input type="radio" name="PhanLoaiThang" value="ThaoNgay" />
                        Tuần</label>
                    <div class="col-sm-10">
                        <div>
                            <label class="checkbox-inline">
                                <input name="cbk_weekofmonth" type="checkbox" value="1" />1
                            </label>
                            <label class="checkbox-inline">
                                <input name="cbk_weekofmonth" type="checkbox" value="2" />2
                            </label>
                            <label class="checkbox-inline">
                                <input name="cbk_weekofmonth" type="checkbox" value="3" />3
                            </label>
                            <label class="checkbox-inline">
                                <input name="cbk_weekofmonth" type="checkbox" value="4" />4
                            </label>
                            <label class="checkbox-inline">
                                <input name="cbk_weekofmonth" type="checkbox" value="5" />Cuối
                            </label>
                        </div>
                        <div>
                            <div class="row">
                                <label class="no-padding-md-x col-xs-12 col-sm-3 col-md-3 control-label">
                                    Thứ trong tuần
                                </label>
                                <div class="col-sm-9">
                                    <label class="checkbox-inline">
                                        <input name="cbk_dayofweek" type="checkbox" value="1" />CN
                                    </label>
                                    <label class="checkbox-inline">
                                        <input name="cbk_dayofweek" type="checkbox" value="2" />2
                                    </label>
                                    <label class="checkbox-inline">
                                        <input name="cbk_dayofweek" type="checkbox" value="3" />3
                                    </label>
                                    <label class="checkbox-inline">
                                        <input name="cbk_dayofweek" type="checkbox" value="4" />4
                                    </label>
                                    <label class="checkbox-inline">
                                        <input name="cbk_dayofweek" type="checkbox" value="5" />5
                                    </label>
                                    <label class="checkbox-inline">
                                        <input name="cbk_dayofweek" type="checkbox" value="6" />6
                                    </label>
                                    <label class="checkbox-inline">
                                        <input name="cbk_dayofweek" type="checkbox" value="7" />7
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Đơn vị tham gia</label>
            <div class="col-sm-10">
                <select data-selected="<%=string.Join(",", oKyBaoCao.DonViThamGiaBC.Select(x=>x.LookupId)) %>" data-url="/UserControls/AdminCMS/LGroup/pAction.asp?do=QUERYDATA&GroupIsDonVi=1" data-place="Chọn Đơn vị tham gia" name="DonViThamGiaBC" id="DonViThamGiaBC" class="form-control" multiple="multiple"></select>
            </div>
        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Cần đơn vị phê duyệt</label>
            <div class="col-sm-4">
                <input value="1" type="checkbox" name="isLDDonViDuyet" id="isLDDonViDuyet" <%=oKyBaoCao.isLDDonViDuyet == 1 ? "checked" : string.Empty%> />
            </div>
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Đối tượng thực hiện</label>
            <div class="col-sm-4">
                 <select id="DoiTuongThucHien" name="DoiTuongThucHien" class="form-control">
                    <option value="1">Giao đơn vị</option>
                    <option value="2">Giao cá nhân</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-sm-10">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#btnDonVi").click(function () {

            });
            $('input[name="BCTuNgay"]').daterangepicker({
                timePicker: true,
                singleDatePicker: true,
                locale: {
                    format: 'DD/MM/YYYY HH:mm'
                }
            });

            $('input[name="BCDenNgay"]').daterangepicker({
                timePicker: true,
                singleDatePicker: true,
                locale: {
                    format: 'DD/MM/YYYY HH:mm'
                }
            });

            $("#BCLoai").val("<%:oKyBaoCao.BCLoai%>");
            $("#DoiTuongThucHien").val("<%:oKyBaoCao.DoiTuongThucHien%>");
            SetLoaiBaoCao('<%:oKyBaoCao.BCLoai%>');
            $('#BCLoai').on('change', function () {
                var $BCLoai = this.value;
                SetLoaiBaoCao($BCLoai);
            });

            $("#LoaiBaoCao").smSelect2018V2({
                dropdownParent: "#frm-KyBaoCao",
                parameterPlus: function (para) {
                    // Hàm post dữ liệu.
                    para["DonViId"] = '<%=CurentUser.Groups.ID%>';
                    para["moder"] = 1;
                }
            });
            //DonViThamGiaBC
            $("#DonViThamGiaBC").smSelect2018V2({
                dropdownParent: "#frm-KyBaoCao"
            });
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oKyBaoCao.ListFileAttach)%>'
            });
            $("#Title").focus();

            $("#frm-KyBaoCao").validate({
                ignore: [],
                rules: {
                    Title: "required",
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề",
                },
                submitHandler: function (form) {
                    $.post("/UserControls/BaoCao/BCTongHop/KyBaoCao/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-KyBaoCao").trigger("click");
                            $(form).closeModal();
                            $('#tblKyBaoCao').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            //checbox ngày
            var checkboxes = $('.divNgayThu input:checkbox'),
                span = $('.divNgayThu #allChecked');

            checkboxes.on('change', function () {
                var checked = checkboxes.filter(':checked');
                //fix lại giá trị cho 
                var dayids = jQuery.map(checked, function (n, i) {
                    return n.id;
                });
                $("#NgayThu").val(dayids.join(','));
                if (checked.length == checkboxes.length)
                    span.text('(Tất cả ngày trong tuần)');
                else {
                    var days = jQuery.map(checked, function (n, i) {
                        return $(n).attr("data-title");
                    });
                    span.text('(Đã chọn: ' + days.join(', ') + ')');
                }
            });
            SetCheckBoxThu();

            //check phần tháng.
            var checkboxes_thang = $('.divThangBaoBao input:checkbox'),
                spanThang = $('.divThangBaoBao #allChecked_ThangBaoBao');

            checkboxes_thang.on('change', function () {
                var checked = checkboxes_thang.filter(':checked');
                //fix lại giá trị cho 
                var dayids = jQuery.map(checked, function (n, i) {
                    return $(n).attr("data-id");
                });
                $("#ThangBaoBao").val(dayids.join(','));
                if (checked.length == checkboxes.length)
                    spanThang.text('(Tất cả các tháng trong năm)');
                else {
                    var days = jQuery.map(checked, function (n, i) {
                        return $(n).attr("data-title");
                    });
                    spanThang.text('(Đã chọn: ' + days.join(', ') + ')');
                }
            });
            //cbk_dayofmonth
            var $cbk_dayofmonth = $('.divThangBaoBao input[name="cbk_dayofmonth"]');
            $cbk_dayofmonth.on('change', function () {
                var checked = $cbk_dayofmonth.filter(':checked');
                //fix lại giá trị cho 
                var dayids = jQuery.map(checked, function (n, i) {
                    return n.value;
                });
                $("#NgayThu").val(dayids.join(','));
            });
            //end
            //check tuần tuần trong báo cáo.
            var $cbk_weekofmonth = $('.divThangBaoBao input[name="cbk_weekofmonth"]');
            $cbk_weekofmonth.on('change', function () {
                var checked = $cbk_weekofmonth.filter(':checked');
                //fix lại giá trị cho 
                var dayids = jQuery.map(checked, function (n, i) {
                    return n.value;
                });
                $("#TuanThu").val(dayids.join(','));
            });

            var $cbk_dayofweek = $('.divThangBaoBao input[name="cbk_dayofweek"]');
            $cbk_dayofweek.on('change', function () {
                var checked = $cbk_dayofweek.filter(':checked');
                //fix lại giá trị cho 
                var dayids = jQuery.map(checked, function (n, i) {
                    return n.value;
                });
                $("#NgayThu").val(dayids.join(','));
            });
            $("#frm-KyBaoCao").viForm();
        });

        function SetCheckBoxThu() {
            span = $('.divNgayThu #allChecked');
            var $ThuChon = $("#NgayThu").val();
            var checkboxes = $('.divNgayThu input:checkbox');
            const myArrId = $ThuChon.split(",");
            checkboxes.each(function () {
                var sThisVal = $(this).attr("id");
                //sList += (sList == "" ? sThisVal : "," + sThisVal);
                var temp = jQuery.inArray(sThisVal, myArrId);
                if (temp != "-1") {
                    $(this).prop('checked', true);
                }

            });
            var checked = checkboxes.filter(':checked');
            var days = jQuery.map(checked, function (n, i) {
                return $(n).attr("data-title");
            });
            span.text('(Đã chọn: ' + days.join(', ') + ')');
        }

        function SetLoaiBaoCao($BCLoai) {
            //reset form về ban đầu.
            $(".customchoie").hide();
            //$(".BCTuNgay").show();
            switch ($BCLoai) {
                case "1":
                    // code block
                    //$("#BCTuNgay").hide();
                    break;
                case "2":
                    // code block
                    break;
                case "3":
                    $("div[for='ThoiGianLapLai']").show();
                    $("#ForThoiGianLapLai").html('Ngày')
                    // code block
                    break;
                case "4":
                    $("div[for='ThoiGianLapLai']").show();
                    $("div[for='NgayThu']").show();
                    $("#ForThoiGianLapLai").html('Tuần');
                    break;
                case "5":
                    //$("div[for='ThoiGianLapLai']").show();
                    $("div[for='ThangBaoBao']").show();
                    //$("#ForThoiGianLapLai").html('Tuần');
                    //divThangBaoBao ThangBaoBao
                    break;

                default:
                // code block
            }
        }
    </script>

    <style type="text/css">
        #frm-KyBaoCao .checkbox-group {
        }

            #frm-KyBaoCao .checkbox-group ul {
                border-radius: 4px;
                display: inline-block;
                margin-bottom: 0;
                margin-left: 0;
                padding-left: 0;
            }

                #frm-KyBaoCao .checkbox-group ul > li {
                    display: inline;
                }

            #frm-KyBaoCao .checkbox-group input {
                float: left;
            }

            #frm-KyBaoCao .checkbox-group input {
                display: none;
            }

            #frm-KyBaoCao .checkbox-group ul > li > label {
                -moz-border-bottom-colors: none;
                -moz-border-left-colors: none;
                -moz-border-right-colors: none;
                -moz-border-top-colors: none;
                background-color: rgba(0, 0, 0, 0);
                border-color: #CCCCCC;
                border-image: none;
                border-style: solid;
                border-width: 1px 1px 1px 0;
                box-shadow: 0 5px 5px -6px rgba(0, 0, 0, 0.8) inset;
                color: #444444;
                float: left;
                font-family: helvetica;
                font-size: 12px;
                line-height: 20px;
                padding: 5px 12px 2px;
                text-decoration: none;
            }

            #frm-KyBaoCao .checkbox-group ul > li:first-child > label {
                border-bottom-left-radius: 4px;
                border-left-width: 1px;
                border-top-left-radius: 4px;
            }

            #frm-KyBaoCao .checkbox-group ul > li:last-child > label {
                border-bottom-right-radius: 4px;
                border-top-right-radius: 4px;
            }

            #frm-KyBaoCao .checkbox-group ul > li > *:checked + label {
                background-color: #3399CC;
                background-image: -moz-linear-gradient(center top, rgba(255, 255, 255, 0) 0%, rgba(0, 0, 0, 0.2) 100%);
                color: #FFFFFF;
            }

            #frm-KyBaoCao .checkbox-group ul > li > *:checked + *:hover, .checkbox-group ul > li > *:checked + label:focus {
                background-color: #3399CC;
                background-image: none;
            }

            #frm-KyBaoCao .checkbox-group ul > li > label:hover, .checkbox-group ul > li > label:focus {
                background-color: #CCCCCC;
                background-image: none;
                color: #FFFFFF;
                cursor: pointer;
            }
    </style>
</body>
</html>
