using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortalAPP
{
    public partial class pViewKyBaoCaoGiao : pFormBase
    {
        public KyBaoCaoItem oKyBaoCao = new KyBaoCaoItem();
        public KyBaoCaoItem RootKyDanhGi = new KyBaoCaoItem();
        public int isTongHop = 0;
        public DMBaoCaoItem oDMBaoCaoItem { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oKyBaoCao = new KyBaoCaoItem();
            oDMBaoCaoItem = new DMBaoCaoItem();
            if (ItemID > 0)
            {
                KyBaoCaoDA oKyBaoCaoDA = new KyBaoCaoDA();
                oKyBaoCao = oKyBaoCaoDA.GetByIdToObject<KyBaoCaoItem>(ItemID);
                if(oKyBaoCao.LoaiBaoCao.LookupId > 0)
                {
                    DMBaoCaoDA dMBaoCaoDA = new DMBaoCaoDA();
                    oDMBaoCaoItem = dMBaoCaoDA.GetByIdToObject<DMBaoCaoItem>(oKyBaoCao.LoaiBaoCao.LookupId);
                    
                }
                if (oDMBaoCaoItem.LoaiBaoCao == 2)
                {
                    isTongHop = 1;
                }
                if (oKyBaoCao.idRootKyDanhGia > 0)
                {
                    RootKyDanhGi = oKyBaoCaoDA.GetByIdToObject<KyBaoCaoItem>(oKyBaoCao.idRootKyDanhGia);

                }
                else RootKyDanhGi = oKyBaoCao;
                //
            }
        }
    }
}