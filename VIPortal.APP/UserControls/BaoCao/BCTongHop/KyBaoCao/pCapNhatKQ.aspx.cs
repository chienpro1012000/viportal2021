﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData;

namespace VIPortal.APP.UserControls.BaoCao.BCTongHop.KyBaoCao
{
    public partial class pCapNhatKQ : pFormBase
    {
        public KyBaoCaoItem oKyBaoCao { get; set; }
        public int isjsonBCCT = 0;
        public DMBaoCaoItem oDMBaoCaoItem { get; set; }
        public string urlfile { get; private set; }
        public string UrlFileBaoCaoChiTietNhap { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oKyBaoCao = new KyBaoCaoItem();
            UrlFileBaoCaoChiTietNhap = "";
            if (ItemID > 0)
            {
                KyBaoCaoDA oKyBaoCaoDA = new KyBaoCaoDA();
                oKyBaoCao = oKyBaoCaoDA.GetByIdToObject<KyBaoCaoItem>(ItemID);
                if (oKyBaoCao.LoaiBaoCao.LookupId > 0)
                {
                    DMBaoCaoDA dMBaoCaoDA = new DMBaoCaoDA();
                    oDMBaoCaoItem = dMBaoCaoDA.GetByIdToObject<DMBaoCaoItem>(oKyBaoCao.LoaiBaoCao.LookupId);

                    if (!string.IsNullOrEmpty(oDMBaoCaoItem.UrlFileBCChiTiet))
                    {
                        //write file ra thư mục upload.
                        byte[] DataFile = dMBaoCaoDA.SpListProcess.ParentWeb.GetFile(oDMBaoCaoItem.UrlFileBCChiTiet).OpenBinary();
                        urlfile = @"/Uploads/ajaxUpload/MauBCCT" + ItemID + string.Format("{0:ddMMyyHHmmss}", DateTime.Now) + ".xlsx";
                        UrlFileBaoCaoChiTietNhap = Server.MapPath(urlfile);
                        System.IO.File.WriteAllBytes(UrlFileBaoCaoChiTietNhap, DataFile);

                    }

                }

            }
        }
    }
}