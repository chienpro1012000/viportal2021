﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using VIPortalAPP;
using ViPortalData;

namespace VIPortal.APP.UserControls.BaoCao.BCTongHop.KyBaoCao
{
    public partial class pFormDuyetBaoCao : pFormBase
    {
        public KyBaoCaoItem oKyBaoCao { get; set; }
        public int? UserPhuTrach = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            oKyBaoCao = new KyBaoCaoItem();

            if (ItemID > 0)
            {
                KyBaoCaoDA oKyBaoCaoDA = new KyBaoCaoDA();
                oKyBaoCao = oKyBaoCaoDA.GetByIdToObject<KyBaoCaoItem>(ItemID);
                //get ra người trình mới nhất.
                NhatKyXuLyDA nhatKyXuLyDA = new NhatKyXuLyDA();
                List<NhatKyXuLyJson> lstNhatKy = nhatKyXuLyDA.GetListJson(new NhatKyXuLyQuery()
                {
                    LogIDDoiTuong = ItemID,
                    LogDoiTuong = "KyBaoCao",
                    FieldOrder = "ID",
                    Ascending = false
                });
                UserPhuTrach = lstNhatKy.FirstOrDefault(x => x.LogTrangThai == 7)?.CreatedUser.ID;
            }
        }
    }
}