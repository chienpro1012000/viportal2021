﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormGiaoBaoCaoDonVi.aspx.cs" Inherits="VIPortal.APP.UserControls.BaoCao.BCTongHop.KyBaoCao.pFormGiaoBaoCaoDonVi" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-FormGiaoBaoCao" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oKyBaoCao.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" name="DoiTuongTitle" id="DoiTuongTitle" value="Báo cáo Kỳ" />
        <input type="hidden" name="LogDoiTuong" id="LogDoiTuong" value="KyBaoCao" />
        <input type="hidden" name="LogIDDoiTuong" id="LogIDDoiTuong" value="<%=oKyBaoCao.ID %>" />
        <input type="hidden" name="LogThaoTac" id="LogThaoTac" value="ChuyenBaoCao" />
        <input type="hidden" name="LogTrangThai" id="LogTrangThai" value="3" />
        <input type="hidden" name="TrangThaiParent" id="TrangThaiParent" value="<%=oKyBaoCao.TrangThaiBaoCao %>" />
        <input type="hidden" name="CreatedUser" id="CreatedUser" value="<%=CurentUser.ID %>" />
        <div class="form-group row">
            <label for="LogNoiDung" class="col-sm-2 control-label">Ý kiến</label>
            <div class="col-sm-10">
                <textarea name="LogNoiDung" id="LogNoiDung" class="form-control"></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="TrangThaiBaoCao" class="col-sm-2 control-label">Trạng thái</label>
            <div class="col-sm-10">
               <select class="form-control" id="TrangThaiBaoCao" name="TrangThaiBaoCao">
                   <%if(oKyBaoCao.TrangThaiBaoCao == 3) {%>
                   <option value="3">Chuyển tiếp</option>
                   <!--<option value="4">Bàn giao, chờ phân công</option>-->
                   <option value="5">Giao báo cáo</option>
                   <%}%>
                   <%if(oKyBaoCao.TrangThaiBaoCao == 6) {%>
                   <option value="7">Trình phê duyệt kết quả báo cáo</option>
                   <%}%>
               </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="UserPhuTrach" class="col-sm-2 control-label">Cán bộ</label>
            <div class="col-sm-10">
                <select data-selected="" data-url="/UserControls/AdminCMS/LUser/pAction.asp?do=QUERYDATA" data-place="Chọn cán bộ" name="UserPhuTrach" id="UserPhuTrach" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#FileAttach").regFileUpload({
                files: '[]'
            });
            //UserPhuTrach
            $("#UserPhuTrach").smSelect2018V2({
                dropdownParent: "#frm-FormGiaoBaoCao",
                parameterPlus: function (para) {
                    // Hàm post dữ liệu.
                    para["Groups"] = '<%=oKyBaoCao.fldGroup.LookupId > 0 ? oKyBaoCao.fldGroup.LookupId : CurentUser.Groups.ID%>'
                },
            });
            $("#frm-FormGiaoBaoCao").validate({
                rules: {
                    LogNoiDung: "required",
                    UserPhuTrach: "required",
                    //TrangThaiBaoCao: "required",
                },
                messages: {     
                    LogNoiDung: "Vui lòng nhập nội dung"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/BaoCao/BCTongHop/KyBaoCao/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-DMLinhVuc").trigger("click");
                            $(form).closeModal();
                            $("#frmViewBaoCao").closeModal();
                            var idtable = $('#contentbc div.tab-pane.active table.dataTable').attr('id');
                            $('#' + idtable).DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-FormGiaoBaoCao").viForm();
        });
    </script>
</body>
</html>
