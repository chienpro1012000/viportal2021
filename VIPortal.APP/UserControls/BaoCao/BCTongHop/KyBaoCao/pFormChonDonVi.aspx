﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pFormChonDonVi.aspx.cs" Inherits="VIPortal.APP.UserControls.BaoCao.BCTongHop.KyBaoCao.pFormChonDonVi" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        $(document).ready(function () {


            $('#treeLgroup').fancytree({
                checkbox: true,
                selectMode: 2,
                //  extensions: ['contextMenu'],
                source: {
                    url: "/VIPortalAPI/api/Lgroup/QUERYTREE?GroupIsDonVi=1"
                },
                renderNode: function (event, data) {
                    //console.log(data);
                    var node = data.node;
                    $(node.span).attr('data-id', data.node.key);

                },
                activate: function (event, data) {
                    // A node was activated: display its title:
                    //var node = data.node;
                    //var tree = $('#treePhongBan').fancytree('getTree');
                    //tree.reload({
                    //    url: "/VIPortalAPI/api/Lgroup/QUERYTREE?isGetParent=true&isGetByParent=true&GroupIsDonVi=2&GroupParent=" + node.key
                    //});
                    //$('#UserPhongBan').val(data.node.key);
                    //$("#btnAddUser").attr("data-UserPhongBan", data.node.key);
                    //$("#btnAddUser").data('UserPhongBan', data.node.key);
                    //$('#tblLUser').DataTable().ajax.reload();
                },
                select: function (event, data) {
                    // Display list of selected nodes
                    var selNodes = data.tree.getSelectedNodes();
                    // convert to title/key array
                    var selKeys = $.map(selNodes, function (node) {
                        return "[" + node.key + "]: '" + node.title + "'";
                    });
                    console.log(selKeys);
                },
            });
            $("#frm-pFormChonDonVi").validate({
                submitHandler: function (form) {
                    //set select2 value
                    var tree = $('#treeLgroup').fancytree('getTree');
                    var selNodes = tree.getSelectedNodes();
                    var selKeys = $.map(selNodes, function (node) {
                        return node.key;
                    });
                    $("#DonViThamGiaBC").val(selKeys).trigger('change');

                    //set id va obj
                    $(form).closeModal();
                }
            });

            $("#frm-pFormChonDonVi").closest('.bootstrap-dialog').on('hidden.bs.modal', function (event) {
                console.log(123);
                if ($('.modal:visible').length) {
                    $('body').addClass('modal-open');
                }
            });
        });
    </script>
</head>
<body>
    <form id="frm-pFormChonDonVi" class="form-horizontal">

        <div class="container-fluid">
            <!-- Control the column width, and how they should appear on different devices -->
            <div class="row">
                <div class="col-sm-10">
                    <h5>Cơ cấu tổ chức</h5>
                    <div id="treeLgroup"></div>
                </div>

            </div>
        </div>


    </form>
</body>
</html>
