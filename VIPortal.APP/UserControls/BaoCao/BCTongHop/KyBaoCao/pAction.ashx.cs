using DevExpress.Spreadsheet;
using Microsoft.SharePoint;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;
using ViPortalData;
using ViPortalData.Lconfig;

namespace VIPortalAPP.KyBaoCao
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {

        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            KyBaoCaoDA oKyBaoCaoDA = new KyBaoCaoDA();
            KyBaoCaoItem oKyBaoCaoItem = new KyBaoCaoItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    #region MyRegion
                    oResult.IsQuery = true;
                    KyBaoCaoQuery kyBaoCaoQuery = new KyBaoCaoQuery(context.Request);
                    kyBaoCaoQuery.fldUser = CurentUser.ID;
                    var oData = oKyBaoCaoDA.GetListJson(kyBaoCaoQuery);
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw,
                        oKyBaoCaoDA.TongSoBanGhiSauKhiQuery);
                    oGrid.Query = oKyBaoCaoDA.Queryreturn;
                    oResult.OData = oGrid;
                    AddLog("QUERYDATA", "KyBaoCao", "QUERYDATA", 0, "Xem danh sách kỳ báo cáo");
                    break;
                #endregion
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oKyBaoCaoDA.GetListJson(new KyBaoCaoQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "SIGNXLSX":
                    if (!string.IsNullOrEmpty(context.Request["urlfile"]))
                    {
                        string urllocalpdf = @"/Uploads/ajaxUpload/BCCT" + oGridRequest.ItemID + string.Format("{0:ddMMyyHHmmss}", DateTime.Now) + ".pdf";
                        string UrlFileBaoCaoChiTietNhapPDF = HttpContext.Current.Server.MapPath(urllocalpdf);
                        string urlfile = context.Request["urlfile"];
                        byte[] FileData = oKyBaoCaoDA.SpListProcess.ParentWeb.GetFile(urlfile).OpenBinary();
                        if (urlfile.EndsWith(".xlsx"))
                        {
                            string urllocal = @"/Uploads/ajaxUpload/BCCT" + oGridRequest.ItemID + string.Format("{0:ddMMyyHHmmss}", DateTime.Now) + ".xlsx";
                            string UrlFileBaoCaoChiTietNhap = HttpContext.Current.Server.MapPath(urllocal);
                            System.IO.File.WriteAllBytes(UrlFileBaoCaoChiTietNhap, FileData);
                            // Specify the cancellation token.
                            // Create a new Workbook object.
                            using (Workbook workbook = new Workbook())
                            {
                                try
                                {
                                    // Asynchronously load the XLSX file.
                                    workbook.LoadDocument(UrlFileBaoCaoChiTietNhap);
                                    // Asynchronously convert the XLSX file to PDF.
                                    workbook.ExportToPdf(UrlFileBaoCaoChiTietNhapPDF);
                                }
                                catch (OperationCanceledException)
                                {
                                    UrlFileBaoCaoChiTietNhapPDF = "";
                                }
                            }
                        }else if (urlfile.EndsWith(".pdf"))
                        {
                            System.IO.File.WriteAllBytes(UrlFileBaoCaoChiTietNhapPDF, FileData);
                        }
                        else
                        {
                            UrlFileBaoCaoChiTietNhapPDF = "";
                        }
                        if (!string.IsNullOrEmpty(UrlFileBaoCaoChiTietNhapPDF))
                        {
                            //update vào bản ghi đó.
                            KyBaoCaoItem kyBaoCaoItem = oKyBaoCaoDA.GetByIdToObject<KyBaoCaoItem>(oGridRequest.ItemID);
                            byte[] documentContentAsByteArray = clsFucUtils.ReadFile(UrlFileBaoCaoChiTietNhapPDF);
                            //thực hiện ký số ở đây.
                            string Base64File = Convert.ToBase64String(documentContentAsByteArray);

                            //https://portal.evnhanoi.vn/apigateway/CAPUBLIC_GD

                            LconfigDA oLconfigDA = new LconfigDA();
                            string APIKYSO = oLconfigDA.GetListJson(new LconfigQuery() { ConfigType = "APIKYSO" }).FirstOrDefault()?.ConfigValue;
                            if (string.IsNullOrEmpty(APIKYSO))
                                APIKYSO = "https://portal.evnhanoi.vn/apigateway";
                            HttpClient client = new HttpClient();
                            var byteArray = Encoding.ASCII.GetBytes("EVNHANOI:Evnhanoi@123");
                            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
                            HttpResponseMessage response = client.PostAsync(APIKYSO + "/CAPUBLIC_GD", new StringContent(
                                Newtonsoft.Json.JsonConvert.SerializeObject(new
                                {
                                    MaDonVi = "X5",
                                    HoTenNguoiKy = "Nguyễn Quang Anh",
                                    ChucVuNguoiKy = "GIÁM ĐỐC",
                                    Base64File = Base64File
                                }), Encoding.UTF8, "application/json")).Result;

                            //HttpResponseMessage response = client.PostAsJsonAsync(APIEVNDanhBa + "/HRMS_DanhBa_NhanVien").Result;
                            response.EnsureSuccessStatusCode();
                            string responseBody = response.Content.ReadAsStringAsync().Result;
                            MeesageKySo oMeesageKySo = Newtonsoft.Json.JsonConvert.DeserializeObject<MeesageKySo>(responseBody);
                            if (oMeesageKySo.suc)
                            {
                                byte[] bytes = System.Convert.FromBase64String(oMeesageKySo.data);
                                kyBaoCaoItem.ListFileAttachAdd.Add(
                                new ViPortal_Utils.Base.FileAttach() { Name = Path.GetFileNameWithoutExtension(urlfile) + ".signed.pdf", DataFile = bytes });
                                oKyBaoCaoDA.UpdateObject<KyBaoCaoItem>(kyBaoCaoItem);
                                oResult.Message = "Ký thành công file báo cáo";
                            }
                            else
                            {
                                oResult.Message = "Ký số không thành công. " + oMeesageKySo.msg;
                                oResult.State = ActionState.Error;
                            }
                            //kyBaoCaoItem.TitleKetQua = "Báo cáo số liệu File excel";
                            //kyBaoCaoItem.TrangThaiBaoCao = 6;
                            //kyBaoCaoItem.ListFileRemove = kyBaoCaoItem.ListFileAttach.Select(x => x.Name).ToList();
                        }
                        else
                        {
                            oResult.Message = "Định dạng không thể ký";
                            oResult.State = ActionState.Error;
                        }
                    }
                    else
                    {
                        oResult.Message = "Không xác định bản ghi cần lưu trữ.";
                        oResult.State = ActionState.Error;
                    }
                    break;
                case "GETJSONBCCT":
                    if (oGridRequest.ItemID > 0)
                    {
                        oKyBaoCaoItem = oKyBaoCaoDA.GetByIdToObject<KyBaoCaoItem>(oGridRequest.ItemID);
                        if (!string.IsNullOrEmpty(oKyBaoCaoItem.jsonBCCT))
                        {
                            JObject json = JObject.Parse(oKyBaoCaoItem.jsonBCCT);
                            oResult.OData = json;
                        }
                        else oResult = new ResultAction() { State = ActionState.Error, Message = "Không xác định cấu trúc báo cáo" };
                    }
                    else
                        oResult.Message = "Không xác định bản ghi cần lưu trữ.";
                    break;
                case "UPDATE":
                    #region MyRegion
                    if (oGridRequest.ItemID > 0)
                    {
                        //chạy vào đây và update thêm một số thứ.
                        oKyBaoCaoItem = oKyBaoCaoDA.GetByIdToObject<KyBaoCaoItem>(oGridRequest.ItemID);
                        oKyBaoCaoItem.UpdateObject(context.Request);
                        oKyBaoCaoDA.UpdateObject<KyBaoCaoItem>(oKyBaoCaoItem);
                        AddLog("UPDATE", "KyBaoCao", "UPDATE", oGridRequest.ItemID, $"Cập nhật kỳ báo cáo{oKyBaoCaoItem.Title}");
                    }
                    else
                    {
                        oKyBaoCaoItem.UpdateObject(context.Request);
                        oKyBaoCaoDA.UpdateObject<KyBaoCaoItem>(oKyBaoCaoItem);
                        AddLog("UPDATE", "KyBaoCao", "ADD", oGridRequest.ItemID, $"Thêm mới kỳ báo cáo{oKyBaoCaoItem.Title}");
                    }
                    #endregion
                    oResult.Message = "Lưu thành công";
                    break;
                case "THUHOI":
                    #region MyRegion
                    if (oGridRequest.ItemID > 0)
                    {
                        //chạy vào đây và update thêm một số thứ.
                        oKyBaoCaoItem = oKyBaoCaoDA.GetByIdToObject<KyBaoCaoItem>(oGridRequest.ItemID);
                        //lấy theo logtext lấy lại thông tin.
                        if (!string.IsNullOrEmpty(oKyBaoCaoItem.LogText))
                        {
                            string lastLog = oKyBaoCaoItem.LogText.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault();
                            if (lastLog.StartsWith($"{CurentUser.ID}_"))
                            {
                                int CountNhatky = oKyBaoCaoItem.LogText.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries).Length;
                                if (CountNhatky >= 2)
                                {
                                    string NeartheEnd = oKyBaoCaoItem.LogText.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries)[CountNhatky - 2];
                                    //30_3_30
                                    oKyBaoCaoItem.TrangThaiBaoCao = Convert.ToInt32(NeartheEnd.Split('_')[1]);
                                    oKyBaoCaoItem.CurrentUserPT = $",{CurentUser.ID},";
                                    int UserRemove = Convert.ToInt32(NeartheEnd.Split('_')[2]);
                                    int indexUserREmove = oKyBaoCaoItem.UserThamGia.FindLastIndex(x => x.LookupId == UserRemove);
                                    oKyBaoCaoItem.UserThamGia.RemoveAt(indexUserREmove);

                                }
                                else
                                {
                                    //nếu ko có thì quay về bước tiếp nhận.
                                    oKyBaoCaoItem.TrangThaiBaoCao = 3;
                                    oKyBaoCaoItem.CurrentUserPT = $",{CurentUser.ID},";
                                    oKyBaoCaoItem.UserThamGia = new SPFieldLookupValueCollection($"{CurentUser.ID};#{CurentUser.Title}");
                                }
                                List<string> lstLog = oKyBaoCaoItem.LogText.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                                lstLog.RemoveAt(lstLog.Count - 1);
                                oKyBaoCaoItem.LogText = string.Join("|", lstLog);
                                //
                                //thay người phụ trách khi giao.
                                Dictionary<string, object> lstUpdate = new Dictionary<string, object>()
                            {
                                {"TrangThaiBaoCao", oKyBaoCaoItem.TrangThaiBaoCao},
                                {"UserThamGia",  oKyBaoCaoItem.UserThamGia },
                                {"LogText", oKyBaoCaoItem.LogText },
                                 {"CurrentUserPT", oKyBaoCaoItem.CurrentUserPT }
                            };
                                oResult.Message = oKyBaoCaoDA.UpdateOneOrMoreField(oGridRequest.ItemID, lstUpdate, true);
                                if (string.IsNullOrEmpty(oResult.Message))
                                {
                                    NhatKyXuLyItem nhatKyXuLyItem = new NhatKyXuLyItem();
                                    nhatKyXuLyItem.UpdateObject(context.Request);
                                    nhatKyXuLyItem.LogThaoTac = "THUHOI";
                                    NhatKyXuLyDA nhatKyXuLyDA = new NhatKyXuLyDA();
                                    nhatKyXuLyDA.UpdateObject<NhatKyXuLyItem>(nhatKyXuLyItem);
                                    oResult.Message = "Thu hồi thành công";
                                }
                                else
                                {
                                    oResult = new ResultAction()
                                    {
                                        State = ActionState.Error,
                                        Message = oResult.Message
                                    };
                                }
                            }
                        }
                    }
                    else
                    {
                        oResult = new ResultAction()
                        {
                            State = ActionState.Error,
                            Message = "Không xác định đối tượng thực hiện"
                        };
                    }
                    #endregion
                    break;
                case "UPDATEKETQUA":
                    if (oGridRequest.ItemID > 0)
                    {
                        //chạy vào đây và update thêm một số thứ.
                        oKyBaoCaoItem = oKyBaoCaoDA.GetByIdToObject<KyBaoCaoItem>(oGridRequest.ItemID);
                        #region Update nhận ký
                        NhatKyXuLyItem nhatKyXuLyItem = new NhatKyXuLyItem()
                        {
                            DoiTuongTitle = "Báo cáo Kỳ",
                            LogDoiTuong = "KyBaoCao",
                            LogIDDoiTuong = oGridRequest.ItemID,
                            LogThaoTac = "CapNhatKetQuaBC",
                            TrangThaiParent = oKyBaoCaoItem.TrangThaiBaoCao + "",
                            CreatedUser = new SPFieldLookupValue(CurentUser.ID, CurentUser.Title),
                            LogNoiDung = context.Request["LogNoiDung"]
                        };
                        #endregion
                        oKyBaoCaoItem.UpdateObject(context.Request);
                        //if (!string.IsNullOrEmpty(context.Request["base64ChiTiet"]))
                        //{
                        //    string base64ChiTiet = context.Request["base64ChiTiet"];
                        //    byte[] Database64ChiTiet = Convert.FromBase64String(base64ChiTiet);
                        //    oKyBaoCaoItem.ListFileRemove = oKyBaoCaoItem.ListFileAttach.Select(x => x.Name).ToList();
                        //    oKyBaoCaoItem.ListFileAttachAdd.Add(new FileAttach()
                        //    {
                        //        Name = "KetQuaBaoCao_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx",
                        //        DataFile = Database64ChiTiet
                        //    });
                        //}
                        oKyBaoCaoDA.UpdateObject<KyBaoCaoItem>(oKyBaoCaoItem);
                        nhatKyXuLyItem.LogTrangThai = oKyBaoCaoItem.TrangThaiBaoCao;
                        NhatKyXuLyDA nhatKyXuLyDA = new NhatKyXuLyDA();
                        nhatKyXuLyDA.UpdateObject<NhatKyXuLyItem>(nhatKyXuLyItem);
                        AddLog("UPDATE", "KyBaoCao", "UPDATE", oGridRequest.ItemID, $"Cập nhật kết quả kỳ báo cáo {oKyBaoCaoItem.Title}");
                        //tạo nhật ký.

                        //nhatKyXuLyItem.UpdateObject(context.Request);
                    }
                    oResult.Message = "Lưu thành công";
                    break;
                case "DELETE":
                    var outb = oKyBaoCaoDA.DeleteObjectV2(oGridRequest.ItemID);
                    AddLog("DELETE", "KyBaoCao", "DELETE", oGridRequest.ItemID, $"Xóa kỳ báo cáo{outb.Message}");
                    if (outb.State == ActionState.Succeed)
                        oResult.Message = "Xóa thành công";
                    break;
                case "DELETE-MULTI":
                    AddLog("DELETE-MULTI", "KyBaoCao", "DELETE-MULTI", oGridRequest.ItemID, $"Xóa nhiều kỳ báo cáo");
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oKyBaoCaoDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    AddLog("APPROVED", "KyBaoCao", "APPROVED", oGridRequest.ItemID, $"Duyệt kỳ báo cáo{oKyBaoCaoDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                    oKyBaoCaoDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    AddLog("PENDDING", "KyBaoCao", "PENDDING", oGridRequest.ItemID, $"Hủy duyệt kỳ báo cáo{oKyBaoCaoDA.GetTitleByID(oGridRequest.ItemID)}");
                    oResult.Message = "Hủy duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oKyBaoCaoItem = oKyBaoCaoDA.GetByIdToObjectSelectFields<KyBaoCaoItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oKyBaoCaoItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
                case "SENDBC":
                    if (oGridRequest.ItemID > 0)
                    {
                        oKyBaoCaoItem = oKyBaoCaoDA.GetByIdToObject<KyBaoCaoItem>(oGridRequest.ItemID);
                        oKyBaoCaoItem.UpdateObject(context.Request);
                        if (oKyBaoCaoItem.UserThamGia.Count > 0)
                        {
                            LUserDA lUserDA = new LUserDA();
                            List<LUserJson> lUserJsons = lUserDA.GetListJson(new LUserQuery() { isGetBylistID = true, lstIDget = oKyBaoCaoItem.UserThamGia.Select(x => x.LookupId).ToList() });
                            oKyBaoCaoItem.UserThamGia = new Microsoft.SharePoint.SPFieldLookupValueCollection(string.Join(";#", lUserJsons.Select(x => $"{x.ID};#{x.Title}")));

                        }
                        //tính thời gian báo cáo gần nhất.
                        #region Tính toán phần thời gian này cho chuẩn.
                        if (oKyBaoCaoItem.BCLoai == 4)
                        {
                            List<int> lstThu = clsFucUtils.GetDanhSachIDsQuaFormPost(oKyBaoCaoItem.NgayThu);
                            int NgayTiepTheo = lstThu.Where(x => x > (int)DateTime.Now.DayOfWeek).FirstOrDefault();
                            if (NgayTiepTheo == 0) NgayTiepTheo = lstThu.FirstOrDefault();
                            oKyBaoCaoItem.BCDenNgayTemp = clsFucUtils.GetNextWeekday(DateTime.Now.Date, (DayOfWeek)(NgayTiepTheo - 1)); //chỗ này phải tính toán thêm.
                        }
                        else if (oKyBaoCaoItem.BCLoai == 5) //báo cáo tháng quý
                        {
                            if (string.IsNullOrEmpty(oKyBaoCaoItem.TuanThu))
                            {
                                //ngày thứ bao nhiêu đó của tháng.
                                List<int> lstThu = clsFucUtils.GetDanhSachIDsQuaFormPost(oKyBaoCaoItem.NgayThu);
                                List<int> lstThang = clsFucUtils.GetDanhSachIDsQuaFormPost(oKyBaoCaoItem.ThangBaoBao);
                                int CurentData = DateTime.Now.Day;
                                //check xem tháng hiện tại có nằm trong báo cáo hay ko.
                                //
                                if (lstThang.Contains(DateTime.Now.Month) && CurentData < lstThu.Min())
                                {
                                    //tháng nay phải có báo cáo.
                                    //thì lần báo cáo sẽ tính là ngày gần nhất.
                                    oKyBaoCaoItem.BCDenNgayTemp = new DateTime(DateTime.Now.Year, DateTime.Now.Month, lstThu.Min());
                                }
                                else
                                {
                                    //ko thì sẽ là tháng tiếp theo báo cáo + ngày bé nhất.
                                    int ThangBaoCao = lstThang.Where(x => x > DateTime.Now.Month).FirstOrDefault();
                                    int NamBaoCao = DateTime.Now.Year;
                                    if (ThangBaoCao == 0)
                                    {
                                        ThangBaoCao = lstThang.Min();
                                        NamBaoCao = DateTime.Now.Year + 1;
                                    }
                                    oKyBaoCaoItem.BCDenNgayTemp = new DateTime(NamBaoCao, ThangBaoCao, lstThu.Min());
                                    //tiếp ngày min và năm .

                                }
                            }
                            else
                            {
                                //nếu luồng để theo tuần và thứ trong tuần thì sẽ phải tính.

                            }
                        }
                        else
                            oKyBaoCaoItem.BCDenNgayTemp = DateTime.Now.AddDays(7); //chỗ này phải tính toán thêm. 
                        #endregion
                        oKyBaoCaoDA.UpdateOneOrMoreField(oGridRequest.ItemID, new Dictionary<string, object>() {
                            { "UserThamGia", oKyBaoCaoItem.UserThamGia },
                            { "DonViThamGiaBC", oKyBaoCaoItem.DonViThamGiaBC },
                             { "DoiTuongThucHien", oKyBaoCaoItem.DoiTuongThucHien },
                             { "BCDenNgayTemp", oKyBaoCaoItem.BCDenNgayTemp }
                        }, true);
                        oKyBaoCaoItem.ID = 0;
                        //tạo kỳ báo cáo cho đơn vị giao .
                        oKyBaoCaoItem.idRoot = oGridRequest.ItemID;
                        oKyBaoCaoItem.TrangThaiBaoCao = 2;
                        oKyBaoCaoItem.BCTuNgayTemp = DateTime.Now;
                        oKyBaoCaoDA.UpdateObject<KyBaoCaoItem>(oKyBaoCaoItem);
                        oKyBaoCaoDA.UpdateSPModerationStatus(oKyBaoCaoItem.ID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                        int idRootKyDanhGia = oKyBaoCaoItem.ID;
                        NhatKyXuLyDA nhatKyXuLyDA = new NhatKyXuLyDA();
                        //giao đơn vị thì thực hiện.
                        if (oKyBaoCaoItem.DoiTuongThucHien == 1)
                        {


                            foreach (var item in oKyBaoCaoItem.DonViThamGiaBC)
                            {
                                oKyBaoCaoItem.ID = 0;
                                oKyBaoCaoItem.fldGroup = item;
                                oKyBaoCaoItem.idRootKyDanhGia = idRootKyDanhGia;
                                oKyBaoCaoItem.TrangThaiBaoCao = 2;//chờ tiếp nhận
                                oKyBaoCaoDA.UpdateObject<KyBaoCaoItem>(oKyBaoCaoItem);
                                oKyBaoCaoDA.UpdateSPModerationStatus(oKyBaoCaoItem.ID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                                NhatKyXuLyItem nhatKyXuLyItem = new NhatKyXuLyItem()
                                {
                                    CreatedUser = new SPFieldLookupValue(CurentUser.ID, CurentUser.Title),
                                    CreatedDate = DateTime.Now,
                                    LogNoiDung = context.Request["LogNoiDung"],
                                    LogDoiTuong = "KyBaoCao",
                                    LogIDDoiTuong = oKyBaoCaoItem.ID,
                                    LogThaoTac = "GiaoBaoCao",
                                    LogTrangThai = 2,
                                    TrangThaiParent = "0"
                                };
                                nhatKyXuLyDA.UpdateObject<NhatKyXuLyItem>(nhatKyXuLyItem);

                                //Tạo nhật ký giao cho các nội dung này.

                            }
                        }
                        else
                        {
                            foreach (var item in oKyBaoCaoItem.UserThamGia)
                            {
                                oKyBaoCaoItem.ID = 0;
                                oKyBaoCaoItem.fldGroup = new SPFieldLookupValue();
                                oKyBaoCaoItem.fldUser = string.Format("{0};#{1}", item.LookupId, item.LookupValue);
                                oKyBaoCaoItem.idRootKyDanhGia = idRootKyDanhGia;
                                oKyBaoCaoItem.TrangThaiBaoCao = 2;//chờ tiếp nhận
                                oKyBaoCaoDA.UpdateObject<KyBaoCaoItem>(oKyBaoCaoItem);
                                oKyBaoCaoDA.UpdateSPModerationStatus(oKyBaoCaoItem.ID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                                NhatKyXuLyItem nhatKyXuLyItem = new NhatKyXuLyItem()
                                {
                                    CreatedUser = new SPFieldLookupValue(CurentUser.ID, CurentUser.Title),
                                    CreatedDate = DateTime.Now,
                                    LogNoiDung = context.Request["LogNoiDung"],
                                    LogDoiTuong = "KyBaoCao",
                                    LogIDDoiTuong = oKyBaoCaoItem.ID,
                                    LogThaoTac = "GiaoBaoCao",
                                    LogTrangThai = 2,
                                    TrangThaiParent = "0"
                                };
                                nhatKyXuLyDA.UpdateObject<NhatKyXuLyItem>(nhatKyXuLyItem);

                                //Tạo nhật ký giao cho các nội dung này.

                            }
                        }
                        Dictionary<string, object> lstUpdate = new Dictionary<string, object>();
                        lstUpdate.Add("TrangThaiBaoCao", 1); //đã giao báo cáo.
                        lstUpdate.Add("DonViChuaXuLy", oKyBaoCaoItem.DonViThamGiaBC); //đã giao báo cáo.
                        oKyBaoCaoDA.UpdateOneOrMoreField(oGridRequest.ItemID, lstUpdate, true);
                        oResult.Message = "Phân công thành công";
                    }
                    break;
                case "SENDBCDONVI":
                    if (oGridRequest.ItemID > 0)
                    {
                        oKyBaoCaoItem = oKyBaoCaoDA.GetByIdToObject<KyBaoCaoItem>(oGridRequest.ItemID);
                        oKyBaoCaoItem.UpdateObject(context.Request);
                        //oKyBaoCaoItem._ModerationStatus = Microsoft.SharePoint.SPModerationStatusType.Approved;
                        oKyBaoCaoDA.UpdateObject<KyBaoCaoItem>(oKyBaoCaoItem);
                        oKyBaoCaoDA.UpdateSPModerationStatus(oKyBaoCaoItem.ID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                        oResult.Message = "Chuyển thành công";
                    }
                    break;
                case "CHUYENBC":
                    if (oGridRequest.ItemID > 0)
                    {
                        oKyBaoCaoItem = oKyBaoCaoDA.GetByIdToObject<KyBaoCaoItem>(oGridRequest.ItemID);
                        NhatKyXuLyItem nhatKyXuLyItem = new NhatKyXuLyItem();
                        nhatKyXuLyItem.UpdateObject(context.Request);
                        if (!string.IsNullOrEmpty(context.Request["TrangThaiBaoCao"]))
                        {
                            //chờ phân công
                            if (context.Request["TrangThaiBaoCao"] == "5")
                            {
                                nhatKyXuLyItem.LogThaoTac = "GiaoThucHien";
                            }
                            if (context.Request["TrangThaiBaoCao"] == "7")
                            {
                                nhatKyXuLyItem.LogThaoTac = "TrinhDuyet";
                            }
                            if (context.Request["TrangThaiBaoCao"] == "9")
                            {
                                nhatKyXuLyItem.LogThaoTac = "Duyet_TraVe";
                            }
                        }
                        LUserItem lUserItem = new LUserItem();
                        if (!string.IsNullOrEmpty(context.Request["UserPhuTrach"]))
                        {
                            LUserDA lUserDA = new LUserDA();
                            lUserItem = lUserDA.GetByIdToObject<LUserItem>(Convert.ToInt32(context.Request["UserPhuTrach"]));
                            nhatKyXuLyItem.UserNhanXuLyText = $"{lUserItem.ID};#{lUserItem.Title}";
                            oKyBaoCaoItem.UserThamGia.Add(new Microsoft.SharePoint.SPFieldLookupValue(lUserItem.ID, lUserItem.Title));
                        }
                        if (oKyBaoCaoItem.LogText == null)
                            oKyBaoCaoItem.LogText = "";
                        oKyBaoCaoItem.LogText += $"|{CurentUser.ID}_{context.Request["TrangThaiBaoCao"]}_{lUserItem.ID}|";
                        Dictionary<string, object> lstUpdate = new Dictionary<string, object>()
                        {
                            {"TrangThaiBaoCao", context.Request["TrangThaiBaoCao"]},
                            {"UserThamGia", oKyBaoCaoItem.UserThamGia },
                            {"CurrentUserPT", $",{lUserItem.ID}," },
                             {"LogText", oKyBaoCaoItem.LogText }
                        };

                        //chang trạng thái của kỳ báo cáo sagn thông tin khác.
                        oKyBaoCaoDA.UpdateOneOrMoreField(oGridRequest.ItemID, lstUpdate, true);
                        NhatKyXuLyDA nhatKyXuLyDA = new NhatKyXuLyDA();
                        nhatKyXuLyItem.LogTrangThai = Convert.ToInt32(context.Request["TrangThaiBaoCao"]);
                        nhatKyXuLyItem.LogTrangThaiTiepTheo = context.Request["TrangThaiBaoCao"];
                        nhatKyXuLyDA.UpdateObject<NhatKyXuLyItem>(nhatKyXuLyItem);
                        oResult.Message = "Chuyển thành công";
                    }
                    break;
                case "TIEPNHAN":
                    if (oGridRequest.ItemID > 0)
                    {
                        NhatKyXuLyItem nhatKyXuLyItem = new NhatKyXuLyItem();
                        nhatKyXuLyItem.UpdateObject(context.Request);
                        if (nhatKyXuLyItem.LogTrangThai == 3)
                        {
                            //chang trạng thái của kỳ báo cáo sagn thông tin khác.
                            //add thêm vào đây thông tin thì mới được.
                            Dictionary<string, object> lstUpdate = new Dictionary<string, object>()
                            {
                                {"TrangThaiBaoCao", 3},
                                {"UserThamGia",  new SPFieldLookupValueCollection($"{CurentUser.ID};#{CurentUser.Title}")  },
                                {"CurrentUserPT", $",{CurentUser.ID}," }
                            };
                            oResult.Message = oKyBaoCaoDA.UpdateOneOrMoreField(oGridRequest.ItemID, lstUpdate, true);
                            if (string.IsNullOrEmpty(oResult.Message))
                            {
                                NhatKyXuLyDA nhatKyXuLyDA = new NhatKyXuLyDA();
                                nhatKyXuLyDA.UpdateObject<NhatKyXuLyItem>(nhatKyXuLyItem);
                                oResult.Message = "Tiếp nhận thành công";
                            }
                            else
                            {
                                oResult = new ResultAction()
                                {
                                    State = ActionState.Error,
                                    Message = oResult.Message
                                };
                            }
                        }
                        else if (nhatKyXuLyItem.LogTrangThai == 10) //xác nhận hoàn thành.
                        {
                            //chang trạng thái của kỳ báo cáo sagn thông tin khác.
                            //add thêm vào đây thông tin thì mới được.
                            Dictionary<string, object> lstUpdate = new Dictionary<string, object>()
                            {
                                {"TrangThaiBaoCao", nhatKyXuLyItem.LogTrangThai},
                                {"UserThamGia",  new SPFieldLookupValueCollection($"{CurentUser.ID};#{CurentUser.Title}")  },
                                {"CurrentUserPT", $",{CurentUser.ID}," },
                                {"NgayHoanThanhDonVi", DateTime.Now }
                            };
                            //
                            oKyBaoCaoDA.UpdateOneOrMoreField(oGridRequest.ItemID, lstUpdate, true);

                            oKyBaoCaoItem = oKyBaoCaoDA.GetByIdToObject<KyBaoCaoItem>(oGridRequest.ItemID);
                            if (oKyBaoCaoItem.idRootKyDanhGia > 0)
                            {
                                KyBaoCaoItem kyBaoCaoItemRoot = oKyBaoCaoDA.GetByIdToObject<KyBaoCaoItem>(oKyBaoCaoItem.idRootKyDanhGia);
                                kyBaoCaoItemRoot.LogText = "|0_99_0|" + kyBaoCaoItemRoot.LogText;
                                oKyBaoCaoDA.UpdateOneField(oKyBaoCaoItem.idRootKyDanhGia, "LogText", kyBaoCaoItemRoot.LogText);

                                DMBaoCaoDA dMBaoCaoDA = new DMBaoCaoDA();
                                DMBaoCaoItem oDMBaoCaoItem = dMBaoCaoDA.GetByIdToObject<DMBaoCaoItem>(kyBaoCaoItemRoot.LoaiBaoCao.LookupId);
                                if (oDMBaoCaoItem.LoaiBaoCao == 2)
                                {
                                    //bảo cáo dạng số liệu.
                                    //lấy shet của file đính kèm + và sheet tổng hợp.

                                    #region MyRegion
                                    //đơn vị đầu tiên báo cáo. 
                                    Stream DataFileTH = dMBaoCaoDA.SpListProcess.ParentWeb.GetFile(oDMBaoCaoItem.UrlBaoCaoTongHop).OpenBinaryStream();
                                    XSSFWorkbook hssfwbTongHop = new XSSFWorkbook(DataFileTH);
                                    ISheet sheet = hssfwbTongHop.GetSheetAt(0);


                                    //đơn vị đầu tiên báo cáo. 

                                    Stream DataFileMauTH = dMBaoCaoDA.SpListProcess.ParentWeb.GetFile(oDMBaoCaoItem.UrlBaoCaoTongHop).OpenBinaryStream();
                                    XSSFWorkbook hssfwbMauTongHop = new XSSFWorkbook(DataFileMauTH);
                                    ISheet sheetMauTH = hssfwbMauTongHop.GetSheetAt(0);

                                    if (kyBaoCaoItemRoot.ListFileAttach.Count > 0)
                                    {
                                        DataFileTH = oKyBaoCaoDA.SpListProcess.ParentWeb.GetFile(kyBaoCaoItemRoot.ListFileAttach[0].Url).OpenBinaryStream();
                                        hssfwbTongHop = new XSSFWorkbook(DataFileTH);
                                        sheet = hssfwbTongHop.GetSheetAt(0);
                                    }

                                    //oKyBaoCaoItem 
                                    Stream DataFileCT = oKyBaoCaoDA.SpListProcess.ParentWeb.GetFile(oKyBaoCaoItem.ListFileAttach.Where(x => x.Name.EndsWith(".xlsx")).FirstOrDefault().Url).OpenBinaryStream();
                                    XSSFWorkbook hssfwbCT = new XSSFWorkbook(DataFileCT);
                                    ISheet sheetCT = hssfwbCT.GetSheetAt(0);




                                    sheetCT.CopyTo(hssfwbTongHop, CurentUser.Groups.Title, true, true);
                                    #endregion

                                    int StartRowInsert = 0;
                                    if (oDMBaoCaoItem.PhanLoaiHinhThucTongHop == 1 && !string.IsNullOrEmpty(oDMBaoCaoItem.JsonConfig)) //loại insert dòng sang bên kia.
                                    {
                                        #region MyRegion
                                        ConfigExcel configExcel = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigExcel>(oDMBaoCaoItem.JsonConfig);
                                        //lấy row bắt đầu inser của bảng tổng hợp đang có.
                                        for (int i = configExcel.Type1_DongBatDau; i < sheet.LastRowNum; i++)
                                        {
                                            StartRowInsert = i;
                                            IRow temp = sheet.GetRow(i);
                                            if (temp == null)
                                                break;
                                            else
                                            {
                                                ICell tempcell = temp.GetCell(0);
                                                if (tempcell == null)
                                                    break;
                                                else
                                                {

                                                    if (tempcell.CellType == CellType.String && string.IsNullOrEmpty(tempcell.StringCellValue))
                                                    {
                                                        break;
                                                    }
                                                    if (tempcell.CellType == CellType.Blank)
                                                    {
                                                        break;
                                                    }

                                                }
                                            }
                                        }
                                        #endregion
                                        int RowChiTiet = 0;
                                        //insert row từ sheet của đơn vị vào sheet TH
                                        #region Lấy số row cần copy
                                        for (int i = configExcel.Type1_DongBatDau; i < sheetCT.LastRowNum; i++)
                                        {
                                            //bắt đầu tính từ row start, cho đến khi gặp row mà giá trị đầu tiên bị null thì bỏ
                                            IRow temp = sheetCT.GetRow(i);
                                            if (temp == null)
                                                break;
                                            else
                                            {
                                                ICell tempcell = temp.GetCell(0);
                                                if (tempcell == null)
                                                    break;
                                                else
                                                {
                                                    if (clsFucUtils.GetCellValue(tempcell) == null)
                                                    {
                                                        break;
                                                    }
                                                }
                                            }
                                            RowChiTiet++;
                                        }
                                        #endregion
                                        //số lượng dòng phải chèn vào là bao nhiêu.
                                        clsFucUtils.NPOIInsertRows(ref sheet, StartRowInsert + 1, RowChiTiet);
                                        //WriteToFile(hssfwbTongHop, "tonghop1.xlsx");
                                        //
                                        for (int i = 0; i < RowChiTiet; i++)
                                        {
                                            int j = i + configExcel.Type1_DongBatDau;
                                            //nếu đủ đk thì chạy tiếp vào hàm insert
                                            clsFucUtils.copyFromSourceToDestinationRow(hssfwbTongHop, sheetCT, j, sheet, StartRowInsert, configExcel);
                                            StartRowInsert++;
                                        }
                                        //WriteToFile(hssfwbTongHop, "tonghop.xlsx");
                                    }
                                    else if (oDMBaoCaoItem.PhanLoaiHinhThucTongHop == 0)
                                    {
                                        for (int i = 0; i < sheetMauTH.LastRowNum; i++)
                                        {
                                            IRow cellall = sheetMauTH.GetRow(i);
                                            for (int j = 0; j < cellall.LastCellNum; j++)
                                            {
                                                ICell cell = cellall.GetCell(j);
                                                object CellValue = clsFucUtils.GetCellValue(cell);
                                                if (CellValue != null)
                                                {
                                                    string Function = CellValue.ToString();
                                                    switch (Function)
                                                    {
                                                        case "SUMALLCELL":
                                                        case "'SUMALLCELL":
                                                            #region MyRegion
                                                            try
                                                            {
                                                                double SUMALLCELL = 0;
                                                                ICell CellTongHop = sheet.GetRow(i).GetCell(j);
                                                                //check xem CellTongHop đã có giá trị hay chưa.
                                                                if (CellTongHop.CellType == CellType.Numeric)
                                                                {
                                                                    SUMALLCELL = CellTongHop.NumericCellValue;
                                                                }

                                                                ICell cellTemp = sheetCT.GetRow(i).GetCell(j);
                                                                CellTongHop.SetCellValue(SUMALLCELL + cellTemp.NumericCellValue);
                                                            }
                                                            catch (Exception ex)
                                                            {

                                                            }
                                                            break;
                                                        #endregion
                                                        case "AVGCALLCELL":
                                                        case "'AVGCALLCELL":
                                                            #region MyRegion
                                                            try
                                                            {
                                                                double SUMALLCELL = 0;
                                                                ICell CellTongHop = sheet.GetRow(i).GetCell(j);

                                                                //check xem CellTongHop đã có giá trị hay chưa.
                                                                if (CellTongHop.CellType == CellType.Numeric)
                                                                {
                                                                    SUMALLCELL = CellTongHop.NumericCellValue;
                                                                    SUMALLCELL = (SUMALLCELL * (hssfwbTongHop.NumberOfSheets - 2));
                                                                }

                                                                ICell cellTemp = sheetCT.GetRow(i).GetCell(j);
                                                                CellTongHop.SetCellValue((SUMALLCELL + cellTemp.NumericCellValue) / (hssfwbTongHop.NumberOfSheets - 1));
                                                            }
                                                            catch (Exception ex)
                                                            {

                                                            }
                                                            break;
                                                        #endregion
                                                        case "MAXALLCELL":
                                                        case "'MAXALLCELL":
                                                            #region MyRegion
                                                            try
                                                            {
                                                                double MAXALLCELL = 0;
                                                                ICell CellTongHop = sheet.GetRow(i).GetCell(j);
                                                                //check xem CellTongHop đã có giá trị hay chưa.
                                                                if (CellTongHop.CellType == CellType.Numeric)
                                                                {
                                                                    MAXALLCELL = CellTongHop.NumericCellValue;
                                                                }
                                                                ICell cellTemp = sheetCT.GetRow(i).GetCell(j);
                                                                object ValueCell = clsFucUtils.GetCellValue(cellTemp);
                                                                if (ValueCell != null)
                                                                {
                                                                    if ((double)ValueCell > MAXALLCELL)
                                                                    {
                                                                        //set giá trị cho bằng MaxValueDonVi
                                                                        CellTongHop.SetCellValue((double)ValueCell);
                                                                    }
                                                                }
                                                            }
                                                            catch (Exception ex)
                                                            {

                                                            }
                                                            break;
                                                        #endregion
                                                        case "MINALLCELL":
                                                        case "'MINALLCELL":
                                                            #region MyRegion
                                                            try
                                                            {
                                                                double MINALLCELL = 999999999;
                                                                ICell CellTongHop = sheet.GetRow(i).GetCell(j);
                                                                //check xem CellTongHop đã có giá trị hay chưa.
                                                                if (CellTongHop.CellType == CellType.Numeric)
                                                                {
                                                                    MINALLCELL = CellTongHop.NumericCellValue;
                                                                }
                                                                ICell cellTemp = sheetCT.GetRow(i).GetCell(j);
                                                                object ValueCell = clsFucUtils.GetCellValue(cellTemp);
                                                                if (ValueCell != null)
                                                                {
                                                                    if ((double)ValueCell < MINALLCELL)
                                                                    {
                                                                        //set giá trị cho bằng MaxValueDonVi
                                                                        CellTongHop.SetCellValue((double)ValueCell);
                                                                    }
                                                                }
                                                            }
                                                            catch (Exception ex)
                                                            {

                                                            }
                                                            break;
                                                            #endregion
                                                    }
                                                }

                                            }

                                        }
                                        //WriteToFile(hssfwbTongHop, "tonghop.xlsx");
                                    }
                                    // code to create workbook 
                                    #region Lưu data vào list
                                    using (var exportData = new MemoryStream())
                                    {
                                        hssfwbTongHop.Write(exportData);
                                        //string saveAsFileName = string.Format("MembershipExport-{0:d}.xls", DateTime.Now).Replace("/", "-");

                                        byte[] bytes = exportData.ToArray();

                                        kyBaoCaoItemRoot.ListFileAttachAdd.Add(new FileAttach() { Name = "BaoCaoTongHop.xlsx", DataFile = bytes });
                                        SPListItem itembc = oKyBaoCaoDA.SpListProcess.GetItemById(kyBaoCaoItemRoot.ID);
                                        try
                                        {
                                            itembc.Attachments.RecycleNow("BaoCaoTongHop.xlsx");
                                        }
                                        catch (Exception ex)
                                        {

                                        }
                                        itembc.Attachments.AddNow("BaoCaoTongHop.xlsx", bytes);
                                    }
                                    #endregion
                                }

                                ////nếu là báo cáo dạng file thì phải insert vào file này vào.
                                ////oKyBaoCaoItem
                                //if (!string.IsNullOrEmpty(oKyBaoCaoItem.jsonBCCT))
                                //{
                                //    DMBaoCaoItem oDMBaoCaoItem = new DMBaoCaoItem();
                                //    DMBaoCaoDA oDMBaoCaoDA = new DMBaoCaoDA();
                                //    oDMBaoCaoItem = oDMBaoCaoDA.GetByIdToObject<DMBaoCaoItem>(oKyBaoCaoItem.LoaiBaoCao.LookupId);

                                //}

                            }


                            NhatKyXuLyDA nhatKyXuLyDA = new NhatKyXuLyDA();
                            nhatKyXuLyItem.LogThaoTac = "XacNhanHoanThanh";
                            nhatKyXuLyDA.UpdateObject<NhatKyXuLyItem>(nhatKyXuLyItem);
                            oResult.Message = "Xác nhận hoàn thành thành công";
                        }
                        else if (nhatKyXuLyItem.LogTrangThai == 99) //xác nhận hoàn thành.
                        {
                            //chang trạng thái của kỳ báo cáo sagn thông tin khác.
                            //add thêm vào đây thông tin thì mới được.
                            Dictionary<string, object> lstUpdate = new Dictionary<string, object>()
                            {
                                {"TrangThaiBaoCao", nhatKyXuLyItem.LogTrangThai},
                                {"NgayHoanThanhDonVi", DateTime.Now } //xác định ngày hoàn thành.
                            };
                            oKyBaoCaoDA.UpdateOneOrMoreField(oGridRequest.ItemID, lstUpdate, true);
                            NhatKyXuLyDA nhatKyXuLyDA = new NhatKyXuLyDA();
                            nhatKyXuLyItem.LogThaoTac = "XacNhanHoanThanhGiao";
                            nhatKyXuLyDA.UpdateObject<NhatKyXuLyItem>(nhatKyXuLyItem);
                            oResult.Message = "Xác nhận hoàn thành thành công";

                            //tính đúng chậm hạn của thông tin.
                            List<KyBaoCaoJson> lstKyBaoCao = oKyBaoCaoDA.GetListJson(new KyBaoCaoQuery() { idRootKyDanhGia = oGridRequest.ItemID });
                            foreach (KyBaoCaoJson item in lstKyBaoCao)
                            {
                                nhatKyXuLyItem.ID = 0;
                                nhatKyXuLyItem.LogIDDoiTuong = item.ID;
                                nhatKyXuLyDA.UpdateObject<NhatKyXuLyItem>(nhatKyXuLyItem);

                                lstUpdate = new Dictionary<string, object>()
                                {
                                    {"TrangThaiBaoCao", nhatKyXuLyItem.LogTrangThai}
                                };
                                if (item.NgayHoanThanhDonVi.HasValue)
                                {
                                    int resultDate = DateTime.Compare(item.NgayHoanThanhDonVi.Value, item.BCDenNgayTemp.Value);
                                    //string relationship;

                                    if (resultDate <= 0)
                                    {
                                        lstUpdate.Add("TrangThaiHoanThanh", 1);
                                    }
                                    else
                                    {
                                        lstUpdate.Add("TrangThaiHoanThanh", 2);
                                    }
                                }
                                else
                                {
                                    lstUpdate.Add("TrangThaiHoanThanh", 3);
                                }
                                oKyBaoCaoDA.UpdateOneOrMoreField(item.ID, lstUpdate, true);
                            }
                        }
                    }
                    break;
            }
            oResult.ResponseData();

        }
    }
}