<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewKyBaoCaoGiao.aspx.cs" Inherits="VIPortalAPP.pViewKyBaoCaoGiao" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
    <style type="text/css">
        #frmViewBaoCao fieldset.scheduler-border {
            border: solid 1px #DDD !important;
            padding: 0 10px 10px 10px;
            border-bottom: none;
            margin-bottom: 15px;
        }

        #frmViewBaoCao legend.scheduler-border {
            width: auto !important;
            border: none;
            font-size: 18px;
            padding: 0px 10px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            //tblKyBaoCaoThucHien
            $("#tblKyBaoCaoThucHien").viDataTable(
                {
                    "url": "/UserControls/BaoCao/BCTongHop/KyBaoCao/pAction.asp",
                    "aoColumns":
                        [
                            {
                                "name": "Created", "sTitle": "Ngày giao",
                                "mData": function (o) {
                                    return formatDate(o.Created);
                                },
                                "sWidth": "130px",
                            },{
                                "name": "BCDenNgayTemp", "sTitle": "Ngày Hoàn thành dự kiến",
                                "mData": function (o) {
                                    return formatDate(o.BCDenNgayTemp);
                                },
                                "sWidth": "130px",
                            }, {
                                "data": "TrangThaiBaoCaoTitle",
                                "name": "TrangThaiBaoCaoTitle", "sTitle": "Trạng thái"
                            }
                        ],
                    "aaSorting": [0, 'asc']
                });
            $("#tblKyBaoCaoRoot").viDataTable(
                {
                    "url": "/UserControls/BaoCao/BCTongHop/KyBaoCao/pAction.asp",
                    "aoColumns":
                        [
                            {
                                "name": "NgayHoanThanhDonVi", "sTitle": "Ngày Hoàn thành",
                                "mData": function (o) {
                                    return formatDate(o.NgayHoanThanhDonVi);
                                },
                                "sWidth": "100px",
                            }, {
                                //"data": "fldGroup",
                                "name": "fldGroup.Title", "sTitle": "Cán bộ/Đơn vị",
                                "mData": function (o) {
                                    if (o.fldGroup.ID != 0)
                                        return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.fldGroup.Title + '</a>';
                                    else return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.fldUser.Title + '</a>';
                                },
                            }, {
                                "name": "TitleKetQua", "sTitle": "Kết quả",
                                "mData": function (o) {
                                    var html = '';
                                    if (o.TitleKetQua != '' && o.TitleKetQua != null) {
                                        html += `${o.TitleKetQua}</br>`;
                                    }
                                    $.each(o.ListFileAttach, function (key, value) {
                                        html += `<a class="clslink" target="_blank" href="${value.Url}">${value.Name}</a><br\>`;
                                    });
                                    return html;
                                },
                            }, {
                                "data": "TrangThaiBaoCaoTitle",
                                "name": "TrangThaiBaoCaoTitle", "sTitle": "Trạng thái",
                                "sWidth": "150px",
                            }
                        ],
                    "aaSorting": [0, 'asc']
                });
        });
    </script>
</head>
<body>
    <form class="form-horizontal form-viewdetail" id="frmViewBaoCao">
        <div class="form-group row">
            <div class="col-sm-12 text-right" data-TrangThai="<%=oKyBaoCao.TrangThaiBaoCao %>">
                <%if(oKyBaoCao.TrangThaiBaoCao == 1 && oKyBaoCao.idRoot != 0){  %>
                <button type="button" title="Kết thúc/Xác nhận hoàn thành" url="/UserControls/BaoCao/BCTongHop/KyBaoCao/pFormTiepNhan.aspx?ItemID=<%=oKyBaoCao.ID %>" size="800" class="btn btn-primary mb-2 btnfrm">Kết thúc/Xác nhận hoàn thành</button>
                <%}else if(oKyBaoCao.TrangThaiBaoCao == 2 && oKyBaoCao.idRoot != 0){  %>
                <button type="button" title="Kết thúc/Xác nhận hoàn thành" url="/UserControls/BaoCao/BCTongHop/KyBaoCao/pFormTiepNhan.aspx?ItemID=<%=oKyBaoCao.ID %>" size="800" class="btn btn-primary mb-2 btnfrm">Kết thúc/Xác nhận hoàn thành</button>
                <%if(isTongHop == 1){ %>
                <button type="button" title="Tổng hợp báo cáo" url="/UserControls/BaoCao/BCTongHop/KyBaoCao/pViewTongHopBaoCao.aspx?ItemID=<%=oKyBaoCao.ID %>" size="1024" class="btn btn-primary mb-2 btnfrm">Tổng hợp</button>
                <%} %>
                <%} else if(oKyBaoCao.TrangThaiBaoCao == 0){  %>
                <button type="button" title="Giao báo cáo Đơn vị/Người dùng" url="/UserControls/BaoCao/BCTongHop/KyBaoCao/pFormGiaoBaoCao.aspx?ItemID=<%=oKyBaoCao.ID %>&do=SENDBC" size="900" class="btn btn-primary mb-2 btnfrm">Giao báo cáo</button>
                <%} %>
            </div>
        </div>
        <fieldset class="scheduler-border">
            <legend class="scheduler-border">Thông tin kỳ báo cáo</legend>
            <div class="form-group row">
                <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
                <div class="col-sm-10">
                    <%=oKyBaoCao.Title%>
                </div>
            </div>
            <div class="form-group row">
                <label for="DMMoTa" class="col-sm-2 control-label">Mô tả</label>
                <div class="col-sm-10">
                    <%=oKyBaoCao.DMMoTa%>
                </div>
            </div>
            <div class="form-group row">
                <label for="LoaiBaoCao" class="col-sm-2 control-label">Loại báo cáo</label>
                <div class="col-sm-4">
                    <%=oKyBaoCao.LoaiBaoCao.LookupValue%>
                </div>
                <label for="DMVietTat" class="col-sm-2 control-label">DM viết tắt</label>
                <div class="col-sm-4">
                    <%:oKyBaoCao.DMVietTat%>
                </div>
            </div>
            <%if(oDMBaoCaoItem.LoaiBaoCao == 2){ %>
            <div class="form-group row">
                <label for="LoaiBaoCao" class="col-sm-2 control-label">Mẫu báo cáo</label>
                <div class="col-sm-4">
                    <a href="<%=oDMBaoCaoItem.UrlFileBCChiTiet %>" title="Tải fiel mẫu báo cáo" target="_blank">Mẫu báo cáo</a>
                </div>
            </div>
            <%} %>
            <div class="form-group row">
                <label for="BCTuNgay" class="col-sm-2 control-label">Từ ngày</label>
                <div class="col-sm-4">
                    <div class="">
                        <%:string.Format("{0:dd-MM-yyyy}", oKyBaoCao.BCTuNgayTemp)%>
                    </div>
                </div>
                <label for="QCNgayVanBan" class="col-sm-2 control-label">Hạn dến ngày</label>
                <div class="col-sm-4">
                    <%:string.Format("{0:dd-MM-yyyy}", oKyBaoCao.BCDenNgayTemp)%>
                </div>
            </div>
            <div class="form-group row">
                <label for="BCTuNgay" class="col-sm-2 control-label">Đơn vị tham gia</label>
                <div class="col-sm-10">
                    <div class="">
                        <%=string.Join(", ", oKyBaoCao.DonViThamGiaBC.Select(x=>x.LookupValue)) %>
                    </div>
                </div>

            </div>
            <div class="form-group row">
                <label class="col-sm-2 control-label">File đính kèm</label>
                <div class="col-sm-10">
                    <%for(int i=0; i< RootKyDanhGi.ListFileAttach.Count;i++){ %>
                    <div><a href="<%=RootKyDanhGi.ListFileAttach[i].Url %>"><%=RootKyDanhGi.ListFileAttach[i].Name%></a></div>
                    <%} %>
                </div>
            </div>
        </fieldset>

        <div class="form-group row" id="clsNhomKy" <%=oKyBaoCao.idRoot == 0 ? "style=\"display:none\"": "" %> >
            <div class="col-sm-12">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Báo cáo đơn vị</a>
                    </li>

                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                        <div role="body-data" data-title="kỳ báo cáo" data-size="1024" class="content_wp" data-action="/UserControls/BaoCao/BCTongHop/KyBaoCao/pAction.asp" data-form="/UserControls/BaoCao/BCTongHop/KyBaoCao/pFormKyBaoCao.aspx" data-view="/UserControls/BaoCao/BCTongHop/KyBaoCao/pViewKyBaoCao.aspx">
                            <div class="card card-default color-palette-box">
                                <div class="card-body">
                                    <div class="clsmanager row">
                                        <div class="col-sm-9">
                                            <div id="KyBaoCaoSearch" class="form-inline zonesearch">
                                                <input type="hidden" name="do" id="do" value="QUERYDATA" />
                                                <input type="hidden" name="idRootKyDanhGia" value="<%=ItemID %>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clsgrid table-responsive">
                                        <table id="tblKyBaoCaoRoot" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>



            </div>
        </div>
        <div class="form-group row"  id="clsNhomKy2" <%=oKyBaoCao.idRoot != 0 ? "style=\"display:none\"": "" %> >
            <div class="col-sm-12">
                <ul class="nav nav-tabs" id="myTab2" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="home-kybc" data-toggle="tab" href="#kybc" role="tab" aria-controls="home" aria-selected="true">Các kỳ báo cáo</a>
                    </li>

                </ul>
                <div class="tab-content" id="myTabContent2">
                    <div class="tab-pane fade show active" id="kybc" role="tabpanel" aria-labelledby="home-kybc">

                        <div role="body-data" data-title="kỳ báo cáo" data-size="1024" class="content_wp" data-action="/UserControls/BaoCao/BCTongHop/KyBaoCao/pAction.asp" data-form="/UserControls/BaoCao/BCTongHop/KyBaoCao/pFormKyBaoCao.aspx" data-view="/UserControls/BaoCao/BCTongHop/KyBaoCao/pViewKyBaoCao.aspx">
                            <div class="card card-default color-palette-box">
                                <div class="card-body">
                                    <div class="clsmanager row">
                                        <div class="col-sm-9">
                                            <div id="KyBaoCaoSearch2" class="form-inline zonesearch">
                                                <input type="hidden" name="do"  value="QUERYDATA" />
                                                <input type="hidden" name="idRootKyDanhGia" value="-1" />
                                                <input type="hidden" name="idRoot" value="<%=ItemID %>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clsgrid table-responsive">
                                        <table id="tblKyBaoCaoThucHien" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>



            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>

</html>
