<%@ control language="C#" autoeventwireup="true" codebehind="UC_KyBaoCao.aspx.cs" inherits="VIPortalAPP.UC_KyBaoCao" %>
<div role="body-data" data-title="kỳ báo cáo" class="content_wp" data-action="/UserControls/BaoCao/BCTongHop/KyBaoCao/pAction.asp" data-form="/UserControls/BaoCao/BCTongHop/KyBaoCao/pFormKyBaoCao.aspx" data-view="/UserControls/BaoCao/BCTongHop/KyBaoCao/pViewKyBaoCao.aspx">
    <div class="card card-default color-palette-box">
        <div class="card-body">
            <div class="clsmanager row">
                <div class="col-sm-9">
                    <div id="KyBaoCaoSearch" class="form-inline zonesearch">
                        <div class="form-group">
                            <input type="hidden" name="do" id="do" value="QUERYDATA" />
                            <input type="hidden" name="LoaiQuery" id="LoaiQuery" value="1" />
                            <label for="Keyword">Từ khóa</label>
                            <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Từ khóa">
                            <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title" />
                        </div>
                        <button type="button" class="btn btn-default act-search">Tìm kiếm</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="text-right">
                        <button class="btn btn-primary act-add" type="button">Thêm mới</button>
                    </p>
                </div>
            </div>

            <div class="clsgrid table-responsive">

                <table id="tblKyBaoCao" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                </table>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        var $tagvalue = $(".content_wp");
        var $tblDanhMucChung = $("#tblKyBaoCao").viDataTable(
            {
                "frmSearch": "KyBaoCaoSearch",
                "url": $tagvalue.data("action"),
                "aoColumns":
                    [
                        {
                            "name": "Created", "sTitle": "Ngày tạo",
                            "mData": function (o) {
                                return formatDate(o.Created);
                            },
                            "sWidth": "80px",
                        }, {
                            "data": "DMVietTat",
                            "name": "DMVietTat", "sTitle": "Viết tắt",
                            "sWidth": "60px",
                        },{
                            "mData": function (o) {
                                return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>' + `</br> Báo cáo: ${o.LoaiBaoCao.Title}`;
                            },
                            "name": "Title", "sTitle": "Tiêu đề"
                        }, {
                            "name": "MoTaTriggers", "sTitle": "Thời gian báo cáo",
                            "mData": function (o) {
                                var htmltemp = o.MoTaTriggers;
                                if (o.BCDenNgayTemp != null) {
                                    htmltemp += `</br><span class="clsGhichu">Thời gian thực hiện báo cáo gần nhất ${formatDate(o.BCDenNgayTemp)}</span>`;
                                }
                                return htmltemp ;
                            }
                        }, {
                            "data": "TrangThaiBaoCaoTitle",
                            "name": "TrangThaiBaoCaoTitle", "sTitle": "Trạng thái",
                            "sWidth": "100px",
                        },
                        {
                            "mData": null,
                            "bSortable": false,
                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) {
                                if (o._ModerationStatus == 0)

                                    return '<a title="Hủy duyệt" data-id="' + o.ID + '" class="btn default btn-xs black act-pendding" href="javascript:;"><i class="fa fa-toggle-on"></i></a>';
                                else
                                    return '<a title="Duyệt" data-id="' + o.ID + '" class="btn default btn-xs blue act-approved" href="javascript:;"><i class="fa fa-toggle-off"></i></a>';
                            }
                        }, {

                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<a url="/UserControls/BaoCao/BCTongHop/KyBaoCao/pFormGiaoBaoCao.aspx" size=800 class="btn default btn-xs purple btnfrm" title="Giao báo cáo" data-do="SENDBC" data-ItemID="' + o.ID + '" href="javascript:;"><i class="far fa-share-square"></i></a>'; }
                        },{

                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" title="Sửa" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
                        }, {
                            "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                            "mData": null,
                            "bSortable": false,

                            "className": "all",
                            "sWidth": "18px",
                            "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" href="javascript:;"><i class="far fa-trash-alt"></i></a>';; }
                        }, {
                            "sTitle": '<input class="checked-all" type="checkbox" />',
                            "mData": null,
                            "bSortable": false,
                            "sWidth": "18px",
                            "className": "all",
                            "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
                        }
                    ]
            });
    });
</script>
