using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData;

namespace VIPortalAPP
{
    public partial class pViewKyBaoCaoDVNhan : pFormBase
    {
        public KyBaoCaoItem oKyBaoCao = new KyBaoCaoItem();
        protected void Page_Load(object sender, EventArgs e)
        {
            oKyBaoCao = new KyBaoCaoItem();
            if (ItemID > 0)
            {
                KyBaoCaoDA oKyBaoCaoDA = new KyBaoCaoDA();
                oKyBaoCao = oKyBaoCaoDA.GetByIdToObject<KyBaoCaoItem>(ItemID);
                
            }
        }
    }
}