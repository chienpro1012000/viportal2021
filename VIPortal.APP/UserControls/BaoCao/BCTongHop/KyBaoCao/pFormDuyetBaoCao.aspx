﻿<%@ page language="C#" autoeventwireup="true" codebehind="pFormDuyetBaoCao.aspx.cs"
    inherits="VIPortal.APP.UserControls.BaoCao.BCTongHop.KyBaoCao.pFormDuyetBaoCao" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-FormGiaoBaoCao" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oKyBaoCao.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" name="DoiTuongTitle" id="DoiTuongTitle" value="Báo cáo Kỳ" />
        <input type="hidden" name="LogDoiTuong" id="LogDoiTuong" value="KyBaoCao" />
        <input type="hidden" name="LogIDDoiTuong" id="LogIDDoiTuong" value="<%=oKyBaoCao.ID %>" />
        <input type="hidden" name="LogThaoTac" id="LogThaoTac" value="PheDuyetBaoCao" />
        <input type="hidden" name="LogTrangThai" id="LogTrangThai" value="3" />
        <input type="hidden" name="TrangThaiParent" id="TrangThaiParent" value="<%=oKyBaoCao.TrangThaiBaoCao %>" />
        <input type="hidden" name="CreatedUser" id="CreatedUser" value="<%=CurentUser.ID %>" />
        <input type="hidden" name="UserPhuTrach" id="UserPhuTrach" value="<%=UserPhuTrach %>" />
         <fieldset class="scheduler-border">
            <legend class="scheduler-border">Kết quả báo cáo</legend>
            <div class="form-group row">
                <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
                <div class="col-sm-10">
                    <%=oKyBaoCao.TitleKetQua%>
                </div>
            </div>
            <div class="form-group row">
                <label for="Title" class="col-sm-2 control-label">Mô tả</label>
                <div class="col-sm-10">
                    <%=oKyBaoCao.MoTaKetQua%>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 control-label">File đính kèm</label>
                <div class="col-sm-10">
                    <%for(int i=0; i< oKyBaoCao.ListFileAttach.Count;i++){ %>
                    <div>
                        <a href="<%=oKyBaoCao.ListFileAttach[i].Url %>">
                            <%=oKyBaoCao.ListFileAttach[i].Name%></a><%if(oKyBaoCao.ListFileAttach[i].Name.ToLower().EndsWith(".docx")){ %><a target="_blank" class="clslinkedit" style="padding-left: 3px;" href="/htbaocao/_layouts/15/WopiFrame.aspx?sourcedoc=<%=oKyBaoCao.ListFileAttach[i].Url %>&file=<%=oKyBaoCao.ListFileAttach[i].Name%>&action=default"><i class="far fa-window-restore"></i></a><%} %><%if(oKyBaoCao.ListFileAttach[i].Name.ToLower().EndsWith(".xlsx") || oKyBaoCao.ListFileAttach[i].Name.ToLower().EndsWith(".pdf")){ %><a target="_blank" class="clslinkedit signxlsx" style="padding-left: 3px;" href="<%=oKyBaoCao.ListFileAttach[i].Url %>"><i class="fas fa-pen-alt"></i></a><%} %>
                    </div>
                    <%} %>
                </div>
            </div>
        </fieldset>
        <div class="form-group row">
            <label for="LogNoiDung" class="col-sm-2 control-label">Ý kiến</label>
            <div class="col-sm-10">
                <textarea name="LogNoiDung" id="LogNoiDung" class="form-control"></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-10">
                <span style="float: left; margin-top: 10px;">
                    <input type="radio" value="8" id="isDuyetVaPhatHanhKyHT" name="TrangThaiBaoCao" checked="checked" />&nbsp;Đồng ý Duyệt 
                    <input type="radio" value="9" id="btnTraVe" name="TrangThaiBaoCao" />&nbsp;Trả về  &nbsp;&nbsp;&nbsp; 
                </span>
            </div>
        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".signxlsx").click(function (event) {
                event.preventDefault();
                var urlfile = $(this).attr('href');
                //convert sang pdf rồi ký.
                $.post("/UserControls/BaoCao/BCTongHop/KyBaoCao/pAction.asp", { do: "SIGNXLSX", urlfile: urlfile, ItemID: "<%=ItemID%>" }, function (result) {
                    $("#frm-FormGiaoBaoCao").closeModal();
                    //open lại form cũ.
                    openDialog("Phê duyệt báo cáo", "/UserControls/BaoCao/BCTongHop/KyBaoCao/pFormDuyetBaoCao.aspx?ItemID=<%=oKyBaoCao.ID %>&do=CHUYENBC", {}, 900);
                });
            });
            $("#FileAttach").regFileUpload({
                files: '[]'
            });

            $("#frm-FormGiaoBaoCao").validate({
                rules: {
                    LogNoiDung: "required",
                    UserPhuTrach: "required",
                    TrangThaiBaoCao: "required",
                },
                messages: {
                    LogNoiDung: "Vui lòng nhập nội dung"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/BaoCao/BCTongHop/KyBaoCao/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-DMLinhVuc").trigger("click");
                            $(form).closeModal();
                            $("#frmViewBaoCao").closeModal();
                            var idtable = $('#contentbc div.tab-pane.active table.dataTable').attr('id');
                            $('#' + idtable).DataTable().ajax.reload();
                            if ($('.modal:visible').length) {
                                $('body').addClass('modal-open');
                            }
                        }
                    }).always(function () { });
                }
            });
            $("#frm-FormGiaoBaoCao").viForm();
        });
    </script>
</body>
</html>
