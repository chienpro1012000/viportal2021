﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_ChiTieuKinhDoanh.ascx.cs" inherits="VIPortal.APP.UserControls.BaoCao.ChiTieuKinhDoanh.UC_ChiTieuKinhDoanh" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<link type="text/css" href="/Content/dist/css/StyleBaoCao.css" rel="stylesheet" />
<script src="/Content/plugins/kendoui/js/kendo.core.min.js"></script>

<style>
    .zone_search {
        overflow-x: hidden;
    }
</style>
<body>
    <div id="BaoCaoSoDangKy" class="smgrid" data-title="Văn bản đến" data-grid="grid_BaoCaoSoDKVanBanDen"
        data-form="/iVBDH/VanBanDen/pFormVanBanDen.aspx" data-formwidth="1200" data-action="/Vbden/api/VanBanDen/VBDGetListJsonGroupSolr" data-view="/Vbden/VanBanDen/pViewVanBanDen.aspx">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal" >
                <div class="title_icon">
                </div>
               
                <div class="form-group row text-search align-items-center">
                    <input type="hidden" name="FieldOption" value="true" />
                    <label for="vbdSoCongVanLookup" class="col-sm-1 control-label">Sổ công văn</label>
                    <div class="col-sm-2">
                        <select data-place="Chọn sổ công văn" data-url="/iVBDH/api/LSoCongVan/QUERYDATASOLR?socvLoaiVanBan=2" name="vbdiSoCongVanLookup" id="vbdiSoCongVanLookup" class="form-control">
                        </select>
                    </div>
                    <label for="vbdiLoaiVanBan" class="col-sm-1 control-label">Loại văn bản</label>
                    <div class="col-sm-2">
                        <select data-place="Chọn loại văn bản" data-url="/iVBDH/api/LPhanLoaiVanBan/GetListJson?phanloaiVanBan=2" name="vbdiLoaiVanBan" id="vbdiLoaiVanBan" class="form-control"></select>
                    </div>
                    <label class="col-sm-1 control-label">Từ ngày:</label>

                    <div class="col-sm-1">
                        <input type="text" name="SearchTuNgay" id="SearchTuNgay" class="form-control input-datetime" placeholder="Từ ngày" value="<%=DateTime.Now.AddMonths(-1).ToString("dd/MM/yyyy") %>" />
                    </div>
                    <label class="col-sm-1 control-label">Đến ngày:</label>
                    <div class="col-sm-1">
                        <input type="text" name="SearchDenNgay" id="SearchDenNgay" class="form-control input-datetime" placeholder="Đến ngày" value="<%=DateTime.Now.ToString("dd/MM/yyyy") %>" />
                    </div>
                    <div class="col-sm-1">  <button type="button" class="btn btn-primary thongke">Thông kê</button></div>
                </div>
            </div>
        </div>
    <hr />
        <div class="mainContentBaoCao">
            <div style="">
                <div style="width: 50%; float: left; text-align: center">
                </div>
                <div style="width: 50%; float: right; text-align: center">
                    <p style="font-size: 12pt; width: 100%"><b>CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</b></p>
                    <p style="font-size: 12pt; width: 100%"><b>Độc lập - Tự do - Hạnh phúc</b></p>
                    <p>____o0o____</p>
                </div>

            </div>
            <div style="width: 100%; float: right; text-align: center">
                <p style="font-size: 12pt; width: 100%" id="TitleBC"><b>TÌNH HÌNH VĂN BẢN ĐI</b></p>
                <p style="font-size: 12pt; width: 100%"><i>Từ ngày: <i id="tungay"></i>- đến ngày: <i id="denngay"></i></i></p>
                <p style="font-size: 12pt; width: 100%; display: none" id="socongvandiv"><i>Sổ công văn: <i id="socongvan"></i></i></p>
                <p style="font-size: 12pt; width: 100%; display: none" id="loaivanbandiv"><i>Loại văn bản: <i id="loaivanban"></i></i></p>
            </div>

            <div style="text-align: right; width: 100%; clear: both;"><span class="" style="margin-right: 1.25%;" id="tongbanghi"></span></div>
           
            <div class="smtable_header">
                <table class="smtable">
                    <thead>
                        <tr>
                            <th style="width: 8%">Số, ký hiệu văn bản</th>
                            <th style="width: 6%;">Ngày tháng năm văn bản</th>
                            <th>Tên loại và trích yếu nội dung văn bản</th>
                            <th style="width: 9%;">Người ký</th>
                            <th style="width: 11%;">Nơi nhận văn bản</th>

                            <th style="width: 11%;">Đơn vị, người soạn thảo</th>

                            <th style="width: 4%;">Số lượng bản</th>
                            <th style="width: 6%;">Ghi chú</th>
                        </tr>
                    </thead>
                    <tbody class="clsContent"></tbody>
                </table>
            </div>
            <script id="javascriptTemplate" type="text/x-kendo-template">
            <tr class="clsvbd" data-id="#=ID#">
                <td style="width: 8%" class="text-center"></td>
                <td class="text-center" style="width: 6%;"></td>
                <td class="text-left">#=Title#  </td>
                <td class="text-left" style="width: 9%;"></td>                          
                <td style="width: 11%;" class="text-left"></td>
                <td class="text-left" style="width: 11%;"></td>
                <td class="text-center" style="width: 4%;"></td>
                <td class="text-left" style="width: 6%;"></td>
               
            </tr>
            </script>
            <div style="">

                <div style="width: 50%; float: right; text-align: center; margin-top: 15px;">
                    <p style="width: 100%">Ngày <%=DateTime.Now.Day %> tháng <%=DateTime.Now.Month %> năm <%=DateTime.Now.Year %></p>
                    <p style="font-size: 12pt; width: 100%"><b>NGƯỜI LẬP BÁO CÁO</b></p>
                    <br />
                    <br />
                    <br />
                    <br />
                    <p style="font-size: 12pt; width: 100%"><b><%=CurentUser.Title.ToUpper() %></b></p>
                </div>

            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        $(".thongke").click(function () {
            ThongKe();
        });
        $(".input-datetime").daterangepicker({
            singleDatePicker: true,
            locale: { format: 'DD/MM/YYYY' }
        });

        $("#btnExel").click(function () {
            var $datathongke = $(".zone_search form").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VbDi/api/VanBanDi/VBDiQueryDataSolr?excel=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");
        });
        $("#btnword").click(function () {
            var $datathongke = $(".zone_search form").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VbDi/api/VanBanDi/VBDiQueryDataSolr?word=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");

        });
    });
    function ThongKe() {
        $("#tungay").text($("#SearchTuNgay").val());
        $("#denngay").text($("#SearchDenNgay").val());
        var data = $(".zone_search").siSerializeArray();
        $.post("/VIPortalAPI/api/BaiViet/QUERYDATA?", data, function (data) {
            var htmlTEmp = '';
            var templatehtmltr = kendo.template($("#javascriptTemplate").html());
            $(".mainContentBaoCao .clsContent").html("");
            if (data.data.length > 0) {
                for (var i = 0; i < data.data.length; i++) {
                    data.data[i]["STT"] = i + 1;
                    htmlTEmp += templatehtmltr(data.data[i]);
                }

                $(".mainContentBaoCao .clsContent").append(htmlTEmp);
            }
            $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');

            $('#tongbanghi').text("Có tổng số: " + data.data.length + " bản ghi");

            $(".clsvbd").css('cursor', 'pointer');

        });
    }

</script>
</html>
