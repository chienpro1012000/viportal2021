﻿<%@ control language="C#" autoeventwireup="true" codebehind="ChiTietBaiViet.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Forum.ChiTietBaiViet" %>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">Start Bootstrap</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home
              <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Services</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contact</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Post Content Column -->
        <div class="col-lg-8">

            <!-- Title -->
            <h1 class="mt-4"><%=oBaiVietItem.Title %></h1>

            <!-- Author -->
            <p class="lead">
                by
          <a href="#">Start Bootstrap</a>
            </p>

            <hr>

            <!-- Date/Time -->
            <p>Posted on January 1, 2019 at 12:00 PM</p>

            <hr>
            <div class="clsThreadNoiDung">
                <%=oBaiVietItem.ThreadNoiDung %>
            </div>


            <div class="card">
                <div class="card-body text-center">
                    <h4 class="card-title">Latest Comments</h4>
                </div>
                <div class="comment-widgets">
                    <!-- Comment Row -->
                    <div class="d-flex flex-row comment-row m-t-0">
                        <div class="p-2">
                            <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1574583336/AAA/4.jpg" alt="user" width="50" class="rounded-circle">
                        </div>
                        <div class="comment-text w-100">
                            <h6 class="font-medium">James Thomas</h6>
                            <span class="m-b-15 d-block">This is awesome website. I would love to comeback again. </span>
                            <div class="comment-footer">
                                <span class="text-muted float-right">April 14, 2019</span>
                                <button type="button" class="btn btn-cyan btn-sm">Thích</button>
                                <button type="button" class="btn btn-success btn-sm">Trả lời</button>
                                <button type="button" class="btn btn-danger btn-sm">Báo xấu</button>
                            </div>
                        </div>
                    </div>
                    <!-- Comment Row -->
                    <div class="d-flex flex-row comment-row">
                        <div class="p-2">
                            <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1574583319/AAA/3.jpg" alt="user" width="50" class="rounded-circle">
                        </div>
                        <div class="comment-text active w-100">
                            <h6 class="font-medium">Michael Hussey</h6>
                            <span class="m-b-15 d-block">Thanks bbbootstrap.com for providing such useful snippets. </span>
                            <div class="comment-footer">
                                <span class="text-muted float-right">May 10, 2019</span>
                                <button type="button" class="btn btn-cyan btn-sm">Thích</button>
                                <button type="button" class="btn btn-success btn-sm">Trả lời</button>
                                <button type="button" class="btn btn-danger btn-sm">Báo xấu</button>
                            </div>
                        </div>
                    </div>
                    <!-- Comment Row -->
                    <div class="d-flex flex-row comment-row">
                        <div class="p-2">
                            <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1574583246/AAA/2.jpg" alt="user" width="50" class="rounded-circle">
                        </div>
                        <div class="comment-text w-100">
                            <h6 class="font-medium">Johnathan Doeting</h6>
                            <span class="m-b-15 d-block">Great industry leaders are not the real heroes of stock market. </span>
                            <div class="comment-footer">
                                <span class="text-muted float-right">August 1, 2019</span>
                                <button type="button" class="btn btn-cyan btn-sm">Thích</button>
                                <button type="button" class="btn btn-success btn-sm">Trả lời</button>
                                <button type="button" class="btn btn-danger btn-sm">Báo xấu</button>
                            </div>
                        </div>
                    </div>

                    <div class="bg-light p-2">
                        <div class="d-flex flex-row align-items-start">
                            <img class="rounded-circle" src="https://i.imgur.com/RpzrMR2.jpg" width="60">
                            <input type="hidden" name="" />
                            <textarea class="form-control ml-1 shadow-none textarea" id="NDComments"></textarea>
                        </div>
                        <div class="mt-2 text-right">
                            <button class="btn btn-primary btn-sm shadow-none" type="button">Gửi trả lời</button>
                        </div>
                    </div>
                </div>
                <!-- Card -->
            </div>
        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

            <!-- Search Widget -->
            <div class="card my-4">
                <h5 class="card-header">Search</h5>
                <div class="card-body">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-append">
                            <button class="btn btn-secondary" type="button">Go!</button>
                        </span>
                    </div>
                </div>
            </div>

            <!-- Categories Widget -->
            <div class="card my-4">
                <h5 class="card-header">Categories</h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a href="#">Web Design</a>
                                </li>
                                <li>
                                    <a href="#">HTML</a>
                                </li>
                                <li>
                                    <a href="#">Freebies</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-6">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a href="#">JavaScript</a>
                                </li>
                                <li>
                                    <a href="#">CSS</a>
                                </li>
                                <li>
                                    <a href="#">Tutorials</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Side Widget -->
            <div class="card my-4">
                <h5 class="card-header">Side Widget</h5>
                <div class="card-body">
                    You can put anything you want inside of these side widgets. They are easy to use, and feature the new Bootstrap 4 card containers!
                </div>
            </div>

        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->

<!-- Footer -->
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Your Website 2020</p>
    </div>
    <!-- /.container -->
</footer>

<script type="text/javascript">
    //NDComments
    $(document).ready(function () {
        $("#NDComments").viCustomFck();
    });
</script>
