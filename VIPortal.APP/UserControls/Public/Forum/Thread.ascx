﻿<%@ control language="C#" autoeventwireup="true" codebehind="Thread.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Forum.Thread" %>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">Start Bootstrap</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home
              <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Services</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contact</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Page Content -->
<div class="container" id="bodyforum">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-9" id="body_rightthread">
            <!-- Title -->
            <h1 class="mt-4"><%=oForumChuDeItem.Title %></h1>

            <!-- Author -->
            <p class="lead">
                by
          <a href="#">Start Bootstrap</a>
            </p>
            <hr>
            <!-- Comments Form -->
            <div class="card my-4">
                <h5 class="card-header">Đăng bài viết mới</h5>
                <div class="card-body">
                    <div id="frmBaiViet">
                        <div class="form-group">

                            <input class="form-control" id="TitleBaiViet" name="Title" placeholder="Tiêu đề bài viết" />
                        </div>
                        <div class="form-group" id="ckNoidung" style="display: none;">
                            <input type="hidden" name="ChuDe" id="ChuDe" value="<%=oForumChuDeItem.Title%>" />
                            <textarea class="form-control" rows="3" id="ThreadNoiDung" name="ThreadNoiDung"></textarea>
                        </div>
                        <button type="button" id="submitThread" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>

            <div>
                <div id="list-data" data-role="fullGrid">
                    <div class="box-wrap-search-page -style01" style="padding-right: 0px !important">
                        <div role="search" class="box-search -adv row" id="frmsearch">
                        </div>
                    </div>

                    <div class="modal" id="advsearch">
                        <div class="popup-box" style="max-width: 470px;">
                        </div>
                    </div>
                    <div style="clear: both"></div>
                    <div class="main-title-sub">
                    </div>

                    <script id="template" type="text/template">
                        <div class="structItem structItem--thread is-prefix93 is-unread js-inlineModContainer js-threadListItem-22522" data-author="ken1805">
                            <div class="structItem-cell structItem-cell--icon">
                                <div class="structItem-iconContainer">
                                    <a href="/members/ken1805.128046/" class="avatar avatar--s" data-user-id="128046" data-xf-init="member-tooltip" id="js-XFUniqueId26">
                                        <img src="https://vn-z.vn/data/avatars/s/128/128046.jpg?1591167877" srcset="https://vn-z.vn/data/avatars/s/128/128046.jpg?1591167877" alt="ken1805" class="avatar-u128046-s" width="48" height="48" loading="lazy">
                                    </a>
                                </div>
                            </div>
                            <div class="structItem-cell structItem-cell--main" data-xf-init="touch-proxy">
                                <div class="structItem-title">
                                    <a href="/forums/phan-mem.144/?prefix_id=93" class="labelLink" rel="nofollow"><span class="label label--green" dir="auto">Cần Bán</span></a>
                                    <a href="/diendan/Pages/chitietbaiviet.aspx?ItemID={ID}" class="" data-tp-primary="on" data-xf-init="preview-tooltip" id="js-XFUniqueId27">{Title}</a>
                                </div>
                                <div class="structItem-minor">
                                    <ul class="structItem-parts">
                                        <li><a href="/members/ken1805.128046/" class="username " dir="auto" itemprop="name" data-user-id="128046" data-xf-init="member-tooltip" id="js-XFUniqueId28">ken1805</a></li>
                                        <li class="structItem-startDate"><a href="/threads/key-adobe-creative-cloud-20-app-100gb-cloud-chinh-chu-gia-cuc-re.22522/" rel="nofollow"><time class="u-dt" dir="auto" datetime="2020-06-03T14:34:28+0700" data-time="1591169668" data-date-string="3/6/20" data-time-string="14:34" title="3/6/20 lúc 14:34">3/6/20</time></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="structItem-cell structItem-cell--meta" title="First message reaction score: 0">
                                <dl class="pairs pairs--justified">
                                    <dt>Trả lời</dt>
                                    <dd>3</dd>
                                </dl>
                                <dl class="pairs pairs--justified structItem-minor">
                                    <dt>Lượt xem</dt>
                                    <dd>1K</dd>
                                </dl>
                            </div>
                            <div class="structItem-cell structItem-cell--latest">
                                <a href="/threads/key-adobe-creative-cloud-20-app-100gb-cloud-chinh-chu-gia-cuc-re.22522/latest" rel="nofollow"><time class="structItem-latestDate u-dt" dir="auto" datetime="2021-03-07T12:29:16+0700" data-time="1615094956" data-date-string="7/3/21" data-time-string="12:29" title="7/3/21 lúc 12:29">7/3/21</time></a>
                                <div class="structItem-minor">
                                    <a href="/members/nthaih.166480/" class="username " dir="auto" itemprop="name" data-user-id="166480" data-xf-init="member-tooltip" id="js-XFUniqueId29">nthaih</a>
                                </div>
                            </div>
                            <div class="structItem-cell structItem-cell--icon structItem-cell--iconEnd">
                                <div class="structItem-iconContainer">
                                    <a href="/members/nthaih.166480/" class="avatar avatar--xxs avatar--default avatar--default--dynamic" data-user-id="166480" data-xf-init="member-tooltip" style="background-color: #ebadcc; color: #b82e73" id="js-XFUniqueId30">
                                        <span class="avatar-u166480-s">N</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </script>
                    <div role="grid">
                    </div>
                    <div class="clspaging">
                    </div>
                </div>
            </div>

        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-3">

            <!-- Search Widget -->
            <div class="card my-4">
                <h5 class="card-header">Search</h5>
                <div class="card-body">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-append">
                            <button class="btn btn-secondary" type="button">Go!</button>
                        </span>
                    </div>
                </div>
            </div>

            <!-- Categories Widget -->
            <div class="card my-4">
                <h5 class="card-header">Categories</h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a href="#">Web Design</a>
                                </li>
                                <li>
                                    <a href="#">HTML</a>
                                </li>
                                <li>
                                    <a href="#">Freebies</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-6">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a href="#">JavaScript</a>
                                </li>
                                <li>
                                    <a href="#">CSS</a>
                                </li>
                                <li>
                                    <a href="#">Tutorials</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Side Widget -->
            <div class="card my-4">
                <h5 class="card-header">Side Widget</h5>
                <div class="card-body">
                    You can put anything you want inside of these side widgets. They are easy to use, and feature the new Bootstrap 4 card containers!
                </div>
            </div>

        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->

<!-- Footer -->
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Your Website 2020</p>
    </div>
    <!-- /.container -->
</footer>
<script type="text/javascript">
    $(document).ready(function () {
        //ckNoidung
        $("#TitleBaiViet").focus(function () {
            if ($("#ckNoidung").css('display') == 'none') {
                $("#ckNoidung").toggle();
            }
        });
        $("#submitThread").click(function () {
            CK_jQ();
            var oDataPost = {
                ChuDe: $("#ChuDe").val(),
                Title: $("#TitleBaiViet").val(),
                ThreadNoiDung: $("#ThreadNoiDung").val(),
            };
            $.post("/VIPortalAPI/api/ForumChuDe/AddThread", oDataPost, function (result) {
                debugger;
                if (result.State != 2) {
                    showMsg(result.Message);
                    //var $idtable = $tagData.find("table.smdataTable").first().dataTable().fnDraw(false);
                    $("#list-data").data("smGrid2018").RefreshGrid();
                    $("#frmBaiViet").clear_form_elements();
                    CKEDITOR.instances.ThreadNoiDung.setData('');

                }
            });
        });
        $("#list-data").smGrid2018({
            "UrlPost": "/VIPortalAPI/api/BaiViet/QUERYDATA",
            pageSize: 20,
            odata: { "do": "QUERYDATA" },
            template: "template",
            dataBound: function (data) {

            },
        });

    });
</script>
