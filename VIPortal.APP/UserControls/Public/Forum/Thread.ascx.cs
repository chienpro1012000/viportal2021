﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using VIPortalAPP;

namespace VIPortal.APP.UserControls.Public.Forum
{
    public partial class Thread : BaseUC_Web
    {
        public ForumChuDeItem oForumChuDeItem { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if(ItemID > 0)
            {
                ForumChuDeDA oForumChuDeDA = new ForumChuDeDA();
                oForumChuDeItem = oForumChuDeDA.GetByIdToObject<ForumChuDeItem>(ItemID);
            }
        }
    }
}