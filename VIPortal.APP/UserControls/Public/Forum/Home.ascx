﻿<%@ control language="C#" autoeventwireup="true" codebehind="Home.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Forum.Home" %>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">Start Bootstrap</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home
              <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Services</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contact</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Page Content -->
<div class="container" id="bodyforum">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-9" id="body_rightdiendan">
            <script id='block_category' type='text/template'>
                <div class="block block--category block--category81">
                    <span class="u-anchorTarget" id="trung-tam-dieu-hanh.82"></span>
                    <div class="block-container">
                        <h2 class="block-header">
                            <a href="/forums/#trung-tam-dieu-hanh.81">{title}</a>
                            <span class="block-desc">{mota}</span>
                        </h2>
                        <div class="block-body">
                            {htmlchild}
                        </div>
                    </div>
                </div>

            </script>
            <script id="node_category" type="text/html">
                <div class="node node--id83 node--depth2 node--forum node--unread">
                    <div class="node-body">
                        <span class="node-icon" aria-hidden="true">
                            <i class="fa--xf far fa-comments" aria-hidden="true"></i>
                        </span>
                        <div class="node-main js-nodeMain">
                            <h3 class="node-title">
                                <a href="/diendan/Pages/Thread.aspx?ItemID={key}" data-xf-init="element-tooltip" data-shortcut="node-description">{title}</a>
                            </h3>
                            <div class="node-description node-description--tooltip js-nodeDescTooltip">{mota}</div>
                            <div class="node-meta">
                                <div class="node-statsMeta">
                                    <dl class="pairs pairs--inline">
                                        <dt>Chủ đề</dt>
                                        <dd>176</dd>
                                    </dl>
                                    <dl class="pairs pairs--inline">
                                        <dt>Bài viết</dt>
                                        <dd>2.3K</dd>
                                    </dl>
                                </div>
                            </div>
                        </div>
                        <div class="node-stats">
                            <dl class="pairs pairs--rows">
                                <dt>Chủ đề</dt>
                                <dd>176</dd>
                            </dl>
                            <dl class="pairs pairs--rows">
                                <dt>Bài viết</dt>
                                <dd>2.3K</dd>
                            </dl>
                        </div>
                        <div class="node-extra">
                            <div class="node-extra-icon">
                                <a href="/members/administrator.1/" class="avatar avatar--xs" data-user-id="1" data-xf-init="member-tooltip" id="js-XFUniqueId45">
                                    <span class="avatar-u167013-s">N</span>
                                </a>
                            </div>
                            <div class="node-extra-row">
                                <a href="/threads/gop-y-chinh-sua-vn-zoom-phien-ban-2021.36353/post-514925" class="node-extra-title" title="Góp ý chỉnh sửa Vn-Zoom phiên bản 2021">Góp ý chỉnh sửa Vn-Zoom phiên bản 2021</a>
                            </div>
                            <div class="node-extra-row">
                                <ul class="listInline listInline--bullet">
                                    <li><time class="node-extra-date u-dt" dir="auto" datetime="2021-03-18T21:10:44+0700" data-time="1616076644" data-date-string="18/3/21" data-time-string="21:10" title="18/3/21 lúc 21:10">lúc 21:10, Thứ năm </time></li>
                                    <li class="node-extra-user"><a href="/members/administrator.1/" class="username " dir="auto" itemprop="name" data-user-id="1" data-xf-init="member-tooltip" id="js-XFUniqueId46"><span class="username--moderator username--admin">Administrator</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </script>
        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-3">

            <!-- Search Widget -->
            <div class="card my-4">
                <h5 class="card-header">Search</h5>
                <div class="card-body">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-append">
                            <button class="btn btn-secondary" type="button">Go!</button>
                        </span>
                    </div>
                </div>
            </div>

            <!-- Categories Widget -->
            <div class="card my-4">
                <h5 class="card-header">Categories</h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a href="#">Web Design</a>
                                </li>
                                <li>
                                    <a href="#">HTML</a>
                                </li>
                                <li>
                                    <a href="#">Freebies</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-6">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a href="#">JavaScript</a>
                                </li>
                                <li>
                                    <a href="#">CSS</a>
                                </li>
                                <li>
                                    <a href="#">Tutorials</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Side Widget -->
            <div class="card my-4">
                <h5 class="card-header">Side Widget</h5>
                <div class="card-body">
                    You can put anything you want inside of these side widgets. They are easy to use, and feature the new Bootstrap 4 card containers!
                </div>
            </div>

        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->

<!-- Footer -->
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Your Website 2020</p>
    </div>
    <!-- /.container -->
</footer>
<script type="text/javascript">
    $(document).ready(function () {
        ///UserControls/Forum/ForumChuDe/pAction.ashx?do=MENUTREE
        $.post("/VIPortalAPI/api/ForumChuDe/QUERYTREE", { "FieldOrder": "DMSTT", Ascending: true }, function (data) {
            var $htmlcategory = $("#block_category").html();
            var $htmltempnode = $("#node_category").html();
            var htmlrender = '';
            //body_rightdiendan
            $.each(data, function (i, item) {
                var htmlrendernode = '';
                //children
                $.each(item.children, function (j, itemchild) {
                    htmlrendernode += nano($htmltempnode, itemchild);
                });
                item['htmlchild'] = htmlrendernode; 
                htmlrender += nano($htmlcategory, item);

            });
            $("#body_rightdiendan").append(htmlrender);
        });
    });
</script>
