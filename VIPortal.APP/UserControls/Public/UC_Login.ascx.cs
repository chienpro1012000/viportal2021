﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.IdentityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils;
using ViPortalData;
using ViPortalData.LogHeThong;

namespace VIPortal.APP.UserControls.Public
{
    


    public partial class UC_Login : BaseUC_Web
    {
        public bool checkdangnhapSSO = false;
        public string UrlReturn = "/";
        public string UrlReturnSSO = "/";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request["loginasanotheruser"]) && Request["loginasanotheruser" ]== "true")
            {
                //http://10.9.125.78:8081/sso/logout?appCode=" + APP_CODE + " &redirectUrl=" + URL_HOST
                this.Response.Redirect($"http://14.238.40.33:6801/sso/logout?appCode=PORTAL&redirectUrl=https://portal.evnhanoi.vn/login/pages/login.aspx");
            }
            SimpleLogger oSimpleLogger = new SimpleLogger();
            LogHeThongDA oLogHeThongDA = new LogHeThongDA(true);
            oSimpleLogger.Info("Page_Load:start");
            if (!string.IsNullOrEmpty(Request["Source"]))
            {
                UrlReturn = Request["Source"];
                if ( string.IsNullOrEmpty(Request["ticket"])) //không có urlreturn và ko có ticket thì chuyển sang trang đăng nhập.
                {
                    this.Response.Redirect($"http://14.238.40.33:6801/sso/login?appCode=PORTAL&redirectUrl=https://portal.evnhanoi.vn/login/pages/login.aspx");
                }
            }
            
            //if (SPContext.Current.Web.CurrentUser == null && string.IsNullOrEmpty(Request["ticket"]) && string.IsNullOrEmpty(Request["loginbase"]))
            //{
            //    this.Response.Redirect($"http://14.238.40.33:6801/sso/login?appCode=PORTAL&redirectUrl=https://portal.evnhanoi.vn/login/pages/login.aspx");
            //}
            //else 
            if (!string.IsNullOrEmpty(Request["ticket"]))
            {
                string ticket = Request["ticket"];
                //http://14.238.40.33:1111//sso/serviceValidate?ticket=207f7906-3c6d-41aa-a2ca-d777dfd0d204&appCode=PORTAL
                var client = new HttpClient();
                //oSimpleLogger.Info("http://10.9.125.79:1111/sso/serviceValidate?ticket=" + ticket + "&appCode=PORTAL");
                var content = client.GetStringAsync("http://10.9.125.79:1111/sso/serviceValidate?ticket=" + ticket + "&appCode=PORTAL").Result;

                oSimpleLogger.Info(content);
                SSOInfor oSSOInfor = Newtonsoft.Json.JsonConvert.DeserializeObject<SSOInfor>(content);
                oSimpleLogger.Info(oSSOInfor.data.identity.username);
                oLogHeThongDA.UpdateObject<LogHeThongItem>(new LogHeThongItem()
                {
                    LogFunction = "Login",
                    CreatedUser = CurentUser != null ? new SPFieldLookupValue(CurentUser.ID, CurentUser.Title) : new SPFieldLookupValue(),
                    fldGroup = CurentUser != null ? new SPFieldLookupValue(CurentUser.Groups.ID, CurentUser.Groups.Title) : new SPFieldLookupValue(),
                    LogDoiTuong = "UserSSO",
                    LogThaoTac = "serviceValidate",
                    LogNoiDung = content,
                    LogIDDoiTuong = 0,
                    LogTrangThai = 1
                });
                if (oSSOInfor.code == "API-000")
                {
                    //check doan nay trong db xem co truongf nafy chua, neu chua co thi phai tao moi trong db sql user, vaf update truwongf cho list.
                    LUserDA lUserDA = new LUserDA(true);
                    List<LUserJson> lstUser = lUserDA.GetListJson(new LUserQuery() { staffCode = oSSOInfor.data.identity.staffCode, _ModerationStatus = 1, Length = 3 });
                    oLogHeThongDA.UpdateObject<LogHeThongItem>(new LogHeThongItem()
                    {
                        LogFunction = "Login",
                        CreatedUser = CurentUser != null ? new SPFieldLookupValue(CurentUser.ID, CurentUser.Title) : new SPFieldLookupValue(),
                        fldGroup = CurentUser != null ? new SPFieldLookupValue(CurentUser.Groups.ID, CurentUser.Groups.Title) : new SPFieldLookupValue(),
                        LogDoiTuong = "UserSSO",
                        LogThaoTac = "serviceValidate",
                        LogNoiDung = Newtonsoft.Json.JsonConvert.SerializeObject(lstUser),
                        LogIDDoiTuong = 0,
                        LogTrangThai = 1
                    });
                    if (lstUser.Count > 0)
                    {
                        LUserJson oLUserItem = lstUser[0];

                        if (!string.IsNullOrEmpty(Request["KeySSO"]))
                        {
                            lUserDA.SystemUpdateOneField(oLUserItem.ID, "KeySSO", Request["KeySSO"]);
                        }

                        if (string.IsNullOrEmpty(lstUser[0].TaiKhoanTruyCap))
                        {
                            //capaj nhatj lai cho thang nay vao tao trong sql
                            lUserDA.SystemUpdateOneField(lstUser[0].ID, "TaiKhoanTruyCap", (oSSOInfor.data.identity.username.ToLower()));
                            oLUserItem.TaiKhoanTruyCap = oSSOInfor.data.identity.username.ToLower();
                            MembershipCreateStatus status;
                            string passwordQuestion = "";
                            string passwordAnswer = "";

                            if (Membership.RequiresQuestionAndAnswer)
                            {
                                passwordQuestion = "evn";
                                passwordAnswer = "evn";
                            }
                            try
                            {
                                MembershipUser newUser = Membership.CreateUser(oLUserItem.TaiKhoanTruyCap, "123456a@",
                                                                               $"{oLUserItem.TaiKhoanTruyCap}@evnhn.vn", passwordQuestion,
                                                                               passwordAnswer, true, out status);
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                        bool checkdangnhap = SPClaimsUtility.AuthenticateFormsUser(new Uri(SPContext.Current.Web.Site.RootWeb.Url), oSSOInfor.data.identity.username, "123456a@");
                        oSimpleLogger.Info("checkdangnhap" + checkdangnhap);
                        if (checkdangnhap)
                        {
                            
                            //query ra đươn vị để rediẻct
                            LGroupDA lGroupDA = new LGroupDA();
                            LGroupItem oLGroupItem = lGroupDA.GetByIdToObject<LGroupItem>(oLUserItem.Groups.ID);
                            if (!string.IsNullOrEmpty(oLGroupItem.UrlSite))
                            {
                                //this.Response.Redirect(oLGroupItem.UrlSite);
                                UrlReturnSSO = oLGroupItem.UrlSite;
                            }
                            else UrlReturnSSO = "/";
                            //else this.Response.Redirect(this.UrlReturn);

                            oSimpleLogger.Info("Có chạy vao day: " + SPContext.Current.Web.Site.RootWeb.Url);
                            //this.Response.Redirect(SPContext.Current.Web.Site.RootWeb.Url);
                            //this.Response.Redirect("/Pages/HomePortal.aspx", false);
                            //Response.Redirect("/Pages/HomePortal.aspx", true);
                            checkdangnhapSSO = true;
                        }
                        else
                        {
                            lblMes.Text = "Tài khoản hoặc mật khẩu không hợp lệ";
                        }
                    }
                    else
                    {
                        lblMes.Text = "Tài khoản hoặc mật khẩu không hợp lệ";
                    }
                }
                else
                {
                    lblMes.Text = "Đăng nhập SSO Không thành công";
                }
            }


            //if (checkdangnhapSSO)
            //{
            //    this.Response.Redirect("/Pages/HomePortal.aspx", false);
            //}
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            string strCurrentUserName = this.Account.Text.Trim();
            string usermacdinh = System.Configuration.ConfigurationManager.AppSettings["UserMacDinh"];
            string passmacdinh = System.Configuration.ConfigurationManager.AppSettings["PassMacdinh"];
            string source = Request["Source"];
            if (!string.IsNullOrEmpty(strCurrentUserName) && !string.IsNullOrEmpty(password.Text))
            {
                //check tài khoản thông tin.
                LUserDA oLUserDA = new LUserDA(true);
                List<LUserJson> lstUser = oLUserDA.GetListJson(new LUserQuery() { TaiKhoanTruyCap = strCurrentUserName, _ModerationStatus = 1 });
                if (lstUser.Count > 0)
                {
                    bool checkdangnhap = SPClaimsUtility.AuthenticateFormsUser(Context.Request.UrlReferrer, strCurrentUserName, password.Text);
                    if (checkdangnhap)
                    {
                        if (this.UrlReturn == "/")
                        {
                            //query ra đươn vị để rediẻct
                            LGroupDA lGroupDA = new LGroupDA();
                            LGroupItem oLGroupItem = lGroupDA.GetByIdToObject<LGroupItem>(lstUser[0].Groups.ID);
                            if (!string.IsNullOrEmpty(oLGroupItem.UrlSite))
                            {
                                this.Response.Redirect(oLGroupItem.UrlSite);
                            }
                            else this.Response.Redirect(this.UrlReturn);
                        }
                        else
                        {
                            this.Response.Redirect(this.UrlReturn);
                        }
                    }
                    else
                    {
                        lblMes.Text = "Tài khoản hoặc mật khẩu không hợp lệ";
                    }
                }
                else
                {
                    lblMes.Text = "Tài khoản chưa được phê duyệt/Không tồn tại";
                }
            }
            else
            {
                lblMes.Text = "Tài khoản hoặc mật khẩu không hợp lệ";
            }
        }
    }
}