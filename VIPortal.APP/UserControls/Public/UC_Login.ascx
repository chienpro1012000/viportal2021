﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_Login.ascx.cs" inherits="VIPortal.APP.UserControls.Public.UC_Login" %>

<style type="text/css">
    .submit_form input, .submit_form input:hover {
        margin-left: 0px;
        background: none;
        font-size: 16px;
        font-weight: bold;
        color: white;
        border: none;
        cursor: pointer;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        var checkdangnhapSSO = '<%=checkdangnhapSSO%>';

        if (checkdangnhapSSO == 'True') {
            //alert('<%=UrlReturnSSO%>');
            window.location.href = "<%=UrlReturnSSO%>";

        }
    });

</script>
<div class="wrapper-login">
    <div class="login">


        <div class="header_login">


            <select name="" id="gender" class="my-select">

                <option data-thumbnail="/Content/themev1/img_login/background/vietnam.png" value="tiengviet">Tiếng việt</option>
                <option data-thumbnail="/Content/themev1/img_login/background/vietnam.png" value="tienganh">Tiếng anh</option>
            </select>

        </div>

        <div class="main_login ">
            <div class="form_login text-center">
                <div class="logo-from-login">
                    <img src="/Content/themev1/img/logo/logo_xanh_ngang.png" alt="">
                </div>
                <div class="title_from_login">
                    <h6 class="font-size-16">Đăng nhập cổng thông tin nội bộ</h6>

                </div>
                <div class="content_form pt-2">
                    <div action="" method="get">
                        <div class="form-group">
                            
                            <asp:hiddenfield id="UrlReturn" value="<%=UrlReturn %>" runat="server"></asp:hiddenfield>
                            <asp:textbox autocompletetype="None" cssclass="form-control" id="Account" placeholder="Tên đăng nhập" runat="server"></asp:textbox>
                            <div class="form_icon">
                                <i class="fas fa-user"></i>
                            </div>
                        </div>
                        <div class="form-group">
                            <!--<input type="text" name="" id="" class="form-control" placeholder="Mật khẩu ..." aria-describedby="helpId">-->
                            <asp:textbox autocompletetype="None" id="password" textmode="Password" cssclass="form-control" placeholder="Mật khẩu" runat="server"></asp:textbox>
                            <div class="form_icon">
                                <i class="fas fa-lock"></i>
                            </div>
                        </div>
                        <div class="form-group">
                            <a title="Đăng nhập với SSO" href="http://14.238.40.33:6801/sso/login?appCode=PORTAL&redirectUrl=https://portal.evnhanoi.vn/login/pages/login.aspx">SSO</a>
                        </div>
                        <p>
                            <asp:label id="lblMes" runat="server" text="" class="clsMes"></asp:label>
                        </p>
                        <div class="form-group">
                            <div class="submit_form font-size-16 box-shadow-basic">
                                <!--<a href="" class="submit">Đăng nhập</a>-->

                                <!--<button class="btn btn-main">Đăng nhập</button>-->
                                <asp:button cssclass="" id="Button1" onclick="Button1_Click" runat="server" text="Đăng nhập" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="img_login_right">
                <img src="/Content/themev1/img_login/background/1 (1).png" alt="">
            </div>
        </div>
    </div>
</div>
