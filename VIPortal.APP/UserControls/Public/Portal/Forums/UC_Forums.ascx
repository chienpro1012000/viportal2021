﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_Forums.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.Forums.UC_Forums" %>


<style type="text/css">
    .main-forum div.item_li_1 {
        margin: auto 20px;
    }
    .display-flex{
        flex-wrap: nowrap !important;
    }
    .main-forum .display-flex-righteous {
    display: flex;
    justify-content: normal;
}
</style>
<script type="text/javascript">
    function showChuDe(idtab) {
        //alert(idtab);
        $("div.item_li_1").removeClass("active");
        $("div.item_li_1[data-tabid='" + idtab + "']").addClass("active");
        $(".main-forum.mt-4").css("display", "none");
        $("#" + idtab).css("display", "block");
    }

    $(document).ready(function () {
        SetConfigLang('<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>', 'Forums');
        $.post("/VIPortalAPI/api/ForumChuDe/QUERYTREE", { "FieldOrder": "DMSTT", Ascending: true }, function (data) {
            var $jsTemplatelstChuDe = $("#jsTemplatelstChuDe").html();
            var template = kendo.template($jsTemplatelstChuDe);
            var result = kendo.render(template, data); //render the template
            $("#lstChuDe").html(result); //append the result to the page
            $("div.item_li_1:first-child").addClass("active");

            var $jsTemplatemainforum = $("#jsTemplatemainforum").html();
            $jsTemplatemainforum = kendo.template($jsTemplatemainforum);
            var resultmain = kendo.render($jsTemplatemainforum, data); //render the template
            $("#maincontent").html(resultmain); //append the result to the page
            $("#maincontent div.main-forum:first-child").css("display", "block");


            $.each(data, function (index, value) {
                //alert(index + ": " + value);
                var $idelement = "#lstBaiViet" + value.key;
                $($idelement).smGrid2018({
                    "TypeTemp": "kendo",
                    "UrlPost": "/VIPortalAPI/api/BaiViet/QUERYDATA",
                    ConfigLanguages: '<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>',
                    pageSize: 20,
                    odata: { "do": "QUERYDATA" },
                    template: "template_new",
                    dataBound: function (e) {
                        $(".editor").click(function () {
                            var _ItemID = $(this).attr("data-ItemID");
                            if (_ItemID > 0) {
                                openDialogv2("Chỉnh sửa chủ đề", "/UserControls/Public/Portal/Forums/frmAdd.aspx", { ItemIDBaiViet: _ItemID }, 1024);
                            }
                        });
                    },
                });
            });
        });

        $("#themoi").click(function () {
            var classActive = document.querySelectorAll('[class ^= "item_li_1 active"]');
            if (classActive.length > 0) {
                var IDChuDe = classActive[0].id;
            }
            openDialogv2("Thêm mới bài viết", "/UserControls/Public/Portal/Forums/frmAdd.aspx", {ItemID: IDChuDe},800);
        });
    });
    function openDialogv2(stitle, urlpageLoad, data, size, buttonName) {
        console.log(BootstrapDialog);
        BootstrapDialog.show({
            z_index_backdrop: 9000,
            z_index_modal: 9010,
            title: stitle,
            message: function (dialog) {
                var $message = dialog.getModalBody();
                //NProgress.start();
                var pageToLoad = dialog.getData('pageToLoad');
                if (data == undefined) {
                    data = {};
                }
                if (size == "NORMAL")
                    dialog.setSize(BootstrapDialog.SIZE_NORMAL);
                else if (size == "SMALL")
                    dialog.setSize(BootstrapDialog.SIZE_SMALL);
                else if (size == "LARGE")
                    dialog.setSize(BootstrapDialog.SIZE_LARGE);
                else if (size == "FULL") {
                    dialog.setSize(BootstrapDialog.SIZE_FULL);
                } else if ($.isNumeric(size)) {
                    var ModalDialog = dialog.getModalDialog();
                    ModalDialog.removeClass("modal-lg");
                    //ModalDialog.addClass("modal-fullscreen");
                    //fullscreen-xl 
                    ModalDialog.css("width", size + "px");
                    ModalDialog.css("max-width", size + "px");
                } else
                    dialog.setSize(BootstrapDialog.SIZE_WIDE);
                return "";
            },
            closable: true,
            closeByBackdrop: false,
            draggable: true,
            data: {
                pageToLoad: urlpageLoad
            },
            onshow: function (dialogRef) {

            },
            onshown: function (dialogRef) {
                //alert('Dialog is popped up.');
                //alert('Dialog is popping up, its message is ' + dialogRef.getMessage());
                var $message = dialogRef.getModalBody();
                //NProgress.start();
                var pageToLoad = dialogRef.getData('pageToLoad');
                if (data == undefined) {
                    data = {};
                }
                $.ajax({
                    method: "POST",
                    cache: false,
                    url: pageToLoad,
                    data: data
                }).done(function (msg) {
                    $message.html(msg);
                });
            },
            onhide: function (dialogRef) {
                //alert('Dialog is popping down, its message is ' + dialogRef.getMessage());
            },
            onhidden: function (dialogRef) {
                //alert('Dialog is popped down.');
            }
        });
    }
</script>

<section class="Forums">
    <script id="jsTemplatelstChuDe" type="text/x-kendo-template">
        <div class="item_li_1" id="#=key#" data-tabid="tab-#=key#" style="cursor: pointer;"><a onclick="showChuDe('tab-#=key#');">#=title#</a></div>
    </script>
    <script id="jsTemplatemainforum" type="text/x-kendo-template">
         <div class="main-forum mt-4 "  id="tab-#=key#" style="display: none;"> 
              <div id="lstBaiViet#=key#" data-role="fullGrid" data-urllist="">
                  <div role="search" class="display-flex-righteous formsearchpublic" id="frmsearch#=key#">
                      <input type="hidden" name="ChuDe" value="#=key#" />
                      <div class="items-form">
                          <input type="text" name="keyword"  data-lang="NhapTuKhoa" placeholder="Nhập từ khóa">
                          <input type="hidden" name="SearchInAdvance" value="Title">
                      </div>
                      <div class="items-form">
                          <input type="text" class="input-datetime" name="SearchTuNgay" id="SearchTuNgay" placeholder="Tạo từ ngày">
                      </div>
                      <div class="items-form">
                          <input type="text" class="input-datetime" name="SearchDenNgay" id="SearchDenNgay" placeholder="Tạo đến ngày">
                      </div>  
                      <div class="icon-button display-flex pl-2">
                      <button data-toggle="tooltip" data-placement="top" title="Xóa lọc tất cả"  class="reset_data"><i class="fas fa-sync-alt"></i></button>
                      <button type="button" class="btn btnsearch btn-search-meeting">Tìm kiếm</button>
                  </div>
                  </div>
                  <div role="grid" class="">
                  </div>
                  <div class="clspaging">
                  </div>
              </div>
          </div>
    </script>
    <script id="template_new" type="text/x-kendo-template">
        <div class="display-flex forum-items mt-2 box-shadow-basic">
             <div class="top-forum">
                 <img src="#=AnhDaiDien#" alt="" style="border-radius:50%;width: 80px; height: 80px;">
             </div>
             <div class="bottom-forum ml-4">
                 <h6 class="font-size-18 font-weight-bold color-table-meeting title-forum-botom">
                     <a href="/Pages/diendanchitiet.aspx?Itemid=#=ID#">#=Title#</a></h6>
                 <p class="margin-p item-forum-calendar">
                     <img src="/Content/themeV1/img_forum/user-avatar-with-check-mark.png" alt="">
                     <span class="font-size-12 color-title-FAQ">#=CreatedUser.Title#</span>
                 </p>
                 <div class="display-flex information-forum">
                     <p class="margin-p pr-3 item-forum-calendar">
                         <img src="/Content/themeV1/img_forum/calendar.png" alt="">
                         <span class="pl-3">#=formatDate(Created)#</span>
                     </p>
                     <p class="margin-p pr-3 item-forum-calendar">
                         <img src="/Content/themeV1/img_forum/view.png" alt=""><span class="pl-3">#=SoLuotXem#</span>
                     </p>
                     <p class="margin-p item-forum-calendar">
                         <img src="/Content/themeV1/img_forum/message.png" alt=""><span class="pl-3">#=SoLuotComment#</span>
                     </p>                     
                 </div>
             </div>
        </div>
    </script>
    <div class="main-forum bg-color-box-items box-shadow-basic">
        <div class="left_header_calendar pt-3 pb-3">
            <div class="list_calendar" id="lstChuDe">
                <div class="item_li_1 active" style="cursor: pointer;"><a onclick="show('tab');" class="font-size-14">Công việc</a></div>
                <div class="item_li_1" style="cursor: pointer;">
                    <a onclick="show('tab-one');">Xã hội</a>
                </div>
                <div class="item_li_1" style="cursor: pointer;">
                    <a onclick="show('tab-two');">Thư giãn</a>
                </div>               
            </div>
            <%--<div class="col-sm-3" style="float: right;">
                    <p class="text-right">
                        <a href=> </a>
                        <button class="btn btn-primary" id="themoi" type="button" >Thêm mới</button>
                    </p>
             </div>--%>
            <div id="maincontent">
            </div>
        </div>
    </div>

</section>