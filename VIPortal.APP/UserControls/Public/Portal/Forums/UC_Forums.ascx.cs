﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using VIPortalAPP;

namespace VIPortal.APP.UserControls.Public.Portal.Forums
{
    public partial class UC_Forums : BaseUC_Web
    {
        public BaiVietItem oBaiVietItem { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ItemID > 0)
            {
                BaiVietDA oBaiVietDA = new BaiVietDA();
                oBaiVietItem = oBaiVietDA.GetByIdToObject<BaiVietItem>(ItemID);
            }
        }
    }
}