﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using VIPortalAPP;

namespace VIPortal.APP.UserControls.Public.Portal.Forums
{
    public partial class frmAdd : pFormBase
    {
        public ForumChuDeItem oForumChuDeItem = new ForumChuDeItem();
        public BaiVietItem oBaiVietItem = new BaiVietItem();     
        public int ItemIDBaiViet { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request["ItemIDBaiViet"]))
            {
                ItemIDBaiViet = Convert.ToInt32(Request["ItemIDBaiViet"]);
            }
            else ItemIDBaiViet = 0;
            if (ItemIDBaiViet > 0)
            {
                BaiVietDA oBaiVietDA = new BaiVietDA();
                oBaiVietItem = oBaiVietDA.GetByIdToObject<BaiVietItem>(ItemIDBaiViet);
            }
            if (ItemID > 0)
            {
                ForumChuDeDA oForumChuDeDA = new ForumChuDeDA();
                oForumChuDeItem = oForumChuDeDA.GetByIdToObject<ForumChuDeItem>(ItemID);
            }           
        }
    }
}