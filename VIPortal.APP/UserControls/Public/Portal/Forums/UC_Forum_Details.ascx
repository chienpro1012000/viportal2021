﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_Forum_Details.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.Forums.UC_Forum_Details" %>

<script type="text/javascript">
    $(document).ready(function () {
        function loopLi() {
            setInterval(function () { // this code is executed every 500 milliseconds:
                var current = '/Pages/diendan.aspx';
                var actived = false;
                $('#items_sliderbar li a').each(function () {
                    var $this = $(this);
                    // if the current path is like this link, make it active
                    if ($this.attr('href').indexOf(current) !== -1) {
                        $this.closest("li").addClass('active');
                        actived = true;
                    }
                });
                if (actived) return;
            }, 500);
        }

        $(loopLi);
        $("#lstComment").smGrid2018({
            "TypeTemp": "kendo",
            "UrlPost": "/VIPortalAPI/api/BinhLuan/QUERYDATA?BaiVietID=<%=oBaiVietItem.ID%>",
            pageSize: 20,
            infoEmpty : "Chưa có bình luận",
            odata: { "do": "QUERYDATA" },
            template: "jsTemplateComment",
            dataBound: function (e) {
            },
        });
        $.post("/VIPortalAPI/api/BaiViet/QUERYDATA?ChuDe=<%=oBaiVietItem.ChuDe.LookupId%>&NotGetID=<%=oBaiVietItem.ID%>", { "FieldOrder": "Created", Ascending: false, "length": 5 }, function (odata) {
            var templateContent = $("#template_baiviet").html();
            var template = kendo.template(templateContent);

            odata.data = jQuery.map(odata.data, function (n, i) {
                odata.data[i].STT = i + 1;
                return odata.data[i];
            });

            var result = kendo.render(template, odata.data); //render the template

            $("#lstBaivietlienquan").html(result); //append the result to the page
        });

        $("#submitComment").click(function () {
            CK_jQ();
            if ($("#ndcomment").val().trim() != '') {
                var oDataPost = {
                    Title: $("#ndcomment").val(),
                    BaiViet: <%=oBaiVietItem.ID%>,
                    IDChuDe:'<%=oBaiVietItem.ChuDe.ToString()%>',
                    CreatedUser: <%=CurentUser.ID%>,
                    "AnhDaiDien": GetPathUrlString( "<%=CurentUser.AnhDaiDien%>"),
                };
                $.post("/VIPortalAPI/api/BinhLuan/AddComment", oDataPost, function (result) {
                    if (result.State != 2) {
                        showMsg(result.Message);
                        //var $idtable = $tagData.find("table.smdataTable").first().dataTable().fnDraw(false);
                        $("#lstComment").data("smGrid2018").RefreshGrid();
                        $("#frmComment").clear_form_elements();
                        CKEDITOR.instances.ThreadNoiDung.setData('');
                    }
                });
            } else {
                BootstrapDialog.show({
                    title: "Lỗi",
                    message: 'Nhập nội dung bình luận'
                });
            }
        });
    });
</script>
<section>
    <script id="jsTemplateComment" type="text/x-kendo-template">
         <div class="row post-comment-fourm">
                        <div class="col-md-3 user-forumdetail text-center ml-3 mr-3">
                            #if(AnhDaiDien != ""){#
                                <img src="#=AnhDaiDien#" style="width: 70px; height: 70px; border-radius:50%" >
                            #}else{#<img src="/Content/themeV1/img/avatar/avatar_nam.jpg" style="width: 70px; height: 70px; border-radius:50%">#}#
                            <p class="font-size-14 margin-p">#=CreatedUser.Title#</p>
                            <p class="font-size-12 margin-p" style="color: \#ff0000;">Quản trị viên </p>
                        </div>
                        <div class=" col-md-9 content-user-forumdetail">
                            <p class="font-size-15">
                                     #=Title#
                            </p>
                        </div>
                    </div>
    </script>

    <script id="template_baiviet" type="text/x-kendo-template">
          <div class="display-flex news-fourm-right box-shadow-basic pt-3">
                            <div class="pl-3">
                                
                                <img class="pt-2" src="#=AnhDaiDien#" alt="">
                            </div>
                            <div class="pl-3 pb-2 pr-3">
                                <h6 class="font-size-16 font-weight-bold color-table-meeting"> <a href="/Pages/diendanchitiet.aspx?Itemid=#=ID#">#=Title#</a></h6>
                                <p class="margin-p">
                                    <img src="/Content/themeV1/img_forum/user-avatar-with-check-mark.png" alt="">
                                    <span class="font-size-12 color-title-FAQ">#=CreatedUser.Title#</span>
                                </p>
                                <div class="display-flex information-forum">
                                    <p class="margin-p pr-3">
                                        <img src="/Content/themeV1/img_forum/calendar.png" alt="">
                                        <span class="pl-3">#=formatDate(Created)#</span>
                                    </p>
                                </div>
                            </div>
                        </div>
    </script>


    <div class="main-forumdetail bg-color-box-items mt-3">
        <div class="header-forumdetail pt-2 pl-3 ">
            <p class="font-size-14 color-table-meeting"><a href="/Pages/HomePortal.aspx">Trang chủ</a> - <a href="/Pages/diendan.aspx">Diễn đàn</a></p>
        </div>
        <div class="header-forumdetail pt-2 pl-3 ">
            <p class="font-size-14 color-table-meeting"><%=oBaiVietItem.Title%></p>
        </div>
        <div class="content-forumdetail pl-3 pr-3">

            <%=oBaiVietItem.ThreadNoiDung%>
            <p class="font-size-13 ">
                <img src="/Content/themeV1/img_news/calendar.png" alt=""><span class="pl-2"><%=oBaiVietItem.Created%></span>
            </p>
        </div>
        <div id="maincontent">
            <div class="main-content-forumdetail">
                <div class="row">
                    <div class="col-md-7 pb-5" data-role="fullGrid" id="lstComment">
                        <div role="search" class="box-search -adv row" id="frmsearch">
                            <input type="hidden" name="UrlListTinTuc" value="" />
                        </div>
                        <div role="grid">
                        </div>
                        <div class="clspaging">
                        </div>

                        <div class="enter_information display-flex" id="frmComment">
                            <div class="user-iput">
                                <img src="" alt="">
                            </div>
                            <div class="pl-3 text-information">
                                <textarea name="" id="ndcomment" cols="92" rows="5" placeholder="Nhập bình luận ..."></textarea>
                            </div>
                            <div>
                                <button type="button" id="submitComment" class="btn btn-primary">Gửi</button>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-5 pb-5">
                        <div class="title_left_forumdetail pl-3 mr-3 pt-1">
                            <p class="font-size-18 font-weight-bold">Bài liên quan</p>
                        </div>
                        <div role="grid" class="" id="lstBaivietlienquan">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<style type="text/css">
    #lstBaivietlienquan img.pt-2 {
        position: relative;
        width: 40px;
        height: 40px;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        border-radius: 50%;
        margin-right: 15px;
        padding-top: 0px !important;
    }
</style>
