﻿<%@ page language="C#" autoeventwireup="true" codebehind="frmAdd.aspx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.Forums.frmAdd" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <div class="form-horizontal">
        <div id="frmBaiViet">
            <form id="exForm">
                <div class="form-group">
                    <label for="TitleBaiViet">Tiêu đề bài viết</label>
                    <input type="text" class="form-control" id="TitleBaiViet" name="TitleBaiViet" required="required" value="<%:oBaiVietItem.Title %>" placeholder="Tiêu đề bài viết" />

                </div>
                <div class="form-group">
                    <label for="ThreadNoiDung">Nội dung</label>
                    <textarea class="form-control" id="ThreadNoiDung" name="ThreadNoiDung" required="required" ><%:oBaiVietItem.ThreadNoiDung%></textarea>
                </div>
                <button type="button" id="submitThread" class="btn btn-primary">Cập nhập</button>
            </form>

        </div>
    </div>
</body>
</html>

<script type="text/javascript">
    $(document).ready(function () {
        //cknoidung
        $("#titlebaiviet").focus(function () {
            if ($("#cknoidung").css('display') == 'none') {
                $("#cknoidung").toggle();
            }
        });
        CKEDITOR.replace('ThreadNoiDung', {
            // Define the toolbar groups as it is a more accessible solution.
            toolbarGroups: [{
                "name": "basicstyles",
                "groups": ["basicstyles"]
            },
            {
                "name": "links",
                "groups": ["links"]
            },
            {
                "name": "paragraph",
                "groups": ["list", "blocks"]
            },
            {
                "name": "document",
                "groups": ["mode"]
            },
            {
                "name": "insert",
                "groups": ["insert"]
            },
            {
                "name": "styles",
                "groups": ["styles"]
            },
            {
                "name": "about",
                "groups": ["about"]
            }
            ],
            // Remove the redundant buttons from toolbar groups defined above.
            removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
        });
        $("#exForm").validate({
            ignore: [],
            rules: {
                TitleBaiViet: "required",
                ThreadNoiDung: "required"
            },
            messages: {
                TitleBaiViet: "Vui lòng nhập tiêu đề bài viết",
                ThreadNoiDung: "Vui lòng nhập nội dung bài viết",
            },
            submitHandler: function (form) {
                CK_jQ();
                debugger;
                var odatapost = {                   
                    chude: <%=ItemIDBaiViet > 0 ? oBaiVietItem.ChuDe.LookupId : oForumChuDeItem.ID %>,
                    title: $("#TitleBaiViet").val(),
                    threadnoidung: $("#ThreadNoiDung").val(),
                    CreatedUser: <%=CurentUser.ID%>,
                    "AnhDaiDien": GetPathUrlString("<%=CurentUser.AnhDaiDien%>"),
                    ItemIDBaiViet: <%=ItemIDBaiViet %>,
                };
                $.post("/VIPortalAPI/api/ForumChuDe/AddThread", odatapost, function (result) {
                    if (result.State == 2) {
                        BootstrapDialog.show({
                            title: "Lỗi",
                            message: result.Message
                        });
                    }
                    else {
                        showMsg(result.Message);
                        $(form).closeModal();
                    }
                }).always(function () { });
            }
        });
        $("#submitThread").click(function () {
            $("#exForm").submit();
        });
        $("#exForm").viForm();
    });
</script>

