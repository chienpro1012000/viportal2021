﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HoiDapHome.ascx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.HoiDapHome" %>

<script type="text/javascript">
    $(document).ready(function () {
        SetConfigLang('<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>', 'HoiDapHomeDiv');
        ///UserControls/Forum/ForumChuDe/pAction.ashx?do=MENUTREE
        $.post("/VIPortalAPI/api/FAQThuongGap/QUERYDATA", { "FieldOrder": "Created", Ascending: false, "length": 5 }, function (odata) {
            //xử lý phần stt.
            var DataCustom = jQuery.map(odata.data, function (n, i) {
                odata.data[i].STT = i + 1;
                return odata.data[i];
            });
            var templateContent = $("#jsTemplateFAQ").html();
            var template = kendo.template(templateContent);
            var result = kendo.render(template, DataCustom); //render the template
            $("#accordionExample").html(result); //append the result to the page
        });
        //loading();
    });
</script>
<%if(!string.IsNullOrEmpty(HtmlOutput)){ %>
<%=HtmlOutput %>
<%}else{ %>
<script id="jsTemplateFAQ" type="text/x-kendo-template">
        <div class="list pt-2">
            <div class="list_question" id="heading#=STT#">
                <p class="title-question-list  font-weight-bold font-size-16">
                    <img src="/Content/themeV1/icon/question.png" alt="">
                    <span class="pl-3">#=Title#</span>
                </p>
                <p class="btn collapsed font-size-14" type="button" data-toggle="collapse" data-target="\#collapse#=STT#" aria-expanded="false" aria-controls="collapse#=STT#">
                    <span class="pr-2">Xem trả lời</span><img src="/Content/themeV1/img_FAQ/icon/Polygon 3.png" class="/Content/themeV1/img_FAQ img-question-list" alt="">
                </p>
            </div>
            <div id="collapse#=STT#" class="collapse" aria-labelledby="heading#=STT#" data-parent="\#accordionExample">
                <div class="reply-FAQ pl-5">
                    <img src="/Content/themeV1/img_FAQ/icon/Solid.png" alt="">
                    <img src="/Content/themeV1/img_FAQ/icon/user-avatar-with-check-mark.png" alt="">
                    <span class="color-title-FAQ">#=FQNguoiTraLoi.Title#</span>
                    <p class="rep-ply pl-5 font-size-14" style="color: \#194398;">
                        #=FAQNoiDungTraLoi#<a href="/" class="font-size-12" style="color: \#FB0EEB;">&nbsp;&nbsp;Xem tiếp</a>
                    </p>
                </div>
            </div>
        </div>

</script>

<div class="question mt-3 box-shadow-basic HoiDapHomeDiv">
    <div class="header_question ">
        <div class="title_question pl-3 pt-2 pb-2 ">
            <p class="margin-p font-size-16 font-weight-bold"><a href="/Pages/questions.aspx" data-lang="CauHoiThuongGap">Câu hỏi thường gặp</a></p>
            <p class="margin-p" data-lang="CauHoiThuongGap">Những câu hỏi thường gặp</p>
        </div>
        <div class="next_question pr-3 pt-3 ">
            <a href="/Pages/questions.aspx"><span class="pl-2" data-lang="XemTatCa">Xem tất cả</span>
                <svg class="svg-inline--fa fa-angle-double-right fa-w-14" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-double-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                    <path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34zm192-34l-136-136c-9.4-9.4-24.6-9.4-33.9 0l-22.6 22.6c-9.4 9.4-9.4 24.6 0 33.9l96.4 96.4-96.4 96.4c-9.4 9.4-9.4 24.6 0 33.9l22.6 22.6c9.4 9.4 24.6 9.4 33.9 0l136-136c9.4-9.2 9.4-24.4 0-33.8z"></path></svg><!-- <i class="fas fa-angle-double-right "></i> Font Awesome fontawesome.com --></a>
        </div>

    </div>
    <div class="accordion " id="accordionExample">
        <div class="list pt-2">
            <div class="list_question" id="headingOne">
                <p class="title-question-list  font-weight-bold font-size-16">
                    <img src="/Content/themeV1/icon/question.png" alt="">
                    <span class="pl-3" >Chế độ bảo hiểm như nào ?</span>
                </p>
                <p class="btn collapsed font-size-14" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                    <span class="pr-2" >xem trả lời</span><img src="/Content/themeV1/img_FAQ/icon/Polygon 3.png" class="/Content/themeV1/img_FAQ img-question-list" alt="">
                </p>
            </div>
            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="reply-FAQ pl-5">
                    <img src="/Content/themeV1/img_FAQ/icon/Solid.png" alt="">
                    <img src="/Content/themeV1/img_FAQ/icon/user-avatar-with-check-mark.png" alt="">
                    <span class="color-title-FAQ" >Quản trị viên</span>
                    <p class="rep-ply pl-5 font-size-14" style="color: #194398;" >
                        Lao động nữ sinh con được nghỉ việc hưởng chế độ thai sản trước và sau khi sinh con là 06 tháng. Trường hợp lao động nữ sinh đôi trở lên thì tính từ con thứ hai trở đi, cứ mỗi con, người mẹ được nghỉ thêm 01
                                                tháng - Thời gian nghỉ hưởng chế độ thai sản trước khi sinh tối đa không quá 02 tháng. <a href="/" class="font-size-12" style="color: #FB0EEB;">Xem tiếp</a>
                    </p>
                </div>
            </div>
        </div>

        <div class="list pt-2">
            <div class="list_question" id="headingTwo">
                <p class=" title-question-list font-weight-bold font-size-16">
                    <img src="/Content/themeV1/icon/question.png" alt="">
                    <span class="pl-3" >Hỏi về quy chế đi
                                                muộn của công ty ?</span>
                </p>
                <p class="btn  collapsed font-size-14" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    <span class="pr-2" >xem trả lời</span><img src="/Content/themeV1/img_FAQ/icon/Polygon 3.png" class="/Content/themeV1/img_FAQ img-question-list" alt="">
                </p>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                <div class=" reply-FAQ pl-5">
                    <img src="/Content/themeV1/img_FAQ/icon/Solid.png" alt="">
                    <img src="/Content/themeV1/img_FAQ/icon/user-avatar-with-check-mark.png" alt="">
                    <span class="color-title-FAQ">Quản trị viên</span>
                    <p class="rep-ply pl-5 font-size-14" style="color: #194398;">
                        Lao động nữ sinh con được nghỉ việc hưởng chế độ thai sản trước và sau khi sinh con là 06 tháng. Trường hợp lao động nữ sinh đôi trở lên thì tính từ con thứ hai trở đi, cứ mỗi con, người mẹ được nghỉ thêm 01
                                                tháng - Thời gian nghỉ hưởng chế độ thai sản trước khi sinh tối đa không quá 02 tháng.
                                                <a href="/" class="font-size-12" style="color: #FB0EEB;">Xem
                                                    tiếp</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="list pt-2">
            <div class="list_question" id="headingThree">
                <p class="title-question-list  font-weight-bold font-size-16">
                    <img src="/Content/themeV1/icon/question.png" alt=""><span class="pl-3">Thời gian ngỉ
                                                đẻ</span>
                </p>
                <p class="btn collapsed font-size-14" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    <span class="pr-2">xem trả lời</span><img src="/Content/themeV1/img_FAQ/icon/Polygon 3.png" class="/Content/themeV1/img_FAQ img-question-list" alt="">
                </p>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                <div class="reply-FAQ pl-5">
                    <img src="/Content/themeV1/img_FAQ/icon/Solid.png" alt="">
                    <img src="/Content/themeV1/img_FAQ/icon/user-avatar-with-check-mark.png" alt="">
                    <span class="color-title-FAQ">Quản trị viên</span>
                    <p class="rep-ply pl-5 font-size-14" style="color: #194398;">
                        Lao động nữ sinh con được nghỉ việc hưởng chế độ thai sản trước và sau khi sinh con là 06 tháng. Trường hợp lao động nữ sinh đôi trở lên thì tính từ con thứ hai trở đi, cứ mỗi con, người mẹ được nghỉ thêm 01
                                                tháng - Thời gian nghỉ hưởng chế độ thai sản trước khi sinh tối đa không quá 02 tháng.
                                                <a href="/" class="font-size-12" style="color: #FB0EEB;">Xem
                                                    tiếp</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="list pt-2">
            <div class="list_question" id="headingFour">
                <p class="title-question-list  font-weight-bold font-size-16">
                    <img src="/Content/themeV1/icon/question.png" alt=""><span class="pl-3">Thời gian ngỉ
                                                phép</span>
                </p>
                <p class="btn collapsed font-size-14" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                    <span class="pr-2">xem trả lời</span><img src="/Content/themeV1/img_FAQ/icon/Polygon 3.png" class="/Content/themeV1/img_FAQ img-question-list" alt="">
                </p>
            </div>
            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                <div class="reply-FAQ pl-5">
                    <img src="/Content/themeV1/img_FAQ/icon/Solid.png" alt="">
                    <img src="/Content/themeV1/img_FAQ/icon/user-avatar-with-check-mark.png" alt="">
                    <span class="color-title-FAQ">Quản trị viên</span>
                    <p class="rep-ply pl-5 font-size-14" style="color: #194398;">
                        Lao động nữ sinh con được nghỉ việc hưởng chế độ thai sản trước và sau khi sinh con là 06 tháng. Trường hợp lao động nữ sinh đôi trở lên thì tính từ con thứ hai trở đi, cứ mỗi con, người mẹ được nghỉ thêm 01
                                                tháng - Thời gian nghỉ hưởng chế độ thai sản trước khi sinh tối đa không quá 02 tháng.
                                                <a href="/" class="font-size-12" style="color: #FB0EEB;">Xem
                                                    tiếp</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="list pt-2">
            <div class="list_question" id="headingFive">
                <p class="title-question-list font-weight-bold font-size-16">
                    <img src="/Content/themeV1/icon/question.png" alt=""><span class="pl-3">Thời gian ngỉ trưa
                                                ?</span>
                </p>
                <p class="btn collapsed font-size-14" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                    <span class="pr-2">xem trả lời</span><img src="/Content/themeV1/img_FAQ/icon/Polygon 3.png" class="/Content/themeV1/img_FAQ img-question-list" alt="">
                </p>
            </div>
            <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                <div class="reply-FAQ pl-5">
                    <img src="/Content/themeV1/img_FAQ/icon/Solid.png" alt="">
                    <img src="/Content/themeV1/img_FAQ/icon/user-avatar-with-check-mark.png" alt="">
                    <span class="color-title-FAQ">Quản trị viên</span>
                    <p class="rep-ply pl-5 font-size-14" style="color: #194398;">
                        Lao động nữ sinh con được nghỉ việc hưởng chế độ thai sản trước và sau khi sinh con là 06 tháng. Trường hợp lao động nữ sinh đôi trở lên thì tính từ con thứ hai trở đi, cứ mỗi con, người mẹ được nghỉ thêm 01
                                                tháng - Thời gian nghỉ hưởng chế độ thai sản trước khi sinh tối đa không quá 02 tháng.
                                                <a href="/" class="font-size-12" style="color: #FB0EEB;">Xem
                                                    tiếp</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<%} %>
