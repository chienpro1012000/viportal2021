﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData;

namespace VIPortal.APP.UserControls.Public.Portal
{
    public partial class LichLamViec : BaseUC_Web
    {
        public LGroupItem oLGroupItem { get; set; }
        public LGroupItem oDonVi { get; set; }
        public string MaDonViSelected { get; set; }
        public string MaPhongBanSelected { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oLGroupItem = new LGroupItem();
            LGroupDA oLGroupDA = new LGroupDA();
            oDonVi = new LGroupItem();
            if (CurentUser.UserPhongBan.ID > 0 && CurentUser != null)
            {
                oLGroupItem = oLGroupDA.GetByIdToObject<LGroupItem>(CurentUser.UserPhongBan.ID);
            }
            if (CurentUser.Groups.ID > 0 && CurentUser != null)
            {
                oDonVi = oLGroupDA.GetByIdToObject<LGroupItem>(CurentUser.Groups.ID);
                
            }
            if (!string.IsNullOrEmpty(UrlSite) && UrlSite != "/en") //nếu là site đơn vị thì như này còn ko thì quy về site tổng.
            {
                //LGroupDA lGroupDA = new LGroupDA();
                LGroupJson lGroupJson = oLGroupDA.GetListJson(new LGroupQuery() { UrlSite = UrlSite }).FirstOrDefault();
                if (lGroupJson != null)
                {
                    if (!string.IsNullOrEmpty(lGroupJson.orgCode))
                        MaDonViSelected = lGroupJson.OldID;
                    else
                    {
                        LGroupItem oLGroupItem = oLGroupDA.GetByIdToObject<LGroupItem>(lGroupJson.GroupParent.ID);
                        MaPhongBanSelected = lGroupJson.OldID;
                        MaDonViSelected = oLGroupItem.OldID;
                    }
                }

            }
            else
            {
                MaDonViSelected = "276";
            }
        }
    }
}