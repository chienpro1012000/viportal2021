﻿<%@ control language="C#" autoeventwireup="true" codebehind="ThuVienAnhHome.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.ThuVienAnhHome" %>

<script type="text/javascript">
    var slideIndex = 1;
    showSlides(slideIndex);

    function plusSlides(n) {
        showSlides(slideIndex += n);
    }

    function currentSlide(n) {
        showSlides(slideIndex = n);
    }

    function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");

        if (n > slides.length) { slideIndex = 1 }
        if (n < 1) { slideIndex = slides.length }
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        if (slides[slideIndex - 1] != undefined) {
            slides[slideIndex - 1].style.display = "block";
        }
    }
    $(document).ready(function () {
        SetConfigLang('<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>', 'ThuVienAnhHome');
        ///UserControls/Forum/ForumChuDe/pAction.ashx?do=MENUTREE
        $.post("/VIPortalAPI/api/HinhAnhVideo/QUERYDATA", { "FieldOrder": "TBNgayDang", Ascending: false, "length": 6 }, function (odata) {
            var templateContent = $("#jsTemplateHinhAnhSlide1").html();
            var template = kendo.template(templateContent);
            var result = kendo.render(template, odata.data); //render the template
            $("#slideimage1").html(result); //append the result to the page
            $("#slideimage1 .mySlides:first-child")
                .css("display", "block");

            templateContent = $("#jsTemplateHinhAnhSlide2").html();
             template = kendo.template(templateContent);
            result = template(odata.data); //render the template
            $("#thumlimage").html(result); //append the result to the page
           
        });
    });
</script>
<%if(!string.IsNullOrEmpty(HtmlOutput)){ %>
<%=HtmlOutput %>
<%}else{ %>
<div class="main_two1 sub_ss_main_two  pt-4 pl-2 pr-2 pb-2 ThuVienAnhHome">
    <div class="header_news">
        <div class="title_new">
            <div class="title-new-content font-size-16 bold">
                <img src="/Content/themeV1/icon/image.png"><span class="pl-2 font-size-20 color-table-meeting font-weight-bold" data-lang="ThuVienMediaTitle">Thư viện media</span>
            </div>
            <p class="font-size-13 mt-1 opa-65" data-lang="ThuVienMediaContent">Những video hình ảnh đẹp về EVN</p>
        </div>
        <div class="next_news">
            <a href="<%=UrlSite %>/Pages/image.aspx" class="color-table-meeting"><span class="pr-2" data-lang="XemTatCaTitle" >Xem tất cả</span><svg class="svg-inline--fa fa-angle-double-right fa-w-14" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-double-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34zm192-34l-136-136c-9.4-9.4-24.6-9.4-33.9 0l-22.6 22.6c-9.4 9.4-9.4 24.6 0 33.9l96.4 96.4-96.4 96.4c-9.4 9.4-9.4 24.6 0 33.9l22.6 22.6c9.4 9.4 24.6 9.4 33.9 0l136-136c9.4-9.2 9.4-24.4 0-33.8z"></path></svg><!-- <i class="fas fa-angle-double-right"></i> Font Awesome fontawesome.com --></a>
        </div>
    </div>
    <script id="jsTemplateHinhAnhSlide1" type="text/x-kendo-template">
        <div class="mySlides img-slider-media img-meida-show" style="display: none;">
            <img src="#=ImageNews#">
        </div>
    </script>
    <div class="container">
        <div id="slideimage1">
            <div class="mySlides img-slider-media img-meida-show" style="display: block;">
                <img src="/Content/themeV1/img/media/1546c82c87ab72f52bba.jpg">
            </div>
            <div class="mySlides img-slider-media img-meida-show" style="display: none;">
                <img src="/Content/themeV1/img/News/d4f7a76917ede2b3bbfc.jpg">
            </div>
            <div class="mySlides img-slider-media img-meida-show" style="display: none;">
                <img src="/Content/themeV1/img/media/Mask-2.png">
            </div>
            <div class="mySlides img-slider-media img-meida-show" style="display: none;">
                <img src="/Content/themeV1/img/media/Mask-4@2x.png">
            </div>
            <div class="mySlides img-slider-media img-meida-show" style="display: none;">
                <img src="/Content/themeV1/img/media/Mask-5.png">
            </div>
        </div>
        <a class="prev" onclick="plusSlides(-1)">❮</a>
        <a class="next" onclick="plusSlides(1)">❯</a>
        <script id="jsTemplateHinhAnhSlide2" type="text/x-kendo-template">
        #for (var i = 0; i < data.length; i++) {
            #
            <div class="column">
                <img class="demo cursor" src="#=data[i].ImageNews#" style="width: 100%" onclick="currentSlide(#=i+1#)">
            </div>
        #}#
    </script>
        <div class="pt-2 pb-5" id="thumlimage">
            <div class="column">
                <img class="demo cursor" src="/Content/themeV1/img/media/1546c82c87ab72f52bba.jpg" style="width: 100%" onclick="currentSlide(1)">
            </div>
            <div class="column">
                <img class="demo cursor" src="/Content/themeV1/img/News/d4f7a76917ede2b3bbfc.jpg" style="width: 100%" onclick="currentSlide(2)">
            </div>
            <div class="column">
                <img class="demo cursor" src="/Content/themeV1/img/media/Mask-2.png" style="width: 100%" onclick="currentSlide(3)">
            </div>
            <div class="column">
                <img class="demo cursor" src="/Content/themeV1/img/media/Mask-4@2x.png" style="width: 100%" onclick="currentSlide(4)">
            </div>
            <div class="column">
                <img class="demo cursor" src="/Content/themeV1/img/media/Mask-5.png" style="width: 100%" onclick="currentSlide(5)">
            </div>
        </div>
    </div>
</div>
<%} %>
