﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Banner_home.ascx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.Banner_home" %>
<style>
    .carousel-control-prev {
        /*width:10% !important;*/
    }
</style>
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner vinhhq">

        <%foreach (VIPortalAPP.BannerJson item in LstBanner)
            {%>
         <div class="carousel-item img_banner ">
            <img src="<%=item.ColumnImageSrc %>" class="d-block w-100" alt="..." />
        </div>
            <%} %>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous
                                        
        </span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next
                                        
        </span>
    </a>
</div>
<script>
    $(".carousel-item:first").addClass("active");
    $('.owl-carousel').owlCarousel({
        loop: true,
        nav: true,
        dots: false,
        margin: 10,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
           },
            1000: {
                items: 1
            }
        }
    })
</script>
