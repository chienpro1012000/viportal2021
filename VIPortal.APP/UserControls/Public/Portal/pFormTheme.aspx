﻿<%@ page language="C#" autoeventwireup="true" codebehind="pFormTheme.aspx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.pFormTheme" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-FormTheme" class="form-horizontal">
        <div class="form-group row">
            <label for="Bg_Body" class="col-sm-2 control-label">Màu nền</label>
            <div class="col-sm-10">
                <input type="color" id="Bg_Body" name="Bg_Body" value="<%=Theme_Bg_Body.ConfigValue %>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="Bg_Body" class="col-sm-2 control-label">Màu Webpart</label>
            <div class="col-sm-10">
                <input type="color" id="Bg_Webpart" name="Bg_Webpart" value="<%=Theme_Bg_Webpart.ConfigValue %>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="Bg_Body" class="col-sm-2 control-label">Font chữ</label>
            <div class="col-sm-10">
                <select class="form-control" id="font_Body" name="font_Body">
                    <option style="font-family: arial !important;" value="Arial">Arial</option>
                    <option style="font-family: Times New Roman !important;" value="Times New Roman">Times New Roman</option>
                    <option style="font-family: Verdana !important;" value="Verdana">Verdana</option>
                    <option style="font-family: Comic Sans Serif !important;" value="Comic Sans Serif">Comic Sans Serif</option>
                    <option style="font-family: Trebuchet MS !important;" value="Trebuchet MS">Trebuchet MS</option>
                </select>

            </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
        <button type="button" class="btn btn-primary" id="btnUpdate">Cập nhật</button>
            <button type="button" class="btn btn-secondary" id="btnReset">Khôi phục</button>
      </div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#btnUpdate").click(function () {
                CK_jQ();
                $("#frm-FormTheme").submit();
            });
            $("#btnReset").click(function () {
                $.post("/VIPortalAPI/api/LUser/DeleteConfig", {}, function (result) {
                    showMsg(result.Message);
                    $("#frm-FormTheme").closeModal();
                    location.reload();
                });
            });
            $("#font_Body").val("<%=Theme_font_Body.ConfigValue %>");
            $("#frm-FormTheme").validate({
                rules: {

                },
                messages: {
                },
                submitHandler: function (form) {
                    var _Bg_Webpart = $("#Bg_Webpart").val();
                    console.log(_Bg_Webpart);
                    $.post("/VIPortalAPI/api/LUser/UpdateConfig", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            $(form).closeModal();
                            location.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-FormTheme").viForm();
        });
    </script>
</body>
</html>
