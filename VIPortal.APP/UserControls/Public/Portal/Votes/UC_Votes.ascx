﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_Votes.ascx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.Votes.UC_Votes" %>
<%if(ItemID > 0){%>
<script type="text/javascript">
    $(document).ready(function () {
    
        $(".selectDA").on('click', function (event) {
            debugger;
            var dapan = $("#DapAn").val();
            var idA = event.target.id;
            var idQuestion = event.target.dataset.idcauhoi;
            var loaibinhchon = event.target.dataset.loai;

            if (loaibinhchon == 1) {
                //nếu lần đầu lựa chọn của mỗi câu hỏi add vào dapan
                //if (questionOld == "" || !questionOld.includes(idQuestion)) {
                //    questionOld += idQuestion;
                if (!dapan.includes(idA)) {
                    var $body = $(this).closest("[class='check-one-votes']");
                    var selecteditems = [];

                    $body.find('.selectDA').each(function (i, ob) {
                        selecteditems.push($(ob).attr('data-id'));
                    });
                    var lstidcautraloi = selecteditems.toString().split(",");
                    for (var i = 0; i < lstidcautraloi.length; i++) {
                        dapan = dapan.replace("," + lstidcautraloi[i], "");
                        $("#" + lstidcautraloi[i] + "").removeClass("acvtiveChoose");
                    }
                    dapan += "," + idA;
                    $("#" + idA + "").addClass("acvtiveChoose");
                } else if (dapan.includes(idA)) {
                    //nếu lựa chọn 2 lần cùng 1 đáp án
                    dapan = dapan.replace("," + idA, "");
                    $("#" + idA + "").removeClass("acvtiveChoose");
                }
            } else {
                if (!dapan.includes(idA)) {
                    dapan += "," + idA;
                    $("#" + idA + "").addClass("acvtiveChoose");
                } else {
                    dapan = dapan.replace("," + idA, "");
                    $("#" + idA + "").removeClass("acvtiveChoose");
                }
            }
            $("#DapAn").val(dapan);
        });
        var ngaykethuc = '<%=oKSBoChuDeItem.KSDenNgay.Value.ToString("dd/MM/yyyy") %>';
        var currentdate = new Date();
        var datetime = currentdate.getDate() + "/" + (currentdate.getMonth() + 1) + "/" + currentdate.getFullYear();
        
        $("#btnSend").click(function () {
            var dapan = $("#DapAn").val();
            $.post("/UserControls/AdminCMS/KSCauTraLoi/pAction.asp?do=UPDATEBINHCHON" + "&DapAn=" + dapan, { "BoChuDe": "<%=oKSBoChuDeItem.ID%>" }, function (data) {
                if (data.Erros) {
                    BootstrapDialog.show({
                        title: "Lỗi",
                        message: data.Message
                    });
                }
                else {
                    openDialogMsg("Thông báo", data.Message);
                }
            })
        })
    })
</script>
<style>
    .checkone .dap-an {
        cursor: pointer;
        padding: 10px 10px;
        width: 100%;
        border-radius: 10px;
        font-weight: bold;
        outline: none;
        background-color: #F6F9FD;
        text-align: center;
    }

        .checkone .dap-an:hover {
            cursor: pointer;
            padding: 10px 10px;
            width: 100%;
            border-radius: 10px;
            background-color: #F5820F;
            color: #fff;
            font-weight: bold;
        }

    .acvtiveChoose {
        color: #f00 !important;
    }
</style>
<section>
    <div class="vote-detail bg-color-box-items box-shadow-basic Votes">
        <div class="header-notification-detail">
            <p class="font-size-14 color-table-meeting pl-3 pt-2" data-lang="TrangChuBinhChon">
                <span >Trang chủ</span> - <span >Bình chọn</span>
                        
            </p>
        </div>
        <div class="main-vote-detail">
            <div class="row">
                <div class="col-md-6 mb-3">
                    <div class="box-shadow-basic pt-3 pb-3 mb-3 pl-3 pr-3 ml-5">
                        <div class="display-flex">
                            <div class="icon-calendar-vote pr-2 pt-2">
                                <img src="img/icon/calendar (1).png" alt="">
                            </div>
                            <div class="time-votes">
                                <p class="margin-p font-size-10 color-table-meeting">Kết thúc</p>
                                <p class="margin-p font-size-12 color-table-meeting"><%=oKSBoChuDeItem.KSDenNgay.Value.ToString("dd/MM/yyyy") %></p>
                            </div>
                        </div>
                        <div class="img-vote-detail  text-center">
                            <img src="img/icon/Group 5791.png" alt="">
                        </div>

                        <div class="title-vote-detail text-center">
                            <p class="font-size-30 color-table-meeting margin-p"><%=oKSBoChuDeItem.Title %></p>
                            <p class="font-size-14 margin-p">Thông tin, tin tức cập nhập thường ngày</p>
                        </div>
                        <input type="hidden" name="DapAn" id="DapAn" value="" />
                        <input type="hidden" name="QuestionOld" id="QuestionOld" value="" />
                        <%int i=1; %>
                        <%foreach(var item in lstBoCauHoi){%>
                        <div class="pt-3">
                            <div class="check-one-votes">
                                <div class="display-flex item-check-vote">
                                    <span class="mr-3"><%=i %></span>
                                    <p class="pt-1"><%=item.Title %></p>
                                </div>
                                <div class="input-check-votes">
                                    <div class="form-check">
                                        <input type="hidden" id="lstID<%=i %>" value="<%=getListId(item.ID) %>" />
                                        <%foreach(var itemCTL in GetCauTraLoiByID(item.ID)){%>
                                        <div class="checkone pt-3">
                                            <div class="dap-an">
                                                <a class="selectDA" id="<%=itemCTL.ID %>" data-id="<%=itemCTL.ID %>" title="<%=item.Title %>" data-loai="<%=item.LoaiTraLoi %>" data-idcauhoi="<%=i %>">
                                                    <%=itemCTL.Title %> <%if(isHetHan)  { %> (<%=itemCTL.SoLuotBinhChon %> phiếu  ) <%  } %>
                                                </a>
                                            </div>
                                        </div>
                                        <%} %>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%i++; %>
                        <%} %>
                    </div>
                    <div class="btn-votes text-center">
                        <button class="btn-one" <%if(isHetHan) { %> disabled="disabled" <% } %>>Làm lại</button>
                        <button class="btn-two" type="button" <%if(isHetHan) { %> disabled="disabled" <% } %>  id="btnSend" >Gửi khảo sát</button>

                    </div>
                </div>

                <div class="col-md-6">
                </div>

            </div>


        </div>
    </div>
</section>
<%-- optional --%>
<script>
    function dot_01(el) {
        if (el.style.backgroundColor === 'Olive') {
            el.style.backgroundColor = 'Gray';
        } else el.style.backgroundColor = 'Olive';
    }
</script>

<%}else{%>
<script type="text/javascript">
    $(document).ready(function () {
        SetConfigLang('<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>', 'Votes');
        $("#list_tb").smGrid2018({
            "TypeTemp": "kendo",
            "UrlPost": "/VIPortalAPI/api/Vote/QUERYDATA",
            ConfigLanguages: '<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>',
            pageSize: 10,
            odata: { "do": "QUERYDATA" },
            template: "template_new",
            dataBound: function (e) {
            },
        });
    });
</script>
<section class="voted Votes" id="list_tb" data-role="fullGrid">
    <script id="template_new" type="text/x-kendo-template">
             <div class="voted-from-item-col">
                <div class="voted-from-item-col-content">
                    <div class="voted-from-item-col-content-text-tab"></div>
                    <div class="voted-from-item-col-content-text">
                        <a href="<%=UrlSite %>/Pages/voted.aspx?ItemId=#=ID#">#=Title#</a>
                    </div>
                </div>
                <div class="voted-from-item-col-date">
                    <i class="far fa-calendar-alt"></i> #=formatDate(KSTuNgay)#  -  #=formatDate(KSDenNgay)#
                </div>
                #if(TrangThaiText == "Đang diễn ra"){#<div class="voted-from-item-col-active">Đang diễn ra</div>#}else{#<div class="voted-from-item-col-active" style="color: red !important">Đã kết thúc</div>#}#
                
            </div>
        </script>
    <div class="voted-category">
        <div class="lib-category-title-fire"></div>
        <div class="img-category-title-lib1">
            <a href="#" data-lang="TrangChu">Trang chủ</a> - <a href="#" data-lang="BinhChon">bình chọn</a>
        </div>
    </div>
    <div class="voted-form">
        <div class="voted-form-select" >
            <div action="" method="get" id="voted-form" role="search">
                <div class="form-group pr-2">
                    <label for="fkey" data-lang="ThoiGianKhaoSat">Thời gian khảo sát</label>
                    <div class="popup-container">
                        <div class="popup-group" id="pop-1">
                            <input type="text" class="input-datetime" name="KSTuNgay" data-lang="TuNgay" placeholder="Từ ngày">
                        </div>
                        <div class="popup-group" id="pop-2">
                            <input type="text" class="input-datetime" name="KSDenNgay" data-lang="DenNgay" placeholder="Đến ngày">
                        </div>
                    </div>
                </div>

                 <div class="form-group pr-3">
                    <label for="fkey" data-lang="TuKhoa">Từ khóa</label>
                     <div class="key-input-vote">
                         <input type="text" id="keyword" name="keyword" data-lang="PlaceSearch" placeholder="Nhập từ khóa" />
                    <input type="hidden" name="SearchInAdvance" value="Title" />
                     </div>
                    
                </div>
                <div class="icon-button icon-button-vote display-flex">
                     <button data-toggle="tooltip" data-placement="top" title="Xóa lọc tất cả"  class="reset_data"><i class="fas fa-sync-alt"></i></button>
                     <button type="button" id="search" class="btn-voted btnsearch btn-search-meeting" data-lang="TimKiem">Tìm kiếm</button>
                 </div>
                
            </div>
        </div>
        
        <div class="voted-from-item" >
            <div role="grid" class="row">
            </div>
            
        </div>
        <div class="clspaging">
            </div>
    </div>
</section>
<%} %>





