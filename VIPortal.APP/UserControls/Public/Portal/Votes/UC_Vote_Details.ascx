﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_Vote_Details.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.Votes.UC_Vote_Details" %>
<script type="text/javascript">
    $(document).ready(function () {
        SetConfigLang('<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>', 'VoteDetails');
        $(".selectDA").on('click', function (event) {
            
            var dapan = $("#DapAn").val();
            var idA = event.target.id;
            var question = event.target.attributes[2].value;
            var loaibinhchon = event.target.dataset.loai;
            var idCauHoi = event.target.dataset.idcauhoi;
            var questionOld = $("#QuestionOld").val();
            
            if (loaibinhchon == 1) {
                //nếu lần đầu lựa chọn của mỗi câu hỏi add vào dapan
                if (questionOld == "" || !questionOld.includes(question)) {
                    questionOld += question;
                    if (dapan != ""  && !dapan.includes(idA)) {
                        dapan += "," + idA ;
                    }
                    $("#" + idA + "").addClass("acvtiveChoose");
                } else if (questionOld.includes(question) && dapan.includes(idA)) {
                    //nếu lần 2 lựa chọn đáp án cùng 1 câu hỏi
                    //check đáp án thuộc câu hỏi nào

                    alert("a");
                }
            } else {
                if (dapan != "" && !dapan.includes(idA)) {
                    dapan += "," + idA;
                }
                $("#" + idA + "").addClass("acvtiveChoose");
            }
            $("#DapAn").val(dapan);
            $("#QuestionOld").val(questionOld);
        });
        $("#btnSend").click(function () {
            var dapan = $("#DapAn").val();
            $.post("/UserControls/AdminCMS/KSCauTraLoi/pAction.ashx", "&do=UPDATEBINHCHON" + "&DapAn=" + dapan, { "BoChuDe": "<%=oKSBoChuDeItem.ID%>"}, function (data) {
                if (data.Erros) {
                    BootstrapDialog.show({
                        title: "Lỗi",
                        message: data.Message
                    });
                }
                else {
                    openDialogMsg("Thông báo", data.Message);
                }
            })
        })
    })
</script>
<style>
    .checkone .dap-an {
        cursor: pointer;
        padding: 10px 10px;
        width: 100%;
        border-radius: 10px;
        font-weight: bold;
        outline: none;
        background-color: #F6F9FD;
        text-align: center;
    }

        .checkone .dap-an:hover {
            cursor: pointer;
            padding: 10px 10px;
            width: 100%;
            border-radius: 10px;
            background-color: #F5820F;
            color: #fff;
            font-weight: bold;
        }

    .acvtiveChoose {
        color: #f00 !important;
    }
</style>
<section>
    <div class="vote-detail bg-color-box-items box-shadow-basic VoteDetails ">
        <div class="header-notification-detail">
            <p class="font-size-14 color-table-meeting pl-3 pt-2" data-lang="TrangChuBinhChon" >
                Trang chủ - Bình chọn
                           
            </p>
        </div>
        <div class="main-vote-detail">
            <div class="row">
                <div class="col-md-6 mb-3">
                    <div class="box-shadow-basic pt-3 pb-3 mb-3 pl-3 pr-3 ml-5">
                        <div class="display-flex">
                            <div class="icon-calendar-vote pr-2 pt-2">
                                <img src="img/icon/calendar (1).png" alt="">
                            </div>
                            <div class="time-votes">
                                <p class="margin-p font-size-10 color-table-meeting">Kết thúc</p>
                                <p class="margin-p font-size-12 color-table-meeting"><%=oKSBoChuDeItem.KSDenNgay.Value.ToString("dd/MM/yyyy") %></p>
                            </div>
                        </div>
                        <div class="img-vote-detail  text-center">
                            <img src="img/icon/Group 5791.png" alt="">
                        </div>

                        <div class="title-vote-detail text-center">
                            <p class="font-size-30 color-table-meeting margin-p"><%=oKSBoChuDeItem.Title %></p>
                            <p class="font-size-14 margin-p">Thông tin, tin tức cập nhập thường ngày</p>
                        </div>
                        <input type="hidden" name="DapAn" id="DapAn" value="" />
                        <input type="hidden" name="QuestionOld" id="QuestionOld" value="" />
                        <%int i=1; %>
                        <%foreach(var item in lstBoCauHoi){%>
                        <div class="pt-3">
                            <div class="check-one-votes">
                                <div class="display-flex item-check-vote">
                                    <span class="mr-3"><%=i %></span>
                                    <p class="pt-1"><%=item.Title %></p>
                                </div>
                                <div class="input-check-votes">
                                    <div class="form-check">
                                        <input type="hidden" id="lstID<%=i %>" value="<%=getListId(item.ID) %>" />
                                        <%foreach(var itemCTL in GetCauTraLoiByID(item.ID)){%>
                                        <div class="checkone pt-3">
                                            <div class="dap-an">
                                                <a class="selectDA" id="<%=itemCTL.ID %>" title="<%=item.Title %>" data-loai="<%=item.LoaiTraLoi %>" data-idCauHoi="<%=item.ID %>">
                                                    <%=itemCTL.Title %>
                                                </a>
                                            </div>
                                        </div>
                                        <%} %>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%i++; %>
                        <%} %>
                    </div>
                    <div class="btn-votes text-center">
                        <button class="btn-one">Làm lại</button>
                        <button class="btn-two" type="button" id="btnSend">Gửi khảo sát</button>

                    </div>
                </div>

                <div class="col-md-6">
                </div>

            </div>


        </div>
    </div>
</section>
<%-- optional --%>
<script>
    function dot_01(el) {
        if (el.style.backgroundColor === 'Olive') {
            el.style.backgroundColor = 'Gray';
        } else el.style.backgroundColor = 'Olive';
    }
</script>
