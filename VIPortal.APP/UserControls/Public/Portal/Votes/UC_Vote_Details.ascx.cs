﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData.KSBoCauHoi;
using ViPortalData.KSBoChuDe;
using ViPortalData.KSCauTraLoi;

namespace VIPortal.APP.UserControls.Public.Portal.Votes
{
    public partial class UC_Vote_Details : BaseUC_Web
    {
        public string lstIDCauTl { get; set; }
        public KSBoChuDeItem oKSBoChuDeItem { get; set; }
        public List<KSBoCauHoiJson> lstBoCauHoi { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            KSBoCauHoiDA oKSBoCauHoi = new KSBoCauHoiDA(UrlSite);
            KSBoChuDeDA oKSBoChuDeDA = new KSBoChuDeDA(UrlSite);
            oKSBoChuDeItem = oKSBoChuDeDA.GetByIdToObject<KSBoChuDeItem>(ItemID);
            lstBoCauHoi = oKSBoCauHoi.GetListJson(new KSBoCauHoiQuery() { IdChuDe = ItemID, _ModerationStatus=1 });
        }
        public static List<KSCauTraLoiJson> GetCauTraLoiByID(int ItemID)
        {
            string lstIDCauTl = "";
            List<KSCauTraLoiJson> lstCauTraLoi = new List<KSCauTraLoiJson>();
            KSCauTraLoiDA objCauTraLoi = new KSCauTraLoiDA(true);
            lstCauTraLoi = objCauTraLoi.GetListJson(new KSCauTraLoiQuery() { IDCauHoi = ItemID });
            foreach (var item in lstCauTraLoi)
            {
                lstIDCauTl = lstIDCauTl += item.ID;
            }
            return lstCauTraLoi;
        }
        public static string getListId(int ItemID)
        {
            string lstIDCauTl = "";
            List<KSCauTraLoiJson> lstCauTraLoi = new List<KSCauTraLoiJson>();
            KSCauTraLoiDA objCauTraLoi = new KSCauTraLoiDA(true);
            lstCauTraLoi = objCauTraLoi.GetListJson(new KSCauTraLoiQuery() { IDCauHoi = ItemID });
            foreach (var item in lstCauTraLoi)
            {
                if (lstIDCauTl == "")
                {
                    lstIDCauTl += item.ID;
                }
                else
                {
                    lstIDCauTl += "," + item.ID;
                }
                
            }
            return lstIDCauTl;
        }
    }
}