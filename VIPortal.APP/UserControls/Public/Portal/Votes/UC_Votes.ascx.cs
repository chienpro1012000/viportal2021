﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData.KSBoCauHoi;
using ViPortalData.KSBoChuDe;
using ViPortalData.KSCauTraLoi;

namespace VIPortal.APP.UserControls.Public.Portal.Votes
{
    public partial class UC_Votes : BaseUC_Web
    {
        public string lstIDCauTl { get; set; }
        public bool isHetHan { get; set; }
        public KSBoChuDeItem oKSBoChuDeItem { get; set; }
        public List<KSBoCauHoiJson> lstBoCauHoi { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            KSBoCauHoiDA oKSBoCauHoi = new KSBoCauHoiDA(UrlSite);
            KSBoChuDeDA oKSBoChuDeDA = new KSBoChuDeDA(UrlSite);
            if (ItemID > 0)
            {
                oKSBoChuDeItem = oKSBoChuDeDA.GetByIdToObject<KSBoChuDeItem>(ItemID);
                if(oKSBoChuDeItem.KSDenNgay.HasValue && oKSBoChuDeItem.KSDenNgay.Value.Date < DateTime.Now.Date || oKSBoChuDeItem.KSTuNgay.HasValue && oKSBoChuDeItem.KSTuNgay.Value.Date > DateTime.Now.Date)
                {
                    isHetHan = true;
                }
                lstBoCauHoi = oKSBoCauHoi.GetListJson(new KSBoCauHoiQuery() { IdChuDe = ItemID, _ModerationStatus = 1 });
            }
        }
        public List<KSCauTraLoiJson> GetCauTraLoiByID(int ItemID)
        {
            string lstIDCauTl = "";
            List<KSCauTraLoiJson> lstCauTraLoi = new List<KSCauTraLoiJson>();
            KSCauTraLoiDA objCauTraLoi = new KSCauTraLoiDA(UrlSite);
            lstCauTraLoi = objCauTraLoi.GetListJson(new KSCauTraLoiQuery() { IDCauHoi = ItemID });
            foreach (var item in lstCauTraLoi)
            {
                lstIDCauTl = lstIDCauTl += item.ID;
            }
            return lstCauTraLoi;
        }
        public string getListId(int ItemID)
        {
            string lstIDCauTl = "";
            List<KSCauTraLoiJson> lstCauTraLoi = new List<KSCauTraLoiJson>();
            KSCauTraLoiDA objCauTraLoi = new KSCauTraLoiDA(UrlSite);
            lstCauTraLoi = objCauTraLoi.GetListJson(new KSCauTraLoiQuery() { IDCauHoi = ItemID });
            foreach (var item in lstCauTraLoi)
            {
                if (lstIDCauTl == "")
                {
                    lstIDCauTl += item.ID;
                }
                else
                {
                    lstIDCauTl += "," + item.ID;
                }

            }
            return lstIDCauTl;
        }
    }
}