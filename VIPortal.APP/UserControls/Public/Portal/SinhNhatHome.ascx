﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SinhNhatHome.ascx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.SinhNhatHome" %>

<style>
    .item-BdHome {
        border-radius: 5px;
        box-shadow: rgb(149 157 165 / 20%) 0px 8px 24px;
        padding-bottom: 15px;
        padding-top: 10px;
    }

    .title-top-birtday1 {
        padding: 0 15px;
    }

    .calendaBirtday {
        padding: 0 15px;
    }

    .image-userBdHome img {
        width: 80px;
    height: 110px;
    object-fit: cover;
    }

    .nameBdHome {
        overflow: hidden;
        text-overflow: ellipsis;
        -webkit-line-clamp: 1;
        -webkit-box-orient: vertical;
        display: -webkit-box;
    }

    #birthday a {
        background-color: #fff !important;
    }

    .margin-p {
        margin: 0;
        padding: 0;
    }

    .font-size-13 {
        font-size: 13px;
    }

    .font-size-14 {
        font-size: 14px;
    }

    .font-size-20 {
        font-size: 20px;
    }

    .display-flex {
        display: flex;
        flex-wrap: wrap;
    }

    .nav-tabs .nav-link {
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        border: none;
        color: #134298 !important;
        font-size: 14px;
        font-weight: 600;
        padding: 0.5rem 1.5rem;
    }

        .nav-tabs .nav-link.active {
            background: #f8f8f8;
            position: relative;
            display: block;
            border-bottom: 3px solid #fff;
            color: #ee1d23 !important;
        }

            .nav-tabs .nav-link.active:after {
                content: "";
                background-image: -moz-linear-gradient( 0deg, rgb(21,67,152) 0%, rgb(238,29,35) 100%);
                background-image: -webkit-linear-gradient( 0deg, rgb(21,67,152) 0%, rgb(238,29,35) 100%);
                background-image: -ms-linear-gradient( 0deg, rgb(21,67,152) 0%, rgb(238,29,35) 100%);
                display: block;
                height: 3px;
                width: 100%;
                position: absolute;
                bottom: -3px;
                left: 0;
            }

    .owl-theme .owl-nav [class*=owl-]:hover {
        background: none !important;
        color: #FFF;
        text-decoration: none;
    }

    .owl-carousel .owl-nav button.owl-next,
    .owl-carousel .owl-nav button.owl-prev,
    .owl-carousel button.owl-dot {
        background: 0 0;
        color: #ffffff !important;
        border: none;
        padding: 0 !important;
        font: inherit;
    }

    button:focus {
        outline: none !important;
    }

    button.owl-next {
        position: absolute;
        right: 20px;
    }

    button.owl-prev {
        position: absolute;
        left: 20px;
    }

    .header_birday {
        border-left: 4px solid red;
        padding-left: 10px;
        padding-top: 5px;
        padding-bottom: 5px;
    }

    .img_birtday {
        position: relative;
    }

        .img_birtday img {
            position: absolute;
            width: 85px;
            top: -8px;
            left: -33px;
            border-radius: 50%;
        }

    .title-top-birtday {
        display: flex;
        justify-content: space-between;
        text-align: inherit;
    }

    .color-table-meeting {
        color: #173f90;
    }

    .color-title-FAQ {
        color: #ee1d23;
    }

    .display-flex-righteous {
        display: flex;
        justify-content: space-between;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $("#lstSinhNhat").smGrid2018({
            "TypeTemp": "kendo",
            "UrlPost": "/VIPortalAPI/api/LUser/GETDATA_SINHNHAT",
            pageSize: 10,
            odata: { "ID_DONVI": '<%=oLGroupItem.OldID %>', "length" : 4},
            template: "jsTemplateSinhNhat",
            dataBound: function (e) {
            },
        });

        $("#lstSinhNhatCBNV").smGrid2018({
            "TypeTemp": "kendo",
            "UrlPost": "/VIPortalAPI/api/LUser/GETDATA_SINHNHAT_CBNV",
            pageSize: 10,
            odata: { "ID_DONVI": '<%=MaDonViSelected %>', "PhongBan": '<%=MaPhongBanSelected %>', "length": 4 },
            template: "jsTemplateSinhNhatCBNV",
            dataBound: function (e) {
            },
        });
    });
</script>

<%if(!string.IsNullOrEmpty(HtmlOutput)){ %>
<%=HtmlOutput %>
<%}else{ %>
<div class="birday-index bg-color-box-items box-shadow-basic mt-3">
    <div class="header-birday-index">
        <div class="items-header-birtday-index display-flex">
            <p class="margin-p">
                <img src="/Content/themeV1/icon/birthday-cake.png" alt="">
            </p>
            <p class="pl-3 margin-p font-size-20 font-weight-bold color-table-meeting">
                <a class="birthday" href="<%=UrlSite %>/Pages/ngaysinh.aspx">Sinh nhật sắp diễn ra</a>
            </p>
        </div>
    </div>
    <div class="mt-4 position-relative">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#lstSinhNhat">BAN LÃNH ĐẠO</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#lstSinhNhatCBNV">CBCNV</a>
            </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content mt-0">
            <div id="lstSinhNhat" class="container-fluid tab-pane active">                
                <div class="row" role="grid">
                    
                </div>
            </div>

            <div id="lstSinhNhatCBNV" class="container-fluid tab-pane fade">                
                <div class="row" role="grid">
                </div>
            </div>
        </div>
        <script id="jsTemplateSinhNhat" type="text/x-kendo-template">
        <div class="col-md-3">
                        <div class="item-BdHome mb-5">
                            <div class="image-userBdHome text-center pt-3">
                                <img src="#=ImageAvatar#" alt="">
                            </div>
                            <div class="title-top-birtday1 display-flex-righteous">
                                <div class="pt-2">
                                    <p class="margin-p font-size-13 font-weight-bold">#=TENKHAISINH#</p>
                                    <p class="margin-p font-size12 nameBdHome">#=TEN_DONVI#</p>
                                </div>
                                <div class="iconRightBd pt-3">
                                    <span><i class="far fa-comment-dots"></i></span>
                                </div>
                            </div>
                            <div class="calendaBirtday">
                                <p class="margin-p font-size-12">
                                    <span class="icon-calen"><i class="fas fa-birthday-cake"></i></span><span class="pl-2">#=formatDateDDMM(BIRTHDAY)#</span>
                                </p>
                            </div>
                        </div>
          </div>
        </script>
        <script id="jsTemplateSinhNhatCBNV" type="text/x-kendo-template">
        <div class="col-md-3">
                        <div class="item-BdHome mb-5">
                            <div class="image-userBdHome text-center pt-3">
                                <img src="#=ImageAvatar#" alt="">
                            </div>
                            <div class="title-top-birtday1 display-flex-righteous">
                                <div class="pt-2">
                                    <p class="margin-p font-size-13 font-weight-bold">#=tenkhaisinh#</p>
                                    <p class="margin-p font-size12 nameBdHome">#=DEPARTMENT_NAME#</p>
                                </div>
                                <div class="iconRightBd pt-3">
                                    <span><i class="far fa-comment-dots"></i></span>
                                </div>
                            </div>
                            <div class="calendaBirtday">
                                <p class="margin-p font-size-12">
                                    <span class="icon-calen"><i class="fas fa-birthday-cake"></i></span><span class="pl-2">#=formatDateDDMM(ngaysinh)#</span>
                                </p>
                            </div>
                        </div>
           </div>     
        </script>
    </div>
</div>
<%} %>
