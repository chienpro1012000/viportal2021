﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_Login.ascx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.Login.UC_Login" %>

    <div class="header_login">


        <select name="" id="gender" class="my-select">

            <option data-thumbnail="img_login/background/vietnam.png" value="tiengviet">Tiếng việt</option>
            <option data-thumbnail="img_login/background/vietnam.png" value="tienganh">Tiếng anh</option>
        </select>

    </div>

    <div class="main_login ">
        <div class="form_login text-center">
            <div class="logo-from-login">
                <img src="img/logo/logo_xanh_ngang.png" alt="">
            </div>
            <div class="title_from_login">
                <h6 class="font-size-18">Đăng nhập EVN Hà Nội</h6>
                <p class="font-size-secondary-2 pt-2">Vui lòng sử dụng tài khoản EVN Hà Nội để đăng nhập</p>
            </div>
            <div class="content_form pt-2">
                <form action="" method="get">
                    <div class="form-group form-login-evn">
                        <input type="text" name="" id="" class="form-control" placeholder=" Tên đăng nhập ..." aria-describedby="helpId">
                        <div class="form_icon">
                            <i class="fas fa-user"></i>
                        </div>
                    </div>
                    <div class="form-group form-login-evn">
                        <input type="text" name="" id="" class="form-control" placeholder="Mật khẩu ..." aria-describedby="helpId">
                        <div class="form_icon">
                            <i class="fas fa-lock"></i>
                        </div>
                    </div>
                    <div class="submit_form font-size-16 box-shadow-basic">
                        <a href="" class="submit">Đăng nhập</a>
                    </div>
                </form>
            </div>
        </div>
        <div class="img_login_right">
            <img src="img_login/background/1 (1).png" alt="">

        </div>
    </div>

