﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_Questions.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.Questions.UC_Questions" %>

<section class="answer pb-3 QuestionsUC">
    <div class="header_FAQ-1 ">
        <div class="header_answer box-ask-reply display-flex-righteous mt-3 ">
            <div class="">
                <p class="margin-p color-title-FAQ title_answer" data-lang="HoiDap">Hỏi đáp FAQ</p>
            </div>
        </div>
        <div class=" main-form-FAQ">
            <div id="formGuiCauHoi">
                <div class="row">
                    <div class="col-md-7 input_form">
                        <input type="text" name="Title" id="Title" class="form-control"  data-lang="Title" placeholder="Nhập tiêu đề câu hỏi... ">
                    </div>
                    <div class="col-md-5 input_form">
                        <select data-selected="" class="form-control" data-url="/UserControls/AdminCMS/DMLinhVuc/pAction.asp?do=QUERYDATA&moder=1" data-lang="Select" data-place="Chọn Lĩnh vực" name="FAQLinhVuc" id="FAQLinhVuc"></select>
                    </div>
                </div>
                <div class="content_form">
                    <textarea class="form-control" id="FAQCauHoi" name="FAQCauHoi" rows="3" data-lang="Content" placeholder="Nhập nội dung câu hỏi ..."></textarea>
                </div>
            </div>
        </div>
        <div class="btn_submit">
            <a href="javascript:;" id="btndelete" class="delete font-size-14" data-lang="CancelCreate">Hủy tạo</a>
            <a href="javascript:;" id="btnSendCauHoi" class="send font-size-14" data-lang="Submit">Gửi câu hỏi</a>
        </div>


    </div>

    <div class="header_answer box-ask-reply display-flex-righteous mt-3 box-shadow-basic">
        <div class="title_answer">
            <p class="margin-p color-title-FAQ" data-lang="Frequently">Danh sách câu hỏi thường xuyên</p>
        </div>
    </div>
    <div class="accordion mt-3 box-shadow-basic" id="accordionExample1">
        <script id="template_hoidap" type="text/x-kendo-template">
                             <div class="list">
                <div class="list_question" id="heading2">
                    <p class="ask-item font-size-14 " style="font-weight: 500;">
                        <img src="/Content/themeV1/img_FAQ/icon/question.png" alt=""></i>&nbsp;&nbsp;&nbsp; #=Title#
                        <br>
                        <span class="font-size-14 pl-4" style="opacity: 0.6;">#=FAQCauHoi#</span>
                    </p>
                    <p class="btn btn-link collapsed font-size-12" type="button" data-toggle="collapse" data-target="\#collapse#=ID#" aria-expanded="false" aria-controls="collapse#=ID#">
                        <span>Xem câu trả lời</span>&nbsp;<img src="/Content/themeV1/img_FAQ/icon/Polygon 3.png" class="img_FAQ" alt="">
                    </p>
                </div>
                <div id="collapse#=ID#" class="collapse" aria-labelledby="heading2" data-parent="\#accordionExample1">
                    <div class="reply-FAQ card-body">
                        <img src="/Content/themeV1/img_FAQ/icon/Solid.png" alt="">
                        <img src="/Content/themeV1/img_FAQ/icon/user-avatar-with-check-mark.png" alt="">
                        <span class="color-title-FAQ">Quản trị viên</span>
                        <p class="rep-ply rep-plyfaq pl-5 font-size-14" style="color: \#194398;">
                           #=SubStringShort(extractContent(FAQNoiDungTraLoi), 150)#....
                                            <a href="javascript:;" class="font-size-12 readmore" style="color: \#FB0EEB;">Xem tiếp >></a>

                         <div class="rep-ply rep-plyfaq pl-5 font-size-14" style="color: \#194398; display: none;">
                           #=FAQNoiDungTraLoi#
                                            <a href="javascript:;" class="font-size-12 readmore" style="color: \#FB0EEB;">Thu nhỏ >></a>
                        </div>
                    </div>
                </div>
            </div>
        </script>

        <div id="list_FAQ" data-role="fullGrid" data-urllist="TinNoiBo">
            <div class="box-wrap-search-page -style01" style="padding-right: 0px !important">
                <div role="search" class="box-search -adv row" id="frmsearchlist_FAQ">
                </div>
            </div>
            <div role="grid" class="">
            </div>
            <div class="clspaging">
            </div>
        </div>
    </div>

    <!--Câu hỏi của người dùng-->
    <div class="header_answer box-ask-reply display-flex-righteous mt-3 box-shadow-basic">
        <div class="title_answer">
            <p class="margin-p color-title-FAQ" data-lang="Asked">Danh sách câu hỏi đã đặt</p>
        </div>
    </div>
    <div class="accordion mt-3 box-shadow-basic" id="accordionExample">

        <div class="list">
            <script id="template_hoidap_gui" type="text/x-kendo-template">
            <div class="list_question" id="heading2">
               <div>
                  <div class="ask-item font-size-14 " style="font-weight: 500;">
                     <img src="/Content/themeV1/img_FAQ/icon/question.png" alt=""></i>&nbsp;&nbsp;&nbsp; #=Title#
                  </div>
                  <div class="font-size-14 pl-4" style="opacity: 0.6;">#=FAQCauHoi#</div>
               </div>
               #if(TieuDeTraLoi != null && TieuDeTraLoi != "") {#
               <p data-traloi="#=FQNguoiTraLoi.ID#" class="btn btn-link collapsed font-size-12 linkxemtraloi" type="button" data-toggle="collapse" data-target="\#collapsehoidap#=ID#" aria-expanded="false" aria-controls="collapsehoidap#=ID#">
                  <span> Xem câu trả lời </span>&nbsp;<img src="/Content/themeV1/img_FAQ/icon/Polygon 3.png" class="img_FAQ" alt="">
               </p>
               #}#
            </div>
            <div id="collapsehoidap#=ID#" class="collapse" aria-labelledby="heading2" data-parent="\#accordionExample">
               <div class="reply-FAQ card-body">
                  <img src="/Content/themeV1/img_FAQ/icon/Solid.png" alt="">
                  <img src="/Content/themeV1/img_FAQ/icon/user-avatar-with-check-mark.png" alt="">
                  <span class="color-title-FAQ">#=FQNguoiTraLoi.Title#</span>
                  <div class="rep-ply rep-plyhoidap pl-5 font-size-14" style="color: \#194398;">
                     <div>  #=SubStringShort(TieuDeTraLoi, 150)#</div>
                     <a href="javascript:;" class="font-size-12 readmorehoidap" style="color: \#FB0EEB;">
                        Xem tiếp >> </a>
                  </div>
                  <div class="rep-ply rep-plyhoidap pl-5 font-size-14" style="color: \#194398; display: none;">
                  <div>  #=NoiDungTraLoi#</div>
                  <a href="javascript:;" class="font-size-12 readmorehoidap" style="color: \#FB0EEB;">Thu nhỏ >></a>
                  </div>
               </div>
            </div>
            </script>
            <div id="list_CauHoi" data-role="fullGrid" data-urllist="TinNoiBo">
                <div class="box-wrap-search-page -style01" style="padding-right: 0px !important">
                    <div role="search" class="box-search -adv row" id="frmsearchlist_CauHoi">
                        <input type="hidden"  name="FQNguoiHoi"  id="FQNguoiHoi" value="<%=CurentUser.ID %>"/>
                    </div>
                </div>
                <div role="grid" class="">
                </div>
                <div class="clspaging">
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    function extractContent(s) {
        var span = document.createElement('span');
        span.innerHTML = s;
        return span.innerText;
    };
    $(document).ready(function () {
        SetConfigLang('<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>', 'QuestionsUC');
        $("#FAQLinhVuc").smSelect2018V2({
            dropdownParent: "#formGuiCauHoi"
        });
        $("#btndelete").click(function () {
            $("#formGuiCauHoi").clear_form_elements();
        });
        $("#btnSendCauHoi").click(function () {
            if ($("#Title").val() == '' || $("#FAQCauHoi").val() == '') {
                BootstrapDialog.show({
                    title: "Lỗi",
                    message: "Nhập tiêu đề và nội dung câu hỏi"
                });
                return false;
            }
            $.post("/VIPortalAPI/api/HoiDap/GuiCauHoi", $("#formGuiCauHoi").viSerializeKhaiThac(), function (result) {
                if (result.State == 2) {
                    BootstrapDialog.show({
                        title: "Lỗi",
                        message: result.Message
                    });
                }
                else {
                    showMsg(result.Message);
                    $("#formGuiCauHoi").clear_form_elements();
                }
            }).always(function () { });
        });

        $("#list_CauHoi").smGrid2018({
            "TypeTemp": "kendo",
            "UrlPost": "/VIPortalAPI/api/HoiDap/QUERYDATA",
            ConfigLanguages: '<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>',
            pageSize: 20,
            odata: { "do": "QUERYDATA" },
            template: "template_hoidap_gui",
            dataBound: function (e) {
                $(".readmorehoidap").click(function () {
                    //$(".rep-plyhoidap").show();
                    $(this).parent().parent().find('.rep-plyhoidap').show();
                    $(this).parent().hide();
                });
            },
        });

        $("#list_FAQ").smGrid2018({
            "TypeTemp": "kendo",
            "UrlPost": "/VIPortalAPI/api/FAQThuongGap/QUERYDATA",
            ConfigLanguages: '<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>',
            pageSize: 20,
            odata: { "do": "QUERYDATA" },
            template: "template_hoidap",
            dataBound: function (e) {
                $(".readmore").click(function () {
                    //$(".rep-plyfaq").show();
                    $(this).parent().parent().find('.rep-plyfaq').show();
                    $(this).parent().hide();
                });
            },
        });

    });
</script>
