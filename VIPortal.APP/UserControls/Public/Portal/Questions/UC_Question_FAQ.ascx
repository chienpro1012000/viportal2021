﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_Question_FAQ.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.Questions.UC_Question_FAQ" %>

<section class="answer pb-3">

                    <div class="header_FAQ-1">
                        <div class="header_answer box-ask-reply display-flex-righteous mt-3 ">
                            <div class="">
                                <p class="margin-p color-title-FAQ title_answer">Hỏi đáp FAQ</p>
                            </div>
                        </div>
                        <div class=" main-form-FAQ">
                            <form action="" method="post">
                                <div class="row">
                                    <div class="col-md-7 input_form ">
                                        <input type="text" name="" id="" class="form-control" placeholder="Nhập tiêu đề câu hỏi... ">
                                    </div>
                                    <div class="col-md-5 input_form pt-3">
                                        <input type="text" name="" id="" class="form-control" placeholder="Nhập lĩnh mục hỏi đáp...">
                                    </div>
                                </div>
                                <div class="content_form">
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Nhập nội dung câu hỏi ..."></textarea>
                                </div>
                            </form>
                        </div>
                        <div class="btn_submit">
                            <a href="" class="delete font-size-14">Hủy tạo</a>
                            <a href="" class="send font-size-14">Gửi câu hỏi</a>
                        </div>

                    <div class="header_answer box-ask-reply display-flex-righteous mt-3 box-shadow-basic">
                        <div class="title_answer">
                            <p class="margin-p color-title-FAQ">Danh sách câu hỏi thường xuyên hỏi</p>
                        </div>
                    </div>
                    <div class="accordion mt-3 box-shadow-basic" id="accordionExample1">
                        <div class="list">
                            <div class="list_question" id="heading1">
                                <p class="ask-item font-size-14" style="font-weight: 500;"><img src="img_FAQ/icon/question.png" alt="">&nbsp;&nbsp;&nbsp; Không ăn trưa ở công ty thì có thể nhận được tiền no <br>
                                    <span class="font-size-14 pl-4" style="opacity: 0.6;">Em muốn hỏi
                                        chế độ bảo hiểm của công ty như thế nào ạ. Cám ơn mọi
                                        người</span>
                                </p>
                                <p class="btn btn-link collapsed font-size-12" type="button" data-toggle="collapse" data-target="#collapse1" aria-expanded="false" aria-controls="collapse1">
                                    <span>xem câu trả lời</span>&nbsp;<img src="img_FAQ/icon/Polygon 3.png" class="img_FAQ" alt="">
                                </p>
                            </div>
                            <div id="collapse1" class="collapse" aria-labelledby="heading1" data-parent="#accordionExample1">
                                <div class="reply-FAQ card-body">
                                    <img src="img_FAQ/icon/Solid.png" alt="">
                                    <img src="img_FAQ/icon/user-avatar-with-check-mark.png" alt="">
                                    <span class="color-title-FAQ">Quản trị viên</span>
                                    <p class="rep-ply pl-5 font-size-14" style="color:#194398;">Lao động nữ sinh con được nghỉ việc hưởng chế độ thai sản trước và sau khi sinh con là 06 tháng. Trường hợp lao động nữ sinh đôi trở lên thì tính từ con thứ hai trở đi, cứ mỗi con, người mẹ được nghỉ thêm 01 tháng -
                                        Thời gian nghỉ hưởng chế độ thai sản trước khi sinh tối đa không quá 02 tháng.
                                        <a href="/" class="font-size-12" style="color:#FB0EEB ;">Xem
                                            tiếp
                                            &gt;&gt;</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="next_ask text-center">
                            <a href="/" class="font-size-12" style="color:#0E5CFB ;">Xem thêm tất cả <br>
                                <img src="img_FAQ/icon/Group 5810.png" alt=""></a>
                        </div>
                    </div>
                </section>