﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_Rules.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.Rules.UC_Rules" %>
<style type="text/css">
    .regul-form .input-datetime {
        background: url('/Content/dist/img/calendar_bg.png') no-repeat;
        background-position: right 10px center;
        padding-right: 25px;
    }
</style>
<section class="regul mt-3 Rules" id="lstvbtl" data-role="fullGrid">
    <script id="jsTemplateVBTL1" type="text/x-kendo-template">
                    <tr class="font-size-14px">
                        <td class="font-weight-bold color-table-meeting">#=STT#</td>
                        <td style="text-align:left">#=Title#</td>
                        <td>#=QCSoKyHieu#</td>
                        <td>#=formatDate(QCNgayDang)#</td>
                        <td>#=formatDate(QCNgayVanBan)#</td>
                        <td>#if(QCHieuLuc == 1){#Có hiệu lực#}else{#Hết hiệu lực#}#</td>
                        <td style="text-align:left">#if(QCTrichYeu.length > 200){##=QCTrichYeu.substring(0,200)#...#}else{##=QCTrichYeu##}#</td>                
                        <td>#=DMQuyChe.Title#</td>                        
                        <td><a href="<%=UrlSite %>/Pages/regulation-detail.aspx?ItemID=#=ID#">Chi tiết</a></td>                        
                    </tr>
    </script>

    <div class="voted-category">
        <div class="lib-category-title-fire"></div>
        <div class="img-category-title-lib1">
            <a href="#" data-lang="TrangChu">Trang chủ</a> - <a href="#" data-lang="QuyChe">Quy chế nội bộ</a>
        </div>
    </div>
    <div class="regul-form">
        <div class="regul-form-select">
            <div action="" method="get" id="reg-form" role="search">
                <div class="form-group pr-2 form-rules-input">
                    <label for="fkey" data-lang="SoKyHieu">Số ký hiệu</label>
                    <br/>
                    <input type="text" id="QCSoKyHieu" name="QCSoKyHieu" data-lang="SoKyHieu" placeholder="Nhập số ký hiệu" />
                </div>
                <div class="form-group pr-3">
                    <label for="fkey" data-lang="ThoiGianDang">Thời gian đăng</label>
                    <br/>
                    <div class="popup-container">
                        <div class="popup-group" id="pop-1">
                            <input type="text" class="input-datetime" name="TuNgay_QCNgayDang" id="TuNgay_QCNgayDang" data-lang="TuNgay" placeholder="Từ ngày">
                        </div>
                        <div class="popup-group" id="pop-2">
                            <input type="text" class="input-datetime" name="DenNgay_QCNgayDang" id="DenNgay_QCNgayDang" data-lang="DenNgay" placeholder="Đến ngày">
                        </div>
                    </div>
                </div>
                <div class="form-group form-rules-input pr-3">
                    <label for="fkey" data-lang="TuKhoa">Từ khóa</label>
                    <br/>
                    <input type="text" id="keyword" name="keyword" data-lang="NhapTuKhoa" placeholder="Nhập từ khóa" />
                    <input type="hidden" name="SearchInAdvance" value="Titles,QCSoKyHieu,QCTrichYeu" />
                    <input type="hidden" name="HasDMQuyChe" value="0" />
                </div>
                 <div class="form-group form-rules-input pr-1" style="width:300px;" >
                    <label for="fkey" data-lang="Category">Danh mục</label>
                    <br/>
                    <select data-selected="" data-url="/UserControls/AdminCMS/DanhMucQuyChe/pAction.asp?do=QUERYDATA" name="QC_DMQuyChe" id="DMQuyCheSearch" data-place ="Chọn danh mục" class="form-control">
                </select>
                </div>
                <div class="icon-button icon-button-vote display-flex pl-2">
                     <button data-toggle="tooltip" data-placement="top" title="Xóa lọc tất cả"  class="reset_data"><i class="fas fa-sync-alt"></i></button>
                     <button type="button"  class="btn-voted btnsearch btn-search-meeting" style="width:80px;" data-lang="TimKiem">Tìm kiếm</button>
                </div>
               
            </div>
        </div>
    </div>
    <div class="regul-content">
        <div class="main-calendar-datepicke mt-3" style="background-color: #fff">
            <div id="demo-regul">
                <table class="table table-bordered text-center">
                    <thead>
                        <tr class="color-table-meeting font-size-16 font-weight-bold text-center bg-color-header-meeting-tr">
                            <th>STT</th>
                            <th style="width: 20%;" data-lang="TenQuyChe">Tên quy chế</th>
                            <th data-lang="SoKyHieu">Số kí hiệu</th>
                            <th data-lang="PostingDay">Ngày đăng</th>
                            <th data-lang="TextDate">Ngày văn bản</th>
                            <th data-lang="Effect">Hiệu lực</th>
                            <th data-lang="TrichYeu" style="width: 20%;">Trích yếu</th>
                            <th data-lang="Category">Danh mục</th>
                            <th data-lang="Detail">Chi tiết</th>
                        </tr>
                    </thead>
                    <tbody role="grid">
                        <tr>
                        </tr>
                    </tbody>
                </table>
                <div class="clspaging"></div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        SetConfigLang('<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>', 'Rules');
        //DMQuyCheSearch
        $("#DMQuyCheSearch").smSelect2018V2({
            dropdownParent: ".regul-form"
        });
        $("#lstvbtl").smGrid2018({
            "TypeTemp": "kendo",
            "UrlPost": "/VIPortalAPI/api/QuyCheNoiBo/QUERYDATA",
            ConfigLanguages: '<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>',
            pageSize: 20,
            hasSTT: true,
            odata: { "do": "QUERYDATA" },
            template: "jsTemplateVBTL1",
            dataBound: function (e) {
            },
        });
        $("#reg-form").SMValidate({
            rules: {
                TuNgay_QCNgayDang: "date",
                DenNgay_QCNgayDang: { greaterThan: "#TuNgay_QCNgayDang", dateFormat: true }
            }
        });
    });
</script>
