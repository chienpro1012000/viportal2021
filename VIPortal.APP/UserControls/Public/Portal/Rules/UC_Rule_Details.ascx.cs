﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData.QuyCheNoiBo;

namespace VIPortal.APP.UserControls.Public.Portal.Rules
{
    public partial class UC_Rule_Details : BaseUC_Web
    {
        public QuyCheNoiBoItem oQuyChe { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            QuyCheNoiBoDA oQuyCheDA = new QuyCheNoiBoDA(UrlSite);
            if (ItemID > 0)
            {
                oQuyChe = oQuyCheDA.GetByIdToObject<QuyCheNoiBoItem>(ItemID);
            }
        }
    }
}