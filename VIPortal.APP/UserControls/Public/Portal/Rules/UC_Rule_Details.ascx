﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_Rule_Details.ascx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.Rules.UC_Rule_Details" %>
<style>
    .notification-detail table tr .th {
        width: 150px;
        font-weight: 600;
        text-align: right
    }
    .notification-detail ul{
        padding:0;
    }
    .notification-detail ul li{
        list-style-type:none;
    }
</style>
<section>
    
    <div class="notification-detail bg-color-box-items box-shadow-basic">
        <div class="header-notification-detail">
            <p class="font-size-14 color-table-meeting pl-3 pt-2"><a href="<%=UrlSite %>/Pages/HomePortal.aspx">Trang chủ</a> - <a href="<%=UrlSite %>/Pages/regulation.aspx">Quy chế nội bộ</a>  - <a href="#">Chi tiết quy chế</a></p>
        </div>
        <%if(ItemID > 0){%>
        <div class="main-notification-detail display-flex pl-3 pr-3">
            <table class="table table-bordered">
                <tr>
                    <td class="th">Số ký hiệu</td>
                    <td><%=oQuyChe.QCSoKyHieu %></td>
                </tr>
                <tr>
                    <td class="th">Tên quy chế</td>
                    <td><%=oQuyChe.Title %></td>
                </tr>
                <tr>
                    <td class="th">Ngày đăng</td>
                    <td><%=string.Format("{0:dd/MM/yyyy}", oQuyChe.QCNgayDang) %></td>
                </tr>
                <tr>
                    <td class="th">Trích yếu</td>
                    <td><%=oQuyChe.QCTrichYeu %></td>
                </tr>
                <tr>
                    <td class="th">Trạng thái</td>
                    <td><%=oQuyChe.QCHieuLuc == 1 ? "Có hiệu lực" : "Hết hiệu lực" %></td>
                </tr>
                <tr>
                    <td class="th">Danh mục quy chế</td>
                    <td><%=oQuyChe.DMQuyChe.LookupValue %></td>
                </tr>
                <tr>
                    <td class="th">Toàn văn</td>
                    <td><%=oQuyChe.QCToanVan %></td>
                </tr>
                <tr>
                    <td class="th">File đính kèm</td>
                    <td>
                        <%for(int i=0; i< oQuyChe.ListFileAttach.Count;i++){ %>
                        <div class="file">
                            <ul>
                                <li><i class="fas fa-download" style="color:#26b2fa"></i><a href="<%=oQuyChe.ListFileAttach[i].Url %>" style="font-size: 13px; color: #337ab7;"><%=oQuyChe.ListFileAttach[i].Name%></a></li>
                            </ul>
                        </div>
                        <%} %>
                    </td>
                </tr>
            </table>
        </div>
        <%}else{%>
        Không có bản ghi phù hợp với ID!!!
    <%} %>
    </div>
    
</section>


<script type="text/javascript">
    function loopLi() {
        setInterval(function () { // this code is executed every 500 milliseconds:
            var current = '/Pages/regulation.aspx';
            var actived = false;
            $('#items_sliderbar li a').each(function () {
                var $this = $(this);
                // if the current path is like this link, make it active
                if ($this.attr('href').indexOf(current) !== -1) {
                    $this.closest("li").addClass('active');
                    actived = true;
                }
            });
            if (actived) return;
        }, 500);
    }

    $(loopLi);
</script>




