﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_Birthdays.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.Birthdays.UC_Birthdays" %>
<script type="text/javascript">    
    $(document).ready(function () {
        SetConfigLang('<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>', 'Birthdays');
        $("#UserPhongBan").smSelect2018V2({
            dropdownParent: "#PhongBan"
        });
        //Lãnh đạo
        $("#lstSinhNhat").smGrid2018({
            "TypeTemp": "kendo",
            "UrlPost": "/VIPortalAPI/api/LUser/GETDATA_SINHNHAT",
            pageSize: 15,
            odata: { "ID_DONVI": '<%=oLGroupItem.OldID %>'},
            template: "jsTemplateSinhNhat",
            dataBound: function (e) {
            },
        });
        setTimeout(
            function () {
            $("#lstSinhNhatCBNV").smGrid2018({
                "TypeTemp": "kendo",
                "UrlPost": "/VIPortalAPI/api/LUser/GETDATA_SINHNHAT_CBNV",
                pageSize: 15,
                odata: { "ID_DONVI": '<%=MaDonViSelected %>', "PhongBan": '<%=MaPhongBanSelected %>' },
                template: "jsTemplateSinhNhatCBNV",
                dataBound: function (e) {
                },
            });
        }, 5000);
    });
    function formatDateNgaySinh(odata) {
        return formatDate(odata.UserNgaySinh)
    }
</script>

<style>
    .margin-p {
        margin: 0;
        padding: 0;
    }
    
    .imgBirday-Lt {
        width: 120px;
        height: 165px;
    }
    
    .imgBirday-Lt img {
        width: 100%;
        height: 100%;
        object-fit: cover;
    }
    
    .title-info-Lt {
        font-weight: 700;
        font-size: 15px;
    }
    .title-info-Lt{
         overflow: hidden;
        text-overflow: ellipsis;
        -webkit-line-clamp: 2;
        -webkit-box-orient: vertical;
        display: -webkit-box;
    }
    .address-info-Lt {
        font-weight: 500;
        font-size: 14px;
        overflow: hidden;
        text-overflow: ellipsis;
        -webkit-line-clamp: 2;
        -webkit-box-orient: vertical;
        display: -webkit-box;
    }
    
    .border-after {
        position: relative;
    }
    
    .border-after::before {
        content: "";
        position: absolute;
        bottom: -25px;
        left: 0;
        border-bottom: 1px solid #ccc;
        width: 100%;
    }
    @media screen and (max-width: 1024px) and (min-width: 768px){
        .imgBirday-Lt {
            width: 100%;
            height: 100%;
        }
        .title-info-Lt,.address-info-Lt{
            font-size:11px;
           
        }
        

    }

    .nav-tabs .nav-link.active {
        background: #f8f8f8;
        position: relative;
        display: block;
        border-bottom: 3px solid #fff;
        color: #ee1d23 !important;
    }

        .nav-tabs .nav-link.active:after {
            content: "";
            background-image: -moz-linear-gradient( 0deg, rgb(21,67,152) 0%, rgb(238,29,35) 100%);
            background-image: -webkit-linear-gradient( 0deg, rgb(21,67,152) 0%, rgb(238,29,35) 100%);
            background-image: -ms-linear-gradient( 0deg, rgb(21,67,152) 0%, rgb(238,29,35) 100%);
            display: block;
            height: 3px;
            width: 100%;
            position: absolute;
            bottom: -3px;
            left: 0;
        }

    .nav-tabs .nav-link {
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        border: none;
        color: #134298 !important;
        font-size: 14px;
        font-weight: 600;
        padding: 0.5rem 1.5rem;
    }
</style>

 <section class="Birthdays">
        <div class=" mt-4 position-relative">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#lstSinhNhat" data-lang="CORPORATIONS">TỔNG CÔNG TY</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#lstSinhNhatCBNV" data-lang="UNIT">ĐƠN VỊ</a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content mt-0">
                <div id="lstSinhNhat" class="container-fluid tab-pane active">
                    <div class="header_birday display-flex mt-5">
                        <span><img src="img_birday/birthday-cake.png" alt=""></span>
                        <p class="pl-3 font-size-16 font-weight-bold color-table-meeting margin-p" data-lang="LeadershipBirthday">Sinh nhật ban lãnh đạo
                        </p>
                    </div>
                    <div class="row" role="grid">
                        
                    </div>
                </div>
                <div id="lstSinhNhatCBNV" class="container-fluid tab-pane fade">
                    <div class="header_birday display-flex mt-5 ">
                        <span><img src="img_birday/birthday-cake.png" alt=""></span>
                        <p class="pl-3 font-size-16 font-weight-bold color-table-meeting margin-p" data-lang="StaffBirthday">Sinh nhật cán bộ nhân viên
                        </p>
                    </div>

                    <div class="row" role="grid">
                       
                    </div>
                </div>
            </div>
        </div>
     <script id="jsTemplateSinhNhat" type="text/x-kendo-template">
         <div class="col-md-4 border-after pt-5">
                            <div class="d-flex">
                                <div class="imgBirday-Lt ">
                                    <img src="#=ImageAvatar#" alt="">
                                </div>
                                <div class="infoBirdat-Lt pl-3">
                                    <p class="title-info-Lt margin-p" data-lang="Comrade">Đồng chí #=TENKHAISINH#</p>
                                     <p class="position-info-Lt margin-p pt-2">#=TEN_CVU#</p>

                                    <p class="address-info-Lt margin-p pt-2">#=TEN_DONVI#</p>
                                    <p class="position-info-Lt margin-p pt-2">#=TEN_PHONGB#</p>
                                    <p class="date-info-Lt margin-p pt-2"><span><i class="fas fa-birthday-cake"></i></span><span class="pl-2">#=formatDateDDMM(BIRTHDAY)#</span></p>
                                </div>
                            </div>
                        </div>
        </script>
        <script id="jsTemplateSinhNhatCBNV" type="text/x-kendo-template">
            <div class="col-md-4 border-after pt-5">
                            <div class="d-flex">
                                <div class="imgBirday-Lt ">
                                    <img src="#=ImageAvatar#" alt="">
                                </div>
                                <div class="infoBirdat-Lt pl-3">
                                    <p class="title-info-Lt margin-p" data-lang="Comrade">Đồng chí #=tenkhaisinh#</p>
                                    <p class="address-info-Lt margin-p pt-2">#=TEN_VTRICDANH == null ? "Chuyên viên" : TEN_VTRICDANH#</p>
                                    <p class="position-info-Lt margin-p pt-2">#=DEPARTMENT_NAME#</p>
                                    <p class="date-info-Lt margin-p pt-2"><span><i class="fas fa-birthday-cake"></i></span><span class="pl-2">#=formatDateDDMM(ngaysinh)#</span></p>
                                </div>
                            </div>
                        </div>
         
        </script>
    </section>

