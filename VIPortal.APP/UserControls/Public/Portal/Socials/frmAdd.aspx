﻿<%@ page language="C#" autoeventwireup="true" codebehind="frmAdd.aspx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.Socials.frmAdd" %>

<%@ import namespace="ViPortal_Utils" %>

<!DOCTYPE html>
<style>
    .uploadfiles .name {
        display: none !important;
    }

    .uploadfiles .preview-img {
        width: 50%;
    }
</style>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <div id="frmBaiViet">
        <form id="exForm" class="form-horizontal">
            <input type="hidden" name="CreatedUser" value="<%=CurentUser.ID%>" />
            <input type="hidden" name="fldGroup" value="<%=CurentUser.UserPhongBan.ID%>" />
            <input type="hidden" name="AnhDaiDien" value="<%=CurentUser.AnhDaiDien.GetPathAndQuery()%>" />
            <input type="hidden" name="do" value="UPDATE" />
            <div class="form-group-socialnetworrk">
                <textarea class="form-control" id="message-text" name="ThreadNoiDung" required placeholder="Cảm xúc hôm nay của bạn là gì ?"></textarea>
                <%--<textarea class="form-control" id="ThreadNoiDung" name="ThreadNoiDung" required="required" ></textarea>--%>
            </div>
            <div class="form-group-socialnetworrk" style="padding-top:10px">                
                    <select data-url="/UserControls/AdminCMS/DanhMucMXH/pAction.asp?do=QUERYDATA&moder=1" data-place="Chọn danh mục bài đăng" name="DanhMuc" id="DanhMuc" class="form-control"></select>
                
            </div>
            <div class="display-flex-righteous">
                <div class="title-socilanetwork pt-2">
                    <p class="margin-p">Thêm vào bài đăng</p>
                    <p class="margin-p pt-2">
                        <%--<img src="/Content/themeV1/icon/image-social.jpg" alt="">--%>
                        <%--<input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />--%>
                        <input style="width: auto;" type="file" value="Chọn ảnh" id="FileAttachDaiDien" name="FileAttachDaiDien" multiple="multiple" />
                    </p>
                    <div class="button-solicanetwork pt-2">
                        <button type="button" class="btn-solicanetwork" id="submitThread">Đăng</button>
                    </div>
                </div>

            </div>
        </form>
    </div>
</body>
</html>

<script type="text/javascript">
    $(document).ready(function () {
        $("#DanhMuc").smSelect2018V2({
            dropdownParent: "#exForm"
        });
        $("#FileAttach").regFileUploadV2({
            files: ''
        });
        $("#FileAttachDaiDien").regFileUpload({
            files: '',
            preview: true,
            OnlyOne: true
        });
        ///Gửi bài đăng 
        $("#exForm").validate({
            ignore: [],
            rules: {
                ThreadNoiDung: "required"
            },
            messages: {
                ThreadNoiDung: "Vui lòng nhập nội dung bài viết",
            },
            submitHandler: function (form) {
                debugger;
                <%--var odatapost = {
                    threadnoidung: $("#message-text").val(),
                    CreatedUser: <%=CurentUser.ID%>,
                    fldGroup: <%=CurentUser.UserPhongBan.ID%>,
                    "AnhDaiDien": GetPathUrlString("<%=CurentUser.AnhDaiDien%>"),

                };--%>
                $.post("/UserControls/AdminCMS/MangXaHoi/pAction.asp", $(form).viSerialize(), function (result) {
                    if (result.State == 2) {
                        BootstrapDialog.show({
                            title: "Lỗi",
                            message: result.Message
                        });
                    }
                    else {
                        showMsg(result.Message);
                        $(form).closeModal();
                    }
                }).always(function () { });
            }
        });
        $("#submitThread").click(function () {
            $("#exForm").submit();
        });
        $("#exForm").viForm();
        <%--<%--//cknoidung
        $("#titlebaiviet").focus(function () {
            if ($("#cknoidung").css('display') == 'none') {
                $("#cknoidung").toggle();
            }
        });
        CKEDITOR.replace('ThreadNoiDung', {
            // Define the toolbar groups as it is a more accessible solution.
            toolbarGroups: [{
                "name": "basicstyles",
                "groups": ["basicstyles"]
            },
            {
                "name": "links",
                "groups": ["links"]
            },
            {
                "name": "paragraph",
                "groups": ["list", "blocks"]
            },
            {
                "name": "document",
                "groups": ["mode"]
            },
            {
                "name": "insert",
                "groups": ["insert"]
            },
            {
                "name": "styles",
                "groups": ["styles"]
            },
            {
                "name": "about",
                "groups": ["about"]
            }
            ],
            // Remove the redundant buttons from toolbar groups defined above.
            removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
        });
        $("#exForm").validate({
            ignore: [],
            rules: {
                TitleBaiViet: "required",
                ThreadNoiDung: "required"
            },
            messages: {
                TitleBaiViet: "Vui lòng nhập tiêu đề bài viết",
                ThreadNoiDung: "Vui lòng nhập nội dung bài viết",
            },
            submitHandler: function (form) {
                CK_jQ();
                debugger;
                var odatapost = {
                    title: $("#TitleBaiViet").val(),
                    threadnoidung: $("#ThreadNoiDung").val(),
                    CreatedUser: <%=CurentUser.ID%>,
                    "AnhDaiDien": GetPathUrlString("<%=CurentUser.AnhDaiDien%>"),
                };
                $.post("/VIPortalAPI/api/BaiViet/AddBaiVienMangXaHoi", odatapost, function (result) {
                    if (result.State == 2) {
                        BootstrapDialog.show({
                            title: "Lỗi",
                            message: result.Message
                        });
                    }
                    else {
                        showMsg(result.Message);
                        $(form).closeModal();
                    }
                }).always(function () { });
            }
        });
        $("#submitThread").click(function () {
            $("#exForm").submit();
        });
        $("#exForm").viForm();--%>
    });
</script>

