﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using VIPortalAPP;

namespace VIPortal.APP.UserControls.Public.Portal.Socials
{
    public partial class UC_SocialNetwork_Details : BaseUC_Web
    {
        public MangXaHoiItem oMangXaHoiItem { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            MangXaHoiDA oMangXaHoiDA = new MangXaHoiDA();
            
            oMangXaHoiItem = oMangXaHoiDA.GetByIdToObject<MangXaHoiItem>(ItemID);
            Dictionary<string, object> lstUpdateLUser = new Dictionary<string, object>();
            lstUpdateLUser.Add("SoLuotXem", oMangXaHoiItem.SoLuotXem + 1);
            oMangXaHoiDA.UpdateOneOrMoreField(ItemID, lstUpdateLUser, true);
        }
        public string GetTimeLast(DateTime lastDateTime)
        {            
            if (lastDateTime != null)
            {
                DateTime today = DateTime.Now;
                // Find the distance between now and the count down date
                double days = Math.Round((today - lastDateTime).TotalDays);
                double minutes = Math.Round(Convert.ToDouble(today.Minute - lastDateTime.Minute));
                double hours = Math.Round(Convert.ToDouble(today.Hour - lastDateTime.Hour));
                double seconds = Math.Round(Convert.ToDouble(today.Second - lastDateTime.Second));
                // Time calculations for days, hours, minutes and seconds
                //var days = Math.Round(distance / (1000 * 60 * 60 * 24));
                //var hours = Math.Round((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                //var minutes = Math.Round((distance % (1000 * 60 * 60)) / (1000 * 60));
                //var seconds = Math.Round((distance % (1000 * 60)) / 1000);
                if (days > 0)
                {
                    return days + " ngày";
                }
                else if (hours > 0)
                {
                    return hours + " giờ";
                }
                else if (minutes > 0)
                {
                    return minutes + " phút";
                }
                else if (seconds > 0)
                {
                    return "vài giây";
                }
                else
                    return "Vừa xong";
            }
            else return "Vừa xong";
        }
    }
}