﻿using SolrNet.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using VIPortalAPP;

namespace VIPortal.APP.UserControls.Public.Portal.Socials
{
    public partial class UC_SocialNetwork : BaseUC_Web
    {
        public MangXaHoiJson oMangXaHoi { get; set; }
        public MangXaHoiItem oMangXaHoiItem { get; set; }
        public UC_SocialNetwork()
        {
            
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            MangXaHoiDA oMangXaHoiDA = new MangXaHoiDA();
            oMangXaHoi = oMangXaHoiDA.GetListJson(new MangXaHoiQuery() { FieldOrder = "Created" , Ascending = false,_ModerationStatus=1})[0];
            oMangXaHoiItem = oMangXaHoiDA.GetByIdToObject<MangXaHoiItem>(oMangXaHoi.ID);
        }
        public string GetTimeLastV1(DateTime? Created)
        { 
            DateTime lastDateTime = Convert.ToDateTime(Created);
            if (lastDateTime != null)
            {
                DateTime today = DateTime.Now;
                // Find the distance between now and the count down date
                double days = Math.Round((today - lastDateTime).TotalDays);
                double minutes = Math.Round(Convert.ToDouble(today.Minute - lastDateTime.Minute));
                double hours = Math.Round(Convert.ToDouble(today.Hour - lastDateTime.Hour));
                double seconds = Math.Round(Convert.ToDouble(today.Second - lastDateTime.Second));
                if (days > 0)
                {
                    return days + " ngày";
                }
                else if (hours > 0)
                {
                    return hours + " giờ";
                }
                else if (minutes > 0)
                {
                    return minutes + " phút";
                }
                else if (seconds > 0)
                {
                    return "vài giây";
                }
                else
                    return "Vừa xong";
            }
            else return "Vừa xong";
        }
    }
}