﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_SocialNetwork_Details.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.Socials.UC_SocialNetwork_Details" %>
<style>
    .post-box {
        padding: 10px 10px;
        border-radius: 50px;
        margin-left: 10px;
        width: 440px;
    }
    .post-social-lib-content-action-like{
        cursor:pointer
    }
</style>
<script type="text/javascript">
    function GetTimeLast(lastDateTime) {
        if (lastDateTime != '' && lastDateTime != null) {
            var countDownDate = new Date(lastDateTime);
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = now - countDownDate;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            if (days > 0) {
                return `${days} ngày`;
            } else if (hours > 0) {
                return `${hours} giờ`;
            }
            else if (minutes > 0) {
                return `${minutes} phút`;
            }
            else if (seconds > 0) {
                return `vài giây`;
            } else
                return 'Vừa xong';
        }
        else return 'Vừa xong';
    }
    $(document).ready(function () {
        $("#lstComment").smGrid2018({
            "TypeTemp": "kendo",
            "UrlPost": "/VIPortalAPI/api/BinhLuan/QUERYDATA_MANGXAHOI?BaiVietID=<%=oMangXaHoiItem.ID%>",
            pageSize: 20,
            odata: { "do": "QUERYDATA" },
            template: "jsTemplateComment",
            dataBound: function (e) {
            },
        });

        $.post("/VIPortalAPI/api/BaiViet/QUERYDATA_MANGXAHOI?NotGetID=<%=oMangXaHoiItem.ID%>", { "FieldOrder": "Created", Ascending: false, "length": 5 }, function (odata) {
            var templateContent = $("#template_baiviet").html();
            var template = kendo.template(templateContent);

            odata.data = jQuery.map(odata.data, function (n, i) {
                odata.data[i].STT = i + 1;
                return odata.data[i];
            });

            var result = kendo.render(template, odata.data); //render the template

            $("#lstBaivietlienquan").html(result); //append the result to the page
        });
        $.post("/VIPortalAPI/api/BaiViet/QUERYDATA_MANGXAHOI?NotGetID=<%=oMangXaHoiItem.ID%>", { "FieldOrder": "Created", Ascending: false, "length": 3 }, function (odata) {
            var templateContent = $("#template_baiviet").html();
            var template = kendo.template(templateContent);
            odata.data = jQuery.map(odata.data, function (n, i) {
                odata.data[i].STT = i + 1;
                return odata.data[i];
            });
            var result = kendo.render(template, odata.data); //render the template
            $("#listCreated").html(result); //append the result to the page
        });
        $('#post-box-comment').keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                debugger;
                if ($("#post-box-comment").val().trim() != '') {
                    var oDataPost = {
                        Titles: $("#post-box-comment").val(),
                        BaiViet: <%=oMangXaHoiItem.ID%>,
                        CreatedUser: <%=CurentUser.ID%>,
                        "AnhDaiDien": "<%=CurentUser.AnhDaiDienSrcImg%>",
                    };
                    debugger;
                    $.post("/VIPortalAPI/api/BinhLuan/AddComment_MANGXAHOI", oDataPost, function (result) {
                        if (result.State != 2) {
                            showMsg(result.Message);
                            //var $idtable = $tagData.find("table.smdataTable").first().dataTable().fnDraw(false);
                            $("#lstComment").data("smGrid2018").RefreshGrid();
                            $("#frmComment").clear_form_elements();
                            //CKEDITOR.instances.ThreadNoiDung.setData('');
                        }
                    });
                } else {
                    BootstrapDialog.show({
                        title: "Lỗi",
                        message: 'Nhập nội dung bình luận'
                    });
                }
            }
        });
        $("#Like").click(function () {
            var oDataPost = {
                BaiViet: <%=oMangXaHoiItem.ID%>,
                CurrentUser: '<%=CurentUser.Title%>',
                CurrentUserID: '<%=CurentUser.ID%>',
            };
            $.post("/VIPortalAPI/api/BaiViet/Like", oDataPost, function (result) {
                if (result.State != 2) {
                    debugger;
                    $(".post-social-lib-content-action-like").css("color", "blue");
                    $("#luotLike").text(parseInt($("#luotLike").text()) + 1);
                    //$('.post-social-lib-content-action-like').removeAttr("id");
                    //$(".post-social-lib-content-action-like").attr('id', 'Dislike');
                    window.location.reload();
                }
            });
        });
        $("#Dislike").click(function () {
            var oDataPost = {
                BaiViet: <%=oMangXaHoiItem.ID%>,
                CurrentUser: '<%=CurentUser.Title%>',
                CurrentUserID: '<%=CurentUser.ID%>',
            };
            $.post("/VIPortalAPI/api/BaiViet/Dislike", oDataPost, function (result) {
                if (result.State != 2) {
                    debugger;
                    $("#Dislike").css("color", "#606670");
                    $("#luotLike").text(parseInt($("#luotLike").text()) - 1);
                    //$('.post-social-lib-content-action-like').removeAttr("id");
                    //$(".post-social-lib-content-action-like").attr('id', 'Like');
                    window.location.reload();
                }
            });
        });
        loadAjaxContent("/UserControls/Public/Portal/Socials/frmAdd.aspx", "#formthemmoi", {});
    });
</script>
<section class="social display-flex e-row-mr-5 pt-3">
    <script id="jsTemplateComment" type="text/x-kendo-template">
         <div class="post-social-lib-content-boxchat-comment-interactive">
                        <img src="/Content/themeV1/img/avatar/avatar_nam.jpg" style="width: 36px; height: 36px">
                        <div class="post-social-lib-content-boxchat-comment-interactive-nd">
                            <div class="post-social-lib-content-boxchat-comment-interactive-nd-name">
                                #=CreatedUser.Title#   
                            </div>
                            <div class="posts-social-content-nd-date" style="font-weight: 100;">
                                 <i class="far fa-calendar-alt"></i> #=GetTimeLast(Modified)#                      
                            </div>
                            <div class="post-social-lib-content-boxchat-comment-interactive-nd-cmt">
                                 #=Titles#
                            </div>
                        </div>
                    </div>
    </script>
    <script id="template_baiviet" type="text/x-kendo-template">
          <div class="posts-social-content">
                <a href="/Pages/socialnetwork-details.aspx?ItemID=#=ID#"><img src="/Content/themeV1/img_lib_media/Mask_hc.png" alt=""></a>
                <div class="posts-social-content-nd">
                    <div class="posts-social-content-nd-text">
                        <a href="/Pages/socialnetwork-details.aspx?ItemID=#=ID#">#if(ThreadNoiDung.length > 150){##=ThreadNoiDung.substring(0,150)#...#}else{##=ThreadNoiDung##}#</a>
                    </div>
                    <div class="posts-social-content-nd-date">
                        <i class="far fa-calendar-alt"></i>#=formatDate(Created)#                                   
                    </div>
                </div>
            </div>
    </script>
    <div class="news-social e-colum-pd-5" style="width: 50%">
        <div class="voted-category">
            <div class="lib-category-title-fire"></div>
            <div class="img-category-title-lib1">
                <a href="#">Trang chủ</a> -
                               
                <a href="#">Mạng xã hội nội bộ</a>
            </div>
        </div>
        <div class="post-social">
            <%if(CurentUser.AnhDaiDien != null) {%>
            <img src="<%=CurentUser.AnhDaiDien %>" alt="" style="display: block; width: 36px; height: 36px; border-radius: 50%">
            <%}else {%>
            <img src="/Content/themeV1/img/avatar/avatar_nam.jpg" alt="" style="display: block; width: 36px; height: 36px; border-radius: 50%">
            <%} %>
            <div class="post-social-box" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">
                <input type="text" name="" id="post-box" class="box-shadow-basic border-radius-basic" placeholder="Cảm xúc hôm nay của bạn là gì?" />
            </div>
        </div>
        <!-- modal bai đăng -->

        <div class="modal modal-socilnetwork fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="text-center pt-3">
                        <p class="font-size-20 color-table-meeting font-weight-bold text-center" id="exampleModalLabel">Tạo bài đăng mới</p>
                    </div>
                    <div class="display-flex pl-3">
                        <div>
                            <%if(CurentUser.AnhDaiDien != null) {%>
                            <img src="<%=CurentUser.AnhDaiDien %>" alt="" style="display: block; width: 36px; height: 36px; border-radius: 50%">
                            <%}else {%>
                            <img src="/Content/themeV1/img/avatar/avatar_nam.jpg" alt="" style="display: block; width: 36px; height: 36px; border-radius: 50%">
                            <%} %>
                        </div>
                        <div class="pl-3 pt-1">
                            <p class="font-size-14 font-weight-bold margin-p"><%=CurentUser.Title %></p>
                            <p class="font-size-12 margin-p"><%=CurentUser.UserPhongBan.Title %></p>
                        </div>
                    </div>
                    <div class="modal-body pl-3 pr-3 pt-3 pb-3" id="formthemmoi">
                    </div>

                </div>
            </div>
        </div>
        <!-- end bài đăng -->
        <div class="post-social-category">
            <div class="lib-category-title-fire"></div>
            <div class="post-social-lib">Bài đăng mới</div>
        </div>
        <div class="post-social-lib">
            <div class="post-social-lib-poster">
                <div class="post-social-lib-poster-img">
                    <%if(CurentUser.AnhDaiDien != null) {%>
                    <img src="<%=CurentUser.AnhDaiDien %>" alt="" style="display: block; width: 36px; height: 36px; border-radius: 50%">
                    <%}else {%>
                    <img src="/Content/themeV1/img/avatar/avatar_nam.jpg" alt="" style="display: block; width: 36px; height: 36px; border-radius: 50%">
                    <%} %>
                </div>
                <div class="post-social-lib-poster-name">
                    <div class="name-poster"><%=oMangXaHoiItem.CreatedUser.LookupValue %></div>
                    <div class="office"><%=CurentUser.UserPhongBan.Title %></div>
                    <div class="time-poster">
                        <i class="far fa-calendar-alt"></i> &nbsp;<%=GetTimeLast(oMangXaHoiItem.Created) %>
                    </div>
                </div>
            </div>
            <div class="post-social-lib-content">
                <div class="post-social-lib-content-import">
                    <%=oMangXaHoiItem.ThreadNoiDung %>
                </div>
                <div class="post-social-lib-content-img">
                    <%for(int i=0; i< oMangXaHoiItem.ListFileAttach.Count;i++){ %>
                    <div class="file">
                        <img src="<%=oMangXaHoiItem.ListFileAttach[i].Url %>" alt="Alternate Text" />
                    </div>
                    <%} %>
                </div>
                <div class="post-social-lib-content-like">
                    <img src="/Content/themeV1/img_lib_media/Like.png" alt="">
                    <span id="luotLike"><%=oMangXaHoiItem.LuotLike %></span>

                </div>
                <div class="post-social-lib-content-action">
                    <%if(!string.IsNullOrEmpty(oMangXaHoiItem.CurrentUsertLike) && oMangXaHoiItem.CurrentUsertLike.Contains(CurentUser.Title)){%>
                    <div class="post-social-lib-content-action-like" id="Dislike" style="color: blue; font-size: 14px">
                        <i class="fas fa-thumbs-up"></i>  Like
                    </div>
                    <%}else{%>
                    <div class="post-social-lib-content-action-like" id="Like" style="font-size: 14px">
                        <i class="fas fa-thumbs-up"></i>  Like
                    </div>
                    <%} %>
                    <div class="post-social-lib-content-action-comment" id="comment" style="font-size: 14px">
                        <i class="fas fa-comment-alt"></i>  Comment                                   
                    </div>
                </div>
                <div class="post-social-lib-content-boxchat">
                    <%if(CurentUser.AnhDaiDien != null) {%>
                    <img src="<%=CurentUser.AnhDaiDien %>" alt="" style="display: block; width: 36px; height: 36px; border-radius: 50%">
                    <%}else {%>
                    <img src="/Content/themeV1/img/avatar/avatar_nam.jpg" alt="" style="display: block; width: 36px; height: 36px; border-radius: 50%">
                    <%} %>
                    <div class="post-social-lib-content-boxchat-chat" id="frmComment">
                        <input type="text" name="Titles" id="post-box-comment" class="box-shadow-basic border-radius-basic post-box" placeholder="Add comment" style="font-size: 14px; padding: 10px;" />
                    </div>
                </div>
                <div class="post-social-lib-content-boxchat-comment" id="lstComment" data-role="fullGrid">
                    <div role="search" class="box-search -adv row" id="frmsearch">
                        <%--<input type="hidden" name="UrlListTinTuc" value="" />--%>
                    </div>
                    <div role="grid">
                    </div>
                    <div class="clspaging" style="display: none">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="posts-social e-colum-pd-5" style="width: 50%">
        <div class="posts-social-categroy">
            <div class="posts-social-categry-read">
                <div class="posts-social-categry-read-title">
                    Bài đọc nhiều                               
                </div>
                <div class="posts-social-categry-tab"></div>
            </div>
            <div id="lstBaivietlienquan" role="grid"></div>

        </div>
        <div class="posts-social-news">
            <div class="posts-social-news-read">
                <div class="posts-social-news-read-title">
                    Tin mới nhất
                </div>
                <div class="posts-social-categry-tab"></div>
            </div>
            <div class="posts-social-news-content" id="listCreated">                
                <div class="posts-social-content">
                    <a href="#" class="color-a-basic">
                        <img src="img_lib_media/Mask_hc.png" alt="" />
                        <div class="posts-social-content-nd">
                            <div class="posts-social-content-nd-text">
                                EVN Hà Nội khởi động chiến dịch mùa hè không nóng 2021
                                           
                            </div>
                            <div class="posts-social-content-nd-date">
                                <i class="far fa-calendar-alt"></i>05/04/2021
                                           
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
