﻿<%@ control language="C#" autoeventwireup="true" codebehind="TinTucHome.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.TinTucHome" %>

<style>
    .img-new-index {
        padding-bottom: 10px;
    }

        .img-new-index img {
            max-height: 100px;
        }
</style>
<script type="text/javascript">
    $(document).ready(function () {

        SetConfigLang('<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>', 'TinTucHome');
    });
</script>

<%if(!string.IsNullOrEmpty(HtmlOutput)){ %>
<%=HtmlOutput %>
<%}else{ %>
<div class="news-card pl-3 pb-3 pr-3 full-width box-shadow-basic sub_ss_main_one TinTucHome">
    <div class="header_news">
        <div class="title_new pt-3">
            <div class="title-new-content font-size-16 bold">
                <img src="/Content/themeV1/icon/news.png"><span class="color-table-meeting font-weight-bold font-size-20 pl-2" data-lang="TinTucMoi">Tin tức mới</span>
            </div>
        </div>
        <div class="next_news pt-3">
            <a href="<%=UrlSite %>/Pages/hotnews.aspx" class="color-table-meeting" ><span class="pr-2 " data-lang="XemTatCa">Xem tất cả</span><svg class="svg-inline--fa fa-angle-double-right fa-w-14" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-double-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34zm192-34l-136-136c-9.4-9.4-24.6-9.4-33.9 0l-22.6 22.6c-9.4 9.4-9.4 24.6 0 33.9l96.4 96.4-96.4 96.4c-9.4 9.4-9.4 24.6 0 33.9l22.6 22.6c9.4 9.4 24.6 9.4 33.9 0l136-136c9.4-9.2 9.4-24.4 0-33.8z"></path></svg><!-- <i class="fas fa-angle-double-right"></i> Font Awesome fontawesome.com --></a>
        </div>
    </div>
    <p class="font-size-13 mt-1 opa-65" data-lang="ThongTinTinTucCapNhatHangNgay">Thông tin tin tức cập nhật hằng ngày</p>
    <script id="jsTemplateNewHot" type="text/x-kendo-template">
        <div class="col-md-6 img_caption_item">
            <div class="img_news">
                <div class="img-thumbnail">
                    <a href="<%=UrlSite %>/Pages/hotnews.aspx?ItemID=#=ID#">
        <img onerror="this.onerror=null; this.onload=null; if (!this.attributes.src.value || this.attributes.src.value == 'null') this.attributes.src.value='/Content/themeV1/img/News/893070839d8469da3095.jpg';" src="#=ImageNews#" alt=""  class="border-radius-basic"></a>
                </div>
                <div class="caption_news pt-1">
                    <p class=" margin-p text-overflow">
                        <a class="font-size-15 font-weight-bold color-a-basic " href="<%=UrlSite %>/Pages/hotnews.aspx?ItemID=#=ID#">#=Title#</a>
                    </p>
                    <div class="display-flex cal-img">
                        <p class="time-news-block ">
                            <img src="/Content/themeV1/icon/calendar.png" alt="">
                        </p>
                        <p class="pl-3">#=formatDate(CreatedDate)#</p>
                    </div>
                </div>
            </div>
        </div>
    </script>
    <div class="row" id="thoisu">
        <%foreach (ViPortalData.TinNoiBat.TinNoiBatJson oTinNoiBatJson in oDataTop2)
            {%>
        <div class="col-md-6 img_caption_item">
            <div class="img_news">
                <div class="img-thumbnail">
                    <a href="<%=UrlSite %>/Pages/hotnews.aspx?ItemID=<%=oTinNoiBatJson.ID %>">
                        <img onerror="this.onerror=null; this.onload=null; if (!this.attributes.src.value || this.attributes.src.value == 'null') this.attributes.src.value='/Content/themeV1/img/News/893070839d8469da3095.jpg';" src="<%=oTinNoiBatJson.ImageNews %>" alt="" class="border-radius-basic"></a>
                </div>
                <div class="caption_news pt-1">
                    <p class=" margin-p text-overflow">
                        <a class="font-size-15 font-weight-bold color-a-basic " href="<%=UrlSite %>/Pages/hotnews.aspx?ItemID=<%=oTinNoiBatJson.ID %>"><%=oTinNoiBatJson.Title %></a>
                    </p>
                    <div class="display-flex cal-img">
                        <p class="time-news-block ">
                            <img src="/Content/themeV1/icon/calendar.png" alt="">
                        </p>
                        <p class="pl-3"><%=string.Format("{0:dd/MM/yyyy}", oTinNoiBatJson.CreatedDate) %></p>
                    </div>
                </div>
            </div>
        </div>
        <%} %>
    </div>

    <div class="items-news-index pt-4">
        <script id="jsTemplateNew" type="text/x-kendo-template">
        <div class="col-md-6">
                <div class="row">
                    <div class="col-md-5 img-new-index">
                        <a href="<%=UrlSite %>/Pages/hotnews.aspx?ItemID=#=ID#">
                            <img onerror="this.onerror=null; this.onload=null; if (!this.attributes.src.value || this.attributes.src.value == 'null') this.attributes.src.value='/Content/themeV1/img/News/893070839d8469da3095.jpg';" src="#=ImageNews#" alt="" width="100%"></a>
                    </div>
                    <div class="col-md-7 title-new-index">
                        <a href="<%=UrlSite %>/Pages/hotnews.aspx?ItemID=#=ID#" class="color-a-basic">
                            <p class="margin-p font-weight-bold">#=Title#</p>
                        </a>
                        <div class="display-flex cal-img">
                            <p class="time-news-block ">
                                <img src="/Content/themeV1/icon/calendar.png" alt="">
                            </p>
                            <p class="pl-3">#=formatDate(CreatedDate)#</p>
                        </div>
                    </div>
                </div>
            </div>
        </script>
        <div class="row" id="lstNewHome">

            <%foreach (ViPortalData.TinNoiBat.TinNoiBatJson oTinNoiBatJson in oDataTop6)
            {%>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-5 img-new-index">
                        <a href="<%=UrlSite %>/Pages/hotnews.aspx?ItemID=<%=oTinNoiBatJson.ID %>">
                            <img onerror="this.onerror=null; this.onload=null; if (!this.attributes.src.value || this.attributes.src.value == 'null') this.attributes.src.value='/Content/themeV1/img/News/893070839d8469da3095.jpg';" src="<%=oTinNoiBatJson.ImageNews %>" alt="" width="100%"></a>
                    </div>
                    <div class="col-md-7 title-new-index">
                        <a href="<%=UrlSite %>/Pages/hotnews.aspx?ItemID=<%=oTinNoiBatJson.ID %>" class="color-a-basic">
                            <p class="margin-p font-weight-bold"><%=oTinNoiBatJson.Title %></p>
                        </a>
                        <div class="display-flex cal-img">
                            <p class="time-news-block ">
                                <img src="/Content/themeV1/icon/calendar.png" alt="">
                            </p>
                            <p class="pl-3"><%=string.Format("{0:dd/MM/yyyy}", oTinNoiBatJson.CreatedDate) %></p>
                        </div>
                    </div>
                </div>
            </div>

            <%} %>
        </div>
    </div>

</div>

<%} %>

