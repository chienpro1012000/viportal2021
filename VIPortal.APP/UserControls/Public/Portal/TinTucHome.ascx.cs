﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils;
using ViPortalData.TinNoiBat;

namespace VIPortal.APP.UserControls.Public.Portal
{
    public partial class TinTucHome : BaseUC_Web
    {
        public List<TinNoiBatJson> oData { get; private set; }
        public List<TinNoiBatJson> oDataTop2 { get; private set; }
        public List<TinNoiBatJson> oDataTop6 { get; private set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oData = new List<TinNoiBatJson>();
            oDataTop2 = new List<TinNoiBatJson>();
            oDataTop6 = new List<TinNoiBatJson>();
            SimpleLogger oSimpleLogger = new SimpleLogger();
            oSimpleLogger.Info(Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages));


            TinNoiBatDA oNewsDA = new TinNoiBatDA(UrlSite);
            TinNoiBatQuery oNewsQuery = new TinNoiBatQuery()
            {
                FieldOrder = "CreatedDate",
                Length = 6,
                Ascending = false,
                _ModerationStatus = 1
            };
            //oNewsQuery.UrlList = new List<string>() { "/noidung/Lists/TinNoiBat"};
            oData = oNewsDA.GetListJson(oNewsQuery);
            if (oData.Count > 0)
            {
                oDataTop2 = oData.GetRange(0, oData.Count > 2 ? 2 : oData.Count);
                if (oData.Count > 2)
                    oDataTop6 = oData.GetRange(1, oData.Count - 2);
            }
            
        }
    }
}