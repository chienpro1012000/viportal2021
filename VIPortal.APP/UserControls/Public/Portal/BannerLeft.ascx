﻿<%@ control language="C#" autoeventwireup="true" codebehind="BannerLeft.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.BannerLeft" %>

<style>
    .img-star-background {
        position: absolute;
        top: 976px;
        opacity: 0.3;
        z-index: -99;
    }

        .img-star-background img {
            width: 100%;
        }

    .logout-slider, .w3-sidebar, pt-3 {
        z-index: 100;
    }
</style>
<!-- nav mobile -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark bg-moblibe BannerLeft">
    <a class="navbar-brand" href="#">
        <img src="/Content/themeV1/img/logo/logo_xanh_ngang.png" alt=""></a>
    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="list_item_sliderbar font-size-14 navbar-nav" id="items_sliderbar_mobile">
            <%foreach(var item in lstMenu){%>
            <li class="item_slider">
                <a href="<%=item.MenuLink %>" class="font-size-14" >
                    <%=item.MenuIcon %>
                    <span class="pl-2" data-lang="Item<%=item.ID %>"><%=item.Title %></span></a>
            </li>
            <%} %>
        </ul>
        <div class="logout-slider">
            <a href="/_layouts/closeConnection.aspx?loginasanotheruser=true" data-lang="DangXuat">Đăng xuất <span class="pl-2"><i class="fas fa-sign-out-alt"></i></span></a>
        </div>
    </div>

</nav>
<!-- end -->
<div class="menu-bar BannerLeft">
    <a class="close-menu-btn" onclick="w3_close()" id="icon_thuvao">
        <img class="close-menu-icon" src="/Content/themeV1/img/logo/left-arrow.svg" alt="">
    </a>
    <div class="w3-sidebar w3-bar-block w3-card w3-animate-left" id="mySidebar">
        <div class="pt-3">
            <div class="avatar">
                <img src="<%=CurentUser.AnhDaiDien != "" ? CurentUser.AnhDaiDien : "/Content/themeV1/img/avatar/avatar_nam.jpg" %>" alt="" style="border-radius: 50%;">
                <div class=" name_avatar">
                    <div class="dropdown">
                        <a style="color: white;" href="#" class="dropdown-toggle" type="button" data-toggle="dropdown"><span class="font-size-14"><%=CurentUser.Title %></span></a>
                        <ul class="dropdown-menu custheme">
                            <li><a id="btnTheme" href="javascript:;">Theme</a></li>
                        </ul>
                    </div>
                    <span class="font-size-secondary-2"><%=CurentUser.Groups.Title %></span>
                </div>
            </div>
            <div class="icon_exit">
                <a class="close-menu-btn" onclick="w3_close()">
                    <img class="close-menu-icon" src="/Content/themeV1/img/logo/left-arrow.svg" alt="">
                </a>
            </div>
        </div>
        <div class="pt-3">
            <script id="jsTemplateMenu" type="text/x-kendo-template">
            <li class="item_slider">
                <a href="#=MenuLink#" class="font-size-14">
                    #=checkURL(MenuIcon)#
            <span class="pl-2">#=Title#</span></a>
            </li>
            </script>
            <ul id="items_sliderbar" class="list_item_sliderbar font-size-14">
                <%foreach(var item in lstMenu){%>
                <li class="item_slider">
                    <a href="<%=item.MenuLink %>" class="font-size-14">
                        <%=item.MenuIcon %>
                        <span class="pl-2" data-lang="Item<%=item.ID %>"><%=item.Title %></span></a>
                </li>
                <%} %>
            </ul>
        </div>
        <div class="logout-slider">
            <a href="/_layouts/closeConnection.aspx?loginasanotheruser=true" data-lang="DangXuat">Đăng xuất <span class="pl-2"><i class="fas fa-sign-out-alt"></i></span></a>
        </div>
        <div class="img-star-background">
            <img src="/Content/themeV1/img/logo/sao_trang.png" />
        </div>
    </div>

</div>

<style type="text/css">
    #items_sliderbar svg.svg-inline--fa.fa-w-16 {
        width: 26px;
        height: auto;
    }
</style>

<script type="text/javascript">
    function checkURL(url) {
        if (url.match(/\.(jpeg|jpg|gif|png)$/) != null) {
            return `<img src="${url}" alt="">`;
        } else return url;
    }

    $(document).ready(function () {
        SetConfigLang('<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>', 'BannerLeft');
        $("#btnTheme").click(function () {
            openDialogView("Thiết lập giao diện", "/UserControls/Public/Portal/pFormTheme.aspx", {}, 768);
        });
        ///UserControls/Forum/ForumChuDe/pAction.ashx?do=MENUTREE

        var result = `<li class="item_slider BannerLeft">
                <a href="<%=LinkSite%>" class="font-size-14 LinkChangeSite">
                    <i class="fas fa-house-user"></i>
            <span class="pl-2 " data-lang="Item<%=LinkSite%>"><%=TitleLink%></span></a>
            </li>`;
        $("#items_sliderbar").append(result); //append the result to the page
        var current = location.pathname;
        $('#items_sliderbar li a').each(function () {
            var $this = $(this);
            // if the current path is like this link, make it active
            if ($this.attr('href').indexOf(current) !== -1) {
                $this.closest("li").addClass('active');
            }
        })


        //var templateContent_Mobie = $("#jsTemplateMenu").html();
        //var template_Mobie = kendo.template(templateContent_Mobie);
        //var result_Mobie = kendo.render(template_Mobie, odata.data); //render the template
        $("#items_sliderbar_mobile").append(result); //append the result to the page
        var current = location.pathname;
        $('#items_sliderbar_mobile li a').each(function () {
            var $this = $(this);
            // if the current path is like this link, make it active
            if ($this.attr('href').indexOf(current) !== -1) {
                $this.closest("li").addClass('active');
            }
        })
        $(".LinkChangeSite").click(function (evt) {
            //reset cookie
            evt.preventDefault();
            var href = $(this).attr('href');
            $.get("/VIPortalAPI/api/LUser/ResetCookie", { Url: href }, function (odata) {
                console.log(odata);

                window.location.href = href;

            });
        });

    });
</script>

