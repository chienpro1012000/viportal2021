﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_TimKiem.ascx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.UC_TimKiem" %>


<script type="text/javascript">
    $(document).ready(function () {
        $("#lstKQ").smGrid2018({
            "TypeTemp": "kendo",
            "UrlPost": "/VIPortalAPI/api/TimKiem/QUERYDATA",
            pageSize: 20,
            odata: { "do": "QUERYDATA" },
            template: "template_new",
            dataBound: function (e) {
            },
        });
    });
</script>


<script id="template_new" type="text/x-kendo-template">
                             <div class="item-content-main  pl-3 pt-3">
                                <div class="icon-announcement">
                                    <img src="/Content/themeV1/thongbaonoibo/amplifier.png" alt="">
                                </div>
                                <div class="title-content-main pl-3">
                                    <p class="margin-p font-size-15 font-weight-bold"><a href="<%=UrlSite %>#=urlChiTiet#">#=Title#</a></p>
                                    <p class="margin-p pt-2"><span class="pr-3"><img src="/Content/themeV1/thongbaonoibo/calendar.png" alt=""></span>#=formatDate(Created)#</p>
                                    <p class="margin-p pt-2">#=Title#</p>
                                </div>
                            </div>
</script>
<div class="briefings bg-color-box-items" id="lstKQ" data-role="fullGrid">
    <div class="header-briefings">
        <p class="font-size-14 color-table-meeting pl-3 pt-2">Tìm kiếm</p>
    </div>
    <div class="form-briefings pl-3 pr-3 pt-3 pb-3">
        <div action="" class="row" id="frmSearchKL" role="search">
            <div class="col-md-4 items-form">
                <label for="" class="font-size-16 color-table-meeting font-weight-bold">
                    Từ khóa tìm kiếm</label><br>
                <input type="text" name="Keyword" id="Keyword" value="<%=keyword %>" style="width: 100%">
                <input type="hidden" name="SearchInSimple" id="SearchInSimple" value="FullText">
            </div>
            <div class="col-md-6 items-form">
                <label for="TuNgay_TBNgayRaThongBao" class="font-size-16 color-table-meeting font-weight-bold">
                    Thời gian</label><br>
                <div class="row">
                    <div class="col-md-4" style="padding-right: 0">
                        <input type="text" id="SearchTuNgay" class="input-datetime" placeholder="Từ ngày" name="SearchTuNgay" style="width: 100%">
                    </div>
                    <div class="col-md-4 text-center" style="padding: 0">
                        <label for="DenNgay_TBNgayRaThongBao" class="pt-1">Đến ngày:</label>
                    </div>
                    <div class="col-md-4" style="padding-left: 0">
                        <input type="text" id="SearchDenNgay" class="input-datetime" placeholder="Đến ngày" name="SearchDenNgay" style="width: 100%">
                    </div>
                </div>
            </div>
            <div class="col-md-2 btn">
                <label for=""></label>
                <br>
                <button type="button" class="btnsearch">Tìm kiếm</button>
            </div>
        </div>
    </div>
    <div class="content-main-announcement">
        <div role="grid" class="">
        </div>
        <div class="clspaging">
        </div>
    </div>
</div>
