﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_Footer.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.UC_Footer" %>
<script>
    $(document).ready(function () {
        let $ConfigLanguages = '<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>';

            SetConfigLang($ConfigLanguages, 'FooterUC');
        });
</script>
<footer class="footer mt-5 FooterUC">
    
    <div class="row  ">
        <div class="col-md-2 pt-4">
            <div class="img_footer">
                <img src="/Content/themeV1/img/logo/logo_xanh_ngang.png" alt="" width="100%" />
            </div>
        </div>
        <div class="col-md-10 pt-2 FooterUC">
            <div class="title_footer">
                <div class="text-center title_ft">
                    <h3 data-lang="TitleFooter">TỔNG CÔNG TY ĐIỆN LỰC TP.HÀ NỘI @2021
 
                    </h3>
                </div>
                <ul class=" row contact_ft text-center pt-3">
                    <li class="col-md-6">
                        <i class="fas fa-phone-alt"></i>

                        <span data-lang="CustomerCareCenter" style="color:black">Trung tâm CSKH: 
                        </span><span>19001288</span>
                    </li>
                    <li class="col-md-6" >
                        <i class="fas fa-home"></i>
                        <span data-lang="DiaChi" style="color:black">  Địa chỉ :</span>
                       <span data-lang="DiaChiDetail">69 Đinh Tiên Hoàng, Hoàn Kiếm, Hà Nội
                        </span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-2">
            <div class="scroll-btn">
                <i class="fas fa-angle-up"></i>
            </div>
        </div>
    </div>
</footer>
<!-- footter-mobile -->
<div class="footer-mobile">
    <div class="row pt-2">
        <div class="col-md-3 col-sm-3 col-xs-3 col-3 text-center">
            <a href="">
                <i class="fas fa-home header-mobile-icon-home"></i>
            </a>
            <p class="header-mobile-text-1">
                Trang chủ
            </p>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3 col-3 text-center">
            <a href="">
                <i class="far fa-calendar-alt header-mobile-icon-home-1"></i>
            </a>
            <p class="header-mobile-text-2">
                Lịch Họp
            </p>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3 col-3 text-center">
            <a href="">
                <i class="far fa-comments header-mobile-icon-home-2"></i>
            </a>
            <p class="header-mobile-text-3">
                Hỏi Đáp FAQ
            </p>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3 col-3 text-center">
            <a href="">
                <i class="far fa-comments header-mobile-icon-home-3"></i>
            </a>
            <p class="header-mobile-text-5">
                Xem Thêm
            </p>
        </div>
    </div>
</div>

