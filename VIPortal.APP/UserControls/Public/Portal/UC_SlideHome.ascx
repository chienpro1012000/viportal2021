﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_SlideHome.ascx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.UC_SlideHome" %>

<style>
    .slide-small{
        background-color: #fff;
        border-radius: 5px;
        box-shadow: var(--box-shadow-basic);
    }
    
</style>
<div class="row p-4 mb-3 mt-3 slide-small w-100">
    <div class="col-3 mb-2">
        <a href="https://dichvucong.gov.vn/p/home/dvc-dich-vu-cong-truc-tuyen.html?type=1"><img class="w-100" src="\Content\themeV1\img\slide\cong_dv.jpg" /></a>
    </div>
    <div class="col-3 mb-2">
        <a href="https://evnhanoi.vn/user/login?returnUrl=/dashboard/home/quan-ly-hoa-don/thanh-toan-tien-dien-va-hoa-don-dich-vu"><img class="w-100" src="\Content\themeV1\img\slide\dien_tt.jpg" /></a>
    </div>
    <div class="col-3 mb-2">
        <a href="https://evnhanoi.vn/cskh/thanh-toan-truc-tuyen "><img class="w-100" src="\Content\themeV1\img\slide\thanh_toan_TT.jpg" /></a>
    </div>
    <div class="col-3 mb-2">
        <a href="https://play.google.com/store/apps/details?id=cskh.evnhanoi "><img class="w-100" src="\Content\themeV1\img\slide\TAI_APP.jpg" /></a>
    </div>
    <div class="col-3 mb-2">
        <a href="https://www.evn.com.vn/d6/event/EVN-voi-chuyen-doi-so-6-12-92.aspx"><img class="w-100" src="\Content\themeV1\img\slide\chuyen_doi_so.jpg" /></a>
    </div>
    <div class="col-3 mb-2">
        <a href="https://evnhanoi.vn/cms/category?k=tin-hoat-dong"><img class="w-100" src="\Content\themeV1\img\slide\chuyen_doi_sEVN.jpg" /></a>
    </div>
    <div class="col-3 mb-2">
        <a href="https://evnhanoi.vn/cskh/cong-cu-tinh-hd-tien-dien "><img class="w-100" src="\Content\themeV1\img\slide\CONG_CU_TINH_HD.jpg" /></a>
    </div>
    <div class="col-3 mb-2">
        <a href="https://home.evn.com.vn/pages/danh-ba-evn.aspx"><img class="w-100" src="\Content\themeV1\img\slide\z3529031275382_bc176a86cc4a5a5923dcdf86a9d05ee2.jpg" /></a>
    </div>
</div>