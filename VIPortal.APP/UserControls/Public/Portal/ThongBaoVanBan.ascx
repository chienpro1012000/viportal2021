﻿<%@ control language="C#" autoeventwireup="true" codebehind="ThongBaoVanBan.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.ThongBaoVanBan" %>
<style>
    .pt-2{
        padding-top: .5rem !important;
    }
</style>
<script type="text/javascript">
    
    $(document).ready(function () {
        let $ConfigLanguages = '<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>';

        SetConfigLang($ConfigLanguages, 'Thongbaovanban');
        ///UserControls/Forum/ForumChuDe/pAction.ashx?do=MENUTREE
        $.post("/VIPortalAPI/api/VanBanTaiLieu/QUERYDATA", { "FieldOrder": "Created", Ascending: false, "length": 4 }, function (odata) {
            var templateContent = $("#jsTemplateVBTL").html();
            var template = kendo.template(templateContent);
            var result = kendo.render(template, odata.data); //render the template
            $("#lstvbtl").html(result); //append the result to the page
            //calendar_main_two2_active 
            $("#lstvbtl .items_main_two2:first-child .text-center").removeClass("calendar_main_two2").addClass("calendar_main_two2_active");
        });

        $.post("/VIPortalAPI/api/VanBanTaiLieu/QUERYDATA_THONGBAO", { "FieldOrder": "Created", Ascending: false, "length": 4 }, function (odata) {
            var templateContent = $("#jsTemplateTB").html();
            var template = kendo.template(templateContent);
            var result = kendo.render(template, odata.data); //render the template
            $("#LSTTHONGBAO").html(result); //append the result to the page
            //calendar_main_two2_active 
            $("#LSTTHONGBAO .items_main_two2:first-child .text-center").removeClass("calendar_main_two2").addClass("calendar_main_two2_active");
        });
    });
</script>
<style>
    hr:last-child {
        display: none;
    }
</style>
<%if(!string.IsNullOrEmpty(HtmlOutput)){ %>
<%=HtmlOutput %>
<%}else{ %>
<script id="jsTemplateVBTL" type="text/x-kendo-template">
       <div class="items_main_two2 pt-2 pb-2">
                    <div class="calendar_main_two2 text-center">
                        <h6>#=new Date(VBNgayBanHanh).format('dd')#
                        </h6>
                        <p>
                            #=new Date(VBNgayBanHanh).format('MM/yyyy')#
                        </p>
                    </div>
                    <div class="title_main_two2">
                        
    <a href="<%=UrlSite %>/Pages/chitietvanban.aspx?ItemID=#=ID#" class="color-a-basic"><p>
                            #if(Title.length > 90){##=Title.substring(0,90)#...#}else{##=Title##}#
                        </p></a>
                    </div>
                </div>
                <hr />
</script>
<div class="e-row-mr-5 Thongbaovanban" id="Thongbaovanban">
    <div class="notification e-colum-pd-5 ">
        <div class="sub_ss_main_two card box-shadow-basic pl-2 pr-2 pt-3">
            <div class="header_main_two2 pt-2 ">
                <div class="header_one active">
                    <a class="header-one-items">
                        <img src="/Content/themeV1/icon/rss.png" alt="" class="pb-1"><span data-lang="TitleTB" class="pl-2">Thông báo</span></a>
                </div>
                <div class="next_header_one">
                    <a href="<%=UrlSite %>/Pages/notifications.aspx" class="next-header-one-items"><span class="pr-2" data-lang="XemTatCaTitle">Xem tất cả</span><svg class="svg-inline--fa fa-angle-double-right fa-w-14" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-double-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34zm192-34l-136-136c-9.4-9.4-24.6-9.4-33.9 0l-22.6 22.6c-9.4 9.4-9.4 24.6 0 33.9l96.4 96.4-96.4 96.4c-9.4 9.4-9.4 24.6 0 33.9l22.6 22.6c9.4 9.4 24.6 9.4 33.9 0l136-136c9.4-9.2 9.4-24.4 0-33.8z"></path></svg><!-- <i class="fas fa-angle-double-right"></i> Font Awesome fontawesome.com --></a>
                </div>
            </div>
            <script id="jsTemplateTB" type="text/x-kendo-template">
               <div class="items_main_two2 pt-2 pb-2">
                            <div class="calendar_main_two2 text-center">
                                <h6>#=new Date(NgayHieuLuc).format('dd')#
                                </h6>
                                <p>
                                    #=new Date(NgayHieuLuc).format('MM/yyyy')#
                                </p>
                            </div>
                            <div class="title_main_two2">                        
            <a href="<%=UrlSite %>/Pages/chitietthongbao.aspx?ItemID=#=ID#" class="color-a-basic"><p>
                                    #if(Titles.length > 90){##=Titles.substring(0,90)#...#}else{##=Titles##}#
                                </p></a>
                            </div>
                        </div>
                <hr />
            </script>
            <div class="content_main_two2">
                <p class="pt-1" data-lang="MoTa">Cập nhật thông báo hằng ngày</p>
                <div id="LSTTHONGBAO">
                </div>
            </div>
        </div>
    </div>
    <div class="document  e-colum-pd-5">
        <div class="card sub_ss_main_two box-shadow-basic pl-2 pr-2 pt-3">
            <div class="header_main_two2 pt-2 ">
                <div class="header_one active">
                    <a class="header-one-items">
                        <img src="/Content/themeV1/icon/file (1).png" alt="" class="pb-1"><span class="pl-2" data-lang="TitleVB">Tài liệu văn bản</span>
                    </a>
                </div>
                <div class="next_header_one">
                    <a href="<%=UrlSite %>/Pages/vanbantailieu.aspx" class="next-header-one-items"><span class="pr-2" data-lang="XemTatCaTitle">Xem tất
                                                    cả</span><svg class="svg-inline--fa fa-angle-double-right fa-w-14" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-double-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34zm192-34l-136-136c-9.4-9.4-24.6-9.4-33.9 0l-22.6 22.6c-9.4 9.4-9.4 24.6 0 33.9l96.4 96.4-96.4 96.4c-9.4 9.4-9.4 24.6 0 33.9l22.6 22.6c9.4 9.4 24.6 9.4 33.9 0l136-136c9.4-9.2 9.4-24.4 0-33.8z"></path></svg><!-- <i class="fas fa-angle-double-right"></i> Font Awesome fontawesome.com --></a>
                </div>
            </div>
            <div class="content_main_two2">
                <p class="pt-1" data-lang="DanhSachTaiLieuVanBanMoi">Danh sách tài liệu văn bản mới</p>
                <div id="lstvbtl">
                </div>
            </div>
        </div>
    </div>
</div>
<%} %>