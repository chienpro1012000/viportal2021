﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData.DMVideo;

namespace VIPortal.APP.UserControls.Public.Portal.Videos
{
    public partial class UC_Video_Details : BaseUC_Web
    {
        public List<DMVideoJson> lstDanhMuc { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            DMVideoDA oDanhMucVideo = new DMVideoDA(UrlSite);
            lstDanhMuc = oDanhMucVideo.GetListJson(new DMVideoQuery() { _ModerationStatus = 1 });
        }
    }
}