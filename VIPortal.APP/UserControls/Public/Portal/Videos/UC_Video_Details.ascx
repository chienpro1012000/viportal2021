﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_Video_Details.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.Videos.UC_Video_Details" %>

<style type="text/css">
    .modal-img-slier .carousel-item p {
        text-align: center;
        padding-top: 15px;
    }

    .title-type-1 {
        font-size: 24px;
        color: #134197;
        padding: 20px 0;
        border-bottom: 1px solid #ebebeb;
        position: relative;
        margin-bottom: 30px;
        font-weight: bold;
    }

        .title-type-1::after {
            content: "";
            width: 80px;
            height: 4px;
            background-image: -moz-linear-gradient( 0deg, rgb(21,67,152) 0%, rgb(238,29,35) 100%);
            background-image: -webkit-linear-gradient( 0deg, rgb(21,67,152) 0%, rgb(238,29,35) 100%);
            background-image: -ms-linear-gradient( 0deg, rgb(21,67,152) 0%, rgb(238,29,35) 100%);
            position: absolute;
            left: 40px;
            bottom: -2px;
            margin: 0px 0 0 -40px;
        }

    .lib-media_img-wrap {
        width: 100%;
        height: 100%;
        position: relative;
    }

    .lib-media_img {
        width: 100%;
        height: 235px;
    }

    .lib-media_img-info {
        background-color: rgba(0, 0, 0, .4);
        position: absolute;
        text-align: center;
        left: 0;
        right: 0;
        top: 0;
        margin: auto;
        bottom: 0;
    }

        .lib-media_img-info:hover {
            cursor: pointer;
        }

        .lib-media_img-info h4 {
            color: #fff;
            font-size: 18px;
            line-height: 1.3;
            margin-bottom: 10px;
            margin-top: 165px !important;
            font-weight: bold;
        }

    .lib-media_img-num {
        background: rgba(255,255,255,.3);
        padding: 5px 10px;
        border-radius: 12px;
        display: inline-block;
        font-size: 14px;
        color: #fff;
        line-height: 1;
        font-weight: bold;
    }

    .owl-carousel .owl-nav button.owl-next,
    .owl-carousel .owl-nav button.owl-prev {
        background: 0 0;
        color: #ffffff !important;
        border: none;
        padding: 0 !important;
        font: icon;
        font-size: 30px;
        width: 40px;
        height: 40px;
        background-color: #0e2f6a !important;
        border-radius: 20px;
    }

    button.owl-next {
        position: absolute;
        right: -65px !important;
        top: 180px !important;
    }

    button.owl-prev {
        position: absolute;
        left: -65px !important;
        top: 180px !important;
    }

    .owl-theme .owl-nav [class*=owl-]:hover {
        background: #0e2f6a !important;
        color: #FFF;
        text-decoration: none;
    }

    #lib-inner {
        background-color: black;
    }

    @media (max-width: 576px) {
        button.owl-next {
            position: absolute;
            right: 20px !important;
            top: 180px !important;
        }

        button.owl-prev {
            position: absolute;
            left: 20px !important;
            top: 180px !important;
        }

        .owl-theme .owl-dots .owl-dot span {
            width: 20px;
            height: 20px;
        }
    }
</style>
<%--<section class="img-category">
    <h3 class="title-type-1">Thư viện video</h3>

    <div class="row mt-3">
        <div class="col-lg-3 mt-3" id="gallery" data-toggle="modal" data-target="#vid-detail-1">
            <div class="lib-media_img-wrap">
                <img class="lib-media_img" src="img/Media/1.jpg" alt="First slide" data-target="#carouselExample" data-slide-to="0">
                <div class="lib-media_img-info">
                    <h4>Hệ thống lưới điện</h4>
                    <div class="lib-media_img-num">2 video</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 mt-3" id="gallery" data-toggle="modal" data-target="#vid-detail-2">
            <div class="lib-media_img-wrap">
                <img class="lib-media_img" src="img/Media/2.jpg" alt="First slide" data-target="#carouselExample" data-slide-to="0">
                <div class="lib-media_img-info">
                    <h4>Hệ thống lưới điện</h4>
                    <div class="lib-media_img-num">2 video</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 mt-3" id="gallery" data-toggle="modal" data-target="#vid-detail-3">
            <div class="lib-media_img-wrap">
                <img class="lib-media_img" src="img/Media/3.jpg" alt="First slide" data-target="#carouselExample" data-slide-to="0">
                <div class="lib-media_img-info">
                    <h4>Hệ thống lưới điện</h4>
                    <div class="lib-media_img-num">8 video</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 mt-3" id="gallery" data-toggle="modal" data-target="#vid-detail-4">
            <div class="lib-media_img-wrap">
                <img class="lib-media_img" src="img/Media/4.jpg" alt="First slide" data-target="#carouselExample" data-slide-to="0">
                <div class="lib-media_img-info">
                    <h4>Hệ thống lưới điện</h4>
                    <div class="lib-media_img-num">8 video</div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-lg-3 mt-0">
        <div class="col-lg-3 mt-3" id="gallery" data-toggle="modal" data-target="#vid-detail-5">
            <div class="lib-media_img-wrap">
                <img class="lib-media_img" src="img/Media/5.jpg" alt="First slide" data-target="#carouselExample" data-slide-to="0">
                <div class="lib-media_img-info">
                    <h4>Hệ thống lưới điện</h4>
                    <div class="lib-media_img-num">2 video</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 mt-3" id="gallery" data-toggle="modal" data-target="#vid-detail-6">
            <div class="lib-media_img-wrap">
                <img class="lib-media_img" src="img/Media/6.jpg" alt="First slide" data-target="#carouselExample" data-slide-to="0">
                <div class="lib-media_img-info">
                    <h4>Hệ thống lưới điện</h4>
                    <div class="lib-media_img-num">2 video</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 mt-3" id="gallery" data-toggle="modal" data-target="#vid-detail-7">
            <div class="lib-media_img-wrap">
                <img class="lib-media_img" src="img/Media/7.jpg" alt="First slide" data-target="#carouselExample" data-slide-to="0">
                <div class="lib-media_img-info">
                    <h4>Hệ thống lưới điện</h4>
                    <div class="lib-media_img-num">8 video</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 mt-3" id="gallery" data-toggle="modal" data-target="#vid-detail-8">
            <div class="lib-media_img-wrap">
                <img class="lib-media_img" src="img/Media/8.jpg" alt="First slide" data-target="#carouselExample" data-slide-to="0">
                <div class="lib-media_img-info">
                    <h4>Hệ thống lưới điện</h4>
                    <div class="lib-media_img-num">8 video</div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="vid-detail-1" tabindex="-1" role="dialog" aria-hidden="true">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"></span>
        </button>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="owl-carousel owl-theme owl-loaded">
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/0Rgma9xYHx0" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/0Rgma9xYHx0" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/0Rgma9xYHx0" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/0Rgma9xYHx0" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/0Rgma9xYHx0" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/0Rgma9xYHx0" alt="First slide"></iframe>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="vid-detail-2" tabindex="-1" role="dialog" aria-hidden="true">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"></span>
        </button>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">

                    <div class="owl-carousel owl-theme owl-loaded">
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/1kPxWjY_BrU" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/1kPxWjY_BrU" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/1kPxWjY_BrU" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/1kPxWjY_BrU" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/1kPxWjY_BrU" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/1kPxWjY_BrU" alt="First slide"></iframe>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="vid-detail-3" tabindex="-1" role="dialog" aria-hidden="true">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"></span>
        </button>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">

                    <div class="owl-carousel owl-theme owl-loaded">
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/0Rgma9xYHx0" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/0Rgma9xYHx0" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/0Rgma9xYHx0" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/0Rgma9xYHx0" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/0Rgma9xYHx0" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/0Rgma9xYHx0" alt="First slide"></iframe>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="vid-detail-4" tabindex="-1" role="dialog" aria-hidden="true">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"></span>
        </button>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">

                    <div class="owl-carousel owl-theme owl-loaded">
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/1kPxWjY_BrU" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/1kPxWjY_BrU" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/1kPxWjY_BrU" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/1kPxWjY_BrU" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/1kPxWjY_BrU" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/1kPxWjY_BrU" alt="First slide"></iframe>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="vid-detail-5" tabindex="-1" role="dialog" aria-hidden="true">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"></span>
        </button>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">

                    <div class="owl-carousel owl-theme owl-loaded">
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/0Rgma9xYHx0" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/0Rgma9xYHx0" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/0Rgma9xYHx0" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/0Rgma9xYHx0" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/0Rgma9xYHx0" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/0Rgma9xYHx0" alt="First slide"></iframe>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="vid-detail-6" tabindex="-1" role="dialog" aria-hidden="true">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"></span>
        </button>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">

                    <div class="owl-carousel owl-theme owl-loaded">
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/1kPxWjY_BrU" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/1kPxWjY_BrU" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/1kPxWjY_BrU" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/1kPxWjY_BrU" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/1kPxWjY_BrU" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/1kPxWjY_BrU" alt="First slide"></iframe>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="vid-detail-7" tabindex="-1" role="dialog" aria-hidden="true">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"></span>
        </button>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">

                    <div class="owl-carousel owl-theme owl-loaded">
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/0Rgma9xYHx0" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/0Rgma9xYHx0" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/0Rgma9xYHx0" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/0Rgma9xYHx0" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/0Rgma9xYHx0" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/0Rgma9xYHx0" alt="First slide"></iframe>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="vid-detail-8" tabindex="-1" role="dialog" aria-hidden="true">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"></span>
        </button>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">

                    <div class="owl-carousel owl-theme owl-loaded">
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/1kPxWjY_BrU" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/1kPxWjY_BrU" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/1kPxWjY_BrU" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/1kPxWjY_BrU" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/1kPxWjY_BrU" alt="First slide"></iframe>
                                </div>
                                <div class="owl-item">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/1kPxWjY_BrU" alt="First slide"></iframe>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Custom Styling Toggle. For demo purposes only. -->
</section>--%>


<section class="img-category VideoDetails">
    <div class="img-category-title">
        <div class="lib-category-title-fire"></div>
        <div class="img-category-title-lib1">
            <a href="#" data-lang="TrangChu">Trang chủ</a> - <a href="#" data-lang="ThuVienVideo">Thư viện video</a>
        </div>
    </div>



    <div id="videogird" data-role="fullGrid" data-urllist="" role="search">
        <div class="box-wrap-search-page -style01" style="padding-right: 0px !important; padding-top: 15px" >
            <div role="search" class="box-search" id="frmsearchvideo">
                <div action="" method="post" class="display-flex formsearchpublic">
                    <div class="items-form">
                                <input type="text" name="keyword" data-lang="PlaceSearch" placeholder="Nhập từ khóa" >
                                <input type="hidden" name="SearchInAdvance" value="Titles">
                            </div>
                    <div class="select-1" id="DanhMucVideo">
                        <select data-selected="" class="form-control" data-url="/UserControls/AdminCMS/DMVideo/pAction.asp?do=QUERYDATA&_ModerationStatus==1" data-lang="ChonDanhMuc" data-place="Chọn danh mục" name="DanhMuc" id="DanhMuc"></select>
                    </div>
                    <div class="icon-button display-flex pl-2">
                        <button data-toggle="tooltip" data-placement="top" title="Xóa lọc tất cả"  class="reset_data"><i class="fas fa-sync-alt"></i></button>
                        <button type="button" id="search" class="btnsearch btn-search-meeting" data-lang="TimKiem">Tìm kiếm</button>
                    </div>
                </div>
            </div>
        </div>
        <div role="grid" class="img-category-item row">
        </div>
        <div class="clspaging">
        </div>
    </div>


    <script id="template_video" type="text/x-kendo-template">
        <div class="img-category-item-lib col-md-3">
            <div class="modal fade modal-video-media" id="modal#=ID#" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content modal-main-video">
                        <div class="modal-header">
                        <h5 class="modal-title">Xem video</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                        <div class="modal-body mb-0 p-0">
                            <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                                #=RenderView(data)#
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="lib-image-item box-shadow-basic boxitem-video">
                <div class="lib-image-item-img">
                    <img src="#=ImageNews#" alt="" data-toggle="modal" data-target="\#modal#=ID#">
                </div>
                <div class="lib-image-item-content pt-2">
                    <div class="lib-image-item-content-title" data-toggle="modal" data-target="\#modal#=ID#">
                        #=Title#
                    </div>
                    <div class="lib-image-item-content-days">
                        <i class="far fa-calendar-alt"></i><span class="pl-2">#=formatDate(NgayDang)#</span>                        
                    </div>
                </div>
            </div>
        </div>
    </script>
</section>
<script type="text/javascript">
    function RenderView(odata) {
        if (odata.LinkVideo != null && odata.LinkVideo != '') {
            return `<iframe allowfullscreen="allowfullscreen"  width="420" height="345" src="${odata.LinkVideo}?rel=0"></iframe>`;
        } else if (odata.ListFileAttach.length > 0)
            return `<video controls="" name="media"><source  src="${odata.ListFileAttach[0].Url}" type="video/mp4"></video>`;
        else return '';
    }
    $(document).ready(function () {
        SetConfigLang('<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>', 'VideoDetails');
        $('body').on('hidden.bs.modal', '.modal-video-media', function () {
            console.log($(this));
            $(this).find('video').trigger('pause');
            $(this).find('iframe').attr("src", $(this).find('iframe').attr("src"));

        });

        $("#DanhMuc").smSelect2018V2({
            dropdownParent: "#DanhMucVideo"

        });
        $("#videogird").smGrid2018({
            "TypeTemp": "kendo",
            "UrlPost": "/VIPortalAPI/api/Video/VIDEO_QUERYDATA",
            pageSize: 20,
            odata: { "do": "QUERYDATA" },
            template: "template_video",
            dataBound: function (e) {
                $("#videogird img[src='']").attr("src", "/Content/themeV1/img/videoicon.png");
                $(".modal-video-media").click(function () {
                    $("video").trigger('pause');
                    // $("video").pause();
                    $("video").currentTime = 0;

                })
            },
        });

    });
</script>

<script>
    $('.owl-carousel').owlCarousel({
        items: 1,
        merge: true,
        loop: true,
        margin: 10,
        video: true,
        lazyLoad: true,
        center: true,
        responsive: {
            0: {
                items: 1
            },

        }
    })
</script>

