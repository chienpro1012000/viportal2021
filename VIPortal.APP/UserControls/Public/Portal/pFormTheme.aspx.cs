﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData.Lconfig;

namespace VIPortal.APP.UserControls.Public.Portal
{
    public partial class pFormTheme : pFormBase
    {
        public LconfigItem Theme_Bg_Body { get; set; }
        public LconfigItem Theme_Bg_Webpart { get; set; }
        public LconfigItem Theme_font_Body { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            Theme_Bg_Body = new LconfigItem();
            Theme_Bg_Webpart = new LconfigItem();
            Theme_font_Body = new LconfigItem();
            LconfigDA lconfigDA = new LconfigDA();
            if (CurentUser.ID > 0)
            {
                int IDOld = lconfigDA.CheckExit(0, $"Theme_{CurentUser.ID}_Bg_Body");
                if (IDOld > 0)
                {
                    Theme_Bg_Body = lconfigDA.GetByIdToObject<LconfigItem>(IDOld);
                }
                else
                {
                    Theme_Bg_Body.ConfigValue = "#ffffff";
                }

                IDOld = lconfigDA.CheckExit(0, $"Theme_{CurentUser.ID}_Bg_Webpart");
                if (IDOld > 0)
                {
                    Theme_Bg_Webpart = lconfigDA.GetByIdToObject<LconfigItem>(IDOld);
                }
                else
                {
                    Theme_Bg_Webpart.ConfigValue = "#ffffff";
                }
                IDOld = lconfigDA.CheckExit(0, $"Theme_{CurentUser.ID}_font_Body");
                if (IDOld > 0)
                {
                    Theme_font_Body = lconfigDA.GetByIdToObject<LconfigItem>(IDOld);
                }
                else
                {
                    Theme_font_Body.ConfigValue = "Arial";
                }
            }
        }
    }
}