﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using VIPortalAPP;
using ViPortalData;

namespace VIPortal.APP.UserControls.Public.Portal.BaoCao
{
    public partial class DashBoard : BaseUC_Web
    {
        public int BaoCaoDaTao { get; set; }
        public int BaoCaoDaGiao { get; set; }
        public int BaoCaoDaGiaoCoCapNhatMoi { get; set; }
        public int TongSoKyBaoCaoDaThucHien { get; set; }
        public int BaoCaoChoTiepNhan { get; private set; }
        public int BaoCaoChoXuLy { get; private set; }
        public int BaoCaoDaXuLy { get; private set; }
        public int BaoCaoToanBo { get; private set; }
        public List<MenuQuanTriJson> lstMenuLinhVuc { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            
            lstMenuLinhVuc = new List<MenuQuanTriJson>();
            try
            {
                #region MyRegion
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                lstMenuLinhVuc = oMenuQuanTriDA.GetListJson(new MenuQuanTriQuery()
                {
                    PhanLoaiMenu = 2,
                    MenuParent = -1, //lấy ra các cái ko parent root,
                    _ModerationStatus = 1
                });

                MenuQuanTriQuery oMenuQuanTriQuery = new MenuQuanTriQuery(HttpContext.Current.Request);
                oMenuQuanTriQuery.Length = 0;
                oMenuQuanTriQuery.Ascending = true;
                oMenuQuanTriQuery.FieldOrder = "MenuIndex";
                oMenuQuanTriQuery.PhanLoaiMenu = 2;
                List<int> lstperUser = new List<int>();
                //foreach (var item in CurentUser.Permissions)
                //{
                //    lstperUser.Add(item.ID);
                //}
                //if (lstperUser.Count > 0)
                //{
                //    oMenuQuanTriQuery.lstPermistion = lstperUser;
                //}
                var oDataJsonMenu = oMenuQuanTriDA.GetListJson(oMenuQuanTriQuery);

                foreach (var item in lstMenuLinhVuc)
                {
                    item.CountMenuSub = oDataJsonMenu.Count(x => x.MenuParent.ID == item.ID);
                }
                #endregion
                //

                KyBaoCaoDA oKyBaoCaoDA = new KyBaoCaoDA();
                //lịch đã giao của đơn vị
                BaoCaoDaTao = oKyBaoCaoDA.GetCount(new KyBaoCaoQuery()
                {
                    fldGroup = CurentUser.Groups.ID,
                    LoaiQuery = 1
                });
                //Kỳ báo cáo đang giao cho đơn vị thực hiện.
                BaoCaoDaGiao = oKyBaoCaoDA.GetCount(new KyBaoCaoQuery()
                {
                    fldGroup = CurentUser.Groups.ID,
                    LoaiQuery = 2,
                    TrangThaiBaoCao = 2
                });
                //báo cáo có cập nhật mới
                BaoCaoDaGiaoCoCapNhatMoi = oKyBaoCaoDA.GetCount(new KyBaoCaoQuery()
                {
                    fldGroup = CurentUser.Groups.ID,
                    LoaiQuery = 2,
                    TrangThaiBaoCao = 2,
                    LogText = "|0_99_0|"
                });
                TongSoKyBaoCaoDaThucHien = oKyBaoCaoDA.GetCount(new KyBaoCaoQuery()
                {
                    fldGroup = CurentUser.Groups.ID,
                    LoaiQuery = 2
                });


                //tiếp nhận báo cáo cho đơn vị.
                BaoCaoChoTiepNhan = oKyBaoCaoDA.GetCount(new KyBaoCaoQuery()
                {
                    fldGroup = CurentUser.Groups.ID,
                    fldUser = CurentUser.ID,
                LoaiQuery = 3,
                    TrangThaiBaoCao = 2
                });

                BaoCaoChoXuLy = oKyBaoCaoDA.GetCount(new KyBaoCaoQuery()
                {
                   
                    LoaiQuery = 3,
                   // fldUser = CurentUser.ID,
                    //fldGroup = CurentUser.Groups.ID,
                    HasNgayHoanThanhDonVi = 2,
                    CurrentUserPT = CurentUser.ID
                });
                BaoCaoDaXuLy = oKyBaoCaoDA.GetCount(new KyBaoCaoQuery()
                {
                    fldGroup = CurentUser.Groups.ID,
                    fldUser = CurentUser.ID,
                    LoaiQuery = 3,
                    HasNgayHoanThanhDonVi = 1,
                    CurrentUserPT = CurentUser.ID
                });
                BaoCaoToanBo = oKyBaoCaoDA.GetCount(new KyBaoCaoQuery()
                {
                    fldGroup = CurentUser.Groups.ID,
                    LoaiQuery = 3,
                    fldUser = CurentUser.ID,
                    UserThamGia = CurentUser.ID
                });
            }
            catch (Exception ex)
            {
                var error = new Dictionary<string, string>
                {
                    {"Type", ex.GetType().ToString()},
                    {"Message", ex.Message},
                    {"StackTrace", ex.StackTrace}
                };

                foreach (DictionaryEntry data in ex.Data)
                    error.Add(data.Key.ToString(), data.Value.ToString());

                string json = JsonConvert.SerializeObject(error, Formatting.Indented);
                Page.Response.Write(string.Format("Exception:{0}", json));
            }
        }
    }
}