﻿<%@ control language="C#" autoeventwireup="true" codebehind="DashBoard.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.BaoCao.DashBoard" %>
<style>
    .report-wrapper {
        background: white;
        padding-bottom: 20px;
    }

    .report-data {
        background: #F5F8FD;
        border-radius: 10px;
        padding: 40px 0;
        margin-bottom: 25px;
        box-shadow: -6px 2px 23px -2px rgb(241 245 246 / 59%);
        min-height:268px;
    }

        .report-data img {
            width: 150px;
            height: 100%;
            object-fit: cover;
        }

    .report-title {
        padding-top: 20px;
        font-weight: 600;
    }

        .report-title span:last-child {
            color: red;
        }

    .report-delivery {
        background: #F5F8FD;
        border-radius: 10px;
        padding: 40px 0;
        box-shadow: -6px 2px 23px -2px rgb(241 245 246 / 59%);
        margin-bottom: 20px;
    }

    .title-delivery {
        font-size: 80px;
        font-weight: 600;
        color: #173f90;
    }
    .title-delivery123 {
        color:red;

    }

    .delivery-title {
        display: flex;
        align-items: center;
        justify-content: center;
    }

        .delivery-title img {
            width: 25px;
            height: 25px;
            margin-right: 10px;
        }

    .report-text {
        font-weight: 600;
    }

    @media screen and (device-width: 768px) {
        .report-data img {
            width: 100%;
        }
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        SetConfigLang('<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>', 'DashBoard');
        $(".bcgiao").click(function () {
            var tab = $(this).attr('data-tab');
            window.location.href = '<%=UrlSite %>/Pages/quanlybaocao.aspx#' + tab;
        });
        //bctiepnhan
        $(".bctiepnhan").click(function () {
            var tab = $(this).attr('data-tab');
            window.location.href = '<%=UrlSite %>/Pages/baocao.aspx#' + tab;
        });

        //if ($('.bcgiao, .bctiepnhan').text() == "00") {
        //    console.log('1223');
        //}
        $('.title-delivery a').each(function (i, obj) {
            var text = $(this).text();
            if (text == "00") {
                $(this).addClass("title-delivery")
            } else {
                $(this).addClass("title-delivery123")
            }
            console.log($(this).text());
        });

    });
</script>
<section class="report-wrapper DashBoard" data-per="<%=CurentUser.PermissionsQuery %>">
    <div class="header_birday display-flex mt-5 mb-3">
        <span>
            <img src="img_birday/birthday-cake.png" alt=""></span>
        <p class="pl-3 font-size-20 font-weight-bold color-table-meeting margin-p text-uppercase" data-lang="DATAREPORT">
            BÁO CÁO SỐ LIỆU
        </p>
    </div>
    <div class="row pr-4 pl-4">
        <%foreach (VIPortalAPP.MenuQuanTriJson item in lstMenuLinhVuc)
            {%>
        <div class="col-lg-4 col-md-4">
            <div class="report-data text-center">
                <a href="<%=UrlSite %>/Pages/baocaotichhop.aspx?lv=<%=item.ID %>">
                    <img src="<%=item.MenuIcon %>" alt=""></a>
                <div class="report-title text-uppercase">
                    <span class="report-text "  data-lang="BaoCao<%=item.ID %>"><%=item.Title %></span>
                    <span>(<%=item.CountMenuSub %>)</span>
                </div>
            </div>
        </div>
        <%} %>
    </div>
    
    <%if(!string.IsNullOrEmpty(CurentUser.PermissionsQuery) && CurentUser.PermissionsQuery.Contains("|0904|")){ %>
    <div class="header_birday display-flex mt-1 mb-3">
        <span>
            <img src="img_birday/birthday-cake.png" alt=""></span>
        <p class="pl-3 font-size-20 font-weight-bold color-table-meeting margin-p text-uppercase" data-lang="DELIVERYREPORT">
            BÁO CÁO GIAO
        </p>
    </div>
    <div class="row pr-4 pl-4">
        <div class="col-lg-3 col-md-3">


            <div class="report-delivery text-center">
                <h2 class="title-delivery"> <a class="bcgiao" data-tab="nav-home" title="Báo cáo đã tạo" id="BaoCaoDaTao" href="javascript:;"> <%=BaoCaoDaTao.ToString("D2") %></a></h2>
                <div class="delivery-title text-uppercase">
                    <img src="/Content/themeV1/img/Group5988.png" alt="">
                    <span class="report-text " data-lang="ReportCreated">Báo cáo đã tạo</span>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3">
            <div class="report-delivery text-center">
                <h2 class="title-delivery"><a class="bcgiao" data-tab="nav-profile" id="BaoCaoDaGiao" href="javascript:;"> <%=BaoCaoDaGiao.ToString("D2") %></a></h2>
                <div class="delivery-title text-uppercase">
                    <img src="/Content/themeV1/img/Group5988.png" alt="">
                    <span class="report-text " data-lang="BCDaPhanDangXL">Báo cáo đã phân đang xử lý</span>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3">
            <div class="report-delivery text-center">
                <h2 class="title-delivery"><a class="bcgiao" data-tab="nav-kqmoi" title="Báo cáo mới cập nhật" id="BaoCaoDaGiaoCoCapNhatMoi" href="javascript:;"> <%=BaoCaoDaGiaoCoCapNhatMoi.ToString("D2") %></a></h2>
                <div class="delivery-title text-uppercase">
                    <img src="/Content/themeV1/img/Group5988.png" alt="">
                    <span class="report-text " data-lang="BCCoCapNhatMoi">Báo cáo có cập nhật mới</span>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3">
            <div class="report-delivery text-center">
                <h2 class="title-delivery"><a class="bcgiao" data-tab="nav-choduyet" title="Báo cáo mới cập nhật" id="TongSoKyBaoCaoDaThucHien" href="javascript:;"> <%=TongSoKyBaoCaoDaThucHien.ToString("D2") %></a></h2>
                <div class="delivery-title text-uppercase">
                    <img src="/Content/themeV1/img/Group5988.png" alt="">
                    <span class="report-text " data-lang="ToanBoKyBC">Toàn bộ kỳ báo cáo</span>
                </div>
            </div>
        </div>
    </div>
    <%} %>
    <%if(!string.IsNullOrEmpty(CurentUser.PermissionsQuery) && CurentUser.PermissionsQuery.Contains("|0903|")){ %>
    <div class="header_birday display-flex mt-1 mb-3">
        <span>
            <img src="img_birday/birthday-cake.png" alt=""></span>
        <p class="pl-3 font-size-20 font-weight-bold color-table-meeting margin-p text-uppercase" data-lang="BCNHAN">
            BÁO CÁO NHẬN
        </p>
    </div>
    <div class="row pr-4 pl-4">
        <div class="col-lg-3 col-md-3">
            <div class="report-delivery text-center">
                <h2 class="title-delivery"><a class="bctiepnhan" data-tab="nav-home" title="Báo cáo chờ tiếp nhận" id="BaoCaoChoTiepNhan" href="javascript:;"><%=BaoCaoChoTiepNhan.ToString("D2") %></a></a></h2>
                <div class="delivery-title text-uppercase">
                    <img src="/Content/themeV1/img/Group5988.png" alt="">
                    <span class="report-text " data-lang="ChoTiepNhan">Chờ tiếp nhận</span>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3">
            <div class="report-delivery text-center">
                <h2 class="title-delivery"><a class="bctiepnhan" data-tab="nav-profile" title="Báo cáo phải xử lý" id="BaoCaoChoXuLy" href="javascript:;"><%=BaoCaoChoXuLy.ToString("D2") %></a></h2>
                <div class="delivery-title text-uppercase">
                    <img src="/Content/themeV1/img/Group5988.png" alt="">
                    <span class="report-text " data-lang="ChoXuLy">Chờ xử lý</span>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3">
            <div class="report-delivery text-center">
                <h2 class="title-delivery"><a class="bctiepnhan" data-tab="nav-cnkq" title="Báo cáo đã xử lý" id="BaoCaoDaXuLy" href="javascript:;"><%=BaoCaoDaXuLy.ToString("D2") %></a></h2>
                <div class="delivery-title text-uppercase">
                    <img src="/Content/themeV1/img/Group5988.png" alt="">
                    <span class="report-text " data-lang="Accomplished">Đã hoàn thành</span>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3">
            <div class="report-delivery text-center">
                <h2 class="title-delivery"><a class="bctiepnhan" data-tab="nav-choduyet" title="Toàn bộ báo cáo nhận" id="BaoCaoToanBo" href="javascript:;"><%=BaoCaoToanBo.ToString("D2") %></a></h2>
                <div class="delivery-title text-uppercase">
                    <img src="/Content/themeV1/img/Group5988.png" alt="">
                    <span class="report-text " data-lang="FullReport">Toàn bộ báo cáo</span>
                </div>
            </div>
        </div>
    </div>
    <%} %>
</section>
