﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_QlyBaoCao.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.BaoCao.UC_QlyBaoCao" %>

<section class="news-event">
    <div class="newsevent" id="contentbc">
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active show" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Báo cáo đã tạo</a>
                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Báo cáo đã giao theo kỳ</a>

                <!--<a class="nav-item nav-link" id="nav-cnkq-tab" data-toggle="tab" href="#nav-cnkq" role="tab" aria-controls="nav-cnkq" aria-selected="false">Báo cáo theo kỳ</a>-->
                <a class="nav-item nav-link" id="nav-kqmoi-tab" data-toggle="tab" href="#nav-kqmoi" role="tab" aria-controls="nav-cnkq" aria-selected="false">Báo cáo mới cập nhật</a>
                <a class="nav-item nav-link" id="nav-choduyet-tab" data-toggle="tab" href="#nav-choduyet" role="tab" aria-controls="nav-choduyet" aria-selected="false">Toàn bộ Báo cáo</a>

                <!--<a class="nav-item nav-link" id="nav-choky-tab" data-toggle="tab" href="#nav-choky" role="tab" aria-controls="nav-choky" aria-selected="false">Chờ ký</a>

                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Contact</a>-->
            </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade active show" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div role="body-data" data-title="kỳ báo cáo" data-size="1244" class="content_wp" data-action="/UserControls/BaoCao/BCTongHop/KyBaoCao/pAction.asp" data-form="/UserControls/BaoCao/BCTongHop/KyBaoCao/pFormKyBaoCao.aspx" data-view="/UserControls/BaoCao/BCTongHop/KyBaoCao/pViewKyBaoCaoGiao.aspx">
                    <div class="card card-default color-palette-box">
                        <div class="card-body">
                            <div class="clsmanager row">
                                <div class="col-sm-9">
                                    <div id="KyBaoCaoSearch" class="form-inline zonesearch">
                                        <input type="hidden" name="do" id="do" value="QUERYDATA" />
                                        <input type="hidden" name="LoaiQuery" id="LoaiQuery" value="1" />
                                        <input type="hidden" name="fldGroup" id="fldGroup" value="<%=CurentUser.fldGroup %>" />
                                        <label for="Keyword" class="mb-2 mr-sm-2">Từ khóa:</label>
                                        <input type="text" class="form-control mb-2 mr-sm-2" id="Keyword" placeholder="Từ khóa tìm kiếm" name="Keyword">
                                        <input type="hidden" name="SearchInAdvance" id="SearchInAdvance" value="Title" />
                                        <button type="button" class="btn btn-primary mb-2 act-search">Tìm kiếm</button>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <p class="text-right">
                                        <button class="btn btn-primary act-add" type="button">Thêm mới</button>
                                    </p>
                                </div>
                            </div>
                            <div class="clsgrid table-responsive">
                                <table id="tblKyBaoCao" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                <div role="body-data" data-title="kỳ báo cáo" data-size="1024" class="content_wp" data-action="/UserControls/BaoCao/BCTongHop/KyBaoCao/pAction.asp" data-form="/UserControls/BaoCao/BCTongHop/KyBaoCao/pFormKyBaoCao.aspx" data-view="/UserControls/BaoCao/BCTongHop/KyBaoCao/pViewKyBaoCaoGiao.aspx">
                    <div class="card card-default color-palette-box">
                        <div class="card-body">
                            <div class="clsmanager row">
                                <div class="col-sm-9">
                                    <div id="KyBaoCaoSearchXuLy" class="form-inline zonesearch">
                                        <input type="hidden" name="do" value="QUERYDATA" />
                                        <input type="hidden" name="LoaiQuery" value="2" />
                                        <input type="hidden" name="TrangThaiBaoCao" value="2" />
                                        <input type="hidden" name="fldGroup" value="<%=CurentUser.fldGroup %>" />
                                        <label for="Keyword" class="mb-2 mr-sm-2">Từ khóa:</label>
                                        <input type="text" class="form-control mb-2 mr-sm-2" placeholder="Từ khóa tìm kiếm" name="Keyword">
                                        <input type="hidden" name="SearchInAdvance" value="Title" />
                                        <button type="button" class="btn btn-primary mb-2 act-search">Tìm kiếm</button>
                                    </div>
                                </div>
                            </div>
                            <div class="clsgrid table-responsive">
                                <table id="tblKyBaoCaoDaGiaoXL" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--<div class="tab-pane fade" id="nav-cnkq" role="tabpanel" aria-labelledby="nav-cnkq-tab">
                <div role="body-data" data-title="kỳ báo cáo" data-size="1024" class="content_wp" data-action="/UserControls/BaoCao/BCTongHop/KyBaoCao/pAction.asp" data-form="/UserControls/BaoCao/BCTongHop/KyBaoCao/pFormKyBaoCao.aspx" data-view="/UserControls/BaoCao/BCTongHop/KyBaoCao/pViewKyBaoCao.aspx">
                    <div class="card card-default color-palette-box">
                        <div class="card-body">
                            <div class="clsmanager row">
                                <div class="col-sm-9">
                                    <div id="KyBaoCaoSearchDCNKQ" class="form-inline zonesearch">
                                        <input type="hidden" name="do" value="QUERYDATA" />
                                        <input type="hidden" name="LoaiQuery" value="2" />
                                        <input type="hidden" name="TrangThaiBaoCao" value="2" />
                                        <input type="hidden" name="fldGroup" value="<%=CurentUser.fldGroup %>" />
                                        <label for="Keyword" class="mb-2 mr-sm-2">Từ khóa:</label>
                                        <input type="text" class="form-control mb-2 mr-sm-2" placeholder="Từ khóa tìm kiếm" name="Keyword">
                                        <input type="hidden" name="SearchInAdvance" value="Title" />
                                        <button type="button" class="btn btn-primary mb-2 act-search">Tìm kiếm</button>
                                    </div>
                                </div>
                            </div>
                            <div class="clsgrid table-responsive">
                                <table id="tblKyBaoCaoDCNKQ" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
            <div class="tab-pane fade" id="nav-kqmoi" role="tabpanel" aria-labelledby="nav-kqmoi-tab">
                <div role="body-data" data-title="kỳ báo cáo" data-size="1024" class="content_wp" data-action="/UserControls/BaoCao/BCTongHop/KyBaoCao/pAction.asp" data-form="/UserControls/BaoCao/BCTongHop/KyBaoCao/pFormKyBaoCao.aspx" data-view="/UserControls/BaoCao/BCTongHop/KyBaoCao/pViewKyBaoCaoGiao.aspx">
                    <div class="card card-default color-palette-box">
                        <div class="card-body">
                            <div class="clsmanager row">
                                <div class="col-sm-9">
                                    <div id="KyBaoCaoSearchMoi" class="form-inline zonesearch">
                                        <input type="hidden" name="do" value="QUERYDATA" />
                                        <input type="hidden" name="LoaiQuery" value="2" />
                                        <input type="hidden" name="TrangThaiBaoCao" value="2" />
                                        <input type="hidden" name="LogText" value="|0_99_0|" />
                                        <input type="hidden" name="fldGroup" value="<%=CurentUser.fldGroup %>" />
                                        <label for="Keyword" class="mb-2 mr-sm-2">Từ khóa:</label>
                                        <input type="text" class="form-control mb-2 mr-sm-2" placeholder="Từ khóa tìm kiếm" name="Keyword">
                                        <input type="hidden" name="SearchInAdvance" value="Title" />
                                        <button type="button" class="btn btn-primary mb-2 act-search">Tìm kiếm</button>
                                    </div>
                                </div>
                            </div>
                            <div class="clsgrid table-responsive">
                                <table id="tblKyBaoCaoMoiCapNhat" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="nav-choduyet" role="tabpanel" aria-labelledby="nav-choduyet-tab">
                <div role="body-data" data-title="kỳ báo cáo" data-size="1024" class="content_wp" data-action="/UserControls/BaoCao/BCTongHop/KyBaoCao/pAction.asp" data-form="/UserControls/BaoCao/BCTongHop/KyBaoCao/pFormKyBaoCao.aspx" data-view="/UserControls/BaoCao/BCTongHop/KyBaoCao/pViewKyBaoCao.aspx">
                    <div class="card card-default color-palette-box">
                        <div class="card-body">
                            <div class="clsmanager row">
                                <div class="col-sm-9">
                                    <div id="KyBaoCaoSearchChoDuyet" class="form-inline zonesearch">

                                        <input type="hidden" name="do" value="QUERYDATA" />
                                        <input type="hidden" name="UserThamGia" id="UserThamGia" value="<%=CurentUser.ID %>" />
                                        <input type="hidden" name="fldGroup" id="fldGroupCD" value="<%=CurentUser.fldGroup %>" />
                                        <label for="Keyword" class="mb-2 mr-sm-2">Từ khóa:</label>
                                        <input type="text" class="form-control mb-2 mr-sm-2" id="KeywordCD" placeholder="Từ khóa tìm kiếm" name="Keyword">
                                        <input type="hidden" name="SearchInAdvance" id="SearchInAdvanceCD" value="Title" />
                                        <button type="button" class="btn btn-primary mb-2 act-search">Tìm kiếm</button>
                                    </div>
                                </div>
                            </div>
                            <div class="clsgrid table-responsive">
                                <table id="tblKyBaoCaoCD" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-choky" role="tabpanel" aria-labelledby="nav-choky-tab">
                <div role="body-data" data-title="kỳ báo cáo" data-size="1024" class="content_wp" data-action="/UserControls/BaoCao/BCTongHop/KyBaoCao/pAction.asp" data-form="/UserControls/BaoCao/BCTongHop/KyBaoCao/pFormKyBaoCao.aspx" data-view="/UserControls/BaoCao/BCTongHop/KyBaoCao/pViewKyBaoCao.aspx">
                    <div class="card card-default color-palette-box">
                        <div class="card-body">
                            <div class="clsmanager row">
                                <div class="col-sm-9">
                                    <div id="KyBaoCaoSearchCK" class="form-inline zonesearch">

                                        <input type="hidden" name="do" value="QUERYDATA" />
                                        <input type="hidden" name="LoaiQuery" id="LoaiQueryCK" value="3" />
                                        <input type="hidden" name="TrangThaiBaoCao" id="TrangThaiBaoCaoCK" value="6" />
                                        <input type="hidden" name="fldGroup" id="fldGroupCK" value="<%=CurentUser.fldGroup %>" />
                                        <label for="Keyword" class="mb-2 mr-sm-2">Từ khóa:</label>
                                        <input type="text" class="form-control mb-2 mr-sm-2" id="KeywordCK" placeholder="Từ khóa tìm kiếm" name="Keyword">
                                        <input type="hidden" name="SearchInAdvance" id="SearchInAdvanceCK" value="Title" />
                                        <button type="button" class="btn btn-primary mb-2 act-search">Tìm kiếm</button>
                                    </div>
                                </div>
                            </div>
                            <div class="clsgrid table-responsive">
                                <table id="tblKyBaoCaoCK" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">...</div>
        </div>
    </div>
</section>
<script type="text/javascript">
    function LoadTable(tableid) {
        
        var allcolumn = [
            {
                "name": "BCDenNgayTemp", "sTitle": "Hạn nộp",
                "mData": function (o) {
                    return formatDate(o.BCDenNgayTemp);
                },
                "visible": (tableid != 'tblKyBaoCao'),
                "sWidth": "100px",
            },
            {
                "data": "DMVietTat",
                "name": "DMVietTat", "sTitle": "Viết tắt",
                "sWidth": "60px",
            }, {
                "mData": function (o) {
                    return '<a data-Itemid="' + o.ID + '" class="act-view" href="javascript:;">' + o.Title + '</a>' + `</br> Danh mục báo cáo: ${o.LoaiBaoCao.Title}`;
                },
                "name": "Title", "sTitle": "Tiêu đề"
            }, {
                "data": "MoTaTriggers",
                "name": "MoTaTriggers", "sTitle": "Thời gian báo cáo"
            }, {
                "data": "TrangThaiBaoCaoTitle",
                "name": "TrangThaiBaoCaoTitle", "sTitle": "Trạng thái",
                "sWidth": "150px",
            },
            {
                "mData": null,
                "bSortable": false,
                "sWidth": "18px",
                "className": "all",
                "mRender": function (o) { return '<a class="btn default btn-xs purple act-edit" title="Sửa" data-ItemID="' + o.ID + '" href="javascript:;"><i class="fa fa-edit"></i></a>'; }
            },
            {
                "sTitle": '<a class="btn default btn-xs origan act-delete-multi" href="javascript:;"><i class="far fa-trash-alt"></i></a>',
                "mData": null,
                "bSortable": false,

                "className": "all",
                "sWidth": "18px",
                "mRender": function (o) { return '<a data-id="' + o.ID + '" class="btn default btn-xs origan act-delete" href="javascript:;"><i class="far fa-trash-alt"></i></a>';; }
            },
            {
                "sTitle": '<input class="checked-all" type="checkbox" />',
                "mData": null,
                "bSortable": false,
                "sWidth": "18px",
                "className": "all",
                "mRender": function (o) { return '<input class="check-item" type="checkbox" data-id="' + o.ID + '" />'; }
            }
        ];

        allcolumn.unshift({
            "name": "Created", "sTitle": "Ngày tạo",
            "mData": function (o) {
                return formatDate(o.Created);
            },
            "visible": (tableid == 'tblKyBaoCao'),
            "sWidth": "100px",
        });
        if (tableid == 'tblKyBaoCaoMoiCapNhat') {
            allcolumn.unshift({
                "name": "Modified", "sTitle": "Ngày cập nhật",
                "mData": function (o) {
                    return formatDateTime(o.Modified);
                },
                "sWidth": "120px",
            });
        }
        $('#' + tableid).viDataTable(
            {
                "url": "/UserControls/BaoCao/BCTongHop/KyBaoCao/pAction.asp",
                "aoColumns": allcolumn,
                "aaSorting": [0, 'desc']
            });
    }
    $(document).ready(function () {
        if (curentTab == "" || curentTab == null) {
            LoadTable('tblKyBaoCao');
        }
        //$('#contentbc .nav-tabs a[href="#nav-home"]').tab('show');
        $('#contentbc a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("href") // activated tab
            window.location.hash = target;
            //alert(target);
            var TableElements = $("table");
            var table = $(target).find(TableElements)[0];
            var htmltable = $.trim($(table).html());
            if (htmltable == "") {
                var tableid = $(table).attr('id');
                LoadTable(tableid)
            }

        });

        var curentTab = location.hash;
        console.log(curentTab);
        if (curentTab != "" && curentTab != null) {
            $('#contentbc .nav-tabs a[href="' + curentTab + '"]').tab('show');
        }
    });
</script>
