﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using VIPortalAPP;

namespace VIPortal.APP.UserControls.Public.Portal.BaoCao
{
    public partial class UC_BaoCaoTichHop : BaseUC_Web
    {
        public MenuQuanTriItem oMenuQuanTriItem { get; private set; }
        public List<MenuQuanTriJson> oDataJsonMenu { get; private set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request["lv"]))
            {
                int lv = Convert.ToInt32(Request["lv"]);
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(lv);
                MenuQuanTriQuery oMenuQuanTriQuery = new MenuQuanTriQuery(HttpContext.Current.Request);
                oMenuQuanTriQuery.Length = 0;
                oMenuQuanTriQuery.Ascending = true;
                oMenuQuanTriQuery.FieldOrder = "MenuIndex";
                oMenuQuanTriQuery.PhanLoaiMenu = 2;
                oMenuQuanTriQuery.MenuParent = lv;
                List<int> lstperUser = new List<int>();
                foreach (var item in CurentUser.Permissions)
                {
                    lstperUser.Add(item.ID);
                }
                if (lstperUser.Count > 0)
                {
                    oMenuQuanTriQuery.lstPermistion = lstperUser;
                }
                oDataJsonMenu = oMenuQuanTriDA.GetListJson(oMenuQuanTriQuery);

            }
        }
    }
}