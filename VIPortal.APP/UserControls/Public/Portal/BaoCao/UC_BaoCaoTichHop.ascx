﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_BaoCaoTichHop.ascx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.BaoCao.UC_BaoCaoTichHop" %>
<style>
    .bc-report {
    width: 100%;
}
.bc-report span{
    padding-right: 32px;
}
.bc-report select {
    font-weight: 500;
    text-transform: uppercase;
    width: auto;
    width: 45%;
    font-size: 14px;
}
.color-pr {
    color: #2A3667;
    font-size: 14px;
    font-weight: bold;
}
.smtable thead th{
        word-wrap: break-word;
}
.boder-pr {
    border: 1px solid #eeeeee !important;
    padding: 0.375rem 0.75rem;
    outline: none;
    border-radius: 5px;
    font-size: 16px;
}
.month-rp {
    width: 100%;
    display: flex;
    align-items: center;
}
.month-rp span {
    padding-right: 10px;
}
.month-rp select {
    width: 75%;
    font-size: 14px;
}
.unit-rp {
    width: 100%;
}
.unit-rp span {
    width: 25%;
    padding-right: 21px;
}
.unit-rp select {
    width: 75%;
    font-size: 14px;
}
.button-rp {
    float: right;
}
.button-rp button {
    width: 115px !important;
    font-size: 14px;
}
.wrapper-section {
    border-bottom: 1px solid black;
    padding-bottom: 5px;
    margin-bottom: 15px;
}

.form-rp .title {
    display: flex;
    justify-content: space-between;
    font-size: 14px;
    font-weight: 600;
    padding: 20px 0;
}
.note-form-rp {
    display: flex;
    justify-content: space-between;
    font-size: 14px;
    font-weight: 600;
    padding: 0 10px;
}
.wrapper-form-rp .title-th h4 {
    color: red;
    font-size: 18px;
    font-weight: 600;
    padding: 10px 0;
}
.wrapper-form-rp .title-th p {
    font-size: 16px;
    font-weight: 600;
}
.color-red {
    color: red;
}
.wrapper-form-rp {
    border: 1px solid #eeeeee;
}
.table-rp {
    font-size: 14px;
    padding-top: 10px;
}
.table-rp .tb-color {
    color: var(--color-table-meeting);
}
.cl-number {
    color: var(--color-table-meeting);
}
.button-rp button {
    background: #034881 !important;
    border:  #034881 !important;
    padding: .275rem .75rem !important;
}
.color-f {
    color: #BDBDBD;
}
.table-report td, .table-report th {
    padding: 0.25rem;
}
.bgr-color {
    background-color: #EFEFEF;
}
.table-report tbody tr >th:first-child {
    color: var(--color-table-meeting);
}
@media screen and (max-width: 480px){
    .bc-report {
        width: 100%;
        display: block !important;
    }
    .bc-report span {
        padding-bottom: 5px;
    }
    .month-rp {
        padding-bottom: 15px;
        display: block;
    }
    .month-rp select {
        width: 100%;
    }
    .month-rp span {
        padding-bottom: 5px;
        width: 100%;
    }
    .unit-rp {
        padding-bottom: 15px;
    }
    .unit-rp span {
        padding-bottom: 5px;
    }
    .unit-rp select {
        width: 100%;
    }
    .header_birday .pl-3 {
        padding-left: 0 !important;
    }
    .bc-report span {
        width: 100%;
    }
    .bc-report select {
        width: 100%;
    }
}
@media screen and (device-width: 768px) {
    .month-rp select {
        width: 100%;
    }
    .month-rp {
        display: block;
    }
    .button-rp button {
        margin-bottom: 5px;
    }
}
@media screen and (device-width: 820px) {
    .month-rp select {
        width: 100%;
    }
    .month-rp {
        display: block;
    }
    .button-rp button {
        margin-bottom: 5px;
    }
}
@media screen and (device-width: 1024px) {
    .month-rp select {
        width: 100%;
    }
    .month-rp {
        display: block;
    }
    .button-rp button {
        margin-bottom: 5px;
    }
}
@media screen and (device-width: 1440px) {
     .button-rp button {
        margin-bottom: 5px;
    }
}
</style>

<script type="text/javascript">
    $(document).ready(function () {
        SetConfigLang('<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>', 'BaoCaoTichHop');
        //DMBaoCaoSelect
        $("#DMBaoCaoSelect").change(function () {
            //alert('The option with value ' + $(this).val() + ' and text ' + $(this).text() + ' was selected.');
            var element = $(this).find('option:selected');
            var UrlForm = element.attr("data-Url");
            if (UrlForm != '') {
                loadAjaxContent(UrlForm, "#BaoCaoMain", { ItemID: $(this).val() });
            }
        });
    });
</script>

<section class="report-wrapper BaoCaoTichHop">
    <div class="wrapper-section">
        <div class="header_birday display-flex mt-5 mb-2">
            <span><img src="img_birday/birthday-cake.png" alt=""></span>
            <p class="pl-3 font-size-20 font-weight-bold color-table-meeting margin-p text-uppercase"><a href="/Pages/dashboard.aspx" data-lang="DATAREPORT">BÁO CÁO SỐ LIỆU</a> <i class="fas fa-angle-double-right"></i> <%=oMenuQuanTriItem.Title %> <span class="color-red">(<%=oDataJsonMenu.Count %>)</span>
            </p>
        </div>
        <div class="">
            <div class="">
                <div class="d-flex align-items-center bc-report pb-3">
                    <span class="color-pr" data-lang="Report">Báo cáo:</span>
                    <select id="DMBaoCaoSelect" class="form-select form-select-sm boder-pr" aria-label=".form-select-sm example">
                        <option data-Url="" value="0" data-lang="SelectReport">Chọn báo cáo</option>
                        <%foreach (VIPortalAPP.MenuQuanTriJson item in oDataJsonMenu)
            {%>
                        <option data-Url="<%=item.MenuLink %>" value="<%=item.ID %>"><%=item.Title %></option>
                        <%} %>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <section class="form-rp" id="BaoCaoMain">
        
    </section>
</section>
