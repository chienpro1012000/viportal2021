﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_Header.ascx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.UC_Header" %>
<style>
    .dropdown-menu:not(.custheme) {
        min-width: 20rem !important;
        max-height: 663px;
        overflow-y: auto;
    }
    
    input#myInput {
        width: 85%;
        margin: 15px;
    }
</style>
<header class="header_lert Header">
    <div class="menu-top-bar">
        <div class="menu-top-bar__left">
            <div class="icon_open" style="display: none;" id="btn_open_sliderbar">
                <a id="openNav" onclick="w3_open()">
                    <i class="fas fa-bars"></i>
                </a>
            </div>
            <div class="logo_header">
                <img src="/Content/themeV1/img/logo/logo_xanh_ngang.png" alt="" />
            </div>
        </div>
        <div class="menu-top-bar__right">
            <div class="input-search-global">
                <div class="form_seach">
                    <div action="" method="post">
                        <input type="text" name="keyTimKiem" data-lang="PlaceSearch" id="keyTimKiem" class="box-shadow-basic border-radius-basic" placeholder="Tìm kiếm nhanh ..." />
                        <span class="search_icon"><i class="fas fa-search"></i></span>
                    </div>
                </div>
            </div>

            <div class="select-language">
                <div class="seclect_  box-shadow-basic border-radius-basic">
                    <select class="vodiapicker">
                        <option value="en" class="test" data-thumbnail="/Content/themeV1/img/icon/vietnam.png" data-lang="TiengViet">Tiếng Việt</option>
                        <option value="au" data-thumbnail="/Content/themeV1/img/icon/us.png" data-lang="">Tiếng Anh</option>
                    </select>
                    <div class="lang-select">
                        <button class="btn-select" value=""></button>
                        <div class="b">
                            <ul id="a">
                               <%foreach(var item in lstDanhSachNgonNguJson){%>
                                <li class="linklag"  value="<%=item.URLWeb %>"><img src="<%=item.ColumnImageSrc %>" alt="" value="<%=item.URLWeb %>"/><span data-lang="NgonNgu<%=item.ID %>"><%=item.Title %></span></li>
                                <%} %>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>


            <div class="notification-btn box-shadow-basic border-radius-basic">
                <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link d-flex justify-content-center" data-toggle="dropdown" href="#">
                                <img src="/Content/themeV1/img/icon nav/Group.png" alt="" class="img-notification-header">
                                <span class="badge badge-warning navbar-badge" id="TotalNotifi"><%=TotalNotifi %>
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right notifi  blue">
                                <span class="dropdown-item dropdown-header"><%=TotalNotifi %> <span data-lang="NewMessage">thông báo mới</span></span>
                                <input type="text" placeholder="Search.." id="myInput" onkeyup="filterFunction()">
                                <div class="dropdown-divider">
                                </div>
                                <div id="lstNotifi">
                                <%foreach(var item in lstLNotification){%>
                                <a data-itemid="<%=item.ID %>" data-daxem="<%=item.isDaXem %>" href="<%=item.UrlLink %>" class="dropdown-item">
                                    <i class="fas fa-bullhorn" style="color: red; padding-top: 10px;"></i><span style="white-space: normal !important; font-size: 14px;"><%=item.Title %></span><br />
                                    <span class="float-right text-muted text-sm"><%=string.Format("{0:dd/MM/yyyy}", item.LNotiSentTime )%></span>
                                </a>
                                <%} %>
                                    </div>
                            </div>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
<script id="jsTemplateNoti" type="text/x-kendo-template">
            <a data-itemid="#=ID#" data-daxem="#=isDaXem#" href="#=UrlLink#" class="dropdown-item">
                                    <i class="fas fa-bullhorn" style="color: red; padding-top: 10px;"></i><span style="white-space: normal !important; font-size: 14px;">#=Title#</span><br />
                                    <span class="float-right text-muted text-sm">#=formatDateTime(LNotiSentTime)#</span>
                                </a>
            </script>

<script type="text/javascript">

    function ReloadThongBao() {
        //CountChuaXem
        $.post("/VIPortalAPI/api/LNotification/CountChuaXem", {}, function (odata) {
            $("#TotalNotifi").html(odata.Message);
        });
        $.post("/VIPortalAPI/api/LNotification/QUERYDATA", {
            "FieldOrder": "Created", Ascending: false,
            "length": 40
        }, function (odata) {
            var userlog = ",<%=CurentUser.ID%>,";
            odata.data = jQuery.map(odata.data, function (n, i) {
                if (odata.data[i].LNotiDaXem.indexOf(userlog) == -1)
                    odata.data[i].isDaXem = "False";
                else odata.data[i].isDaXem = "True";
                return odata.data[i];
            });
            var templateContent = $("#jsTemplateNoti").html();
            var template = kendo.template(templateContent);
            var result = kendo.render(template, odata.data); //render the template
            $("#lstNotifi").html(result); //append the result to the page
            //$("#TotalNotifi").html(odata.recordsTotal);
        });
    }

    

    function filterFunction() {
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        $.post("/VIPortalAPI/api/LNotification/QUERYDATA", {
            "FieldOrder": "Created", Ascending: false,
            "length": 20, keyword: filter, SearchInAdvance: "Title"
        }, function (odata) {
            var userlog = ",<%=CurentUser.ID%>,";
            odata.data = jQuery.map(odata.data, function (n, i) {
                if (odata.data[i].LNotiDaXem.indexOf(userlog) == -1)
                    odata.data[i].isDaXem = "False";
                else odata.data[i].isDaXem = "True";
                return odata.data[i];
            });
            var templateContent = $("#jsTemplateNoti").html();
            var template = kendo.template(templateContent);
            var result = kendo.render(template, odata.data); //render the template
            $("#lstNotifi").html(result); //append the result to the page
            $("#TotalNotifi").html(odata.recordsTotal);
        });
    }


    $(document).ready(function () {
        SetConfigLang('<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>', 'Header');
        setInterval(ReloadThongBao, 500000);
        $('#keyTimKiem').keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                event.preventDefault();
                window.location.href = "<%=UrlSite %>/Pages/timkiem.aspx?q=" + $(this).val();
            }
        });
        $(".search_icon").click(function () {
            event.preventDefault();
            window.location.href = "<%=UrlSite %>/Pages/timkiem.aspx?q=" + $(this).val();
        });
        //dropdown-item
        $(".notifi a.dropdown-item").click(function (e) {
            e.preventDefault();
            var href = $(this).attr("href");
            var Itemid = $(this).attr("data-ItemID");
            $.post("/VIPortalAPI/api/LNotification/UpdateDaXem?ItemID=" + Itemid, {}, function (odata) {
                window.location.href = href;
            });
        });

        $('.btn-select').html($('#a li[value="<%=_UrlSite%>"]').html());

        $(".linklag").click(function () {
            var _urlsite = $(this).attr("value");
            window.location.href = _urlsite + '?n=' + new Date().getTime();
        });

        

        $(".btn-select").click(function () {
            event.preventDefault();
            $(".b").toggle();
        });

    });
</script>
