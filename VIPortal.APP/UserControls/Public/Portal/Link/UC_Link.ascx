﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_Link.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.Link.UC_Link" %>
<style>
    .blue-link {
        color: #00478F !important;
    }

    .color-icont-link {
        color: red !important;
    }

    .size-20 {
        font-size: 20px !important;
    }
</style>
<section class="news-event">
    <div class="newsevent" id="content">
        <script id="template_CTP" type="text/x-kendo-template">
              <div class="col-md-6">
                   <div class="caption_news_event pt-3">
                        <h5 class="font-size-15 font-weight-bold d-flex">
                            <i class="fas fa-angle-right color-icont-link pr-1"></i>
                            <a class="clslink color-table-meeting blue-link" href="#=Link#">#=Title#</a>
                        </h5>
                    </div>
              </div>
        </script>
        <div class="left_header_calendar pt-3 pb-3">
            <div class="list_calendar">
                <%foreach (var item in LstLienKetRoot)
                    {%>
                <div class="item_li_1 tabtitle" data-tab-target="#tab-<%=item.ID %>" style="cursor: pointer;">
                    <a onclick="showTinTuc('tab-<%=item.ID %>');"><%=item.Title %></a>
                </div>
                <%} %>
            </div>
        </div>
        <%foreach (var item in LstLienKetRoot)
                    {%>
        <div class="main-news-event" id="tab-<%=item.ID %>" style="display: none">
            <div class="row">

                <%foreach (var itemSub in LstLienKetRootAll.Where(x=>x.ParentLienKet.ID ==item.ID ))
                    {%>

                <div class="col-lg-6 col-md-6">
                    <div class="box-items_event">
                        <div class="pb-2 d-flex align-items-center">
                            <i class="fas fa-th-large pr-2 blue-link font-size-20"></i>
                            <h4 class="font-size-14 blue-link font-weight-bold"><%=itemSub.Title %></h4>
                        </div>
                        <div id="list-new" data-role="fullGrid">
                            <div class="caption_news_event pt-3">

                                <%foreach (var itemSub2 in LstLienKetRootAll.Where(x=>x.ParentLienKet.ID ==itemSub.ID ))
                            {%>
                                <div class="items_news_event d-flex align-items-center pl-2 pb-2">
                                    <i class="fas fa-link color-icont-link font-size-12"></i>
                                    <a href="<%=itemSub2.Link %>" class="font-size-14 font-weight-bold ml-2"><%=itemSub2.Title %></a>
                                </div>
                                <%} %>

                            </div>
                        </div>
                    </div>
                </div>
                <%} %>
            </div>
        </div>
        <%} %>
    </div>
</section>
<script type="text/javascript">
    function showTinTuc(idtab) {
        $("#content .tabtitle").removeClass("active");
        $(".left_header_calendar [data-tab-target='#" + idtab + "']").addClass("active");
        $(".main-news-event").css("display", "none");
        $("#" + idtab).css("display", "block");
    }
    $(document).ready(function () {
        $("div.tabtitle:first-child").addClass("active");
        console.log($(".main-news-event"));
        $(".main-news-event").first().show();
    });
</script>
