﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using VIPortalAPP;

namespace VIPortal.APP.UserControls.Public.Portal.Link
{
    public partial class UC_Link : BaseUC_Web
    {
        public List<LienKetWebsiteJson> LstLienKetRoot { get; private set; }
        public List<LienKetWebsiteJson> LstLienKetRootAll { get; private set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            LienKetWebsiteDA oLienKetWebsiteDA = new LienKetWebsiteDA(UrlSite);
            LstLienKetRoot = oLienKetWebsiteDA.GetListJson(new LienKetWebsiteQuery() { NotHasLink = true, HasParentLienKet = true, FieldOrder = "STT", Ascending = true });

            LstLienKetRootAll = oLienKetWebsiteDA.GetListJson(new LienKetWebsiteQuery() { FieldOrder = "STT", Ascending = true });
            
        }
    }
}