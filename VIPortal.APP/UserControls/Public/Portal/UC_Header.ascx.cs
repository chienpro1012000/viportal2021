﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.DanhSachNgonNgu;
using ViPortalData.Lconfig;

namespace VIPortal.APP.UserControls.Public.Portal
{
    public partial class UC_Header : BaseUC_Web
    {
        public string _UrlSite = "";
        public List<LNotificationJson> lstLNotification { get; set; }
        public List<DanhSachNgonNguJson> lstDanhSachNgonNguJson { get; set; }
        public int TotalNotifi { get; set; }
        public string UrlLogo = "/Content/themeV1/img/logo/logo_xanh_ngang.png";
        public UC_Header()
        {
            lstLNotification = new List<LNotificationJson>();
            lstDanhSachNgonNguJson = new List<DanhSachNgonNguJson>();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //check nếu trang đó, url chưa được duyệt thì sẽ bỏ.
            if (!string.IsNullOrEmpty(UrlSite))
            {
                CongThanhPhanDA congThanhPhanDA = new CongThanhPhanDA();
                CongThanhPhanJson lstCongTP = congThanhPhanDA.GetListJson(new CongThanhPhanQuery() { UrlSite = UrlSite }).FirstOrDefault();
                if (lstCongTP != null && lstCongTP._ModerationStatus != 0)
                {
                    Response.Redirect("/");
                }
                //get thông tin đơn vị theo site.
                LGroupDA lGroupDA = new LGroupDA();
                LGroupJson lGroupJson =  lGroupDA.GetListJson(new LGroupQuery() { UrlSite = UrlSite }).FirstOrDefault();
                if(lGroupJson != null && !string.IsNullOrEmpty(lGroupJson.UrlLogo))
                {
                    UrlLogo = lGroupJson.UrlLogo;
                }
                
            }

            
            try
            {
                DanhSachNgonNguDA oDanhSachNgonNguDA = new DanhSachNgonNguDA();
                _UrlSite = UrlSite;
                if (string.IsNullOrEmpty(UrlSite))
                    _UrlSite = "/";
                lstDanhSachNgonNguJson = oDanhSachNgonNguDA.GetListJson(new DanhSachNgonNguQuery() { _ModerationStatus = 1, FieldOrder = "DMSTT", Ascending = true });
                DanhSachNgonNguJson oDanhSachNgonNgu = lstDanhSachNgonNguJson.FirstOrDefault(x => x.URLWeb == _UrlSite);
                if (oDanhSachNgonNgu != null)
                {
                    IDNgonNgu = oDanhSachNgonNgu.ID;
                }
                LNotificationDA oLNotificationDA = new LNotificationDA(UrlSite);
                lstLNotification = oLNotificationDA.GetListJson(new LNotificationQuery() { _ModerationStatus = 1, FieldOrder = "Created", Ascending = false, LNotiNguoiNhan_Id = CurentUser.ID, Length = 30 });
                lstLNotification = lstLNotification.Select(x => { x.isDaXem = x.LNotiDaXem.Contains($",{CurentUser.ID},"); return x; }).ToList();
                TotalNotifi = oLNotificationDA.GetCount(new LNotificationQuery() { LNotiChuaXem_Id = CurentUser.ID, _ModerationStatus = 1, FieldOrder = "Created", Ascending = false, LNotiNguoiNhan_Id = CurentUser.ID });
            }
            catch (Exception ex)
            {
                Page.Response.Write(string.Format("{0}", ex.Message + "\r\n" + ex.StackTrace));
            }

            LconfigDA lconfigDA = new LconfigDA();
            List<LconfigJson> LstValueConfig = lconfigDA.GetListJson(new LconfigQuery() { ConfigGroup = $"Theme_{CurentUser.ID}" });
            if (LstValueConfig.Count > 0) {
                LiteralControl ltr = new LiteralControl();

                ltr.Text = "<style type=\"text/css\" rel=\"stylesheet\">";
            if (LstValueConfig.FirstOrDefault(x => x.ConfigType == "Bg_Body") != null)
                {
                    ltr.Text += 
                    @".content_wraper
                    {
                        background-color:" + LstValueConfig.FirstOrDefault(x => x.ConfigType == "Bg_Body").ConfigValue + @";
                    }";
                }

                if (LstValueConfig.FirstOrDefault(x => x.ConfigType == "Bg_Webpart") != null)
                {
                    ltr.Text +=
                    @".ms-webpart-zone .card, .ms-webpart-zone .news-card 
                    {
                        background-color:" + LstValueConfig.FirstOrDefault(x => x.ConfigType == "Bg_Webpart").ConfigValue + @";
                    }";
                }
                if (LstValueConfig.FirstOrDefault(x => x.ConfigType == "font_Body") != null)
                {
                    ltr.Text +=
                    @"*
                    {
                        font-family:" + LstValueConfig.FirstOrDefault(x => x.ConfigType == "font_Body").ConfigValue + @"!important;
                    }";
                }

                ltr.Text += "</style>";
                this.Page.Header.Controls.Add(ltr);
            }

        }
    }
}