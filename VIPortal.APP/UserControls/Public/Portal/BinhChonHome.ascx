﻿<%@ control language="C#" autoeventwireup="true" codebehind="BinhChonHome.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.BinhChonHome" %>
<style>
    .checkone .dap-an {
        cursor: pointer;
        padding: 10px 10px;
        width: 100%;
        border-radius: 10px;
        font-weight: bold;
        outline: none;
        background-color: #F6F9FD;
        text-align: center;
    }

        .checkone .dap-an:hover {
            cursor: pointer;
            padding: 10px 10px;
            width: 100%;
            border-radius: 10px;
            background-color: #F5820F;
            color: #fff;
            font-weight: bold;
        }

    .acvtiveChoose {
        color: #F5820F !important;
    }
</style>

<%if(lstBoChuDe.Count > 0){ %>
<script type="text/javascript">
    $(document).ready(function () {
        SetConfigLang('<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>', 'BinhChonHome');
        $(".selectDA").on('click', function (event) {
            debugger;
            var dapan = $("#DapAn").val();
            var idA = event.target.id;
            var idQuestion = event.target.dataset.idcauhoi;
            var loaibinhchon = event.target.dataset.loai;

            if (loaibinhchon == 1) {
                //nếu lần đầu lựa chọn của mỗi câu hỏi add vào dapan
                //if (questionOld == "" || !questionOld.includes(idQuestion)) {
                //    questionOld += idQuestion;
                if (!dapan.includes(idA)) {
                    var $body = $(this).closest("[class='voted_one']");
                    var selecteditems = [];

                    $body.find('.selectDA').each(function (i, ob) {
                        selecteditems.push($(ob).attr('data-id'));
                    });
                    var lstidcautraloi = selecteditems.toString().split(",");
                    for (var i = 0; i < lstidcautraloi.length; i++) {
                        dapan = dapan.replace("," + lstidcautraloi[i], "");
                        $("#" + lstidcautraloi[i] + "").removeClass("acvtiveChoose");
                    }
                    dapan += "," + idA;
                    $("#" + idA + "").addClass("acvtiveChoose");
                } else if (dapan.includes(idA)) {
                    //nếu lựa chọn 2 lần cùng 1 đáp án
                    dapan = dapan.replace("," + idA, "");
                    $("#" + idA + "").removeClass("acvtiveChoose");
                }
            } else {
                if (!dapan.includes(idA)) {
                    dapan += "," + idA;
                    $("#" + idA + "").addClass("acvtiveChoose");
                } else {
                    dapan = dapan.replace("," + idA, "");
                    $("#" + idA + "").removeClass("acvtiveChoose");
                }
            }
            $("#DapAn").val(dapan);
        });

        $("#btnSend").click(function () {
            var dapan = $("#DapAn").val();
            $.post("/UserControls/AdminCMS/KSCauTraLoi/pAction.asp", { do: "UPDATEBINHCHON", "DapAn": dapan, "BoChuDe": "<%=lstBoChuDe[0].ID%>" }, function (data) {
                if (data.Erros) {
                    //thông bao.
                    BootstrapDialog.show({
                        title: "Lỗi",
                        message: data.Message
                    });
                }
                else {
                    openDialogMsg("Thông báo", data.Message);
                    //$(".voted_one").load(".voted_one");
                    $("#DapAn").val("");
                    $(".voted-index a.selectDA").removeClass("acvtiveChoose");
                }
            })
        })
    })

</script>
<div class="voted-index card box-shadow-basic pl-2 pr-2 pt-3 mt-3 BinhChonHome">
    <div class="row">
        <div class="col-md-3">
        </div>
        <div class="col-md-6">
            <div class="img-voted-content text-center">
                <img src="/Content/themeV1/icon/Group 5791.png" alt="" width="100%">
            </div>
        </div>
        <div class="col-md-3">
            <div class="next_voted_main display-flex">
                <div class="pt-1">
                    <p class="margin-p">
                        <img src="/Content/themeV1/icon/lichbinhchon.jpg" alt="">
                    </p>
                </div>
                <div class="pl-2">
                    <p class="margin-p font-size-10 color-table-meeting">
                        Kết thúc
                    </p>
                    <p class="margin-p font-size-12 color-table-meeting">
                        <%if(lstBoChuDe.Count > 0){%>
                        <%=lstBoChuDe[0].KSDenNgay.Value.ToString("dd/MM/yyyy") %>
                        <%} %>
                    </p>
                </div>
            </div>

        </div>
    </div>
    <div class="title_voted_company">
        <form action="">
            <input type="hidden" name="DapAn" id="DapAn" value="" />
            <input type="hidden" name="QuestionOld" id="QuestionOld" value="" />
            <%if(lstBoChuDe.Count > 0){%>
            <div class="text-center title_compnany_vote">
                <h3><a href="/Pages/voted.aspx?ItemId=<%=lstBoChuDe[0].ID %>"><%=lstBoChuDe[0].Title %></a></h3>
                <p class="font-size-14 text-opacity" data-lang="BinhChonContent">Thông tin, tin tức cập nhập thường ngày</p>
            </div>
            <% int i = 1; %>
            <%foreach(var item in lstBoCauHoi){%>
            <div class="voted_one">
                <p class="title-vote-p">
                    <span class="mr-3"><%=i %></span><%=item.Title %>
                </p>
                <%foreach(var itemCTL in GetCauTraLoiByID(item.ID)){%>
                <div class="checkone pt-3">
                    <div class="dap-an">
                        <a class="selectDA" id="<%=itemCTL.ID %>" data-id="<%=itemCTL.ID %>" data-loai="<%=item.LoaiTraLoi %>" data-idcauhoi="<%=item.ID %>">
                            <%=itemCTL.Title %>
                        </a>
                    </div>
                </div>
                <%} %>
            </div>
            <%i++; %>
            <%} %>

            <div class="button_vote pt-4 pb-4" style="text-align: center">
                <button class="btn btn-secondary" data-lang="LamLai">Làm lại</button>
                <button type="button" id="btnSend" class="btn btn-primary" data-lang="GuiKhaoSat">Gửi khảo sát</button>
            </div>
            <%}else{%>
            Không có chủ đề nào
            <%} %>
        </form>
    </div>
</div>

<%} %>
