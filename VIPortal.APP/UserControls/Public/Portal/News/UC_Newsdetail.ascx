﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_Newsdetail.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.News.UC_Newsdetail" %>
<section>
    <div class="main-news-detail bg-color-box-items pb-3">
        <div class="row">
            <div class="col-md-6">
                <div class="header-news-detail">
                    <p class="font-size-14 color-table-meeting pl-3 pt-2 pb-2">
                        Trang chủ - Tin tức sự kiện
                                   
                    </p>
                </div>
                <div class="content-news-detail">
                    <h5 class="title-news-detail font-size-25 font-weight-bold pl-3">EVNHANOI hợp lực đẩy lùi bệnh viêm đường hô hấp cấp (Covid-19)
                                    </h5>
                    <p class="font-size-14 pl-3">
                        <img src="img_news/calendar.png" alt=""><span>23/04/2021</span></p>
                </div>
                <div>
                    <h6 class="title-item-news font-size-15 pl-3">Tổng công ty Điện lực TP Hà Nội (EVNHANOI) đã tổ chức “Lễ phát động ủng hộ phòng, chống dịch Covid-19” trong toàn Tổng công ty.</h6>
                    <p class="font-size-14 pl-3">
                        Ngày 19/3/2020, hưởng ứng lời kêu gọi của Đoàn chủ tịch Ủy ban Trung ương mặt trận Tổ quốc Việt Nam về việc "Toàn dân tham gia ủng hộ phòng chống dịch COVID-19". Tổng công ty Điện lực TP Hà Nội (EVNHANOI) đã tổ chức
                                        “Lễ phát động ủng hộ phòng, chống dịch Covid-19” trong toàn Tổng công ty.
                                   
                    </p>
                    <p class="font-size-14 pl-3">
                        Trước tình hình dịch Covid-19 diễn biến phức tạp, gây tác động lớn đến đời sống nhân dân đặc biệt là kinh tế, xã hội của đất nước, với tinh thần "Hợp lực đẩy lùi Covid-19" EVNHANOI đã phát động ủng hộ qua hội nghị truyền
                                        hình với 39 điểm cầu, nhằm thể hiện sự đồng lòng cùng Đảng, Nhà nước có thêm nguồn lực để tăng cường các biện pháp phòng, chữa bệnh, đảm bảo an sinh xã hội, an toàn đời sống và sức khỏe của nhân dân
                    </p>
                    <div class="img_news_detail text-center pl-3">
                        <img src="img_detail_news/NoPath - Copy (25).png" alt="">
                        <p class="font-size-14">
                            <i>CBCNV Công ty Điện lực Hoàng Mai tham gia ủng hộ</i>
                        </p>
                    </div>
                    <p class="font-size-14 pl-3">
                        Phát động tại buổi lễ, Ông Nguyễn Anh Tuấn – Chủ tịch Hội đồng thành viên EVNHANOI phát biểu "Với trách nhiệm của một công dân của Thành phố Hà Nội, công dân nước Việt Nam, mỗi CBCNV của EVNHANOI hãy tham gia ủng hộ
                                        phòng, chống dịch Covid-19 theo lời kêu gọi của Ủy ban Trung ương mặt trận Tổ quốc Việt Nam để góp phần bảo vệ sức khỏe của cộng đồng và sức khỏe của chính chúng ta".
                    </p>
                    <p class="font-size-14 pl-3">
                        Phát động tại buổi lễ, Ông Nguyễn Anh Tuấn – Chủ tịch Hội đồng thành viên EVNHANOI phát biểu "Với trách nhiệm của một công dân của Thành phố Hà Nội, công dân nước Việt Nam, mỗi CBCNV của EVNHANOI hãy tham gia ủng hộ
                                        phòng, chống dịch Covid-19 theo lời kêu gọi của Ủy ban Trung ương mặt trận Tổ quốc Việt Nam để góp phần bảo vệ sức khỏe của cộng đồng và sức khỏe của chính chúng ta".
                    </p>
                    <p class="font-size-14 pl-3">
                        Phát động tại buổi lễ, Ông Nguyễn Anh Tuấn – Chủ tịch Hội đồng thành viên EVNHANOI phát biểu "Với trách nhiệm của một công dân của Thành phố Hà Nội, công dân nước Việt Nam, mỗi CBCNV của EVNHANOI hãy tham gia ủng hộ
                                        phòng, chống dịch Covid-19 theo lời kêu gọi của Ủy ban Trung ương mặt trận Tổ quốc Việt Nam để góp phần bảo vệ sức khỏe của cộng đồng và sức khỏe của chính chúng ta".
                    </p>
                    <p class="font-size-14 pl-3">
                        Phát động tại buổi lễ, Ông Nguyễn Anh Tuấn – Chủ tịch Hội đồng thành viên EVNHANOI phát biểu "Với trách nhiệm của một công dân của Thành phố Hà Nội, công dân nước Việt Nam, mỗi CBCNV của EVNHANOI hãy tham gia ủng hộ
                                        phòng, chống dịch Covid-19 theo lời kêu gọi của Ủy ban Trung ương mặt trận Tổ quốc Việt Nam để góp phần bảo vệ sức khỏe của cộng đồng và sức khỏe của chính chúng ta".
                    </p>

                    <div class="author-news pt-3 col-md-2 offset-md-10">
                        <p class="font-size-15 font-weight-bold">Minh Thu</p>

                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="main-top-detail">
                    <div class="title-top-detail pl-3 pt-2 pb-2">
                        <p class="font-size-18 font-weight-bold">Bài đọc nhiều</p>
                        <hr style="width: 10%; border: 1px solid #ff0000">
                    </div>
                    <div class=" row main-top-detail items pl-3">
                        <div class="col-md-3 col-3">
                            <a href="">
                                <img src="img_detail_news/Mask.png" alt="" width="100%"></a>
                        </div>
                        <div class="col-md-9 col-9">
                            <h5 class="font-size-14 font-weight-bold">EVN Hà Nội khởi động chiến dịch mùa hè không nóng 2021</h5>
                            <p class="font-size-14">
                                <img src="img_news/calendar.png" alt=""><span>23/04/2021</span></p>
                        </div>
                    </div>
                    <div class=" row main-top-detail items pt-3 pl-3">
                        <div class="col-md-3 col-3">
                            <a href="">
                                <img src="img_detail_news/Mask.png" alt="" width="100%"></a>
                        </div>
                        <div class="col-md-9 col-9">
                            <h5 class="font-size-14 font-weight-bold">EVN Hà Nội khởi động chiến dịch mùa hè không nóng 2021</h5>
                            <p class="font-size-14">
                                <img src="img_news/calendar.png" alt=""><span>23/04/2021</span></p>
                        </div>
                    </div>
                    <div class=" row main-top-detail items pt-3 pl-3">
                        <div class="col-md-3 col-3">
                            <a href="">
                                <img src="img_detail_news/Mask.png" alt="" width="100%"></a>
                        </div>
                        <div class="col-md-9 col-9">
                            <h5 class="font-size-14 font-weight-bold">EVN Hà Nội khởi động chiến dịch mùa hè không nóng 2021</h5>
                            <p class="font-size-14">
                                <img src="img_news/calendar.png" alt=""><span>23/04/2021</span></p>
                        </div>
                    </div>
                    <div class=" row main-top-detail items pt-3 pl-3">
                        <div class="col-md-3 col-3">
                            <a href="">
                                <img src="img_detail_news/Mask.png" alt="" width="100%"></a>
                        </div>
                        <div class="col-md-9 col-9">
                            <h5 class="font-size-14 font-weight-bold">EVN Hà Nội khởi động chiến dịch mùa hè không nóng 2021</h5>
                            <p class="font-size-14">
                                <img src="img_news/calendar.png" alt=""><span>23/04/2021</span></p>
                        </div>
                    </div>
                    <div class=" row main-top-detail items pt-3 pl-3">
                        <div class="col-md-3 col-3">
                            <a href="">
                                <img src="img_detail_news/Mask.png" alt="" width="100%"></a>
                        </div>
                        <div class="col-md-9 col-9">
                            <h5 class="font-size-14 font-weight-bold">EVN Hà Nội khởi động chiến dịch mùa hè không nóng 2021</h5>
                            <p class="font-size-14">
                                <img src="img_news/calendar.png" alt=""><span>23/04/2021</span></p>
                        </div>
                    </div>



                </div>

                <div class="main-bottom-detail">
                    <div class="title-bottom-detail pt-3 pl-3">
                        <p class="font-size-15 font-weight-bold">Tin tức mới nhất</p>
                        <hr style="width: 10%; border: 1px solid #ff0000;">
                    </div>
                    <div class=" row main-bottom-detail-items pl-3">
                        <div class="col-md-3 col-3">
                            <a href="">
                                <img src="img_detail_news/moinhat.jpg" alt="" width="100%"></a>
                        </div>
                        <div class="col-md-9 col-9">
                            <h5 class="font-size-14 font-weight-bold">EVN Hà Nội khởi động chiến dịch mùa hè không nóng 2021</h5>
                            <p class="font-size-14">
                                <img src="img_news/calendar.png" alt=""><span>23/04/2021</span></p>
                        </div>
                    </div>
                    <div class=" row main-bottom-detail-items pt-3 pl-3">
                        <div class="col-md-3 col-3">
                            <a href="">
                                <img src="img_detail_news/moinhat.jpg" alt="" width="100%"></a>
                        </div>
                        <div class="col-md-9 col-9">
                            <h5 class="font-size-14 font-weight-bold">EVN Hà Nội khởi động chiến dịch mùa hè không nóng 2021</h5>
                            <p class="font-size-14">
                                <img src="img_news/calendar.png" alt=""><span>23/04/2021</span></p>
                        </div>
                    </div>
                    <div class=" row main-bottom-detail-items pt-3 pl-3">
                        <div class="col-md-3 col-3">
                            <a href="">
                                <img src="img_detail_news/moinhat.jpg" alt="" width="100%"></a>
                        </div>
                        <div class="col-md-9 col-9">
                            <h5 class="font-size-14 font-weight-bold">EVN Hà Nội khởi động chiến dịch mùa hè không nóng 2021</h5>
                            <p class="font-size-14">
                                <img src="img_news/calendar.png" alt=""><span>23/04/2021</span></p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>





<div class="scroll-btn">
    <i class="fas fa-angle-up"></i>
</div>
<script>
    const scrollBtn = document.querySelector('.scroll-btn');


    window.addEventListener('scroll', () => {
        if (document.body.scrollTop > 20 || document
            .documentElement
            .scrollTop > 20) {
            scrollBtn.style.display = 'block';
        } else {
            scrollBtn.style.display = 'none';
        }
    })
    scrollBtn.addEventListener('click', () => {
        window.scroll({
            top: 0,
            behavior: "smooth"
        })
    })
                </script>
<%-- optional --%>
<script type="text/javascript">
    function show(elementId) {
        document.getElementById("demo").style.display = "none";
        document.getElementById("demo1").style.display = "none";
        document.getElementById("demo2").style.display = "none";

        document.getElementById(elementId).style.display = "block";
    }
</script>
<script>
    var slideIndex = 1;
    showSlides(slideIndex);

    function plusSlides(n) {
        showSlides(slideIndex += n);
    }

    function currentSlide(n) {
        showSlides(slideIndex = n);
    }

    function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("demo");
        var captionText = document.getElementById("caption");
        if (n > slides.length) {
            slideIndex = 1
        }
        if (n < 1) {
            slideIndex = slides.length
        }
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex -
            1].style.display = "block";
        dots[slideIndex - 1].className += " active";
        captionText.innerHTML = dots[slideIndex -
            1].alt;
    }
</script>
<script>
    function w3_open() {
        document.getElementById("main").style.marginLeft = "250px";
        document.getElementById("mySidebar").style.width = "250px";
        document.getElementById("mySidebar").style.display = "block";
        document.getElementById("openNav").style.display = 'none';
        document.getElementById("btn_open_sliderbar").style.display = "none"
        document.getElementById("btn_open_sliderbar").style.width = "100px"
    }

    function w3_close() {
        document.getElementById("main").style.marginLeft = "0%";
        document.getElementById("mySidebar").style.display = "none";
        document.getElementById("openNav").style.display = "block";
        document.getElementById("btn_open_sliderbar").style.display = "block"
        document.getElementById("icon_thuvao").style.display = "none"
    }
</script>