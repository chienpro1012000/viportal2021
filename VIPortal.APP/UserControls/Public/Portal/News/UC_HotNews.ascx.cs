﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using VIPortalAPP;
using ViPortalData.TinNoiBat;

namespace VIPortal.APP.UserControls.Public.Portal.News
{
    public partial class UC_HotNews : BaseUC_Web
    {
        public string TitleNew = "";
        //public string output = "";
        public TinNoiBatItem oTinTucItem { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oTinTucItem = new TinNoiBatItem();
            if (ItemID > 0)
            {
                TinNoiBatDA oTinTucDA = new TinNoiBatDA(UrlSite);
                oTinTucItem = oTinTucDA.GetByIdToObject<TinNoiBatItem>(ItemID);

                if(oTinTucItem.ItemTinTuc > 0 && !string.IsNullOrEmpty( oTinTucItem.UrlList)){
                    TinTucDA oTinTucNewDA = new TinTucDA(UrlSite, oTinTucItem.UrlList.Split('/').LastOrDefault());
                    TinTucItem oTinTucItemNew = oTinTucNewDA.GetByIdToObject<TinTucItem>(oTinTucItem.ItemTinTuc);
                    oTinTucItem.ContentNews = oTinTucItemNew.ContentNews;
                }

                TitleNew = oTinTucDA.SpListProcess.Title;
                oTinTucDA.SystemUpdateOneField(ItemID, "ReadCount", ++oTinTucItem.ReadCount);
            }
        }
    }
}