﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_GioiThieu.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.News.UC_GioiThieu" %>
<style>
    .caption_news_event a {
        color: #212529;
    }

    #ContentNews img {
        width: 100% !important;
        height: auto !important
    }

    #oImageNews img {
        max-height: 110px;
    }

    .image_news-event img {
        max-height: 245px;
    }

    a {
        text-decoration: none;
    }

        a:hover {
            text-decoration: none;
        }

    .active a {
        border-radius: 0;
        padding: 0;
        text-decoration: none;
        font-size: 16px;
        background-color: transparent !important;
        /*color: #134298 !important;*/
        margin: 10px 0 0;
        font-weight: bold;
    }

    .news-img-wrap .news-box-zoom {
        height: auto;
        margin: 10px auto;
        overflow: hidden;
        position: relative;
        display: block;
    }

    .img-fluid {
        width: 350px;
        height: 250px;
        transition: transform 1.5s;
        object-fit: cover
    }

        .img-fluid:hover {
            -ms-transform: scale(1.5); /* IE 9 */
            -webkit-transform: scale(1.5); /* Safari 3-8 */
            transform: scale(1.5);
        }

    .t1-news-title {
        font-size: 16px;
        color: #154398 !important;
        line-height: 1.5;
        margin-bottom: 0;
        font-weight: bold;
    }

        .t1-news-title:hover {
            text-decoration: none;
            color: #ee1d23 !important;
            cursor: pointer;
        }

    .t1-news-date {
        font-size: 12px;
        color: #777 !important;
        margin-bottom: 0;
    }

    .t1-news-des {
        font-size: 14px;
        color: #777 !important;
        margin-bottom: 0;
    }

    .p1-news-subtitle {
        font-size: 14px;
        color: #ee1d23 !important;
        font-weight: bold
    }

        .p1-news-subtitle:hover {
            text-decoration: none;
            color: #154398 !important;
        }

    .p1-news-title {
        font-size: 16px;
        color: #154398 !important;
        margin: 10px 0 0;
        font-weight: bold;
    }

        .p1-news-title:hover {
            text-decoration: none;
        }

    .p1-news-date {
        font-size: 12px;
        color: #777 !important;
        margin-bottom: 0;
    }

    .nav-tabs .nav-link {
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        border: none;
        color: #134298 !important;
        font-size: 16px;
        font-weight: 600;
        padding: 0.5rem 1.5rem;
    }

        .nav-tabs .nav-link.active {
            background: #f8f8f8;
            position: relative;
            display: block;
            border-bottom: 3px solid #fff;
            color: #ee1d23 !important;
        }

            .nav-tabs .nav-link.active:after {
                content: "";
                background-image: -moz-linear-gradient( 0deg, rgb(21,67,152) 0%, rgb(238,29,35) 100%);
                background-image: -webkit-linear-gradient( 0deg, rgb(21,67,152) 0%, rgb(238,29,35) 100%);
                background-image: -ms-linear-gradient( 0deg, rgb(21,67,152) 0%, rgb(238,29,35) 100%);
                display: block;
                height: 3px;
                width: 100%;
                position: absolute;
                bottom: -3px;
                left: 0;
            }

    .tabs-viewmore {
        position: absolute;
        top: 20px;
        right: 0;
    }

        .tabs-viewmore a {
            color: #134298 !important;
            font-weight: bold;
        }

    @media (max-width: 768px) {
        .tabs-viewmore {
            position: unset;
            float: right;
            margin-top: -25px;
        }

        .nav {
            flex-wrap: nowrap;
        }

        .nav-tabs .nav-link {
            font-size: 16px;
        }
    }
</style>
<%if(ItemID == 0){ %>
<section>
    <div class="container-fluid mt-4 position-relative" id="content">

         <script id="template_new_detail" type="text/x-kendo-template">                              
            <div class="content-news-detail">
                    <h5 class="title-news-detail font-size-25 font-weight-bold pl-3">#=Title#
                    </h5>
                    <div class="pl-3" id="ContentNews">
                        #=ContentNews#
                    </div>
                </div>
        </script>

        <script id="template_new" type="text/x-kendo-template">                              
            <div class="row border-bottom mt-3 mb-3">
                            <div class="col-md-4">
                                <div class="news-img-wrap">
                                    <a class="news-box-zoom" href="<%=AbsolutePath%>?l=#=UrlList#&Itemid=#=ID#">
                                        <img class="img-fluid" src="#if(ImageNews != null && ImageNews.length > 0){##=ImageNews##}else{#/Content/dist/img/no-image.png#}#" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-8 mt-2">
                                <a href="<%=AbsolutePath%>?l=#=UrlList#&Itemid=#=ID#" class="t1-news-title p-0">#=Titles#</a>
                                <p class="t1-news-date mt-4">#=formatDate(Created)#</p>                                
                                <p class="t1-news-des mt-3">
                                    #if(DescriptionNews != null && DescriptionNews.length > 150){
                                        ##=DescriptionNews.substring(0,150)#...#
                                    }else{
                                        
                                            ##=DescriptionNews == null ? "":DescriptionNews ##
                                        
                                    }#
                                </p>
                                
                            </div>
                        </div>
        </script>
        <script id="template_hotnew" type="text/x-kendo-template">
            <div class="row border-bottom flex-column">
                <a href="<%=AbsolutePath%>?l=#=UrlList#&Itemid=#=ID#" class="p1-news-subtitle" style="color: red !important;">#=DanhMucTin#</a>                
                <a href="<%=AbsolutePath%>?l=#=UrlList#&Itemid=#=ID#" class="p1-news-title">#=Title#</a>
                <p class="p1-news-date mt-3 mb-4">#=formatDateTime(CreatedDate)#</p>
            </div>
        </script>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">

            <%int i=0; %>
            <%foreach(var item in lstDanhMuc){ %>
            <li class="nav-item " data-tab-target="#tab-<%=i %>" style="cursor: pointer;">
                <a class="nav-link" <%--href="#home"--%> onclick="showTinTuc('tab-<%=i %>');"><%=item.Title %></a>
            </li>
            <%i++; %>
            <%} %>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content mt-0">
            <%int j = 0; %>
            <%foreach(var item in lstDanhMuc){ %>
            <div class="container-fluid main-news-event" data-group="TinTuc" id="tab-<%=j %>" data-urllist="<%=item.UrlList %>">
                <div id="listnew<%=item.UrlList %>" data-role="fullGrid" data-urllist="<%=item.UrlList %>">
                    <div class="box-wrap-search-page -style01" style="padding-right: 0px !important">
                        <div role="search" class="box-search -adv row" id="frmsearch<%=item.UrlList %>">
                            <input type="hidden" name="UrlListTinTuc" value="<%=item.UrlList %>" />
                            <input type="hidden" name="isTichHop" value="<%=item.isTichHop %>" />
                            <input type="hidden" name="DanhMucTin" value="<%=item.Title %>" />
                        </div>
                    </div>
                    <div class="row" >
                        <div role="grid" class="col-md-12" id="NewItemOne<%=item.UrlList %>">
                        </div>
                        <!--<div class="col-md-4" id="hotnew_newlist">
                        </div>-->
                        <div class="clspaging">
                        </div>
                    </div>
                </div>
            </div>
            <%j++; %>
            <%} %>
        </div>
        <div class="tabs-viewmore">
            <a href="<%=UrlSite %>/Pages/hotnews.aspx">Xem thêm >></a>
        </div>
    </div>

</section>
<script type="text/javascript">
    //$(".left_header_calendar [data-tab-target='#tab-0']").addClass("active");
    $(".nav-tabs [data-tab-target='#tab-0'] .nav-link").addClass("active");
    function showTinTuc(idtab) {
        $("#content .nav-link").removeClass("active");
        $(".nav-tabs [data-tab-target='#" + idtab + "'] .nav-link").addClass("active");
        $(".main-news-event").css("display", "none");
        $("#" + idtab).css("display", "block");
    }
    $(document).ready(function () {
        var json = JSON.parse('<%=output%>');
        $.each(json, function (index, value) {
            var id = "#listnew" + value.UrlList + "";
            var tab_index = "#tab-" + index + "";
            if (index == 0) {
                $(id).smGrid2018({
                    "TypeTemp": "kendo",
                    "UrlPost": "/VIPortalAPI/api/News/QUERYDATA",
                    pageSize: 8,
                    odata: { "do": "QUERYDATA" },
                    template: "template_new",
                    dataBound: function (e) {
                        //console.log(e);
                        if ('<%=Config%>' != '' && '<%=Config%>' != null) {
                            if (e.data.length == 1) {
                                $.post("/VIPortalAPI/api/News/GetByIdNew", { "UrlListTinTuc": e.data[0].UrlList, "ItemID": e.data[0].ID }, function (result) {
                                    var template_new_detail = $("#template_new_detail").html();
                                    template_new_detail = kendo.template(template_new_detail);
                                    var resulthtml = template_new_detail(result); //Pass the data to the compiled template
                                    $("#NewItemOne" + e.data[0].UrlList).html(resulthtml); //display the result
                                    //clspaging
                                });
                            }
                        }

                    },
                });
            } else {
                $(tab_index).css("display", "none")
                setTimeout(
                    function () {
                        $(id).smGrid2018({
                            "TypeTemp": "kendo",
                            "UrlPost": "/VIPortalAPI/api/News/QUERYDATA",
                            pageSize: 20,
                            odata: { "do": "QUERYDATA" },
                            template: "template_new",
                            dataBound: function (e) {
                                console.log(e);
                                if ('<%=Config%>' != '' && '<%=Config%>' != null) {
                                    if (e.data.length == 1) {
                                        $.post("/VIPortalAPI/api/News/GetByIdNew", { "UrlListTinTuc": e.data[0].UrlList, "ItemID": e.data[0].ID }, function (result) {
                                            var template_new_detail = $("#template_new_detail").html();
                                            template_new_detail = kendo.template(template_new_detail);
                                            var resulthtml = template_new_detail(result); //Pass the data to the compiled template
                                            $("#NewItemOne" + e.data[0].UrlList).html(resulthtml); //display the result
                                            //clspaging
                                        });
                                    }
                                }
                            },
                        });
                    }, 10000);
            }
        });
    });
</script>
<%}else{ %>
<section>
    <div class="main-news-detail bg-color-box-items pb-3">
        <div class="row">
            <div class="col-md-6">
                <div class="header-news-detail">
                    <p class="font-size-14 color-table-meeting pl-3 pt-2 pb-2">
                        <a href="/<%=UrlSite %>">Trang chủ </a>- <a href="<%=AbsolutePath%>?l=<%=Request["l"] %>" class="color-table-meeting"><%=TitleNew %> </a>
                    </p>
                </div>
                <div class="content-news-detail">
                    <h5 class="title-news-detail font-size-25 font-weight-bold pl-3"><%=oTinTucItem.Title %>
                    </h5>
                    <p class="font-size-14 pl-3">
                        <img src="/Content/themeV1/img_news/calendar.png" alt="">
                        <span style="vertical-align: middle;"><%=string.Format("{0:dd/MM/yyyy}", oTinTucItem.CreatedDate)%></span>
                        <span>
                            <a class="send-mail" style="cursor: pointer; float: right">
                                <i class="fa fa-envelope" aria-hidden="true" style="margin-right: 2px"></i>Gửi email 
                            </a>
                        </span>
                    </p>
                </div>
                <div>
                    <h6 class="title-item-news font-size-15 pl-3"><%=oTinTucItem.DescriptionNews %></h6>
                    <div class="pl-3" id="ContentNews">
                        <%=oTinTucItem.ContentNews %>
                    </div>
                    <%if(oTinTucItem.ListFileAttach.Count > 0){ %>
                    <div class="pl-3">
                        <div class="author-news pt-3 col-md-3">
                            Tài liệu kèm theo:
                        </div>
                        <div class="author-news pt-3 col-md-9">
                            <p class="font-size-15 font-weight-bold">
                                <%for(int i=0; i< oTinTucItem.ListFileAttach.Count;i++){ %>
                                <a href="<%=oTinTucItem.ListFileAttach[i].Url %>"><%=oTinTucItem.ListFileAttach[i].Name%></a>
                                <br />
                                <%} %>
                            </p>
                        </div>
                        <div class="author-news pt-3 col-md-10">
                            <p class="font-size-15 font-weight-bold"><%=oTinTucItem.TacGia %></p>
                        </div>
                    </div>
                    <%} %>
                </div>
            </div>
            <div class="col-md-6">
                <div class="main-top-detail">
                    <div class="title-top-detail pl-3 pt-2 pb-2">
                        <p class="font-size-18 font-weight-bold">Bài đọc nhiều</p>
                        <hr style="width: 10%; border: 1px solid #ff0000">
                    </div>
                    <script id="template_newdocnhieu" type="text/x-kendo-template">
                              <div class=" row main-top-detail items pt-3 pl-3">
                        <div class="col-md-3 col-3 boximgdetail" id="oImageNews ">
                            <a href="<%=AbsolutePath%>?l=<%=Request["l"] %>&ItemID=#=ID#">
                                <img src="#=ImageNews#" alt="" width="100%"></a>
                        </div>
                        <div class="col-md-9 col-9">
                            <h5 class="font-size-14 font-weight-bold"><a href="<%=AbsolutePath%>?l=<%=Request["l"] %>&ItemID=#=ID#" class="color-table-meeting">#=Title#</a></h5>
                            <p class="font-size-14 pt-3">
                                <img src="/Content/themeV1/img_news/calendar.png" alt=""> <span>#=formatDate(Created)#</span></p>
                        </div>
                    </div>
                    </script>
                    <div id="listnewread">
                    </div>
                </div>

                <div class="main-bottom-detail">
                    <div class="title-bottom-detail pt-3 pl-3">
                        <p class="font-size-15 font-weight-bold">Tin tức mới nhất</p>
                        <hr style="width: 10%; border: 1px solid #ff0000;">
                    </div>
                    <div id="lstNewscreate">
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Modal send mail-->
    <div class="modal" tabindex="-1" role="dialog" id="sendmail">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" style="font-weight: bold; font-size: 15px">Gửi mail</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="f-send">
                        <input type="hidden" name="link" id="link" value="https://<%=HttpContext.Current.Request.Url.Authority%>/Pages/TinTuc.aspx?Itemid=<%=oTinTucItem.ID %>" />
                        <div class="form-group">
                            <label for="email">Email người nhận:</label>
                            <input type="text" class="form-control" name="lmailto" id="lmailto">
                        </div>
                        <div class="form-group">
                            <label for="pwd">Tiêu đề:</label>
                            <input type="text" class="form-control" name="subject" id="subject">
                        </div>
                        <div class="form-group">
                            <label for="pwd">Nội dung mail:</label>
                            <textarea rows="3" class="form-control" name="content" id="contentMail"></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            <button type="button" class="btn-primary" id="btnSendmail">Gửi email</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        $.post("/VIPortalAPI/api/News/QUERYDATA", { "FieldOrder": "ReadCount", Ascending: false, "length": 5, "UrlListTinTuc": "<%=Request["l"] %>", "isTichHop": "<%=isTichHop%>" }, function (odata) {
            //xử lý phần stt.
            var DataCustom = odata.data;
            var templateContent = $("#template_newdocnhieu").html();
            var template = kendo.template(templateContent);
            var result = kendo.render(template, DataCustom); //render the template
            $("#listnewread").html(result); //append the result to the page
            $('#listnewread div:first').removeClass('pt-3');

        });
        $.post("/VIPortalAPI/api/News/QUERYDATA", { "FieldOrder": "CreatedDate", Ascending: false, "length": 5, "UrlListTinTuc": "<%=Request["l"] %>", "isTichHop": "<%=isTichHop%>" }, function (odata) {
            //xử lý phần stt.
            var DataCustom = odata.data;
            var templateContent = $("#template_newdocnhieu").html();
            var template = kendo.template(templateContent);
            var result = kendo.render(template, DataCustom); //render the template
            $("#lstNewscreate").html(result); //append the result to the page
            $('#lstNewscreate div:first').removeClass('pt-3');

        });
        $(document).on('click', ".send-mail", function (event) {
            $('#sendmail').modal('show');
        });
        var strTemp = "Tiêu đề tin: <%=oTinTucItem.Titles%>." + "\n";
        $("#contentMail").val(strTemp);
        $("#btnSendmail").click(function () {
            debugger
            //$("#f-send").submit();  
            var subject = $("#subject").val();
            var lmailto = $("#lmailto").val();
            var content = $("#contentMail").val();
            var link = $("#link").val().toString();
            if (lmailto == "" && subject == "") {
                BootstrapDialog.show({
                    title: "Thông báo",
                    message: "Bạn cần nhập đầy đủ thông tin"
                });
                return false;
            } else {
                loading();
                $.post("/UserControls/AdminCMS/TinTuc/pAction.asp", "do=SENDMAIL&UrlSite=/noidung&UrlListNews=<%=Request["l"] %>&lmailto=" + lmailto + "&subject=" + subject + "&content=" + content + "&link=" + link, function (result) {
                    if (result.State == 2) {
                        $("#f-send").trigger('reset');
                        BootstrapDialog.show({
                            title: "Lỗi",
                            message: result.Message
                        });
                    }
                    else {
                        openDialogMsg("Thông báo", result.Message);
                        Endloading();
                        $("#f-send").trigger('reset');
                        $('#sendmail').modal('hide');
                    }
                }).always(function () { });
            }
        });
    });
</script>
<%} %>