﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_HotNews.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.News.UC_HotNews" %>
<style>
    .caption_news_event a {
        color: #212529;
    }

    #ContentNews img {
        width: 100% !important;
        height: auto !important;
    }

    #oImageNews img {
        max-height: 110px;
    }

    .image_news-event img {
        max-height: 245px;
    }
</style>
<%if(ItemID == 0){ %>
<section class="news-event">
    <div class="newsevent" id="content">
        <script id="template_new" type="text/x-kendo-template">
            <div class="col-md-3">
                  <div class="image_news-event">
                      <a class="clslink" href="<%=UrlSite %>/Pages/hotnews.aspx?l=#=UrlList#&ItemID=#=ID#"><img src="#if(ImageNews.length > 0){##=ImageNews##}else{#/Content/dist/img/no-image.png#}#" alt="#=Title#"></a>
                  </div>
                  <div class="caption_news_event pt-3">
                      <h5 class="font-size-15 font-weight-bold"><a class="clslink" href="<%=UrlSite %>/Pages/hotnews.aspx?ItemID=#=ID#">#if(Title.length > 90){##=Title.substring(0,90)#...#}else{##=Title##}#</a></h5>
                      <p class="font-size-14 calendar-news-event"><img src="/Content/themeV1/img_news/calendar.png" alt=""><span class="pl-3 pt-1">#=formatDate(CreatedDate)#</span></p>
                  </div>
            </div>
        </script>
        <div class="header-announcement">
            <p class="pt-3 pl-3 font-size-14 color-table-meeting">Tin nổi bật</p>
        </div>
        <div class="main-news-event" data-group="TinTuc" data-urllist="TinNoiBat">
            <div id="listnew" data-role="fullGrid" data-urllist="TinNoiBat">
                <div class="box-wrap-search-page -style01" style="padding-right: 0px !important">
                    <div role="search" class="box-search -adv row" id="frmsearchTinNoiBat">
                        <input type="hidden" name="UrlListTinTuc" value="TinNoiBat" />
                    </div>
                </div>
                <div role="grid" class="row">
                </div>
                <div class="clspaging">
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        function loopLi() {
            setInterval(function () { // this code is executed every 500 milliseconds:
                var current = '<%=UrlSite %>/Pages/HomePortal.aspx';
                var actived = false;
                $('#items_sliderbar li a').each(function () {
                    var $this = $(this);
                    // if the current path is like this link, make it active
                    if ($this.attr('href').indexOf(current) !== -1) {
                        $this.closest("li").addClass('active');
                        actived = true;
                    }
                });
                if (actived) return;
            }, 500);
        }

        $(loopLi);
        $("#listnew").smGrid2018({
            "TypeTemp": "kendo",
            "UrlPost": "/VIPortalAPI/api/News/QUERYDATAHomeSolr",
            pageSize: 10,
            odata: { "do": "QUERYDATAHomeSolr" },
            template: "template_new",
            dataBound: function (e) {
            },
        });
    });
</script>
<%}else{ %>
<section>
    <div class="main-news-detail bg-color-box-items pb-3">
        <div class="row">
            <div class="col-md-6">
                <div class="header-news-detail">
                    <p class="font-size-14 color-table-meeting pl-3 pt-2 pb-2">
                        <a href="/<%=UrlSite %>">Trang chủ </a>- <a href="<%=UrlSite %>/Pages/hotnews.aspx">Tin nổi bật</a>
                    </p>
                </div>
                <div class="content-news-detail">
                    <h5 class="title-news-detail font-size-25 font-weight-bold pl-3"><%=oTinTucItem.Title %>
                    </h5>
                    <p class="font-size-14 pl-3">
                        <img src="/Content/themeV1/img_news/calendar.png" alt=""> <span style="vertical-align: middle;"><%=string.Format("{0:dd/MM/yyyy}", oTinTucItem.CreatedDate)%></span>
                    </p>
                </div>
                <div>
                    <h6 class="title-item-news font-size-15 pl-3"><%=oTinTucItem.DescriptionNews %></h6>          
                    <div class="pl-3" id="ContentNews">
                        <%=oTinTucItem.ContentNews %>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="main-top-detail">
                    <div class="title-top-detail pl-3 pt-2 pb-2">
                        <p class="font-size-18 font-weight-bold">Tin khác</p>
                        <hr style="width: 10%; border: 1px solid #ff0000">
                    </div>
                    <script id="template_newdocnhieu" type="text/x-kendo-template">
                              <div class=" row main-top-detail items pt-3 pl-3">
                        <div class="col-md-3 col-3" id="oImageNews">
                            <a href="<%=UrlSite %>/Pages/hotnews.aspx?l=#=UrlList#&ItemID=#=ID#">
                                <img src="#=ImageNews#" alt="" width="100%"></a>
                        </div>
                        <div class="col-md-9 col-9">
                            <h5 class="font-size-14 font-weight-bold"><a href="<%=UrlSite %>/Pages/hotnews.aspx?l=#=UrlList#&ItemID=#=ID#">#=Title#</a></h5>
                            <p class="font-size-14">
                                <img src="/Content/themeV1/img_news/calendar.png" alt=""> <span>#=formatDate(CreatedDate)#</span></p>
                        </div>
                    </div>
                    </script>
                    <div id="listnewread">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">

    $(document).ready(function () {
        function loopLi() {
            setInterval(function () { // this code is executed every 500 milliseconds:
                var current = '/Pages/HomePortal.aspx';
                var actived = false;
                $('#items_sliderbar li a').each(function () {
                    var $this = $(this);
                    // if the current path is like this link, make it active
                    if ($this.attr('href').indexOf(current) !== -1) {
                        $this.closest("li").addClass('active');
                        actived = true;
                    }
                });
                if (actived) return;
            }, 500);
        }

        $(loopLi);
        $.post("/VIPortalAPI/api/News/QUERYDATAHomeSolr", { "FieldOrder": "ReadCount", Ascending: false, "length": 5 }, function (odata) {
            //xử lý phần stt.
            var DataCustom = odata.data;
            var templateContent = $("#template_newdocnhieu").html();
            var template = kendo.template(templateContent);
            var result = kendo.render(template, DataCustom); //render the template
            $("#listnewread").html(result); //append the result to the page
            $('#listnewread div:first').removeClass('pt-3');

        });
    });
</script>
<%} %>