﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using VIPortalAPP;
using ViPortalData.Lconfig;

namespace VIPortal.APP.UserControls.Public.Portal.News
{
    public partial class UC_GioiThieu : BaseUC_Web
    {
        public string TitleNew = "";
        public string output = "";
        public bool isTichHop = false;
        public TinTucItem oTinTucItem { get; set; }
        public string AbsolutePath { get; set; }
        public List<DanhMucThongTinJson> lstDanhMuc { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            AbsolutePath = HttpContext.Current.Request.Url.AbsolutePath;
            lstDanhMuc = new List<DanhMucThongTinJson>();
            oTinTucItem = new TinTucItem();
            if (!string.IsNullOrEmpty(Request["isTichHop"]))
            {
                isTichHop = Convert.ToBoolean(Request["isTichHop"]);
            }
            if (ItemID > 0 && !string.IsNullOrEmpty(Request["l"]))
            {
                UrlList = Request["l"];
                if (isTichHop)
                {
                    LconfigDA lconfigDA = new LconfigDA();
                    string APIUrlEVNHaNoiNew = lconfigDA.GetValueConfigByType("APIUrlEVNHaNoiNew");
                    string Keynew = Request["Keynew"];
                    //https://apicskh.evnhanoi.com.vn/api/PublicNews/GetNewsDetailByNewsKey?key=evnhanoi-trang-bi-he-thong-am-thanh-canh-bao-thong-minh-tu-dong-tu-xa-cho-cac-tram-bien-ap-khong-nguoi-truc
                    string APIEVNHanoiNew = $"{APIUrlEVNHaNoiNew}/api/PublicNews/GetNewsDetailByNewsKey?key={Keynew}";
                    HttpClient client = new HttpClient();
                    HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
                    response.EnsureSuccessStatusCode();
                    string responseBody = response.Content.ReadAsStringAsync().Result;
                    NewShare newShare = Newtonsoft.Json.JsonConvert.DeserializeObject<NewShare>(responseBody);
                    TitleNew = newShare.newsCategoryName;
                    oTinTucItem = new TinTucItem()
                    {
                        Title = newShare.newsTitle,
                        CreatedDate = newShare.newsPublishedDate,
                        ContentNews = newShare.newsContent,
                        TacGia = newShare.newsAuthor
                    };
                }
                else
                {
                    TinTucDA oTinTucDA = new TinTucDA(UrlSite, Request["l"]);
                    oTinTucItem = oTinTucDA.GetByIdToObject<TinTucItem>(ItemID);
                    TitleNew = oTinTucDA.SpListProcess.Title;
                    oTinTucDA.SystemUpdateOneField(ItemID, "ReadCount", ++oTinTucItem.ReadCount);
                }
            }
            else
            {
                if (string.IsNullOrEmpty(Config))
                {
                    //add thêm ở đoạn này các danh mục thông tin.
                    LconfigDA lconfigDA = new LconfigDA(UrlSite);
                    List<LconfigJson> lconfigJsons = lconfigDA.GetListJson(new LconfigQuery() { ConfigGroup = "DanhMucTinEVNHaNoiShare", _ModerationStatus = 1 });
                    lstDanhMuc.AddRange(lconfigJsons.Select(x => new DanhMucThongTinJson()
                    {
                        ID = x.ID + 999,
                        Title = x.Title,
                        isTichHop = true,
                        UrlList = x.ConfigValue
                    }));
                }
                DanhMucThongTinDA oDanhMucTTDA = new DanhMucThongTinDA(UrlSite);
                DanhMucThongTinQuery oDanhMucThongTinQuery =  new DanhMucThongTinQuery() { _ModerationStatus = 1 };
                if (!string.IsNullOrEmpty(Config))
                {
                    oDanhMucThongTinQuery.DBPhanLoai = Convert.ToInt32(Config);
                }
                else oDanhMucThongTinQuery.DBPhanLoai = 9999;
                List<DanhMucThongTinJson> lstDanhMucTin = oDanhMucTTDA.GetListJson(oDanhMucThongTinQuery) ;
                lstDanhMuc.AddRange(lstDanhMucTin);
                output = JsonConvert.SerializeObject(lstDanhMuc);
            }
        }
    }
}