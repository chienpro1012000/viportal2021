﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_Briefings.ascx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.Briefings.UC_Briefings" %>
<%if(ItemID == 0){ %>
<section id="list_thongbaoketluan" data-role="fullGrid" class="Briefings">
    <div class="briefings bg-color-box-items header-briefing">
        <div class="header-briefings">
            <p class="font-size-14 color-table-meeting pl-3 pt-2" ><span data-lang="TrangChu">Trang chủ</span> - <span data-lang="KetLuanCuocHop">Kết luận cuộc họ</span>p</p>
        </div>
        <div class="form-briefings pl-3 pr-3 pt-3 pb-3">
            <div action="" class="row formsearchpublic" id="frmSearchKL" role="search">
                <div class="col-md-4 items-form">
                    <label for="" class="font-size-16 color-table-meeting font-weight-bold" data-lang="TenVanBan">
                        Tên văn bản</label><br>
                    <input type="text" name="Keyword" id="Keyword" style="width: 100%" data-lang="PlaceSearch" placeholder="Nhập tên văn bản ...">
                    <input type="hidden" name="SearchInSimple" id="SearchInSimple" value="Title" />
                </div>
                <div class="col-md-5 items-form">
                    <label for="TuNgay_TBNgayRaThongBao" class="font-size-16 color-table-meeting font-weight-bold" data-lang="ThoiGianBanHanh">
                        Thời gian ban hành</label><br>
                    <div class="row">
                        <div class="col-md-4" style="padding-right: 0">
                            <input type="text" id="TuNgay_TBNgayRaThongBao" class="input-datetime" data-lang="TuNgay" placeholder=" Từ ngày" name="TuNgay_TBNgayRaThongBao" style="width: 100%"></div>
                        <div class="col-md-4 text-center" style="padding: 0">
                            <label for="DenNgay_QCNgayDang" class="pt-1" ><span data-lang="DenNgay">Đến ngày</span>:</label></div>
                        <div class="col-md-4" style="padding-left: 0">
                            <input type="text" id="DenNgay_TBNgayRaThongBao" class="input-datetime" data-lang="DenNgay" placeholder=" Đến ngày" name="DenNgay_TBNgayRaThongBao" style="width: 100%"></div>
                    </div>
                </div>
                <div class="col-md-3 btn d-flex align-items-end justify-content-start button-searh-reset">
                     <label for=""></label>
                      <br>
                      <button data-toggle="tooltip" data-placement="top" title="Xóa lọc tất cả"  class="reset_data"><i class="fas fa-sync-alt"></i></button>
                      <label for=""></label>
                      <br>
                      <button type="button" class="btnsearch" data-lang="TimKiem">Tìm kiếm</button>
                </div>
            </div>
        </div>
    </div>
    <script id="template_TBKL" type="text/x-kendo-template">
                             <tr class="font-size-14px">
                        <td class="font-weight-bold color-table-meeting index"></td>
                        <td class="color-calendar-table">Tháng #=new Date(TBNgayRaThongBao).format('MM/yyyy')#</td>
                        <td style="text-align:left"><a href="?ItemID=#=ID#">#=TBTrichYeu#</a></td>
                        <td>#=TBSoKyHieu#</td>
                        <td>#=formatDate(TBNgayRaThongBao)#</td>
                        <td>
                            <a target="_blank" href="#=ListFileAttach.length > 0 ? ListFileAttach[0].Url: '' #">
                               #=ListFileAttach.length > 0 ? '<img src="/Content/themeV1/img/icon/file.jpg" alt="">' : ''# </a>
                        </td>
                    </tr>
    </script>
    <div class="text-center table-briefings ml-3 mr-3 pt-5 pb-3 box-shadow-basic">
        <table class="table table-bordered">
            <thead>
                <tr class="font-size-16 color-table-meeting font-weight-bold bg-color-header-meeting-tr">
                    <th style="width: 50px;" data-lang="STT">STT</th>
                    <th style="width: 140px;" ><span data-lang="Thang">Tháng</span>/<span data-lang="Nam">Năm</span></th>
                    <th data-lang="TrichYeu">Trích yếu</th>
                    <th data-lang="SoKyHieu">Số Ký hiệu</th>
                    <th style="width: 140px;" data-lang="NgayVanBan">Ngày văn bản</th>
                    <th data-lang="TaiFile">Tải file</th>
                </tr>
            </thead>
            <tbody role="grid">
            </tbody>
        </table>
        <div class="clspaging">
        </div>
    </div>

</section>

<script type="text/javascript">
    //list_thongbaoketluan
    $(document).ready(function () {
        SetConfigLang('<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>', 'Briefings');
        $("#list_thongbaoketluan").smGrid2018({
            "TypeTemp": "kendo",
            "UrlPost": "/VIPortalAPI/api/QuyCheNoiBo/QUERYDATA_THONGBAOKETLUAN",
            
            pageSize: 20,
            odata: { "do": "QUERYDATA" },
            template: "template_TBKL",
            dataBound: function (e) {
                $("td.index").each(function (index) {
                    $(this).text(++index);
                });
            },
        });
        $("#frmSearchKL").SMValidate({
            rules: {
                TuNgay_TBNgayRaThongBao: "date",
                DenNgay_TBNgayRaThongBao: { greaterThan: "#TuNgay_TBNgayRaThongBao", dateFormat: true }
            }
        });
    });
</script>
<%}else{ %>
     
    <div class="vanban-detail bg-color-box-items box-shadow-basic UCBriefings">
        <div class="header-notification-detail">
            <p class="font-size-14 color-table-meeting pl-3 pt-2"><a href="/Pages/HomePortal.aspx">Trang chủ</a> - <a href="/Pages/ketluangiaoban.aspx" data-lang="KetLuanCuocHop">Kết luận cuộc họp</a>  - <a href="#" data-lang="ChiTietKetKuanCuocHop" >Chi tiết kết luận cuộc họp</a></p>
        </div>
        <div class="main-notification-detail display-flex pl-3 pr-3">
            <table class="table table-bordered">
                <tr>
                    <td class="th" style="width:15%;" data-lang="SoKyHieu">Số ký hiệu</td>
                    <td><%=oThongBaoKetLuanItem.TBSoKyHieu %></td>
                </tr>
                <tr>
                    <td class="th">Trích yếu</td>
                    <td><%=oThongBaoKetLuanItem.TBTrichYeu %></td>
                </tr>
                <tr>
                    <td class="th" data-lang="NgayBanHanh">Ngày ban hành</td>
                    <td><%=string.Format("{0:dd/MM/yyyy}", oThongBaoKetLuanItem.TBNgayRaThongBao) %></td>
                </tr>
                
                <tr>
                    <td class="th" data-lang="LanhDao">Lãnh đạo</td>
                    <td><%=oThongBaoKetLuanItem.TBLanhDaoKetLuan %></td>
                </tr>
                 <tr>
                    <td class="th" data-lang="FileDinhKem">File đính kèm</td>
                    <td>
                        <%for(int i=0; i< oThongBaoKetLuanItem.ListFileAttach.Count;i++){ %>
                        <div class="file">
                            <ul>
                                <li><i class="fas fa-download" style="color:#26b2fa"></i><a href="<%=oThongBaoKetLuanItem.ListFileAttach[i].Url %>" style="font-size: 13px; color: #337ab7;"><%=oThongBaoKetLuanItem.ListFileAttach[i].Name%></a></li>
                            </ul>
                        </div>
                        <%} %>
                    </td>
                </tr>
              <tr>
                    <td class="th" data-lang="NoiDung">Nội dung</td>
                    <td><%=oThongBaoKetLuanItem.TBNoiDung %></td>
                </tr>
               
            </table>
        </div>
    </div>

<%} %>
