﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData.ThongBaoKetLuan;

namespace VIPortal.APP.UserControls.Public.Portal.Briefings
{
    public partial class UC_Briefings : BaseUC_Web
    {
        public ThongBaoKetLuanItem oThongBaoKetLuanItem { get; private set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            oThongBaoKetLuanItem = new ThongBaoKetLuanItem();
            if (ItemID > 0)
            {
                ThongBaoKetLuanDA thongBaoKetLuanDA = new ThongBaoKetLuanDA(UrlSite);
                oThongBaoKetLuanItem = thongBaoKetLuanDA.GetByIdToObject<ThongBaoKetLuanItem>(ItemID);
            }
        }
    }
}