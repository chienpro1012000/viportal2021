﻿<%@ page language="C#" autoeventwireup="true" codebehind="frmGroupChat.aspx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.Chat.frmGroupChat" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #listDanhBaUser .clspaging {
            display: none;
        }

        .list {
            padding-left: 0;
            padding-right: 0
        }

        .list-item {
            position: relative;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: column;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word
        }
        .template-download{
            opacity:1 !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#frm-GroupChat").validate({
                ignore: [],
                rules: {
                    Title: "required",
                    UserThamGia: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tên nhóm",
                    UserThamGia: "Vui lòng chọn thành viên nhóm",
                },
                submitHandler: function (form) {
                    $.post("/UserControls/Chat/ChatHopThoai/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            LoadDanhBa();
                            $(form).closeModal();

                        }
                    }).always(function () { });
                }
            });
            $("#FileAttachDaiDien").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oChatHopThoai.ListFileAttach)%>',
                preview: true,
                OnlyOne: true
            });

            $("#listDanhBaUser").smGrid2018({
                "TypeTemp": "kendo",
                "UrlPost": "/UserControls/AdminCMS/LUser/pAction.asp",
                pageSize: 300,
                odata: {
                    "do": "QUERYDATA",
                    "Groups": "<%=CurentUser.Groups.ID%>",
                    "ItemIDNotGet": "<%=CurentUser.ID%>"
                },
                template: "template_danhba_choice",
                dataBound: function (e) {
                    //checked thông tin cái này.
                    //
                    var usergiamgiaid = $("#UserThamGia_Id").val();
                    $('#checkboxuser input[type=checkbox]').each(function () {
                        var $idcheckbox = $(this).attr('data-userid');
                        if (usergiamgiaid.indexOf("," + $idcheckbox + ",") > -1) {   // Returns 13
                            $(this).prop("checked", true);
                        }
                    });

                    //đăng ký sự kiện checkbox cho các checkbox để thêm xóa khỏi danh sách.
                    $('#checkboxuser input[type=checkbox]').change(function () {
                        if (this.checked) {
                            var $UserThamGia = $("#frm-GroupChat #UserThamGia").val();
                            var $UserThamGia_Id = $("#frm-GroupChat #UserThamGia_Id").val();
                            var $id = $(this).attr("data-userid");
                            var $Title = $(this).attr("Title");
                            $UserThamGia += `;#${$id};#${$Title}`;
                            $UserThamGia_Id += `${$id},`;
                            $("#frm-GroupChat #UserThamGia").val($UserThamGia);
                            $("#frm-GroupChat #UserThamGia_Id").val($UserThamGia_Id);
                        }
                        else {
                            var $UserThamGia = $("#frm-GroupChat #UserThamGia").val();
                            var $UserThamGia_Id = $("#frm-GroupChat #UserThamGia_Id").val();
                            var $id = $(this).attr("data-userid");
                            var $Title = $(this).attr("Title");
                            $UserThamGia = $UserThamGia.replace(`;#${$id};#${$Title}`, "");
                            $UserThamGia_Id = $UserThamGia_Id.replace(`,${$id},`, "");
                            $("#frm-GroupChat #UserThamGia").val($UserThamGia);
                            $("#frm-GroupChat #UserThamGia_Id").val($UserThamGia_Id);
                        }
                    });
                },
            });
        });
    </script>
</head>
<body>
    <script id='template_danhba_choice' type="text/x-kendo-template">
        <div class="list-group-item" for="user_#=ID#" chathopthoai_id="0" data-title="#=Title#" data-userid="#=ID#" id="list-user_0">
                    <div class="row main-left  pb-2 mt-3 active-mess">
                        <div class="col-md-2 pl-4">
                            <input type="checkbox" id="user_#=ID#" Title="#=Title#" name="GroupUser" data-userid=#=ID# value="user_#=ID#" />
                        </div>
                        <div class="col-md-2 pl-4">
                            <div class="img-left-mess ">
                                <img src="#=GetPathUrlString(AnhDaiDien)#" alt="">
                            </div>
                        </div>
                        <div class="col-md-8 pr-4">
                            <div class="display-flex-righteous pl-3">
                                <div class="content-left-mess">
                                    <p class="font-size-15 color-table-meeting margin-p font-weight-bold">
                                       <label for="user_#=ID#">  #=Title#</label>
                                    </p>
                                    <p class="font-size-14 margin-p lastmessage"></p>
                                </div>
                                <div class="time-mess pt-1">
                                    <p class="font-size-13 margin-p"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    </script>
    <form id="frm-GroupChat" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oChatHopThoai.ID %>" />
        <input type="hidden" name="isGroupChat" id="isGroupChat" value="True" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group">
            <label for="Title">Tên nhóm</label>
            <input type="text" class="form-control" id="Title" name="Title" value="<%=oChatHopThoai.Title %>" placeholder="Nhập tên nhóm" />

        </div>
        <div class="form-group">
            <label for="Title">Ảnh đại diện</label>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <input style="width:auto;" type="file" value="Chọn ảnh" id="FileAttachDaiDien" name="FileAttachDaiDien" multiple="multiple" />
            </div>
        </div>
        <div class="form-group">
            <label for="Title">Mời bạn tham gia nhóm</label>
            <input type="hidden" name="UserThamGia" id="UserThamGia" value="<%=oChatHopThoai.UserThamGia %>" />
            <input type="hidden" name="UserThamGia_Id" id="UserThamGia_Id" value="<%=oChatHopThoai.UserThamGia_Id %>" />
            <div id="listDanhBaUser" data-role="fullGrid" data-urllist="">
                <div role="search" class="box-search -adv row" id="frmsearch">
                    <input type="hidden" name="UrlListTinTuc" value="" />

                </div>
                <div role="grid" id="checkboxuser" class="list-group mt-3">
                </div>
                <div class="clspaging">
                </div>
            </div>
        </div>

    </form>
</body>
</html>
