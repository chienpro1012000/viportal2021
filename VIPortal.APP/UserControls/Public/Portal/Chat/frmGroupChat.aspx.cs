﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using VIPortalAPP;

namespace VIPortal.APP.UserControls.Public.Portal.Chat
{
    public partial class frmGroupChat : pFormBase
    {
        public ChatHopThoaiItem oChatHopThoai { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            oChatHopThoai = new ChatHopThoaiItem();
            if (ItemID > 0)
            {
                ChatHopThoaiDA chatHopThoaiDA = new ChatHopThoaiDA();
                oChatHopThoai = chatHopThoaiDA.GetByIdToObject<ChatHopThoaiItem>(ItemID);
            }
            else
            {
                oChatHopThoai.UserThamGia = Request["UserThamGia"];
                oChatHopThoai.UserThamGia_Id = Request["UserThamGia_Id"];
            }
        }
    }
}