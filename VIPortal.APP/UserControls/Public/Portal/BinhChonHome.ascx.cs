﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData.KSBoCauHoi;
using ViPortalData.KSBoChuDe;
using ViPortalData.KSCauTraLoi;

namespace VIPortal.APP.UserControls.Public.Portal
{
    public partial class BinhChonHome : BaseUC_Web
    {
        public List<KSBoChuDeJson> lstBoChuDe { get; set; }
        public List<KSBoCauHoiJson> lstBoCauHoi { get; set; }


        public BinhChonHome()
        {
            lstBoCauHoi = new List<KSBoCauHoiJson>();
            lstBoChuDe = new List<KSBoChuDeJson>();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            lstBoChuDe = new List<KSBoChuDeJson>();
            lstBoCauHoi = new List<KSBoCauHoiJson>();
            KSBoChuDeDA oKSBoChuDeDA = new KSBoChuDeDA(UrlSite);
            KSBoCauHoiDA oKSBoCauHoi = new KSBoCauHoiDA(UrlSite);
            lstBoChuDe = oKSBoChuDeDA.GetListJson(new KSBoChuDeQuery() { FieldOrder = "KSDenNgay", Ascending=false, _ModerationStatus = 1});
            if (lstBoChuDe.Count > 0)
            {
                lstBoCauHoi = oKSBoCauHoi.GetListJson(new KSBoCauHoiQuery() { IdChuDe = lstBoChuDe[0].ID, _ModerationStatus = 1 });
            }
        }
        public List<KSCauTraLoiJson> GetCauTraLoiByID(int ItemID)
        {
            List<KSCauTraLoiJson> lstCauTraLoi = new List<KSCauTraLoiJson>();
            KSCauTraLoiDA objCauTraLoi = new KSCauTraLoiDA(UrlSite);
            lstCauTraLoi = objCauTraLoi.GetListJson(new KSCauTraLoiQuery() { IDCauHoi = ItemID });
            return lstCauTraLoi;
        }
    }
}