﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_Image_Details.ascx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.ImagesPage.UC_Image_Details" %>
 <section class="img-category">
                    <div class="img-category-title">
                        <div class="lib-category-title-fire"></div>
                        <div class="img-category-title-lib1">
                            <a href="#">Trang chủ</a> - <a href="#">Thư viện ảnh</a> -
                            <a href="#">Danh mục 1</a>
                        </div>
                    </div>
                    <div class="img-category-item">
                        <div class="img-category-item-lib">
                            <div class="lib-image-item">
                                <a href="#">
                                    <div class="lib-image-item-img">
                                        <img src="img_lib_media/img_gn@2x.png" alt="" />
                                    </div>
                                    <div class="lib-image-item-content">
                                        <div class="lib-image-item-content-title">
                                            ENV Hà Nội khởi động chiến dịch màu hè không nóng 2021.
                                        </div>
                                        <div class="lib-image-item-content-days">
                                            <i class="far fa-calendar-alt"></i>&nbsp; 23/04/2021
                                            <div class="lib-image-item-content-days-num">
                                                23 ảnh
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="img-category-item-lib">
                            <div class="lib-image-item">
                                <a href="#">
                                    <div class="lib-image-item-img">
                                        <img src="img_lib_media/img_gn@2x.png" alt="" />
                                    </div>
                                    <div class="lib-image-item-content">
                                        <div class="lib-image-item-content-title">
                                            ENV Hà Nội khởi động chiến dịch màu hè không nóng 2021.
                                        </div>
                                        <div class="lib-image-item-content-days">
                                            <i class="far fa-calendar-alt"></i>&nbsp; 23/04/2021
                                            <div class="lib-image-item-content-days-num">
                                                23 ảnh
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="img-category-item-lib">
                            <div class="lib-image-item">
                                <a href="#">
                                    <div class="lib-image-item-img">
                                        <img src="img_lib_media/img_gn@2x.png" alt="" />
                                    </div>
                                    <div class="lib-image-item-content">
                                        <div class="lib-image-item-content-title">
                                            ENV Hà Nội khởi động chiến dịch màu hè không nóng 2021.
                                        </div>
                                        <div class="lib-image-item-content-days">
                                            <i class="far fa-calendar-alt"></i>&nbsp; 23/04/2021
                                            <div class="lib-image-item-content-days-num">
                                                23 ảnh
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="img-category-item-lib">
                            <div class="lib-image-item">
                                <a href="#">
                                    <div class="lib-image-item-img">
                                        <img src="img_lib_media/img_gn@2x.png" alt="" />
                                    </div>
                                    <div class="lib-image-item-content">
                                        <div class="lib-image-item-content-title">
                                            ENV Hà Nội khởi động chiến dịch màu hè không nóng 2021.
                                        </div>
                                        <div class="lib-image-item-content-days">
                                            <i class="far fa-calendar-alt"></i>&nbsp; 23/04/2021
                                            <div class="lib-image-item-content-days-num">
                                                23 ảnh
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="img-category-item-lib">
                            <div class="lib-image-item">
                                <a href="#">
                                    <div class="lib-image-item-img">
                                        <img src="img_lib_media/img_gn@2x.png" alt="" />
                                    </div>
                                    <div class="lib-image-item-content">
                                        <div class="lib-image-item-content-title">
                                            ENV Hà Nội khởi động chiến dịch màu hè không nóng 2021.
                                        </div>
                                        <div class="lib-image-item-content-days">
                                            <i class="far fa-calendar-alt"></i>&nbsp; 23/04/2021
                                            <div class="lib-image-item-content-days-num">
                                                23 ảnh
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="img-category-item-lib">
                            <div class="lib-image-item">
                                <a href="#">
                                    <div class="lib-image-item-img">
                                        <img src="img_lib_media/img_gn@2x.png" alt="" />
                                    </div>
                                    <div class="lib-image-item-content">
                                        <div class="lib-image-item-content-title">
                                            ENV Hà Nội khởi động chiến dịch màu hè không nóng 2021.
                                        </div>
                                        <div class="lib-image-item-content-days">
                                            <i class="far fa-calendar-alt"></i>&nbsp; 23/04/2021
                                            <div class="lib-image-item-content-days-num">
                                                23 ảnh
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="img-category-item-lib">
                            <div class="lib-image-item">
                                <a href="#">
                                    <div class="lib-image-item-img">
                                        <img src="img_lib_media/img_gn@2x.png" alt="" />
                                    </div>
                                    <div class="lib-image-item-content">
                                        <div class="lib-image-item-content-title">
                                            ENV Hà Nội khởi động chiến dịch màu hè không nóng 2021.
                                        </div>
                                        <div class="lib-image-item-content-days">
                                            <i class="far fa-calendar-alt"></i>&nbsp; 23/04/2021
                                            <div class="lib-image-item-content-days-num">
                                                23 ảnh
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="img-category-item-lib">
                            <div class="lib-image-item">
                                <a href="#">
                                    <div class="lib-image-item-img">
                                        <img src="img_lib_media/img_gn@2x.png" alt="" />
                                    </div>
                                    <div class="lib-image-item-content">
                                        <div class="lib-image-item-content-title">
                                            ENV Hà Nội khởi động chiến dịch màu hè không nóng 2021.
                                        </div>
                                        <div class="lib-image-item-content-days">
                                            <i class="far fa-calendar-alt"></i>&nbsp; 23/04/2021
                                            <div class="lib-image-item-content-days-num">
                                                23 ảnh
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="img-category-item-lib">
                            <div class="lib-image-item">
                                <a href="#">
                                    <div class="lib-image-item-img">
                                        <img src="img_lib_media/img_gn@2x.png" alt="" />
                                    </div>
                                    <div class="lib-image-item-content">
                                        <div class="lib-image-item-content-title">
                                            ENV Hà Nội khởi động chiến dịch màu hè không nóng 2021.
                                        </div>
                                        <div class="lib-image-item-content-days">
                                            <i class="far fa-calendar-alt"></i>&nbsp; 23/04/2021
                                            <div class="lib-image-item-content-days-num">
                                                23 ảnh
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="img-category-item-lib">
                            <div class="lib-image-item">
                                <a href="#">
                                    <div class="lib-image-item-img">
                                        <img src="img_lib_media/img_gn@2x.png" alt="" />
                                    </div>
                                    <div class="lib-image-item-content">
                                        <div class="lib-image-item-content-title">
                                            ENV Hà Nội khởi động chiến dịch màu hè không nóng 2021.
                                        </div>
                                        <div class="lib-image-item-content-days">
                                            <i class="far fa-calendar-alt"></i>&nbsp; 23/04/2021
                                            <div class="lib-image-item-content-days-num">
                                                23 ảnh
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="img-category-item-lib">
                            <div class="lib-image-item">
                                <a href="#">
                                    <div class="lib-image-item-img">
                                        <img src="img_lib_media/img_gn@2x.png" alt="" />
                                    </div>
                                    <div class="lib-image-item-content">
                                        <div class="lib-image-item-content-title">
                                            ENV Hà Nội khởi động chiến dịch màu hè không nóng 2021.
                                        </div>
                                        <div class="lib-image-item-content-days">
                                            <i class="far fa-calendar-alt"></i>&nbsp; 23/04/2021
                                            <div class="lib-image-item-content-days-num">
                                                23 ảnh
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="img-category-item-lib">
                            <div class="lib-image-item">
                                <a href="#">
                                    <div class="lib-image-item-img">
                                        <img src="img_lib_media/img_gn@2x.png" alt="" />
                                    </div>
                                    <div class="lib-image-item-content">
                                        <div class="lib-image-item-content-title">
                                            ENV Hà Nội khởi động chiến dịch màu hè không nóng 2021.
                                        </div>
                                        <div class="lib-image-item-content-days">
                                            <i class="far fa-calendar-alt"></i>&nbsp; 23/04/2021
                                            <div class="lib-image-item-content-days-num">
                                                23 ảnh
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="img-category-item-lib">
                            <div class="lib-image-item">
                                <a href="#">
                                    <div class="lib-image-item-img">
                                        <img src="img_lib_media/img_gn@2x.png" alt="" />
                                    </div>
                                    <div class="lib-image-item-content">
                                        <div class="lib-image-item-content-title">
                                            ENV Hà Nội khởi động chiến dịch màu hè không nóng 2021.
                                        </div>
                                        <div class="lib-image-item-content-days">
                                            <i class="far fa-calendar-alt"></i>&nbsp; 23/04/2021
                                            <div class="lib-image-item-content-days-num">
                                                23 ảnh
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="img-category-item-lib">
                            <div class="lib-image-item">
                                <a href="#">
                                    <div class="lib-image-item-img">
                                        <img src="img_lib_media/img_gn@2x.png" alt="" />
                                    </div>
                                    <div class="lib-image-item-content">
                                        <div class="lib-image-item-content-title">
                                            ENV Hà Nội khởi động chiến dịch màu hè không nóng 2021.
                                        </div>
                                        <div class="lib-image-item-content-days">
                                            <i class="far fa-calendar-alt"></i>&nbsp; 23/04/2021
                                            <div class="lib-image-item-content-days-num">
                                                23 ảnh
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="img-category-item-lib">
                            <div class="lib-image-item">
                                <a href="#">
                                    <div class="lib-image-item-img">
                                        <img src="img_lib_media/img_gn@2x.png" alt="" />
                                    </div>
                                    <div class="lib-image-item-content">
                                        <div class="lib-image-item-content-title">
                                            ENV Hà Nội khởi động chiến dịch màu hè không nóng 2021.
                                        </div>
                                        <div class="lib-image-item-content-days">
                                            <i class="far fa-calendar-alt"></i>&nbsp; 23/04/2021
                                            <div class="lib-image-item-content-days-num">
                                                23 ảnh
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </section>