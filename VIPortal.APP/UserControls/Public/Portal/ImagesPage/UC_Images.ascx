﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_Images.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.ImagesPage.UC_Images" %>

<style type="text/css">

    /*css modal show*/

    .mdHeaderimg{
        background-color:#fff;
    }
    .mdBodyimg{
        padding-top:1px !important
    }







    .modal-img-slier .carousel-item p {
        text-align: center;
        padding-top: 15px;
    }

    .title-type-1 {
        font-size: 16px !important;
        color: #134197;
        padding: 20px 0;
        border-bottom: 1px solid #ebebeb;
        position: relative;
        margin-bottom: 30px;
        font-weight: bold;
    }

        .title-type-1::after {
            content: "";
            width: 80px;
            height: 4px;
            background-image: -moz-linear-gradient( 0deg, rgb(21,67,152) 0%, rgb(238,29,35) 100%);
            background-image: -webkit-linear-gradient( 0deg, rgb(21,67,152) 0%, rgb(238,29,35) 100%);
            background-image: -ms-linear-gradient( 0deg, rgb(21,67,152) 0%, rgb(238,29,35) 100%);
            position: absolute;
            left: 40px;
            bottom: -2px;
            margin: 0px 0 0 -40px;
        }

    .lib-media_img-wrap {
        width: 100%;
        height: 100%;
        position: relative;
    }

    .lib-media_img {
        width: 100%;
        height: 235px;
    }

    .lib-media_img-info {
        background-color: rgba(0, 0, 0, .4);
        position: absolute;
        text-align: center;
        left: 0;
        right: 0;
        top: 0;
        margin: auto;
        bottom: 0;
    }

        .lib-media_img-info:hover {
            cursor: pointer;
        }

        .lib-media_img-info h4 {
            color: #fff;
            font-size: 15px;
            line-height: 1.3;
            margin-bottom: 10px;
            margin-top: 165px !important;
            font-weight: bold;
        }

    .lib-media_img-num {
        background: rgba(255,255,255,.3);
        padding: 5px 10px;
        border-radius: 12px;
        display: inline-block;
        font-size: 14px;
        color: #fff;
        line-height: 1;
        font-weight: bold;
    }

    .owl-carousel .owl-nav button.owl-next,
    .owl-carousel .owl-nav button.owl-prev {
        background: 0 0;
        color: #ffffff !important;
        border: none;
        padding: 0 !important;
        font: icon;
        font-size: 30px;
        width: 40px;
        height: 40px;
        background-color: #0e2f6a !important;
        border-radius: 20px;
    }

    button.owl-next {
        position: absolute;
        right: -65px !important;
        top: 180px !important;
    }

    button.owl-prev {
        position: absolute;
        left: -65px !important;
        top: 180px !important;
    }

    .owl-theme .owl-nav [class*=owl-]:hover {
        background: #0e2f6a !important;
        color: #FFF;
        text-decoration: none;
    }

    #lib-inner {
        background-color: black;
    }

    @media (max-width: 576px) {
        button.owl-next {
            position: absolute;
            right: 20px !important;
            top: 180px !important;
        }

        button.owl-prev {
            position: absolute;
            left: 20px !important;
            top: 180px !important;
        }

        .owl-theme .owl-dots .owl-dot span {
            width: 20px;
            height: 20px;
        }
    }
</style>

<section class="img-category Images" id="imggird" data-role="fullGrid">
    <h3 class="title-type-1" style="font-size:16px !important" data-lang="AlbumHinhAnh">Album hình ảnh</h3>
    <script id="template_img" type="text/x-kendo-template">
        <div class="col-lg-3 mt-3 lib-image-item-img1" id="gallery" data-ItemIDDB="#=ID#">
            <div class="lib-media_img-wrap">
                <img class="lib-media_img" src="#=ImageNews#" alt="#=Title#" data-target="##carouselExample">
                <div class="lib-media_img-info">
                    <h4>#=Title#</h4>
                    <div class="lib-media_img-num">#=DMSLAnh#</div>
                </div>
            </div>
        </div>
    </script>
    <div class="row mt-3" role="grid">
        
    </div>
    <div class="row mt-lg-3 mt-0">
        
    </div>
</section>

<%--<div class="category-lib" id="imggird" data-role="fullGrid" role="search">
    <div class="tab-content-lib">
        <div id="lib-image" data-tab-content="" class="active">
            <div class="lib-nub1">
                <div class="lib-category">
                    <div class="lib-category-title">
                        <div class="lib-category-title-fire"></div>
                        <div class="lib-category-title-name">
                            <a href="#">Trang chủ</a> - <a href="#">Thư viện hình ảnh</a>
                        </div>
                        <h3 class="title-type-1">Album hình ảnh</h3>
                    </div>
                </div>
                <div class="box-wrap-search-page -style01" style="padding-right: 0px !important; padding-top: 15px">
                    <div role="search" class="box-search" id="frmsearchvideo">
                        <div action="" method="post" class="display-flex formsearchpublic">
                            <div class="items-form">
                                <input type="text" name="keyword" placeholder="Nhập từ khóa">
                                <input type="hidden" name="SearchInAdvance" value="Titles">
                            </div>
                            <div class="select-1" id="DanhMucVideo">
                                <select data-selected="" class="form-control" data-url="/UserControls/AdminCMS/DMHinhAnh/pAction.asp?do=QUERYDATA&moder=1" data-place="Chọn danh mục" name="Album" id="Album"></select>
                            </div>
                            <div class="icon-button display-flex pl-2">
                                 <button data-toggle="tooltip" data-placement="top" title="Xóa lọc tất cả"  class="reset_data"><i class="fas fa-sync-alt"></i></button>
                                <button type="button" id="search" class="btnsearch btn-search-meeting">Tìm kiếm</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="grid" class="lib-image ">
                </div>
                <div class="clspaging">
                </div>
            </div>
        </div>
        <script id="template_img" type="text/x-kendo-template">  
                <div class="lib-image-item pt-3 pl-1 pr-1 pb-3 ">
                    <div class="boxitem-video box-shadow-basic">
                        <div class="lib-image-item-img" data-ItemIDDB="#=ID#">
                        <img src="#=ImageNews#" alt="" class="image" >
                        <div class="overlay">
                            <div class="text"><img src="/Content/themeV1/icon/Path 3148.png" alt="">
                                <p class="margin-p font-size-10 font-weight-bold">#=DMSLAnh# ảnh</p>
                            </div>
                        </div>
                    </div>
                    <div class="lib-image-item-content">
                        <div class="lib-image-item-content-title">
                            #=Title#
                        </div>
                        <div class="lib-image-item-content-days">
                             <i class="far fa-calendar-alt"></i><span class="pl-2">#=formatDate(TBNgayDang)#</span>
                            <div class="lib-image-item-content-days-num">
                                #=DMSLAnh# ảnh
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
        </script>
    </div>
</div>--%>

<script>
    $(document).ready(function () {
        SetConfigLang('<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>', 'Images');
        $("#Album").smSelect2018V2({
            dropdownParent: "#DanhMucVideo"
        });
        $("#imggird").smGrid2018({
            "TypeTemp": "kendo",
            "UrlPost": "/VIPortalAPI/api/HinhAnhVideo/QUERYDATA",
            pageSize: 20,
            odata: { "do": "QUERYDATA" },
            template: "template_img",
            dataBound: function (e) {
                $(".lib-image-item-img1").click(function () {
                    var ItemIDDB = $(this).attr("data-ItemIDDB");
                    $.post("/VIPortalAPI/api/HinhAnhVideo/HINHANH_QUERYDATA", { Album: ItemIDDB }, function (result) {
                        if (result.data.length > 0) {
                            $("#myModal1 .titledanhmucimage").html(result.data[0].Album.Title);
                        }
                        $("#myModal1 div.carousel-inner").html('');
                        $("#myModal1 ol.carousel-indicators").html('');
                        $("#myModal1 .titledanhmucimage").html();
                        $.each(result.data, function (index, value) {
                            //alert(index + ": " + value);
                            $("#myModal1 ol.carousel-indicators").append('<li data-target="#modal1" data-slide-to="' + index + '" class=""></li>');
                            $("#myModal1 ol.carousel-indicators li").first().addClass("active");
                            $("#myModal1 div.carousel-inner").append('<div class="carousel-item"><img class="d-block w-100" src="' + value.ImageNews + '"><p>' + value.Title + '</p></div>');
                            $("#myModal1 div.carousel-inner .carousel-item").first().addClass("active");

                        });
                        $("#myModal1").modal('show');
                    }).always(function () { });


                });
            },
        });

    });
</script>

<!-- The modal1 -->
<div class="modal" id="myModal1">
    <%--<span class="close cursor" data-dismiss="modal">&times;</span>--%>
    <div class="main-panoha-3D">
        <!-- Modal Header -->
        
        <!-- Modal body -->
        <div class="main-img-modal">
            <div class="modal-header mdHeaderimg">
                <h5 class="modal-title">Danh sách hình ảnh</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div id="modal1" class="carousel slide modal-img-slier mdBodyimg" data-ride="carousel" data-interval="false">
                <p class="text-center font-weight-bold font-size-25 titledanhmucimage">Không có bản ghi nào</p>
                <ol class="carousel-indicators">
                    <li data-target="#modal1" data-slide-to="0" class="active"></li>
                    <li data-target="#modal1" data-slide-to="1"></li>
                    <li data-target="#modal1" data-slide-to="2"></li>
                    <li data-target="#modal1" data-slide-to="3"></li>
                    <li data-target="#modal1" data-slide-to="4"></li>
                    <li data-target="#modal1" data-slide-to="5"></li>
                    <li data-target="#modal1" data-slide-to="6"></li>
                    <li data-target="#modal1" data-slide-to="7"></li>
                    <li data-target="#modal1" data-slide-to="8"></li>
                </ol>
                <div class="carousel-inner">
                    
                </div>
                <a class="carousel-control-prev modal-a" href="#modal1" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next modal-a" href="#modal1" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</div>

<script>

    $('.owl-carousel').owlCarousel({
        items: 1,
        merge: true,
        loop: true,
        margin: 10,
        video: true,
        lazyLoad: true,
        center: true,
        responsive: {
            0: {
                items: 1
            },

        }
    })

</script>

<script>

    var owl = $('.owl-carousel');
    owl.owlCarousel();
    // Go to the next item
    $('.customNextBtn').click(function () {
        owl.trigger('next.owl.carousel');
    })
    // Go to the previous item
    $('.customPrevBtn').click(function () {
        // With optional speed parameter
        // Parameters has to be in square bracket '[]'
        owl.trigger('prev.owl.carousel', [300]);
    })

</script>
