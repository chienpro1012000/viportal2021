﻿<%@ control language="C#" autoeventwireup="true" codebehind="LichLamViec.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.LichLamViec" %>


<script type="text/javascript">
    $(document).ready(function () {
        SetConfigLang('<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>', 'LichLamViecDIv');
        var KieuDonvi = '<%=oDonVi.KieuDonVi%>';
        //get weekend
        //convert Sun Aug 15 2021 00:00:00 GMT+0700 (Indochina Time) {} to dd/MM/yyyy
        function convert(str) {
            var date = new Date(str),
                mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                day = ("0" + date.getDate()).slice(-2);
            return [day, mnth, date.getFullYear()].join("/");
        }
        var today = new Date();
        var todayNumber = today.getDay();
        var mondayNumber = 1 - todayNumber;
        var sundayNumber = 7 - todayNumber;
        var monday = new Date(today.getFullYear(), today.getMonth(), today.getDate() + mondayNumber);
        var sunday = new Date(today.getFullYear(), today.getMonth(), today.getDate() + sundayNumber);
        var firstdayformat = convert(today);
        var lastdayformat = convert(today);
        ///UserControls/Forum/ForumChuDe/QUERYDATAHomeSolr?do=MENUTREE
        $.post("/VIPortalAPI/api/LichLamViec/QUERYDATA_LICHTONGCTY", { "FieldOrder": "CreatedDate", Ascending: false, "length": 0, "TuNgay_LichThoiGianBatDau": firstdayformat, "DenNgay_LichThoiGianBatDau": lastdayformat }, function (odata) {
            if (odata.data.length > 0) {
                var templateContent = $("#jsTemplateLichlamviecTong").html();
                var template = kendo.template(templateContent);
                var result = kendo.render(template, odata.data); //render the template
                $("#tab").html(result); //append the result to the page
            } 
        });
        $.post("/VIPortalAPI/api/LichLamViec/QUERYDATA_DONVI", { "FieldOrder": "CreatedDate", Ascending: false, "length": 0, "isLichTrangThai": true, "fldGroup": <%=CurentUser.Groups.ID%>, "TuNgay_LichThoiGianBatDau": firstdayformat, "DenNgay_LichThoiGianBatDau": lastdayformat }, function (odata) {
            if (odata.data.length > 0) {
                var templateContent = $("#jsTemplateLichlamviecDonVi").html();
                var template = kendo.template(templateContent);
                var result = kendo.render(template, odata.data); //render the template
                $("#tab-one").html(result); //append the result to the page
            } 
        });
        if (KieuDonvi == '2') {
            $("#lichPhong").hide();
        }
        setTimeout(function () {
            
            if (KieuDonvi != '2') {
                $.post("/VIPortalAPI/api/LichLamViec/QUERYDATA_PHONG", { "FieldOrder": "CreatedDate", Ascending: false, "length": 0, "isLichTrangThai": true, "fldGroup": <%=CurentUser.UserPhongBan.ID%> , "TuNgay_LichThoiGianBatDau": firstdayformat, "DenNgay_LichThoiGianBatDau": lastdayformat }, function (odata) {
                    if (odata.data.length > 0) {
                        var templateContent = $("#jsTemplateLichlamviecPhong").html();
                        var template = kendo.template(templateContent);
                        var result = kendo.render(template, odata.data); //render the template
                        $("#tab-two").html(result); //append the result to the page
                    } 7
                });
            } else {
                
            }
            $.post("/VIPortalAPI/api/LichLamViec/QUERYDATA_LICHCANHAN", { "FieldOrder": "LichThoiGianBatDau", Ascending: true, "length": 0, "isGetLichTiepTheo": true, "CreatedUser": <%=CurentUser.ID%>, "TuNgay_LichThoiGianBatDau": firstdayformat, "DenNgay_LichThoiGianBatDau": lastdayformat }, function (odata) {
                if (odata.data.length > 0) {
                    var templateContent = $("#jsTemplateLichlamviecCaNhan").html();
                    var template = kendo.template(templateContent);
                    var result = kendo.render(template, odata.data); //render the template
                    $("#tab-three").html(result); //append the result to the page
                } 
            });
            $("#lstSinhNhatCBNV").smGrid2018({
                "TypeTemp": "kendo",
                "UrlPost": "/VIPortalAPI/api/LUser/GETDATA_SINHNHAT_CBNV",
                //pageSize: 10,
                odata: { "ID_DONVI": '<%=MaDonViSelected %>', "PhongBan": '<%=MaPhongBanSelected %>', "length": 4, "SoNgayLay": 7 },
                     template: "jsTemplateSinhNhatCBNV",
                     dataBound: function (e) {
                     },
                 });
        }, 1000);



    });
    function showTinTuc(idtab) {
        $("#content .item_li_1").removeClass("active");
        $(".left_header_calendar [data-tab-target='#" + idtab + "']").addClass("active");
        $(".main-lich-event").css("display", "none");
        $("#" + idtab).css("display", "block");
    }
    function GetTTLich(LogText, DBPhanLoai) {
        var strKetQua = '';
        if (LogText != null) {
            if (LogText.includes("_TT-ho_")) {
                strKetQua = "<span class='LichHoan'>Hoãn</span>";
            }
            else if (LogText.includes("_TT-bs_")) {
                strKetQua = "<span class='LichBoSung'>Bổ sung</span>";
            }
            else if (LogText.includes("_TT-dt_")) {
                strKetQua = "<span class='LichThayDoi'>Thay đổi</span>";
            } else {
                if (DBPhanLoai == 1) {
                    strKetQua = "<span class='LichBoSung'>Bổ sung</span>";
                } else if (DBPhanLoai == 2) {
                    strKetQua = "<span class='LichHoan'>Hoãn</span>";
                } else if (DBPhanLoai == 3) {
                    strKetQua = "<span class='LichThayDoi'>Thay đổi</span>";
                }
            }
        }

        return strKetQua;
    }
    //  js nhảy tab
</script>
<%if(!string.IsNullOrEmpty(HtmlOutput)){ %>
<%=HtmlOutput %>
<%}else{ %>


<style>
    .margin-p {
        margin: 0;
        padding: 0;
    }

    .imgBirday-Lt {
        width: 90px;
        height: 125px;
    }

        .imgBirday-Lt img {
            width: 100%;
            height: 100%;
            object-fit: cover;
        }

    .title-info-Lt {
        font-weight: 700;
        font-size: 15px;
        overflow: hidden;
        text-overflow: ellipsis;
        -webkit-line-clamp: 2;
        -webkit-box-orient: vertical;
        display: -webkit-box;
    }

    .address-info-Lt {
        font-weight: 500;
        font-size: 12px;
        overflow: hidden;
        text-overflow: ellipsis;
        -webkit-line-clamp: 2;
        -webkit-box-orient: vertical;
        display: -webkit-box;
    }

    .border-after {
        position: relative;
    }

        .border-after::before {
            content: "";
            position: absolute;
            top: 7px;
            left: 0;
            border-bottom: 1px solid #f7f7f7;
            width: 100%;
        }

        .border-after:first-child::before {
            display: none;
        }

    #lstSinhNhat {
        max-height: 444px
    }

    @media screen and (max-width: 1024px) and (min-width: 768px) {
        .imgBirday-Lt {
            width: 100%;
            height: 100%;
        }

        .title-info-Lt, .address-info-Lt {
            font-size: 11px;
        }
    }

    /*.lstListBirthday {
        max-height: 580px;
        overflow-y: scroll;
    }*/

    .nav-tabs .nav-link {
        font-size: 13px !important
    }

    .calendar-persional .nav-item .active {
        color: blue !important;
        font-weight: bold;
    }

    .scrollbar123 {
        float: left;
        max-height: 600px !important;
        width: 100%;
        overflow-y: scroll;
        margin-bottom: 15px;
    }

    .scrollbar12 {
        float: left;
        max-height: 600px !important;
        width: 100%;
        overflow-y: scroll;
        margin-bottom: 15px;
        min-height: 553px;
    }

    /*.force-overflow{
	    min-height: 450px;
    }*/

    /*scroll birtday*/
    #style-123::-webkit-scrollbar-track {
        background-color: #F5F5F5;
        border-radius: 10px;
    }

    #style-123::-webkit-scrollbar {
        width: 8px;
        background-color: #fff;
        cursor: pointer
    }

    #style-123::-webkit-scrollbar-thumb {
        border-radius: 10px;
        background-color: #ccc;
    }


    /*scroll datalist table metting*/
    #tab::-webkit-scrollbar-track {
        background-color: #F5F5F5;
        border-radius: 10px;
    }

    #tab::-webkit-scrollbar {
        width: 8px;
        background-color: #fff;
        cursor: pointer
    }

    #tab::-webkit-scrollbar-thumb {
        border-radius: 10px;
        background-color: #ccc;
    }

    /*tab-one*/

    #tab-one::-webkit-scrollbar-track {
        background-color: #F5F5F5;
        border-radius: 10px;
    }

    #tab-one::-webkit-scrollbar {
        width: 8px;
        background-color: #fff;
        cursor: pointer
    }

    #tab-one::-webkit-scrollbar-thumb {
        border-radius: 10px;
        background-color: #ccc;
    }

    /*tab-two*/

    #tab-two::-webkit-scrollbar-track {
        background-color: #F5F5F5;
        border-radius: 10px;
    }

    #tab-two::-webkit-scrollbar {
        width: 8px;
        background-color: #fff;
        cursor: pointer
    }

    #tab-two::-webkit-scrollbar-thumb {
        border-radius: 10px;
        background-color: #ccc;
    }

    /*tab-three*/
    #tab-three::-webkit-scrollbar-track {
        background-color: #F5F5F5;
        border-radius: 10px;
    }

    #tab-three::-webkit-scrollbar {
        width: 8px;
        background-color: #fff;
        cursor: pointer
    }

    #tab-three::-webkit-scrollbar-thumb {
        border-radius: 10px;
        background-color: #ccc;
    }

    li.display-webkit-box svg {
        color: #969494;
        width: 13px;
    }
</style>
<div class="e-row-mr-5 LichLamViecDIv">
    <div class="calendar-global  e-colum-pd-5">
        <div class="card box-shadow-basic sub_ss_main_one" id="content">
            <div class="header_calendar pl-3 pr-3 pt-3 pb-3">
                <div class="left_header_calendar">
                    <div class="list_calendar">
                        <div class="item_li_1 active" data-tab-target="#tab">
                            <a class="tab-index" style="cursor: pointer;" onclick="showTinTuc('tab');">
                                <%--<img src="/Content/themeV1/icon/calendarV2.png" alt="" class="pb-1">--%>
                                <span class="pl-2" data-lang="LichTongTitle">Lịch tổng</span> </a>
                        </div>
                        <div class="item_li_1" id="tabdonvi" data-tab-target="#tab-one">
                            <a class="tab-index" style="cursor: pointer;" onclick="showTinTuc('tab-one');" data-lang="LichDonViTitle">Đơn vị</a>
                        </div>
                        <div class="item_li_1" data-tab-target="#tab-two" id="lichPhong">
                            <a class="tab-index" style="cursor: pointer;" onclick="showTinTuc('tab-two');" data-lang="PhongTitle">Phòng</a>
                        </div>
                        <div class="item_li_1" data-tab-target="#tab-three">
                            <a class="tab-index" style="cursor: pointer;" onclick="showTinTuc('tab-three');" data-lang="CaNhanTitle">Cá nhân</a>
                        </div>
                    </div>
                </div>
            </div>
            <script id="jsTemplateLichlamviecTong" type="text/x-kendo-template">
                <div class="calendar_items  pt-3 border-basic pb-3">
                    <a href="<%=UrlSite %>/Pages/meeting.aspx?Lich=TongCty"><h6 class="font-size-15 pl-3 color-table-meeting font-size-15 font-weight-bold">#=Title#
                    </h6></a>
                    <div class="list_calendar_items ">
                        <ul class="calendar_info pl-3">
                            <li class="time-calendar display-webkit-box">
                                <img src="/Content/themeV1/icon/clock.png" alt=""><span class="pl-2">#=formatDateTime(LichThoiGianBatDau)# #=LichThoiGianKetThuc != null ? ("- " + formatDateTime(LichThoiGianKetThuc)) : ""#</span></li>
                            <li class="display-webkit-box">
                                <i class="fas fa-signal"></i><span class="pl-2">#=GetTTLich(LogText,DBPhanLoai)#</span>
                            </li>
                             <li class="display-webkit-box">
                               <i class="far fa-user"></i><span class="pl-2">#=CHU_TRI != null ? CHU_TRI : "" #</span>
                            </li>
                <li class="display-webkit-box">
                               <i class="fas fa-user-circle"></i><span class="pl-2">#=CHUAN_BI != null ? CHUAN_BI : ""#</span>
                            </li>
                
                            <li class="display-webkit-box">
                                <img src="/Content/themeV1/icon/pin.png" alt=""><span class="pl-2">#=LichDiaDiem#</span>
                            </li>
                            <li class="display-webkit-box">
                                <img src="/Content/themeV1/icon/group (2).png" alt=""><span class="pl-2">#= THANH_PHAN != null ? THANH_PHAN : ""#</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </script>
            <script id="jsTemplateLichlamviecDonVi" type="text/x-kendo-template">
                <div class="calendar_items  pt-3 border-basic pb-3">
                    <a href="<%=UrlSite %>/Pages/meeting.aspx?Lich=DonVi"><h6 class="font-size-15 pl-3 color-table-meeting font-size-15 font-weight-bold">#=Title#
                    </h6></a>
                    <div class="list_calendar_items ">
                        <ul class="calendar_info pl-3">
                            <li class="time-calendar display-webkit-box">
                                <img src="/Content/themeV1/icon/clock.png" alt=""><span class="pl-2">#=formatDateTime(LichThoiGianBatDau)#  #=LichThoiGianKetThuc != null ? ("- " + formatDateTime(LichThoiGianKetThuc)) : ""#</span></li>
                            <li class="display-webkit-box">
                                <i class="fas fa-signal"></i><span class="pl-2">#=GetTTLich(LogText,DBPhanLoai)#</span>
                            </li>            
                            <li class="display-webkit-box">
                               <i class="far fa-user"></i><span class="pl-2">#=CHU_TRI_Title#</span>
                            </li>
                            <li class="display-webkit-box">
                               <i class="fas fa-user-circle"></i><span class="pl-2">#=CHUAN_BI_Title#</span>
                            </li>
                
                            <li class="display-webkit-box">
                                <img src="/Content/themeV1/icon/pin.png" alt=""><span class="pl-2">#=LichDiaDiem#</span>
                            </li>
                            <li class="display-webkit-box">
                                <img src="/Content/themeV1/icon/group (2).png" alt=""><span class="pl-2">#=THANH_PHAN_Title#</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </script>
            <script id="jsTemplateLichlamviecPhong" type="text/x-kendo-template">
                <div class="calendar_items  pt-3 border-basic pb-3">
                    <a href="<%=UrlSite %>/Pages/meeting.aspx?Lich=Phong"><h6 class="font-size-15 pl-3 color-table-meeting font-size-15 font-weight-bold">#=Title#
                    </h6></a>
                    <div class="list_calendar_items ">
                        <ul class="calendar_info pl-3">
                            <li class="time-calendar display-webkit-box">
                                <img src="/Content/themeV1/icon/clock.png" alt=""><span class="pl-2">#=formatDateTime(LichThoiGianBatDau)#  #=LichThoiGianKetThuc != null ? ("- " + formatDateTime(LichThoiGianKetThuc)) : ""#</span></li>
                            <li class="display-webkit-box">
                                <i class="fas fa-signal"></i><span class="pl-2">#=GetTTLich(LogText,DBPhanLoai)#</span>
                            </li>            
                            <li class="display-webkit-box">
                               <i class="far fa-user"></i><span class="pl-2">#=CHU_TRI_Title#</span>
                            </li>
                            <li class="display-webkit-box">
                               <i class="fas fa-user-circle"></i><span class="pl-2">#=CHUAN_BI_Title#</span>
                            </li>
                
                            <li class="display-webkit-box">
                                <img src="/Content/themeV1/icon/pin.png" alt=""><span class="pl-2">#=LichDiaDiem#</span>
                            </li>
                            <li class="display-webkit-box">
                                <img src="/Content/themeV1/icon/group (2).png" alt=""><span class="pl-2">#=THANH_PHAN_Title#</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </script>
            <div class="main-lich-event scrollbar123"  id="tab">
            </div>
            <div class="main-lich-event scrollbar123"  id="tab-one">
            </div>
            <div class="main-lich-event scrollbar123"  id="tab-two">
            </div>
            <div class="main-lich-event scrollbar123"  id="tab-three" style="display: none;">
            </div>
        </div>
    </div>
    <div class="calendar-persional e-colum-pd-5">
        <div class="card box-shadow-basic sub_ss_main_one">
            <%--Viết sinh nhật ở đây nhé--%>
            <section>
                <div class="header_birday display-flex ">
                    <p class="pl-3 font-size-20 font-weight-bold color-table-meeting margin-p" data-lang="ChucMungSinhNhatTitle">
                        Chúc mừng sinh nhật
                    </p>
                </div>
                <div class="container mt-4 position-relative scrollbar12" id="style-123">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#lstSinhNhat" data-lang="SinhNhatTongCongTyTitle">Tổng công ty</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#lstSinhNhatCBNV" data-lang="SinhNhatDonViTitle">Đơn vị</a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content mt-0 lstListBirthday ">
                        <div id="lstSinhNhat" class="container-fluid tab-pane active">
                            <div class="force-overflow"></div>
                            <div class="" role="grid">
                            </div>
                        </div>

                        <div id="lstSinhNhatCBNV" class="container-fluid tab-pane fade">
                            <div class="" role="grid">
                            </div>

                        </div>

                    </div>

                </div>
            </section>
            <script id="jsTemplateSinhNhat" type="text/x-kendo-template">
            <div class=" border-after pt-3">
                 <div class="d-flex">
                     <div class="imgBirday-Lt ">
                         <img src="#=ImageAvatar#" alt="">
                     </div>
                     <div class="infoBirdat-Lt pl-3">
                         <p class="title-info-Lt margin-p">Đồng chí #=TENKHAISINH#</p>
                         <p class="position-info-Lt margin-p pt-2">#=TEN_CVU#</p>
                         <p class="address-info-Lt margin-p pt-2">#=TEN_DONVI#</p>
                         <p class="position-info-Lt margin-p pt-2">#=TEN_PHONGB#</p>
                         <p class="date-info-Lt margin-p pt-2"><span><i class="fas fa-birthday-cake"></i></span><span class="pl-2">#=formatDateDDMM(BIRTHDAY)#</span></p>
                     </div>
                 </div>
             </div>
            
            </script>
            <script id="jsTemplateSinhNhatCBNV" type="text/x-kendo-template">
                <div class=" border-after pt-3">
                    <div class="d-flex">
                        <div class="imgBirday-Lt ">
                            <img src="#=ImageAvatar#" alt="">
                        </div>
                        <div class="infoBirdat-Lt pl-3">
                            <p class="title-info-Lt margin-p">Đồng chí #=tenkhaisinh#</p>
                            <p class="address-info-Lt margin-p pt-2">#=TEN_VTRICDANH == null ? "Chuyên viên" : TEN_VTRICDANH#</p>
                            <p class="position-info-Lt margin-p pt-2">#=DEPARTMENT_NAME#</p>
                            <p class="date-info-Lt margin-p pt-2"><span><i class="fas fa-birthday-cake"></i></span><span class="pl-2">#=formatDateDDMM(ngaysinh)#</span></p>
                        </div>
                    </div>
                </div>  
            </script>
            <script id="jsTemplateLichlamviecCaNhan" type="text/x-kendo-template">
                <div class="calendar_items  pt-3 border-basic pb-3">
                    <a href="<%=UrlSite %>/Pages/meeting.aspx"><h6 class="font-size-15 pl-3 color-table-meeting font-size-15 font-weight-bold">#=Title#
                    </h6></a>
                    <div class="list_calendar_items ">
                        <ul class="calendar_info pl-3">
                            <li class="time-calendar display-webkit-box">
                                <img src="/Content/themeV1/icon/clock.png" alt=""><span class="pl-2">#=formatDateTime(LichThoiGianBatDau)#  #=LichThoiGianKetThuc != null ? ("- " + formatDateTime(LichThoiGianKetThuc)) : ""#</span></li>
                            <li class="display-webkit-box">
                                <i class="fas fa-signal"></i><span class="pl-2">#=GetTTLich(LogText,DBPhanLoai)#</span>
                            </li>            
                            <li class="display-webkit-box">
                               <i class="far fa-user"></i><span class="pl-2">#=CHU_TRI_Title#</span>
                            </li>
                            <li class="display-webkit-box">
                               <i class="fas fa-user-circle"></i><span class="pl-2">#=CHUAN_BI_Title#</span>
                            </li>
                
                            <li class="display-webkit-box">
                                <img src="/Content/themeV1/icon/pin.png" alt=""><span class="pl-2">#=LichDiaDiem#</span>
                            </li>
                            <li class="display-webkit-box">
                                <img src="/Content/themeV1/icon/group (2).png" alt=""><span class="pl-2">#=THANH_PHAN_Title#</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </script>
        </div>
    </div>
</div>
<%} %>
<script type="text/javascript">
    $(document).ready(function () {
        if ("<%=UrlSite%>" != "" && "<%=UrlSite%>" != "/") {
            //sub site thì active tab đơn vị.
            $(".item_li_1").removeClass("active");
            $("#tabdonvi").addClass("active");
            $(".main-lich-event").hide();
            $("#tab-one").show();
        }
        $("#lstSinhNhat").smGrid2018({
            "TypeTemp": "kendo",
            "UrlPost": "/VIPortalAPI/api/LUser/GETDATA_SINHNHAT",
            //pageSize: 10,
            odata: { "ID_DONVI": '<%=oLGroupItem.OldID %>', "length": 4, "SoNgayLay": 7 },
            template: "jsTemplateSinhNhat",
            dataBound: function (e) {
            },
        });


    });
</script>
