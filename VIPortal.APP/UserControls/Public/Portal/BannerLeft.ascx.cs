﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using VIPortalAPP;

namespace VIPortal.APP.UserControls.Public.Portal
{
    public partial class BannerLeft : BaseUC_Web
    {
        public string TitleLink { get; set; }
        public string LinkSite { get; set; }
        public List<MenuQuanTriJson> lstMenu { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            lstMenu = new List<MenuQuanTriJson>();
            if (UrlSite == "" || UrlSite == "/")
            {
                TitleLink = "Portal Đơn vị";
                LinkSite = CurentUser.UrlSite;
            }
            else
            {
                //đang sở site đơn vị thì quay về portal
                TitleLink = "Portal Tổng CTy";
                LinkSite = "/";
            }
            MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA(UrlSite);
            MenuQuanTriQuery oMenuQuanTriQuery = new MenuQuanTriQuery()
            {
                PhanLoaiMenu = 1,
                FieldOrder = "MenuIndex",
                Ascending = true,
                Length = 100,
                _ModerationStatus = 1
                
            };
            lstMenu = oMenuQuanTriDA.GetListJson(oMenuQuanTriQuery);
            //fix + urlsite nêu có
            if (!string.IsNullOrEmpty(UrlSite))
            {
                lstMenu = lstMenu.Select(x => { x.MenuLink = x.MenuLink.StartsWith("http") ? x.MenuLink : UrlSite + x.MenuLink; return x; }).ToList();
            }
            lstMenu = lstMenu.Where(x => x.Permissions.Count == 0 ||
            (x.Permissions.Count > 0 && CurentUser.Permissions.Count(y => x.Permissions.Select(z => z.ID).Contains(y.ID)) > 0)).ToList();
            lstMenu = lstMenu.Select(x => { x.MenuIcon = IsImage(x.MenuIcon) ? $"<img src=\"{x.MenuIcon}\" alt=\"\">" : x.MenuIcon; return x; }).ToList();

        }
        private bool IsImage(string Menuicon)
        {
            return Regex.IsMatch(Menuicon, "^.*\\.(png|PNG|jpg|JPG|gif|GIF)$");
        }
    }
}