﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_ThongBao_Details.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.ThongBao.UC_ThongBao_Details" %>

<style type="text/css">
    .announcement-detail img {
        max-width: 100%;
    }
</style>

<section>
    <div class="announcement-detail bg-color-box-items box-shadow-basic">
        <div class="header-notification-detail">
            <p class="font-size-14 color-table-meeting pl-3 pt-2">
                <a href="/Pages/HomePortal.aspx">Trang chủ</a> - Chi tiết thông báo nội bộ
            </p>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="main-announcement-detail display-flex pl-3 pr-3">
                    <div class="">
                        <img src="/Content/themeV1/img/icon/thongcao.jpg" alt="">
                    </div>
                    <div class="pl-3">
                        <p class="margin-p font-size-15 font-weight-bold"><%=oThongBao.Titles %></p>
                        <div class="display-flex pb-2">
                            <p class="margin-p">
                                <img src="/Content/themeV1/img/icon/calendar.png" alt="">
                            </p>
                            <p class="margin-p pl-3"><%=oThongBao.Created.ToString("dd/MM/yyyy")%> * <%=oThongBao.CreatedUser.LookupValue %></p>
                        </div>
                        <div class="titile-announcement-detail pt-3">
                            <p class="margin-p font-weight-bold"><%=oThongBao.MoTa %></p>

                        </div>
                        <div class="pt-2">
                            <img src="<%=oThongBao.ImageNews %>" alt="" width="100%">
                            <p class="margin-p pt-3"><%=oThongBao.NoiDung %></p>
                        </div>
                        <div class="pt-2">
                            <%for(int i=0; i< oThongBao.ListFileAttach.Count;i++){ %>
                            <div class="file">
                                <ul>
                                    <li><i class="fas fa-download" style="color: #26b2fa"></i><a href="<%=oThongBao.ListFileAttach[i].Url %>" style="font-size: 13px; color: #337ab7;"><%=oThongBao.ListFileAttach[i].Name%></a></li>
                                </ul>
                            </div>
                            <%} %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="title-right-announcement-detail">
                    <p class="margin-p font-size-18 font-weight-bold">Thông báo khác</p>
                    <img src="/Content/themeV1/img/icon/Rectangle 4709.png" alt="">
                </div>
                <%foreach(var item in lstThongBao){%>
                <div class="row pt-3">
                    <div class="col-md-3 col-3">
                        <a href="<%=UrlSite %>/Pages/chitietthongbao.aspx?ItemID=<%=item.ID %>">
                            <%if(!string.IsNullOrEmpty(item.ImageNews)){%>
                            <img src="<%=item.ImageNews %>" alt="" style="width: 168px">
                            <%}else{%>
                            <img src="/Content/themeV1/img/avatar/newicon.png" alt="" style="width: 168px">
                            <%} %>
                        </a>
                    </div>
                    <div class="col-md-9 col-9">
                        <div class="">
                            <p class="margin-p font-weight-bold">
                                <a href="<%=UrlSite %>/Pages/chitietthongbao.aspx?ItemID=<%=item.ID %>"><%=item.Titles %></a>
                            </p>
                            <p class="margin-p">
                                <span class="pr-3">
                                    <img src="/Content/themeV1/img/icon/calendar.png" alt=""></span>
                                <%=item.Created.Value.ToString("dd/MM/yyyy") %>
                            </p>
                        </div>
                    </div>
                </div>
                <%} %>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    function loopLi() {
        setInterval(function () { // this code is executed every 500 milliseconds:
            var current = '/Pages/notifications.aspx';
            var actived = false;
            $('#items_sliderbar li a').each(function () {
                var $this = $(this);
                // if the current path is like this link, make it active
                if ($this.attr('href').indexOf(current) !== -1) {
                    $this.closest("li").addClass('active');
                    actived = true;
                }
            });
            if (actived) return;
        }, 500);
    }

    $(loopLi);
</script>





