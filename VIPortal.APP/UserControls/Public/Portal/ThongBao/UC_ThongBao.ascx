﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_ThongBao.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.ThongBao.UC_ThongBao" %>
<section>
    <div class="main-announcement bg-color-box-items">
        <div class="header-announcement">
            <p class="pt-3 pl-3 font-size-14 color-table-meeting">Trang chủ - thông cáo nội bộ</p>
        </div>
        <div class="content-main-announcement">
            <div class="item-content-main display-flex pl-3 pt-3">
                <div class="icon-announcement">
                    <img src="/Content/themeV1/thongbaonoibo/amplifier.png" alt="">
                </div>
                <div class="title-content-main pl-3">
                    <p class="margin-p font-size-15 font-weight-bold">
                        Thông tin báo chí về việc giả mạo thương hiệu EVN để quảng cáo cho vay tín dụng
                                   
                    </p>
                    <p class="margin-p pt-2">
                        <span class="pr-3">
                            <img src="/Content/themeV1/thongbaonoibo/calendar.png"
                                alt=""></span>23/04/2021 * Ban truyền thông EVN
                    </p>
                    <p class="margin-p pt-2">
                        Phát động tại buổi lễ, Ông Nguyễn Anh Tuấn – Chủ tịch Hội đồng thành viên EVNHANOI phát biểu "Với trách nhiệm của một công dân của Thành phố Hà Nội, công dân nước Việt Nam, mỗi CBCNV của EVNHANOI hãy tham gia ủng hộ
                                        phòng, chống dịch Covid-19 theo lời kêu gọi của Ủy ban Trung ương mặt trận Tổ quốc Việt Nam để góp phần bảo vệ sức khỏe của cộng đồng và sức khỏe của chính chúng ta".
                    </p>
                </div>
            </div>

            <div class="item-content-main display-flex pl-3 pt-3">
                <div class="icon-announcement">
                    <img src="/Content/themeV1/thongbaonoibo/amplifier.png" alt="">
                </div>
                <div class="title-content-main pl-3">
                    <p class="margin-p font-size-15 font-weight-bold">
                        Thông tin báo chí về việc giả mạo thương hiệu EVN để quảng cáo cho vay tín dụng
                                   
                    </p>
                    <p class="margin-p pt-2">
                        <span class="pr-3">
                            <img src="/Content/themeV1/thongbaonoibo/calendar.png"
                                alt=""></span>23/04/2021 * Ban truyền thông EVN
                    </p>
                    <p class="margin-p pt-2">
                        Phát động tại buổi lễ, Ông Nguyễn Anh Tuấn – Chủ tịch Hội đồng thành viên EVNHANOI phát biểu "Với trách nhiệm của một công dân của Thành phố Hà Nội, công dân nước Việt Nam, mỗi CBCNV của EVNHANOI hãy tham gia ủng hộ
                                        phòng, chống dịch Covid-19 theo lời kêu gọi của Ủy ban Trung ương mặt trận Tổ quốc Việt Nam để góp phần bảo vệ sức khỏe của cộng đồng và sức khỏe của chính chúng ta".
                    </p>
                </div>
            </div>

            <div class="item-content-main display-flex pl-3 pt-3">
                <div class="icon-announcement">
                    <img src="/Content/themeV1/thongbaonoibo/amplifier.png" alt="">
                </div>
                <div class="title-content-main pl-3">
                    <p class="margin-p font-size-15 font-weight-bold">
                        Thông tin báo chí về việc giả mạo thương hiệu EVN để quảng cáo cho vay tín dụng
                                   
                    </p>
                    <p class="margin-p pt-2">
                        <span class="pr-3">
                            <img src="/Content/themeV1/thongbaonoibo/calendar.png"
                                alt=""></span>23/04/2021 * Ban truyền thông EVN
                    </p>
                    <p class="margin-p pt-2">
                        Phát động tại buổi lễ, Ông Nguyễn Anh Tuấn – Chủ tịch Hội đồng thành viên EVNHANOI phát biểu "Với trách nhiệm của một công dân của Thành phố Hà Nội, công dân nước Việt Nam, mỗi CBCNV của EVNHANOI hãy tham gia ủng hộ
                                        phòng, chống dịch Covid-19 theo lời kêu gọi của Ủy ban Trung ương mặt trận Tổ quốc Việt Nam để góp phần bảo vệ sức khỏe của cộng đồng và sức khỏe của chính chúng ta".
                    </p>
                </div>
            </div>

            <div class="item-content-main display-flex pl-3 pt-3">
                <div class="icon-announcement">
                    <img src="/Content/themeV1/thongbaonoibo/amplifier.png" alt="">
                </div>
                <div class="title-content-main pl-3">
                    <p class="margin-p font-size-15 font-weight-bold">
                        Thông tin báo chí về việc giả mạo thương hiệu EVN để quảng cáo cho vay tín dụng
                                   
                    </p>
                    <p class="margin-p pt-2">
                        <span class="pr-3">
                            <img src="/Content/themeV1/thongbaonoibo/calendar.png"
                                alt=""></span>23/04/2021 * Ban truyền thông EVN
                    </p>
                    <p class="margin-p pt-2">
                        Phát động tại buổi lễ, Ông Nguyễn Anh Tuấn – Chủ tịch Hội đồng thành viên EVNHANOI phát biểu "Với trách nhiệm của một công dân của Thành phố Hà Nội, công dân nước Việt Nam, mỗi CBCNV của EVNHANOI hãy tham gia ủng hộ
                                        phòng, chống dịch Covid-19 theo lời kêu gọi của Ủy ban Trung ương mặt trận Tổ quốc Việt Nam để góp phần bảo vệ sức khỏe của cộng đồng và sức khỏe của chính chúng ta".
                    </p>
                </div>
            </div>
            <div class="item-content-main display-flex pl-3 pt-3">
                <div class="icon-announcement">
                    <img src="/Content/themeV1/thongbaonoibo/amplifier.png" alt="">
                </div>
                <div class="title-content-main pl-3">
                    <p class="margin-p font-size-15 font-weight-bold">
                        Thông tin báo chí về việc giả mạo thương hiệu EVN để quảng cáo cho vay tín dụng
                                   
                    </p>
                    <p class="margin-p pt-2">
                        <span class="pr-3">
                            <img src="/Content/themeV1/thongbaonoibo/calendar.png"
                                alt=""></span>23/04/2021 * Ban truyền thông EVN
                    </p>
                    <p class="margin-p pt-2">
                        Phát động tại buổi lễ, Ông Nguyễn Anh Tuấn – Chủ tịch Hội đồng thành viên EVNHANOI phát biểu "Với trách nhiệm của một công dân của Thành phố Hà Nội, công dân nước Việt Nam, mỗi CBCNV của EVNHANOI hãy tham gia ủng hộ
                                        phòng, chống dịch Covid-19 theo lời kêu gọi của Ủy ban Trung ương mặt trận Tổ quốc Việt Nam để góp phần bảo vệ sức khỏe của cộng đồng và sức khỏe của chính chúng ta".
                    </p>
                </div>
            </div>

            <div class="panigation-forum col-md-2  offset-md-8">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">

                        <li class="page-item mr-2"><a class="page-link active-pagination box-shadow-basic font-size-20" href="#">1</a>
                        </li>
                        <li class="page-item mr-2"><a class="page-link  box-shadow-basic font-size-20" href="#">2</a>
                        </li>
                        <li class="page-item mr-2"><a class="page-link  box-shadow-basic font-size-20" href="#">3</a>
                        </li>
                        <li class="page-item"><a class="page-link chill-pagination  box-shadow-basic font-size-14" href="#">Tiếptheo</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</section>
