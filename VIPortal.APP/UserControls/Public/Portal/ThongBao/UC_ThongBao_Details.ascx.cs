﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using VIPortalAPP;

namespace VIPortal.APP.UserControls.Public.Portal.ThongBao
{
    public partial class UC_ThongBao_Details : BaseUC_Web
    {
        public ThongBaoItem oThongBao { get; set; }
        public List<ThongBaoJson> lstThongBao { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            ThongBaoDA oThongBaoDA = new ThongBaoDA(UrlSite);
            oThongBao = oThongBaoDA.GetByIdToObject<ThongBaoItem>(ItemID);
            lstThongBao = oThongBaoDA.GetListJson(new ThongBaoQuery() { Length = 5, FieldOrder = "Created", Ascending = false });
            if (lstThongBao.FindIndex(x => x.ID == ItemID) > -1)
                lstThongBao.RemoveAt(lstThongBao.FindIndex(x => x.ID == ItemID));
        }
    }
}