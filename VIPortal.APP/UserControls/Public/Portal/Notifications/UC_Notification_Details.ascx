﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_Notification_Details.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.Notifications.UC_Notification_Details" %>
<script type="text/javascript">
    $(document).ready(function () {
        SetConfigLang('<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>', 'NotificationDetail');
        //$.post("/VIPortalAPI/api/VanBanTaiLieu/QUERYDATA_THONGBAO", { "FieldOrder": "Created", Ascending: false, "length": 10 }, function (odata) {
        //    var templateContent = $("#jsTemplateTB").html();
        //    var template = kendo.template(templateContent);
        //    var result = kendo.render(template, odata.data); //render the template
        //    $("#list_tb").html(result); //append the result to the page

        //});
        $("#list_tb").smGrid2018({
            "TypeTemp": "kendo",
            "UrlPost": "/VIPortalAPI/api/VanBanTaiLieu/QUERYDATA_THONGBAO",
            ConfigLanguages: '<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>',
            pageSize: 10,
            odata: { "do": "QUERYDATA" },
            template: "template_new",
            dataBound: function (e) {
            },
        });
    });
</script>
<section class="NotificationDetail">
    <div class="main-announcement bg-color-box-items">
        <div class="header-announcement">
            <p class="pt-3 pl-3 font-size-14 color-table-meeting" data-lang="TrangChu">Trang chủ - Thông báo nội bộ</p>
        </div>
        <script id="template_new" type="text/x-kendo-template">
                             <div class="item-content-main display-flex pl-3 pt-3">
                                <div class="icon-announcement">
                                    <img src="/Content/themeV1/thongbaonoibo/amplifier.png" alt="">
                                </div>
                                <div class="title-content-main pl-3">
                                    <p class="margin-p font-size-15 font-weight-bold"><a href="<%=UrlSite %>/Pages/chitietthongbao.aspx?ItemID=#=ID#" class="color-table-meeting">#=Titles#</a></p>
                                    <p class="margin-p pt-2"><span class="pr-3"><img src="/Content/themeV1/thongbaonoibo/calendar.png" alt=""></span>#=formatDate(NgayHieuLuc)# * #=CreatedUser.Title#</p>
                                    <p class="margin-p pt-2">#if(MoTa.length > 400){##=MoTa.substring(0,400)#...#}else{##=MoTa##}#</p>
                                </div>
                            </div>
        </script>
        <div class="content-main-announcement" id="list_tb" data-role="fullGrid">
            <div role="grid" class="">
            </div>
            <div class="clspaging">
            </div>
        </div>
    </div>
</section>





