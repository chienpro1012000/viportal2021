﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData;

namespace VIPortal.APP.UserControls.Public.Portal.Phones
{
    public partial class UC_PhoneBooks : BaseUC_Web
    {
        public DanhBaItem danhBaItem { get; set; }
        public string MaDonViSelected { get; set; }
        public string MaPhongBanSelected { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            danhBaItem = new DanhBaItem();
            if (ItemID > 0)
            {
                DanhBaDA oDanhBaDA = new DanhBaDA(UrlSite);
                danhBaItem = oDanhBaDA.GetByIdToObjectSelectFields<DanhBaItem>(ItemID);
            }

            if (!string.IsNullOrEmpty(UrlSite))
            {
                LGroupDA lGroupDA = new LGroupDA();
                LGroupJson lGroupJson = lGroupDA.GetListJson(new LGroupQuery() { UrlSite = UrlSite }).FirstOrDefault();
                if (lGroupJson != null)
                {
                    if (!string.IsNullOrEmpty(lGroupJson.orgCode))
                        MaDonViSelected = lGroupJson.OldID;
                    else
                    {
                        LGroupItem oLGroupItem = lGroupDA.GetByIdToObject<LGroupItem>(lGroupJson.GroupParent.ID);
                        MaPhongBanSelected = lGroupJson.OldID;
                        MaDonViSelected = oLGroupItem.OldID;
                    }
                }

            }
            else
            {
                MaDonViSelected = "276";
            }
        }
    }
}