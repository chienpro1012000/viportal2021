﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewPhoneBook.aspx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.Phones.pViewPhoneBook" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Họ và tên</label>
            <div class="col-sm-10">
                <%=oDanhBa.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="ImageNews" class="col-sm-2 control-label">Ảnh đại diện</label>
            <div class="col-sm-10" data-role="groupImageDD">
                <img style="width:200px;" src="<%=oDanhBa.ImageNews%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="ToChuc" class="col-sm-2 control-label">Đơn vị, tổ chức</label>
            <div class="col-sm-10">
                <%=oDanhBa.DBParentPB.LookupValue%>
            </div>
        </div>
        <div class="form-group row">
            <label for="SDTDiDong" class="col-sm-2 control-label">Chức vụ</label>
            <div class="col-sm-10">
                <%=oDanhBa.DBChucVu.LookupValue%>
            </div>
        </div>
        <div class="form-group row">
            <label for="SDTBan" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-4">
                <%=oDanhBa.DBEmail%>
            </div>
            <label for="DiaChi" class="col-sm-2 control-label">Fax</label>
            <div class="col-sm-4">
                <%=oDanhBa.DBFax%>
            </div>
        </div>
        <div class="form-group row">
            <label for="DiaChi" class="col-sm-2 control-label">Số điện thoại</label>
            <div class="col-sm-4">
                <%=oDanhBa.DBSDT%>
            </div>
            <label for="Email" class="col-sm-2 control-label">Website</label>
            <div class="col-sm-4">
                <%=oDanhBa.DBWebsite%>
            </div>
        </div>
        <div class="form-group row">
            <label for="Email" class="col-sm-2 control-label">Là đơn vị,tổ chức</label>
            <div class="col-sm-4">
                <%=oDanhBa.isOffice == "1" ? "Có" : "Không"%>
            </div>
            <%--<label for="Chucvu" class="col-sm-2 control-label">Chức Vụ</label>
            <div class="col-sm-4">
                <%=oDanhBa.DBChucVu.LookupValue%>
            </div>--%>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>
