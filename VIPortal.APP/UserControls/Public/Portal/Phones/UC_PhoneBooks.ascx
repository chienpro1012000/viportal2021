﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_PhoneBooks.ascx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.Phones.UC_PhoneBooks" %>

<style type="text/css">
    #lstDanhBa .select2-container--default .select2-selection--single .select2-selection__rendered {
        color: #444;
        line-height: 38px;
    }

    #lstDanhBa .select2-container .select2-selection--single {
        box-sizing: border-box;
        cursor: pointer;
        display: block;
        height: 34px;
        user-select: none;
        -webkit-user-select: none;
    }

    .display-flex-righteous {
        justify-content: normal !important;
    }

    select#format {
        width: 100%;
        padding: 7px 19px;
        border-radius: 4px;
    }

    .inputBp input {
        width: 100%;
        padding: 7px 19px;
        border-radius: 4px;
    }

    table, th, td {
        border: 1px solid #ccc;
        border-collapse: collapse;
        padding: 10px;
    }

    th {
        background-color: #044881;
        color: #fff;
    }

    .td-9 {
        text-align: left;
        background: #f5f5f5;
        color: #000;
        font-weight: bold;
    }
</style>

<section class="PhoneBooks">
    <div id="lstDanhBa" class="briefings bg-color-box-items" data-role="fullGrid">
        <div class="main-phonebook form-briefings pl-3 pr-3 pt-3 pb-3">
            <div class="header-forumdetail pt-2 pl-3 ">
                <p class="font-size-14 color-table-meeting" data-lang="HomeContacts">Trang chủ - Danh bạ</p>
            </div>
            <div class="content-forumdetail pl-3 pt-3 pb-3">
                <h5 class="title-forumdetail font-size-16 color-table-meeting font-weight-bold" style="padding-bottom: 5px;" data-lang="CONTACTSEARCH">TRA CỨU DANH BẠ
                </h5>
            </div>
            <div class="" id="frmSearch" role="search">
                <!--<div class="box-shadow-basic select-1" id="DonVi" style="box-shadow: none;">
                    <select data-selected="" class="form-control" data-url="/UserControls/AdminCMS/DanhBa/pAction.asp?do=QUERYDATA&isOffice=1&moder=1&cap=1" data-place="Chọn Đơn Vị" name="DBDonVi" id="DBDonVi"></select>
                </div>
                <div class="box-shadow-basic select-1" id="PhongBan" style="box-shadow: none;">
                    <select data-selected="" class="form-control" data-url="/UserControls/AdminCMS/DanhBa/pAction.asp?do=QUERYDATA&isOffice=1&moder=1" data-place="Chọn Phòng ban" name="IDParent" id="DBParentPB"></select>
                </div>
                <div class="box-shadow-basic select-1" id="ChucVu"  style="box-shadow: none;">
                    <select data-selected="" class="form-control" data-url="/UserControls/AdminCMS/DanhMucChucVu/pAction.ashx?do=QUERYDATA" data-place="Chọn Chức Vụ" name="IDChucVu" id="DBChucVu"></select>

                </div>
               <div class="box-shadow-basic input-3"  style="box-shadow: none; margin-left:15px">
                    <input type="text" name="Keyword" id="Keyword" placeholder="Từ khóa..." />
                    <input type="hidden" name="SearchInSimple" id="SearchInSimple" value="Title,DBSDT,DBEmail" />
                </div>
                <div class=" icon-button  display-flex pl-2">
                    <button data-toggle="tooltip" data-placement="top" title="Xóa lọc tất cả"  class="reset_data"><i class="fas fa-sync-alt"></i></button>
                    <button type="button" class="btnsearch btn-search-meeting">Tìm kiếm</button>
                </div>-->


                <div class="row">
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-4 lable-inputPb pl-3 font-size-13  text-center font-weight-bold">
                                <span class="ps-ab" data-lang="unit">Đơn vị:</span>
                            </div>
                            <div class="col-md-8">
                                <div class="select">
                                    <select data-selected="<%=MaDonViSelected %>" class="form-control" data-url="/VIPortalAPI/api/DanhBa/QUERYDATADONVI" data-place="Chọn Đơn Vị" name="MaDonVi" id="MaDonVi"></select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-4 lable-inputPb pl-3 font-size-13  text-center font-weight-bold">
                                <span class="ps-ab" data-lang="Department">Phòng ban:</span>
                            </div>
                            <div class="col-md-8">
                                <div class="select">
                                    <select data-selected="<%=MaPhongBanSelected %>" class="form-control" data-url="/VIPortalAPI/api/DanhBa/QUERYDATAPHONGBAN" data-place="Chọn Đơn Vị" name="PhongBan" id="PhongBan"></select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-4 font-size-13  lable-inputPb text-center font-weight-bold">
                                <span class="ps-ab" data-lang="Name">Họ và tên:</span>
                            </div>
                            <div class="col-md-8 inputBp">
                                <input type="text" name="Keyword" id="Keyword" data-lang="TuKhoa" placeholder="Từ khóa..." />
                                <input type="hidden" name="SearchInSimple" id="SearchInSimple" value="Title,DBSDT,DBEmail" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <button type="button" class="btnsearch btn-search-meeting" data-lang="Search">Tìm kiếm</button>
                    </div>
                </div>
            </div>
            <table style="width: 100%">
                <tr class="text-center">
                    <th rowspan="2" data-lang="STT">STT</th>
                    <th rowspan="2" data-lang="HoTen">Họ tên</th>
                    <th rowspan="2" data-lang="ChucVu">Chức vụ</th>
                    <th colspan="2" data-lang="DienThoai">Điện thoại</th>
                    <th rowspan="2" data-lang="Email">Email</th>
                </tr>
                <tr class="text-center">
                    <th data-lang="NoiBo">Nội bộ</th>
                    <th data-lang="DiDong">Di động</th>
                </tr>
                <tbody role="grid">
                    <tr>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>
<script id="jsTemplateDanhBa" type="text/x-kendo-template">
                      <tr class="itemdb" data-iddep="#=DEPARTMENT_ID#" data-titledep="#=DEPARTMENT_NAME#">
                    <td class="text-center">#=STT#</td>
                    <td>#=tenkhaisinh#</td>
                    <td>#=TEN_VTRICDANH != null ? TEN_VTRICDANH : ""#</td>
                    <td>#=DIENTHOAI_NOIBO != null ? DIENTHOAI_NOIBO : ""#</td>
                    <td>#=dienthoai#</td>
                    <td>#=email#</td>
                </tr>
</script>
<script type="text/javascript">
    $(document).ready(function () {

        SetConfigLang('<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>', 'PhoneBooks');
        //$("#DBChucVu").smSelect2018V2({
        //    dropdownParent: "#ChucVu"
        //});
        //$("#MaDonVi").val('<%=MaDonViSelected %>');
        $("#MaDonVi").smSelect2018V2({
            dropdownParent: "#lstDanhBa"
        });
        $("#PhongBan").smSelect2018V2({
            dropdownParent: "#lstDanhBa",
            Ajax: false,
            TemplateValue: "{OldID}",
            parameterPlus: function (option) {
                option["orgId"] = '<%=MaDonViSelected %>';
            },
        });
        $("#MaDonVi").on('change', function () {
            $("#PhongBan").empty();
            $("#PhongBan").smSelect2018V2({
                dropdownParent: "#lstDanhBa",
                Ajax: false,
                TemplateValue: "{OldID}",
                parameterPlus: function (option) {
                    var val = $("#MaDonVi").find(':selected').val();
                    option["orgId"] = val;
                },
            });
        });
        $("#lstDanhBa").smGrid2018({
            "TypeTemp": "kendo",
            "UrlPost": "/VIPortalAPI/api/DanhBa/QUERYDATA",
            hasSTT: true,
            pageSize: 10,
            odata: { "do": "QUERYDATA", "isUser": "1" },
            template: "jsTemplateDanhBa",
            parameterPlus: function (odata) {
                if (odata["MaDonVi"] == undefined) {
                    odata["MaDonVi"] = '<%=MaDonViSelected %>';
                }
            },
            dataBound: function (e) {
                //xử lý đuyệt qua các tr chỗ này.
                var DEPARTMENT_ID = "";
                $('#lstDanhBa table tbody tr.itemdb').each(function () {
                    var iddep = $(this).attr('data-iddep');
                    if (DEPARTMENT_ID != iddep) {
                        var titledep = $(this).attr('data-titledep');
                        $(this).before(`<tr>
                                <td colspan="6" class="td-9 pl-3">${titledep}</td>
                        </tr>`);
                        DEPARTMENT_ID = iddep;
                    }
                    //do your stuff, you can use $(this) to get current cell
                })
            },
        });
    });
</script>

