﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LogoBaner.ascx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.LogoBaner" %>
<div class="row LogoBanner">
                            <div class="col-md-6 pt-3">
                                <a href="/Pages/diendan.aspx" class="color-a-basic">
                                    <div class="Utilities-items display-flex-righteous bg-color-box-items box-shadow-basic">
                                        <div class="icon1_forum">
                                            <img src="/Content/themeV1/icon/diendan.jpg" alt="">
                                        </div>
                                        <div class="title_forrum">
                                            <h5 data-lang="DienDan">Diễn đàn</h5>
                                            <p data-lang="DienDanBanLuan">Diễn đàn bàn luận vấn đề</p>
                                        </div>
                                        <div class="icon2_forum">
                                            <img src="/Content/themeV1/icon/next.png" alt="">
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 pt-3">
                                <a href="/Pages/questions.aspx" class="color-a-basic">
                                    <div class="Utilities-items display-flex-righteous bg-color-box-items box-shadow-basic">
                                        <div class="icon1_forum">
                                            <img src="/Content/themeV1/icon/hoidap.jpg" alt="">
                                        </div>
                                        <div class="title_forrum">
                                            <h5 data-lang="FAQ">Hỏi đáp FAQ</h5>
                                            <p data-lang="GiaiDap">Giải đáp thắc mắc </p>
                                        </div>
                                        <div class="icon2_forum">
                                            <img src="/Content/themeV1/icon/next.png" alt="">
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 pt-3">
                                <a href="/Pages/socialnetwork.aspx" class="color-a-basic">
                                    <div class="Utilities-items display-flex-righteous bg-color-box-items box-shadow-basic">
                                        <div class="icon1_forum">
                                            <img src="/Content/themeV1/icon/mangxahoi.jpg" alt="">
                                        </div>
                                        <div class="title_forrum">
                                            <h5 data-lang="MXH">Mạng xã hội</h5>
                                            <p data-lang="DienDanBanLuan">Diễn đàn bàn luận vấn đề</p>
                                        </div>
                                        <div class="icon2_forum">
                                            <img src="/Content/themeV1/icon/next.png" alt="">
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 pt-3">
                                <a href="/Pages/phonebook.aspx" class="color-a-basic">
                                    <div class="Utilities-items display-flex-righteous bg-color-box-items box-shadow-basic">
                                        <div class="icon1_forum">
                                            <img src="/Content/themeV1/icon/danhba.jpg" alt="">
                                        </div>
                                        <div class="title_forrum">
                                            <h5 data-lang="DanhBa">Danh bạ</h5>
                                            <p data-lang="DanhSachNhanVien">Danh sách nhân viên</p>
                                        </div>
                                        <div class="icon2_forum">
                                            <img src="/Content/themeV1/icon/next.png" alt="">
                                        </div>
                                    </div>
                                </a>
                            </div>

                        </div>
<script>
    $(document).ready(function () {
        let $ConfigLanguages = '<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>';

        SetConfigLang($ConfigLanguages, 'LogoBanner');
    });
</script>