﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DanhSachCongTrinhThucTeKC.aspx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.DauTuXayDung.DanhSachCongTrinhThucTeKC" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link type="text/css" href="/Content/dist/css/StyleBaoCao.css" rel="stylesheet" />
    <style type="text/css">
        .zone_search {
            overflow-x: hidden;
        }
        
    </style>
</head>
<body>
       <div id="BaoCaoSoDangKy" class="smgrid" data-title="Văn bản đến" data-grid="grid_BaoCaoSoDKVanBanDen"
        data-form="/iVBDH/VanBanDen/pFormVanBanDen.aspx" data-formwidth="1200" data-action="/Vbden/api/VanBanDen/VBDGetListJsonGroupSolr" data-view="/Vbden/VanBanDen/pViewVanBanDen.aspx">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />
                <input type="hidden" name="CurentUser" value="<%=CurentUser.Title %>" />
                <div class="title_icon">
                </div>
                <div class="form-group row text-search align-items-center">
                    <label class="col-sm-2 control-label">Nhập Năm:</label>
                    <div class="col-sm-2">
                        <input type="text" name="Nam" id="Nam" class="form-control " placeholder="Nhập năm" value="<%=DateTime.Now.ToString("yyyy") %>" />
                    </div>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-primary thongke">Thống kê</button>
                        <button type="button" class="btn btn-primary" id="btnExel">Excel</button>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="mainContentBaoCao">
            <div style="">
                <div style="width: 30%; float: left; text-align:center">
                      <p style="font-size: 10pt; width: 100%"><b class="TieuDeTrai1"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeTrai2"></b></p>
                </div>
                <div style="width: 55%; float: left; text-align:center">
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeGiua1"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeGiua2"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="Title"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b> <i class="Nam"></i></b></p>
                </div>
                <div style="width: 15%; float: left; text-align:center">
                      <p style="font-size: 12pt; width: 100%"><p class="LoaiBM"></p></p>
                </div>
            </div>
       
            <div class="smtable_header" style="float:left;overflow-x:scroll">
                <table class="smtable" style="width:100%">
                    <thead>
                        <tr>
                            <th style="width:3%;">STT</th>
                            <th style="width:7%;">Danh mục công trình </th>
                            <th style="width:5%;" >Nhóm dự án</th>
                            <th style="width:7%;" >Quy Mô</th>                             
                            <th style="width:10%;">Nguồn Vốn </th>
                            <th style="width:5%;">Đơn vị quản lý dự án</th>
                            <th style="width:8%; "colspan="2">Tiến độ khởi công </th>
                            <th style="width:8%;" colspan="2">Tiếng độ đóng điện</th>
                            <th style="width:15%;" >Tình trạng dự án</th>
                            <th style="width:10%;" > Ghi chú </th>
                            
                        </tr>
                        <tr>                              
                            <th ></th>  
                            <th ></th>  
                            <th ></th>  
                            <th ></th>  
                            <th ></th>  
                            <th ></th>  
                            <th >Kế Hoạch</th>  
                            <th >Thực tế </th>
                            <th >Kế Hoạch</th>  
                            <th >Thực tế </th>
                            <th ></th>  
                            <th ></th>  
                        </tr>
                        <tr>                              
                            <th >1</th>  
                            <th >2</th>  
                            <th >3</th>  
                            <th >4</th>  
                            <th >5</th>  
                            <th >6</th>  
                            <th >7</th>  
                            <th >8</th>
                            <th >9</th>  
                            <th >10</th>
                            <th >11</th>  
                            <th >12</th>  
                        </tr>
                    </thead>
                    <tbody class="clsContent">
                    </tbody>
                </table>
            </div>
                 <div class="text-center mt-4 checkdatanull" style="width:100% ; float: left ">
                <p style="font-size: 12pt; width: 100%">Chưa có dữ liệu </p>
            </div>
            <script id="javascriptTemplate" type="text/x-kendo-template">
            <tr class="clsvbd" data-id="">
                <td style="width: 3%" >#=STT#</td>
                <td style="width:7%">#=DanhMucCongTrinh!=null?DanhMucCongTrinh:""#</td>
                <td style="width: 5%">#=NhomDuAn!=null?NhomDuAn:""#</td>
                <td style="width: 7%">#=QuyMo!=null?QuyMo:""#</td>                          
                <td style="width :10%">#=NguonVon!=null?NguonVon:""#</td> 
                <td style="width :5%">#=DonViQLDA!=null?DonViQLDA:""#</td> 
                <td style="width :4%">#=TienDoKCKeHoach!=null?TienDoKCKeHoach:""#</td> 
                <td style="width :4%">#=TienDoKCThuHien!=null?fomatdatetime(TienDoKCThuHien):""#</td> 
                <td style="width:4%" >#=TienDoDongDienKeHoach!=null?TienDoDongDienKeHoach:""#</td>                          
                <td style="width:4%">#=TienDoDongDienThuHien!=null?fomatdatetime(TienDoDongDienThuHien):""#</td>               
                <td style="width:15%">#=TinhTrangDuAn!=null?TinhTrangDuAn:""#</td>                          
                <td style="width:10%">#=GhiChu#</td>                          
            </tr>
            </script> 
        

        </div>
    </div>
</body>
    <script type="text/javascript">
        $(document).ready(function () {
            ThongKe();
            $(".thongke").click(function () {
                ThongKe();
            });
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });

            $("#btnExel").click(function () {
                var $datathongke = $(".zone_search").siSerializeDivFrm();
                var str = jQuery.param($datathongke);
                var Url = "/VIPortalAPI/api/LinhVucDauTuXayDung/QUERYDATA_BaoCaoCongTrinhThucTeKC?excel=1&" + str + "&TitleBC=" + $("#TitleBC").text();
                my_window = window.open(Url, "mywindow1");
            });
            $("#btnword").click(function () {
                var $datathongke = $(".zone_search").siSerializeDivFrm();
                var str = jQuery.param($datathongke);
                var Url = "/VIPortalAPI/api/LinhVucDauTuXayDung/QUERYDATA_BaoCaoCongTrinhThucTeKC?word=1&" + str + "&TitleBC=" + $("#TitleBC").text();
                my_window = window.open(Url, "mywindow1");

            });
        });
        function ThongKe() {
            $("#tungay").text($("#SearchTuNgay").val());
            $("#denngay").text($("#SearchDenNgay").val());
            var data = $(".zone_search").siSerializeArray();
            loading();
            $.post("/VIPortalAPI/api/LinhVucDauTuXayDung/QUERYDATA_BaoCaoCongTrinhThucTeKC?", data, function (data) {

                $(".Title").text(data.Title);
                $(".TieuDeTrai1").text(data.TieuDeTrai1);
                $(".TieuDeTrai2").text(data.TieuDeTrai2);
                $(".TieuDeGiua1").text(data.TieuDeGiua1);
                $(".TieuDeGiua2").text(data.TieuDeGiua2);
                $(".Nam").text(data.Nam);


                var htmlTEmp = '';
                var templatehtmltr = kendo.template($("#javascriptTemplate").html());
                $(".mainContentBaoCao .clsContent").html("");
                if (data.data.length > 0) {
                    $(".checkdatanull").hide();
                    for (var i = 0; i < data.data.length; i++) {
                        data.data[i]["STT"] = i + 1;
                        htmlTEmp += templatehtmltr(data.data[i]);
                    }

                    $(".mainContentBaoCao .clsContent").append(htmlTEmp);
                }
                $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');

                $('#tongbanghi').text("Có tổng số: " + data.data.length + " bản ghi");

                $(".clsvbd").css('cursor', 'pointer');
               
            });
            Endloading();
        }
    </script>
    <script>
        function fomatdatetime(dateStr) {
            var pattern = /(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/;

            var formatDateStr = dateStr.replace(pattern, '$1/$2/$3 $4:$5:$6');
            var formatDate = new Date(formatDateStr);
            return formatDate.format("dd/MM/yyyy");
        }
    </script>
</html>