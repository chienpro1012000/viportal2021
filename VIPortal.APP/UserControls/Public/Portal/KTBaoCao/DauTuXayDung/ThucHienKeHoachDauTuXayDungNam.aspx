﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ThucHienKeHoachDauTuXayDungNam.aspx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.DauTuXayDung.ThucHienKeHoachDauTuXayDungNam" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link type="text/css" href="/Content/dist/css/StyleBaoCao.css" rel="stylesheet" />
    <style type="text/css">
        .zone_search {
            overflow-x: hidden;
        }
        
    </style>
</head>
<body>
       <div id="BaoCaoSoDangKy" class="smgrid" data-title="Văn bản đến" data-grid="grid_BaoCaoSoDKVanBanDen"
        data-form="/iVBDH/VanBanDen/pFormVanBanDen.aspx" data-formwidth="1200" data-action="/Vbden/api/VanBanDen/VBDGetListJsonGroupSolr" data-view="/Vbden/VanBanDen/pViewVanBanDen.aspx">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />
                <input type="hidden" name="CurentUser" value="<%=CurentUser.Title %>" />
                <div class="title_icon">
                </div>
                <div class="form-group row text-search align-items-center">
                 <label class="col-sm-2 control-label">Đơn Vị:</label>
                    <div class="col-sm-3">
                        <select class="form-control" id="maDonVi" name="maDonVi">
                            <option value="80000">Tổng Công ty Điện lực Hà Nội</option>
                            <option value="80007">Công ty Điện lực Hoàn Kiếm</option>
                            <option value="80008">Công ty Điện lực Hai Bà Trưng</option>
                            <option value="80009">Công ty Điện lực Ba Đình</option>
                            <option value="80010">Công ty Điện lực Đống Đa</option>
                            <option value="80014">Công ty Điện lực Nam Từ Liêm</option>
                            <option value="80036">Công ty Điện lực Thanh Trì</option>
                            <option value="80015">Công ty Điện lực Gia Lâm</option>
                            <option value="80016">Công ty Điện lực Đông Anh</option>
                            <option value="80017">Công ty Điện lực Sóc Sơn</option>
                            <option value="80011">Công ty Điện lực Tây Hồ</option>
                            <option value="80013">Công ty Điện lực Thanh Xuân</option>
                            <option value="80012">Công ty Điện lực Cầu Giấy</option>
                            <option value="80018">Công ty Điện lực Hoàng Mai</option>
                            <option value="80019">Công ty Điện lực Long Biên</option>
                            <option value="80020">Công ty Điện lực Mê Linh</option>
                            <option value="80021">Công ty Điện lực Hà Đông</option>
                            <option value="80022">Công ty Điện lực Sơn Tây</option>
                            <option value="80023">Công ty Điện lực Chương Mỹ</option>
                            <option value="80024">Công ty Điện lực Thạch Thất</option>
                            <option value="80025">Công ty Điện lực Thường Tín</option>
                            <option value="80026">Công ty Điện lực  Ba Vì</option>
                            <option value="80027">Công ty Điện lực Đan Phượng</option>
                            <option value="80028">Công ty Điện lực Hoài Đức</option>
                            <option value="80029">Công ty Điện lực Mỹ Đức</option>
                            <option value="80030">Công ty Điện lực Phú Xuyên</option>
                            <option value="80031">Công ty Điện lực Phúc Thọ</option>
                            <option value="80032">Công ty Điện lực Quốc Oai</option>
                            <option value="80033">Công ty Điện lực Thanh Oai</option>
                            <option value="80034">Công ty Điện lực Ứng Hòa</option>
                            <option value="80039">Công ty Điện lực Bắc Từ Liêm</option>
                            <option value="80004">Công ty Công nghệ thông tin Điện lực Hà Nội</option>
                            <option value="80040">Trung tâm Điều độ hệ thống điện TP Hà Nội</option>
                            <option value="80005">Công ty Thí nghiệm điện Điện lực Hà Nội</option>
                            <option value="80037">Công ty Lưới điện Cao thế TP Hà Nội</option>
                            <option value="80041">Công ty Dịch vụ Điện lực Hà Nội</option>
                            <option value="80001">Ban QLDA lưới điện Hà Nội</option>
                            <option value="80002">BQLDA Phát triển điện lực Hà Nội</option>
                            <option value="80042"> Ban Quản lý Xây dựng Nhà điều hành</option>
                            <option value="80003">Trung tâm thông tin điều độ</option>
                            <option value="80006">Phòng Kiểm định đo lường</option>
                            <option value="80035">Công ty cơ điện Điện lực Hà Nội</option>
                            <option value="80038">Văn phòng Tổng công ty</option>
                        </select>                 
                    </div>
                </div>
                <div class="form-group row text-search align-items-center">
                    <label class="col-sm-2 control-label">Nhập Năm:</label>
                    <div class="col-sm-3">
                        <input type="text" name="Nam" id="Nam" class="form-control " placeholder="Nhập năm" value="<%=DateTime.Now.ToString("yyyy") %>" />
                    </div>
                </div>
                <div class="form-group row text-search align-items-center">
                    <label class="col-sm-2 control-label">Nhập Tháng:</label>
                    <div class="col-sm-3">
                            <select class="form-control" id="thang" name="thang">
                             <option value="01" <%=DateTime.Now.ToString("MM") == "01" ? "selected" : "" %>>Tháng 1</option>
                            <option value="02 " <%=DateTime.Now.ToString("MM") == "02" ? "selected" : "" %>>Tháng 2</option>
                            <option value="03 " <%=DateTime.Now.ToString("MM") == "03" ? "selected" : "" %>>Tháng 3</option>
                            <option value="04 " <%=DateTime.Now.ToString("MM") == "04" ? "selected" : "" %>>Tháng 4</option>
                            <option value="05 " <%=DateTime.Now.ToString("MM") == "05" ? "selected" : "" %>>Tháng 5</option>
                            <option value="06 " <%=DateTime.Now.ToString("MM") == "06" ? "selected" : "" %>>Tháng 6</option>
                            <option value="07 " <%=DateTime.Now.ToString("MM") == "07" ? "selected" : "" %>>Tháng 7</option>
                            <option value="08 " <%=DateTime.Now.ToString("MM") == "08" ? "selected" : "" %>>Tháng 8</option>
                            <option value="09 " <%=DateTime.Now.ToString("MM") == "09" ? "selected" : "" %>>Tháng 9</option>
                            <option value="10 " <%=DateTime.Now.ToString("MM") == "10" ? "selected" : "" %>>Tháng 10</option>
                            <option value="11" <%=DateTime.Now.ToString("MM") == "11" ? "selected" : "" %>>Tháng 11</option>
                            <option value="12 " <%=DateTime.Now.ToString("MM") == "12" ? "selected" : "" %>>Tháng 12</option>
                        </select>
                    </div>
                    
                </div>
                <div class="form-group row text-search align-items-center">
                 
                     <label class="col-sm-2 control-label" >Lựa chọn kiểu hiển thị: </label>
                     <div class="col-sm-3">
                        <select class="form-control" id="" name="">
                            <option value="80000">Theo loại hình dự án</option>
                            <option value="80007">Theo đơn vị</option>
                            <option value="80008">Theo dự án</option>
                        </select>                 
                    </div>
                    
                    <div class="col-sm-4"> 
                        <button type="button" class="btn btn-primary thongke">Thống kê</button>
                        &nbsp; &nbsp; 

                        <button type="button" class="btn btn-primary" id="btnExel">Excel</button>
                    </div>
                    </div>
                    
                     </div>
            </div>
        </div>
	
        <hr />
        <div class="mainContentBaoCao">
           <div style="">
                <div style="width: 30%; float: left; text-align:center">
                      <p style="font-size: 12pt; width: 100%"><b class="TieuDeTrai1"></b></p>
                    <p style="font-size: 10pt; width: 100%"><b class="TieuDeTrai2"></b></p>
                </div>
                <div style="width: 70%; float: left; text-align:center">
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeGiua1"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeGiua2"></b></p>
                    
                </div>
              
                <div style="width: 15%; float: left; text-align:center">
                      <p style="font-size: 12pt; width: 100%"><p class="LoaiBM"></p></p>
                </div>
                <div style="width: 100%; float: left; text-align:center">
                   <p style="font-size: 12pt; width: 100%"><b class="Title"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b> <i class="Thang"></i></b><b> <i class="Nam"></i></b></p>
               </div>
            </div>
       
            <div class="smtable_header" style="float:left;overflow-x:scroll">
                <table class="smtable" style="width:100%">
                    <thead>
                        <tr>
                            <th style="width:3%; ">STT</th>
                            <th style="width:7%; ">Danh mục</th>
                            <th style="width:20%; "colspan="4">Kế Hoạch giao trong năm </th>
                            <th style="width:20%; "colspan="4">Thực hiện tháng trước </th>                             
                            <th style="width:20%;"colspan="4">Ước tháng báo cáo  </th>
                            <th style="width:20%; "colspan="4">Lũy kế thực hiện từ đầu năm</th>
                            <th style="width:8%; "rowspan="2">Giá trị nghiệm thu từ đầu năm</th>
                            <th style="width:8%; "rowspan="2">Giá trị phiếu giá từ đầu năm</th>
                            <th style="width:8%; "rowspan="2">Giá trị giải ngân từ đầu năm</th>
                           
                            
                        </tr>
                        <tr>                              
                            <th ></th>  
                            <th ></th> 
                            <th >Tổng số </th>  
                            <th >Xây lắp </th>  
                            <th >Thiết bị</th>  
                            <th >Khác</th>  
                            <th >Tổng số </th>  
                            <th >Xây lắp </th>  
                            <th >Thiết bị</th>  
                            <th >Khác</th> 
                            <th >Tổng số </th>  
                            <th >Xây lắp </th>  
                            <th >Thiết bị</th>  
                            <th >Khác</th> 
                            <th >Tổng số </th>  
                            <th >Xây lắp </th>  
                            <th >Thiết bị</th>  
                            <th >Khác</th> 
                            
                             
                        </tr>
                        <tr>                              
                            <th >1</th>  
                            <th >2</th>  
                            <th >3</th>  
                            <th >4</th>  
                            <th >5</th>  
                            <th >6</th>  
                            <th >7</th>  
                            <th >8</th>
                            <th >9</th>  
                            <th >10</th>
                            <th >11</th>  
                            <th >12</th>  
                            <th >13</th>  
                            <th >14</th>  
                            <th >15</th>  
                            <th >16</th>  
                            <th >17</th>  
                            <th >18</th>  
                            <th >19</th>  
                            <th >20</th>  
                            <th >21</th>  
                        </tr>
                    </thead>
                    <tbody class="clsContent">
                    </tbody>
                </table>
            </div>
                 <div class="text-center mt-4 checkdatanull" style="width:100% ; float: left ">
                <p style="font-size: 12pt; width: 100%">Chưa có dữ liệu </p>
            </div>
            <script id="javascriptTemplate" type="text/x-kendo-template">
            <tr class="clsvbd" data-id="">
                <td style="width: 3%;text-align:center;" >#=STT !=null ? STT : ""#</td>
                <td style="width: 7%;word-wrap: break-word; text-align:left;">#=LoaiHinh !=null ? LoaiHinh  : ""#</td>
                <td style="width: 5%;word-wrap: break-word; text-align:right;">#=TongSoKH !=null ? formatMoney(TongSoKH):""#</td>
                <td style="width: 5%;word-wrap: break-word; text-align:right;">#=XayLapKH !=null ?formatMoney(XayLapKH) :""#</td>                          
                <td style="width:5%;word-wrap: break-word; text-align:right;">#=ThietBiKH !=null ?formatMoney(ThietBiKH) :""#</td> 
                <td style="width:5%;word-wrap: break-word; text-align:right;">#=KhacKH !=null ?formatMoney(KhacKH) :""#</td> 
                <td style="width:5%;word-wrap: break-word; text-align:right;">#=TongSoTH !=null ?formatMoney(TongSoTH) :""#</td> 
                <td style="width:5%;word-wrap: break-word; text-align:right;">#=XayLapTH !=null ?formatMoney(XayLapTH) :""#</td> 
                <td style="width:5%;word-wrap: break-word; text-align:right;">#=ThietBiTH !=null ?formatMoney(ThietBiTH) :""#</td> 
                <td style="width:5%;word-wrap: break-word; text-align:right;">#=KhacTH !=null ?formatMoney(KhacTH) :""#</td> 
                <td style="width:5%;word-wrap: break-word; text-align:right;">#=TongSoUocBC!=null?formatMoney(TongSoUocBC):""#</td> 
                <td style="width:5%;word-wrap: break-word; text-align:right;" >#=XayLapUocBC!=null?formatMoney(XayLapUocBC):""#</td>                          
                <td style="width:5%;word-wrap: break-word; text-align:right;" >#=ThietBiUocBC!=null?formatMoney(ThietBiUocBC):""#</td>                          
                <td style="width:5%;word-wrap: break-word; text-align:right;" >#=KhacUocBC!=null?formatMoney(KhacUocBC):""#</td>                          
                <td style="width:5%;word-wrap: break-word; text-align:right;" >#=TongSoLK!=null?formatMoney(TongSoLK):""#</td>                          
                <td style="width:5%;word-wrap: break-word; text-align:right;" >#=XayLapLK!=null?formatMoney(XayLapLK):""#</td>                          
                <td style="width:5%;word-wrap: break-word; text-align:right;" >#=ThietBiLK!=null?formatMoney(ThietBiLK):""#</td>                          
                <td style="width:5%;word-wrap: break-word; text-align:right;" >#=KhacLK!=null? formatMoney(KhacLK):""#</td>                          
                <td style="width:8%;word-wrap: break-word; text-align:right;">#=GiaTriNghiemThu !=null? formatMoney(GiaTriNghiemThu):""#</td>                          
                <td style="width:8%;word-wrap: break-word; text-align:right;">#=GiaTriPhieu !=null? formatMoney(GiaTriPhieu):""#</td>                          
                <td style="width:8%;word-wrap: break-word; text-align:right;">#=GiaTriGiaiNgan !=null? formatMoney(GiaTriGiaiNgan):""#</td>                          
                                     
                 
            </tr>
            </script> 
             <div style="">
                  <div style="width: 50%; float: right; text-align: center; margin-top: 15px;">
                    <p style="width: 100%"><b>Ngày          </b><b>Tháng            </b><b>Năm</b></p>
                    <p style="font-size: 12pt; width: 100%" class="chucvu"><b>Người ký</b></p>
                      <p style="width: 100%"><i>(Ký và ghi rõ họ tên)</i></p>
                    <br />
                    <br />
                    <br />
                    <br />
                    <p style="font-size: 12pt; width: 100%"><p class="NguoiKy"></p></p>
                </div>
                 <div style="width: 50%;  text-align: center; margin-top: 15px;">
                    <p style="width: 100%"><b>NGƯỜI LẬP BIỂU</b></p>
                     <p style="width: 100%"><i>(Ký và ghi rõ họ tên)</i></p>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <p style="font-size: 12pt; width: 100%"><p class="NguoiLapBieu"></p></p>
                </div>
               
            </div>

        </div>
    </div
</body>
    <script type="text/javascript">
        $(document).ready(function () {
            ThongKe();
            $(".thongke").click(function () {
                ThongKe();
            });
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });

            $("#btnExel").click(function () {
                var $datathongke = $(".zone_search").siSerializeDivFrm();
                var str = jQuery.param($datathongke);
                var Url = "/VIPortalAPI/api/LinhVucDauTuXayDung/QUERYDATA_ThuHienKeHoachDTXD?excel=1&" + str + "&TitleBC=" + $("#TitleBC").text();
                my_window = window.open(Url, "mywindow1");
            });
            $("#btnword").click(function () {
                var $datathongke = $(".zone_search").siSerializeDivFrm();
                var str = jQuery.param($datathongke);
                var Url = "/VIPortalAPI/api/LinhVucDauTuXayDung/QUERYDATA_ThuHienKeHoachDTXD?word=1&" + str + "&TitleBC=" + $("#TitleBC").text();
                my_window = window.open(Url, "mywindow1");

            });
        });
        function ThongKe() {
            $("#tungay").text($("#SearchTuNgay").val());
            $("#denngay").text($("#SearchDenNgay").val());
            var data = $(".zone_search").siSerializeArray();
            loading();
            $.post("/VIPortalAPI/api/LinhVucDauTuXayDung/QUERYDATA_ThuHienKeHoachDTXD?", data, function (data) {

                $(".Title").text(data.Title);
                $(".TieuDeTrai1").text(data.TieuDeTrai1);
                $(".TieuDeTrai2").text(data.TieuDeTrai2);
                $(".TieuDeGiua1").text(data.TieuDeGiua1);
                $(".TieuDeGiua2").text(data.TieuDeGiua2);
                $(".Nam").text(data.Nam);
                $(".Thang").text(data.Thang);


                var htmlTEmp = '';
                var templatehtmltr = kendo.template($("#javascriptTemplate").html());
                $(".mainContentBaoCao .clsContent").html("");
                if (data.data.length > 0) {
                    $(".checkdatanull").hide();
                    for (var i = 0; i < data.data.length; i++) {
                        data.data[i]["STT"] = i + 1;
                        htmlTEmp += templatehtmltr(data.data[i]);
                    }

                    $(".mainContentBaoCao .clsContent").append(htmlTEmp);
                }
                $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');

                $('#tongbanghi').text("Có tổng số: " + data.data.length + " bản ghi");

                $(".clsvbd").css('cursor', 'pointer');
               
            });
            Endloading();
        }
    </script>
</html>