﻿<%@ page language="C#" autoeventwireup="true" codebehind="ThucHienDauTuTheoCongTrinhHangMucCongTrinh.aspx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.BaoCaoTaiChinh.ThucHienDauTuTheoCongTrinhHangMucCongTrinh" %>



<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link type="text/css" href="/Content/dist/css/StyleBaoCao.css" rel="stylesheet" />
    <style type="text/css">
        .zone_search {
            overflow-x: hidden;
        }
    </style>
</head>
<body>
    <div id="BaoCaoSoDangKy" class="smgrid" data-title="Văn bản đến" data-grid="grid_BaoCaoSoDKVanBanDen"
        data-form="/iVBDH/VanBanDen/pFormVanBanDen.aspx" data-formwidth="1200" data-action="/Vbden/api/VanBanDen/VBDGetListJsonGroupSolr" data-view="/Vbden/VanBanDen/pViewVanBanDen.aspx">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />
                <input type="hidden" name="CurentUser" value="<%=CurentUser.Title %>" />
                <div class="title_icon">
                </div>
                <div class="form-group row text-search align-items-center">

                    <label class="col-sm-2 control-label">Tháng:</label>
                    <div class="col-sm-2">
                        <select class="form-control" id="ThangBaoCao" name="ThangBaoCao">
                            <option value="01" <%=DateTime.Now.ToString("MM") == "01" ? "selected" : "" %>>Tháng 1</option>
                            <option value="02 " <%=DateTime.Now.ToString("MM") == "02" ? "selected" : "" %>>Tháng 2</option>
                            <option value="03 " <%=DateTime.Now.ToString("MM") == "03" ? "selected" : "" %>>Tháng 3</option>
                            <option value="04 " <%=DateTime.Now.ToString("MM") == "04" ? "selected" : "" %>>Tháng 4</option>
                            <option value="05 " <%=DateTime.Now.ToString("MM") == "05" ? "selected" : "" %>>Tháng 5</option>
                            <option value="06 " <%=DateTime.Now.ToString("MM") == "06" ? "selected" : "" %>>Tháng 6</option>
                            <option value="07 " <%=DateTime.Now.ToString("MM") == "07" ? "selected" : "" %>>Tháng 7</option>
                            <option value="08 " <%=DateTime.Now.ToString("MM") == "08" ? "selected" : "" %>>Tháng 8</option>
                            <option value="09 " <%=DateTime.Now.ToString("MM") == "09" ? "selected" : "" %>>Tháng 9</option>
                            <option value="10 " <%=DateTime.Now.ToString("MM") == "10" ? "selected" : "" %>>Tháng 10</option>
                            <option value="11" <%=DateTime.Now.ToString("MM") == "11" ? "selected" : "" %>>Tháng 11</option>
                            <option value="12 " <%=DateTime.Now.ToString("MM") == "12" ? "selected" : "" %>>Tháng 12</option>
                        </select>
                    </div>

                    <label class="col-sm-3 control-label text-right">Năm:</label>
                    <div class="col-sm-2">
                        <input type="text" name="SearchNam" id="SearchNam" class="form-control " placeholder="Nhập năm" value="<%=DateTime.Now.ToString("yyyy") %>" />
                    </div>

                </div>
                <div class="form-group row text-search align-items-center">

                    <label class="col-sm-2 control-label">Đơn Vị:</label>
                    <div class="col-sm-5">
                        <select class="form-control" id="LoaiDV" name="LoaiDV">
                            <option value="all">Tất cả</option>
                            <option value="074000">Tổng Công ty điện lực TP Hà Nội</option>
                            <option value="070100">Công ty Điện lực Hoàn Kiếm</option>
                            <option value="070200">Công ty Điện lực Hai Bà Trưng</option>
                            <option value="070300">Công ty Điện lực Ba Đình</option>
                            <option value="070400">Công ty Điện lực Đống Đa</option>
                            <option value="070500">Công ty Điện lực Nam Từ Liêm</option>
                            <option value="070600">Công ty Điện lực Thanh Trì</option>
                            <option value="070700">Công ty Điện lực Gia Lâm</option>
                            <option value="070800">Công ty Điện lực Đông Anh</option>
                            <option value="070900">Công ty Điện lực Sóc Sơn</option>
                            <option value="071000">Công ty Điện lực Tây Hồ</option>
                            <option value="071100">Công ty Điện lực Thanh Xuân</option>
                            <option value="071200">Công ty Điện lực Cầu Giấy</option>
                            <option value="071300">Công ty Điện lực Hoàng Mai</option>
                            <option value="071400">Công ty Điện lực Long Biên</option>
                            <option value="071500">Công ty Điện lực Mê Linh</option>
                            <option value="071600">Công ty Điện lực Hà Đông</option>
                            <option value="071700">Công ty Điện lực Sơn Tây</option>
                            <option value="071800">Công ty Điện lực Chương Mỹ</option>
                            <option value="071900">Công ty Điện lực Thạch Thất</option>
                            <option value="072000">Công ty Điện lực Thường Tín</option>
                            <option value="072100">Công ty Điện lực Ba Vì</option>
                            <option value="072200">Công ty Điện lực Đan Phượng</option>
                            <option value="072300">Công ty Điện lực Hoài Đức</option>
                            <option value="072400">Công ty Điện lực Mỹ Đức</option>
                            <option value="072500">Công ty Điện lực Phú Xuyên</option>
                            <option value="072600">Công ty Điện lực Phúc Thọ</option>
                            <option value="072700">Công ty Điện lực Quốc Oai</option>
                            <option value="072800">Công ty Điện lực Thanh Oai</option>
                            <option value="072900">Công ty Điện lực Ứng Hòa</option>
                            <option value="073000">Công ty Điện lực Bắc Từ Liêm</option>
                            <option value="073100">Công ty Công nghệ thông tin Điện lực Hà Nội</option>
                            <option value="073200">Trung tâm Điều độ Hệ thống điện TP Hà Nội</option>
                            <option value="073500">Công ty Thí nghiệm điện Điện lực Hà Nội</option>
                            <option value="073600">Công ty Lưới điện cao thế TP Hà Nội</option>
                            <option value="073700">Công ty Dịch vụ Điện lực Hà Nội</option>
                            <%--<option value="073901">Ban Quản lý dự án Lưới điện Hà Nội - DEP</option>--%>
                            <option value="073902">Ban Quản lý dự án Lưới điện Hà Nội - XDCB</option>
                            <%--<option value="073801">Ban Quản lý dự án phát triển Điện lực Hà Nội - ADB</option>--%>
                            <option value="073802">Ban Quản lý dự án phát triển Điện lực Hà Nội - XDCB</option>
                            <%--<option value="000100">Tập đoàn Điện lực Việt Nam - Kế toán ngành</option>--%>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-primary thongke">Thống kê</button>
                        <button type="button" class="btn btn-primary" id="btnExel">Excel</button>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="mainContentBaoCao">
            <div style="">
                <div style="width: 30%; float: left; text-align: center">
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeTrai1"></b></p>
                    <p style="font-size: 10pt; width: 100%"><b class="TieuDeTrai2"></b></p>
                </div>
                <div style="width: 55%; float: left; text-align: center">
                    <p style="font-size: 12pt; width: 100%"><b class="Title"></b></p>
                    <p style="font-size: 12pt; width: 100%">Tháng <i class="Thang"></i>- năm <i class="Nam"></i></p>
                </div>
                <div style="width: 15%; float: left; text-align: center">
                    <p style="font-size: 12pt; width: 100%">
                        <p class="LoaiBM"></p>
                    </p>
                    <p style="font-size: 10pt; width: 100%" class="DVTinh"></p>
                </div>


            </div>
             <div class="smtable_header" style="float:left;overflow-x:scroll" >
                <table class="smtable" style="width:150%" >
                    <thead>
                        <tr>
                            <th style="width: 5%" rowspan="4">STT</th>
                            <th style="width: 25%" rowspan="4">Công trình, hạng mục công trình</th>
                            <th style="width: 15%;" rowspan="4">Tổng dự toán</th>
                            <th style="width: 15%;" rowspan="4">Số dư đầu năm thực hiện</th>
                            <th style="width: 135%;" colspan="9">Thực hiện đầu tư trong Kỳ báo cáo</th>
                            <th style="width: 15%;" rowspan="4">Thực hiện đầu tư tăng từ đầu năm đến cuối Kỳ báo cáo</th>
                            <th style="width: 15%;" rowspan="4">Thực hiện đầu tư giảm từ đầu năm đến cuối Kỳ báo cáo</th>
                            <th style="width: 20%;" rowspan="4">Số dư cuối Kỳ thực hiện</th>
                        </tr>
                        <tr>
                            <th rowspan="3">Tổng số</th>
                            <th colspan="8">Trong đó</th>

                        </tr>
                        <tr>
                            <th rowspan="2">Xây dựng</th>
                            <th colspan="2">Chi phí thiết bị</th>
                            <th rowspan="2">Chi phí bồi thường và hỗ trợ tái định cư</th>
                            <th rowspan="2">Chi phí quản lý dự án</th>
                            <th rowspan="2">Chi phí tư vấn đầu tư xây dựng</th>
                            <th colspan="2">KTCB khác</th>

                        </tr>
                        <tr>
                            <th>Lắp đặt</th>
                            <th>Thiết bị</th>
                            <th>Tổng số</th>
                            <th>Trong đó: chi phí lãi vay</th>

                        </tr>
                        <tr>
                            <th>A</th>
                            <th>B</th>
                            <th>1</th>
                            <th>2</th>
                            <th>3</th>
                            <th>4</th>
                            <th>5</th>
                            <th>6</th>
                            <th>7</th>
                            <th>8</th>
                            <th>9</th>
                            <th>10</th>
                            <th>11</th>
                            <th>12</th>
                            <th>13</th>
                            <th>14</th>


                        </tr>


                    </thead>
                    <tbody class="clsContent">
                    </tbody>
                </table>
            </div>
            <div class="chekdata mt-3" style="  width: 100% ;    float: left;text-align: center">
                <p style="font-size: 12pt; width: 100%">Không có bản ghi </p>
            </div>
            <script id="javascriptTemplate" type="text/x-kendo-template">
            <tr class="clsvbd" data-id="">
                 <td style="width: 5%; text-align:center;" >#=STT#</td>
                 <td style="width: 25%;word-wrap: break-word;" >#=DISPLAY_ATT1 != null ? DISPLAY_ATT1 : ""#-#=CongTrinhHangMucCT !=null ?CongTrinhHangMucCT :""#</td>
                 <td style="width: 15%; text-align: right;word-wrap: break-word;" >#=TongDuDoan != null ?formatMoney(TongDuDoan) :""#</td>
                 <td style="width: 15%; text-align: right;word-wrap: break-word;" >#=SoDuDauNamThucHien !=null ? formatMoney(SoDuDauNamThucHien) :"" #</td>
                 <td style="width: 15%; text-align: right;word-wrap: break-word;" >#=TongSo != null ? formatMoney(TongSo) : ""#</td>
                 <td style="width: 15%; text-align: right;word-wrap: break-word;" >#=XayDung != null ? formatMoney(XayDung) :""#</td>
                 <td style="width: 15%; text-align: right;word-wrap: break-word;" >#=LapDat != null ? formatMoney(LapDat) :""#</td>
                 <td style="width: 15%; text-align: right;word-wrap: break-word;" >#=ThietBi != null ? formatMoney(ThietBi) : ""#</td>
                 <td style="width: 15%; text-align: right;word-wrap: break-word;" >#=ChiPhiBoiThuong !=null ? formatMoney(ChiPhiBoiThuong) : ""#</td>
                 <td style="width: 15%; text-align: right;word-wrap: break-word;" >#=ChiPhiQuanLy !=null ? formatMoney(ChiPhiQuanLy) : "" #</td>
                 <td style="width: 15%; text-align: right;word-wrap: break-word;" >#=ChiPhiTuVanDauTuXayDung != null ? formatMoney(ChiPhiTuVanDauTuXayDung) : ""#</td>
                 <td style="width: 15%; text-align: right;word-wrap: break-word;" >#=TongSoKhac !=null ? formatMoney(TongSoKhac) :"" #</td>
                 <td style="width: 15%; text-align: right;word-wrap: break-word;" >#=ChiPhiLaiVay != null ? formatMoney(ChiPhiLaiVay) : "" #</td>
                 <td style="width: 15%; text-align: right;word-wrap: break-word;" >#=ThucHienDauTuTang !=null ?formatMoney(ThucHienDauTuTang) :""#</td>
                 <td style="width: 15%; text-align: right;word-wrap: break-word;" >#=ThucHienDauTuGiam !=null ?formatMoney(ThucHienDauTuGiam) : ""#</td>
                 <td style="width: 20%; text-align: right;word-wrap: break-word;" >#=SoDuCuoiKyThucHien != null ? formatMoney(SoDuCuoiKyThucHien) : ""#</td>
            </tr>
            </script>

            <div style="">


                <div style="width: 33%; float: right; text-align: center; margin-top: 15px;">
                    <p style="width: 100%">Hà nội, ngày <i class="Ngay"></i>tháng <i class="Thang"></i>năm <i class="Nam"></i></p>
                    <p style="width: 100%">THỦ TRƯỞNG ĐƠN VỊ</p>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <p style="font-size: 12pt; width: 100%">
                        <p class="NguoiKy"></p>
                    </p>
                    <p style="font-size: 12pt; width: 100%">
                        <p class="">(Ký, họ tên, đóng dấu)</p>
                    </p>
                </div>
                <div style="width: 33%; float: right; text-align: center; margin-top: 15px;">
                    <br />
                    <p style="width: 100%">KẾ TOÁN TRƯỞNG  </p>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <p style="font-size: 12pt; width: 100%">
                        <p class="KeToanTruong"></p>
                    </p>
                    <p style="font-size: 12pt; width: 100%">
                        <p class="">(Ký, họ tên)</p>
                    </p>
                </div>
                <div style="width: 33%; float: right; text-align: center; margin-top: 15px;">
                    <br />
                    <p style="width: 100%">NGƯỜI LẬP BIỂU </p>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <p style="font-size: 12pt; width: 100%">
                        <p class="NguoiLapBieu"></p>
                    </p>
                    <p style="font-size: 12pt; width: 100%">
                        <p class="">(Ký, họ tên)</p>
                    </p>
                </div>

            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        ThongKe();
        $(".thongke").click(function () {
            ThongKe();
        });
        $(".input-datetime").daterangepicker({
            singleDatePicker: true,
            locale: { format: 'DD/MM/YYYY' }
        });

        $("#btnExel").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BaoCaoTaiChinh/QUERYDATA_ThucHienDauTuTheoCongTrinhHangMucCongTrinh?excel=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");
        });
        $("#btnword").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BaoCaoTaiChinh/QUERYDATA_ThucHienDauTuTheoCongTrinhHangMucCongTrinh?word=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");

        });
    });
    function ThongKe() {
        $("#tungay").text($("#SearchTuNgay").val());
        $("#denngay").text($("#SearchDenNgay").val());
        var data = $(".zone_search").siSerializeArray();
        loading();
        $.post("/VIPortalAPI/api/BaoCaoTaiChinh/QUERYDATA_ThucHienDauTuTheoCongTrinhHangMucCongTrinh?", data, function (data) {

            $(".Title").text(data.Title);
            $(".LoaiBM").text(data.LoaiBM);
            $(".TieuDeTrai1").text(data.TieuDeTrai1);
            $(".TieuDeTrai2").text(data.TieuDeTrai2);
            $(".NguoiKy").text(data.NguoiKy);
            $(".Thang").text(data.Thang);
            $(".Nam").text(data.Nam);
            $(".NguoiLapBieu").text(data.NguoiLapBieu);
            $(".KeToanTruong").text(data.KeToanTruong);
            $(".DVTinh").text(data.DVTinh);



            var htmlTEmp = '';
            var templatehtmltr = kendo.template($("#javascriptTemplate").html());
            $(".mainContentBaoCao .clsContent").html("");
            if (data.data.length > 0) {
                $(".chekdata").hide();
                for (var i = 0; i < data.data.length; i++) {
                    data.data[i]["STT"] = i + 1;
                    htmlTEmp += templatehtmltr(data.data[i]);
                }
                $('#tongbanghi').text("Có tổng số: " + data.data.length + " bản ghi");

                $(".mainContentBaoCao .clsContent").append(htmlTEmp);
            } else {
                $(".chekdata").show();

            }
            $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');


            $(".clsvbd").css('cursor', 'pointer');  
        });
        Endloading();

    }

</script>
</html>




