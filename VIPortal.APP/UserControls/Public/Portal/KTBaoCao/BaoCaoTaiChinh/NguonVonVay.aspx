﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NguonVonVay.aspx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.BaoCaoTaiChinh.NguonVonVay" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link type="text/css" href="/Content/dist/css/StyleBaoCao.css" rel="stylesheet" />
    <style type="text/css">
        .zone_search {
            overflow-x: hidden;
        }
    </style>
</head>
<body>
    <div id="BaoCaoSoDangKy" class="smgrid" data-title="Văn bản đến" data-grid="grid_BaoCaoSoDKVanBanDen"
        data-form="/iVBDH/VanBanDen/pFormVanBanDen.aspx" data-formwidth="1200" data-action="/Vbden/api/VanBanDen/VBDGetListJsonGroupSolr" data-view="/Vbden/VanBanDen/pViewVanBanDen.aspx">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />
                <input type="hidden" name="CurentUser" value="<%=CurentUser.Title %>" />
                <div class="title_icon">
                </div>
                <div class="form-group row text-search align-items-center">

                    <label class="col-sm-2 control-label">Tháng:</label>
                    <div class="col-sm-2">
                        <select class="form-control" id="ThangBaoCao" name="ThangBaoCao">
                            <option value="01" <%=DateTime.Now.ToString("MM") == "01" ? "selected" : "" %>>Tháng 1</option>
                            <option value="02 " <%=DateTime.Now.ToString("MM") == "02" ? "selected" : "" %>>Tháng 2</option>
                            <option value="03 " <%=DateTime.Now.ToString("MM") == "03" ? "selected" : "" %>>Tháng 3</option>
                            <option value="04 " <%=DateTime.Now.ToString("MM") == "04" ? "selected" : "" %>>Tháng 4</option>
                            <option value="05 " <%=DateTime.Now.ToString("MM") == "05" ? "selected" : "" %>>Tháng 5</option>
                            <option value="06 " <%=DateTime.Now.ToString("MM") == "06" ? "selected" : "" %>>Tháng 6</option>
                            <option value="07 " <%=DateTime.Now.ToString("MM") == "07" ? "selected" : "" %>>Tháng 7</option>
                            <option value="08 " <%=DateTime.Now.ToString("MM") == "08" ? "selected" : "" %>>Tháng 8</option>
                            <option value="09 " <%=DateTime.Now.ToString("MM") == "09" ? "selected" : "" %>>Tháng 9</option>
                            <option value="10 " <%=DateTime.Now.ToString("MM") == "10" ? "selected" : "" %>>Tháng 10</option>
                            <option value="11" <%=DateTime.Now.ToString("MM") == "11" ? "selected" : "" %>>Tháng 11</option>
                            <option value="12 " <%=DateTime.Now.ToString("MM") == "12" ? "selected" : "" %>>Tháng 12</option>
                        </select>
                    </div>

                    <label class="col-sm-3 control-label text-right">Năm:</label>
                    <div class="col-sm-2">
                        <input type="text" name="SearchNam" id="SearchNam" class="form-control " placeholder="Nhập năm" value="<%=DateTime.Now.ToString("yyyy") %>" />
                    </div>

                </div>
                <div class="form-group row text-search align-items-center">

                    <label class="col-sm-2 control-label">Đơn Vị:</label>
                    <div class="col-sm-5">
                        <select class="form-control" id="LoaiDV" name="LoaiDV">
                            <option value="all">Tất cả</option>
                            <option value="074000">Tổng Công ty điện lực TP Hà Nội</option>
                            <option value="070100">Công ty Điện lực Hoàn Kiếm</option>
                            <option value="070200">Công ty Điện lực Hai Bà Trưng</option>
                            <option value="070300">Công ty Điện lực Ba Đình</option>
                            <option value="070400">Công ty Điện lực Đống Đa</option>
                            <option value="070500">Công ty Điện lực Nam Từ Liêm</option>
                            <option value="070600">Công ty Điện lực Thanh Trì</option>
                            <option value="070700">Công ty Điện lực Gia Lâm</option>
                            <option value="070800">Công ty Điện lực Đông Anh</option>
                            <option value="070900">Công ty Điện lực Sóc Sơn</option>
                            <option value="071000">Công ty Điện lực Tây Hồ</option>
                            <option value="071100">Công ty Điện lực Thanh Xuân</option>
                            <option value="071200">Công ty Điện lực Cầu Giấy</option>
                            <option value="071300">Công ty Điện lực Hoàng Mai</option>
                            <option value="071400">Công ty Điện lực Long Biên</option>
                            <option value="071500">Công ty Điện lực Mê Linh</option>
                            <option value="071600">Công ty Điện lực Hà Đông</option>
                            <option value="071700">Công ty Điện lực Sơn Tây</option>
                            <option value="071800">Công ty Điện lực Chương Mỹ</option>
                            <option value="071900">Công ty Điện lực Thạch Thất</option>
                            <option value="072000">Công ty Điện lực Thường Tín</option>
                            <option value="072100">Công ty Điện lực Ba Vì</option>
                            <option value="072200">Công ty Điện lực Đan Phượng</option>
                            <option value="072300">Công ty Điện lực Hoài Đức</option>
                            <option value="072400">Công ty Điện lực Mỹ Đức</option>
                            <option value="072500">Công ty Điện lực Phú Xuyên</option>
                            <option value="072600">Công ty Điện lực Phúc Thọ</option>
                            <option value="072700">Công ty Điện lực Quốc Oai</option>
                            <option value="072800">Công ty Điện lực Thanh Oai</option>
                            <option value="072900">Công ty Điện lực Ứng Hòa</option>
                            <option value="073000">Công ty Điện lực Bắc Từ Liêm</option>
                            <option value="073100">Công ty Công nghệ thông tin Điện lực Hà Nội</option>
                            <option value="073200">Trung tâm Điều độ Hệ thống điện TP Hà Nội</option>
                            <option value="073500">Công ty Thí nghiệm điện Điện lực Hà Nội</option>
                            <option value="073600">Công ty Lưới điện cao thế TP Hà Nội</option>
                            <option value="073700">Công ty Dịch vụ Điện lực Hà Nội</option>
                            <%--<option value="073901">Ban Quản lý dự án Lưới điện Hà Nội - DEP</option>--%>
                            <option value="073902">Ban Quản lý dự án Lưới điện Hà Nội - XDCB</option>
                            <%--<option value="073801">Ban Quản lý dự án phát triển Điện lực Hà Nội - ADB</option>--%>
                            <option value="073802">Ban Quản lý dự án phát triển Điện lực Hà Nội - XDCB</option>
                            <%--<option value="000100">Tập đoàn Điện lực Việt Nam - Kế toán ngành</option>--%>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-primary thongke">Thống kê</button>
                        <button type="button" class="btn btn-primary" id="btnExel">Excel</button>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="mainContentBaoCao">
            <div style="">
                <div style="width: 30%; float: left; text-align:center">
                      <p style="font-size: 12pt; width: 100%"><b class="TieuDeTrai1"></b></p>
                    <p style="font-size: 10pt; width: 100%"><b class="TieuDeTrai2"></b></p>
                </div>
                <div style="width: 55%; float: left; text-align:center">
                    <p style="font-size: 12pt; width: 100%"><b class="Title NDTieude"></b></p>
                    <p style="font-size: 12pt; width: 100%">Tháng <i class="Thang thang"></i> - Năm <i class="Nam nam"></i></p>
                </div>
                <div style="width: 15%; float: left; text-align:center">
                      <p style="font-size: 12pt; width: 100%"><p class="LoaiBM"></p></p>
                     <p  style="font-size: 10pt; width: 100%" class="DVTinh"></p>
                </div>
            </div>
            <div style="text-align: right; width: 100%; clear: both;"><span class="" style="margin-right: 1.25%;" id="tongbanghi"></span></div>    
               <div class="smtable_header" style="float:left;overflow-x:scroll" >
                <table class="smtable" style="width:150%" >
                    <thead>
                        <tr>
                            <th style="width: 5%" rowspan="3">Số TT</th>
                            <th style="width: 25%" rowspan="3" >Tên công trình</th>                             
                            <th style="width: 25%"rowspan="3">Đối tác vay</th>
                            <th style="width: 40%"colspan="4">Hợp đồng vay</th>   
                            <th style="width: 10%"rowspan="3">Số dư đầu năm</th>
                            <th style="width: 33%"colspan="3">Phát sinh tăng</th> 
                            <th style="width: 33%"colspan="3">Phát sinh giảm</th> 
                            <th style="width: 20%"colspan="2">Dư nợ vay cuối quý</th> 
                            
                        </tr>
                        <tr>                              
                            <th colspan="3">Vay ngoại tệ</th>                            
                            <th rowspan="2">Vay bằng VNĐ </th>                             
                            <th rowspan="2">Kỳ báo cáo</th>                             
                            <th rowspan="2">Từ đầu năm đến cuối kỳ báo cáo</th>                            
                            <th rowspan="2">Lũy kế từ khi bắt đầu rút vốn theo hợp đồng đến cuối kỳ báo cáo </th>    
                            <th rowspan="2">Kỳ báo cáo</th>                             
                            <th rowspan="2">Từ đầu năm đến cuối kỳ báo cáo</th>                            
                            <th rowspan="2">Lũy kế từ khi bắt đầu rút vốn theo hợp đồng đến cuối kỳ báo cáo </th>    
                            <th rowspan="2">Tổng số</th>
                            <th rowspan="2">Nợ đến hạn phải trả</th>
                        </tr>
                        <tr>
                            <th >Đơn vị tính</th>  
                            <th >Nguyên tệ</th>
                            <th >Quy đổi VNĐ</th>
                        </tr>
                        <tr>   
                            <th >A</th>  
                            <th >B</th>
                            <th >1</th>  
                            <th >2</th>  
                            <th >3</th>  
                            <th >4</th>
                            <th >5</th>  
                            <th >6</th>  
                            <th >7</th>
                            <th >8</th> 
                            <th >9</th>  
                            <th >10</th>
                            <th >11</th> 
                            <th >12</th>  
                            <th >13</th> 
                            <th >14</th>
                              
                        </tr>
                        <tr>
                            <th style="text-align:left" colspan="3">Tài khoản: 33634100000</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>

                        </tr>
                    </thead>
                    <tbody class="clsContent">
                   
                    </tbody>
                    <thead>
                        <tr>
                        <th style="text-align:left" colspan="3">Tài khoản: 34112100000</th>
                    </tr>
                    </thead>
                    <tbody class="clsContent1">
                   
                    </tbody>
                    
                </table>
            </div>
                <div class="chekdata mt-3" style="text-align: center">
                <p style="font-size: 12pt; width: 100%">Không có bản ghi </p>
            </div>

            <script id="javascriptTemplate" type="text/x-kendo-template">
                #if(DoiTacVay=="HANOI - Văn phòng Tổng công ty"){#
            <tr class="clsvbd " data-id="" > 
                 <td style="width: 5%;text-align:center;">#=STT#</td>
                 <td style="width: 25%;word-wrap: break-word;">#=TenCongTrinh !=null ? TenCongTrinh : ""#</td>
                 <td style="width: 25%;word-wrap: break-word;">#=DoiTacVay  !=null ? DoiTacVay : ""#</td>
                 <td style="width: 10%;word-wrap: break-word;">#=DonViTinh !=null ? DonViTinh : ""#</td>
                 <td style="width: 10%;word-wrap: break-word;text-align:right;">#=NguyenTe !=null ? NguyenTe : ""#</td>
                 <td style="width: 10%;word-wrap: break-word;text-align:right;">#=QuyDoiVND !=null ? formatMoney(QuyDoiVND) : ""#</td>
                 <td style="width: 10%;word-wrap: break-word;text-align:right;">#=VayBangVND !=null ? formatMoney(VayBangVND) : ""#</td>
                 <td style="width:  10%;word-wrap: break-word;text-align:right;">#=SoDuDauNam !=null ? formatMoney(SoDuDauNam) : ""#</td>
                 <td style="width: 11%;word-wrap: break-word;text-align:right;">#=KyBaoCaoTang !=null ? formatMoney(KyBaoCaoTang) : ""#</td> 
                 <td style="width: 11%;word-wrap: break-word;text-align:right;">#=TuDauNamDenKyBaoCaoTang !=null ? formatMoney(TuDauNamDenKyBaoCaoTang) : ""#</td>
                 <td style="width: 11%;word-wrap: break-word;text-align:right;">#=LuyKeBatDauRutVonTang !=null ? formatMoney(LuyKeBatDauRutVonTang) : ""#</td>
                 <td style="width: 11%;word-wrap: break-word;text-align:right;">#=KyBaoCaoGiam !=null ? formatMoney(KyBaoCaoGiam) : ""#</td>
                 <td style="width: 11%;word-wrap: break-word;text-align:right;">#=TuDauNamDenKyBaoCaoGiam !=null ?formatMoney(TuDauNamDenKyBaoCaoGiam)  : ""#</td>
                 <td style="width: 11%;word-wrap: break-word;text-align:right;">#=LuyKeBatDauRutVonGiam !=null ? formatMoney(LuyKeBatDauRutVonGiam) : ""#</td>
                 <td style="width: 10%;word-wrap: break-word;text-align:right;">#=TongSo !=null ?formatMoney(TongSo)  : ""#</td>
                 <td style="width: 10%;word-wrap: break-word;text-align:right;">#=NoDenHanPhaiTra !=null ? formatMoney(NoDenHanPhaiTra)  : ""#</td> 
            </tr>#}#
            </script>

            <script id="javascriptTemplate1" type="text/x-kendo-template">
                #if(DoiTacVay!="HANOI - Văn phòng Tổng công ty"){#
            <tr class="clsvbd " data-id="" > 
                 <td style="width: 5%;text-align:center;">#=STT#</td>
                 <td style="width: 25%;word-wrap: break-word;">#=TenCongTrinh !=null ? TenCongTrinh : ""#</td>
                 <td style="width: 25%;word-wrap: break-word;">#=DoiTacVay  !=null ? DoiTacVay : ""#</td>
                 <td style="width: 10%;word-wrap: break-word;">#=DonViTinh !=null ? DonViTinh : ""#</td>
                 <td style="width: 10%;word-wrap: break-word;text-align:right;">#=NguyenTe !=null ? NguyenTe : ""#</td>
                 <td style="width: 10%;word-wrap: break-word;text-align:right;">#=QuyDoiVND !=null ? formatMoney(QuyDoiVND) : ""#</td>
                 <td style="width: 10%;word-wrap: break-word;text-align:right;">#=VayBangVND !=null ? formatMoney(VayBangVND) : ""#</td>
                 <td style="width:  10%;word-wrap: break-word;text-align:right;">#=SoDuDauNam !=null ? formatMoney(SoDuDauNam) : ""#</td>
                 <td style="width: 11%;word-wrap: break-word;text-align:right;">#=KyBaoCaoTang !=null ? formatMoney(KyBaoCaoTang) : ""#</td> 
                 <td style="width: 11%;word-wrap: break-word;text-align:right;">#=TuDauNamDenKyBaoCaoTang !=null ? formatMoney(TuDauNamDenKyBaoCaoTang) : ""#</td>
                 <td style="width: 11%;word-wrap: break-word;text-align:right;">#=LuyKeBatDauRutVonTang !=null ? formatMoney(LuyKeBatDauRutVonTang) : ""#</td>
                 <td style="width: 11%;word-wrap: break-word;text-align:right;">#=KyBaoCaoGiam !=null ? formatMoney(KyBaoCaoGiam) : ""#</td>
                 <td style="width: 11%;word-wrap: break-word;text-align:right;">#=TuDauNamDenKyBaoCaoGiam !=null ?formatMoney(TuDauNamDenKyBaoCaoGiam)  : ""#</td>
                 <td style="width: 11%;word-wrap: break-word;text-align:right;">#=LuyKeBatDauRutVonGiam !=null ? formatMoney(LuyKeBatDauRutVonGiam) : ""#</td>
                 <td style="width: 10%;word-wrap: break-word;text-align:right;">#=TongSo !=null ?formatMoney(TongSo)  : ""#</td>
                 <td style="width: 10%;word-wrap: break-word;text-align:right;">#=NoDenHanPhaiTra !=null ? formatMoney(NoDenHanPhaiTra)  : ""#</td> 
            </tr>#}#
            </script>
            
    </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        ThongKe();
    });
    $(document).ready(function () {
        $(".thongke").click(function () {
            ThongKe();
        });
        $(".input-datetime").daterangepicker({
            singleDatePicker: true,
            locale: { format: 'DD/MM/YYYY' }
        });

        $("#btnExel").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BaoCaoTaiChinh/QUERYDATA_NguonVonVay?excel=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");
        });
        $("#btnword").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BaoCaoTaiChinh/QUERYDATA_NguonVonVay?word=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");

        });
    });
    function loadGiaoDien() {
        $("#tungay").text($("#SearchTuNgay").val());
        $("#denngay").text($("#SearchDenNgay").val());
        var data = $(".zone_search").siSerializeArray();
        loading();
        $.post("/VIPortalAPI/api/BaoCaoTaiChinh/QUERYDATA_NguonVonVay?", data, function (data) {
            console.log(data.Title);
            
        

            var htmlTEmp = '';
            var htmlTEmp1 = '';
            var templatehtmltr = kendo.template($("#javascriptTemplate").html());
            var templatehtmltr1 = kendo.template($("#javascriptTemplate1").html());
        

            $(".mainContentBaoCao .clsContent").html("");
            if (data.clsContent.length > 0) {
                for (var i = 0; i < data.data.length; i++) {
                    data.data[i]["STT"] = i + 1;
                    htmlTEmp += templatehtmltr(data.data[i]);
                }

                $(".mainContentBaoCao .clsContent").append(htmlTEmp);
            }
            $(".mainContentBaoCao .clsContent1").html("");
            if (data.data.length > 0) {
                for (var i = 0; i < data.data.length; i++) {
                    data.data[i]["STT"] = i + 1;
                    htmlTEmp1 += templatehtmltr1(data.data[i]);
                }

                $(".mainContentBaoCao .clsContent1").append(htmlTEmp1);
            }
            $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');

            $('#tongbanghi').text("Có tổng số: " + data.data.length + " bản ghi");

            $(".clsvbd").css('cursor', 'pointer');
        });
        Endloading();
    }
    function ThongKe() {
        $("#tungay").text($("#SearchTuNgay").val());
        $("#denngay").text($("#SearchDenNgay").val());
        var data = $(".zone_search").siSerializeArray();
        loading();
        $.post("/VIPortalAPI/api/BaoCaoTaiChinh/QUERYDATA_NguonVonVay?", data, function (data) {
            console.log(data.Title);
            $(".NDTieude").text(data.Title);
            $(".LoaiBM").text(data.LoaiBM);
            $(".TieuDeTrai1").text(data.TieuDeTrai1);
            $(".TieuDeTrai2").text(data.TieuDeTrai2);
            $(".NguoiKy").text(data.NguoiKy);
            $(".thang").text(data.Thang);
            $(".nam").text(data.Nam);
            var htmlTEmp = '';
            var htmlTEmp1 = '';
            var templatehtmltr = kendo.template($("#javascriptTemplate").html());
            var templatehtmltr1 = kendo.template($("#javascriptTemplate1").html());
            $(".mainContentBaoCao .clsContent").html("");
            if (data.data.length > 0) {
                $(".chekdata").hide();
                for (var i = 0; i < data.data.length; i++) {
                    data.data[i]["STT"] = i + 1;
                    htmlTEmp += templatehtmltr(data.data[i]);
                }

                $(".mainContentBaoCao .clsContent").append(htmlTEmp);
            }else {
                $(".chekdata").show();
            }
            $(".mainContentBaoCao .clsContent1").html("");
            if (data.data.length > 0) {
                $(".chekdata").hide();
                for (var i = 0; i < data.data.length; i++) {
                    data.data[i]["STT"] = i + 1;
                    htmlTEmp1 += templatehtmltr1(data.data[i]);
                }
                $(".mainContentBaoCao .clsContent1").append(htmlTEmp1);
            } else {
                $(".chekdata").show();
            }
            $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');

            $('#tongbanghi').text("Có tổng số: " + data.data.length + " bản ghi");

            $(".clsvbd").css('cursor', 'pointer');
          
        });
        Endloading();
    }

</script>
            

</html>