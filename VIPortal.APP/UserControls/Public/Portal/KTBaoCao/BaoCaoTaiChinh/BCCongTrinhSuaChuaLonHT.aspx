﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BCCongTrinhSuaChuaLonHT.aspx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.BaoCaoTaiChinh.BCCongTrinhSuaChuaLonHT" %>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link type="text/css" href="/Content/dist/css/StyleBaoCao.css" rel="stylesheet" />
    <style type="text/css">
        .zone_search {
            overflow-x: hidden;
        }
    </style>
</head>
<body>
    <div id="BaoCaoSoDangKy" class="smgrid" data-title="Văn bản đến" data-grid="grid_BaoCaoSoDKVanBanDen"
        data-form="/iVBDH/VanBanDen/pFormVanBanDen.aspx" data-formwidth="1200" data-action="/Vbden/api/VanBanDen/VBDGetListJsonGroupSolr" data-view="/Vbden/VanBanDen/pViewVanBanDen.aspx">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />
                <input type="hidden" name="CurentUser" value="<%=CurentUser.Title %>" />
                <div class="title_icon">
                </div>
               <div class="form-group row text-search align-items-center">

                    <label class="col-sm-2 control-label">Tháng:</label>
                    <div class="col-sm-2">
                        <select class="form-control" id="ThangBaoCao" name="ThangBaoCao">
                            <option value="01" <%=DateTime.Now.ToString("MM") == "01" ? "selected" : "" %>>Tháng 1</option>
                            <option value="02" <%=DateTime.Now.ToString("MM") == "02" ? "selected" : "" %>>Tháng 2</option>
                            <option value="03" <%=DateTime.Now.ToString("MM") == "03" ? "selected" : "" %>>Tháng 3</option>
                            <option value="04" <%=DateTime.Now.ToString("MM") == "04" ? "selected" : "" %>>Tháng 4</option>
                            <option value="05" <%=DateTime.Now.ToString("MM") == "05" ? "selected" : "" %>>Tháng 5</option>
                            <option value="06" <%=DateTime.Now.ToString("MM") == "06" ? "selected" : "" %>>Tháng 6</option>
                            <option value="07" <%=DateTime.Now.ToString("MM") == "07" ? "selected" : "" %>>Tháng 7</option>
                            <option value="08" <%=DateTime.Now.ToString("MM") == "08" ? "selected" : "" %>>Tháng 8</option>
                            <option value="09" <%=DateTime.Now.ToString("MM") == "09" ? "selected" : "" %>>Tháng 9</option>
                            <option value="10" <%=DateTime.Now.ToString("MM") == "10" ? "selected" : "" %>>Tháng 10</option>
                            <option value="11" <%=DateTime.Now.ToString("MM") == "11" ? "selected" : "" %>>Tháng 11</option>
                            <option value="12" <%=DateTime.Now.ToString("MM") == "12" ? "selected" : "" %>>Tháng 12</option>
                        </select>
                    </div>

                    <label class="col-sm-3 control-label text-right">Năm:</label>
                    <div class="col-sm-2">
                        <input type="text" name="SearchNam" id="SearchNam" class="form-control " placeholder="Nhập năm" value="<%=DateTime.Now.ToString("yyyy") %>" />
                    </div>

                </div>
                <div class="form-group row text-search align-items-center">

                    <label class="col-sm-2 control-label">Đơn Vị:</label>
                    <div class="col-sm-5">
                        <select class="form-control" id="LoaiDV" name="LoaiDV">
                            <option value="all">Tất cả</option>
                            <option value="074000">Tổng Công ty điện lực TP Hà Nội</option>
                            <option value="070100">Công ty Điện lực Hoàn Kiếm</option>
                            <option value="070200">Công ty Điện lực Hai Bà Trưng</option>
                            <option value="070300">Công ty Điện lực Ba Đình</option>
                            <option value="070400">Công ty Điện lực Đống Đa</option>
                            <option value="070500">Công ty Điện lực Nam Từ Liêm</option>
                            <option value="070600">Công ty Điện lực Thanh Trì</option>
                            <option value="070700">Công ty Điện lực Gia Lâm</option>
                            <option value="070800">Công ty Điện lực Đông Anh</option>
                            <option value="070900">Công ty Điện lực Sóc Sơn</option>
                            <option value="071000">Công ty Điện lực Tây Hồ</option>
                            <option value="071100">Công ty Điện lực Thanh Xuân</option>
                            <option value="071200">Công ty Điện lực Cầu Giấy</option>
                            <option value="071300">Công ty Điện lực Hoàng Mai</option>
                            <option value="071400">Công ty Điện lực Long Biên</option>
                            <option value="071500">Công ty Điện lực Mê Linh</option>
                            <option value="071600">Công ty Điện lực Hà Đông</option>
                            <option value="071700">Công ty Điện lực Sơn Tây</option>
                            <option value="071800">Công ty Điện lực Chương Mỹ</option>
                            <option value="071900">Công ty Điện lực Thạch Thất</option>
                            <option value="072000">Công ty Điện lực Thường Tín</option>
                            <option value="072100">Công ty Điện lực Ba Vì</option>
                            <option value="072200">Công ty Điện lực Đan Phượng</option>
                            <option value="072300">Công ty Điện lực Hoài Đức</option>
                            <option value="072400">Công ty Điện lực Mỹ Đức</option>
                            <option value="072500">Công ty Điện lực Phú Xuyên</option>
                            <option value="072600">Công ty Điện lực Phúc Thọ</option>
                            <option value="072700">Công ty Điện lực Quốc Oai</option>
                            <option value="072800">Công ty Điện lực Thanh Oai</option>
                            <option value="072900">Công ty Điện lực Ứng Hòa</option>
                            <option value="073000">Công ty Điện lực Bắc Từ Liêm</option>
                            <option value="073100">Công ty Công nghệ thông tin Điện lực Hà Nội</option>
                            <option value="073200">Trung tâm Điều độ Hệ thống điện TP Hà Nội</option>
                            <option value="073500">Công ty Thí nghiệm điện Điện lực Hà Nội</option>
                            <option value="073600">Công ty Lưới điện cao thế TP Hà Nội</option>
                            <option value="073700">Công ty Dịch vụ Điện lực Hà Nội</option>
                            <%--<option value="073901">Ban Quản lý dự án Lưới điện Hà Nội - DEP</option>--%>
                            <option value="073902">Ban Quản lý dự án Lưới điện Hà Nội - XDCB</option>
                            <%--<option value="073801">Ban Quản lý dự án phát triển Điện lực Hà Nội - ADB</option>--%>
                            <option value="073802">Ban Quản lý dự án phát triển Điện lực Hà Nội - XDCB</option>
                            <%--<option value="000100">Tập đoàn Điện lực Việt Nam - Kế toán ngành</option>--%>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-primary thongke">Thống kê</button>
                        <button type="button" class="btn btn-primary" id="btnExel">Excel</button>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="mainContentBaoCao">
            <div style="">
                <div style="width: 30%; float: left; text-align:center">
                      <p style="font-size: 12pt; width: 100%"><b class="TieuDeTrai1"></b></p>
                    <p style="font-size: 10pt; width: 100%"><b class="TieuDeTrai2"></b></p>
                </div>
                <div style="width: 55%; float: left; text-align:center">
                    <p style="font-size: 12pt; width: 100%"><b class="Title"></b></p>
                    <p style="font-size: 12pt; width: 100%">Tháng <i class="Thang"></i> - Năm <i class="Nam"></i></p>
                </div>
                <div style="width: 15%; float: left; text-align:center">
                      <p style="font-size: 12pt; width: 100%"><p class="LoaiBM"></p></p>
                     <p  style="font-size: 10pt; width: 100%" class="DVTinh"></p>
                </div>
            </div>
            <div class="smtable_header">
                <table class="smtable">
                    <thead>
                        <tr>
                            <th style="width: 15%"rowspan="2">Tên công trình</th>
                            <th style="width: 13%;" rowspan="2">Mã công trình</th>
                            <th style="width: 13%;" rowspan="2">Dự toán được duyệt</th>                             
                            <th style="width: 13%;" rowspan="2">KH duyệt năm nay</th>                             
                            <th style="width: 13%;" rowspan="2">Dở dang năm trước</th>                             
                            <th style="width: 13%;" rowspan="2">Đã chi năm nay</th>                             
                            <th style="width: 65%;" colspan="5">Lũy kế đã chi</th>                             
                            <th style="width: 15%;" rowspan="2">Giá trị quyết toán được duyệt</th>                             
                        </tr>
                        <tr>                              
                            <th>Vật liệu</th>
                            <th>Nhân công</th>
                            <th>Máy thi công</th>
                            <th>Chi phí khác</th>
                            <th>Cộng</th>
                           
                        </tr>
                      
                    </thead>
                    <tbody class="clsContent">
                       
                    </tbody>
                </table>
            </div>
              <div class="chekdata mt-3" style="text-align: center">
                <p style="font-size: 12pt; width: 100%">Không có bản ghi </p>
            </div>
            <script id="javascriptTemplate" type="text/x-kendo-template">
            <tr class="clsvbd" data-id="">
                 <td style="width: 15%;text-align: left; word-break: break-word;" >#=TenCongTrinh != null ? TenCongTrinh :""#</td>
                 <td style="width: 13%;text-align: center; word-break: break-word;" >#=MaCongTrinh != null ? MaCongTrinh: ""#</td>
                 <td style="width: 13%;text-align: right; word-break: break-word;" >#=DuToanDuocDuyet != null ? formatMoney(DuToanDuocDuyet) : "" #</td>
                 <td style="width: 13%;text-align: right; word-break: break-word;" >#=KHDuyetNamNay != null  ? formatMoney(KHDuyetNamNay) : ""#</td>
                 <td style="width: 13%;text-align: right; word-break: break-word;" >#=DoDangNamTruoc != null ? formatMoney(DoDangNamTruoc) : ""#</td>
                 <td style="width: 13%;text-align: right; word-break: break-word;" >#=DaChiNamNay != null ? formatMoney(DaChiNamNay) : "" #</td>
                 <td style="width: 13%;text-align: right; word-break: break-word;" >#=VatLieuLKDC != null ? formatMoney(VatLieuLKDC) : ""#</td>
                 <td style="width: 13%;text-align: right; word-break: break-word;" >#=NhanCongLKDC != null ? formatMoney(NhanCongLKDC) : "" #</td>
                 <td style="width: 13%;text-align: right; word-break: break-word;" >#=MayThiCongLKDC !=null ? formatMoney(NhanCongLKDC) : ""#</td>
                 <td style="width: 13%;text-align: right; word-break: break-word;" >#=ChiPhiKhacLKDC != null ? formatMoney(ChiPhiKhacLKDC) : "" #</td>
                 <td style="width: 13%;text-align: right; word-break: break-word;" >#=CongLKDC !=null ? formatMoney(CongLKDC) : ""#</td>
                 <td style="width: 15%;text-align: right; word-break: break-word;" >#=GiaTriQuyetToanDuocDuyet != null ? formatMoney(GiaTriQuyetToanDuocDuyet) : ""#</td>
            </tr>
            </script>
           
            <div style="">

               
                <div style="width: 33%; float: right; text-align: center; margin-top: 15px;">
                     <p style="width: 100%">  Hà nội, ngày <i class="ngay"> ... </i> tháng <i class=""> ... </i> năm <i class=""><%=DateTime.Now.ToString("yyyy") %></i></p>
                    <p style="width: 100%">  GIÁM ĐỐC</p>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <p style="font-size: 12pt; width: 100%"><p class="NguoiKy"></p></p>
                    <p style="font-size: 12pt; width: 100%"><p class="">(Ký, họ tên, đóng dấu)</p></p>
                </div>
                <div style="width: 33%; float: right; text-align: center; margin-top: 15px;">
                     <br />
                    <p style="width: 100%">KẾ TOÁN TRƯỞNG  </p>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <p style="font-size: 12pt; width: 100%"><p class="KeToanTruong"></p></p>
                      <p style="font-size: 12pt; width: 100%"><p class="">(Ký, họ tên)</p></p>
                </div>
                 <div style="width: 33%; float: right; text-align: center; margin-top: 15px;">
                      <br />
                    <p style="width: 100%"> NGƯỜI LẬP BIỂU </p>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <p style="font-size: 12pt; width: 100%"><p class="NguoiLapBieu"></p></p>
                       <p style="font-size: 12pt; width: 100%"><p class="">(Ký, họ tên)</p></p>
                </div>
                
            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        ThongKe();
        $(".thongke").click(function () {
            ThongKe();
        });
        $(".input-datetime").daterangepicker({
            singleDatePicker: true,
            locale: { format: 'DD/MM/YYYY' }
        });

        $("#btnExel").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BaoCaoTaiChinh/QUERYDATA_BCCongTrinhSuaChuaLonHT?excel=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");
        });
        $("#btnword").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BaoCaoTaiChinh/QUERYDATA_BCCongTrinhSuaChuaLonHT?word=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");

        });
    });
    function ThongKe() {
        loading();
        $("#tungay").text($("#SearchTuNgay").val());
        $("#denngay").text($("#SearchDenNgay").val());
        var data = $(".zone_search").siSerializeArray();
        loading();
        $.post("/VIPortalAPI/api/BaoCaoTaiChinh/QUERYDATA_BCCongTrinhSuaChuaLonHT?", data, function (data) {

            $(".Title").text(data.Title);
            $(".LoaiBM").text(data.LoaiBM);
            $(".TieuDeTrai1").text(data.TieuDeTrai1);
            $(".TieuDeTrai2").text(data.TieuDeTrai2);
            $(".NguoiKy").text(data.NguoiKy);
            $(".Thang").text(data.Thang);
            $(".Nam").text(data.Nam);
            $(".NguoiLapBieu").text(data.NguoiLapBieu);
            $(".KeToanTruong").text(data.KeToanTruong);
            $(".DVTinh").text(data.DVTinh);



            var htmlTEmp = '';
            var templatehtmltr = kendo.template($("#javascriptTemplate").html());
            $(".mainContentBaoCao .clsContent").html("");
            if (data.data.length > 0) {
                $(".chekdata").hide();
                for (var i = 0; i < data.data.length; i++) {
                    data.data[i]["STT"] = i + 1;
                    htmlTEmp += templatehtmltr(data.data[i]);
                }

                $(".mainContentBaoCao .clsContent").append(htmlTEmp);
            } else {
                $(".chekdata").show();
            }
            $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');

            $('#tongbanghi').text("Có tổng số: " + data.data.length + " bản ghi");

            $(".clsvbd").css('cursor', 'pointer');
            
        });
        Endloading();
    }

</script>
</html>



