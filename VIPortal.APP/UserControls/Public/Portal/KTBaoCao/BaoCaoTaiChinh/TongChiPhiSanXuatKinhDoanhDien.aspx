﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TongChiPhiSanXuatKinhDoanhDien.aspx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.BaoCaoTaiChinh.TongChiPhiSanXuatKinhDoanhDien" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link type="text/css" href="/Content/dist/css/StyleBaoCao.css" rel="stylesheet" />
    <style type="text/css">
        .zone_search {
            overflow-x: hidden;
        }
    </style>
</head>
<body>
    <div id="BaoCaoSoDangKy" class="smgrid" data-title="Văn bản đến" data-grid="grid_BaoCaoSoDKVanBanDen"
        data-form="/iVBDH/VanBanDen/pFormVanBanDen.aspx" data-formwidth="1200" data-action="/Vbden/api/VanBanDen/VBDGetListJsonGroupSolr" data-view="/Vbden/VanBanDen/pViewVanBanDen.aspx">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />
                <input type="hidden" name="CurentUser" value="<%=CurentUser.Title %>" />
                <div class="title_icon">
                </div>
                  <div class="form-group row text-search align-items-center">

                    <label class="col-sm-2 control-label">Báo cáo theo quý:</label>
                    <div class="col-sm-2">
                        <select class="form-control" id="QuyBaoCao" name="QuyBaoCao">
                            <option value="1">Qúy 1</option>
                            <option value="2">Qúy 2</option>
                            <option value="3">Qúy 3</option>
                            <option value="4">Qúy 4</option>
                        </select>
                    </div>

                    <label class="col-sm-3 control-label text-right">Năm:</label>
                    <div class="col-sm-2">
                        <input type="text" name="SearchNam" id="SearchNam" class="form-control " placeholder="Nhập năm" value="<%=DateTime.Now.ToString("yyyy") %>" />
                    </div>

                </div>
                <div class="form-group row text-search align-items-center">

                    <label class="col-sm-2 control-label">Đơn Vị:</label>
                    <div class="col-sm-5">
                        <select class="form-control" id="LoaiDV" name="LoaiDV">
                            <option value="all">Tất cả</option>
                            <option value="074000">Tổng Công ty điện lực TP Hà Nội</option>
                            <option value="070100">Công ty Điện lực Hoàn Kiếm</option>
                            <option value="070200">Công ty Điện lực Hai Bà Trưng</option>
                            <option value="070300">Công ty Điện lực Ba Đình</option>
                            <option value="070400">Công ty Điện lực Đống Đa</option>
                            <option value="070500">Công ty Điện lực Nam Từ Liêm</option>
                            <option value="070600">Công ty Điện lực Thanh Trì</option>
                            <option value="070700">Công ty Điện lực Gia Lâm</option>
                            <option value="070800">Công ty Điện lực Đông Anh</option>
                            <option value="070900">Công ty Điện lực Sóc Sơn</option>
                            <option value="071000">Công ty Điện lực Tây Hồ</option>
                            <option value="071100">Công ty Điện lực Thanh Xuân</option>
                            <option value="071200">Công ty Điện lực Cầu Giấy</option>
                            <option value="071300">Công ty Điện lực Hoàng Mai</option>
                            <option value="071400">Công ty Điện lực Long Biên</option>
                            <option value="071500">Công ty Điện lực Mê Linh</option>
                            <option value="071600">Công ty Điện lực Hà Đông</option>
                            <option value="071700">Công ty Điện lực Sơn Tây</option>
                            <option value="071800">Công ty Điện lực Chương Mỹ</option>
                            <option value="071900">Công ty Điện lực Thạch Thất</option>
                            <option value="072000">Công ty Điện lực Thường Tín</option>
                            <option value="072100">Công ty Điện lực Ba Vì</option>
                            <option value="072200">Công ty Điện lực Đan Phượng</option>
                            <option value="072300">Công ty Điện lực Hoài Đức</option>
                            <option value="072400">Công ty Điện lực Mỹ Đức</option>
                            <option value="072500">Công ty Điện lực Phú Xuyên</option>
                            <option value="072600">Công ty Điện lực Phúc Thọ</option>
                            <option value="072700">Công ty Điện lực Quốc Oai</option>
                            <option value="072800">Công ty Điện lực Thanh Oai</option>
                            <option value="072900">Công ty Điện lực Ứng Hòa</option>
                            <option value="073000">Công ty Điện lực Bắc Từ Liêm</option>
                            <option value="073100">Công ty Công nghệ thông tin Điện lực Hà Nội</option>
                            <option value="073200">Trung tâm Điều độ Hệ thống điện TP Hà Nội</option>
                            <option value="073500">Công ty Thí nghiệm điện Điện lực Hà Nội</option>
                            <option value="073600">Công ty Lưới điện cao thế TP Hà Nội</option>
                            <option value="073700">Công ty Dịch vụ Điện lực Hà Nội</option>
                            <%--<option value="073901">Ban Quản lý dự án Lưới điện Hà Nội - DEP</option>--%>
                            <option value="073902">Ban Quản lý dự án Lưới điện Hà Nội - XDCB</option>
                            <%--<option value="073801">Ban Quản lý dự án phát triển Điện lực Hà Nội - ADB</option>--%>
                            <option value="073802">Ban Quản lý dự án phát triển Điện lực Hà Nội - XDCB</option>
                            <%--<option value="000100">Tập đoàn Điện lực Việt Nam - Kế toán ngành</option>--%>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-primary thongke">Thống kê</button>
                        <button type="button" class="btn btn-primary" id="btnExel">Excel</button>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="mainContentBaoCao">
            <div style="">
                <div style="width: 30%; float: left; text-align:center">
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeTrai1"></b></p>
                    <p style="font-size: 10pt; width: 100%"><b class="TieuDeTrai2"></b></p>
                </div>
                <div style="width: 55%; float: left; text-align:center">
                    <p style="font-size: 12pt; width: 100%" id="TitleBC"><b class="NDTieude"></b></p>
                    <p style="font-size: 12pt; width: 100%"><i style="color: #212529;">Qúy <i class="Thang"></i> Năm  <i class="Nam"></i></i></p>
                </div>
                <div style="width: 15%; float: left; text-align:center">
                      <p style="float: right ; text-align:center" class="LoaiBM"></p>
                </div>
            </div>

            <div style="text-align: right; width: 100%; clear: both;"><span class="" style="margin-right: 1.25%;" id="tongbanghi"></span></div>    
            <div class="smtable_header">
                <table class="smtable">
                    <thead>
                        <tr>
                            <th style="width: 20%" rowspan="3">DIỄN GIẢI </th>
                            <th style="width: 8%" rowspan="3" >Mã số</th>
                            <th style="width: 36%"colspan="3">Quý báo cáo</th>                             
                            <th style="width: 36%"colspan="3">Lũy kế từ đầu năm</th>
                            
                        </tr>
                        <tr>                              
                            <th rowspan="2">Tổng số</th>                            
                            <th rowspan="2">Giá thành đơn vị (đ/kwh) </th>                             
                            <th rowspan="2">Tỷ trọng (%)</th>                             
                            <th rowspan="2">Tổng số</th>                            
                            <th rowspan="2">Giá thành đơn vị (đ/kwh) </th>                             
                            <th rowspan="2">Tỷ trọng (%)</th>
                        </tr>
                        <tr></tr>
                        <tr>                              
                            <th >1</th>  
                            <th >2</th>  
                            <th >3</th>  
                            <th >4</th>
                            <th >5</th>  
                            <th >6</th>  
                            <th >7</th>
                            <th >9</th>  
                              
                        </tr>
                    </thead>
                    <tbody class="clsContent">
                       
                    </tbody>
                </table>
            </div>
               <div class="chekdata mt-3" style="text-align: center">
                <p style="font-size: 12pt; width: 100%">Không có bản ghi </p>
            </div>
            <script id="javascriptTemplate" type="text/x-kendo-template">
            <tr class="clsvbd" data-id="">
                 <td style="width: 20%;text-align: left; word-break: break-word;">#=DienGiai !=null ? DienGiai:"" #</td>
                 <td style="width: 8%;text-align: center; word-break: break-word;">#=MaSo !=null ?MaSo : ""#</td>
                 <td style="width: 12%;text-align: right; word-break: break-word;">#=TongQuy !=null ? formatMoney(TongQuy) : "" #</td>
                 <td style="width: 12%;text-align: right; word-break: break-word;">#=GiaThanhDonViQuy !=null ? formatMoney(GiaThanhDonViQuy) : ""#</td>
                 <td style="width: 12%;text-align: right; word-break: break-word;">#=TyTrongQuy !=null ? formatMoney(TyTrongQuy) : ""#</td>
                 <td style="width: 12%;text-align: right; word-break: break-word;">#=TongLuyKe !=null ? formatMoney(TongLuyKe) : ""#</td>
                 <td style="width: 12%;text-align: right; word-break: break-word;">#=GiaThanhDonViLuyKe !=null ? formatMoney(GiaThanhDonViLuyKe) : ""#</td>
                 <td style="width: 12%;text-align: right; word-break: break-word;">#=TyTrongLuyKe !=null ? formatMoney(TyTrongLuyKe) :""#</td>               
            </tr>
            </script>
            <div style="display:flex;">
            <div style="text-align:center;margin:40px 15px 15px;width:33%">
                <p><b>NGƯỜI LẬP BIỂU</b></p>
                <p><b>(Ký, họ tên)</b></p>
            </div>
            <div style="text-align:center; margin:40px 15px 15px;width:33%">
                <p><b>KẾ TOÁN TRƯỞNG</b></p>
                <p><b>(Ký, họ tên)</b></p>
            </div>
            <div style="text-align:center;margin:40px 15px 15px;width:33%">
                <p><b>GIÁM ĐỐC</b></p>
                <p><b>(Ký, họ tên,đóng dấu)</b></p>
            </div>
            </div>
    </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        ThongKe();
    });
    $(document).ready(function () {
        $(".thongke").click(function () {
            ThongKe();
        });
        $(".input-datetime").daterangepicker({
            singleDatePicker: true,
            locale: { format: 'DD/MM/YYYY' }
        });

        $("#btnExel").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BaoCaoTaiChinh/QUERYDATA_TongHopChiPhiSanXuatKinhDoanhDien?excel=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");
        });
        $("#btnword").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BaoCaoTaiChinh/QUERYDATA_TongHopChiPhiSanXuatKinhDoanhDien?word=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");

        });
    });
    function loadGiaoDien() {
        $("#tungay").text($("#SearchTuNgay").val());
        $("#denngay").text($("#SearchDenNgay").val());
        var data = $(".zone_search").siSerializeArray();
        $.post("/VIPortalAPI/api/BaoCaoTaiChinh/QUERYDATA_TongHopChiPhiSanXuatKinhDoanhDien?", data, function (data) {
            console.log(data.Title);
            
         

            var htmlTEmp = '';
            var templatehtmltr = kendo.template($("#javascriptTemplate").html());
            $(".mainContentBaoCao .clsContent").html("");
            if (data.data.length > 0) {
                for (var i = 0; i < data.data.length; i++) {
                    data.data[i]["STT"] = i + 1;
                    htmlTEmp += templatehtmltr(data.data[i]);
                }

                $(".mainContentBaoCao .clsContent").append(htmlTEmp);
            }
            $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');

            $('#tongbanghi').text("Có tổng số: " + data.data.length + " bản ghi");

            $(".clsvbd").css('cursor', 'pointer');
        });
    }
    function ThongKe() {
        $("#tungay").text($("#SearchTuNgay").val());
        $("#denngay").text($("#SearchDenNgay").val());
        var data = $(".zone_search").siSerializeArray();
        loading();
        $.post("/VIPortalAPI/api/BaoCaoTaiChinh/QUERYDATA_TongHopChiPhiSanXuatKinhDoanhDien?", data, function (data) {
            console.log(data.Title);
            $(".NDTieude").text(data.Title);
            $(".LoaiBM").text(data.LoaiBM);
            $(".TieuDeTrai1").text(data.TieuDeTrai1);
            $(".TieuDeTrai2").text(data.TieuDeTrai2);
            $(".NguoiKy").text(data.NguoiKy);
            $(".Thang").text(data.Thang);
            $(".Nam").text(data.Nam);
            var htmlTEmp = '';
            var templatehtmltr = kendo.template($("#javascriptTemplate").html());
            $(".mainContentBaoCao .clsContent").html("");
            if (data.data.length > 0) {
                $(".chekdata").hide();
                for (var i = 0; i < data.data.length; i++) {
                    data.data[i]["STT"] = i + 1;
                    htmlTEmp += templatehtmltr(data.data[i]);
                }

                $(".mainContentBaoCao .clsContent").append(htmlTEmp);
                $('#tongbanghi').text("Có tổng số: " + data.data.length + " bản ghi");

            } else {
                $(".chekdata").show();
            }
            $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');


            $(".clsvbd").css('cursor', 'pointer');
            Endloading();
        });
        
    }

</script>
            

</html>
