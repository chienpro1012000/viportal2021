﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NhatKyMatDien.aspx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.BaoCaoKyThuat.NhatKyMatDien" %>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link type="text/css" href="/Content/dist/css/StyleBaoCao.css" rel="stylesheet" />
    <style type="text/css">
        .zone_search {
            overflow-x: hidden;
        }
        
    </style>
</head>
<body>
    <div id="BaoCaoSoDangKy" class="smgrid" data-title="Văn bản đến" data-grid="grid_BaoCaoSoDKVanBanDen"
        data-form="/iVBDH/VanBanDen/pFormVanBanDen.aspx" data-formwidth="1200" data-action="/Vbden/api/VanBanDen/VBDGetListJsonGroupSolr" data-view="/Vbden/VanBanDen/pViewVanBanDen.aspx">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />
                <input type="hidden" name="CurentUser" value="<%=CurentUser.Title %>" />
                <div class="title_icon">
                </div>
                <div class="form-group row text-search align-items-center">
                    <label class="col-sm-2 control-label">Từ ngày:</label>

                    <div class="col-sm-2">
                        <input type="text" name="SearchTuNgay" id="SearchTuNgay" class="form-control input-datetime" placeholder="Từ ngày" value="<%=DateTime.Now.AddMonths(-1).ToString("dd/MM/yyyy") %>" />
                    </div>
                    <label class="col-sm-2 control-label">Đến ngày:</label>
                    <div class="col-sm-2">
                        <input type="text" name="SearchDenNgay" id="SearchDenNgay" class="form-control input-datetime" placeholder="Đến ngày" value="<%=DateTime.Now.ToString("dd/MM/yyyy") %>" />
                    </div>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-primary thongke">Thống kê</button>
                        <button type="button" class="btn btn-primary" id="btnExel">Excel</button>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="mainContentBaoCao">
             <div style="">
                <div style="width: 30%; float: left; text-align:center">
                      <p style="font-size: 10pt; width: 100%"><b class="TieuDeTrai1"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeTrai2"></b></p>
                </div>
                <div style="width: 55%; float: left; text-align:center">
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeGiua1"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeGiua2"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="Title"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b> <i class="Nam"></i></b></p>
                </div>
                <div style="width: 15%; float: left; text-align:center">
                      <p style="font-size: 12pt; width: 100%"><p class="LoaiBM"></p></p>
                </div>
            </div>
            <div class="smtable_header"style="float:left;overflow-x:scroll">
                <table class="smtable"style="width:150%">
                    <thead>
                        <tr>
                            <th style="width:5%">STT</th>
                            <th style="width:10%">Mã LMĐ</th>
                            <th style="width:10%">Mã LĐĐ</th> 
                            <th style="width:10%">Đơn Vị</th>
                            <th style="width:10%">Đơn vị tính độ tin cậy</th> 
                            <th style="width:10%">Định Danh</th>
                            <th style="width:10%">Tên phần tử cắt</th>
                            <th style="width:10%">Tên phần tử đóng</th>
                            <th style="width:10%">Ngày mất điện</th> 
                            <th style="width:10%">Ngày có điện</th>
                            <th style="width:10%">ID Lịch</th> 
                            <th style="width:10%">Số phút mất điện</th>
                            <th style="width:10%">Số khách hàng</th>
                            <th style="width:10%">Mã nguyên nhân</th>
                            <th style="width:20%">Nguyên nhân</th> 
                            <th style="width:20%">Khu vực</th>
                            <th style="width:20%">Lý do</th> 
                            <th style="width:10%">Ghi chú</th>
                            <th style="width:10%">Người cắt</th> 
                            <th style="width:10%">Người đóng</th>
                            <th style="width:10%">Ngày thao tác cắt</th>
                            <th style="width:10%">Ngày thao tác đóng</th>
                           <%-- <th style="width:10%">MAIFI</th> 
                            <th style="width:10%">SAIFI</th>
                            <th style="width:10%">SAIDI</th>--%> 
                        </tr>
                    </thead>
                    <tbody class="clsContent">
                    </tbody>
                </table>
            </div>
            <script id="javascriptTemplate" type="text/x-kendo-template">
            <tr class="clsvbd" data-id="">
               <td style="width:5%">#=STT#</td>
               <td style="width:10%">#=MaLDM!=null?MaLDM:""#</td>
               <td style="width:10%">#=MaLDD!=null?MaLDD:""#</td> 
               <td style="width:10%">#=DonVi!=null?DonVi:""#</td>
               <td style="width:10%">#=DonViTinhDoTinCay!=null?DonViTinhDoTinCay:""#</td> 
               <td style="width:10%">#=DinhDanh!=null?DinhDanh:""#</td>
               <td style="width:10%">#=TenPhanTuCat!=null?TenPhanTuCat:""#</td>
               <td style="width:10%">#=TenPhanTuDong!=null?TenPhanTuDong:""#</td>
               <td style="width:10%">#=NgayMatDien!=null?fomatdate(NgayMatDien):""#</td> 
               <td style="width:10%">#=NgayCoDien!=null?fomatdate(NgayCoDien):""#</td>
               <td style="width:10%">#=IDLich!=null?IDLich:""#</td> 
               <td style="width:10%">#=SoPhutMatDien!=null?SoPhutMatDien:""#</td>
               <td style="width:10%">#=SoKH!=null?SoKH:""#</td>
               <td style="width:10%">#=MaNguyenNhan!=null?MaNguyenNhan:""#</td>
               <td style="width:20%">#=NguyenNhan!=null?NguyenNhan:""#<td> 
               <td style="width:20%">#=KhuVuc!=null?KhuVuc:""#</td>
               <td style="width:20%">#=LyDo!=null?LyDo:""#</td> 
               <td style="width:10%">#=GhiChu!=null?GhiChu:""#</td>
               <td style="width:10%">#=NguoiCat!=null?NguoiCat:""#</td> 
               <td style="width:10%">#=NguoiDong!=null?NguoiDong:""#</td>
               <td style="width:10%">#=NgayThaoTacCat!=null?fomatdate(NgayThaoTacCat):""#</td>
               <td style="width:10%">#=NgayThaoTacDong!=null?fomatdate(NgayThaoTacDong):""#</td>
               <%--<td style="width:10%">#=MAIFI#</td> 
               <td style="width:10%">#=SAIFI#</td>
               <td style="width:10%">#=SAIDI#</td> --%>                                    
            </tr>
            </script>
        </div>
    </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        ThongKe();
        $(".thongke").click(function () {
            ThongKe();
        });
        $(".input-datetime").daterangepicker({
            singleDatePicker: true,
            locale: { format: 'DD/MM/YYYY' }
        });

        $("#btnExel").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BaoCaoKyThuat/QUERYDATA_NhatKyMatDien?excel=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");
        });
        $("#btnword").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BaoCaoKyThuat/QUERYDATA_NhatKyMatDien?word=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");

        });
    });
    function ThongKe() {
        $("#tungay").text($("#SearchTuNgay").val());
        $("#denngay").text($("#SearchDenNgay").val());
        var data = $(".zone_search").siSerializeArray();
        loading();
        $.post("/VIPortalAPI/api/BaoCaoKyThuat/QUERYDATA_NhatKyMatDien?", data, function (data) {

            $(".Title").text(data.Title);
            $(".TieuDeTrai1").text(data.TieuDeTrai1);
            $(".TieuDeTrai2").text(data.TieuDeTrai2);
            $(".Nam").text(data.Nam);


            var htmlTEmp = '';
            var templatehtmltr = kendo.template($("#javascriptTemplate").html());
            $(".mainContentBaoCao .clsContent").html("");
            if (data.data.length > 0) {
                for (var i = 0; i < data.data.length; i++) {
                    data.data[i]["STT"] = i + 1;
                    htmlTEmp += templatehtmltr(data.data[i]);
                }

                $(".mainContentBaoCao .clsContent").append(htmlTEmp);                
            }
            $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');

            $('#tongbanghi').text("Có tổng số: " + data.data.length + " bản ghi");

            $(".clsvbd").css('cursor', 'pointer');
           

        });
        Endloading();
    }

</script>
    <script>
        function fomatdate(dateStr) {
            var pattern = /(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/;

            var formatDateStr = dateStr.replace(pattern, '$1/$2/$3 $4:$5:$6');
            var formatDate = new Date(formatDateStr);
            return formatDate.format("dd/MM/yyyy");
        }
    </script>
</html>
