﻿<%@ page language="C#" autoeventwireup="true" codebehind="TongHopDoTinCayLuoiDien110_550KV.aspx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.BaoCaoKyThuat.TongHopDoTinCayLuoiDien110_550KV" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link type="text/css" href="/Content/dist/css/StyleBaoCao.css" rel="stylesheet" />
    <style type="text/css">
        .zone_search {
            overflow-x: hidden;
        }







        table {
            width: 100%;
            border-collapse: collapse;
            border: 1px solid black;
        }

      /*  th {
            height: 50px;
            border: 1px solid black;
        }

        td {
            border: 1px solid black;
        }*/
         th, td {
    border: 1px solid #96d1f3 !important;
    vertical-align: middle;
    padding: 5px 3px !important;
    font-size: 12px;
}
      


        .flex-one {
            flex: 1;
        }

        .no-wrap {
            white-space: nowrap;
        }
    </style>
</head>
<body>
    <div id="BaoCaoSoDangKy" class="smgrid" data-title="Văn bản đến" data-grid="grid_BaoCaoSoDKVanBanDen"
        data-form="/iVBDH/VanBanDen/pFormVanBanDen.aspx" data-formwidth="1200" data-action="/Vbden/api/VanBanDen/VBDGetListJsonGroupSolr" data-view="/Vbden/VanBanDen/pViewVanBanDen.aspx">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />
                <input type="hidden" name="CurentUser" value="<%=CurentUser.Title %>" />
                <div class="title_icon">
                </div>
                <div class="form-group row text-search align-items-center">
                    <label class="col-sm-2 control-label">Nhập Tháng:</label>

                    <div class="col-sm-2">
                        <select class="form-control" id="Thang" name="Thang">
                            <option value="01" <%=DateTime.Now.ToString("MM") == "01" ? "selected" : "" %>>Tháng 1</option>
                            <option value="02 " <%=DateTime.Now.ToString("MM") == "02" ? "selected" : "" %>>Tháng 2</option>
                            <option value="03 " <%=DateTime.Now.ToString("MM") == "03" ? "selected" : "" %>>Tháng 3</option>
                            <option value="04 " <%=DateTime.Now.ToString("MM") == "04" ? "selected" : "" %>>Tháng 4</option>
                            <option value="05 " <%=DateTime.Now.ToString("MM") == "05" ? "selected" : "" %>>Tháng 5</option>
                            <option value="06 " <%=DateTime.Now.ToString("MM") == "06" ? "selected" : "" %>>Tháng 6</option>
                            <option value="07 " <%=DateTime.Now.ToString("MM") == "07" ? "selected" : "" %>>Tháng 7</option>
                            <option value="08 " <%=DateTime.Now.ToString("MM") == "08" ? "selected" : "" %>>Tháng 8</option>
                            <option value="09 " <%=DateTime.Now.ToString("MM") == "09" ? "selected" : "" %>>Tháng 9</option>
                            <option value="10 " <%=DateTime.Now.ToString("MM") == "10" ? "selected" : "" %>>Tháng 10</option>
                            <option value="11" <%=DateTime.Now.ToString("MM") == "11" ? "selected" : "" %>>Tháng 11</option>
                            <option value="12 " <%=DateTime.Now.ToString("MM") == "12" ? "selected" : "" %>>Tháng 12</option>
                        </select>
                    </div>
                    <label class="col-sm-2 control-label">Nhập Năm:</label>
                    <div class="col-sm-2">
                        <input type="text" name="Nam" id="Nam" class="form-control " placeholder="Nhập năm" value="<%=DateTime.Now.ToString("yyyy") %>" />
                    </div>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-primary thongke">Thống kê</button>
                        <button type="button" class="btn btn-primary" id="btnExel">Excel</button>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="mainContentBaoCao">
            <div style="display: flex">
                <div style="width: 30%; float: left; text-align: center">
                    <p style="font-size: 10pt; width: 100%"><b class="TieuDeTrai1"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeTrai2"></b></p>
                </div>
                <div style="width: 55%; float: left; text-align: center">
                    <p style="font-size: 12pt; width: 100%"><b class="Title"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b><i class="Nam"></i></b></p>
                </div>
                <div style="width: 15%; float: left; text-align: center">
                    <p style="font-size: 12pt; width: 100%">
                        <p class="LoaiBM"></p>
                    </p>
                </div>


            </div>
        <%--    /class="smtable_header" id="Bang-append"
            //smtable--%>
            <div >
                <table class="myTable ">
                    <thead>
                        <tr>
                            <th style="width: 60%" colspan="3">Tên đơn vị</th>
                            <th style="width: 10%">110kV</th>
                            <th style="width: 10%">220kV</th>
                            <th style="width: 10%">500kV</th>
                            <th style="width: 10%">Tổng</th>
                        </tr>
                    </thead>
                    <tbody class="clsContent">
                        <tr>
                            <td rowspan="6">Độ sẵn sàng (SA)</td>
                            <td rowspan="3">Đường dây</td>
                            <td>Tổng thời gian không vận hành ĐZ</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>

                        </tr>
                        <tr>
                            <td>Tổng số mạch đường dây</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Độ sẵn sàng đường dây</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>


                        <tr>
                            <td rowspan="3">MBA</td>
                            <td>Tổng số MBA</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>

                        </tr>
                        <tr>
                            <td>Tổng thời gian không vận hành MBA</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>

                        </tr>
                        <tr>
                            <td>Độ sẵn sàng MBA</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td rowspan="3">Độ lệch điện áp (VDI)</td>
                            <td rowspan="3"></td>
                            <td>Tổng số nút</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>

                        </tr> 
                        <tr>
                            <td>Tổng thời gian điện áp nút nằm ngoài dải cho phép</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>

                        </tr>
                        <tr>
                            <td>Độ lệch điện áp</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>

                        </tr> 
                        <tr>
                            <td rowspan="10">Mức độ mang tải</td>
                            <td rowspan="5">Đường dây</td>
                            <td>Mang tải <40%</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>

                        </tr>
                        <tr>
                            <td>Mang tải 80%-90%</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>

                        </tr>
                        <tr>
                            <td>Mang tải 90%-100%</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>

                        </tr>
                        <tr>
                            <td>Mang tải 100%-110%</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>

                        </tr>
                        <tr>
                            <td>Mang tải >110%</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>

                        </tr>
                        <tr>
                            <td rowspan="5">MBA</td>
                            <td>Mang tải <40%</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>

                        </tr>
                          <tr>
                            <td>Mang tải 80%-90%</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>

                        </tr>
                        <tr>
                            <td>Mang tải 90%-100%</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>

                        </tr>
                        <tr>
                            <td>Mang tải 100%-110%</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>

                        </tr>
                        <tr>
                            <td>Mang tải >110%</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>

                        </tr>
                    </tbody>
                </table>
            </div>
      
            <div style="">


                <div style="width: 33%; float: right; text-align: center; margin-top: 15px;">
                    <p style="width: 100%"><b>THỦ TRƯỞNG ĐƠN VỊ</b></p>
                    <p style="font-size: 12pt; width: 100%" class="chucvu"><b></b></p>
                    <br />
                    <br />
                    <br />
                    <br />
                    <p style="font-size: 12pt; width: 100%">
                        <p class="NguoiKy"></p>
                    </p>
                </div>
                <div style="width: 33%; float: right; text-align: center; margin-top: 15px;">
                </div>
                <div style="width: 33%; float: right; text-align: center; margin-top: 15px;">
                    <p style="width: 100%"><b>NGƯỜI LẬP BÁO CÁO</b>  </p>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <p style="font-size: 12pt; width: 100%">
                        <p class="ToTongHop"></p>
                    </p>
                </div>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        ThongKe();
        $(".thongke").click(function () {
            ThongKe();
        });
        $(".input-datetime").daterangepicker({
            singleDatePicker: true,
            locale: { format: 'DD/MM/YYYY' }
        });

        $("#btnExel").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BaoCaoKyThuat/QUERYDATA_TongHopDoTinCayLuoiDien110_550kV?excel=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");
        });
        $("#btnword").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BaoCaoKyThuat/QUERYDATA_TongHopDoTinCayLuoiDien110_550kV?word=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");

        });
    });
    function ThongKe() {
        $(".removetable").remove();
        $("#tungay").text($("#SearchTuNgay").val());
        $("#denngay").text($("#SearchDenNgay").val());
        var data = $(".zone_search").siSerializeArray();
        loading();
        $.post("/VIPortalAPI/api/BaoCaoKyThuat/QUERYDATA_TongHopDoTinCayLuoiDien110_550kV?", data, function (data) {
            $(".Title").text(data.Title);
            $(".TieuDeTrai1").text(data.TieuDeTrai1);
            $(".TieuDeTrai2").text(data.TieuDeTrai2);
            $(".Nam").text(data.Nam);
            var htmlTEmp = '';
           
            if (data.data.length > 0) {
                let element = []
                $.each(data.data, function (ind, value) {
                    let keys = Object.keys(value);
                    element = []
                    element = $(".myTable").find("td.data" + (ind + 1))
                    let lengthData = keys.length > element.length ? element.length : keys.length
                  
                    console.log(lengthData)
                    console.log(keys)
                    console.log(element.length)
                  
                    for (var i = 0; i < lengthData; i++) {
                        $(element[i]).html(value[keys[i]])
                    }
                  
                
                    
                });

                

            }
           


            $(".clsvbd").css('cursor', 'pointer');
            Endloading();
        });

    }

</script>
</html>
