﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BaoCaoTinhHinhSuCoDien.aspx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.BaoCaoKyThuat.WebForm1" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link type="text/css" href="/Content/dist/css/StyleBaoCao.css" rel="stylesheet" />
    <style type="text/css">
        .zone_search {
            overflow-x: hidden;
        }
        
    </style>
</head>
<body>
    <div id="BaoCaoSoDangKy" class="smgrid" data-title="Văn bản đến" data-grid="grid_BaoCaoSoDKVanBanDen"
        data-form="/iVBDH/VanBanDen/pFormVanBanDen.aspx" data-formwidth="1200" data-action="/Vbden/api/VanBanDen/VBDGetListJsonGroupSolr" data-view="/Vbden/VanBanDen/pViewVanBanDen.aspx">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />
                <input type="hidden" name="CurentUser" value="<%=CurentUser.Title %>" />
                <div class="title_icon">
                </div>
                <div class="form-group row text-search align-items-center">
                    <label class="col-sm-2 control-label">Nhập Tháng:</label>

                    <div class="col-sm-2">
                            <select class="form-control" id="SearchThang" name="SearchThang">
                           <option value= "1" <%=DateTime.Now.ToString("MM") ==  "1" ? "selected" : "" %>>Tháng 1</option>
                            <option value="2 " <%=DateTime.Now.ToString("MM") == "2" ? "selected" : "" %>>Tháng 2</option>
                            <option value="3 " <%=DateTime.Now.ToString("MM") == "3" ? "selected" : "" %>>Tháng 3</option>
                            <option value="4 " <%=DateTime.Now.ToString("MM") == "4" ? "selected" : "" %>>Tháng 4</option>
                            <option value="5 " <%=DateTime.Now.ToString("MM") == "5" ? "selected" : "" %>>Tháng 5</option>
                            <option value="6 " <%=DateTime.Now.ToString("MM") == "6" ? "selected" : "" %>>Tháng 6</option>
                            <option value="7 " <%=DateTime.Now.ToString("MM") == "7" ? "selected" : "" %>>Tháng 7</option>
                            <option value="8 " <%=DateTime.Now.ToString("MM") == "8" ? "selected" : "" %>>Tháng 8</option>
                            <option value="9 " <%=DateTime.Now.ToString("MM") == "9" ? "selected" : "" %>>Tháng 9</option>
                            <option value="10 " <%=DateTime.Now.ToString("MM") == "10" ? "selected" : "" %>>Tháng 10</option>
                            <option value="11" <%=DateTime.Now.ToString("MM") == "11" ? "selected" : "" %>>Tháng 11</option>
                            <option value="12 " <%=DateTime.Now.ToString("MM") == "12" ? "selected" : "" %>>Tháng 12</option>
                        </select>
                    </div>
                    <label class="col-sm-2 control-label">Nhập Năm:</label>
                    <div class="col-sm-2">
                        <input type="text" name="SearchNam" id="SearchNam" class="form-control " placeholder="Nhập năm" value="<%=DateTime.Now.ToString("yyyy") %>" />
                    </div>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-primary thongke">Thống kê</button>
                        <button type="button" class="btn btn-primary" id="btnExel">Excel</button>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="mainContentBaoCao">
            <div style="">
                <div style="width: 30%; float: left; text-align:center">
                      <p style="font-size: 10pt; width: 100%"><b class="TieuDeTrai1"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeTrai2"></b></p>
                </div>
                <div style="width: 55%; float: left; text-align:center">
                    <p style="font-size: 12pt; width: 100%"><b class="Title"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b> Tháng <i class="Nam"></i></b></p>
                </div>
                <div style="width: 15%; float: left; text-align:center">
                      <p style="font-size: 12pt; width: 100%"><p class="LoaiBM"></p></p>
                </div>
            </div>
       
            <div class="smtable_header" style="float:left;overflow-x:scroll">
                <table class="smtable" style="width:150%">
                    <thead>
                        <tr>
                            <th style="width:2%;" rowspan="3">STT</th>
                            <th style="width:5%;"rowspan="3">Tên đơn vị</th>
                            <th style="width:5%;" rowspan="3">Tên thiết bị(ĐZ,MBA)</th>
                            <th style="width:3%;" rowspan="3">Cấp điện áp</th>                             
                            <th style="width:14%;" colspan="4">Thời điểm sự cố</th>
                            <th style="width:32%;" colspan="12"> Dạng sự cố</th>
                            <th style="width:8%; "rowspan="3">Diễn biến sự cố</th>
                            <th style="width:8%;" rowspan="3">Nguyên nhân</th>
                            <th style="width:4%;" rowspan="3">Thời gian gián đoạn cung cấp điện - phút</th>
                            <th style="width:4%;"rowspan="3"> Thời gian sự cố (phút) </th>
                            <th style="width:4%;"rowspan="3"> Công suất tải trước sự cố(MW)</th>
                            <th style="width:4%;"rowspan="3"> Điện năng không cung cấp được(kWh)</th>
                        </tr>
                        <tr>                              
                            <th colspan="2">Xuất Hiện</th>  
                            <th colspan="2">Khôi phục</th>  
                            <th colspan="2">ĐZ</th>  
                            <th colspan="4">Cáp</th>  
                            <th colspan="6">Trạm</th>  
                        </tr>
                        <tr>                              
                            <th >Ngày</th>  
                            <th >Giờ</th>  
                            <th >Ngày</th>  
                            <th >Giờ</th>  
                            <th >KD</th>
                            <th >TQ</th>  
                            <th >Cách điện</th>  
                            <th >Mối nối</th>  
                            <th >TB phụ</th>  
                            <th >Khác</th>  
                            <th >MBA</th>  
                            <th >MC</th>  
                            <th >BV</th>  
                            <th >CS</th>  
                            <th >TU/TI</th>  
                            <th >Khác</th>  
                        </tr>
                    </thead>
                    <tbody class="clsContent">
                    </tbody>
                </table>
            </div>
                 <div class="text-center mt-4 checkdatanull" style="width:100% ; float: left ">
                <p style="font-size: 12pt; width: 100%">Chưa có dữ liệu </p>
            </div>
            <script id="javascriptTemplate" type="text/x-kendo-template">
            <tr class="clsvbd" data-id="">
                <td style="width: 2%" >#=STT !=null ?STT : ""#</td>
                <td style="width:5%">#=TenDonVi !=null ?TenDonVi : ""#</td>
                <td style="width: 5%">#=TenThietBi !=null ?TenThietBi : ""#</td>
                <td style="width: 3%">#=CapDienAp !=null ?CapDienAp : ""#</td>                          
                <td style="width :3.5%">#=NgayXHSC !=null ?fomatdate(NgayXHSC) : ""#</td> 
                <td style="width :3.5%">#=GioXHSC !=null ?fomattime(GioXHSC) : ""#</td> 
                <td style="width :3.5%">#=NgayKPSC !=null ?fomatdate(NgayKPSC) : ""#</td> 
                <td style="width :3.5%">#=GioKPSC !=null ?fomattime(GioKPSC) : ""#</td> 
                <td class="text-left" style="width: 2.66667%" >#=KD !=null ?KD : ""#</td>                          
                <td class="text-left" style="width:2.66667%">#=TQ !=null ?TQ : ""#</td>                          
                <td class="text-left" style="width: 2.66667%">#=CachDien !=null ?CachDien : ""#</td>                          
                <td class="text-left" style="width: 2.66667%">#=MoiNoi !=null ?MoiNoi : ""#</td>                          
                <td class="text-left" style="width: 2.66667%">#=TBPhu !=null ?TBPhu : ""#</td>                          
                <td class="text-left" style="width: 2.66667%">#=KhacCap !=null ?KhacCap : ""#</td>                          
                <td class="text-left" style="width: 2.66667%">#=MayBienAp !=null ?MayBienAp : ""#</td>                          
                <td class="text-left" style="width: 2.66667%">#=MayCat !=null ?MayCat : ""#</td>                          
                <td class="text-left" style="width: 2.66667%">#=BVTram !=null ?BVTram : ""#</td>                          
                <td class="text-left" style="width: 2.66667%">#=CSTram !=null ?CSTram : ""#</td> 
                <td class="text-left" style="width: 2.66667%">#=MayBienDienApDongDien !=null ?MayBienDienApDongDien : ""#</td>
                <td class="text-left" style="width: 2.66667%">#=KhacTram !=null ?KhacTram : ""#</td>
                <td class="text-left" style="width: 8%;">#=DienBienSuCo !=null ?DienBienSuCo : ""#</td>
                <td class="text-left" style="width: 8%">#=NguyenNhan !=null ?NguyenNhan : ""#</td>
                <td class="text-left" style="width: 4%">#=ThoiGianGianDoanCungCapDien !=null ?ThoiGianGianDoanCungCapDien : ""#</td>
                <td class="text-left" style="width: 4%">#=ThoiGianSuCo !=null ?ThoiGianSuCo : ""#</td>
                <td class="text-left" style="width: 4%">#=CongXuatTaiTruocSuCo !=null? CongXuatTaiTruocSuCo: ""#</td>
                <td class="text-left" style="width: 4%">#=DienNangKhongCungCapDuoc !=null? DienNangKhongCungCapDuoc : ""#</td>             
            </tr>
            </script> 
        

        </div>
    </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        ThongKe();
        $(".thongke").click(function () {
            ThongKe();
        });
        $(".input-datetime").daterangepicker({
            singleDatePicker: true,
            locale: { format: 'DD/MM/YYYY' }
        });

        $("#btnExel").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BaoCaoKyThuat/QUERYDATA_BaoCaoTinhHinhSuCoDien?excel=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");
        });
        $("#btnword").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BaoCaoKyThuat/QUERYDATA_BaoCaoTinhHinhSuCoDien?word=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");

        });
    });
    function ThongKe() {
        $("#tungay").text($("#SearchTuNgay").val());
        $("#denngay").text($("#SearchDenNgay").val());
        var data = $(".zone_search").siSerializeArray();
        loading();
        $.post("/VIPortalAPI/api/BaoCaoKyThuat/QUERYDATA_BaoCaoTinhHinhSuCoDien?", data, function (data) {

            $(".Title").text(data.Title);
            $(".TieuDeTrai1").text(data.TieuDeTrai1);
            $(".TieuDeTrai2").text(data.TieuDeTrai2);
            $(".Nam").text(data.Nam);


            var htmlTEmp = '';
            var templatehtmltr = kendo.template($("#javascriptTemplate").html());
            $(".mainContentBaoCao .clsContent").html("");
            if (data.data.length > 0) {
                $(".checkdatanull").hide();
                for (var i = 0; i < data.data.length; i++) {
                    data.data[i]["count"] = i + 1;
                    htmlTEmp += templatehtmltr(data.data[i]);
                }

                $(".mainContentBaoCao .clsContent").append(htmlTEmp);
                $('#tongbanghi').text("Có tổng số: " + data.data.length + " bản ghi");

            } else {
               
                $(".checkdatanull").show();

            }
            //$(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');


            $(".clsvbd").css('cursor', 'pointer');
            Endloading();

        });
    }
</script>
<script>
    function fomatdate(dateStr) {
        var pattern = /(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/;

        var formatDateStr = dateStr.replace(pattern, '$1/$2/$3 $4:$5:$6');
        var formatDate = new Date(formatDateStr);
        return formatDate.format("dd/MM/yyyy");
    }
    function fomattime(dateStr) {
        var pattern = /(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/;

        var formatDateStr = dateStr.replace(pattern, '$1/$2/$3 $4:$5:$6');
        var formatDate = new Date(formatDateStr);
        return formatDate.format("HH:mm:ss");
    }
</script>
</html>