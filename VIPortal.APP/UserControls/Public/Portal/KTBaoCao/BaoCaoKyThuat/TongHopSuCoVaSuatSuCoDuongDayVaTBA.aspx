﻿<%@ page language="C#" autoeventwireup="true" codebehind="TongHopSuCoVaSuatSuCoDuongDayVaTBA.aspx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.BaoCaoKyThuat.TongHopSuCoVaSuatSuCoDuongDayVaTBA" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link type="text/css" href="/Content/dist/css/StyleBaoCao.css" rel="stylesheet" />

    <style type="text/css">
        .zone_search {
            overflow-x: hidden;
        }

        .smtable {
            width: 63% !important;
            border-collapse: collapse;
        }

        .smtable_header {
            display: flex;
            /* align-items: center;*/
            /*justify-content: center;*/
            text-align: center;
        }


        tr {
            border: 0.5px solid #96d1f3;
        }

        .chekhey {
            height: 28.24px !important;
            padding: 5px 3px !important;
            font-size: 12px;
        }


        table {
            /* width: 100%;*/
            border-collapse: collapse;
        }



        .flex-one {
            flex: 1;
        }

        .no-wrap {
            white-space: nowrap;
        }
    </style>
</head>
<body>
    <div id="BaoCaoSoDangKy" class="smgrid" data-title="Văn bản đến" data-grid="grid_BaoCaoSoDKVanBanDen"
        data-form="/iVBDH/VanBanDen/pFormVanBanDen.aspx" data-formwidth="1200" data-action="/Vbden/api/VanBanDen/VBDGetListJsonGroupSolr" data-view="/Vbden/VanBanDen/pViewVanBanDen.aspx">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />
                <input type="hidden" name="CurentUser" value="<%=CurentUser.Title %>" />
                <div class="title_icon">
                </div>
                <div class="form-group row text-search align-items-center">
                    <label class="col-sm-2 control-label">Nhập Tháng:</label>

                    <div class="col-sm-2">
                        <select class="form-control" id="Thang" name="Thang">
                            <option value="01" <%=DateTime.Now.ToString("MM") == "01" ? "selected" : "" %>>Tháng 1</option>
                            <option value="02 " <%=DateTime.Now.ToString("MM") == "02" ? "selected" : "" %>>Tháng 2</option>
                            <option value="03 " <%=DateTime.Now.ToString("MM") == "03" ? "selected" : "" %>>Tháng 3</option>
                            <option value="04 " <%=DateTime.Now.ToString("MM") == "04" ? "selected" : "" %>>Tháng 4</option>
                            <option value="05 " <%=DateTime.Now.ToString("MM") == "05" ? "selected" : "" %>>Tháng 5</option>
                            <option value="06 " <%=DateTime.Now.ToString("MM") == "06" ? "selected" : "" %>>Tháng 6</option>
                            <option value="07 " <%=DateTime.Now.ToString("MM") == "07" ? "selected" : "" %>>Tháng 7</option>
                            <option value="08 " <%=DateTime.Now.ToString("MM") == "08" ? "selected" : "" %>>Tháng 8</option>
                            <option value="09 " <%=DateTime.Now.ToString("MM") == "09" ? "selected" : "" %>>Tháng 9</option>
                            <option value="10 " <%=DateTime.Now.ToString("MM") == "10" ? "selected" : "" %>>Tháng 10</option>
                            <option value="11" <%=DateTime.Now.ToString("MM") == "11" ? "selected" : "" %>>Tháng 11</option>
                            <option value="12 " <%=DateTime.Now.ToString("MM") == "12" ? "selected" : "" %>>Tháng 12</option>
                        </select>
                    </div>
                    <label class="col-sm-2 control-label">Nhập Năm:</label>
                    <div class="col-sm-2">
                        <input type="text" name="Nam" id="Nam" class="form-control " placeholder="Nhập năm" value="<%=DateTime.Now.ToString("yyyy") %>" />
                    </div>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-primary thongke">Thống kê</button>
                        <button type="button" class="btn btn-primary" id="btnExel">Excel</button>
                    </div>
                </div>

            </div>
        </div>
        <hr />
        <div class="mainContentBaoCao">
            <div style="display: flex">
                <div style="width: 30%; text-align: center">
                    <p style="font-size: 10pt; width: 100%"><b class="TieuDeTrai1"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeTrai2"></b></p>
                </div>
                <div style="width: 55%; text-align: center">
                    <p style="font-size: 12pt; width: 100%"><b class="Title"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b>Tháng <i class="Nam"></i></b></p>
                </div>
                <div style="width: 15%; text-align: center">
                    <p style="font-size: 12pt; width: 100%">
                        <p class="LoaiBM"></p>
                    </p>
                </div>


            </div>
            <div>
                <table class="myTable">



                    <thead>
                        <tr>
                            <th style="width: 60%" colspan="3">Tên đơn vị</th>
                            <th style="width: 10%">110kV</th>
                            <th style="width: 10%">220kV</th>
                            <th style="width: 10%">500kV</th>
                            <th style="width: 10%">Tổng</th>
                        </tr>

                    </thead>
                    <tbody class="clsContent">
                        <tr>
                            <td rowspan="6">Sự Cố Đường Dây Trên Không</td>
                            <td colspan="2">Chiều dài đường dây (km)</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td rowspan="2">Sự cố thoáng qua</td>
                            <td>Số sự cố thoáng qua</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Suất sự cố thoáng qua</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td rowspan="2">Suất sự cố kéo dài</td>
                            <td>Số sự cố kéo dài</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Suất sự cố kéo dài</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td rowspan="11">Sự Cố Đường Cáp</td>
                            <td rowspan="3">Sự cố cách điện</td>
                            <td>Chiều dài mạch cáp (km)</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr> 
                        <tr>
                            <td>Số sự cố cách điện</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Suất sự cố cách điện</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td rowspan="3">Sự cố mối nối</td>
                            <td>Tổng số mối nối</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr> 
                        <tr>
                            <td>Số sự cố mối nối</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Suất sự cố mối nối</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td rowspan="3">Sự cố thiết bị phụ</td>
                            <td>Tổng số mạch cáp</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr> 
                        <tr>
                            <td>Số sự cố thiết bị phụ</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Suất sự cố thiết bị phụ</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr> 
                        <tr>
                            <td rowspan="2">Sự cố nguyên nhân khác</td>
                            <td>Số sự cố nguyên nhân khác</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr> 
                        <tr>
                            <td>Suất sự cố thiết bị phụ</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td rowspan="27">Sự Cố Trạm Biến Áp</td>
                            <td rowspan="3">Sự cố TBA</td>
                            <td>Tổng số TBA</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Số sự cố tại TBA</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Suất sự cố TBA</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Số sự cố lũy kế năm</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr> 
                        <tr>
                            <td rowspan="3">Sự cố MBA</td>
                            <td>Tổng số MBA</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Số sự cố tại MBA</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Suất sự cố MBA</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                         <tr>
                            <td rowspan="3">Sự cố máy cắt khi có lệnh thao tác</td>
                            <td>Tổng số MC</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Số lần thao tác không thành công</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Suất sự cố MC</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr> 
                        <tr>
                            <td rowspan="3">Sự cố máy cắt khi không có lệnh thao tác</td>
                            <td>Tổng số MC</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Số lần MC đóng nhầm</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Suất sự cố MC</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr> 
                        <tr>
                            <td rowspan="3">Sự cố HT rơ le bảo vệ, điều khiển khi có sự cố</td>
                            <td>Tổng số sự cố của đơn vị</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Số lần HT tác động sai</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Suất sự cố HT BVĐK</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>   
                        <tr>
                            <td rowspan="3">Sự cố HT rơ le bảo vệ, điều khiển khi ko có sự cố</td>
                            <td>Tổng số ngăn lộ</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Số lần HT tác động sai</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Suất sự cố HT BVĐK</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td rowspan="3">Sự cố DCL</td>
                            <td>Tổng số DCL</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Số sự cố DCL</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Suất sự cố DCL</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr> 
                        <tr>
                            <td rowspan="3">Sự cố thiết bị chống sét</td>
                            <td>Tổng số thiết bị CS</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Số sự cố thiết bị CS</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Suất sự cố thiết bị CS</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr> 
                        <tr>
                            <td rowspan="3">Sự cố máy biến dòng/biến áp</td>
                            <td>Tổng số TU/TI</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Số sự cố TU/TI</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Suất sự cố TU/TI</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr> 
                        <tr>
                            <td rowspan="3">Sự cố các thiết bị khác</td>
                            <td>Tổng số ngăn lộ</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Số sự cố các thiết bị khác</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td>Xuất sự cố các thiết bị khác</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        <tr>
                            <td rowspan="2"></td>
                            <td colspan="2">Tổng số sự cố đường dây trên không</td>
                            <td class="data1"></td>
                            <td class="data2"></td>
                            <td class="data3"></td>
                            <td class="data4"></td>
                        </tr>
                        
                    </tbody>
                </table>
            </div>


            <script id="javascriptTemplate" type="text/x-kendo-template">
            <table class="flex-one removetable">
                <tr>
                    <th class="chekhey">#=TitleHeader!=null?TitleHeader:""#</th>
                </tr>
                <tr> <td class="chekhey">#=INDEX1!=null?INDEX1:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX2!=null?INDEX2:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX3!=null?INDEX3:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX4!=null?INDEX4:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX5!=null?INDEX5:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX6!=null?INDEX6:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX7!=null?INDEX7:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX8!=null?INDEX8:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX9!=null?INDEX9:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX10!=null?INDEX10:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX11!=null?INDEX11:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX12!=null?INDEX12:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX13!=null?INDEX13:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX14!=null?INDEX14:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX15!=null?INDEX15:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX16!=null?INDEX16:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX17!=null?INDEX17:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX18!=null?INDEX18:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX19!=null?INDEX19:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX18!=null?INDEX18:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX20!=null?INDEX20:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX21!=null?INDEX21:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX22!=null?INDEX22:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX23!=null?INDEX23:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX24!=null?INDEX24:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX25!=null?INDEX25:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX26!=null?INDEX26:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX27!=null?INDEX27:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX28!=null?INDEX28:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX29!=null?INDEX29:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX30!=null?INDEX30:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX31!=null?INDEX31:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX32!=null?INDEX32:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX33!=null?INDEX33:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX34!=null?INDEX34:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX35!=null?INDEX35:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX36!=null?INDEX36:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX37!=null?INDEX37:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX38!=null?INDEX38:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX39!=null?INDEX39:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX40!=null?INDEX40:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX41!=null?INDEX41:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX42!=null?INDEX42:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX43!=null?INDEX43:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX44!=null?INDEX44:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX45!=null?INDEX45:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX46!=null?INDEX46:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX47!=null?INDEX47:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX48!=null?INDEX48:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX49!=null?INDEX49:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX50!=null?INDEX50:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX51!=null?INDEX51:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX52!=null?INDEX52:""# </td> </tr>
                <tr> <td class="chekhey">#=INDEX53!=null?INDEX53:""# </td> </tr>
            </table>
            </script>
            <script id="javascriptTemplatechekone" type="text/x-kendo-template">
            <table class="flex-one removetable">
                <tr>
                    <th class="chekhey">110kV</th>
                </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
            </table> <table class="flex-one removetable">
                <tr>
                    <th class="chekhey">220KV</th>
                </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
            </table> <table class="flex-one removetable">
                <tr>
                    <th class="chekhey">500kV</th>
                </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
            </table> <table class="flex-one removetable">
                <tr>
                    <th class="chekhey">Tổng</th>
                </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
                <tr> <td class="chekhey"> </td> </tr>
            </table>
            </script>
            <div style="">


                <div style="width: 33%; float: right; text-align: center; margin-top: 15px;">
                    <p style="width: 100%"><b>THỦ TRƯỞNG ĐƠN VỊ</b></p>
                    <p style="font-size: 12pt; width: 100%" class="chucvu"><b></b></p>
                    <br />
                    <br />
                    <br />
                    <br />
                    <p style="font-size: 12pt; width: 100%">
                        <p class="NguoiKy"></p>
                    </p>
                </div>
                <div style="width: 33%; float: right; text-align: center; margin-top: 15px;">
                </div>
                <div style="width: 33%; float: right; text-align: center; margin-top: 15px;">
                    <p style="width: 100%"><b>NGƯỜI LẬP BÁO CÁO</b>  </p>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <p style="font-size: 12pt; width: 100%">
                        <p class="ToTongHop"></p>
                    </p>
                </div>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        ThongKe();
        $(".thongke").click(function () {
            ThongKe();
        });
        $(".input-datetime").daterangepicker({
            singleDatePicker: true,
            locale: { format: 'DD/MM/YYYY' }
        });

        $("#btnExel").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BaoCaoKyThuat/QUERYDATA_TongHopSuCoVaSuatSuCoDuongDayVaTBA?excel=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");
        });
        $("#btnword").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BaoCaoKyThuat/QUERYDATA_TongHopSuCoVaSuatSuCoDuongDayVaTBA?word=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");

        });
    });
    function ThongKe() {
        $(".removetable").remove();
        $("#tungay").text($("#SearchTuNgay").val());
        $("#denngay").text($("#SearchDenNgay").val());
        var data = $(".zone_search").siSerializeArray();
        loading();
        $.post("/VIPortalAPI/api/BaoCaoKyThuat/QUERYDATA_TongHopSuCoVaSuatSuCoDuongDayVaTBA?", data, function (data) {

            $(".Title").text(data.Title);
            $(".TieuDeTrai1").text(data.TieuDeTrai1);
            $(".TieuDeTrai2").text(data.TieuDeTrai2);
            $(".Nam").text(data.Nam);
            //<th style="width: 10%">110kV</th>
            //                <th style="width: 10%">220kV</th>
            //                <th style="width: 10%">500kV</th>
            //                <th style="width: 10%">Tổng</th>

            var htmlTEmp = '';
            var templatehtmltr = kendo.template($("#javascriptTemplate").html());
            $(".mainContentBaoCao .clsContent").html("");
            if (data.data.length > 0) {
                for (var i = 0; i < data.data.length; i++) {
                    if (i == 0) {
                        data.data[0]["TitleHeader"] = "110kV";

                    }
                    else if (i == 1) {
                        data.data[1]["TitleHeader"] = "220kV";
                    }
                    else if (i == 2) {
                        data.data[2]["TitleHeader"] = "500kV";
                    }
                    else if (i == 3) {
                        data.data[3]["TitleHeader"] = "Tổng";
                    }
                    else {

                    }
                    console.log(data.data[i]["TitleHeader"])
                    // data.data[i]["STT"] = i + 1;
                    htmlTEmp += templatehtmltr(data.data[i]);

                }
                $("#Bang-append").append(htmlTEmp);

            } else {
                var templatehtmltrs = $("#javascriptTemplatechekone").html();
                $("#Bang-append").append(templatehtmltrs);
            }
            $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');

            $('#tongbanghi').text("Có tổng số: " + data.data.length + " bản ghi");

            $(".clsvbd").css('cursor', 'pointer');
            Endloading();
        });

    }

</script>
</html>
