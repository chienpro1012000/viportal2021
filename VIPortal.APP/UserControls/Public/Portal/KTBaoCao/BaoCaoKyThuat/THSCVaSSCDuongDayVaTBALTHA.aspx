﻿<%@ page language="C#" autoeventwireup="true" codebehind="THSCVaSSCDuongDayVaTBALTHA.aspx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.BaoCaoKyThuat.THSCVaSSCDuongDayVaTBALTHA" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link type="text/css" href="/Content/dist/css/StyleBaoCao.css" rel="stylesheet" />

    <style type="text/css">
        .zone_search {
            overflow-x: hidden;
        }

        .smtable {
            width: 63% !important;
            border-collapse: collapse;
        }

        .smtable_header {
            display: flex; 
            /* align-items: center;*/
            /*justify-content: center;*/
            text-align: center;
        }


        .flex-one tr td {
            border: 0.5px solid #96d1f3;
                height: 29.24px;
        }

        .flex-one tr th {
            border: 0.5px solid #96d1f3;
                height: 29.24px;
        }

        .chekhey {
            height: 29.24px !important;
            padding: 5px 3px !important;
            font-size: 12px;
        }


        table {
            /* width: 100%;*/
            border-collapse: collapse;
        }



        .flex-one {
            flex: 1;
        }

        .no-wrap {
            white-space: nowrap;
        }
    </style>
</head>
<body>
    <div id="BaoCaoSoDangKy" class="smgrid" data-title="Văn bản đến" data-grid="grid_BaoCaoSoDKVanBanDen"
        data-form="/iVBDH/VanBanDen/pFormVanBanDen.aspx" data-formwidth="1200" data-action="/Vbden/api/VanBanDen/VBDGetListJsonGroupSolr" data-view="/Vbden/VanBanDen/pViewVanBanDen.aspx">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />
                <input type="hidden" name="CurentUser" value="<%=CurentUser.Title %>" />
                <div class="title_icon">
                </div>
                <div class="form-group row text-search align-items-center">
                    <label class="col-sm-2 control-label">Nhập Tháng:</label>

                    <div class="col-sm-2">
                        <select class="form-control" id="Thang" name="Thang">
                            <option value="01" <%=DateTime.Now.ToString("MM") == "01" ? "selected" : "" %>>Tháng 1</option>
                            <option value="02 " <%=DateTime.Now.ToString("MM") == "02" ? "selected" : "" %>>Tháng 2</option>
                            <option value="03 " <%=DateTime.Now.ToString("MM") == "03" ? "selected" : "" %>>Tháng 3</option>
                            <option value="04 " <%=DateTime.Now.ToString("MM") == "04" ? "selected" : "" %>>Tháng 4</option>
                            <option value="05 " <%=DateTime.Now.ToString("MM") == "05" ? "selected" : "" %>>Tháng 5</option>
                            <option value="06 " <%=DateTime.Now.ToString("MM") == "06" ? "selected" : "" %>>Tháng 6</option>
                            <option value="07 " <%=DateTime.Now.ToString("MM") == "07" ? "selected" : "" %>>Tháng 7</option>
                            <option value="08 " <%=DateTime.Now.ToString("MM") == "08" ? "selected" : "" %>>Tháng 8</option>
                            <option value="09 " <%=DateTime.Now.ToString("MM") == "09" ? "selected" : "" %>>Tháng 9</option>
                            <option value="10 " <%=DateTime.Now.ToString("MM") == "10" ? "selected" : "" %>>Tháng 10</option>
                            <option value="11" <%=DateTime.Now.ToString("MM") == "11" ? "selected" : "" %>>Tháng 11</option>
                            <option value="12 " <%=DateTime.Now.ToString("MM") == "12" ? "selected" : "" %>>Tháng 12</option>
                        </select>
                    </div>
                    <label class="col-sm-2 control-label">Nhập Năm:</label>
                    <div class="col-sm-2">
                        <input type="text" name="Nam" id="Nam" class="form-control " placeholder="Nhập năm" value="<%=DateTime.Now.ToString("yyyy") %>" />
                    </div>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-primary thongke">Thống kê</button>
                        <button type="button" class="btn btn-primary" id="btnExel">Excel</button>
                    </div>
                </div>

            </div>
        </div>
        <hr />
        <div class="mainContentBaoCao">
            <div style="display: flex">
                <div style="width: 30%; text-align: center">
                    <p style="font-size: 10pt; width: 100%"><b class="TieuDeTrai1"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeTrai2"></b></p>
                </div>
                <div style="width: 55%; text-align: center">
                    <p style="font-size: 12pt; width: 100%"><b class="Title"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b>Tháng <i class="Nam"></i></b></p>
                </div>
                <div style="width: 15%; text-align: center">
                    <p style="font-size: 12pt; width: 100%">
                        <p class="LoaiBM"></p>
                    </p>
                </div>


            </div>
            <div class="smtable_header" id="Bang-append">
                <table class="smtable">



                    <thead>
                        <tr>
                            <th style="width: 60%" colspan="3">Tên đơn vị</th>
                            <%-- <th style="width: 10%">110kV</th>
                            <th style="width: 10%">220kV</th>
                            <th style="width: 10%">500kV</th>
                            <th style="width: 10%">Tổng</th>--%>
                        </tr>

                        <tr>
                            <th style="width: 15%" rowspan="25">Sự Cố Đường Dây Trên Không</th>
                            <th style="width: 25%" colspan="2">Chiều dài đường dây (km)</th>

                        </tr>
                        <tr>
                            <th style="width: 10%" rowspan="2">Sự cố thoáng qua </th>
                            <th style="width: 15%">Số sự cố thoáng qua</th>

                        </tr>
                        <tr>
                            <th>Suất sự cố thoáng qua</th>

                        </tr>
                        <tr>
                            <th rowspan="2">Suất sự cố kéo dài</th>
                            <th>Số sự cố kéo dài</th>
                        </tr>
                        <tr>
                            <th>Suất sự cố kéo dài</th>
                        </tr>
                        <tr>
                            <tr>
                                <th rowspan="3">Sự cố t.bị đóng cắt do thao tác không thành công</th>
                                <th>Số lần đóng cắt  t.bị đóng cắt</th>
                            </tr>
                            <tr>
                                <th>Số lần thao tác không thành công</th>
                            </tr>
                            <tr>
                                <th>Suất sự cố  t.bị đóng cắt</th>
                            </tr>
                            <tr>
                                <tr>
                                    <th rowspan="3">Sự cố t.bị đóng cắt đóng cắt nhầm</th>
                                    <th>Tổng số  t.bị đóng cắt</th>
                                </tr>
                                <tr>
                                    <th>Số lần  t.bị đóng cắt đóng nhầm</th>
                                </tr>
                                <tr>
                                    <th>Suất sự cố  t.bị đóng cắt</th>
                                </tr>
                                <tr>
                                    <tr>
                                        <th rowspan="3">Sự cố thiết bị tự động đóng lặp lại (recloser)</th>
                                        <th>Tổng số  t.bị recloser</th>
                                    </tr>
                                    <tr>
                                        <th>Số lần sự cố recloser </th>
                                    </tr>
                                    <tr>
                                        <th>Suất sự cố  recloser </th>
                                    </tr>
                                    <tr>
                                        <tr>
                                            <th rowspan="3">Sự cố thiết bị thiết bị chống sét </th>
                                            <th>Tổng số  t.bị chống sét</th>
                                        </tr>
                                        <tr>
                                            <th>Số lần sự cố t.bị chống sét </th>
                                        </tr>
                                        <tr>
                                            <th>Suất sự cố t.bị chống sét </th>
                                        </tr>
                                        <tr>
                                            <tr>
                                                <th rowspan="3">Sự cố thiết bị khác</th>
                                                <th>Tổng số  t.bị khác</th>
                                            </tr>
                                            <tr>
                                                <th>Số lần sự cố t.bị khác</th>
                                            </tr>
                                            <tr>
                                                <th>Suất sự cố  t.bị khác</th>
                                            </tr>
                                            <tr>
                                                <tr>
                                                    <th style="width: 15%" rowspan="13">Sự cố đường cáp</th>

                                                </tr>
                                                <tr>
                                                    <th style="width: 10%" rowspan="3">Sự cố cách điện </th>
                                                    <th style="width: 15%">Chiều dài mạch cáp (km)</th>

                                                </tr>
                                                <tr>
                                                    <th>Số sự cố cách điện</th>

                                                </tr>
                                                <tr>
                                                    <th>Suất sự cố cách điện</th>
                                                </tr>
                                                <tr>
                                                    <th rowspan="3">Sự cố mối nối</th>
                                                    <th>Tổng số mối nối</th>
                                                </tr>
                                                <tr>
                                                    <th>Số sự cố mối nối</th>
                                                </tr>
                                                <tr>
                                                    <th>Suất sự cố mối nối</th>
                                                </tr>
                                                <tr>
                                                    <tr>
                                                        <th rowspan="3">Sự cố thiết bị phụ</th>
                                                        <th>Tổng số mạch cáp</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Số sự cố thiết bị phụ</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Suất sự cố thiết bị phụ</th>
                                                    </tr>
                                                    <tr>
                                                        <th rowspan="2">Sự cố nguyên nhân khác</th>
                                                        <th>Số sự cố nguyên nhân khác</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Suất sự cố nguyên nhân khác</th>
                                                    </tr>


                                                    <tr>
                                                        <th style="width: 15%" rowspan="6">Sự cố trạm biến áp</th>

                                                    </tr>
                                                    <tr>
                                                        <th style="width: 10%" rowspan="3">Sự cố MBA </th>
                                                        <th style="width: 15%">Tổng số MBA</th>

                                                    </tr>
                                                    <tr>
                                                        <th>Số sự cố MBA</th>

                                                    </tr>
                                                    <tr>
                                                        <th>Suất sự cố MBA</th>
                                                    </tr>
                                                    <tr>
                                                        <th rowspan="2">Sự cố TBA</th>
                                                        <th>Số sự cố TBA</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Suất sự cố TBA</th>
                                                    </tr>

                                                    <tr>
                                                        <th rowspan="7"></th>
                                                        <th colspan="2">Tổng số sự cố đường dây trên không</th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2">Tổng số sự cố đường cáp</th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2">Tổng số sự cố TBA</th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2">Tổng thời gian sự cố đường dây trên không (phút)</th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2">Tổng thời gian sự cố đường cáp (phút)</th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2">Tổng thời gian sự cố MBA (phút)</th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2">Tổng sản lượng điện không cung cấp được (tr. kWh)</th>
                                                    </tr>
                    </thead>

                </table>
                <table class="flex-one checkdata ">

                    <tr>
                        <th>35kV</th>
                        <th>22kV</th>
                        <th>15kV</th>
                        <th>10kv</th>
                        <th>6kV</th>
                        <th>0.4kV</th>
                        <th>Tổng</th>
                    </tr>
                    <tbody class="clsContent">
                        <tr>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                        </tr>
                        <tr>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                        </tr> 
                        <tr>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                        </tr>
                        <tr>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                        </tr> 
                        <tr>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                        </tr>
                        <tr>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                        </tr> 
                        <tr>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                        </tr>
                      
                    </tbody>


                </table>
            </div>


            <script id="javascriptTemplate" type="text/x-kendo-template">
                 <tr>
                     <td class="chekhey">#=INDEX1!=null?INDEX1:""# </td>
                     <td class="chekhey">#=INDEX2!=null?INDEX2:""# </td>
                     <td class="chekhey">#=INDEX3!=null?INDEX3:""# </td>
                     <td class="chekhey">#=INDEX4!=null?INDEX4:""# </td>
                     <td class="chekhey">#=INDEX5!=null?INDEX5:""# </td>
                     <td class="chekhey">#=INDEX6!=null?INDEX6:""# </td>
                     <td class="chekhey">#=INDEX7!=null?INDEX7:""# </td>
                   </tr>
            </script> 
            <script id="javascriptTemplate_nodedata" type="text/x-kendo-template">
                      <tr>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                            <td class="chekhey"></td>
                        </tr>
            </script>
            <div style="">


                <div style="width: 33%; float: right; text-align: center; margin-top: 15px;">
                    <p style="width: 100%"><b>THỦ TRƯỞNG ĐƠN VỊ</b></p>
                    <p style="font-size: 12pt; width: 100%" class="chucvu"><b></b></p>
                    <br />
                    <br />
                    <br />
                    <br />
                    <p style="font-size: 12pt; width: 100%">
                        <p class="NguoiKy"></p>
                    </p>
                </div>
                <div style="width: 33%; float: right; text-align: center; margin-top: 15px;">
                </div>
                <div style="width: 33%; float: right; text-align: center; margin-top: 15px;">
                    <p style="width: 100%"><b>NGƯỜI LẬP BÁO CÁO</b>  </p>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <p style="font-size: 12pt; width: 100%">
                        <p class="ToTongHop"></p>
                    </p>
                </div>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        ThongKe();
        $(".thongke").click(function () {
            ThongKe();
        });
        $(".input-datetime").daterangepicker({
            singleDatePicker: true,
            locale: { format: 'DD/MM/YYYY' }
        });

        $("#btnExel").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BaoCaoKyThuat/QUERYDATA_THSCVaSSCDuongDayVaTBALTHA?excel=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");
        });
        $("#btnword").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BaoCaoKyThuat/QUERYDATA_THSCVaSSCDuongDayVaTBALTHA?word=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");

        });
    });
    function ThongKe() {
        $(".removetable").remove();
        $("#tungay").text($("#SearchTuNgay").val());
        $("#denngay").text($("#SearchDenNgay").val());
        var data = $(".zone_search").siSerializeArray();
        loading();
        $.post("/VIPortalAPI/api/BaoCaoKyThuat/QUERYDATA_THSCVaSSCDuongDayVaTBALTHA?", data, function (data) {

            $(".Title").text(data.Title);
            $(".TieuDeTrai1").text(data.TieuDeTrai1);
            $(".TieuDeTrai2").text(data.TieuDeTrai2);
            $(".Nam").text(data.Nam);
            //<th style="width: 10%">110kV</th>
            //                <th style="width: 10%">220kV</th>
            //                <th style="width: 10%">500kV</th>
            //                <th style="width: 10%">Tổng</th>

            var htmlTEmp = '';
            var templatehtmltr = kendo.template($("#javascriptTemplate").html());
            $(".mainContentBaoCao .clsContent").html("");
            if (data.data.length > 0) {
                $(".checkdata").show();
                for (var i = 0; i < data.data.length; i++) {

                    console.log(data.data[i]["TitleHeader"])
                    // data.data[i]["STT"] = i + 1;
                    htmlTEmp += templatehtmltr(data.data[i]);

                }

                $(".clsContent").append(htmlTEmp);

            } else {
                var htmlTEmps = "";
                var templatehtmltrs =$("#javascriptTemplate_nodedata").html();

                for (var i = 0; i < 43; i++) {
                    htmlTEmps += templatehtmltrs;

                }
                $(".clsContent").append(htmlTEmps);
            }
            $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');


            $(".clsvbd").css('cursor', 'pointer');
            Endloading();

        });
    }

</script>
</html>
