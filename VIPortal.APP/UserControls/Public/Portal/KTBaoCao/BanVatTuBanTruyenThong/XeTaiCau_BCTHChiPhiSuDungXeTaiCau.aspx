﻿<%@ page language="C#" autoeventwireup="true" codebehind="XeTaiCau_BCTHChiPhiSuDungXeTaiCau.aspx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.BanVatTuBanTruyenThong.XeTaiCau_BCTHChiPhiSuDungXeTaiCau" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link type="text/css" href="/Content/dist/css/StyleBaoCao.css" rel="stylesheet" />
    <style type="text/css">
        .zone_search {
            overflow-x: hidden;
        }
    </style>
</head>
<body>
    <div id="BaoCaoSoDangKy" class="smgrid" data-title="Văn bản đến" data-grid="grid_BaoCaoSoDKVanBanDen"
        data-form="/iVBDH/VanBanDen/pFormVanBanDen.aspx" data-formwidth="1200" data-action="/Vbden/api/VanBanDen/VBDGetListJsonGroupSolr" data-view="/Vbden/VanBanDen/pViewVanBanDen.aspx">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />
                <input type="hidden" name="CurentUser" value="<%=CurentUser.Title %>" />
                <div class="title_icon">
                </div>
                <div class="form-group row text-search align-items-center">
                    <label class="col-sm-2 control-label">Tháng báo cáo:</label>
                    <div class="col-sm-3">
                        <select class="form-control" id="Thang" name="Thang">
                            <option value="01" <%=DateTime.Now.ToString("MM") == "01" ? "selected" : "" %>>Tháng 1</option>
                            <option value="02 " <%=DateTime.Now.ToString("MM") == "02" ? "selected" : "" %>>Tháng 2</option>
                            <option value="03 " <%=DateTime.Now.ToString("MM") == "03" ? "selected" : "" %>>Tháng 3</option>
                            <option value="04 " <%=DateTime.Now.ToString("MM") == "04" ? "selected" : "" %>>Tháng 4</option>
                            <option value="05 " <%=DateTime.Now.ToString("MM") == "05" ? "selected" : "" %>>Tháng 5</option>
                            <option value="06 " <%=DateTime.Now.ToString("MM") == "06" ? "selected" : "" %>>Tháng 6</option>
                            <option value="07 " <%=DateTime.Now.ToString("MM") == "07" ? "selected" : "" %>>Tháng 7</option>
                            <option value="08 " <%=DateTime.Now.ToString("MM") == "08" ? "selected" : "" %>>Tháng 8</option>
                            <option value="09 " <%=DateTime.Now.ToString("MM") == "09" ? "selected" : "" %>>Tháng 9</option>
                            <option value="10 " <%=DateTime.Now.ToString("MM") == "10" ? "selected" : "" %>>Tháng 10</option>
                            <option value="11" <%=DateTime.Now.ToString("MM") == "11" ? "selected" : "" %>>Tháng 11</option>
                            <option value="12 " <%=DateTime.Now.ToString("MM") == "12" ? "selected" : "" %>>Tháng 12</option>
                        </select>
                    </div>
                    <label class="col-sm-2 control-label">Năm báo cáo :</label>
                    <div class="col-sm-3">
                        <input type="text" name="Nam" id="Nam" class="form-control " placeholder="Nhập năm" value="<%=DateTime.Now.ToString("yyyy") %>" />
                    </div>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-primary thongke">Thống kê</button>
                        <button type="button" class="btn btn-primary" id="btnExel">Excel</button>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="mainContentBaoCao">
            <div style="">
                <div style="width: 30%; float: left; text-align: center">
                    <p style="font-size: 10pt; width: 100%"><b class="TieuDeTrai1">TỔNG CÔNG TY ĐIỆN LỰC TP.HÀ NỘI</b></p>
                    <p style="font-size: 10pt; width: 100%"><b class="TieuDeTrai2">BAN VẬT TƯ </b></p>
                    <p style="font-size: 10pt; width: 100%"><b class="">Số :____/TVU </b></p>
                </div>
                <div style="width: 55%; float: left; text-align: center">
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeGiua1">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM </b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeGiua2">Độc lập - Tự do -Hạnh phúc</b></p>
                    <p style="font-size: 12pt; width: 100%"><b>_____________________</b></p>
                </div>
                <div style="width: 15%; float: left; text-align: center">
                    <p style="font-size: 12pt; width: 100%">
                        <p class="LoaiBM"></p>
                    </p>
                </div>

                <div style="width: 100%; float: left; text-align: center">
                    <p style="font-size: 12pt; width: 100%"><b class="Title"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b>Tháng <i class="Thang"></i>- Năm <i class="Nam"></i></b></p>
                </div>
            </div>
            <div class="smtable_header">
                <table class="smtable">
                    <thead>
                        <tr>
                            <th style="width: 5%;">STT</th>
                            <th style="width: 20%">Đơn Vị</th>
                            <th style="width: 10%;">Số chuyến </th>
                            <th style="width: 10%;">Số km </th>
                            <th style="width: 10%;">Số giờ cẩu </th>
                            <th style="width: 15%;">Số tiền</th>
                            <th style="width: 20%;">Ghi chú </th>
                        </tr>
                        <tr>
                            <th style="width: 5%;">1</th>
                            <th style="width: 20%">2</th>
                            <th style="width: 10%;">3</th>
                            <th style="width: 10%;">4</th>
                            <th style="width: 10%;">5</th>
                            <th style="width: 15%;">6</th>
                            <th style="width: 20%;">7</th>
                        </tr>

                    </thead>
                    <tbody class="clsContent">
                    </tbody>
                </table>
            </div>
            <div class="text-center mt-4 checkdatanull" style="width: 100%; float: left">
                <p style="font-size: 12pt; width: 100%">Chưa có dữ liệu </p>
            </div>
            <script id="javascriptTemplate" type="text/x-kendo-template">
            <tr class="clsvbd" data-id="">
                <td style="width:5%;" >#=STT  != null ? STT : ""#</td>
                <td style="width:20%" >#=donvisudung != null ? donvisudung : ""#</td>
                <td style="width:10%;">#=SoChuyen != null ? SoChuyen  : ""#</td>
                <td style="width:10%;">#=SoKM != null ? SoKM : ""#</td>
                <td style="width:10%;">#=SoGioCau != null ? SoGioCau : ""#</td>
                <td style="width:15%;"></td>
                <td style="width:20%;">#=ghichu != null ? ghichu : ""#</td>
            </tr>
            </script>
        </div>
    </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        ThongKe();
        $(".thongke").click(function () {
            ThongKe();
        });
        $(".input-datetime").daterangepicker({
            singleDatePicker: true,
            locale: { format: 'DD/MM/YYYY' }
        });

        $("#btnExel").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BanVatTuBanTruyenThong/QUERYDATA_XeTaiCau_BCTHChiPhiSuDungXeTaiCau?excel=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");
        });
        $("#btnword").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BanVatTuBanTruyenThong/QUERYDATA_XeTaiCau_BCTHChiPhiSuDungXeTaiCau?word=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");

        });
    });
    function ThongKe() {
        $("#tungay").text($("#SearchTuNgay").val());
        $("#denngay").text($("#SearchDenNgay").val());
        var data = $(".zone_search").siSerializeArray();
        loading();
        $.post("/VIPortalAPI/api/BanVatTuBanTruyenThong/QUERYDATA_XeTaiCau_BCTHChiPhiSuDungXeTaiCau?", data, function (data) {

            $(".Title").text(data.Title);
            $(".TieuDeTrai1").text(data.TieuDeTrai1);
            $(".TieuDeTrai2").text(data.TieuDeTrai2);
            $(".Nam").text(data.Nam);
            $(".Thang").text(data.Thang);
            $(".TieuDeGiua1").text(data.TieuDeGiua1);
            $(".TieuDeGiua2").text(data.TieuDeGiua2);
            $(".LoaiBM").text(data.LoaiBM);


            var htmlTEmp = '';
            var templatehtmltr = kendo.template($("#javascriptTemplate").html());
            $(".mainContentBaoCao .clsContent").html("");
            if (data.data.length > 0) {
                $(".checkdatanull").hide();
                for (var i = 0; i < data.data.length; i++) {
                    data.data[i]["count"] = i + 1;
                    htmlTEmp += templatehtmltr(data.data[i]);
                }

                $(".mainContentBaoCao .clsContent").append(htmlTEmp);
                $('#tongbanghi').text("Có tổng số: " + data.data.length + " bản ghi");

            } else {
                $(".checkdatanull").show();
            }
            $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');


            $(".clsvbd").css('cursor', 'pointer');

        });
        Endloading();
    }

</script>

</html>
