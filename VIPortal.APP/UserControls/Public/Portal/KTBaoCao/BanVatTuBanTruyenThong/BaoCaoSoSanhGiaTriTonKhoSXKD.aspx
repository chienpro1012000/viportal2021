﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BaoCaoSoSanhGiaTriTonKhoSXKD.aspx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.BanVatTuBanTruyenThong.BaoCaoSoSanhGiaTriTonKhoSXKD" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link type="text/css" href="/Content/dist/css/StyleBaoCao.css" rel="stylesheet" />
    <style type="text/css">
        .zone_search {
            overflow-x: hidden;
        }
        
    </style>
</head>
<body>
    <div id="BaoCaoSoDangKy" class="smgrid" data-title="Văn bản đến" data-grid="grid_BaoCaoSoDKVanBanDen"
        data-form="/iVBDH/VanBanDen/pFormVanBanDen.aspx" data-formwidth="1200" data-action="/Vbden/api/VanBanDen/VBDGetListJsonGroupSolr" data-view="/Vbden/VanBanDen/pViewVanBanDen.aspx">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />
                <input type="hidden" name="CurentUser" value="<%=CurentUser.Title %>" />
                <div class="title_icon">
                </div>
                <div class="form-group row text-search align-items-center">
                    <label class="col-sm-2 control-label">Tháng báo cáo:</label>
                    <div class="col-sm-2">
                        <select class="form-control" id="Thang" name="Thang">
                            <option value="01" <%=DateTime.Now.ToString("MM") == "01" ? "selected" : "" %>>Tháng 1</option>
                            <option value="02 " <%=DateTime.Now.ToString("MM") == "02" ? "selected" : "" %>>Tháng 2</option>
                            <option value="03 " <%=DateTime.Now.ToString("MM") == "03" ? "selected" : "" %>>Tháng 3</option>
                            <option value="04 " <%=DateTime.Now.ToString("MM") == "04" ? "selected" : "" %>>Tháng 4</option>
                            <option value="05 " <%=DateTime.Now.ToString("MM") == "05" ? "selected" : "" %>>Tháng 5</option>
                            <option value="06 " <%=DateTime.Now.ToString("MM") == "06" ? "selected" : "" %>>Tháng 6</option>
                            <option value="07 " <%=DateTime.Now.ToString("MM") == "07" ? "selected" : "" %>>Tháng 7</option>
                            <option value="08 " <%=DateTime.Now.ToString("MM") == "08" ? "selected" : "" %>>Tháng 8</option>
                            <option value="09 " <%=DateTime.Now.ToString("MM") == "09" ? "selected" : "" %>>Tháng 9</option>
                            <option value="10 " <%=DateTime.Now.ToString("MM") == "10" ? "selected" : "" %>>Tháng 10</option>
                            <option value="11" <%=DateTime.Now.ToString("MM") == "11" ? "selected" : "" %>>Tháng 11</option>
                            <option value="12 " <%=DateTime.Now.ToString("MM") == "12" ? "selected" : "" %>>Tháng 12</option>
                        </select>
                    </div>
                    <label class="col-sm-2 control-label">Năm báo cáo :</label>
                    <div class="col-sm-2">
                        <input type="text" name="Nam" id="Nam" class="form-control " placeholder="Nhập năm" value="<%=DateTime.Now.ToString("yyyy") %>" />
                    </div>
                    
                </div>
                 <div class="form-group row text-search align-items-center">
                   <label class="col-sm-2 control-label">Đơn Vị:</label>
                       <input type="hidden" name="DonViText" id="DonViText" value="" />
                    <div class="col-sm-2">
                        <select class="form-control" id="MaDonVi" name="MaDonVi">
                             <option value="all">Tất cả</option>
                            <option value="071400">Công ty Điện lực Long Biên</option>
                            <option value="071500">Công ty Điện lực Mê Linh</option>
                            <option value="072200">Công ty Điện lực Đan Phượng</option>
                            <option value="072900">Công ty Điện lực Ứng Hòa</option>
                            <option value="073100">Công ty Công nghệ thông tin Điện lực Hà Nội</option>
                            <option value="073901">Ban Quản lý dự án Lưới điện Hà Nội - DEP</option>
                            <option value="070400">Công ty Điện lực Đống Đa</option>
                            <option value="072000">Công ty Điện lực Thường Tín</option>
                            <option value="072300">Công ty Điện lực Hoài Đức</option>
                            <option value="073500">Công ty Thí nghiệm điện Điện lực Hà Nội</option>
                            <option value="073802">Ban Quản lý dự án phát triển Điện lực Hà Nội - XDCB</option>
                            <option value="070200">Công ty Điện lực Hai Bà Trưng</option>
                            <option value="071700">Công ty Điện lực Sơn Tây</option>
                            <option value="071900">Công ty Điện lực Thạch Thất</option>
                            <option value="072800">Công ty Điện lực Thanh Oai</option>
                            <option value="073200">Trung tâm Điều độ Hệ thống điện TP Hà Nội</option>
                            <option value="073902">Ban Quản lý dự án Lưới điện Hà Nội - XDCB</option>
                            <option value="071000">Công ty Điện lực Tây Hồ </option>
                            <option value="071100">Công ty Điện lực Thanh Xuân</option>
                            <option value="072500">Công ty Điện lực Phú Xuyên</option>
                            <option value="070300">Công ty Điện lực Ba Đình</option>
                            <option value="070800">Công ty Điện lực Đông Anh</option>
                            <option value="071300">Công ty Điện lực Hoàng Mai</option>
                            <option value="072400">Công ty Điện lực Mỹ Đức</option>
                            <option value="070100">Công ty Điện lực Hoàn Kiếm</option>
                            <option value="071200">Công ty Điện lực Cầu Giấy</option>
                            <option value="071600">Công ty Điện lực Hà Đông</option>
                            <option value="072100">Công ty Điện lực Ba Vì</option>
                            <option value="073000">Công ty Điện lực Bắc Từ Liêm</option>
                            <option value="074000">Tổng Công ty điện lực TP Hà Nội</option>
                            <option value="070500">Công ty Điện lực Nam Từ Liêm</option>
                            <option value="073801">Ban Quản lý dự án phát triển Điện lực Hà Nội - ADB</option>
                            <option value="000100">Tập đoàn Điện lực Việt Nam - Kế toán ngành</option>
                            <option value="070600">Công ty Điện lực Thanh Trì</option>
                            <option value="070700">Công ty Điện lực Gia Lâm</option>
                            <option value="070900">Công ty Điện lực Sóc Sơn</option>
                            <option value="071800">Công ty Điện lực Chương Mỹ</option>
                            <option value="072600">Công ty Điện lực Phúc Thọ</option>
                            <option value="072700">Công ty Điện lực Quốc Oai</option>
                            <option value="073600">Công ty Lưới điện cao thế TP Hà Nội</option>
                            <option value="073700">Công ty Dịch vụ Điện lực Hà Nội</option>
                        </select>
                    </div>
                  
                    <div class="col-sm-3">
                        <button type="button" class="btn btn-primary thongke">Thống kê</button>
                        <button type="button" class="btn btn-primary" id="btnExel">Excel</button>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="mainContentBaoCao">
            <div style="">
                <div style="width: 30%; float: left; text-align:center">
                      <p style="font-size: 10pt; width: 100%"><b class="TieuDeTrai1"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeTrai2"></b></p>
                     <p style="font-size: 12pt; width: 100%"><b class="TieuDeTrai3"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="">Số:_____/ BC</b></p>
                </div>
                <div style="width: 55%; float: left; text-align:center">
                     <p style="font-size: 12pt; width: 100%"><b class="TieuDeGiua1"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeGiua2"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="Title"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b> Tháng <i class="Thang"></i></b><b> Năm <i class="Nam"></i></b></p>
                </div>
                <div style="width: 15%; float: left; text-align:center">
                      <p style="font-size: 12pt; width: 100%"><p class="LoaiBM"></p></p>
                </div>
              

            </div>
            <div class="smtable_header">
                <table class="smtable">
                    <thead>
                        <tr>
                            <th style="width:10%;"rowspan="3">Số thứ tự</th>
                            <th style="width:20%;">Đơn vị</th>
                            <th style="width:36%;">Số liệu chốt 20 định kỳ tháng trước</th>  
                            <th style="width:36%;">Số liệu chốt ngày 20 đinh kỳ tháng sau</th>
                            <th style="width:18%;"> Tăng giảm (T8,T9) </th>
                            
                        </tr>
                       
                        <tr>                              
                              
                            <th >Dữ liệu trên ERP</th>  
                            <th >Trên ERP biểu 011B( ngoài kho)</th>  
                            <th >Trên ERP biểu 011B( trong kho)</th>                          
                            <th >%</th>
                        </tr>
                        <tr>                              
                                                        
                            <th >1</th>  
                            <th >2</th> 
                            <th >3</th>  
                            <th >4=(3/2)*100-100</th>
                            
                        </tr>
                    </thead>
                    <tbody class="clsContent">
                    </tbody>
                </table>
            </div>
              <div class="text-center mt-4 checkdatanull" style="width:100% ; float: left ">
                <p style="font-size: 12pt; width: 100%">Chưa có dữ liệu </p>
            </div>
            <script id="javascriptTemplate" type="text/x-kendo-template">
            <tr class="clsvbd" data-id="">
                <td style="width: 10%">#=STT#</td>                                     
                <td style="width: 20%">#=DonVi#</td>                                     
                <td style="width: 36%">#=NUM1!=null?NUM1:""#</td>                                     
                <td style="width: 36%">#=NUM2!=null?NUM2:""#</td>                                     
                <td style="width: 18%">#=NUM3!=null?NUM3:""#</td>                                     
                                                   
            </tr>
            </script>
               <div style="">

                    <div style="width: 50%; float: left; text-align: left; margin-top: 15px;">
                        <p style="width: 100%">Ghi chú : </p>
                        <p style="font-size: 10pt; width: 100%"> Kỳ vọng dữ liệu tích hợp từ chương trình ERP là tốt nhất </p>
                        <p style="font-size: 10pt; width: 100%">  Nếu nhập tay thì vẫn có thể nhầm lần  </p>
                      
                    </div> 
                   <div style="width: 50%; float: right; text-align: center; margin-top: 15px;">
                        <p style="width: 100%"><b>TRƯỞNG ĐƠN VỊ </b></p>
                        <p style="font-size: 12pt; width: 100%"><i>(ký tên, đóng dấu)</i></p>
                        <br />
                        <br />
                        <br />
                        <br />
                        <p style="font-size: 12pt; width: 100%"><b class="NguoiKy"></b></p>
                    </div>
                </div>
        </div>
    </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        ThongKe();
        $(".thongke").click(function () {
            ThongKe();
        });
        $(".input-datetime").daterangepicker({
            singleDatePicker: true,
            locale: { format: 'DD/MM/YYYY' }
        });

        $("#btnExel").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BanVatTuBanTruyenThong/QUERYDATA_BangSoSanhGiaTriTonKhoSXKD?excel=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");
        });
        $("#btnword").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BanVatTuBanTruyenThong/QUERYDATA_BangSoSanhGiaTriTonKhoSXKD?word=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");

        });
    });
    function ThongKe() {
        $("#tungay").text($("#SearchTuNgay").val());
        $("#denngay").text($("#SearchDenNgay").val());
        $("#DonViText").val($("#MaDonVi option:selected").text());
        var data = $(".zone_search").siSerializeArray();
        loading();
        $.post("/VIPortalAPI/api/BanVatTuBanTruyenThong/QUERYDATA_BangSoSanhGiaTriTonKhoSXKD?", data, function (data) {

            $(".Title").text(data.Title);
            $(".TieuDeTrai1").text(data.TieuDeTrai1);
            $(".TieuDeTrai2").text(data.TieuDeTrai2);
            $(".TieuDeGiua1").text(data.TieuDeGiua1);
            $(".TieuDeGiua2").text(data.TieuDeGiua2);
            $(".TieuDeTrai3").text(data.TieuDeTrai3);
            $(".Nam").text(data.Nam);
            $(".Thang").text(data.Thang);


            var htmlTEmp = '';
            var templatehtmltr = kendo.template($("#javascriptTemplate").html());
            $(".mainContentBaoCao .clsContent").html("");
            if (data.data.length > 0) {
                $(".checkdatanull").hide();
                for (var i = 0; i < data.data.length; i++) {
                    data.data[i]["STT"] = i + 1;
                    htmlTEmp += templatehtmltr(data.data[i]);
                }

                $(".mainContentBaoCao .clsContent").append(htmlTEmp);                
            }
            $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');

            $('#tongbanghi').text("Có tổng số: " + data.data.length + " bản ghi");

            $(".clsvbd").css('cursor', 'pointer');

        });
        Endloading();
    }

</script>

</html>
