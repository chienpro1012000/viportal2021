﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BaoCaoSuDungXeTaiCau.aspx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.BanVatTuBanTruyenThong.BaoCaoSuDungXeTaiCau" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link type="text/css" href="/Content/dist/css/StyleBaoCao.css" rel="stylesheet" />
    <style type="text/css">
        .zone_search {
            overflow-x: hidden;
        }
    </style>
</head>
<body>
    <div id="BaoCaoSoDangKy" class="smgrid" data-title="Văn bản đến" data-grid="grid_BaoCaoSoDKVanBanDen"
        data-form="/iVBDH/VanBanDen/pFormVanBanDen.aspx" data-formwidth="1200" data-action="/Vbden/api/VanBanDen/VBDGetListJsonGroupSolr" data-view="/Vbden/VanBanDen/pViewVanBanDen.aspx">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />
                <input type="hidden" name="CurentUser" value="<%=CurentUser.Title %>" />
                <div class="title_icon">
                </div>
                <div class="form-group row text-search align-items-center">
                    <label class="col-sm-2 control-label">Tháng báo cáo:</label>
                    <div class="col-sm-3">
                        <select class="form-control" id="thang" name="thang">
                            <option value="01" <%=DateTime.Now.ToString("MM") == "01" ? "selected" : "" %>>Tháng 1</option>
                            <option value="02" <%=DateTime.Now.ToString("MM") == "02" ? "selected" : "" %>>Tháng 2</option>
                            <option value="03" <%=DateTime.Now.ToString("MM") == "03" ? "selected" : "" %>>Tháng 3</option>
                            <option value="04" <%=DateTime.Now.ToString("MM") == "04" ? "selected" : "" %>>Tháng 4</option>
                            <option value="05" <%=DateTime.Now.ToString("MM") == "05" ? "selected" : "" %>>Tháng 5</option>
                            <option value="06" <%=DateTime.Now.ToString("MM") == "06" ? "selected" : "" %>>Tháng 6</option>
                            <option value="07" <%=DateTime.Now.ToString("MM") == "07" ? "selected" : "" %>>Tháng 7</option>
                            <option value="08" <%=DateTime.Now.ToString("MM") == "08" ? "selected" : "" %>>Tháng 8</option>
                            <option value="09" <%=DateTime.Now.ToString("MM") == "09" ? "selected" : "" %>>Tháng 9</option>
                            <option value="10" <%=DateTime.Now.ToString("MM") == "10" ? "selected" : "" %>>Tháng 10</option>
                            <option value="11" <%=DateTime.Now.ToString("MM") == "11" ? "selected" : "" %>>Tháng 11</option>
                            <option value="12" <%=DateTime.Now.ToString("MM") == "12" ? "selected" : "" %>>Tháng 12</option>
                        </select>
                    </div>
                    <label class="col-sm-2 control-label">Năm báo cáo :</label>
                    <div class="col-sm-3">
                        <input type="text" name="Nam" id="Nam" class="form-control " placeholder="Nhập năm" value="<%=DateTime.Now.ToString("yyyy") %>" />
                    </div>
                </div>
                <div class="form-group row text-search align-items-center">
                    <input type="hidden" name="DonViText" id="DonViText" />
                    <label class="col-sm-2 control-label">Đơn vị báo cáo:</label>
                    <div class="col-sm-3">
                        <select class="form-control" id="MaDonVi" name="MaDonVi">
                            <option value="B01">Văn phòng Tổng  Công ty</option>
                            <option value="B02">Ban Kế hoạch	</option>
                            <option value="B03">Ban Tổ chức</option>
                            <option value="B04">Ban Kỹ thuật</option>
                            <option value="B05">Ban Tài chính kế toán</option>
                            <option value="B06">Ban Vật tư</option>
                            <option value="B07">Ban Bảo vệ quân sự</option>
                            <option value="B08">Ban Quản lý đầu tư xây dựng</option>
                            <option value="B09">Ban Kinh doanh Bán điện</option>
                            <option value="B10">Ban KTĐN và XNK</option>
                            <option value="B11">Ban KHCN & MT</option>
                            <option value="B12">Ban Bảo hộ Lao động</option>
                            <option value="B13">Ban Quản lý đấu thầu</option>
                            <option value="B14">Ban Kiểm tra và giám sát mua bán điện</option>
                            <option value="B15">Ban Truyền thông</option>
                            <option value="B16">Ban Kiểm tra và Thanh tra</option>
                            <option value="B17">Ban Quản lý tiếp nhận lưới điện nông thôn</option>
                            <option value="C01">Công ty Điện lực  Hoàn Kiếm</option>
                            <option value="C02">Công ty Điện lực  Hai Bà Trưng</option>
                            <option value="C03">Công ty Điện lực  Ba Đình</option>
                            <option value="C04">Công ty Điện lực  Đống Đa	</option>
                            <option value="C05">Công ty Điện lực Nam Từ Liêm	</option>
                            <option value="C06">Công ty Điện lực Thanh Trì	</option>
                            <option value="C07">Công ty Điện lực  Gia Lâm	</option>
                            <option value="C08">Công ty Điện lực  Đông Anh	</option>
                            <option value="C09">Công ty Điện lực  Sóc Sơn	</option>
                            <option value="C10">Công ty Điện lực  Tây Hồ	</option>
                            <option value="C11">Công ty Điện lực  Thanh Xuân	</option>
                            <option value="C12">Công ty Điện lực  Cầu Giấy	</option>
                            <option value="C13">Công ty Điện lực  Hoàng Mai	</option>
                            <option value="C14">Công ty Điện lực  Long Biên	</option>
                            <option value="C15">Công ty Điện lực Mê Linh	</option>
                            <option value="C16">Công ty Điện lực Hà Đông	</option>
                            <option value="C17">Công ty Điện lực Sơn Tây	</option>
                            <option value="C18">Công ty Điện lực Chương Mỹ	</option>
                            <option value="C19">Công ty Điện lực Thạch Thất	</option>
                            <option value="C20">Công ty Điện lực Thường Tín	</option>
                            <option value="C21">Công ty Điện lực Điện Ba Vì	</option>
                            <option value="C22">Công ty Điện lực Đan Phượng	</option>
                            <option value="C23">Công ty Điện lực  Hoài Đức	</option>
                            <option value="C24">Công ty Điện lực Mỹ Đức	</option>
                            <option value="C25">Công ty Điện lực  Phú Xuyên	</option>
                            <option value="C26">Công ty Điện lực Phúc Thọ	</option>
                            <option value="C27">Công ty Điện lực Quốc Oai	</option>
                            <option value="C28">Công ty Điện lực  Thanh Oai	</option>
                            <option value="C29">Công ty Điện lực  Ứng Hoà	</option>
                            <option value="C30">Công ty Điện lực Bắc Từ Liêm	</option>
                            <option value="HD">Hội đồng sáng kiến	</option>
                            <option value="P12">Ban Quản lý dự án lưới điện HN	</option>
                            <option value="VPCD ">Văn phòng Công đoàn	</option>
                            <option value="VPDTN">Văn phòng Đoàn Thanh Niên	</option>
                            <option value="VPDU ">Văn phòng Đảng Uỷ	</option>
                            <option value="VPKSV">Văn phòng Kiểm soát viên	</option>
                            <option value="X1">Công ty Công nghệ thông tin	</option>
                            <option value="X10">Ban quản lý dự án phát triển ĐLHN	</option>
                            <option value="X11">Ban quản lý xây dựng nhà điều hành	</option>
                            <option value="X2">Trung tâm Điều độ hệ thống điện TP.Hà Nội</option>
                            <option value="X3">Trung tâm chăm sóc khách hàng	</option>
                            <option value="X5">Công ty thí nghiệm điện ĐLHN	</option>
                            <option value="X6">Công ty lưới điện cao thế ĐLHN	</option>
                            <option value="X7">Công ty Cơ điện ĐLHN	</option>
                            <option value="X8">Công ty Dịch vụ Điện lực Hà Nội	</option>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-primary thongke">Thống kê</button>
                        <button type="button" class="btn btn-primary" id="btnExel">Excel</button>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="mainContentBaoCao">
            <div style="">
                <div style="width: 30%; float: left; text-align: center">
                    <p><b class="TieuDeTrai1"></b></p>
                    <p><b class="TieuDeTrai2"></b></p>

                </div>
                <div style="width: 55%; float: left; text-align: center">
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeGiua1"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeGiua2"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="Title"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b>Tháng <i class="Thang"></i></b>&nbsp;<b>Năm <i class="Nam"></i></b></p>
                </div>
                <div style="width: 15%; float: left; text-align: center">
                    <p style="font-size: 12pt; width: 100%">
                        <p class="LoaiBM"></p>
                    </p>
                </div>
            </div>
            <div class="smtable_header">
                <table class="smtable">
                    <thead>
                        <tr>
                            <th style="width: 10%;">Số thứ tự</th>
                            <th style="width: 20%">Thời gian</th>
                            <th style="width: 20%;">Mã lệnh</th>
                            <th style="width: 35%;">Nội dung công việc</th>
                            <th style="width: 20%;">BKS Xe</th>
                            <th style="width: 20%;">Số Tiền</th>
                            <th style="width: 35%;">Ghi chú</th>
                        </tr>
                        <tr>
                            <th>(1)</th>
                            <th>(2)</th>
                            <th>(3)</th>
                            <th>(4)</th>
                            <th>(5)</th>
                            <th>(6)</th>
                            <th>(7)</th>

                        </tr>
                    </thead>
                    <tbody class="clsContent">
                    </tbody>
                </table>
            </div>
            <div class="text-center mt-4 checkdatanull" style="width: 100%; float: left">
                <p style="font-size: 12pt; width: 100%">Chưa có dữ liệu </p>
            </div>
            <script id="javascriptTemplate" type="text/x-kendo-template">
            <tr class="clsvbd" data-id="">
                <td style="width: 10%">#=STT#</td>
                <td style="width: 20%">#=ThoiGian!=null?fomatdatetime(ThoiGian):""#</td>
                <td style="width: 20%">#=MaLenh#</td>
                <td style="width: 35%">#=NoiDungCongViec#</td>
                <td style="width: 20%">#=BKSXe#</td>
                <td style="width: 20%">#=SoTien!=null?SoTien:""#</td>
                <td style="width: 35%">#=GhiChu#</td>
                                                    
            </tr>
            </script>
            <div style="">

                <div style="width: 50%; float: left; text-align: left; margin-top: 15px;">
                    <p style="width: 100%">Nơi nhận: : </p>
                    <p style="font-size: 10pt; width: 100%">- Như trên; </p>
                    <p style="font-size: 10pt; width: 100%">- Lưu: VT </p>

                </div>
                <div style="width: 50%; float: right; text-align: center; margin-top: 15px;">
                    <p style="width: 100%"><b>TRƯỞNG ĐƠN VỊ </b></p>
                    <p style="font-size: 12pt; width: 100%"><i>(ký tên, đóng dấu)</i></p>
                    <br />
                    <br />
                    <br />
                    <br />
                    <p style="font-size: 12pt; width: 100%"><b class="NguoiKy"></b></p>
                </div>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        ThongKe();
        $(".thongke").click(function () {
            ThongKe();
        });
        $("#MaDonVi").smSelect2018V2({
            dropdownParent: "#baocao"
        });
        $(".input-datetime").daterangepicker({
            singleDatePicker: true,
            locale: { format: 'DD/MM/YYYY' }
        });

        $("#btnExel").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BanVatTuBanTruyenThong/QUERYDATA_BaoCaoVeViecSuDungXeTaiCau?excel=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");
        });
        $("#btnword").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BanVatTuBanTruyenThong/QUERYDATA_BaoCaoVeViecSuDungXeTaiCau?word=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");

        });
    });
    function ThongKe() {
        $("#tungay").text($("#SearchTuNgay").val());
        $("#denngay").text($("#SearchDenNgay").val());
        $("#DonViText").val($("#MaDonVi option:selected").text());
        var data = $(".zone_search").siSerializeArray();
        loading();
        $.post("/VIPortalAPI/api/BanVatTuBanTruyenThong/QUERYDATA_BaoCaoVeViecSuDungXeTaiCau?", data, function (data) {

            $(".Title").text(data.Title);
            $(".TieuDeTrai1").text(data.TieuDeTrai1);
            $(".TieuDeTrai2").text(data.TieuDeTrai2);
            $(".TieuDeTrai3").text(data.TieuDeTrai3);
            $(".TieuDeGiua1").text(data.TieuDeGiua1);
            $(".TieuDeGiua2").text(data.TieuDeGiua2);

            $(".Nam").text(data.Nam);
            $(".Thang").text(data.Thang);


            var htmlTEmp = '';
            var templatehtmltr = kendo.template($("#javascriptTemplate").html());
            $(".mainContentBaoCao .clsContent").html("");
            if (data.data.length > 0) {
                $(".checkdatanull").hide();
                for (var i = 0; i < data.data.length; i++) {
                    data.data[i]["STT"] = i + 1;
                    htmlTEmp += templatehtmltr(data.data[i]);
                }
                $(".mainContentBaoCao .clsContent").append(htmlTEmp);
            }
            $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');

            $('#tongbanghi').text("Có tổng số: " + data.data.length + " bản ghi");

            $(".clsvbd").css('cursor', 'pointer');
        });
        Endloading();
    }

</script>
<script>
    function fomatdatetime(dateStr) {
        var pattern = /(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/;

        var formatDateStr = dateStr.replace(pattern, '$1/$2/$3 $4:$5:$6');
        var formatDate = new Date(formatDateStr);
        return formatDate.format("dd/MM/yyyy");
    }
</script>
</html>
