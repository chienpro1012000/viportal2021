﻿<%@ page language="C#" autoeventwireup="true" codebehind="BaoCaoLDThuNhap.aspx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.BaoCaoLinhVucQuanTri.BaoCaoLDThuNhap" %>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link type="text/css" href="/Content/dist/css/StyleBaoCao.css" rel="stylesheet" />
    <style type="text/css">
        .zone_search {
          
        }
        /* .smtable th, .smtable td {
            border: 1px solid #96d1f3 !important;
            vertical-align: middle;
            padding: 5px 3px !important;
            font-size: 8px;
        }*/
    </style>
</head>
<body>
    <div id="BaoCaoSoDangKy" class="smgrid" data-title="Văn bản đến" data-grid="grid_BaoCaoSoDKVanBanDen"
        data-form="/iVBDH/VanBanDen/pFormVanBanDen.aspx" data-formwidth="1200" data-action="/Vbden/api/VanBanDen/VBDGetListJsonGroupSolr" data-view="/Vbden/VanBanDen/pViewVanBanDen.aspx">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />
                <input type="hidden" name="CurentUser" value="<%=CurentUser.Title %>" />
                <div class="title_icon">
                </div>
                <div class="form-group row text-search align-items-center">
                    <label class="col-sm-2 control-label">Loại báo cáo:</label>

                    <div class="col-sm-3">
                        <select class="form-control" id="LoaiBaoCao" name="LoaiBaoCao">
                            <option value="1">Báo cáo tháng</option>
                            <option value="2">Báo cáo quý</option>
                            <option value="3">Báo cáo 6 tháng</option>
                            <option value="4">Báo cáo năm</option>
                        </select>
                    </div>
                    <label class="col-sm-2 control-label">Kỳ báo cáo:</label>
                    <div class="col-sm-3">
                        <select class="form-control" id="KyBaoCao" name="KyBaoCao">
                            <option value="1">Tháng báo cáo</option>
                            <option value="2">Qúy báo cáo</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row text-search align-items-center">
                    <label class="col-sm-2 control-label">Năm báo cáo :</label>
                    <div class="col-sm-3">
                        <input type="text" name="BaoCaoNam" id="BaoCaoNam" class="form-control " placeholder="Đến ngày" value="<%=DateTime.Now.ToString("yyyy") %>" />
                    </div>
                    <label class="col-sm-2 control-label">Đơn Vị:</label>
                    <div class="col-sm-3">
                        <select class="form-control" id="LoaiDV" name="LoaiDV">
                           <option value="284">Công ty Điện lực Hoàn Kiếm</option>
                            <option value="285">Công ty Điện lực Hai Bà Trưng</option>
                            <option value="286">Công ty Điện lực Ba Đình</option>
                            <option value="287">Công ty Điện lực Đống Đa</option>
                            <option value="291">Công ty Điện lực Nam Từ Liêm</option>
                            <option value="295">Công ty Điện lực Thanh Trì</option>
                            <option value="292">Công ty Điện lực Gia Lâm</option>
                            <option value="293">Công ty Điện lực Đông Anh</option>
                            <option value="294">Công ty Điện lực Sóc Sơn</option>
                            <option value="288">Công ty Điện lực Tây Hồ</option>
                            <option value="290">Công ty Điện lực Thanh Xuân</option>
                            <option value="289">Công ty Điện lực Cầu Giấy</option>
                            <option value="296">Công ty Điện lực Hoàng Mai</option>
                            <option value="297">Công ty Điện lực Long Biên</option>
                            <option value="298">Công ty Điện lực Mê Linh</option>
                            <option value="299">Công ty Điện lực Hà Đông</option>
                            <option value="300">Công ty Điện lực Sơn Tây</option>
                            <option value="301">Công ty Điện lực Chương Mỹ</option>
                            <option value="302">Công ty Điện lực Thạch Thất</option>
                            <option value="303">Công ty Điện lực Thường Tín</option>
                            <option value="304">Công ty Điện lực  Ba Vì</option>
                            <option value="305">Công ty Điện lực Đan Phượng</option>
                            <option value="306">Công ty Điện lực Hoài Đức</option>
                            <option value="307">Công ty Điện lực Mỹ Đức</option>
                            <option value="308">Công ty Điện lực Phú Xuyên</option>
                            <option value="309">Công ty Điện lực Phúc Thọ</option>
                            <option value="310">Công ty Điện lực Quốc Oai</option>
                            <option value="311">Công ty Điện lực Thanh Oai</option>
                            <option value="312">Công ty Điện lực Ứng Hòa</option>
                            <option value="481">Công ty Điện lực Bắc Từ Liêm</option>
                            <option value="281">Công ty Công nghệ thông tin Điện lực Hà Nội</option>
                            <option value="280">Trung tâm điều độ Hệ thống điện TP Hà Nội</option>
                            <option value="488">Trung tâm chăm sóc khách hàng</option>
                            <option value="282">Công ty Thí nghiệm điện Điện lực Hà Nội</option>
                            <option value="318">Công ty lưới điện cao thế Thành phố Hà Nội</option>
                            <option value="313">Công ty Dịch vụ Điện lực Hà Nội</option>
                            <option value="277">Ban QLDA lưới điện Hà Nội</option>
                            <option value="477">BQLDA Phát triển điện lực Hà Nội</option>
                        </select>
                    </div>

                </div>
                <div class="form-group row text-search align-items-center">
                    <div class="col-sm-4 ">
                        <button type="button" class="btn btn-primary thongke">Thống kê</button>
                        <button type="button" class="btn btn-primary" id="btnExel">Excel</button>

                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="mainContentBaoCao">
            <div style="">
                <div style="width: 35%; float: left; text-align: center">
                    <p style="font-size: 10pt; width: 100%"><b class="TieuDeTrai1"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeTrai2"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="">—————————</b></p>
                </div>
                <div style="width: 50%; float: left; text-align: center">
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeGiua1"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeGiua2"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="">—————————</b></p>

                </div>
                <div style="width: 15%; float: left; text-align: center">
                    <p style="font-size: 12pt; width: 100%">
                        <p class="LoaiBM"></p>
                    </p>
                </div>
                <div>
                    <div style="width: 100%; float: left">
                        <p style="font-size: 14pt; width: 100%; text-align: center"><b class="Title"></b></p>
                        <p style="font-size: 12pt; width: 100%; text-align: center"><b>Năm <i class="Nam"></i></b></p>
                        <p style="font-size: 12pt; width: 100%; text-align: center" class="GiTruTieuDe"></p>
                        <p style="font-size: 12pt; width: 100%; text-align: end" class="SoBM"></p>
                    </div>
                </div>

            </div>
            <div class="smtable_header" style="float: left; overflow-x:auto;">
                <div>
                    <table class="smtable" style="width:200%">
                        <thead>
                            <tr>
                                <th style="width: 1.5%" rowspan="4">STT</th>
                                <th style="width: 8%" rowspan="4">Ngành nghề kinh tế </th>
                                <th style="width: 2%;" rowspan="4">Mã số</th>
                                <th style="width: 24%;" colspan="12">Lao Động(Người)</th>
                                <th style="width: 18%;" colspan="6">Thu nhập của người lao động (1.000 Đồng)</th>
                                <th style="width: 3%;" rowspan="4">ĐÓNG GÓP CỦA ĐƠN VỊ về BHXH, BHYT, BHTN, KPCĐ (đvt: 1000đ)</th>
                            </tr>
                            <tr>
                                <th colspan="4">Số LĐ đầu kỳ</th>
                                <th colspan="4">Số LĐ cuối kỳ</th>
                                <th rowspan="3">LĐ tăng trong kỳ</th>
                                <th rowspan="3">LĐ giảm trong kỳ</th>
                                <th rowspan="3">LĐ không có nhu cầu sử dụng</th>
                                <th rowspan="3">Số LĐ bình quân trong kỳ báo cáo</th>
                                <th rowspan="3">Tổng thu nhập</th>
                                <th colspan="3">Trong đó:</th>
                                <th rowspan="3">Thu nhập bình quân</th>
                                <th rowspan="3">Tiền lương bình quân</th>

                            </tr>
                            <tr>
                                <th rowspan="2">Tổng</th>
                                <th colspan="3">Trong đó </th>
                                <th rowspan="2">Tổng </th>
                                <th colspan="3">Trong đó</th>
                                <th rowspan="2">Tiền lương và các khoản có tính chất lương </th>
                                <th rowspan="2">BHXH trả thay lương</th>
                                <th rowspan="2">Các khoản thu nhập khác không tính vào chi phí SXKD</th>
                            </tr>
                            <tr>
                                <th>LĐ nữ</th>
                                <th>LĐ được đóng BHXH</th>
                                <th>LĐHĐ dưới 6 tháng </th>
                                <th>LĐ nữ</th>
                                <th>LĐ được đóng BHXH</th>
                                <th>LĐHĐ dưới 6 tháng </th>

                            </tr>
                        </thead>
                        <tbody class="clsContent">
                        </tbody>
                    </table>
                </div>
                <div class="checkdata mt-3 w-100" style="text-align: center">
                    <p style="font-size: 12pt;">Không có bản ghi</p>
                </div>
            </div>

            <script id="javascriptTemplate" type="text/x-kendo-template">
                #if(STT!="11"){#
            <tr class="clsvbd styletr#=Style#" data-id="">
                 <td style="width: 1.5%; text-align:center;" >#=NotHasSTT != 1 ? STT : ""#</td>
                <td style="width: 8%; word-wrap: break-word;" >#=NganhNgheKT !=null ? NganhNgheKT : ""#</td>
                <td style="width: 2%;text-align:center;" >#=MaSo !=null ?MaSo : ""#</td>
                <td style="width: 2%;word-wrap: break-word;text-align:right;" >#=TongSoLDDauKy  !=null ? formatMoney(TongSoLDDauKy): ""#</td>
                <td style="width: 2%;word-wrap: break-word; text-align: right;" >#=LDNuDauKy  !=null ? formatMoney(LDNuDauKy) : ""#</td>
                <td style="width: 2%;word-wrap: break-word; text-align: right;" >#=LDDongBHXKDauKy   !=null ? formatMoney(LDDongBHXKDauKy): ""#</td>
                <td style="width: 2%;word-wrap: break-word; text-align: right;" >#=LDHDDuoi6ThangDauKy  !=null ?formatMoney(LDHDDuoi6ThangDauKy) : ""#</td>
                <td style="width: 2%;word-wrap: break-word; text-align: right;" >#=TongSoLDCuoiKy !=null ?formatMoney(TongSoLDCuoiKy) : ""#</td>
                <td style="width: 2%;word-wrap: break-word; text-align: right;" >#=LDNuCuoiKy !=null ? formatMoney(LDNuCuoiKy): ""#</td>
                <td style="width: 2%;word-wrap: break-word; text-align: right;" >#=LDDongBHXKCuoiKy !=null ? formatMoney(LDDongBHXKCuoiKy): ""#</td>
                <td style="width: 2%;word-wrap: break-word; text-align: right;" >#=LDHDDuoi6ThangCuoiKy !=null ?formatMoney(LDHDDuoi6ThangCuoiKy) : ""#</td>
                <td style="width: 2%;word-wrap: break-word; text-align: right;" >#=LDTangTrongKy !=null ? formatMoney(LDTangTrongKy): ""#</td>
                <td style="width: 2%;word-wrap: break-word; text-align: right;" >#=LDGiamTrongKy !=null ? formatMoney(LDGiamTrongKy) : ""#</td>
                <td style="width: 2%;word-wrap: break-word; text-align: right;" >#=LDKhongCoNhuCauSudung  !=null ? formatMoney(LDKhongCoNhuCauSudung) : ""#</td>
                <td style="width: 2%;word-wrap: break-word; text-align: right;" >#=SoLDBinhQuanTrongKiBC !=null ? formatMoney(SoLDBinhQuanTrongKiBC): ""#</td>
                <td style="width: 3%;word-wrap: break-word; text-align: right;" >#=TongThuNhap !=null ?formatMoney(TongThuNhap) : ""#</td>
                <td style="width: 3%;word-wrap: break-word; text-align: right;" >#=TienLuongVaCacKhoanTCL !=null ?formatMoney(TienLuongVaCacKhoanTCL) : ""#</td>
                <td style="width: 3%;word-wrap: break-word; text-align: right;" >#=BHXTraThayLuong  !=null ? formatMoney(BHXTraThayLuong): ""#</td>
                <td style="width: 3%;word-wrap: break-word; text-align: right;" >#=CacThuNhapKhacKTinhVaoCPSXKD !=null ? formatMoney(CacThuNhapKhacKTinhVaoCPSXKD): ""#</td>
                <td style="width: 3%;word-wrap: break-word; text-align: right;" >#=ThuNhapBinhQuan !=null ? formatMoney(ThuNhapBinhQuan): ""#</td>
                <td style="width: 3%;word-wrap: break-word; text-align: right;" >#=TienLuongBinhQuan !=null ? formatMoney(TienLuongBinhQuan): ""#</td>
                <td style="width: 3%;word-wrap: break-word;" >#=DongGopCuaDVveBHXHYTTNKPCD !=null ? DongGopCuaDVveBHXHYTTNKPCD: ""#</td>
            </tr>#}#
            </script>


        </div>
    </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        ThongKe();
        $(".thongke").click(function () {
            ThongKe();
        });
        $(".input-datetime").daterangepicker({
            singleDatePicker: true,
            locale: { format: 'DD/MM/YYYY' }
        });

        $("#btnExel").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BCLinhVucQuanTri/QUERYDATA_BaoCaoLDThuNhap?excel=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");
        });
        $("#btnword").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BCLinhVucQuanTri/QUERYDATA_BaoCaoLDThuNhap?word=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");

        });
    });
    function ThongKe() {
        $("#tungay").text($("#SearchTuNgay").val());
        $("#denngay").text($("#SearchDenNgay").val());
        loading();
        var data = $(".zone_search").siSerializeArray();
        $.post("/VIPortalAPI/api/BCLinhVucQuanTri/QUERYDATA_BaoCaoLDThuNhap?", data, function (data) {

            $(".Title").text(data.Title);
            $(".LoaiBM").text(data.LoaiBM);
            $(".TieuDeTrai1").text(data.TieuDeTrai1);
            $(".TieuDeTrai2").text(data.TieuDeTrai2);
            $(".Nam").text(data.Nam);
            $(".Thang").text(data.Thang);
            $(".GiTruTieuDe").text(data.GiTruTieuDe);
            $(".TieuDeGiua1").text(data.TieuDeGiua1);
            $(".TieuDeGiua2").text(data.TieuDeGiua2);
            $(".TieuDeGiua3").text(data.TieuDeGiua3);
            $(".SoBM").text(data.SoBM);
            var htmlTEmp = '';
            var templatehtmltr = kendo.template($("#javascriptTemplate").html());
            $(".mainContentBaoCao .clsContent").html("");
            if (data.data.length > 0) {
                $(".checkdata").hide();
                for (var i = 0; i < data.data.length; i++) {
                    data.data[i]["STT"] = i + 1;
                    htmlTEmp += templatehtmltr(data.data[i]);
                }

                $(".mainContentBaoCao .clsContent").append(htmlTEmp);
                $('#tongbanghi').text("Có tổng số: " + data.data.length + " bản ghi");

            } else {
                $(".checkdata").show();
            }
            $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');


            $(".clsvbd").css('cursor', 'pointer');
             Endloading();
        });
      
    }

</script>
</html>

