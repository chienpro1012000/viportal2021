﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BCCoCauLaoDong.aspx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.BaoCaoLinhVucQuanTri.BCCoCauLaoDong" %>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link type="text/css" href="/Content/dist/css/StyleBaoCao.css" rel="stylesheet" />
    <style type="text/css">
        .zone_search {
            overflow-x: hidden;
        }
       /* .smtable th, .smtable td {
            border: 1px solid #96d1f3 !important;
            vertical-align: middle;
            padding: 5px 3px !important;
            font-size: 8px;
        }*/
    </style>
</head>
<body>
    <div id="BaoCaoSoDangKy" class="smgrid" data-title="Văn bản đến" data-grid="grid_BaoCaoSoDKVanBanDen"
        data-form="/iVBDH/VanBanDen/pFormVanBanDen.aspx" data-formwidth="1200" data-action="/Vbden/api/VanBanDen/VBDGetListJsonGroupSolr" data-view="/Vbden/VanBanDen/pViewVanBanDen.aspx">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />
                <input type="hidden" name="CurentUser" value="<%=CurentUser.Title %>" />
                <div class="title_icon">
                </div>
                 <div class="form-group row text-search align-items-center">
                    <label class="col-sm-2 control-label">Loại báo cáo:</label>

                    <div class="col-sm-3">
                     <select class="form-control" id="LoaiBaoCao" name="LoaiBaoCao">
                        <option value="1">Báo cáo tháng</option>
                        <option value="2">Báo cáo quý</option>
                        <option value="3">Báo cáo 6 tháng</option>
                        <option value="4">Báo cáo năm</option>
                      </select>
                    </div>
                    <label class="col-sm-2 control-label">Kỳ báo cáo:</label>
                    <div class="col-sm-3">
                       <select class="form-control" id="KyBaoCao" name="KyBaoCao">
                        <option value="1">Tháng báo cáo</option>
                        <option value="2">Qúy báo cáo</option>
                      </select>
                    </div> 
                </div> 
                <div class="form-group row text-search align-items-center">
                   <label class="col-sm-2 control-label">Năm báo cáo :</label>
                    <div class="col-sm-3">
                        <input type="text" name="BaoCaoNam" id="BaoCaoNam" class="form-control " placeholder="Đến ngày" value="<%=DateTime.Now.ToString("yyyy") %>" />
                    </div>
                      <label class="col-sm-2 control-label">Đơn Vị:</label>
                    <div class="col-sm-3">
                        <select class="form-control" id="LoaiDV" name="LoaiDV">
                            <option value="284">Công ty Điện lực Hoàn Kiếm</option>
                            <option value="285">Công ty Điện lực Hai Bà Trưng</option>
                            <option value="286">Công ty Điện lực Ba Đình</option>
                            <option value="287">Công ty Điện lực Đống Đa</option>
                            <option value="291">Công ty Điện lực Nam Từ Liêm</option>
                            <option value="295">Công ty Điện lực Thanh Trì</option>
                            <option value="292">Công ty Điện lực Gia Lâm</option>
                            <option value="293">Công ty Điện lực Đông Anh</option>
                            <option value="294">Công ty Điện lực Sóc Sơn</option>
                            <option value="288">Công ty Điện lực Tây Hồ</option>
                            <option value="290">Công ty Điện lực Thanh Xuân</option>
                            <option value="289">Công ty Điện lực Cầu Giấy</option>
                            <option value="296">Công ty Điện lực Hoàng Mai</option>
                            <option value="297">Công ty Điện lực Long Biên</option>
                            <option value="298">Công ty Điện lực Mê Linh</option>
                            <option value="299">Công ty Điện lực Hà Đông</option>
                            <option value="300">Công ty Điện lực Sơn Tây</option>
                            <option value="301">Công ty Điện lực Chương Mỹ</option>
                            <option value="302">Công ty Điện lực Thạch Thất</option>
                            <option value="303">Công ty Điện lực Thường Tín</option>
                            <option value="304">Công ty Điện lực  Ba Vì</option>
                            <option value="305">Công ty Điện lực Đan Phượng</option>
                            <option value="306">Công ty Điện lực Hoài Đức</option>
                            <option value="307">Công ty Điện lực Mỹ Đức</option>
                            <option value="308">Công ty Điện lực Phú Xuyên</option>
                            <option value="309">Công ty Điện lực Phúc Thọ</option>
                            <option value="310">Công ty Điện lực Quốc Oai</option>
                            <option value="311">Công ty Điện lực Thanh Oai</option>
                            <option value="312">Công ty Điện lực Ứng Hòa</option>
                            <option value="481">Công ty Điện lực Bắc Từ Liêm</option>
                            <option value="281">Công ty Công nghệ thông tin Điện lực Hà Nội</option>
                            <option value="280">Trung tâm điều độ Hệ thống điện TP Hà Nội</option>
                            <option value="488">Trung tâm chăm sóc khách hàng</option>
                            <option value="282">Công ty Thí nghiệm điện Điện lực Hà Nội</option>
                            <option value="318">Công ty lưới điện cao thế Thành phố Hà Nội</option>
                            <option value="313">Công ty Dịch vụ Điện lực Hà Nội</option>
                            <option value="277">Ban QLDA lưới điện Hà Nội</option>
                            <option value="477">BQLDA Phát triển điện lực Hà Nội</option>
                        </select>
                    </div>  
                    
                </div>
                <div class="form-group row text-search align-items-center">
                     <div class="col-sm-4 ">
                         <button type="button" class="btn btn-primary thongke">Thống kê</button>
                        <button type="button" class="btn btn-primary" id="btnExel">Excel</button>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="mainContentBaoCao">
            <div style="">
                <div style="width: 35%; float: left; text-align:center">
                      <p style="font-size: 10pt; width: 100%"><b class="TieuDeTrai1"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeTrai2"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="">—————————</b></p>
                </div>
                <div style="width: 50%; float: left; text-align:center">
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeGiua1"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeGiua2"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="">—————————</b></p>
                   
                </div>
                <div style="width: 15%; float: left; text-align:center">
                      <p style="font-size: 12pt; width: 100%"><p class="LoaiBM"></p></p>
                </div>
                <div>
                    <div style="width:100%  ;float:left" >
                        <p style="font-size: 14pt; width: 100%; text-align: center"><b class="Title"></b></p>
                         <p style="font-size: 12pt; width: 100% ;text-align: center"><b>   Năm <i class="Nam"></i></b></p>
                         <p style="font-size: 12pt; width: 100%; text-align:end" class="SoBM"></p>
                    </div>
                </div>

            </div>
            <div class="smtable_header" style="float:left;overflow-x:scroll">
                <table class="smtable" style="width:200%">
                    <thead>
                        <tr>
                            <th style="width: 1.5%" rowspan="3">STT</th>
                            <th style="width: 8%"rowspan="3">Chức danh </th>
                            <th style="width: 3%;" rowspan="3">Mã số</th>
                            <th style="width: 3%;" rowspan="3">Tổng số lao động có mặt đến cuối kỳ báo cáo</th>                             
                            <th style="width: 3%;" rowspan="3">Đảng viên</th>
                            <th style="width: 3%;" rowspan="3">LĐ nữ</th>
                            <th style="width: 3%;" rowspan="3">LĐ là người nước ngoài</th>
                            <th style="width: 3%;" rowspan="3">Dân tộc ít người</th>
                            <th style="width: 15%;" colspan="5">Tuổi đời</th>
                            <th style="width: 30%;" colspan="13">Trình độ đào tạo</th>
                            <th style="width: 15%;" colspan="4">Trình độ lý luận chính trị</th>
                        </tr>
                        <tr>                              
                            <th rowspan="2">Dưới 30</th>  
                            <th rowspan="2">30-39</th>  
                            <th rowspan="2">40-49</th>  
                            <th rowspan="2">50-59</th>  
                            <th rowspan="2">Trên 60</th>  
                            <th colspan="2">Tiến sĩ</th>  
                            <th colspan="2">Thạc sĩ</th>  
                            <th colspan="2">Đại học</th>  
                            <th colspan="2">Cao đẳng</th>  
                            <th colspan="2">Trung cấp</th>  
                            <th rowspan="2">Công nhân kỹ thuật</th>  
                            <th rowspan="2">Đào tạo  nghề ngắn hạn (có chứng chỉ nghề)</th>  
                            <th rowspan="2">Chưa qua đào tạo</th>  
                            <th rowspan="2">Trên đại học</th>  
                            <th rowspan="2">Cử nhân</th>  
                            <th rowspan="2">Cao cấp</th>  
                            <th rowspan="2">Trung cấp</th>  
                          
                        </tr>
                        <tr>                              
                            <th>Kỹ thuật</th>  
                            <th>Kinh tế - Xã hội</th>  
                            <th>Kỹ thuật</th>  
                            <th>Kinh tế - Xã hội</th>  
                            <th>Kỹ thuật</th>  
                            <th>Kinh tế - Xã hội</th>  
                            <th>Kỹ thuật</th>  
                            <th>Kinh tế - Xã hội</th>
                            <th>Kỹ thuật</th>  
                            <th>Kinh tế - Xã hội</th>  
                            
                        </tr>
                        <tr>
                            <td style="text-align:center;"><b>A</b></td>
                            <td style="text-align:center;"><b>B</b></td>
                            <td style="text-align:left;"><b>C</b></td>
                            <td style="text-align:center;">1</td>
                            <td style="text-align:center;">2</td>
                            <td style="text-align:center;">3</td>
                            <td style="text-align:center;">4</td>
                            <td style="text-align:center;">5</td>
                            <td style="text-align:center;">6</td>
                            <td style="text-align:center;">7</td>
                            <td style="text-align:center;">8</td>
                            <td style="text-align:center;">9</td>
                            <td style="text-align:center;">10</td>
                            <td style="text-align:center;">11</td>
                            <td style="text-align:center;">12</td>
                            <td style="text-align:center;">13</td>
                            <td style="text-align:center;">14</td>
                            <td style="text-align:center;">15</td>
                            <td style="text-align:center;">16</td>
                            <td style="text-align:center;">17</td>
                            <td style="text-align:center;">18</td>
                            <td style="text-align:center;">19</td>
                            <td style="text-align:center;">20</td>
                            <td style="text-align:center;">21</td>
                            <td style="text-align:center;">22</td>
                            <td style="text-align:center;">23</td>
                            <td style="text-align:center;">24</td>
                            <td style="text-align:center;">25</td>
                            <td style="text-align:center;">26</td>
                            <td style="text-align:center;">27</td>
                        </tr>
                    </thead>
                    <tbody class="clsContent">
                       
                    </tbody>
                </table>
            </div>
            <script id="javascriptTemplate" type="text/x-kendo-template">                  
            <tr class="clsvbd styletr#=Style#" data-id="">
                <td style="width: 1.5%; word-wrap: break-word;text-align:center;" >#=NotHasSTT !=1? STT :""#</td>
                <td style="width: 8%;word-wrap: break-word;" >#=ChucDanh !=null? ChucDanh :""#</td>
                <td style="width: 3%;word-wrap: break-word;text-align:center;" >#=MaSo !=null? MaSo :""#</td>
                <td style="width: 3%;word-wrap: break-word;text-align:right;" >#=TongSoLDCMDenCuoiKiBC !=null? formatMoney(TongSoLDCMDenCuoiKiBC) :""#</td>
                <td style="width: 3%;word-wrap: break-word;text-align:right;" >#=DangVien !=null? formatMoney(DangVien) :""#</td>
                <td style="width: 3%;word-wrap: break-word;text-align:right;" >#=LDNu !=null? formatMoney(LDNu) :""#</td>
                <td style="width: 3%;word-wrap: break-word;text-align:right;" >#=LDNNuocNgoai !=null?  formatMoney(LDNNuocNgoai) :""#</td>
                <td style="width: 3%;word-wrap: break-word;text-align:right;" >#=DanTocItNguoi !=null? formatMoney(DanTocItNguoi) :""#</td>
                <td style="width: 3%;word-wrap: break-word;text-align:right;" >#=TDuoi30 !=null? formatMoney(TDuoi30) :""#</td>
                <td style="width: 3%;word-wrap: break-word;text-align:right;" >#=Tuoi30D39!=null? formatMoney(Tuoi30D39) :""#</td>
                <td style="width: 3%;word-wrap: break-word;text-align:right;" >#=Tuoi40D49!=null? formatMoney(Tuoi40D49) :""#</td>
                <td style="width: 3%;word-wrap: break-word;text-align:right;" >#=Tuoi50D59!=null? formatMoney(Tuoi50D59) :""#</td>
                <td style="width: 3%;word-wrap: break-word;text-align:right;" >#=TuoiTren60!=null? formatMoney(TuoiTren60) :""#</td>
                <td style="width: 2.3%;word-wrap: break-word;text-align:right;" >#=TienSiKT!=null? formatMoney(TienSiKT) :""#</td>
                <td style="width: 2.3%;word-wrap: break-word;text-align:right;" >#=TienSiKTXH!=null? formatMoney(TienSiKTXH) :""#</td>
                <td style="width: 2.3%;word-wrap: break-word;text-align:right;" >#=ThacSiKT!=null? formatMoney(ThacSiKT) :""#</td>
                <td style="width: 2.3%;word-wrap: break-word;text-align:right;" >#=ThacSiKTXH!=null? formatMoney(ThacSiKTXH) :""#</td>
                <td style="width: 2.3%;word-wrap: break-word;text-align:right;" >#=DaiHocKT!=null? formatMoney(DaiHocKT) :""#</td>                            
                <td style="width: 2.3%;word-wrap: break-word;text-align:right;" >#=DaiHocKTXH!=null? formatMoney(DaiHocKTXH) :""#</td>
                <td style="width: 2.3%;word-wrap: break-word;text-align:right;" >#=CaoDangKT!=null? formatMoney(CaoDangKT) :""#</td>
                <td style="width: 2.3%;word-wrap: break-word;text-align:right;" >#=CaoDangKTXH!=null? formatMoney(CaoDangKTXH) :""#</td>
                <td style="width: 2.3%;word-wrap: break-word;text-align:right;" >#=TrungCapKT!=null? formatMoney(TrungCapKT) :""#</td>
                <td style="width: 2.3%;word-wrap: break-word;text-align:right;" >#=TrungCapKTXH!=null? formatMoney(TrungCapKTXH) :""#</td>
                <td style="width: 2.3%;word-wrap: break-word;text-align:right;" >#=CongNhanKT!=null? formatMoney(CongNhanKT) :""#</td>
                <td style="width: 2.3%;word-wrap: break-word;text-align:right;" >#=DaoTaoNgheNganHan!=null? formatMoney(DaoTaoNgheNganHan) :""#</td>
                <td style="width: 2.3%;word-wrap: break-word;text-align:right;" >#=ChuaQuaDaoTao!=null? formatMoney(ChuaQuaDaoTao) :""#</td>
                <td style="width: 3.75%;word-wrap: break-word;text-align:right;" >#=TrenDaiHoc!=null? formatMoney(TrenDaiHoc) :""#</td>
                <td style="width: 3.75%;word-wrap: break-word;text-align:right;" >#=CuNhan!=null? formatMoney(CuNhan) :""#</td>
                <td style="width: 3.75%;word-wrap: break-word;text-align:right;" >#=CaoCap!=null? formatMoney(CaoCap) :""#</td>
                <td style="width: 3.75%;word-wrap: break-word;text-align:right;" >#=TrungCap!=null? formatMoney(TrungCap) :""#</td>
             
               
               
            </tr>
            </script>
           
           
        </div>
    </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        ThongKe();
        $(".thongke").click(function () {
            ThongKe();
        });
        $(".input-datetime").daterangepicker({
            singleDatePicker: true,
            locale: { format: 'DD/MM/YYYY' }
        });

        $("#btnExel").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BCLinhVucQuanTri/QUERYDATA_BCCoCauLaoDong?excel=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");
        });
        $("#btnword").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BCLinhVucQuanTri/QUERYDATA_BCCoCauLaoDong?word=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");

        });
    });
    function ThongKe() {
        $("#tungay").text($("#SearchTuNgay").val());
        $("#denngay").text($("#SearchDenNgay").val());
        loading();
        var data = $(".zone_search").siSerializeArray();
        $.post("/VIPortalAPI/api/BCLinhVucQuanTri/QUERYDATA_BCCoCauLaoDong?", data, function (data) {

            $(".Title").text(data.Title);
            $(".LoaiBM").text(data.LoaiBM);
            $(".TieuDeTrai1").text(data.TieuDeTrai1);
            $(".TieuDeTrai2").text(data.TieuDeTrai2);
            $(".Nam").text(data.Nam);
            $(".TieuDeGiua1").text(data.TieuDeGiua1);
            $(".TieuDeGiua2").text(data.TieuDeGiua2);
            $(".SoBM").text(data.SoBM);

           



            var htmlTEmp = '';
            var templatehtmltr = kendo.template($("#javascriptTemplate").html());
            $(".mainContentBaoCao .clsContent").html("");
            if (data.data.length > 0) {
                for (var i = 0; i < data.data.length; i++) {
                    data.data[i]["Count"] = i + 1;
                    htmlTEmp += templatehtmltr(data.data[i]);
                }

                $(".mainContentBaoCao .clsContent").append(htmlTEmp);                
            }
            $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');

            $('#tongbanghi').text("Có tổng số: " + data.data.length + " bản ghi");

            $(".clsvbd").css('cursor', 'pointer');
            Endloading();
        });
       
    }

</script>
</html>
