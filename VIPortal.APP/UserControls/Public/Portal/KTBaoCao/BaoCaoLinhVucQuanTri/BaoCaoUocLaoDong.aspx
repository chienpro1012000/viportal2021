﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BaoCaoTrinhDoVienChucCMNV.aspx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.BaoCaoLinhVucQuanTri.BaoCaoTrinhDoVienChucCMNV" %>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link type="text/css" href="/Content/dist/css/StyleBaoCao.css" rel="stylesheet" />
    <style type="text/css">
        .zone_search {
            overflow-x: hidden;
        }
        .clsContent .clsvbd:nth-child(3) td:first-child {
            padding-left: 58px !important;
        }
    </style>
</head>
<body>
    <div id="BaoCaoSoDangKy" class="smgrid" data-title="Văn bản đến" data-grid="grid_BaoCaoSoDKVanBanDen"
        data-form="/iVBDH/VanBanDen/pFormVanBanDen.aspx" data-formwidth="1200" data-action="/Vbden/api/VanBanDen/VBDGetListJsonGroupSolr" data-view="/Vbden/VanBanDen/pViewVanBanDen.aspx">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />
                <input type="hidden" name="CurentUser" value="<%=CurentUser.Title %>" />
                <div class="title_icon">
                </div>
                      <div class="form-group row text-search align-items-center">
                    <label class="col-sm-2 control-label">Loại báo cáo:</label>

                    <div class="col-sm-3">
                     <select class="form-control" id="LoaiBaoCao" name="LoaiBaoCao">
                        <option value="1">Báo cáo tháng</option>
                        <option value="2">Báo cáo quý</option>
                        <option value="3">Báo cáo 6 tháng</option>
                        <option value="4">Báo cáo năm</option>
                      </select>
                    </div>
                    <label class="col-sm-2 control-label">Kỳ báo cáo:</label>
                    <div class="col-sm-3">
                       <select class="form-control" id="KyBaoCao" name="KyBaoCao">
                        <option value="1">Tháng báo cáo</option>
                        <option value="2">Qúy báo cáo</option>
                      </select>
                    </div> 
                </div> 
                <div class="form-group row text-search align-items-center">
                   <label class="col-sm-2 control-label">Năm báo cáo :</label>
                    <div class="col-sm-3">
                        <input type="text" name="BaoCaoNam" id="BaoCaoNam" class="form-control " placeholder="Đến ngày" value="<%=DateTime.Now.ToString("yyyy") %>" />
                    </div>
                    <label class="col-sm-2 control-label">Đơn Vị:</label>
                    <div class="col-sm-3">
                        <select class="form-control" id="maDonvi" name="maDonvi">
                            <option value="284">Công ty Điện lực Hoàn Kiếm</option>
                            <option value="285">Công ty Điện lực Hai Bà Trưng</option>
                            <option value="286">Công ty Điện lực Ba Đình</option>
                            <option value="287">Công ty Điện lực Đống Đa</option>
                            <option value="291">Công ty Điện lực Nam Từ Liêm</option>
                            <option value="295">Công ty Điện lực Thanh Trì</option>
                            <option value="292">Công ty Điện lực Gia Lâm</option>
                            <option value="293">Công ty Điện lực Đông Anh</option>
                            <option value="294">Công ty Điện lực Sóc Sơn</option>
                            <option value="288">Công ty Điện lực Tây Hồ</option>
                            <option value="290">Công ty Điện lực Thanh Xuân</option>
                            <option value="289">Công ty Điện lực Cầu Giấy</option>
                            <option value="296">Công ty Điện lực Hoàng Mai</option>
                            <option value="297">Công ty Điện lực Long Biên</option>
                            <option value="298">Công ty Điện lực Mê Linh</option>
                            <option value="299">Công ty Điện lực Hà Đông</option>
                            <option value="300">Công ty Điện lực Sơn Tây</option>
                            <option value="301">Công ty Điện lực Chương Mỹ</option>
                            <option value="302">Công ty Điện lực Thạch Thất</option>
                            <option value="303">Công ty Điện lực Thường Tín</option>
                            <option value="304">Công ty Điện lực  Ba Vì</option>
                            <option value="305">Công ty Điện lực Đan Phượng</option>
                            <option value="306">Công ty Điện lực Hoài Đức</option>
                            <option value="307">Công ty Điện lực Mỹ Đức</option>
                            <option value="308">Công ty Điện lực Phú Xuyên</option>
                            <option value="309">Công ty Điện lực Phúc Thọ</option>
                            <option value="310">Công ty Điện lực Quốc Oai</option>
                            <option value="311">Công ty Điện lực Thanh Oai</option>
                            <option value="312">Công ty Điện lực Ứng Hòa</option>
                            <option value="481">Công ty Điện lực Bắc Từ Liêm</option>
                            <option value="281">Công ty Công nghệ thông tin Điện lực Hà Nội</option>
                            <option value="280">Trung tâm điều độ Hệ thống điện TP Hà Nội</option>
                            <option value="488">Trung tâm chăm sóc khách hàng</option>
                            <option value="282">Công ty Thí nghiệm điện Điện lực Hà Nội</option>
                            <option value="318">Công ty lưới điện cao thế Thành phố Hà Nội</option>
                            <option value="313">Công ty Dịch vụ Điện lực Hà Nội</option>
                            <option value="277">Ban QLDA lưới điện Hà Nội</option>
                            <option value="477">BQLDA Phát triển điện lực Hà Nội</option>
                        </select>          
                    </div>
                </div>
             
                <div class="form-group row text-search align-items-center">
                <div class="col-sm-4 ">
                         <button type="button" class="btn btn-primary thongke">Thống kê</button>
                        <button type="button" class="btn btn-primary" id="btnExel">Excel</button>
                       
                    </div>
                 </div>
            </div>
        </div>
        <hr />
        <div class="mainContentBaoCao">
            <div style="">
                <div style="width: 35%; float: left; text-align:center">
                      <p style="font-size: 10pt; width: 100%"><b class="TieuDeTrai1"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeTrai2"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="">—————————</b></p>
                </div>
                <div style="width: 50%; float: left; text-align:center">
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeGiua1"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeGiua2"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="">—————————</b></p>
                   
                </div>
                <div style="width: 15%; float: left; text-align:center">
                      <p style="font-size: 12pt; width: 100%"><p class="LoaiBM"></p></p>
                </div>
                <div>
                    <div style="width:100%  ;float:left" >
                        <p style="font-size: 14pt; width: 100%; text-align: center"><b class="Title"></b></p>
                         <p style="font-size: 12pt; width: 100% ;text-align: center"> <b>  Năm <i class="Nam"></i></b></p>
                         <p style="font-size: 12pt; width: 100%; text-align:end" class="SoBM"></p>
                    </div>
                </div>

            </div>
            <div >
                <p style="font-size: 12pt;float:left; width: 40% ;text-align: left"> Tên doanh nghiệp:</p>
               
                <p style="font-size: 12pt;float:left; width: 60% ;text-align: left"> Địa chỉ:</p>
              
                <p style="font-size: 12pt;float:left; width: 40% ;text-align: left"> Điện thoại:</p>
                
                <p style="font-size: 12pt;float:left; width: 60% ;text-align: left"> Email:</p>
                
                <p style="font-size: 12pt;float:left; width: 40% ;text-align: left"> Ngành sản xuất kinh doanh chính:</p>
               
                <p style="font-size: 12pt;float:left; width: 60% ;text-align: left"> Loại hình kinh tế doanh nghiệp:</p>
               
                <b style="font-size: 13pt; float:left;width: 100% ;text-align: left">1. Lao động</b>
            </div>
            <div style="width:100%; float:left">
                <p style="float:right">Đơn vị tính: Người</p>
            </div>
            <div class="smtable_header">
                <table class="smtable">
                    <thead>
                        <tr>
                            <th style="width: 15%"rowspan="2">Tên chỉ tiêu</th>
                            <th style="width: 5%;" rowspan="2">Mã số</th>
                            <th style="width: 20%;" colspan="2">Đầu kỳ</th>                             
                            <th style="width: 20%;" colspan="2">Cuối kỳ</th>
                        </tr>
                        <tr>                              
                            <th>Tổng số</th>  
                            <th>Tr.đó: Nữ</th> 
                            <th>Tổng số</th>  
                            <th>Tr.đó: Nữ</th>  
                        </tr>
                        <tr>
                            <th>A</th>  
                            <th>B</th> 
                            <th>1</th>  
                            <th>2</th>  
                            <th>3</th>  
                            <th>4</th>  
                        </tr>
                    </thead>
                    <tbody class="clsContent">
                       
                    </tbody>
                </table>

            </div>
            <div style="margin-top:20px;margin-bottom:10px">
                  <b style="font-size: 13pt; float:left;width: 100% ;text-align: left">2. Thu nhập của người lao động và đóng góp của chủ doanh nghiệp về BHXH, y tế, thất nghiệp, kinh phí công đoàn</b>
            </div>
            <div style="width:100%; float:left">
                <p style="float:right">Đơn vị tính: Triệu đồng</p>
            </div>
            <div class="smtable_header">
                <table class="smtable">
                    <thead>
                        <tr>
                            <th style="width: 15%">Tên chỉ tiêu</th>
                            <th style="width: 5%;">Mã số</th>
                            <th style="width: 20%;" >Phát sinh trong kỳ</th>                             
                        </tr>
                        <tr>                              
                            <th>A</th>  
                            <th>B</th> 
                            <th>1</th>  
                        </tr>
                        
                    </thead>
                    <tbody class="clsContent2">
                       
                    </tbody>
                </table>
            </div>
            <div class="text-center mt-4 checkdatanull" style="width:100% ; float: left ">
                <p style="font-size: 12pt; width: 100%">Chưa có dữ liệu </p>
            </div>
            <script id="javascriptTemplate" type="text/x-kendo-template">
            <tr class="clsvbd" data-id="">
                <td style="width: 15%; word-wrap: break-word;" >#=TenChiTieu !=null ? TenChiTieu :""#</td>
                <td style="width: 5%;word-wrap: break-word;text-align:center;" >#=MaSo !=null ? MaSo :""#</td>
                <td style="width: 10%;word-wrap: break-word; text-align:right;" >#=TongSoDauKy !=null ? formatMoney(TongSoDauKy) :""#</td>
                <td style="width: 10%;word-wrap: break-word; text-align:right;" >#=LDNuDauKy !=null ? formatMoney(LDNuDauKy) :""#</td>
                <td style="width: 10%;word-wrap: break-word; text-align:right;" >#=TongSoCuoiKy !=null ? formatMoney(TongSoCuoiKy) :""#</td>
                <td style="width: 10%;word-wrap: break-word; text-align:right;" >#=LDNuCuoiKy !=null ? formatMoney(LDNuCuoiKy) :""#</td>
            </tr>
            </script>
            <script id="javascriptTemplate2" type="text/x-kendo-template">
            <tr class="clsvbd" data-id="">
                <td style="width: 15%; word-wrap: break-word;" >#=TenChiTieu !=null ? TenChiTieu : "" #</td>
                <td style="width: 5%; word-wrap: break-word;text-align:center;" >#=MaSo !=null ? MaSo : ""#</td>
                <td style="width: 20%;word-wrap: break-word; text-align:right;" >#=PhatSinhTrongKy !=null ? formatMoney(PhatSinhTrongKy) : ""#</td>
            </tr>
            </script>
           
           
        </div>
    </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        ThongKe();
        $(".thongke").click(function () {
            ThongKe();
        });
        $(".input-datetime").daterangepicker({
            singleDatePicker: true,
            locale: { format: 'DD/MM/YYYY' }
        });

        $("#btnExel").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BCLinhVucQuanTri/QUERYDATA_BaoCaoUocLaoDong?excel=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");
        });
        $("#btnword").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BCLinhVucQuanTri/QUERYDATA_BaoCaoUocLaoDong?word=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");

        });
    });
    function ThongKe() {
        $("#tungay").text($("#SearchTuNgay").val());
        $("#denngay").text($("#SearchDenNgay").val());
        var data = $(".zone_search").siSerializeArray();
        loading();
        $.post("/VIPortalAPI/api/BCLinhVucQuanTri/QUERYDATA_BaoCaoUocLaoDong?", data, function (data) {
            console.log(data.data.LaoDongs);
            $(".Title").text(data.Title);
            $(".LoaiBM").text(data.LoaiBM);
            $(".TieuDeTrai1").text(data.TieuDeTrai1);
            $(".TieuDeTrai2").text(data.TieuDeTrai2);
            $(".Nam").text(data.Nam);
            $(".TieuDeGiua1").text(data.TieuDeGiua1);
            $(".TieuDeGiua2").text(data.TieuDeGiua2);
            $(".SoBM").text(data.SoBM);  

            var htmlTEmp = '';
            var htmlTEmp2 = '';
            var templatehtmltr = kendo.template($("#javascriptTemplate").html());
            var templatehtmltr2 = kendo.template($("#javascriptTemplate2").html());
            $(".mainContentBaoCao .clsContent").html("");
            if (data.data.length > 0) {
                $(".checkdatanull").hide();
                for (var i = 0; i < 17; i++) {
                    data.data[i]["STT"] = i + 1;
                    htmlTEmp += templatehtmltr(data.data[i]);
                }
                $(".mainContentBaoCao .clsContent").append(htmlTEmp);
            }
            else {
                $(".checkdatanull").show();

            }
            $(".mainContentBaoCao .clsContent2").html("");
            if (data.data.length > 0) {
                $(".checkdatanull").hide();
                for (var i = 17; i < data.data.length; i++) {
                    data.data[i]["STT"] = i + 1;
                    htmlTEmp2 += templatehtmltr2(data.data[i]);
                }
                $(".mainContentBaoCao .clsContent2").append(htmlTEmp2);
            }
            else {
                $(".checkdatanull").show();

            }
            
          
            $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');

            $('#tongbanghi').text("Có tổng số: " + data.data.length + " bản ghi");

            $(".clsvbd").css('cursor', 'pointer');
             Endloading();
        });
       
    }

</script>
</html>
