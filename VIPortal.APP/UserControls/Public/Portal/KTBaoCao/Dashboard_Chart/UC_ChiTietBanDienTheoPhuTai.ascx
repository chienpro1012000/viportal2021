﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_ChiTietBanDienTheoPhuTai.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.Dashboard_Chart.UC_ChiTietBanDienTheoPhuTai" %>
<!DOCTYPE html>
<html>
<body>
    <div id="example1">
        <div class="configurator">
            <div class="box-col">
                <ul class="options">
                    <li style="display: none">
                        <input id="labels" checked="checked" type="checkbox" autocomplete="off" />
                        <label for="labels">Show labels</label>
                    </li>
                </ul>
            </div>
        </div>
        <div class="demo-section k-content wide">
            <div id="chart_phutai"></div>
        </div>
        <script>            
            function createChart_phutai() {
                $("#chart_phutai").kendoChart({
                    title: {
                        text: "Chi tiết bán điện theo thành phần phụ tải"
                    },
                    legend: {
                        position: "top"
                    },
                    seriesDefaults: {
                        labels: {
                            template: "#= category # - #= kendo.format('{0:P}', percentage)#",
                            position: "outsideEnd",
                            visible: true,
                            background: "transparent"
                        }
                    },
                    series: [{
                        type: "pie",
                        data: [
                            get_data()
                        ]
                        //data: [{
                        //    category: "Football",
                        //    value: 35
                        //}, {
                        //    category: "Basketball",
                        //    value: 25
                        //}, {
                        //    category: "Volleyball",
                        //    value: 20
                        //}, {
                        //    category: "Rugby",
                        //    value: 10
                        //}, {
                        //    category: "Tennis",
                        //    value: 10
                        //}]
                    }],
                    tooltip: {
                        visible: true,
                        template: "#= category # - #= kendo.format('{0:P}', percentage) #"
                    }
                });
            }


            function refresh() {
                var chart = $("#chart_phutai").data("kendoChart"),
                    pieSeries = chart.options.series[0],
                    labels = $("#labels").prop("checked"),
                    alignInputs = $("input[name='alignType']"),
                    alignLabels = alignInputs.filter(":checked").val();
                    chart.options.transitions = false;
                    pieSeries.labels.visible = labels;
                    pieSeries.labels.align = alignLabels;
                    alignInputs.attr("disabled", !labels);

                    chart.refresh();
            }
        </script>
        <script type="text/javascript">
            function get_data() {
                var new_data = new Array();                
                $.ajax({
                    url: "/VIPortalAPI/api/Dashboard/QUERYDATA_ChiTietBanDienTheoTTPhuTai",
                    async: false,
                    //very important: else php_data will be returned even before we get Json from the url
                    dataType: 'json',
                    success: function (result) {
                        debugger
                        var lenght = result.data.length;
                        if (result.data.length > 0) {                          
                            for (var i = 0; i < result.data.length; i++) {
                                var object = {};
                                object["category"] = result.data[i].ThanhPhanPhuTai;
                                object["value"] = parseInt(result.data[i].ThangBCBieu1) + parseInt(result.data[i].ThangBCBieu2) + parseInt(result.data[i].ThangBCBieu3) + parseInt(result.data[i].ThangBCBanDien1LoaiG);
                                new_data.push(object);
                            }
                        }
                    }
                });
                debugger;
                return new_data;
            };
            //function binddingdata(data) {                
            //}
            function binddingdata() {
                $.post("/VIPortalAPI/api/Dashboard/QUERYDATA_ChiTietBanDienTheoTTPhuTai?", function (data) {
                    var databin = [];
                    var object = {};
                    async: false;
                    debugger;
                    if (data.data.length > 0) {
                        for (var i = 0; i < data.data.length; i++) {
                            object["category"] = data.data[i].ThanhPhanPhuTai;
                            object["value"] = parseInt(data.data[i].ThangBCBieu1) + parseInt(data.data[i].ThangBCBieu2) + parseInt(data.data[i].ThangBCBieu3) + parseInt(data.data[i].ThangBCBanDien1LoaiG);
                            databin.push(object);
                        }
                    }                    
                    return databin;                    
                });
            }
            $(document).ready(function () {
                createChart_phutai();                
                //$(".thongke").click(function () {
                //    createChart();
                //});
                //$(".input-datetime").daterangepicker({
                //    singleDatePicker: true,
                //    locale: { format: 'DD/MM/YYYY' }
                //});
            });
            $(document).bind("kendo:skinChange", createChart_phutai);
            $(".box-col").bind("change", refresh);
        </script>
    </div>
</body>
</html>
