﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_BanDienTheoPhuTai_V2.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.Dashboard_Chart.UC_BanDienTheoPhuTai_V2" %>
<!DOCTYPE html>
<html>
<body>
    <div id="example_PhuTai_V2">
        <div class="zone_search1">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />
                <input type="hidden" name="CurentUser" value="<%=CurentUser.Title %>" />
                <div class="title_icon">
                </div>
                <div class="form-group row text-search align-items-center">
                   <label class="col-sm-2 control-label">Tháng báo cáo:</label>
                    <div class="col-sm-3">
                        <select class="form-control" id="Thang" name="Thang">
                            <option value="01" <%=DateTime.Now.ToString("MM") == "01" ? "selected" : "" %>>Tháng 1</option>
                            <option value="02 " <%=DateTime.Now.ToString("MM") == "02" ? "selected" : "" %>>Tháng 2</option>
                            <option value="03 " <%=DateTime.Now.ToString("MM") == "03" ? "selected" : "" %>>Tháng 3</option>
                            <option value="04 " <%=DateTime.Now.ToString("MM") == "04" ? "selected" : "" %>>Tháng 4</option>
                            <option value="05 " <%=DateTime.Now.ToString("MM") == "05" ? "selected" : "" %>>Tháng 5</option>
                            <option value="06 " <%=DateTime.Now.ToString("MM") == "06" ? "selected" : "" %>>Tháng 6</option>
                            <option value="07 " <%=DateTime.Now.ToString("MM") == "07" ? "selected" : "" %>>Tháng 7</option>
                            <option value="08 " <%=DateTime.Now.ToString("MM") == "08" ? "selected" : "" %>>Tháng 8</option>
                            <option value="09 " <%=DateTime.Now.ToString("MM") == "09" ? "selected" : "" %>>Tháng 9</option>
                            <option value="10 " <%=DateTime.Now.ToString("MM") == "10" ? "selected" : "" %>>Tháng 10</option>
                            <option value="11" <%=DateTime.Now.ToString("MM") == "11" ? "selected" : "" %>>Tháng 11</option>
                            <option value="12 " <%=DateTime.Now.ToString("MM") == "12" ? "selected" : "" %>>Tháng 12</option>
                        </select>
                    </div>
                    <label class="col-sm-2 control-label">Năm báo cáo :</label>
                    <div class="col-sm-3">
                        <input type="text" name="Nam" id="Nam" class="form-control " placeholder="Nhập năm" value="<%=DateTime.Now.ToString("yyyy") %>" />
                    </div>
                </div>
                  <div class="form-group row text-search align-items-center">

                    <label class="col-sm-2 control-label">Loại báo cáo:</label>

                    <div class="col-sm-3">
                        <select class="form-control" id="LoaiBaoCao" name="LoaiBaoCao">
                            <option value="1">Báo cáo tháng</option>
                            <option value="2">Báo cáo quý</option>
                            <option value="3">Báo cáo 6 tháng</option>
                            <option value="4">Báo cáo năm</option>
                        </select>
                    </div>
                    <label class="col-sm-2 control-label">Đơn vị:</label>
                    <div class="col-sm-3">
                        <select class="form-control" id="MaDonVi" name="MaDonVi">
                             <option value="all">Tất cả</option>
                            <option value="PD">Tổng Công ty Điện lực TP Hà Nội</option>
                            <option value="PD0600">Công ty Điện lưc Thanh Trì</option>
                            <option value="PD1400">Công ty Điện lực Long Biên</option>
                            <option value="P">Tập đoàn Điện Lực Việt Nam</option>
                            <option value="PD0700">Công ty Điện lực Gia Lâm</option>
                            <option value="PD0800">Công ty Điện lực Đông Anh</option>
                            <option value="PD0900">Công ty Điện lực Sóc Sơn</option>
                            <option value="PD1800">Công ty Điện lực Chương Mỹ</option>
                            <option value="PD2000">Công ty Điện lực Thường Tín</option>
                            <option value="PD2400">Công ty Điện lực Mỹ Đức</option>
                            <option value="PD2600">Công ty Điện lực Phúc Thọ</option>
                            <option value="PD2500">Công ty Điện lực Phú Xuyên</option>
                            <option value="PD2800">Công ty Điện lực Thanh Oai</option>
                            <option value="PD2900">Công ty Điện lực Ứng Hòa</option>
                            <option value="PD0100">Công ty Điện lực Hoàn Kiếm </option>
                            <option value="PD1500">Công ty Điện lực Mê Linh </option>
                            <option value="PD1700">Công ty Điện lực Sơn Tây</option>
                            <option value="PD1900">Công ty Điện lực Thạch Thất </option>
                            <option value="PD2100">Công ty Điện lực Ba Vì</option>
                            <option value="PD2200">Công ty Điện lực Đan Phượng </option>
                            <option value="PD2300">Công ty Điện lực Hoài Đức</option>
                            <option value="PD2700">Công ty Điện lực Quốc Oai </option>
                            <option value="PD0400">Công ty Điện lực Đống Đa</option>
                            <option value="PD0500">Công ty Điện lực Nam Từ Liêm</option>
                            <option value="PD0200">Công ty Điện lực Hai Bà Trưng </option>
                            <option value="PD1000">Công ty Điện lực Tây Hồ </option>
                            <option value="PD1100">Công ty Điện lực Thanh Xuân</option>
                            <option value="PD1200">Công ty Điện lực Cầy giấy</option>
                            <option value="PD3000">Công ty Điện lực Bắc Từ Liêm</option>
                            <option value="PD1300">Công ty Điện lực Hoàng Mai</option>
                            <option value="PD0300">Công ty Điện lực Ba Đình</option>
                            <option value="PD1600">Công ty Điện lực Hà Đông </option>
                            <option value="PD6900">Tổng Công ty Điện lực TP Hà Nội </option>
                        </select>
                    </div>
                </div>
                <div class="form-group row text-search align-items-center">
                 <div class="col-sm-4">
                        <button type="button" class="btn btn-primary thongke_phutai_v2">Thống kê</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="demo-section k-content wide">
            <div id="chart_phutai_v2"></div>
        </div>
        <script>
            function createChart_phutai_v2() {
                var data = $(".zone_search1").siSerializeArray();
                var str = jQuery.param(data);
                $("#chart_phutai_v2").kendoChart({
                    title: {
                        text: "Chi tiết bán điện theo thành phần phụ tải"
                    },
                    legend: {
                        visible: false
                    },
                    seriesDefaults: {
                        type: "column"
                    },
                    series: [{
                        data: get_data(str),
                        //errorBars: {
                        //    value: "stderr"
                        //}
                    }],
                    valueAxis: {
                        labels: {
                            format: "{0}"
                        },
                        line: {
                            visible: false
                        },
                        axisCrossingValue: 0
                    },
                    categoryAxis: {
                        categories: binddingtitle(str),
                        line: {
                            visible: false
                        },
                        labels: {
                            padding: { top: 100 }
                        }
                    },
                    tooltip: {
                        visible: true,
                        format: "{0}"
                    }
                });
            }

        </script>
        <script type="text/javascript">            
            function get_data(str) {
                var new_data = [];
                $.ajax({
                    url: "/VIPortalAPI/api/Dashboard/QUERYDATA_ChiTietBanDienTheoTTPhuTai?" + str,
                    async: false,
                    //very important: else php_data will be returned even before we get Json from the url
                    dataType: 'json',
                    success: function (result) {
                        var lenght = result.data.length;
                        if (result.data.length > 0) {
                            for (var i = 0; i < result.data.length; i++) {
                                new_data.push(parseInt(result.data[i].LuyKeNBieu1) + parseInt(result.data[i].LuyKeNBieu2) + parseInt(result.data[i].LuyKeNBieu3) + parseInt(result.data[i].LuyKeNBanDien1LoaiG));
                            }
                        }
                    }
                });
                console.log(new_data);
                return new_data;
            };
            
            function binddingtitle(str) {
                var new_data = [];
                $.ajax({
                    url: "/VIPortalAPI/api/Dashboard/QUERYDATA_ChiTietBanDienTheoTTPhuTai?" + str,
                    async: false,
                    //very important: else php_data will be returned even before we get Json from the url
                    dataType: 'json',
                    success: function (result) {                       
                        var lenght = result.data.length;
                        if (result.data.length > 0) {
                            for (var i = 0; i < result.data.length; i++) {
                                var object = {};
                                //object["category"] = result.data[i].ThanhPhanPhuTai;
                                //object["value"] = parseInt(result.data[i].ThangBCBieu1) + parseInt(result.data[i].ThangBCBieu2) + parseInt(result.data[i].ThangBCBieu3) + parseInt(result.data[i].ThangBCBanDien1LoaiG);
                                new_data.push(result.data[i].ThanhPhanPhuTai);
                            }
                        }
                    }
                });
                console.log(new_data);
                return new_data;
            }
            $(document).ready(function () {
                createChart_phutai_v2();
                $(".thongke_phutai_v2").click(function () {
                    createChart_phutai_v2();
                });
                //$(".input-datetime").daterangepicker({
                //    singleDatePicker: true,
                //    locale: { format: 'DD/MM/YYYY' }
                //});
            });
            $(document).bind("kendo:skinChange", createChart_phutai_v2);            
        </script>
    </div>



</body>
</html>
