﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_ChiTietSoDuSoThuCacKhoanThu.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.Dashboard_Chart.UC_ChiTietSoDuSoThuCacKhoanThu" %>
<div id="example" style="margin-top: 45px">
    <div class="demo-section k-content wide">
        <div id="chart_KhoanThu"></div>
    </div>
    <script>
        function createChart_KhoanThu() {
            $("#chart_KhoanThu").kendoChart({
                dataSource: {
                    transport: {
                        read: {
                            url: "/VIPortalAPI/api/Dashboard/QUERYDATA_ChiTietSoThuVaSoDuCacKPT",
                            dataType: "json"
                        }
                    },
                    sort: {
                        field: "",
                        dir: ""
                    }
                },
                title: {
                    text: "Chi tiết số thu và số dư các khoản phải thu"
                },
                legend: {
                    position: "top"
                },
                series: [{
                    field: "PTRA_DKY",
                    categoryField: "TEN_KM",
                    name: "Đầu tháng phải trả"
                }, {
                    field: "PTHU_DKY",
                    categoryField: "TEN_KM",
                    name: "Đầu tháng phải thu"
                }, {
                    field: "PTRA_CKY",
                    categoryField: "TEN_KM",
                    name: "Cuối tháng phải trả"
                }, {
                    field: "PTHU_CKY",
                    categoryField: "TEN_KM",
                    name: "Cuối tháng phải thu"
                }],
                categoryAxis: {
                    labels: {
                        rotation: -15
                    },
                    majorGridLines: {
                        visible: false
                    }
                },
                valueAxis: {
                    labels: {
                        format: "N0"
                    },
                    majorUnit: 5000000000,
                    max: 20000000000,
                    line: {
                        visible: false
                    }
                },
                tooltip: {
                    visible: true,
                    format: "N0"
                }
            });
        }
            //$(document).ready(createChart);
            //$(document).bind("kendo:skinChange", createChart);
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            createChart_KhoanThu();
            //$(".thongke").click(function () {
            //    createChart();
            //});
            //$(".input-datetime").daterangepicker({
            //    singleDatePicker: true,
            //    locale: { format: 'DD/MM/YYYY' }
            //});
        });
        $(document).bind("kendo:skinChange", createChart_KhoanThu);
    </script>
</div>

