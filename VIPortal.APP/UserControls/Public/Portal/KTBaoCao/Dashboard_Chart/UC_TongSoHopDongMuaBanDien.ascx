﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_TongSoHopDongMuaBanDien.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.Dashboard_Chart.UC_TongSoHopDongMuaBanDien" %>
<link rel="stylesheet" href="/Content/plugins/kendoui/styles/kendo.common.min.css" />
<link rel="stylesheet" href="/Content/plugins/kendoui/styles/kendo.default.min.css" />
<link rel="stylesheet" href="/Content/plugins/kendoui/styles/kendo.default.mobile.min.css" />
<script src="/Content/plugins/kendoui/js/kendo.all.min.js"></script>
<body>
    <div id="example">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />                
                <div class="title_icon">
                </div>
                <div class="form-group row text-search align-items-center">

                    <label class="col-sm-2 control-label">Tháng báo cáo:</label>
                    <div class="col-sm-3">
                        <select class="form-control" id="SearchThang" name="SearchThang">
                            <option value="01" <%=DateTime.Now.ToString("MM") == "01" ? "selected" : "" %>>Tháng 1</option>
                            <option value="02 " <%=DateTime.Now.ToString("MM") == "02" ? "selected" : "" %>>Tháng 2</option>
                            <option value="03 " <%=DateTime.Now.ToString("MM") == "03" ? "selected" : "" %>>Tháng 3</option>
                            <option value="04 " <%=DateTime.Now.ToString("MM") == "04" ? "selected" : "" %>>Tháng 4</option>
                            <option value="05 " <%=DateTime.Now.ToString("MM") == "05" ? "selected" : "" %>>Tháng 5</option>
                            <option value="06 " <%=DateTime.Now.ToString("MM") == "06" ? "selected" : "" %>>Tháng 6</option>
                            <option value="07 " <%=DateTime.Now.ToString("MM") == "07" ? "selected" : "" %>>Tháng 7</option>
                            <option value="08 " <%=DateTime.Now.ToString("MM") == "08" ? "selected" : "" %>>Tháng 8</option>
                            <option value="09 " <%=DateTime.Now.ToString("MM") == "09" ? "selected" : "" %>>Tháng 9</option>
                            <option value="10 " <%=DateTime.Now.ToString("MM") == "10" ? "selected" : "" %>>Tháng 10</option>
                            <option value="11" <%=DateTime.Now.ToString("MM") == "11" ? "selected" : "" %>>Tháng 11</option>
                            <option value="12 " <%=DateTime.Now.ToString("MM") == "12" ? "selected" : "" %>>Tháng 12</option>
                        </select>
                    </div>
                    <label class="col-sm-2 control-label">Năm báo cáo :</label>
                    <div class="col-sm-3">
                        <input type="text" name="SearchNam" id="SearchNam" class="form-control " placeholder="Nhập năm" value="<%=DateTime.Now.ToString("yyyy") %>" />
                    </div>
                </div>
                <div class="form-group row text-search align-items-center">
                    <label class="col-sm-2 control-label">Đơn vị:</label>
                    <div class="col-sm-3">
                        <select class="form-control" id="MaDonVi" name="MaDonVi">
                            <option value="all">Tất cả</option>
                            <option value="PD">Tổng Công ty Điện lực TP Hà Nội</option>
                            <option value="PD0600">Công ty Điện lưc Thanh Trì</option>
                            <option value="PD1400">Công ty Điện lực Long Biên</option>
                            <option value="P">Tập đoàn Điện Lực Việt Nam</option>
                            <option value="PD0700">Công ty Điện lực Gia Lâm</option>
                            <option value="PD0800">Công ty Điện lực Đông Anh</option>
                            <option value="PD0900">Công ty Điện lực Sóc Sơn</option>
                            <option value="PD1800">Công ty Điện lực Chương Mỹ</option>
                            <option value="PD2000">Công ty Điện lực Thường Tín</option>
                            <option value="PD2400">Công ty Điện lực Mỹ Đức</option>
                            <option value="PD2600">Công ty Điện lực Phúc Thọ</option>
                            <option value="PD2500">Công ty Điện lực Phú Xuyên</option>
                            <option value="PD2800">Công ty Điện lực Thanh Oai</option>
                            <option value="PD2900">Công ty Điện lực Ứng Hòa</option>
                            <option value="PD0100">Công ty Điện lực Hoàn Kiếm </option>
                            <option value="PD1500">Công ty Điện lực Mê Linh </option>
                            <option value="PD1700">Công ty Điện lực Sơn Tây</option>
                            <option value="PD1900">Công ty Điện lực Thạch Thất </option>
                            <option value="PD2100">Công ty Điện lực Ba Vì</option>
                            <option value="PD2200">Công ty Điện lực Đan Phượng </option>
                            <option value="PD2300">Công ty Điện lực Hoài Đức</option>
                            <option value="PD2700">Công ty Điện lực Quốc Oai </option>
                            <option value="PD0400">Công ty Điện lực Đống Đa</option>
                            <option value="PD0500">Công ty Điện lực Nam Từ Liêm</option>
                            <option value="PD0200">Công ty Điện lực Hai Bà Trưng </option>
                            <option value="PD1000">Công ty Điện lực Tây Hồ </option>
                            <option value="PD1100">Công ty Điện lực Thanh Xuân</option>
                            <option value="PD1200">Công ty Điện lực Cầy giấy</option>
                            <option value="PD3000">Công ty Điện lực Bắc Từ Liêm</option>
                            <option value="PD1300">Công ty Điện lực Hoàng Mai</option>
                            <option value="PD0300">Công ty Điện lực Ba Đình</option>
                            <option value="PD1600">Công ty Điện lực Hà Đông </option>
                            <option value="PD6900">Tổng Công ty Điện lực TP Hà Nội </option>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-primary thongke">Thống kê</button>
                        <button type="button" class="btn btn-primary" id="btnExel">Excel</button>
                    </div>
                </div>
                <div class="form-group row text-search align-items-center ">
                </div>
            </div>
        </div>
        <br />
        <br />
        <div class="demo-section k-content wide">
            <div id="chart"></div>
        </div>
        <script>
            function createChart() {
                $("#tungay").text($("#SearchTuNgay").val());
                $("#denngay").text($("#SearchDenNgay").val());
                var data = $(".zone_search").siSerializeArray();
                var str = jQuery.param(data);
                $("#chart").kendoChart({
                    dataSource: {
                        transport: {
                            read: {
                                url: "/VIPortalAPI/api/Dashboard/QUERYDATA_TongHopSoHDMuaBanDienCuaDV?" + str,
                                dataType: "json"
                            }
                        },
                        sort: {
                            field: "TenDonVi",
                            dir: "asc"
                        }
                    },
                    title: {
                        text: "Tổng hợp số hợp đồng mua bán điện các đơn vị"
                    },
                    legend: {
                        position: "top"
                    },
                    seriesDefaults: {
                        type: "column"
                    },
                    series:
                        [{
                            field: "TongSo",
                            categoryField: "TongSo",
                            name: "Tổng số"
                        }, {
                            field: "HDSinhHoat",
                            categoryField: "HDSinhHoat",
                            name: "Sinh hoạt"
                        }, {
                            field: "HDNgoaiSinhHoat",
                            categoryField: "HDNgoaiSinhHoat",
                            name: "Ngoài sinh hoạt"
                        }],
                    categoryAxis: {
                        labels: {
                            rotation: 0
                        },
                        majorGridLines: {
                            visible: false
                        }
                    },
                    valueAxis: {
                        labels: {
                            //format: "N0"
                        },
                        majorUnit: 10000,
                        line: {
                            visible: false
                        }
                    },
                    tooltip: {
                        visible: true,
                        //format: "N0"
                    }
                });
            }

            //$(document).ready(createChart);
            
        </script>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            createChart();
            $(".thongke").click(function () {
                createChart();
            });
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });
        });
        $(document).bind("kendo:skinChange", createChart);
    </script>
</body>
