﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SoVanBanDi.aspx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.VanPhong.SoVanBanDi" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link type="text/css" href="/Content/dist/css/StyleBaoCao.css" rel="stylesheet" />
    <style type="text/css">
        .zone_search {
            overflow-x: hidden;
        }
        
    </style>
</head>
<body>
    <div id="BaoCaoSoDangKy" class="smgrid" data-title="Văn bản đến" data-grid="grid_BaoCaoSoDKVanBanDen"
        data-form="/iVBDH/VanBanDen/pFormVanBanDen.aspx" data-formwidth="1200" data-action="/Vbden/api/VanBanDen/VBDGetListJsonGroupSolr" data-view="/Vbden/VanBanDen/pViewVanBanDen.aspx">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />
                <input type="hidden" name="CurentUser" value="<%=CurentUser.Title %>" />
                <div class="title_icon">
                </div>
                  <div class="form-group row text-search align-items-center">
                    <label class="col-sm-2 control-label">Phân loại:</label>
                    <div class="col-sm-2">
                        <select class="form-control" id="PhanLoai" name="PhanLoai">
                            <option value="IN_SO">In số</option>
                            <option value="IN_NGAY">In Ngày</option>
                        </select>
                    </div>
                       <label class="col-sm-2 control-label">Kiểu gửi nhận:</label>
                    <div class="col-sm-2">
                        <select class="form-control" id="KieuGuiNhan" name="KieuGuiNhan">
                            <option value="ALL">Tất cả</option>
                            <option value="ALL">Văn bản giấy </option>
                            <option value="ALL">Văn bản điện tử</option>                        
                        </select>
                    </div>
                  <div class="col-sm-4">
                        <button type="button" class="btn btn-primary thongke">Thống kê</button>
                        <button type="button" class="btn btn-primary" id="btnExel">Excel</button>
                    </div>
                </div> 
                <div class="form-group row text-search align-items-center">
                    <input type="hidden" name="DonViText" id="DonViText" value="" />
                    <label class="col-sm-2 control-label">Đơn Vị:</label>
                    <div class="col-sm-6">
                        <select class="form-control" id="LoaiDV" name="LoaiDV">
                            <option value="276">Tất cả</option>
                            <option value="284">Công ty Điện lực Hoàn Kiếm </option>
                            <option value="285">Công ty Điện lực Hai Bà Trưng </option>
                            <option value="286">Công ty Điện lực Ba Đình </option>
                            <option value="287">Công ty Điện lực Đống Đa </option>
                            <option value="291">Công ty Điện lực Nam Từ Liêm </option>
                            <option value="295">Công ty Điện lực Thanh Trì </option>
                            <option value="292">Công ty Điện lực Gia Lâm</option>
                            <option value="293">Công ty Điện lực Đông Anh </option>
                            <option value="288">Công ty Điện lực Tây Hồ</option>
                            <option value="294">Công ty Điện lực Sóc Sơn </option>
                            <option value="290">Công ty Điện lực Thanh Xuân </option>
                            <option value="289">Công ty Điện lực Cầu Giấy </option>
                            <option value="296">Công ty Điện lực Hoàng Mai </option>
                            <option value="297">Công ty Điện lực Long Biên</option>
                            <option value="298">Công ty Điện lực Mê Linh </option>
                            <option value="299">Công ty Điện lực Hà Đông</option>
                            <option value="300">Công ty Điện lực Sơn Tây</option>
                            <option value="301">Công ty Điện lực Chương Mỹ </option>
                            <option value="302">Công ty Điện lực Thạch Thất </option>
                            <option value="303">Công ty Điện lực Thường Tín </option>
                            <option value="304">Công ty Điện lực  Ba Vì</option>
                            <option value="305">Công ty Điện lực Đan Phượng</option>
                            <option value="306">Công ty Điện lực Hoài Đức </option>
                            <option value="307">Công ty Điện lực Mỹ Đức</option>
                            <option value="308">Công ty Điện lực Phú Xuyên </option>
                            <option value="309">Công ty Điện lực Phúc Thọ </option>
                            <option value="310">Công ty Điện lực Quốc Oai</option>
                            <option value="311">Công ty Điện lực Thanh Oai </option>
                            <option value="312">Công ty Điện lực Ứng Hòa</option>
                            <option value="481">Công ty Điện lực Bắc Từ Liêm</option>
                            <option value="281">Công ty Công nghệ thông tin Điện lực Hà Nội </option>
                            <option value="280">Trung tâm điều độ Hệ thống điện TP Hà Nội </option>
                            <option value="488">Trung tâm chăm sóc khách hàng </option>
                            <option value="282">Công ty Thí nghiệm điện Điện lực Hà Nội</option>
                            <option value="318">Công ty lưới điện cao thế Thành phố Hà Nội </option>
                            <option value="313">Công ty Dịch vụ Điện lực Hà Nội </option>
                            <option value="277">Ban QLDA lưới điện Hà Nội </option>
                            <option value="477">BQLDA Phát triển điện lực Hà Nội </option>
                        </select>
                    </div>
                       <%--<label class="col-sm-2 control-label">Phân loại văn bản :</label>
                    <div class="col-sm-2">
                        <select class="form-control" id="PhanLoaiVB" name="PhanLoaiVB">
                            <option value="VBDE">Văn bản đến </option>
                            <option value="VBDI">Văn bản đi</option>
                        
                        </select>
                    </div>--%>
                    <input type="hidden" name="PhanLoaiVB" value="VBDI" />
                </div> 
                <div class="form-group row text-search align-items-center" id="kieuinngay">
                    <label class="col-sm-2 control-label">Từ ngày:</label>

                    <div class="col-sm-2">
                        <input type="text" name="SearchTuNgay" id="SearchTuNgay" class="form-control input-datetime" placeholder="Từ ngày" value="<%--<%=DateTime.Now.ToString("dd/MM/yyyy") %>--%>" />
                    </div>
                    <label class="col-sm-2 control-label">Đến ngày:</label>
                    <div class="col-sm-2">
                        <input type="text" name="SearchDenNgay" id="SearchDenNgay" class="form-control input-datetime" placeholder="Đến ngày" value="<%--<%=DateTime.Now.ToString("dd/MM/yyyy") %>--%>" />
                    </div>
                    
                </div>
              
                <div class="form-group row text-search align-items-center" id="kieuinso">

                    <label class="col-sm-2 control-label">Từ số :</label>
                    <div class="col-sm-2">
                        <input type="text" name="TuSo" id="TuSo" class="form-control " placeholder="Từ số" value="0" />
                    </div>
                       <label class="col-sm-2 control-label">Đến số:</label>
                    <div class="col-sm-2">
                        <input type="text" name="DenSo" id="DenSo" class="form-control " placeholder="Đến số" value="0" />
                    </div>
                  
                </div>
            </div>
        </div>
        <hr />
        <div class="mainContentBaoCao">
            <div style="">
                <div style="width: 30%; float: left; text-align:center">
                      <p style="font-size: 10pt; width: 100%"><b class="TieuDeTrai1"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeTrai2"></b></p>
                </div>
                <div style="width: 55%; float: left; text-align:center">
                    <p style="font-size: 12pt; width: 100%"><b class="Title"></b></p>
                    
                </div>
                <div style="width: 15%; float: left; text-align:center">
                      <p style="font-size: 12pt; width: 100%"><p class="LoaiBM"></p></p>
                </div>
              

            </div>
            <div class="smtable_header">
                <table class="smtable">
                    <thead>
                        <tr>
                            <th style="width:5%" rowspan="2">STT</th>
                            <th style="width:10%;"rowspan="2">Số,ký hiệu văn bản</th>
                            <th style="width:10%;" rowspan="2">Ngày tháng văn bản</th>
                            <th style="width:10%;" rowspan="2">Tên loại và trích yếu nội dung văn bản</th>
                            <th style="width:15%;" rowspan="2">Người ký</th>
                            <th style="width:20%;" rowspan="2">Nơi nhận văn bản</th>
                            <th style="width:15%;" rowspan="2">Ghi chú</th>
                        </tr>
                        <tr></tr>
                        <tr>
                           <th>(1)</th>
                           <th>(2)</th>
                           <th>(3)</th>
                           <th>(4)</th>
                           <th>(5)</th>
                           <th>(6)</th>
                           <th>(7)</th>
                        </tr>
                    </thead>
                    <tbody class="clsContent">
                    </tbody>
                </table>
            </div>
             <div class="checkdata mt-3" style="width: 100%; text-align: center">
            <p style="font-size: 12pt">Chưa có bản ghi</p>
        </div>
            <script id="javascriptTemplatess" type="text/x-kendo-template">
            <tr class="clsvbd" data-id="">
                <td style="width: 5%;text-align:center;" >#=STT#</td>
                <td style="width:10%;word-wrap: break-word; text-align:left;">#=SoKyHieuVanBan !=null ? SoKyHieuVanBan : "" #</td>
                <td style="width:10%;word-wrap: break-word; text-align:right;">#=NgayThangVanBan !=null ? NgayThangVanBan : "" #</td>
                <td style="width:10%;word-wrap: break-word; text-align:left;">#=TenLoaiVaTrichYeuVanBan != null ? TenLoaiVaTrichYeuVanBan : ""#</td>
                <td style="width:15%;word-wrap: break-word; text-align:left;">#=NguoiKy !=null ? NguoiKy:""#</td> 
                <td style="width:20%;word-wrap: break-word; text-align:left;">#=NoiNhanVanBan !=null ?NoiNhanVanBan :""#</td> 
                <td style="width:15%;word-wrap: break-word; text-align:left;" >#=GhiChu !=null && GhiChu != "NULL"  ? GhiChu : ""#</td>           
            </tr>
            </script>
        </div>
    </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        ThongKe();
        $("#kieuinngay").hide();
        $("#kieuinso").show();
       // $("#SearchTuNgay").val("ALL");
       // $("#SearchDenNgay").val("ALL");
        $("#PhanLoai").change(function () {
           
            var check = $(this).find(":selected").val();
            if (check == "IN_SO") {
                $("#kieuinngay").hide();
                $("#kieuinso").show();
              //  $("#SearchTuNgay").val("ALL");
              //  $("#SearchDenNgay").val("ALL");
            } else {
                $("#kieuinngay").show();
                $("#kieuinso").hide();
                $("#TuSo").val(0);
                $("#DenSo").val(0);
            }
        });
        $("#LoaiDV").smSelect2018V2({
            dropdownParent: "#baocao"
        });
        $(".thongke").click(function () {
            ThongKe();

        });
        $(".input-datetime").daterangepicker({
            singleDatePicker: true,
            locale: { format: 'DD/MM/YYYY' }
        });

        $("#btnExel").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/VanPhong/QUERYDATA_SoVanBanDi?excel=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");
        });
        $("#btnword").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/VanPhong/QUERYDATA_SoVanBanDi?word=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");

        });
    });
   
    function ThongKe() {
        $("#tungay").text($("#SearchTuNgay").val());
        $("#denngay").text($("#SearchDenNgay").val());
        var dvtext = $("#LoaiDV option:selected").text();
        $("#DonViText").val($("#LoaiDV option:selected").text());
        var data = $(".zone_search").siSerializeArray();
        loading();
        $.post("/VIPortalAPI/api/VanPhong/QUERYDATA_SoVanBanDi?", data, function (data) {

            $(".Title").text(data.Title);
            $(".TieuDeTrai1").text(data.TieuDeTrai1);
            $(".TieuDeTrai2").text(data.TieuDeTrai2);
            $(".Nam").text(data.Nam);


            var htmlTEmp = '';
            var templatehtmltr = kendo.template($("#javascriptTemplatess").html());
            $(".mainContentBaoCao .clsContent").html("");
            if (data.data.length > 0) {
                $(".checkdata").hide();
                for (var i = 0; i < data.data.length; i++) {
                    data.data[i]["STT"] = i + 1;
                    htmlTEmp += templatehtmltr(data.data[i]);
                }

                $(".mainContentBaoCao .clsContent").append(htmlTEmp);
                $('#tongbanghi').text("Có tổng số: " + data.data.length + " bản ghi");

            } else {
                $(".checkdata").show();
            }
            $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');


            $(".clsvbd").css('cursor', 'pointer');
             Endloading();
        });
       
    }

</script>
</html>