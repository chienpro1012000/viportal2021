﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_KTBaoCao.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.UC_KTBaoCao" %>
<style>
    #treeDMbaocao {
        overflow-y: scroll;
        height: 500px;
    }
</style>
<section class="news-event">
    <div class="newsevent" id="content">
        <div class="card card-default color-palette-box">
            <div class="card-body">
                <div class="container-fluid">
                    <!-- Control the column width, and how they should appear on different devices -->
                    <div class="row">
                        <div class="col-sm-2">
                            <h5>Danh mục báo cáo</h5>
                            <div id="treeDMbaocao"></div>
                        </div>

                        <div class="col-sm-10" id="BaoCaoMain">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
    #treeDMbaocao span span.fancytree-title {
        white-space: normal;
        width: 85%;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        w3_close();
        $('#treeDMbaocao').fancytree({
            //  extensions: ['contextMenu'],
            source: {
                url: "/VIPortalAPI/api/MenuQuanTri/GetMenuTree?PhanLoaiMenu=2"
            },
            renderNode: function (event, data) {

                var node = data.node;
                $(node.span).attr('data-id', data.node.key);
                $(node.span).attr('data-ItemIDBC', data.node.extraClasses);

            },
            activate: function (event, data) {
                console.log(data.node);
                if (data.node.key != "") {
                    loadAjaxContent(data.node.key, "#BaoCaoMain", { ItemID: data.node.extraClasses });
                } else {
                    alert("Báo cáo chưa được cài đặt");
                }
            },

        });
    });
</script>
