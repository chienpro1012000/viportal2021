﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CacChiTieuKD.aspx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.KinhDoanhVaDichVuKH.BaoCaoChiTieuDVKH" %>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link type="text/css" href="/Content/dist/css/StyleBaoCao.css" rel="stylesheet" />
    <style type="text/css">
        .zone_search {
            overflow-x: hidden;
        }
    </style>
</head>
<body>
    <div id="BaoCaoSoDangKy" class="smgrid" data-title="Văn bản đến" data-grid="grid_BaoCaoSoDKVanBanDen"
        data-form="/iVBDH/VanBanDen/pFormVanBanDen.aspx" data-formwidth="1200" data-action="/Vbden/api/VanBanDen/VBDGetListJsonGroupSolr" data-view="/Vbden/VanBanDen/pViewVanBanDen.aspx">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />
                <input type="hidden" name="CurentUser" value="<%=CurentUser.Title %>" />
                <div class="title_icon">
                </div>
                <div class="form-group row text-search align-items-center">
                   <label class="col-sm-2 control-label">Tháng:</label>

                    <div class="col-sm-3">
                        <select class="form-control" id="SearchThang" name="SearchThang">
                            <option value="1" <%=DateTime.Now.ToString("MM") == "01" ? "selected" : "" %>>Tháng 1</option>
                            <option value="2 " <%=DateTime.Now.ToString("MM") == "02" ? "selected" : "" %>>Tháng 2</option>
                            <option value="3 " <%=DateTime.Now.ToString("MM") == "03" ? "selected" : "" %>>Tháng 3</option>
                            <option value="4 " <%=DateTime.Now.ToString("MM") == "04" ? "selected" : "" %>>Tháng 4</option>
                            <option value="5 " <%=DateTime.Now.ToString("MM") == "05" ? "selected" : "" %>>Tháng 5</option>
                            <option value="6 " <%=DateTime.Now.ToString("MM") == "06" ? "selected" : "" %>>Tháng 6</option>
                            <option value="7 " <%=DateTime.Now.ToString("MM") == "07" ? "selected" : "" %>>Tháng 7</option>
                            <option value="8 " <%=DateTime.Now.ToString("MM") == "08" ? "selected" : "" %>>Tháng 8</option>
                            <option value="9 " <%=DateTime.Now.ToString("MM") == "09" ? "selected" : "" %>>Tháng 9</option>
                            <option value="10 " <%=DateTime.Now.ToString("MM") == "10" ? "selected" : "" %>>Tháng 10</option>
                            <option value="11" <%=DateTime.Now.ToString("MM") == "11" ? "selected" : "" %>>Tháng 11</option>
                            <option value="12 " <%=DateTime.Now.ToString("MM") == "12" ? "selected" : "" %>>Tháng 12</option>
                        </select>
                    </div>
                    <label class="col-sm-2 control-label"> Năm:</label>
                    <div class="col-sm-3">
                        <input type="text" name="SearchNam" id="SearchNam" class="form-control " placeholder="Nhập năm" value="<%=DateTime.Now.ToString("yyyy") %>" />
                    </div>
                    
                </div>
               <div class="form-group row text-search align-items-center">
                    <label class="col-sm-2 control-label">Đơn vị:</label>
                    <div class="col-sm-3">
                        <select class="form-control" id="MaDonVi" name="MaDonVi">
                          <option value="all">Tất cả</option>
                            <option value="PD">Tổng Công ty Điện lực TP Hà Nội </option>
                            <option value="PD0100">Công ty Điện lực Hoàn Kiếm </option>
                            <option value="PD0200">Công ty Điện lực Hai Bà Trưng </option>
                            <option value="PD0300">Công ty Điện lực Ba Đình</option>
                            <option value="PD0400">Công ty Điện lực Đống Đa</option>
                            <option value="PD0500">Công ty Điện lực Nam Từ Liêm</option>
                            <option value="PD0600">Công ty Điện lực Thanh Trì</option>
                            <option value="PD0700">Công ty Điện lực Gia Lâm</option>
                            <option value="PD0800">Công ty Điện lực Đông Anh</option>
                            <option value="PD0900">Công ty Điện lực Sóc Sơn</option>
                            <option value="PD1000">Công ty Điện lực Tây Hồ </option>
                            <option value="PD1100">Công ty Điện lực Thanh Xuân</option>
                            <option value="PD1200">Công ty Điện lực Cầy giấy</option>
                            <option value="PD1300">Công ty Điện lực Hoàng Mai</option>
                            <option value="PD1400">Công ty Điện lực Long Biên</option>
                            <option value="PD1500">Công ty Điện lực Mê Linh </option>
                            <option value="PD1600">Công ty Điện lực Hà Đông </option>
                            <option value="PD1700">Công ty Điện lực Sơn Tây</option>
                            <option value="PD1800">Công ty Điện lực Chương Mỹ</option>
                            <option value="PD1900">Công ty Điện lực Thạch Thất </option>
                            <option value="PD2000">Công ty Điện lực Thường Tín</option>
                            <option value="PD2100">Công ty Điện lực Ba Vì</option>
                            <option value="PD2200">Công ty Điện lực Đan Phượng </option>
                            <option value="PD2300">Công ty Điện lực Hoài Đức</option>
                            <option value="PD2400">Công ty Điện lực Mỹ Đức</option>
                            <option value="PD2500">Công ty Điện lực Phú Xuyên</option>
                            <option value="PD2600">Công ty Điện lực Phúc Thọ</option>
                            <option value="PD2700">Công ty Điện lực Quốc Oai </option>
                            <option value="PD2800">Công ty Điện lực Thanh Oai</option>
                            <option value="PD2900">Công ty Điện lực Ứng Hòa</option>
                            <option value="PD3000">Công ty Điện lực Bắc Từ Liêm</option>
                        </select>                 
                    </div>
                 <div class="col-sm-4">
                        <button type="button" class="btn btn-primary thongke">Thống kê</button>
                        <button type="button" class="btn btn-primary" id="btnExel">Excel</button>
                    </div>

                   </div>
            </div>
        </div>
        <hr />
        <div class="mainContentBaoCao">
            <div style="">
                <div style="width: 30%; float: left; text-align:center">
                      <p style="font-size: 12pt; width: 100%"><b class="TieuDeTrai1"></b></p>
                    <p style="font-size: 10pt; width: 100%"><b class="TieuDeTrai2"></b></p>
                </div>
                <div style="width: 55%; float: left; text-align:center">
                    <p style="font-size: 12pt; width: 100%"><b class="Title"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b>Tháng <i class="Thang"></i>   năm <i class="Nam"></i></b></p>
                </div>
                <div style="width: 15%; float: left; text-align:center">
                      <p style="font-size: 12pt; width: 100%"><p class="LoaiBM"></p></p>
                </div>
              

            </div>
            <div class="smtable_header">
                <table class="smtable">
                    <thead>
                        <tr>
                            <th style="width: 5%" rowspan="2">STT</th>
                            <th style="width: 27%"rowspan="2">Chỉ tiêu</th>
                            <th style="width: 8%;" rowspan="2">Đơn vị</th>
                            <th style="width: 30%;" colspan="6">Tháng báo cáo</th>                             
                            <th style="width: 30%;" colspan="6">Lũy kế đến tháng báo cáo</th>
                        </tr>
                        <tr>                              
                            <th>Số lượng (vụ/lần/KH)</th>  
                            <th>So cùng kì</th>  
                            <th>Tỉ trọng (%)</th>  
                            <th>Tổng thời gian t.hiện</th>  
                            <th>Thời gian TB</th>  
                            <th>So cùng kì</th> 
                            <th>Số lượng (vụ/lần/KH)</th>  
                            <th>So cùng kì</th>  
                            <th>Tỉ trọng (%)</th>  
                            <th>Tổng thời gian t.hiện</th>  
                            <th>Thời gian TB</th>   
                            <th>So cùng kì</th>  
                        </tr>
                        <tr>                              
                            <th>1</th>  
                            <th>2</th>  
                            <th>3</th>  
                            <th>4</th>  
                            <th>5</th>  
                            <th>6</th> 
                            <th>7</th>  
                            <th>8=7/4</th>  
                            <th>9</th>  
                            <th>10</th>  
                            <th>11</th>   
                            <th>12</th>  
                            <th>13</th>  
                            <th>14=13/10</th>  
                            <th>15</th>  
                        </tr>
                    </thead>
                    <tbody class="clsContent">
                       
                    </tbody>
                </table>
            </div>
                <div class="text-center mt-4 checkdatanull" style="width:100% ; float: left ">
                <p style="font-size: 12pt; width: 100%">Chưa có dữ liệu </p>
            </div>
            <script id="javascriptTemplate" type="text/x-kendo-template">
            <tr class="clsvbd" data-id="">
                <td style="width: 5%; text-align:center;" >#=STT != null ? STT : ""#</td>
                <td  style="width: 27%;">#=ChiTieu!=null?ChiTieu:""#</td>
                <td  style="width:8%; text-align:center;">#=DonVi!=null?DonVi:""#</td>
                <td  style="width: 5%; text-align: right; word-wrap: break-word;">#=SoLuongTBC!=null?formatMoney(SoLuongTBC):""#</td>                          
                <td style="width: 5%; text-align: right; word-wrap: break-word;">#=SoCungKiTBC!=null?SoCungKiTBC:""#</td>                          
                <td style="width: 5%; text-align: right; word-wrap: break-word;" >#=TiTrongTBC!=null?TiTrongTBC:""#</td>                          
                <td style="width:5%; text-align: right; word-wrap: break-word;">#=TThoiGianTHienTBC!=null?TThoiGianTHienTBC:""#</td>                          
                <td style="width: 5%; text-align: right; word-wrap: break-word;">#=ThoiGianTBTBC!=null?ThoiGianTBTBC:""#</td>                          
                <td style="width: 5%; text-align: right; word-wrap: break-word;">#=SoCungKi2TBC!=null?SoCungKi2TBC:""#</td>                          
                <td style="width: 5%; text-align: right; word-wrap: break-word;">#=SoLuongLK!=null?formatMoney(SoLuongLK):""#</td>                          
                <td style="width: 5%; text-align: right; word-wrap: break-word;">#=SoCungKiLK!=null?SoCungKiLK:""#</td>                          
                <td style="width: 5%; text-align: right; word-wrap: break-word;">#=TiTrongLK!=null?TiTrongLK:""#</td>                          
                <td style="width: 5%; text-align: right; word-wrap: break-word;">#=TThoiGianTHienLK!=null?TThoiGianTHienLK:""#</td>                          
                <td style="width: 5%; text-align: right; word-wrap: break-word;">#=ThoiGianBTLK!=null?ThoiGianBTLK:""#</td>                          
                <td style="width: 5%; text-align: right; word-wrap: break-word;">#=SoCungKi2LK!=null?SoCungKi2LK:""#</td>                          
              
               
            </tr>
            </script>
           
            <div style="">

               
                <div style="width: 33%; float: right; text-align: center; margin-top: 15px;">
                    <p style="width: 100%">  KT.TỔNG GIÁM ĐỐC</p>
                    <p style="font-size: 12pt; width: 100%" class="chucvu"><b>PHÓ TỔNG GIÁM ĐỐC</b></p>
                    <br />
                    <br />
                    <br />
                    <br />
                    <p style="font-size: 12pt; width: 100%"><p class="NguoiKy"></p></p>
                </div>
                 <div style="width: 33%; float: right; text-align: center; margin-top: 15px;">
                    <p style="width: 100%"> NGƯỜI LẬP BIỂU </p>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <p style="font-size: 12pt; width: 100%"><p class="NguoiLapBieu"></p></p>
                </div>
                <div style="width: 33%; float: right; text-align: center; margin-top: 15px;">
                    <p style="width: 100%">  TỔ TỔNG HỢP     </p>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <p style="font-size: 12pt; width: 100%"><p class="ToTongHop"></p></p>
                </div>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        ThongKe();
        $(".thongke").click(function () {
            ThongKe();
        });
        $(".input-datetime").daterangepicker({
            singleDatePicker: true,
            locale: { format: 'DD/MM/YYYY' }
        });

        $("#btnExel").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BaoCaoChiTeuDVKH/QUERYDATA_BaoCaoChiTieuDVKH?excel=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");
        });
        $("#btnword").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BaoCaoChiTeuDVKH/QUERYDATA_BaoCaoChiTieuDVKH?word=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");

        });
    });
    function ThongKe() {
        $("#tungay").text($("#SearchTuNgay").val());
        $("#denngay").text($("#SearchDenNgay").val());
        var data = $(".zone_search").siSerializeArray();
        loading();
        $.post("/VIPortalAPI/api/BaoCaoChiTeuDVKH/QUERYDATA_BaoCaoChiTieuDVKH?", data, function (data) {

            $(".Title").text(data.Title);
            $(".LoaiBM").text(data.LoaiBM);
            $(".TieuDeTrai1").text(data.TieuDeTrai1);
            $(".TieuDeTrai2").text(data.TieuDeTrai2);
            $(".NguoiKy").text(data.NguoiKy);
            $(".Thang").text(data.Thang);
            $(".Nam").text(data.Nam);
            $(".NguoiLapBieu").text(data.NguoiLapBieu);
            $(".ToTongHop").text(data.ToTongHop);



            var htmlTEmp = '';
            var templatehtmltr = kendo.template($("#javascriptTemplate").html());
            $(".mainContentBaoCao .clsContent").html("");
            if (data.data.length > 0) {
                $(".checkdatanull").hide();
                for (var i = 0; i < data.data.length; i++) {
                    data.data[i]["count"] = i + 1;
                    htmlTEmp += templatehtmltr(data.data[i]);
                }

                $(".mainContentBaoCao .clsContent").append(htmlTEmp);                
            }
            $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');

            $('#tongbanghi').text("Có tổng số: " + data.data.length + " bản ghi");

            $(".clsvbd").css('cursor', 'pointer');
            Endloading(); 
        });
       
    }

</script>
</html>
