﻿<%@ page language="C#" autoeventwireup="true" codebehind="CacChiTieuKD.aspx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.KinhDoanhVaDichVuKH.CacChiTieuKD" %>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link type="text/css" href="/Content/dist/css/StyleBaoCao.css" rel="stylesheet" />
    <style type="text/css">
        .zone_search {
            overflow-x: hidden;
        }
    </style>
</head>
<body>
    <div id="BaoCaoSoDangKy" class="smgrid" data-title="Văn bản đến" data-grid="grid_BaoCaoSoDKVanBanDen"
        data-form="/iVBDH/VanBanDen/pFormVanBanDen.aspx" data-formwidth="1200" data-action="/Vbden/api/VanBanDen/VBDGetListJsonGroupSolr" data-view="/Vbden/VanBanDen/pViewVanBanDen.aspx">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />
                <input type="hidden" name="CurentUser" value="<%=CurentUser.Title %>" />
                <div class="title_icon">
                </div>
                <div class="form-group row text-search align-items-center">
                    <label class="col-sm-2 control-label">Nhập Tháng:</label>

                    <div class="col-sm-3">
                        <select class="form-control" id="Thang" name="Thang">
                            <option value="01" <%=DateTime.Now.ToString("MM") == "01" ? "selected" : ""  %>>Tháng 1</option>
                            <option value="02 " <%=DateTime.Now.ToString("MM") == "02" ? "selected" : "" %>>Tháng 2</option>
                            <option value="03 " <%=DateTime.Now.ToString("MM") == "03" ? "selected" : "" %>>Tháng 3</option>
                            <option value="04 " <%=DateTime.Now.ToString("MM") == "04" ? "selected" : "" %>>Tháng 4</option>
                            <option value="05 " <%=DateTime.Now.ToString("MM") == "05" ? "selected" : "" %>>Tháng 5</option>
                            <option value="06 " <%=DateTime.Now.ToString("MM") == "06" ? "selected" : "" %>>Tháng 6</option>
                            <option value="07 " <%=DateTime.Now.ToString("MM") == "07" ? "selected" : "" %>>Tháng 7</option>
                            <option value="08 " <%=DateTime.Now.ToString("MM") == "08" ? "selected" : "" %>>Tháng 8</option>
                            <option value="09 " <%=DateTime.Now.ToString("MM") == "09" ? "selected" : "" %>>Tháng 9</option>
                            <option value="10 " <%=DateTime.Now.ToString("MM") == "10" ? "selected" : "" %>>Tháng 10</option>
                            <option value="11" <%=DateTime.Now.ToString("MM") == "11" ? "selected" : ""  %>>Tháng 11</option>
                            <option value="12 " <%=DateTime.Now.ToString("MM") == "12" ? "selected" : "" %>>Tháng 12</option>
                        </select>
                    </div>
                    <label class="col-sm-2 control-label">Nhập Năm:</label>
                    <div class="col-sm-3">
                        <input type="text" name="Nam" id="Nam" class="form-control " placeholder="Nhập năm" value="<%=DateTime.Now.ToString("yyyy") %>" />
                    </div>
                </div>
                <div class="form-group row text-search align-items-center">
                    <label class="col-sm-2 control-label">Đơn vị:</label>
                    <div class="col-sm-3">
                        <select class="form-control" id="MaDonVi" name="MaDonVi">
                            <option value="all">Tất cả</option>
                            <option value="PD">Tổng Công ty Điện lực TP Hà Nội </option>
                            <option value="PD0100">Công ty Điện lực Hoàn Kiếm </option>
                            <option value="PD0200">Công ty Điện lực Hai Bà Trưng </option>
                            <option value="PD0300">Công ty Điện lực Ba Đình</option>
                            <option value="PD0400">Công ty Điện lực Đống Đa</option>
                            <option value="PD0500">Công ty Điện lực Nam Từ Liêm</option>
                            <option value="PD0600">Công ty Điện lực Thanh Trì</option>
                            <option value="PD0700">Công ty Điện lực Gia Lâm</option>
                            <option value="PD0800">Công ty Điện lực Đông Anh</option>
                            <option value="PD0900">Công ty Điện lực Sóc Sơn</option>
                            <option value="PD1000">Công ty Điện lực Tây Hồ </option>
                            <option value="PD1100">Công ty Điện lực Thanh Xuân</option>
                            <option value="PD1200">Công ty Điện lực Cầy giấy</option>
                            <option value="PD1300">Công ty Điện lực Hoàng Mai</option>
                            <option value="PD1400">Công ty Điện lực Long Biên</option>
                            <option value="PD1500">Công ty Điện lực Mê Linh </option>
                            <option value="PD1600">Công ty Điện lực Hà Đông </option>
                            <option value="PD1700">Công ty Điện lực Sơn Tây</option>
                            <option value="PD1800">Công ty Điện lực Chương Mỹ</option>
                            <option value="PD1900">Công ty Điện lực Thạch Thất </option>
                            <option value="PD2000">Công ty Điện lực Thường Tín</option>
                            <option value="PD2100">Công ty Điện lực Ba Vì</option>
                            <option value="PD2200">Công ty Điện lực Đan Phượng </option>
                            <option value="PD2300">Công ty Điện lực Hoài Đức</option>
                            <option value="PD2400">Công ty Điện lực Mỹ Đức</option>
                            <option value="PD2500">Công ty Điện lực Phú Xuyên</option>
                            <option value="PD2600">Công ty Điện lực Phúc Thọ</option>
                            <option value="PD2700">Công ty Điện lực Quốc Oai </option>
                            <option value="PD2800">Công ty Điện lực Thanh Oai</option>
                            <option value="PD2900">Công ty Điện lực Ứng Hòa</option>
                            <option value="PD3000">Công ty Điện lực Bắc Từ Liêm</option>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-primary thongke">Thống kê</button>
                        <button type="button" class="btn btn-primary" id="btnExel">Excel</button>
                    </div>
                </div>
            </div>
            <hr />
            <div class="mainContentBaoCao">
                <div style="">
                    <div style="width: 30%; float: left; text-align:center">
                         <p style="font-size: 12pt; width: 100%"><b class="TieuDeTrai1"></b></p>
                         <p style="font-size: 10pt; width: 100%"><b class="TieuDeTrai2"></b></p>
                    </div>
                    <div style="width: 55%; float: left; text-align:center">
                        <p style="font-size: 12pt; width: 100%" id="TitleBC"><b class="NDTieude"></b></p>
                        <p style="font-size: 12pt; width: 100%"><i style="color: #212529;">Tháng  <i class="thang"></i> - Năm  <i class="nam"></i></i></p>
                    </div>
                    <div style="width: 15%; float: left; text-align:center">
                        <p style="float: right" class="LoaiBM"></p>
                    </div>
                </div>

                <div style="text-align: right; width: 100%; clear: both;"><span class="" style="margin-right: 1.25%;" id="tongbanghi"></span></div>
                <div style="width: 100%; float: left;">
                    <p style="font-size: 12pt;"><b>1. Kết quả kinh doanh trong tháng</b></p>
                </div>
                <div class="smtable_header">
                    <table class="smtable">
                        <thead>
                            <tr>
                                <th style="width: 2%" rowspan="2">STT</th>
                                <th style="width: 15%" rowspan="2">Chỉ tiêu</th>
                                <th style="width: 4%; text-align: center" rowspan="2">Đơn vị tính</th>
                                <th style="width: 20%;" colspan="2">Thực hiện</th>
                                <th style="width: 10%;" colspan="2">So sánh</th>
                            </tr>
                            <tr>
                                <th>Tháng</th>
                                <th>Lũy  kế năm</th>
                                <th>Cùng kỳ</th>
                                <th>K.Hoạch</th>
                            </tr>
                        </thead>
                        <tbody class="clsContent">
                        </tbody>
                    </table>
                </div>
                    <div class="text-center mt-4 checkdatanull" style="width:100% ; float: left ">
                <p style="font-size: 12pt; width: 100%">Chưa có dữ liệu </p>
            </div>
                <script id="javascriptTemplate" type="text/x-kendo-template">
            <tr class="clsvbd" data-id="">
                 <td style="width: 2%;text-align:center">#=STT != null ? STT : ""#</td>
                 <td style="width: 15%">#=ChiTieu!=null?ChiTieu:""#</td>
                 <td style="width: 4%; text-align:center">#=DonViTinh!=null?DonViTinh:""#</td>
                 <td style="width: 10%; text-align:right; word-wrap: break-word;">#=Thang!=null?formatMoney(Thang):""#</td>
                 <td style="width: 10%; text-align:right; word-wrap: break-word;">#=LuyKeNam!=null?formatMoney(LuyKeNam):""#</td>
                 <td style="width: 5%; text-align:right; word-wrap: break-word;">#=CungKy!=null?CungKy:""#</td>
                 <td style="width: 5%; text-align:right; word-wrap: break-word;">#=KHoach!=null?KHoach:""#</td>
            </tr>
                </script>
                <div style="width: 100%; float: left; margin-top: 20px;">
                    <p style="font-size: 12pt;"><b>2. Phân tích đánh giá tình hình thực hiện các chỉ tiêu kinh doanh và nhiệm vụ đột xuất</b></p>
                </div>
                <div style="width: 100%; float: left;">
                    <p style="font-size: 12pt;"><b>3. Các ý kiến đề xuất và kiến nghị với EVN</b></p>
                </div>
                <div style="">

                    <div style="width: 50%; float: right; text-align: center; margin-top: 15px;">
                        <p style="width: 100%">KT.TỔNG GIÁM ĐỐC</p>
                        <p style="font-size: 12pt; width: 100%"><b>PHÓ TỔNG GIÁM ĐỐC</b></p>
                        <br />
                        <br />
                        <br />
                        <br />
                        <p style="font-size: 12pt; width: 100%"><b class="NguoiKy"></b></p>
                    </div>
                </div>
            </div>
        </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        loadGiaoDien();
    });
    $(document).ready(function () {
        $(".thongke").click(function () {
            loadGiaoDien();
        });
        $(".input-datetime").daterangepicker({
            singleDatePicker: true,
            locale: { format: 'DD/MM/YYYY' }
        });

        $("#btnExel").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BC1KHDVKH/QUERYDATA_ChiTieuKinhDoanh?excel=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");
        });
        $("#btnword").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BC1KHDVKH/QUERYDATA_ChiTieuKinhDoanh?word=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");

        });
    });
    function loadGiaoDien() {
        $("#tungay").text($("#SearchTuNgay").val());
        $("#denngay").text($("#SearchDenNgay").val());
        var data = $(".zone_search").siSerializeArray();
        loading();
        $.post("/VIPortalAPI/api/BC1KHDVKH/QUERYDATA_ChiTieuKinhDoanh?", data, function (data) {
            console.log(data.Title);

            $(".NDTieude").text(data.Title);
            $(".LoaiBM").text(data.LoaiBM);
            $(".TieuDeTrai1").text(data.TieuDeTrai1);
            $(".TieuDeTrai2").text(data.TieuDeTrai2);
            $(".NguoiKy").text(data.NguoiKy);
            $(".thang").text(data.Thang);
            $(".nam").text(data.Nam);

            var htmlTEmp = '';
            var templatehtmltr = kendo.template($("#javascriptTemplate").html());
            $(".mainContentBaoCao .clsContent").html("");
            if (data.data.length > 0) {
                $(".checkdatanull").hide();
                for (var i = 0; i < data.data.length; i++) {
                    data.data[i]["count"] = i + 1;
                    htmlTEmp += templatehtmltr(data.data[i]);
                }

                $(".mainContentBaoCao .clsContent").append(htmlTEmp);
            }
            $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');

            $('#tongbanghi').text("Có tổng số: " + data.data.length + " bản ghi");

            $(".clsvbd").css('cursor', 'pointer');
            Endloading();

        });
    }
    function ThongKe() {
        $("#tungay").text($("#SearchTuNgay").val());
        $("#denngay").text($("#SearchDenNgay").val());
        var data = $(".zone_search").siSerializeArray();
        loading();
        $.post("/VIPortalAPI/api/BC1KHDVKH/QUERYDATA_ChiTieuKinhDoanh?", data, function (data) {
            console.log(data.Title);

            var htmlTEmp = '';
            var templatehtmltr = kendo.template($("#javascriptTemplate").html());
            $(".mainContentBaoCao .clsContent").html("");
            if (data.data.length > 0) {
                for (var i = 0; i < data.data.length; i++) {
                    data.data[i]["STT"] = i + 1;
                    htmlTEmp += templatehtmltr(data.data[i]);
                }

                $(".mainContentBaoCao .clsContent").append(htmlTEmp);
            }
            $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');

            $('#tongbanghi').text("Có tổng số: " + data.data.length + " bản ghi");

            $(".clsvbd").css('cursor', 'pointer');
           
        });
        Endloading();
    }

</script>
</html>
