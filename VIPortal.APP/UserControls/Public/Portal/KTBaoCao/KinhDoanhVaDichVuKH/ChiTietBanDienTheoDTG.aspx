﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChiTietBanDienTheoDTG.aspx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.KinhDoanhVaDichVuKH.ChiTietBanDienTheoDTG" %>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link type="text/css" href="/Content/dist/css/StyleBaoCao.css" rel="stylesheet" />
    <style type="text/css">
        .zone_search {
            overflow-x: hidden;
        }
    </style>
</head>
<body>
    <div id="BaoCaoSoDangKy" class="smgrid" data-title="Văn bản đến" data-grid="grid_BaoCaoSoDKVanBanDen"
        data-form="/iVBDH/VanBanDen/pFormVanBanDen.aspx" data-formwidth="1200" data-action="/Vbden/api/VanBanDen/VBDGetListJsonGroupSolr" data-view="/Vbden/VanBanDen/pViewVanBanDen.aspx">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />
                <input type="hidden" name="CurentUser" value="<%=CurentUser.Title %>" />
                <div class="title_icon">
                </div>
                   <div class="form-group row text-search align-items-center">

                    <label class="col-sm-2 control-label">Tháng báo cáo:</label>
                    <div class="col-sm-3">
                        <select class="form-control" id="SearchThang" name="SearchThang">
                            <option value="01" <%=DateTime.Now.ToString("MM") == "01" ? "selected" : "" %>>Tháng 1</option>
                            <option value="02 " <%=DateTime.Now.ToString("MM") == "02" ? "selected" : "" %>>Tháng 2</option>
                            <option value="03 " <%=DateTime.Now.ToString("MM") == "03" ? "selected" : "" %>>Tháng 3</option>
                            <option value="04 " <%=DateTime.Now.ToString("MM") == "04" ? "selected" : "" %>>Tháng 4</option>
                            <option value="05 " <%=DateTime.Now.ToString("MM") == "05" ? "selected" : "" %>>Tháng 5</option>
                            <option value="06 " <%=DateTime.Now.ToString("MM") == "06" ? "selected" : "" %>>Tháng 6</option>
                            <option value="07 " <%=DateTime.Now.ToString("MM") == "07" ? "selected" : "" %>>Tháng 7</option>
                            <option value="08 " <%=DateTime.Now.ToString("MM") == "08" ? "selected" : "" %>>Tháng 8</option>
                            <option value="09 " <%=DateTime.Now.ToString("MM") == "09" ? "selected" : "" %>>Tháng 9</option>
                            <option value="10 " <%=DateTime.Now.ToString("MM") == "10" ? "selected" : "" %>>Tháng 10</option>
                            <option value="11" <%=DateTime.Now.ToString("MM") == "11" ? "selected" : "" %>>Tháng 11</option>
                            <option value="12 " <%=DateTime.Now.ToString("MM") == "12" ? "selected" : "" %>>Tháng 12</option>
                        </select>
                    </div>
                    <label class="col-sm-2 control-label">Năm báo cáo :</label>
                    <div class="col-sm-3">
                        <input type="text" name="SearchNam" id="SearchNam" class="form-control " placeholder="Nhập năm" value="<%=DateTime.Now.ToString("yyyy") %>" />
                    </div>

                </div>
                <div class="form-group row text-search align-items-center">

                  
                    <label class="col-sm-2 control-label">Đơn vị:</label>
                    <div class="col-sm-3">
                        <select class="form-control" id="MaDonVi" name="MaDonVi">
                            <option value="all">Tất cả</option>
                            <option value="PD">Tổng Công ty Điện lực TP Hà Nội</option>
                            <option value="PD0100">Công ty Điện lực Hoàn Kiếm </option>
                            <option value="PD0200">Công ty Điện lực Hai Bà Trưng </option>
                            <option value="PD0300">Công ty Điện lực Ba Đình</option>
                            <option value="PD0400">Công ty Điện lực Đống Đa</option>
                            <option value="PD0500">Công ty Điện lực Nam Từ Liêm</option>
                            <option value="PD0600">Công ty Điện lực Thanh Trì</option>
                            <option value="PD0700">Công ty Điện lực Gia Lâm</option>
                            <option value="PD0800">Công ty Điện lực Đông Anh</option>
                            <option value="PD0900">Công ty Điện lực Sóc Sơn</option>
                            <option value="PD1000">Công ty Điện lực Tây Hồ </option>
                            <option value="PD1100">Công ty Điện lực Thanh Xuân</option>
                            <option value="PD1200">Công ty Điện lực Cầy giấy</option>
                            <option value="PD1300">Công ty Điện lực Hoàng Mai</option>
                            <option value="PD1400">Công ty Điện lực Long Biên</option>
                            <option value="PD1500">Công ty Điện lực Mê Linh </option>
                            <option value="PD1600">Công ty Điện lực Hà Đông </option>
                            <option value="PD1700">Công ty Điện lực Sơn Tây</option>
                            <option value="PD1800">Công ty Điện lực Chương Mỹ</option>
                            <option value="PD1900">Công ty Điện lực Thạch Thất </option>
                            <option value="PD2000">Công ty Điện lực Thường Tín</option>
                            <option value="PD2100">Công ty Điện lực Ba Vì</option>
                            <option value="PD2200">Công ty Điện lực Đan Phượng </option>
                            <option value="PD2300">Công ty Điện lực Hoài Đức</option>
                            <option value="PD2400">Công ty Điện lực Mỹ Đức</option>
                            <option value="PD2500">Công ty Điện lực Phú Xuyên</option>
                            <option value="PD2600">Công ty Điện lực Phúc Thọ</option>
                            <option value="PD2700">Công ty Điện lực Quốc Oai </option>
                            <option value="PD2800">Công ty Điện lực Thanh Oai</option>
                            <option value="PD2900">Công ty Điện lực Ứng Hòa</option>
                            <option value="PD3000">Công ty Điện lực Bắc Từ Liêm</option>
                        </select>
                    </div>
                     <label class="col-sm-2 control-label">Loại báo cáo:</label>

                    <div class="col-sm-3">
                        <select class="form-control" id="LoaiBaoCao" name="LoaiBaoCao">
                            <option value="1">Báo cáo tháng</option>
                            <option value="2">Báo cáo quý</option>
                            <option value="3">Báo cáo 6 tháng</option>
                            <option value="4">Báo cáo năm</option>
                        </select>
                    </div>
                     
                </div>
                <div class="form-group row text-search align-items-center ">
                   <div class="col-sm-4">
                        <button type="button" class="btn btn-primary thongke">Thống kê</button>
                        <button type="button" class="btn btn-primary" id="btnExel">Excel</button>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="mainContentBaoCao">
            <div style="">
                <div style="width: 30%; float: left; text-align:center">
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeTrai1"></b></p>
                    <p style="font-size: 10pt; width: 100%"><b class="TieuDeTrai2"></b></p>
                </div>
                <div style="width: 55%; float: left; text-align:center">
                    <p style="font-size: 12pt; width: 100%"><b class="Title"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b>Tháng <i class="Thang"></i>   năm <i class="Nam"></i></b></p>
                </div>
                <div style="width: 15%; float: left; text-align:center">
                      <p style="font-size: 12pt; width: 100%"><p class="LoaiBM"></p></p>
                     <p  style="font-size: 10pt; width: 100%" class="DVTinh"></p>
                </div>
              

            </div>
            <div style="text-align: right; width: 100%; clear: both;"><span class="" style="margin-right: 1.25%;" id="tongbanghi"></span></div>  
            <div class="smtable_header">
                <table class="smtable">
                    <thead>
                        <tr>
                            <th style="width: 5%" rowspan="2">STT</th>
                            <th style="width: 11%"rowspan="2">Mã đối tượng giá </th>
                            <th style="width: 6%;" rowspan="2">Mức giá</th>
                            <th style="width: 30%;" colspan="3">Tháng</th>                             
                            <th style="width: 33%;" colspan="3">Lũy kế năm</th>                             
                        </tr>
                        
                        <tr>                              
                            <th>Điện năng  (kWh) </th>
                            <th>Tỷ trọng (%)</th>
                            <th> Thành tiền (đ)</th>
                            <th>Điện năng  (kWh) </th>
                            <th>Tỷ trọng (%)</th>
                            <th> Thành tiền (đ)</th>
                        </tr>
                        <tr>
                            <th>1</th>  
                            <th>2</th>  
                            <th>3</th>  
                            <th>4</th>  
                            <th>5</th>  
                            <th>6=3x4</th>  
                            <th>7</th>  
                            <th>8</th>  
                            <th>9=3x7</th>  
                        </tr>
                          <tr>
                           <th style="width: 5%">A</th>
                           <th style="width: 11%">Giá Chính phủ</th>
                           <th style="width: 6%">(đ/kWh)</th>
                           <th style="width: 10%"></th>
                           <th style="width: 10%"></th>
                           <th style="width: 10%">đ</th>
                           <th style="width: 11%"></th>
                           <th style="width: 11%"></th>
                           <th style="width: 11%">đ</th>
                       </tr>
                    </thead>
                    <tbody class="clsContent">

                    </tbody>
                        
                </table>
            </div>
             <div class="chekdata mt-3" style="text-align: center">
                <p style="font-size: 12pt; width: 100%">Chưa có dữ liệu </p>
            </div>
            <script id="javascriptTemplate" type="text/x-kendo-template">
            <tr class="clsvbd styletr#=Style#" data-id="">
                <td style="width: 5%; text-align:center;" >#=NotHasSTT != 1 ?STT:""#</td>
                <td style="width: 11%" >#=MaDT != null ? MaDT : ""#</td>
                <td style="width: 6%; word-wrap: break-word; text-align:right;" >#=MucGia != null ? MucGia :""#</td>
                <td style="width: 10%; word-wrap: break-word; text-align:right;" >#=T_DienNang != null ? formatMoney(T_DienNang) :""#</td>
                <td style="width: 10%; word-wrap: break-word; text-align:right;" >#=T_TiTrong != null ? formatPercent(T_TiTrong,10000) : ""#</td>
                <td style="width: 10%; word-wrap: break-word; text-align:right;" >#=T_ThanhTien != null ? formatMoney(T_ThanhTien) : ""#</td>
                <td style="width: 11%; word-wrap: break-word; text-align:right;" >#=LKN_DienNang != null ? formatMoney(LKN_DienNang) : ""#</td>
                <td style="width: 11%; word-wrap: break-word; text-align:right;" >#=LKN_TiTrong != null ? formatPercent(LKN_TiTrong,10000) : "" #</td>
                <td style="width: 11%; word-wrap: break-word; text-align:right;" >#=LKN_ThanhTien != null ? formatMoney(LKN_ThanhTien) : "" #</td>
            </tr>
            </script>
           
            <div style="">

               
                <div style="width: 33%; float: right; text-align: center; margin-top: 15px;">
                    <p style="width: 100%">  KT.TRƯỞNG BAN</p>
                    <p style="font-size: 12pt; width: 100%" class="chucvu"><b>PHÓ TRƯỞNG BAN</b></p>
                    <br />
                    <br />
                    <br />
                    <br />
                    <p style="font-size: 12pt; width: 100%"><p class="NguoiKy"></p></p>
                </div>
                <div style="width: 33%; float: right; text-align: center; margin-top: 15px;">
                    <p style="width: 100%">  TỔ TỔNG HỢP     </p>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <p style="font-size: 12pt; width: 100%"><p class="ToTongHop"></p></p>
                </div>
                 <div style="width: 33%; float: right; text-align: center; margin-top: 15px;">
                    <p style="width: 100%"> NGƯỜI LẬP BIỂU </p>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <p style="font-size: 12pt; width: 100%"><p class="NguoiLapBieu"></p></p>
                </div>
                
            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        ThongKe();
        $(".thongke").click(function () {
            ThongKe();
        });
        $(".input-datetime").daterangepicker({
            singleDatePicker: true,
            locale: { format: 'DD/MM/YYYY' }
        });

        $("#btnExel").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BC1KHDVKH/QUERYDATA_ChiTietBanDienTheoDTG?excel=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");
        });
        $("#btnword").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BC1KHDVKH/QUERYDATA_TongHopSoHDMuaBanDienCuaDV?word=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");

        });
    });
    function ThongKe() {
        $("#tungay").text($("#SearchTuNgay").val());
        $("#denngay").text($("#SearchDenNgay").val());
        var data = $(".zone_search").siSerializeArray();
        loading();
        $.post("/VIPortalAPI/api/BC1KHDVKH/QUERYDATA_ChiTietBanDienTheoDTG?", data, function (data) {

            $(".Title").text(data.Title);
            $(".LoaiBM").text(data.LoaiBM);
            $(".TieuDeTrai1").text(data.TieuDeTrai1);
            $(".TieuDeTrai2").text(data.TieuDeTrai2);
            $(".NguoiKy").text(data.NguoiKy);
            $(".Thang").text(data.Thang);
            $(".Nam").text(data.Nam);
            $(".NguoiLapBieu").text(data.NguoiLapBieu);
            $(".ToTongHop").text(data.ToTongHop);
            $(".DVTinh").text(data.DVTinh);



            var htmlTEmp = '';
            var templatehtmltr = kendo.template($("#javascriptTemplate").html());
            $(".mainContentBaoCao .clsContent").html("");
            if (data.data.length > 0) {
                $(".chekdata").hide();
                var T_DienNang;
                var LKN_ThanhTien;
                var LKN_TiTrong;
                var LKN_DienNang;
                var T_TiTrong;
                var T_ThanhTien;             
                for (var i = 0; i < data.data.length; i++) {
                    data.data[i]["STT"] = i + 1;
                    T_DienNang += parseInt(data.data[i]["T_DienNang"]);
                    T_TiTrong += parseInt(data.data[i]["T_TiTrong"]);
                    T_ThanhTien += parseInt(data.data[i]["T_ThanhTien"]);
                    LKN_DienNang += parseInt(data.data[i]["LKN_DienNang"]);
                    LKN_TiTrong += parseInt(data.data[i]["LKN_TiTrong"]);
                    LKN_ThanhTien += parseInt(data.data[i]["LKN_ThanhTien"]);
                    htmlTEmp += templatehtmltr(data.data[i]);
                }
                console.log(T_DienNang);
                $(".T_DienNang").text(T_DienNang);
                $(".LKN_ThanhTien").text(LKN_ThanhTien);
                $(".LKN_TiTrong").text(LKN_TiTrong);
                $(".LKN_DienNang").text(LKN_DienNang);
                $(".T_TiTrong").text(T_TiTrong);
                $(".T_ThanhTien").text(T_ThanhTien);
                
                $(".mainContentBaoCao .clsContent").append(htmlTEmp);
                $('#tongbanghi').text("Có tổng số: " + data.data.length + " bản ghi");

            } else {
                $(".chekdata").show();
            }
            $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');


            $(".clsvbd").css('cursor', 'pointer');
  
        });
        Endloading();
    }

</script>
</html>



