﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ERP_BCTuoiThoHangTonKho_DTXD.aspx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.BanKeHoach.ERP_BCTuoiThoHangTonKho_DTXD" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link type="text/css" href="/Content/dist/css/StyleBaoCao.css" rel="stylesheet" />
    <style type="text/css">
        .zone_search {
            overflow-x: hidden;
        }
    </style>
</head>
<body>
    <div id="BaoCaoSoDangKy" class="smgrid" data-title="Văn bản đến" data-grid="grid_BaoCaoSoDKVanBanDen"
        data-form="/iVBDH/VanBanDen/pFormVanBanDen.aspx" data-formwidth="1200" data-action="/Vbden/api/VanBanDen/VBDGetListJsonGroupSolr" data-view="/Vbden/VanBanDen/pViewVanBanDen.aspx">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />
                <input type="hidden" name="CurentUser" value="<%=CurentUser.Title %>" />
                <div class="title_icon">
                </div>
                 <div class="form-group row text-search align-items-center">

                    <label class="col-sm-2 control-label">Tháng:</label>
                    <div class="col-sm-2">
                        <select class="form-control" id="Thang" name="Thang">
                            <option value="01" <%=DateTime.Now.ToString("MM") == "01" ? "selected" : "" %>>Tháng 1</option>
                            <option value="02 " <%=DateTime.Now.ToString("MM") == "02" ? "selected" : "" %>>Tháng 2</option>
                            <option value="03 " <%=DateTime.Now.ToString("MM") == "03" ? "selected" : "" %>>Tháng 3</option>
                            <option value="04 " <%=DateTime.Now.ToString("MM") == "04" ? "selected" : "" %>>Tháng 4</option>
                            <option value="05 " <%=DateTime.Now.ToString("MM") == "05" ? "selected" : "" %>>Tháng 5</option>
                            <option value="06 " <%=DateTime.Now.ToString("MM") == "06" ? "selected" : "" %>>Tháng 6</option>
                            <option value="07 " <%=DateTime.Now.ToString("MM") == "07" ? "selected" : "" %>>Tháng 7</option>
                            <option value="08 " <%=DateTime.Now.ToString("MM") == "08" ? "selected" : "" %>>Tháng 8</option>
                            <option value="09 " <%=DateTime.Now.ToString("MM") == "09" ? "selected" : "" %>>Tháng 9</option>
                            <option value="10 " <%=DateTime.Now.ToString("MM") == "10" ? "selected" : "" %>>Tháng 10</option>
                            <option value="11" <%=DateTime.Now.ToString("MM") == "11" ? "selected" : "" %>>Tháng 11</option>
                            <option value="12 " <%=DateTime.Now.ToString("MM") == "12" ? "selected" : "" %>>Tháng 12</option>
                        </select>
                    </div>

                    <label class="col-sm-2 control-label">Năm:</label>
                    <div class="col-sm-2">
                        <input type="text" name="Nam" id="Nam" class="form-control " placeholder="Nhập năm" value="<%=DateTime.Now.ToString("yyyy") %>" />
                    </div>

                </div>
                <div class="form-group row text-search align-items-center">

                    <label class="col-sm-2 control-label">Đơn Vị:</label>
                    <div class="col-sm-2">
                        <select class="form-control" id="MaDonVi" name="MaDonVi">
                            <option value="all">Tất cả</option>
                            <option value="071400">Công ty Điện lực Long Biên</option>
                            <option value="071500">Công ty Điện lực Mê Linh</option>
                            <option value="072200">Công ty Điện lực Đan Phượng</option>
                            <option value="072900">Công ty Điện lực Ứng Hòa</option>
                            <option value="073100">Công ty Công nghệ thông tin Điện lực Hà Nội</option>
                            <option value="073901">Ban Quản lý dự án Lưới điện Hà Nội - DEP</option>
                            <option value="070400">Công ty Điện lực Đống Đa</option>
                            <option value="072000">Công ty Điện lực Thường Tín</option>
                            <option value="072300">Công ty Điện lực Hoài Đức</option>
                            <option value="073500">Công ty Thí nghiệm điện Điện lực Hà Nội</option>
                            <option value="073802">Ban Quản lý dự án phát triển Điện lực Hà Nội - XDCB</option>
                            <option value="070200">Công ty Điện lực Hai Bà Trưng</option>
                            <option value="071700">Công ty Điện lực Sơn Tây</option>
                            <option value="071900">Công ty Điện lực Thạch Thất</option>
                            <option value="072800">Công ty Điện lực Thanh Oai</option>
                            <option value="073200">Trung tâm Điều độ Hệ thống điện TP Hà Nội</option>
                            <option value="073902">Ban Quản lý dự án Lưới điện Hà Nội - XDCB</option>
                            <option value="071000">Công ty Điện lực Tây Hồ </option>
                            <option value="071100">Công ty Điện lực Thanh Xuân</option>
                            <option value="072500">Công ty Điện lực Phú Xuyên</option>
                            <option value="070300">Công ty Điện lực Ba Đình</option>
                            <option value="070800">Công ty Điện lực Đông Anh</option>
                            <option value="071300">Công ty Điện lực Hoàng Mai</option>
                            <option value="072400">Công ty Điện lực Mỹ Đức</option>
                            <option value="070100">Công ty Điện lực Hoàn Kiếm</option>
                            <option value="071200">Công ty Điện lực Cầu Giấy</option>
                            <option value="071600">Công ty Điện lực Hà Đông</option>
                            <option value="072100">Công ty Điện lực Ba Vì</option>
                            <option value="073000">Công ty Điện lực Bắc Từ Liêm</option>
                            <option value="074000">Tổng Công ty điện lực TP Hà Nội</option>
                            <option value="070500">Công ty Điện lực Nam Từ Liêm</option>
                            <option value="073801">Ban Quản lý dự án phát triển Điện lực Hà Nội - ADB</option>
                            <option value="000100">Tập đoàn Điện lực Việt Nam - Kế toán ngành</option>
                            <option value="070600">Công ty Điện lực Thanh Trì</option>
                            <option value="070700">Công ty Điện lực Gia Lâm</option>
                            <option value="070900">Công ty Điện lực Sóc Sơn</option>
                            <option value="071800">Công ty Điện lực Chương Mỹ</option>
                            <option value="072600">Công ty Điện lực Phúc Thọ</option>
                            <option value="072700">Công ty Điện lực Quốc Oai</option>
                            <option value="073600">Công ty Lưới điện cao thế TP Hà Nội</option>
                            <option value="073700">Công ty Dịch vụ Điện lực Hà Nội</option>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-primary thongke">Thống kê</button>
                        <button type="button" class="btn btn-primary" id="btnExel">Excel</button>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="mainContentBaoCao">
           <div style="">
                <div style="width: 30%; float: left; text-align: center">
                    <p style="font-size: 10pt; width: 100%"><b class="TieuDeTrai1"></b></p>
                    <p style="font-size: 10pt; width: 100%"><b class="TieuDeTrai2">Đơn vị : ......</b></p>
                    <p style="font-size: 10pt; width: 100%"><b class="">Số :____/BC </b></p>
                </div>
                <div style="width: 55%; float: left; text-align: center">
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeGiua1">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM </b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeGiua2">Độc lập - Tự do -Hạnh phúc</b></p>
                    <p style="font-size: 12pt; width: 100%"><b>_____________________</b></p>
                </div>
                <div style="width: 15%; float: left; text-align: center">
                    <p style="font-size: 12pt; width: 100%">
                        <p class="LoaiBM"></p>
                    </p>
                </div>

                <div style="width: 100%; float: left; text-align: center">
                    <p style="font-size: 12pt; width: 100%"><b class="Title"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b>Tháng <i class="Thang"></i>- Năm <i class="Nam"></i></b></p>
                </div>
            </div>
            <div style="text-align: right; width: 100%; clear: both;"><span class="" style="margin-right: 1.25%;" id="tongbanghi"></span></div>    
            <div class="smtable_header">
                <table class="smtable">
                    <thead>
                        <tr>
                            <th style="width: 4%" rowspan="2">STT</th>
                            <th style="width: 15%" rowspan="2" >Đơn vị </th>
                            <th style="width: 5%" rowspan="2" >Tổng tồn kho ĐTXD</th>
                            <th style="width: 35%"colspan="7">VTTB trong kho (chốt số liệu ngày 20 định kỳ hàng tháng )</th>                             
                            <th style="width: 35%"colspan="7">VTTB ngoài kho (giao cho đơn vị nhà thầu ), (Chốt số liệu 20 định kỳ hàng tháng )</th> 
                               <th style="width: 15%" rowspan="2" >Ghi chú </th>
                        </tr>
                        <tr>                              
                            <th >Tổng trong kho </th>                            
                            <th >Dưới 6 tháng </th>                            
                            <th >Từ 6 tháng đến < 12 tháng  </th>                            
                            <th >Từ 1 năm đến < 2 năm</th>                            
                            <th >Từ 2 năm đến < 3 năm </th>                            
                            <th >Từ 3 năm đến < 4 năm </th>                            
                            <th >Từ 4 năm đến < 5 năm </th>  
                            <th >Tổng ngoài kho </th>                            
                            <th >Dưới 6 tháng </th>                            
                            <th >Từ 6 tháng đến < 12 tháng  </th>                            
                            <th >Từ 1 năm đến < 2 năm</th>                            
                            <th >Từ 2 năm đến < 3 năm </th>                            
                            <th >Từ 3 năm đến < 4 năm </th>                            
                            <th >Từ 4 năm đến < 5 năm </th>                            
                        </tr> 
                        <tr>                              
                            <th ></th>                            
                            <th >Dữ liệu trên ERP</th>                            
                            <th >Biểu 011B tồn kho ĐTXD</th>                            
                            <th >Biểu 049  </th>                            
                            <th >Biểu 049 </th>                            
                            <th >Biểu 049  </th>                            
                            <th >Biểu 049  </th>                            
                            <th >Biểu 049  </th>  
                            <th >Biểu 049 </th>                            
                            <th >Biểu 049 </th>                            
                            <th >Biểu 049 </th>                            
                            <th >Biểu 049</th>                            
                            <th >Biểu 049 </th>                            
                            <th >Biểu 049 </th>                            
                            <th >Biểu 049 </th>                            
                            <th >Biểu 049 </th>                            
                            <th >Biểu 049 </th>                            
                            <th >không dựa vào bảng (đây là phần giải thích) </th>                            
                        </tr>
                        <tr>                              
                            <th ></th>                            
                            <th >1</th>                            
                            <th >2=3+10</th>                            
                            <th >3=4+5+6+7+8+9 </th>                            
                            <th >4 </th>                            
                            <th >5  </th>                            
                            <th >6  </th>                            
                            <th > 7 </th>  
                            <th > 8</th>                            
                            <th > 9</th>                            
                            <th > 10 = 11+12+13+14+15+16 </th>                            
                            <th >11</th>                            
                            <th >12</th>                            
                            <th >13</th>                            
                            <th >14</th>                            
                            <th >15</th>                            
                            <th >16</th>                            
                            <th ></th>                            
                        </tr>
                        <tr></tr>
                    </thead>
                    <tbody class="clsContent">
                       
                    </tbody>
                </table>
            </div>
               <div class="chekdata mt-3" style="text-align: center">
                <p style="font-size: 12pt; width: 100%">Không có bản ghi </p>
            </div>
            <script id="javascriptTemplate" type="text/x-kendo-template">
            <tr class="clsvbd" data-id="">
                 <td style="width: 4%;">#=STT != null ? STT : ""#</td>
                 <td style="width: 15%;">#=LINE_DESCRIPTION != null ? LINE_DESCRIPTION : ""#</td>
                 <td style="width: 5%;">#=TONGTIEN != null ? TONGTIEN : ""#</td>
                 <td style="width: 5%;">#=NUM1 != null ? NUM1 : ""#</td>
                 <td style="width: 5%;">#=NUM2 != null ? NUM2 : "" #</td>
                 <td style="width: 5%;">#=NUM3 != null ? NUM3 : ""#</td>  
                 <td style="width: 5%;">#=NUM4 != null ? NUM4 : ""#</td>
                 <td style="width: 5%;">#=NUM5 != null ? NUM5 : "" #</td>
                 <td style="width: 5%;">#=NUM6 != null ? NUM6 : ""#</td>
                 <td style="width: 5%;">#=NUM7 != null ? NUM7 : ""#</td>
                 <td style="width: 5%;">#=NUM8 != null ? NUM8 : ""#</td>
                 <td style="width: 5%;">#=NUM9 != null ? NUM9 : ""#</td>
                 <td style="width: 5%;">#=NUM10 != null ? NUM10 : ""#</td>
                 <td style="width: 5%;">#=NUM11 != null ? NUM11 : ""#</td>
                 <td style="width: 5%;">#=NUM12 != null ? NUM12 : ""#</td>
                 <td style="width: 5%;">#=NUM13 != null ? NUM13 : ""#</td>
                 <td style="width: 5%;">#=NUM14 != null ? NUM14 : ""#</td>
                 <td style="width: 15%;"></td>
            </tr>
            </script>
             <div style="">

                    <div style="width: 50%; float: left; text-align: left; margin-top: 15px;">
                        <p style="width: 100%">Ghi chú : </p>
                        <p style="font-size: 10pt; width: 100%"> Kỳ vọng dữ liệu tích hợp từ chương trình ERP là tốt nhất </p>
                        <p style="font-size: 10pt; width: 100%">  Nếu nhập tay thì vẫn có thể nhầm lần  </p>
                      
                    </div> 
                   <div style="width: 50%; float: right; text-align: center; margin-top: 15px;">
                        <p style="width: 100%"><b>TRƯỞNG ĐƠN VỊ </b></p>
                        <p style="font-size: 12pt; width: 100%"><i>(ký tên, đóng dấu)</i></p>
                        <br />
                        <br />
                        <br />
                        <br />
                        <p style="font-size: 12pt; width: 100%"><b class="NguoiKy"></b></p>
                    </div>
                </div>
           <%-- <div style="display:flex;">
            <div style="text-align:center;margin:40px 15px 15px;width:33%">
                <p><b>NGƯỜI LẬP BIỂU</b></p>
                <p><b>(Ký, họ tên)</b></p>
            </div>
            <div style="text-align:center; margin:40px 15px 15px;width:33%">
                <p><b>KẾ TOÁN TRƯỞNG</b></p>
                <p><b>(Ký, họ tên)</b></p>
            </div>
            <div style="text-align:center;margin:40px 15px 15px;width:33%">
                <p><b>GIÁM ĐỐC</b></p>
                <p><b>(Ký, họ tên,đóng dấu)</b></p>
            </div>
            </div>   --%>
    </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        ThongKe();
    });
    $(document).ready(function () {
        $(".thongke").click(function () {
            ThongKe();
        });
        $(".input-datetime").daterangepicker({
            singleDatePicker: true,
            locale: { format: 'DD/MM/YYYY' }
        });

        $("#btnExel").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BanVatTuBanTruyenThong/QUERYDATA_ERP_BCTuoiThoHangTonKho_DTXD?excel=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");
        });
        $("#btnword").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BanVatTuBanTruyenThong/QUERYDATA_ERP_BCTuoiThoHangTonKho_DTXD?word=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");

        });
    });
    
    function ThongKe() {
        $("#tungay").text($("#SearchTuNgay").val());
        $("#denngay").text($("#SearchDenNgay").val());
        var data = $(".zone_search").siSerializeArray();
        loading();
        $.post("/VIPortalAPI/api/BanVatTuBanTruyenThong/QUERYDATA_ERP_BCTuoiThoHangTonKho_DTXD?", data, function (data) {
            console.log(data.Title);
            $(".Title").text(data.Title);
            $(".LoaiBM").text(data.LoaiBM);
            $(".TieuDeTrai1").text(data.TieuDeTrai1);
            $(".TieuDeTrai2").text(data.TieuDeTrai2);
            $(".NguoiKy").text(data.NguoiKy);
            $(".Thang").text(data.Thang);
            $(".Nam").text(data.Nam);
            var htmlTEmp = '';
            var templatehtmltr = kendo.template($("#javascriptTemplate").html());
            $(".mainContentBaoCao .clsContent").html("");
            if (data.data.length > 0) {
                $(".chekdata").hide();
                for (var i = 0; i < data.data.length; i++) {
                    data.data[i]["STT"] = i + 1;
                    htmlTEmp += templatehtmltr(data.data[i]);
                }
                $('#tongbanghi').text("Có tổng số: " + data.data.length + " bản ghi");
                $(".mainContentBaoCao .clsContent").append(htmlTEmp);
            } else {
                $(".chekdata").show();
            }
            $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');


            $(".clsvbd").css('cursor', 'pointer');
           Endloading();
        });
        
    }

</script>
            

</html>

