﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TongHopTienDoKhoiCongDongDien.aspx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.BanKeHoach.TongHopTienDoKhoiCongDongDien" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link type="text/css" href="/Content/dist/css/StyleBaoCao.css" rel="stylesheet" />
    <style type="text/css">
        .zone_search {
            overflow-x: hidden;
        }
    </style>
</head>
<body>
    <div id="BaoCaoSoDangKy" class="smgrid" data-title="Văn bản đến" data-grid="grid_BaoCaoSoDKVanBanDen"
        data-form="/iVBDH/VanBanDen/pFormVanBanDen.aspx" data-formwidth="1200" data-action="/Vbden/api/VanBanDen/VBDGetListJsonGroupSolr" data-view="/Vbden/VanBanDen/pViewVanBanDen.aspx">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />
                <input type="hidden" name="CurentUser" value="<%=CurentUser.Title %>" />
                <div class="title_icon">
                </div>
                <div class="form-group row text-search align-items-center">
                    <label class="col-sm-2 control-label">Năm báo cáo :</label>
                    <div class="col-sm-3">
                        <input type="text" name="Nam" id="Nam" class="form-control " placeholder="Nhập năm" value="<%=DateTime.Now.ToString("yyyy") %>" />
                    </div>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-primary thongke">Thống kê</button>
                        <button type="button" class="btn btn-primary" id="btnExel">Excel</button>
                    </div>
                </div>
               
            </div>
            <hr />
            <div class="mainContentBaoCao">
                  <div style="">
                    
                   <div style="width: 100%; float: left; text-align:center">
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeGiua1"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="Title"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b> Năm <i class="Nam"></i></b></p>
                </div>
                </div>
                
            </div>
               
                <div style="text-align: right; width: 100%; clear: both;"><span class="" style="margin-right: 1.25%;" id="tongbanghi"></span></div>
                
                <div class="smtable_header">
                    <table class="smtable">
                        <thead>
                        <tr>
                            <th style="width:5%;"rowspan="2">STT</th>
                            <th style="width:20%;"rowspan="2">Danh mục công trình</th>
                            <th style="width:10%;"rowspan="2">Nhóm dự án </th>                             
                            <th style="width:30%;"rowspan="2">Quy mô</th>                             
                            <th style="width:10%;"rowspan="2">Nguồn vốn</th>
                            <th style="width:15%;"rowspan="2">Đơn vị QLDA</th>
                            <th style="width:20%;"colspan="2">Tiến độ giao</th>
                            <th style="width:20%;"colspan="2">Tiến độ thực tế</th>
                            <th style="width:20%;"rowspan="2">Tình trạng dự án</th>
                            
                        </tr> 
                        <tr>

                            <th>Khởi công(tháng/năm)</th>
                            <th>Đóng điện(tháng/năm)</th>
                            <th>Khởi công(tháng/năm)</th>
                            <th>Đóng điện(tháng/năm)</th>
                        </tr>
                        <tr>
                            <th style="width:5%;" >1</th>
                            <th style="width:20%" >2</th>
                            <th style="width:10%;">3</th>                             
                            <th style="width:10%;">4</th>
                            <th style="width:10%;">5</th>
                            <th style="width:15%;">6</th>
                            <th style="width:20%;">7</th>
                            <th style="width:20%;">8</th>
                            <th style="width:20%;">9</th>
                            <th style="width:20%;">10</th>
                            <th style="width:20%;">11</th>
                            
                        </tr>
                      
                    </thead>
                        <tbody class="clsContent" id="cls_content">

                        </tbody>
                    </table>
                </div>
                    <div class="text-center mt-4 checkdatanull" style="width:100% ; float: left ">
                <p style="font-size: 12pt; width: 100%">Chưa có dữ liệu </p>
            </div>
                <script id="javascriptTemplate" type="text/x-kendo-template">
            <tr class="clsvbd" data-id="">
                <td style="width: 5% " >#=STT#</td>
                <td style="width: 20%" >#=DanhMucCongTrinh#</td>
                <td style="width: 10%" >#=NhomDuAn!=null?NhomDuAn:""#</td>
                <td style="width: 30%" >#=QuyMo#</td>
                <td style="width: 10%" >#=NguonVon#</td>
                <td style="width: 15%" >#=DonViQLDA#</td>
                <td style="width: 10%" >#=TienDoGiaoKC!=null?(TienDoGiaoKC):""#</td>
                <td style="width: 10%" >#=TienDoGiaoDD!=null?(TienDoGiaoDD):""#</td>
                <td style="width: 10%" >#=TienDoThucTeKC!=null?fomatdatetime(TienDoThucTeKC):""#</td>
                <td style="width: 10%" >#=TienDoThucTeDD!=null?fomatdatetime(TienDoThucTeDD):""#</td>
                <td style="width: 20%" >#=TinhTrangDuAn#</td>
                
            </tr>
                </script>
                
            </div>
        </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        ThongKe();
        $(".thongke").click(function () {
            ThongKe();
        });
        $(".input-datetime").daterangepicker({
            singleDatePicker: true,
            locale: { format: 'DD/MM/YYYY' }
        });

        $("#btnExel").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BanKeHoach2/QUERYDATA_TongHopTienDoKhoiCongDonDien?excel=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");
        });
        $("#btnword").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BanKeHoach2/QUERYDATA_TongHopTienDoKhoiCongDonDien?word=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");

        });
    });
    function ThongKe() {
        $("#tungay").text($("#SearchTuNgay").val());
        $("#denngay").text($("#SearchDenNgay").val());
        var data = $(".zone_search").siSerializeArray();
        loading();
        $.post("/VIPortalAPI/api/BanKeHoach2/QUERYDATA_TongHopTienDoKhoiCongDonDien?", data, function (data) {
            $(".Title").text(data.Title);
            $(".Nam").text(data.Nam);
            $(".TieuDeGiua1").text(data.TieuDeGiua1);



            var htmlTEmp = '';
            var templatehtmltr = kendo.template($("#javascriptTemplate").html());
            $(".mainContentBaoCao .clsContent").html("");
            if (data.data.length > 0) {
                $(".checkdatanull").hide();
                for (var i = 0; i < data.data.length; i++) {
                   
                    data.data[i]["STT"] = i + 1;
                    htmlTEmp += templatehtmltr(data.data[i]);
                }
               
                $("#cls_content").append(htmlTEmp);
                $('#tongbangh').text("Có tổng số: " + data.data.length + " bản ghi");

            } else {
                $(".checkdatanull").show();
            }
            $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');
            $(".clsvbd").css('cursor', 'pointer');
             Endloading();  
        });
       
    }

</script>
    <script>
        function fomatdatetime(dateStr) {
            var pattern = /(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/;

            var formatDateStr = dateStr.replace(pattern, '$1/$2/$3 $4:$5:$6');
            var formatDate = new Date(formatDateStr);
            return formatDate.format("dd/MM/yyyy");
        }
    </script>
</html>
