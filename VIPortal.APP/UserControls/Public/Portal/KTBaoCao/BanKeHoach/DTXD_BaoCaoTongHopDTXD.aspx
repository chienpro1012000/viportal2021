﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DTXD_BaoCaoTongHopDTXD.aspx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.DauTuXayDung.TongHopDauTuXayDung" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link type="text/css" href="/Content/dist/css/StyleBaoCao.css" rel="stylesheet" />
    <style type="text/css">
        .zone_search {
            overflow-x: hidden;
        }
        
    </style>
</head>
<body>
       <div id="BaoCaoSoDangKy" class="smgrid" data-title="Văn bản đến" data-grid="grid_BaoCaoSoDKVanBanDen"
        data-form="/iVBDH/VanBanDen/pFormVanBanDen.aspx" data-formwidth="1200" data-action="/Vbden/api/VanBanDen/VBDGetListJsonGroupSolr" data-view="/Vbden/VanBanDen/pViewVanBanDen.aspx">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />
                <input type="hidden" name="CurentUser" value="<%=CurentUser.Title %>" />
                <div class="title_icon">
                </div>
                <div class="form-group row text-search align-items-center">
                  <label class="col-sm-2 control-label">Nhập Năm:</label>
                    <div class="col-sm-2">
                        <input type="text" name="Nam" id="Nam" class="form-control " placeholder="Nhập năm" value="<%=DateTime.Now.ToString("yyyy") %>" />
                    </div>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-primary thongke">Thống kê</button>
                        <button type="button" class="btn btn-primary" id="btnExel">Excel</button>
                    </div>
                   </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="mainContentBaoCao">
            <div style="">
                 <div style="width: 30%; float: left; text-align:center">
                      <p style="font-size: 10pt; width: 100%"><b class="TieuDeTrai1"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeTrai2"></b></p>
                </div>
                <div style="width: 55%; float: left; text-align:center">
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeGiua1"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeGiua2"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="Title"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b> <i class="Nam"></i></b></p>
                </div>
                <div style="width: 15%; float: left; text-align:center">
                      <p style="font-size: 12pt; width: 100%"><p class="LoaiBM"></p></p>
                </div>
            </div>
            <div style="width:100%">
                <div style="width:100%; display:flex">
                    <p style="font-size: 12pt; width: 100%">TỔNG SỐ DỰ ÁN : <i class="tongduan"> 0</i></p>
                </div> 
                <div style="width:100%">
                    <p style="font-size: 12pt; width: 100%">SỐ DỰ ÁN ĐÃ KHỞI CÔNG / SỐ DỰ ÁN GIAO KHỞI CÔNG : <i class="soduan_kc_gkc"> 0</i></p>
                </div>
                <div style="width:100%">
                    <p style="font-size: 12pt; width: 100%">SỐ DỰ ÁN ĐÃ HOÀN THÀNH / SỐ DỰ ÁN GIAO HOÀN THÀNH: <i class="soduan_ht_ght"> 0</i></p>
                </div>
                <div style="width:100% ;text-align:right">
                    <p style="font-size: 10pt; width: 100%">(Đơn vị tính: triệu đồng)</p>
                </div>
            </div>
            <div class="smtable_header" style="float:left;overflow-x:scroll">
                <table class="smtable" style="width:150%">
                    <thead>
                        <tr>
                            <th style="width:3%; " rowspan="2">STT</th>
                            <th style="width:15%;" rowspan="2">Tên dự án </th>
                            <th style="width:15%; " rowspan="2">Quy Mô</th>                             
                            <th style="width:10%;" rowspan="2">Nguồn Vốn </th>
                            <th style="width:10%; "colspan="2">Quyết định giao A</th>
                            <th style="width:10%; "colspan="2">Quyết định phê duyệt dự án  </th>
                            <th style="width:10%; "colspan="2">Khởi công </th>
                            <th style="width:10%; "colspan="2">Hoàn thành</th>
                            <th style="width:10%;" rowspan="2">Tình trạng dự án</th>
                            
                        </tr>
                        <tr>                              
                           
                            <th >Ngày giao</th>  
                            <th >Vốn kế hoạch</th>
                            <th >Số quyết định</th>  
                            <th >Tổng mức đầu tư</th>
                            <th >Kế Hoạch</th>  
                            <th >Thực tế </th>
                            <th >Kế Hoạch</th>  
                            <th >Thực tế </th>
                            
                             
                        </tr>
                        <tr>                              
                            <th >1</th>  
                            <th >2</th>  
                            <th >3</th>  
                            <th >4</th>  
                            <th >5</th>  
                            <th >6</th>  
                            <th >7</th>  
                            <th >8</th>
                            <th >9</th>  
                            <th >10</th>
                            <th >11</th>  
                            <th >12</th>  
                            <th >13</th>  
                        </tr>
                    </thead>
                    <tbody class="clsContent">
                    </tbody>
                </table>
            </div>
                 <div class="text-center mt-4 checkdatanull" style="width:100% ; float: left ">
                <p style="font-size: 12pt; width: 100%">Chưa có dữ liệu </p>
            </div>
            <script id="javascriptTemplate" type="text/x-kendo-template">
            <tr class="clsvbd" data-id="">
                <td style="width: 3%" >#=STT#</td>
                <td style="width:15%">#=TenDuAN!=null?TenDuAN:""#</td>
                <td style="width: 15%">#=QuyMo!=null?QuyMo:""#</td>                          
                <td style="width :10%">#=NguonVon!=null?NguonVon:""#</td> 
                <td style="width :5%">#=NgayGiao!=null?NgayGiao:""#</td> 
                <td style="width :5%">#=VonKeHoach!=null?VonKeHoach:""#</td> 
                <td style="width :5%">#=SoQuyetDinh!=null?SoQuyetDinh:""#</td> 
                <td style="width :5%">#=TongMucDauTu!=null?TongMucDauTu:""#</td> 
                <td style="width :5%">#=KhoiCongKH!=null?KhoiCongKH:""#</td> 
                <td style="width :5%">#=KhoiCongThucTe!=null?KhoiCongThucTe:""#</td> 
                <td style="width:5%" >#=HoanThanhKeHoach!=null?HoanThanhKeHoach:""#</td>                          
                <td style="width:5%">#=HoanThanhThucTe!=null?HoanThanhThucTe:""#</td>                          
                <td style="width:10%">#=TinhTrangDuAn!=null?TinhTrangDuAn:""#</td>                          
            </tr>
            </script> 
        

        </div>
    </div>
</body>
    <script type="text/javascript">
        $(document).ready(function () {
            ThongKe();
            $(".thongke").click(function () {
                ThongKe();
            });
            $(".input-datetime").daterangepicker({
                singleDatePicker: true,
                locale: { format: 'DD/MM/YYYY' }
            });

            $("#btnExel").click(function () {
                var $datathongke = $(".zone_search").siSerializeDivFrm();
                var str = jQuery.param($datathongke);
                var Url = "/VIPortalAPI/api/BanKeHoach/QUERYDATA_DTXD_BaoCaoTongHopDTXD?excel=1&" + str + "&TitleBC=" + $("#TitleBC").text();
                my_window = window.open(Url, "mywindow1");
            });
            $("#btnword").click(function () {
                var $datathongke = $(".zone_search").siSerializeDivFrm();
                var str = jQuery.param($datathongke);
                var Url = "/VIPortalAPI/api/BanKeHoach/QUERYDATA_DTXD_BaoCaoTongHopDTXD?word=1&" + str + "&TitleBC=" + $("#TitleBC").text();
                my_window = window.open(Url, "mywindow1");

            });
        });
        function ThongKe() {
            $("#tungay").text($("#SearchTuNgay").val());
            $("#denngay").text($("#SearchDenNgay").val());
            var data = $(".zone_search").siSerializeArray();
            loading();
            $.post("/VIPortalAPI/api/BanKeHoach/QUERYDATA_DTXD_BaoCaoTongHopDTXD?", data, function (data) {

                $(".Title").text(data.Title);
                $(".TieuDeTrai1").text(data.TieuDeTrai1);
                $(".TieuDeTrai2").text(data.TieuDeTrai2);
                $(".TieuDeGiua1").text(data.TieuDeGiua1);
                $(".TieuDeGiua2").text(data.TieuDeGiua2);
                $(".Nam").text(data.Nam);

                var tongduan = 0;
                var soda_khoicong = 0;
                var soda_giaokc = 0;
                var sodaht = 0;
                var sodagiaoht = 0;
                var htmlTEmp = '';
                var templatehtmltr = kendo.template($("#javascriptTemplate").html());
                $(".mainContentBaoCao .clsContent").html("");
                if (data.data.length > 0) {
                    $(".checkdatanull").hide();
                    for (var i = 0; i < data.data.length; i++) {
                        data.data[i]["STT"] = i + 1;
                        htmlTEmp += templatehtmltr(data.data[i]);
                        tongduan += parseInt(data.data[i]["ThongKe_DuAn_TongSo"] != null ? data.data[i]["ThongKe_DuAn_TongSo"] : 0 )
                        soda_khoicong += parseInt(data.data[i]["SoDA_KhoiCong"] != null ? data.data[i]["SoDA_KhoiCong"] : 0)
                        soda_giaokc += parseInt(data.data[i]["SoDA_GiaoKhoiCong"] != null ? data.data[i]["SoDA_GiaoKhoiCong"] : 0)
                        sodaht += parseInt(data.data[i]["SoDA_HT"] != null ? data.data[i]["SoDA_HT"] : 0)
                        sodagiaoht += parseInt(data.data[i]["SoDA_GiaoHT"] != null ? data.data[i]["SoDA_GiaoHT"] : 0)
                    }

                    $(".tongduan").text(tongduan.toString());
                    $(".soduan_kc_gkc").text(soda_khoicong.toString() + "/" + soda_giaokc.toString());
                    $(".soduan_ht_ght").text(sodaht.toString() + "/" + sodagiaoht.toString());
                    $(".mainContentBaoCao .clsContent").append(htmlTEmp);
                }

                $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');

                $('#tongbanghi').text("Có tổng số: " + data.data.length + " bản ghi");

                $(".clsvbd").css('cursor', 'pointer');

            });
            Endloading();
        }
    </script>
    <script>
        function fomatdatetime(dateStr) {
            var pattern = /(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/;

            var formatDateStr = dateStr.replace(pattern, '$1/$2/$3 $4:$5:$6');
            var formatDate = new Date(formatDateStr);
            return formatDate.format("dd/MM/yyyy");
        }
    </script>
</html>