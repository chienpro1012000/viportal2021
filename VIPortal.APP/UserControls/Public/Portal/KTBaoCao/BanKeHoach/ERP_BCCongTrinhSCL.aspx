﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ERP_BCCongTrinhSCL.aspx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.KTBaoCao.BanKeHoach.ERP_BCCongTrinhSCL" %>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link type="text/css" href="/Content/dist/css/StyleBaoCao.css" rel="stylesheet" />
    <style type="text/css">
        .zone_search {
            overflow-x: hidden;
        }
    </style>
</head>
<body>
    <div id="BaoCaoSoDangKy" class="smgrid" data-title="Văn bản đến" data-grid="grid_BaoCaoSoDKVanBanDen"
        data-form="/iVBDH/VanBanDen/pFormVanBanDen.aspx" data-formwidth="1200" data-action="/Vbden/api/VanBanDen/VBDGetListJsonGroupSolr" data-view="/Vbden/VanBanDen/pViewVanBanDen.aspx">
        <div class="zone_search" id="baocao">
            <div class="form-horizontal">
                <input type="hidden" name="ItemID" value="<%=ItemID %>" />
                <input type="hidden" name="CurentUser" value="<%=CurentUser.Title %>" />
                <div class="title_icon">
                </div>
                   <div class="form-group row text-search align-items-center">

                    <label class="col-sm-2 control-label">Tháng:</label>
                    <div class="col-sm-2">
                        <select class="form-control" id="ThangBaoCao" name="ThangBaoCao">
                            <option value="01" <%=DateTime.Now.ToString("MM") == "01" ? "selected" : "" %>>Tháng 1</option>
                            <option value="02 " <%=DateTime.Now.ToString("MM") == "02" ? "selected" : "" %>>Tháng 2</option>
                            <option value="03 " <%=DateTime.Now.ToString("MM") == "03" ? "selected" : "" %>>Tháng 3</option>
                            <option value="04 " <%=DateTime.Now.ToString("MM") == "04" ? "selected" : "" %>>Tháng 4</option>
                            <option value="05 " <%=DateTime.Now.ToString("MM") == "05" ? "selected" : "" %>>Tháng 5</option>
                            <option value="06 " <%=DateTime.Now.ToString("MM") == "06" ? "selected" : "" %>>Tháng 6</option>
                            <option value="07 " <%=DateTime.Now.ToString("MM") == "07" ? "selected" : "" %>>Tháng 7</option>
                            <option value="08 " <%=DateTime.Now.ToString("MM") == "08" ? "selected" : "" %>>Tháng 8</option>
                            <option value="09 " <%=DateTime.Now.ToString("MM") == "09" ? "selected" : "" %>>Tháng 9</option>
                            <option value="10 " <%=DateTime.Now.ToString("MM") == "10" ? "selected" : "" %>>Tháng 10</option>
                            <option value="11" <%=DateTime.Now.ToString("MM") == "11" ? "selected" : "" %>>Tháng 11</option>
                            <option value="12 " <%=DateTime.Now.ToString("MM") == "12" ? "selected" : "" %>>Tháng 12</option>
                        </select>
                    </div>

                    <label class="col-sm-2 control-label">Năm:</label>
                    <div class="col-sm-2">
                        <input type="text" name="SearchNam" id="SearchNam" class="form-control " placeholder="Nhập năm" value="<%=DateTime.Now.ToString("yyyy") %>" />
                    </div>

                </div>
                <div class="form-group row text-search align-items-center">

                    <label class="col-sm-2 control-label">Đơn Vị:</label>
                    <div class="col-sm-2">
                        <select class="form-control" id="LoaiDV" name="LoaiDV">
                            <option value="all">Tất cả</option>
                            <option value="PD">Tổng Công ty Điện lực TP Hà Nội</option>
                            <option value="PD0600">Công ty Điện lưc Thanh Trì</option>
                            <option value="PD1400">Công ty Điện lực Long Biên</option>
                            <option value="P">Tập đoàn Điện Lực Việt Nam</option>
                            <option value="PD0700">Công ty Điện lực Gia Lâm</option>
                            <option value="PD0800">Công ty Điện lực Đông Anh</option>
                            <option value="PD0900">Công ty Điện lực Sóc Sơn</option>
                            <option value="PD1800">Công ty Điện lực Chương Mỹ</option>
                            <option value="PD2000">Công ty Điện lực Thường Tín</option>
                            <option value="PD2400">Công ty Điện lực Mỹ Đức</option>
                            <option value="PD2600">Công ty Điện lực Phúc Thọ</option>
                            <option value="PD2500">Công ty Điện lực Phú Xuyên</option>
                            <option value="PD2800">Công ty Điện lực Thanh Oai</option>
                            <option value="PD2900">Công ty Điện lực Ứng Hòa</option>
                            <option value="PD0100">Công ty Điện lực Hoàn Kiếm </option>
                            <option value="PD1500">Công ty Điện lực Mê Linh </option>
                            <option value="PD1700">Công ty Điện lực Sơn Tây</option>
                            <option value="PD1900">Công ty Điện lực Thạch Thất </option>
                            <option value="PD2100">Công ty Điện lực Ba Vì</option>
                            <option value="PD2200">Công ty Điện lực Đan Phượng </option>
                            <option value="PD2300">Công ty Điện lực Hoài Đức</option>
                            <option value="PD2700">Công ty Điện lực Quốc Oai </option>
                            <option value="PD0400">Công ty Điện lực Đống Đa</option>
                            <option value="PD0500">Công ty Điện lực Nam Từ Liêm</option>
                            <option value="PD0200">Công ty Điện lực Hai Bà Trưng </option>
                            <option value="PD1000">Công ty Điện lực Tây Hồ </option>
                            <option value="PD1100">Công ty Điện lực Thanh Xuân</option>
                            <option value="PD1200">Công ty Điện lực Cầy giấy</option>
                            <option value="PD3000">Công ty Điện lực Bắc Từ Liêm</option>
                            <option value="PD1300">Công ty Điện lực Hoàng Mai</option>
                            <option value="PD0300">Công ty Điện lực Ba Đình</option>
                            <option value="PD1600">Công ty Điện lực Hà Đông </option>
                            <option value="PD6900">Tổng Công ty Điện lực TP Hà Nội </option>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-primary thongke">Thống kê</button>
                        <button type="button" class="btn btn-primary" id="btnExel">Excel</button>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="mainContentBaoCao">
            <div style="">
                <div style="width: 30%; float: left; text-align:center">
                      <p style="font-size: 10pt; width: 100%"><b class="TieuDeTrai1"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b class="TieuDeTrai2"></b></p>
                </div>
                <div style="width: 55%; float: left; text-align:center">
                    <p style="font-size: 12pt; width: 100%"><b class="Title"></b></p>
                    <p style="font-size: 12pt; width: 100%"><b>Tháng <i class="Thang"></i>   năm <i class="Nam"></i></b></p>
                </div>
                <div style="width: 15%; float: left; text-align:center">
                      <p style="font-size: 12pt; width: 100%"><p class="LoaiBM"></p></p>
                     <p  style="font-size: 10pt; width: 100%" class="DVTinh"></p>
                </div>
              

            </div>
            <div class="smtable_header" style="float:left;overflow-x:scroll" >
                <table class="smtable" style="width:150%" >
                    <thead>
                        <tr>
                            <th style="width: 30%"rowspan="3">Tên công trình</th>
                            <th style="width: 15%;" rowspan="3">Mã công trình</th>
                            <th style="width: 15%;" rowspan="3">Dự toán được duyệt</th>                             
                            <th style="width: 15%;" rowspan="3">KH duyệt năm nay</th>                             
                            <th style="width: 15%;" rowspan="3">Số dư đầu năm</th>                             
                            <th style="width: 60%;" colspan="6">Đã chi năm nay</th>                             
                            <th style="width: 50%;" colspan="5">Lũy kế đã chi</th>                             
                            <th style="width: 15%;" rowspan="3">Ghi chú</th>                             
                            <th style="width: 15%;" rowspan="3">Tổng hợp chung toàn Tổng công ty (trong đó chi tiết của từng đơn vị từng công trình )</th>                             
                            <th style="width: 15%;" rowspan="3">So Sánh % tự thực hiện và thuê ngoài so vơi tổng giá trị kế hoạch giao</th>                             
                        </tr>
                        <tr>                              
                            <th rowspan="2">Trong kỳ báo cáo</th>
                            <th colspan="5">Lũy kế từ đầu năm báo cáo</th>
                            <th rowspan="2">Vật liệu</th>
                            <th rowspan="2">Nhân công</th>
                            <th rowspan="2" >Máy thi công</th>
                            <th rowspan="2">Chi phí khác</th>
                            <th rowspan="2">Cộng</th>
                        </tr>
                        <tr>                              
                            <th >Vật liệu</th>
                            <th >Nhân công</th>
                            <th  >Máy thi công</th>
                            <th >Chi phí khác</th>
                            <th >Cộng</th>
                        </tr>
                    </thead>
                    <tbody class="clsContent">
                       
                    </tbody>
                </table>
            </div>
              <div class="chekdata mt-3" style="text-align: center">
                <p style="font-size: 12pt; width: 100%">Không có bản ghi </p>
            </div>
            <script id="javascriptTemplate" type="text/x-kendo-template">
            <tr class="clsvbd" data-id="">
                 <td style="width: 30%" >#=TenCongTrinh !=null ? TenCongTrinh : ""#</td>
                 <td style="width: 15%" >#=MaCongTrinh != null ? MaCongTrinh: ""#</td>
                 <td style="width: 15%" >#=DuAnDuocDuyet != null ? DuAnDuocDuyet :""#</td>
                 <td style="width: 15%" >#=KHDuyetNamNay !=null ? KHDuyetNamNay : "" #</td>
                 <td style="width: 15%" >#=SoDuDauNam !=null ? SoDuDauNam : "" #</td>
                 <td style="width: 10%" >#=DaTriNamNayTrongKyBaoCao !=null ? DaTriNamNayTrongKyBaoCao :""#</td>
                 <td style="width: 10%" >#=VatLieuTuDauNam != null ? VatLieuTuDauNam : "" #</td>
                 <td style="width: 10%" >#=NhanCongTuDauNam != null ? NhanCongTuDauNam :""#</td>
                 <td style="width: 10%" >#=MayThiCongTuDauNam != null ? MayThiCongTuDauNam : ""#</td>
                 <td style="width: 10%" >#=KhacTuDauNam != null ? KhacTuDauNam : "" #</td>
                 <td style="width: 10%" >#=CongTuDauNam != null ? CongTuDauNam : ""#</td>
                 <td style="width: 10%" >#=VatLieuLKDC != null ? VatLieuLKDC :""#</td>
                 <td style="width: 10%" >#=NhanCongLKDC != null ? NhanCongLKDC : ""#</td>
                 <td style="width: 10%" >#=MayThiCongLKDC !=null ? MayThiCongLKDC : ""#</td>
                 <td style="width: 10%" >#=KhacLKDC !=null ? KhacLKDC :""#</td>
                 <td style="width: 10%" >#=CongLKDC !=null ? CongLKDC : ""#</td>
                 <td style="width: 15%" >#=GhiChu !=null ? GhiChu :""#</td>
                 <td style="width: 15%" >#=TongHopChung !=null ? TongHopChung :""#</td>
                 <td style="width: 15%" >#=SoSanhPhanTram !=null ? SoSanhPhanTram :""#</td>
            </tr>
            </script>
           
            <div style="">

               
                <div style="width: 33%; float: right; text-align: center; margin-top: 15px;">
                    <p style="width: 100%">  Hà nội, ngày <i class="Ngay"></i> tháng <i class="Thang"></i>năm <i class="Nam"></i></p>
                    <p style="width: 100%">  GIÁM ĐỐC</p>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <p style="font-size: 12pt; width: 100%"><p class="NguoiKy"></p></p>
                    <p style="font-size: 12pt; width: 100%"><p class="">(Ký, họ tên, đóng dấu)</p></p>
                </div>
                <div style="width: 33%; float: right; text-align: center; margin-top: 15px;">
                     <br />
                    <p style="width: 100%">KẾ TOÁN TRƯỞNG  </p>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <p style="font-size: 12pt; width: 100%"><p class="KeToanTruong"></p></p>
                      <p style="font-size: 12pt; width: 100%"><p class="">(Ký, họ tên)</p></p>
                </div>
                 <div style="width: 33%; float: right; text-align: center; margin-top: 15px;">
                      <br />
                    <p style="width: 100%"> NGƯỜI LẬP BIỂU </p>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <p style="font-size: 12pt; width: 100%"><p class="NguoiLapBieu"></p></p>
                       <p style="font-size: 12pt; width: 100%"><p class="">(Ký, họ tên)</p></p>
                </div>
                
            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        ThongKe();
        $(".thongke").click(function () {
            ThongKe();
        });
        $(".input-datetime").daterangepicker({
            singleDatePicker: true,
            locale: { format: 'DD/MM/YYYY' }
        });

        $("#btnExel").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BanKeHoach/QUERYDATA_ERP_BCCongTrinhSCL?excel=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");
        });
        $("#btnword").click(function () {
            var $datathongke = $(".zone_search").siSerializeDivFrm();
            var str = jQuery.param($datathongke);
            var Url = "/VIPortalAPI/api/BanKeHoach/QUERYDATA_ERP_BCCongTrinhSCL?word=1&" + str + "&TitleBC=" + $("#TitleBC").text();
            my_window = window.open(Url, "mywindow1");

        });
    });
    function ThongKe() {
        $("#tungay").text($("#SearchTuNgay").val());
        $("#denngay").text($("#SearchDenNgay").val());
        var data = $(".zone_search").siSerializeArray();
        loading();
        $.post("/VIPortalAPI/api/BanKeHoach/QUERYDATA_ERP_BCCongTrinhSCL?", data, function (data) {

            $(".Title").text(data.Title);
            $(".LoaiBM").text(data.LoaiBM);
            $(".TieuDeTrai1").text(data.TieuDeTrai1);
            $(".TieuDeTrai2").text(data.TieuDeTrai2);
            $(".NguoiKy").text(data.NguoiKy);
            $(".Thang").text(data.Thang);
            $(".Nam").text(data.Nam);
            $(".NguoiLapBieu").text(data.NguoiLapBieu);
            $(".KeToanTruong").text(data.KeToanTruong);
            $(".DVTinh").text(data.DVTinh);



            var htmlTEmp = '';
            var templatehtmltr = kendo.template($("#javascriptTemplate").html());
            $(".mainContentBaoCao .clsContent").html("");
            if (data.data.length > 0) {
                $(".chekdata").hide();
                for (var i = 0; i < data.data.length; i++) {
                    data.data[i]["STT"] = i + 1;
                    htmlTEmp += templatehtmltr(data.data[i]);
                }

                $(".mainContentBaoCao .clsContent").append(htmlTEmp);
                $('#tongbanghi').text("Có tổng số: " + data.data.length + " bản ghi");

            } else {
                $(".chekdata").show();
            }
            $(".smtable thead").css('width', 'calc( 100% - ' + getScrollBarWidth() + 'px )');


            $(".clsvbd").css('cursor', 'pointer');
            
        });
        Endloading();
    }

</script>
</html>




