﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData.VanBanTaiLieu;

namespace VIPortal.APP.UserControls.Public.Portal.Documents
{
    public partial class UC_Documents_Detail : BaseUC_Web
    {
        public VanBanTaiLieuItem oVanBanTaiLieuItem { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if(ItemID > 0)
            {
                VanBanTaiLieuDA oVanBanTaiLieuDA = new VanBanTaiLieuDA(UrlSite);
                oVanBanTaiLieuItem = oVanBanTaiLieuDA.GetByIdToObject<VanBanTaiLieuItem>(ItemID);
            }
        }
    }
}