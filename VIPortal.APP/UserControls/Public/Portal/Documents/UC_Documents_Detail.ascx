﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_Documents_Detail.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.Documents.UC_Documents_Detail" %>

<style>
    .vanban-detail table tr .th {
        width: 150px;
        font-weight: 600;
        text-align: right
    }

    .vanban-detail ul {
        padding: 0;
    }

        .vanban-detail ul li {
            list-style-type: none;
        }
</style>

<section>

    <div class="vanban-detail bg-color-box-items box-shadow-basic">
        <div class="header-notification-detail">
            <p class="font-size-14 color-table-meeting pl-3 pt-2"><a href="<%=UrlSite %>/Pages/HomePortal.aspx">Trang chủ</a> - <a href="<%=UrlSite %>/Pages/vanbantailieu.aspx">Văn bản tài liệu</a>  - <a href="#">Chi tiết văn bản</a></p>
        </div>
        <%if(ItemID > 0){%>
        <div class="main-notification-detail display-flex pl-3 pr-3">
            <table class="table table-bordered">
                <tr>
                    <td class="th">Số ký hiệu</td>
                    <td><%=oVanBanTaiLieuItem.VBSoKyHieu %></td>
                </tr>
                <tr>
                    <td class="th">Tên văn bản</td>
                    <td><%=oVanBanTaiLieuItem.Title %></td>
                </tr>
                <tr>
                    <td class="th">Ngày ban hành</td>
                    <td><%=string.Format("{0:dd/MM/yyyy}", oVanBanTaiLieuItem.VBNgayBanHanh) %></td>
                </tr>
                <tr>
                    <td class="th">Ngày hiệu lực</td>
                    <td><%=string.Format("{0:dd/MM/yyyy}", oVanBanTaiLieuItem.VBNgayHieuLuc) %></td>
                </tr>
                <tr>
                    <td class="th">Trích yếu</td>
                    <td><%=oVanBanTaiLieuItem.VBTrichYeu %></td>
                </tr>
                <tr>
                    <td class="th">Người ký</td>
                    <td><%=oVanBanTaiLieuItem.VBNguoiKy %></td>
                </tr>

                <tr>
                    <td class="th">Loại tài liệu</td>
                    <td><%=oVanBanTaiLieuItem.DMLoaiTaiLieu.LookupValue %></td>
                </tr>
                <tr>
                    <td class="th">Cơ quan ban hành</td>
                    <td><%=oVanBanTaiLieuItem.DMCoQuan.LookupValue %></td>
                </tr>

                <tr>
                    <td class="th">File đính kèm</td>
                    <td>
                        <%for(int i=0; i< oVanBanTaiLieuItem.ListFileAttach.Count;i++){ %>
                        <div class="file">
                            <ul>
                                <li><i class="fas fa-download" style="color: #26b2fa"></i><a href="<%=oVanBanTaiLieuItem.ListFileAttach[i].Url %>" style="font-size: 13px; color: #337ab7;"><%=oVanBanTaiLieuItem.ListFileAttach[i].Name%></a></li>
                            </ul>
                        </div>
                        <%} %>
                    </td>
                </tr>
                <tr>
                    <td class="th">Chia sẻ</td>
                    <td><span>
                        <a class="send-mail" style="cursor: pointer;">
                            <i class="fa fa-envelope" aria-hidden="true" style="margin-right: 2px"></i>Gửi email 
                        </a>
                    </span></td>
                </tr>
            </table>
        </div>
        <%}else{%>
        Không có bản ghi phù hợp với ID!!!
    <%} %>
    </div>
    <!-- Modal send mail-->
    <div class="modal" tabindex="-1" role="dialog" id="sendmail">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" style="font-weight: bold; font-size: 15px">Gửi mail</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="f-send">
                        <input type="hidden" name="link" id="link" value="https://<%=HttpContext.Current.Request.Url.Authority%>/Pages/chitietvanban.aspx?ItemID=<%=oVanBanTaiLieuItem.ID %>" />
                        <div class="form-group">
                            <label for="email">Email người nhận:</label>
                            <input type="text" class="form-control" name="lmailto" id="lmailto">
                        </div>
                        <div class="form-group">
                            <label for="pwd">Tiêu đề email:</label>
                            <input type="text" class="form-control" name="subject" id="subject">
                        </div>
                        <div class="form-group">
                            <label for="pwd">Nội dung mail:</label>
                            <textarea rows="3" class="form-control" name="content" id="contentMail"></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            <button type="button" class="btn-primary" id="btnSendmail">Gửi email</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {

        function loopLi() {
            setInterval(function () { // this code is executed every 500 milliseconds:
                var current = '/Pages/vanbantailieu.aspx';
                var actived = false;
                $('#items_sliderbar li a').each(function () {
                    var $this = $(this);
                    // if the current path is like this link, make it active
                    if ($this.attr('href').indexOf(current) !== -1) {
                        $this.closest("li").addClass('active');
                        actived = true;
                    }
                });
                if (actived) return;
            }, 500);
        }

        $(loopLi);

        $(document).on('click', ".send-mail", function (event) {
            $('#sendmail').modal('show');
        });
        var strTemp = "Tên tài liệu: <%=oVanBanTaiLieuItem.Title%>." + "\n";
        $("#contentMail").val(strTemp);
        $("#btnSendmail").click(function () {
            debugger
            //$("#f-send").submit();  
            var subject = $("#subject").val();
            var lmailto = $("#lmailto").val();
            var content = $("#contentMail").val();
            var link = $("#link").val().toString();
            if (lmailto == "" && subject == "") {
                BootstrapDialog.show({
                    title: "Thông báo",
                    message: "Bạn cần nhập đầy đủ thông tin"
                });
                return false;
            } else {
                loading();
                $.post("/UserControls/AdminCMS/VanBanTaiLieu/pAction.ashx", "do=SENDMAIL&lmailto=" + lmailto + "&subject=" + subject + "&content=" + content + "&link=" + link, function (result) {
                    if (result.State == 2) {
                        $("#f-send").trigger('reset');
                        BootstrapDialog.show({
                            title: "Lỗi",
                            message: result.Message
                        });
                    }
                    else {
                        openDialogMsg("Thông báo", result.Message);
                        Endloading();
                        $("#f-send").trigger('reset');
                        $('#sendmail').modal('hide');
                    }
                }).always(function () { });
            }
        });
    });
</script>
