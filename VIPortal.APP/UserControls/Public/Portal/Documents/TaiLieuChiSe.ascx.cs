﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData;

namespace VIPortal.APP.UserControls.Public.Portal.Documents
{
    public partial class TaiLieuChiSe : BaseUC_Web
    {
        public List<LGroupItem> lstGroupItem { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            lstGroupItem = new List<LGroupItem>();
            LGroupDA lGroupDA = new LGroupDA();
            int PhongBanId = CurentUser.UserPhongBan.ID;
            while (PhongBanId > 0)
            {
                LGroupItem oLGroupItem = lGroupDA.GetByIdToObject<LGroupItem>(PhongBanId);
                lstGroupItem.Add(oLGroupItem);
                if (oLGroupItem.GroupParent != null)
                {
                    PhongBanId = oLGroupItem.GroupParent.LookupId;
                }
                else
                {
                    PhongBanId = 0;
                }
            }
            
        }
    }
}