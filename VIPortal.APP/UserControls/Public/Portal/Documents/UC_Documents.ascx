﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_Documents.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.Documents.UC_Documents" %>
<style type="text/css">
    #frmSearchVB {
        padding-left: 15px;
    }

        #frmSearchVB .input-datetime {
            background: url('/Content/dist/img/calendar_bg.png') no-repeat;
            background-position: right 10px center;
            padding-right: 25px;
            width: 160px;
        }
</style>
<section id="lstvbtl" data-role="fullGrid" class="Document">
    <script id="jsTemplateVBTL1" type="text/x-kendo-template">
                    <tr class="font-size-14px">
                        <td class="font-weight-bold color-table-meeting">#=STT#</td>
                        <td style="text-align:left" ><a href="<%=UrlSite %>/Pages/chitietvanban.aspx?ItemID=#=ID#" class="color-table-meeting">#=Title#</td>
                        <td>#=VBSoKyHieu#</td>
                        
                        <td>#=DMLoaiTaiLieu.Title#</td>                        
                        <td>#=VBNguoiKy != null ? VBNguoiKy : ""#</td>
                        <td>#=DMCoQuan.Title#</td>
                        <td>#=formatDate(VBNgayBanHanh)#</td>
                        <td>#=formatDate(VBNgayHieuLuc)#</td>   
                        <td style="width:190px;text-align:left">#=LichGhiChu#</td>
                    </tr>
    </script>
    <div class="briefings bg-color-box-items">
        <div class="header-briefings">
            <p class="font-size-14 color-table-meeting pl-3 pt-2"><span data-lang="TrangChu">Trang chủ</span> - <span data-lang="VBTaiLieu">Văn bản tài liệu</span></p>
        </div>

        <div action="" class="display-flex-righteous" id="frmSearchVB" role="search">
            <div class="items-form">
                <label for="" class="font-size-16 color-table-meeting font-weight-bold" data-lang="SoKyHieu">
                    Số kí hiệu</label><br>
                <input type="text" id="VBSoKyHieu" class="input-text" data-lang="SoKyHieu" name="VBSoKyHieu" placeholder="Nhập số ký hiệu" />
            </div>
            <div class="items-form">
                <label for="" class="font-size-16 color-table-meeting font-weight-bold" data-lang="TGBanHanh">
                    Thời gian ban hành</label><br>
                <input type="text" id="TuNgay_VBNgayBanHanh" class="input-datetime" data-lang="TuNgay" placeholder="Từ ngày" name="TuNgay_VBNgayBanHanh">
            </div>
            <div class="items-form">
                <label for="" class="font-size-16 color-table-meeting font-weight-bold" style="color: #fff" data-lang="TGBanHanh">
                    Thời gian ban hành</label><br>
                <input type="text" id="DenNgay_VBNgayBanHanh" class="input-datetime" data-lang="DenNgay" placeholder="Đến ngày" name="DenNgay_VBNgayBanHanh">
            </div>
            <div class="items-form">
                <label for="" class="font-size-16 color-table-meeting font-weight-bold" data-lang="TuKhoa">
                    Từ khóa</label><br>
                <input type="text" name="Keyword" id="Keyword" data-lang="TuKhoa" placeholder="Từ khóa" />
                <input type="hidden" name="SearchInSimple" id="SearchInSimple" value="VBTrichYeu,Title,VBSoKyHieu" />
            </div>
            <div class="items-form">
                <label for="" class="font-size-16 color-table-meeting font-weight-bold" data-lang="DanhMuc">
                    Danh mục</label><br>
                <select data-selected="" data-url="/UserControls/AdminCMS/LoaiTaiLieu/pAction.asp?do=QUERYDATA&moder=1" name="DMLoaiTaiLieu" id="DMLoaiTaiLieuSearch" data-place="Chọn danh mục" class="form-control">
                </select>
            </div>
            <div class="btn">
                <label for=""></label>
                <br>
                <button data-toggle="tooltip" data-placement="top" title="Xóa lọc tất cả" class="reset_data"><i class="fas fa-sync-alt"></i></button>
                <button type="button" class="btnsearch" data-lang="TimKiem">Tìm kiếm</button>
            </div>
        </div>
        <div class="text-center table-briefings ml-3 mr-3 pt-5 pb-3 box-shadow-basic" id="lich_div_parent">
            <table class="table table-bordered">
                <thead>
                    <tr class="font-size-16 color-table-meeting font-weight-bold bg-color-header-meeting-tr">
                        <th data-lang="STT">STT</th>
                        <th data-lang="TenVB">Tên văn bản</th>
                        <th data-lang="SoKyHieu">Số kí hiệu</th>
                        <th data-lang="LoaiTaiLieu">Loại tài liệu</th>
                        <th data-lang="NguoiKy">Người ký</th>
                        <th data-lang="CoQuanBanHanh">Đơn vị phát hành</th>
                        <th data-lang="NgayBanHanh">Ngày ban hành</th>
                        <th data-lang="NgayHieuLuc">Ngày hiệu lực</th>
                        <th data-lang="TrichYeu">Ghi chú</th>
                    </tr>
                </thead>
                <tbody role="grid">
                    <tr>
                    </tr>
                </tbody>

            </table>
            <div class="clspaging"></div>
        </div>
    </div>

</section>
<script type="text/javascript">
    $(document).ready(function () {
        SetConfigLang('<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>', 'Document');
        //DMLoaiTaiLieuSearch
        $("#DMLoaiTaiLieuSearch").smSelect2018V2({
            dropdownParent: "#lstvbtl"
        });
        $("#lstvbtl").smGrid2018({
            "TypeTemp": "kendo",
            "UrlPost": "/VIPortalAPI/api/VanBanTaiLieu/QUERYDATA",
            ConfigLanguages: '<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>',
            hasSTT: true,
            odata: { "do": "QUERYDATA" },
            template: "jsTemplateVBTL1",
            dataBound: function (e) {

            },
        });
    });
</script>

