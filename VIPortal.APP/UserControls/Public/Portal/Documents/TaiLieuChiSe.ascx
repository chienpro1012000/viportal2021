﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TaiLieuChiSe.ascx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.Documents.TaiLieuChiSe" %>
<style type="text/css">
    #frmSearchVB {
        padding-left: 15px;
    }

    #frmSearchVB .input-datetime {
        background: url('/Content/dist/img/calendar_bg.png') no-repeat;
        background-position: right 10px center;
        padding-right: 25px;
    }

    
</style>
<section id="lstvbtl" data-role="fullGrid" >
    <script id="jsTemplateVBTL1" type="text/x-kendo-template">
                    <tr class="font-size-12px">
                        <td>#=Title#</td>
                        <td>#=DMTaiLieu.Title#</td>                        
                        <td>#=CreatedUser.Title#</td>
                        <td>#=fldGroup.Title#</td>
                        <td>#=formatDate(Created)#</td>    
                        <td> # for (var i = 0; i < ListFileAttach.length; i++) { #
                               <a target="_blank" href="#=ListFileAttach[i].Url #">
                               #=ListFileAttach[i].Name# </a> <br>
                            # } #</td>
                        <td>
                                #if(CreatedUser.ID == <%=CurentUser.ID %>) {# <a href="javascript:EditTaiLieu(#=ID#);"><i class="fas fa-edit"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:DeleteTaiLieu(#=ID#);"><i class="far fa-trash-alt"></i></a>#}#
                        </td>
                    </tr>
        </script>
    <div  class="briefings bg-color-box-items TaiLieuChiSe">
        <div class="header-briefings">
            <p class="font-size-14 color-table-meeting pl-3 pt-2"><span data-lang="TrangChu">Trang chủ</span> - <span data-lang="TLChiaSe">Tài liệu chia sẻ</span></p>
        </div>

        <div action="" class="display-flex-righteous formsearchpublic" id="frmSearchVB" role="search">
            <div class="items-form" style="width:20%;">
                <label for="" class="font-size-16 color-table-meeting font-weight-bold" data-lang="TuKhoa">
                    Từ khóa</label><br>
                <input type="text" name="Keyword" id="Keyword" data-lang="TuKhoa" placeholder="Từ khóa" />
                <input type="hidden" name="SearchInSimple" id="SearchInSimple" value="Title" />
                <input type="hidden" name="lstfldGroup" id="lstfldGroup" value="<%=string.Join(",", lstGroupItem.Select(x=>x.ID)) %>" />
            </div>
            <div class="items-form form-group" style="width:20%;">
                <label for="" class="font-size-16 color-table-meeting font-weight-bold" data-lang="LoaiDuLieu">
                    Loại dữ liệu</label><br>
                 <select data-selected="" data-url="/UserControls/AdminCMS/DanhMucChiaSeTL/pAction.asp?do=QUERYDATA" data-place="Chọn Danh mục" name="DMTaiLieu" id="DMTaiLieuSearch" class="form-control"></select>
            </div>
            <div class="btn">
                <label for=""></label>
                <br>
                <button data-toggle="tooltip" data-placement="top" title="Xóa lọc tất cả"  class="reset_data"><i class="fas fa-sync-alt"></i></button>
                <button type="button" class="btnsearch" data-lang="TimKiem">Tìm kiếm</button>
                <button type="button" id="btnThemMoiPhongBan" class="btn  btn-search-meeting" data-lang="ThemMoi">Thêm mới</button>
            </div>
        </div>
        <div class="table-briefings ml-3 mr-3 pt-5 pb-3 box-shadow-basic" id="lich_div_parent">
            <table class="table table-bordered">
                <thead>
                    <tr class="font-size-15 color-table-meeting font-weight-bold bg-color-header-meeting-tr">
                        <th data-lang="TieuDe">Tiêu đề</th>
                        <th data-lang="LoaiDuLieu">Loại dữ liệu</th>
                        <th data-lang="NguoiCap">Người cấp</th>
                        <th data-lang="PhamViChiaSe">Phạm vi chia sẻ</th>
                        <th data-lang="NgayCap" style="width:100px;">Ngày cấp</th>
                        <th data-lang="FileDinhKem">File đính kèm</th>
                        <th data-lang="ThaoTac" style="width:90px;">Thao tác</th>
                    </tr>
                </thead>
                <tbody role="grid">
                    <tr>
                    </tr>
                </tbody>

            </table>
            <div class="clspaging"></div>
        </div>
    </div>

</section>
<script type="text/javascript">
    function EditTaiLieu(itemid) {
        openDialog("Thêm mới tài liệu", "/UserControls/AdminCMS/TaiLieuChiaSe/pFormTaiLieuChiaSe.aspx", { ItemID: itemid }, 800);
    }
    function DeleteTaiLieu(itemid) {
        //openDialog("Thêm mới tài liệu", "/UserControls/AdminCMS/TaiLieuChiaSe/pFormTaiLieuChiaSe.aspx", { ItemID: itemid }, 800);
        //DeleteFromDialogFunc(urlAction, odata, title, funCallBack)
        DeleteFromDialogFunc("/UserControls/AdminCMS/TaiLieuChiaSe/pAction.asp", { do: "DELETE", ItemID: itemid }, "Xóa tài liệu", function () {
            $("#lstvbtl").data("smGrid2018").RefreshGrid();
        });
    }
    $(document).ready(function () {
        SetConfigLang('<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>', 'TaiLieuChiSe');
        //DMTaiLieuSearch
        $("#DMTaiLieuSearch").smSelect2018V2({
            dropdownParent: "#lstvbtl"
        });
        $("#btnThemMoiPhongBan").click(function () {
            openDialog("Thêm mới tài liệu", "/UserControls/AdminCMS/TaiLieuChiaSe/pFormTaiLieuChiaSe.aspx", {}, 800);
        });
        $("#lstvbtl").smGrid2018({
            "TypeTemp": "kendo",
            "UrlPost": "/VIPortalAPI/api/VanBanTaiLieu/QUERYDATA_TAILIEUCHIASE",
            ConfigLanguages: '<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>',
            hasSTT: true,
            odata: { "do": "QUERYDATA" },
            template: "jsTemplateVBTL1",
            dataBound: function (e) {

            },
        });
    });
</script>

