﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_Meetings.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.Meetings.UC_Meetings" %>
<style>
    .display-flex-righteous {
        justify-content: normal !important;
        padding-bottom: 15px;
    }

    .time {
        color: red;
    }

    .details-control {
        cursor: pointer;
        color: cornflowerblue;
        text-align: left !important;
    }

    .rowdetail table {
        background-color: #f1d293;
    }

    .lich_div_1 {
        text-align: center;
    }

    .main-calendar-datepicke .main-news-event {
        margin-top: 15px;
    }

    .fa-check-circle {
        color: red;
    }
    .LichThayDoi,
    .LichBoSung,
    .LichHoan {
        color: red;
        padding: 0 2px;
    }

    .zonebutton {
        margin-left: auto;
    }

        .zonebutton .btn-search-meeting {
            margin-bottom: 0px;
            margin-right: 30px;
            padding: 8px 11px;
        }

    span.clsTipLD, span.clsTipChuTri {
        display: grid;
        border-bottom: 1px solid #333;
    }
    span.LichTraVeTuLD {
        color:red;
    }
    .Lich_ChiTiet a svg {
    /* padding-right: 3px; */
    /* width: 20px; */
    display: block;
    margin-bottom: 3px;
    margin: 5px auto;
}
</style>
<section class="pb-1 mt-1 Meetings" id="lichhop" data-role="fullGrid">
    <div class="header-calendar-datepicke" style="background-color: #fff;" id="content">
        <div class="left_header_calendar pt-3 pb-3 pl-4">
            <div class="list_calendar">

                <div class="item_li_1 active" data-tab-target="#tab-1" style="cursor: pointer;">
                    <a href="#tab-1" onclick="show('tab-1');" class="font-size-14" data-lang="LichTongCongTy">Lịch tổng công ty</a>
                </div>
                <div class="item_li_1" data-tab-target="#tab-2" style="cursor: pointer;">
                    <a href="#tab-2" onclick="show('tab-2');" data-lang="DonVi">Đơn vị</a>
                </div>
                <div class="item_li_1" id="DivLichphong" data-tab-target="#tab-3" style="cursor: pointer;">
                    <a href="#tab-3" onclick="show('tab-3');" data-lang="Phong">Phòng</a>
                </div>
                <div class="item_li_1" data-tab-target="#tab-4" style="cursor: pointer;">
                    <a href="#tab-4" onclick="show('tab-4');" data-lang="CaNhan">Cá nhân</a>
                </div>
                <div class="zonebutton">
                    <button type="button" id="btnThemMoiPhongBan" class="btn  btn-search-meeting" data-lang="DangKyLich">Đăng ký lịch Đơn vị/Phòng</button>
                </div>
            </div>
        </div>
    </div>
    <script id="jsDetail" type="text/x-kendo-template">
        <tr class="rowdetail">
                        <td colspan="7">
                            <table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px; width:100%;">
                                <tr>
                                    <td class="text-right" style="width:20%;">Ghi chú</td>
                                    <td class="text-left">#=LichGhiChu#</td>
                                </tr>
                                <tr>
                                    <td class="text-right">Thành phần tham gia</td>
                                    <td class="text-left">#=LichThanhPhanThamGia.map(x=>x.LookupValue).join(", ")#</td>
                                </tr>
                                <tr>
                                    <td class="text-right">Nội dung chuẩn bị</td>
                                    <td class="text-left">#=NoiDungChuanBi#</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
    </script>
    <script id="jsDetailTapDoan" type="text/x-kendo-template">
        <tr class="rowdetail">
                        <td colspan="7">
                            <table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px; width:100%;">
                                <tr>
                                    <td class="text-right" style="width:20%;">Ghi chú</td>
                                    <td class="text-left">#=LichGhiChu#</td>
                                </tr>
                                <tr>
                                    <td class="text-right">Thành phần tham gia</td>
                                    <td class="text-left">#=LichThanhPhanThamGia.map(x=>x.LookupValue).join(", ")#</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
    </script>
    <script id="jsTemplateLichHop" type="text/x-kendo-template">
                    <tr class="font-size-14">
                        <td>#=Title#</td>
                        <td class="tghop color-table-meeting font-weight-bold">#=new Date(LichThoiGianBatDau).format('dd/MM (HH:mm)')# - #=new Date(LichThoiGianKetThuc).format('dd/MM (HH:mm)')#</td>
                        <td>#=LichDiaDiem#</td>
                        <td>#=LichLanhDaoChuTri.Title#</td>
                        <td class="details-control" data-ItemID="#=ID#">#if(LichNoiDung != null){##=LichNoiDung##}else{# #}#</td>
                    </tr>
    </script>

    <div class="main-calendar-datepicke mt-3" style="background-color: #fff;">

        <div id="tab-1" class="main-news-event" style="display: block;">
            <section class="pb-1 mt-1">
                <div id="lich_div_parent" data-role="fullGrid">
                    <div class="lich_div_1">
                        <font face="Tahoma" color="#0033CC" style="font-size: 17pt">
                            <b data-lang="LichTongCongTy">LỊCH TỔNG CÔNG TY</b>
                        </font>
                    </div>
                    <div class="today2">
                        <div style="display: inline;">
                            <a href="javascript:;" class="lnkLastMonth" data-lang="TuanTruoc">
                                <img src="/Content/themeV1/icon/left.gif" alt="left" class="next1 pr-2" border="0">Tuần trước</a>&emsp;&emsp;&emsp;
                            <a data-lang="Week"> Tuần thứ <a class="clsweekcur"> </a></a>
                            
                            &emsp;&emsp;&emsp;<a href="javascript:;" class="lnkNexkMonth" data-lang="TuanSau">Tuần sau<img src="/Content/themeV1/icon/right.gif" alt="left" class="pl-2 next1" border="0"></a>
                        </div>
                        <div class="detail-week" style="padding-top: 10px; font-weight: 600"></div>
                    </div>
                    <div class="display-flex-righteous formsearchpublic" id="frmSearchLichTCTY" role="search">
                        <div class="items-form" style="width: 20%">
                            <input type="text" name="keyword" data-lang="PlaceSearch" placeholder="Nhập từ khóa">
                            <input type="hidden" name="SearchInAdvance" value="Title,LichNoiDung,LichLanhDaoChuTri,LichDiaDiem,FullText,THANH_PHAN,CHU_TRI,CHUAN_BI">
                            <input type="hidden" name="p_week" class="week" value="" />

                        </div>
                        <div class="items-form">
                            <input type="text" class="input-datetime" name="TuNgay_LichHop" id="TuNgay_LichHopTCTY" data-lang="TuNgay" placeholder="Từ ngày">
                        </div>
                        <div class="items-form">
                            <input type="text" class="input-datetime" name="DenNgay_LichHop" id="DenNgay_LichHopTCTY" data-lang="DenNgay" placeholder="Đến ngày">
                        </div>
                        <div class="icon-button display-flex pl-2">
                            <button data-toggle="tooltip" data-placement="top" title="Xóa lọc tất cả" class="reset_data"><i class="fas fa-sync-alt"></i></button>
                            <button type="button" class="btn btnsearch btn-search-meeting" data-lang="TimKiem">Tìm kiếm</button>
                            <button type="button" id="btnDongBo" class="btn btnDongBo btn-search-meeting" data-lang="DongBo">Đồng Bộ</button>
                        </div>
                    </div>
                    <div class="lich_div_2">
                        <div style="width: 100%;" class="lichDiv">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tbody>
                                    <tr>
                                        <td style="">
                                            <div style="margin: 20px auto 10px; width: 100%;">
                                                <div style="background: rgb(198, 219, 255) no-repeat scroll left top; clear: both; display: block; -moz-background-clip: -moz-initial; -moz-background-origin: -moz-initial; -moz-background-inline-policy: -moz-initial; margin-bottom: 10px; font-size: 12px; font-family: Arial;">
                                                    <div style="clear: both; background-color: #e8eef7">
                                                        <table class="tblHeader" width="100%" border="0" cellpadding="1" cellspacing="1" style="background-color: #e8eef7; border: 1px solid #72a1de">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="date_left" valign="middle" width="7%" align="left" style="border-right: solid 1px #C6DBFF; background-color: #ffffcc;">
                                                                        <b data-lang="Thu/Ngay">Thứ/Ngày</b>
                                                                    </td>
                                                                    <td class="date_right" valign="middle" width="91%" align="left" style="background-color: #ffffcc;">
                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="date_right lich_Header" valign="middle" width="7%" align="center">
                                                                                        <b data-lang="ThoiGian">Thời gian</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="15%" align="center">
                                                                                        <b data-lang="DiaDiem">Địa điểm</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="" align="center">
                                                                                        <b data-lang="NoiDung">Nội dung</b>
                                                                                    </td>
                                                                                    
                                                                                    <td class="date_right lich_Header" valign="middle" width="15%" align="center">
                                                                                        <b data-lang="ChuTri/ChuanBi">Chủ trì/Chuẩn bị</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="15%" align="center">
                                                                                        <b data-lang="ThanhPhanThamGia">Thành phần tham gia TCT</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="15%" align="center">
                                                                                        <b data-lang="ThamGiaTaiDonVi">Tham gia tại Đơn vị</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="3%" align="center">
                                                                                        <b></b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="3%" align="center">
                                                                                        <b>
                                                                                            <input type="checkbox" name="CheckALL" class="CheckALLDonvi" value="1" /></b>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div style="clear: both;">
                                                        <table class="clscontentLich" width="100%" border="0" cellpadding="1" cellspacing="1">
                                                            <tbody role="grid">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>
                <script id="jsTemplateNgay" type="text/x-kendo-template">
                  <tr>
                                                        <td class="date_left" valign="middle" width="7%" align="left">
                                                            <font color="\#000000">
                                                                <b>#=Thu#</b><br>
                                                                <b>#=Ngay#
                                                                 </b>
                                                            </font>
                                                        </td>
                                                        <td class="date_right" valign="middle" width="91%" align="left">
                                                           #=Content#
                                                        </td>
                                                    </tr>
                </script>
            </section>
        </div>
        <div id="tab-2" class="main-news-event" style="display: none">
            <section class="pb-1 mt-1">
                <div id="lichDonVi_div_parent" data-role="fullGrid">
                    <div class="lich_div_1">
                        <font face="Tahoma" color="#0033CC" style="font-size: 17pt">
                            <b data-lang="LichDonVi">LỊCH ĐƠN VỊ</b>
                        </font>
                    </div>
                    <div class="today2">
                        <div style="display: inline;">
                            <a href="javascript:;" class="lnkLastMonth" data-lang="TuanTruoc">
                                <img src="/Content/themeV1/icon/left.gif" alt="left" class="next1 pr-2" border="0">Tuần trước</a>&emsp;&emsp;&emsp;
                            <a data-lang="Week"> Tuần thứ <a class="clsweekcur"> </a></a>
                            &emsp;&emsp;&emsp;<a href="javascript:;" class="lnkNexkMonth" data-lang="TuanSau">Tuần sau<img src="/Content/themeV1/icon/right.gif" alt="left" class="next1 pl-2" border="0"></a>
                        </div>
                        <div class="detail-week" style="padding-top: 10px; font-weight: 600"></div>
                    </div>
                    <div class="display-flex-righteous formsearchpublic" id="frmSearchLichDonVi" role="search">
                        <div class="items-form" style="width: 20%">
                            <input type="text" name="keyword" data-lang="PlaceSearch" placeholder="Nhập từ khóa">
                            <input type="hidden" name="SearchInAdvance" value="Title,LichNoiDung,LichLanhDaoChuTri,LichDiaDiem,FullText,THANH_PHAN,CHU_TRI,CHUAN_BI">
                            <input type="hidden" name="p_week" class="week" value="" />
                        </div>
                        <div class="items-form">
                            <input type="text" class="input-datetime" name="TuNgay_LichHop" id="TuNgay_LichHopDonVi" data-lang="TuNgay" placeholder="Từ ngày">
                        </div>
                        <div class="items-form">
                            <input type="text" class="input-datetime" name="DenNgay_LichHop" id="DenNgay_LichHopDonVi" data-lang="DenNgay" placeholder="Đến ngày">
                        </div>
                        <div class="icon-button display-flex pl-2">
                            <button data-toggle="tooltip" data-placement="top" title="Xóa lọc tất cả" class="reset_data"><i class="fas fa-sync-alt"></i></button>
                            <button type="button" class="btn btnsearch btn-search-meeting" data-lang="TimKiem">Tìm kiếm</button>

                        </div>
                    </div>
                    <div class="lich_div_2">
                        <div style="width: 100%;" class="lichDiv">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tbody>
                                    <tr>
                                        <td style="">
                                            <div style="margin: 20px auto 10px; width: 100%;">
                                                <div style="background: rgb(198, 219, 255) no-repeat scroll left top; clear: both; display: block; -moz-background-clip: -moz-initial; -moz-background-origin: -moz-initial; -moz-background-inline-policy: -moz-initial; margin-bottom: 10px; font-size: 12px; font-family: Arial;">
                                                    <div style="clear: both; background-color: #e8eef7">
                                                        <table class="tblHeader" width="100%" border="0" cellpadding="1" cellspacing="1" style="background-color: #e8eef7; border: 1px solid #72a1de">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="date_left" valign="middle" width="7%" align="left" style="border-right: solid 1px #C6DBFF; background-color: #ffffcc;">
                                                                        <b data-lang="Thu/Ngay">Thứ/Ngày</b>
                                                                    </td>
                                                                    <td class="date_right" valign="middle" width="91%" align="left" style="background-color: #ffffcc;">
                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="date_right lich_Header" valign="middle" width="7%" align="center">
                                                                                        <b data-lang="ThoiGian">Thời gian</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="15%" align="center">
                                                                                        <b data-lang="DiaDiem">Địa điểm</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="" align="center">
                                                                                        <b data-lang="NoiDung">Nội dung</b>
                                                                                    </td>
                                                                                    
                                                                                    <td class="date_right lich_Header" valign="middle" width="15%" align="center">
                                                                                        <b data-lang="ChuTri/ChuanBi">Chủ trì/Chuẩn bị</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="15%" align="center">
                                                                                        <b data-lang="ThanhPhanThamGia">Thành phần tham gia TCT</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="15%" align="center">
                                                                                        <b data-lang="ThamGiaTaiDonVi">Tham gia tại Đơn vị</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="3%" align="center">
                                                                                        <b>
                                                                                            <input type="checkbox" name="CheckALL" class="CheckALLDonvi" value="1" /></b>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div style="clear: both;">
                                                        <table class="clscontentLich" width="100%" border="0" cellpadding="1" cellspacing="1">
                                                            <tbody role="grid">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>
            </section>
        </div>
        <div id="tab-3" class="main-news-event" style="display: none">
            <!--Lịch phòng-->
            <section class="pb-1 mt-1">
                <div id="lichPhongBan_div_parent" data-role="fullGrid">
                    <div class="lich_div_1">
                        <font face="Tahoma" color="#0033CC" style="font-size: 17pt">
                            <b data-lang="LichPhong">LỊCH PHÒNG</b>
                        </font>
                    </div>
                    <div class="today2">
                        <div style="display: inline;">
                            <a href="javascript:;" class="lnkLastMonth" data-lang="TuanTruoc">
                                <img src="/Content/themeV1/icon/left.gif" alt="left" class="next1" border="0" >Tuần trước</a>&emsp;&emsp;&emsp;
                            <a data-lang="Week"> Tuần thứ <a class="clsweekcur"> </a></a>
                            &emsp;&emsp;&emsp;<a href="javascript:;" class="lnkNexkMonth" data-lang="TuanSau">Tuần sau<img src="/Content/themeV1/icon/right.gif" alt="left" class="next1" border="0"></a>
                        </div>
                        <div class="detail-week" style="padding-top: 10px; font-weight: 600"></div>
                    </div>
                    <div class="display-flex-righteous formsearchpublic" id="frmSearchLichPhongBan" role="search">
                        <div class="items-form" style="width: 20%">
                            <input type="text" name="keyword" data-lang="PlaceSearch" placeholder="Nhập từ khóa">
                            <input type="hidden" name="SearchInAdvance" value="Title,LichNoiDung,LichLanhDaoChuTri,LichDiaDiem,FullText,THANH_PHAN,CHU_TRI,CHUAN_BI">
                            <input type="hidden" name="p_week" class="week" value="" />
                        </div>
                        <div class="items-form">
                            <input type="text" class="input-datetime" name="TuNgay_LichHop" id="TuNgay_LichHopPhongBan" data-lang="TuNgay"  placeholder="Từ ngày">
                        </div>
                        <div class="items-form">
                            <input type="text" class="input-datetime" name="DenNgay_LichHop" id="DenNgay_LichHopPhongBan" data-lang="DenNgay" placeholder="Đến ngày">
                        </div>
                        <div class="icon-button display-flex pl-2">
                            <button data-toggle="tooltip" data-placement="top" title="Xóa lọc tất cả" class="reset_data"><i class="fas fa-sync-alt"></i></button>
                            <button type="button" class="btn btnsearch btn-search-meeting" data-lang="TimKiem">Tìm kiếm</button>


                            <!--<button type="button" id="btnPhanCongPhongBan" class="btn  btn-search-meeting">Phân công</button>
                            <button type="button" id="btnDaXemPhongBan" class="btn  btn-search-meeting">Đã xem</button>-->
                        </div>
                    </div>
                    <div class="lich_div_2">
                        <div style="width: 100%;" class="lichDiv">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tbody>
                                    <tr>
                                        <td style="">
                                            <div style="margin: 20px auto 10px; width: 100%;">
                                                <div style="background: rgb(198, 219, 255) no-repeat scroll left top; clear: both; display: block; -moz-background-clip: -moz-initial; -moz-background-origin: -moz-initial; -moz-background-inline-policy: -moz-initial; margin-bottom: 10px; font-size: 12px; font-family: Arial;">
                                                    <div style="clear: both; background-color: #e8eef7">
                                                        <table class="tblHeader" width="100%" border="0" cellpadding="1" cellspacing="1" style="background-color: #e8eef7; border: 1px solid #72a1de">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="date_left" valign="middle" width="7%" align="left" style="border-right: solid 1px #C6DBFF; background-color: #ffffcc;">
                                                                        <b data-lang="Thu/Ngay">Thứ/Ngày</b>
                                                                    </td>
                                                                    <td class="date_right" valign="middle" width="91%" align="left" style="background-color: #ffffcc;">
                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="date_right lich_Header" valign="middle" width="7%" align="center">
                                                                                        <b data-lang="ThoiGian">Thời gian</b>
                                                                                    </td>
                                                                                    
                                                                                    <td class="date_right lich_Header" valign="middle" width="15%" align="center">
                                                                                        <b data-lang="DiaDiem">Địa điểm</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="" align="center">
                                                                                        <b data-lang="NoiDung">Nội dung</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="15%" align="center">
                                                                                        <b data-lang="ChuTri/ChuanBi">Chủ trì/Chuẩn bị</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="15%" align="center">
                                                                                        <b data-lang="ThanhPhanThamGia">Thành phần tham gia TCT</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="15%" align="center">
                                                                                        <b data-lang="ThamGiaTaiDonVi">Tham gia tại Đơn vị</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="3%" align="center">
                                                                                        <b></b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="3%" align="center">
                                                                                        <b>
                                                                                            <input type="checkbox" name="CheckALL" class="CheckALLDonvi" value="1" /></b>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div style="clear: both;">
                                                        <table class="clscontentLich" width="100%" border="0" cellpadding="1" cellspacing="1">
                                                            <tbody role="grid">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>
            </section>
        </div>
        <div id="tab-4" class="main-news-event" style="display: none">
            <!--Lịch phòng-->
            <section class="pb-1 mt-1">
                <div id="lichCaNhan_div_parent" data-role="fullGrid">
                    <div class="lich_div_1">
                        <font face="Tahoma" color="#0033CC" style="font-size: 17pt">
                            <b data-LANG="LichCaNhan">LỊCH CÁ NHÂN</b>
                        </font>
                    </div>
                    <div class="today2">
                        <div style="display: inline;">
                            <a href="javascript:;" class="lnkLastMonth" data-lang="TuanTruoc">
                                <img src="/Content/themeV1/icon/left.gif" alt="left" class="next1" border="0" >Tuần trước</a>&emsp;&emsp;&emsp;
                            <a data-lang="Week"> Tuần thứ <a class="clsweekcur"> </a></a>
                            &emsp;&emsp;&emsp;<a href="javascript:;" class="lnkNexkMonth" data-lang="TuanSau">Tuần sau<img src="/Content/themeV1/icon/right.gif" alt="left" class="next1" border="0"></a>
                        </div>
                        <div class="detail-week" style="padding-top: 10px; font-weight: 600"></div>
                    </div>
                    <div class="display-flex-righteous formsearchpublic" id="frmSearchLichCaNhan" role="search">
                        <div class="items-form" style="width: 20%">
                            <input type="text" name="keyword" data-lang="PlaceSearch" placeholder="Nhập từ khóa">
                            <input type="hidden" name="SearchInAdvance" value="Title,LichNoiDung,LichLanhDaoChuTri,LichDiaDiem,FullText,THANH_PHAN,CHU_TRI,CHUAN_BI">
                            <input type="hidden" name="p_week" class="week" value="" />
                        </div>
                        <div class="items-form">
                            <input type="text" class="input-datetime" name="TuNgay_LichHop" id="TuNgay_LichCaNhan" data-lang="TuNgay" placeholder="Từ ngày">
                        </div>
                        <div class="items-form">
                            <input type="text" class="input-datetime" name="DenNgay_LichHop" id="DenNgay_LichCaNhan" data-lang="DenNgay" placeholder="Đến ngày">
                        </div>
                        <div class="icon-button display-flex pl-2">
                            <button data-toggle="tooltip" data-placement="top" title="Xóa lọc tất cả" class="reset_data"><i class="fas fa-sync-alt"></i></button>
                            <button type="button" class="btn btnsearch btn-search-meeting" data-lang="TimKiem">Tìm kiếm</button>
                            <button type="button" id="btnThemMoiCaNhan" class="btn  btn-search-meeting" data-lang="ThemMoi">Thêm mới</button>
                        </div>
                    </div>
                    <div class="lich_div_2">
                        <div style="width: 100%;" class="lichDiv">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tbody>
                                    <tr>
                                        <td style="">
                                            <div style="margin: 20px auto 10px; width: 100%;">
                                                <div style="background: rgb(198, 219, 255) no-repeat scroll left top; clear: both; display: block; -moz-background-clip: -moz-initial; -moz-background-origin: -moz-initial; -moz-background-inline-policy: -moz-initial; margin-bottom: 10px; font-size: 12px; font-family: Arial;">
                                                    <div style="clear: both; background-color: #e8eef7">
                                                        <table class="tblHeader" width="100%" border="0" cellpadding="1" cellspacing="1" style="background-color: #e8eef7; border: 1px solid #72a1de">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="date_left" valign="middle" width="7%" align="left" style="border-right: solid 1px #C6DBFF; background-color: #ffffcc;">
                                                                        <b data-lang="Thu/Ngay">Thứ/Ngày</b>
                                                                    </td>
                                                                    <td class="date_right" valign="middle" width="91%" align="left" style="background-color: #ffffcc;">
                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="date_right lich_Header" valign="middle" width="7%" align="center">
                                                                                        <b data-lang="ThoiGian">Thời gian</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="15%" align="center">
                                                                                        <b data-lang="DiaDiem">Địa điểm</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="" align="center">
                                                                                        <b data-lang="NoiDung">Nội dung</b>
                                                                                    </td>
                                                                                    
                                                                                    <td class="date_right lich_Header" valign="middle" width="15%" align="center">
                                                                                        <b data-lang="ChuTri/ChuanBi">Chủ trì/Chuẩn bị</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="15%" align="center">
                                                                                        <b data-lang="ThanhPhanThamGia">Thành phần tham gia TCT</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="15%" align="center">
                                                                                        <b data-lang="ThamGiaTaiDonVi">Tham gia tại Đơn vị</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="3%" align="center">
                                                                                        <b>
                                                                                            <input type="checkbox" name="CheckALL" class="CheckALLDonvi" value="1" /></b>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div style="clear: both;">
                                                        <table class="clscontentLich" width="100%" border="0" cellpadding="1" cellspacing="1">
                                                            <tbody role="grid">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>
            </section>
        </div>
    </div>
</section>

<script type="text/javascript">
    var ItemActive = "";

    //  js nhảy tab 
    function show(idtab) {
        $("#content .item_li_1").removeClass("active");
        $(".left_header_calendar [data-tab-target='#" + idtab + "']").addClass("active");
        $(".main-news-event").css("display", "none");
        $("#" + idtab).css("display", "block");

    }

    function Getdetail(urlaction, itemid) {
        $.get({
            async: false,
            url: urlaction,
            data: { ItemID: itemid }
        }).done(function (odata) {
            var jsDetail = kendo.template($("#jsDetail").html());
            jsDetail = jsDetail(odata); //Pass the data to the compiled template
            return jsDetail;
        });
    }
    function SuaPhanCong(itemid) {
        //active cái tr đó lên.
        ItemActive = "#lichDonVi_div_parent td.linkview[data-itemid=" + itemid + "]";
        $("#lichDonVi_div_parent tr").removeClass("tractive");
        $("#lichDonVi_div_parent td.linkview[data-itemid=" + itemid + "]").closest("tr").addClass("tractive");
        openDialog("Sửa Phân công lịch họp", "/UserControls/Public/Portal/Meetings/pformGiaoLich.aspx", { ItemIDs: itemid, "TypeLich": "LichDonVi", "do": "SUAGIAOLICHDONVI" }, 900);
    }
    function CopyLichDonvi(ItemID) {
        ItemActive = "#lichDonVi_div_parent td.linkview[data-itemid=" + ItemID + "]";
        $("#lichDonVi_div_parent tr").removeClass("tractive");
        $("#lichDonVi_div_parent td.linkview[data-itemid=" + ItemID + "]").closest("tr").addClass("tractive");
        openDialog("Nhân bản lịch đơn vị", "/UserControls/Public/Portal/Meetings/frmLichPhong.aspx", { IDCopy: ItemID, "do": "DKLICH", "TypeLich": "LichDonVi" }, 1024);
    }
    function CopyLichPhong(ItemID) {
        ItemActive = "#lichPhongBan_div_parent td.linkview[data-itemid=" + ItemID + "]";
        $("#lichPhongBan_div_parent tr").removeClass("tractive");
        $("#lichPhongBan_div_parent td.linkview[data-itemid=" + ItemID + "]").closest("tr").addClass("tractive");
        openDialog("Nhân bản lịch phòng", "/UserControls/Public/Portal/Meetings/frmLichPhong.aspx", { IDCopy: ItemID, "do": "DKLICH" }, 1024);
    }
    function PhanCong(itemid) {
        //active cái tr đó lên.
        ItemActive = "#lich_div_parent td.linkview[data-itemid=" + itemid + "]";
        $("#lich_div_parent tr").removeClass("tractive");
        $("#lich_div_parent td.linkview[data-itemid=" + itemid + "]").closest("tr").addClass("tractive");
        openDialog("Phân công lịch họp", "/UserControls/Public/Portal/Meetings/pformGiaoLich.aspx", { ItemIDs: itemid, "TypeLich": "LichTongCongTy", "do": "GIAOLICHDONVI" }, 900);
    }
    function DaxemTongCTY(itemid) {
        ItemActive = "#lich_div_parent td.linkview[data-itemid=" + itemid + "]";
        $("#lich_div_parent tr").removeClass("tractive");
        $("#lich_div_parent td.linkview[data-itemid=" + itemid + "]").closest("tr").addClass("tractive");
        BootstrapDialog.confirm({
            title: 'Xác nhận',
            message: "Bạn có chắc chắn đã xem bản ghi này?",
            type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
            closable: true, // <-- Default value is false
            draggable: true, // <-- Default value is false
            btnCancelLabel: 'Hủy', // <-- Default value is 'Cancel',
            btnOKLabel: 'Đồng ý!', // <-- Default value is 'OK',
            btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
            callback: function (result) {
                // result will be true if button was click, while it will be false if users close the dialog directly.
                if (result) {

                    $.post("/UserControls/AdminCMS/LichTongCongTy/pAction.asp", { ItemIDs: itemid, "do": "XACNHANDAXEM" }).done(function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            $("#lich_div_parent").data("smGrid2018").RefreshGrid();
                            //REFRESH LẠI THÔNG TIN.
                        }

                    }).always(function () {

                    });
                } else {
                    //alert(123);
                }
            }
        });
    }
    function XoaLich(ItemID) {
        BootstrapDialog.confirm({
            title: 'Xác nhận',
            message: "Bạn có chắc chắn xóa bản ghi này?",
            type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
            closable: true, // <-- Default value is false
            draggable: true, // <-- Default value is false
            btnCancelLabel: 'Hủy', // <-- Default value is 'Cancel',
            btnOKLabel: 'Đồng ý!', // <-- Default value is 'OK',
            btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
            callback: function (result) {
                // result will be true if button was click, while it will be false if users close the dialog directly.
                if (result) {
                    $.post("/UserControls/AdminCMS/LichDonVi/pAction.asp", { ItemID: ItemID, "do": "DELETE" }).done(function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#lich_div_parent").data("smGrid2018").RefreshGrid();
                            $("#lichDonVi_div_parent").data("smGrid2018").RefreshGrid();
                            //REFRESH LẠI THÔNG TIN.
                        }

                    }).always(function () {

                    });
                } else {
                    //alert(123);
                }
            }
        });
    }
    function XoaLichPhong(ItemID) {
        BootstrapDialog.confirm({
            title: 'Xác nhận',
            message: "Bạn có chắc chắn xóa bản ghi này?",
            type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
            closable: true, // <-- Default value is false
            draggable: true, // <-- Default value is false
            btnCancelLabel: 'Hủy', // <-- Default value is 'Cancel',
            btnOKLabel: 'Đồng ý!', // <-- Default value is 'OK',
            btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
            callback: function (result) {
                // result will be true if button was click, while it will be false if users close the dialog directly.
                if (result) {
                    $.post("/UserControls/AdminCMS/LichPhong/pAction.asp", { ItemID: ItemID, "do": "DELETE" }).done(function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#lich_div_parent").data("smGrid2018").RefreshGrid();
                            $("#lichPhongBan_div_parent").data("smGrid2018").RefreshGrid();
                            //REFRESH LẠI THÔNG TIN.
                        }

                    }).always(function () {

                    });
                } else {
                    //alert(123);
                }
            }
        });
    }
    function PhanCongLichPhong(ItemID) {
        openDialog("Phân công lịch họp", "/UserControls/Public/Portal/Meetings/pformGiaoLichPhong.aspx", { ItemIDs: ItemID, "TypeLich": "LichPhong", "do": "GIAOLICHDONVI" }, 900);
    }
    function SuaPhanCongLichPhong(itemid) {
        //active cái tr đó lên.
        ItemActive = "#lichDonVi_div_parent td.linkview[data-itemid=" + itemid + "]";
        $("#lichDonVi_div_parent tr").removeClass("tractive");
        $("#lichDonVi_div_parent td.linkview[data-itemid=" + itemid + "]").closest("tr").addClass("tractive");
        openDialog("Sửa Phân công lịch họp", "/UserControls/Public/Portal/Meetings/pformGiaoLichPhong.aspx", { ItemIDs: itemid, "TypeLich": "LichPhong", "do": "SUAGIAOLICHDONVI" }, 900);
    }
    function SuaLichPhong(ItemID) {
        openDialog("Sửa lịch họp đã đăng ký", "/UserControls/Public/Portal/Meetings/frmLichPhong.aspx", { ItemID: ItemID, "do": "DKLICH" }, 1024);
    }
    function SuaLichDonVi(ItemID, TrangThai) {
        openDialog("Sửa lịch đơn vị", "/UserControls/Public/Portal/Meetings/frmLichPhong.aspx", { ItemID: ItemID, "do": "DKLICH", "TypeLich": "LichDonVi" }, 1024);
    }
    function KiemDuyetLichDonvi(ItemID) {
        openDialog("Kiểm duyệt lịch đơn vị", "/UserControls/Public/Portal/Meetings/frmLichPhong.aspx", { ItemID: ItemID, "do": "DKLICH", "TypeLich": "LichDonVi", "LichTrangThai": "2" }, 1024);
    }
    function SuaLichCaNhan(ItemID) {
        openDialog("Sửa lịch cá nhân", "/UserControls/AdminCMS/LichCaNhan/pFormLichCaNhan.aspx", { ItemID: ItemID }, 900);
    }
    function XoaLichCaNhan(ItemID) {
        BootstrapDialog.confirm({
            title: 'Xác nhận',
            message: "Bạn có chắc chắn xóa bản ghi này?",
            type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
            closable: true, // <-- Default value is false
            draggable: true, // <-- Default value is false
            btnCancelLabel: 'Hủy', // <-- Default value is 'Cancel',
            btnOKLabel: 'Đồng ý!', // <-- Default value is 'OK',
            btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
            callback: function (result) {
                // result will be true if button was click, while it will be false if users close the dialog directly.
                if (result) {
                    $.post("/UserControls/AdminCMS/LichCaNhan/pAction.asp", { ItemID: ItemID, "do": "DELETE" }).done(function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#lich_div_parent").data("smGrid2018").RefreshGrid();
                            $("#lichCaNhan_div_parent").data("smGrid2018").RefreshGrid();
                            //REFRESH LẠI THÔNG TIN.
                        }

                    }).always(function () {

                    });
                } else {
                    //alert(123);
                }
            }
        });
    }
    $(document).ready(function () {
        SetConfigLang('<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>', 'Meetings');
        var curentTab = location.hash;
        var KieuDonvi = '<%=oDonVi.KieuDonVi%>';
        if (KieuDonvi == '2') {
            $("#DivLichphong").hide();
        }
        //alert(curentTab);

        if (curentTab != "" && curentTab != null) {
            $('.list_calendar a[href="' + curentTab + '"]').click();
        } else {
            if ('<%=UrlSite%>' != '') {
                $('.list_calendar a[href="#tab-2"]').click();
            }
        }

        $("#btnPhanCongLichTongcty").click(function () {
            const selectedValues = $('input[name="IDLich"]:checked').map(function () {
                return $(this).val();
            })
                .get()
                .join(',');
            console.log(selectedValues);
            openDialog("Phân công lịch họp", "/UserControls/Public/Portal/Meetings/pformGiaoLich.aspx", { ItemIDs: selectedValues, "TypeLich": "LichTongCongTy", "do": "GIAOLICHDONVI" }, 900);
        });
        $("#btnDongBo").click(function () {
            loading();
            var SearchTuNgay = $("#TuNgay_LichHopTCTY").val();
            var SearchDenNgay = $("#DenNgay_LichHopTCTY").val();
            $.post("/UserControls/AdminCMS/LichTongCongTy/pAction.asp",
                { "do": "DONGBO", "SearchTuNgay": SearchTuNgay, "SearchDenNgay": SearchDenNgay }).done(function (result) {
                    Endloading();
                    if (result.State == 2) {
                        BootstrapDialog.show({
                            title: "Lỗi",
                            message: result.Message
                        });
                    }
                    else {
                        showMsg(result.Message);
                        $("#lich_div_parent").data("smGrid2018").RefreshGrid();
                        //REFRESH LẠI THÔNG TIN.
                    }

                }).always(function () {

                });
        });
        //btnDaXemPhongBan
        $("#btnDaXemPhongBan").click(function () {
            const selectedValues = $('#lichPhongBan_div_parent input[name="IDLich"]:checked').map(function () {
                return $(this).val();
            })
                .get()
                .join(',');
            console.log(selectedValues);
            BootstrapDialog.confirm({
                title: 'Xác nhận',
                message: "Bạn có chắc chắn đã xem bản ghi này?",
                type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Hủy', // <-- Default value is 'Cancel',
                btnOKLabel: 'Đồng ý!', // <-- Default value is 'OK',
                btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        $.post("/UserControls/AdminCMS/LichPhong/pAction.asp", { ItemIDs: selectedValues, "do": "XACNHANDAXEM" }).done(function (result) {
                            if (result.State == 2) {
                                BootstrapDialog.show({
                                    title: "Lỗi",
                                    message: result.Message
                                });
                            }
                            else {
                                showMsg(result.Message);
                                //REFRESH LẠI THÔNG TIN.
                                $("#lichPhongBan_div_parent").data("smGrid2018").RefreshGrid();
                            }

                        }).always(function () {

                        });
                    } else {
                        //alert(123);
                    }
                }
            });
        });

        //btnThemMoiPhongBan
        $("#btnThemMoiPhongBan").click(function () {
            openDialog("Thêm mới lịch họp", "/UserControls/Public/Portal/Meetings/frmLichPhong.aspx", { "do": "DKLICH" }, 1024);
        });
        $("#btnThemMoiCaNhan").click(function () {
            openDialog("Thêm mới lịch", "/UserControls/AdminCMS/LichCaNhan/pFormLichCaNhan.aspx", {}, 1024);
        });
        //fldGroupDonVi
        $("#fldGroupDonVi").smSelect2018V2({
            dropdownParent: "#lichhop"
        });
        // Add event listener for opening and closing details
        $('#tab-1').on('click', 'td.details-control', function () {
            var ItemID = $(this).attr("data-Itemid");
            var tr = $(this).closest('tr');
            $(".rowdetail").hide();

            if (tr.hasClass("dt-hasChild")) {
                tr.next().show();
            } else {
                tr.addClass("dt-hasChild");
                //var jsDetail = Getdetail('/VIPortalAPI/api/LichLamViec/GETBYID_LICHTONGCTY', ItemID);
                $.get({
                    async: false,
                    url: '/VIPortalAPI/api/LichLamViec/GETBYID_LICHTONGCTY',
                    data: { ItemID: ItemID }
                }).done(function (odata) {
                    var jsDetail = kendo.template($("#jsDetail").html());
                    jsDetail = jsDetail(odata); //Pass the data to the compiled template
                    tr.after(jsDetail);
                });
            }
        });
        $('#tab-0').on('click', 'td.details-control', function () {
            var ItemID = $(this).attr("data-Itemid");
            var tr = $(this).closest('tr');
            $(".rowdetail").hide();

            if (tr.hasClass("dt-hasChild")) {
                tr.next().show();
            } else {
                tr.addClass("dt-hasChild");
                //var jsDetail = Getdetail('/VIPortalAPI/api/LichLamViec/GETBYID_LICHTONGCTY', ItemID);
                $.get({
                    async: false,
                    url: '/VIPortalAPI/api/LichLamViec/GETBYID_LICHTAPDOAN',
                    data: { ItemID: ItemID }
                }).done(function (odata) {
                    var jsDetail = kendo.template($("#jsDetailTapDoan").html());
                    jsDetail = jsDetail(odata); //Pass the data to the compiled template
                    tr.after(jsDetail);
                });
            }
        });
        $('#tab-2').on('click', 'td.details-control', function () {
            var ItemID = $(this).attr("data-Itemid");
            var tr = $(this).closest('tr');
            $(".rowdetail").hide();

            if (tr.hasClass("dt-hasChild")) {
                tr.next().show();
            } else {
                tr.addClass("dt-hasChild");
                //var jsDetail = Getdetail('/VIPortalAPI/api/LichLamViec/GETBYID_LICHTONGCTY', ItemID);
                $.get({
                    async: false,
                    url: '/VIPortalAPI/api/LichLamViec/GETBYID_DONVI',
                    data: { ItemID: ItemID }
                }).done(function (odata) {
                    var jsDetail = kendo.template($("#jsDetail").html());
                    jsDetail = jsDetail(odata); //Pass the data to the compiled template
                    tr.after(jsDetail);
                });
            }
        });
        $('#tab-3').on('click', 'td.details-control', function () {
            var ItemID = $(this).attr("data-Itemid");
            var tr = $(this).closest('tr');
            $(".rowdetail").hide();

            if (tr.hasClass("dt-hasChild")) {
                tr.next().show();
            } else {
                tr.addClass("dt-hasChild");
                //var jsDetail = Getdetail('/VIPortalAPI/api/LichLamViec/GETBYID_LICHTONGCTY', ItemID);
                $.get({
                    async: false,
                    url: '/VIPortalAPI/api/LichLamViec/GETBYID_PHONG',
                    data: { ItemID: ItemID }
                }).done(function (odata) {
                    var jsDetail = kendo.template($("#jsDetail").html());
                    jsDetail = jsDetail(odata); //Pass the data to the compiled template
                    tr.after(jsDetail);
                });
            }
        });



        $("#lich_div_parent .lnkLastMonth").click(function () {
            var clsweekcur = $("#lich_div_parent .week").val();
            var clsweekcurInt = parseInt(clsweekcur);
            $("#lich_div_parent .week").val(clsweekcurInt - 1);
            $("#lich_div_parent").data("smGrid2018").RefreshGrid();
        });
        $("#lich_div_parent .lnkNexkMonth").click(function () {
            var clsweekcur = $("#lich_div_parent .week").val();
            var clsweekcurInt = parseInt(clsweekcur);
            $("#lich_div_parent .week").val(clsweekcurInt + 1);
            $("#lich_div_parent").data("smGrid2018").RefreshGrid();

        });

        $("#lich_div_parent").smGrid2018({
            "TypeTemp": "kendo",
            "UrlPost": "/VIPortalAPI/api/LichLamViec/GetListLichHienThi",
            odata: { "do": "QUERYDATA", "UrlList": "/noidung/Lists/LichTongCongTy" },
            template: "jsTemplateNgay",
            dataBound: function (e) {
                $("#lich_div_parent .week").val(e.Query);
                $("#lich_div_parent .detail-week").text(e.result);
                console.log(">>>Check detail-week: ",e.result)
                //clsweekcur
                $("#lich_div_parent .clsweekcur").text(`${e.Query}`);

                $("#lich_div_parent td[data-itemid='<%=ItemID%>']").parent().addClass("clsdetail");
                //duyệt qua các tr để lấy thông tin.
                $("#lich_div_parent .fa-check-circle").closest('tr').addClass('checked');
                if (ItemActive != "") {
                    $(ItemActive + "").closest("tr").addClass("tractive");
                }

            },
        });

        //$('[data-toggle="tooltip"]').tooltip();
        setTimeout(
            function () {
                //lichCaNhan_div_parent
                $("#lichCaNhan_div_parent").smGrid2018({
                    "TypeTemp": "kendo",
                    "UrlPost": "/VIPortalAPI/api/LichLamViec/GetListLichHienThi",
                    odata: { "do": "QUERYDATA", "UrlList": "/noidung/Lists/LichCaNhan", "CreatedUser": <%=CurentUser.ID%> },
                    template: "jsTemplateNgay",
                    dataBound: function (e) {
                        $('#lichCaNhan_div_parent a[data-toggle="tooltip"]').tooltip();
                        $("#lichCaNhan_div_parent .week").val(e.Query);
                        $("#lichCaNhan_div_parent .detail-week").text(e.result);
                        $("#lichCaNhan_div_parent .clsweekcur").text(`${e.Query}`);
                        $("#lich_div_parent td[data-itemid='<%=ItemID%>']").parent().addClass("clsdetail");
                    },
                });

                $("#lichCaNhan_div_parent .lnkLastMonth").click(function () {
                    var clsweekcur = $("#lichCaNhan_div_parent .week").val();
                    var clsweekcurInt = parseInt(clsweekcur);
                    $("#lichCaNhan_div_parent .week").val(clsweekcurInt - 1);
                    $("#lichCaNhan_div_parent").data("smGrid2018").RefreshGrid();
                });
                $("#lichCaNhan_div_parent .lnkNexkMonth").click(function () {
                    var clsweekcur = $("#lichCaNhan_div_parent .week").val();
                    var clsweekcurInt = parseInt(clsweekcur);
                    $("#lichCaNhan_div_parent .week").val(clsweekcurInt + 1);
                    $("#lichCaNhan_div_parent").data("smGrid2018").RefreshGrid();

                });

                //lichDonVi_div_parent
                $("#lichDonVi_div_parent").smGrid2018({
                    "TypeTemp": "kendo",
                    "UrlPost": "/VIPortalAPI/api/LichLamViec/GetListLichHienThi",
                    odata: { "do": "QUERYDATA", "UrlList": "/noidung/Lists/LichDonVi", "fldGroup": <%=CurentUser.Groups.ID%>  },
                    template: "jsTemplateNgay",
                    dataBound: function (e) {
                        if (ItemActive != "") {
                            $(ItemActive + "").closest("tr").addClass("tractive");
                        }
                        $('#lichDonVi_div_parent a[data-toggle="tooltip"]').tooltip();
                        $("#lichDonVi_div_parent .week").val(e.Query);
                        $("#lichDonVi_div_parent .detail-week").text(e.result);
                        $("#lichDonVi_div_parent .clsweekcur").text(`${e.Query}`);
                        $("#lich_div_parent td[data-itemid='<%=ItemID%>']").parent().addClass("clsdetail");
                        //$("#lichPhongBan_div_parent .CalendarCheck").css("cursor", "pointer");
                        $("#lichDonVi_div_parent .CalendarCheck").click(function () {
                            var $ItemID = $(this).attr("data-ItemID");
                            BootstrapDialog.confirm({
                                title: 'Duyệt lịch',
                                message: "Bạn có chắc chắn duyệt đăng ký lịch này?",
                                type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                closable: true, // <-- Default value is false
                                draggable: true, // <-- Default value is false
                                btnCancelLabel: 'Không đồng ý', // <-- Default value is 'Cancel',
                                btnOKLabel: 'Đồng ý!', // <-- Default value is 'OK',
                                btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
                                callback: function (result) {
                                    // result will be true if button was click, while it will be false if users close the dialog directly.
                                    if (result) {
                                        $.post("/UserControls/AdminCMS/LichDonVi/pAction.asp", { ItemID: $ItemID, "do": "DUYETLICH" }).done(function (result) {
                                            if (result.State == 2) {
                                                BootstrapDialog.show({
                                                    title: "Lỗi",
                                                    message: result.Message
                                                });
                                            }
                                            else {
                                                showMsg(result.Message);
                                                //REFRESH LẠI THÔNG TIN.
                                                $("#lichDonVi_div_parent").data("smGrid2018").RefreshGrid();
                                            }

                                        }).always(function () {

                                        });
                                    } else {
                                        $.post("/UserControls/AdminCMS/LichDonVi/pAction.asp", { ItemID: $ItemID, "do": "DUYETLICH", "LoaiDuyet": 2 }).done(function (result) {
                                            if (result.State == 2) {
                                                BootstrapDialog.show({
                                                    title: "Lỗi",
                                                    message: result.Message
                                                });
                                            }
                                            else {
                                                showMsg(result.Message);
                                                //REFRESH LẠI THÔNG TIN.
                                                $("#lichDonVi_div_parent").data("smGrid2018").RefreshGrid();
                                            }

                                        }).always(function () {

                                        });

                                    }
                                }
                            });
                        });
                        //CalendarUnCheck
                        $("#lichDonVi_div_parent .CalendarUnCheck").click(function () {
                            var $ItemID = $(this).attr("data-ItemID");
                            BootstrapDialog.confirm({
                                title: 'Duyệt lịch',
                                message: "Bạn có chắc chắn Hủy đăng ký lịch này?",
                                type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                closable: true, // <-- Default value is false
                                draggable: true, // <-- Default value is false
                                btnCancelLabel: 'Không đồng ý', // <-- Default value is 'Cancel',
                                btnOKLabel: 'Đồng ý!', // <-- Default value is 'OK',
                                btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
                                callback: function (result) {
                                    // result will be true if button was click, while it will be false if users close the dialog directly.
                                    if (result) {
                                        $.post("/UserControls/AdminCMS/LichDonVi/pAction.asp", { ItemID: $ItemID, "do": "HUYDUYETLICH" }).done(function (result) {
                                            if (result.State == 2) {
                                                BootstrapDialog.show({
                                                    title: "Lỗi",
                                                    message: result.Message
                                                });
                                            }
                                            else {
                                                showMsg(result.Message);
                                                //REFRESH LẠI THÔNG TIN.
                                                $("#lichDonVi_div_parent").data("smGrid2018").RefreshGrid();
                                            }

                                        }).always(function () {

                                        });
                                    }
                                }
                            });
                        });
                    },
                });

                $("#lichDonVi_div_parent .lnkLastMonth").click(function () {
                    var clsweekcur = $("#lichDonVi_div_parent .week").val();
                    var clsweekcurInt = parseInt(clsweekcur);
                    $("#lichDonVi_div_parent .week").val(clsweekcurInt - 1);
                    $("#lichDonVi_div_parent").data("smGrid2018").RefreshGrid();
                });
                $("#lichDonVi_div_parent .lnkNexkMonth").click(function () {
                    var clsweekcur = $("#lichDonVi_div_parent .week").val();
                    var clsweekcurInt = parseInt(clsweekcur);
                    $("#lichDonVi_div_parent .week").val(clsweekcurInt + 1);
                    $("#lichDonVi_div_parent").data("smGrid2018").RefreshGrid();

                });


                if (KieuDonvi != '2') {
                    $("#lichPhongBan_div_parent").smGrid2018({
                        "TypeTemp": "kendo",
                        "UrlPost": "/VIPortalAPI/api/LichLamViec/GetListLichHienThi",
                        odata: { "do": "QUERYDATA", "UrlList": "/noidung/Lists/LichPhong", "fldGroup": <%=CurentUser.UserPhongBan.ID%> },
                        template: "jsTemplateNgay",
                        dataBound: function (e) {
                            $('#lichPhongBan_div_parent a[data-toggle="tooltip"]').tooltip();
                            $("#lichPhongBan_div_parent .week").val(e.Query);
                            $("#lichPhongBan_div_parent .detail-week").text(e.result);
                            $("#lichPhongBan_div_parent .clsweekcur").text(` ${e.Query}`);
                            $("#lichPhongBan_div_parent .CalendarCheck").click(function () {
                                var $ItemID = $(this).attr("data-ItemID");
                                BootstrapDialog.confirm({
                                    title: 'Duyệt lịch',
                                    message: "Bạn có chắc chắn duyệt đăng ký lịch này?",
                                    type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                    closable: true, // <-- Default value is false
                                    draggable: true, // <-- Default value is false
                                    btnCancelLabel: 'Không đồng ý', // <-- Default value is 'Cancel',
                                    btnOKLabel: 'Đồng ý!', // <-- Default value is 'OK',
                                    btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
                                    callback: function (result) {
                                        // result will be true if button was click, while it will be false if users close the dialog directly.
                                        if (result) {
                                            $.post("/UserControls/AdminCMS/LichPhong/pAction.asp", { ItemID: $ItemID, "do": "DUYETLICH" }).done(function (result) {
                                                if (result.State == 2) {
                                                    BootstrapDialog.show({
                                                        title: "Lỗi",
                                                        message: result.Message
                                                    });
                                                }
                                                else {
                                                    showMsg(result.Message);
                                                    //REFRESH LẠI THÔNG TIN.
                                                    $("#lichPhongBan_div_parent").data("smGrid2018").RefreshGrid();
                                                }

                                            }).always(function () {

                                            });
                                        } else {
                                            $.post("/UserControls/AdminCMS/LichPhong/pAction.asp", { ItemID: $ItemID, "do": "DUYETLICH", "LoaiDuyet": 2 }).done(function (result) {
                                                if (result.State == 2) {
                                                    BootstrapDialog.show({
                                                        title: "Lỗi",
                                                        message: result.Message
                                                    });
                                                }
                                                else {
                                                    showMsg(result.Message);
                                                    //REFRESH LẠI THÔNG TIN.
                                                    $("#lichPhongBan_div_parent").data("smGrid2018").RefreshGrid();
                                                }

                                            }).always(function () {

                                            });

                                        }
                                    }
                                });
                            });

                            //CalendarUnCheck
                            $("#lichPhongBan_div_parent .CalendarUnCheck").click(function () {
                                var $ItemID = $(this).attr("data-ItemID");
                                BootstrapDialog.confirm({
                                    title: 'Hủy Duyệt lịch',
                                    message: "Bạn có chắc chắn Hủy đăng ký lịch này?",
                                    type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                    closable: true, // <-- Default value is false
                                    draggable: true, // <-- Default value is false
                                    btnCancelLabel: 'Hủy', // <-- Default value is 'Cancel',
                                    btnOKLabel: 'Đồng ý!', // <-- Default value is 'OK',
                                    btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
                                    callback: function (result) {
                                        // result will be true if button was click, while it will be false if users close the dialog directly.
                                        if (result) {
                                            $.post("/UserControls/AdminCMS/LichPhong/pAction.asp", { ItemID: $ItemID, "do": "HUYDUYETLICH" }).done(function (result) {
                                                if (result.State == 2) {
                                                    BootstrapDialog.show({
                                                        title: "Lỗi",
                                                        message: result.Message
                                                    });
                                                }
                                                else {
                                                    showMsg(result.Message);
                                                    //REFRESH LẠI THÔNG TIN.
                                                    $("#lichPhongBan_div_parent").data("smGrid2018").RefreshGrid();
                                                }

                                            }).always(function () {

                                            });
                                        }
                                    }
                                });
                            });
                        },
                    });


                    $("#lichPhongBan_div_parent .lnkLastMonth").click(function () {
                        var clsweekcur = $("#lichPhongBan_div_parent .week").val();
                        var clsweekcurInt = parseInt(clsweekcur);
                        $("#lichPhongBan_div_parent .week").val(clsweekcurInt - 1);
                        $("#lichPhongBan_div_parent").data("smGrid2018").RefreshGrid();
                    });
                    $("#lichPhongBan_div_parent .lnkNexkMonth").click(function () {
                        var clsweekcur = $("#lichPhongBan_div_parent .week").val();
                        var clsweekcurInt = parseInt(clsweekcur);
                        $("#lichPhongBan_div_parent .week").val(clsweekcurInt + 1);
                        $("#lichPhongBan_div_parent").data("smGrid2018").RefreshGrid();

                    });
                }
            }, 1000);
    });
</script>

