﻿<%@ page language="C#" autoeventwireup="true" codebehind="frmLichPhong.aspx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.Meetings.frmLichPhong" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
    <style type="text/css">
        .border .boxgrouplich {
            padding: 20px 0px !important;
        }

        .boxgrouplich i {
            font-size: 12px;
            font-style: italic;
        }
    </style>
</head>
<body>
    <form id="frm-LichPhong" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oLichPhong.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" name="NguoiTao" id="NguoiTao" value="<%=CurentUser.Title %>" />
        <input type="hidden" name="fldGroup" id="fldGroup" value="<%=CurentUser.UserPhongBan.ID %>" />
        <input type="hidden" name="LichTrangThai" value="<%=oLichPhong.LichTrangThai %>" id="LichTrangThai" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Phân loại</label>
            <div class="col-sm-10">
                <select class="form-control" name="UrlList" id="UrlList">
                    <option value="/noidung/Lists/LichDonVi" <%= oLichPhong.fldGroup.LookupId != CurentUser.UserPhongBan.ID ? "selected" : ""%>>Lịch đơn vị</option>
                    <option value="/noidung/Lists/LichPhong" <%= oLichPhong.fldGroup.LookupId == CurentUser.UserPhongBan.ID ? "selected" : ""%>>Lịch phòng</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Nội dung</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" class="form-control" placeholder="Nhập tên cuộc họp" value="<%=oLichPhong.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="LichDiaDiem" class="col-sm-2 control-label">Địa điểm</label>
            <div class="col-sm-10">
                <input type="text" name="LichDiaDiem" id="LichDiaDiem" placeholder="Nhập địa điểm" class="form-control" value="<%=oLichPhong.LichDiaDiem%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="LichThoiGianBatDau" class="col-sm-2 control-label">Ngày bắt đầu</label>
            <div class="col-sm-4">
                <input type="text" name="LichThoiGianBatDau" id="LichThoiGianBatDau" value="<%:string.Format("{0:dd/MM/yyyy HH:mm}", oLichPhong.LichThoiGianBatDau)%>" class="form-control datetime" />
            </div>
            <label for="LichThoiGianKetThuc" class="col-sm-2 control-label">Ngày kết thúc</label>
            <div class="col-sm-4">
                <input type="text" name="LichThoiGianKetThuc" id="LichThoiGianKetThuc" value="<%:string.Format("{0:dd/MM/yyyy HH:mm}",oLichPhong.LichThoiGianKetThuc)%>" class="form-control datetime" />
            </div>
        </div>
        <fieldset class="border p-2 boxgrouplich ldonvi">
            <legend class="w-auto" style="font-size: 16px;">Chủ trì cuộc họp<span class="clslable">(<span class="clsred">*</span>)</span><i class="clsghichu">(Nhập một trong hai thông tin sau)</i></legend>
            <div class="form-group row ldonvi">
                <label for="CHU_TRI" class="col-sm-2 control-label">Đơn vị</label>
                <div class="col-sm-4">
                    <select data-selected="<%=CHU_TRIID %>" data-url="/UserControls/AdminCMS/LGroup/pAction.asp?do=QUERYDATA&GroupParent=<%=CurentUser.Groups.ID %>&isGetByParent=true" data-place="Chọn Phòng ban" name="CHU_TRI" id="CHU_TRI" class="form-control"></select>
                </div>
                <label for="CHU_TRIKhac" class="col-sm-2 control-label">Chủ trì khác</label>
                <div class="col-sm-4">
                    <input type="text" name="CHU_TRIKhac" id="CHU_TRIKhac" class="form-control" placeholder="Chủ trì ngoài đơn vị" value="<%=CHU_TRIKhac %>" />
                </div>

            </div>

        </fieldset>
        <fieldset class="border p-2 boxgrouplich lphongban" style="display: none;">
            <legend class="w-auto" style="font-size: 16px;">Chủ trì cuộc họp</legend>
            <div class="form-group row">
                <label for="CHU_TRIUser" class="col-sm-2 control-label">Cán bộ</label>
                <div class="col-sm-4">
                    <select data-selected="<%=CHU_TRIID %>" data-url="/UserControls/AdminCMS/LUser/pAction.asp?do=QUERYDATA&Groups=<%=CurentUser.Groups.ID %>&UserPhongBan=<%=CurentUser.UserPhongBan.ID %>&GetUserPhongBan=true" data-place="Chọn cán bộ" name="CHU_TRIUser" id="CHU_TRIUser" class="form-control"></select>
                </div>
                <label for="CHU_TRIUserKhac" class="col-sm-2 control-label">Chủ trì khác</label>
                <div class="col-sm-4">
                    <input type="text" name="CHU_TRIUserKhac" id="CHU_TRIUserKhac" class="form-control" placeholder="Chủ trì ngoài phòng" value="<%=CHU_TRIKhac %>" />
                </div>
            </div>
        </fieldset>
        <div class="form-group row">
            <label for="LichLanhDaoChuTri" class="col-sm-2 control-label">Lãnh đạo đơn vị/Phòng</label>
            <div class="col-sm-4">
                <select data-selected="<%=oLichPhong.LichLanhDaoChuTri.LookupId %>" data-url="/UserControls/AdminCMS/LUser/pAction.asp?do=QUERYDATA" data-place="Chọn người" name="LichLanhDaoChuTri" id="LichLanhDaoChuTri" class="form-control">
                    <option value="<%=oLichPhong.LichLanhDaoChuTri.LookupId %>" selected="selected"><%=oLichPhong.LichLanhDaoChuTri.LookupValue %></option>
                </select>
            </div>
            <label for="" class="col-sm-2 control-label">Loại cuộc họp</label>
            <div class="col-sm-4">
                <select class="form-control" name="DBPhanLoai" id="DBPhanLoai">
                    <option value="0">Bình thường</option>
                    <option value="1">Bổ sung</option>
                    <option value="2">Hoãn</option>
                    <option value="3">Thay đổi thông tin</option>
                </select>
            </div>
        </div>
        <div class="form-group row" style="display: none;">
            <label for="LichNoiDung" class="col-sm-2 control-label">Nội dung</label>
            <div class="col-sm-10">
                <textarea name="LichNoiDung" id="LichNoiDung" placeholder="Nhập nội dung" class="form-control"><%=oLichPhong.LichNoiDung%></textarea>
            </div>
        </div>
        <fieldset class="border p-2 boxgrouplich ldonvi">
            <legend class="w-auto" style="font-size: 16px;">Chuẩn bị cuộc họp<span class="clslable">(<span class="clsred">*</span>)</span><i class="clsghichu">(Nhập một trong hai thông tin sau)</i></legend>
            <div class="form-group row">
                <label for="CHUAN_BI" class="col-sm-2 control-label">Đơn vị chuẩn bị</label>
                <div class="col-sm-4">
                    <select data-selected="<%=CHUAN_BIID %>" multiple="multiple" data-url="/UserControls/AdminCMS/LGroup/pAction.asp?do=QUERYDATA&GroupParent=<%=CurentUser.Groups.ID %>&isGetByParent=true" data-place="Chọn Phòng ban" name="CHUAN_BI" id="CHUAN_BI" class="form-control"></select>
                </div>
                <label for="" class="col-sm-2 control-label">Chuẩn bị khác</label>
                <div class="col-sm-4">
                    <input type="text" name="CHUAN_BI_Khac" id="CHUAN_BI_Khac" class="form-control" placeholder="Chuẩn bị ngoài đơn vị" value="<%=CHUAN_BI_Khac %>" />
                </div>
            </div>
        </fieldset>
        <fieldset class="border p-2 boxgrouplich lphongban" style="display: none;">
            <legend class="w-auto" style="font-size: 16px;">Chuẩn bị cuộc họp</legend>
            <div class="form-group row">
                <label for="CHUAN_BIUser" class="col-sm-2 control-label">Chuẩn bị</label>
                <div class="col-sm-4">
                    <select data-selected="<%=CHUAN_BIID %>" data-url="/UserControls/AdminCMS/LUser/pAction.asp?do=QUERYDATA&Groups=<%=CurentUser.Groups.ID %>&UserPhongBan=<%=CurentUser.UserPhongBan.ID %>&GetUserPhongBan=true" data-place="Chọn cán bộ" name="CHUAN_BIUser" id="CHUAN_BIUser" class="form-control"></select>
                </div>
                <label for="CHUAN_BIUser_Khac" class="col-sm-2 control-label">Chuẩn bị khác</label>
                <div class="col-sm-4">
                    <input type="text" name="CHUAN_BIUser_Khac" id="CHUAN_BIUser_Khac" class="form-control" placeholder="Chuẩn bị ngoài phòngị" value="<%=CHUAN_BI_Khac %>" />
                </div>
            </div>
        </fieldset>
        <div class="form-group row">
            <label for="NoiDungChuanBi" class="col-sm-2 control-label">Nội dung chuẩn bị</label>
            <div class="col-sm-10">
                <textarea name="NoiDungChuanBi" id="NoiDungChuanBi" placeholder="Nhập nội dung chuẩn bị" class="form-control"><%=oLichPhong.NoiDungChuanBi%></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="LichGhiChu" class="col-sm-2 control-label">Ghi chú</label>
            <div class="col-sm-10">
                <textarea name="LichGhiChu" id="LichGhiChu" placeholder="Ghi chú" class="form-control"><%=oLichPhong.LichGhiChu %></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-10">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <fieldset class="border p-2">
            <legend class="w-auto" style="font-size: 16px;">Thành phần tham gia</legend>
            <div class="form-group row">
                <label for="LichThanhPhanThamGia" class="col-sm-2 control-label requPhong">Cán bộ tham gia</label>
                <div class="col-sm-10">
                    <div class="input-group mb-3">
                        <input type="text" disabled="disabled" value="" placeholder="Thành phần tham gia" id="UserChoie" class="form-control" />
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" data-idselected="<%=string.Join(",", oLichPhong.LichThanhPhanThamGia.Select(x=>x.LookupId)) %>" data-do="CHONNGUOI" title="Chọn người dùng" size="1240" url="/UserControls/AdminCMS/LichDonVi/pFormChooseThamGia.aspx" id="btnChonNguoi" name="btnChonNguoi" type="button">Chọn người dùng</button>
                        </div>
                    </div>
                    <input type="hidden" name="lstNguoiDung_Value" id="lstNguoiDung_Value" value="<%=string.Join(",", oLichPhong.LichThanhPhanThamGia.Select(x=>x.LookupId)) %>" />
                    <input type="hidden" name="LichThanhPhanThamGia" id="lstLichThamGia" value="<%=string.Join(",", oLichPhong.LichThanhPhanThamGia.Select(x=>x.LookupId)) %>" />
                    <textarea id="LichThanhPhanThamGia" name="" disabled="disabled" placeholder="Thành phần tham gia" class="form-control"><%=string.Join(",", oLichPhong.LichThanhPhanThamGia.Select(x=>x.LookupValue)) %></textarea>
                </div>
            </div>
            <div class="form-group row ldonvi">
                <label for="LichPhongBanThamGia" class="col-sm-2 control-label">Phòng ban<span class="clslable">(<span class="clsred">*</span>)</span></label>
                <div class="col-sm-10">
                    <div class="input-group mb-3">
                        <select data-selected="<%=string.Join(",", oLichPhong.LichPhongBanThamGia.Select(x => x.LookupId)) %>" multiple="multiple" data-url="/UserControls/AdminCMS/LGroup/pAction.asp?do=QUERYDATA&GroupParent=<%=CurentUser.Groups.ID %>&isGetByParent=true" data-place="Chọn Phòng ban" name="LichPhongBanThamGia" id="LichPhongBanThamGia" class="form-control"></select>
                        <div class="input-group-append">
                            <input type="checkbox" id="checkboxallPhongBan" />Tất cả
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="NoiDungChuanBi" class="col-sm-2 control-label">Thành phần khác</label>
                <div class="col-sm-10">
                    <textarea name="THANH_PHAN" id="THANH_PHAN" placeholder="Nhập thành phần tham gia họp khác" class="form-control"><%=oLichPhong.THANH_PHAN %></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="LichSoNguoiThamgia" class="col-sm-2 control-label">Số lượng người</label>
                <div class="col-sm-4">
                    <input type="text" name="LichSoNguoiThamgia" id="LichSoNguoiThamgia" class="form-control" placeholder="Số lượng người tham gia" value="<%=LichSoNguoiThamgia %>" />
                </div>
        </fieldset>
        
        <fieldset class="border p-2">
            <legend class="w-auto" style="font-size: 16px;">Khách mời</legend>
            <div class="form-group row">
                <label for="" class="col-sm-2 control-label">Danh sách khách mời</label>
                <div class="col-sm-10">
                    <input type="hidden" name="JsonKhachMoi" id="JsonKhachMoi" value="<%=jsonKhachMoi %>" />
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center">Tên</th>
                                <th class="text-center">Số điện thoại</th>
                                <th class="text-center">CMND/CCCD</th>
                                <th class="text-center" style="width: 5%">Xóa</th>
                            </tr>
                        </thead>
                        <tbody id="tbodyKhachMoi">
                        </tbody>
                    </table>
                    <button class="btn btn-md btn-primary"
                        id="addBtn" type="button">
                        Thêm khách mời
                    </button>
                </div>
            </div>
        </fieldset>

    </form>
    <script type="text/javascript">
        function RemoveTR($rowidx) {
            $("tr#R" + $rowidx).remove();
        }
        //LichThanhPhanThamGia
        jQuery.validator.addMethod("LichThanhPhanThamGiaLichPhong", function (value, element) {
            console.log("LichThanhPhanThamGiaLichPhong");
            if ($("#UrlList").val() == "/noidung/Lists/LichPhong") {
                value = $("#lstLichThamGia").val();
                if (value == '' || value == undefined || value == null)
                    return false;
                else return true;
            } else {
                return true;
            }
        }, "Chọn cán bộ tham gia");

        jQuery.validator.addMethod("PHongBanThamGiaDonVi", function (value, element) {
            if ($("#UrlList").val() == "/noidung/Lists/LichDonVi") {
                if (value == '' || value == undefined || value == null)
                    return false;
                else return true;
            } else {
                return true;
            }
        }, "Chọn phòng ban tham gia");
        $(document).ready(function () {
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oLichPhong.ListFileAttach)%>'
            });
            $("#checkboxallPhongBan").click(function () {
                if ($("#checkboxallPhongBan").is(':checked')) {
                    $("#LichPhongBanThamGia > option").prop("selected", "selected");
                    $("#LichPhongBanThamGia").trigger("change");
                } else {
                    $("#LichPhongBanThamGia > option").removeAttr("selected");
                    $("#LichPhongBanThamGia").val(null).trigger("change");
                }
            });

            $("#btnChonNguoi").click(function () {
                var idSelected = $(this).attr("data-idSelected");
                openDialog("Chọn cán bộ", "/UserControls/AdminCMS/LichDonVi/pFormChooseThamGia.aspx", { "do": "CHONNGUOI", "idSelected": idSelected }, 1024);
            });
            $("#DBPhanLoai").val('<%=oLichPhong.DBPhanLoai%>');
            $("#UrlList").change(function () {
                $(".ldonvi").toggle();
                $(".lphongban").toggle();

            });
            if ($("#UrlList").val() == "/noidung/Lists/LichPhong") {
                $(".ldonvi").toggle();
                $(".lphongban").toggle();
            }
            // Denotes total number of rows
            var rowIdx = <%=countKhachMoi%>;
            var arrKM = <%=jsonKhachMoi%>;
            if (arrKM.length > 0) {
                for (var i = 0; i < arrKM.length; i++) {
                    $('#tbodyKhachMoi').append(`<tr id="R${i + 1}">
             <td class="row-index text-center">
             <p> <input type="text" name="HoTen${i + 1}" id="HoTen${i + 1}" class="form-control HoTen" placeholder="Họ tên" value="${arrKM[i]["Hoten"]}" /></p>
             </td>
            <td class="row-index text-center">
             <p> <input type="text" name="SDT${i + 1}" id="SDT${i + 1}" class="form-control SDT" placeholder="Số điện thoại" value="${arrKM[i]["SDT"]}" /></p>
             </td>
            <td class="row-index text-center">
             <p> <input type="text" name="CMNDCCCD${i + 1}" id="CMNDCCCD${i + 1}" class="form-control CMNDCCCD" placeholder="Chứng minh nhân dân/Căn cước" value="${arrKM[i]["CMNDCCCD"]}" /></p>
             </td>
              <td class="text-center">
                 <a class="remove" href="javascript:RemoveTR(${i});" title="Xóa"><i class="far fa-trash-alt"></i></a>
                </td>
              </tr>`);
                }
            }
            // jQuery button click event to add a row
            $('#addBtn').on('click', function () {

                // Adding a row inside the tbody.
                $('#tbodyKhachMoi').append(`<tr id="R${++rowIdx}">
             <td class="row-index text-center">
             <p> <input type="text" name="HoTen${rowIdx}" id="HoTen${rowIdx}" class="form-control HoTen" placeholder="Họ tên" value="" /></p>
             </td>
            <td class="row-index text-center">
             <p> <input type="text" name="SDT${rowIdx}" id="SDT${rowIdx}" class="form-control SDT" placeholder="Số điện thoại" value="" /></p>
             </td>
            <td class="row-index text-center">
             <p> <input type="text" name="CMNDCCCD${rowIdx}" id="CMNDCCCD${rowIdx}" class="form-control CMNDCCCD" placeholder="Chứng minh nhân dân/Căn cước" value="" /></p>
             </td>
              <td class="text-center">
                 <a class="remove" href="javascript:RemoveTR(${rowIdx});" title="Xóa"><i class="far fa-trash-alt"></i></a>
                </td>
              </tr>`);
            });
            //CHUAN_BIUser
            $("#CHUAN_BIUser").smSelect2018V2({
                dropdownParent: "#frm-LichPhong"
            });
            //CHU_TRIUser
            $("#CHU_TRIUser").smSelect2018V2({
                dropdownParent: "#frm-LichPhong"
            });
            $("#CHU_TRI").smSelect2018V2({
                dropdownParent: "#frm-LichPhong",
                parameterPlus: function (odata) {
                    odata["FieldOrder"] = "DMSTT";
                    odata["Ascending"] = true;

                }
            });
            //LichPhongBanThamGia
            $("#LichPhongBanThamGia").smSelect2018V2({
                dropdownParent: "#frm-LichPhong",
                closeOnSelect: false,
                parameterPlus: function (odata) {
                    odata["FieldOrder"] = "DMSTT";
                    odata["Ascending"] = true;

                }
            });
            $("#CHUAN_BI").smSelect2018V2({
                dropdownParent: "#frm-LichPhong",
                closeOnSelect: false,
                parameterPlus: function (odata) {
                    odata["FieldOrder"] = "DMSTT";
                    odata["Ascending"] = true;
                }
            });
            $("#LichLanhDaoChuTri").smSelect2018V2({
                dropdownParent: "#frm-LichPhong",
                Ajax: true,
                parameterPlus: function (odata) {
                    var $UrlList = $("#UrlList").val();
                    if ($UrlList == '/noidung/Lists/LichDonVi') {
                        if ($("#CHU_TRI").val() == '' || $("#CHU_TRI").val() == '0') {
                            odata["Groups"] = "<%=CurentUser.Groups.ID %>";
                            odata["PerQuery"] = "<%=CurentUser.Groups.ID %>_5001";
                        } else {
                            odata["UserPhongBan"] = $("#CHU_TRI").val();
                            odata["GetUserPhongBan"] = true,
                                odata["PerQuery"] = "<%=CurentUser.Groups.ID %>_01010306";
                        }
                    } else {
                        odata["UserPhongBan"] = "<%=CurentUser.UserPhongBan.ID %>";
                        odata["GetUserPhongBan"] = true,
                            odata["PerQuery"] = "<%=CurentUser.Groups.ID %>_01010306";
                    }
                },
            });

            $("#Title").focus();
            $("#frm-LichPhong .datetime").daterangepicker({
                timePicker: true,
                singleDatePicker: true,
                autoUpdateInput: false,
                timePicker24Hour: true,
                locale: { format: 'DD/MM/YYYY HH:mm' }
            }).on("apply.daterangepicker", function (e, picker) {
                picker.element.val(picker.startDate.format(picker.locale.format));
            });

            $("#frm-LichPhong").validate({
                //ignore: [],
                rules: {
                    Title: "required",
                    //btnChonNguoi: "LichThanhPhanThamGiaLichPhong",
                    LichPhongBanThamGia: "PHongBanThamGiaDonVi",
                    LichThoiGianBatDau: "required",
                    //LichThoiGianKetThuc: "required",
                    LichDiaDiem: "required",
                    LichNoiDung: "required",
                },
                messages: {
                    Title: "Vui lòng nhập tên cuộc họp",
                    LichLanhDaoChuTri: "Vui lòng nhập lãnh đạo chủ trì",
                    LichThoiGianBatDau: "Vui lòng nhập ngày bắt đầu",
                    LichThoiGianKetThuc: "Vui lòng nhập ngày kết thúc",
                    LichDiaDiem: "Vui lòng nhập địa điểm",
                    LichNoiDung: "Vui lòng nhập nội dung",
                },

                submitHandler: function (form) {
                    if ($("#UrlList").val() == "/noidung/Lists/LichPhong") {
                        value = $("#lstLichThamGia").val();
                        if (value == '' || value == undefined || value == null) {
                            alert("Chọn cán bộ tham gia");
                            //BootstrapDialog.alert('Chọn cán bộ tham gia');

                            return false;
                        }
                    };
                    if ($("#UrlList").val() == "/noidung/Lists/LichDonVi") {
                        //alert($("#CHU_TRI").val() + "--" + $("#CHU_TRIKhac").val());
                        if ($("#CHU_TRI").val() == '' & $("#CHU_TRIKhac").val() == '') {
                            alert("Chọn/Nhập chủ trì cuộc họp");
                            //BootstrapDialog.alert('Chọn/Nhập chủ trì cuộc họp');
                            return false;
                        }
                        if ($("#CHUAN_BI").val() == '' & $("#CHUAN_BIKhac").val() == '') {
                            alert('Chọn/Nhập chuẩn bị cuộc họp');
                            return false;
                        }
                    };
                    var from = $("#LichThoiGianBatDau").val();
                    var to = $("#LichThoiGianKetThuc").val();
                    console.log(moment(from, 'dd/MM/YYYY HH:mm'));
                    console.log(moment(to, 'dd/MM/YYYY HH:mm'));
                    if (to != "" && moment(from, 'dd/MM/YYYY HH:mm') > moment(to, 'dd/MM/YYYY HH:mm')) {
                        alert('Thời gian bắt đầu phải nhỏ hơn thời gian kết thúc');
                    }
                    else {
                        //nếu là lịch đơn vị reque

                        var objKhachMoi = [];
                        $('#tbodyKhachMoi > tr').each(function (index, tr) {
                            var $Hoten = $(tr).find('.HoTen').val();
                            var $SDT = $(tr).find('.SDT').val();
                            var $CMNDCCCD = $(tr).find('.CMNDCCCD').val();
                            objKhachMoi.push({
                                Hoten: $Hoten,
                                SDT: $SDT,
                                CMNDCCCD: $CMNDCCCD,
                            });

                        });
                        $("#JsonKhachMoi").val(JSON.stringify(objKhachMoi));
                        loading();
                        $.post("/UserControls/AdminCMS/LichHop/pAction.asp", $(form).viSerialize(), function (result) {
                            Endloading();
                            if (result.State == 2) {
                                BootstrapDialog.show({
                                    title: "Lỗi",
                                    message: result.Message
                                });
                            }
                            else {
                                showMsg(result.Message);
                                //$("#btn-find-LichPhong").trigger("click");
                                $(form).closeModal();
                                $('#tblLichPhong').DataTable().ajax.reload();
                                $("#lichPhongBan_div_parent").data("smGrid2018").RefreshGrid();
                                $("#lichDonVi_div_parent").data("smGrid2018").RefreshGrid();
                            }
                        }).always(function () { });
                    }
                }
            });
            $("#frm-LichPhong").viForm();
        });
    </script>
</body>
</html>

