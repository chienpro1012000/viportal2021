﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.LichDonVi;
using ViPortalData.LichPhong;

namespace VIPortal.APP.UserControls.Public.Portal.Meetings
{
    public partial class pformGiaoLichPhong : pFormBase
    {
        public GiaoLichDonVi oGiaoLichDonvi { get; set; }
        public List<int> lstItemID { get; set; }
        public LichPhongItem objLichHop { get; private set; }
        public string lstThanhPhanTG { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            objLichHop = new LichPhongItem();
            if (!string.IsNullOrEmpty(Request["ItemIDs"]))
            {
                lstItemID = Request["ItemIDs"].Split(',').Select(x => Convert.ToInt32(x)).ToList();
                if (lstItemID.Count > 0)
                {
                    LichPhongDA oLichPhongDA = new LichPhongDA();
                    objLichHop = oLichPhongDA.GetByIdToObject<LichPhongItem>(lstItemID[0]);
                    if (objLichHop.LichThanhPhanThamGia != null && objLichHop.LichThanhPhanThamGia.Count > 0)
                    {
                        LUserDA lUserDA = new LUserDA();
                        List<LUserJson> lstUser =  lUserDA.GetListJson(new LUserQuery() { isGetBylistID = true, lstIDget = objLichHop.LichThanhPhanThamGia.Select(x => x.LookupId).ToList() });
                        lstThanhPhanTG = string.Join(",", lstUser.Where(x=>x.UserPhongBan.ID == CurentUser.UserPhongBan.ID).Select(x => x.ID));
                    }
                }

            }
            oGiaoLichDonvi = new GiaoLichDonVi();


        }
    }
}