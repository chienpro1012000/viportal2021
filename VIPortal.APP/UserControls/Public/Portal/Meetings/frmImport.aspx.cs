﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;

namespace VIPortal.APP.UserControls.Public.Portal.Meetings
{
    public partial class frmImport : pFormBase
    {
        public int fldGroup { get; set; }
        public DateTime TuNgay { get; set; }
        public DateTime DenNgay { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request["fldGroup"]))
            {
                fldGroup = Convert.ToInt32(Request["fldGroup"]);
            }
        }
    }
}