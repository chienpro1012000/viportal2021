﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using VIPortalAPP;

namespace VIPortal.APP.UserControls.Public.Portal.Meetings
{
    public partial class UC_LichHop : BaseUC_Web
    {
        int intCurYear = System.DateTime.Today.Year;

        public DateTime firstDay = System.DateTime.Today;
        
        int intCurWeek = 1;

        string strYear = "";
        string strlastyear = "";
        string strnextyear = "";

        string strWeek = "";
        string strlastweek = "";
        string strnextweek = "";
        public List<LichHopJson> lstLichHop { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            LayNgayDauVaKhoiTaoGiaTriTuan_Nam();
        }

        protected void HienThiSuKienTrongNgay(DateTime dtCurDay, int SoTTNgayTrongTuan)
        {
            try
            {
                Label lblLich;
                try
                {
                    lblLich = (Label)this.FindControl("lblDay" + SoTTNgayTrongTuan.ToString());
                    lblLich.Text = string.Format("{0:dd/MM/yyyy}", dtCurDay);
                }
                catch (Exception exp)
                {
                    // Page.Response.Write(exp.ToString());
                }

                LichHopDA objLichPhongban = new LichHopDA(UrlSite);
                lstLichHop = objLichPhongban.GetListJson(new LichHopQuery()
                {
                    TuNgay_LichHop = dtCurDay,
                    DenNgay_LichHop = dtCurDay,
                    _ModerationStatus = 1
                });
                //SPQuery spQuery = LaySuKienTheoNgay(dtCurDay, intCurPhongban);
                //SPListItemCollection dsSuKien = objLichPhongban.LaySuKienTheoNgay(spQuery);
                if (lstLichHop != null && lstLichHop.Count > 0)
                {
                    foreach (LichHopJson curitem in lstLichHop)
                    {
                        string strKetQua = "";

                        strKetQua = strKetQua + "<table class='tblChiTietLich' cellpadding='0' cellspacing='0'>";
                        //strKetQua = strKetQua + "<tbody>";
                        strKetQua = strKetQua + "<tr class='clsle'>";
                        strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 7% ; text-align:center'>" + curitem.LichThoiGianBatDau.Value.ToString("HH:mm") + "-" +  curitem.LichThoiGianKetThuc.Value.ToString("HH:mm") + "</td>";
                        strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 45%'>" + curitem.Title + "</td>";
                        strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 10%'>" + curitem.LichLanhDaoChuTri.Title + "</td>";
                        strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'>" + curitem.LichDiaDiem + "</td>";
                        strKetQua = strKetQua + "</tr>";
                        //strKetQua = strKetQua + "</tbody>";
                        strKetQua = strKetQua + "</table>";
                        lblLich = (Label)this.FindControl("lblTime" + SoTTNgayTrongTuan.ToString());
                        lblLich.Text = lblLich.Text + strKetQua;
                    }
                }
                else
                {
                    string strKetQua = "";

                    strKetQua = strKetQua + "<table class='tblChiTietLich' cellpadding='0' cellspacing='0'>";
                    //strKetQua = strKetQua + "<tbody>";
                    strKetQua = strKetQua + "<tr class='clsle'>";
                    strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 7% ; text-align:center'></td>";
                    strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 45%'></td>";
                    strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 10%'></td>";
                    strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'></td>";
                    strKetQua = strKetQua + "</tr>";
                    //strKetQua = strKetQua + "</tbody>";
                    strKetQua = strKetQua + "</table>";
                    lblLich = (Label)this.FindControl("lblTime" + SoTTNgayTrongTuan.ToString());
                    lblLich.Text = lblLich.Text + strKetQua;
                }
            }
            catch (Exception ex)
            {
                //Response.Write(ex.ToString());
            }
        }
        protected void LayNgayDauVaKhoiTaoGiaTriTuan_Nam()
        {
            // lay nam
            try
            {
                if (Request["p_year"] != null) strYear = Request["p_year"].ToString().Trim();
                intCurYear = int.Parse(strYear);
                if (intCurYear > 9999 || intCurYear < 1900)
                {
                    intCurYear = System.DateTime.Today.Year;
                    strYear = intCurYear.ToString();
                }
            }
            catch
            {
                intCurYear = System.DateTime.Today.Year;
                strYear = intCurYear.ToString();
            }

            int SoTuanTrongNam = LaySoTuanTrongNam(intCurYear);

            // lay tuan
            try
            {
                if (Request["p_week"] != null) strWeek = Request["p_week"].ToString().Trim();
                intCurWeek = int.Parse(strWeek);

                int startdayofyear = ThuCuaNgayDauNam(intCurYear);
                if (intCurWeek < 1 || intCurWeek > SoTuanTrongNam)
                {
                    if (intCurYear != DateTime.Today.Year) intCurWeek = 1;
                    else
                    {
                        // lay tuan hien thoi
                        DateTime today = System.DateTime.Today;

                        int NgayHienthoi = today.DayOfYear + startdayofyear;
                        intCurWeek = NgayHienthoi / 7;
                        if (NgayHienthoi % 7 > 0) intCurWeek = intCurWeek + 1;
                    }
                }
                strWeek = intCurWeek.ToString();

            }
            catch
            {

                int startdayofyear = ThuCuaNgayDauNam(intCurYear);

                if (intCurYear != DateTime.Today.Year) intCurWeek = 1;
                else
                {
                    // lay tuan hien thoi
                    DateTime today = System.DateTime.Today;

                    int NgayHienthoi = today.DayOfYear + startdayofyear;
                    intCurWeek = NgayHienthoi / 7;
                    if (NgayHienthoi % 7 > 0) intCurWeek = intCurWeek + 1;
                }
                strWeek = intCurWeek.ToString();
            }

            // lay gia tri cua ngay dau tuan

            firstDay = NgayDauTuan(intCurWeek, intCurYear);

            //lblNgayChon.Text = "Từ ngày: " + string.Format("{0:dd/MM/yyyy}", firstDay) + " - Đến ngày: " + string.Format("{0:dd/MM/yyyy}", firstDay.AddDays(6));

            if (intCurWeek == SoTuanTrongNam)
            {
                strnextweek = "1";
                int nam = intCurYear + 1;
                strnextyear = nam.ToString();

                int tuan = intCurWeek - 1;
                strlastweek = tuan.ToString();
                strlastyear = strYear;

            }
            else if (intCurWeek == 1)
            {


                int tuan = intCurWeek + 1;
                strnextweek = tuan.ToString();
                strnextyear = strYear;


                int nam = intCurYear - 1;
                strlastyear = nam.ToString();
                tuan = LaySoTuanTrongNam(nam);
                strlastweek = tuan.ToString();

            }
            else
            {

                strnextyear = strYear;
                strlastyear = strYear;


                int tuan = intCurWeek + 1;
                strnextweek = tuan.ToString();

                tuan = intCurWeek - 1;
                strlastweek = tuan.ToString();
            }

        }

        public static DateTime NgayDauTuan(int CurWeek, int CurYear)
        {
            //Khai bao, khoi tao gia tri ngay dau tuan
            DateTime firstDate = new DateTime(CurYear, 1, 1);
            // khoi tao ngay dau tien cua nam
            DateTime startYear = new DateTime(CurYear, 1, 1);
            // khoi tao ngay cuoi cung cua nam
            DateTime endYear = new DateTime(CurYear, 12, 31);
            // khai bao ngay dau tuan dau tien cua nam
            DateTime NgayDauTuanThu1 = new DateTime(CurYear, 1, 1);
            // So ngay trong nam
            int SoNgayTrongNam = endYear.DayOfYear;

            // kiem tra ngay dau tien cua nam vao thu nao
            int startdayofyear = ThuCuaNgayDauNam(CurYear);


            // Lay so tuan trong 1 nam
            int SoTuanTrongNam = LaySoTuanTrongNam(CurYear);

            // kiem tra so tuan truyen vao co hop le hay khong
            // neu khong hop le se lay tuan hien thoi
            if (CurWeek < 1 || CurWeek > SoTuanTrongNam)
            {
                if (CurYear != DateTime.Today.Year) CurWeek = 1;
                else
                {
                    // lay tuan hien thoi
                    DateTime today = System.DateTime.Today;

                    int NgayHienthoi = today.DayOfYear + startdayofyear;
                    CurWeek = NgayHienthoi / 7;
                    if (NgayHienthoi % 7 > 0) CurWeek = CurWeek + 1;
                }
            }

            // lay ngay dau tien cua tuan dau tien
            NgayDauTuanThu1 = startYear.AddDays(startdayofyear * -1);

            int NgayDauTuan = (CurWeek - 1) * 7;
            // Lay ngay dau tien cua tuan can lay
            firstDate = NgayDauTuanThu1.AddDays(NgayDauTuan);

            // tra ve ngay dau tien cua tuan can lay
            return firstDate;
        }
        /// <summary>
        /// Lay ra gia tri int cua thu - ngay dau nam
        /// </summary>
        /// <param name="nam">Nam</param>
        /// <returns>0: Thu 2; 1: thu 3; ..... 5: thu bay,6: Chu Nhat</returns>
        public static int ThuCuaNgayDauNam(int nam)
        {
            // khoi tao ngay dau tien cua nam
            DateTime startYear = new DateTime(nam, 1, 1);
            int startdayofyear = 0;

            if (startYear.DayOfWeek == DayOfWeek.Monday) startdayofyear = 0; // thu 2
            else if (startYear.DayOfWeek == DayOfWeek.Tuesday) startdayofyear = 1; // thu 3
            else if (startYear.DayOfWeek == DayOfWeek.Wednesday) startdayofyear = 2;
            else if (startYear.DayOfWeek == DayOfWeek.Thursday) startdayofyear = 3;
            else if (startYear.DayOfWeek == DayOfWeek.Friday) startdayofyear = 4;
            else if (startYear.DayOfWeek == DayOfWeek.Saturday) startdayofyear = 5;
            else startdayofyear = 6;
            return startdayofyear;
        }
        /// <summary>
        /// Lay so tuan trong 1 nam
        /// </summary>
        /// <param name="nam">Nam can lay</param>
        /// <returns>So tuan, tinh tu tuan 1</returns>
        public static int LaySoTuanTrongNam(int nam)
        {
            // kiem tra ngay dau tien cua nam vao thu nao
            int startdayofyear = ThuCuaNgayDauNam(nam);

            DateTime endYear = new DateTime(nam, 12, 31);
            // khai bao ngay dau tuan dau tien cua nam
            int SoNgayTrongNam = endYear.DayOfYear;

            // Lay so tuan trong 1 nam
            int SoTuanTrongNam = 0;
            SoTuanTrongNam = (SoNgayTrongNam + startdayofyear) / 7;
            int SoDu = (SoNgayTrongNam + startdayofyear) % 7;
            if (SoDu > 0) SoTuanTrongNam = SoTuanTrongNam + 1;

            return SoTuanTrongNam;
        }
    }
}