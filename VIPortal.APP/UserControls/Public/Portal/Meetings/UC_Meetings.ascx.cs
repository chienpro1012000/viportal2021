﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortalData;

namespace VIPortal.APP.UserControls.Public.Portal.Meetings
{
    public partial class UC_Meetings : BaseUC_Web
    {
        public string weekPoral { get; set; }
        public LGroupItem oDonVi { get; private set; }
        public DateTime? today { get; set; }
        public int intCurYear = System.DateTime.Today.Year;

        public DateTime? firstDay = System.DateTime.Today;
        public int intCurWeek = 1;

        string strYear = "";

        public string strWeek = "";
        public string LichNotification = "";
        public int NamHienTai { get; set; }
        public int weeknext { get; set; }
        public int curWeekPrint { get; set; }
        public int curYearPrint { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
           
            today = DateTime.Now;
            LayNgayDauVaKhoiTaoGiaTriTuan_Nam();
            LichNotification = Request["Lich"];
            if (ItemID > 0)
            {
                firstDate = null;
            }
            LGroupDA oLGroupDA = new LGroupDA();
            oDonVi = new LGroupItem();
            if (CurentUser.Groups.ID > 0 && CurentUser != null)
            {
                oDonVi = oLGroupDA.GetByIdToObject<LGroupItem>(CurentUser.Groups.ID);

            }
        }
        public static int ThuCuaNgayDauNam(int nam)
        {
            // khoi tao ngay dau tien cua nam
            DateTime startYear = new DateTime(nam, 1, 1);
            int startdayofyear = 0;

            if (startYear.DayOfWeek == DayOfWeek.Monday) startdayofyear = 0; // thu 2
            else if (startYear.DayOfWeek == DayOfWeek.Tuesday) startdayofyear = 1; // thu 3
            else if (startYear.DayOfWeek == DayOfWeek.Wednesday) startdayofyear = 2;
            else if (startYear.DayOfWeek == DayOfWeek.Thursday) startdayofyear = 3;
            else if (startYear.DayOfWeek == DayOfWeek.Friday) startdayofyear = 4;
            else if (startYear.DayOfWeek == DayOfWeek.Saturday) startdayofyear = 5;
            else if (startYear.DayOfWeek == DayOfWeek.Sunday) startdayofyear = -1;
            else startdayofyear = 6;
            return startdayofyear;
        }
        public static int LaySoTuanTrongNam(int nam)
        {
            // kiem tra ngay dau tien cua nam vao thu nao
            int startdayofyear = ThuCuaNgayDauNam(nam);

            DateTime endYear = new DateTime(nam, 12, 31);
            // khai bao ngay dau tuan dau tien cua nam
            int SoNgayTrongNam = endYear.DayOfYear;

            // Lay so tuan trong 1 nam
            int SoTuanTrongNam = 0;
            SoTuanTrongNam = (SoNgayTrongNam + startdayofyear) / 7;
            int SoDu = (SoNgayTrongNam + startdayofyear) % 7;
            if (SoDu > 0) SoTuanTrongNam = SoTuanTrongNam + 1;

            return SoTuanTrongNam;
        }
        public DateTime? firstDate { get; set; }
        public DateTime? NgayDauTuan(int CurWeek, int CurYear)
        {
            //Khai bao, khoi tao gia tri ngay dau tuan
            firstDate = new DateTime(CurYear, 1, 1);
            // khoi tao ngay dau tien cua nam
            DateTime startYear = new DateTime(CurYear, 1, 1);
            // khoi tao ngay cuoi cung cua nam
            DateTime endYear = new DateTime(CurYear, 12, 31);
            // khai bao ngay dau tuan dau tien cua nam
            DateTime NgayDauTuanThu1 = new DateTime(CurYear, 1, 1);
            // So ngay trong nam
            int SoNgayTrongNam = endYear.DayOfYear;

            // kiem tra ngay dau tien cua nam vao thu nao
            int startdayofyear = ThuCuaNgayDauNam(CurYear);


            // Lay so tuan trong 1 nam
            int SoTuanTrongNam = LaySoTuanTrongNam(CurYear);

            // kiem tra so tuan truyen vao co hop le hay khong
            // neu khong hop le se lay tuan hien thoi
            if (CurWeek < 1 || CurWeek > SoTuanTrongNam)
            {
                if (CurYear != DateTime.Today.Year) CurWeek = 1;
                else
                {
                    // lay tuan hien thoi
                    DateTime today = System.DateTime.Today;

                    int NgayHienthoi = today.DayOfYear + startdayofyear;
                    CurWeek = NgayHienthoi / 7;
                    if (NgayHienthoi % 7 > 0) CurWeek = CurWeek + 1;
                }
            }

            // lay ngay dau tien cua tuan dau tien
            NgayDauTuanThu1 = startYear.AddDays(startdayofyear * -1);

            int NgayDauTuan = (CurWeek - 1) * 7;
            // Lay ngay dau tien cua tuan can lay
            firstDate = NgayDauTuanThu1.AddDays(NgayDauTuan);

            // tra ve ngay dau tien cua tuan can lay
            return firstDate;
        }
        protected void LayNgayDauVaKhoiTaoGiaTriTuan_Nam()
        {

            // lay nam
            try
            {
                // lay thong tin nam
                if (!string.IsNullOrEmpty(Request["p_year"]))
                    intCurYear = int.Parse(Request["p_year"]);
                else intCurYear = System.DateTime.Today.Year;
                if (!string.IsNullOrEmpty(Request["p_week"]))
                {
                    strWeek = Request["p_week"].Trim();
                    intCurWeek = Convert.ToInt32(strWeek);
                }
                else
                {
                    DateTime today = System.DateTime.Today;
                    int startdayofyear = ThuCuaNgayDauNam(intCurYear);
                    int NgayHienthoi = today.DayOfYear + startdayofyear;
                    intCurWeek = NgayHienthoi / 7;
                    if (NgayHienthoi % 7 > 0) intCurWeek = intCurWeek + 1;
                }
                NamHienTai = LaySoTuanTrongNam(intCurYear);

                if (intCurYear > 9999 || intCurYear < 1900)
                {
                    intCurYear = System.DateTime.Today.Year;
                    strYear = intCurYear.ToString();
                }
            }
            catch
            {
                intCurYear = System.DateTime.Today.Year;
                strYear = intCurYear.ToString();
            }
            if (Request["next"] != null && Request["next"].ToString() == "next")
            {
                intCurWeek = intCurWeek + 1;
                if (intCurWeek > NamHienTai)
                {
                    //strYear = Page.Request["p_year"].ToString().Trim() + 1;
                    intCurYear++;
                    intCurWeek = 1;
                }
                curWeekPrint = intCurWeek;
                curYearPrint = intCurYear;
            }
            else if (Request["last"] != null && Request["last"].ToString() == "last")
            {
                intCurWeek = intCurWeek - 1;
                if (intCurWeek == 0)
                {
                    //strYear = Page.Request["p_year"].ToString().Trim() + 1;
                    intCurYear--;
                    intCurWeek = LaySoTuanTrongNam(intCurYear);
                }
                curWeekPrint = intCurWeek;
                curYearPrint = intCurYear;
            }
            else
            {
                curWeekPrint = intCurWeek;
                curYearPrint = intCurYear;
            }
            // lay gia tri cua ngay dau tuan

            firstDay = NgayDauTuan(intCurWeek, intCurYear);
            if (firstDay.HasValue)
                weekPoral = "Từ ngày: " + string.Format("{0:dd/MM/yyyy}", firstDay) + " - Đến ngày: " + string.Format("{0:dd/MM/yyyy}", firstDay.Value.AddDays(6));
            
            
        }
    }
}