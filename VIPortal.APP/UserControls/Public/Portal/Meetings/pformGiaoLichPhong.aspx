﻿<%@ page language="C#" autoeventwireup="true" codebehind="pformGiaoLichPhong.aspx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.Meetings.pformGiaoLichPhong" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmGiaoLich" class="form-horizontal">
        <input type="hidden" name="ItemIDs" id="ItemIDs" value="<%=Request["ItemIDs"] %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" name="TPRemove" id="TPRemove" value="<%=lstThanhPhanTG %>" />
        <fieldset class="border p-2">
            <legend class="w-auto">Thông tin giao lịch</legend>
            <div class="form-group row">
                <label for="LichGhiChuGiao" class="col-sm-2 control-label">Ghi chú</label>
                <div class="col-sm-10">
                    <textarea name="TBNoiDung" id="TBNoiDung" placeholder="Nhập ghi chú" class="form-control"><%=objLichHop.TBNoiDung %></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="LichThanhPhanThamGia" class="col-sm-2 control-label">Cá nhân tham gia</label>
                <div class="col-sm-10">
                    <select data-selected="<%=lstThanhPhanTG %>" multiple="multiple" data-url="/UserControls/AdminCMS/LUser/pAction.asp?do=QUERYDATA&Groups=<%=CurentUser.Groups.ID %>&UserPhongBan=<%=CurentUser.UserPhongBan.ID %>&GetUserPhongBan=true" data-place="Chọn người" name="LichThanhPhanThamGia" id="LichThanhPhanThamGia" class="form-control"></select>
                </div>
            </div>
        </fieldset>

        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
           
            $("#LichThanhPhanThamGia").smSelect2018V2({
                dropdownParent: "#frmGiaoLich",
                closeOnSelect: false,
            });
            
            $("#frmGiaoLich").validate({
                rules: {
                    LichThanhPhanThamGia: "required"
                },
                messages: {
                    LichThanhPhanThamGia: "Vui lòng chọn cán bộ"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/AdminCMS/LichPhong/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-DMTrangThai").trigger("click");
                            $("#lichPhongBan_div_parent").data("smGrid2018").RefreshGrid();
                            $(form).closeModal();
                            //$('#tblDMTrangThai').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frmGiaoLich").viForm();
        });
    </script>
</body>
</html>
