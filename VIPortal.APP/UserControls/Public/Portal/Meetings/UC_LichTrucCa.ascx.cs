﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;

namespace VIPortal.APP.UserControls.Public.Portal.Meetings
{
    public partial class UC_LichTrucCa : BaseUC_Web
    {
        public DateTime TuNgay { get; set; }
        public DateTime DenNgay { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            TuNgay = DateTime.Now.Date.AddDays(-1); //new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DenNgay =  new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).AddDays(-1);
            
        }
    }
}