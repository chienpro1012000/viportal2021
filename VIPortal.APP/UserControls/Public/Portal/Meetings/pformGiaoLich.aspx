﻿<%@ page language="C#" autoeventwireup="true" codebehind="pformGiaoLich.aspx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.Meetings.pformGiaoLich" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmGiaoLich" class="form-horizontal">
        <input type="hidden" name="ItemIDs" id="ItemIDs" value="<%=Request["ItemIDs"] %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Nhận lịch về đơn vị</label>
            <div class="col-sm-10">
                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked="checked" disabled="disabled" />
            </div>
        </div>
        <fieldset class="border p-2">
            <legend class="w-auto">Thông tin giao lịch</legend>
            <div class="form-group row">
                <label for="LichLanhDaoDonvi" class="col-sm-2 control-label">Lãnh đạo đơn vị</label>
                <div class="col-sm-10">
                    <select data-selected="<%=oGiaoLichDonvi.LichLanhDaoChuTri.LookupId %>" data-url="/UserControls/AdminCMS/LUser/pAction.asp?do=QUERYDATA&Groups=<%=CurentUser.Groups.ID %>&PerQuery=<%=CurentUser.Groups.ID %>_5001" data-place="Chọn người" name="LichLanhDaoChuTri" id="LichLanhDaoChuTri" class="form-control"></select>
                </div>
            </div>
            <div class="form-group row" id="DivLichphongPB">
                <label for="LichPhongBanThamGia" class="col-sm-2 control-label">Thành phần tham gia</label>
                <div class="col-sm-10">
                      <div class="input-group mb-3">
                    <select data-selected="<%=string.Join(",", oGiaoLichDonvi.LichPhongBanThamGia.Select(x=>x.LookupId)) %>" multiple="multiple" data-url="/UserControls/AdminCMS/LGroup/pAction.asp?do=QUERYDATA&FieldOrder=DMSTT&Ascending=True&GroupParent=<%=CurentUser.Groups.ID %>&isGetByParent=true" data-place="Chọn Phòng ban" name="LichPhongBanThamGia" id="LichPhongBanThamGia" class="form-control"></select>
                          <div class="input-group-append">
                            <input type="checkbox" id="checkboxallPhongBan" />Tất cả
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="LichThanhPhanThamGia" class="col-sm-2 control-label">Cá nhân tham gia</label>
                <div class="col-sm-10">
                    <select data-selected="<%=string.Join(",", oGiaoLichDonvi.LichThanhPhanThamGia.Select(x=>x.LookupId)) %>" multiple="multiple" data-url="/UserControls/AdminCMS/LUser/pAction.asp?do=QUERYDATA&moder=1&Groups=<%=CurentUser.Groups.ID %>" data-place="Chọn cán bộ tham gia" name="LichThanhPhanThamGia" id="LichThanhPhanThamGia" class="form-control"></select>
                </div>
            </div>
            <div class="form-group row">
                <label for="LichDiaDiem" class="col-sm-2 control-label">Địa điểm</label>
                <div class="col-sm-10">
                    <input type="text" name="LichDiaDiem" id="LichDiaDiem" placeholder="Nhập địa điểm" class="form-control" value="<%=LichDiaDiem %>" />
                </div>
            </div>
            <div class="form-group row">
                <label for="CHUAN_BI" class="col-sm-2 control-label">Chuẩn bị</label>
                <div class="col-sm-10">
                    <input type="text" name="CHUAN_BI" id="CHUAN_BI" placeholder="Nhập chuẩn bị" class="form-control" value="<%=CHUAN_BI %>" />
                </div>
            </div>
            <div class="form-group row">
                <label for="LichGhiChuGiao" class="col-sm-2 control-label">Ghi chú</label>
                <div class="col-sm-10">
                    <textarea name="LichGhiChuGiao" id="LichGhiChuGiao" placeholder="Nhập Ghi chú" class="form-control"><%=oGiaoLichDonvi.LichGhiChuGiao %></textarea>
                </div>
            </div>
            
            <div class="form-group row">
                <label for="LichKhachMoi" class="col-sm-2 control-label">Khách mời</label>
                <div class="col-sm-10">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center">Tên</th>
                                <th class="text-center">Số điện thoại</th>
                                <th class="text-center">CMND/CCCD</th>
                                <th class="text-center" style="width: 5%">Xóa</th>
                            </tr>
                        </thead>
                        <tbody id="tbodyKhachMoi">
                            <%for (int i = 0; i < oGiaoLichDonvi.JsonKhachMoi.Count; i++)
                            {%>
                            <tr id="R<%=i %>">
             <td class="row-index text-center">
             <p> <input type="text" name="HoTen<%=i %>" class="HoTen" id="HoTen<%=i %>" placeholder="Họ tên" value="<%=oGiaoLichDonvi.JsonKhachMoi[i].Hoten %>"  /></p>
             </td>
            <td class="row-index text-center">
             <p> <input type="text" name="SDT<%=i %>" class="SDT" id="SDT<%=i %>" placeholder="Số điện thoại" value="<%=oGiaoLichDonvi.JsonKhachMoi[i].SDT %>" /></p>
             </td>
            <td class="row-index text-center">
             <p> <input type="text" name="CMNDCCCD<%=i %>" class="CMNDCCCD" id="CMNDCCCD<%=i %>" placeholder="Chứng minh nhân dân/Căn cước" value="<%=oGiaoLichDonvi.JsonKhachMoi[i].CMNDCCCD %>" /></p>
             </td>
              <td class="text-center">
                 <a class="remove" href="javascript:RemoveTR(<%=i %>);" title="Xóa"><i class="far fa-trash-alt"></i></a>
                </td>
              </tr>
                            <%} %>
                        </tbody>
                    </table>
                    <button class="btn btn-md btn-primary"
                        id="addBtn" type="button">
                        Thêm khách mời
                    </button>
                </div>
            </div>
            <div class="form-group row">
                <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
                <div class="col-xs-12 col-sm-10 col-md-10">
                    <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
                </div>
            </div>
        </fieldset>

        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        function RemoveTR($rowidx) {
            $("tr#R" + $rowidx).remove();
        }
        $(document).ready(function () {
            var KieuDonvi = '<%=oDonVi.KieuDonVi%>';
            console.log(KieuDonvi);
            if (KieuDonvi == '2') {
                $("#DivLichphongPB").hide();
            }
            $("#checkboxallPhongBan").click(function () {
                if ($("#checkboxallPhongBan").is(':checked')) {
                    $("#LichPhongBanThamGia > option").prop("selected", "selected");
                    $("#LichPhongBanThamGia").trigger("change");
                } else {
                    $("#LichPhongBanThamGia > option").removeAttr("selected");
                    $("#LichPhongBanThamGia").val(null).trigger("change");
                }
            });
            var rowIdx = <%=oGiaoLichDonvi.JsonKhachMoi.Count%>;
            // jQuery button click event to add a row
            $('#addBtn').on('click', function () {

                // Adding a row inside the tbody.
                $('#tbodyKhachMoi').append(`<tr id="R${++rowIdx}">
             <td class="row-index text-center">
             <p> <input type="text" name="HoTen${rowIdx}" class="HoTen" id="HoTen${rowIdx}" class="form-control" placeholder="Họ tên" value="" /></p>
             </td>
            <td class="row-index text-center">
             <p> <input type="text" name="SDT${rowIdx}" class="SDT" id="SDT${rowIdx}" class="form-control" placeholder="Số điện thoại" value="" /></p>
             </td>
            <td class="row-index text-center">
             <p> <input type="text" name="CMNDCCCD${rowIdx}" class="CMNDCCCD"  id="CMNDCCCD${rowIdx}" class="form-control" placeholder="Chứng minh nhân dân/Căn cước" value="" /></p>
             </td>
              <td class="text-center">
                 <a class="remove" href="javascript:RemoveTR(${rowIdx});" title="Xóa"><i class="far fa-trash-alt"></i></a>
                </td>
              </tr>`);
            });

            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(lstFileAttach)%>'
            });
            $("#LichLanhDaoChuTri").smSelect2018V2({
                dropdownParent: "#frmGiaoLich"
            });
            $("#LichPhongBanThamGia").smSelect2018V2({
                dropdownParent: "#frmGiaoLich",
                closeOnSelect: false,
                dropdownCssClass: "HasCheckbox"
            });
            $("#LichThanhPhanThamGia").smSelect2018V2({
                dropdownParent: "#frmGiaoLich",
                closeOnSelect: false,
                TemplateText: "{Title}-{UserPhongBan.Title}"
            });
            $("#LichPhongChuTri").smSelect2018V2({
                dropdownParent: "#frmGiaoLich"
            });
            $("#frmGiaoLich").validate({
                rules: {
                    Title: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    var objKhachMoi = [];
                    $('#tbodyKhachMoi > tr').each(function (index, tr) {
                        var $Hoten = $(tr).find('.HoTen').val();
                        var $SDT = $(tr).find('.SDT').val();
                        var $CMNDCCCD = $(tr).find('.CMNDCCCD').val();
                        objKhachMoi.push({
                            Hoten : $Hoten,
                            SDT: $SDT,
                            CMNDCCCD: $CMNDCCCD,
                        });

                    });

                    var objdata = $(form).siSerializeArray();
                    objdata["JsonKhachMoi"] = JSON.stringify(objKhachMoi);
                    console.log(objdata);
                    loading();
                    $.post("<%=urlDoAction%>", objdata, function (result) {
                        Endloading();
                        if (result.State == 2) {

                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        } else if (result.State == 1) {
                            //cảnh báo thì sao nhỉ?
                            BootstrapDialog.confirm({
                                title: 'Cảnh báo trùng',
                                message: result.Message + ". Bạn có chắc chắn giao lịch này không?",
                                type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                closable: true, // <-- Default value is false
                                draggable: true, // <-- Default value is false
                                btnCancelLabel: 'Không đồng ý', // <-- Default value is 'Cancel',
                                btnOKLabel: 'Đồng ý!', // <-- Default value is 'OK',
                                btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
                                callback: function (result) {
                                    // result will be true if button was click, while it will be false if users close the dialog directly.
                                    if (result) {
                                        objdata["NoCheck"] = 1;
                                        loading();
                                        $.post("<%=urlDoAction%>", objdata, function (result) {
                                            Endloading();
                                            if (result.State == 2) {
                                                BootstrapDialog.show({
                                                    title: "Lỗi",
                                                    message: result.Message
                                                });
                                            }
                                            else {
                                                showMsg(result.Message);
                                                $(form).closeModal();
                                                //$('#tblDMTrangThai').DataTable().ajax.reload();
                                                $("#lich_div_parent").data("smGrid2018").RefreshGrid();
                                                $("#lichDonVi_div_parent").data("smGrid2018").RefreshGrid();
                                            }

                                        }).always(function () {

                                        });
                                    } else {
                                        $(form).closeModal();
                                    }
                                }
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-DMTrangThai").trigger("click");
                            $(form).closeModal();
                            //$('#tblDMTrangThai').DataTable().ajax.reload();
                            $("#lich_div_parent").data("smGrid2018").RefreshGrid();
                            $("#lichDonVi_div_parent").data("smGrid2018").RefreshGrid();
                        }
                    }).always(function () { });
                }
            });
        });
    </script>
</body>
</html>
