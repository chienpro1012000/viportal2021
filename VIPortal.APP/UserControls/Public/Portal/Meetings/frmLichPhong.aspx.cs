﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData.LichDonVi;
using ViPortalData.LichPhong;

namespace VIPortal.APP.UserControls.Public.Portal.Meetings
{
    public partial class frmLichPhong : pFormBase
    {
        public LichPhongItem oLichPhong { get; set; }
        public string jsonKhachMoi { get; set; }
        public int CHU_TRIID { get; set; }
        public string CHU_TRIKhac { get; set; }
        public string CHUAN_BI_Khac { get; set; }
        public string CHUAN_BIID { get; set; }
        public int countKhachMoi { get; set; }
        public string LichSoNguoiThamgia { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            jsonKhachMoi = "[]";
            CHU_TRIKhac = "";
            oLichPhong = new LichPhongItem();
            int IDCopy = 0;
            if (!string.IsNullOrEmpty(Request["IDCopy"]))
            {
                IDCopy = Convert.ToInt32(Request["IDCopy"]);
                ItemID = IDCopy;
            }
            if (ItemID > 0)
            {
                if (!string.IsNullOrEmpty(Request["TypeLich"]) && Request["TypeLich"] == "LichDonVi")
                {
                    LichDonViDA lichDonViDA = new LichDonViDA();
                    oLichPhong = lichDonViDA.GetByIdToObject<LichPhongItem>(ItemID);
                    if (!string.IsNullOrEmpty(oLichPhong.LogNoiDung))
                    {
                        GiaoLichDonVi giaoLichDonVi = new GiaoLichDonVi();
                        giaoLichDonVi = Newtonsoft.Json.JsonConvert.DeserializeObject<GiaoLichDonVi>(oLichPhong.LogNoiDung);
                        if (giaoLichDonVi.JsonKhachMoi != null && giaoLichDonVi.JsonKhachMoi.Count > 0)
                        {
                            jsonKhachMoi = Newtonsoft.Json.JsonConvert.SerializeObject(giaoLichDonVi.JsonKhachMoi);
                            countKhachMoi = giaoLichDonVi.JsonKhachMoi.Count;
                        }
                        LichSoNguoiThamgia = giaoLichDonVi.LichSoNguoiThamgia;
                    }
                }
                else
                {
                    LichPhongDA oLichPhongDA = new LichPhongDA();
                    oLichPhong = oLichPhongDA.GetByIdToObject<LichPhongItem>(ItemID);
                    if (!string.IsNullOrEmpty(oLichPhong.LogNoiDung))
                    {
                        GiaoLichDonVi giaoLichDonVi = new GiaoLichDonVi();
                        giaoLichDonVi = Newtonsoft.Json.JsonConvert.DeserializeObject<GiaoLichDonVi>(oLichPhong.LogNoiDung);
                        if (giaoLichDonVi.JsonKhachMoi != null && giaoLichDonVi.JsonKhachMoi.Count > 0)
                        {
                            jsonKhachMoi = Newtonsoft.Json.JsonConvert.SerializeObject(giaoLichDonVi.JsonKhachMoi);
                            countKhachMoi = giaoLichDonVi.JsonKhachMoi.Count;
                        }
                    }
                }
                if (!string.IsNullOrEmpty(oLichPhong.CHUAN_BI))
                {
                    List<LookupData> CHUAN_BI = clsFucUtils.GetDanhSachMutilLoookupByStringFiledData(oLichPhong.CHUAN_BI);
                    if (CHUAN_BI.Count == 1 && CHUAN_BI[0].ID == -1)
                    {
                        CHUAN_BI_Khac = CHUAN_BI[0].Title;
                    }
                    else
                        CHUAN_BIID = string.Join(",", CHUAN_BI.Select(x => x.ID));

                }
                if (!string.IsNullOrEmpty(oLichPhong.CHU_TRI))
                {

                    LookupData CHU_TRI = clsFucUtils.GetLoookupByStringFiledData(oLichPhong.CHU_TRI);
                    CHU_TRIID = CHU_TRI.ID;
                    if (CHU_TRIID == -1)
                    {
                        CHU_TRIKhac = CHU_TRI.Title;
                    }
                }

            }
            else
            {
                oLichPhong.LichThoiGianBatDau = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 8, 0, 0);
            }
            if(IDCopy > 0)
            {
                ItemID = 0;
                oLichPhong.ID = 0;

            }
            if (!string.IsNullOrEmpty(Request["LichTrangThai"]))
            {
                oLichPhong.LichTrangThai = Convert.ToInt32(Request["LichTrangThai"]);
            }
            else oLichPhong.LichTrangThai = 1;//default để là 1.
        }
    }
}