﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmImport.aspx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.Meetings.frmImport" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frmImprtData" class="form-horizontal">
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <input type="hidden" name="fldGroup" id="fldGroup" value="<%=fldGroup %>" />
        <div class="form-group row">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#FileAttach").regFileUpload({
            });
            $("#frmImprtData").validate({
                rules: {
                    Title: "required"
                },
                messages: {
                    Title: "Vui lòng nhập tiêu đề"
                },
                submitHandler: function (form) {
                    loading();
                    $.post("/UserControls/AdminCMS/LichCaTruc/pAction.asp", $(form).viSerialize(), function (result) {
                        Endloading();
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-DMCaTruc").trigger("click");
                            $(form).closeModal();
                            $("#lich_div_parent").data("smGrid2018").RefreshGrid();
                        }
                    }).always(function () { });
                }
            });
        });
    </script>
</body>
</html>
