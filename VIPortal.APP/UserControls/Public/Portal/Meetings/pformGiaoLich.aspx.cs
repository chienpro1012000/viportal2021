﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;
using ViPortalData;
using ViPortalData.LichDonVi;
using ViPortalData.LichTongCongTy;

namespace VIPortal.APP.UserControls.Public.Portal.Meetings
{
    public partial class pformGiaoLich : pFormBase
    {
        public List<int> lstItemID { get; set; }
        public string urlDoAction { get; set; }
        public List<FileAttach> lstFileAttach { get; set; }
        public List<KhachMoi> lstKhachMoi { get; set; }
        public GiaoLichDonVi oGiaoLichDonvi { get; set; }
        public string LichDiaDiem { get; set; }
        public string CHUAN_BI { get; set; }
        public LGroupItem oDonVi { get; private set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            lstFileAttach = new List<FileAttach>();
            oGiaoLichDonvi = new GiaoLichDonVi();
            urlDoAction = "/UserControls/AdminCMS/LichTongCongTy/pAction.asp";
            if (!string.IsNullOrEmpty(Request["ItemIDs"]))
            {
                lstItemID = Request["ItemIDs"].Split(',').Select(x => Convert.ToInt32(x)).ToList();
            }
            if(doAction == "GIAOLICHDONVI")
            {
                ItemID = lstItemID[0];
                LichTongCongTyDA oLichTongCongTyDA = new LichTongCongTyDA();
                LichTongCongTyItem oLichTongCongTyItem = oLichTongCongTyDA.GetByIdToObject<LichTongCongTyItem>(ItemID);
                LichDiaDiem = oLichTongCongTyItem.LichDiaDiem;
                CHUAN_BI = oLichTongCongTyItem.CHUAN_BI;
            }
            if(doAction == "SUAGIAOLICHDONVI") //chỗ này viết vậy đã chưa xong 12/05/2022
            {
                ItemID = lstItemID[0];
                LichDonViDA lichDonViDA = new LichDonViDA();
                LichDonViItem oLichDonViItem = lichDonViDA.GetByIdToObject<LichDonViItem>(ItemID);
                LichDiaDiem = oLichDonViItem.LichDiaDiem;
                CHUAN_BI = oLichDonViItem.CHUAN_BI;
                if (!string.IsNullOrEmpty(oLichDonViItem.LogNoiDung))
                {
                    oGiaoLichDonvi = Newtonsoft.Json.JsonConvert.DeserializeObject<GiaoLichDonVi>(oLichDonViItem.LogNoiDung);
                    lstFileAttach = oGiaoLichDonvi.ListFileAttach.Select(x => new FileAttach()
                    {
                        Name = x.Name,
                        Url = x.Url
                    }).ToList();

                }
                
                //urlDoAction = "/UserControls/AdminCMS/LichDonVi/pAction.asp";
            }
            LGroupDA oLGroupDA = new LGroupDA();
            oDonVi = new LGroupItem();
            if (CurentUser.Groups.ID > 0 && CurentUser != null)
            {
                oDonVi = oLGroupDA.GetByIdToObject<LGroupItem>(CurentUser.Groups.ID);

            }
        }
    }
}