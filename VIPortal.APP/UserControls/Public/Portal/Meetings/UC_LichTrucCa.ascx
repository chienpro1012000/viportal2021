﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_LichTrucCa.ascx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.Meetings.UC_LichTrucCa" %>
<style>
    .display-flex-righteous {
        justify-content: normal !important;
        padding-bottom: 15px;
    }



    .time {
        color: red;
    }

    .details-control {
        cursor: pointer;
        color: cornflowerblue;
        text-align: left !important;
    }

    .rowdetail table {
        background-color: #f1d293;
    }

    .lich_div_1 {
        text-align: center;

        margin-bottom: 30px;
    }

    .main-calendar-datepicke .main-news-event {
        margin-top: 15px;
    }

    .fa-check-circle {
        color: red;
    }
</style>
<section class="pb-1 mt-1 LichTrucCa" id="lichhop" data-role="fullGrid">
    <div class="header-calendar-datepicke" style="background-color: #fff;" id="content">
        <div class="left_header_calendar pt-3 pb-3">
            <div class="list_calendar">
                <div class="item_li_1 active" data-tab-target="#tab-1" style="cursor: pointer;">
                    <a href="#tab-1" onclick="show('tab-1');" class="font-size-14" data-lang="LichTrucCT">Lịch trực công ty</a>
                </div>
                <div class="item_li_1" data-tab-target="#tab-2" style="cursor: pointer;">
                    <a href="#tab-2" onclick="show('tab-2');" data-lang="LichTrucPhong">Lịch trực phòng</a>
                </div>
            </div>
        </div>
    </div>
    <script id="jsDetail" type="text/x-kendo-template">
        <tr class="rowdetail">
                        <td colspan="7">
                            <table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px; width:100%;">
                                <tr>
                                    <td class="text-right" style="width:20%;">Ghi chú</td>
                                    <td class="text-left">#=LichGhiChu#</td>
                                </tr>
                                <tr>
                                    <td class="text-right">Thành phần tham gia</td>
                                    <td class="text-left">#=LichThanhPhanThamGia.map(x=>x.LookupValue).join(", ")#</td>
                                </tr>
                                <tr>
                                    <td class="text-right">Nội dung chuẩn bị</td>
                                    <td class="text-left">#=NoiDungChuanBi#</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
    </script>
    <script id="jsDetailTapDoan" type="text/x-kendo-template">
        <tr class="rowdetail">
                        <td colspan="7">
                            <table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px; width:100%;">
                                <tr>
                                    <td class="text-right" style="width:20%;">Ghi chú</td>
                                    <td class="text-left">#=LichGhiChu#</td>
                                </tr>
                                <tr>
                                    <td class="text-right">Thành phần tham gia</td>
                                    <td class="text-left">#=LichThanhPhanThamGia.map(x=>x.LookupValue).join(", ")#</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
    </script>
    <script id="jsTemplateLichHop" type="text/x-kendo-template">
                    <tr class="font-size-14">
                        <td>#=Title#</td>
                        <td class="tghop color-table-meeting font-weight-bold">#=new Date(LichThoiGianBatDau).format('dd/MM (HH:mm)')# - #=new Date(LichThoiGianKetThuc).format('dd/MM (HH:mm)')#</td>
                        <td>#=LichDiaDiem#</td>
                        <td>#=LichLanhDaoChuTri.Title#</td>
                        <td class="details-control" data-ItemID="#=ID#">#if(LichNoiDung != null){##=LichNoiDung##}else{# #}#</td>
                    </tr>
    </script>

    <div data-Per="<%=CurentUser.PermissionsQuery %>" class="main-calendar-datepicke mt-3" style="background-color: #fff;">
        
        <div id="tab-1" class="main-news-event" style="display: block;">
            <section class="pb-1 mt-1">
                <div id="lich_div_parent" data-role="fullGrid">
                    <div class="lich_div_1">
                        <font face="Tahoma" color="#0033CC" style="font-size: 17pt">
                            <b data-lang="LichTrucCongTy">LỊCH TRỰC CÔNG TY</b>
                        </font>
                    </div>
                   
                    <div class="display-flex-righteous formsearchpublic" id="frmSearchLichTCTY" role="search">
                        <div class="items-form" style="width: 20%">
                            <input type="text" name="keywordUser" data-lang="PlaceSearch" placeholder="Nhập từ khóa">
                            <input type="hidden" name="SearchInAdvance" value="Title">
                            <input type="hidden" name="p_week" class="week" value="" />
                        </div>
                        <div class="items-form">
                            <input type="text" class="input-datetime" name="SearchTuNgay" id="TuNgay_LichHopTCTY" value="<%:string.Format("{0:dd/MM/yyyy}",TuNgay)%>" placeholder="Từ ngày">
                        </div>
                        <div class="items-form">
                            <input type="text" class="input-datetime" name="SearchDenNgay" id="DenNgay_LichHopTCTY" value="<%:string.Format("{0:dd/MM/yyyy}",DenNgay)%>" placeholder="Đến ngày">
                        </div>
                        <div class="icon-button display-flex pl-2">
                            <button data-toggle="tooltip" data-placement="top" title="Xóa lọc tất cả" class="reset_data"><i class="fas fa-sync-alt"></i></button>
                            <button type="button" class="btn btnsearch btn-search-meeting" data-lang="TimKiem">Tìm kiếm</button>
                            <%if(!string.IsNullOrEmpty(CurentUser.PermissionsQuery) && CurentUser.PermissionsQuery.Contains("010108")){ %>
                            <button type="button" id="btnThemMoi" class="btn btn-search-meeting" data-lang="ThemMoi">Thêm mới</button>
                            <button type="button" id="btnThemMoi2" class="btn btn-search-meeting" data-lang="ThemMoi2">Thêm mới 2</button>
                            <button type="button" id="btnThemMoi3" class="btn btn-search-meeting" data-lang="ThemMoi3">Thêm mới 3</button>
                            <button type="button" id="btnImportMauExcelDonvi" class="btn btn-search-meeting" data-lang="ImportExcel">Import excel</button>
                            <%} %>
                           
                        </div>
                    </div>
                    <div class="lich_div_2">
                        <div style="width: 100%;" class="lichDiv">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tbody>
                                    <tr>
                                        <td style="">
                                            <div style="margin: 20px auto 10px; width: 100%;">
                                                <div style="background: rgb(198, 219, 255) no-repeat scroll left top; clear: both; display: block; -moz-background-clip: -moz-initial; -moz-background-origin: -moz-initial; -moz-background-inline-policy: -moz-initial; margin-bottom: 10px; font-size: 12px; font-family: Arial;">
                                                    <div style="clear: both; background-color: #e8eef7">
                                                        <table class="tblHeader" width="100%" border="0" cellpadding="1" cellspacing="1" style="background-color: #e8eef7; border: 1px solid #72a1de">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="date_left" valign="middle" width="7%" align="left" style="border-right: solid 1px #C6DBFF; background-color: #ffffcc;">
                                                                        <b data-lang="ThuNgay">Thứ/Ngày</b>
                                                                    </td>
                                                                    <td class="date_right" valign="middle" width="91%" align="left" style="background-color: #ffffcc;">
                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="date_right lich_Header" valign="middle" width="13%" align="center">
                                                                                        <b data-lang="CaTrucThoiGian">Ca Trực/Thời gian</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle"width="15%"  align="center">
                                                                                        <b data-lang="ChucDanh">Chức danh</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="" align="center">
                                                                                        <b data-lang="NguoiTruc">Người trực</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="15%" align="center">
                                                                                        <b data-lang="SoDienThoai">Số điện thoại</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="15%" align="center">
                                                                                        <b data-lang="Email">Email</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="15%" align="center">
                                                                                        <b data-lang="GhiChu">Ghi chú</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="3%" align="center">
                                                                                        <b></b>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div style="clear: both;">
                                                        <table class="clscontentLich" width="100%" border="0" cellpadding="1" cellspacing="1">
                                                            <tbody role="grid">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <script id="jsTemplateNgay" type="text/x-kendo-template">
                  <tr>
                                                        <td class="date_left" valign="middle" width="7%" align="left">
                                                            <font color="\#000000">
                                                                <b>#=Thu#</b><br>
                                                                <b>#=Ngay#
                                                                 </b>
                                                            </font>
                                                        </td>
                                                        <td class="date_right" valign="middle" width="91%" align="left">
                                                           #=Content#
                                                        </td>
                                                    </tr>
                </script>
            </section>
        </div>
        <div id="tab-2" class="main-news-event" style="display: none">
            <section class="pb-1 mt-1">
                <div id="lichDonVi_div_parent" data-role="fullGrid">
                    <div class="lich_div_1">
                        <font face="Tahoma" color="#0033CC" style="font-size: 17pt">
                            <b data-lang="LichTrucPhong1">LỊCH TRỰC PHÒNG</b>
                        </font>
                    </div>
                   
                    <div class="display-flex-righteous formsearchpublic" id="frmSearchLichDonVi" role="search">
                        <div class="items-form" style="width: 20%">
                            <input type="text" name="keywordUser" placeholder="Nhập từ khóa">
                            <input type="hidden" name="SearchInAdvance" value="Title,UserEmail,UserSDT,UserChucVu">
                        </div>
                        <div class="items-form">
                            <input type="text" class="input-datetime" name="SearchTuNgay" id="SearchTuNgay" value="<%:string.Format("{0:dd/MM/yyyy}",TuNgay)%>" placeholder="Từ ngày">
                        </div>
                        <div class="items-form">
                            <input type="text" class="input-datetime" name="SearchDenNgay" id="SearchDenNgay" value="<%:string.Format("{0:dd/MM/yyyy}",DenNgay)%>" placeholder="Đến ngày">
                        </div>
                        <div class="icon-button display-flex pl-2">
                            <button data-toggle="tooltip" data-placement="top" title="Xóa lọc tất cả" class="reset_data"><i class="fas fa-sync-alt"></i></button>
                            <button type="button" class="btn btnsearch btn-search-meeting" data-lang="TimKiem">Tìm kiếm</button>
                            <%if(!string.IsNullOrEmpty(CurentUser.PermissionsQuery) && CurentUser.PermissionsQuery.Contains("010107")){ %>
                            <button type="button" id="btnThemMoiPB" class="btn btn-search-meeting" data-lang="ThemMoi">Thêm mới</button>
                            <button type="button" id="btnThemMoiPB2" class="btn btn-search-meeting" data-lang="ThemMoi2">Thêm mới 2</button>
                            <button type="button" id="btnThemMoiMultiPB" class="btn btn-search-meeting" data-lang="ThemMoiTuDong">Thêm mới tự động</button>
                            <button type="button" id="btnMauExcel" class="btn btn-search-meeting" data-lang="XuatExcMau">Xuất excel(Mẫu)</button>
                             <button type="button" id="btnImportMauExcelPB" class="btn btn-search-meeting" data-lang="ImportExcel">Import excel</button>
                            <%} %>
                        </div>
                    </div>
                    <div class="lich_div_2">
                        <div style="width: 100%;" class="lichDiv">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tbody>
                                    <tr>
                                        <td style="">
                                            <div style="margin: 20px auto 10px; width: 100%;">
                                                <div style="background: rgb(198, 219, 255) no-repeat scroll left top; clear: both; display: block; -moz-background-clip: -moz-initial; -moz-background-origin: -moz-initial; -moz-background-inline-policy: -moz-initial; margin-bottom: 10px; font-size: 12px; font-family: Arial;">
                                                    <div style="clear: both; background-color: #e8eef7">
                                                        <table class="tblHeader" width="100%" border="0" cellpadding="1" cellspacing="1" style="background-color: #e8eef7; border: 1px solid #72a1de">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="date_left" valign="middle" width="7%" align="left" style="border-right: solid 1px #C6DBFF; background-color: #ffffcc;">
                                                                        <b data-lang="ThuNgay">Thứ/Ngày</b>
                                                                    </td>
                                                                    <td class="date_right" valign="middle" width="91%" align="left" style="background-color: #ffffcc;">
                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="date_right lich_Header" valign="middle" width="13%" align="center">
                                                                                        <b data-lang="CaTrucThoiGian">Ca Trực/Thời gian</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle"width="15%"  align="center">
                                                                                        <b data-lang="ChucDanh">Chức danh</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="" align="center">
                                                                                        <b data-lang="NguoiTruc">Người trực</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="15%" align="center">
                                                                                        <b data-lang="SoDienThoai">Số điện thoại</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="15%" align="center">
                                                                                        <b data-lang="Email">Email</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="15%" align="center">
                                                                                         <b data-lang="GhiChu">Ghi chú</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="3%" align="center">
                                                                                        <b></b>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div style="clear: both;">
                                                        <table class="clscontentLich" width="100%" border="0" cellpadding="1" cellspacing="1">
                                                            <tbody role="grid">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>
            </section>
        </div>
    </div>
</section>

<script type="text/javascript">
    var ItemActive = "";
    //  js nhảy tab 
    function show(idtab) {
        $("#content .item_li_1").removeClass("active");
        $(".left_header_calendar [data-tab-target='#" + idtab + "']").addClass("active");
        $(".main-news-event").css("display", "none");
        $("#" + idtab).css("display", "block");

    }
    
    function XoaCaTruc(itemid) {
        BootstrapDialog.confirm({
            title: 'Xác nhận',
            message: "Bạn có chắc chắn xóa bản ghi này?",
            type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
            closable: true, // <-- Default value is false
            draggable: true, // <-- Default value is false
            btnCancelLabel: 'Hủy', // <-- Default value is 'Cancel',
            btnOKLabel: 'Đồng ý!', // <-- Default value is 'OK',
            btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
            callback: function (result) {
                // result will be true if button was click, while it will be false if users close the dialog directly.
                if (result) {
                    $.post("/UserControls/AdminCMS/LichCaTruc/pAction.asp", { ItemID: itemid, "do": "DELETE" }).done(function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            $("#lich_div_parent").data("smGrid2018").RefreshGrid();
                            $("#lichDonVi_div_parent").data("smGrid2018").RefreshGrid();
                            //REFRESH LẠI THÔNG TIN.
                        }

                    }).always(function () {

                    });
                } else {
                    //alert(123);
                }
            }
        });
    }
    function SuaCaTruc(itemid) {

        openDialog("Sửa lịch trực công ty", "/UserControls/AdminCMS/LichCaTruc/pFormLichCaTruc.aspx", { ItemID: itemid }, 800);
    }
    function SuaCaTrucPhong(itemid) {
        openDialog("Sửa lịch trực phòng", "/UserControls/AdminCMS/LichCaTruc/pFormLichCaTrucPhong.aspx", { ItemID: itemid }, 800);
    }
    $(document).ready(function () {
        SetConfigLang('<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>', 'LichTrucCa');
        var curentTab = location.hash;
        //alert(curentTab);
        if (curentTab != "" && curentTab != null) {
            $('.list_calendar a[href="' + curentTab + '"]').click();
        }
        $("#btnMauExcel").click(function () {
            var SearchTuNgay = $("#SearchTuNgay").val();
            var SearchDenNgay = $("#SearchDenNgay").val();
            window.open(`/VIPortalAPI/api/LichLamViec/ExportExcel?SearchTuNgay=${SearchTuNgay}&SearchDenNgay=${SearchDenNgay}&fldGroup=<%=CurentUser.UserPhongBan.ID%>&UrlFix=<%=UrlSite%>`, "_blank");
        });
        $("#btnImportMauExcelDonvi").click(function () {
            openDialog("Import lịch trực công ty", "/UserControls/Public/Portal/Meetings/frmImport.aspx", { "fldGroup": <%=CurentUser.Groups.ID%>, "do" :"ImportExcel" }, 700);
        });
        //
        //btnImportMauExcelPB
        $("#btnImportMauExcelPB").click(function () {
            openDialog("Import lịch trực phòng", "/UserControls/Public/Portal/Meetings/frmImport.aspx", { "fldGroup": <%=CurentUser.UserPhongBan.ID%>, "do": "ImportExcel" }, 700);
        });
        $("#btnThemMoi").click(function () {
            openDialog("Thêm mới lịch trực công ty", "/UserControls/AdminCMS/LichCaTruc/pFormLichCaTruc.aspx", { "fldGroup": <%=CurentUser.Groups.ID%> }, 800);
        });
        //btnThemMoi2
        $("#btnThemMoi2").click(function () {
            var SearchTuNgay = $("#TuNgay_LichHopTCTY").val();
            var SearchDenNgay = $("#DenNgay_LichHopTCTY").val();
            openDialog("Thêm mới lịch trực công ty", "/UserControls/AdminCMS/LichCaTruc/pFormLichCaTruc2.aspx", {
                "fldGroup": <%=CurentUser.Groups.ID%> , "SearchTuNgay": SearchTuNgay, "SearchDenNgay": SearchDenNgay, "do": "CREATEMULTIV2"
                }, 1000);
        });
        $("#btnThemMoi3").click(function () {
            var SearchTuNgay = $("#TuNgay_LichHopTCTY").val();
            var SearchDenNgay = $("#DenNgay_LichHopTCTY").val();
            openDialog("Thêm mới lịch trực công ty", "/UserControls/AdminCMS/LichCaTruc/pFormLichCaTruc3.aspx", {
                "fldGroup": <%=CurentUser.Groups.ID%> , "SearchTuNgay": SearchTuNgay, "SearchDenNgay": SearchDenNgay, "do": "CREATEMULTIV3"
            }, 1000);
        });
        //btnThemMoiPB2
        $("#btnThemMoiPB2").click(function () {
            var SearchTuNgay = $("#SearchTuNgay").val();
            var SearchDenNgay = $("#SearchDenNgay").val();
            openDialog("Thêm mới lịch trực công ty", "/UserControls/AdminCMS/LichCaTruc/pFormLichCaTruc2.aspx", {
                "fldGroup": <%=CurentUser.UserPhongBan.ID%> , "SearchTuNgay": SearchTuNgay, "SearchDenNgay": SearchDenNgay, "do": "CREATEMULTIV2"
            }, 1000);
        });
        //btnThemMoiMulti
        $("#btnThemMoiMulti").click(function () {
            openDialog("Thêm mới lịch trực công ty tự động", "/UserControls/AdminCMS/LichCaTruc/pFormTuDongSinhLich.aspx", { "do": "CREATEMULTI", "fldGroup": <%=CurentUser.Groups.ID%> }, 800);
        });
        //
        //btnThemMoiMultiPB
        $("#btnThemMoiMultiPB").click(function () {
            openDialog("Thêm mới lịch trực phòng tự động", "/UserControls/AdminCMS/LichCaTruc/pFormTuDongSinhLich.aspx", { "do": "CREATEMULTI", "fldGroup": <%=CurentUser.UserPhongBan.ID%> }, 800);
        });
        //btnThemMoiPB
        $("#btnThemMoiPB").click(function () {
            openDialog("Thêm mới lịch trực phòng", "/UserControls/AdminCMS/LichCaTruc/pFormLichCaTrucPhong.aspx", { "fldGroup": <%=CurentUser.UserPhongBan.ID%>}, 800);
        });
        $("#lich_div_parent .lnkLastMonth").click(function () {
            var clsweekcur = $("#lich_div_parent .week").val();
            var clsweekcurInt = parseInt(clsweekcur);
            $("#lich_div_parent .week").val(clsweekcurInt - 1);
            $("#lich_div_parent").data("smGrid2018").RefreshGrid();
        });
        $("#lich_div_parent .lnkNexkMonth").click(function () {
            var clsweekcur = $("#lich_div_parent .week").val();
            var clsweekcurInt = parseInt(clsweekcur);
            $("#lich_div_parent .week").val(clsweekcurInt + 1);
            $("#lich_div_parent").data("smGrid2018").RefreshGrid();
            
        });

        $("#lich_div_parent").smGrid2018({
            "TypeTemp": "kendo",
            "UrlPost": "/VIPortalAPI/api/LichLamViec/GetListCaTrucHienThi",
            odata: { "do": "QUERYDATA", "fldGroup": <%=CurentUser.Groups.ID%>  },
            template: "jsTemplateNgay",
            dataBound: function (e) {
                
            },
        });

        //$('[data-toggle="tooltip"]').tooltip();
        setTimeout(
            function () {
                
                //lichDonVi_div_parent
                $("#lichDonVi_div_parent").smGrid2018({
                    "TypeTemp": "kendo",
                    "UrlPost": "/VIPortalAPI/api/LichLamViec/GetListCaTrucHienThi",
                    odata: { "do": "QUERYDATA", "UrlList": "/noidung/Lists/LichDonVi", "fldGroup": <%=CurentUser.UserPhongBan.ID%>  },
                    template: "jsTemplateNgay",
                    dataBound: function (e) {
                        
                    },
                });


            }, 200);
    });
</script>

