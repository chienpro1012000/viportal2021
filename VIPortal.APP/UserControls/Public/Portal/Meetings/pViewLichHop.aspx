﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pViewLichHop.aspx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.Meetings.pViewLichHop" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên</label>
            <div class="col-sm-10">
                <%=oLichHop.Title%>
            </div>
        </div>
        
        <div class="form-group row">
            <label for="LichLanhDaoChuTri" class="col-sm-2 control-label">Lãnh đạo chủ trì</label>
            <div class="col-sm-4">
                <%=oLichHop.LichLanhDaoChuTri.LookupValue%>
            </div>
            <label for="LichDiaDiem" class="col-sm-2 control-label">Địa điểm</label>
            <div class="col-sm-4">
                <%=oLichHop.LichDiaDiem%>
            </div>
        </div>
        <div class="form-group row">
            <label for="LichGhiChu" class="col-sm-2 control-label">Ghi chú</label>
            <div class="col-sm-10">
                <%=oLichHop.LichGhiChu%>
            </div>
        </div>
        <div class="form-group row">
            <label for="LichNoiDung" class="col-sm-2 control-label">Nội dung</label>
            <div class="col-sm-10">
                <%=oLichHop.LichNoiDung%>
            </div>
        </div>
        <div class="form-group row">
            <label for="LichThanhPhanThamGia" class="col-sm-2 control-label">Thành phần tham gia</label>
            <div class="col-sm-10">
                <%=string.Join(", ", oLichHop.LichThanhPhanThamGia.Select(x=>x.LookupValue))%>
            </div>
        </div>
        <div class="form-group row">
            <label for="LichThoiGianBatDau" class="col-sm-2 control-label">Thời gian bắt đầu</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oLichHop.LichThoiGianBatDau)%>
            </div>
        </div>
        <div class="form-group row">
            <label for="LichThoiGianKetThuc" class="col-sm-2 control-label">Thời gian kết thúc</label>
            <div class="col-sm-10">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oLichHop.LichThoiGianKetThuc)%>
            </div>
        </div>

        <%--<div class="form-group row">
            <label class="col-sm-2 control-label">File đính kèm</label>
            <div class="col-sm-10">
                <%for(int i=0; i< oLichHop.ListFileAttach.Count;i++){ %>
                <div><a href="<%=oLichHop.ListFileAttach[i].Url %>"><%=oLichHop.ListFileAttach[i].Name%></a></div>
                <%} %>
            </div>
        </div>--%>
        <div class="clearfix"></div>
    </form>
</body>
</html>
