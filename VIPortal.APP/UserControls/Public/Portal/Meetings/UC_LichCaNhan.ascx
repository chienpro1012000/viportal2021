﻿<%@ control language="C#" autoeventwireup="true" codebehind="UC_LichCaNhan.ascx.cs" inherits="VIPortal.APP.UserControls.Public.Portal.Meetings.UC_LichCaNhan" %>
<section class="pb-3 mt-5">
    <div id="lich_div_parent" data-role="fullGrid">
        <div id="lich_div_1">
            <font face="Tahoma" color="#0033CC" style="font-size: 17pt">
                <b>LỊCH CÁ NHÂN</b>
            </font>
        </div>
        <div class="today2">
            <a href="javascript:;" class="lnkLastMonth">
                <img src="/Content/themeV1/icon/left.gif" alt="left" class="next1" border="0">Tuần trước</a>
            &emsp;&emsp;&emsp;&emsp;&emsp;
            <a href="javascript:;" class="lnkNexkMonth">
                Tuần sau <img src="/Content/themeV1/icon/right.gif" alt="left" class="next1" border="0"></a>
            <div id="detail-week" style="padding-top:10px; font-weight:600"></div>
            <div style="display:none" id="week"></div>
        </div>
        <div class="display-flex-righteous formsearchpublic" id="frmSearchLich" role="search">
            <div class="items-form" style="width: 20%">
                <input type="text" name="keyword" placeholder="Nhập từ khóa">
                <input type="hidden" name="SearchInAdvance" value="Title,LichNoiDung,LichLanhDaoChuTri,LichDiaDiem,FullText">
            </div>
            <div class="items-form">
                <input type="text" class="input-datetime" name="TuNgay_LichHop" id="TuNgay_LichHop" placeholder="Từ ngày">
            </div>
            <div class="items-form">
                <input type="text" class="input-datetime" name="DenNgay_LichHop" id="DenNgay_LichHop" placeholder="Đến ngày">
            </div>
            <div class="icon-button display-flex pl-2">
                <button data-toggle="tooltip" data-placement="top" title="Xóa lọc tất cả" class="reset_data"><i class="fas fa-sync-alt"></i></button>
                <button type="button" class="btn btnsearch btn-search-meeting">Tìm kiếm</button>
                 <button class="btn btn-search-meeting" id="btnAddLich" type="button" style="margin-left:15px">Thêm mới</button>
            </div>
        </div>
        <div id="lich_div_2">
            <div style="width: 100%; overflow: auto; height: 1495px;" id="lichDiv">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td style="">
                                <div style="margin: 20px auto 10px; width: 100%;">
                                    <div style="background: rgb(198, 219, 255) no-repeat scroll left top; clear: both; display: block; -moz-background-clip: -moz-initial; -moz-background-origin: -moz-initial; -moz-background-inline-policy: -moz-initial; margin-bottom: 10px; font-size: 12px; font-family: Arial;">
                                        <div style="clear: both; background-color: #e8eef7">
                                            <table class="tblHeader" width="100%" border="0" cellpadding="1" cellspacing="1" style="background-color: #e8eef7; border: 1px solid #72a1de">
                                                <tbody>
                                                    <tr>
                                                        <td class="date_left" valign="middle" width="9%" align="left" style="border-right: solid 1px #C6DBFF; background-color: #ffffcc;">
                                                            <b>Thứ/Ngày</b>
                                                        </td>
                                                        <td class="date_right" valign="middle" width="91%" align="left" style="background-color: #ffffcc;">
                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                <tbody>
                                                                     <tr>
                                                                                    <td class="date_right lich_Header" valign="middle" width="7%" align="center">
                                                                                        <b>Thời gian</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="" align="center">
                                                                                        <b>Nội dung</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="15%" align="center">
                                                                                        <b>Địa điểm</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="15%" align="center">
                                                                                        <b>Chủ trì/Chuẩn bị</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="15%" align="center">
                                                                                        <b>Thành phần tham gia</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="15%" align="center">
                                                                                        <b>Tham gia tại Đơn vị</b>
                                                                                    </td>
                                                                                    <td class="date_right lich_Header" valign="middle" width="3%" align="center">
                                                                                        <b>
                                                                                            <input type="checkbox" name="CheckALL" class="CheckALLDonvi" value="1" /></b>
                                                                                    </td>
                                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div style="clear: both;">
                                            <table class="clscontentLich" width="100%" border="0" cellpadding="1" cellspacing="1">
                                                <tbody role="grid">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>

    </div>
    <script id="jsTemplateNgay" type="text/x-kendo-template">
                  <tr>
                                                        <td class="date_left" valign="middle" width="9%" align="left">
                                                            <font color="\#000000">
                                                                <b>#=Thu#</b><br>
                                                                <b>#=Ngay#
                                                                 </b>
                                                            </font>
                                                        </td>
                                                        <td class="date_right" valign="middle" width="91%" align="left">
                                                           #=Content#
                                                        </td>
                                                    </tr>
    </script>
</section>
<style type="text/css">
    tr.clsdetail {
        color:red;
    }
</style>
<script type="text/javascript">

    $(document).ready(function () {
        $("#btnAddLich").click(function () {
            openDialog("Thêm mới lịch", "/UserControls/AdminCMS/LichCaNhan/pFormLichCaNhan.aspx", {}, 1024);
        });
        $("#lich_div_parent").smGrid2018({
            "TypeTemp": "kendo",
            "UrlPost": "/VIPortalAPI/api/LichLamViec/GetListLichHienThi",
            odata: { "do": "QUERYDATA", "UrlList": "/noidung/Lists/LichCaNhan", "CreatedUser": "<%=CurentUser.ID%>" },
            template: "jsTemplateNgay",
            dataBound: function (e) {
                $("#week").text(e.Query);
                $("#detail-week").text(e.result);
                $("td.linkview").click(function () {
                    var _ItemID = $(this).attr("data-ItemID");
                    if (_ItemID > 0) {
                        openDialogView("Xem chi tiết lịch", "/UserControls/Public/Portal/Meetings/pViewLichHop.aspx", { ItemID: _ItemID }, 1024);
                    }
                });
                $("td[data-itemid='<%=ItemID%>']").parent().addClass("clsdetail");
            },
        });
        $(".lnkLastMonth").click(function () {
            $("#lich_div_parent").smGrid2018({
                "TypeTemp": "kendo",
                "UrlPost": "/VIPortalAPI/api/LichLamViec/GetListLichHienThi",
                odata: { "do": "QUERYDATA", "last": "last", "p_week": $("#week").text() },
                template: "jsTemplateNgay",
                dataBound: function (e) {
                    $("#detail-week").text(e.result);
                    $("#week").text(e.Query);
                    $("td.linkview").click(function () {
                        var _ItemID = $(this).attr("data-ItemID");
                        if (_ItemID > 0) {
                            openDialogView("Xem chi tiết lịch", "/UserControls/Public/Portal/Meetings/pViewLichHop.aspx", { ItemID: _ItemID }, 1024);
                        }
                    });
                },
            });
        });
        $(".lnkNexkMonth").click(function () {
            $("#lich_div_parent").smGrid2018({
                "TypeTemp": "kendo",
                "UrlPost": "/VIPortalAPI/api/LichLamViec/GetListLichHienThi",
                odata: { "do": "QUERYDATA", "next": "next", "p_week": $("#week").text() },
                template: "jsTemplateNgay",
                dataBound: function (e) {
                    $("#detail-week").text(e.result);
                    $("#week").text(e.Query);
                    $("td.linkview").click(function () {
                        var _ItemID = $(this).attr("data-ItemID");
                        if (_ItemID > 0) {
                            openDialogView("Xem chi tiết lịch", "/UserControls/Public/Portal/Meetings/pViewLichHop.aspx", { ItemID: _ItemID }, 1024);
                        }
                    });
                },
            });
        });
    });
</script>
