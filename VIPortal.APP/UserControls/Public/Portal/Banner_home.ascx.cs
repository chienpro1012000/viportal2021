﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using VIPortalAPP;

namespace VIPortal.APP.UserControls.Public.Portal
{
    public partial class Banner_home : BaseUC_Web
    {
        public List<BannerJson> LstBanner = new List<BannerJson>();
        protected void Page_Load(object sender, EventArgs e)
        {
            BannerDA bannerDA = new BannerDA(UrlSite);
            LstBanner = bannerDA.GetListJson(new BannerQuery() { _ModerationStatus = 1 });
            
        }
    }
}