﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QuyChe.ascx.cs" Inherits="VIPortal.APP.UserControls.Public.Portal.QuyChe" %>



<script type="text/javascript">
    $(document).ready(function () {
        SetConfigLang('<%= Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages)%>', 'QuyCheHomeDiv');
        $.post("/VIPortalAPI/api/QuyCheNoiBo/QUERYDATA", { "FieldOrder": "Created", Ascending: false, "length": 5 }, function (odata) {
            var templateContent = $("#jsTemplateQuyChe").html();
            var template = kendo.template(templateContent);

            odata.data = jQuery.map(odata.data, function (n, i) {
                odata.data[i].STT = i + 1;
                return odata.data[i];
            });

            var result = kendo.render(template, odata.data); //render the template

            $("#lstQuyTrinh").html(result); //append the result to the page
        });
    });
</script>
<%if(!string.IsNullOrEmpty(HtmlOutput)){ %>
<%=HtmlOutput %>
<%}else{ %>
<div class="regulation mt-3 box-shadow-basic card pt-3 QuyCheHomeDiv">
    <div class="regulation_header pl-3 pr-3">
        <div class="title_regulation active">
            <a>
                <img src="/Content/themeV1/icon/card.png" alt=""><span class="pl-2" data-lang="QuyCheMoi">Quy chế mới</span>
            </a>
            <p class="margin-p pt-2 pb-2" data-lang="ThongTinQuyCheMoiBoSung">Thông tin quy chế mới bổ sung</p>
        </div>
        <div class="next_regulation">
            <a href="/Pages/regulation.aspx"><span class="pr-2" data-lang="XemTatCa">Xem tất cả</span><svg class="svg-inline--fa fa-angle-double-right fa-w-14" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-double-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34zm192-34l-136-136c-9.4-9.4-24.6-9.4-33.9 0l-22.6 22.6c-9.4 9.4-9.4 24.6 0 33.9l96.4 96.4-96.4 96.4c-9.4 9.4-9.4 24.6 0 33.9l22.6 22.6c9.4 9.4 24.6 9.4 33.9 0l136-136c9.4-9.2 9.4-24.4 0-33.8z"></path></svg><!-- <i class="fas fa-angle-double-right"></i> Font Awesome fontawesome.com --></a>
        </div>
    </div>
    <script id="jsTemplateQuyChe" type="text/x-kendo-template">
             <div class="display-flex-righteous">
            <div class="pt-2 display-flex" style="width:80%">
                <span class=" mr-3 ">#=STT#</span>
                <a href="/Pages/regulation.aspx" class="color-a-basic">
                    <p>#=Title#</p>
                </a>
            </div>
            <div class=" pr-4">
                <img src="/Content/themeV1/icon/fc980afaacfc58a201ed.jpg" alt="" class="pr-2">#=new Date(QCNgayDang).format('dd/MM/yyyy')#
            </div>
        </div>
            </script>
    <div class="main_regulation pt-3 pl-4" id="lstQuyTrinh">
        <div class="display-flex-righteous">
            <div class="pt-2 display-flex">
                <span class=" mr-3 ">1</span>
                <a href="" class="color-a-basic">
                    <p>Quy chế về vấn đề đi làm muộn</p>
                </a>
            </div>
            <div class=" pr-4">
                <img src="/Content/themeV1/icon/fc980afaacfc58a201ed.jpg" alt="" class="pr-2">31/10/2021
            </div>
        </div>
        <div class="display-flex-righteous">
            <div class="pt-2 display-flex">
                <span class=" mr-3 ">1</span>
                <a href="" class="color-a-basic">
                    <p>Quy chế về vấn đề đi làm muộn</p>
                </a>
            </div>
            <div class=" pr-4">
                <img src="/Content/themeV1/icon/fc980afaacfc58a201ed.jpg" alt="" class="pr-2">31/10/2021
            </div>
        </div>
        <div class="display-flex-righteous">
            <div class="pt-2 display-flex">
                <span class=" mr-3 ">1</span>
                <a href="" class="color-a-basic">
                    <p>Quy chế về vấn đề đi làm muộn</p>
                </a>
            </div>
            <div class=" pr-4">
                <img src="/Content/themeV1/icon/fc980afaacfc58a201ed.jpg" alt="" class="pr-2">31/10/2021
            </div>
        </div>
        <div class="display-flex-righteous">
            <div class="pt-2 display-flex">
                <span class=" mr-3 ">1</span>
                <a href="" class="color-a-basic">
                    <p>Quy chế về vấn đề đi làm muộn</p>
                </a>
            </div>
            <div class=" pr-4">
                <img src="/Content/themeV1/icon/fc980afaacfc58a201ed.jpg" alt="" class="pr-2">31/10/2021
            </div>
        </div>
        <div class="display-flex-righteous">
            <div class="pt-2 display-flex">
                <span class=" mr-3 ">1</span>
                <a href="" class="color-a-basic">
                    <p>Quy chế về vấn đề đi làm muộn</p>
                </a>
            </div>
            <div class=" pr-4">
                <img src="/Content/themeV1/icon/fc980afaacfc58a201ed.jpg" alt="" class="pr-2">31/10/2021
            </div>
        </div>
    </div>
</div>
<%} %>
