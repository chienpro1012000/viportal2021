using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIPORTAL.oWeb;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public partial class pFormForumChuDe : pFormBase
    {
        public ForumChuDeItem oForumChuDe {get;set;}
        protected void Page_Load(object sender, EventArgs e)
        {
			oForumChuDe = new ForumChuDeItem();
            if (ItemID > 0)
            {
                ForumChuDeDA oForumChuDeDA = new ForumChuDeDA();
                oForumChuDe = oForumChuDeDA.GetByIdToObject<ForumChuDeItem>(ItemID);
            }
            else
            {
                if(!string.IsNullOrEmpty(Request["idparent"]))
                {
                    oForumChuDe.ParentChuDe = new Microsoft.SharePoint.SPFieldLookupValue(Request["idparent"] + ";#");
                }    
            }
        }
    }
}