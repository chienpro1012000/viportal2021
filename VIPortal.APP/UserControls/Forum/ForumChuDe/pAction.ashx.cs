using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPORTAL;
using ViPortal_Utils.Base;

namespace VIPortalAPP.ForumChuDe
{
    /// <summary>
    /// Summary description for pAcion
    /// </summary>
    public class pAction : hActionBase, IHttpHandler
    {

        public GridRequest oGridRequest { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequestBase(context);

            oGridRequest = new GridRequest(context);
            ResultAction oResult = new ResultAction();
            ForumChuDeDA oForumChuDeDA = new ForumChuDeDA();
            ForumChuDeItem oForumChuDeItem = new ForumChuDeItem();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.IsQuery = true;

                    var oData = oForumChuDeDA.GetListJsonSolr(new ForumChuDeQuery(context.Request));
                    DataGridRender oGrid = new DataGridRender(oData, oGridRequest.Draw, 
                        oForumChuDeDA.TongSoBanGhiSauKhiQuery);
                    oResult.OData = oGrid;
                    break;
                case "MENUTREE":
                    List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
                    oResult.IsQuery = true;
                    var oDataDanhMuc = oForumChuDeDA.GetListJson(new ForumChuDeQuery(context.Request));

                    treeViewItems = BuildTree(oDataDanhMuc, 0);

                    oResult.OData = treeViewItems;
                    break;
                case "ALLJSON":
                    oResult.IsQuery = true;
                    oGridRequest = new GridRequest(context);
                    var oDataJson = oForumChuDeDA.GetListJson(new ForumChuDeQuery(context.Request));
                    oResult.OData = oDataJson;
                    break;
                case "UPDATE":
                    if (oGridRequest.ItemID > 0)
                    {
                        oForumChuDeItem = oForumChuDeDA.GetByIdToObject<ForumChuDeItem>(oGridRequest.ItemID);
                        oForumChuDeItem.UpdateObject(context.Request);
                        if (oForumChuDeDA.CheckExit(oGridRequest.ItemID, oForumChuDeItem.Title) == 0)
                        {
                            oForumChuDeDA.UpdateObject<ForumChuDeItem>(oForumChuDeItem);
                            oResult.Message = "Lưu thành công";
                        }
                        else
                        {
                            oResult = new ResultAction()
                            {
                                State = ActionState.Error,
                                Message = $"Tiêu đề chủ đề \"{oForumChuDeItem.Title}\" đã tồn tại trong hệ thống"
                            };
                        }
                    }
                    else
                    {
                        oForumChuDeItem.UpdateObject(context.Request);

                        if (oForumChuDeDA.CheckExit(oGridRequest.ItemID, oForumChuDeItem.Title) == 0)
                        {
                            oForumChuDeDA.UpdateObject<ForumChuDeItem>(oForumChuDeItem);
                            oResult.Message = "Lưu thành công";
                        }
                        else
                        {
                            oResult = new ResultAction()
                            {
                                State = ActionState.Error,
                                Message = $"Tiêu đề chủ đề \"{oForumChuDeItem.Title}\" đã tồn tại trong hệ thống"
                            };
                        }
                    }
                    
                    break;
                case "DELETE":
                    string outb = oForumChuDeDA.DeleteObject(oGridRequest.ItemID);
                    if (string.IsNullOrEmpty(outb))
                        oResult.Message = "Xóa thành công";
                    else oResult.Message = outb;
                    break;
                case "DELETE-MULTI":
                    break;
                case "APPROVED-MULTI":
                    break;
                case "APPROVED":
                    oForumChuDeDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "PENDDING":
                     oForumChuDeDA.UpdateSPModerationStatus(oGridRequest.ItemID, Microsoft.SharePoint.SPModerationStatusType.Pending);
                    oResult.Message = "Duyệt thành công";
                    break;
                case "CHECKAPPROVED":
                    if (oGridRequest.ItemID > 0)
                    {
                        oForumChuDeItem = oForumChuDeDA.GetByIdToObjectSelectFields<ForumChuDeItem>(oGridRequest.ItemID, "_ModerationStatus");
                        oResult.Source = (oForumChuDeItem._ModerationStatus == Microsoft.SharePoint.SPModerationStatusType.Approved) ? "1" : "0";
                    }
                    break;
            }
            oResult.ResponseData();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private List<TreeViewItem> BuildTree(List<ForumChuDeJson> lstLForumChuDeJson, int ChuDeParent = 0)
        {
            List<TreeViewItem> lsTreeViewItem = new List<TreeViewItem>();
            foreach (ForumChuDeJson item in lstLForumChuDeJson.Where(x => x.ParentChuDe.ID == ChuDeParent))
            {
                lsTreeViewItem.Add(new TreeViewItem(item.DMSTT + "." + item.Title, Convert.ToString(item.ID),
                    BuildTree(lstLForumChuDeJson, item.ID), false, true));
            }
            return lsTreeViewItem;
        }
    }
}