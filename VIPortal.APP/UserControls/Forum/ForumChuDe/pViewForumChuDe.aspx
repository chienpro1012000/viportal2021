<%@ page language="C#" autoeventwireup="true" codebehind="pViewForumChuDe.aspx.cs" inherits="VIPortalAPP.pViewForumChuDe" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form class="form-horizontal form-viewdetail">
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tiêu đề</label>
            <div class="col-sm-10">
                <%=oForumChuDe.Title%>
            </div>
        </div>
        <div class="form-group row">
            <label for="MoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <%=oForumChuDe.MoTa%>
            </div>
        </div>
        <%--<div class="form-group">
            <label for="ParentChuDe" class="col-sm-2 control-label">ParentChuDe</label>
            <div class="col-sm-10">
                <%=oForumChuDe.ParentChuDe%>
            </div>
        </div>
        <div class="form-group">
            <label for="CountBaiViet" class="col-sm-2 control-label">CountBaiViet</label>
            <div class="col-sm-10">
                <%=oForumChuDe.CountBaiViet%>
            </div>
        </div>
        <div class="form-group">
            <label for="CountComment" class="col-sm-2 control-label">CountComment</label>
            <div class="col-sm-10">
                <%=oForumChuDe.CountComment%>
            </div>
        </div>--%>

        <div class="form-group row">
            <label class="col-sm-2 control-label">Ngày tạo</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}", oForumChuDe.Created)%>
            </div>
            <label class="col-sm-2 control-label">Ngày sửa cuối</label>
            <div class="col-sm-4">
                <%=string.Format("{0:dd/MM/yyyy hh:mm tt}",oForumChuDe.Modified)%>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 control-label">Người tạo</label>
            <div class="col-sm-4">
                <%=oForumChuDe.Author.LookupValue%>
            </div>
            <label class="col-sm-2 control-label">Người sửa</label>
            <div class="col-sm-4">
                <%=oForumChuDe.Editor.LookupValue%>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</body>
</html>
