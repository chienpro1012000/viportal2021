<%@ page language="C#" autoeventwireup="true" codebehind="pFormForumChuDe.aspx.cs" inherits="VIPortalAPP.pFormForumChuDe" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <form id="frm-ForumChuDe" class="form-horizontal">
        <input type="hidden" name="ItemID" id="ItemID" value="<%=oForumChuDe.ID %>" />
        <input type="hidden" name="do" id="do" value="<%=doAction %>" />
        <div class="form-group row">
            <label for="Title" class="col-sm-2 control-label">Tên chủ đề</label>
            <div class="col-sm-10">
                <input type="text" name="Title" id="Title" placeholder="Nhập tiêu đề chủ đề" class="form-control" value="<%=oForumChuDe.Title%>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="MoTa" class="col-sm-2 control-label">Mô tả</label>
            <div class="col-sm-10">
                <textarea name="MoTa" id="MoTa" placeholder="Nhập mô tả chủ đề" class="form-control"><%:oForumChuDe.MoTa%></textarea>
            </div>
        </div>
         <div class="form-group row" style="display:none;">
            <label for="ParentChuDe" class="col-sm-2 control-label">Chủ đề cha</label>
            <div class="col-sm-10">
                <select data-selected="<%:oForumChuDe.ParentChuDe.LookupId%>" data-url="/UserControls/Forum/ForumChuDe/pAction.asp?do=QUERYDATA" data-place="Chọn ParentChuDe" name="ParentChuDe" id="ParentChuDe" class="form-control"></select>
            </div>
        </div>
        <div class="form-group row" style="display:none;">
            <label for="CountBaiViet" class="col-sm-2 control-label">Số bài viết</label>
            <div class="col-sm-10">
                <input type="text" name="CountBaiViet" id="CountBaiViet" placeholder="Nhập countbaiviet" value="<%:oForumChuDe.CountBaiViet%>" class="form-control" />
            </div>
        </div>
       <div class="form-group row" style="display:none;">
            <label for="CountComment" class="col-sm-2 control-label">Số comment</label>
            <div class="col-sm-10">
                <input type="text" name="CountComment" id="CountComment" placeholder="Nhập countcomment" value="<%:oForumChuDe.CountComment%>" class="form-control" />
            </div>
        </div>

        <div class="form-group row" style="display:none;">
            <label class="no-padding-md-x col-xs-12 col-sm-2 col-md-2 control-label">Tệp đính kèm</label>
            <div class="col-xs-12 col-sm-10 col-md-2">
                <input type="file" id="FileAttach" name="FileAttach" multiple="multiple" />
            </div>
        </div>
        <div class="clearfix"></div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#ParentChuDe").smSelect2018V2({
                dropdownParent: "#frm-ForumChuDe"
            });
            $("#FileAttach").regFileUpload({
                files: '<%=ViPortal_Utils.Base.clsFucUtils.SerializerJSON(oForumChuDe.ListFileAttach)%>'
            });
            $("#Title").focus();



            $("#frm-ForumChuDe").validate({
                rules: {
                    Title: "required",
                    DMSTT: {
                        numberchar: true, required: true
                    }
                },
                messages: {
                    Title: "Vui lòng nhập tên chủ đề"
                },
                submitHandler: function (form) {
                    $.post("/UserControls/Forum/ForumChuDe/pAction.asp", $(form).viSerialize(), function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$("#btn-find-ForumChuDe").trigger("click");
                            $(form).closeModal();
                            $('#tblForumChuDe').DataTable().ajax.reload();
                        }
                    }).always(function () { });
                }
            });
            $("#frm-ForumChuDe").viForm();
        });
    </script>
</body>
</html>
