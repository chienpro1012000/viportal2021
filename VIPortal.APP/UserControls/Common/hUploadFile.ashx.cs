﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using ViPortal_Utils.Base;

namespace VIPortalAPP.Common
{
    /// <summary>
    /// Summary description for hUploadFile
    /// </summary>
    public class hUploadFile : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            List<UploadFilesResult> resultUpLoadItems = new List<UploadFilesResult>();
            System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
            string pathFile = string.Empty;
            foreach (string file in context.Request.Files)
            {
                HttpPostedFile hpf = context.Request.Files[file] as HttpPostedFile;
                if (hpf.ContentLength == 0)
                    throw new Exception("File không chứa nội dung");
                var resultUpLoad = new UploadFilesResult();
                try
                {
                    string fileServer = Guid.NewGuid() + Path.GetExtension(hpf.FileName);
                    string pathServer = string.Format(@"{0}\{1}", @"Uploads\ajaxUpload\", fileServer);
                    pathFile = string.Format(@"{0}\{1}", HttpContext.Current.Server.MapPath(@"\"), pathServer);
                    hpf.SaveAs(pathFile);

                    resultUpLoad.pathFile = fileServer;
                    resultUpLoad.url = string.Format(@"{0}/{1}", @"/Uploads/ajaxUpload/", fileServer);
                    resultUpLoad.size = hpf.ContentLength;
                }
                catch (Exception ex)
                {
                    resultUpLoad.error = true;
                    resultUpLoad.errorMessage = ex.Message;
                }
                finally
                {
                    resultUpLoad.name = hpf.FileName;
                    resultUpLoadItems.Add(resultUpLoad);
                }

            }

            var uploadedFiles = new
            {
                files = resultUpLoadItems.ToArray()
            };
            string jsonObj = js.Serialize(uploadedFiles);
            context.Response.Write(jsonObj);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}