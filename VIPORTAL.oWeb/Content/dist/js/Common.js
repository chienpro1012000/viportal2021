﻿(function ($) {

    $.fn.siSerializeDivFrm = function () {
        var values = {};
        var $input = $('input, select, textarea');
        var $inputs = $(this).find($input);
        $inputs.each(function () {
            if ($(this).val() != "" && $(this).val() != undefined) {
                var attr = $(this).attr('multiple');
                if (typeof attr !== typeof undefined && attr !== false) {
                    var $mangvalue = $(this).val();
                    values[this.name] = $mangvalue.join(",");
                }
                else if ($(this).hasClass('input-datetime')) {
                    if (CheckDate($(this).val())) {
                        values[this.name] = $(this).val();
                    } else {
                    }
                }
                else if (this.type == "radio") {
                    var vlin = $("input[name='" + this.name + "']:checked").val();;
                    values[this.name] = vlin;
                } else
                    if (this.type == "checkbox") {
                        values[this.name] = this.checked;
                    } 
                    else {
                        values[this.name] = this.value;
                    }
            }
        });
        //console.log(values);
        //returning += $(this).serialize();
        return values;
    };
    ///Nâng tầm plugin. :D
    var smSelect2018V2 = function () {
        //debugger;
        return {
            options: {
                Url: "",
                ItemID: 0,
                SearchInSimple: "Title",
                TemplateText: "{Title}",
                TemplateValue: "{ID}", //"#=ID#;\\##=Title#"
                Ajax: false,
                placeholder: "Chọn",
                DropdownParent: ".k-window",
                IDSelected: "",
                Length: 20,
                dropdownCssClass: "",
                closeOnSelect: true,
                Remove: true,
                Width: 0,
                tags: false,
                theme: 'default',
                minimumInputLength: 0,
                data: {},
                dataBound: function (data) {
                    //Hàm sau khi load xong
                },
                parameterPlus: function (para) {
                    // Hàm post dữ liệu.
                },
                attribute: [], // ["ID","Title"],
            },
            // --------------------------------------------------------------------
            init: function (options, element) {
                //check has selct2
                if ($(element).data('select2')) {
                    $(element).select2('destroy');
                }
                var $option = this.getconfig(options, element);
                console.log(element);
                var elementid = $(element).attr('id');
                if ($option.Ajax) { //neu ajax remote data
                    console.log($option.IDSelected);
                    

                    $("#" + elementid).select2({
                        allowClear: true,
                        dropdownCssClass: $option.dropdownCssClass,
                        closeOnSelect: $option.closeOnSelect,
                        ajax: {
                            url: $option.Url,
                            dataType: 'json', 
                            data: function (params) {
                                var datapost = {
                                    Keyword: params.term, // search term,
                                    SearchInSimple: $option.SearchInSimple, // search term
                                    Page: params.page,
                                    length: $option.Length
                                };
                                if (jQuery.isFunction($option.parameterPlus)) {
                                    $option.parameterPlus(datapost); //
                                }
                                return datapost;
                            },
                            processResults: function (data, params) {
                                // parse the results into the format expected by Select2
                                // since we are using custom formatting functions we do not need to
                                // alter the remote JSON data, except to indicate that infinite
                                // scrolling can be used
                                params.page = params.page || 1;

                                return {
                                    //results: data.items,
                                    results: jQuery.map(data.data, function (a) {
                                        return {
                                            "id": nano($option.TemplateValue, a),
                                            "text": nano($option.TemplateText, a)
                                        };
                                    }),
                                    pagination: {
                                        more: (params.page * $option.Length) < data.recordsTotal
                                    }
                                };
                            },
                            cache: false
                        },
                        dropdownParent: $(element).closest($option.dropdownParent),
                        placeholder: $option.placeholder,
                        dropdownCssClass: `ZoneDrop${$(element).attr('id')}`,
                        minimumInputLength: $option.minimumInputLength,
                        theme: $option.theme
                    });
                    if ($option.IDSelected != undefined && ($option.IDSelected != "" || $option.IDSelected != 0)) {
                        var ElmSelect = $("#" + element.id);
                        $.ajax({
                            type: 'POST',
                            url: $option.Url,
                            data: { isGetBylistID: true, lstIDget: $option.IDSelected }
                        }).then(function (data) {
                            // create the option and append to Select2
                            $.each(data.data, function (index, value) {
                                //alert(index + ": " + value);
                                var option = new Option(value.Title, value.ID, true, true);
                                ElmSelect.append(option)
                            });

                            ElmSelect.trigger('change');

                            // manually trigger the `select2:select` event
                            ElmSelect.trigger({
                                type: 'select2:select',
                                params: {
                                    data: data
                                }
                            });
                        });
                    }
                } else {

                    //sau khi success thì đăng ký select2.
                    if (!$(element).prop('multiple')) { //Không phải multi thì mới add
                        $(element).append($('<option>', { value: '', text: $option.placeholder }));
                    }
                    //lấy dữ liệu ra khi đăng ký.
                    this.getData();
                }
                //đăng ký sự kiện ở đây.
                this.attachEvents();
            },
            getconfig: function (options, element) {
                //console.log("init");
                this.options = $.extend(this.options, options);
                // Khởi tạo thông tin để lấy dữ liệu.
                // Post de lay du lieu ve hien thi.
                var max_width = "100%"; //$this.width() + 12;
                this.elem = element;
                var thisID = $(this.elem).attr("id");
                //console.log(thisID);
                //set value cho option nếu có khi set trên att
                var $select = $("#" + thisID);
                var $place = $(this.elem).attr("data-place");
                var $disable = $(this.elem).attr("data-disable");
                var $selected = $(this.elem).attr("data-selected");
                var $url = $(this.elem).attr("data-url");
                if ($url !== undefined && $url !== "") {
                    this.options.Url = $url;
                }
                if (this.options.width !== undefined && this.options.width !== "" && this.options.width != 0) {
                    max_width = this.options.width + "px";
                }
                if ($place != undefined && $place != "") {
                    this.options.placeholder = $place;
                }
                if ($selected != undefined && $selected != "") {
                    this.options.IDSelected = $selected;
                }
                var $option = this.options;
                return $option;
            },
            getData: function () {
                var $select = $(this.elem);
                var thisID = $(this.elem).attr("id");
                var config = this.options;
                // debugger;
                // Hàm set add html cho thẻ select.

                if (jQuery.isFunction(config.parameterPlus)) {
                    config.parameterPlus(config.data); //
                }
                if (config.data["length"] == undefined) config.data["length"] = 0;

                $.ajax({
                    url: config.Url,
                    async: config.async,
                    contentType: 'application/json',
                    data: config.data,
                    success: function (odata) {
                        var dataItem = odata.data;

                        var $option = null;
                        $.each(dataItem, function (key, val) {
                            var $optionvalue = {
                                value: nano(config.TemplateValue, val),
                                text: nano(config.TemplateText, val)
                            };
                            $option = $('<option>', $optionvalue);
                            $select.append($option);
                        });
                        if (jQuery.isFunction(config.dataBound)) {
                            config.dataBound(dataItem); //
                        }
                        console.log(config.IDSelected);
                        // set sleected ở đây.
                        if (config.IDSelected != undefined && (config.IDSelected != "" || config.IDSelected != 0)) {
                            var arraySelect = config.IDSelected.split(",");
                            $.each(arraySelect, function (index, value) {
                                //alert(index + ": " + value);
                                $("#" + thisID + " option[value='" + value + "']").prop("selected", true);
                            });
                            //$select.val(config.IDSelected).change();
                            //$('.id_100 option[value=val2]').attr('selected', 'selected');
                            //console.log($("#" + thisID + " option[value='" + config.IDSelected + "']"));
                            
                        }

                        $select.select2({
                            dropdownParent: $select.closest(config.dropdownParent),
                            placeholder: config.placeholder,
                            dropdownCssClass: `ZoneDrop${thisID}`,
                            allowClear: true,
                            tags: config.tags,
                            dropdownCssClass: config.dropdownCssClass,
                            theme: config.theme,
                            closeOnSelect: config.closeOnSelect,
                            //closeOnSelect: true,
                        });
                    }
                });

                return $(this.elem); // for chaining
            },
            refreshData: function () {

            },
            // --------------------------------------------------------------------
            someMethod: function () {
                alert('someMethod called');
                return $(this.elem); // for chaining
            },
            Searching: function () {

                //$select.trigger('change'); 
            },
            // --------------------------------------------------------------------
            attachEvents: function () {
                var self = this;
                var thisID = $(this.elem).attr("id");
                $(self.elem).on("hover", function (e) {
                    //console.log("hover");
                });
                $(self.elem).on("click", function (e) {
                    //console.log("click");
                });
                //đăng ký sự kiện tab cho input.
            }
        };
    };
    $.fn.smSelect2018V2 = function (options) {
        // create an instance for each element in the set
        return this.each(function () {
            var mysmSelect2018V2 = new smSelect2018V2();
            mysmSelect2018V2.init(options, this);
            // this lets us call methods on the selector
            $(this).data("smSelect2018V2", mysmSelect2018V2);
        });
    };


    $.fn.clear_form_elements = function () {
        var thisID = $(this).attr("id");
        $("#" + thisID).find(':input').each(function () {
            switch (this.type) {
                case 'password':
                case 'text':
                case 'textarea':
                case 'file':
                case 'select-one':
                case 'select-multiple':
                case 'date':
                case 'number':
                case 'tel':
                case 'email':
                    jQuery(this).val('');
                    break;
                case 'checkbox':
                case 'radio':
                    this.checked = false;
                    break;
            }
            if (this.type == 'select-one' || this.type == 'select-multiple') {
                $(this).val(null).trigger('change');
            }
        });
    };
    $.fn.viForm = function () {
        var thisID = $(this).attr("id");
        $("#" + thisID).find('input[type=date]').on("change", function () {
            this.setAttribute(
                "data-date",
                moment(this.value, "YYYY-MM-DD")
                    .format("DD/MM/YYYY")
            )
        }).trigger("change");
        $("#" + thisID).find('input[type=datetime]').on("change", function () {
            this.setAttribute(
                "data-date",
                moment(this.value, "YYYY-MM-DDThh:mm")
                    .format("DD/MM/YYYY HH:mm")
            )
        }).trigger("change");

        $("#" + thisID + " .input-datetime").daterangepicker({
            autoUpdateInput: false,
            singleDatePicker: true,
            locale: vidatalocale,
            useCurrent: false
        });
        $('#' + thisID + ' .input-datetime').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY')).change();
            var attr = $(this).attr('data-min');
            // For some browsers, `attr` is undefined; for others, `attr` is false. Check for both.
            if (typeof attr !== typeof undefined && attr !== false) {
                //$('#' + attr).data("daterangepicker").setOptions({ minDate: $(this).val() });
                $('#' + attr).data("daterangepicker").minDate = picker.startDate;
                //$('#datetimepicker').data("DateTimePicker")
            }
            attr = $(this).attr('data-max');
            // For some browsers, `attr` is undefined; for others, `attr` is false. Check for both.
            if (typeof attr !== typeof undefined && attr !== false) {
                //$('#' + attr).data("daterangepicker").setOptions({ maxDate: $(this).val() });
                $('#' + attr).data("daterangepicker").maxDate = picker.startDate;
            }
        });

        // Requiments
        var validate = $("#" + thisID).validate();
        if (validate != undefined) {
            var allreles = validate.settings.rules;
            for (var key in allreles) {
                //console.log('key: ' + key + '\n' + 'value: ' + allreles[key]);
                if (allreles[key]["required"] != undefined && allreles[key]["required"] == true) {
                    $("#" + thisID + " label[for='" + key + "']").addClass("required");
                }
                allreles[key]["normalizer"] = function (value) {
                    return $.trim(value);
                }
            }
        }
        $('#' + thisID + ' label.required').each(function () {
            let str = $(this).text();
            if (str.indexOf("*") == -1) {
                $(this).append("<span class='clslable'>(<span class='clsred'>*</span>)</span>");
            }

        });
    };

    $.fn.DangKyFrmSearch2018 = function () {
        var values = {};
        var $input = $('input, select');
        var $inputs = $(this).find($input);
        var thisID = $(this).attr("id");
        //$inputs.change(function () {
        //    //alert("Handler for .change() called.");
        //    var $tagData = $(this).closest("div[data-role='fullGrid']");
        //    //var $idtable = $tagData.find("table.smdataTable").first().dataTable().fnDraw(false);
        //    $tagData.data("smGrid2018").RefreshGrid();
        //});
        $(this).find(".btnsearch").click(function () {
            var $tagData = $(this).closest("div[data-role='fullGrid']");
            //var $idtable = $tagData.find("table.smdataTable").first().dataTable().fnDraw(false);
            $tagData.data("smGrid2018").RefreshGrid();
        });
        //$('.input-datetime').daterangepicker({
        //    autoUpdateInput: false,
        //    singleDatePicker: true,
        //    locale: {
        //        cancelLabel: 'Clear'
        //    }
        //});
        //$('.input-datetime').on('apply.daterangepicker', function (ev, picker) {
        //    $(this).val(picker.startDate.format('DD/MM/YYYY'));
        //});
        //
        //$('.input-datetime').on('cancel.daterangepicker', function (ev, picker) {
        //    $(this).val('');
        //});
    };
    var smGrid2018 = function () {
        return {
            options: {
                TypeTemp: "nano",
                UrlPost: "",
                infoEmpty: "",
                ConfigLanguages: '{"txtHienThi": "Hiển thị","txtDen": "đến", "txtTongSo":"trên số", "txtBanGhi":"bản ghi", "txtInfoEmpty": ""}',
                hasSTT: false,
                pageSize: 20,
                height: 0,
                async: true,
                "template": "#SiTemplate",
                modelID: "ID",
                odata: { "action": "QUERYDATA" },
                group: null,
                Sort: [{ field: "ID", dir: "desc" }],
                renderItem: null,
                // Ham gan tham so post
                parameterPlus: function (para) {

                },
                // su kien change select
                change: function () {
                },
                // Su kien load xong du lieeuj
                dataBound: function () {

                },
                defaultOptsPagging: {
                    totalPages: 20
                }
            },
            // --------------------------------------------------------------------
            init: function (options, element) {
                //console.log("init");
                this.options = $.extend(this.options, options);

                // Khởi tạo thông tin để lấy dữ liệu.
                // Post de lay du lieu ve hien thi.

                this.elem = element;
                this.attachEvents();
                this.initializerGrid();
            },
            GetDataPost: function (oDataRefresh) {
                var oDataPost = {
                    draw: 1, start: 0, length: this.options.pageSize,
                    "fieldOrder": this.options.Sort[0].field, "ascending": this.options.Sort[0].dir == "desc" ? "false" : "true"
                };
                if (oDataRefresh != undefined && oDataRefresh != null) {
                    oDataPost = $.extend(oDataPost, oDataRefresh);
                }
                oDataPost = $.extend(oDataPost, this.options.odata);
                //console.log($(this.elem));
                // Get thông tin trên role=search
                var _element = $(this.elem);
                var $zonesearch = _element.find("[role=\"search\"]").first();
                if ($zonesearch != undefined) {
                    //console.log($zonesearch.siSerializeDivFrm());
                    oDataPost = $.extend(oDataPost, $zonesearch.viSerializeKhaiThac());
                } else {
                    console.log("Không thấy form search");
                }
                if (jQuery.isFunction(this.options.parameterPlus)) {
                    this.options.parameterPlus(oDataPost); //
                }
                return oDataPost;
            },
            RenderPaging: function (GridData, currentEntries) {
                var _SMGrid = this;
                //console.log(_SMGrid);
                var _recordsTotal = GridData.recordsTotal;
                var _element = $(this.elem);
                var $Config = this.options;
                var currentPage = 1;
                var totalPages = parseInt(_recordsTotal / $Config.pageSize);
                if (totalPages * $Config.pageSize < _recordsTotal)
                    totalPages = totalPages + 1;
                else if (totalPages * $Config.pageSize > _recordsTotal)
                    totalPages = totalPages - 1;

                // Vẽ lại phân trang.
                var $pagination = _element.find(".clspaging").first();
                var current = 1;
                if ($pagination != undefined) {
                    //debugger
                    var $langPaging = $Config.ConfigLanguages;
                    if ($langPaging == null || $langPaging == '' || $langPaging == undefined) {
                        $langPaging = '{"txtHienThi": "Hiển thị","txtDen": "đến", "txtTongSo":"trên số", "txtBanGhi":"bản ghi", "txtInfoEmpty": ""}';
                    }
                    $langPaging = jQuery.parseJSON($langPaging);
                    $pagination.twbsPagination('destroy');
                    if (_recordsTotal > 0) {
                        
                        $pagination.twbsPagination($.extend({}, $Config.defaultOptsPagging, {

                            startPage: currentPage,
                            totalPages: totalPages,
                            initiateStartPageClick: false,
                            onPageClick: function (event, page) {
                                current = page;
                                //console.log("onPageClick");
                                _SMGrid.ChangePage({ start: (page - 1) * $Config.pageSize, totalItem: GridData.recordsTotal });
                                // $('#content').text('Tong so ban ghi ' + totalPages);
                            }
                        }));
                        //$pagination
                        var curentEntrie = 1
                        var toEntries = current * $Config.pageSize;
                        if (_recordsTotal < current * $Config.pageSize)
                            toEntries = _recordsTotal;
                        console.log(_recordsTotal);
                        $pagination.prepend(`<div class='dataTables_info' role='status' aria-live='polite'><span data-lang='txtHienThi'>${$langPaging.txtHienThi}</span>${curentEntrie}<span data-lang='txtDen'> ${$langPaging.txtDen} </span>${toEntries}<span data-lang='txtTongSo'> ${$langPaging.txtTongSo} </span>${_recordsTotal}<span data-lang='txtBanGhi'> ${$langPaging.txtBanGhi} </span></div>`);
                    } else {
                        $pagination.prepend("<div class='dataTables_info col-xs-12' style='text-align:center;padding-bottom:20px;' role='status' aria-live='polite' data-lang='txtInfoEmpty'>" + $langPaging.txtInfoEmpty + "</div>");
                    }
                    //set thông tin.


                }

            },
            RenderGrid: function (oDataPost, isRenderPaging) {
                var Entries = oDataPost.start + 1;
                var toEntries = oDataPost.start + oDataPost.length;
                //$(".dataTables_info").html("Hiển thị " + Entries + " đến " + toEntries + " trên số " + oDataPost.totalItem + " bản ghi");
                isRenderPaging = (typeof isRenderPaging === "undefined") ? true : isRenderPaging
                // Tham số sẽ post đi.
                var _SMGrid = this;
                var _element = $(this.elem);
                var $elemGrid = _element.find("[role=\"grid\"]").first();
                //$elemGrid.html('<img src="/Content/images/fb-loading.gif" />');
                var $Config = this.options;
                // Post dữ liệu
                $.ajax({
                    type: "POST",
                    url: $Config.UrlPost,
                    data: oDataPost,
                    async: $Config.async,
                    success: function (GridData) {
                        // get ra template code.

                        var $template = _element.find("[type=\"text/template\"]").first();
                        if ($Config.TypeTemp == 'kendo') {
                            $template = _element.find("[type=\"text/x-kendo-template\"]").first();

                        }
                        var htmlTEmp = '';
                        var templatehtmltr = $("#" + $Config.template).html();
                        if ($Config.TypeTemp == 'kendo')
                            templatehtmltr = kendo.template(templatehtmltr);
                        for (i = 0; i < GridData.data.length; i++) {
                            //xử lý đoạn này để có stt;
                            if ($Config.hasSTT) {
                                GridData.data[i]["STT"] = (oDataPost.start + i + 1);
                            }
                            if (!jQuery.isFunction($Config.renderItem) && $template != undefined) {
                                if ($Config.TypeTemp == 'kendo') {
                                    htmlTEmp += templatehtmltr(GridData.data[i]);
                                } else {
                                    htmlTEmp += nano(templatehtmltr, GridData.data[i]);//templatehtmltr(GridData.Data[i]);
                                }
                            }
                            else {
                                htmlTEmp += $Config.renderItem(GridData.data[i]);
                            }
                        }
                        //$elemGrid.removeClass("smgridloading");
                        $elemGrid.html(htmlTEmp);
                        if (isRenderPaging)
                            _SMGrid.RenderPaging(GridData);
                        if (jQuery.isFunction($Config.dataBound)) {
                            $Config.dataBound(GridData); //
                        }
                    }
                });
            },
            ChangePage: function (oDataRefresh) {
                //console.log("RefreshGrid");
                var oDataPost = this.GetDataPost(oDataRefresh);
                this.RenderGrid(oDataPost, false);
            },
            // refresh grid.
            RefreshGrid: function (oDataRefresh) {
                //console.log("RefreshGrid");
                if (oDataRefresh == undefined)
                    oDataRefresh = {};
                var oDataPost = this.GetDataPost(oDataRefresh);
                this.RenderGrid(oDataPost);
            },
            // --------------------------------------------------------------------
            initializerGrid: function () {
                // Tham số sẽ post đi.
                var oDataPost = this.GetDataPost();
                this.RenderGrid(oDataPost);
            },
            initializerFormSearch: function () {
                console.log('initializerFormSearch');
                //ddk form search.
                var _element = $(this.elem);
                var self = this;
                var $zonesearch = _element.find("[role=\"search\"]").first();
                if ($zonesearch != undefined) {
                    var $btnsearch = $zonesearch.find(".btnsearch");
                    $($btnsearch).on("click", function (e) {
                        console.log("click");
                        if ($zonesearch.data("SMValidate") != null && !$zonesearch.data("SMValidate").CheckVali()) {
                            return;
                        } else
                            self.RefreshGrid();
                    });

                    var _inputdate = $('.input-datetime');
                    var $inputdates = $zonesearch.find(_inputdate);
                    $inputdates.daterangepicker({
                        autoUpdateInput: false,
                        singleDatePicker: true,
                        locale: vidatalocale,
                        useCurrent: false
                    });
                    $inputdates.on('apply.daterangepicker', function (ev, picker) {
                        $(this).val(picker.startDate.format('DD/MM/YYYY'));
                        $(this).trigger("change");
                        self.RefreshGrid();
                    });

                    $inputdates.on('cancel.daterangepicker', function (ev, picker) {
                        $(this).val('');
                        self.RefreshGrid();
                    });

                    //enter input
                    var _inputtext = $('input[type=text]:not(.input-datetime)');
                    var $inputtext = $zonesearch.find(_inputtext);
                    $inputtext.keypress(function (event) {
                        var keycode = (event.keyCode ? event.keyCode : event.which);
                        if (keycode == '13') {
                            event.preventDefault();
                            self.RefreshGrid();
                        }
                    });
                    var $inputselect = $('select');
                    var $inputsselect = $zonesearch.find($inputselect);
                    //var thisID = $(this).attr("id");
                    $inputsselect.change(function () {
                        self.RefreshGrid();
                    });
                    //reset_data
                    var $reset_data = $zonesearch.find(".reset_data");
                    if ($reset_data != undefined && $reset_data.length > 0) {
                        $reset_data.click(function (e) {
                            e.preventDefault();
                            $zonesearch.clear_form_elements();
                        });
                    }
                }

            },
            someMethod: function () {
                alert('someMethod called');
                return $(this.elem); // for chaining
            },
            // --------------------------------------------------------------------
            attachEvents: function () {

                var self = this;
                $(self.elem).data("smGrid2018", self);
                $(self.elem).on("hover", function (e) {
                    //console.log("hover");
                });
                $(self.elem).on("click", function (e) {
                    //console.log("click");
                });
                //btnsearch
                self.initializerFormSearch();

            }
        };
    };

    // --------------------------------------------------------------------

    $.fn.smGrid2018 = function (options) {
        // create an instance for each element in the set
        return this.each(function () {
            var mysmGrid2018 = new smGrid2018();
            mysmGrid2018.init(options, this);
            // this lets us call methods on the selector
            $(this).data("smGrid2018", mysmGrid2018);
        });
    };

    $.fn.SMValidate = function (options) {
        var $this = $(this);
        var self = this;
        var thisID = $this.attr("id");
        var config = {
            rules: {}
        }

        if (options) $.extend(config, options);
        var myPrivateMethod = function ($input, rule, valurule,) {
            // do something ...
            console.log({ rule: rule, valurule: valurule });
            var valueinput = $($input).val().trim();
            switch (rule) {
                case "required":
                    if (valueinput.trim() == "") {
                        $($input).removeClass("valid").addClass("invalid");
                        return false;
                    }
                    else {
                        $($input).removeClass("invalid").addClass("valid");
                        return true;
                    }
                    break;
                case "date":
                case "dateFormat":
                    if (valueinput != '') {
                        var regex = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
                        if (regex.test(valueinput)) {
                            $($input).removeClass("invalid").addClass("valid");
                            return true;
                        } else {
                            $($input).removeClass("valid").addClass("invalid");
                            return false;
                        }
                    } else return true;
                    break;
                case "greaterThan":
                    if (valueinput == '')
                        return true;
                    if ($(valurule).val() == '')
                        return true;
                    var dateGreater = new Date(valueinput.replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3"));
                    var dateLees = new Date($(valurule).val().replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3"));

                    console.log(dateGreater);
                    console.log(dateLees);
                    if (!/Invalid|NaN/.test(dateGreater) && !/Invalid|NaN/.test(dateLees)) {
                        if (dateGreater > dateLees) {
                            $($input).removeClass("invalid").addClass("valid");
                            return true;
                        } else {
                            $($input).removeClass("valid").addClass("invalid");
                            return false;
                        }
                    } else return true;
                    break;
            }

        }
        self.CheckVali = function () {
            var kq = true;
            console.log(config.rules);
            $.each(config.rules, function (key, value) {
                $("#" + thisID).find(':input[name="' + key + '"]').each(function () {
                    //alert("Handler for .change() called.");
                    if (typeof value == 'string') {
                        var resultcheck = myPrivateMethod(this, value, true);
                        if (!resultcheck) {
                            $(this).focus();
                            kq = false;
                        }
                    } else {
                        var keys = Object.keys(value);
                        for (var i = 0; i < keys.length; i++) {
                            if (kq) {
                                var val = value[keys[i]];
                                var resultcheck = myPrivateMethod(this, keys[i], val);
                                if (!resultcheck) {
                                    $(this).focus();
                                    kq = false;
                                }
                            }
                        }
                    }
                });
            });
            return kq;
        }
        $.each(config.rules, function (key, value) {
            $("#" + thisID).find(':input[name="' + key + '"]').each(function () {
                if (typeof value == 'string') {
                    $(this).on('change', function () {
                        myPrivateMethod(this, value, true);
                    });
                } else {
                    $(this).data('rule', value);
                    $(this).on('change', function () {
                        var $rule = $(this).data('rule');
                        console.log($rule);
                        var keys = Object.keys($rule);
                        var checkrule = true;
                        for (var i = 0; i < keys.length; i++) {
                            if (checkrule) {
                                var val = $rule[keys[i]];
                                checkrule = myPrivateMethod(this, keys[i], val);
                            }
                        }
                    });
                }
            });
        });
        $this.data("SMValidate", self);
    };
})(jQuery);
function formatDate(strValue) {
    if (strValue == null) return "";
    var date = new Date(strValue);
    return date.format("dd/MM/yyyy");
}
function formatDateDDMM(strValue) {
    if (strValue == null) return "";
    var date = new Date(strValue);
    return date.format("dd/MM");
}
function CheckDate(txtdate) {
    if (txtdate == "" || txtdate == null)
        return true;
    var text = txtdate;
    var comp = text.split('/');
    if (comp.length == 3) {
        if ($.isNumeric(comp[0]) && $.isNumeric(comp[1]) && $.isNumeric(comp[2])) {
            var m = parseInt(comp[1], 10);
            var d = parseInt(comp[0], 10);
            var y = parseInt(comp[2], 10);
            var date = new Date(y, m - 1, d);
            if (date.getFullYear() == y && date.getMonth() + 1 == m && date.getDate() == d) {
                return true;
            } else {
                return false
            }
        } else return false;
    } else return false;
}
function formatDateTime(strValue) {
    if (strValue == null) return "";
    var date = new Date(strValue);
    return date.format('dd/MM/yyyy HH:mm');
}
function SubStringShort(yourString, maxLength) {
    if (yourString == null)
        return "";
    if (yourString.length < maxLength)
        return yourString;
    if (yourString != undefined && yourString != null) {
        //trim the string to the maximum length
        var trimmedString = yourString.substr(0, maxLength);

        //re-trim if we are in the middle of a word
        trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")));
        return trimmedString;
    }
}
function GetPathUrlString(urlfull) {
    if (urlfull != null && urlfull != '') {
        var reg = /.+?\:\/\/.+?(\/.+?)(?:#|\?|$)/;
        var pathname = reg.exec(urlfull)[1];
        return pathname;
    } else return "";
}
function GetFileName(urlfile) {
    var filename = urlfile.split('/').pop();
    return filename;
}
function CheckExtension(filename, exts) {
    return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(filename.toLowerCase());
}
function SetConfigLang(ConfigLanguages, idparentdiv ) {
    
    var  $ConfigLanguages = jQuery.parseJSON(ConfigLanguages);
    console.log($ConfigLanguages);
    $(`.${idparentdiv} [data-lang]`).each(function () {
        var element = $(this);
        var keylang = $(this).attr("data-lang");
        if ($ConfigLanguages[keylang] != null && $ConfigLanguages[keylang] != undefined) {
            if (element.is("input")) {
                element.attr("placeholder", $ConfigLanguages[keylang]);

            } else {
                element.html($ConfigLanguages[keylang]);
            }
        }
        // Do something with the element
    });
}