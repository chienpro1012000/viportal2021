﻿
var vidatalocale = {
    "format": "DD/MM/YYYY",
    "separator": "/",
    "applyLabel": "Apply",
    "cancelLabel": "Cancel",
    "fromLabel": "From",
    "toLabel": "To",
    "customRangeLabel": "Custom",
    "weekLabel": "W",
    "daysOfWeek": [
        "CN",
        "T2",
        "T3",
        "T4",
        "T5",
        "T6",
        "T7"
    ],
    "monthNames": [
        "Tháng 1",
        "Tháng 2",
        "Tháng 3",
        "Tháng 4",
        "Tháng 5",
        "Tháng 6",
        "Tháng 7",
        "Tháng 8",
        "Tháng 9",
        "Tháng 10",
        "Tháng 11",
        "Tháng 12"
    ],
    "firstDay": 1
};
(function ($) {
    
    jQuery.validator.addMethod(
        "LeqToDay",
        function (value, element) {
            var dtToDay = new Date();
            var dateMomentObject = moment(value, "DD/MM/YYYY"); // 1st argument - string, 2nd argument - format
            var dateObject = dateMomentObject.toDate(); // convert moment.js object to Date object
            if (dateObject <= dtToDay) {
                return true;
            }
            return false
        },
        "Ngày nhập không vượt quá ngày hiện tại"
    );
    jQuery.validator.addMethod("specialChars", function (value, element) {
        var regex = new RegExp("^[a-zA-Z0-9]+$");
        var key = value;

        if (!regex.test(key)) {
            return false;
        }
        return true;
    }, "Nhập ký tự chữ, hoặc số không bao gồm ký tự đặc biệt");
    jQuery.validator.addMethod("numberchar", function (value, element) {
        var regex = new RegExp("^[0-9]+$");
        var key = value;

        if (!regex.test(key)) {
            return false;
        }
        return true;
    }, "Nhập đúng định dạnh số thứ tự. Không nhập chữ cái và số âm");
    //jQuery.validator.addMethod("alphanumeric", function (value, element) {
    //    return this.optional(element) || value.indexOf(" ") || /^\w+$/i.test(value);
    //}, "Nhập ký tự chữ, hoặc số không bao gồm ký tự đặc biệt");
    jQuery.validator.addMethod("phoneNumber", function (phone_number, element) {
        phone_number = phone_number.replace(/\s+/g, "");
        return this.optional(element) || phone_number.length > 9 && phone_number.match(/^(\+84|0)[1-9]\d{8,9}$/);
    }, "Nhập đúng định dạng số điện thoại");

    jQuery.validator.addMethod("gmail", function (value, element) {
        // Kiểm tra định dạng của chuỗi nhập vào bằng biểu thức chính quy
        return this.optional(element) || /^.+@gmail\.com$/.test(value);
    }, "Nhập đúng định dạng Gmail");
    jQuery.validator.addMethod("Attachments", function (value, element) {
        // Kiểm tra định dạng của chuỗi nhập vào bằng biểu thức chính quy
        var $parentattachmnet = $(element).closest('.fileupload-buttonbar');
        var checkAttach = $parentattachmnet.find('table.uploadfiles tr').length;
        console.log(checkAttach);
        if (checkAttach > 0) {
            return true;
        } else return false;
        //return this.optional(element) || /^.+@gmail\.com$/.test(value);
    }, "Đính kèm tài liệu");
    jQuery.extend(jQuery.validator.messages, {
        required: "Trường yêu cầu bắt buộc nhập",
        remote: "Please fix this field.",
        email: "Nhập đúng định dạng Email",
        url: "Please enter a valid URL.",
        date: "Please enter a valid date.",
        dateISO: "Please enter a valid date (ISO).",
        number: "Please enter a valid number.",
        digits: "Please enter only digits.",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Please enter the same value again.",
        accept: "Please enter a value with a valid extension.",
        maxlength: jQuery.validator.format("Nhập tối đa {0} ký tự."),
        minlength: jQuery.validator.format("Please enter at least {0} characters."),
        rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
        range: jQuery.validator.format("Please enter a value between {0} and {1}."),
        max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
        min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
    });
    $.fn.viDataTable = function (settings) {
        var $this = $(this);
        var thisID = $this.attr("id");
        var config = {
            searching: false,
            paging: true,
            frmSearch: "",
            "order": [[0, "desc"]],
            data: {},
            parameterPlus: function (para) {

            },
        }

        if (settings) $.extend(config, settings);
        var $tabledata = $this.DataTable({
            scrollCollapse: true,
            "paging": config.paging,
            "language": {
                "info": "Hiển thị _START_ đến _END_ trên _TOTAL_ bản ghi",
                "lengthMenu": "Hiển thị _MENU_ bản ghi",
                "infoEmpty": "",
                "paginate": {
                    "first": "Đầu",
                    "last": "Cuối",
                    "next": "Tiếp",
                    "previous": "Trước"
                },
                "zeroRecords": "Không tìm thấy bản ghi nào",
            },
            "pagingType": "full_numbers",
            "lengthMenu": [
                [20, 30, 50, 100],
                [20, 30, 50, 100]
            ],
            //"dom": 't<"col-xs-12 col-sm-3 col-md-3 nopadding"l><"col-xs-12 col-sm-5 col-md-5 nopadding infortotal"i><"col-xs-12 col-sm-4 col-md-4 nopadding"p><"clear">',
            "dom": "t<'row'<'col-sm-12 col-md-3'l><'col-sm-12 col-md-5'i><'col-sm-12 col-md-4'p>>",
            "searching": false,
            "lengthChange": true,
            "length": 10,
            "processing": true, //show processing text while request data
            "serverSide": true, // request data from server side
            "order": config.order,
            "ajax": {
                type: "POST",
                "url": config.url,
                "data": function (d) {

                    var odata = {};
                    d = $.extend({}, d, config.data);
                    console.log(odata);
                    if (config.frmSearch != "") {
                        odata = $.extend({}, d, $("#" + config.frmSearch).siSerializeArray());
                    } else {
                        var _element = $this.closest("[role='body-data']");
                        var $zonesearch = _element.find(".zonesearch").first();
                        if ($zonesearch != undefined) {
                            odata = $.extend({}, d, $($zonesearch).siSerializeArray());
                        }
                    }
                    if (jQuery.isFunction(config.parameterPlus)) {
                        config.parameterPlus(odata); //
                    }
                    return odata;
                }
            },
            "aoColumns": config.aoColumns
        });
        $tabledata.on('draw', function () {
            if (!config.paging) {
                $(".dataTables_info").hide();
            }
            console.log('Redraw occurred at: ' + new Date().getTime());

            //check permision ở đây.
            var PerMission = $("#BannerLog").data("permission");
            var $input = $('#' + thisID + " [data-per]");
            var $inputs = $this.find($input);
            $inputs.each(function () {
                var $per = $(this).attr("data-per");
                if (PerMission.indexOf(`|${$per}|`) == -1) {
                    $(this).remove();
                }
            });
        });
        //tìm kiếm trên form.
        var $body = $(this).closest("[role='body-data']");
        $body.find(".zonesearch input[type=text]").keypress(function (e) {
            if (e.which == 13) {
                e.preventDefault();
                $this.DataTable().ajax.reload();
            }
        });
        $body.find(".zonesearch select").change(function () {
            $this.DataTable().ajax.reload();
        });
        return $tabledata;
    }
    $.fn.viCustomFck = function (settings) {
        var $this = $(this);
        var thisID = $this.attr("id");

        var config = {
            height: 150
        }
        if (settings) $.extend(config, settings);
        CKEDITOR.replace(thisID, {
            //extraPlugins: 'easyimage',
            removePlugins: 'easyimage',
            //cloudServices_tokenUrl: '/VIPortalAPI/easyimage/token',
            //cloudServices_uploadUrl: '/VIPortalAPI/easyimage/upload',
            // Define the toolbar groups as it is a more accessible solution.
            //toolbarGroups: [
            //	{ "name": "basicstyles", "groups": ["basicstyles"] },
            //	{ "name": "links", "groups": ["links"] },
            //	{ "name": "about", "groups": ["about"] }
            //],
            toolbar: [
                {
                    name: 'basicstyles',
                    groups: ['basicstyles', 'cleanup'],
                    items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']
                },
                {
                    name: 'styles',
                    items: ['Styles', 'Format', 'Font', 'FontSize']
                }, {
                    name: 'insert',
                    "groups": ["insert"],
                    items: ['Image']
                }
            ],
            height: 100
            // Remove the redundant buttons from toolbar groups defined above.
            //removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
            //height: config.height
            // Remove the redundant buttons from toolbar groups defined above.
            //removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
        });
    }

    $.fn.CheckPer = function (settings) {
        return this.each(function () {
            var $this = $(this);
            var thisID = $this.attr("id");
            var PerMission = $("#BannerLog").data("permission");
            var $input = $('#' + thisID + " [data-per]");
            var $inputs = $this.find($input);
            $inputs.each(function () {
                var $per = $(this).attr("data-per");
                if (PerMission.indexOf(`|${$per}|`) == -1) {
                    $(this).remove();
                }
            });
        })
    }
    // xử lý xự kiện đống modal từ form con
    $.fn.closeModal = function () {
        return this.each(function () {
            var $this = $(this);
            var $modal = $this.closest("div[role='dialog']");
            $modal.modal('hide');
            $($modal).removeData();
        })
    };
    $.fn.viSerialize = function () {
        var returning = '';
        $('input[type=checkbox]:not(:checked)', this).each(function () {
            if (this.name != '' && this.value != '1') {
                this.value = 'false';
                returning += this.name + '=false&';
            }
        });
        returning += $(this).serialize();
        return returning;
    };
    $.fn.viSerializeKhaiThac = function () {
        // get all the inputs into an array.
        var $id = $(this).attr('id');
        var $inputs = $('#' + $id + ' :input');

        // not sure if you wanted this, but I thought I'd add it.
        // get an associative array of just the values.
        var values = {};
        $inputs.each(function () {
            if ($(this).val() != "" && $(this).val() != undefined)
                values[this.name] = $(this).val();
        });
        //select
        return values;
    };
    $.fn.siSerializeArray = function () {
        // get all the inputs into an array.
        var $id = $(this).attr('id');
        var $inputs = $(this).find(':input') //$('#' + $id + ' :input');

        // not sure if you wanted this, but I thought I'd add it.
        // get an associative array of just the values.
        var values = {};
        $inputs.each(function () {
            if ($(this).val() != "" && $(this).val() != undefined)
                values[this.name] = $(this).val();
        });

        return values;
    };

})(jQuery);
///Check Quyen nguoi dung
function CheckPermiss(lstUser, userid) {
    if (lstUser == null)
        return true;
    if (lstUser.findIndex(x => x.ID == userid) > -1)
        return true;
    else return false;
}

function getScrollBarWidth() {
    var w1 = $(".smtable tbody").width();
    var w2 = $(".smtable tbody tr").first().width();
    return (w1 - w2);
};


function getTenVietTatKhongUtf8(tendaydu) {
    tendaydu = trim12(tendaydu);
    tendaydu = tendaydu.replace(/\s+/g, ' ');

    var tenviettat = '';
    if (tendaydu.length > 0) {
        tenviettat += tendaydu[0];
        for (var indexc = 0; indexc < tendaydu.length; indexc++) {
            if (tendaydu[indexc] == ' ') {
                tenviettat += tendaydu[indexc + 1];
            }
        }
    }
    tenviettat = tenviettat.replace(/\s+/g, ' ');
    tenviettat = tenviettat.replace(/ /g, '');
    tenviettat = tenviettat.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    tenviettat = tenviettat.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    tenviettat = tenviettat.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    tenviettat = tenviettat.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    tenviettat = tenviettat.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    tenviettat = tenviettat.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    tenviettat = tenviettat.replace(/đ/g, "d");
    tenviettat = tenviettat.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    tenviettat = tenviettat.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    tenviettat = tenviettat.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    tenviettat = tenviettat.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    tenviettat = tenviettat.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    tenviettat = tenviettat.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    tenviettat = tenviettat.replace(/Đ/g, "D");

    return tenviettat.toUpperCase();
}
function trim12(strs) {
    if (strs != null && strs != undefined) {
        var str = strs.replace(/^\s\s*/, ''),
            ws = /\s/,
            i = str.length;
        while (ws.test(str.charAt(--i)));
        return str.slice(0, i + 1);
    }
    return "";
}

function openDialogMsg(stitle, sMsg) {
    BootstrapDialog.show({
        title: stitle,
        message: sMsg,
        closable: true,
        draggable: true,
        buttons: [{
            label: "Thoát",
            action: function (dialogRef) {
                dialogRef.close();
            }
        }]
    });
}

function openDialog(stitle, urlpageLoad, data, size, buttonName) {
    console.log(BootstrapDialog);
    BootstrapDialog.show({
        z_index_backdrop: 9000,
        z_index_modal: 9010,
        title: stitle,
        message: function (dialog) {
            var $message = dialog.getModalBody();
            //NProgress.start();
            var pageToLoad = dialog.getData('pageToLoad');
            if (data == undefined) {
                data = {};
            }
            if (size == "NORMAL")
                dialog.setSize(BootstrapDialog.SIZE_NORMAL);
            else if (size == "SMALL")
                dialog.setSize(BootstrapDialog.SIZE_SMALL);
            else if (size == "LARGE")
                dialog.setSize(BootstrapDialog.SIZE_LARGE);
            else if (size == "FULL") {
                dialog.setSize(BootstrapDialog.SIZE_FULL);
            } else if ($.isNumeric(size)) {
                var ModalDialog = dialog.getModalDialog();
                ModalDialog.removeClass("modal-lg");
                //ModalDialog.addClass("modal-fullscreen");
                //fullscreen-xl 
                ModalDialog.css("width", size + "px");
                ModalDialog.css("max-width", size + "px");
            } else
                dialog.setSize(BootstrapDialog.SIZE_WIDE);
            return "";
        },
        closable: true,
        closeByBackdrop: false,
        draggable: true,
        //buttons: [{
        //    label: (buttonName != 'undefined' && buttonName != '' && buttonName != null) ? buttonName : 'Cập nhật',
        //    cssClass: 'btn-primary vibtn btnformsubmit',
        //    hotkey: 13, // Enter.
        //    action: function (dialogRef) {
        //        //var $formPost = dialogRef.getModalBody().find("form");
        //        //$formPost.data("dlgref", dialogRef);
        //        //CK_jQ();
        //        //$formPost.submit();
        //    }
        //},
        //{
        //    cssClass: 'vibtn',
        //    label: "Thoát",
        //    action: function (dialogRef) {
        //        dialogRef.close();
        //    }
        //}
        //],
        data: {
            pageToLoad: urlpageLoad
        },
        onshow: function (dialogRef) {

        },
        onshown: function (dialogRef) {
            $(document).off('focusin.modal');
            //alert('Dialog is popped up.');
            //alert('Dialog is popping up, its message is ' + dialogRef.getMessage());
            //debugger;
            //console.log(dialogRef.getModalFooter());
            var $footer = dialogRef.getModalFooter();
            $footer.show();
            $footer.html('<button class="btn btn-primary vibtn btnformsubmit bootstrap4-dialog-button" type="button">' + ((buttonName != 'undefined' && buttonName != '' && buttonName != null) ? buttonName : 'Cập nhật') + '</button><button class="btn vibtn btnClosefrm bootstrap4-dialog-button">Thoát</button>');

            //var btnsubmit = dialogRef.getModalFooter().find("button.btn-primary");
            //btnsubmit.attr("type", "button");
            var $message = dialogRef.getModalBody();
            //NProgress.start();
            var pageToLoad = dialogRef.getData('pageToLoad');
            if (data == undefined) {
                data = {};
            }
            $.ajax({
                method: "POST",
                cache: false,
                url: pageToLoad,
                data: data
            }).done(function (msg) {
                $message.html(msg);
            });
        },
        onhide: function (dialogRef) {
            //alert('Dialog is popping down, its message is ' + dialogRef.getMessage());
        },
        onhidden: function (dialogRef) {
            //alert('Dialog is popped down.');
        }
    });
}

function openDialogView(stitle, urlpageLoad, data, size, buttonName) {
    console.log(BootstrapDialog);
    BootstrapDialog.show({
        z_index_backdrop: 9000,
        z_index_modal: 9010,
        title: stitle,
        message: function (dialog) {
            var $message = dialog.getModalBody();
            //NProgress.start();
            var pageToLoad = dialog.getData('pageToLoad');
            if (data == undefined) {
                data = {};
            }
            if (size == "NORMAL")
                dialog.setSize(BootstrapDialog.SIZE_NORMAL);
            else if (size == "SMALL")
                dialog.setSize(BootstrapDialog.SIZE_SMALL);
            else if (size == "LARGE")
                dialog.setSize(BootstrapDialog.SIZE_LARGE);
            else if (size == "FULL") {
                dialog.setSize(BootstrapDialog.SIZE_FULL);
            } else if ($.isNumeric(size)) {
                var ModalDialog = dialog.getModalDialog();
                ModalDialog.removeClass("modal-lg");
                //ModalDialog.addClass("modal-fullscreen");
                //fullscreen-xl 
                ModalDialog.css("width", size + "px");
                ModalDialog.css("max-width", size + "px");
            } else
                dialog.setSize(BootstrapDialog.SIZE_WIDE);
            return "";
        },
        closable: true,
        closeByBackdrop: false,
        draggable: true,
        data: {
            pageToLoad: urlpageLoad
        },
        onshow: function (dialogRef) {

        },
        onshown: function (dialogRef) {
            //alert('Dialog is popped up.');
            //alert('Dialog is popping up, its message is ' + dialogRef.getMessage());
            var $message = dialogRef.getModalBody();
            //NProgress.start();
            var pageToLoad = dialogRef.getData('pageToLoad');
            if (data == undefined) {
                data = {};
            }
            $.ajax({
                method: "POST",
                cache: false,
                url: pageToLoad,
                data: data
            }).done(function (msg) {
                $message.html(msg);
            });
        },
        onhide: function (dialogRef) {
            //alert('Dialog is popping down, its message is ' + dialogRef.getMessage());
        },
        onhidden: function (dialogRef) {
            //alert('Dialog is popped down.');
        }
    });
}

function GetValueArrayLookupDate(lstdata) {
    var blkstr = [];
    $.each(lstdata, function (idx2, val2) {
        blkstr.push(val2.Title);
    });
    return blkstr.join(", ");
}
// load ajax lấy nội dụng từ file urlContent đổ vào thẻ html container
function loadAjaxContent(urlContent, container, data) {
    try {
        var $container = $(container);
        if (typeof data == undefined)
            data = {};
        //$(container).html("<img src='/Content/build/images/loading.gif' />");
        var dfd = $.Deferred();
        $container.load(urlContent, data, function (response, status, xhr) {
            if (status == "error") {
                var msg = "Đã có lỗi xảy ra khi tải trang, vui lòng kiểm tra lại kết nối internet" + xhr.status + " " + xhr.statusText;
                $container.html(msg);
            }
            return dfd.resolve();
        });
        return dfd.promise();
    } catch (e) {
        $container.html(e.message);
    }
}

function showMsg(msg) {
    toastr.success(msg);
}
function showMsgwarning(msg) {
    toastr.warning(msg);
}
function showMsgNew(msg) {
    var $boxMsg = $("#msg");
    var $boxContent = $boxMsg.find("> span");
    $boxContent.html(msg);
    $boxMsg.fadeIn();
}

function getRoleTable($tag) {
    return $tag.closest("table");
}

function getRoleData($tag) {
    return $tag.closest("div[role='body-data']");
}
//Fckfinder


function DeleteImage(btnXoaAnh, imgAnhdaiDien, hdfUrlImages) {
    $("#" + btnXoaAnh + "").click(function () {
        $("#" + imgAnhdaiDien + "").attr('src', '');
        $("#" + hdfUrlImages + "").val("");
    });
}

function DeleteFromDialogFunc(urlAction, odata, title, funCallBack) {
    BootstrapDialog.confirm({
        title: 'Xác nhận xóa ' + title,
        message: "Bạn có chắc chắn xóa bản ghi này?",
        type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
        closable: true, // <-- Default value is false
        draggable: true, // <-- Default value is false
        btnCancelLabel: 'Hủy', // <-- Default value is 'Cancel',
        btnOKLabel: 'Đồng ý!', // <-- Default value is 'OK',
        btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
        callback: function (result) {
            // result will be true if button was click, while it will be false if users close the dialog directly.
            if (result) {
                $.post(urlAction, odata).done(function (result) {
                    if (result.State == 2) {
                        BootstrapDialog.show({
                            title: "Lỗi",
                            message: result.Message
                        });
                    }
                    else {
                        showMsg(result.Message);
                        if (funCallBack != undefined && jQuery.isFunction(funCallBack)) {
                            funCallBack();
                        }
                    }

                }).always(function () {

                });
            }
        }
    });
}
function DeleteFromDialog(urlAction, odata, urlReload) {
    BootstrapDialog.confirm({
        title: 'Xác nhận xóa',
        message: "Bạn có chắc chắn xóa bản ghi này?",
        type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
        closable: true, // <-- Default value is false
        draggable: true, // <-- Default value is false
        btnCancelLabel: 'Hủy', // <-- Default value is 'Cancel',
        btnOKLabel: 'Đồng ý!', // <-- Default value is 'OK',
        btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
        callback: function (result) {
            // result will be true if button was click, while it will be false if users close the dialog directly.
            if (result) {
                $.post(urlAction, odata).done(function (result) {
                    if (result.State == 2) {
                        BootstrapDialog.show({
                            title: "Lỗi",
                            message: result.Message
                        });
                    }
                    else {
                        showMsg(result.Message);
                        //$tagTable.dataTable().fnDraw(false);
                        loadAjaxContent(urlReload, "#tblDanhBa", { ItemID: result.ID });
                    }

                }).always(function () {

                });
            }
        }
    });
}

function CK_jQ() {
    for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].updateElement();
    }
}

function loading() {
    $("#s4-workspace").addClass("loading");
}
function Endloading() {
    $("#s4-workspace").removeClass("loading");
}

$(function () {
    //auto update element ckeditor
    CKEDITOR.on('instanceReady', function () {
        $.each(CKEDITOR.instances, function (instance) {
            CKEDITOR.instances[instance].document.on("focusout", CK_jQ);
            CKEDITOR.instances[instance].document.on("keyup", CK_jQ);
            CKEDITOR.instances[instance].document.on("paste", CK_jQ);
            CKEDITOR.instances[instance].document.on("keypress", CK_jQ);
            CKEDITOR.instances[instance].document.on("blur", CK_jQ);
            CKEDITOR.instances[instance].document.on("change", CK_jQ);
            CKEDITOR.instances[instance].document.on("focus", CK_jQ);
        });
    });
    //checked-all
    $(document).on("click", ".checked-all", function (event) {
        var $tagData = getRoleData($(this));
        var $tagTable = getRoleTable($(this));
        $tagData.find(".check-item").prop('checked', this.checked);

    });
    // add form thêm câu hỏi
    //act-delete-multi
    $(document).on("click", ".act-delete-multi", function (event) {
        var $tagData = getRoleData($(this));
        var $tagTable = getRoleTable($(this));
        console.log($tagData);
        var selecteditems = [];

        $tagData.find(".check-item:checked").each(function (i, ob) {
            selecteditems.push($(ob).attr('data-id'));
        });
        var $idchecked = selecteditems.join(",");
        if ($idchecked == '') {
            BootstrapDialog.show({
                title: "Lỗi",
                message: "Chọn bản ghi cần xóa"
            });
        } else {
            var datapost = $tagData.data("parameters");
            if (datapost == undefined || datapost == 'undefined') {
                datapost = {}
            }
            $.extend(datapost, $(this).data());
            datapost["LstItemID"] = $idchecked;
            if (datapost["do"] == undefined || datapost["do"] == null) {
                datapost["do"] = "DELETE-MULTI";
            }
            //openDialog("Sửa " + $tagData.data("title"), $tagData.data("action"), datapost, $tagData.data("size"));
            BootstrapDialog.confirm({
                title: 'Xác nhận xóa',
                message: "Bạn có chắc chắn xóa các bản ghi đã chọn?",
                type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Hủy', // <-- Default value is 'Cancel',
                btnOKLabel: 'Đồng ý!', // <-- Default value is 'OK',
                btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        $.post($tagData.data("action"), datapost).done(function (result) {
                            if (result.State == 2) {
                                BootstrapDialog.show({
                                    title: "Lỗi",
                                    message: result.Message
                                });
                            }
                            else {
                                showMsg(result.Message);
                                $tagTable.dataTable().fnDraw(false);
                            }

                        }).always(function () {

                        });
                    }
                }
            });
            event.stopPropagation();
        }
    });
    //btnformsubmit 
    $(document).on("click", "button.btnformsubmit", function (event) {
        var modeldialog = $(this).closest('.modal-content');
        var $form = modeldialog.find('form');
        CK_jQ();
        $form.submit();
    });
    $(document).on("click", "button.btnClosefrm", function (event) {
        var modeldialog = $(this).closest('.modal-content');
        var $form = modeldialog.find('form');
        $($form).closeModal();
    });
    $(document).on("click", "a.act-edit_v1", function (event) {
        var $tagData = getRoleData($(this));
        var datapost = $tagData.data("parameters");
        if (datapost == undefined || datapost == 'undefined') {
            datapost = {}
        }
        $.extend(datapost, $(this).data());
        console.log($(this).data());
        openDialog("Thêm mới bộ câu hỏi " + $tagData.data("title"), $tagData.data("formch"), datapost, $tagData.data("size"));
        event.stopPropagation();
    });
    //xử lý sự kiện mở form thêm mới
    $(document).on("click", "button.act-add", function (event) {
        var $tagData = getRoleData($(this));
        var datapost = $tagData.data("parameters");
        if (datapost == undefined || datapost == 'undefined') {
            datapost = {}
        }

        var datapostSub = $(this).data();
        if (datapostSub == undefined || datapostSub == 'undefined') {
            datapostSub = {}
        }
        var datapostFull = $.extend({}, datapost, datapostSub);
        var width = $(".container-fluid").width();
        if (width < "768") {
            openDialog("Thêm mới " + $tagData.data("title"), $tagData.data("form"), datapostFull);
        } else {
            openDialog("Thêm mới " + $tagData.data("title"), $tagData.data("form"), datapostFull, $tagData.data("size"));
        }        
        event.stopPropagation();
    });

    //act-approved
    $(document).on('click', "a.act-approved", function (event) {
        var $tagData = getRoleData($(this));
        var $tagTable = getRoleTable($(this));
        var datapost = $tagData.data("parameters");
        if (datapost == 'undefined') {
            datapost = {}
        }
        var dataFinish = { ItemID: $(this).data("id"), "do": "APPROVED" };
        $.extend(dataFinish, datapost);
        //NProgress.start();
        $.post($tagData.data("action"), dataFinish).done(function (result) {
            if (result.State == 2) {
                BootstrapDialog.show({
                    title: "Lỗi",
                    message: result.Message
                });
            }
            else {
                showMsg(result.Message);
                $tagTable.dataTable().fnDraw(false);
            }

        }).always(function () {
            //NProgress.done();
        });
        event.stopPropagation();
    });
    //sangvd add approve danh bạ
    $(document).on('click', "a.act-approvedDanhBa", function (event) {
        var $tagData = getRoleData($(this));
        var $tagTable = getRoleTable($(this));
        var datapost = $tagData.data("parameters");
        if (datapost == 'undefined') {
            datapost = {}
        }
        var dataFinish = { ItemID: $(this).data("id"), "do": "APPROVED" };
        $.extend(dataFinish, datapost);
        //$('body').waitMe();
        $.post($tagData.data("action"), dataFinish).done(function (result) {
            if (result.State == 2) {
                BootstrapDialog.show({
                    title: "Lỗi",
                    message: result.Message
                });
            }
            else {
                showMsg(result.Message);
                loadAjaxContent($tagData.data("list"), "#tblDanhBa", { ItemID: result.ID });
            }

        }).always(function () {
            //$('body').waitMe("hide");
        });
        event.stopPropagation();
    });
    $(document).on('click', "a.act-pendding", function (event) {
        var $tagData = getRoleData($(this));
        var $tagTable = getRoleTable($(this));
        var datapost = $tagData.data("parameters");
        if (datapost == 'undefined') {
            datapost = {}
        }
        var dataFinish = { ItemID: $(this).data("id"), "do": "PENDDING" };
        $.extend(dataFinish, datapost);
        //NProgress.start();
        $.post($tagData.data("action"), dataFinish).done(function (result) {
            if (result.State == 2) {
                BootstrapDialog.show({
                    title: "Lỗi",
                    message: result.Message
                });
            }
            else {
                showMsg(result.Message);
                $tagTable.dataTable().fnDraw(false);
            }

        }).always(function () {
            //NProgress.done();
        });
        event.stopPropagation();
    });
    //sangvd add
    $(document).on('click', "a.act-penddingDanhBa", function (event) {
        var $tagData = getRoleData($(this));
        var $tagTable = getRoleTable($(this));
        var datapost = $tagData.data("parameters");
        if (datapost == 'undefined') {
            datapost = {}
        }
        //var dtstartpage = $(".paginate_button.active a").data("dt-idx");
        //var dtstart = 0;
        //if (dtstartpage > 1)
        //    dtstart = 10 * dtstartpage;
        var dataFinish = { ItemID: $(this).data("id"), "do": "PENDDING" };
        $.extend(dataFinish, datapost);
        //$('body').waitMe();
        $.post($tagData.data("action"), dataFinish).done(function (result) {
            if (result.State == 2) {
                BootstrapDialog.show({
                    title: "Lỗi",
                    message: result.Message
                });
            }
            else {
                console.log(result.ID);
                showMsg(result.Message);
                loadAjaxContent($tagData.data("list"), "#tblDanhBa", { ItemID: result.ID });
            }

        }).always(function () {
            //$('body').waitMe("hide");
        });
        event.stopPropagation();
    });
    //act-edit
    $(document).on("click", "a.act-edit", function (event) {
        var $tagData = getRoleData($(this));
        var datapost = $tagData.data("parameters");
        if (datapost == undefined || datapost == 'undefined') {
            datapost = {}
        }
        var datapostFull = $.extend({}, datapost, $(this).data());
        if (datapostFull["wftrangthai"] == "999") {
            BootstrapDialog.show({
                title: "Thông báo",
                message: "Hủy duyệt trước khi thực hiện sửa"
            });
            return;
        }
        console.log($tagData.data("parameters"));
        openDialog("Sửa " + $tagData.data("title"), $tagData.data("form"), datapostFull, $tagData.data("size"));
        event.stopPropagation();
    });
    //act-workflow
    $(document).on("click", "a.act-workflow", function (event) {
        var $tagData = getRoleData($(this));
        var datapost = $tagData.data("parameters");
        if (datapost == undefined || datapost == 'undefined') {
            datapost = {}
        }
        var datapostFull = $.extend({}, datapost, $(this).data());
        console.log($tagData.data("parameters"));
        openDialog("Quy trình xuất bản nội dung", "/UserControls/QTQuyTrinhBT/Workflow/pFormThaoTac.aspx", datapostFull, 900);
        event.stopPropagation();
    });
    //act-view
    $(document).on("click", "a.act-view", function (event) {
        var $tagData = getRoleData($(this));
        var datapost = $tagData.data("parameters");
        if (datapost == undefined || datapost == 'undefined') {
            datapost = {}
        }
        var datapostFull = $.extend({}, datapost, $(this).data());
        //$.extend(datapost, $(this).data());
        console.log($(this).data());
        openDialogView("Xem " + $tagData.data("title"), $tagData.data("view"), datapostFull, $tagData.data("size"));
        event.stopPropagation();
    });
    // act-view tin tức
    $(document).on('click', "a.act-view-news", function (event) {
        var $tagData = getRoleData($(this));
        var datapost = $tagData.data("parameters");
        if (datapost == 'undefined') {
            datapost = {}
        }
        var dataFinish = { ItemID: $(this).data("id"), UrlListNews: $("#UrlListNews").val() };
        $.extend(dataFinish, datapost);
        openDialogView("Hiển thị thông tin", $tagData.data("view"), dataFinish, $tagData.data("size"));
        event.stopPropagation();
        return false;
    });
    //act-view bộ chủ đề - câu hỏi
    $(document).on("click", "a.act-view-ch", function (event) {
        var $tagData = getRoleData($(this));
        var datapost = $tagData.data("parameters");
        if (datapost == undefined || datapost == 'undefined') {
            datapost = {}
        }
        $.extend(datapost, $(this).data());
        console.log($(this).data());
        openDialogView("Xem " + $tagData.data("title"), $tagData.data("viewch"), datapost, $tagData.data("size"));
        event.stopPropagation();
    });
    //act-edit
    $(document).on("click", "a.btnfrm, button.btnfrm", function (event) {
        var datapost = {};
        var $tagData = getRoleData($(this));
        var datapost = $tagData.data("parameters");

        if (datapost == undefined || datapost != null)
            datapost = {};
        else {
            if (Object.prototype.toString.call(datapost) == '[object String]') {
                // a string
                datapost = jQuery.parseJSON(datapost);
            }
        }
        $.extend(datapost, $(this).data());
        var url = $(this).attr("url");
        var title = $(this).attr("title");
        var size = $(this).attr("size");
        var Itemid = $(this).data("id");
        //var dataFinishcheck = { ItemID: Itemid, do: "CHECKAPPROVED" };
        //$.extend(dataFinishcheck, datapost);
        $.post($tagData.data("action"), { ItemID: Itemid, do: "CHECKAPPROVED" }).done(function (result) {
            if (result.State == 2) {
                openDialogMsg("Lỗi", result.Message);
            }
            else {
                if (result.Source == 0)
                    openDialogMsg("Thông báo", "Bạn cần duyệt kết luận trước khi gửi mail");
                else {
                    console.log($(this).data());
                    openDialog(title, url, datapost, size);
                    event.stopPropagation();
                }
            }
        });
    });


    //act-edit
    $(document).on("click", "a.btnfrmnochek, button.btnfrmnochek", function (event) {
        var datapost = {};
        var $tagData = getRoleData($(this));
        var datapost = $tagData.data("parameters");

        if (datapost == undefined || datapost != null)
            datapost = {};
        else {
            if (Object.prototype.toString.call(datapost) == '[object String]') {
                // a string
                datapost = jQuery.parseJSON(datapost);
            }
        }
        $.extend(datapost, $(this).data());
        var url = $(this).attr("url");
        var title = $(this).attr("title");
        var size = $(this).attr("size");
        var Itemid = $(this).data("id");
        //var dataFinishcheck = { ItemID: Itemid, do: "CHECKAPPROVED" };
        //$.extend(dataFinishcheck, datapost);
        openDialog(title, url, datapost, size);
    });
    //act-edit
    $(document).on("click", "a.btnfrmnocheknobutton, button.btnfrmnocheknobutton", function (event) {
        var datapost = {};
        var $tagData = getRoleData($(this));
        var datapost = $tagData.data("parameters");

        if (datapost == undefined || datapost != null)
            datapost = {};
        else {
            if (Object.prototype.toString.call(datapost) == '[object String]') {
                // a string
                datapost = jQuery.parseJSON(datapost);
            }
        }
        $.extend(datapost, $(this).data());
        var url = $(this).attr("url");
        var title = $(this).attr("title");
        var size = $(this).attr("size");
        //var dataFinishcheck = { ItemID: Itemid, do: "CHECKAPPROVED" };
        //$.extend(dataFinishcheck, datapost);
        openDialogView(title, url, datapost, size);
    });
    //act-delete
    $(document).on("click", "a.act-delete", function (event) {
        var $tagData = getRoleData($(this));
        var $tagTable = getRoleTable($(this));
        var datapost = $tagData.data("parameters");
        if (datapost == undefined || datapost == 'undefined') {
            datapost = {}
        }
        var Itemdelete = $(this).data("id");
        var dataFinishcheck = { ItemID: Itemdelete, do: "CHECKAPPROVED" };
        $.extend(dataFinishcheck, datapost);
        //find title của bản ghi.
        var $Title;
        if ($(this).parent().parent().find('.act-view').first().text() != "")
            $Title = $(this).parent().parent().find('.act-view').first().text();
        else {
            $Title = $(this).parent().parent().find('.act-view-news').first().text();
        }
        $.post($tagData.data("action"), dataFinishcheck).done(function (result) {
            if (result.State == 2) {
                //$('body').waitMe("hide");
                openDialogMsg("Lỗi", result.Message);
            }
            else {
                if (result.Source == 1)
                    openDialogMsg("Thông báo", "Bạn cần hủy duyệt trước khi xóa");
                else {
                    var dataFinish = { ItemID: Itemdelete, do: "DELETE" };
                    BootstrapDialog.confirm({
                        title: 'Xác nhận xóa',
                        message: `Bạn có chắc chắn xóa bản ghi "${$Title}" ?`,
                        type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                        closable: true, // <-- Default value is false
                        draggable: true, // <-- Default value is false
                        btnCancelLabel: 'Hủy', // <-- Default value is 'Cancel',
                        btnOKLabel: 'Đồng ý!', // <-- Default value is 'OK',
                        btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
                        callback: function (result) {
                            // result will be true if button was click, while it will be false if users close the dialog directly.
                            if (result) {
                                $.extend(dataFinish, datapost);
                                $.post($tagData.data("action"), dataFinish).done(function (result) {
                                    if (result.State == 2) {
                                        BootstrapDialog.show({
                                            title: "Lỗi",
                                            message: result.Message
                                        });
                                    }
                                    else {
                                        showMsg(result.Message);
                                        $tagTable.dataTable().fnDraw(false);
                                    }

                                }).always(function () {

                                });
                            }
                        }
                    });
                }
            }
            event.stopPropagation();
        })
    });
    //act-delete danh bạ 
    $(document).on("click", "a.act-deleteDanhBa", function (event) {
        var $tagData = getRoleData($(this));
        var $tagTable = getRoleTable($(this));
        var datapost = $(this).data();
        if (datapost == undefined || datapost == 'undefined') {
            datapost = {}
        }
        //$.extend(datapost, $(this).data());
        //file title của bản ghi.
        var $Title = $(this).parent().parent().find('.act-view').first().text();
        BootstrapDialog.confirm({
            title: 'Xác nhận xóa',
            message: "Bạn có chắc chắn xóa bản ghi này?",
            type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
            closable: true, // <-- Default value is false
            draggable: true, // <-- Default value is false
            btnCancelLabel: 'Hủy', // <-- Default value is 'Cancel',
            btnOKLabel: 'Đồng ý!', // <-- Default value is 'OK',
            btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
            callback: function (result) {
                // result will be true if button was click, while it will be false if users close the dialog directly.
                if (result) {
                    $.post($tagData.data("action"), datapost).done(function (result) {
                        if (result.State == 2) {
                            BootstrapDialog.show({
                                title: "Lỗi",
                                message: result.Message
                            });
                        }
                        else {
                            showMsg(result.Message);
                            //$tagTable.dataTable().fnDraw(false);
                            loadAjaxContent($tagData.data("list"), "#tblDanhBa", { ItemID: result.ID });
                        }

                    }).always(function () {

                    });
                }
            }
        });
        event.stopPropagation();
    });
    $(document).on("click", "button.act-search", function (event) {
        var $tagData = getRoleData($(this));
        var $idtable = $tagData.find("table.dataTable").first().dataTable().fnDraw(false);
        //alert($idtable);
    });
    // Add remove loading class on body element based on Ajax request status
    $(document).on({
        ajaxStart: function () {
            $("body").addClass("loading");
        },
        ajaxStop: function () {
            $("body").removeClass("loading");
        }
    });

});