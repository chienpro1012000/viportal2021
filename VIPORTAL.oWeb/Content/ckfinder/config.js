/*
 Copyright (c) 2007-2019, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.html or https://ckeditor.com/sales/license/ckfinder
 */

var config = {
    connectorPath: '/VIPortalAPI/api/Ckfinder/connector',
    language : 'en'
};

// Set your configuration options below.

// Examples:
// config.language = 'pl';
// config.skin = 'jquery-mobile';
console.log('load config');
CKFinder.define(config);
