/**
 * @license Copyright (c) 2003-2021, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
    var ck_defaultImageUrl = '/ckfinder/ckfinder.html';
    config.language = 'vi';
    config.filebrowserUploadUrl = '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        config.filebrowserImageBrowseUrl = ck_defaultImageUrl;
    config.filebrowserFlashBrowseUrl = '/ckfinder/ckfinder.html';
    config.allowedContent = true;
    config.filebrowserBrowseUrl = ck_defaultImageUrl;
    config.filebrowserVideoBrowseUrl = ck_defaultImageUrl;
    config.enterMode = CKEDITOR.ENTER_BR;
    config.extraPlugins = 'video';
};
