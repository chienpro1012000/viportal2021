﻿/*
 * jQuery File Upload Plugin JS Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */
/* global $, window */
//thay đổi template vị trí hiển thị ở đây
var fileuploadHTML = '<div class="fileupload-buttonbar">' +
                        '<span class="btn btn-success fileinput-button">' +
                            '<i class="glyphicon glyphicon-plus"></i>' +
                            '<span class="control-name">Thêm tệp</span>' +
                        '</span>' +
                        '<input type="hidden" class="value-fileattach" />' +
                        '<input type="hidden" class="value-fileattach-remove" />' +
                        '<table role="presentation" class="uploadfiles"></table>' +
    '</div>';
var fileuploadHTMLV2 = '<div class="fileupload-buttonbar">' +
                        '<span class="fileinput-button">' +
                            '<i class="glyphicon glyphicon-plus"></i>' +
                            '<img src="/Content/themeV1/icon/image-social.jpg" alt="">' +
                        '</span>' +
                        '<input type="hidden" class="value-fileattach" />' +
                        '<input type="hidden" class="value-fileattach-remove" />' +
                        '<table role="presentation" class="uploadfiles"></table>' +
    '</div>';

function findAndRemoveJSON(fileJSON, key, value) {
    var y = jQuery.grep(fileJSON, function (item) {
        return item[key] != value;
    });
    return y;
}
function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
$(function () {
    'use strict';
    // Initialize the jQuery File Upload widget:

    $.fn.regFileUpload = function (options) {
        var defaults = {
            files: '',
            ValueHidden: true,
            url: '/UserControls/Common/hUploadFile.asp',
            autoUpload: true,
            maxFileSize: 20,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png|doc?x|xls?x|ppt?x|pdf|rar|bak|zip|mp4|xls|xlsx|doc|docx)$/i,
            preview: false,
            formData: {},
            limitMultiFileUploads: undefined,
            maxNumberOfFiles: 20000,
            OnlyOne: false,
            dataBound: function ($row) {
                //Hàm sau khi load xong
                return $row;
            },
        }
        var oSettings = $.extend(defaults, options);
        if (typeof oSettings.files == 'string' && oSettings.files.length > 0)
            oSettings.files = JSON.parse(oSettings.files);
        oSettings.maxFileSize *= 50000000;
        return this.each(function () {
            var $fileUploadTag = $(this);
            var $boxFileUpload = $(fileuploadHTML);
            var valueName = $(this).attr('value');
            if (valueName != "" && valueName != 'undefined' && valueName != null)
                $boxFileUpload.find("span.control-name").html(valueName);
            var idFileUpload = $fileUploadTag.attr("id");
            var $listFile = $boxFileUpload.find("input.value-fileattach");
            var $listFileRemove = $boxFileUpload.find("input.value-fileattach-remove");
            $listFile.attr("name", "listValue" + idFileUpload).attr("id", "listValue" + idFileUpload);
            $listFileRemove.attr("name", "listValueRemove" + idFileUpload).attr("id", "listValueRemove" + idFileUpload);
            $boxFileUpload.insertAfter($fileUploadTag);
            var $spanUpload = $boxFileUpload.find("> span");
            $fileUploadTag.detach().prependTo($spanUpload);
            var $containers = $boxFileUpload.find("> table.uploadfiles");
            // Add files old
            console.log(oSettings.files);
            if (oSettings.files.length > 0) {
                $.each(oSettings.files, function (index, value) {
                    var row = $('<tr class="template-download fade in">' +
                          '<td class="box-upload-button"><button type="button" class="btn btn-danger btn-xs delete">Xóa</button></td>' +
                             (oSettings.preview ? '<td class="col-preview"><span class="preview"></span></td>' : '') +
                           '<td><p class="name"></p></td>' +
                        
                           //'<td class="box-upload-size"><span class="size"></span></td>' +
                         
                           '</tr>');
                    row.data("filesv", value.Name);
                    row.find(".name").append('<a href="' + value.Url + '" >' + value.Name + '</a>');
                    if (oSettings.preview) {
                        row.find('.preview').append($('<img class="preview-img"/>').attr("src", value.Url));
                    }
                    row.find("button.delete").click(function () {
                        var valueListFileRemove = $listFileRemove.attr('value');
                        if (valueListFileRemove != '' && valueListFileRemove != null) {
                            valueListFileRemove += '#' + $(this).closest("tr").data("filesv");
                        }
                        else
                            valueListFileRemove = $(this).closest("tr").data("filesv");
                        $listFileRemove.attr("value", valueListFileRemove);
                        $(this).closest("tr").remove();
                    });
                    $containers.append(row);
                });
            }
            // Initialize the jQuery File Upload widget:
            $fileUploadTag.fileupload(
                {
                    url: oSettings.url,
                    limitMultiFileUploads: oSettings.limitMultiFileUploads,
                    maxNumberOfFiles: oSettings.maxNumberOfFiles,
                    autoUpload: oSettings.autoUpload,
                    maxFileSize: oSettings.maxFileSize,
                    acceptFileTypes: oSettings.acceptFileTypes,
                    filesContainer: $containers,
                    formData: oSettings.formData,
                    uploadTemplateId: null,
                    downloadTemplateId: null,
                    uploadTemplate: function (o) {
                        var rows = $();
                        $.each(o.files, function (index, file) {
                            var row = $('<tr class="template-upload">' +
                                 '<td class="box-upload-button fileupload-buttonbar">' +
                                (!index && !o.options.autoUpload ?
                                    '<button type="button" class="btn btn-success btn-xs start" disabled>Upload</button>' : '') +
                                (!index ? '<button type="button" class="btn btn-danger btn-xs cancel">Hủy</button>' : '') +
                                '</td>' +
                                '<td class="tdname"><p class="name"></p>' +
                                    '<div class="error"></div>' +
                                '</td>' +
                                '<td class="box-upload-progress">' +
                                    '<div class="progress"><div class="progress-bar progress-bar-striped active" style="width:0%;"><span>0%</span></div></div>' +
                                '</td>' +
                                '<td class="box-upload-size"><p class="size"></p></td>' +
                               
                                '</tr>');
                            row.find('.name').text(file.name);
                            row.find('.size').text(o.formatFileSize(file.size));
                            if (file.error) {
                                row.find('.error').text(file.error);
                                row.find('.box-upload-progress').remove();
                                row.find('.box-upload-size').remove();
                            }
                            console.log(row);
                            rows = rows.add(row);
                        });
                        return rows;
                    },
                    downloadTemplate: function (o) {
                        if (oSettings.ValueHidden) {
                            if (o.files.length > 0 && oSettings.OnlyOne) {

                            }
                            var listFileValue = [];
                            var valueListFile = $listFile.attr('value');
                            if (valueListFile != '' && valueListFile != null) {
                                listFileValue = JSON.parse(valueListFile);
                            }

                            var rows = $();
                            console.log(o);
                            $.each(o.files, function (index, file) {
                                var row = $('<tr class="template-download">' +
                                    '<td class="box-upload-button fileupload-buttonbar"><button type="button" class="btn btn-danger btn-xs delete"><i class="fa fa-times" aria-hidden="true"></i></button></td>' +
                                    (oSettings.preview ? '<td class="col-preview"><span class="preview"></span></td>' : '') +
                                    '<td><p class="name"></p>' +
                                    (file.error ? '<div class="error"></div>' : '') +
                                    '</td>' +
                                    '</tr>');
                                //row.find('.size').text(o.formatFileSize(file.size));
                                if (file.error) {
                                    row.find('.name').text(file.name);
                                    row.find('.error').text(file.error);
                                } else {
                                    var fileAttach = { FileServer: file.pathFile, FileName: file.name };
                                    listFileValue.push(fileAttach);
                                    if (oSettings.preview) {
                                        row.find('.preview').append($('<img class="preview-img" />').attr("src", file.url));
                                    }
                                    row.data("filesv", file.pathFile);
                                    row.find('.name').append($('<a></a>').text(file.name));
                                    row.find('a')
                                        .attr('data-gallery', '')
                                        .prop('href', file.url);
                                    row.find('button.delete')
                                        .attr('data-type', file.delete_type)
                                        .attr('data-url', file.delete_url)
                                        .click(function () {
                                            console.log(file);
                                            var valueListFileRemove = $listFile.attr('value');
                                            if (valueListFileRemove != '' && valueListFileRemove != null) {
                                                var fileJSON = JSON.parse(valueListFileRemove);
                                                fileJSON = findAndRemoveJSON(fileJSON, "FileServer", file.pathFile);
                                                $listFile.attr('value', JSON.stringify(fileJSON));
                                            }
                                        });
                                }
                              
                                if (jQuery.isFunction(oSettings.dataBound)) {
                                    oSettings.dataBound(row, file); //
                                }
                                console.log(row.html());
                                rows = rows.add(row);
                            });

                            $listFile.attr('value', JSON.stringify(listFileValue));

                            return rows;
                        } else return;
                    },
                    change: function (e, data) {
                        if (data.files.length > 0 && oSettings.OnlyOne) {
                            $listFile.attr('value', '[]');
                            $containers.html('');
                        }
                    }
                });

            $.ajax({
                url: $fileUploadTag.fileupload('option', 'url'),
                dataType: 'json',
                context: $fileUploadTag[0]
            }).always(function () {
                $(this).removeClass('fileupload-processing');
            }).done(function (result) {
                $(this).fileupload('option', 'done')
                    .call(this, $.Event('done'), { result: result });
            });
            //
        })
    };
    $.fn.regFileUploadV2 = function (options) {
        var defaults = {
            files: '',
            url: '/UserControls/Common/hUploadFile.ashx',
            autoUpload: true,
            ValueHidden: true,
            maxFileSize: 20,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png|doc?x|xls?x|ppt?x|pdf|rar|bak|zip|mp4)$/i,
            preview: false,
            formData: {},
            limitMultiFileUploads: undefined,
            maxNumberOfFiles: 20000,
            OnlyOne: false
        }
        var oSettings = $.extend(defaults, options);
        if (typeof oSettings.files == 'string' && oSettings.files.length > 0)
            oSettings.files = JSON.parse(oSettings.files);
        oSettings.maxFileSize *= 1000000;
        return this.each(function () {
            var $fileUploadTag = $(this);
            var $boxFileUpload = $(fileuploadHTMLV2);
            var valueName = $(this).attr('value');
            if (valueName != "" && valueName != 'undefined' && valueName != null)
                $boxFileUpload.find("span.control-name").html(valueName);
            var idFileUpload = $fileUploadTag.attr("id");
            var $listFile = $boxFileUpload.find("input.value-fileattach");
            var $listFileRemove = $boxFileUpload.find("input.value-fileattach-remove");
            $listFile.attr("name", "listValue" + idFileUpload).attr("id", "listValue" + idFileUpload);
            $listFileRemove.attr("name", "listValueRemove" + idFileUpload).attr("id", "listValueRemove" + idFileUpload);
            $boxFileUpload.insertAfter($fileUploadTag);
            var $spanUpload = $boxFileUpload.find("> span");
            $fileUploadTag.detach().prependTo($spanUpload);
            var $containers = $boxFileUpload.find("> table.uploadfiles");
            // Add files old
            console.log(oSettings.files);
            if (oSettings.files.length > 0) {
                $.each(oSettings.files, function (index, value) {
                    
                    var row = $('<tr class="template-download fade in">' +
                        '<td><p class="name"></p></td>' + '</tr>');
                    row.data("filesv", value.Name);
                    row.find(".name").append('<img src="' + value.Url + '" />');
                    if (oSettings.preview) {
                        row.find('.preview').append($('<img class="preview-img"/>').attr("src", value.Url));
                    }
                    row.find("button.delete").click(function () {
                        var valueListFileRemove = $listFileRemove.attr('value');
                        if (valueListFileRemove != '' && valueListFileRemove != null) {
                            valueListFileRemove += ',' + $(this).closest("tr").data("filesv");
                        }
                        else
                            valueListFileRemove = $(this).closest("tr").data("filesv");
                        $listFileRemove.attr("value", valueListFileRemove);
                        $(this).closest("tr").remove();
                    });
                    $containers.append(row);
                });
            }
            // Initialize the jQuery File Upload widget:
            $fileUploadTag.fileupload(
                {
                    url: oSettings.url,
                    limitMultiFileUploads: oSettings.limitMultiFileUploads,
                    maxNumberOfFiles: oSettings.maxNumberOfFiles,
                    autoUpload: oSettings.autoUpload,
                    maxFileSize: oSettings.maxFileSize,
                    acceptFileTypes: oSettings.acceptFileTypes,
                    filesContainer: $containers,
                    formData: oSettings.formData,
                    uploadTemplateId: null,
                    downloadTemplateId: null,
                    uploadTemplate: function (o) {
                        var rows = $();
                        $.each(o.files, function (index, file) {
                            var row = $('<tr class="template-upload">' +
                                '<td class="box-upload-button fileupload-buttonbar">' +
                                (!index && !o.options.autoUpload ?
                                    '<button type="button" class="btn btn-success btn-xs start" disabled>Upload</button>' : '') +
                                (!index ? '<button type="button" class="btn btn-danger btn-xs cancel">Hủy</button>' : '') +
                                '</td>' +
                                '<td class="tdname"><p class="name"></p>' +
                                '<div class="error"></div>' +
                                '</td>' +
                                '<td class="box-upload-progress">' +
                                '<div class="progress"><div class="progress-bar progress-bar-striped active" style="width:0%;"><span>0%</span></div></div>' +
                                '</td>' +
                                '<td class="box-upload-size"><p class="size"></p></td>' +

                                '</tr>');
                            row.find('.name').text(file.name);
                            row.find('.size').text(o.formatFileSize(file.size));
                            if (file.error) {
                                row.find('.error').text(file.error);
                                row.find('.box-upload-progress').remove();
                                row.find('.box-upload-size').remove();
                            }
                            console.log(row);
                            rows = rows.add(row);
                        });
                        return rows;
                    },
                    downloadTemplate: function (o) {
                        if (oSettings.ValueHidden) {
                            if (o.files.length > 0 && oSettings.OnlyOne) {

                            }
                            var listFileValue = [];
                            var valueListFile = $listFile.attr('value');
                            if (valueListFile != '' && valueListFile != null) {
                                listFileValue = JSON.parse(valueListFile);
                            }

                            var rows = $();
                            $.each(o.files, function (index, file) {
                                var row = $('<td><p class="name"></p>' +
                                    (file.error ? '<div class="error"></div>' : '') + '</td>' +
                                    '</tr>');
                                //row.find('.size').text(o.formatFileSize(file.size));
                                if (file.error) {
                                    row.find('.name').text(file.name);
                                    row.find('.error').text(file.error);
                                } else {
                                    var fileAttach = { FileServer: file.pathFile, FileName: file.name };
                                    listFileValue.push(fileAttach);
                                    if (oSettings.preview) {
                                        row.find('.preview').append($('<img class="preview-img" />').attr("src", file.url));
                                    }
                                    row.data("filesv", file.pathFile);
                                    row.find('.name').append('<img src="' + file.url + '" style="width: 100%" />');
                                    row.find('a')
                                        .attr('data-gallery', '')
                                        .prop('href', file.url);
                                    row.find('button.delete')
                                        .attr('data-type', file.delete_type)
                                        .attr('data-url', file.delete_url)
                                        .click(function () {
                                            console.log(file);
                                            var valueListFileRemove = $listFile.attr('value');
                                            if (valueListFileRemove != '' && valueListFileRemove != null) {
                                                var fileJSON = JSON.parse(valueListFileRemove);
                                                fileJSON = findAndRemoveJSON(fileJSON, "FileServer", file.pathFile);
                                                $listFile.attr('value', JSON.stringify(fileJSON));
                                            }
                                        });
                                }
                                console.log(row.html());
                                rows = rows.add(row);
                            });

                            $listFile.attr('value', JSON.stringify(listFileValue));

                            return rows;
                        } else return;
                    },
                    change: function (e, data) {
                        if (data.files.length > 0 && oSettings.OnlyOne) {
                            $listFile.attr('value', '[]');
                            $containers.html('');
                        }
                    }
                });

            $.ajax({
                url: $fileUploadTag.fileupload('option', 'url'),
                dataType: 'json',
                context: $fileUploadTag[0]
            }).always(function () {
                $(this).removeClass('fileupload-processing');
            }).done(function (result) {
                $(this).fileupload('option', 'done')
                    .call(this, $.Event('done'), { result: result });
            });
            //
        })
    };
});
