var page = window.location.pathname.split('/').slice(-1).join('');
window.onload = function() {
    $('#items_sliderbar li').removeClass('active');
    switch (page) {
        case "index.html":
            $('#items_sliderbar').find('.index').addClass('active');
            break;
        case "news.html":
            $('#items_sliderbar').find('.news').addClass('active');
            break;
        case "meeting.html":
            $('#items_sliderbar').find('.meeting').addClass('active');
            break;
        case "lib-media.html":
            $('#items_sliderbar').find('.lib-media').addClass('active');
            break;
        case "video-detail.html":
            $('#items_sliderbar').find('.video-detail').addClass('active');
            break;
        case "askreplyFAQ.html":
            $('#items_sliderbar').find('.askreplyFAQ').addClass('active');
            break;
        case "voted.html":
            $('#items_sliderbar').find('.vote').addClass('active');
            break;
        case "document.html":
            $('#items_sliderbar').find('.documents').addClass('active');
            break;
        case "announcement.html":
            $('#items_sliderbar').find('.announcement').addClass('active');
            break;
        case "regulation.html":
            $('#items_sliderbar').find('.regulation').addClass('active');
            break;
        case "phonebook.html":
            $('#items_sliderbar').find('.phonebook').addClass('active');
            break;
        case "forum.html":
            $('#items_sliderbar').find('.forum').addClass('active');
            break;
        case "socialnetwork.html":
            $('#items_sliderbar').find('.socialnetwork').addClass('active');
            break;
        case "birtday.html":
            $('#items_sliderbar').find('.birtday').addClass('active');
            break;
        default:
            break;

    }
}