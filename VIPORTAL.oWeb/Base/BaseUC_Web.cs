﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using ViPortal_Utils;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.DanhSachNgonNgu;
using ViPortalData.LVaiTro;

namespace VIPORTAL.oWeb
{
    public class BaseUC_Web : UserControl
    {
        /// <summary>
        /// Dường dẫn full list
        /// </summary>
        public string UrlList { get; set; }
        public string UrlControls { get; set; }
        public string UrlSite { get; set; }
        public string TitleWebpart { get; set; }
        public string Config { get; set; }
        public int RowsLimit { get; set; }
        public bool isNotLogin { get; set; }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }
        public LUserJson FuncLogin()
        {
            string userName = HttpContext.Current.User.Identity.Name;
            //0#.w|udtt\hoannt
            if (!string.IsNullOrEmpty(userName))
            {
                if (userName.Contains("|"))
                {
                    userName = userName.Split('|').LastOrDefault();
                }
                //0#.w|simax\maianh
                if (userName.Contains("\\"))
                {
                    userName = userName.Split('\\')[1];
                }
            }

            LUserJson oLUserJson = new LUserJson();
            if (Session["ObjectUser"] != null)
            {
                oLUserJson = (LUserJson)Session["ObjectUser"];
                if (oLUserJson.TaiKhoanTruyCap != userName || oLUserJson.CurSiteAction != UrlSite) //nếu đổi tên truy cập hoặc thay đổi site làm việc thì sẽ phải tính toán lại thông tin người dùng.
                {
                    Session["ObjectUser"] = null;
                    oLUserJson = new LUserJson();
                    //return FuncLogin();
                }
            }

            if(oLUserJson.ID == 0) //neeus chua lay duoc thong tin sesssion thi nhay vao day.
            {
                #region Lấy dữ liệu json
                //LUserDA oLUserDA = new LUserDA();

                var tempuser = olUserDA.GetListJson(new LUserQuery()
                {
                    TaiKhoanTruyCap = userName
                });
                if (tempuser.Count > 0)
                {
                    oLUserJson = tempuser[0];
                    if (oLUserJson.CurSiteAction != null)
                    {
                        if (oLUserJson.CurSiteAction != UrlSite && UrlSite != "/en")
                        {
                            //olUserDA.SystemUpdateOneField(oLUserJson.ID, "CurSiteAction", UrlSite);
                            //redirect về trang đúng của nó.
                            if (!string.IsNullOrEmpty(UrlSite))
                            {

                                string PathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
                                PathAndQuery = PathAndQuery.Replace(UrlSite + "/", oLUserJson.CurSiteAction + "/");
                                this.Response.Redirect(PathAndQuery);
                                return oLUserJson;

                            }
                            else
                            {
                                string PathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
                                if (!string.IsNullOrEmpty(oLUserJson.CurSiteAction))
                                {
                                    PathAndQuery = oLUserJson.CurSiteAction + PathAndQuery;
                                    this.Response.Redirect(PathAndQuery);
                                    return oLUserJson;
                                }
                            }
                        }
                        else
                        {

                        }
                    }
                    if (UrlSite != "/en")
                    {
                        //caafn up.
                        olUserDA.SystemUpdateOneField(oLUserJson.ID, "CurSiteAction", UrlSite);
                        oLUserJson.CurSiteAction = UrlSite;
                    }
                    //update laij usser voi siteaction moi.

                    LGroupDA lGroupDA = new LGroupDA();
                    var temp = lGroupDA.GetListJson(new LGroupQuery() { UrlSite = UrlSite, GroupIsDonVi = 1 }).FirstOrDefault();
                    if (temp != null)
                        CurFldGroup = temp.ID;

                    //chỗ này check lại chỗ này.
                    if (CurFldGroup > 0)
                    {
                        if (!string.IsNullOrEmpty(oLUserJson.PerJson))
                        {
                            List<PermisonGroup> permisonGroups = new List<PermisonGroup>();
                            permisonGroups = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PermisonGroup>>(oLUserJson.PerJson);

                            PermisonGroup oPermisonGroup = permisonGroups.FirstOrDefault(x => x.fldGroup == CurFldGroup);
                            if (oPermisonGroup == null) oPermisonGroup = new PermisonGroup();
                            oLUserJson.Roles = oPermisonGroup.Roles == null ? new List<ViPortal_Utils.Base.LookupData>() : oPermisonGroup.Roles;
                            oLUserJson.Permissions = oPermisonGroup.Permissions == null ? new List<ViPortal_Utils.Base.LookupData>() : oPermisonGroup.Permissions;

                        }
                    }

                    if (oLUserJson.Roles != null && oLUserJson.Roles.Count > 0)
                    {
                        LVaiTroDA oLVaiTroDA = new LVaiTroDA();
                        List<LVaiTroJson> lstVaiTro = oLVaiTroDA.GetListJson(new LVaiTroQuery() { lstIDget = oLUserJson.Roles.Select(x => x.ID).ToList(), isGetBylistID = true });
                        foreach (var item in lstVaiTro)
                        {
                            oLUserJson.Permissions.AddRange(item.Permission);
                        }
                    }
                    oLUserJson.Permissions = oLUserJson.Permissions.DistinctBy(x => x.ID).ToList();
                    LPermissionDA lPermissionDA = new LPermissionDA();
                    List<LPermissionJson> lstPermission = lPermissionDA.GetListJson(new LPermissionQuery() { lstIDget = oLUserJson.Permissions.Select(x => x.ID).ToList(), isGetBylistID = true });
                    oLUserJson.PermissionsQuery = "|" + string.Join("|", lstPermission.Select(x => x.MaQuyen)) + "|";

                    // LGroupDA lGroupDA = new LGroupDA();
                    LGroupItem itemGroup = lGroupDA.GetByIdToObject<LGroupItem>(oLUserJson.Groups.ID);
                    oLUserJson.UrlSite = itemGroup.UrlSite;
                    Session["ObjectUser"] = oLUserJson;
                }
                #endregion
            }
            return oLUserJson;
        }
        public BaseUC_Web()
        {
            this.Load += new EventHandler(this.Page_Load);
        }
        private LUserDA olUserDA { get; set; }
        private void Page_Load(object sender, EventArgs e)
        {
            if (!isNotLogin)
            {
                olUserDA = new LUserDA();

                ConfigLanguages = new Dictionary<string, string>();
                oAbsolutePath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
                //System.IO.FileInfo info = new System.IO.FileInfo(oAbsolutePath);
                PageName = Path.GetFileNameWithoutExtension(oAbsolutePath);

                if (!string.IsNullOrEmpty(Request["ItemID"]))
                {
                    ItemID = Convert.ToInt32(Request["ItemID"]);
                }
                UrlSite = GetCurentSite();
                #region MyRegion
                //try
                //{
                //    string userName = HttpContext.Current.User.Identity.Name;
                //    //0#.w|udtt\hoannt
                //    if (!string.IsNullOrEmpty(userName))
                //    {
                //        if (userName.Contains("|"))
                //        {
                //            userName = userName.Split('|').LastOrDefault();
                //        }
                //        //0#.w|simax\maianh
                //        if (userName.Contains("\\"))
                //        {
                //            userName = userName.Split('\\')[1];
                //        }
                //    }
                //    if (Response.Cookies["CurSiteAction" + userName] != null)
                //    {

                //        //string CurSiteAction = Response.Cookies["CurSiteAction" + userName].Value;
                //        if (Request.Cookies["CurSiteAction" + userName] != null)
                //        {
                //            string CurSiteAction = Request.Cookies["CurSiteAction" + userName].Value;
                //            if (CurSiteAction != null && CurSiteAction != UrlSite)
                //            {
                //                //redirect về trang đúng của nó.
                //                if (!string.IsNullOrEmpty(UrlSite))
                //                {
                //                    string PathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
                //                    PathAndQuery = PathAndQuery.Replace(UrlSite + "/", CurSiteAction + "/");
                //                    this.Response.Redirect(PathAndQuery);

                //                }
                //                else
                //                {
                //                    string PathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
                //                    if (!string.IsNullOrEmpty(CurSiteAction))
                //                    {
                //                        PathAndQuery = CurSiteAction + PathAndQuery;
                //                        this.Response.Redirect(PathAndQuery);
                //                    }
                //                }
                //            }
                //        }
                //    }
                //}
                //catch (Exception ex)
                //{

                //} 
                #endregion

                CurentUser = FuncLogin();


                CheckConfig();
                SimpleLogger oSimpleLogger = new SimpleLogger();
                oSimpleLogger.Info(UrlControls);
            }
        }

        public int IDNgonNgu { get; set; }
        public string HtmlOutput { get; set; }
        public Dictionary<string, string> ConfigLanguages { get; set; }
        public void CheckConfig()
        {
            //SimpleLogger oSimpleLogger = new SimpleLogger();

            DanhSachNgonNguDA oDanhSachNgonNguDA = new DanhSachNgonNguDA();
            ModuleChucNangDA oModuleChucNangDA = new ModuleChucNangDA();
            string _UrlSite = UrlSite;
            if (string.IsNullOrEmpty(UrlSite))
                _UrlSite = "/";

            ModuleChucNangJson oModuleChucNangJson = oModuleChucNangDA.GetListJson(new ModuleChucNangQuery() { UrlControl = UrlControls }).FirstOrDefault();
            DanhSachNgonNguJson oDanhSachNgonNgu = oDanhSachNgonNguDA.GetListJson(new DanhSachNgonNguQuery() { URLWeb = _UrlSite }).FirstOrDefault();



            if (oModuleChucNangJson != null)
            {
                //fix check thông tin Giao diện layout.
                LMauGiaoDienDA lMauGiaoDienDA = new LMauGiaoDienDA();
                var LMauGiaoDienItem = lMauGiaoDienDA.GetListJson(new LMauGiaoDienQuery() { Length = 1, _ModerationStatus = 1, Module = oModuleChucNangJson.ID }).FirstOrDefault();
                if (LMauGiaoDienItem != null && !string.IsNullOrEmpty(LMauGiaoDienItem.TemplateHtml))
                    HtmlOutput = LMauGiaoDienItem.TemplateHtml;


                if (oDanhSachNgonNgu != null && !string.IsNullOrEmpty(oDanhSachNgonNgu.Language))
                {
                    if (JObject.Parse(oDanhSachNgonNgu.Language).ContainsKey(oModuleChucNangJson.Title))
                    {
                        //dynamic data = JObject.Parse(oDanhSachNgonNgu.Language);
                        string datalang = JObject.Parse(oDanhSachNgonNgu.Language)[oModuleChucNangJson.Title].ToString();
                        string datalangUtilsCommon = JObject.Parse(oDanhSachNgonNgu.Language)["UtilsCommon"].ToString();
                        if (!string.IsNullOrEmpty(datalangUtilsCommon))
                        {
                            ConfigLanguages = JsonConvert.DeserializeObject<Dictionary<string, string>>(datalangUtilsCommon);
                        }
                        ConfigLanguages.AddRangeOverride(JsonConvert.DeserializeObject<Dictionary<string, string>>(datalang));

                    }
                    else
                    {

                    }

                }
                List<ConfigNgonngu> lstConfig = new List<ConfigNgonngu>();
                if (!string.IsNullOrEmpty(oModuleChucNangJson.ConfigNgonNgu))
                {
                    lstConfig = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ConfigNgonngu>>(oModuleChucNangJson.ConfigNgonNgu);
                }
                if (lstConfig.Count > 0)
                {
                    //oSimpleLogger.Info(_UrlSite);

                    if (oDanhSachNgonNgu != null)
                    {
                        IDNgonNgu = oDanhSachNgonNgu.ID;
                        //oSimpleLogger.Info(IDNgonNgu + "");
                        ConfigNgonngu oConfigNgonngu = lstConfig.FirstOrDefault(x => x.IDNgonNgu == IDNgonNgu);
                        if (oConfigNgonngu != null)
                            ConfigLanguages = oConfigNgonngu.ConfigKeyValue;
                    }
                    else
                    {
                        //default trang tienegs vieetj.
                        oDanhSachNgonNgu = oDanhSachNgonNguDA.GetListJson(new DanhSachNgonNguQuery() { URLWeb = "/" }).FirstOrDefault();
                        if (oDanhSachNgonNgu != null)
                        {
                            IDNgonNgu = oDanhSachNgonNgu.ID;
                            ConfigNgonngu oConfigNgonngu = lstConfig.FirstOrDefault(x => x.IDNgonNgu == IDNgonNgu);
                            ConfigLanguages = oConfigNgonngu.ConfigKeyValue;
                        }
                    }
                }


            }
            //oSimpleLogger.Info(Newtonsoft.Json.JsonConvert.SerializeObject(ConfigLanguages));
        }

        public string GetCurentSite()
        {
            try
            {
                string siteTemp = string.Empty;
                //if (HttpContext.Current.Request.UrlReferrer != null)
                //    siteTemp = HttpContext.Current.Request.UrlReferrer.ToString();
                //else
                siteTemp = HttpContext.Current.Request.Url.AbsoluteUri;
                string site = siteTemp.Split('/')[3];
                if (site.Equals("noidung") || site.Equals("cms") || site.Equals("Pages"))
                    return "";
                else
                    return "/" + site;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string oAbsolutePath { get; set; }

        public string PageName { get; set; }

        public int ItemID { get; set; }
        public LUserJson CurentUser { get; private set; }
        public int CurFldGroup { get; private set; }
    }
}