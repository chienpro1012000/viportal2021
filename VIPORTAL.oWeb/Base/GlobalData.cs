﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIPortalAPP;

namespace VIPORTAL.oWeb.Base
{
    public static class GlobalData
    {
        private static List<DMTrangThaiJson> _lstTrangThai { get; set; }

        public static List<DMTrangThaiJson> LstTrangThai
        {
            get
            {
                if (_lstTrangThai == null)
                {
                    //set cho thằng này.
                    DMTrangThaiDA oDamTrangThaiDA = new DMTrangThaiDA();
                    _lstTrangThai = oDamTrangThaiDA.GetListJson(new DMTrangThaiQuery());
                    return _lstTrangThai;
                }
                else return _lstTrangThai;
            }
        }
    }
}