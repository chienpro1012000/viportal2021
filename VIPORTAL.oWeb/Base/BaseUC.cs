﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using ViPortal_Utils;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.LVaiTro;

namespace VIPORTAL.oWeb
{
    public class BaseUC : System.Web.UI.UserControl
    {
        public LUserJson CurentUser { get; set; }
        /// <summary>
        /// Dường dẫn full list
        /// </summary>
        public string UrlList { get; set; }
        public string UrlSite { get; set; }
        public int CurFldGroup { get; set; }
        public int ItemID { get; set; }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        public BaseUC()
        {
            this.Load += new EventHandler(this.Page_Load);
        }

        private void Page_Load(object sender, EventArgs e)
        {
            oAbsolutePath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
            //System.IO.FileInfo info = new System.IO.FileInfo(oAbsolutePath);
            PageName = Path.GetFileNameWithoutExtension(oAbsolutePath);
            UrlSite = GetCurentSite();
            //Groupid lấy thông tin người dùng.

            CurentUser = FuncLogin();

            if (!string.IsNullOrEmpty(Request["ItemID"]))
            {
                ItemID = Convert.ToInt32(Request["ItemID"]);
            }
            if (!string.IsNullOrEmpty(Request["UrlList"]))
            {
                UrlList = Convert.ToString(Request["UrlList"]);
            }
            //check quyền trên menu.

        }

        public LUserJson FuncLogin()
        {
            string userName = HttpContext.Current.User.Identity.Name;
            //0#.w|udtt\hoannt
            if (!string.IsNullOrEmpty(userName))
            {
                if (userName.Contains("|"))
                {
                    userName = userName.Split('|').LastOrDefault();
                }
                //0#.w|simax\maianh
                if (userName.Contains("\\"))
                {
                    userName = userName.Split('\\')[1];
                }
            }
            LUserJson oLUserJson = new LUserJson();
            string MessageLog = "";
            MessageLog += (userName + "\r\n");
            try
            {
                if (Session["ObjectUser"] != null)
                {
                    oLUserJson = (LUserJson)Session["ObjectUser"];
                    if (oLUserJson.TaiKhoanTruyCap != userName || oLUserJson.CurSiteAction != UrlSite) //nếu đổi tên truy cập hoặc thay đổi site làm việc thì sẽ phải tính toán lại thông tin người dùng.
                    {
                        Session["ObjectUser"] = null;
                        return FuncLogin();
                    }
                }
                else
                {
                    #region Lấy dữ liệu json
                    LUserDA oLUserDA = new LUserDA();

                    var tempuser = oLUserDA.GetListJson(new LUserQuery()
                    {
                        TaiKhoanTruyCap = userName
                    });
                    MessageLog += (Newtonsoft.Json.JsonConvert.SerializeObject(tempuser) + "\r\n");
                    if (tempuser.Count > 0)
                    {
                        oLUserJson = tempuser[0];
                        oLUserJson.CurSiteAction = UrlSite; //site đang thao tác nghiệp vụ
                        LGroupDA lGroupDA = new LGroupDA();
                        var temp = lGroupDA.GetListJson(new LGroupQuery() { UrlSite = UrlSite, GroupIsDonVi = 1 }).FirstOrDefault();
                        if (temp != null)
                            CurFldGroup = temp.ID;
                        MessageLog += (CurFldGroup + "\r\n");
                        //chỗ này check lại chỗ này.
                        if (CurFldGroup > 0)
                        {
                            if (!string.IsNullOrEmpty(oLUserJson.PerJson))
                            {
                                List<PermisonGroup> permisonGroups = new List<PermisonGroup>();
                                permisonGroups = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PermisonGroup>>(oLUserJson.PerJson);

                                PermisonGroup oPermisonGroup = permisonGroups.FirstOrDefault(x => x.fldGroup == CurFldGroup);
                                if (oPermisonGroup == null) oPermisonGroup = new PermisonGroup();
                                oLUserJson.Roles = oPermisonGroup.Roles == null ? new List<ViPortal_Utils.Base.LookupData>() : oPermisonGroup.Roles;
                                oLUserJson.Permissions = oPermisonGroup.Permissions == null ? new List<ViPortal_Utils.Base.LookupData>() : oPermisonGroup.Permissions;

                            }
                        }
                        MessageLog += (Newtonsoft.Json.JsonConvert.SerializeObject(oLUserJson) + "\r\n");
                        if (oLUserJson.Roles != null && oLUserJson.Roles.Count > 0)
                        {
                            LVaiTroDA oLVaiTroDA = new LVaiTroDA();
                            List<LVaiTroJson> lstVaiTro = oLVaiTroDA.GetListJson(new LVaiTroQuery() { lstIDget = oLUserJson.Roles.Select(x => x.ID).ToList(), isGetBylistID = true });
                            foreach (var item in lstVaiTro)
                            {
                                oLUserJson.Permissions.AddRange(item.Permission);
                            }
                        }
                        MessageLog += ("oLUserJson2:" + Newtonsoft.Json.JsonConvert.SerializeObject(oLUserJson) + "\r\n");
                        oLUserJson.Permissions = oLUserJson.Permissions.DistinctBy(x => x.ID).ToList();
                        LPermissionDA lPermissionDA = new LPermissionDA();
                        List<LPermissionJson> lstPermission = lPermissionDA.GetListJson(new LPermissionQuery() { lstIDget = oLUserJson.Permissions.Select(x => x.ID).ToList(), isGetBylistID = true });
                        oLUserJson.PermissionsQuery = "|" + string.Join("|", lstPermission.Select(x => x.MaQuyen)) + "|";


                        //LGroupDA lGroupDA = new LGroupDA();
                        LGroupItem itemGroup = lGroupDA.GetByIdToObject<LGroupItem>(oLUserJson.Groups.ID);
                        oLUserJson.UrlSite = itemGroup.UrlSite;
                        Session["ObjectUser"] = oLUserJson;
                    }
                    else
                    {
                        throw new Exception($"Erro Message: {MessageLog}");
                    }
                    #endregion
                }
            }catch (Exception ex)
            {
                throw new Exception($"Erro Message: {MessageLog}");
            }
            return oLUserJson;
        }
        public static string GetCurentSite()
        {
            try
            {
                string siteTemp = string.Empty;
                if (HttpContext.Current.Request.Url.AbsoluteUri != null)
                    siteTemp = HttpContext.Current.Request.Url.AbsoluteUri;
                else if (HttpContext.Current.Request.UrlReferrer != null)
                    siteTemp = HttpContext.Current.Request.UrlReferrer.ToString();
                string site = siteTemp.Split('/')[3];
                if (site == "login")
                {
                    siteTemp = HttpContext.Current.Request.Url.AbsoluteUri;
                    site = siteTemp.Split('/')[3];
                }
                if (site.Equals("noidung") || site.Equals("cms") || site.Equals("Pages"))
                    return "";
                else
                    return "/" + site;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string oAbsolutePath { get; set; }

        public string PageName { get; set; }
    }
}