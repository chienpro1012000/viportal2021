﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using ViPortal_Utils;
using ViPortalData;
using ViPortalData.LVaiTro;

namespace VIPORTAL.oWeb
{
    public class pFormBase : Page
    {
        public LUserJson CurentUser { get; set; }
        public pFormBase()
        {
            this.Load += new EventHandler(this.Page_Load);
        }
        public LUserJson FuncLogin()
        {
            string userName = HttpContext.Current.User.Identity.Name;
            //0#.w|udtt\hoannt
            if (!string.IsNullOrEmpty(userName))
            {
                if (userName.Contains("|"))
                {
                    userName = userName.Split('|').LastOrDefault();
                }
                //0#.w|simax\maianh
                if (userName.Contains("\\"))
                {
                    userName = userName.Split('\\')[1];
                }
            }
            LUserJson oLUserJson = new LUserJson();
            if (Session["ObjectUser"] != null)
            {
                oLUserJson = (LUserJson)Session["ObjectUser"];
                if (oLUserJson.TaiKhoanTruyCap != userName || oLUserJson.CurSiteAction != UrlSite) //nếu đổi tên truy cập hoặc thay đổi site làm việc thì sẽ phải tính toán lại thông tin người dùng.
                {
                    Session["ObjectUser"] = null;
                    return FuncLogin();
                }
            }
            else
            {
                #region Lấy dữ liệu json
                LUserDA oLUserDA = new LUserDA();
                
                var tempuser = oLUserDA.GetListJson(new LUserQuery()
                {
                    TaiKhoanTruyCap = userName
                });
                if (tempuser.Count > 0)
                {
                    oLUserJson = tempuser[0];
                    oLUserJson.CurSiteAction = UrlSite; //site đang thao tác nghiệp vụ
                    LGroupDA lGroupDA = new LGroupDA();
                    var temp = lGroupDA.GetListJson(new LGroupQuery() { UrlSite = UrlSite, GroupIsDonVi = 1 }).FirstOrDefault();
                    if (temp != null)
                        CurFldGroup = temp.ID;

                    //chỗ này check lại chỗ này.
                    if (CurFldGroup > 0)
                    {
                        if (!string.IsNullOrEmpty(oLUserJson.PerJson))
                        {
                            List<PermisonGroup> permisonGroups = new List<PermisonGroup>();
                            permisonGroups = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PermisonGroup>>(oLUserJson.PerJson);

                            PermisonGroup oPermisonGroup = permisonGroups.FirstOrDefault(x => x.fldGroup == CurFldGroup);
                            if (oPermisonGroup == null) oPermisonGroup = new PermisonGroup();
                            oLUserJson.Roles = oPermisonGroup.Roles;
                            oLUserJson.Permissions = oPermisonGroup.Permissions;

                        }
                    }

                    if (oLUserJson.Roles.Count > 0)
                    {
                        LVaiTroDA oLVaiTroDA = new LVaiTroDA();
                        List<LVaiTroJson> lstVaiTro = oLVaiTroDA.GetListJson(new LVaiTroQuery() { lstIDget = oLUserJson.Roles.Select(x => x.ID).ToList(), isGetBylistID = true });
                        foreach (var item in lstVaiTro)
                        {
                            oLUserJson.Permissions.AddRange(item.Permission);
                        }
                    }
                    oLUserJson.Permissions = oLUserJson.Permissions.DistinctBy(x => x.ID).ToList();
                    LPermissionDA lPermissionDA = new LPermissionDA();
                    List<LPermissionJson> lstPermission = lPermissionDA.GetListJson(new LPermissionQuery() { lstIDget = oLUserJson.Permissions.Select(x => x.ID).ToList(), isGetBylistID = true });
                    oLUserJson.PermissionsQuery = "|" + string.Join("|", lstPermission.Select(x => x.MaQuyen)) + "|";


                    //LGroupDA lGroupDA = new LGroupDA();
                    LGroupItem itemGroup = lGroupDA.GetByIdToObject<LGroupItem>(oLUserJson.Groups.ID);
                    oLUserJson.UrlSite = itemGroup.UrlSite;
                    Session["ObjectUser"] = oLUserJson;
                }
                #endregion
            }
            return oLUserJson;
        }
        private void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request["ItemID"]))
            {
                ItemID = Convert.ToInt32(Request["ItemID"]);
            }
            else ItemID = 0;
            if (!string.IsNullOrEmpty(Request["do"]))
            {
                doAction = Request["do"];
            }
            else
                doAction = "UPDATE";
            if (!string.IsNullOrEmpty(Request["UrlList"]))
                UrlList = Request["UrlList"];
            if (!string.IsNullOrEmpty(Request["UrlSiteFix"]))
            {
                UrlSite = Request["UrlSiteFix"];
            }
            else
                UrlSite = GetCurentSite();
            CurentUser = FuncLogin();
            

        }
        public static string GetCurentSite()
        {
            try
            {
                string siteTemp = string.Empty;
                if (HttpContext.Current.Request.UrlReferrer != null)
                    siteTemp = HttpContext.Current.Request.UrlReferrer.ToString();
                else
                    siteTemp = HttpContext.Current.Request.Url.AbsoluteUri;
                string site = siteTemp.Split('/')[3];
                if (site.Equals("noidung") || site.Equals("quantri") || site.Equals("cms") || site.Equals("Pages"))
                    return "";
                else
                    return "/" + site;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int ItemID { get; set; }

        public string doAction { get; set; }

        public string UrlList { get; set; }
        public string UrlSite { get; set; }
        public int CurFldGroup { get; private set; }
    }
}
