﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.SessionState;
using ViPortal_Utils;
using ViPortalData;
using ViPortalData.Lconfig;
using ViPortalData.LogHeThong;
using ViPortalData.LVaiTro;

namespace VIPORTAL
{
    public class hActionBase : IRequiresSessionState
    {
        public LUserJson CurentUser { get; set; }
        public string UrlSite { get; set; }
        LogHeThongDA oLogHeThongDA { get; set; }
        LconfigDA oLconfigDA { get; set; }
        public int CurFldGroup { get; set; }

        public hActionBase()
        {

        }

        public void ProcessRequestBase(HttpContext context)
        {
            if (context.Request["UrlSiteFix"] != null)
                UrlSite = context.Request["UrlSiteFix"];
            else
                UrlSite = GetCurentSite();
            //LGroupDA lGroupDA = new LGroupDA();
            //var temp = lGroupDA.GetListJson(new LGroupQuery() { UrlSite = UrlSite, GroupIsDonVi = 1 }).FirstOrDefault();
            //if (temp != null)
            //    CurFldGroup = temp.ID;


            LUserJson oLUserJson = new LUserJson();
            if (context.Session["ObjectUser"] != null)
            {
                CurentUser = (LUserJson)context.Session["ObjectUser"];
            }
            else
            {


                #region Lấy dữ liệu json
                LUserDA oLUserDA = new LUserDA();
                string userName = HttpContext.Current.User.Identity.Name;
                //0#.w|udtt\hoannt
                if (!string.IsNullOrEmpty(userName))
                {
                    if (userName.Contains("|"))
                    {
                        userName = userName.Split('|').LastOrDefault();
                    }
                    //0#.w|simax\maianh
                    if (userName.Contains("\\"))
                    {
                        userName = userName.Split('\\')[1];
                    }
                }
                var tempuser = oLUserDA.GetListJson(new LUserQuery()
                {
                    TaiKhoanTruyCap = userName
                });
                if (tempuser.Count > 0)
                {
                    oLUserJson = tempuser[0];

                    oLUserJson.CurSiteAction = UrlSite; //site đang thao tác nghiệp vụ
                    LGroupDA lGroupDA = new LGroupDA();
                    var temp = lGroupDA.GetListJson(new LGroupQuery() { UrlSite = UrlSite, GroupIsDonVi = 1 }).FirstOrDefault();
                    if (temp != null)
                        CurFldGroup = temp.ID;

                    //chỗ này check lại chỗ này.
                    if (CurFldGroup > 0)
                    {
                        if (!string.IsNullOrEmpty(oLUserJson.PerJson))
                        {
                            List<PermisonGroup> permisonGroups = new List<PermisonGroup>();
                            permisonGroups = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PermisonGroup>>(oLUserJson.PerJson);

                            PermisonGroup oPermisonGroup = permisonGroups.FirstOrDefault(x => x.fldGroup == CurFldGroup);
                            if (oPermisonGroup == null) oPermisonGroup = new PermisonGroup();
                            oLUserJson.Roles = oPermisonGroup.Roles;
                            oLUserJson.Permissions = oPermisonGroup.Permissions;

                        }
                    }

                    if (oLUserJson.Roles.Count > 0)
                    {
                        LVaiTroDA oLVaiTroDA = new LVaiTroDA();
                        List<LVaiTroJson> lstVaiTro = oLVaiTroDA.GetListJson(new LVaiTroQuery() { lstIDget = oLUserJson.Roles.Select(x => x.ID).ToList(), isGetBylistID = true });
                        foreach (var item in lstVaiTro)
                        {
                            oLUserJson.Permissions.AddRange(item.Permission);
                        }
                    }
                    oLUserJson.Permissions = oLUserJson.Permissions.DistinctBy(x => x.ID).ToList();
                    LPermissionDA lPermissionDA = new LPermissionDA();
                    List<LPermissionJson> lstPermission = lPermissionDA.GetListJson(new LPermissionQuery() { lstIDget = oLUserJson.Permissions.Select(x => x.ID).ToList(), isGetBylistID = true });
                    oLUserJson.PermissionsQuery = "|" + string.Join("|", lstPermission.Select(x => x.MaQuyen)) + "|";
                    //LGroupDA lGroupDA = new LGroupDA();
                    LGroupItem itemGroup = lGroupDA.GetByIdToObject<LGroupItem>(oLUserJson.Groups.ID);
                    oLUserJson.UrlSite = itemGroup.UrlSite;
                    context.Session["ObjectUser"] = oLUserJson;
                    CurentUser = oLUserJson;
                }
                #endregion
            }

            #region MyRegion
            if (!string.IsNullOrEmpty(context.Request["do"]))
                doAction = context.Request["do"].ToUpper();
            #endregion

            oLogHeThongDA = new LogHeThongDA();
            oLconfigDA = new LconfigDA();
        }
        public string doAction = string.Empty;
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public void AddLog(string _LogFunction, string _LogDoiTuong, string _LogThaoTac, int _LogIDDoiTuong, string LogNoiDung, int _LogTrangThai = 0)
        {
            oLogHeThongDA.UpdateObject<LogHeThongItem>(new LogHeThongItem()
            {
                LogFunction = _LogFunction,
                CreatedUser = CurentUser != null ? new SPFieldLookupValue(CurentUser.ID, CurentUser.Title) : new SPFieldLookupValue(),
                fldGroup = CurentUser != null ? new SPFieldLookupValue(CurentUser.Groups.ID, CurentUser.Groups.Title) : new SPFieldLookupValue(),
                LogDoiTuong = _LogDoiTuong,
                LogThaoTac = _LogThaoTac,
                LogNoiDung = LogNoiDung,
                LogIDDoiTuong = _LogIDDoiTuong,
                LogTrangThai = _LogTrangThai
            });
        }
        public void SendMail(string subject, string Content, List<string> mailto)
        {
            try
            {
                string Hostmail = oLconfigDA.GetValueConfigByType("Hostmail");
                string MailFrom = oLconfigDA.GetValueConfigByType("AccountMail");
                string PassEmail = oLconfigDA.GetValueConfigByType("PassEmail");
                string Port = oLconfigDA.GetValueConfigByType("Port");
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(Hostmail);
                mail.From = new MailAddress(MailFrom);
                foreach (string item in mailto)
                    mail.To.Add(item);
                mail.Subject = subject;
                mail.Body = Content;
                mail.IsBodyHtml = true;
                SmtpServer.Port = Convert.ToInt32(Port);
                SmtpServer.Credentials = new System.Net.NetworkCredential(MailFrom, PassEmail);
                //SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);
                AddLog("Email", "Email", "Email", 0, $"Gửi mail thành công");
            }
            catch (Exception ex)
            {
                AddLog("Email", "Email", "Email", 0, $"Gửi mail không thành công {ex.Message}", 1);
            }
        }
        public string GetCurentSite()
        {
            try
            {
                string siteTemp = string.Empty;
                if (HttpContext.Current.Request.UrlReferrer != null)
                    siteTemp = HttpContext.Current.Request.UrlReferrer.ToString();
                else
                    siteTemp = HttpContext.Current.Request.Url.AbsoluteUri;
                string site = siteTemp.Split('/')[3];
                if (site.Equals("noidung") || site.Equals("cms") || site.Equals("Pages"))
                    return "";
                else
                    return "/" + site;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetFullURL(string urlList)
        {
            string url = GetCurentSite();
            if (url.Length > 0 && !(urlList.StartsWith(url) || urlList.StartsWith("/" + url)))
            {
                urlList = url + urlList;
            }

            return urlList.Replace("//", "/");
        }
    }
}