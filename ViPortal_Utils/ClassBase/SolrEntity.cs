﻿using Microsoft.SharePoint;
using SolrNet.Attributes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortal_Utils
{
    /// <summary>
    /// Base Item cho solr item
    /// Author      Date        Comments
    /// VINHHQ      26/09/2018  Tạo mới V1
    /// </summary>
    public class EntitySolr
    {
        [SolrField()] public string id { get; set; }
        [SolrField()] public string Title { get; set; }
        [SolrField()] public string Titles { get; set; }
        [SolrField()] public int SP_ID { get; set; }
        [SolrField()] public string UrlList { get; set; }
        [SolrField()] public string FullUrlList { get; set; }
        [SolrField()] public string Author { get; set; }
        /// <summary>
        /// Người sửa
        /// </summary>
        [SolrField()] public string Editor { get; set; }
        /// <summary>
        /// Ngày tạo
        /// </summary>
        [SolrField()] public DateTime? Created { get; set; }
        /// <summary>
        /// Ngày sửa
        /// </summary>
        [SolrField()] public DateTime? Modified { get; set; }
        [SolrField()] public string fldGroup { get; set; }
        [SolrField()] public int fldGroup_Id { get; set; }
        [SolrField()] public string Attachments { get; set; }
        [SolrField()]
        public int _ModerationStatus { get; set; }
        [SolrField()]
        public string FullContent { get; set; }
     
        /// <summary>
        /// Update objCacs dư lieu trong solr.
        /// </summary>
        /// <param name="request"></param>
        public void UpdateObject(HttpRequest request)
        {
            DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
            var properties = this.GetType().GetProperties();
            //Các trường hợp xử lý đặc biệt
            List<string> ltsRemove = new List<string>(){
                "TextAttachments"
            };

            string values;

            foreach (System.Reflection.PropertyInfo pinfo in properties.Where(p => !ltsRemove.Contains(p.Name)).ToList())
            {
                if (request[pinfo.Name] != null) //Chỉ xét các tham số được post đi.
                {
                    values = request[pinfo.Name];
                    try
                    {
                        Type objType = pinfo.GetType();
                        //Check các kiểu dữ liệu gán vào object
                        switch (pinfo.PropertyType.FullName)
                        {
                            #region MyRegion
                            case "System.Boolean":
                                {
                                    if (!string.IsNullOrEmpty(values))
                                    {
                                        if (values == "on")
                                            pinfo.SetValue(this, true, null);
                                        else
                                            pinfo.SetValue(this, Convert.ToBoolean(values), null);
                                    }
                                    else
                                        pinfo.SetValue(this, false, null);
                                    break;
                                }
                            case "System.Double":
                                if (!string.IsNullOrEmpty(values))
                                    pinfo.SetValue(this, Convert.ToDouble(values), null);
                                break;
                            case "System.Int32":
                                if (!string.IsNullOrEmpty(values))
                                    pinfo.SetValue(this, Convert.ToInt32(values), null);
                                break;
                            case "System.Nullable`1[[System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]"://"System.Nullable`1[[System.DateTime, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]":
                            case "System.DateTime ?":
                            case "System.DateTime":
                                if (!string.IsNullOrEmpty(values))
                                {
                                    //Định dạng ngày tháng
                                    dtfi.ShortDatePattern = "dd/MM/yyyy";
                                    dtfi.DateSeparator = "/";
                                    string temp = pinfo.Name;
                                    //Lấy giá trị request
                                    //Lấy kiểu dữ liệu
                                    DateTime dtTemp = DateTime.Now;
                                    if (DateTime.TryParse(values, dtfi, DateTimeStyles.None, out dtTemp))
                                    {
                                        DateTime dtTemp2 = Convert.ToDateTime(values, dtfi);
                                        if (dtTemp2.Year < 1900 && dtTemp2.Year > 2099)
                                            dtTemp2 = new DateTime(2012, 2, 30);
                                        pinfo.SetValue(this, ((DateTime)dtTemp2), null);
                                    }
                                    else
                                    {
                                        //pinfo.SetValue(this, null, null);
                                        throw new UpdateException(string.Format("Trường {0} nhập không đúng định dạng ngày tháng dd/MM/yyyy hoặc dd/MM/yyyy HH:mm.", pinfo.Name));
                                    }
                                }
                                else
                                {
                                    //;\Convert.ChangeType(value, propertyInfo.PropertyType),
                                    pinfo.SetValue(this, null, null);
                                }
                                break;
                            default:
                            case "System.String":
                                if (!string.IsNullOrEmpty(values))
                                {
                                    pinfo.SetValue(this, ((string)values).Trim(), null);
                                }
                                else
                                {
                                    pinfo.SetValue(this, "", null);
                                }
                                break;
                                #endregion
                        }
                        // Nếu có trường name + "_Id" thì sẽ phải set cho id đó.
                        // Check với các field ID của field Lookup
                        System.Reflection.PropertyInfo pinfo_id = properties.FirstOrDefault(x => x.Name == (pinfo.Name + "_Id"));
                        if (pinfo_id != null)
                        {

                            List<int> templk = values.GetListIDFormStringLookup();
                            if (templk.Count > 0)
                            {
                                pinfo_id.SetValue(this, "," + string.Join(",", templk.Select(x => x.ToString()).ToArray()) + ",", null);
                            }
                            else pinfo_id.SetValue(this, "", null);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new UpdateException(string.Format("Nhập không đúng định dạng trường: <----{0}--->, value:---{1}---, Message: {2}", pinfo.Name, values.Trim(), ex.Message));
                    }
                }

            }
        }

        public  List<string> LstFieldConent =
            new List<string>() {
                "QCSoKyHieu", "QCTrichYeu", "ThreadNoiDung",
                "QCToanVan", "VBSoKyHieu", "VBTrichYeu", "DMLoaiTaiLieu", "VBCoQuanBanHanh","QCNgayVanBan","QCNgayDang"
            };
        public EntitySolr()
        {
            Created = DateTime.Now;
            Modified = DateTime.Now;
        }
        public EntitySolr(SPListItem oSPListItem, params string[] Fields)
        {
            string values = string.Empty;
            DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
            List<string> ltsRemove = new List<string>() { "id", "Attachments", "SP_ID", "fldGroup", "fldGroup_Id", "_ModerationStatus" };
            List<string> lstFieldsException = new List<string>() { "ImageNews", "FullContent" };
            //Fix các trường đặc biệt.
            id = (oSPListItem.ParentList.ParentWeb.ServerRelativeUrl + "/" + oSPListItem.ParentList.RootFolder.Url + "_" + oSPListItem.ID).Replace("/", "-");
            UrlList = oSPListItem.ParentList.ParentWeb.ServerRelativeUrl + "/" + oSPListItem.ParentList.RootFolder.Url;
            FullUrlList = oSPListItem.ParentList.ParentWeb.ServerRelativeUrl + "/" + oSPListItem.ParentList.RootFolder.Url;
            SP_ID = oSPListItem.ID;
            Titles = oSPListItem.Title;
            if (oSPListItem.Fields.ContainsFieldWithStaticName("fldGroup") && (Fields.Contains("fldGroup") || Fields.Count() == 0))
            {
                fldGroup = Convert.ToString(oSPListItem["fldGroup"]);
                fldGroup_Id = new SPFieldLookupValue(fldGroup).LookupId;
            }
            if (oSPListItem.ModerationInformation != null && (Fields.Contains("_ModerationStatus") || Fields.Count() == 0))
            {
                _ModerationStatus = Convert.ToInt32(oSPListItem.ModerationInformation.Status);
            }
            if ((Fields.Contains("Attachments") || Fields.Count() == 0) && oSPListItem.Fields.ContainsField("Attachments"))
            {
                var fileItems = new List<string>();

                if ((oSPListItem.Attachments != null && oSPListItem.Attachments.Count > 0))
                {
                    string urlListItem = oSPListItem.ParentList.DefaultViewUrl.Substring(0, oSPListItem.ParentList.DefaultViewUrl.LastIndexOf("/"));

                    for (int index = 0; index <= oSPListItem.Attachments.Count - 1; index++)
                        fileItems.Add(urlListItem + "/Attachments/" + oSPListItem.ID + "/" + oSPListItem.Attachments[index] + ";" + oSPListItem.Attachments[index]);
                }

                Attachments = string.Join("#", fileItems);
            }
            List<string> lstValues = new List<string>();
            //fix trường fullcontent
            //Fix cho thằng content
            foreach (string oFieldName in LstFieldConent)
            {

                if (oSPListItem.Fields.ContainsField(oFieldName) && oSPListItem[oFieldName] != null)
                {
                    lstValues.Add(oSPListItem[oFieldName].ToString());
                }
            }
            FullContent = string.Join("\r\n", lstValues);
            
            var properties = this.GetType().GetProperties();
            // Get ra các ProperTyInfor cần thiết để convert
            IEnumerable<System.Reflection.PropertyInfo> lstProperties = properties.Where(p => !ltsRemove.Contains(p.Name) && oSPListItem.Fields.ContainsField(p.Name));
            if (Fields != null && Fields.Count() > 0)
                lstProperties = lstProperties.Where(p => Fields.Contains(p.Name));

            foreach (System.Reflection.PropertyInfo pinfo in lstProperties)
            {
                try
                {

                    if (oSPListItem[pinfo.Name] != null)
                    {
                        if (!lstFieldsException.Contains(pinfo.Name))
                        {
                            #region Nếu list chứa trường thuộc đối tượng
                            values = oSPListItem[pinfo.Name].ToString();
                            switch (pinfo.PropertyType.FullName)
                            {
                                #region các kiểu dữ liệu
                                case "System.Boolean":
                                case "System.Nullable`1[[System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]":
                                    {
                                        if (!string.IsNullOrEmpty(values))
                                            pinfo.SetValue(this, System.Convert.ToBoolean(values), null);
                                        else
                                            pinfo.SetValue(this, false, null);
                                        break;
                                    }
                                case "System.Double":
                                case "System.Nullable`1[[System.Double, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]":
                                    if (!string.IsNullOrEmpty(values))
                                        pinfo.SetValue(this, System.Convert.ToDouble(values), null);
                                    break;

                                case "System.Int32":
                                case "System.Nullable`1[[System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]":
                                    if (!string.IsNullOrEmpty(values))
                                    {
                                        try
                                        {
                                            pinfo.SetValue(this, System.Convert.ToInt32(values), null);
                                        }
                                        catch
                                        {
                                        }
                                    }
                                    break;
                                case "System.Nullable`1[[System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]":
                                case "System.DateTime ?":
                                case "System.DateTime":
                                    if (!string.IsNullOrEmpty(values))
                                    {
                                        //dtfi.ShortDatePattern = "dd/MM/yyyy";
                                        //dtfi.DateSeparator = "/";
                                        DateTime dtTemp = System.Convert.ToDateTime(values);
                                        if (dtTemp.Year < 1900 && dtTemp.Year > 2099)
                                            dtTemp = new DateTime(2012, 2, 30);
                                        pinfo.SetValue(this, ((DateTime)dtTemp), null);
                                    }
                                    else
                                    {
                                        object dateNullValue = null;
                                        pinfo.SetValue(this, dateNullValue, null);
                                    }
                                    break;

                                default:
                                case "System.String":
                                    if (!string.IsNullOrEmpty(values))
                                    {
                                        pinfo.SetValue(this, (values).Trim(), null);
                                    }
                                    else
                                    {
                                        pinfo.SetValue(this, "", null);
                                    }
                                    break;
                                    #endregion
                            }
                            // Check với các field ID của field Lookup
                            System.Reflection.PropertyInfo pinfo_id = properties.FirstOrDefault(x => x.Name == (pinfo.Name + "_Id"));
                            if (pinfo_id != null)
                            {
                                List<string> templk = new List<string>();
                                //nếu là permission thì lấy mã ko lấy id
                                if (pinfo_id.Name != "roleListPermission_x003a_permist_Id")
                                    templk = values.GetListIDFormStringLookup().Select(x => x.ToString()).ToList();
                                else
                                    templk = values.GetListValueFormStringLookup();
                                if (templk.Count > 0)
                                {
                                    pinfo_id.SetValue(this, "," + string.Join(",", templk.ToArray()) + ",", null);
                                }
                                else pinfo_id.SetValue(this, null, null);
                            }
                            #endregion
                        }
                        else
                        {
                            switch (pinfo.Name)
                            {
                                case "ImageNews":
                                    values = oSPListItem["ImageNews"].ToString();
                                    SPFieldUrlValue oSPFieldType = new SPFieldUrlValue(values);
                                    values = oSPFieldType.Description;
                                    pinfo.SetValue(this, values, null);
                                    break;
                            }
                        }
                    }
                    else
                    {
                        pinfo.SetValue(this, null, null);
                        System.Reflection.PropertyInfo pinfo_id = properties.FirstOrDefault(x => x.Name == (pinfo.Name + "_Id"));
                        if (pinfo_id != null)
                        {

                            pinfo_id.SetValue(this, null, null);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new UpdateException(string.Format("Lỗi tại trường <----{0}--->, value:---{1}---. Mess: {2}", pinfo.Name, values.Trim(), ex.Message));
                }
            }
        }
        public void SPToSolr(SPListItem oSPListItem, params string[] Fields)
        {
            string values = string.Empty;
            DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
            List<string> ltsRemove = new List<string>() { "id", "Attachments", "SP_ID", "fldGroup", "fldGroup_Id", "_ModerationStatus" };
            List<string> lstFieldsException = new List<string>() { "ImageNews", "FullContent" };
            //Fix các trường đặc biệt.
            id = (oSPListItem.ParentList.ParentWeb.ServerRelativeUrl + "/" + oSPListItem.ParentList.RootFolder.Url + "_" + oSPListItem.ID).Replace("/", "-");
            UrlList = oSPListItem.ParentList.ParentWeb.ServerRelativeUrl + "/" + oSPListItem.ParentList.RootFolder.Url;
            FullUrlList = oSPListItem.ParentList.ParentWeb.ServerRelativeUrl + "/" + oSPListItem.ParentList.RootFolder.Url;
            SP_ID = oSPListItem.ID;
            Titles = oSPListItem.Title;
            if (oSPListItem.Fields.ContainsFieldWithStaticName("fldGroup") && (Fields.Contains("fldGroup") || Fields.Count() == 0))
            {
                fldGroup = Convert.ToString(oSPListItem["fldGroup"]);
                fldGroup_Id = new SPFieldLookupValue(fldGroup).LookupId;
            }
            if (oSPListItem.ModerationInformation != null && (Fields.Contains("_ModerationStatus") || Fields.Count() == 0))
            {
                _ModerationStatus = Convert.ToInt32(oSPListItem.ModerationInformation.Status);
            }
            if (((Fields != null &&  Fields.Contains("Attachments")) || Fields.Count() == 0) && oSPListItem.Fields.ContainsField("Attachments"))
            {
                var fileItems = new List<string>();

                if ((oSPListItem.Attachments != null && oSPListItem.Attachments.Count > 0))
                {
                    string urlListItem = oSPListItem.ParentList.DefaultViewUrl.Substring(0, oSPListItem.ParentList.DefaultViewUrl.LastIndexOf("/"));

                    for (int index = 0; index <= oSPListItem.Attachments.Count - 1; index++)
                        fileItems.Add(urlListItem + "/Attachments/" + oSPListItem.ID + "/" + oSPListItem.Attachments[index] + ";" + oSPListItem.Attachments[index]);
                }

                Attachments = string.Join("#", fileItems);
            }
            List<string> lstValues = new List<string>();
            //fix trường fullcontent
            //Fix cho thằng content
            foreach (string oFieldName in LstFieldConent)
            {

                if (oSPListItem.Fields.ContainsField(oFieldName) && oSPListItem[oFieldName] != null)
                {
                    lstValues.Add(oSPListItem[oFieldName].ToString());
                }
            }
            FullContent = string.Join("\r\n", lstValues);

            var properties = this.GetType().GetProperties();
            // Get ra các ProperTyInfor cần thiết để convert
            IEnumerable<System.Reflection.PropertyInfo> lstProperties = properties.Where(p => !ltsRemove.Contains(p.Name) && oSPListItem.Fields.ContainsField(p.Name));
            if (Fields != null && Fields.Count() > 0)
                lstProperties = lstProperties.Where(p => Fields.Contains(p.Name));

            foreach (System.Reflection.PropertyInfo pinfo in lstProperties)
            {
                try
                {

                    if (oSPListItem[pinfo.Name] != null)
                    {
                        if (!lstFieldsException.Contains(pinfo.Name))
                        {
                            #region Nếu list chứa trường thuộc đối tượng
                            values = oSPListItem[pinfo.Name].ToString();
                            switch (pinfo.PropertyType.FullName)
                            {
                                #region các kiểu dữ liệu
                                case "System.Boolean":
                                case "System.Nullable`1[[System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]":
                                    {
                                        if (!string.IsNullOrEmpty(values))
                                            pinfo.SetValue(this, System.Convert.ToBoolean(values), null);
                                        else
                                            pinfo.SetValue(this, false, null);
                                        break;
                                    }
                                case "System.Double":
                                case "System.Nullable`1[[System.Double, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]":
                                    if (!string.IsNullOrEmpty(values))
                                        pinfo.SetValue(this, System.Convert.ToDouble(values), null);
                                    break;

                                case "System.Int32":
                                case "System.Nullable`1[[System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]":
                                    if (!string.IsNullOrEmpty(values))
                                    {
                                        try
                                        {
                                            pinfo.SetValue(this, System.Convert.ToInt32(values), null);
                                        }
                                        catch
                                        {
                                        }
                                    }
                                    break;
                                case "System.Nullable`1[[System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]":
                                case "System.DateTime ?":
                                case "System.DateTime":
                                    if (!string.IsNullOrEmpty(values))
                                    {
                                        //dtfi.ShortDatePattern = "dd/MM/yyyy";
                                        //dtfi.DateSeparator = "/";
                                        DateTime dtTemp = System.Convert.ToDateTime(values);
                                        if (dtTemp.Year < 1900 && dtTemp.Year > 2099)
                                            dtTemp = new DateTime(2012, 2, 30);
                                        pinfo.SetValue(this, ((DateTime)dtTemp), null);
                                    }
                                    else
                                    {
                                        object dateNullValue = null;
                                        pinfo.SetValue(this, dateNullValue, null);
                                    }
                                    break;

                                default:
                                case "System.String":
                                    if (!string.IsNullOrEmpty(values))
                                    {
                                        pinfo.SetValue(this, (values).Trim(), null);
                                    }
                                    else
                                    {
                                        pinfo.SetValue(this, "", null);
                                    }
                                    break;
                                    #endregion
                            }
                            // Check với các field ID của field Lookup
                            System.Reflection.PropertyInfo pinfo_id = properties.FirstOrDefault(x => x.Name == (pinfo.Name + "_Id"));
                            if (pinfo_id != null)
                            {
                                List<string> templk = new List<string>();
                                //nếu là permission thì lấy mã ko lấy id
                                if (pinfo_id.Name != "roleListPermission_x003a_permist_Id")
                                {
                                    List<string> templkTitle = values.GetListValueFormStringLookup();
                                    if (templkTitle.Count > 0)
                                    {
                                        templk = values.GetListIDFormStringLookup().Select(x => x.ToString()).ToList();
                                    }
                                }
                                else
                                    templk = values.GetListValueFormStringLookup();
                                if (templk.Count > 0)
                                {
                                    pinfo_id.SetValue(this, "," + string.Join(",", templk.ToArray()) + ",", null);
                                }
                                else pinfo_id.SetValue(this, null, null);
                            }
                            #endregion
                        }
                        else
                        {
                            switch (pinfo.Name)
                            {
                                case "ImageNews":
                                    values = oSPListItem["ImageNews"].ToString();
                                    SPFieldUrlValue oSPFieldType = new SPFieldUrlValue(values);
                                    values = oSPFieldType.Description;
                                    pinfo.SetValue(this, values, null);
                                    break;
                            }
                        }
                    }
                    else
                    {
                        pinfo.SetValue(this, null, null);
                        System.Reflection.PropertyInfo pinfo_id = properties.FirstOrDefault(x => x.Name == (pinfo.Name + "_Id"));
                        if (pinfo_id != null)
                        {

                            pinfo_id.SetValue(this, null, null);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new UpdateException(string.Format("Lỗi tại trường <----{0}--->, value:---{1}---. Mess: {2}", pinfo.Name, values.Trim(), ex.Message));
                }
            }
        }



    }
}
