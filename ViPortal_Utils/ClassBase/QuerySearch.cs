﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace ViPortal_Utils.Base
{
    public class QuerySearch
    {
        /// <summary>
        /// query treen db index  solr
        /// </summary>
        public bool SearchIndex { get; set; }
        public bool isGetBylistID { get; set; }
        public List<int> lstIDget { get; set; }

        public int ID { get; set; }
        public int ItemID { get; set; }
        public int ItemIDNotGet { get; set; }
        public string Keyword { get; set; }
        public bool isToDay { get; set; }
        public bool isYesterday { get; set; }
        public bool isWeek { get; set; }
        public List<string> UrlList { get; set; }
        public string UrlSiteStart { get; set; }
        public string Urlsite { get; set; }
        public List<string> UrlListNotGet { get; set; }
        
        /// <summary>
        /// Đến ngày
        /// </summary>
        public DateTime? DenNgay { get; set; }
        public List<string> SearchIn { get; set; }
        /// <summary>
        /// Từ ngày		/// </summary>
        public DateTime? TuNgay { get; set; }


        public int Author { get; set; }
        public int _ModerationStatus = -1;

        #region Nhom thong tin grid 
        public int Draw { get; set; }

        public int Start { get; set; }

        public int Length { get; set; }

        public bool Ascending { get; set; }

        public string FieldOrder { get; set; }
        #endregion
        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        ///<modified>
        /// Author				created date					comments
        /// Author				10/12/2013					    Tạo mới
        ///</modified>
        public QuerySearch()
        {
            _ModerationStatus = -1;
            lstIDget = new List<int>();
            UrlList = new List<string>();
        }

        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        ///<modified>
        /// Author				created date					comments
        /// Author				10/12/2013					    Tạo mới
        ///</modified>
        public QuerySearch(HttpRequest request)
        {
            UrlList = new List<string>();
            lstIDget = new List<int>();
            #region Nhom dieu kien tim kiem
            _ModerationStatus = -1;
            DateTimeFormatInfo dtfiParser;
            dtfiParser = new DateTimeFormatInfo();
            dtfiParser.ShortDatePattern = "dd/MM/yyyy";
            dtfiParser.DateSeparator = "/";
            if (!string.IsNullOrEmpty(request["SearchDenNgay"]))
                DenNgay = Convert.ToDateTime(request["SearchDenNgay"], dtfiParser);
            else
                DenNgay = null;
            if (!string.IsNullOrEmpty(request["SearchTuNgay"]))
                TuNgay = Convert.ToDateTime(request["SearchTuNgay"], dtfiParser);
            else
                TuNgay = null;
            // timkiem theo ngay thang
            if (!string.IsNullOrEmpty(request["date"]))
            {
                if (request["date"].ToString().Equals("1"))
                {
                    isToDay = true;
                }
                else if (request["date"].ToString().Equals("2"))
                {
                    isYesterday = true;
                }
                else if (request["date"].ToString().Equals("3"))
                {
                    isWeek = true;
                }
            }

            /// tim kiem theo tu khoa:  keyword
            if (!string.IsNullOrEmpty(request["Keyword"]))
            {
                Keyword = request["Keyword"].Trim();
                Keyword = Regex.Replace(Keyword, @"\s+", " ");//Xóa khoảng trắng
            }
            else
                Keyword = string.Empty;
            //SearchInAdvance=Titles,Fulltext
            string tempSearchIn;
            if (!string.IsNullOrEmpty(request["SearchInAdvance"]))
                tempSearchIn = request["SearchInAdvance"];
            else if (!string.IsNullOrEmpty(request["SearchInSimple"]))
                tempSearchIn = request["SearchInSimple"];
            else
                tempSearchIn = string.Empty;

            if (!string.IsNullOrEmpty(tempSearchIn))
            {
                if (tempSearchIn.Contains(','))
                    SearchIn = tempSearchIn.Split(',').ToList();
                else
                    SearchIn = new List<string>() { tempSearchIn };
            }
            else
                SearchIn = new List<string>();
            if (!string.IsNullOrEmpty(request["Author"]))
                Author = Convert.ToInt32(request["Author"]);
            else Author = 0;

            if (!string.IsNullOrEmpty(request["moder"]))
                _ModerationStatus = Convert.ToInt32(request["moder"]);
            else _ModerationStatus = -1;

            int _id = 0;
            int.TryParse(request["ItemID"], out _id);
            ItemID = _id;
            if (!string.IsNullOrEmpty(request["lstIDget"]))
            {
                lstIDget = clsFucUtils.GetDanhSachIDsQuaFormPost(request["lstIDget"]);
            }
            if (!string.IsNullOrEmpty(request["isGetBylistID"]))
            {
                isGetBylistID = Convert.ToBoolean(request["isGetBylistID"]);
            }
            //isGetBylistID
            #endregion
            //ItemIDNotGet
            if (!string.IsNullOrEmpty(request["ItemIDNotGet"]))
                ItemIDNotGet = Convert.ToInt32(request["ItemIDNotGet"]);
            #region Nhom săp xep grid,
            int draw = 0, start = 0, length = 0, id = 0,  _curentPage = 0;
            int.TryParse(request["draw"], out draw);
            int.TryParse(request["start"], out start);
            if (!string.IsNullOrEmpty(request["length"]))
                int.TryParse(request["length"], out length);
            else length = 20;
            if(!string.IsNullOrEmpty(request["Page"]))
                int.TryParse(request["Page"], out _curentPage);
            //int.TryParse(request["ItemID"], out _curentPage);
            Draw = draw;
            Start = start;
            Length = length;
            Ascending = request["order[0][dir]"] == "desc" ? false : true;
            string indexCol = request["order[0][column]"];
            if (indexCol != null && indexCol.Length > 0)
            {
                FieldOrder = request[string.Format("columns[{0}][name]", indexCol)];
                if (FieldOrder == null || FieldOrder.Length == 0)
                    FieldOrder = request[string.Format("columns[{0}][data]", indexCol)];
            }
            else
            {
                if (!string.IsNullOrEmpty(request["FieldOrder"]))
                {
                    FieldOrder = request["FieldOrder"];
                }
                else FieldOrder = "ID";
                if (!string.IsNullOrEmpty(request["Ascending"]))
                {
                    Ascending = Convert.ToBoolean(request["Ascending"]);
                }
                else Ascending = false;
            } 
            #endregion

        }
        ///public int currentPage { get; set; }
        ///
        /// <summary>
        /// Get value Request
        /// </summary>
        /// <param name="pinfo"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public object RequestValue(PropertyInfo pinfo, HttpRequest request)
        {
            object valueReturn = null;

            DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
            //if (!string.IsNullOrEmpty(request[pinfo.Name]))
            //{
            var values = request[pinfo.Name];
            switch (pinfo.PropertyType.FullName)
            {
                case "System.Int32":
                    if (!string.IsNullOrEmpty(values))
                    {
                        valueReturn = System.Convert.ToInt32(values);
                    }
                    break;
                case "System.Int64":
                    if (!string.IsNullOrEmpty(values))
                    {
                        valueReturn = System.Convert.ToInt64(values);
                    }
                    break;
                case "System.Nullable`1[[System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]":
                case "System.DateTime ?":
                case "System.DateTime":
                    if (!string.IsNullOrEmpty(values))
                    {
                        dtfi.ShortDatePattern = "dd/MM/yyyy";
                        dtfi.DateSeparator = "/";
                        DateTime dtTemp = System.Convert.ToDateTime(values, dtfi);
                        if (dtTemp.Year < 1900 && dtTemp.Year > 2099)
                            dtTemp = new DateTime(2012, 2, 30);
                        valueReturn = ((DateTime)dtTemp);
                    }
                    else
                    {
                        object dateNullValue = null;
                        pinfo.SetValue(this, dateNullValue, null);
                    }
                    break;
                case "System.Collections.Generic.List`1[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]":
                    if (!string.IsNullOrEmpty(values))
                    {
                        List<string> lstValues = values.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        valueReturn = lstValues;
                    }
                    else
                        valueReturn = new List<string>();
                    break;
                case "System.Collections.Generic.List`1[[System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]":
                    if (!string.IsNullOrEmpty(values))
                    {
                        List<int> lstValuesInt = clsFucUtils.GetDanhSachIDsQuaFormPost(values);
                        valueReturn = lstValuesInt;
                    }
                    else
                        valueReturn = new List<int>();
                    break;
                case "System.Boolean":
                    if (!string.IsNullOrEmpty(values))
                    {
                        valueReturn = Convert.ToBoolean(values);
                    }
                    else
                        valueReturn = false;
                    break;
                default:
                case "System.String":
                    if (!string.IsNullOrEmpty(values))
                    {
                        valueReturn = (values).Trim();
                    }
                    else
                    {
                        valueReturn = "";
                    }
                    break;
                    //      }
            }
            return valueReturn;
        }
    }

}
