﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace ViPortal_Utils
{
    /*
MIT License
Copyright (c) 2016 Heiswayi Nrird
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

    public class SimpleLogger
    {
        private const string FILE_EXT = ".log";
        private static ReaderWriterLock locker = new ReaderWriterLock();

        private readonly string datetimeFormat;
        private readonly string logFilename;
        private bool LoggingEnabled = false;

        /// <summary>
        /// Initiate an instance of SimpleLogger class constructor.
        /// If log file does not exist, it will be created automatically.
        /// </summary>
        public SimpleLogger()
        {
            LoggingEnabled = CheckLoggingEnabled();
            if (LoggingEnabled)
            {
                datetimeFormat = "yyyy-MM-dd HH:mm:ss.fff";

                logFilename = @"C:\LogSimax\" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + FILE_EXT;
                // Log file header line
                string logHeader = logFilename + " is created.";
                if (!System.IO.File.Exists(logFilename))
                {
                    WriteLine(System.DateTime.Now.ToString(datetimeFormat) + " " + logHeader);
                }
            }
        }

        /// <summary>
        /// Log a DEBUG message
        /// </summary>
        /// <param name="text">Message</param>
        public void Debug(string text)
        {
            WriteFormattedLog(LogLevel.DEBUG, text);
        }

        /// <summary>
        /// Log an ERROR message
        /// </summary>
        /// <param name="text">Message</param>
        public void Error(string text)
        {
            WriteFormattedLog(LogLevel.ERROR, text);
        }

        /// <summary>
        /// Log a FATAL ERROR message
        /// </summary>
        /// <param name="text">Message</param>
        public void Fatal(string text)
        {
            WriteFormattedLog(LogLevel.FATAL, text);
        }

        /// <summary>
        /// Log an INFO message
        /// </summary>
        /// <param name="text">Message</param>
        public void Info(string text)
        {
            WriteFormattedLog(LogLevel.INFO, text);
        }

        /// <summary>
        /// Log a TRACE message
        /// </summary>
        /// <param name="text">Message</param>
        public void Trace(string text)
        {
            WriteFormattedLog(LogLevel.TRACE, text);
        }

        /// <summary>
        /// Log a WARNING message
        /// </summary>
        /// <param name="text">Message</param>
        public void Warning(string text)
        {
            WriteFormattedLog(LogLevel.WARNING, text);
        }

        private void WriteLine(string text, bool append = false)
        {
            if (string.IsNullOrEmpty(text))
            {
                return;
            }
            try
            {
                locker.AcquireWriterLock(int.MaxValue); //You might wanna change timeout value 
                using (System.IO.StreamWriter writer = new System.IO.StreamWriter(logFilename, append, System.Text.Encoding.UTF8))
                {
                    writer.WriteLine(text);
                }
            }
            finally
            {
                locker.ReleaseWriterLock();
            }
        }

        private void WriteFormattedLog(LogLevel level, string text)
        {
            if (LoggingEnabled)
            {
                string pretext;
                switch (level)
                {
                    case LogLevel.TRACE:
                        pretext = System.DateTime.Now.ToString(datetimeFormat) + " [TRACE]   ";
                        break;
                    case LogLevel.INFO:
                        pretext = System.DateTime.Now.ToString(datetimeFormat) + " [INFO]    ";
                        break;
                    case LogLevel.DEBUG:
                        pretext = System.DateTime.Now.ToString(datetimeFormat) + " [DEBUG]   ";
                        break;
                    case LogLevel.WARNING:
                        pretext = System.DateTime.Now.ToString(datetimeFormat) + " [WARNING] ";
                        break;
                    case LogLevel.ERROR:
                        pretext = System.DateTime.Now.ToString(datetimeFormat) + " [ERROR]   ";
                        break;
                    case LogLevel.FATAL:
                        pretext = System.DateTime.Now.ToString(datetimeFormat) + " [FATAL]   ";
                        break;
                    default:
                        pretext = "";
                        break;
                }

                WriteLine(pretext + text, true);
            }
        }

        [System.Flags]
        private enum LogLevel
        {
            TRACE,
            INFO,
            DEBUG,
            WARNING,
            ERROR,
            FATAL
        }


        /// <summary>
        /// Check Logginstatus config file is exist.
        /// If exist read the value set the loggig status
        /// </summary>
        private bool CheckLoggingEnabled()
        {
            string strLoggingStatusConfig = string.Empty;

            strLoggingStatusConfig = GetLoggingStatusConfigFileName();

            //If it's empty then enable the logging status 
            if (strLoggingStatusConfig.Equals(string.Empty))
            {
                return true;
            }
            else
            {

                //Read the value from xml and set the logging status
                bool bTemp = GetValueFromXml(strLoggingStatusConfig);
                return bTemp;
            }
        }

        /// <summary>
        /// Check the Logginstatus config under debug or 
        /// release folder. If not exist, check under 
        /// project folder. If not exist again, return empty string
        /// </summary>
        /// <RETURNS>empty string if file not exists</RETURNS>
        private string GetLoggingStatusConfigFileName()
        {
            string strCheckinBaseDirecotry =
                      AppDomain.CurrentDomain.BaseDirectory +
                      "LoggingStatus.Config";

            if (File.Exists(strCheckinBaseDirecotry))
                return strCheckinBaseDirecotry;
            else
            {
                string strCheckinApplicationDirecotry =
                    Directory.GetCurrentDirectory() + "LoggingStatus.Config";

                if (File.Exists(strCheckinApplicationDirecotry))
                    return strCheckinApplicationDirecotry;
                else
                    return string.Empty;
            }
        }

        ///<summary> 
        /// Read the xml file and getthe logging status
        /// <param name="strXmlPath"></param>
        ///</summary> <RETURNS></RETURNS>
        private bool GetValueFromXml(string strXmlPath)
        {
            try
            {
                //Open a FileStream on the Xml file
                FileStream docIn = new FileStream(strXmlPath,
                   FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

                XmlDocument contactDoc = new XmlDocument();
                //Load the Xml Document
                contactDoc.Load(docIn);

                //Get a node
                XmlNodeList UserList =
                    contactDoc.GetElementsByTagName("LoggingEnabled");

                //get the value
                string strGetValue = UserList.Item(0).InnerText.ToString();

                if (strGetValue.Equals("0"))
                    return false;
                else if (strGetValue.Equals("1"))
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
