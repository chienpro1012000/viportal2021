﻿using Microsoft.SharePoint;
using Newtonsoft.Json;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ViPortal_Utils.Base
{
    public static class clsFucUtils
    {
        public static byte[] ReadFile(string filePath)
        {
            byte[] buffer;
            FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            try
            {
                int length = (int)fileStream.Length;  // get file length
                buffer = new byte[length];            // create buffer
                int count;                            // actual number of bytes read
                int sum = 0;                          // total number of bytes read

                // read until Read method returns 0 (end of the stream has been reached)
                while ((count = fileStream.Read(buffer, sum, length - sum)) > 0)
                    sum += count;  // sum is a buffer offset for next reading
            }
            finally
            {
                fileStream.Close();
            }
            return buffer;
        }
        /// <summary>
        /// laya ten truy cap
        /// </summary>
        /// <param name="UserNameFull"></param>
        /// <returns></returns>
        public static string GetUserName(string UserNameFull)
        {
            string userName = UserNameFull;
            if (!string.IsNullOrEmpty(UserNameFull))
            {
                if (userName.Contains("|"))
                {
                    userName = userName.Split('|').LastOrDefault();
                }
                //0#.w|simax\maianh
                if (userName.Contains("\\"))
                {
                    userName = userName.Split('\\')[1];
                }
            }
            return userName;
        }
        /// <summary>
        /// Lấy về danh sách ID
        /// fomat string 1,2,3,4,
        /// </summary>
        /// <param name="arrID"></param>
        /// <returns></returns>
        public static List<int> GetDanhSachIDsQuaFormPost(string arrID)
        {
            List<int> dsID = new List<int>();
            if (!string.IsNullOrEmpty(arrID))
            {
                string[] tempIDs = arrID.Split(',');
                foreach (string idConvert in tempIDs)
                {
                    int _id = 0;
                    if (int.TryParse(idConvert, out _id))
                    {
                        if (_id > 0)
                            dsID.Add(_id);
                    }
                }
            }
            return dsID;
        }
        /// <summary>
        /// Hàm chuyển từ định dang ,1,2,3,4
        /// </summary>
        /// <param name="lookupIds"></param>
        /// <returns></returns>
        public static SPFieldLookupValueCollection StringToLookup(string lookupIds)
        {
            SPFieldLookupValueCollection values = new SPFieldLookupValueCollection();
            if (!string.IsNullOrEmpty(lookupIds))
            {
                foreach (int id in GetDanhSachIDsQuaFormPost(lookupIds))
                    values.Add(new SPFieldLookupValue(id, string.Empty));
            }
            return values;
        }
        public static string SerializerJSON(object oTT)
        {

            return Newtonsoft.Json.JsonConvert.SerializeObject(oTT);
        }
        /// <summary>
        /// Lấy về link ảnh qua thẻ html img
        /// </summary>
        /// <param name="htmlImage"></param>
        /// <returns></returns>
        public static string getUrlImageFromHtml(string htmlImage)
        {
            if (htmlImage == "")
                return string.Empty;
            Match mTemp = Regex.Match(htmlImage, @"<img.*src\x3D\x22(?<src>[^\x22]+)\x22.*>$", RegexOptions.IgnoreCase);
            htmlImage = mTemp.Groups["src"].Value.ToString();
            return htmlImage;

        }

        public static DateTime GetNextWeekday(DateTime start, DayOfWeek day)
        {
            // The (... + 7) % 7 ensures we end up with a value in the range [0, 6]
            int daysToAdd = ((int)day - (int)start.DayOfWeek + 7) % 7;
            return start.AddDays(daysToAdd);
        }
        public static string GetDayOfWeek(int DayofWeek)
        {
            string _Thu = "";
            switch (DayofWeek)
            {
                case 1:
                    _Thu = "Chủ nhật";
                    break;
                case 2:
                    _Thu = "Thứ 2";
                    break;
                case 3:
                    _Thu = "Thứ 3";
                    break;
                case 4:
                    _Thu = "Thứ 4";
                    break;
                case 5:
                    _Thu = "Thứ 5";
                    break;

                case 6:
                    _Thu = "Thứ 6";
                    break;
                case 7:
                    _Thu = "Thứ 7";
                    break;
            }
            return _Thu;
        }


        public static void SendNotiFi(string title, string body, string token, object odata)
        {
            //string ServerKey = "";
            //string FireBasePushNotificationsURL = "";
            Uri FireBasePushNotificationsURL = new Uri("https://fcm.googleapis.com/fcm/send");
            string ServerKey = "AAAACLhtkSU:APA91bGjzGuk1E9YIe4v_jVCztr8QQ_mIccnFDbBiGg6ObdMQGs-PkwNJk07g-cSW3zKlh8abfy92MkFco8kyeKf5Aa14CQbxw4EfZfXmQlI-xecSQpE7fk_-In8T6ZxpRecgBXKusmo";
            string[] chuoicat = { ";#" };
            int count = 2;
            String[] strlist = token.Split(chuoicat, count,
               StringSplitOptions.RemoveEmptyEntries);
            var messageInformation = new Message()
            {
                notification = new
                {
                    title = title,
                    body = body,
                    sound = "default",
                    click_action = "FCM_PLUGIN_ACTIVITY",
                    icon = "fcm_push_icon"
                },
                data = odata,
                registration_ids = strlist
            };
            //Object to JSON STRUCTURE => using Newtonsoft.Json;
            string jsonMessage = JsonConvert.SerializeObject(messageInformation);

            // Create request to Firebase API
            var request = new HttpRequestMessage(HttpMethod.Post, FireBasePushNotificationsURL);
            request.Headers.TryAddWithoutValidation("Authorization", "key=" + ServerKey);
            request.Content = new StringContent(jsonMessage, Encoding.UTF8, "application/json");
            HttpResponseMessage result;
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });

            using (var client = new HttpClient())
            {
                result = client.SendAsync(request).Result;
                string temp = result.Content.ReadAsStringAsync().Result;
            }

        }

        public static void NPOIInsertRows(ref ISheet iSheet, int iFromRow, int iCount)
        {
            for (int _count = 1; _count <= iCount; _count++)
            {
                int _lines_count = iSheet.PhysicalNumberOfRows;
                for (int _row = _lines_count; _row >= iFromRow - 1; _row--)
                {
                    iSheet.ShiftRows(_row, _row, 1);

                    // Take a sample of the style from previous line, create a new line and copy everything from the sample
                    IRow _row_source = iSheet.GetRow(_row - 1);
                    IRow _row_insert = iSheet.CreateRow(_row);
                    _row_insert.Height = _row_source.Height;
                    for (int colIndex = 0; colIndex < _row_source.LastCellNum; colIndex++)
                    {
                        ICell cellSource = _row_source.GetCell(colIndex);
                        ICell cellInsert = _row_insert.CreateCell(colIndex);

                        //  Make sure that merged cells are merged too in the new line
                        if (cellSource != null && cellSource.IsMergedCell)
                        {
                            foreach (CellRangeAddress _range in iSheet.MergedRegions)
                            {
                                if (_range.IsInRange(cellSource.RowIndex, cellSource.ColumnIndex))
                                {
                                    if (cellInsert.ColumnIndex == _range.FirstColumn)
                                    {
                                        CellRangeAddress _new_range = new CellRangeAddress(cellInsert.RowIndex, cellInsert.RowIndex, cellInsert.ColumnIndex, _range.LastColumn);
                                        iSheet.AddMergedRegion(_new_range);
                                    }
                                }
                            }
                        }

                        if (cellSource != null)
                        {
                            cellInsert.CellStyle = cellSource.CellStyle;
                        }
                    }
                }
            }
        }
        public static object GetCellValue(ICell cell)
        {
            object value = null;
            switch (cell.CellType)
            {
                case CellType.String:
                    value = cell.StringCellValue;
                    break;
                case CellType.Numeric:
                    value = cell.NumericCellValue;
                    break;
                case CellType.Formula:
                    switch (cell.CachedFormulaResultType)
                    {
                        case CellType.String:
                            value = cell.StringCellValue;
                            break;
                        case CellType.Numeric:
                            //excel trigger is probably out-of-date
                            value = (cell.CellFormula == "TODAY()" ? DateTime.Today.ToOADate() : cell.NumericCellValue).ToString();
                            break;
                    }
                    break;
            }
            return value;
        }

        public static void copyFromSourceToDestinationRow(XSSFWorkbook workbook, ISheet sourceWorksheet, int sourceRowNum, ISheet destinationWorksheet, int destinationRowNum, ConfigExcel configExcel)
        {
            // Get the source / new row
            IRow sourceRow = sourceWorksheet.GetRow(sourceRowNum);

            IRow newRow = destinationWorksheet.GetRow(destinationRowNum);

            //HSSFRow newRow = worksheet.GetRow(destinationRowNum);
            //HSSFRow sourceRow = worksheet.GetRow(sourceRowNum);


            // Loop through source columns to add to new row
            for (int i = 0; i < sourceRow.LastCellNum; i++)
            {

                // Grab a copy of the old/new cell
                ICell oldCell = sourceRow.GetCell(i);
                ICell newCell = newRow.CreateCell(i);
                // If the old cell is null jump to next cell
                if (oldCell == null)
                {
                    continue;
                }
                if (configExcel.Type1_HasSTT && configExcel.Type1_STT >= 0 && i == configExcel.Type1_STT)
                {
                    //là ô số thứ tự thì sẽ phải sét giá trị là stt.
                    newCell.SetCellValue(destinationRowNum - configExcel.Type1_DongBatDau + 1);
                }
                else
                {
                    // Copy style from old cell and apply to new cell
                    ICellStyle newCellStyle = workbook.CreateCellStyle();
                    newCellStyle.CloneStyleFrom(oldCell.CellStyle);
                    newCell.CellStyle = newCellStyle;

                    // If there is a cell comment, copy
                    if (oldCell.CellComment != null)
                    {
                        newCell.CellComment = oldCell.CellComment;
                    }

                    // If there is a cell hyperlink, copy
                    if (oldCell.Hyperlink != null)
                    {
                        newCell.Hyperlink = oldCell.Hyperlink;
                    }

                    // Set the cell data type
                    //newCell.CellFormula = oldCell.CellFormula;

                    // Set the cell data value
                    switch (oldCell.CellType)
                    {
                        case CellType.Blank:// Cell.CELL_TYPE_BLANK:
                            newCell.SetCellValue(oldCell.StringCellValue);
                            break;
                        case CellType.Boolean:
                            newCell.SetCellValue(oldCell.BooleanCellValue);
                            break;
                        case CellType.Formula:
                            newCell.SetCellValue(oldCell.CellFormula);
                            break;
                        case CellType.Numeric:
                            newCell.SetCellValue(oldCell.NumericCellValue);
                            break;
                        case CellType.String:
                            newCell.SetCellValue(oldCell.RichStringCellValue);
                            break;
                        default:
                            break;
                    }
                }
            }

            // If there are are any merged regions in the source row, copy to new row
            for (int i = 0; i < sourceWorksheet.NumMergedRegions; i++)
            {
                CellRangeAddress cellRangeAddress = sourceWorksheet.GetMergedRegion(i);
                if (cellRangeAddress.FirstRow == sourceRow.RowNum)
                {
                    CellRangeAddress newCellRangeAddress = new CellRangeAddress(newRow.RowNum,
                            (newRow.RowNum + (cellRangeAddress.LastRow - cellRangeAddress.FirstRow)),
                            cellRangeAddress.FirstColumn, cellRangeAddress.LastColumn);
                    destinationWorksheet.AddMergedRegion(newCellRangeAddress);
                }
            }
        }


        public static string GetFileExtension(string base64String)
        {
            var data = base64String.Substring(0, 5);

            switch (data.ToUpper())
            {
                case "IVBOR":
                    return "png";
                case "/9J/4":
                    return "jpg";
                case "AAAAF":
                    return "mp4";
                case "JVBER":
                    return "pdf";
                case "AAABA":
                    return "ico";
                case "UMFYI":
                    return "rar";
                case "E1XYD":
                    return "rtf";
                case "U1PKC":
                    return "txt";
                case "MQOWM":
                case "77U/M":
                    return "srt";
                default:
                    return string.Empty;
            }
        }


        public static LookupData GetLoookupByStringFiledData(string fieldData)
        {
            LookupData objData = new LookupData();
            if (!string.IsNullOrEmpty(fieldData))
            {
                if (fieldData.Contains(";#"))
                {
                    try
                    {
                        string[] splitData = fieldData.Split('#');
                        objData.ID = Convert.ToInt32(splitData[0].Replace(";", ""));
                        if (splitData.Length == 2)
                            objData.Title = splitData[0 + 1].Replace(";", "");
                    }catch (Exception ex)
                    {

                    }
                }
            }
            return objData;
        }
        public static List<LookupData> GetDanhSachMutilLoookupByStringFiledData(string fieldData)
        {
            List<LookupData> ListData = new List<LookupData>();
            try
            {
                LookupData objData;
                if (!string.IsNullOrEmpty(fieldData))
                {
                    string[] splitData = fieldData.Split('#');
                    for (int index = 0; index < splitData.Length; index = index + 2)
                    {
                        objData = new LookupData();
                        objData.ID = Convert.ToInt32(splitData[index].Replace(";", ""));
                        if (index + 1 < splitData.Length)
                            objData.Title = splitData[index + 1].Replace(";", "");
                        ListData.Add(objData);
                    }
                }
            }catch (Exception ex)
            {

            }
            return ListData;
        }

    }
}
