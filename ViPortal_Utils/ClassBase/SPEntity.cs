﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ViPortal_Utils.Base;

namespace ViPortal_Utils.Base
{
    public class SPEntity
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string WFTrangThai { get; set; }
        /// <summary>
        /// Người tạo
        /// </summary>
        public SPFieldLookupValue Author { get; set; }
        public SPFieldLookupValueCollection UserPhuTrach { get; set; }
        /// <summary>
        /// Người sửa
        /// </summary>
        public SPFieldLookupValue Editor { get; set; }
        /// <summary>
        /// Ngày tạo
        /// </summary>
        public DateTime Created { get; set; }
        /// <summary>
        /// Ngày sửa
        /// </summary>
        public DateTime Modified { get; set; }
        public List<FileAttach> ListFileAttachAdd { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<FileAttach> ListFileAttach { get; set; }
        public List<string> ListFileRemove { get; set; }
        public virtual SPModerationStatusType _ModerationStatus { get; set; } = SPModerationStatusType.Pending;
        public SPEntity()
        {
            _ModerationStatus = SPModerationStatusType.Pending;
            Title = string.Empty;
            ListFileAttachAdd = new List<FileAttach>();
            ListFileAttach = new List<FileAttach>();
            ListFileRemove = new List<string>();
        }
        DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
        /// <summary>
        /// Set giá trị request cho object
        /// </summary>
        /// <param name="request">Truyền vào biến Request (this.Request)</param>
        public void UpdateObject(HttpRequest request)
        {
            var properties = this.GetType().GetProperties();
            //Các trường hợp xử lý đặc biệt
            List<string> ltsRemove = new List<string>(){
                "ListFileAttach",
                "ListFileAttachAdd",
                "ListFileRemove",
                "hdfUrlImages",
                "srcImageNews",
                "srcAnhDaiDien",
                "srcAnhSuKien",
                "srcEventImages",
                "_ModerationStatus",
                "srcTC_Image",
                "srcImagesBook",
                "srcBook_AnhDaiDien",
                "Modified",
                "Created",
                "Editor",
                "Author"
            };

            #region lấy về file
            //lấy về danh sách file đính kèm
            if (!string.IsNullOrEmpty(request["listValueFileAttach"]))
            {
                this.ListFileAttachAdd = new List<FileAttach>();
                string strListFileAttach = request["listValueFileAttach"];

                List<FileAttachForm> ltsFileForm = Newtonsoft.Json.JsonConvert.DeserializeObject<List<FileAttachForm>>(strListFileAttach);
                FileAttach fileAttach;
                string filePath = "/Uploads/ajaxUpload/";
                foreach (FileAttachForm fileForm in ltsFileForm)
                {
                    fileAttach = new FileAttach();
                    fileAttach.Name = fileForm.FileName;
                    fileAttach.Url = filePath + fileForm.FileServer;
                    fileAttach.DataFile = clsFucUtils.ReadFile(HttpContext.Current.Server.MapPath(filePath + fileForm.FileServer));
                    this.ListFileAttachAdd.Add(fileAttach);
                }
            }
            //Lấy về danh sách file xóa
            if (!string.IsNullOrEmpty(request["listValueRemoveFileAttach"]))
            {
                string[] tempLinkList = request["listValueRemoveFileAttach"].Split('#');
                for (int indexLink = 0; indexLink < tempLinkList.Length; indexLink++)
                {
                    if (!string.IsNullOrEmpty(tempLinkList[indexLink]))
                        this.ListFileRemove.Add(tempLinkList[indexLink]);
                }
            }
            #endregion
            string values;

            foreach (System.Reflection.PropertyInfo pinfo in properties.Where(p => !ltsRemove.Contains(p.Name)).ToList())
            {
                if (request["" + pinfo.Name + ""] != null) //Chỉ xét các tham số được post đi.
                {
                    values = request["" + pinfo.Name + ""];
                    try
                    {
                        Type objType = pinfo.GetType();
                        //Check các kiểu dữ liệu gán vào object
                        switch (pinfo.PropertyType.FullName)
                        {
                            #region MyRegion
                            case "Microsoft.SharePoint.SPFieldLookupValue":
                                SPFieldLookupValue splookupValue;
                                if (!string.IsNullOrEmpty(values))
                                    splookupValue = new SPFieldLookupValue(Convert.ToInt32(values), "");
                                else
                                    splookupValue = new SPFieldLookupValue();
                                pinfo.SetValue(this, splookupValue, null);
                                break;
                            case "Microsoft.SharePoint.SPFieldLookupValueCollection":
                                if (!string.IsNullOrEmpty(values))
                                    pinfo.SetValue(this, clsFucUtils.StringToLookup(values), null);
                                else
                                    pinfo.SetValue(this, new SPFieldLookupValueCollection(), null);
                                break;
                            case "System.Boolean":
                                {
                                    if (!string.IsNullOrEmpty(values))
                                        pinfo.SetValue(this, Convert.ToBoolean(values), null);
                                    else
                                        pinfo.SetValue(this, false, null);
                                    break;
                                }
                            case "System.Double":
                                if (!string.IsNullOrEmpty(values))
                                    pinfo.SetValue(this, Convert.ToDouble(values), null);
                                break;
                            case "System.Int32":
                                if (!string.IsNullOrEmpty(values))
                                    pinfo.SetValue(this, Convert.ToInt32(values), null);
                                break;
                            case "System.Nullable`1[[System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]"://"System.Nullable`1[[System.DateTime, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]":
                            case "System.DateTime ?":
                            case "System.DateTime":
                                if (!string.IsNullOrEmpty(values))
                                {
                                    //Định dạng ngày tháng
                                    dtfi.ShortDatePattern = "dd/MM/yyyy";
                                    dtfi.DateSeparator = "/";
                                    string temp = pinfo.Name;
                                    //Lấy giá trị request
                                    //Lấy kiểu dữ liệu
                                    DateTime dtTemp = Convert.ToDateTime(values, dtfi);
                                    if (dtTemp.Year < 1900 && dtTemp.Year > 2099)
                                        dtTemp = new DateTime(2012, 2, 30);
                                    pinfo.SetValue(this, ((DateTime)dtTemp), null);
                                }
                                else
                                {
                                    //;\Convert.ChangeType(value, propertyInfo.PropertyType),
                                    pinfo.SetValue(this, null, null);
                                }
                                break;
                            default:
                            case "System.String":
                                if (!string.IsNullOrEmpty(values))
                                {
                                    pinfo.SetValue(this, ((string)values).Trim(), null);
                                }
                                else
                                {
                                    pinfo.SetValue(this, "", null);
                                }
                                break;
                                #endregion
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new UpdateException(string.Format("Lỗi tại trường <----{0}--->, value:---{1}---", pinfo.Name, values.Trim()));
                    }
                }

            }
        }


        /// <summary>
        /// Set giá trị request cho object
        /// </summary>
        /// <param name="request">Truyền vào biến Request (this.Request)</param>
        public void UpdateObjectOnlyFile(HttpRequest request, string FileName)
        {
            var properties = this.GetType().GetProperties();
            //Các trường hợp xử lý đặc biệt
            List<string> ltsRemove = new List<string>(){
                "ListFileAttach",
                "ListFileAttachAdd",
                "ListFileRemove",
                "hdfUrlImages",
                "srcImageNews",
                "srcAnhDaiDien",
                "srcAnhSuKien",
                "srcEventImages",
                "_ModerationStatus",
                "srcTC_Image",
                "srcImagesBook",
                "srcBook_AnhDaiDien",
                "Modified",
                "Created",
                "Editor",
                "Author"
            };

            #region lấy về file
            //lấy về danh sách file đính kèm
            if (!string.IsNullOrEmpty(request["listValueFileAttach" + FileName] ))
            {
                this.ListFileAttachAdd = new List<FileAttach>();
                string strListFileAttach = request["listValueFileAttach" + FileName];

                List<FileAttachForm> ltsFileForm = Newtonsoft.Json.JsonConvert.DeserializeObject<List<FileAttachForm>>(strListFileAttach);
                FileAttach fileAttach;
                string filePath = "/Uploads/ajaxUpload/";
                foreach (FileAttachForm fileForm in ltsFileForm)
                {
                    fileAttach = new FileAttach();
                    fileAttach.Name = fileForm.FileName;
                    fileAttach.Url = filePath + fileForm.FileServer;
                    fileAttach.DataFile = clsFucUtils.ReadFile(HttpContext.Current.Server.MapPath(filePath + fileForm.FileServer));
                    this.ListFileAttachAdd.Add(fileAttach);
                }
            }
            //Lấy về danh sách file xóa
            if (!string.IsNullOrEmpty(request["listValueRemoveFileAttach" + FileName]))
            {
                string[] tempLinkList = request["listValueRemoveFileAttach" + FileName].Split('#');
                for (int indexLink = 0; indexLink < tempLinkList.Length; indexLink++)
                {
                    if (!string.IsNullOrEmpty(tempLinkList[indexLink]))
                        this.ListFileRemove.Add(tempLinkList[indexLink]);
                }
            }
            #endregion
        }
    }
    public class UpdateException : Exception
    {
        public UpdateException()
        {
        }

        public UpdateException(string message)
            : base(message)
        {
        }

        public UpdateException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
    public class DateTimeExcep : Exception
    {
        public override string ToString()
        {
            return base.ToString();
        }
        public override string Message
        {
            get
            {
                return "Sai định dạng dd/mm/yyyy hoặc ngày không tồn tại";
            }
        }
    }

    public class EntityJson
    {
        public string UrlList { get; set; }
        public string UrlListFull { get; set; }
        public int ID { get; set; }
        public string Title { get; set; }
        public List<LookupData> UserPhuTrach { get; set; }
        public string WFTrangThai { get; set; }
        public string WFTrangThaiText
        {
            get
            {
                if (!string.IsNullOrEmpty(WFTrangThai))
                {
                    return GlobalDataUtils.LstTrangThai.FirstOrDefault(x => x.Key == WFTrangThai).Value;
                }
                return "Chờ xuất bản";
            }
        }
        public List<BaseFileAttach> ListFileAttach { get; set; }
        public LookupData Author { get; set; }
        public LookupData Editor { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
        public virtual SPModerationStatusType _ModerationStatus { get; set; }
    }


    public class TreeViewItem
    {
        //sangvd add
        public string email { get; set;  }
        public string diachi { get; set; }
        public string dienthoai { get; set; }
        public bool duyet { get; set; }
        //end sangvd
        public string title { get; set; }
        public string mota { get; set; }
        public string key { get; set; }
        public string madinhdanh { get; set; }
        public List<TreeViewItem> children { get; set; }
        public bool folder { get; set; }
        public bool expanded { get; set; }
        public bool selected { get; set; }
        public bool lazy { get; set; }
        public bool groupActive { get; set; }
        public string extraClasses { get; set; }
        public int IDDaiDien { get; set; }
        public bool groupHasVanBanPhong { get; set; }
        public bool notSelect { get; set; }
        public int phanloai { get; set; }
        public List<LookupData> LtsUser { get; set; }
        public TreeViewItem()
        {
            //children = new List<TreeViewItem>();
            expanded = false;
            selected = false;
            folder = false;
            extraClasses = string.Empty;
            lazy = true;
            LtsUser = new List<LookupData>();
            children = new List<TreeViewItem>();
        }

        public TreeViewItem(string title, string key, List<TreeViewItem> children, bool folder = false, bool expanded = true, bool selected = false, bool notSelect = false, string extraClasses = "")
        {
            this.title = title;
            this.key = key;
            this.children = children;
            this.folder = folder;
            this.expanded = expanded;
            this.selected = selected;
            this.notSelect = notSelect;
            this.extraClasses = extraClasses;
        }

    }
}
