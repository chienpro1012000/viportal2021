﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ViPortal_Utils.Base
{
    /// <summary>
    /// đây là class config thông tin, biểu thức của excel.
    /// </summary>
    public class ConfigExcel : SPEntity
    {
        public int Type1_DongBatDau { get; set; }
        public int Type1_DongKetThuc { get; set; }
        public int Type1_DongBatDauFotter { get; set; }
        public int Type1_DongKetThucFotter { get; set; }
        //mảng json config công thức.
        public string ConfigCell { get; set; }
        public int Type1_STT { get; set; }
        public bool Type1_HasSTT { get; set; }
        public ConfigExcel()
        {
            Type1_STT = 0;
        }
    }
    public class ConfigCell
    {
        public string KeyCell { get; set; }
        public string CongThuc { get; set; }

    }
    public class clsUtils
    {
    }
    public class ObjFIle
    {
        public string Title { get; set; }
        public DateTime TimeEdit { get; set; }
        public long Size { get; set; }
        public ObjFIle(string _title, DateTime _timeedit, long _site)
        {
            Title = _title;
            TimeEdit = _timeedit;
            Size = _site;
        }
    }
    public class LookupData
    {
        int intID;
        public int ID
        {
            get { return intID; }
            set { intID = value; }
        }

        string strTitle;
        public string Title
        {
            get { return strTitle; }
            set { strTitle = value; }
        }

        public LookupData(int id, string title)
        {
            this.Title = title;
            this.ID = id;
        }
        public LookupData()
        {
            ID = 0;
            Title = string.Empty;
        }
    }
    public class FileAttach
    {
        private string strName;
        public string Name
        {
            get { return strName; }
            set { strName = value; }
        }

        private string strUrl;
        public string Url
        {
            get { return strUrl; }
            set { strUrl = value; }
        }

        private byte[] byteDataFile;

        public byte[] DataFile
        {
            get { return byteDataFile; }
            set { byteDataFile = value; }
        }
        public FileAttach(string name, string url)
        {
            Name = name;
            Url = url;
        }

        public FileAttach(string name, byte[] data)
        {
            this.Name = name;
            this.DataFile = data;
        }

        public FileAttach()
        {
            Name = string.Empty;
            Url = string.Empty;
        }
    }
    public class BaseFileAttach
    {

        public string Name
        {
            get;
            set;
        }


        public string Url
        {
            get;
            set;
        }
    }
    [Serializable]
    public class FileAttachForm
    {
        private string fileName;
        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }
        private string fileServer;

        public string FileServer
        {
            get { return fileServer; }
            set { fileServer = value; }
        }

        public FileAttachForm()
        {

        }

        public FileAttachForm(string _filename, string _fileserver)
        {
            fileName = _filename;
            fileServer = _fileserver;
        }
    }
    public enum ActionState
    {
        Succeed,
        Warning,
        Error
    }
    public class ResultAction
    {
        public ActionState State { set; get; }
        public string Source { get; set; }
        public string Message { get; set; }
        public int TotalCount { set; get; }
        public int EffectCount { set; get; }
        public int SucceedCount { set; get; }
        public List<int> Ids { set; get; }
        private object odata;
        public object OData { get { return odata; } set { odata = value; IsQuery = true; } }
        public bool IsQuery { get; set; }
        public int ID { get; set; }
        public ResultAction()
        {
            Ids = new List<int>();
            State = ActionState.Succeed;
        }
        public ResultAction(ActionState _State , object OData)
        {
            Ids = new List<int>();
            State = _State;
            odata = OData;
        }
        public void ResponseData()
        {
            var oTT = IsQuery ? OData : this;
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = "application/json; charset=utf-8";
            HttpContext.Current.Response.Write(JsonConvert.SerializeObject(oTT));
            HttpContext.Current.Response.End();
        }

    }
    /// <summary>
    /// SỬA GÌ Ở ĐÂY PHẢI HỎI ADMIN
    /// </summary>
    public class DataGridRender
    {
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public int draw { get; set; }
        public object data { get; set; }
        public string Query { get; set; }
        public string result { get; set; }
        public DataGridRender() { }
        public DataGridRender(object oData, int oDraw, int oRecordsTotal)
        {
            data = oData;
            draw = oDraw;
            recordsTotal = oRecordsTotal;
            recordsFiltered = oRecordsTotal;
        }

    }

    public class DataBaoCaoRender
    {
        public string KeToanTruong { get; set; }
        public string GiTruTieuDe { get; set; }
        public string SoBM { get; set; }
        public string TieuDeGiua1 { get; set; }
        public string TieuDeGiua2 { get; set; }
        public string TieuDeGiua3 { get; set; }
        public string ToNangLuongTaiTao { get; set; }
        public string ToTongHop { get; set; }
        public string DVTinh { get; set; }
        public string NguoiLapBieu { get; set; }
        public string Nam { get; set; }
        public string Thang { get; set; }
        public string LoaiBM { get; set; }
        public string NguoiKy { get; set; }
        public string TieuDeTrai1 { get; set; }
        public string TieuDeTrai2 { get; set; }
        public string TieuDeTrai3 { get; set; }
        public string Title { get; set; }

        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public int draw { get; set; }
        public object data { get; set; }
        public string Query { get; set; }
        public string result { get; set; }
        public DataBaoCaoRender() { }
        public DataBaoCaoRender(object oData, int oDraw, int oRecordsTotal)
        {
            data = oData;
            draw = oDraw;
            recordsTotal = oRecordsTotal;
            recordsFiltered = oRecordsTotal;
        }

    }
    public class GridRequest
    {
        public GridRequest(HttpContext context)
        {
            LstItemID = new List<int>();
            int draw = 0, start = 0, length = 0, id = 0;
            int.TryParse(context.Request["draw"], out draw);
            int.TryParse(context.Request["start"], out start);
            if (!string.IsNullOrEmpty(context.Request["length"]))
                int.TryParse(context.Request["length"], out length);
            else length = 20;

            if (!string.IsNullOrEmpty(context.Request["LstItemid"]))
            {
                LstItemID = clsFucUtils.GetDanhSachIDsQuaFormPost(context.Request["LstItemid"]);
            }

            int.TryParse(context.Request["ItemID"], out id);
            ItemID = id;
            Draw = draw;
            Start = start;
            Length = length;
            Ascending = context.Request["order[0][dir]"] == "desc" ? false : true;
            string indexCol = context.Request["order[0][column]"];
            if (indexCol != null && indexCol.Length > 0)
            {
                FieldOrder = context.Request[string.Format("columns[{0}][name]", indexCol)];
                if (FieldOrder == null || FieldOrder.Length == 0)
                    FieldOrder = context.Request[string.Format("columns[{0}][data]", indexCol)];
            }
            else
            {
                FieldOrder = "ID";
                Ascending = false;
            }
        }

        public GridRequest(HttpRequest oRequest)
        {
            LstItemID = new List<int>();
            int draw = 0, start = 0, length = 0, id = 0;
            int.TryParse(oRequest["draw"], out draw);
            int.TryParse(oRequest["start"], out start);
            if (!string.IsNullOrEmpty(oRequest["length"]))
                int.TryParse(oRequest["length"], out length);
            else length = 20;
            int.TryParse(oRequest["ItemID"], out id);
            ItemID = id;

            if (!string.IsNullOrEmpty(oRequest["LstItemid"]))
            {
                LstItemID = clsFucUtils.GetDanhSachIDsQuaFormPost(oRequest["LstItemid"]);
            }
            Draw = draw;
            Start = start;
            Length = length;
            Ascending = oRequest["order[0][dir]"] == "desc" ? false : true;
            string indexCol = oRequest["order[0][column]"];
            if (indexCol != null && indexCol.Length > 0)
            {
                FieldOrder = oRequest[string.Format("columns[{0}][name]", indexCol)];
                if (FieldOrder == null || FieldOrder.Length == 0)
                    FieldOrder = oRequest[string.Format("columns[{0}][data]", indexCol)];
            }
            else
            {
                if (!string.IsNullOrEmpty(oRequest["FieldOrder"]))
                {
                    FieldOrder = oRequest["FieldOrder"];
                }
                else FieldOrder = "ID";
                if (!string.IsNullOrEmpty(oRequest["Ascending"]))
                {
                    Ascending = Convert.ToBoolean(oRequest["Ascending"]);
                }
                else Ascending = false;
            }
        }

        public int ItemID { get; set; }
        public List<int> LstItemID { get; set; }

        public int Draw { get; set; }

        public int Start { get; set; }

        public int Length { get; set; }

        public bool Ascending { get; set; }

        public string FieldOrder { get; set; }
    }
    public sealed class UserPermission
    {
        public bool IsAdmin { get; set; }
        public bool AddItem { get; set; }
        public bool EditItem { get; set; }
        public bool DeleteItem { get; set; }
        public bool ApprovedItem { get; set; }
        public UserPermission()
        {
            IsAdmin = true;
            AddItem = true;
            EditItem = true;
            DeleteItem = true;
            ApprovedItem = true;
        }
    }
    public class UploadFilesResult
    {
        public string name { get; set; }
        public int length { set; get; }
        public string type { set; get; }
        public string pathFile { set; get; }
        public bool error { set; get; }
        public string errorMessage { set; get; }
        public string thumbnailUrl { get; set; }
        public string deleteUrl { get; set; }
        public string url { get; set; }
        public int size { get; set; }
        public string deleteType { get; set; }
    }
    public enum UpdateType
    {
        Update,
        SystemUpdate,
        UpdateOverwriteVersion
    }
    public class VPaging
    {

        #region Các thuộc tính
        public int PageStep { set; get; }

        public int TotalPage { set; get; }
        public int CurrentPage { set; get; }

        public string LinkPage { set; get; }
        public string LinkPageExt { set; get; }
        #endregion

        public VPaging()
        {
            CurrentPage = 1;
            LinkPage = string.Empty;
            TotalPage = 1;
            PageStep = 3;
            LinkPageExt = string.Empty;
        }

        public string GetPaging(string urlPage, int pageStep, int currentPage, int rowPerPage, int totalItemsCount)
        {
            PageStep = pageStep;
            CurrentPage = currentPage;
            LinkPage = urlPage;
            rowPerPage = (rowPerPage == 0) ? 10 : rowPerPage;
            TotalPage = (totalItemsCount % rowPerPage == 0) ? totalItemsCount / rowPerPage : ((totalItemsCount - (totalItemsCount % rowPerPage)) / rowPerPage) + 1;
            return BuildPaging();
        }
        public string GetPagingJS(string urlPage, int pageStep, int currentPage, int rowPerPage, int totalItemsCount)
        {
            PageStep = pageStep;
            CurrentPage = currentPage;
            LinkPage = urlPage;
            rowPerPage = (rowPerPage == 0) ? 10 : rowPerPage;
            TotalPage = (totalItemsCount % rowPerPage == 0) ? totalItemsCount / rowPerPage : ((totalItemsCount - (totalItemsCount % rowPerPage)) / rowPerPage) + 1;
            return BuildPagingJS();
        }
        private string BuildPagingJS()
        {
            string strPageHTML = "<ul class=\"pagination\">";
            if (CurrentPage > PageStep + 1)
            {
                strPageHTML += "<li><a data-page=\"" + 1 + "\" href=\"" + LinkPage + 1 + LinkPageExt + "\">« Đầu</a></li>";
                strPageHTML += "<li><a data-page=\"" + (CurrentPage - 1) + "\" href=\"" + LinkPage + (CurrentPage - 1) + LinkPageExt + "\">Trước</a></li>";
                //strPageHTML += "<span>...</span>";
            }

            int BeginFor = ((CurrentPage - PageStep) > 1) ? (CurrentPage - PageStep) : 1;
            int EndFor = ((CurrentPage + PageStep) > TotalPage) ? TotalPage : (CurrentPage + PageStep);

            for (int pNumber = BeginFor; pNumber <= EndFor; pNumber++)
            {
                if (pNumber == CurrentPage)
                    strPageHTML += "<li class=\"active\"><a data-page=\"" + pNumber + "\" href=\"javascript:;\" class=\"current\">" + pNumber + "</a></li>";
                else
                    strPageHTML += "<li><a data-page=\"" + pNumber + "\" href=\"" + LinkPage + pNumber + LinkPageExt + "\">" + pNumber + "</a></li>";
            }

            if (CurrentPage < (TotalPage - PageStep))
            {
                //strPageHTML += "<span>...</span>";
                strPageHTML += "<li><a data-page=\"" + (CurrentPage + 1) + "\" href=\"" + LinkPage + (CurrentPage + 1) + LinkPageExt + "\">Sau</a></li>";
                strPageHTML += "<li><a data-page=\"" + TotalPage + "\" href=\"" + LinkPage + TotalPage + LinkPageExt + "\">Cuối »</a></li>";
            }
            strPageHTML += "</ul>";
            if (TotalPage > 1)
                return strPageHTML;
            else
                return string.Empty;
        }
        private string BuildPaging()
        {
            string strPageHTML = "<ul class=\"pagination\">";
            if (CurrentPage > PageStep + 1)
            {
                strPageHTML += "<li><a data-page=\"" + 1 + "\" href=\"" + LinkPage + 1 + LinkPageExt + "\">« Đầu</a></li>";
                strPageHTML += "<li><a data-page=\"" + (CurrentPage - 1) + "\" href=\"" + LinkPage + (CurrentPage - 1) + LinkPageExt + "\">Trước</a></li>";
                //strPageHTML += "<span>...</span>";
            }

            int BeginFor = ((CurrentPage - PageStep) > 1) ? (CurrentPage - PageStep) : 1;
            int EndFor = ((CurrentPage + PageStep) > TotalPage) ? TotalPage : (CurrentPage + PageStep);

            for (int pNumber = BeginFor; pNumber <= EndFor; pNumber++)
            {
                if (pNumber == CurrentPage)
                    strPageHTML += "<li class=\"active\"><a data-page=\"" + pNumber + "\" href=\"javascript:;\" class=\"current\">" + pNumber + "</a></li>";
                else
                    strPageHTML += "<li><a data-page=\"" + pNumber + "\" href=\"" + LinkPage + pNumber + LinkPageExt + "\">" + pNumber + "</a></li>";
            }

            if (CurrentPage < (TotalPage - PageStep))
            {
                //strPageHTML += "<span>...</span>";
                strPageHTML += "<li><a data-page=\"" + (CurrentPage + 1) + "\" href=\"" + LinkPage + (CurrentPage + 1) + LinkPageExt + "\">Sau</a></li>";
                strPageHTML += "<li><a data-page=\"" + TotalPage + "\" href=\"" + LinkPage + TotalPage + LinkPageExt + "\">Cuối »</a></li>";
            }
            strPageHTML += "</ul>";
            if (TotalPage > 1)
                return strPageHTML;
            else
                return string.Empty;
        }
    }


    public class Message
    {
        public string[] registration_ids { get; set; }
        public object notification { get; set; }
        public object data { get; set; }
    }
}
