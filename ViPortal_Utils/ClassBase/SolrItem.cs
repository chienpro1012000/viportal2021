﻿using Microsoft.SharePoint;
using SolrNet.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViPortal_Utils
{
    public class SolrItem
    {
        [SolrField()]
        public string Title { get; set; }
        [SolrField()]
        public int SP_ID { get; set; }
        [SolrField()]
        public string id { get; set; }
        [SolrField()]
        public string Content { get; set; }
        [SolrField()]
        public DateTime? Created { get; set; }
        [SolrField()]
        public DateTime? Modified { get; set; }
        [SolrField()]
        public string UrlList { get; set; }
        [SolrField()]
        public string ListName { get; set; }
        [SolrField()]
        public int Type_List { get; set; }
        [SolrField()]
        public string UrlView { get; set; }
        [SolrField()]
        public int _ModerationStatus { get; set; }
        public SolrItem()
        {

        }
        public SolrItem(SPListItem ospitem, int TypeList = 1)
        {
            ospitem = ospitem.ParentList.GetItemById(ospitem.ID);
            _ModerationStatus = (int)ospitem.ModerationInformation.Status;
            Type_List = TypeList;
            SP_ID = ospitem.ID;
            Title = ospitem.Title;
            UrlList = ospitem.ParentList.RootFolder.ServerRelativeUrl;
            ListName = UrlList.Split('/')[UrlList.Split('/').Length - 1];
            id = string.Format("{0}_{1}_{2}", Type_List, ListName, SP_ID);
            switch (TypeList)
            {
                case (int)viType_List.TinTuc:
                    UrlView = UrlList.Replace("Lists", "Pages") + ".aspx";
                    break;
                case (int)viType_List.VanBan:
                    UrlView = "/noidung/vanban/Pages/vanbanphapquy.aspx";
                    break;
                case (int)viType_List.TTHC:
                    UrlView = "/noidung/tthc/Pages/thutuchanhchinh.aspx";
                    break;
            }
            Created = Convert.ToDateTime(ospitem["Created"]);
            Modified = Convert.ToDateTime(ospitem["Modified"]);
            List<string> lstValues = new List<string>();
            //Fix cho thằng content
            foreach (string oFieldName in LstFieldConent)
            {

                if (ospitem.Fields.ContainsField(oFieldName) && ospitem[oFieldName] != null)
                {
                    lstValues.Add(ospitem[oFieldName].ToString());
                }
            }
            Content = string.Join("\r\n", lstValues);
        }
        public static List<string> LstFieldConent =
            new List<string>() { 
                "DescriptionNews", "ContentNews",
                "TTHC_CachThucThucHien", "TTHC_CanCuPhapLy", "TTHC_CoQuanThucHien", "TTHC_MauDonToKhai", "TTHC_ThanhPhanHoSo", "TTHC_ThoiHanGiaiQuyet", "TTHC_TrinhTuThucHien", "TTHC_YeuCauThucHien",
                "TrichYeu", "LoaiVanBan","CoQuanBanHanh"
            };
    }

    public enum viType_List { TinTuc = 1, VanBan = 2, TTHC = 3 };
}
