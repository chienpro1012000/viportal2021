﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViPortal_Utils.ClassBase
{
    public static class FormatUtils
    {
        public static string FormatMoney(string strMoney)
        {

            String result = "";

            if (!string.IsNullOrEmpty(strMoney))
            {
                double dMoney = double.Parse(strMoney, System.Globalization.CultureInfo.InvariantCulture);
                double dMoneyRouding = Math.Round(dMoney);
                int iMoney = Convert.ToInt32(dMoneyRouding);
                string strMoneyRounding = String.Format("{0:n0}", iMoney);
                result = strMoneyRounding;
            }

            return result;
        }
    }
}
