﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViPortal_Utils
{
    public static class  GlobalDataUtils
    {
        private static Dictionary<string, string> _lstTrangThai { get; set; }

        public static Dictionary<string, string> LstTrangThai
        {
            get
            {
                if (_lstTrangThai == null)
                {
                    _lstTrangThai = new Dictionary<string, string>();
                    string UrlSite = SPContext.Current.Site.Url;
                    using (SPSite oSite = new SPSite(UrlSite))
                    {
                        using (SPWeb oWeb = oSite.OpenWeb())
                        {
                            SPList SPListProcess = oWeb.GetList("/Lists/DMTrangThai");
                            foreach (SPListItem item in SPListProcess.Items)
                            {
                                _lstTrangThai.Add(item["DMVietTat"].ToString(), item.Title);
                            }
                        }
                    }
                    return _lstTrangThai;
                }
                else return _lstTrangThai;
            }
        }
    }
}
