﻿using Microsoft.SharePoint;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViPortal_Utils
{
    public class JsonConverterCus : Newtonsoft.Json.JsonConverter
    {

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return true;
        }
    }
    public class SPFieldLookupValueConverter : JsonConverter<SPFieldLookupValue>
    {
        public override void WriteJson(JsonWriter writer, SPFieldLookupValue value, JsonSerializer serializer)
        {
            writer.WriteValue(value.ToString());
        }

        public override SPFieldLookupValue ReadJson(JsonReader reader, Type objectType, SPFieldLookupValue existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            string s = (string)reader.Value;

            return new SPFieldLookupValue(s);
        }
    }

    public class SPFieldLookupValueCollectionConverter : JsonConverter<SPFieldLookupValueCollection>
    {
        public override void WriteJson(JsonWriter writer, SPFieldLookupValueCollection value, JsonSerializer serializer)
        {
            writer.WriteValue(value.ToString());
        }

        public override SPFieldLookupValueCollection ReadJson(JsonReader reader, Type objectType, SPFieldLookupValueCollection existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            string s = (string)reader.Value;

            return new SPFieldLookupValueCollection(s);
        }
    }
}
