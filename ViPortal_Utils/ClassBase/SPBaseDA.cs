﻿using CommonServiceLocator;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using SolrNet;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ViPortal_Utils.Base
{
    public class SolrBaseDA<T>
    {
        public ISolrOperations<T> solrWorkerData;
        public SolrBaseDA()
        {
            solrWorkerData = ServiceLocator.Current.GetInstance<ISolrOperations<T>>();
        }
    }
    public class SPBaseDA<U> where U : EntitySolr, new()
    {
        public ISolrOperations<U> solrWorkerDM;
        //public ISolrOperations<DataSolr> solrWorkerData;
        //public ISolrOperations<LogHeThongSolr> solrWorkerLog;
        public bool isHasSolr = false;
        public int Type_List { get; set; }
        public SPBaseDA(string _urlListProcess, bool isSolr = true)
        {
            Init(_urlListProcess, isSolr);
        }
        public SPBaseDA()
        {
            solrWorkerDM = ServiceLocator.Current.GetInstance<ISolrOperations<U>>();
        }
        public void InitAdmin(string _urlListProcess, bool isSolr = true)
        {
            //HoanNT - Checks trường hợp phải là Document,Pages, Libarary...., Not List  
            //Chinh sua
            //var fullPath = SPContext.Current.Site.Url + _urlListProcess + "/AllItems.aspx";
            var fullPath = "http://localhost:6003" + _urlListProcess;
            if (SPContext.Current != null)
                fullPath = SPContext.Current.Site.Url + _urlListProcess;
            string urlsite = "http://localhost:6003";
            if (SPContext.Current != null)
                urlsite = SPContext.Current.Site.Url;
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                string subsitefullpath = _urlListProcess.Split(new[] { "/Lists" }, StringSplitOptions.None)[0];
                using (SPSite cSite = new SPSite(urlsite))
                {
                    using (SPWeb cWeb = cSite.OpenWeb(subsitefullpath))
                    {
                        //Su dung ham get GetList 
                        SpListProcess = cWeb.GetList(_urlListProcess);
                        FullPathUrlListProcess = _urlListProcess;
                    };
                };
                SpListProcess.ParentWeb.AllowUnsafeUpdates = true;
            });
            isHasSolr = isSolr;
            if (isSolr)
            {
                solrWorkerDM = ServiceLocator.Current.GetInstance<ISolrOperations<U>>();
            }
        }
        public void Init(string _urlListProcess, bool isSolr = true)
        {
            //HoanNT - Checks trường hợp phải là Document,Pages, Libarary...., Not List  
            //Chinh sua
            //var fullPath = SPContext.Current.Site.Url + _urlListProcess + "/AllItems.aspx";
            var fullPath = "http://localhost:6003" + _urlListProcess;
            if (SPContext.Current != null)
                fullPath = SPContext.Current.Site.Url + _urlListProcess;

            using (SPSite cSite = new SPSite(fullPath))
            {
                using (SPWeb cWeb = cSite.OpenWeb())
                {
                    //Su dung ham get GetList 
                    SpListProcess = cWeb.GetList(_urlListProcess);
                    FullPathUrlListProcess = _urlListProcess;
                };
            };
            SpListProcess.ParentWeb.AllowUnsafeUpdates = true;
            isHasSolr = isSolr;
            if (isSolr)
            {
                solrWorkerDM = ServiceLocator.Current.GetInstance<ISolrOperations<U>>();
            }
        }
        /// <summary>
        /// get Bản ghi theo ID
        /// Author      Date        Comments
        /// CHINHPN     29/03
        /// </summary>
        /// <typeparam name="T">Lớp đối tượng</typeparam>
        /// <param name="id">ID của bản ghi</param>
        /// <returns></returns>
        public T GetByIdToObject<T>(int id) where T : SPEntity, new()
        {
            try
            {
                SPListItem item = SpListProcess.GetItemById(id);
                return ConvertToObject<T>(item);
            }
            catch (Exception ex)
            {
                throw new Exception($"{ex.Message}. SpListProcess:{SpListProcess.Title}/{SpListProcess.DefaultDisplayFormUrl}. Itemid: {id}");
            }
        }
        /// <summary>
        /// hàm get title
        /// </summary>
        /// <param name="id"></param>
        /// <param name="FieldReturn"></param>
        /// <returns></returns>
        public string GetTitleByID(int id, string FieldReturn = "Title")
        {
            SPListItem item = SpListProcess.GetItemByIdSelectedFields(id, FieldReturn);
            if (item[FieldReturn] != null)
                return item[FieldReturn].ToString();
            else return "";
        }
        public T GetByIdToObjectSelectFields<T>(int id, params string[] Fields) where T : SPEntity, new()
        {
            if (!Fields.Contains("ID"))
                Fields = Fields.Concat(new string[] { "ID" }).ToArray();
            SPListItem item = SpListProcess.GetItemByIdSelectedFields(id, Fields);
            return ConvertToObject<T>(item, Fields);
        }
        /// <summary>
        /// Remove Bản ghi theo ID
        /// Author      Date        Comments
        /// CHINHPN     30/03
        /// </summary>
        /// <typeparam name="T">Lớp đối tượng</typeparam>
        /// <param name="id">ID của bản ghi</param>
        /// <returns></returns>
        public string RemoveObject(int id)
        {
            try
            {
                SPListItem item = SpListProcess.GetItemById(id);
                item.Recycle();
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return null;
        }

        /// <summary>
        /// Delete bản ghi theo ID
        /// Author      Date        Comments
        /// VINHHQ     30/03
        /// </summary>
        /// <typeparam name="T">Lớp đối tượng</typeparam>
        /// <param name="id">ID của bản ghi</param>
        /// <returns></returns>
        public ResultAction DeleteObjectV2(int id)
        {
            ResultAction oResultAction = new ResultAction();
            try
            {
                SPListItem item = SpListProcess.GetItemByIdSelectedFields(id, "ID", "Title");
                oResultAction.Message = item.Title;
                item.Recycle();
                if (isHasSolr)
                {
                    string idSolr = (SpListProcess.ParentWeb.ServerRelativeUrl + "/" + SpListProcess.RootFolder.Url + "_" + id).Replace("/", "-");
                    solrWorkerDM.Delete(new SolrQuery($"id:\"{idSolr}\""));
                    solrWorkerDM.Commit();
                }
            }
            catch (Exception ex)
            {
                oResultAction = new ResultAction() { State = ActionState.Error, Message = ex.Message + ". " + ex.StackTrace };
            }
            return oResultAction;
        }
        /// <summary>
        /// Delete bản ghi theo ID
        /// Author      Date        Comments
        /// CHINHPN     30/03
        /// </summary>
        /// <typeparam name="T">Lớp đối tượng</typeparam>
        /// <param name="id">ID của bản ghi</param>
        /// <returns></returns>
        public string DeleteObject(int id)
        {
            try
            {
                SPListItem item = SpListProcess.GetItemByIdSelectedFields(id, "ID", "Title");
                item.Recycle();
                if (isHasSolr)
                {
                    string idSolr = (SpListProcess.ParentWeb.ServerRelativeUrl + "/" + SpListProcess.RootFolder.Url + "_" + id).Replace("/", "-");
                    solrWorkerDM.Delete(new SolrQuery($"id:\"{idSolr}\""));
                    solrWorkerDM.Commit();
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return null;
        }
        /// <summary>
        /// Thêm mới bản ghi
        /// Author      Date        Comments
        /// CHINHPN     29/03
        /// </summary>
        /// <typeparam name="T">Lớp đối tượng</typeparam>
        /// <returns></returns>
        public string UpdateObject<T>(T oTT) where T : SPEntity, new()
        {
            try
            {
                PropertyInfo[] pers = oTT.GetType().GetProperties();
                //var fieldNames = new HashSet<string>();
                //foreach (SPField f in SpListProcess.Fields)
                //   if (!f.ReadOnlyField) fieldNames.Add(f.InternalName);
                //Các trường hợp xử lý đặc biệt
                List<string> ltsRemove = new List<string>(){
                "ListFileAttach",
                "ListFileAttachAdd",
                "ListFileRemove",
                "ListFileAttachRemove",
                "roleListPermission_x003a_permist",
                "Created",
                "Modified",
                "Author",
                "Editor",
                "ID",
                "_ModerationStatus"
                };
                List<string> lstFieldsException = new List<string>() { "ColumnImage" };
                DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
                dtfi.ShortDatePattern = "dd/MM/yyyy";
                dtfi.DateSeparator = "/";
                var fieldId = oTT.GetType().GetProperty("ID");
                int id = fieldId == null ? 0 : Convert.ToInt32(fieldId.GetValue(oTT, null));
                SPListItem spItem;
                if (id == 0)
                    spItem = SpListProcess.AddItem();
                else
                    spItem = SpListProcess.GetItemById(id);
                #region thêm file đính kèm

                foreach (FileAttach file in oTT.ListFileAttachAdd)
                    spItem.Attachments.Add(file.Name, file.DataFile);

                foreach (var fileDelete in oTT.ListFileRemove)
                    spItem.Attachments.Delete(fileDelete);

                #endregion
                for (int i = 0; i < pers.Length; i++)
                {
                    if (SpListProcess.Fields.ContainsFieldWithStaticName(pers[i].Name) && !SpListProcess.Fields.GetFieldByInternalName(pers[i].Name).ReadOnlyField)
                    {
                        var values = pers[i].GetValue(oTT);
                        if ((values == null || ltsRemove.Contains(pers[i].Name)))
                            continue;
                        if (!lstFieldsException.Contains(pers[i].Name))
                        {
                            switch (pers[i].PropertyType.FullName)
                            {
                                #region Cập nhật trường thông tin
                                case "Microsoft.SharePoint.SPFieldLookupValue":
                                    SPFieldLookupValue splookupValue = new SPFieldLookupValue(values.ToString());
                                    if (splookupValue.LookupId > 0)
                                        spItem[pers[i].Name] = splookupValue.LookupId;
                                    else spItem[pers[i].Name] = null;
                                    break;
                                case "Microsoft.SharePoint.SPFieldLookupValueCollection":
                                    spItem[pers[i].Name] = values;
                                    break;
                                case "System.Nullable`1[[System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]":

                                case "System.DateTime ?":
                                case "System.DateTime":
                                    //Định dạng ngày tháng
                                    dtfi.ShortDatePattern = "MM/dd/yyyy";
                                    dtfi.DateSeparator = "/";
                                    //Lấy giá trị request
                                    //Lấy kiểu dữ liệu
                                    DateTime dtTemp = Convert.ToDateTime(values, dtfi);
                                    if (dtTemp.Year < 1900 && dtTemp.Year > 2099)
                                        dtTemp = new DateTime(2012, 2, 30);
                                    spItem[pers[i].Name.ToString()] = dtTemp;
                                    break;
                                default:
                                    spItem[pers[i].Name.ToString()] = pers[i].GetValue(oTT);
                                    break;
                                    #endregion
                            }
                        }
                        else
                        {
                            switch (pers[i].Name)
                            {
                                case "ImageNews":
                                    spItem["ImageNews"] = (!string.IsNullOrEmpty(values.ToString()) ? string.Format("<img src=\"{0}\" border=\"0\"/>", values.ToString()) : "");
                                    break;
                                case "ColumnImage":
                                    spItem["ColumnImage"] = (!string.IsNullOrEmpty(values.ToString()) ? string.Format("<img src=\"{0}\" border=\"0\"/>", values.ToString()) : "");
                                    break;
                            }
                        }
                    }
                }
                bool isUpdate = true;
                try
                {
                    #region MyRegion
                    //SPList oLConfig = SPContext.Current.Site.RootWeb.GetList("/Lists/Config");
                    //SPListItem ospListitem = oLConfig.GetItemById(1);
                    //if (ospListitem["Datetime"] != null)
                    //{
                    //    DateTime temp = Convert.ToDateTime(ospListitem["Datetime"]);
                    //    if (temp.CompareTo(DateTime.Now.Date) < 0)
                    //    {
                    //        isUpdate = false;
                    //    }
                    //} 
                    #endregion
                }
                catch (Exception ex)
                {

                }
                if (isUpdate)
                {

                    spItem.Update();
                    if (isHasSolr)
                    {
                        U oDanhMucSolr = new U();
                        oDanhMucSolr.SPToSolr(spItem);
                        solrWorkerDM.Add(oDanhMucSolr);
                        solrWorkerDM.Commit();
                    }
                    oTT.ID = spItem.ID;
                }
                else
                {
                    return "Đã có lỗi xảy ra trong quá trình cập nhật";
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return null;
        }
        public T ConvertToObject<T>(SPListItem spItem, List<string> Fields) where T : SPEntity, new()
        {
            T temp;
            IEnumerable<System.Reflection.PropertyInfo> lstPropertyDM = typeof(T).GetProperties();
            //Các trường hợp xử lý đặc biệt
            List<string> ltsRemove = new List<string>(){
                "ListFileAttach",
                "ListFileAttachAdd",
                "ListFileRemove",
            };
            lstPropertyDM = lstPropertyDM.Where(x => !ltsRemove.Contains(x.Name));
            DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
            dtfi.ShortDatePattern = "dd/MM/yyyy";
            dtfi.DateSeparator = "/";
            //System.Reflection.PropertyInfo pinfoSpec;
            temp = new T();
            ///Lấy riêng một số trường đặc biệt Attachment
            ///
            if (Fields.Contains("Attachments"))
                temp.ListFileAttach = getListAttackFileBySPListItem(spItem);
            #region các trường bình thường
            lstPropertyDM = lstPropertyDM.Where(x => Fields.Contains(x.Name));
            foreach (PropertyInfo pinfo in lstPropertyDM)
            {
                if (spItem[pinfo.Name] != null)
                {
                    string values = spItem[pinfo.Name].ToString();
                    values = spItem[pinfo.Name].ToString();
                    switch (pinfo.PropertyType.FullName)
                    {
                        #region Cập nhật trường thông tin
                        case "Microsoft.SharePoint.SPModerationInformation":
                            pinfo.SetValue(temp, spItem.ModerationInformation, null);
                            break;
                        case "Microsoft.SharePoint.SPModerationStatusType":
                            if (spItem.ModerationInformation != null)
                                pinfo.SetValue(temp, spItem.ModerationInformation.Status, null);
                            break;
                        case "Microsoft.SharePoint.SPFieldLookupValue":
                            SPFieldLookupValue splookupValue;
                            if (!string.IsNullOrEmpty(values))
                            {
                                try
                                {
                                    splookupValue = new SPFieldLookupValue(values);
                                }
                                catch
                                {
                                    splookupValue = new SPFieldLookupValue(Convert.ToInt32(values.Split(',')[1]), "");
                                }
                            }
                            else
                                splookupValue = new SPFieldLookupValue();
                            pinfo.SetValue(temp, splookupValue, null);
                            break;
                        case "Microsoft.SharePoint.SPFieldLookupValueCollection":
                            if (!string.IsNullOrEmpty(values))
                                pinfo.SetValue(temp, new SPFieldLookupValueCollection(values), null);
                            else
                                pinfo.SetValue(temp, new SPFieldLookupValueCollection(), null);
                            break;
                        case "System.Boolean":
                            {
                                if (!string.IsNullOrEmpty(values))
                                    pinfo.SetValue(temp, Convert.ToBoolean(values), null);
                                else
                                    pinfo.SetValue(temp, false, null);
                                break;

                            }
                        case "System.Double":
                            if (!string.IsNullOrEmpty(values))
                                pinfo.SetValue(temp, Convert.ToDouble(values), null);
                            break;

                        case "System.Int32":
                            if (!string.IsNullOrEmpty(values))
                                pinfo.SetValue(temp, Convert.ToInt32(values), null);
                            break;

                        case "System.Nullable`1[[System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]":
                        case "System.DateTime ?":
                        case "System.DateTime":
                            if (!string.IsNullOrEmpty(values))
                            {
                                //Định dạng ngày tháng
                                dtfi.ShortDatePattern = "MM/dd/yyyy";
                                dtfi.DateSeparator = "/";
                                //Lấy giá trị request
                                //Lấy kiểu dữ liệu
                                DateTime dtTemp = Convert.ToDateTime(values, dtfi);
                                if (dtTemp.Year < 1900 && dtTemp.Year > 2099)
                                    dtTemp = new DateTime(2012, 2, 30);
                                pinfo.SetValue(temp, ((DateTime)dtTemp), null);
                            }
                            else
                            {
                                pinfo.SetValue(temp, null, null);
                            }
                            break;

                        default:
                        case "System.String":
                            if (!string.IsNullOrEmpty(values))
                            {
                                pinfo.SetValue(temp, ((string)values).Trim(), null);
                            }
                            else
                            {
                                pinfo.SetValue(temp, "", null);
                            }
                            break;
                            #endregion
                    }
                }
            }
            #endregion
            return temp;
        }
        /// <summary>
        /// Convert SPListItem to object
        /// Author      Date        Comments
        /// CHINHPN     29/03
        /// </summary>
        /// <typeparam name="T">Lớp đối tượng</typeparam>
        /// <returns></returns>
        public T ConvertToObject<T>(SPListItem spItem, params string[] Fields) where T : SPEntity, new()
        {
            T temp;
            IEnumerable<System.Reflection.PropertyInfo> lstPropertyDM = typeof(T).GetProperties();
            //Các trường hợp xử lý đặc biệt
            List<string> ltsRemove = new List<string>(){
                "ListFileAttach",
                "ListFileAttachAdd",
                "ListFileRemove",
            };
            lstPropertyDM = lstPropertyDM.Where(x => !ltsRemove.Contains(x.Name));
            DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
            dtfi.ShortDatePattern = "dd/MM/yyyy";
            dtfi.DateSeparator = "/";
            //System.Reflection.PropertyInfo pinfoSpec;
            temp = new T();
            ///Lấy riêng một số trường đặc biệt Attachment
            ///
            if (Fields.Contains("Attachments"))
                temp.ListFileAttach = getListAttackFileBySPListItem(spItem);
            #region các trường bình thường
            lstPropertyDM = lstPropertyDM.Where(x => Fields.Contains(x.Name));
            foreach (PropertyInfo pinfo in lstPropertyDM)
            {
                if (spItem[pinfo.Name] != null)
                {
                    string values = spItem[pinfo.Name].ToString();
                    values = spItem[pinfo.Name].ToString();
                    switch (pinfo.PropertyType.FullName)
                    {
                        #region Cập nhật trường thông tin
                        case "Microsoft.SharePoint.SPModerationInformation":
                            pinfo.SetValue(temp, spItem.ModerationInformation, null);
                            break;
                        case "Microsoft.SharePoint.SPModerationStatusType":
                            if (spItem.ModerationInformation != null)
                                pinfo.SetValue(temp, spItem.ModerationInformation.Status, null);
                            break;
                        case "Microsoft.SharePoint.SPFieldLookupValue":
                            SPFieldLookupValue splookupValue;
                            if (!string.IsNullOrEmpty(values))
                            {
                                try
                                {
                                    splookupValue = new SPFieldLookupValue(values);
                                }
                                catch
                                {
                                    splookupValue = new SPFieldLookupValue(Convert.ToInt32(values.Split(',')[1]), "");
                                }
                            }
                            else
                                splookupValue = new SPFieldLookupValue();
                            pinfo.SetValue(temp, splookupValue, null);
                            break;
                        case "Microsoft.SharePoint.SPFieldLookupValueCollection":
                            if (!string.IsNullOrEmpty(values))
                                pinfo.SetValue(temp, new SPFieldLookupValueCollection(values), null);
                            else
                                pinfo.SetValue(temp, new SPFieldLookupValueCollection(), null);
                            break;
                        case "System.Boolean":
                            {
                                if (!string.IsNullOrEmpty(values))
                                    pinfo.SetValue(temp, Convert.ToBoolean(values), null);
                                else
                                    pinfo.SetValue(temp, false, null);
                                break;

                            }
                        case "System.Double":
                            if (!string.IsNullOrEmpty(values))
                                pinfo.SetValue(temp, Convert.ToDouble(values), null);
                            break;

                        case "System.Int32":
                            if (!string.IsNullOrEmpty(values))
                                pinfo.SetValue(temp, Convert.ToInt32(values), null);
                            break;

                        case "System.Nullable`1[[System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]":
                        case "System.DateTime ?":
                        case "System.DateTime":
                            if (!string.IsNullOrEmpty(values))
                            {
                                //Định dạng ngày tháng
                                dtfi.ShortDatePattern = "MM/dd/yyyy";
                                dtfi.DateSeparator = "/";
                                //Lấy giá trị request
                                //Lấy kiểu dữ liệu
                                DateTime dtTemp = Convert.ToDateTime(values, dtfi);
                                if (dtTemp.Year < 1900 && dtTemp.Year > 2099)
                                    dtTemp = new DateTime(2012, 2, 30);
                                pinfo.SetValue(temp, ((DateTime)dtTemp), null);
                            }
                            else
                            {
                                pinfo.SetValue(temp, null, null);
                            }
                            break;

                        default:
                        case "System.String":
                            if (!string.IsNullOrEmpty(values))
                            {
                                pinfo.SetValue(temp, ((string)values).Trim(), null);
                            }
                            else
                            {
                                pinfo.SetValue(temp, "", null);
                            }
                            break;
                            #endregion
                    }
                }
            }
            #endregion
            return temp;
        }
        /// <summary>
        /// Hàm convert SPListCooll... to list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="spitemcoll"></param>
        /// <param name="lstFields"></param>
        /// <returns></returns>
        public List<T> ConvertListJson<T>(SPListItemCollection spitemcoll, List<string> lstFields) where T : EntityJson, new()
        {
            List<T> lstreturn = new List<T>();
            T temp;
            DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
            dtfi.ShortDatePattern = "dd/MM/yyyy";
            dtfi.DateSeparator = "/";
            IEnumerable<System.Reflection.PropertyInfo> lstPropertyDM = typeof(T).GetProperties();
            if (lstFields != null && lstFields.Count > 0)
            {
                lstPropertyDM = lstPropertyDM.Where(x => lstFields.Contains(x.Name));
            }
            foreach (SPListItem item in spitemcoll)
            {
                temp = new T();
                if (lstFields.Contains("Attachments"))
                {
                    temp.ListFileAttach = getListBaseAttackFileBySPListItem(item);
                }
                foreach (PropertyInfo pinfo in lstPropertyDM)
                {
                    if (pinfo.CanWrite && item[pinfo.Name] != null)
                    {
                        string values = item[pinfo.Name].ToString();
                        values = item[pinfo.Name].ToString();
                        switch (pinfo.PropertyType.FullName)
                        {
                            #region Cập nhật trường thông tin
                            case "Microsoft.SharePoint.SPModerationInformation":
                                pinfo.SetValue(temp, item.ModerationInformation, null);
                                break;
                            case "Microsoft.SharePoint.SPModerationStatusType":
                                if (item.ModerationInformation != null)
                                    pinfo.SetValue(temp, item.ModerationInformation.Status, null);
                                break;
                            case "SIPortal.Utils.LookupData":
                                LookupData temlk = GetLoookupByStringFiledData(values);
                                pinfo.SetValue(temp, temlk, null);
                                break;
                            case "System.Collections.Generic.List`1[[SIPortal.Utils.LookupData, SIPortal.Utils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=6e7da5038eb63432]]":
                                List<LookupData> lsttemlk = GetDanhSachMutilLoookupByStringFiledData(values);
                                pinfo.SetValue(temp, lsttemlk, null);
                                break;
                            case "System.Boolean":
                                {
                                    if (!string.IsNullOrEmpty(values))
                                        pinfo.SetValue(temp, Convert.ToBoolean(values), null);
                                    else
                                        pinfo.SetValue(temp, false, null);
                                    break;
                                }
                            case "System.Double":
                                if (!string.IsNullOrEmpty(values))
                                    pinfo.SetValue(temp, Convert.ToDouble(values), null);
                                break;
                            case "System.Int32":
                                if (!string.IsNullOrEmpty(values))
                                    pinfo.SetValue(temp, Convert.ToInt32(values), null);
                                break;
                            case "System.Nullable`1[[System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]":
                            case "System.DateTime ?":
                            case "System.DateTime":
                                if (!string.IsNullOrEmpty(values))
                                {
                                    DateTime dtTemp = Convert.ToDateTime(values);
                                    if (dtTemp.Year < 1900 && dtTemp.Year > 2099)
                                        dtTemp = new DateTime(2012, 2, 30);
                                    pinfo.SetValue(temp, ((DateTime)dtTemp), null);
                                }
                                else
                                {
                                    pinfo.SetValue(temp, null, null);
                                }
                                break;
                            default:
                            case "System.String":
                                if (!string.IsNullOrEmpty(values))
                                {
                                    pinfo.SetValue(temp, ((string)values).Trim(), null);
                                }
                                else
                                {
                                    pinfo.SetValue(temp, "", null);
                                }
                                break;
                                #endregion
                        }
                    }
                }
                lstreturn.Add(temp);
            }
            return lstreturn;
        }
        public List<T> ConvertListJson<T>(SPListItemCollection spitemcoll, params string[] lstFields) where T : EntityJson, new()
        {
            List<T> lstreturn = new List<T>();
            string UrlList = SpListProcess.RootFolder.Name;
            T temp;
            DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
            dtfi.ShortDatePattern = "dd/MM/yyyy";
            dtfi.DateSeparator = "/";
            IEnumerable<System.Reflection.PropertyInfo> lstPropertyDM = typeof(T).GetProperties();
            if (lstFields != null && lstFields.Count() > 0)
            {
                lstPropertyDM = lstPropertyDM.Where(x => lstFields.Contains(x.Name));
            }
            string _UrlListFull = SpListProcess.RootFolder.ServerRelativeUrl;
            foreach (SPListItem item in spitemcoll)
            {
                temp = new T();
                if (lstFields.Contains("Attachments"))
                {
                    temp.ListFileAttach = getListBaseAttackFileBySPListItem(item);
                }
                temp.ID = item.ID;
                temp.UrlList = UrlList;
                temp.UrlListFull = _UrlListFull;
                foreach (PropertyInfo pinfo in lstPropertyDM)
                {
                    if (pinfo.CanWrite && item[pinfo.Name] != null)
                    {

                        string values = item[pinfo.Name].ToString();
                        //string values = spItem[pinfo.Name].ToString();
                        if (item.Fields.GetFieldByInternalName(pinfo.Name) != null && item.Fields.GetFieldByInternalName(pinfo.Name).Type == SPFieldType.URL)
                        {
                            values = item[pinfo.Name].ToString();
                            SPFieldUrlValue oSPFieldType = new SPFieldUrlValue(values);
                            values = oSPFieldType.Url;
                        }

                        //values = item[pinfo.Name].ToString();
                        switch (pinfo.PropertyType.FullName)
                        {
                            #region Cập nhật trường thông tin
                            case "ViPortal_Utils.Base.LookupData":
                                LookupData temlk = GetLoookupByStringFiledData(values);
                                pinfo.SetValue(temp, temlk, null);
                                break;
                            case "System.Collections.Generic.List`1[[ViPortal_Utils.Base.LookupData, ViPortal_Utils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]]":
                                List<LookupData> lsttemlk = GetDanhSachMutilLoookupByStringFiledData(values);
                                pinfo.SetValue(temp, lsttemlk, null);
                                break;
                            case "System.Boolean":
                                {
                                    if (!string.IsNullOrEmpty(values))
                                        pinfo.SetValue(temp, Convert.ToBoolean(values), null);
                                    else
                                        pinfo.SetValue(temp, false, null);
                                    break;
                                }
                            case "System.Double":
                                if (!string.IsNullOrEmpty(values))
                                    pinfo.SetValue(temp, Convert.ToDouble(values), null);
                                break;
                            case "System.Int32":
                                if (!string.IsNullOrEmpty(values))
                                    pinfo.SetValue(temp, Convert.ToInt32(values), null);
                                break;
                            case "System.Nullable`1[[System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]":
                            case "System.DateTime ?":
                            case "System.DateTime":
                                if (!string.IsNullOrEmpty(values))
                                {
                                    DateTime dtTemp = Convert.ToDateTime(values);
                                    if (dtTemp.Year < 1900 && dtTemp.Year > 2099)
                                        dtTemp = new DateTime(2012, 2, 30);
                                    pinfo.SetValue(temp, ((DateTime)dtTemp), null);
                                }
                                else
                                {
                                    pinfo.SetValue(temp, null, null);
                                }
                                break;
                            case "Microsoft.SharePoint.SPModerationStatusType":
                                pinfo.SetValue(temp, Convert.ToInt32(values), null);
                                break;
                            default:
                            case "System.String":
                                if (!string.IsNullOrEmpty(values))
                                {
                                    pinfo.SetValue(temp, ((string)values).Trim(), null);
                                }
                                else
                                {
                                    pinfo.SetValue(temp, "", null);
                                }
                                break;
                                #endregion
                        }
                    }
                    else if (pinfo.CanWrite)
                    {
                        switch (pinfo.PropertyType.FullName)
                        {
                            case "System.String":
                                pinfo.SetValue(temp, "", null);
                                break;
                        }
                    }
                }
                lstreturn.Add(temp);
            }
            return lstreturn;
        }
        /// <summary>
        /// Convert SPListItem to object
        /// Author      Date        Comments
        /// CHINHPN     29/03
        /// </summary>
        /// <typeparam name="T">Lớp đối tượng</typeparam>
        /// <returns></returns>
        public T ConvertToObject<T>(SPListItem spItem) where T : SPEntity, new()
        {
            T temp;
            IEnumerable<System.Reflection.PropertyInfo> lstPropertyDM = typeof(T).GetProperties();
            //Các trường hợp xử lý đặc biệt
            List<string> ltsRemove = new List<string>(){
                "ListFileAttach",
                "ListFileAttachAdd",
                "ListFileRemove",
            };
            lstPropertyDM = lstPropertyDM.Where(x => !ltsRemove.Contains(x.Name));
            DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
            dtfi.ShortDatePattern = "dd/MM/yyyy";
            dtfi.DateSeparator = "/";
            //System.Reflection.PropertyInfo pinfoSpec;
            temp = new T();
            ///Lấy riêng một số trường đặc biệt Attachment
            temp.ListFileAttach = getListAttackFileBySPListItem(spItem);
            #region các trường bình thường
            foreach (PropertyInfo pinfo in lstPropertyDM)
            {
                if (spItem.Fields.ContainsFieldWithStaticName(pinfo.Name))
                {
                    if (spItem[pinfo.Name] != null)
                    {
                        string values = spItem[pinfo.Name].ToString();
                        if (spItem.Fields.GetFieldByInternalName(pinfo.Name) != null && spItem.Fields.GetFieldByInternalName(pinfo.Name).Type == SPFieldType.URL)
                        {
                            values = spItem[pinfo.Name].ToString();
                            SPFieldUrlValue oSPFieldType = new SPFieldUrlValue(values);
                            values = oSPFieldType.Url;
                        }
                        else if (spItem.Fields.GetFieldByInternalName(pinfo.Name) != null && spItem.Fields.GetFieldByInternalName(pinfo.Name).Type == SPFieldType.Calculated) //Calculated
                        {
                            SPFieldCalculated cf = (SPFieldCalculated)spItem.Fields[pinfo.Name];
                            values = cf.GetFieldValueForEdit(spItem[pinfo.Name]);
                        }
                        else
                            values = spItem[pinfo.Name].ToString();
                        switch (pinfo.PropertyType.FullName)
                        {
                            #region Cập nhật trường thông tin

                            case "Microsoft.SharePoint.SPModerationInformation":
                                pinfo.SetValue(temp, spItem.ModerationInformation, null);
                                break;
                            case "Microsoft.SharePoint.SPModerationStatusType":
                                if (spItem.ModerationInformation != null)
                                    pinfo.SetValue(temp, spItem.ModerationInformation.Status, null);
                                break;
                            case "Microsoft.SharePoint.SPFieldLookupValue":
                                SPFieldLookupValue splookupValue;
                                if (!string.IsNullOrEmpty(values))
                                {
                                    try
                                    {
                                        splookupValue = new SPFieldLookupValue(values);
                                    }
                                    catch
                                    {
                                        splookupValue = new SPFieldLookupValue(Convert.ToInt32(values.Split(',')[1]), "");
                                    }
                                }
                                else
                                    splookupValue = new SPFieldLookupValue();
                                pinfo.SetValue(temp, splookupValue, null);
                                break;
                            case "Microsoft.SharePoint.SPFieldLookupValueCollection":
                                if (!string.IsNullOrEmpty(values))
                                    pinfo.SetValue(temp, new SPFieldLookupValueCollection(values), null);
                                else
                                    pinfo.SetValue(temp, new SPFieldLookupValueCollection(), null);
                                break;
                            case "System.Boolean":
                                {
                                    if (!string.IsNullOrEmpty(values))
                                        pinfo.SetValue(temp, Convert.ToBoolean(values), null);
                                    else
                                        pinfo.SetValue(temp, false, null);
                                    break;

                                }
                            case "System.Double":
                                if (!string.IsNullOrEmpty(values))
                                    pinfo.SetValue(temp, Convert.ToDouble(values), null);
                                break;

                            case "System.Int32":
                                if (!string.IsNullOrEmpty(values))
                                    pinfo.SetValue(temp, Convert.ToInt32(values), null);
                                break;

                            case "System.Nullable`1[[System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]":
                            case "System.DateTime ?":
                            case "System.DateTime":
                                if (!string.IsNullOrEmpty(values))
                                {
                                    //Định dạng ngày tháng
                                    dtfi.ShortDatePattern = "MM/dd/yyyy";
                                    dtfi.DateSeparator = "/";
                                    //Lấy giá trị request
                                    //Lấy kiểu dữ liệu

                                    DateTime dtTemp = Convert.ToDateTime(spItem[pinfo.Name]);
                                    if (dtTemp.Year < 1900 && dtTemp.Year > 2099)
                                        dtTemp = new DateTime(2012, 2, 30);
                                    pinfo.SetValue(temp, ((DateTime)dtTemp), null);

                                }
                                else
                                {
                                    pinfo.SetValue(temp, null, null);
                                }
                                break;

                            default:
                            case "System.String":
                                if (!string.IsNullOrEmpty(values))
                                {

                                    pinfo.SetValue(temp, ((string)values).Trim(), null);
                                }
                                else
                                {
                                    pinfo.SetValue(temp, "", null);
                                }
                                break;
                                #endregion
                        }
                    }
                }
            }
            #endregion
            return temp;
        }
        public List<T> ConvertListObj<T>(SPListItemCollection spitemcoll, List<string> lstFields) where T : SPEntity, new()
        {

            List<T> lstReturn = new List<T>();
            T temp;
            IEnumerable<System.Reflection.PropertyInfo> lstPropertyDM = typeof(T).GetProperties();
            DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
            dtfi.ShortDatePattern = "dd/MM/yyyy";
            dtfi.DateSeparator = "/";
            List<string> ltsRemove = new List<string>(){
                "ListFileAttach",
                "ListFileAttachAdd",
                "ListFileRemove",
            };
            if (lstFields.Count > 0)
            {
                lstPropertyDM = lstPropertyDM.Where(x => lstFields.Contains(x.Name) && !ltsRemove.Contains(x.Name));
            }
            //System.Reflection.PropertyInfo pinfoSpec;
            foreach (SPListItem item in spitemcoll)
            {
                temp = new T();
                ///Lấy riêng một số trường đặc biệt Attachment
                if (lstFields.Contains("Attachments"))
                {
                    temp.ListFileAttach = getListAttackFileBySPListItem(item);
                }
                #region các trường bình thường
                foreach (PropertyInfo pinfo in lstPropertyDM)
                {
                    if (item[pinfo.Name] != null)
                    {
                        string values = item[pinfo.Name].ToString();
                        values = item[pinfo.Name].ToString();
                        switch (pinfo.PropertyType.FullName)
                        {
                            #region Cập nhật trường thông tin
                            case "Microsoft.SharePoint.SPModerationInformation":
                                pinfo.SetValue(temp, item.ModerationInformation, null);
                                break;
                            case "Microsoft.SharePoint.SPModerationStatusType":
                                if (item.ModerationInformation != null)
                                    pinfo.SetValue(temp, item.ModerationInformation.Status, null);
                                break;
                            case "Microsoft.SharePoint.SPFieldLookupValue":
                                SPFieldLookupValue splookupValue;
                                if (!string.IsNullOrEmpty(values))
                                {
                                    try
                                    {
                                        splookupValue = new SPFieldLookupValue(values);
                                    }
                                    catch
                                    {
                                        splookupValue = new SPFieldLookupValue(Convert.ToInt32(values.Split(',')[1]), "");
                                    }
                                }
                                else
                                    splookupValue = new SPFieldLookupValue();
                                pinfo.SetValue(temp, splookupValue, null);
                                break;
                            case "Microsoft.SharePoint.SPFieldLookupValueCollection":
                                if (!string.IsNullOrEmpty(values))
                                    pinfo.SetValue(temp, new SPFieldLookupValueCollection(values), null);
                                else
                                    pinfo.SetValue(temp, new SPFieldLookupValueCollection(), null);
                                break;
                            case "System.Boolean":
                                {
                                    if (!string.IsNullOrEmpty(values))
                                        pinfo.SetValue(temp, Convert.ToBoolean(values), null);
                                    else
                                        pinfo.SetValue(temp, false, null);
                                    break;

                                }
                            case "System.Double":
                                if (!string.IsNullOrEmpty(values))
                                    pinfo.SetValue(temp, Convert.ToDouble(values), null);
                                break;

                            case "System.Int32":
                                if (!string.IsNullOrEmpty(values))
                                    pinfo.SetValue(temp, Convert.ToInt32(values), null);
                                break;

                            case "System.Nullable`1[[System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]":
                            case "System.DateTime ?":
                            case "System.DateTime":
                                if (!string.IsNullOrEmpty(values))
                                {
                                    //Định dạng ngày tháng
                                    dtfi.ShortDatePattern = "MM/dd/yyyy";
                                    dtfi.DateSeparator = "/";
                                    //Lấy giá trị request
                                    //Lấy kiểu dữ liệu
                                    DateTime dtTemp = Convert.ToDateTime(values, dtfi);
                                    if (dtTemp.Year < 1900 && dtTemp.Year > 2099)
                                        dtTemp = new DateTime(2012, 2, 30);
                                    pinfo.SetValue(temp, ((DateTime)dtTemp), null);
                                }
                                else
                                {
                                    pinfo.SetValue(temp, null, null);
                                }
                                break;

                            default:
                            case "System.String":
                                if (!string.IsNullOrEmpty(values))
                                {
                                    pinfo.SetValue(temp, ((string)values).Trim(), null);
                                }
                                else
                                {
                                    pinfo.SetValue(temp, "", null);
                                }
                                break;
                                #endregion
                        }
                    }
                }
                #endregion
                lstReturn.Add(temp);
            }
            return lstReturn;
        }
        public List<T> ConvertListObj<T>(SPListItemCollection spitemcoll, params string[] lstFields) where T : SPEntity, new()
        {
            List<T> lstReturn = new List<T>();
            T temp;
            IEnumerable<System.Reflection.PropertyInfo> lstPropertyDM = typeof(T).GetProperties();
            DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
            dtfi.ShortDatePattern = "dd/MM/yyyy";
            dtfi.DateSeparator = "/";
            List<string> ltsRemove = new List<string>(){
                "ListFileAttach",
                "ListFileAttachAdd",
                "ListFileRemove",
            };
            lstPropertyDM = lstPropertyDM.Where(x => lstFields.Contains(x.Name) && !ltsRemove.Contains(x.Name));
            foreach (SPListItem item in spitemcoll)
            {
                temp = new T();
                ///Lấy riêng một số trường đặc biệt Attachment
                if (lstFields.Contains("Attachments"))
                {
                    temp.ListFileAttach = getListAttackFileBySPListItem(item);
                }

                #region các trường bình thường
                foreach (PropertyInfo pinfo in lstPropertyDM)
                {
                    if (item[pinfo.Name] != null)
                    {
                        string values = item[pinfo.Name].ToString();
                        values = item[pinfo.Name].ToString();
                        switch (pinfo.PropertyType.FullName)
                        {
                            #region Cập nhật trường thông tin
                            case "Microsoft.SharePoint.SPModerationInformation":
                                pinfo.SetValue(temp, item.ModerationInformation, null);
                                break;
                            case "Microsoft.SharePoint.SPModerationStatusType":
                                if (item.ModerationInformation != null)
                                    pinfo.SetValue(temp, item.ModerationInformation.Status, null);
                                break;
                            case "Microsoft.SharePoint.SPFieldLookupValue":
                                SPFieldLookupValue splookupValue;
                                if (!string.IsNullOrEmpty(values))
                                {
                                    try
                                    {
                                        splookupValue = new SPFieldLookupValue(values);
                                    }
                                    catch
                                    {
                                        splookupValue = new SPFieldLookupValue(Convert.ToInt32(values.Split(',')[1]), "");
                                    }
                                }
                                else
                                    splookupValue = new SPFieldLookupValue();
                                pinfo.SetValue(temp, splookupValue, null);
                                break;
                            case "Microsoft.SharePoint.SPFieldLookupValueCollection":
                                if (!string.IsNullOrEmpty(values))
                                    pinfo.SetValue(temp, new SPFieldLookupValueCollection(values), null);
                                else
                                    pinfo.SetValue(temp, new SPFieldLookupValueCollection(), null);
                                break;
                            case "System.Boolean":
                                {
                                    if (!string.IsNullOrEmpty(values))
                                        pinfo.SetValue(temp, Convert.ToBoolean(values), null);
                                    else
                                        pinfo.SetValue(temp, false, null);
                                    break;

                                }
                            case "System.Double":
                                if (!string.IsNullOrEmpty(values))
                                    pinfo.SetValue(temp, Convert.ToDouble(values), null);
                                break;

                            case "System.Int32":
                                if (!string.IsNullOrEmpty(values))
                                    pinfo.SetValue(temp, Convert.ToInt32(values), null);
                                break;

                            case "System.Nullable`1[[System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]":
                            case "System.DateTime ?":
                            case "System.DateTime":
                                if (!string.IsNullOrEmpty(values))
                                {
                                    //Định dạng ngày tháng
                                    dtfi.ShortDatePattern = "MM/dd/yyyy";
                                    dtfi.DateSeparator = "/";
                                    //Lấy giá trị request
                                    //Lấy kiểu dữ liệu
                                    DateTime dtTemp = Convert.ToDateTime(values, dtfi);
                                    if (dtTemp.Year < 1900 && dtTemp.Year > 2099)
                                        dtTemp = new DateTime(2012, 2, 30);
                                    pinfo.SetValue(temp, ((DateTime)dtTemp), null);
                                }
                                else
                                {
                                    pinfo.SetValue(temp, null, null);
                                }
                                break;

                            default:
                            case "System.String":
                                if (!string.IsNullOrEmpty(values))
                                {
                                    pinfo.SetValue(temp, ((string)values).Trim(), null);
                                }
                                else
                                {
                                    pinfo.SetValue(temp, "", null);
                                }
                                break;
                                #endregion
                        }
                    }
                }
                #endregion
                lstReturn.Add(temp);
            }
            return lstReturn;
        }


        /// <summary>
        /// Ham convert tu obj solr sang json
        /// Author  Date        Comment
        /// Vinhhq  16/09/2018  Ham dung chung
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oSolrItem"></param>
        /// <param name="lstFields"></param>
        /// <returns></returns>
        public T ConvertSolrToJson<T>(EntitySolr oSolrItem, params string[] lstFields) where T : EntityJson, new()
        {
            T oT = new T();
            List<string> lstFieldNameRemove = new List<string>() { "Attachments", "SP_ID", "id" };
            DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
            IEnumerable<System.Reflection.PropertyInfo> lstPropertyDM = typeof(T).GetProperties();
            IEnumerable<System.Reflection.PropertyInfo> lstPropertySolr = oSolrItem.GetType().GetProperties();
            #region gán gia trị solr id cho thuộc tinh idsolr
            //System.Reflection.PropertyInfo oProcessInfoSec = lstPropertySolr.FirstOrDefault(x => x.Name == "id");
            //System.Reflection.PropertyInfo oProcessInfoJson = lstPropertyDM.FirstOrDefault(x => x.Name == "idsolr");
            //if (oProcessInfoJson != null)
            //{
            //    oProcessInfoJson.SetValue(oT, oSolrItem.id, null);
            //}

            #endregion

            //if (lstFields.Contains("SP_ID") || lstFields.Contains("ID") || lstFields.Count() == 0)
            //{
            //    System.Reflection.PropertyInfo opopSPID = lstPropertyDM.FirstOrDefault(x => x.Name == "ID");
            //    if (opopSPID != null)
            //        opopSPID.SetValue(oT, oSolrItem.SP_ID, null);
            //}
            if (lstFields != null && lstFields.Count() > 0)
            {
                lstPropertyDM = lstPropertyDM.Where(x => lstFields.Contains(x.Name) && !lstFieldNameRemove.Contains(x.Name));
            }
            foreach (System.Reflection.PropertyInfo pinfo in lstPropertyDM)
            {
                System.Reflection.PropertyInfo oProcessInfo = lstPropertySolr.FirstOrDefault(x => x.Name == pinfo.Name);

                if (oProcessInfo != null)
                {
                    object val = oProcessInfo.GetValue(oSolrItem, null);

                    if (oProcessInfo.Name == "ImageNews")
                    {
                        if (val != null)
                        {
                            string _val = val.ToString();
                            if (_val.StartsWith("http") && (_val.Contains("viportal") || _val.Contains("portal.evnhanoi") || _val.Contains("localhost")))
                            {
                                _val = _val.GetPathAndQuery();
                                pinfo.SetValue(oT, _val, null);
                            }
                            else pinfo.SetValue(oT, _val, null);
                        }
                    }
                    else
                    {
                        switch (pinfo.PropertyType.FullName)
                        {
                            #region Cập nhật trường thông tin
                            case "ViPortal_Utils.Base.LookupData":
                                LookupData temlk = new LookupData();
                                if (val != null)
                                {
                                    temlk = GetLoookupByStringFiledData(val.ToString());
                                }
                                pinfo.SetValue(oT, temlk, null);
                                break;
                            case "System.Collections.Generic.List`1[[ViPortal_Utils.Base.LookupData, ViPortal_Utils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]]":
                                List<LookupData> lsttemlk = new List<LookupData>();
                                if (val != null)
                                {
                                    lsttemlk = GetDanhSachMutilLoookupByStringFiledData(val.ToString());
                                }
                                pinfo.SetValue(oT, lsttemlk, null);
                                break;
                            case "System.Nullable`1[[System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]":
                            case "System.DateTime ?":
                            case "System.DateTime":
                                DateTime? tempdate = (DateTime?)val;
                                if (tempdate.HasValue)
                                    pinfo.SetValue(oT, DateTime.SpecifyKind(tempdate.Value, DateTimeKind.Utc), null);
                                else
                                    pinfo.SetValue(oT, val, null);
                                break;
                            case "System.String":
                                if (val == null)
                                    pinfo.SetValue(oT, string.Empty, null);
                                else pinfo.SetValue(oT, val, null);
                                break;
                            //if (itemsolr.mainNgayTiepNhan.HasValue)
                            //ossimaindata.mainNgayTiepNhan = DateTime.SpecifyKind(itemsolr.mainNgayTiepNhan.Value, DateTimeKind.Unspecified);
                            default:
                                pinfo.SetValue(oT, val, null);
                                break;
                                #endregion
                        }
                    }
                }
            }
            //fix 2 trường này cho chuẩn.
            oT.ID = oSolrItem.SP_ID;

            oT.UrlListFull = string.IsNullOrEmpty(oSolrItem.FullUrlList) ? FullPathUrlListProcess : oSolrItem.FullUrlList;
            if (string.IsNullOrEmpty(oT.UrlList))
            {
                oT.UrlList = oT.UrlListFull.Split('/').LastOrDefault();
            }
            if (!string.IsNullOrEmpty(oSolrItem.Attachments))
            {
                oT.ListFileAttach =
                    oSolrItem.Attachments.Split('#').Select(x => new BaseFileAttach() { Url = x.Split(';')[0], Name = x.Split(';')[1] }).ToList();
            }
            //temp.UrlList = UrlList;
            //temp.UrlListFull = _UrlListFull;
            return oT;
        }

        /// <summary>
        /// Sinh string view field
        /// </summary>
        /// <param name="lstField"></param>
        /// <returns></returns>
        public string BuildViewField(List<string> lstField)
        {
            StringBuilder stbBuilder = new StringBuilder();
            if (lstField.Count == 0)
            {
                stbBuilder.Append("<FieldRef Name=\"Title\" />");
                stbBuilder.Append("<FieldRef Name=\"ID\" />");
            }
            else
            {
                foreach (string field in lstField)
                    stbBuilder.AppendFormat("   <FieldRef Name=\"{0}\" />", field);
            }
            return stbBuilder.ToString();
        }
        public string BuildViewField(params string[] lstField)
        {
            StringBuilder stbBuilder = new StringBuilder();
            if (lstField.Length == 0)
            {
                stbBuilder.Append("<FieldRef Name=\"Title\" />");
                stbBuilder.Append("<FieldRef Name=\"ID\" />");
            }
            else
            {
                foreach (string field in lstField)
                    stbBuilder.AppendFormat("<FieldRef Name=\"{0}\" />", field);
            }
            return stbBuilder.ToString();
        }
        /// <summary>
        /// get caml query EQ Boolean
        /// </summary>
        /// <param name="FieldName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public string GetStringBoolean(string FieldName, int value)
        {
            if (value == 2) value = 0;
            return string.Concat("<Eq><FieldRef Name=\"", FieldName, "\"/><Value Type=\"Interger\">", value, "</Value></Eq>");
        }
        public string GetStringEQNumber(string FieldName, int value)
        {
            return string.Concat("<Eq><FieldRef Name=\"", FieldName, "\"/><Value Type=\"Number\">", value, "</Value></Eq>");
        }
        public string GetStringEQText(string FieldName, string value)
        {
            return string.Concat("<Eq><FieldRef Name=\"", FieldName, "\"/><Value Type=\"Text\">", value, "</Value></Eq>");
        }
        public string getstringNotEQText(string FieldName, string value)
        {
            return string.Concat("<Neq><FieldRef Name=\"", FieldName, "\"/></ FieldRef ><Value Type=\"Text\">", value, "</Value></Neq>");
        }
        public string GetStringEQLookUp(string FieldName, int value)
        {
            return string.Concat("<Eq><FieldRef Name=\"", FieldName, "\" LookupId=\"TRUE\"/><Value Type=\"Lookup\">", value, "</Value></Eq>");
        }
        public string GetStringEQLookUpValue(string FieldName, string value)
        {
            return string.Concat("<Includes><FieldRef Name=\"", FieldName, "\"/><Value Type=\"Lookup\">", value, "</Value></Includes>");
        }
        public string GetStringStartWith(string FieldName, string value)
        {
            return string.Concat("<BeginsWith><FieldRef Name=\"", FieldName, "\"/><Value Type=\"Text\">", value, "</Value></BeginsWith>");
        }
        //public string GetStringStartWith(string FieldName, string value)
        //{
        //    return string.Concat("<BeginsWith><FieldRef Name=\"", FieldName, "\"/><Value Type=\"Text\">", value, "</Value></BeginsWith>");
        //}
        public string GetStringContainsText(string FieldName, string value)
        {
            return string.Concat("<Contains><FieldRef Name=\"", FieldName, "\"/><Value Type=\"Text\">", value, "</Value></Contains>");
        }
        public string getstringNumNotEQ(string FieldName, int value)
        {
            return string.Concat("<Neq><FieldRef Name=\"", FieldName, "\"/><Value Type=\"Number\">", value, "</Value></Neq>");
        }
        public string GetStringCamlDate(string FieldName, DateTime value, string TxtOperator)
        {
            //return string.Concat("<Gt><FieldRef Name=\"", FieldName, "\"/><Value Type=\"DateTime\">", value.ToString("yyyy-MM-dd"), "</Value></Gt>");
            return string.Format("<{0}><FieldRef Name=\"{1}\"/><Value Type=\"DateTime\">{2}</Value></{0}>", TxtOperator, FieldName, value.ToString("yyyy-MM-dd"));
        }
        public string GetStringCamlDateV2(string FieldName, DateTime value, string TxtOperator)
        {
            //return string.Concat("<Gt><FieldRef Name=\"", FieldName, "\"/><Value Type=\"DateTime\">", value.ToString("yyyy-MM-dd"), "</Value></Gt>");
            return string.Format("<{0}><FieldRef Name=\"{1}\"/><Value Type=\"DateTime\">{2}</Value></{0}>", TxtOperator, FieldName, value.ToString("MM/dd/yyyy"));
        }
        /// <summary>
        /// sangvd add
        /// </summary>
        /// <param name="FieldName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetStringGeqDate(string FieldName, DateTime value, bool includetime = true)
        {
            return string.Concat("<Geq><FieldRef Name=\"", FieldName, "\"/><Value  Type=\"DateTime\" " + (includetime ? " IncludeTimeValue='True'>" : ">"), SPUtility.CreateISO8601DateTimeFromSystemDateTime(value), "</Value></Geq>");
        }
        public static string GetStringEqDate(string FieldName, DateTime value, bool includetime = true)
        {
            return string.Format("<Eq><FieldRef Name=\"{0}\"/><Value Type=\"DateTime\">{1}</Value></Eq>", FieldName, value.ToString("yyyy-MM-dd"));
            //return string.Concat("<Eq><FieldRef Name=\"", FieldName, "\"/><Value  Type=\"DateTime\" " + (includetime ? " IncludeTimeValue='True'>" : ">"), SPUtility.CreateISO8601DateTimeFromSystemDateTime(value), "</Value></Eq>");
        }
        public static string GetStringLeqDate(string FieldName, DateTime value, bool includetime = true)
        {
            return string.Concat("<Leq><FieldRef Name=\"", FieldName, "\"/><Value  Type=\"DateTime\" " + (includetime ? " IncludeTimeValue='True'>" : ">"), SPUtility.CreateISO8601DateTimeFromSystemDateTime(value), "</Value></Leq>");
        }
        public string getstringNumGt(string FieldName, int value)
        {
            return string.Concat("<Gt><FieldRef Name=\"", FieldName, "\"/><Value Type=\"Number\">", value, "</Value></Gt>");
        }
        public string getstringNumLt(string FieldName, int value)
        {
            return string.Concat("<Lt><FieldRef Name=\"", FieldName, "\"/><Value Type=\"Number\">", value, "</Value></Lt>");
        }
        public string getstringNumEQ(string FieldName, int value)
        {
            return string.Concat("<Eq><FieldRef Name=\"", FieldName, "\"/><Value Type=\"Number\">", value, "</Value></Eq>");
        }
        public string getstringNumEQ(string FieldName, long value)
        {
            return string.Concat("<Eq><FieldRef Name=\"", FieldName, "\"/><Value Type=\"Number\">", value, "</Value></Eq>");
        }
        public string getstringNotNull(string FieldName = "ID")
        {
            return string.Format("<IsNotNull><FieldRef Name=\"{0}\" /></IsNotNull>", FieldName);
        }
        public string getstringNull(string FieldName = "ID")
        {
            return string.Format("<IsNull><FieldRef Name=\"{0}\" /></IsNull>", FieldName);
        }
        public string StringOrderBy(string FieldSort, bool Ascending)
        {
            return string.Format("<OrderBy><FieldRef Name=\"{0}\" Ascending=\"{1}\" /></OrderBy>", FieldSort, Ascending);
        }
        public string GetStringIncludeLookUp(string FieldName, int value)
        {
            return string.Concat("<Includes><FieldRef Name=\"", FieldName, "\" LookupId=\"TRUE\"/><Value Type=\"Lookup\">", value, "</Value></Includes>");
        }
        public string GetStringNotIncludeLookUp(string FieldName, object value)
        {
            return string.Concat("<NotIncludes><FieldRef Name=\"", FieldName, "\" /><Value Type=\"Lookup\">", value.ToString(), "</Value></NotIncludes>");
        }
        public string GetStringIncludeLookUpValue(string FieldName, string value)
        {
            return string.Concat("<Includes><FieldRef Name=\"", FieldName, "\"/><Value Type=\"Lookup\">", value, "</Value></Includes>");
        }
        public String getStringMultiLookup(List<int> ltsId, string FieldLookupname)
        {
            StringBuilder stbOrQuery = new StringBuilder();
            if (ltsId.Count > 0)
            {
                int intTotal = ltsId.Count;
                if (intTotal > 1)
                {
                    stbOrQuery.Append("      <In>");
                    stbOrQuery.AppendFormat("         <FieldRef Name=\"{0}\" LookupId=\"True\"/>", FieldLookupname);
                    stbOrQuery.Append("         <Values>");
                    for (int index = 0; index < intTotal; index++)
                    {
                        stbOrQuery.AppendFormat("         <Value Type=\"Integer\">{0}</Value>", ltsId[index]);
                    }
                    stbOrQuery.Append("         </Values>");
                    stbOrQuery.Append("      </In>");
                }
                else
                {
                    stbOrQuery.Append("     <Eq>");
                    stbOrQuery.AppendFormat("         <FieldRef Name=\"{0}\" LookupId=\"True\"/>", FieldLookupname);
                    stbOrQuery.AppendFormat("  <Value Type='Lookup'>{0}</Value>", ltsId[0]);
                    stbOrQuery.Append("     </Eq>");
                }
            }
            else stbOrQuery.Append("<IsNull><FieldRef Name=\"ID\" /></IsNull>");
            return stbOrQuery.ToString();
        }
        public String getStringByListId(List<int> ltsId)
        {
            StringBuilder stbOrQuery = new StringBuilder();
            if (ltsId.Count > 1)
            {
                int intTotal = ltsId.Count;

                stbOrQuery.Append("      <In>");
                stbOrQuery.Append("         <FieldRef Name=\"ID\" />");
                stbOrQuery.Append("         <Values>");
                for (int index = 0; index < intTotal; index++)
                {
                    stbOrQuery.AppendFormat("         <Value Type=\"Counter\">{0}</Value>", ltsId[index]);
                }
                stbOrQuery.Append("         </Values>");
                stbOrQuery.Append("      </In>");

            }
            else if (ltsId.Count == 1)
            {

                stbOrQuery.Append("      <Eq>");
                stbOrQuery.Append("            <FieldRef Name=\"ID\" />");
                stbOrQuery.Append("            <Value Type=\"Counter\">" + ltsId[0] + "</Value>");
                stbOrQuery.Append("      </Eq>");

            }
            else
                stbOrQuery.Append("<IsNull><FieldRef Name=\"ID\" /></IsNull>");
            return stbOrQuery.ToString();
        }
        public String getStringInListString(string FiledName, List<string> ltsId)
        {
            StringBuilder stbOrQuery = new StringBuilder();
            if (ltsId.Count > 1)
            {
                int intTotal = ltsId.Count;

                stbOrQuery.Append("      <In>");
                stbOrQuery.Append($"         <FieldRef Name=\"{FiledName}\" />");
                stbOrQuery.Append("         <Values>");
                for (int index = 0; index < intTotal; index++)
                {
                    stbOrQuery.AppendFormat("         <Value Type=\"Text\">{0}</Value>", ltsId[index]);
                }
                stbOrQuery.Append("         </Values>");
                stbOrQuery.Append("      </In>");

            }
            else if (ltsId.Count == 1)
            {

                stbOrQuery.Append("      <Eq>");
                stbOrQuery.Append($"         <FieldRef Name=\"{FiledName}\" />");
                stbOrQuery.Append("            <Value Type=\"Text\">" + ltsId[0] + "</Value>");
                stbOrQuery.Append("      </Eq>");

            }
            else
                stbOrQuery.Append("<IsNull><FieldRef Name=\"ID\" /></IsNull>");
            return stbOrQuery.ToString();
        }
        public String getStringInListNumber(string FiledName, List<int> ltsId)
        {
            StringBuilder stbOrQuery = new StringBuilder();
            if (ltsId.Count > 1)
            {
                int intTotal = ltsId.Count;

                stbOrQuery.Append("      <In>");
                stbOrQuery.Append($"         <FieldRef Name=\"{FiledName}\" />");
                stbOrQuery.Append("         <Values>");
                for (int index = 0; index < intTotal; index++)
                {
                    stbOrQuery.AppendFormat("         <Value Type=\"Number\">{0}</Value>", ltsId[index]);
                }
                stbOrQuery.Append("         </Values>");
                stbOrQuery.Append("      </In>");

            }
            else if (ltsId.Count == 1)
            {

                stbOrQuery.Append("      <Eq>");
                stbOrQuery.Append($"         <FieldRef Name=\"{FiledName}\" />");
                stbOrQuery.Append("            <Value Type=\"Number\">" + ltsId[0] + "</Value>");
                stbOrQuery.Append("      </Eq>");

            }
            else
                stbOrQuery.Append("<IsNull><FieldRef Name=\"ID\" /></IsNull>");
            return stbOrQuery.ToString();
        }
        /// <summary>
        /// Lấy theo id
        /// </summary>
        /// <param name="ltsId"></param>
        /// <returns></returns>
        public String getStringByListIdOnly(List<int> ltsId)
        {
            StringBuilder stbOrQuery = new StringBuilder();
            if (ltsId.Count > 1)
            {
                int intTotal = ltsId.Count;

                stbOrQuery.Append("      <In>");
                stbOrQuery.Append("         <FieldRef Name=\"ID\" />");
                stbOrQuery.Append("         <Values>");
                for (int index = 0; index < intTotal; index++)
                {
                    stbOrQuery.AppendFormat("         <Value Type=\"Counter\">{0}</Value>", ltsId[index]);
                }
                stbOrQuery.Append("         </Values>");
                stbOrQuery.Append("      </In>");

            }
            else if (ltsId.Count == 1)
            {

                stbOrQuery.Append("      <Eq>");
                stbOrQuery.Append("            <FieldRef Name=\"ID\" />");
                stbOrQuery.Append("            <Value Type=\"Counter\">" + ltsId[0] + "</Value>");
                stbOrQuery.Append("      </Eq>");

            }
            else
            {
                stbOrQuery.Append("      <Eq>");
                stbOrQuery.Append("            <FieldRef Name=\"ID\" />");
                stbOrQuery.Append("            <Value Type=\"Counter\">" + 0 + "</Value>");
                stbOrQuery.Append("      </Eq>");
            }
            //stbOrQuery.Append("<IsNull><FieldRef Name=\"ID\" /></IsNull>");
            return stbOrQuery.ToString();
        }
        public string GetStringModer(int _mods)
        {
            return string.Concat("<Eq><FieldRef Name=\"_ModerationStatus\"/><Value Type=\"ModStat\">", _mods, "</Value></Eq>");
        }
        /// <summary>
        /// hàm update một số các field
        /// </summary>
        /// <param name="ItemID"></param>
        /// <param name="Values"></param>
        /// <returns></returns>
        public string UpdateOneOrMoreField(int ItemID, Dictionary<string, object> Values, bool isModerationStatus = false)
        {
            string outtb = string.Empty;
            try
            {
                string[] arryField = Values.Select(x => x.Key).ToArray();
                if (isModerationStatus)
                {
                    List<string> lstField = arryField.ToList();
                    lstField.Add("_ModerationStatus");
                    arryField = lstField.ToArray();
                }
                SPListItem spitem = SpListProcess.GetItemById(ItemID);

                // Loop over pairs with foreach.
                foreach (KeyValuePair<string, object> pair in Values)
                {
                    if (pair.Key == "_ModerationStatus")
                    {

                    }
                    switch (pair.Key)
                    {
                        case "_ModerationStatus":
                            spitem.ModerationInformation.Status = (SPModerationStatusType)pair.Value;
                            break;
                        default:
                            spitem[pair.Key] = pair.Value;
                            break;
                    }

                }
                if (arryField.FirstOrDefault(x => x == "_ModerationStatus") != null)
                {
                    spitem.SystemUpdate();
                }
                else if (isModerationStatus)
                {
                    spitem.ModerationInformation.Status = spitem.ModerationInformation.Status;
                    spitem.SystemUpdate();
                }
                else
                    spitem.Update();
                if (isHasSolr)
                {
                    U oDanhMucSolr = new U();
                    oDanhMucSolr.SPToSolr(spitem);
                    solrWorkerDM.Add(oDanhMucSolr);
                    solrWorkerDM.Commit();
                }
            }
            catch (Exception ex)
            {
                outtb = ex.Message;
            }
            return outtb;
        }
        /// <summary>
        /// update 1 field
        /// </summary>
        /// <param name="ItemID"></param>
        /// <param name="field"></param>
        /// <param name="Values"></param>
        /// <returns></returns>
        public string UpdateOneField(int ItemID, string field, object Values, bool overwrite = true)
        {
            string outtb = string.Empty;
            try
            {
                SPListItem spitem = SpListProcess.GetItemByIdSelectedFields(ItemID, field);
                if (overwrite) //ghi đè
                {
                    spitem[field] = Values;
                    spitem.Update();
                }
                else //nếu không ghi đè
                {
                    if (spitem[field] == null) //chưa có thì mới ghi đè
                    {
                        spitem[field] = Values;
                        spitem.Update();
                    }
                }

            }
            catch (Exception ex)
            {
                outtb = ex.Message;
            }
            return outtb;
        }


        public string SystemUpdateOneField(int ItemID, string field, object Values, bool overwrite = true)
        {
            string outtb = string.Empty;
            try
            {
                SPListItem spitem = SpListProcess.GetItemByIdSelectedFields(ItemID, field);
                if (overwrite) //ghi đè
                {
                    spitem[field] = Values;
                    spitem.ModerationInformation.Status = spitem.ModerationInformation.Status;
                    spitem.SystemUpdate();
                }
                else //nếu không ghi đè
                {
                    if (spitem[field] == null) //chưa có thì mới ghi đè
                    {
                        spitem[field] = Values;
                        spitem.ModerationInformation.Status = spitem.ModerationInformation.Status;
                        spitem.SystemUpdate();
                    }
                }

            }
            catch (Exception ex)
            {
                outtb = ex.Message;
            }
            return outtb;
        }
        /// <summary>
        /// Fix cho truwongf hopj dac biet
        /// </summary>
        /// <param name="spQuery"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="txtOrderBy"></param>
        /// <returns></returns>
        protected SPListItemCollection getPageBySPQueryFix(SPQuery spQuery, int rowPerPage, int start, string txtOrderBy)
        {
            SPQuery spQueryTemp = new SPQuery();
            spQueryTemp.ViewFields = spQuery.ViewFields; //Lưu đánh dấu các trường cần lấy
            spQuery.ViewFields = "<FieldRef Name=\"ID\"/>";
            spQuery.ViewFieldsOnly = true;
            spQuery.RowLimit = 0;
            SPListItemCollection TableFull = SpListProcess.GetItems(spQuery);
            TongSoBanGhiSauKhiQuery = TableFull.Count;

            if (start > 0) //nếu từ trang 2 trở đi thì dùng cách này
            {
                ///Tạo câu lệnh query mới
                #region tạo câu lệnh query mới
                StringBuilder stbOrQueryNew = new StringBuilder();
                stbOrQueryNew.Append(txtOrderBy);
                stbOrQueryNew.Append("<Where>");
                if (TableFull != null && TongSoBanGhiSauKhiQuery > 0)
                {
                    int intBeginFor = start; //index Bản ghi đầu tiên cần lấy trong bảng
                    int intEndFor = start + rowPerPage; //index bản ghi cuối
                    intEndFor = (intEndFor > TongSoBanGhiSauKhiQuery) ? TongSoBanGhiSauKhiQuery : intEndFor; //nếu vượt biên lấy row cuối
                    stbOrQueryNew.Append("      <In>");
                    stbOrQueryNew.Append("         <FieldRef Name=\"ID\" />");
                    stbOrQueryNew.Append("         <Values>");
                    for (int rowIndex = intBeginFor; rowIndex < intEndFor; rowIndex++)
                    {
                        stbOrQueryNew.AppendFormat("         <Value Type=\"Counter\">{0}</Value>", TableFull[rowIndex].ID);
                    }
                    stbOrQueryNew.Append("         </Values>");
                    stbOrQueryNew.Append("      </In>");
                }
                else
                    stbOrQueryNew.Append("<IsNull><FieldRef Name=\"ID\" /></IsNull>");
                stbOrQueryNew.Append("</Where>");
                #endregion
                spQueryTemp.Query = stbOrQueryNew.ToString();
                SPListItemCollection spListReturn = SpListProcess.GetItems(spQueryTemp);
                return spListReturn;
            }
            else
            {
                //chỉ có trang thì dùng luôn câu qury da dc build
                spQueryTemp.Query = spQuery.Query;
                spQueryTemp.RowLimit = (uint)rowPerPage;
                SPListItemCollection spListReturn = SpListProcess.GetItems(spQueryTemp);
                int Counttemp = spListReturn.Count;
                return spListReturn;
            }
        }
        /// <summary>
        /// Get Số thứ tự tiếp theo trên danh mục
        /// </summary>
        /// <returns></returns>
        public int GetLastSTT(string FieldSTT = "DMSTT")
        {
            SPQuery oquery = new SPQuery();
            oquery.ViewFields = BuildViewField("ID", FieldSTT);
            StringBuilder txtQuery = new StringBuilder();
            txtQuery.Append(StringOrderBy(FieldSTT, false));
            txtQuery.Append("<Where>");
            txtQuery.Append(getstringNotNull(FieldSTT));
            txtQuery.Append("</Where>");
            oquery.Query = txtQuery.ToString();
            oquery.RowLimit = 1;
            SPListItemCollection allitems = SpListProcess.GetItems(oquery);
            if (allitems.Count > 0)
            {
                return Convert.ToInt32(allitems[0][FieldSTT]) + 1;
            }
            else return 1;
        }
        protected List<BaseFileAttach> getListBaseAttackFileBySPListItem(SPListItem Item)
        {
            List<BaseFileAttach> ListFile = new List<BaseFileAttach>();
            BaseFileAttach file;
            int countfile = Item.Attachments.Count;
            for (int index = 0; index < countfile; index++) //duyệt danh sách file đính kèm
            {
                file = new BaseFileAttach();
                file.Name = Item.Attachments[index];
                file.Url = Item.Attachments.UrlPrefix + file.Name;
                ListFile.Add(file);
            }
            return ListFile;
        }
        public LookupData GetLoookupByStringFiledData(string fieldData)
        {
            List<LookupData> ListData = new List<LookupData>();
            LookupData objData = new LookupData();
            if (!string.IsNullOrEmpty(fieldData))
            {
                string[] splitData = fieldData.Split('#');
                objData.ID = Convert.ToInt32(splitData[0].Replace(";", ""));
                objData.Title = splitData[0 + 1].Replace(";", "");
            }
            return objData;
        }
        public List<LookupData> GetDanhSachMutilLoookupByStringFiledData(string fieldData)
        {
            List<LookupData> ListData = new List<LookupData>();
            LookupData objData;
            if (!string.IsNullOrEmpty(fieldData))
            {
                string[] splitData = fieldData.Split('#');
                for (int index = 0; index < splitData.Length; index = index + 2)
                {
                    objData = new LookupData();
                    objData.ID = Convert.ToInt32(splitData[index].Replace(";", ""));
                    objData.Title = splitData[index + 1].Replace(";", "");
                    ListData.Add(objData);
                }
            }
            return ListData;
        }
        protected List<FileAttach> getListAttackFileBySPListItem(SPListItem Item)
        {
            List<FileAttach> ListFile = new List<FileAttach>();
            FileAttach file;
            string urlListItem = SpListProcess.DefaultViewUrl.Substring(0, SpListProcess.DefaultViewUrl.LastIndexOf("/")); //lấy về địa chỉ url của SPList
            if (Item.Attachments.Count > 0)
            {
                for (int index = 0; index < Item.Attachments.Count; index++) //duyệt danh sách file đính kèm
                {
                    file = new FileAttach();
                    file.Name = Item.Attachments[index];
                    file.Url = Item.Attachments.UrlPrefix + file.Name;
                    ListFile.Add(file);
                }
            }

            return ListFile;
        }
        /// <summary>
        /// Build câu truy vấn tìm kiếm từ khóa
        /// </summary>
        /// <param name="SearchInFiled"></param>
        /// <param name="Keyword"></param>
        /// <returns></returns>
        public string getDynamicSearch(List<string> SearchInField, string Keyword)
        {
            StringBuilder stbOrQuery = new StringBuilder();
            if (!string.IsNullOrEmpty(Keyword))
            {
                string strBuildFieldSearch = string.Empty;
                int intTotal = SearchInField.Count;
                if (intTotal > 0)
                {
                    #region Build caau lenh Caml Contains
                    if (intTotal >= 2)
                    {
                        for (int index = 1; index < SearchInField.Count; index++)
                        {
                            stbOrQuery.Append("<Or>");
                        }
                        for (int index = 0; index < SearchInField.Count; index++)
                        {
                            stbOrQuery.Append("      <Contains>");
                            stbOrQuery.Append("         <FieldRef Name=\"" + SearchInField[index] + "\" />");
                            stbOrQuery.AppendFormat("   <Value Type=\"Text\">{0}</Value>", HttpUtility.HtmlEncode(Keyword));
                            stbOrQuery.Append("      </Contains>");
                            if (index != 0)
                                stbOrQuery.Append("</Or>");
                        }
                    }
                    else
                    {
                        stbOrQuery.Append("      <Contains>");
                        stbOrQuery.Append("<FieldRef Name=\"" + SearchInField[0] + "\" />");
                        stbOrQuery.Append("<Value Type=\"Text\">" + HttpUtility.HtmlEncode(Keyword) + "</Value>");
                        stbOrQuery.Append("      </Contains>");
                    }
                    #endregion
                }
                else
                {
                    return "<IsNotNull><FieldRef Name=\"ID\" /></IsNotNull>";
                }
            }
            else
            {
                return "<IsNotNull><FieldRef Name=\"ID\" /></IsNotNull>";
            }
            return stbOrQuery.ToString();
        }

        public SPList SpListProcess { get; set; }

        public string FullPathUrlListProcess { get; set; }

        public int TongSoBanGhiSauKhiQuery { get; set; }
        public string Queryreturn { get; set; }

        public void UpdateSPModerationStatus(int id, SPModerationStatusType SPModeration, UpdateType updateType = UpdateType.SystemUpdate)
        {
            SPListItem spItem = SpListProcess.GetItemById(id);
            spItem.ModerationInformation.Status = SPModeration;
            switch (updateType)
            {
                case UpdateType.SystemUpdate:
                    spItem.SystemUpdate();

                    //spItem.UpdateWithSolr();
                    break;
                case UpdateType.Update:
                    spItem.Update();
                    break;
                case UpdateType.UpdateOverwriteVersion:
                    spItem.SystemUpdate(true);
                    break;
            }
            if (isHasSolr)
            {
                U oDanhMucSolr = new U();
                oDanhMucSolr.SPToSolr(spItem);
                solrWorkerDM.Add(oDanhMucSolr);
                solrWorkerDM.Commit();
            }
        }
        /// <summary>
        /// Hàm check exit, return true tồn tại, false nếu không
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="Title"></param>
        /// <param name="Field"></param>
        /// <returns></returns>
        public int CheckExit(int ID, string Value, string Field = "Title")
        {
            SPQuery oQuery = new SPQuery();
            oQuery.ViewFields = BuildViewField("ID");
            oQuery.ViewFieldsOnly = true;
            StringBuilder oBuilder = new StringBuilder();
            oBuilder.Append("<Where>");

            if (!string.IsNullOrEmpty(Value))
                oBuilder.Append("<And>");
            if (ID > 0)
                oBuilder.Append("<And>");
            oBuilder.Append(getstringNotNull());
            if (ID > 0)
            {
                oBuilder.Append(getstringNumNotEQ("ID", ID));
                oBuilder.Append("</And>");
            }
            if (!string.IsNullOrEmpty(Value))
            {
                oBuilder.Append(GetStringEQText(Field, Value));
                oBuilder.Append("</And>");
            }

            oBuilder.Append("</Where>");
            oQuery.Query = oBuilder.ToString();
            oQuery.RowLimit = 1;
            SPListItemCollection spColl = SpListProcess.GetItems(oQuery);
            if (spColl.Count > 0)
                return spColl[0].ID;
            else return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Querys"></param>
        /// <param name="Oper"></param>
        /// <returns></returns>
        public string BuildQueryToString(List<ISolrQuery> Querys, string Oper = "AND")
        {
            //string Queryreturn = "";
            List<string> lstQuery = new List<string>();
            foreach (var item in Querys)
            {
                var type = item.GetType();
                var type2 = typeof(SolrQuery);
                if (type == typeof(SolrQuery))
                {
                    lstQuery.Add(((SolrQuery)item).Query);
                }
                if (type == typeof(SolrQueryByField))
                {
                    lstQuery.Add(((SolrQueryByField)item).FieldName + ":" + ((SolrQueryByField)item).FieldValue);
                }
                if (type == typeof(SolrNet.SolrQueryByRange<DateTime>))
                {
                    var tempdate = (SolrQueryByRange<DateTime>)item;
                    lstQuery.Add(string.Format("{0}:[{1} TO {2}]", tempdate.FieldName, SPUtility.CreateISO8601DateTimeFromSystemDateTime(tempdate.From), SPUtility.CreateISO8601DateTimeFromSystemDateTime(tempdate.To)));
                    //lstQuery.Add(((SolrQueryByField)item).FieldName + ":" + ((SolrQueryByField)item).FieldValue);
                }
                if (type == typeof(SolrNet.SolrQueryByRange<DateTime?>))
                {
                    var tempdate = (SolrQueryByRange<DateTime?>)item;
                    lstQuery.Add(string.Format("{0}:[{1} TO {2}]", tempdate.FieldName, SPUtility.CreateISO8601DateTimeFromSystemDateTime(tempdate.From.Value), SPUtility.CreateISO8601DateTimeFromSystemDateTime(tempdate.To.Value)));
                    //lstQuery.Add(((SolrQueryByField)item).FieldName + ":" + ((SolrQueryByField)item).FieldValue);
                }
                if (type == typeof(SolrNet.SolrNotQuery))
                {
                    //Unable to cast object of type 'SolrNet.SolrQueryByField' to type 'SolrNet.SolrQuery'.
                    ISolrQuery ISolrQuerysub = (((SolrNotQuery)item)).Query;
                    if (ISolrQuerysub.GetType() == typeof(SolrQuery))
                    {
                        lstQuery.Add("!" + ((SolrQuery)(((SolrNotQuery)item).Query)).Query);
                    }
                    if (ISolrQuerysub.GetType() == typeof(SolrQueryByField))
                    {

                        lstQuery.Add("!" + ((SolrQueryByField)ISolrQuerysub).FieldName + ":" + ((SolrQueryByField)ISolrQuerysub).FieldValue);
                    }
                    //lstQuery.Add(((SolrQuery)((SolrNotQuery)item)).Query);

                }
                else if (item.GetType() == typeof(SolrMultipleCriteriaQuery))
                {
                    SolrMultipleCriteriaQuery temp = (SolrMultipleCriteriaQuery)item;

                    string querymulti = BuildQueryToString(temp.Queries.ToList(), temp.Oper); // string.Join(" " + temp.Oper + " ", temp.Queries.Select(x => ((SolrQuery)x).Query));
                    if (!string.IsNullOrEmpty(querymulti))
                        lstQuery.Add("(" + querymulti + ")");
                }
                //SolrNet.SolrQueryByRange
            }
            Queryreturn = string.Join(" " + Oper + " ", lstQuery);
            return Queryreturn;
        }


        public string GetQueryDateTimeSolrGeq(string FieldName, DateTime valueDate)
        {
            return string.Format("{0}:[{1} TO *]", FieldName, SPUtility.CreateISO8601DateTimeFromSystemDateTime(valueDate));
        }
        public string GetQueryDateTimeSolrLeq(string FieldName, DateTime valueDate)
        {
            return string.Format("{0}:[* TO {1}]", FieldName, SPUtility.CreateISO8601DateTimeFromSystemDateTime(valueDate));
        }
        //cannb 13/08/2019
        public string GetQueryDateTimeSolrEq(string FieldName, DateTime valueDate, bool includeTime = false)
        {
            if (includeTime)
                return string.Format("{0}:\"{1}\"", FieldName, SPUtility.CreateISO8601DateTimeFromSystemDateTime(valueDate));
            else
                return string.Format("{0}:[\"{1}\" TO \"{2}\"]", FieldName, SPUtility.CreateISO8601DateTimeFromSystemDateTime(valueDate), SPUtility.CreateISO8601DateTimeFromSystemDateTime(valueDate.AddDays(1).AddTicks(-1)));
        }

        public string SaveSolr(int ItemID)
        {
            try
            {
                if (isHasSolr)
                {
                    SPListItem spitem = SpListProcess.GetItemById(ItemID);
                    U oDanhMucSolr = new U();
                    oDanhMucSolr.SPToSolr(spitem);
                    solrWorkerDM.Add(oDanhMucSolr);
                    solrWorkerDM.Commit();
                }
                return "";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


    }
}
