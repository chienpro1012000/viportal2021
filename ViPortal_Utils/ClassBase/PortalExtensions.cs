﻿using SolrNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ViPortal_Utils.Base;

namespace ViPortal_Utils
{
    public static class CollectionHelper
    {
        public static void AddRangeOverride<TKey, TValue>(this IDictionary<TKey, TValue> dic, IDictionary<TKey, TValue> dicToAdd)
        {
            dicToAdd.ForEach(x => dic[x.Key] = x.Value);
        }

        public static void AddRangeNewOnly<TKey, TValue>(this IDictionary<TKey, TValue> dic, IDictionary<TKey, TValue> dicToAdd)
        {
            dicToAdd.ForEach(x => { if (!dic.ContainsKey(x.Key)) dic.Add(x.Key, x.Value); });
        }

        public static void AddRange<TKey, TValue>(this IDictionary<TKey, TValue> dic, IDictionary<TKey, TValue> dicToAdd)
        {
            dicToAdd.ForEach(x => dic.Add(x.Key, x.Value));
        }

        public static bool ContainsKeys<TKey, TValue>(this IDictionary<TKey, TValue> dic, IEnumerable<TKey> keys)
        {
            bool result = false;
            keys.ForEachOrBreak((x) => { result = dic.ContainsKey(x); return result; });
            return result;
        }

        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (var item in source)
                action(item);
        }

        public static void ForEachOrBreak<T>(this IEnumerable<T> source, Func<T, bool> func)
        {
            foreach (var item in source)
            {
                bool result = func(item);
                if (result) break;
            }
        }
    }
    public static class PortalExtensions
    {
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>
    (this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }
        public static string GetTitle255(this string Titles)
        {
            if (string.IsNullOrEmpty(Titles))
                return Titles;
            else
            {
                if (Titles.Length > 250)
                    return Titles.Substring(0, 250) + "...";
                else return Titles;
            }
        }
        public static string GetTextFormHtml(this string tHtml)
        {
            if (string.IsNullOrEmpty(tHtml))
                return tHtml;
            else
            {
                return tHtml;
            }
        }
        public static List<int> GetListIDFormStringLookup(this string strvaluelk)
        {
            List<int> dictionary = new List<int>();
            if (!string.IsNullOrEmpty(strvaluelk))
            {
                strvaluelk.Trim(';', '#');
                string[] arrStr = strvaluelk.Split(new string[] { ";#" }, StringSplitOptions.RemoveEmptyEntries);
                int valueid = 0;
                for (int i = 0; i < arrStr.Length; i = i + 2)
                {
                    if (Int32.TryParse(arrStr[i], out valueid))
                        dictionary.Add(valueid);
                }
            }
            return dictionary;
        }
        public static List<string> GetListValueFormStringLookup(this string strvaluelk)
        {
            List<string> dictionary = new List<string>();
            if (!string.IsNullOrEmpty(strvaluelk))
            {
                strvaluelk.Trim(';', '#');
                string[] arrStr = strvaluelk.Split(new string[] { ";#" }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 1; i < arrStr.Length; i = i + 2)
                {

                    dictionary.Add(arrStr[i]);
                }
            }
            return dictionary;
        }
        /// <summary>
        /// hàm này dùng để check nếu lookup ko có tiêu đề thì ko hiển thị.
        /// </summary>
        /// <param name="strvaluelk"></param>
        /// <returns></returns>
        public static bool CheckLookupDelete(this string strvaluelk)
        {
            return true;
        }

        public static string GetPathAndQuery(this string UrlFUll)
        {
            if (!string.IsNullOrEmpty(UrlFUll))
            {
                Uri myuri = new Uri(UrlFUll);
                string pathQuery = myuri.PathAndQuery;
                return pathQuery;
            }
            else return "";
        }
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = (7 + (dt.DayOfWeek - startOfWeek)) % 7;
            return dt.AddDays(-1 * diff).Date;
        }
        public static string GetThuByNgay (this DateTime ToDay)
        {
            string ThuText = "Chủ nhật";
            DayOfWeek dayOfWeek =  ToDay.DayOfWeek;
            switch (dayOfWeek)
            {
                case DayOfWeek.Monday:
                    ThuText = "Thứ 2";
                    break;
                case DayOfWeek.Tuesday:
                    ThuText = "Thứ 3";
                    break;
                case DayOfWeek.Wednesday:
                    ThuText = "Thứ 4";
                    break;
                case DayOfWeek.Thursday:
                    ThuText = "Thứ 5";
                    break;
                case DayOfWeek.Friday:
                    ThuText = "Thứ 6";
                    break;
                case DayOfWeek.Saturday:
                    ThuText = "Thứ 7";
                    break;
                case DayOfWeek.Sunday:
                    ThuText = "Chủ nhật";
                    break;

            }
            return ThuText;

        }

        public static string RemoveVietnameseTone(this string text)
        {
            string result = text.ToLower();
            result = Regex.Replace(result, "à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|/g", "a");
            result = Regex.Replace(result, "è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|/g", "e");
            result = Regex.Replace(result, "ì|í|ị|ỉ|ĩ|/g", "i");
            result = Regex.Replace(result, "ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|/g", "o");
            result = Regex.Replace(result, "ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|/g", "u");
            result = Regex.Replace(result, "ỳ|ý|ỵ|ỷ|ỹ|/g", "y");
            result = Regex.Replace(result, "đ", "d");
            StringBuilder sb = new StringBuilder();
            foreach (char c in result)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
            //return result;
        }

        //search solr sear
        public static void AddSearchBase(this List<ISolrQuery> Querys, QuerySearch searhOption)
        {
            if (!string.IsNullOrEmpty(searhOption.Keyword))
            {
                if (searhOption.SearchIn.Count > 0)
                {
                    if (searhOption.SearchIn.Count > 1)
                    {
                        List<ISolrQuery> lstOr = new List<ISolrQuery>();
                        for (int index = 0; index < searhOption.SearchIn.Count; index++)
                        {
                            lstOr.Add(new SolrQuery("(" + searhOption.SearchIn[index] + ":\"" + searhOption.Keyword + "\")"));
                        }
                        Querys.Add(new SolrMultipleCriteriaQuery(lstOr, "OR"));
                    }
                    else
                        Querys.Add(new SolrQuery("(" + searhOption.SearchIn[0] + ":\"" + searhOption.Keyword + "\")"));

                }
            }
            if (searhOption.isGetBylistID)
            {
                if (searhOption.lstIDget.Count > 0)
                    Querys.Add(new SolrQuery("SP_ID:(" + string.Join(" ", searhOption.lstIDget) + ")"));
                else Querys.Add(new SolrQuery("!SP_ID:[* TO *]"));
            }
            if (searhOption._ModerationStatus == 1)
            {
                Querys.Add(new SolrQuery("_ModerationStatus:0"));
            }
        }
    }
}
