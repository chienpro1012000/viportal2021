﻿using Novacode;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
//using SIPortal.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Library_Shared.Utilities
{
    public class ClsExcel
    {
        public int row { get; set; }
        public int cell { get; set; }
        public string values { get; set; }
        public int style { get; set; }
        public bool isBorder { get; set; }
        public bool isMersg { get; set; }
        public int firstRow { get; set; }
        public int lastRow { get; set; }
        public int firstCol { get; set; }
        public int lastCol { get; set; }
        public int fontSize { get; set; }
        public ICellStyle cellStyle { get; set; }
        public ClsExcel(int row, int cell, object values, StyleExcell style, bool isBorder = true, int fontSize = 12, bool isMersg = false, int firstRow = 0, int lastRow = 0, int firstCol = 0, int lastCol = 0)
        {
            this.row = row;
            this.cell = cell;
            this.values = Convert.ToString(values);
            this.style = (int)style;
            this.isMersg = isMersg;
            this.firstRow = firstRow;
            this.lastRow = lastRow;
            this.firstCol = firstCol;
            this.lastCol = lastCol;
            this.fontSize = fontSize;
            this.isBorder = isBorder;
        }
    }
    /// <summary>
    /// Enum style excell
    /// </summary>
    public enum StyleExcell
    {
        TextLeft = 1,
        TextRight,
        TextCenter,
        TextLeftBold,
        TextRightBold,
        TextCenterBold,
        TextDatetime,
    }
    public class ClsWord
    {/// <summary>
     /// id của bản ghi json
     /// </summary>
        public int Row { get; set; }
        /// <summary>
        /// stt ô trong bảng
        /// </summary>
        public int Cell { get; set; }

        public Alignment alignment { get; set; }
        public int fontsize { get; set; }
        public string Value { get; set; }
        /// <summary>
        /// 0 mac dinh
        /// 1 row tổng
        /// </summary>
        public int RowMau { get; set; }
        public bool isBold { get; set; }
        public bool isItalic { get; set; }
        public ClsWord()
        {
            fontsize = 11;
            alignment = Alignment.left;
        }
        public ClsWord(int Row, int Cell, object Value, Alignment alignment, int fontsize = 11, int RowMau = 1, bool isBold = false, bool isItalic = false)
        {
            this.fontsize = fontsize;
            this.alignment = alignment;
            this.Row = Row;
            this.Cell = Cell;
            this.Value = Convert.ToString(Value);
            this.RowMau = RowMau;
            this.isBold = isBold;
            this.isItalic = isItalic;
        }
    }
    public static class BaseWordExcel
    {
        public static HSSFWorkbook ExportExcell(HSSFWorkbook hssBook, List<ClsExcel> lstValueExcel, string sheetName = "Sheet1")
        {
            ISheet ThongKe = hssBook.GetSheet(sheetName);
            IRow row = null;
            List<ClsExcel> lstStyle = SetCellStyle(hssBook, lstValueExcel);
            foreach (var item in lstValueExcel)
            {
                row = ThongKe.GetRow(item.row);
                if (row == null)
                    row = ThongKe.CreateRow(item.row);
                if (item.isMersg)
                    ThongKe.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(item.firstRow, item.lastRow, item.firstCol, item.lastCol));
                if (lstStyle.Count(x => x.style == item.style && x.fontSize == item.fontSize && x.isBorder == item.isBorder) > 0)
                    SetValueCellExcel(row, item.cell, item.values, lstStyle.FirstOrDefault(x => x.style == item.style && x.fontSize == item.fontSize && x.isBorder == item.isBorder).cellStyle);
                else
                    SetValueCellExcel(row, item.cell, item.values, null);
            }
            return hssBook;
        }

        /// <summary>
        /// Export excell
        /// </summary>
        /// <param name="hssBook">document</param>
        /// <param name="lstValueExcel">List Values class ClsExcel</param>
        /// <param name="sheetName">sheet name</param>
        /// <returns></returns>
        public static HSSFWorkbook ExportExcellHiddenColumn(HSSFWorkbook hssBook, List<ClsExcel> lstValueExcel, int column = -1, string sheetName = "Sheet1")
        {
            ISheet ThongKe = hssBook.GetSheet(sheetName);
            IRow row = null;
            List<ClsExcel> lstStyle = SetCellStyle(hssBook, lstValueExcel);
            foreach (var item in lstValueExcel)
            {
                row = ThongKe.GetRow(item.row);
                if (row == null)
                    row = ThongKe.CreateRow(item.row);
                if (item.isMersg)
                    ThongKe.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(item.firstRow, item.lastRow, item.firstCol, item.lastCol));
                if (lstStyle.Count(x => x.style == item.style && x.fontSize == item.fontSize && x.isBorder == item.isBorder) > 0)
                    SetValueCellExcel(row, item.cell, item.values, lstStyle.FirstOrDefault(x => x.style == item.style && x.fontSize == item.fontSize && x.isBorder == item.isBorder).cellStyle);
                else
                    SetValueCellExcel(row, item.cell, item.values, null);
            }
            if (column > -1)
                ThongKe.SetColumnHidden(column, true);
            return hssBook;
        }
        /// <summary>
        /// Export excell
        /// </summary>
        /// <param name="hssBook">document</param>
        /// <param name="lstValueExcel">List Values class ClsExcel</param>
        /// <param name="sheetName">sheet name</param>
        /// <returns></returns>
        public static void ExportExcellV2(Stream FileExcel, List<ClsExcel> lstValueExcel, string FileName, string sheetName = "Sheet1")
        {
            HSSFWorkbook hssBook = new HSSFWorkbook(FileExcel);
            ISheet ThongKe = hssBook.GetSheet(sheetName);
            IRow row = null;
            List<ClsExcel> lstStyle = SetCellStyle(hssBook, lstValueExcel);
            foreach (var item in lstValueExcel)
            {
                row = ThongKe.GetRow(item.row);
                if (row == null)
                    row = ThongKe.CreateRow(item.row);
                if (item.isMersg)
                    ThongKe.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(item.firstRow, item.lastRow, item.firstCol, item.lastCol));
                if (lstStyle.Count(x => x.style == item.style && x.fontSize == item.fontSize && x.isBorder == item.isBorder) > 0)
                    SetValueCellExcel(row, item.cell, item.values, lstStyle.FirstOrDefault(x => x.style == item.style && x.fontSize == item.fontSize && x.isBorder == item.isBorder).cellStyle);
                else
                    SetValueCellExcel(row, item.cell, item.values, null);
            }
            using (MemoryStream exportData = new MemoryStream())
            {
                hssBook.Write(exportData);
                HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", FileName + "_" + DateTime.Now.ToString("dd/MM/yyyy")));
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                HttpContext.Current.Response.End();
                HttpContext.Current.Response.Flush();
            }
        }
        /// <summary>
        /// format cell excel
        /// </summary>
        /// <param name="hssBook">document</param>
        /// <param name="textStyle">style text</param>
        /// <param name="fonsize">font size</param>
        /// <param name="isBorder">has boder</param>
        /// <param name="fontFamily">font family</param>
        /// <returns></returns>
        public static List<ClsExcel> SetCellStyle(HSSFWorkbook hssBook, List<ClsExcel> lstValueExcel)
        {
            List<ClsExcel> lstGroup = new List<ClsExcel>();
            foreach (var item in lstValueExcel)
            {
                if (lstGroup.Count(x => x.style == item.style && x.fontSize == item.fontSize && x.isBorder == item.isBorder) == 0)
                {
                    if (item.style > 0)
                    {
                        IFont foThuong = hssBook.CreateFont();
                        foThuong.FontHeightInPoints = (short)item.fontSize;
                        foThuong.FontName = "Times New Roman";
                        ICellStyle csSTNomarl = hssBook.CreateCellStyle();
                        if (item.isBorder)
                        {
                            csSTNomarl.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                            csSTNomarl.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                            csSTNomarl.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                            csSTNomarl.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                            csSTNomarl.BottomBorderColor = HSSFColor.Black.Index;
                            csSTNomarl.TopBorderColor = HSSFColor.Black.Index;
                            csSTNomarl.RightBorderColor = HSSFColor.Black.Index;
                            csSTNomarl.LeftBorderColor = HSSFColor.Black.Index;
                        }
                        csSTNomarl.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;

                        switch (item.style)
                        {
                            case 1:
                                csSTNomarl.Alignment = HorizontalAlignment.Left;
                                break;
                            case 2:
                                csSTNomarl.Alignment = HorizontalAlignment.Right;
                                break;
                            case 3:
                                csSTNomarl.Alignment = HorizontalAlignment.Center;
                                break;
                            case 4:
                                csSTNomarl.Alignment = HorizontalAlignment.Left;
                                foThuong.Boldweight = (short)800;
                                break;
                            case 5:
                                csSTNomarl.Alignment = HorizontalAlignment.Right;
                                foThuong.Boldweight = (short)800;
                                break;
                            case 6:
                                csSTNomarl.Alignment = HorizontalAlignment.Center;
                                foThuong.Boldweight = (short)800;
                                break;
                            case 7:
                                csSTNomarl.Alignment = HorizontalAlignment.Center;
                                foThuong.IsItalic = true;
                                break;
                        }
                        csSTNomarl.WrapText = true;
                        csSTNomarl.SetFont(foThuong);
                        item.cellStyle = csSTNomarl;
                        lstGroup.Add(item);
                    }
                }
            }
            return lstGroup;
        }
        /// <summary>
        /// Set value cell
        /// </summary>
        /// <param name="objRow"></param>
        /// <param name="STTCell"></param>
        /// <param name="value"></param>
        /// <param name="oICellStyle"></param>
        public static void SetValueCellExcel(IRow objRow, int Cell, string value, ICellStyle oICellStyle)
        {
            ICell objICell = objRow.GetCell(Cell);
            if (objICell == null)
                objICell = objRow.CreateCell(Cell);
            if (oICellStyle != null)
                objICell.CellStyle = oICellStyle;
            objICell.SetCellValue(value);
        }
        

        public static void SetValueNumberCellExcel(IRow objRow, int STTCell, int value, ICellStyle oICellStyle)
        {
            ICell objICell = objRow.GetCell(STTCell);
            if (objICell == null)
                objICell = objRow.CreateCell(STTCell);
            objICell.CellStyle = oICellStyle;
            objICell.SetCellValue(value);
        }
        //public static string GetFileView(int ItemID, List<FileAttach> lstFile, string Type)
        //{
        //    FileAttach oFile = new FileAttach();
        //    if (lstFile.Count > 0)
        //    {
        //        if (lstFile.Count(x => !string.IsNullOrEmpty(x.Name) && x.Name.Contains(".pdf")) > 0)
        //            oFile = lstFile.FirstOrDefault(x => !string.IsNullOrEmpty(x.Name) && x.Name.Contains(".pdf"));
        //        else
        //            oFile = lstFile.FirstOrDefault();
        //    }

        //    var txtBase64 = string.Empty;
        //    var FileMaHoa = 0;
        //    string urlfile = "";
        //    var typefile = "";

        //    if (!string.IsNullOrEmpty(oFile.Url))
        //    {

        //        urlfile = Convert.ToString(oFile.Url.Trim());

        //        if (!oFile.Url.Contains("botp/Lists"))
        //        {
        //            return oFile.Url;
        //        }
        //        else

        //        //string urlimg = string.Empty;
        //        //int inttypefile = urlfile.IndexOf('.', urlfile.Length - 5);
        //        //if (inttypefile != -1)
        //        //{
        //        //    typefile = urlfile.Substring(inttypefile + 1).ToLower();
        //        //}

        //        /////nếu những file nào lưu trên ổ đia thì phải được giải mã ngược trước khi ghi ra
        //        /////tạm thời xử lý với file định dạng pdf, ảnh, những cái khác chưa được
        //        //if (!string.IsNullOrEmpty(oFile.Url) && !oFile.Url.Contains("/Lists/"))
        //        //{
        //        //    string path = System.Configuration.ConfigurationManager.AppSettings["FileData"];
        //        //    string fullurl = path + urlfile;
        //        //    string filename = Path.GetFileName(fullurl);
        //        //    string filePathGoc = "/Uploads/ajaxUpload/";
        //        //    string sourceFile = HttpContext.Current.Server.MapPath(filePathGoc + filename);

        //        //    if (!File.Exists(sourceFile))
        //        //    {
        //        //        //string path = System.Configuration.ConfigurationManager.AppSettings["FileData"];
        //        //        string FileDataBackUp = System.Configuration.ConfigurationManager.AppSettings["FileDataBackUp"];
        //        //        string fullurlBackup = FileDataBackUp + urlfile;
        //        //        //string fullurl = path + urlfile;

        //        //       // byte[] arrayfile = SPUtils.GetByteFileGiaiMa(fullurl, fullurlBackup);
        //        //        //byte[] arrayfile = SPUtils.GetByteFileGiaiMa(fullurl);
        //        //      //  File.WriteAllBytes(sourceFile, arrayfile);
        //        //    }
        //        //    urlfile = filePathGoc + filename;
        //        //}
        //        // else
        //        {
        //            string UrlFile = "/Uploads/ajaxUpload";
        //            string strReturn = UrlFile;
        //            UrlFile = System.Web.HttpContext.Current.Server.MapPath(UrlFile + "/" + Type + "/" + ItemID);
        //            if (!Directory.Exists(UrlFile))
        //                Directory.CreateDirectory(UrlFile);
        //            UrlFile += @"\" + oFile.Name;
        //            ///Check xem file đã tồn tại chưa, nếu tồn tại thì trả về đường dẫn
        //            ///chưa tồn tại thì tạo mới
        //            if (!File.Exists(UrlFile))
        //            {
        //                File.WriteAllBytes(UrlFile, oFile.DataFile);
        //            }
        //            urlfile = strReturn + "/" + Type + "/" + ItemID + "/" + oFile.Name;
        //        }

        //    }

        //    //if (typefile == "tif")
        //    //{
        //    //    byte[] data = Utils.SPUtils.ReadSharepointFile(urlfile);
        //    //    string filename = urlfile.Substring(urlfile.LastIndexOf('/') + 1);

        //    //    urlfile = "/Uploads/ajaxUpload/" + filename;


        //    //    string filepath = HttpContext.Current.Server.MapPath("/Uploads/ajaxUpload/" + filename);


        //    //    using (FileStream stream = new FileStream(filepath, FileMode.Create))
        //    //    {
        //    //        stream.Write(data, 0, data.Length);
        //    //    }
        //    //}
        //    return urlfile;
        //}

        /// <summary>
        /// Can
        /// </summary>
        /// <param name="TyleConfig"></param>
        /// <param name="dict"></param>
        /// <param name="clsWords"></param>
        /// <param name="namefile"></param>
        /// <param name="sottbang">-1 thi ko co bang</param>
        /// <returns></returns>
        public static void XuatWordChung(Stream pathoutdocx, Dictionary<string, string> dict, List<ClsWord> clsWords, string namefile, int sottbang = 0, int removeColumn = 0)
        {

            Novacode.DocX document = Novacode.DocX.Load(pathoutdocx);
            // document.PageLayout.Orientation = Novacode.Orientation.Landscape;
            string doctext = document.Text;
            foreach (var item in dict)
            {
                document.ReplaceText(item.Key, item.Value);
            }
            if (sottbang > -1)
            {// lấy ra các hàng mẫu trong template
                List<int> lstRowMau = clsWords.Select(x => x.RowMau).Distinct().ToList();
                //sắp xếp từ lớn đến bé
                lstRowMau.Sort((a, b) => -1 * a.CompareTo(b));
                Novacode.Table oTable = document.Tables[sottbang];
                if (removeColumn > 0)
                    oTable.RemoveColumn(removeColumn);
                Novacode.Row Rowtitle = oTable.Rows[0];
                Dictionary<int, Novacode.Row> myList = new Dictionary<int, Novacode.Row>();
                foreach (int rowmau in lstRowMau)
                {
                    Novacode.Row Rowmau = oTable.Rows[rowmau];
                    myList.Add(rowmau, Rowmau);
                    oTable.RemoveRow(rowmau);
                }
                Novacode.Row myRow;
                foreach (var data in clsWords.GroupBy(x => x.Row))
                {
                    myRow = oTable.InsertRow(myList[data.FirstOrDefault().RowMau]);
                    foreach (var item in data)
                    {
                        myRow.Cells[item.Cell].Paragraphs.First().Append(item.Value).FontSize(item.fontsize).Alignment = item.alignment;
                        if (item.isBold)
                            myRow.Cells[item.Cell].Paragraphs.First().Bold();
                        if (item.isItalic)
                            myRow.Cells[item.Cell].Paragraphs.First().Italic();
                    }
                }
            }

            using (MemoryStream exportData = new MemoryStream())
            {
                document.SaveAs(exportData);
                document.Dispose();
                HttpContext.Current.Response.ContentType = "application/msword";
                HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.docx", namefile));
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();

            }

        }
    }
}
