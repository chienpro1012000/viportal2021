﻿using Microsoft.SharePoint;
using SolrNet.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViPortal_Utils
{
    public class DanhMucSolr : EntitySolr
    {
        [SolrField()] public string DescriptionNews { get; set; }
        [SolrField()] public DateTime? CreatedDate { get; set; }
        [SolrField()] public string ImageNews { get; set; }
        [SolrField()] public string TacGia { get; set; }
        [SolrField()] public bool? isHotNew { get; set; }
        [SolrField()] public string ContentNews_Id { get; set; }
        [SolrField()] public string DanhMucTin { get; set; }
        [SolrField()] public string NguoiTao { get; set; }
        [SolrField()] public string NguoiTao_Id { get; set; }
        [SolrField()] public string CreatedUser { get; set; }
        [SolrField()] public string CreatedUser_Id { get; set; }
        [SolrField()] public string LichDiaDiem { get; set; }
        [SolrField()] public string LichGhiChu { get; set; }
        [SolrField()] public string LichLanhDaoChuTri { get; set; }
        [SolrField()] public string LichNoiDung { get; set; }
        [SolrField()] public string LichThanhPhanThamGia { get; set; }
        [SolrField()] public DateTime? LichThoiGianBatDau { get; set; }
        [SolrField()] public DateTime? LichThoiGianKetThuc { get; set; }
        [SolrField()] public int? LichTrangThai { get; set; }


        [SolrField()] public string DMVietTat { get; set; }
        [SolrField()] public string ColumnImage_Id { get; set; }
        [SolrField()] public string UrlSite { get; set; }
        [SolrField()] public string MoTa { get; set; }
        [SolrField()] public bool? Public { get; set; }
        [SolrField()] public int? STT { get; set; }
        [SolrField()] public string DMDescription { get; set; }
        [SolrField()] public bool? DMHienThi { get; set; }
        [SolrField()] public string DMMoTa { get; set; }
        [SolrField()] public int? DMSTT { get; set; }
        [SolrField()] public string DBDiaChi { get; set; }
        [SolrField()] public string DBEmail { get; set; }
        [SolrField()] public string DBFax { get; set; }
        [SolrField()] public int DBPhanLoai { get; set; }
        [SolrField()] public string DBSDT { get; set; }
        [SolrField()] public string DBTreeID { get; set; }
        [SolrField()] public string DBWebsite { get; set; }
        [SolrField()] public string DBParentPB { get; set; }
        [SolrField()] public string DBParentPB_Id { get; set; }
        [SolrField()] public string DBNhom { get; set; }
        [SolrField()] public string isOffice { get; set; }
        [SolrField()] public bool? IsDonVi { get; set; }
        [SolrField()] public string DBChucVu { get; set; }
        [SolrField()] public string DBChucVu_Id { get; set; }

        [SolrField()] public string DMAnhListAnh { get; set; }
        [SolrField()] public DateTime? TBNgayDang { get; set; }
        [SolrField()] public int? DMSLAnh { get; set; }
        [SolrField()] public string FAQCauHoi { get; set; }
        [SolrField()] public DateTime? FAQNgayHoi { get; set; }
        [SolrField()] public DateTime? FAQNgayTraLoi { get; set; }
        [SolrField()] public string FAQNoiDungTraLoi { get; set; }
        [SolrField()] public int? FAQSTT { get; set; }
        [SolrField()] public string FQNguoiHoi { get; set; }
        [SolrField()] public string FQNguoiHoi_Id { get; set; }
        [SolrField()] public string FQNguoiTraLoi { get; set; }
        [SolrField()] public string FQNguoiTraLoi_Id { get; set; }
        [SolrField()] public int? FAQLuotXem { get; set; }
        [SolrField()] public string Album { get; set; }
        [SolrField()] public string Album_Id { get; set; }
        [SolrField()] public DateTime? NgayDang { get; set; }
        [SolrField()] public string NoiDungTraLoi { get; set; }
        [SolrField()] public string FAQLinhVuc { get; set; }
        [SolrField()] public string FAQLinhVuc_Id { get; set; }
        [SolrField()] public int? KSHinhThucTraLoi { get; set; }
        [SolrField()] public string KSBoChuDe { get; set; }
        [SolrField()] public string KSBoChuDe_Id { get; set; }
        [SolrField()] public int? LoaiTraLoi { get; set; }
        [SolrField()] public DateTime? KSDenNgay { get; set; }
        [SolrField()] public DateTime? KSTuNgay { get; set; }
        [SolrField()] public string UCThamGia { get; set; }
        [SolrField()] public string KSCauHoi { get; set; }
        [SolrField()] public string KSCauHoi_Id { get; set; }
        [SolrField()] public int? SoLuotBinhChon { get; set; }
        [SolrField()] public string KSCauTraLoi { get; set; }
        [SolrField()] public string KSCauTraLoi_Id { get; set; }
        [SolrField()] public string KSCauTraLoiNoiDung { get; set; }
        [SolrField()] public string NoiDungChuanBi { get; set; }
        [SolrField()] public string Link { get; set; }
        [SolrField()] public string LNotiDaXem { get; set; }
        [SolrField()] public int? LNotiIsSentFirebase { get; set; }
        [SolrField()] public int? LNotiIsSentMail { get; set; }
        [SolrField()] public string LNotiMoTa { get; set; }
        [SolrField()] public string LNotiNguoiNhan { get; set; }
        [SolrField()] public string LNotiNguoiNhan_Id { get; set; }
        [SolrField()] public string LNotiNoiDung { get; set; }
        [SolrField()] public DateTime? LNotiSentTime { get; set; }
        [SolrField()] public string DoiTuongTitle { get; set; }
        [SolrField()] public string LNotiData { get; set; }
        [SolrField()] public string LNotiNguoiGui { get; set; }
        [SolrField()] public string TitleBaiViet { get; set; }
        [SolrField()] public string Url { get; set; }
        [SolrField()] public string LNotiChuaXem_Id { get; set; }
        [SolrField()] public string LogDoiTuong { get; set; }
        [SolrField()] public string LogFunction { get; set; }
        [SolrField()] public string LogNoiDung { get; set; }
        [SolrField()] public string LogThaoTac { get; set; }
        [SolrField()] public int? LogTrangThai { get; set; }
        [SolrField()] public int? LogIDDoiTuong { get; set; }
        [SolrField()] public int? ReadCount { get; set; }
        [SolrField()] public string SourceNews_Id { get; set; }
        [SolrField()] public string MenuLink { get; set; }
        [SolrField()] public bool? MenuNewWindow { get; set; }
        [SolrField()] public int? MenuIndex { get; set; }
        [SolrField()] public bool? MenuShow { get; set; }
        [SolrField()] public bool? CheckLogin { get; set; }
        [SolrField()] public string MenuIcon { get; set; }
        [SolrField()] public string Permissions { get; set; }
        [SolrField()] public string Permissions_Id { get; set; }
        [SolrField()] public string MenuParent { get; set; }
        [SolrField()] public string MenuParent_Id { get; set; }
        [SolrField()] public string UrlCMS { get; set; }
        [SolrField()] public string UrlPortal { get; set; }
        [SolrField()] public DateTime? NgayTao { get; set; }
        [SolrField()] public string UrlControl { get; set; }
        [SolrField()] public DateTime? LastModified { get; set; }
        [SolrField()] public string ConfigNgonNgu { get; set; }
        [SolrField()] public string WFTrangThai { get; set; }
        [SolrField()] public string UserPhuTrach { get; set; }
        [SolrField()] public string UserPhuTrach_Id { get; set; }
        [SolrField()] public string LogText { get; set; }
        [SolrField()] public DateTime? QCNgayDang { get; set; }
        [SolrField()] public string QCSoKyHieu { get; set; }
        [SolrField()] public string QCTrichYeu { get; set; }
        [SolrField()] public string QCToanVan { get; set; }
        [SolrField()] public DateTime? QCNgayVanBan { get; set; }
        [SolrField()] public int? QCHieuLuc { get; set; }
        [SolrField()] public string DMQuyChe { get; set; }
        [SolrField()] public string DMQuyChe_Id { get; set; }
        [SolrField()] public string NoiDung { get; set; }
        [SolrField()] public DateTime? NgayHieuLuc { get; set; }
        [SolrField()] public DateTime? NgayHetHieuLuc { get; set; }
        [SolrField()] public string TBLanhDaoKetLuan { get; set; }
        [SolrField()] public DateTime? TBNgayRaThongBao { get; set; }
        [SolrField()] public string TBNoiDung { get; set; }
        [SolrField()] public string TBSoKyHieu { get; set; }
        [SolrField()] public string TBTrichYeu { get; set; }
        [SolrField()] public string LstMaiLSend { get; set; }
        [SolrField()] public string EditUser { get; set; }
        [SolrField()] public string EditUser_Id { get; set; }
        [SolrField()] public int? ItemTinTuc { get; set; }
        [SolrField()] public string IDHoiDap { get; set; }
        [SolrField()] public string IDHoiDap_Id { get; set; }
        [SolrField()] public string UrlView { get; set; }
        [SolrField()] public string UrlQuanTri { get; set; }
        [SolrField()] public string VBCoQuanBanHanh { get; set; }
        [SolrField()] public DateTime? VBNgayBanHanh { get; set; }
        [SolrField()] public DateTime? VBNgayHieuLuc { get; set; }
        [SolrField()] public string VBNguoiKy { get; set; }
        [SolrField()] public string VBSoKyHieu { get; set; }
        [SolrField()] public string VBTrichYeu { get; set; }
        [SolrField()] public string DMLoaiTaiLieu { get; set; }
        [SolrField()] public string DMLoaiTaiLieu_Id { get; set; }
        [SolrField()] public string DMCoQuan { get; set; }
        [SolrField()] public string DMCoQuan_Id { get; set; }
        [SolrField()] public string DanhMuc { get; set; }
        [SolrField()] public string DanhMuc_Id { get; set; }
        [SolrField()] public string LinkVideo { get; set; }
        [SolrField()] public string ChuDe { get; set; }
        [SolrField()] public string ChuDe_Id { get; set; }

        public string urlChiTiet
        {
            get
            {
                string _urlChitiet = "";
                if (!string.IsNullOrEmpty(FullUrlList))
                {
                    string ListTitle = FullUrlList.Split('/').LastOrDefault();
                    switch (ListTitle)
                    {
                        case "QuyCheNoiBo":
                            _urlChitiet = "/Pages/regulation-detail.aspx?ItemID=" + SP_ID;
                            break;
                        case "VanBanTaiLieu":
                            _urlChitiet = "/Pages/vanbantailieu.aspx?ItemID=" + SP_ID;
                            break;
                        case "LichDonVi":
                            _urlChitiet = "/Pages/meeting.aspx?Lich=DonVi&ItemID=" + SP_ID;
                            break;
                        case "LichCaNhan":
                            _urlChitiet = "/Pages/meeting.aspx?l=LichCaNhan&ItemID=" + SP_ID;
                            break;
                        case "LichPhong":
                            _urlChitiet = "/Pages/meeting.aspx?Lich=Phong&ItemID=" + SP_ID;
                            break;
                        case "LichTapDoan":
                            _urlChitiet = "/Pages/meeting.aspx?Lich=TapDoan&ItemID=" + SP_ID;
                            break;
                        case "LichTongCongTy":
                            _urlChitiet = "/Pages/meeting.aspx?Lich=TongCty&ItemID=" + SP_ID;
                            break;
                        case "LichHop":
                            _urlChitiet = "/Pages/lichhop.aspx?ItemID=" + SP_ID;
                            break;
                        case "FAQThuongGap":
                            _urlChitiet = "/Pages/questions.aspx?ItemID=" + SP_ID;
                            break;
                        case "ThongBao":
                            _urlChitiet = "/Pages/chitietthongbao.aspx?ItemID=" + SP_ID;
                            break;
                        case "ThongBaoKetLuan":
                            _urlChitiet = "/Pages/ketluangiaoban.aspx?ItemID=" + SP_ID;
                            break;
                        case "TinNoiBat":
                            _urlChitiet = "/Pages/hotnews.aspx?ItemID=" + SP_ID;
                            break;
                        case "BaiViet":
                            _urlChitiet = "/Pages/diendanchitiet.aspx?Itemid=" + SP_ID;
                            break;
                        case "MangXaHoi":
                            _urlChitiet = "/Pages/socialnetwork-details.aspx?Itemid=" + SP_ID;
                            break;
                        case "Video":
                            _urlChitiet = "/Pages/videolibrary.aspx?Itemid=" + SP_ID;
                            break;
                        case "DMHinhAnh":
                            _urlChitiet = "/Pages/image.aspx?Itemid=" + SP_ID;
                            break;
                        case "DanhBa":
                            _urlChitiet = "/Pages/phonebook.aspx?Itemid=" + SP_ID;
                            break;
                        case "KSBoChuDe":
                            _urlChitiet = "/Pages/voted.aspx?Itemid=" + SP_ID;
                            break;
                        case "LienKetWebsite":
                            _urlChitiet = "/Pages/link.aspx?Itemid=" + SP_ID;
                            break;
                        default:
                            _urlChitiet = $"/Pages/TinTuc.aspx?l={ListTitle}&Itemid={SP_ID}";
                            break;
                    }
                }
                return _urlChitiet;
            }
        }

        public DanhMucSolr()
        {
        }
        public DanhMucSolr(SPListItem oSPListItem, params string[] Fields)
            : base(oSPListItem, Fields)
        {

        }
    }


    public class DataSolr : EntitySolr
    {
        #region Field class
        [SolrField()] public string DescriptionNews { get; set; }
        //[SolrField()] public string AnhDaiDien { get; set; }
        [SolrField()] public DateTime? CreatedDate { get; set; }
        [SolrField()] public string ImageNews { get; set; }
        [SolrField()] public string TacGia { get; set; }
        [SolrField()] public bool? isHotNew { get; set; }
        [SolrField()] public string ContentNews_Id { get; set; }
        [SolrField()] public string DanhMucTin { get; set; }
        [SolrField()] public string NguoiTao { get; set; }
        [SolrField()] public string NguoiTao_Id { get; set; }
        [SolrField()] public string CreatedUser { get; set; }
        [SolrField()] public string CreatedUser_Id { get; set; }
        [SolrField()] public string LichDiaDiem { get; set; }
        [SolrField()] public string LichGhiChu { get; set; }
        [SolrField()] public string LichLanhDaoChuTri { get; set; }
        [SolrField()] public string LichNoiDung { get; set; }
        [SolrField()] public string LichThanhPhanThamGia { get; set; }
        //LichPhongBanThamGia
        [SolrField()] public string LichPhongBanThamGia { get; set; }
        [SolrField()] public string THANH_PHAN { get; set; }
        [SolrField()] public string CHU_TRI { get; set; }
        [SolrField()] public string CHUAN_BI { get; set; }
        [SolrField()] public DateTime? LichThoiGianBatDau { get; set; }
        [SolrField()] public DateTime? LichThoiGianKetThuc { get; set; }
        [SolrField()] public int? LichTrangThai { get; set; }


        [SolrField()] public string DMVietTat { get; set; }
        [SolrField()] public string ColumnImage_Id { get; set; }
        [SolrField()] public string UrlSite { get; set; }
        [SolrField()] public string MoTa { get; set; }
        [SolrField()] public bool? Public { get; set; }
        [SolrField()] public int? STT { get; set; }
        [SolrField()] public string DMDescription { get; set; }
        [SolrField()] public bool? DMHienThi { get; set; }
        [SolrField()] public string DMMoTa { get; set; }
        [SolrField()] public int? DMSTT { get; set; }
        [SolrField()] public string DBDiaChi { get; set; }
        [SolrField()] public string DBEmail { get; set; }
        [SolrField()] public string DBFax { get; set; }
        [SolrField()] public int DBPhanLoai { get; set; }
        [SolrField()] public string DBSDT { get; set; }
        [SolrField()] public string DBTreeID { get; set; }
        [SolrField()] public string DBWebsite { get; set; }
        [SolrField()] public string DBParentPB { get; set; }
        [SolrField()] public string DBParentPB_Id { get; set; }
        [SolrField()] public string DBNhom { get; set; }
        [SolrField()] public string isOffice { get; set; }
        [SolrField()] public bool? IsDonVi { get; set; }
        [SolrField()] public string DBChucVu { get; set; }
        [SolrField()] public string DBChucVu_Id { get; set; }

        [SolrField()] public string DMAnhListAnh { get; set; }
        [SolrField()] public DateTime? TBNgayDang { get; set; }
        [SolrField()] public int? DMSLAnh { get; set; }
        [SolrField()] public string FAQCauHoi { get; set; }
        [SolrField()] public DateTime? FAQNgayHoi { get; set; }
        [SolrField()] public DateTime? FAQNgayTraLoi { get; set; }
        [SolrField()] public string FAQNoiDungTraLoi { get; set; }
        [SolrField()] public int? FAQSTT { get; set; }
        [SolrField()] public string FQNguoiHoi { get; set; }
        [SolrField()] public string FQNguoiHoi_Id { get; set; }
        [SolrField()] public string FQNguoiTraLoi { get; set; }
        [SolrField()] public string FQNguoiTraLoi_Id { get; set; }
        [SolrField()] public int? FAQLuotXem { get; set; }
        [SolrField()] public string Album { get; set; }
        [SolrField()] public string Album_Id { get; set; }
        [SolrField()] public DateTime? NgayDang { get; set; }
        [SolrField()] public string NoiDungTraLoi { get; set; }
        [SolrField()] public string FAQLinhVuc { get; set; }
        [SolrField()] public string FAQLinhVuc_Id { get; set; }
        [SolrField()] public int? KSHinhThucTraLoi { get; set; }
        [SolrField()] public string KSBoChuDe { get; set; }
        [SolrField()] public string KSBoChuDe_Id { get; set; }
        [SolrField()] public int? LoaiTraLoi { get; set; }
        [SolrField()] public DateTime? KSDenNgay { get; set; }
        [SolrField()] public DateTime? KSTuNgay { get; set; }
        [SolrField()] public string UCThamGia { get; set; }
        [SolrField()] public string KSCauHoi { get; set; }
        [SolrField()] public string KSCauHoi_Id { get; set; }
        [SolrField()] public int? SoLuotBinhChon { get; set; }
        [SolrField()] public string KSCauTraLoi { get; set; }
        [SolrField()] public string KSCauTraLoi_Id { get; set; }
        [SolrField()] public string KSCauTraLoiNoiDung { get; set; }
        [SolrField()] public string NoiDungChuanBi { get; set; }
        [SolrField()] public string Link { get; set; }
        [SolrField()] public string LNotiDaXem { get; set; }
        [SolrField()] public int? LNotiIsSentFirebase { get; set; }
        [SolrField()] public int? LNotiIsSentMail { get; set; }
        [SolrField()] public string LNotiMoTa { get; set; }
        [SolrField()] public string LNotiNguoiNhan { get; set; }
        [SolrField()] public string LNotiNguoiNhan_Id { get; set; }
        [SolrField()] public string LNotiNoiDung { get; set; }
        [SolrField()] public DateTime? LNotiSentTime { get; set; }
        [SolrField()] public string DoiTuongTitle { get; set; }
        [SolrField()] public string LNotiData { get; set; }
        [SolrField()] public string LNotiNguoiGui { get; set; }
        [SolrField()] public string TitleBaiViet { get; set; }
        [SolrField()] public string Url { get; set; }
        [SolrField()] public string LNotiChuaXem_Id { get; set; }
        [SolrField()] public string LogDoiTuong { get; set; }
        [SolrField()] public string LogFunction { get; set; }
        [SolrField()] public string LogNoiDung { get; set; }
        [SolrField()] public string LogThaoTac { get; set; }
        [SolrField()] public int? LogTrangThai { get; set; }
        [SolrField()] public int? LogIDDoiTuong { get; set; }
        [SolrField()] public int? ReadCount { get; set; }
        [SolrField()] public string SourceNews_Id { get; set; }
        [SolrField()] public string MenuLink { get; set; }
        [SolrField()] public bool? MenuNewWindow { get; set; }
        [SolrField()] public int? MenuIndex { get; set; }
        [SolrField()] public bool? MenuShow { get; set; }
        [SolrField()] public bool? CheckLogin { get; set; }
        [SolrField()] public string MenuIcon { get; set; }
        [SolrField()] public string Permissions { get; set; }
        [SolrField()] public string Permissions_Id { get; set; }
        [SolrField()] public string MenuParent { get; set; }
        [SolrField()] public string MenuParent_Id { get; set; }
        [SolrField()] public string UrlCMS { get; set; }
        [SolrField()] public string UrlPortal { get; set; }
        [SolrField()] public DateTime? NgayTao { get; set; }
        [SolrField()] public string UrlControl { get; set; }
        [SolrField()] public DateTime? LastModified { get; set; }
        [SolrField()] public string ConfigNgonNgu { get; set; }
        [SolrField()] public string WFTrangThai { get; set; }
        [SolrField()] public string UserPhuTrach { get; set; }
        [SolrField()] public string UserPhuTrach_Id { get; set; }
        [SolrField()] public string LogText { get; set; }
        [SolrField()] public DateTime? QCNgayDang { get; set; }
        [SolrField()] public string QCSoKyHieu { get; set; }
        [SolrField()] public string QCTrichYeu { get; set; }
        [SolrField()] public string QCToanVan { get; set; }
        [SolrField()] public DateTime? QCNgayVanBan { get; set; }
        [SolrField()] public int? QCHieuLuc { get; set; }
        [SolrField()] public string DMQuyChe { get; set; }
        [SolrField()] public string DMQuyChe_Id { get; set; }
        [SolrField()] public string NoiDung { get; set; }
        [SolrField()] public DateTime? NgayHieuLuc { get; set; }
        [SolrField()] public DateTime? NgayHetHieuLuc { get; set; }
        [SolrField()] public string TBLanhDaoKetLuan { get; set; }
        [SolrField()] public DateTime? TBNgayRaThongBao { get; set; }
        [SolrField()] public string TBNoiDung { get; set; }
        [SolrField()] public string TBSoKyHieu { get; set; }
        [SolrField()] public string TBTrichYeu { get; set; }
        [SolrField()] public string LstMaiLSend { get; set; }
        [SolrField()] public string EditUser { get; set; }
        [SolrField()] public string EditUser_Id { get; set; }
        [SolrField()] public int? ItemTinTuc { get; set; }
        [SolrField()] public string IDHoiDap { get; set; }
        [SolrField()] public string IDHoiDap_Id { get; set; }
        [SolrField()] public string UrlView { get; set; }
        [SolrField()] public string UrlQuanTri { get; set; }
        [SolrField()] public string VBCoQuanBanHanh { get; set; }
        [SolrField()] public DateTime? VBNgayBanHanh { get; set; }
        [SolrField()] public DateTime? VBNgayHieuLuc { get; set; }
        [SolrField()] public string VBNguoiKy { get; set; }
        [SolrField()] public string VBSoKyHieu { get; set; }
        [SolrField()] public string VBTrichYeu { get; set; }
        [SolrField()] public string DMLoaiTaiLieu { get; set; }
        [SolrField()] public string DMLoaiTaiLieu_Id { get; set; }
        [SolrField()] public string DMCoQuan { get; set; }
        [SolrField()] public string DMCoQuan_Id { get; set; }
        [SolrField()] public string DanhMuc { get; set; }
        [SolrField()] public string DanhMuc_Id { get; set; }
        [SolrField()] public string LinkVideo { get; set; }
        //[SolrField()] public string ChuDe { get; set; }
        //[SolrField()] public string ChuDe_Id { get; set; }
        //[SolrField()] public string ThreadNoiDung { get; set; }
        //[SolrField()] public string BaiViet { get; set; }
        //[SolrField()] public string BaiViet_Id { get; set; }

        #endregion
        public string urlChiTiet
        {
            get
            {
                string _urlChitiet = "";
                if (!string.IsNullOrEmpty(FullUrlList))
                {
                    string ListTitle = FullUrlList.Split('/').LastOrDefault();
                    switch (ListTitle)
                    {
                        case "QuyCheNoiBo":
                            _urlChitiet = "/Pages/regulation-detail.aspx?ItemID=" + SP_ID;
                            break;
                        case "VanBanTaiLieu":
                            _urlChitiet = "/Pages/vanbantailieu.aspx?ItemID=" + SP_ID;
                            break;
                        case "LichDonVi":
                            _urlChitiet = "/Pages/meeting.aspx?Lich=DonVi&ItemID=" + SP_ID;
                            break;
                        case "LichCaNhan":
                            _urlChitiet = "/Pages/meeting.aspx?l=LichCaNhan&ItemID=" + SP_ID;
                            break;
                        case "LichPhong":
                            _urlChitiet = "/Pages/meeting.aspx?Lich=Phong&ItemID=" + SP_ID;
                            break;
                        case "LichTapDoan":
                            _urlChitiet = "/Pages/meeting.aspx?Lich=TapDoan&ItemID=" + SP_ID;
                            break;
                        case "LichTongCongTy":
                            _urlChitiet = "/Pages/meeting.aspx?Lich=TongCty&ItemID=" + SP_ID;
                            break;
                        case "LichHop":
                            _urlChitiet = "/Pages/lichhop.aspx?ItemID=" + SP_ID;
                            break;
                        case "FAQThuongGap":
                            _urlChitiet = "/Pages/questions.aspx?ItemID=" + SP_ID;
                            break;
                        case "ThongBao":
                            _urlChitiet = "/Pages/chitietthongbao.aspx?ItemID=" + SP_ID;
                            break;
                        case "ThongBaoKetLuan":
                            _urlChitiet = "/Pages/ketluangiaoban.aspx?ItemID=" + SP_ID;
                            break;
                        case "TinNoiBat":
                            _urlChitiet = "/Pages/hotnews.aspx?ItemID=" + SP_ID;
                            break;
                        case "BaiViet":
                            _urlChitiet = "/Pages/diendanchitiet.aspx?Itemid=" + SP_ID;
                            break;
                        case "MangXaHoi":
                            _urlChitiet = "/Pages/socialnetwork-details.aspx?Itemid=" + SP_ID;
                            break;
                        case "Video":
                            _urlChitiet = "/Pages/videolibrary.aspx?Itemid=" + SP_ID;
                            break;
                        case "DMHinhAnh":
                            _urlChitiet = "/Pages/image.aspx?Itemid=" + SP_ID;
                            break;
                        case "DanhBa":
                            _urlChitiet = "/Pages/phonebook.aspx?Itemid=" + SP_ID;
                            break;
                        case "KSBoChuDe":
                            _urlChitiet = "/Pages/voted.aspx?Itemid=" + SP_ID;
                            break;
                        case "LienKetWebsite":
                            _urlChitiet = "/Pages/link.aspx?Itemid=" + SP_ID;
                            break;
                        default:
                            _urlChitiet = $"/Pages/TinTuc.aspx?l={ListTitle}&Itemid={SP_ID}";
                            break;
                    }
                }
                return _urlChitiet;
            }
        }

        public DataSolr()
        {
        }
        public DataSolr(SPListItem oSPListItem, params string[] Fields)
            : base(oSPListItem, Fields)
        {

        }

    }
    public class LogHeThongSolr : EntitySolr
    {
        #region Field class
        [SolrField()] public string DescriptionNews { get; set; }
        [SolrField()] public DateTime? CreatedDate { get; set; }
        [SolrField()] public string ImageNews { get; set; }
        [SolrField()] public string TacGia { get; set; }
        [SolrField()] public bool? isHotNew { get; set; }
        [SolrField()] public string ContentNews_Id { get; set; }
        [SolrField()] public string DanhMucTin { get; set; }
        [SolrField()] public string NguoiTao { get; set; }
        [SolrField()] public string NguoiTao_Id { get; set; }
        [SolrField()] public string CreatedUser { get; set; }
        [SolrField()] public string CreatedUser_Id { get; set; }
        [SolrField()] public string LichDiaDiem { get; set; }
        [SolrField()] public string LichGhiChu { get; set; }
        [SolrField()] public string LichLanhDaoChuTri { get; set; }
        [SolrField()] public string LichNoiDung { get; set; }
        [SolrField()] public string LichThanhPhanThamGia { get; set; }
        [SolrField()] public DateTime? LichThoiGianBatDau { get; set; }
        [SolrField()] public DateTime? LichThoiGianKetThuc { get; set; }
        [SolrField()] public int? LichTrangThai { get; set; }
        [SolrField()] public string DMVietTat { get; set; }
        [SolrField()] public string ColumnImage_Id { get; set; }
        [SolrField()] public string UrlSite { get; set; }
        [SolrField()] public string MoTa { get; set; }
        [SolrField()] public bool? Public { get; set; }
        [SolrField()] public int? STT { get; set; }
        [SolrField()] public string DMDescription { get; set; }
        [SolrField()] public bool? DMHienThi { get; set; }
        [SolrField()] public string DMMoTa { get; set; }
        [SolrField()] public int? DMSTT { get; set; }
        [SolrField()] public string DBDiaChi { get; set; }
        [SolrField()] public string DBEmail { get; set; }
        [SolrField()] public string DBFax { get; set; }
        [SolrField()] public int DBPhanLoai { get; set; }
        [SolrField()] public string DBSDT { get; set; }
        [SolrField()] public string DBTreeID { get; set; }
        [SolrField()] public string DBWebsite { get; set; }
        [SolrField()] public string DBParentPB { get; set; }
        [SolrField()] public string DBParentPB_Id { get; set; }
        [SolrField()] public string DBNhom { get; set; }
        [SolrField()] public string isOffice { get; set; }
        [SolrField()] public bool? IsDonVi { get; set; }
        [SolrField()] public string DBChucVu { get; set; }
        [SolrField()] public string DBChucVu_Id { get; set; }

        [SolrField()] public string DMAnhListAnh { get; set; }
        [SolrField()] public DateTime? TBNgayDang { get; set; }
        [SolrField()] public int? DMSLAnh { get; set; }
        [SolrField()] public string FAQCauHoi { get; set; }
        [SolrField()] public DateTime? FAQNgayHoi { get; set; }
        [SolrField()] public DateTime? FAQNgayTraLoi { get; set; }
        [SolrField()] public string FAQNoiDungTraLoi { get; set; }
        [SolrField()] public int? FAQSTT { get; set; }
        [SolrField()] public string FQNguoiHoi { get; set; }
        [SolrField()] public string FQNguoiHoi_Id { get; set; }
        [SolrField()] public string FQNguoiTraLoi { get; set; }
        [SolrField()] public string FQNguoiTraLoi_Id { get; set; }
        [SolrField()] public int? FAQLuotXem { get; set; }
        [SolrField()] public string Album { get; set; }
        [SolrField()] public string Album_Id { get; set; }
        [SolrField()] public DateTime? NgayDang { get; set; }
        [SolrField()] public string NoiDungTraLoi { get; set; }
        [SolrField()] public string FAQLinhVuc { get; set; }
        [SolrField()] public string FAQLinhVuc_Id { get; set; }
        [SolrField()] public int? KSHinhThucTraLoi { get; set; }
        [SolrField()] public string KSBoChuDe { get; set; }
        [SolrField()] public string KSBoChuDe_Id { get; set; }
        [SolrField()] public int? LoaiTraLoi { get; set; }
        [SolrField()] public DateTime? KSDenNgay { get; set; }
        [SolrField()] public DateTime? KSTuNgay { get; set; }
        [SolrField()] public string UCThamGia { get; set; }
        [SolrField()] public string KSCauHoi { get; set; }
        [SolrField()] public string KSCauHoi_Id { get; set; }
        [SolrField()] public int? SoLuotBinhChon { get; set; }
        [SolrField()] public string KSCauTraLoi { get; set; }
        [SolrField()] public string KSCauTraLoi_Id { get; set; }
        [SolrField()] public string KSCauTraLoiNoiDung { get; set; }
        [SolrField()] public string NoiDungChuanBi { get; set; }
        [SolrField()] public string Link { get; set; }
        [SolrField()] public string LNotiDaXem { get; set; }
        [SolrField()] public int? LNotiIsSentFirebase { get; set; }
        [SolrField()] public int? LNotiIsSentMail { get; set; }
        [SolrField()] public string LNotiMoTa { get; set; }
        [SolrField()] public string LNotiNguoiNhan { get; set; }
        [SolrField()] public string LNotiNguoiNhan_Id { get; set; }
        [SolrField()] public string LNotiNoiDung { get; set; }
        [SolrField()] public DateTime? LNotiSentTime { get; set; }
        [SolrField()] public string DoiTuongTitle { get; set; }
        [SolrField()] public string LNotiData { get; set; }
        [SolrField()] public string LNotiNguoiGui { get; set; }
        [SolrField()] public string TitleBaiViet { get; set; }
        [SolrField()] public string Url { get; set; }
        [SolrField()] public string LNotiChuaXem_Id { get; set; }
        [SolrField()] public string LogDoiTuong { get; set; }
        [SolrField()] public string LogFunction { get; set; }
        [SolrField()] public string LogNoiDung { get; set; }
        [SolrField()] public string LogThaoTac { get; set; }
        [SolrField()] public int? LogTrangThai { get; set; }
        [SolrField()] public int? LogIDDoiTuong { get; set; }
        [SolrField()] public int? ReadCount { get; set; }
        [SolrField()] public string SourceNews_Id { get; set; }
        [SolrField()] public string MenuLink { get; set; }
        [SolrField()] public bool? MenuNewWindow { get; set; }
        [SolrField()] public int? MenuIndex { get; set; }
        [SolrField()] public bool? MenuShow { get; set; }
        [SolrField()] public bool? CheckLogin { get; set; }
        [SolrField()] public string MenuIcon { get; set; }
        [SolrField()] public string Permissions { get; set; }
        [SolrField()] public string Permissions_Id { get; set; }
        [SolrField()] public string MenuParent { get; set; }
        [SolrField()] public string MenuParent_Id { get; set; }
        [SolrField()] public string UrlCMS { get; set; }
        [SolrField()] public string UrlPortal { get; set; }
        [SolrField()] public DateTime? NgayTao { get; set; }
        [SolrField()] public string UrlControl { get; set; }
        [SolrField()] public DateTime? LastModified { get; set; }
        [SolrField()] public string ConfigNgonNgu { get; set; }
        [SolrField()] public string WFTrangThai { get; set; }
        [SolrField()] public string UserPhuTrach { get; set; }
        [SolrField()] public string UserPhuTrach_Id { get; set; }
        [SolrField()] public string LogText { get; set; }
        [SolrField()] public DateTime? QCNgayDang { get; set; }
        [SolrField()] public string QCSoKyHieu { get; set; }
        [SolrField()] public string QCTrichYeu { get; set; }
        [SolrField()] public string QCToanVan { get; set; }
        [SolrField()] public DateTime? QCNgayVanBan { get; set; }
        [SolrField()] public int? QCHieuLuc { get; set; }
        [SolrField()] public string DMQuyChe { get; set; }
        [SolrField()] public string DMQuyChe_Id { get; set; }
        [SolrField()] public string NoiDung { get; set; }
        [SolrField()] public DateTime? NgayHieuLuc { get; set; }
        [SolrField()] public DateTime? NgayHetHieuLuc { get; set; }
        [SolrField()] public string TBLanhDaoKetLuan { get; set; }
        [SolrField()] public DateTime? TBNgayRaThongBao { get; set; }
        [SolrField()] public string TBNoiDung { get; set; }
        [SolrField()] public string TBSoKyHieu { get; set; }
        [SolrField()] public string TBTrichYeu { get; set; }
        [SolrField()] public string LstMaiLSend { get; set; }
        [SolrField()] public string EditUser { get; set; }
        [SolrField()] public string EditUser_Id { get; set; }
        [SolrField()] public int? ItemTinTuc { get; set; }
        [SolrField()] public string IDHoiDap { get; set; }
        [SolrField()] public string IDHoiDap_Id { get; set; }
        [SolrField()] public string UrlView { get; set; }
        [SolrField()] public string UrlQuanTri { get; set; }
        [SolrField()] public string VBCoQuanBanHanh { get; set; }
        [SolrField()] public DateTime? VBNgayBanHanh { get; set; }
        [SolrField()] public DateTime? VBNgayHieuLuc { get; set; }
        [SolrField()] public string VBNguoiKy { get; set; }
        [SolrField()] public string VBSoKyHieu { get; set; }
        [SolrField()] public string VBTrichYeu { get; set; }
        [SolrField()] public string DMLoaiTaiLieu { get; set; }
        [SolrField()] public string DMLoaiTaiLieu_Id { get; set; }
        [SolrField()] public string DMCoQuan { get; set; }
        [SolrField()] public string DMCoQuan_Id { get; set; }
        [SolrField()] public string DanhMuc { get; set; }
        [SolrField()] public string DanhMuc_Id { get; set; }
        [SolrField()] public string LinkVideo { get; set; }

        #endregion
        public string urlChiTiet
        {
            get
            {
                string _urlChitiet = "";
                if (!string.IsNullOrEmpty(FullUrlList))
                {
                    string ListTitle = FullUrlList.Split('/').LastOrDefault();
                    switch (ListTitle)
                    {
                        case "QuyCheNoiBo":
                            _urlChitiet = "/Pages/regulation-detail.aspx?ItemID=" + SP_ID;
                            break;
                        case "VanBanTaiLieu":
                            _urlChitiet = "/Pages/vanbantailieu.aspx?ItemID=" + SP_ID;
                            break;
                        case "LichDonVi":
                            _urlChitiet = "/Pages/meeting.aspx?Lich=DonVi&ItemID=" + SP_ID;
                            break;
                        case "LichCaNhan":
                            _urlChitiet = "/Pages/meeting.aspx?l=LichCaNhan&ItemID=" + SP_ID;
                            break;
                        case "LichPhong":
                            _urlChitiet = "/Pages/meeting.aspx?Lich=Phong&ItemID=" + SP_ID;
                            break;
                        case "LichTapDoan":
                            _urlChitiet = "/Pages/meeting.aspx?Lich=TapDoan&ItemID=" + SP_ID;
                            break;
                        case "LichTongCongTy":
                            _urlChitiet = "/Pages/meeting.aspx?Lich=TongCty&ItemID=" + SP_ID;
                            break;
                        case "LichHop":
                            _urlChitiet = "/Pages/lichhop.aspx?ItemID=" + SP_ID;
                            break;
                        case "FAQThuongGap":
                            _urlChitiet = "/Pages/questions.aspx?ItemID=" + SP_ID;
                            break;
                        case "ThongBao":
                            _urlChitiet = "/Pages/chitietthongbao.aspx?ItemID=" + SP_ID;
                            break;
                        case "ThongBaoKetLuan":
                            _urlChitiet = "/Pages/ketluangiaoban.aspx?ItemID=" + SP_ID;
                            break;
                        case "TinNoiBat":
                            _urlChitiet = "/Pages/hotnews.aspx?ItemID=" + SP_ID;
                            break;
                        case "BaiViet":
                            _urlChitiet = "/Pages/diendanchitiet.aspx?Itemid=" + SP_ID;
                            break;
                        case "MangXaHoi":
                            _urlChitiet = "/Pages/socialnetwork-details.aspx?Itemid=" + SP_ID;
                            break;
                        case "Video":
                            _urlChitiet = "/Pages/videolibrary.aspx?Itemid=" + SP_ID;
                            break;
                        case "DMHinhAnh":
                            _urlChitiet = "/Pages/image.aspx?Itemid=" + SP_ID;
                            break;
                        case "DanhBa":
                            _urlChitiet = "/Pages/phonebook.aspx?Itemid=" + SP_ID;
                            break;
                        case "KSBoChuDe":
                            _urlChitiet = "/Pages/voted.aspx?Itemid=" + SP_ID;
                            break;
                        case "LienKetWebsite":
                            _urlChitiet = "/Pages/link.aspx?Itemid=" + SP_ID;
                            break;
                        default:
                            _urlChitiet = $"/Pages/TinTuc.aspx?l={ListTitle}&Itemid={SP_ID}";
                            break;
                    }
                }
                return _urlChitiet;
            }
        }

        public LogHeThongSolr()
        {
        }
        public LogHeThongSolr(SPListItem oSPListItem, params string[] Fields)
            : base(oSPListItem, Fields)
        {

        }

    }


}
