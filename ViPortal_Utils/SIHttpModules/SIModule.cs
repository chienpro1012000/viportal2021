﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;

namespace ViPortal_Utils.SIHttpModules
{
    public class SIModule : IHttpModule
    {
        //IHttpModule

        public void Dispose()
        {
            //clean-up code here.  
        }

        public void Init(HttpApplication context)
        {

            // custom logging implementation, hooking event in init Method 
            //context.BeginRequest += new EventHandler(this.Application_BeginRequest);
            context.BeginRequest += new EventHandler(this.Application_BeginRequest);
        }

        private void Application_BeginRequest(object sender, EventArgs e)
        {
            HttpContext currentContext = HttpContext.Current;
            string strCurrentPage = currentContext.Request.RawUrl.ToUpper();

            //If CurrentPage is Signout, remove the ClientToken from Cookies
            if (strCurrentPage.Contains("CLOSECONNECTION"))
            {
                RemoveAuthenticationCookie(currentContext);
            }
        }
        void application_EndRequest(object sender, EventArgs e)
        {
            HttpContext currentContext = HttpContext.Current;
            string strCurrentPage = currentContext.Request.RawUrl.ToUpper();

            //If CurrentPage is Signout, remove the ClientToken from Cookies
            if (strCurrentPage.Contains("CLOSECONNECTION"))
            {
                RemoveAuthenticationCookie(currentContext);
            }
        }

        private void RemoveAuthenticationCookie(HttpContext CurrentContext)
        {

            FormsAuthentication.SignOut();
            //Session.Abandon();


            //FormsAuthentication.RedirectToLoginPage();
            //FormsAuthentication.SignOut();

            //CurrentContext.Response.Redirect("/Login/Pages/login.aspx");
            //FormsAuthentication.RedirectToLoginPage();

            //FormsAuthentication.Redirect;
            //FormsAuthentication.SignOut();
            //HttpContext.Current.Session.Abandon();

            // clear authentication cookie
            //HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            //cookie1.Expires = DateTime.Now.AddYears(-1);
            //HttpContext.Current.Response.Cookies.Add(cookie1);

            //// clear session cookie (not necessary for your current problem but i would recommend you do it anyway)
            //SessionStateSection sessionStateSection = (SessionStateSection)WebConfigurationManager.GetSection("system.web/sessionState");
            //HttpCookie cookie2 = new HttpCookie(sessionStateSection.CookieName, "");
            //cookie2.Expires = DateTime.Now.AddYears(-1);
            //HttpContext.Current.Response.Cookies.Add(cookie2);
            HttpContext.Current.Response.Cookies[FormsAuthentication.FormsCookieName].Expires = DateTime.Now.AddDays(-1);
            //FormsAuthentication.RedirectToLoginPage();
        }
    }
}
