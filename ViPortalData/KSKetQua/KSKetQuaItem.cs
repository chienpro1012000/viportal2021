using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace ViPortalData.KSKetQua
{
	public class KSKetQuaItem : SPEntity
	{
		public string KSCauTraLoiNoiDung { set; get; }
		public SPFieldLookupValue KSBoChuDe { set; get; }
		public SPFieldLookupValue KSCauHoi { set; get; }
		public SPFieldLookupValue KSCauTraLoi { set; get; }
		public SPFieldLookupValue FQNguoiTraLoi { set; get; }

		public KSKetQuaItem()
		{
			KSBoChuDe = new SPFieldLookupValue();
			KSCauHoi = new SPFieldLookupValue();
			KSCauTraLoi = new SPFieldLookupValue();
			KSBoChuDe = new SPFieldLookupValue();
			FQNguoiTraLoi = new SPFieldLookupValue();

		}
	}
	public class KSKetQuaJson : EntityJson
	{
		public string KSCauTraLoiNoiDung { set; get; }
		public LookupData KSBoChuDe { set; get; }
		public LookupData KSCauHoi { set; get; }
		public LookupData KSCauTraLoi { set; get; }
		public LookupData FQNguoiTraLoi { set; get; }
		public KSKetQuaJson()
		{
			KSBoChuDe = new LookupData();
			KSCauHoi = new LookupData();
			KSCauTraLoi = new LookupData();
			FQNguoiTraLoi = new LookupData();

		}
	}
}