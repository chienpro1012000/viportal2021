using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortalData.KSKetQua
{
    public class KSKetQuaDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/noidung/Lists/KSKetQua";
        public KSKetQuaDA()
        {
            Init(_urlList);
        }
        public KSKetQuaDA(string _urlSiteProcess)
            : base(_urlSiteProcess + "/noidung/Lists/KSKetQua")
        {
            _urlList = _urlSiteProcess;
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<KSKetQuaJson> GetListJson(KSKetQuaQuery searhOption, params string[] Fields)
        {
            List<KSKetQuaJson> lstReturn = new List<KSKetQuaJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "KSCauHoi", "KSCauTraLoi", "KSCauTraLoiNoiDung", "FQNguoiTraLoi", "KSBoChuDe", "_ModerationStatus" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<KSKetQuaJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(KSKetQuaQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}