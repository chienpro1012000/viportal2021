using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace ViPortalData
{
    public class HinhAnhItem : SPEntity
    {
        public SPFieldLookupValue Album { set; get; }
        public int STT { set; get; }
        public string ImageNews { set; get; }
        public string MoTa { set; get; }
        public DateTime? NgayDang { set; get; }
        public HinhAnhItem()
        {
            Album = new SPFieldLookupValue();

        }
    }
    public class HinhAnhJson : EntityJson
    {
        public LookupData Album { set; get; }
        public int STT { set; get; }
        public string ImageNews { set; get; }
        public string MoTa { set; get; }
        public DateTime? NgayDang { set; get; }
        public HinhAnhJson()
        {
            Album = new LookupData();

        }
    }
}
