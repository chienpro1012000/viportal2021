﻿using Microsoft.SharePoint;
using SolrNet;
using SolrNet.Commands.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortalData
{
    public class HinhAnhDA : SPBaseDA<DataSolr>
    {
        private string _urlList = "/noidung/Lists/HinhAnh";
        public HinhAnhDA(bool isAdmin = false)
        {
            if (isAdmin)
                InitAdmin(_urlList);
            else
                Init(_urlList);
        }
        public HinhAnhDA(string _urlSite, bool isAdmin = false)
        {
            _urlList = _urlSite + _urlList;
            if (isAdmin)
                InitAdmin(_urlList);
            else
                Init(_urlList);
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<HinhAnhJson> GetListJson(HinhAnhQuery searhOption, params string[] Fields)
        {
            List<HinhAnhJson> lstReturn = new List<HinhAnhJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "Album", "MoTa", "ImageNews", "Created", "_ModerationStatus" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<HinhAnhJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(HinhAnhQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if (searhOption.HasAlbum == 1)
            {
                oBuildQuery.Append("<And>");
            }
            if (searhOption.Album > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");

            if (searhOption._ModerationStatus == 1)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption._ModerationStatus == 1)
            {
                oBuildQuery.Append(GetStringModer(0)); //appro là 0
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetBylistID)
            {
                if (searhOption.lstIdGet.Count > 0)
                {
                    oBuildQuery.Append(getStringByListId(searhOption.lstIdGet));

                }
                else
                {
                    oBuildQuery.Append(getstringNull());
                }
                oBuildQuery.Append("</And>");
            }
            if (searhOption.Album > 0)
            {
                oBuildQuery.Append(GetStringEQLookUp("Album", searhOption.Album));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.HasAlbum == 1)
            {
                oBuildQuery.Append(getstringNotNull("ID"));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }


        public List<ISolrQuery> BuildQuerySolr(HinhAnhQuery searhOption)
        {
            var Querys = new List<ISolrQuery>();
            Querys.Add(new SolrQuery("*:*"));
            if (searhOption.UrlList != null && searhOption.UrlList.Count > 0)
            {
                //UrlList:("/noidung/Lists/VanBanTaiLieuSuKien" "/noidung/Lists/VanBanTaiLieu")
                Querys.Add(new SolrQuery(string.Format("FullUrlList:({0})", string.Join(" ", searhOption.UrlList.Select(x => "\"" + x + "\"")))));
            }
            else if (SpListProcess != null)
            {
                Querys.Add(new SolrQuery(string.Format("FullUrlList:\"{0}\"", SpListProcess.ParentWeb.ServerRelativeUrl + "/" + SpListProcess.RootFolder.Url)));
            }
            Querys.AddSearchBase(searhOption);
            if (searhOption.Album > 0)
                Querys.Add(new SolrQuery(string.Format("Album_Id:\",{0},\"", searhOption.Album)));
            if (searhOption.HasAlbum == 1)
            {
                Querys.Add(new SolrQuery(string.Format("*:*")));
            }
            BuildQueryToString(Querys);
            return Querys;
        }
        public List<HinhAnhJson> GetListJsonSolr(HinhAnhQuery searhOption, params string[] Fields)
        {
            List<HinhAnhJson> lstReturn = new List<HinhAnhJson>();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "SP_ID", "Title", "Album", "MoTa", "ImageNews", "Created", "_ModerationStatus" };
            }
            var Querys = BuildQuerySolr(searhOption);
            #region Option khởi tạo
            var oorders = new List<SortOrder>();

            if (oorders.FindIndex(x => x.FieldName == searhOption.FieldOrder) == -1)
                oorders.Add(new SortOrder(searhOption.FieldOrder == "ID" ? "id" : searhOption.FieldOrder, (searhOption.Ascending) ? Order.ASC : Order.DESC));
            QueryOptions options = new QueryOptions()
            {
                OrderBy = oorders,
                Fields = Fields,
                //StartOrCursor =  new StartOrCursor.Start((currentPage - 1) * rowPerPage)
            };
            if (searhOption.Length > 0)
            {
                options.Rows = searhOption.Length;
                options.Start = searhOption.Start; //Barn ghi dau tien
            }

            #endregion
            var result = solrWorkerDM.Query(new SolrMultipleCriteriaQuery(Querys, "AND"), options);
            TongSoBanGhiSauKhiQuery = Convert.ToInt32(result.NumFound);
            foreach (DataSolr itemsolr in result)
            {
                lstReturn.Add(ConvertSolrToJson<HinhAnhJson>(itemsolr, Fields));
            }
            return lstReturn;
        }
    }
}