﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using ViPortal_Utils.Base;

namespace ViPortalData
{
    public class HinhAnhQuery : QuerySearch
    {
        public int Album { get; set; }
        public int isDanhSach { get; set; }
        public int HasAlbum { get; set; }
        public List<string> lstIdNews { get; set; }
        public List<int> lstIdGet { get; set; }
        public HinhAnhQuery()
        {
            lstIdGet = new List<int>();
            isDanhSach = 1;
        }
        public HinhAnhQuery(HttpRequest request)
            : base(request)
        {
            lstIdGet = new List<int>();
            isDanhSach = 1;
            HasAlbum = 1; //fix mặc định sẽ là có.
            DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                if (!string.IsNullOrEmpty(request[pinfo.Name]))
                {
                    var values = request[pinfo.Name];
                    switch (pinfo.PropertyType.FullName)
                    {
                        case "System.Int32":
                            if (!string.IsNullOrEmpty(values))
                            {
                                pinfo.SetValue(this, System.Convert.ToInt32(values), null);
                            }
                            break;
                        case "System.Collections.Generic.List`1[[System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]":
                            if (!string.IsNullOrEmpty(values))
                            {
                                List<int> lstValuesInt = GetDanhSachIDsQuaFormPost(values);
                                pinfo.SetValue(this, lstValuesInt, null);
                            }
                            else
                                pinfo.SetValue(this, new List<int>(), null);
                            break;
                        case "System.Nullable`1[[System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]":
                        case "System.DateTime ?":
                        case "System.DateTime":
                            if (!string.IsNullOrEmpty(values))
                            {
                                dtfi.ShortDatePattern = "MM/dd/yyyy";
                                dtfi.DateSeparator = "/";
                                DateTime dtTemp = System.Convert.ToDateTime(values, dtfi);
                                if (dtTemp.Year < 1900 && dtTemp.Year > 2099)
                                    dtTemp = new DateTime(2012, 2, 30);
                                pinfo.SetValue(this, ((DateTime)dtTemp), null);
                            }
                            else
                            {
                                object dateNullValue = null;
                                pinfo.SetValue(this, dateNullValue, null);
                            }
                            break;

                        default:
                        case "System.String":
                            if (!string.IsNullOrEmpty(values))
                            {
                                pinfo.SetValue(this, (values).Trim(), null);
                            }
                            else
                            {
                                pinfo.SetValue(this, "", null);
                            }
                            break;
                    }
                }
            }
        }
        /// <summary>
        /// Lấy về danh sách ID
        /// fomat string 1,2,3,4,
        /// </summary>
        /// <param name="arrID"></param>
        /// <returns></returns>
        public static List<int> GetDanhSachIDsQuaFormPost(string arrID)
        {
            List<int> dsID = new List<int>();
            if (!string.IsNullOrEmpty(arrID))
            {
                string[] tempIDs = arrID.Split(',');
                foreach (string idConvert in tempIDs)
                {
                    int _id = 0;
                    if (int.TryParse(idConvert, out _id))
                    {
                        //if (_id > 0)
                        if (!dsID.Contains(_id))
                            dsID.Add(_id);
                    }
                }
            }
            return dsID;
        }
    }
}