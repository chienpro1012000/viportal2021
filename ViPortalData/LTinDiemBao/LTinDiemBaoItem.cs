using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.JSGrid;
using ViPortal_Utils.Base;

namespace ViPortalData.LTinDiemBao
{
	public class LTinDiemBaoItem : SPEntity
	{
		public DateTime? CreatedDate { set; get; }
		public SPFieldLookupValue CreatedUser { set; get; }
		public string ContentNews { set; get; }
		public string ImageNews { set; get; }
		public int ReadCount { set; get; }
		public string DescriptionNews { set; get; }
		public string SourceNews { set; get; }
		public LTinDiemBaoItem()
		{
			CreatedUser = new SPFieldLookupValue();

		}
	}
	public class LTinDiemBaoJson : EntityJson
	{
		public DateTime? CreatedDate { set; get; }
		public LookupData CreatedUser { set; get; }
		public string ContentNews { set; get; }
		public string ImageNews { set; get; }
		public int ReadCount { set; get; }
		public string DescriptionNews { set; get; }
		public string SourceNews { set; get; }
		public LTinDiemBaoJson()
		{
			CreatedUser = new LookupData();
		}
	}
}