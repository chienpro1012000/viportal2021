using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortalData
{
    public class LGroupDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/Lists/LGroup";
        public LGroupDA(bool isAdmin = false)
        {
            if (isAdmin) InitAdmin(_urlList);
            else
            Init(_urlList);
        }
        public LGroupDA(string _urlListProcess)
            : base(_urlListProcess)
        {
            _urlList = _urlListProcess;
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<LGroupJson> GetListJson(LGroupQuery searhOption, params string[] Fields)
        {
            List<LGroupJson> lstReturn = new List<LGroupJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "GroupTreeID", "GroupIsDonVi", "GroupParent", "OldID", "UrlLogo", "orgCode" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<LGroupJson>(SPcoll, Fields);
            return lstReturn;
        }

        public LGroupItem GetDonByPhongBan(int idPhongBan)
        {
            LGroupItem oLGroupItem = GetByIdToObjectSelectFields<LGroupItem>(idPhongBan, "GroupParent", "GroupIsDonVi");
            if (!oLGroupItem.GroupIsDonVi)
            {
                if (oLGroupItem.GroupParent.LookupId > 0)
                {
                    int idparent = oLGroupItem.GroupParent.LookupId;
                    while (idparent > 0)
                    {
                        oLGroupItem = GetByIdToObjectSelectFields<LGroupItem>(idparent, "GroupParent", "GroupIsDonVi");
                        if (oLGroupItem.GroupIsDonVi)
                            return oLGroupItem;
                        else
                        {
                            idparent = oLGroupItem.GroupParent.LookupId;
                        }
                    }
                }
            }
            return oLGroupItem;
        }


        public string BuildQuery(LGroupQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if(searhOption.KieuDonVi > 0)
                oBuildQuery.Append("<And>");
            if (!string.IsNullOrEmpty(searhOption.UrlSite))
                oBuildQuery.Append("<And>");
            if (searhOption.GroupIsDonVi > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetByParent)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            if(searhOption.DefaultGetLgroup == 1)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption.DefaultGetLgroup == 1)
            {
                oBuildQuery.Append(getstringNumNotEQ("KieuDonVi", 9));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetBylistID)
            {
                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetByParent)
            {

                if (searhOption.GroupParent > 0)
                {
                    oBuildQuery.Append(GetStringEQLookUp("GroupParent", searhOption.GroupParent));
                }
                else oBuildQuery.Append(getstringNull("ID"));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.GroupIsDonVi > 0)
            {
                if (searhOption.GroupIsDonVi == 1)
                    oBuildQuery.Append(GetStringBoolean("GroupIsDonVi", 1));
                else oBuildQuery.Append(GetStringBoolean("GroupIsDonVi", 2));
                oBuildQuery.Append("</And>");
            }
            if (!string.IsNullOrEmpty(searhOption.UrlSite))
            {
                oBuildQuery.Append(GetStringEQText("UrlSite", searhOption.UrlSite));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.KieuDonVi > 0)
            {
                oBuildQuery.Append(getstringNumEQ("KieuDonVi", searhOption.KieuDonVi));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}