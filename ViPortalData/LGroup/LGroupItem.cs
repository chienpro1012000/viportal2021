﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace ViPortalData
{
    public class LGroupItem : SPEntity
    {
        public string MaDinhDanh { set; get; }
        public SPFieldLookupValue GroupParent { set; get; }
        public bool GroupIsDonVi { set; get; }
        public string GroupEmail { set; get; }
        public string UrlSite { set; get; }
        public string GroupSDT { set; get; }
        public string DMSTT { set; get; }
        public string GroupTreeID { set; get; }
        /// <summary>
        /// 1 là công ty sản suất
        /// 2 là ban tham mưu
        /// </summary>
        public int KieuDonVi { get; set; }
        public string OldID { get; set; }
        public string orgCode { get; set; }
        public string UrlLogo { get; set; }
        public LGroupItem()
        {
            GroupParent = new SPFieldLookupValue();

        }
    }

    public class LGroupJson : EntityJson
    {
        public string MaDinhDanh { set; get; }
        public LookupData GroupParent { set; get; }
        public bool GroupIsDonVi { set; get; }
        public string GroupEmail { set; get; }
        public string UrlSite { set; get; }
        public string DMSTT { set; get; }
        public string UrlLogo { get; set; }
        public string OldID { get; set; }
        public string orgCode { get; set; }
        public string GroupSDT { set; get; }
        public string GroupTreeID { set; get; }

        public LGroupJson()
        {
            GroupParent = new LookupData();

        }
    }
}
