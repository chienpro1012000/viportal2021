﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using ViPortal_Utils.Base;

namespace ViPortalData
{
    public class LGroupQuery : QuerySearch
    {
        public bool isGetByParent { get; set; }
        public bool isGetParent { get; set; }
        public int GroupParent { get; set; }
        /// <summary>
        /// default sẽ ko lấy các bản ghi có kiểu bàng 9 là các đơn vị muốn ẩn đi.
        /// </summary>
        public int DefaultGetLgroup { get; set; }
        public int KieuDonVi { get; set; }
        public string UrlSite { get; set; }
        /// <summary>
        /// 0 laf get all
        /// 1. laf don vi
        /// 2. la phong ban
        /// </summary>
        public int GroupIsDonVi { get; set; }
        public LGroupQuery()
        {
        }
        public LGroupQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
}