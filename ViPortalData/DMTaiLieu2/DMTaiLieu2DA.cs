using Microsoft.SharePoint;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ViPortal_Utils;
using ViPortal_Utils.Base;
namespace VIPortalData.DMTaiLieu2
{
    public class DMTaiLieu2DA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/noidung/Lists/DMTaiLieu2";
        public DMTaiLieu2DA()
        {
            Init(_urlList);
        }
        public DMTaiLieu2DA(string _urlListProcess)
            : base(_urlListProcess)
        {
            _urlList = _urlListProcess;
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<DMTaiLieu2Json> GetListJson(DMTaiLieu2Query searhOption, params string[] Fields)
        {
            List<DMTaiLieu2Json> lstReturn = new List<DMTaiLieu2Json>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0) 
            {
                Fields = new string[] { "ID", "Title","DMNgayDang","DMMoTa","DMVietTat", "_ModerationStatus" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<DMTaiLieu2Json>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(DMTaiLieu2Query  searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}
