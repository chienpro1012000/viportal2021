using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalData.DMTaiLieu2
{
   public class DMTaiLieu2Item:SPEntity
    {
     public int DMSTT {set;get;}
	public string DMMoTa {set;get;}
	public DateTime? DMNgayDang {set;get;}
	public string DMAnh {set;get;}
	public string DMVietTat {set;get;}
	
	public DMTaiLieu2Item(){
		
		}
    }
	public class DMTaiLieu2Json:EntityJson
    {
		public int DMSTT { set; get; }
		public string DMMoTa { set; get; }
		public DateTime? DMNgayDang { set; get; }
		public string DMAnh { set; get; }
		public string DMVietTat { set; get; }

		public DMTaiLieu2Json()
		{

		}
	}
}

