﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViPortal_Utils.Base;

namespace ViPortalData.DMLinhVuc
{
	public class DMLinhVucItem : SPEntity
	{
		public string DMMoTa { set; get; }
		public int DMSTT { set; get; }
		public string DMVietTat { set; get; }
		public DMLinhVucItem()
		{

		}
	}
}
public class DMLinhVucJson : EntityJson
{
	public string DMMoTa { set; get; }
	public int DMSTT { set; get; }
	public string DMVietTat { set; get; }
	public DMLinhVucJson()
	{
	}
}

