﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ViPortal_Utils.Base;

namespace ViPortalData.CoQuanBanHanh
{
    public class CoQuanBanHanhQuery: QuerySearch
    {
        public CoQuanBanHanhQuery()
        {
        }

        public CoQuanBanHanhQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
}
