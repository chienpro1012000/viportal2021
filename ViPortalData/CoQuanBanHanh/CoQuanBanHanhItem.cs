﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViPortal_Utils.Base;

namespace ViPortalData.CoQuanBanHanh
{
	public class CoQuanBanHanhItem : SPEntity
	{
		public string DMDescription { set; get; }
		public bool DMHienThi { set; get; }
		public string DMMoTa { set; get; }
		public int DMSTT { set; get; }
		public string DMVietTat { set; get; }
		public CoQuanBanHanhItem()
		{

		}
	}
	public class CoQuanBanHanhJson : EntityJson
	{
		public string DMDescription { set; get; }
		public bool DMHienThi { set; get; }
		public string DMMoTa { set; get; }
		public int DMSTT { set; get; }
		public string DMVietTat { set; get; }
		public CoQuanBanHanhJson()
		{
		}
	}
}
