﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortalData.CoQuanBanHanh
{
    public class CoQuanBanHanhDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/noidung/Lists/CoQuanBanHanh";
        public CoQuanBanHanhDA()
        {
            Init(_urlList);
        }
        public CoQuanBanHanhDA(string _urlSiteProcess)
            : base(_urlSiteProcess + "/noidung/Lists/CoQuanBanHanh")
        {
            _urlList = _urlSiteProcess;
        }
        /// <summary>
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>

        public List<CoQuanBanHanhJson> GetListJson(CoQuanBanHanhQuery searhOption, params string[] Fields)
        {
            List<CoQuanBanHanhJson> lstReturn = new List<CoQuanBanHanhJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "DMDescription", "DMHienThi", "DMMoTa", "DMSTT", "DMVietTat", "_ModerationStatus" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<CoQuanBanHanhJson>(SPcoll, Fields);
            return lstReturn;
        }

        public int GetCount(CoQuanBanHanhQuery searhOption)
        {
            SPQuery oQuery = new SPQuery();
            oQuery.ViewFields = BuildViewField("ID");
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = SpListProcess.GetItems(oQuery);
            return SPcoll.Count;
        }

        public string BuildQuery(CoQuanBanHanhQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            if (searhOption.ItemIDNotGet > 0)
                oBuildQuery.Append("<And>");
            if (searhOption._ModerationStatus == 1)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption._ModerationStatus == 1)
            {
                oBuildQuery.Append(GetStringModer(0)); //appro là 0
                oBuildQuery.Append("</And>");
            }
            if (searhOption.ItemIDNotGet > 0)
            {
                oBuildQuery.Append(getstringNumNotEQ("ID", searhOption.ItemIDNotGet));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetBylistID)
            {
                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}
