using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using ViPortal_Utils.Base;

namespace ViPortalData.VanBanTaiLieu
{
    public class VanBanTaiLieuQuery : QuerySearch
    {
        public int DMLoaiTaiLieu { get; set; }
        public string VBSoKyHieu { get; set; }
        public DateTime? TuNgay_VBNgayBanHanh { get; set; }
        public DateTime? DenNgay_VBNgayBanHanh { get; set; }
        public VanBanTaiLieuQuery()
        {
        }
        public VanBanTaiLieuQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
}