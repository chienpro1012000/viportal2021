﻿using Microsoft.SharePoint;
using SolrNet;
using SolrNet.Commands.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortalData.VanBanTaiLieu
{
    public class VanBanTaiLieuDA : SPBaseDA<DataSolr>
    {
        private string _urlList = "/noidung/Lists/VanBanTaiLieu";
        public VanBanTaiLieuDA(bool isAdmin = false)
        {
            if (isAdmin)
                InitAdmin(_urlList, true);
            else
                Init(_urlList, true);
        }
        public VanBanTaiLieuDA(string _urlSiteProcess, bool isAdmin = false)
        {
            _urlList = _urlSiteProcess + _urlList;
            if(isAdmin)
                InitAdmin(_urlList, true);
            else
            Init(_urlList, true);
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<VanBanTaiLieuJson> GetListJson(VanBanTaiLieuQuery searhOption, params string[] Fields)
        {
            List<VanBanTaiLieuJson> lstReturn = new List<VanBanTaiLieuJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "VBCoQuanBanHanh", "VBNgayBanHanh", "VBNgayHieuLuc", "VBNguoiKy", "VBSoKyHieu", "VBTrichYeu", "DMLoaiTaiLieu", "DMCoQuan", "_ModerationStatus", "WFTrangThai", "UserPhuTrach", "Attachments" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<VanBanTaiLieuJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(VanBanTaiLieuQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if (searhOption.DMLoaiTaiLieu > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.TuNgay_VBNgayBanHanh.HasValue)
                oBuildQuery.Append("<And>");
            if (searhOption.DenNgay_VBNgayBanHanh.HasValue)
                oBuildQuery.Append("<And>");
            if (!string.IsNullOrEmpty(searhOption.VBSoKyHieu))
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            if (searhOption._ModerationStatus == 1)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption._ModerationStatus == 1)
            {
                oBuildQuery.Append(GetStringModer(0)); //appro là 0
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            if (!string.IsNullOrEmpty(searhOption.VBSoKyHieu))
            {
                oBuildQuery.Append(GetStringEQText("VBSoKyHieu", searhOption.VBSoKyHieu));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.DenNgay_VBNgayBanHanh.HasValue)
            {
                oBuildQuery.Append(GetStringCamlDate("VBNgayBanHanh", searhOption.DenNgay_VBNgayBanHanh.Value, "Leq"));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.TuNgay_VBNgayBanHanh.HasValue)
            {
                oBuildQuery.Append(GetStringCamlDate("VBNgayBanHanh", searhOption.TuNgay_VBNgayBanHanh.Value, "Geq"));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.DMLoaiTaiLieu > 0)
            {
                oBuildQuery.Append(GetStringIncludeLookUp("DMLoaiTaiLieu", searhOption.DMLoaiTaiLieu));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }


        public int GetCount(VanBanTaiLieuQuery searhOption)
        {
            SPQuery oQuery = new SPQuery();
            oQuery.ViewFields = BuildViewField("ID");
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = SpListProcess.GetItems(oQuery);
            return SPcoll.Count;
        }




        public List<ISolrQuery> BuildQuerySolr(VanBanTaiLieuQuery searhOption)
        {
            var Querys = new List<ISolrQuery>();
            Querys.Add(new SolrQuery("*:*"));
            if (searhOption.UrlList != null && searhOption.UrlList.Count > 0)
            {
                //UrlList:("/noidung/Lists/VanBanTaiLieuSuKien" "/noidung/Lists/VanBanTaiLieu")
                Querys.Add(new SolrQuery(string.Format("FullUrlList:({0})", string.Join(" ", searhOption.UrlList.Select(x => "\"" + x + "\"")))));
            }
            else if (SpListProcess != null)
            {
                Querys.Add(new SolrQuery(string.Format("FullUrlList:\"{0}\"", SpListProcess.ParentWeb.ServerRelativeUrl + "/" + SpListProcess.RootFolder.Url)));
            }
            Querys.AddSearchBase(searhOption);
            if (searhOption.DMLoaiTaiLieu > 0)
                Querys.Add(new SolrQuery(string.Format("DMLoaiTaiLieu_Id:\",{0},\"", searhOption.DMLoaiTaiLieu)));
            if (!string.IsNullOrEmpty(searhOption.VBSoKyHieu))
            {
                Querys.Add(new SolrQuery(string.Format("VBSoKyHieu:*{0}*", searhOption.VBSoKyHieu)));
            }
            if (searhOption.DenNgay_VBNgayBanHanh.HasValue)
            {
                Querys.Add(new SolrQuery(GetQueryDateTimeSolrLeq("VBNgayBanHanh", searhOption.DenNgay_VBNgayBanHanh.Value)));
            }
            if (searhOption.TuNgay_VBNgayBanHanh.HasValue)
            {
                Querys.Add(new SolrQuery(GetQueryDateTimeSolrGeq("VBNgayBanHanh", searhOption.TuNgay_VBNgayBanHanh.Value)));
            }
            BuildQueryToString(Querys);
            return Querys;
        }
        public List<VanBanTaiLieuJson> GetListJsonSolr(VanBanTaiLieuQuery searhOption, params string[] Fields)
        {
            List<VanBanTaiLieuJson> lstReturn = new List<VanBanTaiLieuJson>();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "SP_ID", "Title", "VBCoQuanBanHanh", "VBNgayBanHanh", "VBNgayHieuLuc", "VBNguoiKy", "VBSoKyHieu", "VBTrichYeu", "DMLoaiTaiLieu", "DMCoQuan", "_ModerationStatus", "WFTrangThai", "UserPhuTrach", "Attachments", "FullUrlList", "id" }; //Set default
            }
            var Querys = BuildQuerySolr(searhOption);
            #region Option khởi tạo
            var oorders = new List<SortOrder>();

            if (oorders.FindIndex(x => x.FieldName == searhOption.FieldOrder) == -1)
                oorders.Add(new SortOrder(searhOption.FieldOrder == "ID" ? "id" : searhOption.FieldOrder, (searhOption.Ascending) ? Order.ASC : Order.DESC));
            QueryOptions options = new QueryOptions()
            {
                OrderBy = oorders,
                Fields = Fields,
                //StartOrCursor =  new StartOrCursor.Start((currentPage - 1) * rowPerPage)
            };
            if (searhOption.Length > 0)
            {
                options.Rows = searhOption.Length;
                options.Start = searhOption.Start; //Barn ghi dau tien
            }

            #endregion
            var result = solrWorkerDM.Query(new SolrMultipleCriteriaQuery(Querys, "AND"), options);
            TongSoBanGhiSauKhiQuery = Convert.ToInt32(result.NumFound);
            foreach (DataSolr itemsolr in result)
            {
                lstReturn.Add(ConvertSolrToJson<VanBanTaiLieuJson>(itemsolr, Fields));
            }
            return lstReturn;
        }
    }
}