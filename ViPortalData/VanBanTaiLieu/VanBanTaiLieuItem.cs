using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;

namespace ViPortalData.VanBanTaiLieu
{
	public class VanBanTaiLieuItem : SPEntity
	{
		public string VBCoQuanBanHanh { set; get; }
		public DateTime? VBNgayBanHanh { set; get; }
		public DateTime? VBNgayHieuLuc { set; get; }
		public string VBNguoiKy { set; get; }
		public string VBSoKyHieu { set; get; }
		public string VBTrichYeu { set; get; }
		public SPFieldLookupValue DMLoaiTaiLieu { set; get; }
		public SPFieldLookupValue DMCoQuan { set; get; }
		public string LichGhiChu { get; set; }
		public VanBanTaiLieuItem()
		{
			DMLoaiTaiLieu = new SPFieldLookupValue();
			DMCoQuan = new SPFieldLookupValue();
		}
	}
	public class VanBanTaiLieuJson : EntityJson
	{
		public string VBCoQuanBanHanh { set; get; }
		public DateTime? VBNgayBanHanh { set; get; }
		public DateTime? VBNgayHieuLuc { set; get; }
		public string VBNguoiKy { set; get; }
		public string VBSoKyHieu { set; get; }
		public string VBTrichYeu { set; get; }
		public LookupData DMLoaiTaiLieu { set; get; }
		public string LichGhiChu { get; set; }
		public LookupData DMCoQuan { set; get; }
		public VanBanTaiLieuJson()
		{
			DMLoaiTaiLieu = new LookupData();
			DMCoQuan = new LookupData();
		}
	}
}