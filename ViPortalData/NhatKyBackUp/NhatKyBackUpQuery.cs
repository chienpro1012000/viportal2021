﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public class NhatKyBackUpQuery : QuerySearch
    {
        /// <summary>
        /// query ra những cái đang chờ backup
        /// </summary>
        public bool ChoBackUp { get; set; }
        public NhatKyBackUpQuery()
        {
        }
        public NhatKyBackUpQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
}