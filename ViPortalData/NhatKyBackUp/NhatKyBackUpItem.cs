using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{
    public class NhatKyBackUpItem : SPEntity
    {
        public string RequestedBy { set; get; }
        public DateTime? StartTime { set; get; }
        public DateTime? FinishTime { set; get; }
        public string Directory { set; get; }
        public string BackupID { set; get; }
        public int WarningCount { set; get; }
        public int ErrorCount { set; get; }
        public string Method { set; get; }
        public string GhiChu { get; set; }
        public string TypeBackUpRestore { set; get; }

        public NhatKyBackUpItem()
        {

        }
    }
    public class NhatKyBackUpJson : EntityJson
    {
        public string RequestedBy { set; get; }
        public DateTime? StartTime { set; get; }
        public DateTime? FinishTime { set; get; }
        public string Directory { set; get; }
        public string BackupID { set; get; }
        public string GhiChu { get; set; }
        public int WarningCount { set; get; }
        public int ErrorCount { set; get; }
        public string Method { set; get; }
        public string TypeBackUpRestore { set; get; }

        public NhatKyBackUpJson()
        {

        }
    }
}
