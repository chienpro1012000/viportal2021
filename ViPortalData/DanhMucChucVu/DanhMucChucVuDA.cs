﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViPortal_Utils;
using ViPortal_Utils.Base;


namespace ViPortalData.DanhMucChucVu
{
    public  class DanhMucChucVuDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/noidung/Lists/DMChucVu";
        public DanhMucChucVuDA(bool isAdmin = false)
        {
            if (isAdmin)
                InitAdmin(_urlList);
            else
                Init(_urlList);
        }
        public DanhMucChucVuDA(string _urlSiteProcess)
            : base(_urlSiteProcess + "/noidung/Lists/DMChucVu")
        {
            _urlList = _urlSiteProcess;
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param> 
        /// <returns></returns>

        public List<DanhMucChucVuJson> GetListJson(DanhMucChucVuQuery searhOption, params string[] Fields)
        {
            List<DanhMucChucVuJson> lstReturn = new List<DanhMucChucVuJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "DMDescription", "DMHienThi", "DMMoTa", "DMSTT", "DMVietTat", "_ModerationStatus" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<DanhMucChucVuJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(DanhMucChucVuQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}
