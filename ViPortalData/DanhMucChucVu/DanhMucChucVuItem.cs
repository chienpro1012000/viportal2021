﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;
using Microsoft.SharePoint.JSGrid;
using ViPortal_Utils.Base;

namespace ViPortalData.DanhMucChucVu
{
    public class DanhMucChucVuItem : SPEntity
    {
		public string DMDescription { set; get; }
		public bool DMHienThi { set; get; }
		public string DMMoTa { set; get; }
		public int DMSTT { set; get; }
		public string DMVietTat { set; get; }
		public DanhMucChucVuItem()
		{

		}
	}
	public class DanhMucChucVuJson : EntityJson
	{
		public string DMDescription { set; get; }
		public bool DMHienThi { set; get; }
		public string DMMoTa { set; get; }
		public int DMSTT { set; get; }
		public string DMVietTat { set; get; }
		public DanhMucChucVuJson()
		{
		}
	}
}
