using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace ViPortalData
{
    public class DanhMucChiaSeTLItem : SPEntity
    {
        public string DMDescription { set; get; }
        public bool DMHienThi { set; get; }
        public int DMSTT { set; get; }
        public string DMVietTat { set; get; }
        public SPFieldLookupValue fldGroup { get; set; }
        public DanhMucChiaSeTLItem()
        {

        }
    }

    public class DanhMucChiaSeTLJson : EntityJson
    {
        public string DMDescription { set; get; }
        public bool DMHienThi { set; get; }
        public int DMSTT { set; get; }
        public string DMVietTat { set; get; }

        public DanhMucChiaSeTLJson()
        {

        }
    }
}
