using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortalData
{
    public class DanhMucChiaSeTLDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/noidung/Lists/DanhMucChiaSeTL";
        public DanhMucChiaSeTLDA()
        {
            Init(_urlList);
        }
        public DanhMucChiaSeTLDA(string _urlListProcess)
            : base(_urlListProcess + "/noidung/Lists/DanhMucChiaSeTL")
        {
            _urlList = _urlListProcess;
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<DanhMucChiaSeTLJson> GetListJson(DanhMucChiaSeTLQuery searhOption, params string[] Fields)
        {
            List<DanhMucChiaSeTLJson> lstReturn = new List<DanhMucChiaSeTLJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0) 
            {
                Fields = new string[] { "ID", "Title", "DMDescription", "DMVietTat", "DMSTT", "_ModerationStatus", "fldGroup" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<DanhMucChiaSeTLJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(DanhMucChiaSeTLQuery  searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if(searhOption.fldGroup > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            if (searhOption.fldGroup > 0)
            {
                oBuildQuery.Append(GetStringEQLookUp("fldGroup", searhOption.fldGroup));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}