using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{
    public class MangXaHoiItem : SPEntity
    {
        public string ThreadNoiDung { set; get; }
        public string AttachmentText { set; get; }
        public SPFieldLookupValue CreatedUser { set; get; }
        public SPFieldLookupValue fldGroup { set; get; }
        public SPFieldLookupValue DanhMuc { set; get; }
        public int SoLuotXem { set; get; }
        public int SoLuotComment { set; get; }
        public int LuotLike { set; get; }
        public string AnhDaiDien { set; get; }
        public string CurrentUsertLike { set; get; }

        public MangXaHoiItem()
        {
            CreatedUser = new SPFieldLookupValue();
            fldGroup = new SPFieldLookupValue();
            DanhMuc = new SPFieldLookupValue();

        }
    }
    public class MangXaHoiJson : EntityJson
    {
        public string ThreadNoiDung { set; get; }
        public string AttachmentText { set; get; }
        public LookupData CreatedUser { set; get; }
        public LookupData fldGroup { set; get; }
        public LookupData DanhMuc { set; get; }
        public int SoLuotXem { set; get; }
        public int SoLuotComment { set; get; }
        public int LuotLike { set; get; }
        public string AnhDaiDien { set; get; }
        public string CurrentUsertLike { set; get; }

        public MangXaHoiJson()
        {
            CreatedUser = new LookupData();
            fldGroup = new LookupData();
            DanhMuc = new LookupData();
        }
    }
}
