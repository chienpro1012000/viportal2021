using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;

namespace ViPortalData.HoiDap
{
    public class HoiDapItem : SPEntity
    {
        public string FAQCauHoi { set; get; }
        public DateTime? FAQNgayHoi { set; get; }
        public int FAQSTT { set; get; }
        public SPFieldLookupValue FQNguoiHoi { set; get; }
        public SPFieldLookupValue FAQLinhVuc { set; get; }
        public int FAQLuotXem { set; get; }
        public string NoiDungTraLoi { set; get; }
        public HoiDapItem()
        {
            FQNguoiHoi = new SPFieldLookupValue();
            FAQLinhVuc = new SPFieldLookupValue();
            FAQNgayHoi = DateTime.Now;
        }
    }
    public class HoiDapJson : EntityJson
    {
        public string FAQCauHoi { set; get; }
        public DateTime? FAQNgayHoi { set; get; }
        public int FAQSTT { set; get; }
        public LookupData FQNguoiHoi { set; get; }
        public LookupData FQNguoiTraLoi { set; get; }
        public LookupData FAQLinhVuc { set; get; }
        public int FAQLuotXem { set; get; }
        public string NoiDungTraLoi { set; get; }
        public string TieuDeTraLoi { set; get; }
        public HoiDapJson()
        {
            FQNguoiHoi = new LookupData();
            FQNguoiTraLoi = new LookupData();
            FAQLinhVuc = new LookupData();
        }
    }
}