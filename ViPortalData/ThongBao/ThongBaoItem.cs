using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{
    public class ThongBaoItem : SPEntity
    {
        public string Titles { set; get; }
        public string NoiDung { set; get; }
        public string MoTa { set; get; }
        public string ImageNews { set; get; }
        public SPFieldLookupValue CreatedUser { set; get; }
        public DateTime? NgayHieuLuc { set; get; }
        public DateTime? NgayHetHieuLuc { set; get; }

        public ThongBaoItem()
        {
            CreatedUser = new SPFieldLookupValue();
        }
    }
    public class ThongBaoJson : EntityJson
    {
        public string Titles { set; get; }
        public string NoiDung { set; get; }
        public string MoTa { set; get; }
        public string ImageNews { set; get; }
        public LookupData CreatedUser { set; get; }
        public DateTime? NgayHieuLuc { set; get; }
        public DateTime? NgayHetHieuLuc { set; get; }

        public ThongBaoJson()
        {
            CreatedUser = new LookupData();
        }
    }
}
