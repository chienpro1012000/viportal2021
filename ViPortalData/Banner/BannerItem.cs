using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{
    public class BannerItem : SPEntity
    {
        public string ImageNews { set; get; }
        public string ColumnImageSrc {
            get
            {
                if (!string.IsNullOrEmpty(ColumnImage))
                {
                    string matchString = Regex.Match(ColumnImage, "<img.+?src=[\"'](.+?)[\"'].*?>", RegexOptions.IgnoreCase).Groups[1].Value;
                    return matchString;
                }
                else return "";
            }
        }
        public string ColumnImage { get; set; }
        public string DMVietTat { set; get; }

        public BannerItem()
        {

        }
    }

    public class BannerJson : EntityJson
    {
        public string ImageNews { set; get; }
        public string ColumnImage { set; get; }
        public string ColumnImageSrc
        {
            get
            {
                if (!string.IsNullOrEmpty(ColumnImage))
                {
                    string matchString = Regex.Match(ColumnImage, "<img.+?src=[\"'](.+?)[\"'].*?>", RegexOptions.IgnoreCase).Groups[1].Value;
                    return matchString;
                }
                else return "";
            }
        }
        public string DMVietTat { set; get; }

        public BannerJson()
        {

        }
    }
}
