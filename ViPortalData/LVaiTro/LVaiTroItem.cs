using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.JSGrid;
using ViPortal_Utils.Base;

namespace ViPortalData.LVaiTro
{
	public class LVaiTroItem : SPEntity
	{
		public SPFieldLookupValue fldGroup { set; get; }
		public SPFieldLookupValueCollection Permission { set; get; }
		public string DMMoTa { set; get; }
		public LVaiTroItem()
		{
			fldGroup = new SPFieldLookupValue();
			Permission = new SPFieldLookupValueCollection();
		}
	}
	public class LVaiTroJson : EntityJson
	{
	
		public LookupData fldGroup { set; get; }
		public List<LookupData> Permission { set; get; }
		public string DMMoTa { set; get; }
		
		public LVaiTroJson()
		{
			fldGroup = new LookupData();
		}
	}
}