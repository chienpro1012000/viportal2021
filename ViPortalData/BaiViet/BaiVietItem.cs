using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{

    public class BaiVietItem : SPEntity
    {
        public string ThreadNoiDung { set; get; }
        public string AttachmentText { set; get; }
        public string LienKetChuDe { set; get; }
        public int SoLuotXem { get; set; }
        public int SoLuotComment { get; set; }
        public string AnhDaiDien { get; set; }
        public SPFieldLookupValue CreatedUser { set; get; }
        public SPFieldLookupValue ChuDe { get; set; }
        public BaiVietItem()
        {
            CreatedUser = new SPFieldLookupValue();
            ChuDe = new SPFieldLookupValue();
        }
    }

    public class BaiVietJson : EntityJson
    {
        public string ThreadNoiDung { set; get; }
        public string LienKetChuDe { set; get; }
        public string AttachmentText { set; get; }
        public string AnhDaiDien { get; set; }
        public LookupData CreatedUser { set; get; }
        public int SoLuotXem { get; set; }
        public int SoLuotComment { get; set; }
        public LookupData ChuDe { get; set; }

        public BaiVietJson()
        {
            CreatedUser = new LookupData();
            AnhDaiDien = string.Empty;
            ChuDe = new LookupData();
        }
    }
}
