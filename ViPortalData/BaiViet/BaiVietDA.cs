﻿using Microsoft.SharePoint;
using SolrNet;
using SolrNet.Commands.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public class BaiVietDA : SPBaseDA<DataSolr>
    {
        private string _urlList = "/diendan/Lists/BaiViet";
        public BaiVietDA(bool isAdmin = false)
        {
            if (isAdmin)
                Init(_urlList, true);
            else InitAdmin(_urlList, true);
        }
        public BaiVietDA(string _urlListProcess, bool isAdmin = false)
            //: base(_urlListProcess, true)
        {
            _urlList = _urlListProcess + _urlList;
            if (isAdmin)
                Init(_urlList, true);
            else InitAdmin(_urlList, true);
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<BaiVietJson> GetListJson(BaiVietQuery searhOption, params string[] Fields)
        {
            List<BaiVietJson> lstReturn = new List<BaiVietJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "SoLuotXem", "Created", "CreatedUser", "SoLuotComment", "ThreadNoiDung", "AnhDaiDien", "ChuDe", "_ModerationStatus", "SoLuotXem" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<BaiVietJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(BaiVietQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if (searhOption.DenNgay.HasValue)
                oBuildQuery.Append("<And>");
            if (searhOption.TuNgay.HasValue)
                oBuildQuery.Append("<And>");
            if (searhOption.ChuDe > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            if (searhOption._ModerationStatus == 1)
                oBuildQuery.Append("<And>");
            if (searhOption.NotGetID > 0)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption.NotGetID > 0)
            {
                oBuildQuery.Append(getstringNotEQText("ID", searhOption.NotGetID.ToString()));
                oBuildQuery.Append("</And>");
            }if (searhOption._ModerationStatus == 1)
            {
                oBuildQuery.Append(GetStringModer(0)); //appro là 0
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetBylistID)
            {
                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.ChuDe > 0)
            {
                oBuildQuery.Append(GetStringEQLookUp("ChuDe", searhOption.ChuDe));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.TuNgay.HasValue)
            {
                oBuildQuery.Append(GetStringGeqDate("Created", searhOption.TuNgay.Value));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.DenNgay.HasValue)
            {
                oBuildQuery.Append(GetStringLeqDate("Created", searhOption.DenNgay.Value));
                oBuildQuery.Append("</And>");
            }
            
            oBuildQuery.Append("</Where>");
          
            return oBuildQuery.ToString();
        }
        public List<ISolrQuery> BuildQuerySolr(BaiVietQuery searhOption)
        {
            var Querys = new List<ISolrQuery>();
            Querys.Add(new SolrQuery("*:*"));
            if (searhOption.UrlList != null && searhOption.UrlList.Count > 0)
            {
                //UrlList:("/noidung/Lists/VanBanTaiLieuSuKien" "/noidung/Lists/VanBanTaiLieu")
                Querys.Add(new SolrQuery(string.Format("FullUrlList:({0})", string.Join(" ", searhOption.UrlList.Select(x => "\"" + x + "\"")))));
            }
            else if (SpListProcess != null)
            {
                Querys.Add(new SolrQuery(string.Format("FullUrlList:\"{0}\"", SpListProcess.ParentWeb.ServerRelativeUrl + "/" + SpListProcess.RootFolder.Url)));
            }
            Querys.AddSearchBase(searhOption);
            if (searhOption.ChuDe > 0)
                Querys.Add(new SolrQuery(string.Format("ChuDe_Id:\",{0},\"", searhOption.ChuDe)));
            
            if (searhOption.DenNgay.HasValue)
            {
                Querys.Add(new SolrQuery(GetQueryDateTimeSolrLeq("Created", searhOption.DenNgay.Value)));
            }
            if (searhOption.TuNgay.HasValue)
            {
                Querys.Add(new SolrQuery(GetQueryDateTimeSolrGeq("Created", searhOption.TuNgay.Value)));
            }
            BuildQueryToString(Querys);
            return Querys;
        }
        public List<BaiVietJson> GetListJsonSolr(BaiVietQuery searhOption, params string[] Fields)
        {
            List<BaiVietJson> lstReturn = new List<BaiVietJson>();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "SP_ID", "Title", "SoLuotXem", "Created", "CreatedUser", "SoLuotComment", "ThreadNoiDung", "AnhDaiDien", "ChuDe", "_ModerationStatus", "SoLuotXem", "FullUrlList", "id" }; //Set default
            }
            var Querys = BuildQuerySolr(searhOption);
            #region Option khởi tạo
            var oorders = new List<SortOrder>();

            if (oorders.FindIndex(x => x.FieldName == searhOption.FieldOrder) == -1)
                oorders.Add(new SortOrder(searhOption.FieldOrder == "ID" ? "id" : searhOption.FieldOrder, (searhOption.Ascending) ? Order.ASC : Order.DESC));
            QueryOptions options = new QueryOptions()
            {
                OrderBy = oorders,
                Fields = Fields,
                //StartOrCursor =  new StartOrCursor.Start((currentPage - 1) * rowPerPage)
            };
            if (searhOption.Length > 0)
            {
                options.Rows = searhOption.Length;
                options.Start = searhOption.Start; //Barn ghi dau tien
            }

            #endregion
            var result = solrWorkerDM.Query(new SolrMultipleCriteriaQuery(Querys, "AND"), options);
            TongSoBanGhiSauKhiQuery = Convert.ToInt32(result.NumFound);
            foreach (DataSolr itemsolr in result)
            {
                lstReturn.Add(ConvertSolrToJson<BaiVietJson>(itemsolr, Fields));
            }
            return lstReturn;
        }
    }
}