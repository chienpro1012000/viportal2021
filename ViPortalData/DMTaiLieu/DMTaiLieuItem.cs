using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.JSGrid;
using ViPortal_Utils.Base;

namespace ViPortalData.DMTaiLieu
{
	public class DMTaiLieuItem : SPEntity
	{
		public string DMDescription { set; get; }
		public bool DMHienThi { set; get; }
		public string DMMoTa { set; get; }
		public int DMSTT { set; get; }
		public string DMVietTat { set; get; }
		public DMTaiLieuItem()
		{
			DMHienThi = true;
		}
	}
	public class DMTaiLieuJson : EntityJson
	{
		public string DMDescription { set; get; }
		public bool DMHienThi { set; get; }
		public string DMMoTa { set; get; }
		public int DMSTT { set; get; }
		public string DMVietTat { set; get; }
		public DMTaiLieuJson()
		{
			DMHienThi = true;
		}
	}
}