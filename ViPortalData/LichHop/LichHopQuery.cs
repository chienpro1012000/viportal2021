﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public class LichHopQuery : QuerySearch
    {
        public DateTime? TuNgay_LichHop { get; set; }
        public DateTime? DenNgay_LichHop { get; set; }
        public DateTime? TuNgay_LichThoiGianBatDau { get; set; }
        public DateTime? DenNgay_LichThoiGianBatDau { get; set; }
        public int fldGroup { get; set; }
        public List<int> LstfldGroup { get; set; }
        public bool isGetLichPhongCuadonVi { get; set; }
        /// <summary>
        /// 999 lấy những cái đã duyệt
        /// </summary>
        public int LichTrangThai { get; set; }
        public int CreatedUser { get; set; }
        public DateTime? LichNgay { get; set; }
        public LichHopQuery()
        {
            LstfldGroup = new List<int>();
        }
        public LichHopQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
}