﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using SolrNet;
using SolrNet.Commands.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public class LichHopDA : SPBaseDA<DataSolr>
    {
        private string _urlList = "/noidung/Lists/LichHop";
        public LichHopDA(bool isAdmin = false)
        {
            if (isAdmin)
                InitAdmin(_urlList, true);
            else
                Init(_urlList, true);
        }
        public LichHopDA(string _urlListProcess)
            : base(_urlListProcess + "/noidung/Lists/LichHop", true)
        {
            _urlList = _urlListProcess;
        }
        public LichHopDA(string _urlListProcess, bool FullList)
            : base(_urlListProcess, true)
        {
            _urlList = _urlListProcess;
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<LichHopJson> GetListJson(LichHopQuery searhOption, params string[] Fields)
        {
            List<LichHopJson> lstReturn = new List<LichHopJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "fldGroup", "LichDiaDiem", "LichGhiChu", "LichLanhDaoChuTri", "LichNoiDung", 
                    "LichThanhPhanThamGia", "LichThoiGianBatDau", "LichThoiGianKetThuc", "LichTrangThai", "_ModerationStatus" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<LichHopJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(LichHopQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if (searhOption.DenNgay_LichThoiGianBatDau.HasValue)
                oBuildQuery.Append("<And>");
            if (searhOption.TuNgay_LichThoiGianBatDau.HasValue)
                oBuildQuery.Append("<And>");
            if (searhOption.TuNgay_LichHop.HasValue)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            if (searhOption._ModerationStatus == 1)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption._ModerationStatus == 1)
            {
                oBuildQuery.Append(GetStringModer(0)); //appro là 0
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            if (searhOption.TuNgay_LichHop.HasValue)
            {
                oBuildQuery.Append(GetStringEqDate("LichThoiGianBatDau", searhOption.TuNgay_LichHop.Value, false));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.TuNgay_LichThoiGianBatDau.HasValue)
            {
                oBuildQuery.Append(GetStringGeqDate("LichThoiGianBatDau", searhOption.TuNgay_LichThoiGianBatDau.Value, false));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.DenNgay_LichThoiGianBatDau.HasValue)
            {
                oBuildQuery.Append(GetStringLeqDate("LichThoiGianBatDau", searhOption.DenNgay_LichThoiGianBatDau.Value, false));
                oBuildQuery.Append("</And>");
            }
            
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
        public List<ISolrQuery> BuildQuerySolr(LichHopQuery searhOption)
        {
            var Querys = new List<ISolrQuery>();
            Querys.Add(new SolrQuery("*:*"));
            if (searhOption.UrlList != null && searhOption.UrlList.Count > 0)
            {
                //UrlList:("/noidung/Lists/VanBanTaiLieuSuKien" "/noidung/Lists/VanBanTaiLieu")
                Querys.Add(new SolrQuery(string.Format("FullUrlList:({0})", string.Join(" ", searhOption.UrlList.Select(x => "\"" + x + "\"")))));
            }
            else if (SpListProcess != null)
            {
                Querys.Add(new SolrQuery(string.Format("FullUrlList:\"{0}\"", SpListProcess.ParentWeb.ServerRelativeUrl + "/" + SpListProcess.RootFolder.Url)));
            }
            Querys.AddSearchBase(searhOption);
            if(searhOption.LichTrangThai == 999)
            {
                Querys.Add(new SolrQuery("LichTrangThai:0"));
            }
            if (searhOption.isGetLichPhongCuadonVi)
            {
                if(searhOption.LstfldGroup != null && searhOption.LstfldGroup.Count > 0)
                {
                    Querys.Add(new SolrQuery("fldGroup_Id:(" + string.Join(" ", searhOption.LstfldGroup) + ")"));
                    Querys.Add(new SolrQuery("!LogText:*_LichTongCongTy-* && !LogText:*_LichDonVi-*"));
                }
                else
                {
                    Querys.Add(new SolrQuery("SP_ID:0"));
                }
            }
            if(searhOption.LichNgay.HasValue)
            {
                Querys.Add(new SolrQuery($"(LichThoiGianBatDau:[{SPUtility.CreateISO8601DateTimeFromSystemDateTime(searhOption.LichNgay.Value.AddHours(-7))} TO {SPUtility.CreateISO8601DateTimeFromSystemDateTime(searhOption.LichNgay.Value.AddDays(1).AddHours(-7).AddMinutes(-1))}] OR (LichThoiGianBatDau:[* TO {SPUtility.CreateISO8601DateTimeFromSystemDateTime(searhOption.LichNgay.Value.AddDays(1).AddHours(-7).AddMinutes(-1))}] && LichThoiGianKetThuc:[{SPUtility.CreateISO8601DateTimeFromSystemDateTime(searhOption.LichNgay.Value)} TO *]))"));
            }
            if (searhOption.TuNgay_LichHop.HasValue)
            {
                Querys.Add(new SolrQuery(GetQueryDateTimeSolrEq("LichThoiGianBatDau", searhOption.TuNgay_LichHop.Value)));
            }
            if (searhOption.DenNgay_LichThoiGianBatDau.HasValue)
            {
                Querys.Add(new SolrQuery(GetQueryDateTimeSolrLeq("LichThoiGianBatDau", searhOption.DenNgay_LichThoiGianBatDau.Value)));
            }
            if (searhOption.TuNgay_LichThoiGianBatDau.HasValue)
            {
                Querys.Add(new SolrQuery(GetQueryDateTimeSolrGeq("LichThoiGianBatDau", searhOption.TuNgay_LichThoiGianBatDau.Value)));
            }
            if(searhOption.fldGroup > 0)
            {
                Querys.Add(new SolrQuery("fldGroup_Id:" + searhOption.fldGroup));
            }
            if(searhOption.LstfldGroup != null && searhOption.LstfldGroup.Count > 0)
            {
                Querys.Add(new SolrQuery("fldGroup_Id:(" + string.Join(" ", searhOption.LstfldGroup) + ")"));
            }
            if (searhOption.CreatedUser > 0)
            {
                Querys.Add(new SolrQuery($"CreatedUser_Id:\",{searhOption.CreatedUser},\""));
            }
            BuildQueryToString(Querys);
            return Querys;
        }
        public List<LichHopJson> GetListJsonSolr(LichHopQuery searhOption, params string[] Fields)
        {
            List<LichHopJson> lstReturn = new List<LichHopJson>();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "SP_ID", "Title", "fldGroup", "LichDiaDiem", "LichGhiChu", "LichLanhDaoChuTri", "LichNoiDung","LichThanhPhanThamGia", "LichThoiGianBatDau", "LichThoiGianKetThuc",
                    "LichTrangThai", "_ModerationStatus", "FullUrlList", "id", "THANH_PHAN", "CHU_TRI", "CHUAN_BI", "LogText", "LichPhongBanThamGia", "LogNoiDung", "TBNoiDung", "DBPhanLoai", "Attachments" }; //Set default
            }
            var Querys = BuildQuerySolr(searhOption);
            #region Option khởi tạo
            var oorders = new List<SortOrder>();

            if (oorders.FindIndex(x => x.FieldName == searhOption.FieldOrder) == -1)
                oorders.Add(new SortOrder(searhOption.FieldOrder == "ID" ? "SP_ID" : searhOption.FieldOrder, (searhOption.Ascending) ? Order.ASC : Order.DESC));
            QueryOptions options = new QueryOptions()
            {
                OrderBy = oorders,
                Fields = Fields,
                //StartOrCursor =  new StartOrCursor.Start((currentPage - 1) * rowPerPage)
            };
            if (searhOption.Length > 0)
            {
                options.Rows = searhOption.Length;
                options.Start = searhOption.Start; //Barn ghi dau tien
            }

            #endregion
            var result = solrWorkerDM.Query(new SolrMultipleCriteriaQuery(Querys, "AND"), options);
            TongSoBanGhiSauKhiQuery = Convert.ToInt32(result.NumFound);
            foreach (DataSolr itemsolr in result)
            {
                lstReturn.Add(ConvertSolrToJson<LichHopJson>(itemsolr, Fields));
            }
            return lstReturn;
        }
    }
}