﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
using ViPortalData.LichDonVi;

namespace VIPortalAPP
{
    public class LichHopItem : SPEntity
    {
        public SPFieldLookupValue fldGroup { set; get; }
        public SPFieldLookupValue CreatedUser { set; get; }
        public string LichDiaDiem { set; get; }
        public string LichGhiChu { set; get; }
        public SPFieldLookupValue LichLanhDaoChuTri { set; get; }
        public string LichNoiDung { set; get; }
        public SPFieldLookupValueCollection LichThanhPhanThamGia { set; get; }
        public DateTime? LichThoiGianBatDau { set; get; }
        public DateTime? LichThoiGianKetThuc { set; get; }
        public int LichTrangThai { set; get; }

        public LichHopItem()
        {
            fldGroup = new SPFieldLookupValue();
            CreatedUser = new SPFieldLookupValue();
            LichThanhPhanThamGia = new SPFieldLookupValueCollection();
            LichLanhDaoChuTri = new SPFieldLookupValue();
            LichGhiChu = string.Empty;
            LichNoiDung = string.Empty;
        }
    }
    public class LichHopJson : EntityJson
    {
        public LookupData fldGroup { set; get; }
        public LookupData CreatedUser { set; get; }
        public int OldID { get; set; }
        public string LichDiaDiem { set; get; }
        public string LichGhiChu { set; get; }
        public LookupData LichLanhDaoChuTri { set; get; }
        public string LichNoiDung { set; get; }
        public int DBPhanLoai { get; set; }
        public List<LookupData> LichThanhPhanThamGia { set; get; }
        public List<LookupData> LichPhongBanThamGia { set; get; }
        public DateTime? LichThoiGianBatDau { set; get; }
        public DateTime? LichThoiGianKetThuc { set; get; }
        //lưu thông tin giao lịch của phòng nếu có
        public string TBNoiDung { get; set; }
        public int LichTrangThai { set; get; }
        public string THANH_PHAN { get; set; }
        public string CHU_TRI { get; set; }
        public string CHUAN_BI { get; set; }
        public string LogText { get; set; }

        //public int CreatedUserID
        //{
        //    get
        //    {
        //        if (!string.IsNullOrEmpty(LogText) && LogText.Contains("_CreatedUser"))
        //        {
        //            string tempUser = LogText.Split('_').FirstOrDefault(x => x.Contains("CreatedUser"));
        //            if (!string.IsNullOrEmpty(tempUser))
        //            {
        //                tempUser = tempUser.Replace("CreatedUser", "");
        //                return Convert.ToInt32(tempUser);
        //            }
        //            else return 0;
        //        }
        //        else return 0;
        //    }
        //}
        public string LogNoiDung { set; get; }
        public GiaoLichDonVi objGiaoLichDonVi
        {
            get
            {
                if (!string.IsNullOrEmpty(LogNoiDung))
                {
                    return Newtonsoft.Json.JsonConvert.DeserializeObject<GiaoLichDonVi>(LogNoiDung);
                }
                else return new GiaoLichDonVi();

            }
        }

        public LichHopJson()
        {
            fldGroup = new LookupData();
            CreatedUser = new LookupData();
            LichThanhPhanThamGia = new List<LookupData>();
            LichLanhDaoChuTri = new LookupData();
            LichPhongBanThamGia = new List<LookupData>();
            LogText = string.Empty;
        }
    }

    public class LichHienThi
    {
        public string Thu { get; set; }
        public string Ngay { get; set; }
        public string Content { get; set; }
    }
}
