﻿using Microsoft.SharePoint;
using SolrNet;
using SolrNet.Commands.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortalData.QuyCheNoiBo
{
    public class QuyCheNoiBoDA : SPBaseDA<DataSolr>
    {
        private string _urlList = "/noidung/Lists/QuyCheNoiBo";
        public QuyCheNoiBoDA(bool isAdmin = false)
        {
            if (isAdmin)
                InitAdmin(_urlList, true);
            else
                Init(_urlList, true);
        }
        public QuyCheNoiBoDA(string _urlSiteProcess, bool isAdmin = false)
        {
            _urlList = _urlSiteProcess + "/noidung/Lists/QuyCheNoiBo";
            if (isAdmin)
                InitAdmin(_urlList, true);
            else
                Init(_urlList, true);
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<QuyCheNoiBoJson> GetListJson(QuyCheNoiBoQuery searhOption, params string[] Fields)
        {
            List<QuyCheNoiBoJson> lstReturn = new List<QuyCheNoiBoJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "QCNgayDang", "QCSoKyHieu", "QCTrichYeu", "QCToanVan", "QCNgayVanBan", "QCHieuLuc", "DMQuyChe", "_ModerationStatus" , "Created","STT" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<QuyCheNoiBoJson>(SPcoll, Fields);
            return lstReturn;
        }

        
        public string BuildQuery(QuyCheNoiBoQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if (searhOption.QC_DMQuyChe > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.TuNgay_QCNgayDang.HasValue)
                oBuildQuery.Append("<And>");
            if (searhOption.DenNgay_QCNgayDang.HasValue)
                oBuildQuery.Append("<And>");
            if (!string.IsNullOrEmpty(searhOption.QCSoKyHieu))
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            if (searhOption._ModerationStatus == 1)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption._ModerationStatus == 1)
            {
                oBuildQuery.Append(GetStringModer(0)); //appro là 0
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));
                oBuildQuery.Append("</And>");
            }
            if (!string.IsNullOrEmpty(searhOption.QCSoKyHieu))
            {
                oBuildQuery.Append(GetStringContainsText("QCSoKyHieu", searhOption.QCSoKyHieu));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.TuNgay_QCNgayDang.HasValue)
            {
                oBuildQuery.Append(GetStringCamlDate("QCNgayDang", searhOption.TuNgay_QCNgayDang.Value, "Geq"));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.DenNgay_QCNgayDang.HasValue)
            {
                oBuildQuery.Append(GetStringCamlDate("QCNgayDang", searhOption.DenNgay_QCNgayDang.Value, "Leq"));
                oBuildQuery.Append("</And>");
            }

            if (searhOption.QC_DMQuyChe > 0)
            {
                oBuildQuery.Append(GetStringEQLookUp("DMQuyChe", searhOption.QC_DMQuyChe));
                oBuildQuery.Append("</And>");
            }

            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
        public List<ISolrQuery> BuildQuerySolr(QuyCheNoiBoQuery searhOption)
        {
            var Querys = new List<ISolrQuery>();
            Querys.Add(new SolrQuery("*:*"));
            if (searhOption.UrlList != null && searhOption.UrlList.Count > 0)
            {
                //UrlList:("/noidung/Lists/VanBanTaiLieuSuKien" "/noidung/Lists/VanBanTaiLieu")
                Querys.Add(new SolrQuery(string.Format("FullUrlList:({0})", string.Join(" ", searhOption.UrlList.Select(x => "\"" + x + "\"")))));
            }
            else if (SpListProcess != null)
            {
                Querys.Add(new SolrQuery(string.Format("FullUrlList:\"{0}\"", SpListProcess.ParentWeb.ServerRelativeUrl + "/" + SpListProcess.RootFolder.Url)));
            }
            Querys.AddSearchBase(searhOption);
            if (searhOption.QC_DMQuyChe > 0)
                Querys.Add(new SolrQuery(string.Format("DMQuyChe_Id:\",{0},\"", searhOption.QC_DMQuyChe)));
            if (!string.IsNullOrEmpty(searhOption.QCSoKyHieu))
                Querys.Add(new SolrQuery(string.Format("QCSoKyHieu:\"{0}\"", searhOption.QCSoKyHieu)));
            if (searhOption.DenNgay_QCNgayDang.HasValue)
            {
                Querys.Add(new SolrQuery(GetQueryDateTimeSolrLeq("QCNgayDang", searhOption.DenNgay_QCNgayDang.Value)));
            }
            if (searhOption.TuNgay_QCNgayDang.HasValue)
            {
                Querys.Add(new SolrQuery(GetQueryDateTimeSolrGeq("QCNgayDang", searhOption.TuNgay_QCNgayDang.Value)));
            }
            //DMQuyChe

            BuildQueryToString(Querys);
            if (searhOption.HasDMQuyChe > 0)
            {
                Querys.Add(new SolrQuery("!DMQuyChe_Id:[* TO *]"));
            }
            return Querys;
        }
        public List<QuyCheNoiBoJson> GetListJsonSolr(QuyCheNoiBoQuery searhOption, params string[] Fields)
        {
            List<QuyCheNoiBoJson> lstReturn = new List<QuyCheNoiBoJson>();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "SP_ID", "Title", "QCNgayDang", "QCSoKyHieu", "QCTrichYeu", "QCToanVan", "QCNgayVanBan", "QCHieuLuc", "DMQuyChe", "_ModerationStatus", "WFTrangThai", "UserPhuTrach", "Attachments", "FullUrlList", "id" }; //Set default
            }
            var Querys = BuildQuerySolr(searhOption);
            #region Option khởi tạo
            var oorders = new List<SortOrder>();

            if (oorders.FindIndex(x => x.FieldName == searhOption.FieldOrder) == -1)
                oorders.Add(new SortOrder(searhOption.FieldOrder == "ID" ? "SP_ID" : searhOption.FieldOrder, (searhOption.Ascending) ? Order.ASC : Order.DESC));
            QueryOptions options = new QueryOptions()
            {
                OrderBy = oorders,
                Fields = Fields,
                //StartOrCursor =  new StartOrCursor.Start((currentPage - 1) * rowPerPage)
            };
            if (searhOption.Length > 0)
            {
                options.Rows = searhOption.Length;
                options.Start = searhOption.Start; //Barn ghi dau tien
            }

            #endregion
            var result = solrWorkerDM.Query(new SolrMultipleCriteriaQuery(Querys, "AND"), options);
            TongSoBanGhiSauKhiQuery = Convert.ToInt32(result.NumFound);
            foreach (DataSolr itemsolr in result)
            {
                lstReturn.Add(ConvertSolrToJson<QuyCheNoiBoJson>(itemsolr, Fields));
            }
            return lstReturn;
        }
    }
}