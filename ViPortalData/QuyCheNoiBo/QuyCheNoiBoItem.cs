using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;

namespace ViPortalData.QuyCheNoiBo
{
	public class QuyCheNoiBoItem : SPEntity
	{
		public int STT { set; get; }
		public DateTime? QCNgayDang { set; get; }
		public string QCSoKyHieu { set; get; }
		public string QCTrichYeu { set; get; }
		public string QCToanVan { set; get; }
		public DateTime? QCNgayVanBan { set; get; }
		public int QCHieuLuc { set; get; }
		public SPFieldLookupValue DMQuyChe { set; get; }

		public QuyCheNoiBoItem()
		{
			DMQuyChe = new SPFieldLookupValue();
		}
	}
	public class QuyCheNoiBoJson : EntityJson
	{
		public int STT { set; get; }
		public DateTime? QCNgayDang { set; get; }
		public string QCSoKyHieu { set; get; }
		public string QCTrichYeu { set; get; }
		public string QCToanVan { set; get; }
		public DateTime? QCNgayVanBan { set; get; }
		public int QCHieuLuc { set; get; }
		public LookupData DMQuyChe { set; get; }
		public QuyCheNoiBoJson()
		{
			DMQuyChe = new LookupData();

		}
	}
}