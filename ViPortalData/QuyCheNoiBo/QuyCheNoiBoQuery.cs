using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using ViPortal_Utils.Base;

namespace ViPortalData.QuyCheNoiBo
{
    public class QuyCheNoiBoQuery : QuerySearch
    {
        public int QC_DMQuyChe { get; set; }
        public int HasDMQuyChe { get; set; }
        public string QCSoKyHieu { get; set; }
        public DateTime? TuNgay_QCNgayDang { get; set; }
        public DateTime? DenNgay_QCNgayDang { get; set; }
        public QuyCheNoiBoQuery()
        {
        }
        public QuyCheNoiBoQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
}