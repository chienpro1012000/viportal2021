using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace ViPortalData.KSCauTraLoi
{
	public class KSCauTraLoiItem : SPEntity
	{
		public string DMDescription { set; get; }
		public bool DMHienThi { set; get; }
		public int DMSTT { set; get; }		
		public int SoLuotBinhChon { set; get; }		
		public SPFieldLookupValue KSBoChuDe { set; get; }
		public SPFieldLookupValue KSCauHoi { set; get; }

		public KSCauTraLoiItem()
		{
			KSBoChuDe = new SPFieldLookupValue();
			KSCauHoi = new SPFieldLookupValue();

		}
	}
	public class KSCauTraLoiJson : EntityJson
	{
		public string DMDescription { set; get; }
		public bool DMHienThi { set; get; }
		public int DMSTT { set; get; }
		public int SoLuotBinhChon { set; get; }
		public LookupData KSBoChuDe { set; get; }
		public LookupData KSCauHoi { set; get; }
		public KSCauTraLoiJson()
		{
			KSBoChuDe = new LookupData();
			KSCauHoi = new LookupData();

		}
	}
}