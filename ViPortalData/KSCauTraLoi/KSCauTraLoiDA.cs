﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortalData.KSCauTraLoi
{
    public class KSCauTraLoiDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/noidung/Lists/KSCauTraLoi";
        public KSCauTraLoiDA(bool isAdmin)
        {
            if (!isAdmin)
                Init(_urlList);
            else InitAdmin(_urlList);
        }
        public KSCauTraLoiDA(string _urlSiteProcess)
            : base(_urlSiteProcess + "/noidung/Lists/KSCauTraLoi")
        {
            _urlList = _urlSiteProcess;
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<KSCauTraLoiJson> GetListJson(KSCauTraLoiQuery searhOption, params string[] Fields)
        {
            List<KSCauTraLoiJson> lstReturn = new List<KSCauTraLoiJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "DMDescription", "DMHienThi", "DMSTT", "KSCauHoi", "KSBoChuDe", "_ModerationStatus", "SoLuotBinhChon" , "LoaiTraLoi" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<KSCauTraLoiJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(KSCauTraLoiQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            
            
            if (searhOption.IDCauHoi > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.lstIdGet.Count > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.lstIdGet.Count == 0 && searhOption.IDCauHoi <= 0)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption.lstIdGet.Count == 0 && searhOption.IDCauHoi <= 0)
            {
                oBuildQuery.Append(GetStringEQNumber("ID", 0));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.lstIdGet.Count > 0)
            {
                oBuildQuery.Append(getStringByListId(searhOption.lstIdGet));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.IDCauHoi > 0)
            {
                oBuildQuery.Append(GetStringEQLookUp("KSCauHoi", searhOption.IDCauHoi));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}