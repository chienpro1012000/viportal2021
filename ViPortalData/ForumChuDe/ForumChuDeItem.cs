using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{
    public class ForumChuDeItem : SPEntity
    {
        public string MoTa { set; get; }
        public SPFieldLookupValue ParentChuDe { set; get; }
        public int CountBaiViet { set; get; }
        public int CountComment { set; get; }
        public int DMSTT { get; set; }
        public ForumChuDeItem()
        {
            ParentChuDe = new SPFieldLookupValue();

        }
    }

    public class ForumChuDeJson : EntityJson
    {
        public string MoTa { set; get; }
        public LookupData ParentChuDe { set; get; }
        public int DMSTT { get; set; }
        public int CountBaiViet { set; get; }
        public int CountComment { set; get; }

        public ForumChuDeJson()
        {
            ParentChuDe = new LookupData();

        }
    }
}
