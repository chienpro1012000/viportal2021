﻿using Microsoft.SharePoint;
using SolrNet;
using SolrNet.Commands.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortalData.LichTapDoan
{
    public class LichTapDoanDA : SPBaseDA<DataSolr>
    {
        private string _urlList = "/noidung/Lists/LichTapDoan";
        public LichTapDoanDA(bool isAdmin = false)
        {
            if (isAdmin)
                InitAdmin(_urlList, true);
            else
                Init(_urlList, true);

        }
        public LichTapDoanDA()
        {
            Init(_urlList,true);
        }
        public LichTapDoanDA(string _urlSiteProcess)
        {
            _urlList = _urlSiteProcess + _urlList;
            Init(_urlList,true);
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<LichTapDoanJson> GetListJson(LichTapDoanQuery searhOption, params string[] Fields)
        {
            List<LichTapDoanJson> lstReturn = new List<LichTapDoanJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "LichDiaDiem", "LichGhiChu", "LichLanhDaoChuTri", "LichNoiDung", "LichThanhPhanThamGia", "LichThoiGianBatDau", "LichThoiGianKetThuc", "LichTrangThai", "_ModerationStatus" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<LichTapDoanJson>(SPcoll, Fields);
            return lstReturn;
        }

        public  int GetCount(LichTapDoanQuery searhOption)
        {
            List<LichTapDoanJson> lstReturn = new List<LichTapDoanJson>();
            SPQuery oQuery = new SPQuery();
            oQuery.ViewFields = BuildViewField("ID");
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = SpListProcess.GetItems(oQuery);
            return SPcoll.Count;
        }

        public string BuildQuery(LichTapDoanQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if (searhOption.TuNgay_LichThoiGianBatDau.HasValue)
                oBuildQuery.Append("<And>");
            if (searhOption.DenNgay_LichThoiGianBatDau.HasValue)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            if (searhOption._ModerationStatus == 1)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption._ModerationStatus == 1)
            {
                oBuildQuery.Append(GetStringModer(0)); //appro là 0
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetBylistID)
            {
                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            if (searhOption.DenNgay_LichThoiGianBatDau.HasValue)
            {
                oBuildQuery.Append(GetStringCamlDate("LichThoiGianBatDau", searhOption.DenNgay_LichThoiGianBatDau.Value, "Leq"));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.TuNgay_LichThoiGianBatDau.HasValue)
            {
                oBuildQuery.Append(GetStringCamlDate("LichThoiGianBatDau", searhOption.TuNgay_LichThoiGianBatDau.Value, "Geq"));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
        public List<ISolrQuery> BuildQuerySolr(LichTapDoanQuery searhOption)
        {
            var Querys = new List<ISolrQuery>();
            Querys.Add(new SolrQuery("*:*"));
            if (searhOption.UrlList != null && searhOption.UrlList.Count > 0)
            {
                //UrlList:("/noidung/Lists/VanBanTaiLieuSuKien" "/noidung/Lists/VanBanTaiLieu")
                Querys.Add(new SolrQuery(string.Format("FullUrlList:({0})", string.Join(" ", searhOption.UrlList.Select(x => "\"" + x + "\"")))));
            }
            else if (SpListProcess != null)
            {
                Querys.Add(new SolrQuery(string.Format("FullUrlList:\"{0}\"", SpListProcess.ParentWeb.ServerRelativeUrl + "/" + SpListProcess.RootFolder.Url)));
            }
            Querys.AddSearchBase(searhOption);
            if (searhOption.DenNgay_LichThoiGianBatDau.HasValue)
            {
                Querys.Add(new SolrQuery(GetQueryDateTimeSolrLeq("LichThoiGianBatDau", searhOption.DenNgay_LichThoiGianBatDau.Value)));
            }
            if (searhOption.TuNgay_LichThoiGianBatDau.HasValue)
            {
                Querys.Add(new SolrQuery(GetQueryDateTimeSolrGeq("LichThoiGianBatDau", searhOption.TuNgay_LichThoiGianBatDau.Value)));
            }
            BuildQueryToString(Querys);
            return Querys;
        }
        public List<LichTapDoanJson> GetListJsonSolr(LichTapDoanQuery searhOption, params string[] Fields)
        {
            List<LichTapDoanJson> lstReturn = new List<LichTapDoanJson>();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "SP_ID", "Title", "LichDiaDiem", "LichGhiChu", "LichLanhDaoChuTri", "LichNoiDung", "LichThanhPhanThamGia", "LichThoiGianBatDau", "LichThoiGianKetThuc", "LichTrangThai", "_ModerationStatus", "FullUrlList", "id" }; //Set default
            }
            var Querys = BuildQuerySolr(searhOption);
            #region Option khởi tạo
            var oorders = new List<SortOrder>();

            if (oorders.FindIndex(x => x.FieldName == searhOption.FieldOrder) == -1)
                oorders.Add(new SortOrder(searhOption.FieldOrder == "ID" ? "id" : searhOption.FieldOrder, (searhOption.Ascending) ? Order.ASC : Order.DESC));
            QueryOptions options = new QueryOptions()
            {
                OrderBy = oorders,
                Fields = Fields,
                //StartOrCursor =  new StartOrCursor.Start((currentPage - 1) * rowPerPage)
            };
            if (searhOption.Length > 0)
            {
                options.Rows = searhOption.Length;
                options.Start = searhOption.Start; //Barn ghi dau tien
            }

            #endregion
            var result = solrWorkerDM.Query(new SolrMultipleCriteriaQuery(Querys, "AND"), options);
            TongSoBanGhiSauKhiQuery = Convert.ToInt32(result.NumFound);
            foreach (DataSolr itemsolr in result)
            {
                lstReturn.Add(ConvertSolrToJson<LichTapDoanJson>(itemsolr, Fields));
            }
            return lstReturn;
        }
    }
}