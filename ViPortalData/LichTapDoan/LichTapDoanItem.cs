using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.JSGrid;
using ViPortal_Utils.Base;

namespace ViPortalData.LichTapDoan
{
	public class LichTapDoanItem : SPEntity
	{
		public string LichDiaDiem { set; get; }
		public string LichGhiChu { set; get; }
		public SPFieldLookupValue LichLanhDaoChuTri { set; get; }
		public string LichNoiDung { set; get; }
		public SPFieldLookupValueCollection LichThanhPhanThamGia { set; get; }
		public DateTime? LichThoiGianBatDau { set; get; }
		public DateTime? LichThoiGianKetThuc { set; get; }
		public int LichTrangThai { set; get; }
		public LichTapDoanItem()
		{
			LichLanhDaoChuTri = new SPFieldLookupValue();
			LichThanhPhanThamGia = new SPFieldLookupValueCollection();
			LichGhiChu = string.Empty;
			LichNoiDung = string.Empty;
		}
	}
	public class LichTapDoanJson : EntityJson
	{
		public string LichDiaDiem { set; get; }
		public string LichGhiChu { set; get; }
		public LookupData LichLanhDaoChuTri { set; get; }
		public string LichNoiDung { set; get; }
		public List<LookupData> LichThanhPhanThamGia { set; get; }
		public DateTime? LichThoiGianBatDau { set; get; }
		public DateTime? LichThoiGianKetThuc { set; get; }
		public int LichTrangThai { set; get; }
		public LichTapDoanJson()
		{
			LichLanhDaoChuTri = new LookupData();
			LichThanhPhanThamGia = new List<LookupData>();
		}
	}
}