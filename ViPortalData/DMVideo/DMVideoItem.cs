using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.JSGrid;
using ViPortal_Utils.Base;

namespace ViPortalData.DMVideo
{
	public class DMVideoItem : SPEntity
	{
		public int DMSTT { set; get; }
		public string DMMoTa { set; get; }
		public string ImageNews { set; get; }
		public string DMAnhListAnh { set; get; }
		public DateTime? TBNgayDang { set; get; }
		public DMVideoItem()
		{

		}
	}
	public class DMVideoJson : EntityJson
	{
		public int DMSTT { set; get; }
		public string DMMoTa { set; get; }
		public string ImageNews { set; get; }
		public string DMAnhListAnh { set; get; }
		public DateTime? TBNgayDang { set; get; }
		public DMVideoJson()
		{
		}
	}
}