using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{
   public class LMauGiaoDienItem:SPEntity
    {
        public string TemplateHtml {set;get;}
	public SPFieldLookupValue Module {set;get;}
	public int DMSTT {set;get;}
	public string DMMoTa {set;get;}
	
	public LMauGiaoDienItem(){
		Module=new SPFieldLookupValue();
	
	}
    }

	public class LMauGiaoDienJson : EntityJson
	{
		public string TemplateHtml { set; get; }
		public LookupData Module { set; get; }
		public int DMSTT { set; get; }
		public string DMMoTa { set; get; }

		public LMauGiaoDienJson()
		{
			Module = new LookupData();

		}
	}
}
