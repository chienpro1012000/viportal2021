using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortalData
{
    public class KyBaoCaoDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/htbaocao/Lists/KyBaoCao";
        public KyBaoCaoDA()
        {
            Init(_urlList);
        }
        public KyBaoCaoDA(string _urlListProcess)
            : base(_urlListProcess)
        {
            _urlList = _urlListProcess;
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<KyBaoCaoJson> GetListJson(KyBaoCaoQuery searhOption, params string[] Fields)
        {
            List<KyBaoCaoJson> lstReturn = new List<KyBaoCaoJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "_ModerationStatus", "BCLoai", "BCTuNgay", "BCDenNgay", "BCDenNgayTemp", "fldGroup","Attachments", "NgayHoanThanhDonVi","TitleKetQua", "fldUser", "DoiTuongThucHien", "Modified", "LogText",
                    "ThoiGianLapLai", "NgayThu", "TuanThu", "ThangBaoBao", "LoaiBaoCao", "DMVietTat", "TrangThaiBaoCao", "Created", "idRoot", "idRootKyDanhGia" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<KyBaoCaoJson>(SPcoll, Fields);
            Queryreturn = oQuery.Query;
            return lstReturn;
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public int GetCount(KyBaoCaoQuery searhOption)
        {
            List<KyBaoCaoJson> lstReturn = new List<KyBaoCaoJson>();
            SPQuery oQuery = new SPQuery();
            oQuery.ViewFields = BuildViewField("ID");
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = SpListProcess.GetItems(oQuery);
            Queryreturn = oQuery.Query;
            return SPcoll.Count;
        }

        public string BuildQuery(KyBaoCaoQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if (searhOption.idRoot > 0)
            {
                oBuildQuery.Append("<And>");
            }
            if (searhOption.idRootKyDanhGia == -1)
                oBuildQuery.Append("<And>");
            if (searhOption.idRootKyDanhGia > 0)
            {
                oBuildQuery.Append("<And>");
            }
            if (searhOption.UserThamGia > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.HasNgayHoanThanhDonVi == 1)
                oBuildQuery.Append("<And>");
            if (searhOption.HasNgayHoanThanhDonVi == 2)
                oBuildQuery.Append("<And>");
            if (searhOption.CurrentUserPT > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.TrangThaiBaoCao > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.fldGroup > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.LoaiQuery > 0)
                oBuildQuery.Append("<And>");
            if (!string.IsNullOrEmpty(searhOption.LogText))
                oBuildQuery.Append("<And>");
            if (!string.IsNullOrEmpty(searhOption.LogTextStartWith))
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption.isGetBylistID)
            {
                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));
                oBuildQuery.Append("</And>");
            }
            if (!string.IsNullOrEmpty(searhOption.LogTextStartWith))
            {
                oBuildQuery.Append(GetStringStartWith("LogText", searhOption.LogTextStartWith));
                oBuildQuery.Append("</And>");
            }
            if (!string.IsNullOrEmpty(searhOption.LogText))
            {
                oBuildQuery.Append(GetStringContainsText("LogText", searhOption.LogText));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.LoaiQuery > 0)
            {
                if (searhOption.LoaiQuery == 1)
                    oBuildQuery.Append(GetStringEQNumber("idRoot", 0));
                if (searhOption.LoaiQuery == 2)
                {
                    oBuildQuery.Append("<And>");
                    oBuildQuery.Append(getstringNumGt("idRoot", 0));
                    oBuildQuery.Append(GetStringEQNumber("idRootKyDanhGia", 0));
                    oBuildQuery.Append("</And>");
                }
                if (searhOption.LoaiQuery == 3)
                    oBuildQuery.Append(getstringNumGt("idRootKyDanhGia", 0));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.fldGroup > 0)
            {
                oBuildQuery.Append("<Or>");
                oBuildQuery.Append(GetStringStartWith("fldUser", searhOption.fldUser + ";#"));
                oBuildQuery.Append(GetStringEQLookUp("fldGroup", searhOption.fldGroup));
                oBuildQuery.Append("</Or>");
                oBuildQuery.Append("</And>");
            }
            if (searhOption.TrangThaiBaoCao > 0)
            {
                if (searhOption.TrangThaiBaoCao == 23)
                {
                    oBuildQuery.Append(getStringInListNumber("TrangThaiBaoCao", new List<int>() { 2, 3 }));
                }
                else
                    oBuildQuery.Append(GetStringEQNumber("TrangThaiBaoCao", searhOption.TrangThaiBaoCao));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.CurrentUserPT > 0)
            {
                oBuildQuery.Append(GetStringContainsText("CurrentUserPT", $",{searhOption.CurrentUserPT},"));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.HasNgayHoanThanhDonVi == 2)
            {
                oBuildQuery.Append(getstringNull("NgayHoanThanhDonVi"));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.HasNgayHoanThanhDonVi == 1)
            {
                oBuildQuery.Append(getstringNotNull("NgayHoanThanhDonVi"));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.UserThamGia > 0)
            {
                oBuildQuery.Append("<Or>");
                oBuildQuery.Append(GetStringContainsText("UserThamGia", $";#{searhOption.UserThamGia};#"));
                oBuildQuery.Append(GetStringStartWith("UserThamGia", $"{searhOption.UserThamGia};#"));
                oBuildQuery.Append("</Or>");
                oBuildQuery.Append("</And>");
            }
            if (searhOption.idRootKyDanhGia > 0)
            {
                oBuildQuery.Append(GetStringEQNumber("idRootKyDanhGia", searhOption.idRootKyDanhGia));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.idRootKyDanhGia == -1)
            {
                oBuildQuery.Append(GetStringEQNumber("idRootKyDanhGia", 0));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.idRoot > 0)
            {
                oBuildQuery.Append(GetStringEQNumber("idRoot", searhOption.idRoot));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}