using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using ViPortal_Utils.Base;

namespace ViPortalData
{
    public class KyBaoCaoQuery : QuerySearch
    {
        public int CurrentUserPT { get; set; }
        public int fldUser { get; set; }
        public string LogTextStartWith { get; set; }
        public string LogText { get; set; }
        public int idRoot { get; set; } = -1;
        public int fldGroup { get; set; }
        public int TrangThaiBaoCao { get; set; }
        public int  HasNgayHoanThanhDonVi {get;set;}
        public int idRootKyDanhGia { get; set; }
        public int UserThamGia { get; set; }
        /// <summary>
        /// 
        /// 1. loại báo cáo
        /// 2. kỳ báo cáo giao
        /// 3. báo cáo nhận
        /// </summary>
        public int LoaiQuery { get; set; }
        public KyBaoCaoQuery()
        {
        }
        public KyBaoCaoQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
}