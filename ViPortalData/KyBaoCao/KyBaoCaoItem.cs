﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace ViPortalData
{
    public class KyBaoCaoItem : SPEntity
    {
        public string DMDescription { set; get; }
        public bool DMHienThi { set; get; }
        public string DMMoTa { set; get; }
        public int DMSTT { set; get; }
        public string DMVietTat { set; get; }
        public SPFieldLookupValue LoaiBaoCao { set; get; }
        public DateTime? NgayHoanThanhDonVi { get; set; }
        public string TitleKetQua { get; set; }
        public DateTime? BCTuNgay { set; get; }
        public DateTime? BCDenNgay { set; get; }
        public int BCLoai { set; get; }
        public DateTime? BCTuNgayTemp { set; get; }
        public DateTime? BCDenNgayTemp { set; get; }
        public int ThoiGianLapLai { set; get; }
        /// <summary>
        /// 1,2,3,4
        /// </summary>
        public string NgayThu { set; get; }
        public string TuanThu { set; get; }
        public string ThangBaoBao { set; get; }
        public int TrangThaiPheDuyet { set; get; }
        public int TrangThaiBaoCao { set; get; }
        public SPFieldLookupValue fldGroup { set; get; }
        public SPFieldLookupValueCollection DonViThamGiaBC { set; get; }
        public SPFieldLookupValueCollection UserThamGia { get; set; }
        /// <summary>
        /// luu id,id
        /// </summary>
        public string CurrentUserPT { get; set; }
        /// <summary>
        /// 1 là đơn vị
        /// 2 là người dùng.
        /// </summary>
        public int DoiTuongThucHien { get; set; }
        /// <summary>
        /// id root báo cáo
        /// </summary>
        public int idRoot { set; get; }

        /// <summary>
        /// theo kỳ báo cáo.
        /// </summary>
        public int idRootKyDanhGia { get; set; }

        public string DonViChuaXuLy { set; get; }
        public string DonViDangXuLy { set; get; }

        /// <summary>
        /// Nhóm thông tin kết quả
        /// </summary>
        /// 
       
        public string MoTaKetQua { get; set; }
        /// <summary>
        /// Ngày hoàn thành
        /// </summary>
        public DateTime? NgayTaoKetQua { get; set; }
       
        public int isLDDonViDuyet { get; set; }

        public string LogText { get; set; }
        public string fldUser { get; set; }
        public string jsonBCCT { get; set; }
        public KyBaoCaoItem()
        {
            LoaiBaoCao = new SPFieldLookupValue();
            fldGroup = new SPFieldLookupValue();
            DonViThamGiaBC = new SPFieldLookupValueCollection();
            UserThamGia = new SPFieldLookupValueCollection();
            BCLoai = 1;
            LogText = "";
            DoiTuongThucHien = 1;

        }
    }
    public class KyBaoCaoJson : EntityJson
    {
        public int DoiTuongThucHien { get; set; }
        public DateTime? NgayHoanThanhDonVi { get; set; }
        public string TitleKetQua { get; set; }
        public int idRootKyDanhGia { get; set; }

        public string DMDescription { set; get; }
        public bool DMHienThi { set; get; }
        public string DMMoTa { set; get; }
        public int DMSTT { set; get; }
        public string DMVietTat { set; get; }
        public LookupData LoaiBaoCao { set; get; }
        public DateTime? BCTuNgay { set; get; }
        public DateTime? BCDenNgay { set; get; }
        public int BCLoai { set; get; }
        public DateTime? BCTuNgayTemp { set; get; }
        public DateTime? BCDenNgayTemp { set; get; }
        public int ThoiGianLapLai { set; get; }
        public string NgayThu { set; get; }
        public string TuanThu { set; get; }
        public string ThangBaoBao { set; get; }
        public int TrangThaiPheDuyet { set; get; }
        public int TrangThaiBaoCao { set; get; }
        public LookupData fldUser { get; set; }
        public string TrangThaiBaoCaoTitle
        {
            get
            {

                string _TrangThaiBaoCaoTitle = "";
                switch (TrangThaiBaoCao)
                {
                    case 0:
                        _TrangThaiBaoCaoTitle = "Mới khởi tạo";
                        break;
                    case 1:
                        _TrangThaiBaoCaoTitle = "Đã phân công";
                        break;
                    case 2:
                        if (idRoot != 0 && idRootKyDanhGia == 0) //báo cáo giao
                            _TrangThaiBaoCaoTitle = "Đã phân công";
                        else //báo cáo nhận
                            _TrangThaiBaoCaoTitle = "Chờ tiếp nhận";
                        break;
                    case 3:
                        _TrangThaiBaoCaoTitle = "Chờ xử lý";
                        break;
                    case 4:
                        _TrangThaiBaoCaoTitle = "Chờ cập nhập";
                        break;
                    case 5:
                        _TrangThaiBaoCaoTitle = "Chờ thực hiện báo cáo";
                        break;
                    case 6:
                        _TrangThaiBaoCaoTitle = "Đã cập nhật báo cáo";
                        break;
                    case 7:
                        _TrangThaiBaoCaoTitle = "Chờ phê duyệt";
                        break;
                    case 8:
                        _TrangThaiBaoCaoTitle = "Đồng ý phê duyệt";
                        break;
                    case 9:
                        _TrangThaiBaoCaoTitle = "Bị trả về";
                        break;
                    case 10:
                        _TrangThaiBaoCaoTitle = "Hoàn thành báo cáo";
                        break;

                }
                return _TrangThaiBaoCaoTitle;
            }
        }
        public LookupData fldGroup { set; get; }
        public List<LookupData> DonViThamGiaBC { set; get; }
        public int idRoot { set; get; }
        public string DonViChuaXuLy { set; get; }
        public string DonViDangXuLy { set; get; }

        public string MoTaTriggers
        {
            get
            {
                if (BCLoai == 1)
                {
                    return $"Báo cáo từ ngày {string.Format("{0:dd/MM/yyyy HH:mm}", BCTuNgay)} đến ngày {string.Format("{0:dd/MM/yyyy HH:mm}", BCDenNgay)}";
                }
                else if (BCLoai == 2)
                {
                    return $"Hạn thực hiện báo cáo trước ngày {string.Format("{0:dd/MM/yyyy HH:mm}", BCDenNgay)}";
                }
                else if (BCLoai == 3)
                {
                    if (ThoiGianLapLai <= 1)
                        return $"Báo cáo thường ngày, Vào lúc {string.Format("{0:HH:mm}", BCDenNgay)} hằng ngày";
                    else return $"Báo cáo thường ngày, Vào lúc {string.Format("{0:HH:mm}", BCDenNgay)}, {ThoiGianLapLai} ngày một lần.";
                }
                else if (BCLoai == 4)
                {
                    List<int> lstThu = clsFucUtils.GetDanhSachIDsQuaFormPost(NgayThu);
                    return $"Báo cáo tuần, Vào lúc {string.Format("{0:HH:mm}", BCTuNgay)} các ngày {string.Join(", ", lstThu.Select(x => clsFucUtils.GetDayOfWeek(x)))} mỗi {ThoiGianLapLai} tuần";

                }
                else if (BCLoai == 5)
                {
                    if (string.IsNullOrEmpty(TuanThu))
                    {
                        //ngay trong tháng.
                        List<int> lstThu = clsFucUtils.GetDanhSachIDsQuaFormPost(NgayThu);
                        List<int> lstThang = clsFucUtils.GetDanhSachIDsQuaFormPost(ThangBaoBao);
                        return $"Báo cáo tháng, Vào các ngày {string.Join(", ", lstThu)} hàng {string.Join(", ", lstThang.Select(x => "Tháng " + x))}";
                    }
                    else
                    {
                        //theo tuần trong tháng.
                        List<int> lstThu = clsFucUtils.GetDanhSachIDsQuaFormPost(NgayThu);
                        List<int> lstTuan = clsFucUtils.GetDanhSachIDsQuaFormPost(TuanThu);
                        List<int> lstThang = clsFucUtils.GetDanhSachIDsQuaFormPost(ThangBaoBao);
                        return $"Báo cáo tháng, Vào các ngày {string.Join(", ", lstThu.Select(x => clsFucUtils.GetDayOfWeek(x)))} tuần {string.Join(", ", lstTuan.Select(x => "thứ " + x))} hàng {string.Join(", ", lstThang.Select(x => "Tháng " + x))}";
                    }
                }
                else return "";
            }
        }

        public KyBaoCaoJson()
        {
            LoaiBaoCao = new LookupData();
            fldGroup = new LookupData();
            DonViThamGiaBC = new List<LookupData>();

        }
    }

    public class MeesageKySo
    {
        public bool suc { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
    }
}
