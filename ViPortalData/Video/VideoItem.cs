using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{
    public class VideoItem : SPEntity
    {
        public SPFieldLookupValue DanhMuc { set; get; }
        public string LinkVideo { set; get; }
        public DateTime? NgayDang { set; get; }
        public string ImageNews { set; get; }
        public int STT { set; get; }

        public VideoItem()
        {
            DanhMuc = new SPFieldLookupValue();

        }
    }
    public class VideoJson : EntityJson
    {
        public LookupData DanhMuc { set; get; }
        public string LinkVideo { set; get; }
        public DateTime? NgayDang { set; get; }
        public string ImageNews { set; get; }
        public int STT { set; get; }

        public VideoJson()
        {
            DanhMuc = new LookupData();

        }
    }
}
