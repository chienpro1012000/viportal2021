﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{
    public class DanhMucThongTinItem : SPEntity
    {
        public string UrlList { set; get; }
        public int TrangThai { set; get; }
        public bool isTichHop { set; get; }
        /// <summary>
        /// -0 là tin tức
        /// 1 là giới thiệu
        /// 2. là tin điểm báo
        /// </summary>
        public int DBPhanLoai { get; set; }
        public DanhMucThongTinItem() 
        {

        }
    }
    public class DanhMucThongTinJson : EntityJson
    {
        public int TrangThai { set; get; }
        public string FullUrlList { set; get; }
        //public string UrlList { set; get; }
        public bool isTichHop { set; get; }
        /// <summary>
        /// -0 là tin tức
        /// 1 là giới thiệu
        /// 2. là tin điểm báo
        /// </summary>
        public int DBPhanLoai { get; set; }
        public DanhMucThongTinJson()
        {
            isTichHop = false;
        }
    }
}
