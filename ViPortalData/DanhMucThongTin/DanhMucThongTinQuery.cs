﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public class DanhMucThongTinQuery : QuerySearch
    {
        public string Title { get; set; }
        
        public string UrlList { get; set; }
        /// <summary>
        /// 1- tin tức
        /// 2. là giới thiệu
        /// </summary>
        public int DBPhanLoai { get; set; }
        
        public DanhMucThongTinQuery()
        {
        }
        public DanhMucThongTinQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
}