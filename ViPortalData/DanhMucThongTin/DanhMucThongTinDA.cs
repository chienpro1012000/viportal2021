﻿using Microsoft.SharePoint;
using SolrNet;
using SolrNet.Commands.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public class DanhMucThongTinDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/cms/Lists/DanhMucThongTin";
        public DanhMucThongTinDA(bool isAdmin = false)
        {
            if (isAdmin)
                InitAdmin(_urlList);
            else
                Init(_urlList);
        }
        public DanhMucThongTinDA(string _urlSite)
        {
            _urlList = _urlSite + _urlList;
            Init(_urlList);
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<DanhMucThongTinJson> GetListJson(DanhMucThongTinQuery searhOption, params string[] Fields)
        {
            List<DanhMucThongTinJson> lstReturn = new List<DanhMucThongTinJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "UrlList", "TrangThai", "_ModerationStatus", "UrlList", "DBPhanLoai" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<DanhMucThongTinJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(DanhMucThongTinQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if(searhOption.DBPhanLoai > 0)
                oBuildQuery.Append("<And>");
            if (!string.IsNullOrEmpty(searhOption.UrlList))
                oBuildQuery.Append("<And>");
            if (!string.IsNullOrEmpty(searhOption.Title))
                oBuildQuery.Append("<And>");
            if (searhOption.ItemIDNotGet > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            if (searhOption._ModerationStatus == 1)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption._ModerationStatus == 1)
            {
                oBuildQuery.Append(GetStringModer(0)); //appro là 0
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            if (searhOption.ItemIDNotGet > 0)
            {
                oBuildQuery.Append(getstringNumNotEQ("ID", searhOption.ItemIDNotGet));
                oBuildQuery.Append("</And>");
            }
            if (!string.IsNullOrEmpty(searhOption.Title))
            {
                oBuildQuery.Append(GetStringEQText("Title", searhOption.Title));
                oBuildQuery.Append("</And>");
            }
            if (!string.IsNullOrEmpty(searhOption.UrlList))
            {
                oBuildQuery.Append(GetStringEQText("UrlList", searhOption.UrlList));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.DBPhanLoai > 0)
            {
                if(searhOption.DBPhanLoai == 9999)
                {
                    oBuildQuery.Append("<Or>");
                    oBuildQuery.Append(GetStringEQNumber("DBPhanLoai", 2));
                    oBuildQuery.Append("<Or>");
                    oBuildQuery.Append(GetStringEQNumber("DBPhanLoai", 0));
                    oBuildQuery.Append(getstringNull("DBPhanLoai"));
                    oBuildQuery.Append("</Or>");
                    oBuildQuery.Append("</Or>");
                }
                else
                {
                    oBuildQuery.Append(GetStringEQNumber("DBPhanLoai", searhOption.DBPhanLoai));
                }
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }




        public List<ISolrQuery> BuildQuerySolr(DanhMucThongTinQuery searhOption)
        {
            var Querys = new List<ISolrQuery>();
            Querys.Add(new SolrQuery("*:*"));
            if (!string.IsNullOrEmpty( searhOption.UrlList ))
            {
                Querys.Add(new SolrQuery(string.Format("FullUrlList:\"{0}\"", searhOption.UrlList)));
            }
            else if (SpListProcess != null)
            {
                Querys.Add(new SolrQuery(string.Format("FullUrlList:\"{0}\"", SpListProcess.ParentWeb.ServerRelativeUrl + "/" + SpListProcess.RootFolder.Url)));
            }
            Querys.AddSearchBase(searhOption);
            if (searhOption.ItemIDNotGet > 0)
                Querys.Add(new SolrQuery(string.Format("!SP_ID:\"{0}\"", searhOption.ItemIDNotGet)));
            if (!string.IsNullOrEmpty(searhOption.UrlList))
            {
                Querys.Add(new SolrQuery(string.Format("UrlList:\"{0}\"", searhOption.UrlList)));
            }
            BuildQueryToString(Querys);
            return Querys;
        }
        public List<DanhMucThongTinJson> GetListJsonSolr(DanhMucThongTinQuery searhOption, params string[] Fields)
        {
            List<DanhMucThongTinJson> lstReturn = new List<DanhMucThongTinJson>();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "SP_ID", "Title", "DescriptionNews", "CreatedDate", "ImageNews",
                    "DMSTT", "_ModerationStatus", "ReadCount", "DBPhanLoai",
                    "UrlList", "ItemTinTuc", "FullUrlList" }; //Set default
            }
            var Querys = BuildQuerySolr(searhOption);
            #region Option khởi tạo
            var oorders = new List<SortOrder>();

            if (oorders.FindIndex(x => x.FieldName == searhOption.FieldOrder) == -1)
                oorders.Add(new SortOrder(searhOption.FieldOrder == "ID" ? "id" : searhOption.FieldOrder, (searhOption.Ascending) ? Order.ASC : Order.DESC));
            QueryOptions options = new QueryOptions()
            {
                OrderBy = oorders,
                Fields = Fields,
                //StartOrCursor =  new StartOrCursor.Start((currentPage - 1) * rowPerPage)
            };
            if (searhOption.Length > 0)
            {
                options.Rows = searhOption.Length;
                options.Start = searhOption.Start; //Barn ghi dau tien
            }

            #endregion
            var result = solrWorkerDM.Query(new SolrMultipleCriteriaQuery(Querys, "AND"), options);
            TongSoBanGhiSauKhiQuery = Convert.ToInt32(result.NumFound);
            foreach (DanhMucSolr itemsolr in result)
            {
                lstReturn.Add(ConvertSolrToJson<DanhMucThongTinJson>(itemsolr, Fields));
            }
            return lstReturn;
        }


        public int GetCount(DanhMucThongTinQuery searhOption)
        {
            SPQuery oQuery = new SPQuery();
            oQuery.ViewFields = BuildViewField("ID");
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = SpListProcess.GetItems(oQuery);
            return SPcoll.Count;
        }
    }
}