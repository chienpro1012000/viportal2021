﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{
    public class NhatKyXuLyItem : SPEntity
    {
        public DateTime? CreatedDate { set; get; }
        public SPFieldLookupValue CreatedUser { set; get; }
        public string LogNoiDung { set; get; }
        public string LogDoiTuong { set; get; }
        public string DoiTuongTitle { get; set; }
        public int LogIDDoiTuong { set; get; }
        public string LogThaoTac { set; get; }
        public int LogTrangThai { set; get; }
        public string TrangThaiParent { get; set; }
        public string LogTrangThaiTiepTheo { set; get; }
        public string UserNhanXuLyText { set; get; }

        public NhatKyXuLyItem()
        {
            CreatedUser = new SPFieldLookupValue();
            CreatedDate = DateTime.Now;
        }
    }

    public class NhatKyXuLyJson : EntityJson
    {
        public DateTime? CreatedDate { set; get; }
        public LookupData CreatedUser { set; get; }
        public string LogNoiDung { set; get; }
        public string LogDoiTuong { set; get; }
        public int LogIDDoiTuong { set; get; }
        public string LogThaoTac { set; get; }
        public string LogThaoTacTitle
        {
            get
            {
                string _LogThaoTacTitle = "";
                if (!string.IsNullOrEmpty(LogThaoTac))
                {
                    switch (LogThaoTac)
                    {
                        case "TiepNhanBC":
                            _LogThaoTacTitle = "Tiếp nhận báo cáo";
                            break;
                        case "GiaoBaoCao":
                            _LogThaoTacTitle = "Giao báo cáo";
                            break;
                        case "ChuyenBaoCao":
                            _LogThaoTacTitle = "Chuyển báo cáo";
                            break;
                        case "GiaoThucHien":
                            _LogThaoTacTitle = "Phân/giao báo cáo";
                            break;
                        case "CapNhatKetQuaBC":
                            _LogThaoTacTitle = "Cập nhật kết quả";
                            break;
                        case "TrinhDuyet":
                            _LogThaoTacTitle = "Trình duyệt";
                            break;
                        case "PheDuyetBaoCao":
                            _LogThaoTacTitle = "Phê duyệt kết quả";
                            break;
                        case "Duyet_TraVe":
                            _LogThaoTacTitle = "Yêu cầu làm lại";
                            break;
                        case "XacNhanHoanThanh":
                            _LogThaoTacTitle = "Xác nhận hoàn thành";
                            break;
                        case "XacNhanHoanThanhGiao":
                            _LogThaoTacTitle = "Xác nhận hoàn thành";
                            break;
                        case "THUHOI":
                            _LogThaoTacTitle = "Thu hồi";
                            break;
                    }
                }
                return _LogThaoTacTitle;
            }
        }
        public int LogTrangThai { set; get; }
        public string LogTrangThaiTiepTheo { set; get; }
        public string UserNhanXuLyText { set; get; }
        public NhatKyXuLyJson()
        {
            CreatedUser = new LookupData();
        }
    }
}
