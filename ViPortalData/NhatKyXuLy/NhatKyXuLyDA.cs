using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public class NhatKyXuLyDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/cms/Lists/NhatKyXuLy";
        public NhatKyXuLyDA()
        {
            Init(_urlList);
        }
        public NhatKyXuLyDA(string _urlSiteProcess)
        {
            _urlList = _urlSiteProcess + _urlList;
            Init(_urlList);
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<NhatKyXuLyJson> GetListJson(NhatKyXuLyQuery searhOption, params string[] Fields)
        {
            List<NhatKyXuLyJson> lstReturn = new List<NhatKyXuLyJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0) 
            {
                Fields = new string[] { "ID", "Title", "CreatedDate", "LogNoiDung", "LogThaoTac", "CreatedUser", 
                    "TrangThaiParent", "LogTrangThai", "LogTrangThaiTiepTheo", "Attachments" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<NhatKyXuLyJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(NhatKyXuLyQuery  searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if (!string.IsNullOrEmpty( searhOption.LogDoiTuong ))
                oBuildQuery.Append("<And>");
            if (searhOption.LogIDDoiTuong > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            if (searhOption.LogIDDoiTuong > 0)
            {
                oBuildQuery.Append(GetStringEQNumber("LogIDDoiTuong", searhOption.LogIDDoiTuong));
                oBuildQuery.Append("</And>");
            }
            if (!string.IsNullOrEmpty(searhOption.LogDoiTuong))
            {
                oBuildQuery.Append(GetStringEQText("LogDoiTuong", searhOption.LogDoiTuong));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}