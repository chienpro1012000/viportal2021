﻿using Microsoft.SharePoint;
using SolrNet;
using SolrNet.Commands.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;
using ViPortalData.LichCaNhan;
using ViPortalData.LichPhong;

namespace ViPortalData.LichDonVi
{
    //public class WorkerAddLichPhong
    public class WorkerAddLichPhong
    {
        //string filePath = @"C:/SETUP/test.txt";
        public SPFieldLookupValueCollection LichPhongBanThamGia { get; set; }
        public LichDonViItem oLichDonViItem { get; set; }
        public WorkerAddLichPhong(SPFieldLookupValueCollection _LichPhongBanThamGia, LichDonViItem _oLichDonViItem)
        {
            oLichDonViItem = _oLichDonViItem;
            LichPhongBanThamGia = _LichPhongBanThamGia;
        }
        public void StartProcessing(CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                //thử xem nó làm gì ở đây có được ko.
                LichPhongDA lichPhongDA = new LichPhongDA();
                foreach (var PbHop in LichPhongBanThamGia)
                {
                    LichPhongItem lichPhong = new LichPhongItem()
                    {
                        fldGroup = new SPFieldLookupValue(PbHop.LookupId, ""),
                        OldID = oLichDonViItem.ID.ToString(),
                        Title = oLichDonViItem.Title,
                        LogText = oLichDonViItem.LogText + $"_LichDonVi-{oLichDonViItem.ID}_",
                        LichDiaDiem = oLichDonViItem.LichDiaDiem,
                        LichGhiChu = oLichDonViItem.LichGhiChu,
                        LichNoiDung = oLichDonViItem.LichNoiDung,
                        DBPhanLoai = oLichDonViItem.DBPhanLoai,
                        LichThoiGianBatDau = oLichDonViItem.LichThoiGianBatDau,
                        LichThoiGianKetThuc = oLichDonViItem.LichThoiGianKetThuc,
                        NoiDungChuanBi = oLichDonViItem.NoiDungChuanBi,
                        LichLanhDaoChuTri = oLichDonViItem.LichLanhDaoChuTri, //chỗ này là khi chọn đon vị.
                        LichPhongBanThamGia = oLichDonViItem.LichPhongBanThamGia, //chỗ này là khi chọn đon vị.
                        LichThanhPhanThamGia = oLichDonViItem.LichThanhPhanThamGia, //lấy từ thông tin giao
                        CHUAN_BI = oLichDonViItem.CHUAN_BI,
                        THANH_PHAN = oLichDonViItem.THANH_PHAN,
                        CHU_TRI = oLichDonViItem.CHU_TRI,
                        LogNoiDung = oLichDonViItem.LogNoiDung
                    };

                    lichPhongDA.UpdateObject<LichPhongItem>(lichPhong);
                    lichPhongDA.UpdateSPModerationStatus(lichPhong.ID, SPModerationStatusType.Approved); //duyệt luôn.
                }
            }
            catch (Exception ex)
            {
                ProcessCancellation();

            }
        }

        private void ProcessCancellation()
        {
            Thread.Sleep(10000);
        }
    }
    public class WorkerAddLichCaNhan
    {
        //string filePath = @"C:/SETUP/test.txt";
        public SPFieldLookupValueCollection LichCaNhanThamGia { get; set; }
        public LichDonViItem oLichDonViItem { get; set; }
        public WorkerAddLichCaNhan(SPFieldLookupValueCollection _LichCaNhanThamGia, LichDonViItem _oLichDonViItem)
        {
            oLichDonViItem = _oLichDonViItem;
            LichCaNhanThamGia = _LichCaNhanThamGia;
        }
        public void StartProcessing(CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                //thử xem nó làm gì ở đây có được ko.
                LichCaNhanDA oLichCaNhanDA = new LichCaNhanDA();
                foreach (var cbthamgia in LichCaNhanThamGia)
                {
                    LichCaNhanItem lichCaNhan = new LichCaNhanItem()
                    {
                        CreatedUser = new SPFieldLookupValue(cbthamgia.LookupId, ""),
                        OldID = oLichDonViItem.ID.ToString(),
                        Title = oLichDonViItem.Title,
                        LichDiaDiem = oLichDonViItem.LichDiaDiem,
                        LogNoiDung = oLichDonViItem.LogNoiDung,
                        LogText = oLichDonViItem.LogText + $"_LichDonVi-{oLichDonViItem.ID}_", //xác định được logtxt khi query
                        LichGhiChu = oLichDonViItem.LichGhiChu,
                        LichNoiDung = oLichDonViItem.LichNoiDung,
                        LichThoiGianBatDau = oLichDonViItem.LichThoiGianBatDau,
                        LichThoiGianKetThuc = oLichDonViItem.LichThoiGianKetThuc,
                        NoiDungChuanBi = oLichDonViItem.NoiDungChuanBi,
                        LichLanhDaoChuTri = oLichDonViItem.LichLanhDaoChuTri, //chỗ này là khi chọn đon vị.
                        LichPhongBanThamGia = oLichDonViItem.LichPhongBanThamGia, //chỗ này là khi chọn đon vị.
                        LichThanhPhanThamGia = oLichDonViItem.LichThanhPhanThamGia, //lấy từ thông tin giao
                        CHUAN_BI = oLichDonViItem.CHUAN_BI,
                        THANH_PHAN = oLichDonViItem.THANH_PHAN,
                        CHU_TRI = oLichDonViItem.CHU_TRI
                    };
                    oLichCaNhanDA.UpdateObject<LichCaNhanItem>(lichCaNhan);
                    oLichCaNhanDA.UpdateSPModerationStatus(lichCaNhan.ID, SPModerationStatusType.Approved); //duyệt luôn.
                }
            }
            catch (Exception ex)
            {
                ProcessCancellation();

            }
        }

        private void ProcessCancellation()
        {
            Thread.Sleep(10000);
        }
    }
    public class LichDonViDA : SPBaseDA<DataSolr>
    {
        private string _urlList = "/noidung/Lists/LichDonVi";
        public LichDonViDA(bool isAdmin = false)
        {
            if (isAdmin)
                InitAdmin(_urlList);
            else
                Init(_urlList, true);
        }
        public LichDonViDA(string _urlSiteProcess)
        {
            _urlList = _urlSiteProcess + _urlList;
            Init(_urlList,true);
        }


        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<LichDonViJson> GetListJson(LichDonViQuery searhOption, params string[] Fields)
        {
            List<LichDonViJson> lstReturn = new List<LichDonViJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "fldGroup", "LichDiaDiem", "LichGhiChu", "LichLanhDaoChuTri", 
                    "LichNoiDung", "LichThanhPhanThamGia", "LichThoiGianBatDau", "LichThoiGianKetThuc", "LichTrangThai",
                    "NoiDungChuanBi", "_ModerationStatus", "LogText" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<LichDonViJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(LichDonViQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();

            oBuildQuery.Append("<Where>");
            if(!string.IsNullOrEmpty( searhOption.LogText))
                oBuildQuery.Append("<And>");
            if (searhOption.OldID > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.fldGroup > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.TuNgay_LichThoiGianBatDau.HasValue)
                oBuildQuery.Append("<And>");
            if (searhOption.DenNgay_LichThoiGianBatDau.HasValue)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            if (searhOption._ModerationStatus == 1)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption._ModerationStatus == 1)
            {
                oBuildQuery.Append(GetStringModer(0)); //appro là 0
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            if (searhOption.DenNgay_LichThoiGianBatDau.HasValue)
            {
                oBuildQuery.Append(GetStringCamlDate("LichThoiGianBatDau", searhOption.DenNgay_LichThoiGianBatDau.Value, "Leq"));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.TuNgay_LichThoiGianBatDau.HasValue)
            {
                oBuildQuery.Append(GetStringCamlDate("LichThoiGianBatDau", searhOption.TuNgay_LichThoiGianBatDau.Value, "Geq"));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.fldGroup > 0)
            {
                oBuildQuery.Append(GetStringEQLookUp("fldGroup", searhOption.fldGroup));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.OldID > 0)
            {
                oBuildQuery.Append(GetStringEQText("OldID", searhOption.OldID.ToString()));
                oBuildQuery.Append("</And>");
            }
            if (!string.IsNullOrEmpty(searhOption.LogText))
            {
                oBuildQuery.Append(GetStringContainsText("LogText", searhOption.LogText));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
        public List<ISolrQuery> BuildQuerySolr(LichDonViQuery searhOption)
        {
            var Querys = new List<ISolrQuery>();
            Querys.Add(new SolrQuery("*:*"));
            if(searhOption.isLichTrangThai)
                Querys.Add(new SolrQuery(string.Format("LichTrangThai:\"0\"")));
            if (searhOption.UrlList != null && searhOption.UrlList.Count > 0)
            {
                //UrlList:("/noidung/Lists/VanBanTaiLieuSuKien" "/noidung/Lists/VanBanTaiLieu")
                Querys.Add(new SolrQuery(string.Format("FullUrlList:({0})", string.Join(" ", searhOption.UrlList.Select(x => "\"" + x + "\"")))));
            }
            else if (SpListProcess != null)
            {
                Querys.Add(new SolrQuery(string.Format("FullUrlList:\"{0}\"", SpListProcess.ParentWeb.ServerRelativeUrl + "/" + SpListProcess.RootFolder.Url)));
            }
            Querys.AddSearchBase(searhOption);
            if (searhOption.fldGroup > 0)
                Querys.Add(new SolrQuery(string.Format("fldGroup_Id:\"{0}\"", searhOption.fldGroup)));
            
            if (searhOption.DenNgay_LichThoiGianBatDau.HasValue)
            {
                Querys.Add(new SolrQuery(GetQueryDateTimeSolrLeq("LichThoiGianBatDau", searhOption.DenNgay_LichThoiGianBatDau.Value.AddDays(1))));
            }
            if (searhOption.TuNgay_LichThoiGianBatDau.HasValue)
            {
                Querys.Add(new SolrQuery(GetQueryDateTimeSolrGeq("LichThoiGianBatDau", searhOption.TuNgay_LichThoiGianBatDau.Value)));
            }
            BuildQueryToString(Querys);
            return Querys;
        }
        public List<LichDonViJson> GetListJsonSolr(LichDonViQuery searhOption, params string[] Fields)
        {
            List<LichDonViJson> lstReturn = new List<LichDonViJson>();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "SP_ID", "Title", "fldGroup", "LichDiaDiem", "LichGhiChu", "LichLanhDaoChuTri", "LichNoiDung", "LichThanhPhanThamGia", "LichPhongBanThamGia",
                    "LichThoiGianBatDau", "LichThoiGianKetThuc", "LichTrangThai", "NoiDungChuanBi", "_ModerationStatus", "FullUrlList", "id", "LogNoiDung", 
                    "DBPhanLoai", "LogText", "CHU_TRI", "CHUAN_BI", "THANH_PHAN" }; //Set default
            }
            var Querys = BuildQuerySolr(searhOption);
            #region Option khởi tạo
            var oorders = new List<SortOrder>();

            if (oorders.FindIndex(x => x.FieldName == searhOption.FieldOrder) == -1)
                oorders.Add(new SortOrder(searhOption.FieldOrder == "ID" ? "id" : searhOption.FieldOrder, (searhOption.Ascending) ? Order.ASC : Order.DESC));
            QueryOptions options = new QueryOptions()
            {
                OrderBy = oorders,
                Fields = Fields,
                //StartOrCursor =  new StartOrCursor.Start((currentPage - 1) * rowPerPage)
            };
            if (searhOption.Length > 0)
            {
                options.Rows = searhOption.Length;
                options.Start = searhOption.Start; //Barn ghi dau tien
            }

            #endregion
            var result = solrWorkerDM.Query(new SolrMultipleCriteriaQuery(Querys, "AND"), options);
            TongSoBanGhiSauKhiQuery = Convert.ToInt32(result.NumFound);
            foreach (DataSolr itemsolr in result)
            {
                LichDonViJson lichDonViJson = ConvertSolrToJson<LichDonViJson>(itemsolr, Fields);
                //xử lý đoạn này cho nó cái này.
                //lichDonViJson.LogNoiDung = GiaoLichDonVi giaoLichDonVi = new GiaoLichDonVi()
                lstReturn.Add(lichDonViJson);
            }
            return lstReturn;
        }
    }
}