﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.JSGrid;
using ViPortal_Utils.Base;

namespace ViPortalData.LichDonVi
{

    //    {
    //  "ID_VPKG": "12345",
    //  "Ngay": "2021-09-25 00:00:00.000",
    //  "ThoiGian": "8",
    //  "Phut": "30",
    //  "DenGio": "11",
    //  "DenPhut": "30",
    //  "NoiDung": "Họp duyệt TKKT, TKBVTC công trình xây dựng mới trạm 220/110 kV Đại Mỗ và nhánh rẽ 220kV",
    //  "ThanhPhan": "Ô Tùng X14, Ô Cường TP X1.8",
    //  "DonVi": "X1.8 kết nối Zoom ",
    //  "DiaDiem": "Zoom Cloud meetings",
    //  "ChuTri": "B8",
    //  "NguoiCap": "huongnh",
    //  "TgCap": "2021-09-24 22:11:49.000",
    //  "Duyet": "1",
    //  "TgDuyet": "2021-09-24 22:11:49.000",
    //  "Hoan": "3",
    //  "del": "0",
    //  "del_user": "",
    //  "del_time": ""
    //}
public class DongBoLichDonViDO
    {
        public string ID_VPKG { get; set; }
        public string Ngay { get; set; }
        public string ThoiGian { get; set; }
        public string Phut { get; set; }
        public string DenGio { get; set; }
        public string DenPhut { get; set; }
        public string NoiDung { get; set; }
        public string ThanhPhan { get; set; }
        public string DonVi { get; set; }
        public string DiaDiem { get; set; }
        public string ChuTri { get; set; }
        public string NguoiCap { get; set; }
        public string TgCap { get; set; }
        public string Duyet { get; set; } = "1";
        public string TgDuyet { get; set; }
        public string Hoan { get; set; }
        public string del { get; set; }
        public string del_user { get; set; }
        public string del_time { get; set; }
    }


    public class LichDonViItem : SPEntity
    {
        public SPFieldLookupValue fldGroup { set; get; }
        public string LichDiaDiem { set; get; }
        public string LichGhiChu { set; get; }
        public SPFieldLookupValue LichLanhDaoChuTri { set; get; }
        public string LichNoiDung { set; get; }
        public SPFieldLookupValueCollection LichThanhPhanThamGia { set; get; }
        public SPFieldLookupValueCollection LichPhongBanThamGia { set; get; }
        public string NoiDungChuanBi { set; get; }
        public DateTime? LichThoiGianBatDau { set; get; }
        public DateTime? LichThoiGianKetThuc { set; get; }
        public int LichTrangThai { set; get; }
        public string CHU_TRI { set; get; }
        public string LogText { get; set; }
        public int DBPhanLoai { get; set; }
        public string OldID { get; set; } //luu lịch của tổng cty khi là lịch giao xuống
        public string CHUAN_BI { set; get; }
        public string THANH_PHAN { set; get; }
        public string ThongTinGiaoLich { set; get; }
        public string LogNoiDung { set; get; }
        public LichDonViItem()
        {
            fldGroup = new SPFieldLookupValue();
            LichLanhDaoChuTri = new SPFieldLookupValue();
            LichThanhPhanThamGia = new SPFieldLookupValueCollection();
            LichPhongBanThamGia = new SPFieldLookupValueCollection();
            LichGhiChu = string.Empty;
            ThongTinGiaoLich = string.Empty;
            LichNoiDung = string.Empty;
        }
    }
    public class LichDonViJson : EntityJson
    {
        public LookupData fldGroup { set; get; }
        public string LichDiaDiem { set; get; }
        public string LichGhiChu { set; get; }
        public string CHUAN_BI { set; get; }
        public string CHUAN_BI_Title
        {
            get
            {
                if (!string.IsNullOrEmpty(CHUAN_BI))
                {
                    if (!string.IsNullOrEmpty(LogText) && LogText.Contains("_CreatedUser"))
                    {
                        List<LookupData> CHUAN_BIObj = clsFucUtils.GetDanhSachMutilLoookupByStringFiledData(CHUAN_BI);
                        return string.Join(",", CHUAN_BIObj.Select(x => x.Title));
                    }
                    else
                    {
                        return CHUAN_BI;
                    }
                }
                else return "";
            }
        }
        public string CHU_TRI_Title
        {
            get
            {
                if (!string.IsNullOrEmpty(CHU_TRI))
                {
                    if (!string.IsNullOrEmpty(LogText) && LogText.Contains("_CreatedUser"))
                    {
                        LookupData CHU_TRILookup = clsFucUtils.GetLoookupByStringFiledData(CHU_TRI);
                        return CHU_TRILookup.Title;
                    }
                    else
                    {
                        return CHU_TRI;
                    }
                }
                else return "";
            }
        }
        public string CHU_TRI { set; get; }
        public string THANH_PHAN { set; get; }

        public string THANH_PHAN_Title
        {

            get
            {
                string _THANH_PHAN_Title = "";
                if (!string.IsNullOrEmpty(LogText) && LogText.Contains("_CreatedUser"))
                {
                    //lịch tự đk
                    _THANH_PHAN_Title = $"{((LichLanhDaoChuTri != null && !string.IsNullOrEmpty(LichLanhDaoChuTri.Title)) ? (" ," + LichLanhDaoChuTri.Title) : "")} {(LichPhongBanThamGia != null ? (" ," + string.Join(",", LichPhongBanThamGia.Select(x => x.Title))) : "")}{(LichPhongBanThamGia != null ? (", " + string.Join(",", LichThanhPhanThamGia.Select(x => x.Title))) : "")}{(!string.IsNullOrWhiteSpace(THANH_PHAN) ? ("," + THANH_PHAN) : "")}";
                }
                else
                {
                    //lịch đồng bộ.
                    _THANH_PHAN_Title = $"{((LichLanhDaoChuTri != null && !string.IsNullOrEmpty(LichLanhDaoChuTri.Title)) ? (LichLanhDaoChuTri.Title) : "")}{(LichPhongBanThamGia != null ? (", " + string.Join(",", LichThanhPhanThamGia.Select(x => x.Title))) : "")} {(LichPhongBanThamGia != null ? (" ," + string.Join(",", LichPhongBanThamGia.Select(x => x.Title))) : "")}";
                }
                if (_THANH_PHAN_Title.Trim().StartsWith(","))
                {
                    _THANH_PHAN_Title = _THANH_PHAN_Title.Trim().Substring(1);
                }
                return _THANH_PHAN_Title;
            }
        }

        public LookupData LichLanhDaoChuTri { set; get; }
        public string LichNoiDung { set; get; }
        public List<LookupData> LichThanhPhanThamGia { set; get; }
        public int DBPhanLoai { get; set; }
        public List<LookupData> LichPhongBanThamGia { set; get; }
        public DateTime? LichThoiGianBatDau { set; get; }
        public string LogText { get; set; }
        public DateTime? LichThoiGianKetThuc { set; get; }
        public int LichTrangThai { set; get; }
        public string NoiDungChuanBi { set; get; }
        public string LogNoiDung { set; get; }
        public GiaoLichDonVi objGiaoLichDonVi
        {
            get
            {
                if (!string.IsNullOrEmpty(LogNoiDung))
                {
                    return Newtonsoft.Json.JsonConvert.DeserializeObject<GiaoLichDonVi>(LogNoiDung);
                }
                else return new GiaoLichDonVi();

            }
        }
        public LichDonViJson()
        {
            fldGroup = new LookupData();
            LichLanhDaoChuTri = new LookupData();
            LichThanhPhanThamGia = new List<LookupData>();
            LichPhongBanThamGia = new List<LookupData>();
        }
    }
    public class KhachMoi
    {
        public string Hoten { get; set; }
        public string SDT { get; set; }
        public string CMNDCCCD { get; set; }
    }
    public class GiaoLichDonVi
    {
        public SPFieldLookupValue LichLanhDaoChuTri { get; set; }
        public SPFieldLookupValueCollection LichPhongBanThamGia { get; set; }
        public SPFieldLookupValueCollection LichThanhPhanThamGia { get; set; }
        public List<KhachMoi> JsonKhachMoi { get; set; }
        public List<BaseFileAttach> ListFileAttach { get; set; }
        public string LichGhiChuGiao { get; set; }
        public string LichSoNguoiThamgia { get; set; }

        public GiaoLichDonVi()
        {
            LichLanhDaoChuTri = new SPFieldLookupValue();
            LichPhongBanThamGia = new SPFieldLookupValueCollection();
            LichThanhPhanThamGia = new SPFieldLookupValueCollection();
            JsonKhachMoi = new List<KhachMoi>();
            ListFileAttach = new List<BaseFileAttach>();
            LichSoNguoiThamgia = "";
        }
    }
}