﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using ViPortal_Utils.Base;

namespace ViPortalData.LichDonVi
{
    public class LichDonViQuery : QuerySearch
    {
        public DateTime? TuNgay_LichThoiGianBatDau { get; set; }
        public DateTime? DenNgay_LichThoiGianBatDau { get; set; }
        public int fldGroup { get; set; }
        public int OldID { get; set; }
        public string LogText { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool isLichTrangThai { get; set; }
        public LichDonViQuery()
        {
        }
        public LichDonViQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
}