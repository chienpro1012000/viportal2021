﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;


namespace ViPortalData.ThongBaoKetLuan
{
	public class ThongBaoKetLuanItem : SPEntity
	{
		public string TBLanhDaoKetLuan { set; get; }
		public DateTime? TBNgayDang { set; get; }
		public DateTime? TBNgayRaThongBao { set; get; }
		public string TBNoiDung { set; get; }
		public string TBSoKyHieu { set; get; }
		public string TBTrichYeu { set; get; }

		public SPFieldLookupValueCollection LstMaiLSend { get; set; }
	
		// lookup nhiều	public SPFieldLookupValueCollection BBB { set; get; }
		public ThongBaoKetLuanItem()
		{

			Title = string.Empty;
			TBLanhDaoKetLuan = string.Empty;
			TBNoiDung = string.Empty;
			TBSoKyHieu = string.Empty;
			TBTrichYeu = string.Empty;

		}
	}
	public class ThongBaoKetLuanJson : EntityJson
	{
		public string TBLanhDaoKetLuan { set; get; }
		public DateTime? TBNgayDang { set; get; }
		public DateTime? TBNgayRaThongBao { set; get; }
		public string TBNoiDung { set; get; }
		public string TBSoKyHieu { set; get; }
		public string TBTrichYeu { set; get; }

		// lookup nhiều	public  List<LookupData> SSs { set; get; }
		public ThongBaoKetLuanJson()
		{
			Title = string.Empty;
			TBLanhDaoKetLuan = string.Empty;
			TBNoiDung = string.Empty;
			TBSoKyHieu = string.Empty;
			TBTrichYeu = string.Empty;

		}
	}
	public class ThongBaoKetLuanRespon
	{
		public bool suc { get; set; }
		public string msg { get; set; }
		public string data { get; set; }
		public string file_base64 { get; set; }
	}
	public class VanBanEVN
    {
		public double ID_VB { get; set; }
		public string KY_HIEU { get; set; }
		public string TRICH_YEU { get; set; }
		public DateTime NGAY_VB { get; set; }
		public string NGUOI_KY { get; set; }
		public string file_base64 { get; set; }
		public List<UploadFilesResult> ListFileAttachAdd
		{
			get; set;
		}

		public VanBanEVN()
        {
			ListFileAttachAdd = new List<UploadFilesResult>();
        }

	}
}
