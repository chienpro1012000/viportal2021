using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace ViPortalData
{
    public class TaiLieuChiaSeItem : SPEntity
    {
        public SPFieldLookupValue CreatedUser { set; get; }
        public SPFieldLookupValue fldGroup { set; get; }
        public SPFieldLookupValue DMTaiLieu { set; get; }

        public TaiLieuChiaSeItem()
        {
            CreatedUser = new SPFieldLookupValue();
            fldGroup = new SPFieldLookupValue();
            DMTaiLieu = new SPFieldLookupValue();

        }
    }

    public class TaiLieuChiaSeJson : EntityJson
    {
        public LookupData CreatedUser { set; get; }
        public LookupData fldGroup { set; get; }
        public LookupData DMTaiLieu { set; get; }

        public TaiLieuChiaSeJson()
        {
            CreatedUser = new LookupData();
            fldGroup = new LookupData();
            DMTaiLieu = new LookupData();

        }
    }
}
