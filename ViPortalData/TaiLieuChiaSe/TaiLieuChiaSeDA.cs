using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortalData
{
    public class TaiLieuChiaSeDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/noidung/Lists/TaiLieuChiaSe";
        public TaiLieuChiaSeDA()
        {
            Init(_urlList);
        }
        public TaiLieuChiaSeDA(string _urlSite, bool isAdmin = false)
        {
            _urlList = _urlSite + _urlList;
            if (isAdmin)
                InitAdmin(_urlList, true);
            else
                Init(_urlList, true);
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<TaiLieuChiaSeJson> GetListJson(TaiLieuChiaSeQuery searhOption, params string[] Fields)
        {
            List<TaiLieuChiaSeJson> lstReturn = new List<TaiLieuChiaSeJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0) 
            {
                Fields = new string[] { "ID", "Title", "DMTaiLieu","Created", "CreatedUser", "Attachments", "fldGroup" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<TaiLieuChiaSeJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(TaiLieuChiaSeQuery  searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if(searhOption.lstfldGroup?.Count > 0)
            {
                oBuildQuery.Append("<And>");
            }
            if(searhOption.DMTaiLieu > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            if (searhOption.DMTaiLieu > 0)
            {
                oBuildQuery.Append(GetStringEQLookUp("DMTaiLieu", searhOption.DMTaiLieu));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.lstfldGroup?.Count > 0)
            {
                oBuildQuery.Append(getStringMultiLookup(searhOption.lstfldGroup, "fldGroup"));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}