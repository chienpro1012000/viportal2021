﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ViPortalData;

namespace ViPortalData
{
    public class BaseJsonBC
    {
        /// <summary>
        /// 
        /// </summary>
        //thêm tham số ở đây
        public int NotHasSTT { get; set; }

        /// <summary>
        /// 1 là bold
        /// </summary>
        public int Style { get; set; }

        public BaseJsonBC ()
        {

        }
    }
    class Model
    {
    }
    public class DynamicMappingResolver : DefaultContractResolver 
    {
        private Dictionary<Type, Dictionary<string, string>> memberNameToJsonNameMap;

        public DynamicMappingResolver(Dictionary<Type, Dictionary<string, string>> memberNameToJsonNameMap)
        {
            this.memberNameToJsonNameMap = memberNameToJsonNameMap;
        }

        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            JsonProperty prop = base.CreateProperty(member, memberSerialization);
            Dictionary<string, string> dict;
            string jsonName;
            if (memberNameToJsonNameMap.TryGetValue(member.DeclaringType, out dict) &&
                dict.TryGetValue(member.Name, out jsonName))
            {
                prop.PropertyName = jsonName;
            }
            return prop;
        }
    }

    /// <summary>
    /// Chỉ tiêu kinh doanh
    /// </summary>
    public class ChiTieuKinhDoanhJson
    {

        public string STT { get; set; }
        public string ChiTieu { get; set; }
        public string DonViTinh { get; set; }
        public string NguoiKy { get; set; }
        public string Thang { get; set; }

        public string LuyKeNam { get; set; }
        public string CungKy { get; set; }
        public string KHoach { get; set; }
    }
    public class MapChiTieuKinhDoanhJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data1 { get; set; }
        public string data2 { get; set; }
    }
    public class TongHopDoTinCayLuoiDien110_550kVJson
    {
        public string INDEX1 { get; set; }
        public string INDEX2 { get; set; }
        public string INDEX3 { get; set; }
        public string INDEX4 { get; set; }
        public string INDEX5 { get; set; }
        public string INDEX6 { get; set; }
        public string INDEX7 { get; set; }
        public string INDEX8 { get; set; }
        public string INDEX9 { get; set; }
        public string INDEX10 { get; set; }
        public string INDEX11 { get; set; }
        public string INDEX12 { get; set; }
        public string INDEX13 { get; set; }
        public string INDEX14 { get; set; }
        public string INDEX15 { get; set; }
        public string INDEX16 { get; set; }
        public string INDEX17 { get; set; }
        public string INDEX18 { get; set; }
        public string INDEX19 { get; set; }


    }
    public class THSCVaSSCDuongDayVaTBALTHAJson
    {
        public string INDEX1 { get; set; }
        public string INDEX2 { get; set; }
        public string INDEX3 { get; set; }
        public string INDEX4 { get; set; }
        public string INDEX5 { get; set; }
        public string INDEX6 { get; set; }
        public string INDEX7 { get; set; }
     

    }
    public class MapTHSCVaSSCDuongDayVaTBALTHAJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<THSCVaSSCDuongDayVaTBALTHAJson> data { get; set; }
    } 
    public class MapTongHopDoTinCayLuoiDien110_550kVJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<TongHopDoTinCayLuoiDien110_550kVJson> data { get; set; }
    }
    public class BaoCaoChiTeuDVKHJson
    {

        public string STT { get; set; }
        public string ChiTieu { get; set; }
        public string DonVi { get; set; }

        public string SoLuongTBC { get; set; }
        public string SoCungKiTBC { get; set; }
        public string TiTrongTBC { get; set; }
        public string TThoiGianTHienTBC { get; set; }
        public string ThoiGianTBTBC { get; set; }
        public string SoCungKi2TBC { get; set; }

        public string SoLuongLK { get; set; }
        public string SoCungKiLK { get; set; }
        public string TiTrongLK { get; set; }
        public string TThoiGianTHienLK { get; set; }
        public string ThoiGianBTLK { get; set; }
        public string SoCungKi2LK { get; set; }

    }
    public class MapBaoCaoChiTeuDVKHJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
        public string data2 { get; set; }
    }
    public class BaoCaoDienNangGNDNJson: BaseJsonBC
    {
        public BaoCaoDienNangGNDNJson() { }
        public BaoCaoDienNangGNDNJson(String tenKm)
        {
            STT = "";
            DVGiaoNhan = tenKm;
            DNNBinhThuong = "0";
            DNNCaoDiem = "0";
            DNNThapDiem = "0";
            DNNCto1Gia = "0";
            DNNTongCong = "0";
            DNGBinhThuong = "0";
            DNGCaoDiem = "0";
            DNGThapDiem = "0";
            DNGCto1Gia = "0";
            DNGTongCong = "0";
            DNTNBinhThuong = "0";
            DNTNCaoDiem = "0";
            DNTNThapDiem = "0";
            DNTNTongCong = "0";
        }
        public string DVGiaoNhan { get; set; }
        public string STT { get; set; }
        public string DNNBinhThuong { get; set; }
        public string DNNCaoDiem { get; set; }
        public string DNNThapDiem { get; set; }
        public string DNNCto1Gia { get; set; }
        public string DNNTongCong { get; set; }
        public string DNGBinhThuong { get; set; }
        public string DNGCaoDiem { get; set; }
        public string DNGThapDiem { get; set; }
        public string DNGCto1Gia { get; set; }
        public string DNGTongCong { get; set; }
        public string DNTNBinhThuong { get; set; }
        public string DNTNCaoDiem { get; set; }
        public string DNTNThapDiem { get; set; }
        public string DNTNTongCong { get; set; }
    }
    public class MapBaoCaoDienNangGNDNJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
    }
    public class BaoCaoTinhHinhSuCoLuoiDienJson
    {
        public string STT { get; set; }
        public string TenDonVi { get; set; }
        public string TenThietBi { get; set; }
        public string CapDienAp { get; set; }
        public string NgayXHSC { get; set; }
        public string GioXHSC { get; set; }
        public string NgayKPSC { get; set; }
        public string GioKPSC { get; set; }
        public string DienBienSuCo { get; set; }
        public string NguyenNhan { get; set; }
        public string ThoiGianGianDoanCungCapDien { get; set; }
        public string ThoiGianSuCo { get; set; }
        public string CongXuatTaiTruocSuCo { get; set; }
        public string DienNangKhongCungCapDuoc { get; set; }
        public string CachDien { get; set; }
        public string MoiNoi { get; set; }
        public string TBPhu { get; set; }
        public string KhacCap { get; set; }
        public string MayBienAp { get; set; }
        public string MayCat { get; set; }
        public string BVTram { get; set; }
        public string CSTram { get; set; }
        public string MayBienDienApDongDien { get; set; }
        public string KhacTram { get; set; }
        public string KD { get; set; }
        public string TQ { get; set; }

    }
    public class MapBaoCaoTinhHinhSuCoLuoiDienJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<BaoCaoTinhHinhSuCoLuoiDienJson> data { get; set; }
    }

    public class BaoCaoChiSoDoTinCayLuoiDienPhanPhoiJson
    {
        public string TenDonVi { get; set; }
        public int TongSoKhachHang { get; set; }
        public int TongSoLanMatDienThoangQua { get; set; }
        public int TongSoKHBiMatDienThoang { get; set; }
        public double MAIFI { get; set; }
        public int TongSoLanMatDienKeoDai { get; set; }
        public int TongSoKhachHangBiMatKeoDai { get; set; }
        public double TongThoiGianMatDienCuaKH { get; set; }
        public double SAIDI { get; set; }
        public double SAIFI { get; set; }
        public double CAIDI { get; set; }
        public double SLLKN { get; set; }
        public double SSLKVoiKeHoachNam { get; set; }
    }
    public class MapBaoCaoChiSoDoTinCayLuoiDienPhanPhoiJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<BaoCaoChiSoDoTinCayLuoiDienPhanPhoiJson> data { get; set; }
    } 
    public class PMIS_GetAll_THDTCLuoiDienPhanPhoiJson
    {
        public string TenDonVi { get; set; }
        public int TongSoKhachHang { get; set; }
        public int TongSoLanMatDienThoangQua { get; set; }
        public int TongSoKHBiMatDienThoang { get; set; }
        public double MAIFI { get; set; }
        public int TongSoLanMatDienKeoDai { get; set; }
        public int TongSoKhachHangBiMatKeoDai { get; set; }
        public double TongThoiGianMatDienCuaKH { get; set; }
        public double SAIDI { get; set; }
        public double SAIFI { get; set; }
        public double CAIDI { get; set; }
        public double SLLKN { get; set; }
        public double SSLKVoiKeHoachNam { get; set; }

    }
    public class MapPMIS_GetAll_THDTCLuoiDienPhanPhoiJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<PMIS_GetAll_THDTCLuoiDienPhanPhoiJson> data { get; set; }
    }
    public class TongHopSuCoVaSuatSuCoDuongDayVaTBAJson 
    {
        public string INDEX1 { get; set; }
        public string INDEX2 { get; set; }
        public string INDEX3 { get; set; }
        public string INDEX4 { get; set; }
        public string INDEX5 { get; set; }
        public string INDEX6 { get; set; }
        public string INDEX7 { get; set; }
        public string INDEX8 { get; set; }
        public string INDEX9 { get; set; }
        public string INDEX10 { get; set; }
        public string INDEX11 { get; set; }
        public string INDEX12 { get; set; }
        public string INDEX13 { get; set; }
        public string INDEX14 { get; set; }
        public string INDEX15 { get; set; }
        public string INDEX16 { get; set; }
        public string INDEX17 { get; set; }
        public string INDEX18 { get; set; }
        public string INDEX19 { get; set; }
        public string INDEX20 { get; set; }
        public string INDEX21 { get; set; }
        public string INDEX22 { get; set; }
        public string INDEX23 { get; set; }
        public string INDEX24 { get; set; }
        public string INDEX25 { get; set; }
        public string INDEX26 { get; set; }
        public string INDEX27 { get; set; }
        public string INDEX28 { get; set; }
        public string INDEX29 { get; set; }
        public string INDEX30 { get; set; }
        public string INDEX31 { get; set; }
        public string INDEX32 { get; set; }
        public string INDEX33 { get; set; }
        public string INDEX34 { get; set; }
        public string INDEX35 { get; set; }
        public string INDEX36 { get; set; }
        public string INDEX37 { get; set; }
        public string INDEX38 { get; set; }
        public string INDEX39 { get; set; }
        public string INDEX40 { get; set; }
        public string INDEX41 { get; set; }
        public string INDEX42 { get; set; }
        public string INDEX43 { get; set; }
        public string INDEX44 { get; set; }
        public string INDEX45 { get; set; }
        public string INDEX46 { get; set; }
        public string INDEX47 { get; set; }
        public string INDEX48 { get; set; }
        public string INDEX49 { get; set; }
        public string INDEX50 { get; set; }
        public string INDEX51 { get; set; }
        public string INDEX52 { get; set; }
        public string INDEX53 { get; set; }
    }
    public class MapTongHopSuCoVaSuatSuCoDuongDayVaTBAJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<TongHopSuCoVaSuatSuCoDuongDayVaTBAJson> data { get; set; }
    }
    public class BaoCaoTinhHinhSuCoLuoiDienTrungHaApJson
    {
        public string TenDonVi { get; set; }
        public string TenThietBi { get; set; }
        public double CapDienAp { get; set; }
        public string NgayXHSC { get; set; }
        public string GioXHSC { get; set; }
        public string NgayKPSC { get; set; }
        public string GioKPSC { get; set; }
        public string KD { get; set; }
        public string TQ { get; set; }
        public string TBDC { get; set; }
        public string CachDien { get; set; }
        public string MoiNoi { get; set; }
        public string TBPhu { get; set; }
        public string KhacCap { get; set; }
        public string MayBienAp { get; set; }
        public string KhacTram { get; set; }
        public string DienBienSuCo { get; set; }
        public string NguyenNhan { get; set; }
        public string ThoiGianGianDoanCungCapDien { get; set; }
        public string ThoiGianSuCo { get; set; }
        public string KhuVucBiNgungGiamCungCapDien { get; set; }
        public string SoKhachHangBiNgungGiamCungCapDien { get; set; }
        public string CongXuatTaiTruocSuCo { get; set; }
        public string DienNangKhongCungCapDuoc { get; set; }
    }
    public class MapBaoCaoTinhHinhSuCoLuoiDienTrungHaApJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<BaoCaoTinhHinhSuCoLuoiDienTrungHaApJson> data { get; set; }
    }
    public class DanhMucHoSoJson
    {
        public string KyHieuHoSo { get; set; }
        public string TenHoSo { get; set; }
        public string ThoiHanBaoQuan { get; set; }
        public string NguoiLap { get; set; }
        public string GhiChu { get; set; }
    }
    public class MapDanhMucHoSoJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
    }
    public class TongHopBanDienCuaDVJson : BaseJsonBC
    {
        public TongHopBanDienCuaDVJson() { }

        public TongHopBanDienCuaDVJson(string tenDonVi)
        {
            TenDonVi = tenDonVi;
            DienNangTP = "0";
            DTTongCong = "0";
            //todo
            DTTienDien = "0";
            DTTienCSPK = "0";
            GiaBinhQuan = "0";
            ThueTongCong = "0";
            ThueTienDien = "0";
            ThueTienCSPK = "0";
            DTvaThueGTGT = "0";

        }

        public string TenDonVi { get; set; }
        public string DienNangTP { get; set; }
        public string DTTongCong { get; set; }
        public string DTTienDien { get; set; }
        public string DTTienCSPK { get; set; }

        public string GiaBinhQuan { get; set; }
        public string ThueTongCong { get; set; }
        public string ThueTienDien { get; set; }
        public string ThueTienCSPK { get; set; }
        public string DTvaThueGTGT { get; set; }

    }
    public class MapTongHopBanDienCuaDVJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data1 { get; set; }
        public string data2 { get; set; }
    }
    public class CMIS_TongHopSoHopDongMBDCacDVJson
    {

        public string TenDonVi { get; set; }
        public string DienNangTP { get; set; }
        public string DTTongCong { get; set; }
        public string DTTienDien { get; set; }
        public string DTTienCSPK { get; set; }

        public string GiaBinhQuan { get; set; }
        public string ThueTongCong { get; set; }
        public string ThueTienDien { get; set; }
        public string ThueTienCSPK { get; set; }
        public string DTvaThueGTGT { get; set; }
        public string LuyKeNam { get; set; }
        public string GiaBQ { get; set; }
        public string SoSanhLKVSCK { get; set; }
        public string SoSanhLKVSKHG { get; set; }

    }
    public class MapCMIS_TongHopSoHopDongMBDCacDVJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data1 { get; set; }
        public string data2 { get; set; }
    }
    public class MucLucVanBanTaiLieuJson
    {
        public string STT { get; set; }
        public string SoDen { get; set; }
        public string NgayDen { get; set; }
        public string TacGiaVanBan { get; set; }
        public string SoKyHieu { get; set; }
        public string NgayVanBan { get; set; }
        public string TrichYeu { get; set; }
        public string ToSo { get; set; }
        public string GhiChu { get; set; }
    }
    public class MapMucLucVanBanTaiLieuJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
    }
    public class SoVanBanDiJson
    {
        public string SoKyHieuVanBan { get; set; }
        public string NgayThangVanBan { get; set; }
        public string TenLoaiVaTrichYeuVanBan { get; set; }
        public string NguoiKy { get; set; }
        public string NoiNhanVanBan { get; set; }
        public string GhiChu { get; set; }
    }
    public class MapSoVanBanDiJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<SoVanBanDiJson> data { get; set; }
    }
    public class MucLucHoSoJson
    {
        public string SoKyHieu { get; set; }
        public string NgayVanBan { get; set; }
        public string TrichYeuVanBan { get; set; }
        public string TacGia { get; set; }
        public string NgayThemTaiLieu { get; set; }
        public string GhiChu { get; set; }
    }
    public class MapMucLucHoSoJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
    }
    public class DangKyGiuSoVanBanDiJson
    {
        public string SoDangKyGiu { get; set; }
        public string SoVanBan { get; set; }
        public string NguoiDangKy { get; set; }
        public string BanPhong { get; set; }
        public string ThoiGianDangKy { get; set; }
        public string TinhTrang { get; set; }
        public string GhiChu { get; set; }
    }
    public class MapDangKyGiuSoVanBanDiJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
    }
    public class TTDienNangDungTTvaPPCuaDVJson : BaseJsonBC
    {
        public TTDienNangDungTTvaPPCuaDVJson() { }

        public TTDienNangDungTTvaPPCuaDVJson(string tenDonVi)
        {
            TenDonVi = tenDonVi;
            DNNTongNhan = "0";
            DNNNhanBanThang = "0";
            //todo
            DNNNhanKV = "0";
            DienNangGiao = "0";
            DienNangGiaoNgay = "0";
            DienThuongPham = "0";
            TTVaPPKWH = "0";
            TTVaPPPhanTram = "0";
            
        }

        public string TenDonVi { get; set; }
        public string DNNTongNhan { get; set; }
        public string DNNNhanBanThang { get; set; }
        public string DNNNhanKV { get; set; }
        public string DienNangGiao { get; set; }

        public string DienNangGiaoNgay { get; set; }
        public string DienThuongPham { get; set; }
        public string TTVaPPKWH { get; set; }
        public string TTVaPPPhanTram { get; set; }
        public string LuyKeNam { get; set; }
        public string SSluyKeNamvsCK { get; set; }
        public string SSluyKeNamvsKHG { get; set; }

    }
    public class MapTTDienNangDungTTvaPPCuaDVJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data1 { get; set; }
        public string data2 { get; set; }
    } 

    public class TongHopSoCongToCuaDVJson: BaseJsonBC
    {
        public TongHopSoCongToCuaDVJson() {}

        public TongHopSoCongToCuaDVJson(string tenDonVi)
        {
            TenDonVi = tenDonVi;
            TSToanDonVi = "0";
            TSNoiBo1Pha = "0";
            //todo
            TSNoiBo1Pha = "0";
            TSNoiBo3Pha = "0";
            TSBanDien1Pha = "0";
            TSBanDien3Pha = "0";
            CTBanDien1PhaCoKhi = "0";
            CTBanDien1PhaDienTu1Gia = "0";
            CTBanDien1PhaDienTuNhieuGia = "0";
            CTBanDien3PhaCoKhi = "0";
            CTBanDien3PhaDienTu1Gia = "0";
            CTBanDien3PhaDienTuNhieuGia = "0";
        }

        public string TenDonVi { get; set; }
        public string TSToanDonVi { get; set; }
        public string TSNoiBo1Pha { get; set; }
        public string TSNoiBo3Pha { get; set; }
        public string TSBanDien1Pha { get; set; }
        public string TSBanDien3Pha { get; set; }
        public string CTBanDien1PhaCoKhi { get; set; }
        public string CTBanDien1PhaDienTu1Gia { get; set; }
        public string CTBanDien1PhaDienTuNhieuGia { get; set; }
        public string CTBanDien3PhaCoKhi { get; set; }
        public string CTBanDien3PhaDienTu1Gia { get; set; }
        public string CTBanDien3PhaDienTuNhieuGia { get; set; }

    }
    public class MapTongHopSoCongToCuaDVJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
    }
    public class TongHopSoHDMuaBanDienCuaDVJson : BaseJsonBC
    {
        public TongHopSoHDMuaBanDienCuaDVJson() { }

        public TongHopSoHDMuaBanDienCuaDVJson(string tenDonVi)
        {
            TenDonVi = tenDonVi;
            TongSo = "0";
            HDSinhHoat = "0";
            //todo
            HDNgoaiSinhHoat = "0";
            ThanhPhanPTNNLNTS = "0";
            ThanhPhanPTCNXD = "0";
            ThanhPhanPTKDV = "0";
            ThanhPhanPTQLTD = "0";
            ThanhPhanPTKhac = "0";
        }

        public string TenDonVi { get; set; }
        public string TongSo { get; set; }
        public string HDSinhHoat { get; set; }
        public string HDNgoaiSinhHoat { get; set; }
        public string ThanhPhanPTNNLNTS { get; set; }
        public string ThanhPhanPTCNXD { get; set; }

        public string ThanhPhanPTKDV { get; set; }
        public string ThanhPhanPTQLTD { get; set; }
        public string ThanhPhanPTKhac { get; set; }

    }
    public class MapTongHopSoHDMuaBanDienCuaDVJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
    }
    public class ChiTietBanDienTheoTTPhuTaiJson : BaseJsonBC
    {

        public ChiTietBanDienTheoTTPhuTaiJson() { }

        public ChiTietBanDienTheoTTPhuTaiJson(string tenDonVi)
        {
            ThanhPhanPhuTai = tenDonVi;
            ThangBCTongSanLuong = "0";
            ThangBCBieu1 = "0";
            //todo
            ThangBCBieu2 = "0";
            ThangBCBieu3 = "0";
            ThangBCBanDien1LoaiG = "0";
            ThangBCTienDien = "0";
            LuyKeNTongSanLuong = "0";
            LuyKeNBieu1 = "0";
            LuyKeNBieu2 = "0";
            LuyKeNBieu3 = "0";
            LuyKeNBanDien1LoaiG = "0";
            GiaTriThanhPhanPhuTai = "0";
            TyLeThanhPhanPhuTai = "0";
            LuyKeNTienDien = "0";
        }

        public string ThanhPhanPhuTai { get; set; }
        public string ThangBCTongSanLuong { get; set; }
        public string ThangBCBieu1 { get; set; }
        public string ThangBCBieu2 { get; set; }
        public string ThangBCBieu3 { get; set; }
        public string ThangBCBanDien1LoaiG { get; set; }
        public string ThangBCTienDien { get; set; }

        public string LuyKeNTongSanLuong { get; set; }
        public string LuyKeNBieu1 { get; set; }
        public string LuyKeNBieu2 { get; set; }
        public string LuyKeNBieu3 { get; set; }
        public string LuyKeNBanDien1LoaiG { get; set; }
        public string LuyKeNTienDien { get; set; }
        public string GiaTriThanhPhanPhuTai { get; set; }
        public string TyLeThanhPhanPhuTai { get; set; }


    }
    public class MapChiTietBanDienTheoTTPhuTaiJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data1 { get; set; }
        public string data2 { get; set; }
    } 
    public class CMIS_ChiTietBanDienTheoTPPTJson
    {

        public string ThanhPhanPhuTai { get; set; }
        public string ThangBCTongSanLuong { get; set; }
        public string ThangBCBieu1 { get; set; }
        public string ThangBCBieu2 { get; set; }
        public string ThangBCBieu3 { get; set; }
        public string ThangBCBanDien1LoaiG { get; set; }
        public string ThangBCTienDien { get; set; }

        public string LuyKeNTongSanLuong { get; set; }
        public string LuyKeNBieu1 { get; set; }
        public string LuyKeNBieu2 { get; set; }
        public string LuyKeNBieu3 { get; set; }
        public string LuyKeNBanDien1LoaiG { get; set; }
        public string LuyKeNTienDien { get; set; }
        public string GTTPPTLKN { get; set; }
        public string TiLePTTPPT { get; set; }



    }
    public class MapCMIS_ChiTietBanDienTheoTPPTJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data1 { get; set; }
        public string data2 { get; set; }
    }
    public class ChiTietBanDienTheoNganhNgheJson : BaseJsonBC
    {
        public ChiTietBanDienTheoNganhNgheJson() { }
        public ChiTietBanDienTheoNganhNgheJson(String tenKm)
        {
            TenNN = tenKm;
            SoHopDongMBD = "0";
            DienThuongPham = "0";
        }

        public string MaNN { get; set; }
        public string TenNN { get; set; }
        public string SoHopDongMBD { get; set; }
        public string DienThuongPham { get; set; }


    }
    public class MapChiTietBanDienTheoNganhNgheJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
    }
    public class BaoCaoSanLuongTietKiemCacDVTTJson : BaseJsonBC
    {
        public BaoCaoSanLuongTietKiemCacDVTTJson() { }
        public BaoCaoSanLuongTietKiemCacDVTTJson(String tenKm)
        {
            TenDonVi = tenKm;
            HCSN = "0";
            CQCS = "0";
            SH = "0";
            KDDV = "0";
            SXCN = "0";
            TongCong = "0";
            LuyKeHCSN = "0";
            LuyKeCQCS = "0";
            LuyKeSH = "0";
            LuyKeKDDV = "0";
            LuyKeSXCN = "0";
            LuyKeTongCong = "0";
        }



        public string TenDonVi { get; set; }
        public string HCSN { get; set; }
        public string CQCS { get; set; }
        public string SH { get; set; }
        public string KDDV { get; set; }
        public string SXCN { get; set; }
        public string TongCong { get; set; }
        public string LuyKeHCSN { get; set; }
        public string LuyKeCQCS { get; set; }
        public string LuyKeSH { get; set; }
        public string LuyKeKDDV { get; set; }
        public string LuyKeSXCN { get; set; }
        public string LuyKeTongCong { get; set; }



    }
    public class dataResponseSuCoDuongDay
    {

    }
    public class MapBaoCaoSanLuongTietKiemCacDVTTJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data1 { get; set; }
        public string data2 { get; set; }
    }
    // Linh vuc Quan tri 
    public class BaoCaoTrinhDoVienChucCMNVJson : BaseJsonBC
    {

        public BaoCaoTrinhDoVienChucCMNVJson() { }
        public BaoCaoTrinhDoVienChucCMNVJson(String tenKm)
        {
            PhanTheoNhomNganh = tenKm;
            TongSo = "0";
            TDTienSi = "0";
            TDThacSi = "0";
            TDDaiHoc = "0";
            TDCaoDang = "0";
            TDTrungCap = "0";
            DangVien = "0";
           

        }

        public string PhanTheoNhomNganh { get; set; }
        public string MaSo { get; set; }
        public string TongSo { get; set; }
        public string TDTienSi { get; set; }
        public string TDThacSi { get; set; }
        public string TDDaiHoc { get; set; }
        public string TDCaoDang { get; set; }
        public string TDTrungCap { get; set; }
        public string DangVien { get; set; }

    }
    public class MapBaoCaoTrinhDoVienChucCMNVJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<BaoCaoTrinhDoVienChucCMNVJson> data { get; set; }
    }
    public class BCCoCauLaoDongJson : BaseJsonBC
    {
        public BCCoCauLaoDongJson() { }
        public BCCoCauLaoDongJson(String tenKm)
        {
            ChucDanh = tenKm;
            TongSoLDCMDenCuoiKiBC = "0";
            DangVien = "0";
            LDNu = "0";
            LDNNuocNgoai = "0";
            DanTocItNguoi = "0";
            TDuoi30 = "0";
            Tuoi30D39 = "0";
            Tuoi40D49 = "0";
            Tuoi50D59 = "0";
            TuoiTren60 = "0";
            TienSiKT = "0";
            TienSiKTXH = "0";
            ThacSiKT = "0";
            ThacSiKTXH = "0";
            DaiHocKT = "0";
            DaiHocKTXH = "0";
            CaoDangKT = "0";
            CaoDangKTXH = "0";
            TrungCapKT = "0";
            TrungCapKTXH = "0";
            CongNhanKT = "0";
            DaoTaoNgheNganHan = "0";
            ChuaQuaDaoTao = "0";
            TrenDaiHoc = "0";
            CuNhan = "0";
            CaoCap = "0";
            TrungCap = "0";

        }
        public string STT { get; set; }
        public string ChucDanh { get; set; }
        public string MaSo { get; set; }
        public string TongSoLDCMDenCuoiKiBC { get; set; }
        public string DangVien { get; set; }
        public string LDNu { get; set; }
        public string LDNNuocNgoai { get; set; }
        public string DanTocItNguoi { get; set; }
        public string TDuoi30 { get; set; }
        public string Tuoi30D39 { get; set; }
        public string Tuoi40D49 { get; set; }
        public string Tuoi50D59 { get; set; }
        public string TuoiTren60 { get; set; }
        public string TienSiKT { get; set; }
        public string TienSiKTXH { get; set; }
        public string ThacSiKT { get; set; }
        public string ThacSiKTXH { get; set; }
        public string DaiHocKT { get; set; }
        public string DaiHocKTXH { get; set; }
        public string CaoDangKT { get; set; }
        public string CaoDangKTXH { get; set; }
        public string TrungCapKT { get; set; }
        public string TrungCapKTXH { get; set; }
        public string CongNhanKT { get; set; }
        public string DaoTaoNgheNganHan { get; set; }
        public string ChuaQuaDaoTao { get; set; }
        public string TrenDaiHoc { get; set; }
        public string CuNhan { get; set; }
        public string CaoCap { get; set; }
        public string TrungCap { get; set; }

    }
    public class MapBCCoCauLaoDongJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<BCCoCauLaoDongJson> data { get; set; }
    }
    public class BaoCaoChatLuongCongNhanKTJson : BaseJsonBC
    {

        public BaoCaoChatLuongCongNhanKTJson() { }
        public BaoCaoChatLuongCongNhanKTJson(String tenKm)
        {
            DanhMucNghe = tenKm;
            TongSoCoMat = "0";
            NuCoMat = "0";
            BTThangLuongI = "0";
            BTThangLuongII = "0";
            BTThangLuongIII = "0";
            BTThangLuongIV = "0";
            BTThangLuongV = "0";
            BTThangLuongVI = "0";
            BTThangLuongVII = "0";
            BTBangLuongI = "0";
            BTBangLuongII = "0";
            BTBangLuongIII = "0";
            BTBangLuongIV = "0";
            BTBangLuongV = "0";
        }

        public string STT { get; set; }
        public string DanhMucNghe { get; set; }
        public string MaSo { get; set; }
        public string TongSoCoMat { get; set; }
        public string NuCoMat { get; set; }
        public string BTThangLuongI { get; set; }
        public string BTThangLuongII { get; set; }
        public string BTThangLuongIII { get; set; }
        public string BTThangLuongIV { get; set; }
        public string BTThangLuongV { get; set; }
        public string BTThangLuongVI { get; set; }
        public string BTThangLuongVII { get; set; }
        public string BTBangLuongI { get; set; }
        public string BTBangLuongII { get; set; }
        public string BTBangLuongIII { get; set; }
        public string BTBangLuongIV { get; set; }
        public string BTBangLuongV { get; set; }


    }

    public class MapBaoCaoChatLuongCongNhanKTJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<BaoCaoChatLuongCongNhanKTJson> data { get; set; }
    }
    public class BaoCaoLDThuNhapJson : BaseJsonBC
    {
        public BaoCaoLDThuNhapJson() { }
        public BaoCaoLDThuNhapJson(String tenKm)
        {
            NganhNgheKT = tenKm;
            TongSoLDDauKy = "0";
            LDNuDauKy = "0";
            LDDongBHXKDauKy = "0";
            LDHDDuoi6ThangDauKy = "0";
            TongSoLDCuoiKy = "0";
            LDNuCuoiKy = "0";
            LDDongBHXKCuoiKy = "0";
            LDHDDuoi6ThangCuoiKy = "0";
            LDTangTrongKy = "0";
            LDGiamTrongKy = "0";
            LDKhongCoNhuCauSudung = "0";
            SoLDBinhQuanTrongKiBC = "0";
            TongThuNhap = "0";
            TienLuongVaCacKhoanTCL = "0";
            BHXTraThayLuong = "0";
            CacThuNhapKhacKTinhVaoCPSXKD = "0";
            ThuNhapBinhQuan = "0";
            TienLuongBinhQuan = "0";
            DongGopCuaDVveBHXHYTTNKPCD = "0";
        }

        public string STT { get; set; }
        public string NganhNgheKT { get; set; }
        public string MaSo { get; set; }
        public string TongSoLDDauKy { get; set; }
        public string LDNuDauKy { get; set; }
        public string LDDongBHXKDauKy { get; set; }
        public string LDHDDuoi6ThangDauKy { get; set; }
        public string TongSoLDCuoiKy { get; set; }
        public string LDNuCuoiKy { get; set; }
        public string LDDongBHXKCuoiKy { get; set; }
        public string LDHDDuoi6ThangCuoiKy { get; set; }
        public string LDTangTrongKy { get; set; }
        public string LDGiamTrongKy { get; set; }
        public string LDKhongCoNhuCauSudung { get; set; }
        public string SoLDBinhQuanTrongKiBC { get; set; }
        public string TongThuNhap { get; set; }
        public string TienLuongVaCacKhoanTCL { get; set; }
        public string BHXTraThayLuong { get; set; }
        public string CacThuNhapKhacKTinhVaoCPSXKD { get; set; }
        public string ThuNhapBinhQuan { get; set; }
        public string TienLuongBinhQuan { get; set; }
        public string DongGopCuaDVveBHXHYTTNKPCD { get; set; }


    }
    public class MapBaoCaoLDThuNhapJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<BaoCaoLDThuNhapJson> data { get; set; }
    }
    public class BaoCaoTangGiamLaoDongJson
    {

        public string STT { get; set; }
        public string ChiTieu { get; set; }
        public string MaSo { get; set; }
        public string TongSoLDPhatSinh { get; set; }
        public string LDNguoiNuocNgoaiPhatSinh { get; set; }
        public string LDNuPhatSinh { get; set; }
        public string SanXuatKinhDoanhPhatSinh { get; set; }
        public string TongSoLDTuDauNam { get; set; }
        public string LDNguoiNuocNgoaiTuDauNam { get; set; }
        public string LDNuTuDauNam { get; set; }
        public string SanXuatKinhDoanhTuDauNam { get; set; }
        public string GhiChu { get; set; }


    }
    public class MapBaoCaoTangGiamLaoDongJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<BaoCaoTangGiamLaoDongJson> data { get; set; }
    }
    public class BaoCaoUocLaoDongJson
    {
        public string TenDoanhNghiep { get; set; }
        public string DiaChi { get; set; }
        public string DienThoai { get; set; }
        public string Email { get; set; }
        public string LoaiNghanhSXKinhDoanh { get; set; }
        public string LoaiHinhKinhTeDoanhNghiep { get; set; }
        public string TenChiTieu { get; set; }
        public string MaSo { get; set; }
        public string TongSoDauKy { get; set; }
        public string LDNuDauKy { get; set; }
        public string TongSoCuoiKy { get; set; }
        public string LDNuCuoiKy { get; set; }
        public string PhatSinhTrongKy { get; set; }
    }
    public class MapBaoCaoUocLaoDongJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<BaoCaoUocLaoDongJson> data { get; set; }
    }
    public class LaoDong
    {

        public string TenChiTieu { get; set; }
        public string MaSo { get; set; }
        public double TongSoDauKy { get; set; }
        public double LDNuDauKy { get; set; }
        public double TongSoCuoiKy { get; set; }
        public double LDNuCuoiKy { get; set; }

    }
    public class MapLaoDong
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<LaoDong> data { get; set; }
    }
    public class ThuNhapNguoiLaoDongVaDongGop
    {

        public string TenChiTieu { get; set; }
        public string MaSo { get; set; }
        public double PhatSinhTrongKy { get; set; }

    }
    public class MapThuNhapNguoiLaoDongVaDongGop
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<ThuNhapNguoiLaoDongVaDongGop> data { get; set; }
    }


    //Báo Cáo Tài Chính
    public class BaoCaoSanLuongDienJson
    {
        public string DienGia { get; set; }
        public string MaSo { get; set; }
        public string DonViTinh { get; set; }
        public string KeHoachQuy { get; set; }
        public string ThucHienQuy { get; set; }
        public string TyLeQuy { get; set; }
        public string KeHoachLuyKe { get; set; }
        public string ThucHienLuyKe { get; set; }
        public string TyLeLyKe { get; set; }
    }
    public class MapBaoCaoSanLuongDienJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
    }

    public class BaoCaoKetQuaSanXuatKinhDoanhJson
    {
        public string DienGia { get; set; }
        public int MaSo { get; set; }
        public string ThuyetMinh { get; set; }
        public string NamNayQuy { get; set; }
        public string NamTruocQuy { get; set; }
        public string NamNayLuyKe { get; set; }
        public string NamTruocLuyKe { get; set; }
    }
    public class MapBaoCaoKetQuaSanXuatKinhDoanhJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
    }
    public class NguonVonDauTuJson
    {
        public string NguonVon { get; set; }
        public string SoDuDauNam { get; set; }
        public string KyBaoCaoTang { get; set; }
        public string LuyKeTuDauNamTang { get; set; }
        public string LuyKeTuKhoiCongTang { get; set; }
        public string KyBaoCaoGiam { get; set; }
        public string LuyKeTuDauNamGiam { get; set; }
        public string LuyKeTuKhoiCongGiam { get; set; }
        public string SoDuCuoiKy { get; set; }
    }
    public class MapNguonVonDauTuJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
    }
    public class ChiTietNguonVonDauTuXayDungJson
    {
        public string STT { get; set; }
        public string CongTrinhHangMucCongTrinh { get; set; }
        public string SETUP_CODE { get; set; }
        public string LINE_DESCRIPTION { get; set; }
        public string SoDuDauNam { get; set; }
        public string QuyBaoCaoTang { get; set; }
        public string LuyKeTuDauNamTang { get; set; }
        public string LuyKeTuKhoiCongTang { get; set; }
        public string QuyBaoCaoGiam { get; set; }
        public string LuyKeTuDauNamGiam { get; set; }
        public string LuyKeTuKhoiCongGiam { get; set; }
        public string SoDuCuoiKy { get; set; }
    }
    public class MapChiTietNguonVonDauTuXayDungJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
    }
    public class BCCongTrinhSuaChuaLonHTJson
    {
        public string TenCongTrinh { get; set; }
        public string MaCongTrinh { get; set; }
        public string DuToanDuocDuyet { get; set; }
        public string KHDuyetNamNay { get; set; }
        public string DoDangNamTruoc { get; set; }
        public string DaChiNamNay { get; set; }
        public string VatLieuLKDC { get; set; }
        public string NhanCongLKDC { get; set; }
        public string MayThiCongLKDC { get; set; }
        public string ChiPhiKhacLKDC { get; set; }
        public string CongLKDC { get; set; }
        public string GiaTriQuyetToanDuocDuyet { get; set; }
    }
    public class MapBCCongTrinhSuaChuaLonHTJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
    }
    public class TongHopChiPhiSanXuatKinhDoanhDienJson
    {
        public string DienGiai { get; set; }
        public string MaSo { get; set; }
        public string TongQuy { get; set; }
        public string GiaThanhDonViQuy { get; set; }
        public string TyTrongQuy { get; set; }
        public string TongLuyKe { get; set; }
        public string GiaThanhDonViLuyKe { get; set; }
        public string TyTrongLuyKe { get; set; }
    }
    public class MapTongHopChiPhiSanXuatKinhDoanhDienJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
    }
    public class BCCongTinhSuaChuaLonDoDangJson
    {
        public string TenCongTrinh { get; set; }
        public string MaCongTrinh { get; set; }
        public string DuToanDuocDuyet { get; set; }
        public string KHDuyetNamNay { get; set; }
        public string DoDangNamTruoc { get; set; }
        public string DaChiNamNay { get; set; }
        public string VatLieuLKDC { get; set; }
        public string NhanCongLKDC { get; set; }
        public string MayThiCongLKDC { get; set; }
        public string ChiPhiKhacLKDC { get; set; }
        public string CongLKDC { get; set; }
        public string GhiChu { get; set; }
    }
    public class MapBCCongTinhSuaChuaLonDoDangJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
    }
    public class NguonVonVayJson
    {
        public string TenCongTrinh { get; set; }
        public string DoiTacVay { get; set; }
        public string DonViTinh { get; set; }
        public string NguyenTe { get; set; }
        public string QuyDoiVND { get; set; }
        public string VayBangVND { get; set; }
        public string SoDuDauNam { get; set; }
        public string KyBaoCaoTang { get; set; }
        public string TuDauNamDenKyBaoCaoTang { get; set; }
        public string LuyKeBatDauRutVonTang { get; set; }
        public string KyBaoCaoGiam { get; set; }
        public string TuDauNamDenKyBaoCaoGiam { get; set; }
        public string LuyKeBatDauRutVonGiam { get; set; }
        public string TongSo { get; set; }
        public string NoDenHanPhaiTra { get; set; }


    }
    public class MapNguonVonVayJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
    }
    public class BCCongTrinhThuocNguonVonSuaChuaLonJson
    {
        public string TenCongTrinh { get; set; }
        public string MaCongTrinh { get; set; }
        public string DuAnDuocDuyet { get; set; }
        public string KHDuyetNamNay { get; set; }
        public string SoDuDauNam { get; set; }
        public string DaTriNamNayTrongKyBaoCao { get; set; }
        public string VatLieuTuDauNam { get; set; }
        public string NhanCongTuDauNam { get; set; }
        public string MayThiCongTuDauNam { get; set; }
        public string KhacTuDauNam { get; set; }
        public string CongTuDauNam { get; set; }
        public string VatLieuLKDC { get; set; }
        public string NhanCongLKDC { get; set; }
        public string MayThiCongLKDC { get; set; }
        public string KhacLKDC { get; set; }
        public string CongLKDC { get; set; }
        public string GhiChu { get; set; }
        public string TongHopChung { get; set; }
        public string SoSanhPhanTram { get; set; }



    }
    public class MapBCCongTrinhThuocNguonVonSuaChuaLonJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
    } 
    public class ERP_BCCongTrinhSCLJson
    {
        public string TenCongTrinh { get; set; }
        public string MaCongTrinh { get; set; }
        public string DuAnDuocDuyet { get; set; }
        public string KHDuyetNamNay { get; set; }
        public string SoDuDauNam { get; set; }
        public string DaTriNamNayTrongKyBaoCao { get; set; }
        public string VatLieuTuDauNam { get; set; }
        public string NhanCongTuDauNam { get; set; }
        public string MayThiCongTuDauNam { get; set; }
        public string KhacTuDauNam { get; set; }
        public string CongTuDauNam { get; set; }
        public string VatLieuLKDC { get; set; }
        public string NhanCongLKDC { get; set; }
        public string MayThiCongLKDC { get; set; }
        public string KhacLKDC { get; set; }
        public string CongLKDC { get; set; }
        public string GhiChu { get; set; }
        public string TongHopChung { get; set; }
        public string SoSanhPhanTram { get; set; }


    }
    public class MapERP_BCCongTrinhSCLJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
    }
    public class CTHMCongTrinhHoanThanhBanGiaoJson
    {
        public string STT { get; set; }
        public string TenHangMucCongTrinh { get; set; }
        public string NgayKhoiCong { get; set; }
        public string NgayHoanThanhBGDV { get; set; }
        public string DonViSuDungBGDV { get; set; }
        public string CoQuanQuyetDinhDauTu { get; set; }
        public string TongDuToanDuocDuyet { get; set; }
        public string ChiPhiDauTuDaHoanThanhChuaPheDuyetQTDN { get; set; }
        public string PhatSinhTrongKyChuaPheDuyetQT { get; set; }
        public string LuyKeTuDauNamDenCuoiKyChuaPheDuyetQT { get; set; }
        public string PhatSinhTrongKyDaPheDuyetQT { get; set; }
        public string LuyKeTuDauNamDenCuoiKyDaPheDuyetQT { get; set; }
        public string SoDuCPTHDauTuDaHoanThanhBanGiaoChuaPheDuyetQTCK { get; set; }


    }
    public class MapCTHMCongTrinhHoanThanhBanGiaoJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
    }
    public class ThucHienDauTuXayDungCoBanJson
    {
        public string STT { get; set; }
        public string CoCauChiPhiDauTu { get; set; }
        public string KeHoachNam { get; set; }
        public string ThucHienDauTuKy { get; set; }
        public string KyBaoCaoThucHienDauTu { get; set; }
        public string LuyKeTuDauNamThucHienDauTu { get; set; }
        public string LuyKeTuKhoiCongThucHienDauTu { get; set; }
        public string KyBaoCaoBanGiao { get; set; }
        public string LuyKeTuDauNamBanGiao { get; set; }
        public string LuyKeTuKhoiCongBanGiao { get; set; }
        public string ThucHienDauTuCOnLaiCuoiKy { get; set; }

    }
    public class MapThucHienDauTuXayDungCoBanJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
    }
    public class ThucHienDauTuTheoCongTrinhHangMucCongTrinhJson
    {
        public string DISPLAY_ATT1 { get; set; }
        public string CongTrinhHangMucCT { get; set; }
        public string TongDuDoan { get; set; }
        public string SoDuDauNamThucHien { get; set; }
        public string TongSo { get; set; }
        public string XayDung { get; set; }
        public string LapDat { get; set; }
        public string ThietBi { get; set; }
        public string ChiPhiBoiThuong { get; set; }
        public string ChiPhiQuanLy { get; set; }
        public string ChiPhiTuVanDauTuXayDung { get; set; }
        public string TongSoKhac { get; set; }
        public string ChiPhiLaiVay { get; set; }
        public string ThucHienDauTuTang { get; set; }
        public string ThucHienDauTuGiam { get; set; }
        public string SoDuCuoiKyThucHien { get; set; }
    }
    public class MapThucHienDauTuTheoCongTrinhHangMucCongTrinhJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
    }
    public class NhatKyMatDienJson
    {
        public string MaLDM { get; set; }
        public string MaLDD { get; set; }
        public string DonVi { get; set; }
        public string DonViTinhDoTinCay { get; set; }
        public string DinhDanh { get; set; }
        public string TenPhanTuCat { get; set; }
        public string TenPhanTuDong { get; set; }
        public string NgayMatDien { get; set; }
        public string NgayCoDien { get; set; }
        public string IDLich { get; set; }
        public string SoPhutMatDien { get; set; }
        public int SoKH { get; set; }
        public string MaNguyenNhan { get; set; }
        public string NguyenNhan { get; set; }
        public string KhuVuc { get; set; }
        public string LyDo { get; set; }
        public string GhiChu { get; set; }
        public string NguoiCat { get; set; }
        public string NguoiDong { get; set; }
        public string NgayThaoTacCat { get; set; }
        public string NgayThaoTacDong { get; set; }
        public double MAIFI { get; set; }
        public double SAIFI { get; set; }
        public double SAIDI { get; set; }

    }
    public class MapNhatKyMatDienJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<NhatKyMatDienJson> data { get; set; }
    }
    public class ChiTietBanDienTheoDTGJson : BaseJsonBC
    {
        public ChiTietBanDienTheoDTGJson() { }
        public ChiTietBanDienTheoDTGJson(String tenKm)
        {
            MaDT = tenKm;
            T_DienNang = "0";
            T_TiTrong = "0";
            T_ThanhTien = "0";
            LKN_DienNang = "0";
            LKN_TiTrong = "0";
            LKN_ThanhTien = "0";

        }
        public string STT { get; set; }
        public string MaDT { get; set; }
        public string MucGia { get; set; }
        public string T_DienNang { get; set; }
        public string T_TiTrong { get; set; }
        public string T_ThanhTien { get; set; }
        public string LKN_DienNang { get; set; }
        public string LKN_TiTrong { get; set; }
        public string LKN_ThanhTien { get; set; }
    }
    public class MapChiTietBanDienTheoDTGJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data1 { get; set; }
        public string data2 { get; set; }
    }
    public class MapJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<BaoCaoTinhHinhSuCoLuoiDienJson> data { get; set; }
    }
    public class TongHopTienDoTHCacCTDTXDJson
    {
        public string DanhMucCongTrinh { get; set; }
        public string LoaiHinhDuAn { get; set; }
        public string DonViQuanLy { get; set; }
        public string KhoiCongKH { get; set; }
        public string HoanThanhKH { get; set; }
        public string KhoiCongTT { get; set; }
        public string DongDienTT { get; set; }
        public string TinhTrangDuAn { get; set; }
    }
    public class MapTongHopTienDoTHCacCTDTXDJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<TongHopTienDoTHCacCTDTXDJson> data { get; set; }
    }
    public class DanhSacCongTrinhTheoKeHoachDongDienJson
    {
        public string DanhMucCongTrinh { get; set; }
        public string NhomDuAn { get; set; }
        public string QuyMo { get; set; }
        public string NguonVon { get; set; }
        public string DonViQLDA { get; set; }
        public string TienDoKCKeHoach { get; set; }
        public string TienDoKCThuHien { get; set; }
        public string TienDoDongDienKeHoach { get; set; }
        public string TienDoDongDienThuHien { get; set; }
        public string TinhTrangDuAn { get; set; }
        public string GhiChu { get; set; }
    }
    public class MapDanhSacCongTrinhTheoKeHoachDongDienJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<DanhSacCongTrinhTheoKeHoachDongDienJson> data { get; set; }
    }
    public class DanhSacCongTrinhTheoThucTeKCJson
    {
        public string DanhMucCongTrinh { get; set; }
        public string NhomDuAn { get; set; }
        public string QuyMo { get; set; }
        public string NguonVon { get; set; }
        public string DonViQLDA { get; set; }
        public string TienDoKCKeHoach { get; set; }
        public string TienDoKCThuHien { get; set; }
        public string TienDoDongDienKeHoach { get; set; }
        public string TienDoDongDienThuHien { get; set; }
        public string TinhTrangDuAn { get; set; }
        public string GhiChu { get; set; }
    }
    public class MapDanhSacCongTrinhTheoThucTeKCJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<DanhSacCongTrinhTheoThucTeKCJson> data { get; set; }
    }

    public class DanhSacCongTrinhTheoThucTeDongDienJson
    {
        public string DanhMucCongTrinh { get; set; }
        public string NhomDuAn { get; set; }
        public string QuyMo { get; set; }
        public string NguonVon { get; set; }
        public string DonViQLDA { get; set; }
        public string TienDoKCKeHoach { get; set; }
        public string TienDoKCThuHien { get; set; }
        public string TienDoDongDienKeHoach { get; set; }
        public string TienDoDongDienThuHien { get; set; }
        public string TinhTrangDuAn { get; set; }
        public string GhiChu { get; set; }
    }
    public class MapDanhSacCongTrinhTheoThucTeDongDienJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<DanhSacCongTrinhTheoThucTeDongDienJson> data { get; set; }
    }

    public class TongHopDauTuXDJson
    {
        public string TenDuAN { get; set; }
        public string DonViQuanLy { get; set; }
        public string QuyMo { get; set; }
        public string NguonVon { get; set; }
        public string NgayGiao { get; set; }
        public string VonKeHoach { get; set; }
        public string SoQuyetDinh { get; set; }
        public string TongMucDauTu { get; set; }
        public string KhoiCongKH { get; set; }
        public string KhoiCongThucTe { get; set; }
        public string HoanThanhKeHoach { get; set; }
        public string HoanThanhThucTe { get; set; }
        public string TinhTrangDuAn { get; set; }
        public string VuongMacKienNghi { get; set; }
        public string ThongKe_DuAn_TongSo { get; set; }
        public string SoDA_KhoiCong { get; set; }
        public string SoDA_GiaoKhoiCong { get; set; }
        public string SoDA_HT { get; set; }
        public string SoDA_GiaoHT { get; set; }
    }
    public class MapTongHopDauTuXDJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<TongHopDauTuXDJson> data { get; set; }
    } 
    public class DTXD_BaoCaoTongHopDTXDJson
    {
        public string TenDuAN { get; set; }
        public string DonViQuanLy { get; set; }
        public string QuyMo { get; set; }
        public string NguonVon { get; set; }
        public string NgayGiao { get; set; }
        public string VonKeHoach { get; set; }
        public string SoQuyetDinh { get; set; }
        public string TongMucDauTu { get; set; }
        public string KhoiCongKH { get; set; }
        public string KhoiCongThucTe { get; set; }
        public string HoanThanhKeHoach { get; set; }
        public string HoanThanhThucTe { get; set; }
        public string TinhTrangDuAn { get; set; }
        public string VuongMacKienNghi { get; set; }
        public string ThongKe_DuAn_TongSo { get; set; }
        public string SoDA_KhoiCong { get; set; }
        public string SoDA_GiaoKhoiCong { get; set; }
        public string SoDA_HT { get; set; }
        public string SoDA_GiaoHT { get; set; }
    }

    public class DanhSacCongTrinhTheoKeHoachKCJson
    {
        public string DanhMucCongTrinh { get; set; }
        public string NhomDuAn { get; set; }
        public string QuyMo { get; set; }
        public string NguonVon { get; set; }
        public string DonViQLDA { get; set; }
        public string TienDoKCKeHoach { get; set; }
        public string TienDoKCThuHien { get; set; }
        public string TienDoDongDienKeHoach { get; set; }
        public string TienDoDongDienThuHien { get; set; }
        public string TinhTrangDuAn { get; set; }
        public string GhiChu { get; set; }
    }
    public class MapDanhSacCongTrinhTheoKeHoachKCJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<DanhSacCongTrinhTheoKeHoachKCJson> data { get; set; }
    }
    public class MapDTXD_BaoCaoTongHopDTXDJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<DTXD_BaoCaoTongHopDTXDJson> data { get; set; }
    }
    public class ThucHienKeHoachDauTuXayDungJson
    {
        public string LoaiHinh { get; set; }
        public string TongSoKH { get; set; }
        public string XayLapKH  { get; set; }
        public string ThietBiKH { get; set; }
        public string KhacKH    { get; set; }
        public string TongSoTH { get; set; }
        public string XayLapTH  { get; set; }
        public string ThietBiTH { get; set; }
        public string KhacTH { get; set; }
        public string TongSoUocBC  { get; set; }
        public string XayLapUocBC { get; set; }
        public string ThietBiUocBC { get; set; }
        public string KhacUocBC { get; set; }
        public string TongSoLK { get; set; }
        public string XayLapLK { get; set; }
        public string ThietBiLK { get; set; }
        public string KhacLK { get; set; }
        public string GiaTriNghiemThu { get; set; }
        public string GiaTriPhieu { get; set; }
        public string GiaTriGiaiNgan { get; set; }

    }
    public class MapThucHienKeHoachDauTuXayDungJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<ThucHienKeHoachDauTuXayDungJson> data { get; set; }
    }



    public class MapChiTietSoThuVaSoDuCacKPTJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data1 { get; set; }
        public string data2 { get; set; }
    }
    public class ChiTietSoThuVaSoDuCacKPTJson : BaseJsonBC
    {
        public ChiTietSoThuVaSoDuCacKPTJson() { }
        public ChiTietSoThuVaSoDuCacKPTJson(String tenKm)
        {
            TEN_KM = tenKm;
            PTRA_DKY = "0";
            PTHU_DKY = "0";
            PHATSINH = "0";
            TTHUA_UT = "0";
            TIEN_PBO = "0";
            SO_THUDUOC = "0";
            PTRA_CKY = "0";
            PTHU_CKY = "0";
            NO_COKNTT = "0";
            NO_KOKNTT = "0";
        }

        public string TEN_KM { get; set; }
        public string PTRA_DKY { get; set; }
        public string PTHU_DKY { get; set; }
        public string PHATSINH { get; set; }
        public string TTHUA_UT { get; set; }
        public string TIEN_PBO { get; set; }
        public string SO_THUDUOC { get; set; }
        public string PTRA_CKY { get; set; }
        public string PTHU_CKY { get; set; }
        public string NO_COKNTT { get; set; }
        public string NO_KOKNTT { get; set; }
       
    }
    public class BaoCaoSuDungXeTaiCauJson
    {
        public string ThoiGian { get; set; }
        public string MaLenh { get; set; }
        public string NoiDungCongViec { get; set; }
        public string BKSXe { get; set; }
        public string SoTien { get; set; }
        public string GhiChu { get; set; }
    }
    public class MapBaoCaoSuDungXeTaiCauJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<BaoCaoSuDungXeTaiCauJson> data { get; set; }
    } 
    public class XeTaiCau_BCTHChiPhiSuDungXeTaiCauJson
    {
        public string STT { get; set; }
        public string donvisudung { get; set; }
        public string SoChuyen { get; set; }
        public string SoKM { get; set; }
        public string SoGioCau { get; set; }
        public string ghichu { get; set; }
        public string TenDV { get; set; }
        public string SAPXEP { get; set; }
    }
    public class MapXeTaiCau_BCTHChiPhiSuDungXeTaiCauJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<XeTaiCau_BCTHChiPhiSuDungXeTaiCauJson> data { get; set; }
    } 
    // ban ke hoach
    public class ERP_BCNguonVonDauTuJson
    {
        public string DESCRIPTION { get; set; }
        public string NUM1 { get; set; }
        public string NUM2 { get; set; }
        public string NUM3 { get; set; }
        public string NUM4 { get; set; }
        public string NUM5 { get; set; }
        public string NUM6 { get; set; }
        public string NUM7 { get; set; }
        public string NUM8 { get; set; }
    }
    public class MapERP_BCNguonVonDauTuJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
    }
    public class TongHopTienDoKhoiCongDonDienJson
    {
        public string DanhMucCongTrinh { get; set; }
        public string NhomDuAn { get; set; }
        public string QuyMo { get; set; }
        public string NguonVon { get; set; }
        public string DonViQLDA { get; set; }
        public string TienDoGiaoKC { get; set; }
        public string TienDoGiaoDD { get; set; }
        public string TienDoThucTeKC { get; set; }
        public string TienDoThucTeDD { get; set; }
        public string TinhTrangDuAn { get; set; }
        public string GhiChu { get; set; }
    }
    public class MapTongHopTienDoKhoiCongDonDienJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<TongHopTienDoKhoiCongDonDienJson> data { get; set; }
    }
    public class BKH_TongHopSuCoVaSuatSuCoDuongDayVaTBAJson
    {
        public string INDEX1 { get; set; }
        public string INDEX2 { get; set; }
        public string INDEX3 { get; set; }
        public string INDEX4 { get; set; }
        public string INDEX5 { get; set; }
        public string INDEX6 { get; set; }
        public string INDEX7 { get; set; }
        public string INDEX8 { get; set; }
        public string INDEX9 { get; set; }
        public string INDEX10 { get; set; }
        public string INDEX11 { get; set; }
        public string INDEX12 { get; set; }
        public string INDEX13 { get; set; }
        public string INDEX14 { get; set; }
        public string INDEX15 { get; set; }
        public string INDEX16 { get; set; }
        public string INDEX17 { get; set; }
        public string INDEX18 { get; set; }
        public string INDEX19 { get; set; }
        public string INDEX20 { get; set; }
        public string INDEX21 { get; set; }
        public string INDEX22 { get; set; }
        public string INDEX23 { get; set; }
        public string INDEX24 { get; set; }
        public string INDEX25 { get; set; }
        public string INDEX26 { get; set; }
        public string INDEX27 { get; set; }
        public string INDEX28 { get; set; }
        public string INDEX29 { get; set; }
        public string INDEX30 { get; set; }
        public string INDEX31 { get; set; }
        public string INDEX32 { get; set; }
        public string INDEX33 { get; set; }
        public string INDEX34 { get; set; }
        public string INDEX35 { get; set; }
        public string INDEX36 { get; set; }
        public string INDEX37 { get; set; }
        public string INDEX38 { get; set; }
        public string INDEX39 { get; set; }
        public string INDEX40 { get; set; }
        public string INDEX41 { get; set; }
        public string INDEX42 { get; set; }
        public string INDEX43 { get; set; }
        public string INDEX44 { get; set; }
        public string INDEX45 { get; set; }
        public string INDEX46 { get; set; }
        public string INDEX47 { get; set; }
        public string INDEX48 { get; set; }
        public string INDEX49 { get; set; }
        public string INDEX50 { get; set; }
        public string INDEX51 { get; set; }
        public string INDEX52 { get; set; }
        public string INDEX53 { get; set; }
    }
    public class MapBKH_TongHopSuCoVaSuatSuCoDuongDayVaTBAJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public List<BKH_TongHopSuCoVaSuatSuCoDuongDayVaTBAJson> data { get; set; }
    }
    public class BangSoSanhGiaTriTonKhoDTXDJson
    {
        public string DonVi { get; set; }
        public string NUM1 { get; set; }
        public string NUM2 { get; set; }
        public string NUM3 { get; set; }
        public string NUM4 { get; set; }
        public string NUM5 { get; set; }
        public string NUM6 { get; set; }
        public string NUM7 { get; set; }
        public string NUM8 { get; set; }
        public string NUM9 { get; set; }
    }
    public class MapBangSoSanhGiaTriTonKhoDTXDJson
    {
        public string suc { get; set; }
        public string msg { get; set; } 
        public string data { get; set; }
    }
    public class BangSoSanhGiaTriTonKhoSXKDJson
    {
        public string DonVi { get; set; }
        public string NUM1 { get; set; }
        public string NUM2 { get; set; }
        public string NUM3 { get; set; }
       
    }
    public class MapBangSoSanhGiaTriTonKhoSXKDJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
    } 
    public class ERP_BCTuoiThoHangTonKho_DTXDJson
    {
        public string LINE_DESCRIPTION { get; set; }
        public string TONGTIEN { get; set; }
        public double NUM1 { get; set; }
        public double NUM2 { get; set; }
        public double NUM3 { get; set; }
        public double NUM4 { get; set; }
        public double NUM5 { get; set; }
        public double NUM6 { get; set; }
        public double NUM7 { get; set; }
        public double NUM8 { get; set; }
        public double NUM9 { get; set; }
        public double NUM10 { get; set; }
        public double NUM11 { get; set; }
        public double NUM12 { get; set; }
        public double NUM13 { get; set; }
        public double NUM14 { get; set; }
       
    }
    public class MapERP_BCTuoiThoHangTonKho_DTXDJson
    {
        public string suc { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
    }
}
