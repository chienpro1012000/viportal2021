﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{
    public class LichCaTrucItem : SPEntity
    {
        public SPFieldLookupValue CaTruc { set; get; }
        public SPFieldLookupValue CaTruc_x003a_ThoiGianBatDau { set; get; }
        public SPFieldLookupValue CaTruc_x003a_ThoiGianKetThuc { set; get; }
        public SPFieldLookupValue CreatedUser { set; get; }
        public DateTime ThoiGianKetThucThucTe { get; set; }
        public DateTime ThoiGianBatDauThucTe { get; set; }
        public DateTime? NgayTruc { set; get; }
        public string LichGhiChu { set; get; }
        /// <summary>
        /// 1 là trực chính.
        /// khác phụ
        /// </summary>
        public int DBPhanLoai { set; get; }
        public SPFieldLookupValue fldGroup { set; get; }

        public LichCaTrucItem()
        {
            CaTruc = new SPFieldLookupValue();
            CaTruc_x003a_ThoiGianBatDau = new SPFieldLookupValue();
            CaTruc_x003a_ThoiGianKetThuc = new SPFieldLookupValue();
            CreatedUser = new SPFieldLookupValue();
            fldGroup = new SPFieldLookupValue();

        }
    }

    public class LichCaTrucJson: EntityJson
    {
        public LookupData CaTruc { set; get; }
        public LookupData CaTruc_x003a_ThoiGianBatDau { set; get; }
        public LookupData CaTruc_x003a_ThoiGianKetThuc { set; get; }
        public LookupData CreatedUser { set; get; }
        public DateTime? NgayTruc { set; get; }
        public string LichGhiChu { set; get; }
        public DateTime ThoiGianKetThucThucTe { get; set; }
        public DateTime ThoiGianBatDauThucTe { get; set; }
        public int DBPhanLoai { set; get; }
        public LookupData fldGroup { set; get; }

        public LichCaTrucJson()
        {
            CaTruc = new LookupData();
            CaTruc_x003a_ThoiGianBatDau = new LookupData();
            CaTruc_x003a_ThoiGianKetThuc = new LookupData();
            CreatedUser = new LookupData();
            fldGroup = new LookupData();

        }
    }
}
