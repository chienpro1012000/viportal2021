﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public class LichCaTrucDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/noidung/Lists/LichCaTruc";
        public LichCaTrucDA()
        {
            Init(_urlList);
        }
        public LichCaTrucDA(string _urlSiteProcess)
            : base(_urlSiteProcess + "/noidung/Lists/LichCaTruc")
        {
            _urlList = _urlSiteProcess;
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<LichCaTrucJson> GetListJson(LichCaTrucQuery searhOption, params string[] Fields)
        {
            List<LichCaTrucJson> lstReturn = new List<LichCaTrucJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0) 
            {
                Fields = new string[] { "ID", "Title", "NgayTruc", "CreatedUser", "CaTruc", "LichGhiChu", "ThoiGianBatDauThucTe", "ThoiGianKetThucThucTe", "DBPhanLoai", "fldGroup" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<LichCaTrucJson>(SPcoll, Fields);
            return lstReturn;
        }

        public int GetCount(LichCaTrucQuery searhOption)
        {
            List<LichCaTrucJson> lstReturn = new List<LichCaTrucJson>();
            SPQuery oQuery = new SPQuery();
            oQuery.ViewFields = BuildViewField("ID");
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = SpListProcess.GetItems(oQuery);
            return SPcoll.Count;
        }

        public string BuildQuery(LichCaTrucQuery  searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if (searhOption.DenNgay.HasValue)
                oBuildQuery.Append("<And>");
            if (searhOption.TuNgay.HasValue)
                oBuildQuery.Append("<And>");
            if (searhOption.NgayTruc.HasValue)
                oBuildQuery.Append("<And>");
            if (searhOption.CreatedUser > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.CaTruc > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.fldGroup > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            if (searhOption.fldGroup > 0)
            {
                oBuildQuery.Append(GetStringEQLookUp("fldGroup", searhOption.fldGroup));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.CaTruc > 0)
            {
                oBuildQuery.Append(GetStringEQLookUp("CaTruc", searhOption.CaTruc));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.CreatedUser > 0)
            {
                oBuildQuery.Append(GetStringEQLookUp("CreatedUser", searhOption.CreatedUser));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.NgayTruc.HasValue)
            {
                oBuildQuery.Append(GetStringEqDate("NgayTruc", searhOption.NgayTruc.Value));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.TuNgay.HasValue)
            {
                oBuildQuery.Append(GetStringGeqDate("NgayTruc", searhOption.TuNgay.Value));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.DenNgay.HasValue)
            {
                oBuildQuery.Append(GetStringLeqDate("NgayTruc", searhOption.DenNgay.Value));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}