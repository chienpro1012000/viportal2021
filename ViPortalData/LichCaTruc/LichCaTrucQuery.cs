using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public class LichCaTrucQuery : QuerySearch
    {
        public int CreatedUser { get; set; }
        public DateTime? NgayTruc { get; set; }
        public int CaTruc { get; set; }
        public LichCaTrucQuery()
        {
        }
        public int fldGroup { get; set; }
        public LichCaTrucQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
}