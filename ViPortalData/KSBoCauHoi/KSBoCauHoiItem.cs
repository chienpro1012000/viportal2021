using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
using ViPortalData.KSCauTraLoi;

namespace ViPortalData.KSBoCauHoi
{
    public class KSBoCauHoiItem : SPEntity
    {
        public string DMDescription { set; get; }
        public int LoaiTraLoi { set; get; }
        /// <summary>
        /// ,1,2,3,4,5,6,7,
        /// </summary>
        public string UCThamGia { get; set; }
        public bool DMHienThi { set; get; }
        public int DMSTT { set; get; }
        public int KSHinhThucTraLoi { set; get; }
        public SPFieldLookupValue KSBoChuDe { set; get; }

        public KSBoCauHoiItem()
        {
            KSBoChuDe = new SPFieldLookupValue();
            DMHienThi = true;
        }
    }
    public class KSBoCauHoiJson : EntityJson
    {
        public string UCThamGia { get; set; }
        public string DMDescription { set; get; }
        public int LoaiTraLoi { set; get; }
        public bool DMHienThi { set; get; }
        public int DMSTT { set; get; }
        public int KSHinhThucTraLoi { set; get; }
        public List<KSCauTraLoiJson> lstKSCauTraLoiJson { get; set; }
        public LookupData KSBoChuDe { set; get; }
        public KSBoCauHoiJson()
        {
            KSBoChuDe = new LookupData();
            lstKSCauTraLoiJson = new List<KSCauTraLoiJson>();
            DMHienThi = true;
        }
    }
}