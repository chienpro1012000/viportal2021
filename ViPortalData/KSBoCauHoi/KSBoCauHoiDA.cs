using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortalData.KSBoCauHoi
{
    public class KSBoCauHoiDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/noidung/Lists/KSBoCauHoi";
        public KSBoCauHoiDA(bool isAdmin = false)
        {
            if (!isAdmin)
                Init(_urlList);
            else InitAdmin(_urlList);
        }
        public KSBoCauHoiDA(string _urlSiteProcess)
            : base(_urlSiteProcess + "/noidung/Lists/KSBoCauHoi")
        {
            _urlList = _urlSiteProcess;
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<KSBoCauHoiJson> GetListJson(KSBoCauHoiQuery searhOption, params string[] Fields)
        {
            List<KSBoCauHoiJson> lstReturn = new List<KSBoCauHoiJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "DMDescription", "DMHienThi", "DMSTT", "KSHinhThucTraLoi", "KSBoChuDe", "_ModerationStatus", "LoaiTraLoi" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<KSBoCauHoiJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(KSBoCauHoiQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            if (searhOption.IdChuDe > 0)
                oBuildQuery.Append("<And>");
            if (searhOption._ModerationStatus == 1)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption._ModerationStatus == 1)
            {
                oBuildQuery.Append(GetStringModer(0)); //appro l� 0
                oBuildQuery.Append("</And>");
            }
            if (searhOption.IdChuDe > 0)
            {
                oBuildQuery.Append(GetStringEQLookUp("KSBoChuDe", searhOption.IdChuDe));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}