using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{
    public class OssiDataTypeItem : SPEntity
    {
        public string dataName { set; get; }
        public int dataKieuDuLieu { set; get; }
        public string dataDefaultValue { set; get; }
        public int dataSTT { set; get; }
        public string dataDescription { set; get; }
        public bool dataReadOnly { set; get; }
        public bool dataRequired { set; get; }
        public string dataFormat { set; get; }
        public bool dataIsDateOnly { set; get; }
        public string dataEnum { set; get; }
        public int dataMinLength { set; get; }
        public int dataMaxLength { set; get; }
        public int dataMinimum { set; get; }
        public int dataMaximum { set; get; }
        public string dataListLookupName { set; get; }
        public string dataListLookupUrl { set; get; }
        public int BaoCaoDanhMuc { set; get; }
        public bool dataIsMultiLookup { set; get; }
        public int dataRowMulti { set; get; }
        public string dataTitleRows { set; get; }
        public string dataTitleColumns { set; get; }
        public string ControlHTML { set; get; }

        public OssiDataTypeItem()
        {

        }
    }


    public class OssiDataTypeJson : EntityJson
    {
        public string dataName { set; get; }
        public int dataKieuDuLieu { set; get; }
        public string dataDefaultValue { set; get; }
        public int dataSTT { set; get; }
        public string dataDescription { set; get; }
        public bool dataReadOnly { set; get; }
        public bool dataRequired { set; get; }
        public string dataFormat { set; get; }
        public bool dataIsDateOnly { set; get; }
        public string dataEnum { set; get; }
        public int dataMinLength { set; get; }
        public int dataMaxLength { set; get; }
        public int dataMinimum { set; get; }
        public int dataMaximum { set; get; }
        public string dataListLookupName { set; get; }
        public string dataListLookupUrl { set; get; }
        public int BaoCaoDanhMuc { set; get; }
        public bool dataIsMultiLookup { set; get; }
        public int dataRowMulti { set; get; }
        public string dataTitleRows { set; get; }
        public string dataTitleColumns { set; get; }
        public string ControlHTML { set; get; }

        public OssiDataTypeJson()
        {

        }
    }
}
