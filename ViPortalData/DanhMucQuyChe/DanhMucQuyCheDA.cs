using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortalData.DanhMucQuyChe
{
    public class DanhMucQuyCheDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/noidung/Lists/DanhMucQuyChe";
        public DanhMucQuyCheDA(bool isAdmin = false)
        {
            if (isAdmin)
                InitAdmin(_urlList);
            else
                Init(_urlList);
        }
        public DanhMucQuyCheDA(string _urlSiteProcess, bool isAdmin = false)
        {
            _urlList = _urlSiteProcess + "/noidung/Lists/DanhMucQuyChe";
            if (isAdmin)
                InitAdmin(_urlList);
            else
                Init(_urlList);
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<DanhMucQuyCheJson> GetListJson(DanhMucQuyCheQuery searhOption, params string[] Fields)
        {
            List<DanhMucQuyCheJson> lstReturn = new List<DanhMucQuyCheJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "DMDescription", "Created",
                    "DMHienThi", "DMMoTa", "DMSTT", "DMVietTat", "_ModerationStatus" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<DanhMucQuyCheJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(DanhMucQuyCheQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if (searhOption._ModerationStatus == 1)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            if (searhOption._ModerationStatus == 1)
            {
                oBuildQuery.Append(GetStringModer(0)); //appro l� 0
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}