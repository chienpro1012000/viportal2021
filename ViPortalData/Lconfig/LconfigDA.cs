﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;
namespace ViPortalData.Lconfig
{
    public class LconfigDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/cms/Lists/Lconfig";
        public LconfigDA(bool isAdmin = false)
        {
            if (isAdmin)
                InitAdmin(_urlList);
            else
                Init(_urlList);
        }
        public LconfigDA(string _urlSiteProcess, bool isAdmin = false)
        {
            _urlList = _urlSiteProcess + "/cms/Lists/Lconfig";
            if (isAdmin)
                InitAdmin(_urlList);
            else
                Init(_urlList);
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<LconfigJson> GetListJson(LconfigQuery searhOption, params string[] Fields)
        {
            List<LconfigJson> lstReturn = new List<LconfigJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "ConfigGroup", "ConfigType", "ConfigValue", "_ModerationStatus", 
                    "Permissions", "ConfigValueMulti", "Attachments" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<LconfigJson>(SPcoll, Fields);
            return lstReturn;
        }

        public int GetCount(LconfigQuery searhOption)
        {
            SPQuery oQuery = new SPQuery();
            oQuery.ViewFields = BuildViewField("ID");
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = SpListProcess.GetItems(oQuery);
            return SPcoll.Count;
        }

        public string BuildQuery(LconfigQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if(searhOption.ItemIDNotGet > 0)
                oBuildQuery.Append("<And>");
            if (!string.IsNullOrEmpty(searhOption.ConfigType))
                oBuildQuery.Append("<And>");
            if (!string.IsNullOrEmpty(searhOption.ConfigGroup))
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            if (searhOption._ModerationStatus == 1)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption._ModerationStatus == 1)
            {
                oBuildQuery.Append(GetStringModer(0)); //appro là 0
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            if (!string.IsNullOrEmpty(searhOption.ConfigGroup))
            {
                oBuildQuery.Append(GetStringEQText("ConfigGroup", searhOption.ConfigGroup));
                oBuildQuery.Append("</And>");
            }
            if (!string.IsNullOrEmpty(searhOption.ConfigType))
            {
                oBuildQuery.Append(GetStringEQText("ConfigType", searhOption.ConfigType));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.ItemIDNotGet > 0)
            {
                oBuildQuery.Append(getstringNumNotEQ("ID", searhOption.ItemIDNotGet));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
        public string GetValueConfigByType(string strtype, string FieldName = "ConfigValue")
        {
            SPQuery spQuery = new SPQuery();
            StringBuilder stbBuilder;
            #region Lấy các trường cần thiết
            string[] Fields = new string[] { "ID", "Title", "ConfigType", "ConfigValue" }; //Set default
            spQuery.ViewFields = BuildViewField(Fields);
            #endregion
            #region Build ra câu truy vấn
            stbBuilder = new StringBuilder();
            stbBuilder.Append("<Where>");
            stbBuilder.Append("         <Eq>");
            stbBuilder.Append("            <FieldRef Name=\"ConfigType\" />");
            stbBuilder.Append("            <Value Type=\"Text\">" + strtype + "</Value>");
            stbBuilder.Append("         </Eq>");
            stbBuilder.Append("</Where>");
            spQuery.Query = stbBuilder.ToString();
            #endregion
            //Lấy về danh sách
            SPListItemCollection spLConfigsCol = SpListProcess.GetItems(spQuery);
            if (spLConfigsCol.Count > 0)
            {
                return spLConfigsCol[0][FieldName].ToString();
            }
            else
            {
                return string.Empty;
            }
        }
        public string GetValueMultilConfigByType(string strtype, string FieldName = "ConfigValueMulti")
        {
            SPQuery spQuery = new SPQuery();
            StringBuilder stbBuilder;
            #region Lấy các trường cần thiết
            string[] Fields = new string[] { "ID", "Title", "ConfigType", "ConfigValue" , "ConfigValueMulti" }; //Set default
            spQuery.ViewFields = BuildViewField(Fields);
            #endregion
            #region Build ra câu truy vấn
            stbBuilder = new StringBuilder();
            stbBuilder.Append("<Where>");
            stbBuilder.Append("         <Eq>");
            stbBuilder.Append("            <FieldRef Name=\"ConfigType\" />");
            stbBuilder.Append("            <Value Type=\"Text\">" + strtype + "</Value>");
            stbBuilder.Append("         </Eq>");
            stbBuilder.Append("</Where>");
            spQuery.Query = stbBuilder.ToString();
            #endregion
            //Lấy về danh sách
            SPListItemCollection spLConfigsCol = SpListProcess.GetItems(spQuery);
            if (spLConfigsCol.Count > 0)
            {
                return spLConfigsCol[0][FieldName].ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
