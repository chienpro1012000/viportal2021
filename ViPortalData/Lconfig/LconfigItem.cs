﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;

namespace ViPortalData.Lconfig
{
	public class LconfigItem : SPEntity
	{
		public string ConfigGroup { set; get; }
		public string ConfigType { set; get; }
		public string ConfigValue { set; get; }
		public string ConfigValueMulti { set; get; }
		public SPFieldLookupValueCollection Permissions { get; set; }
		public LconfigItem()
		{
			Permissions = new SPFieldLookupValueCollection();
		}
	}

	public class ConfigAPI
    {
		public string UrlAPI { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }
    }
	public class LconfigJson : EntityJson
	{
		public string ConfigGroup { set; get; }
		public string ConfigType { set; get; }
		public string ConfigValue { set; get; }
		public string ConfigValueMulti { set; get; }
		public List<LookupData> Permissions { get; set; }
		public LconfigJson()
		{
			Permissions = new List<LookupData>();
		}
	}
}