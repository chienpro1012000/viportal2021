using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{
    public class ChatHopThoaiItem : SPEntity
    {
        public string UserThamGia { set; get; }
        public string UserThamGia_Id { set; get; }
        public bool isGroupChat { get; set; }
        public string LastMessage { get; set; }
        public string AnhDaiDien { get; set; }
        public ChatHopThoaiItem()
        {

        }
    }

    public class ChatHopThoaiJson : EntityJson
    {
        public List<LookupData> UserThamGia { set; get; }
        public string UserThamGia_Id { set; get; }
        public string UserID { get; set; }
        public bool isGroupChat { get; set; }
        public string LastMessage { get; set; }
        public string AnhDaiDien { get; set; }
        public string FullText { get; set; }

        public ChatHopThoaiJson()
        {
            LastMessage = string.Empty;
            UserThamGia = new List<LookupData>();
        }
    }
}
