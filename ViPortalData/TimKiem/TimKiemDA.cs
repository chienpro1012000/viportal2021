﻿using CommonServiceLocator;
using SolrNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViPortal_Utils.Base;
using ViPortal_Utils;
using SolrNet.Commands.Parameters;

namespace ViPortalData.TimKiem
{
    public class TimKiemDA : SPBaseDA<DataSolr>
    {
        public TimKiemDA()
        {
            solrWorkerDM = ServiceLocator.Current.GetInstance<ISolrOperations<DataSolr>>();
        }
        public List<ISolrQuery> BuildQuerySolr(QuerySearch searhOption)
        {
            var Querys = new List<ISolrQuery>();
            Querys.Add(new SolrQuery("*:*"));
            if (!string.IsNullOrEmpty(searhOption.UrlSiteStart))
            {
                Querys.Add(new SolrQuery($"id:{searhOption.UrlSiteStart}*"));
            }
            if (searhOption.UrlList != null && searhOption.UrlList.Count > 0)
            {
                //UrlList:("/noidung/Lists/TinTucSuKien" "/noidung/Lists/TinTuc")
                Querys.Add(new SolrQuery(string.Format("UrlList:({0})", string.Join(" ", searhOption.UrlList.Select(x => "\"" + x + "\"")))));
            }
            else if(SpListProcess != null)
            {
                Querys.Add(new SolrQuery(string.Format("UrlList:\"{0}\"", SpListProcess.ParentWeb.ServerRelativeUrl + "/" + SpListProcess.RootFolder.Url)));
            }
            if(searhOption.UrlListNotGet != null && searhOption.UrlListNotGet.Count > 0)
            {
                Querys.Add(new SolrQuery(string.Format("!UrlList:({0})", string.Join(" ", searhOption.UrlListNotGet.Select(x => "\"" + x + "\"")))));
            }
            if (!string.IsNullOrEmpty(searhOption.Keyword))
            {
                if (searhOption.SearchIn.Count > 0)
                {
                    if (searhOption.SearchIn.Count > 1)
                    {
                        List<ISolrQuery> lstOr = new List<ISolrQuery>();
                        for (int index = 0; index < searhOption.SearchIn.Count; index++)
                        {
                            lstOr.Add(new SolrQuery("(" + searhOption.SearchIn[index] + ":\"" + searhOption.Keyword + "\")"));
                        }
                        Querys.Add(new SolrMultipleCriteriaQuery(lstOr, "OR"));
                    }
                    else
                        Querys.Add(new SolrQuery("(" + searhOption.SearchIn[0] + ":\"" + searhOption.Keyword + "\")"));

                }
            }
            if (searhOption.isGetBylistID)
            {
                if (searhOption.lstIDget.Count > 0)
                    Querys.Add(new SolrQuery("SP_ID:(" + string.Join(" ", searhOption.lstIDget) + ")"));
                else Querys.Add(new SolrQuery("!SP_ID:[* TO *]"));
            }
            if (searhOption._ModerationStatus == 1)
            {
                Querys.Add(new SolrQuery("_ModerationStatus:0"));
            }
            if (searhOption.TuNgay.HasValue)
            {
                Querys.Add(new SolrQuery(GetQueryDateTimeSolrGeq("Created", searhOption.TuNgay.Value)));
            }
            if (searhOption.DenNgay.HasValue)
            {
                Querys.Add(new SolrQuery(GetQueryDateTimeSolrGeq("Created", searhOption.DenNgay.Value)));
            }
            BuildQueryToString(Querys);
            return Querys;
        }
        public List<DataSolr> GetListJsonSolr(QuerySearch searhOption, params string[] Fields)
        {
            List<DataSolr> lstReturn = new List<DataSolr>();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "id", "SP_ID", "Title", "CreatedDate", "Created", "UrlList", "FullUrlList" }; //Set default
            }
            var Querys = BuildQuerySolr(searhOption);
            #region Option khởi tạo
            var oorders = new List<SortOrder>();

            if (oorders.FindIndex(x => x.FieldName == searhOption.FieldOrder) == -1)
                oorders.Add(new SortOrder(searhOption.FieldOrder == "ID" ? "id" : searhOption.FieldOrder, (searhOption.Ascending) ? Order.ASC : Order.DESC));
            QueryOptions options = new QueryOptions()
            {
                OrderBy = oorders,
                Fields = Fields,
                //StartOrCursor =  new StartOrCursor.Start((currentPage - 1) * rowPerPage)
            };
            if (searhOption.Length > 0)
            {
                options.Rows = searhOption.Length;
                options.Start = searhOption.Start; //Barn ghi dau tien
            }

            #endregion
            var result = solrWorkerDM.Query(new SolrMultipleCriteriaQuery(Querys, "AND"), options);
            TongSoBanGhiSauKhiQuery = Convert.ToInt32(result.NumFound);
            
            return result;
        }

    }
}
