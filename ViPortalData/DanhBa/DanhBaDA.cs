using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortalData
{
    public class DanhBaDA : SPBaseDA<DataSolr>
    {
        private string _urlList = "/noidung/Lists/DanhBa";
        public DanhBaDA(bool isAdmin = false)
        {
            if (isAdmin)
                InitAdmin(_urlList, true);
            else
                Init(_urlList, true);
        }
        public DanhBaDA(string _urlSite)
        {
            _urlList = _urlSite + _urlList;
            Init(_urlList);
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<DanhBaJson> GetListJson(DanhBaQuery searhOption, params string[] Fields)
        {
            List<DanhBaJson> lstReturn = new List<DanhBaJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "isOffice", "IsDonVi", "IsPhongBan", "DBPhongBan", "DBParentPB", "DBDiaChi", "DBEmail", "DBFax", "DBSDT", "DMSTT", "_ModerationStatus", "DBChucVu", "ImageNews", "oldID" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<DanhBaJson>(SPcoll, Fields);
            return lstReturn;
        }
        public List<DanhBaJson> GetListJson_v2(DanhBaQuery searhOption, params string[] Fields)
        {
            List<DanhBaJson> lstReturn = new List<DanhBaJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "DBNgaySinh", "isOffice", "IsDonVi", "IsPhongBan", "DBPhongBan", "DBParentPB", "DBDiaChi", "DBEmail", "DBFax", "DBSDT", "DMSTT", "_ModerationStatus", "DBChucVu", "ImageNews", "oldID", "DBDonVi" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery_v2(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<DanhBaJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(DanhBaQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();

            oBuildQuery.Append("<Where>");
            if (!string.IsNullOrEmpty(searhOption.Title))
                oBuildQuery.Append("<And>");
            if (searhOption.oldID > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.cap == 1)
                oBuildQuery.Append("<And>");
            if (searhOption.isUser > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.IDChucVu > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.isOffice > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.IDParent > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            if (searhOption._ModerationStatus == 1)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption._ModerationStatus == 1)
            {
                oBuildQuery.Append(GetStringModer(0)); //appro l� 0
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetBylistID)
            {
                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.IDParent > 0)
            {
                oBuildQuery.Append(GetStringEQLookUp("DBParentPB", searhOption.IDParent));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isOffice > 0)
            {
                oBuildQuery.Append(GetStringEQText("isOffice", searhOption.isOffice.ToString()));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.IDChucVu > 0)
            {
                oBuildQuery.Append(GetStringEQLookUp("DBChucVu", searhOption.IDChucVu));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isUser > 0)
            {
                oBuildQuery.Append(getstringNotEQText("isOffice", "1"));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.cap == 1)
            {
                oBuildQuery.Append(getstringNull("DBParentPB"));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.oldID > 0)
            {
                oBuildQuery.Append(GetStringEQNumber("oldID", searhOption.oldID));
                oBuildQuery.Append("</And>");
            }
            if (!string.IsNullOrEmpty(searhOption.Title))
            {
                oBuildQuery.Append(GetStringEQText("Title", searhOption.Title));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
        public string BuildQuery_v2(DanhBaQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();

            oBuildQuery.Append("<Where>");
            if (searhOption.DBDonVi > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.TuNgay_SinhNhat.HasValue)
                oBuildQuery.Append("<And>");
            if (searhOption.DenNgay_SinhNhat.HasValue)
                oBuildQuery.Append("<And>");
            if (searhOption.isOffice > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.IDParent > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            if (searhOption._ModerationStatus == 1)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption._ModerationStatus == 1)
            {
                oBuildQuery.Append(GetStringModer(0)); //appro l� 0
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetBylistID)
            {
                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.IDParent > 0)
            {
                oBuildQuery.Append(GetStringEQLookUp("DBParentPB", searhOption.IDParent));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isOffice > 0)
            {
                oBuildQuery.Append(GetStringEQText("isOffice", searhOption.isOffice.ToString()));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.DenNgay_SinhNhat.HasValue)
            {
                oBuildQuery.Append(GetStringCamlDate("DBNgaySinh", searhOption.DenNgay_SinhNhat.Value, "Leq"));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.TuNgay_SinhNhat.HasValue)
            {
                oBuildQuery.Append(GetStringCamlDate("DBNgaySinh", searhOption.TuNgay_SinhNhat.Value, "Geq"));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.DBDonVi > 0)
            {
                oBuildQuery.Append(GetStringEQLookUp("DBDonVi", searhOption.DBDonVi));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}