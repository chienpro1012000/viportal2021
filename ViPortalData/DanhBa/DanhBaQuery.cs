using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using ViPortal_Utils.Base;

namespace ViPortalData
{
    public class DanhBaQuery : QuerySearch
    {
        public int IDChucVu { set; get; }
        public bool IsDonVi { get; set; }
        public int isOffice { get; set; }
        public int isUser { get; set; }
        public int IDParent { get; set; }
        public int DBDonVi { get; set; }
        public int cap { get; set; }
        public int oldID { get; set; }
        public string Title { get; set; }
        public DateTime? TuNgay_SinhNhat { get; set; }
        public DateTime? DenNgay_SinhNhat { get; set; }
        public int MaDonVi { get; set; }
        public long PhongBan { get; set; }
        public int TuKhoa { get; set; }
        public DanhBaQuery()
        {
        }
        public DanhBaQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
}