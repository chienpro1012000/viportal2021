using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace ViPortalData
{
    public class DanhBaItem : SPEntity
    {
        public string DBDiaChi { set; get; }
        public string DBEmail { set; get; }
        public string DBFax { set; get; }
        public int DBPhanLoai { set; get; }
        public string DBSDT { set; get; }
        public string DBTreeID { set; get; }
        public string DBWebsite { set; get; }
        public SPFieldLookupValue DBParentPB { set; get; }
        public SPFieldLookupValue DBDonVi { set; get; }
        public SPFieldLookupValue DBChucVu { set; get; }
        public string DBNhom { set; get; }
        public string ImageNews { set; get; }
        public int DMSTT { set; get; }
        public bool IsDonVi { set; get; }
        public string isOffice { get; set; }
        public int oldID { get; set; }        
        public DateTime? DBNgaySinh { get; set; }
        public DanhBaItem()
        {
            DBParentPB = new SPFieldLookupValue();
            DBDonVi = new SPFieldLookupValue();
            DBChucVu = new SPFieldLookupValue();
            DBDiaChi = string.Empty;
        }
    }

    public class DanhBaJson : EntityJson
    {
        public string DBDiaChi { set; get; }
        public string DBEmail { set; get; }
        public string DBFax { set; get; }
        public int DBPhanLoai { set; get; }
        public string DBSDT { set; get; }
        public string DBTreeID { set; get; }
        public string DBWebsite { set; get; }
        public LookupData DBParentPB { set; get; }
        public LookupData DBChucVu { set; get; }
        public LookupData DBDonVi { set; get; }

        public string DBNhom { set; get; }
        public string ImageNews { set; get; }
        public int DMSTT { set; get; }
        public bool IsDonVi { set; get; }
        public string isOffice { get; set; }
        public int oldID { get; set; }
        public DateTime? DBNgaySinh { get; set; }
        public DanhBaJson()
        {
            DBParentPB = new LookupData();
            DBChucVu = new LookupData();
            DBDonVi = new LookupData();

        }
    }

    public class LDonViEVN
    {
        public string orgId { get; set; }
        public string orgCode { get; set; }
        public string orgName { get; set; }
        public string parentId { get; set; }
        public string shortName { get; set; }
        public string address { get; set; }
        public int status { get; set; }
    }
    public class LPhongBanEVN
    {
        public string deptId { get; set; }
        public string orgId { get; set; }
        public string name { get; set; }
        public string status { get; set; }
        public string shortName { get; set; }
    }

}
