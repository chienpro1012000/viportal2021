﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViPortalData
{
    public class TreeViewItemV1
    {
        public string title { get; set; }
        public string key { get; set; }
        public List<TreeViewItemV1> children { get; set; }
        public bool folder { get; set; }
        public bool expanded { get; set; }
        public bool selected { get; set; }
        public bool lazy { get; set; }
        public string email { get; set; }
        public string diachi { get; set; }
        public string dienthoai { get; set; }
        public bool duyet { get; set; }
        public TreeViewItemV1()
        {
            expanded = false;
            selected = false;
            folder = false;
            lazy = false;
            duyet = true;
        }
        public TreeViewItemV1(string title, string key, bool folder = false, bool expanded = true, bool selected = false)
        {
            this.title = title;
            this.key = key;
            this.children = children;
            this.folder = folder;
            this.expanded = expanded;
            this.selected = selected;
        }
    }
}