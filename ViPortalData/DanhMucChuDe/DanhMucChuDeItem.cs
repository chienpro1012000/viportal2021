using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{
   public class DanhMucChuDeItem:SPEntity
    {
        public string NoiDung {set;get;}
	public string AnhDaiDien {set;get;}
	public bool TrangThai {set;get;}
	public string UrlList {set;get;}
	
	public DanhMucChuDeItem(){
		
	}
    }
	public class DanhMucChuDeJson: EntityJson
	{
        public string NoiDung {set;get;}
	public string AnhDaiDien {set;get;}
	public bool TrangThai {set;get;}
	public string UrlList {set;get;}
	
	public DanhMucChuDeJson(){
		
	}
    }
}
