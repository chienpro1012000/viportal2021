﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;

namespace ViPortalData.FAQThuongGap
{
	public class FAQThuongGapItem : SPEntity
	{
		public string FAQCauHoi { set; get; }
		public DateTime? FAQNgayHoi { set; get; }
		public DateTime? FAQNgayTraLoi { set; get; }
		public string FAQNoiDungTraLoi { set; get; }
		public int FAQSTT { set; get; }
		public SPFieldLookupValue FQNguoiHoi { set; get; }
		public SPFieldLookupValue FQNguoiTraLoi { set; get; }
		public int FAQLuotXem { set; get; }
		public FAQThuongGapItem()
		{
			FQNguoiHoi = new SPFieldLookupValue();
			FQNguoiTraLoi = new SPFieldLookupValue();
		}
	}
	public class FAQThuongGapJson : EntityJson
	{
		public string FAQCauHoi { set; get; }
		public DateTime? FAQNgayHoi { set; get; }
		public DateTime? FAQNgayTraLoi { set; get; }
		public string FAQNoiDungTraLoi { set; get; }
		public int FAQSTT { set; get; }
		public LookupData FQNguoiHoi { set; get; }
		public LookupData FQNguoiTraLoi { set; get; }
		public int FAQLuotXem { set; get; }
		public FAQThuongGapJson()
		{
			FQNguoiHoi = new LookupData();
			FQNguoiTraLoi = new LookupData();
		}
	}
}