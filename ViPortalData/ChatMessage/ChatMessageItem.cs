using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{
    public class ChatMessageItem : SPEntity
    {
        public string ChatHopThoai { set; get; }
        public int? ChatHopThoai_Id { set; get; }
        public string NoiDungMessage { set; get; }
        public SPFieldLookupValue CreatedUser { set; get; }
        public string UserNhan { set; get; }
        public string UserNhan_Id { set; get; }
        public string AnhDaiDien { set; get; }

        public ChatMessageItem()
        {
            CreatedUser = new SPFieldLookupValue();

        }
    }

    public class ChatMessageJson : EntityJson
    {
        public string ChatHopThoai { set; get; }
        public int ChatHopThoai_Id { set; get; }
        public string UrAnhDaiDien { get; set; }
        public string NoiDungMessage { set; get; }
        public LookupData CreatedUser { set; get; }
        public string UserNhan { set; get; }
        public string UserNhan_Id { set; get; }

        public ChatMessageJson()
        {
            CreatedUser = new LookupData();

        }
    }
}
