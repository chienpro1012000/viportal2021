﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ViPortal_Utils.Base;

namespace ViPortalData
{
    class ModelQuery
    {
    }
    /// <summary>
    /// 
    /// </summary>
    public class ChiTieuKinhDoanhQuery : QuerySearch 
    {
        public int Nam { get; set; }
        public int Thang { get; set; }
        public string MaDonVi { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ChiTieuKinhDoanhQuery()
        {
        } 
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public ChiTieuKinhDoanhQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }

    public class BaoCaoChiTeuDVKHQuery : QuerySearch
    {
        public string SearchThang { get; set; }
        public string SearchNam { get; set; }
        public string MaDonVi { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public BaoCaoChiTeuDVKHQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public BaoCaoChiTeuDVKHQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class TongHopDoTinCayLuoiDien110_550kVQuery : QuerySearch
    {
        public string Thang { get; set; }
        public string Nam { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public TongHopDoTinCayLuoiDien110_550kVQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public TongHopDoTinCayLuoiDien110_550kVQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    } 
    public class THSCVaSSCDuongDayVaTBALTHAQuery : QuerySearch
    {
        public string Thang { get; set; }
        public string Nam { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public THSCVaSSCDuongDayVaTBALTHAQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public THSCVaSSCDuongDayVaTBALTHAQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class BaoCaoTinhHinhSuCoLuoiDienQuery : QuerySearch
    {
        public string SearchThang { get; set; }
        public string SearchNam { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public BaoCaoTinhHinhSuCoLuoiDienQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public BaoCaoTinhHinhSuCoLuoiDienQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class BaoCaoChiSoDoTinCayLuoiDienPhanPhoiQuery : QuerySearch
    {
        public string Thang { get; set; }
        public string Nam { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public BaoCaoChiSoDoTinCayLuoiDienPhanPhoiQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public BaoCaoChiSoDoTinCayLuoiDienPhanPhoiQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    } 
    public class PMIS_GetAll_THDTCLuoiDienPhanPhoiQuery : QuerySearch
    {
        public string Thang { get; set; }
        public string Nam { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public PMIS_GetAll_THDTCLuoiDienPhanPhoiQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public PMIS_GetAll_THDTCLuoiDienPhanPhoiQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class TongHopSuCoVaSuatSuCoDuongDayVaTBAQuery : QuerySearch
    {
        /// <summary>
        /// 
        /// </summary>
        public string Thang { get; set; }
        public string  Nam { get; set; }
        public TongHopSuCoVaSuatSuCoDuongDayVaTBAQuery()
        {    
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public TongHopSuCoVaSuatSuCoDuongDayVaTBAQuery(HttpRequest request) 
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class BaoCaoTinhHinhSuCoLuoiDienTrungHaApQuery : QuerySearch
    {
        /// <summary>
        /// 
        /// </summary>
        public int Thang { get; set; }
        public int Nam { get; set; }
        public BaoCaoTinhHinhSuCoLuoiDienTrungHaApQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public BaoCaoTinhHinhSuCoLuoiDienTrungHaApQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        } 
    }
    public class TongHopSuCoVaSuatSuCoDuongDayVaTBALuoiTrungHaAp : QuerySearch
    {
        /// <summary>
        /// 
        /// </summary>
        public int Thang { get; set; }
        public int Nam { get; set; }
        public TongHopSuCoVaSuatSuCoDuongDayVaTBALuoiTrungHaAp()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public TongHopSuCoVaSuatSuCoDuongDayVaTBALuoiTrungHaAp(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class DanhMucHoSoQuery : QuerySearch
    {
        public string LoaiDV { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DanhMucHoSoQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public DanhMucHoSoQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            } 
        }
    }

    public class MucLucVanBanTaiLieuQuery : QuerySearch
    {
        public string LoaiDV { get; set; }
        public string PhanLoaiVB { get; set; }
        public string PhanLoai { get; set; }
        public string KieuGuiNhan { get; set; }
        public string TuSo { get; set; }
        public string DenSo { get; set; }
        public string DonViText { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public MucLucVanBanTaiLieuQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public MucLucVanBanTaiLieuQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class SoVanBanDiQuery : QuerySearch
    {
        public string LoaiDV { get; set; }
        public string PhanLoaiVB { get; set; }
        public string PhanLoai { get; set; }
        public string KieuGuiNhan { get; set; }
        public string TuSo { get; set; }
        public string DenSo { get; set; }
        public string DonViText { get; set; }
        /// <summary>
        /// 
        /// </summary>   
        public SoVanBanDiQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public SoVanBanDiQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class MucLucHoSoQuery : QuerySearch
    {
        public string LoaiDV { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public MucLucHoSoQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public MucLucHoSoQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class DangKyGiuSoVanBanDiQuery : QuerySearch
    {
        public string LoaiDV { get; set; }
        public string TrangThai { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DangKyGiuSoVanBanDiQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public DangKyGiuSoVanBanDiQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class BaoCaoDienNangGNDNQuery : QuerySearch
    {
        /// <summary>
        /// 
        /// </summary>
        public string  Thang { get; set; }
        public string Nam { get; set; }
        public string MaDonVi { get; set; }
        public string LoaiBaoCao { get; set; }
        public BaoCaoDienNangGNDNQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public BaoCaoDienNangGNDNQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    } 
    public class TongHopBanDienCuaDVQuery : QuerySearch
    {
        public string SearchThang { get; set; }
        public string SearchNam { get; set; }
        public string LoaiBaoCao { get; set; }
        public string MaDonVi { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public TongHopBanDienCuaDVQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public TongHopBanDienCuaDVQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    } 
    public class TTDienNangDungTTvaPPCuaDVQuery : QuerySearch
    {
        public string SearchThang { get; set; }
        public string SearchNam { get; set; }
        public string LoaiBaoCao { get; set; }
        public string MaDonVi { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public TTDienNangDungTTvaPPCuaDVQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public TTDienNangDungTTvaPPCuaDVQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }

    public class TongHopSoCongToCuaDVQuery : QuerySearch
    {
        /// <summary>
        /// 
        /// </summary>
        public string  Nam { get; set; }
        public string Thang { get; set; }
        public string MaDonVi { get; set; }
        public TongHopSoCongToCuaDVQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public TongHopSoCongToCuaDVQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }  
    public class CMIS_TongHopSoHopDongMBDCacDVQuery : QuerySearch
    {
        /// <summary>
        /// 
        /// </summary>
        public string SearchThang { get; set; }
        public string SearchNam { get; set; }
        public string LoaiBaoCao { get; set; }
        public string MaDonVi { get; set; }
        public CMIS_TongHopSoHopDongMBDCacDVQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public CMIS_TongHopSoHopDongMBDCacDVQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    } 
    public class TongHopSoHDMuaBanDienCuaDVQuery : QuerySearch
    {
        public string SearchThang { get; set; }
        public string SearchNam { get; set; }
        public string MaDonVi { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public TongHopSoHDMuaBanDienCuaDVQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public TongHopSoHDMuaBanDienCuaDVQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    } 
    public class ChiTietBanDienTheoDTGQuery : QuerySearch
    {
        public string SearchThang { get; set; }
        public string SearchNam { get; set; }
        public string MaDonVi { get; set; } 
        public string LoaiBaoCao { get; set; } 
        /// <summary>
        /// 
        /// </summary>
        public ChiTietBanDienTheoDTGQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public ChiTietBanDienTheoDTGQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class ChiTietSoThuVaSoDuCacKPTQuery : QuerySearch
    {
        public string SearchThang { get; set; }
        public string SearchNam { get; set; }
        public string MaDonVi { get; set; } 
        public string LoaiBaoCao { get; set; } 
        /// <summary>
        /// 
        /// </summary>
        public ChiTietSoThuVaSoDuCacKPTQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public ChiTietSoThuVaSoDuCacKPTQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    } 
    public class ChiTietBanDienTheoTTPhuTaiQuery : QuerySearch
    {
        /// <summary>
        /// 
        /// </summary>
        public string  MaDonVi { get; set; }
        public int  Thang { get; set; }
        public int Nam { get; set; }
        public string LoaiBaoCao { get; set; }
        public ChiTietBanDienTheoTTPhuTaiQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public ChiTietBanDienTheoTTPhuTaiQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    } 
    public class CMIS_ChiTietBanDienTheoTPPTQuery : QuerySearch
    {
        /// <summary>
        /// 
        /// </summary>
        public string  MaDonVi { get; set; }
        public int  Thang { get; set; }
        public int Nam { get; set; }
        public string LoaiBaoCao { get; set; }
        public CMIS_ChiTietBanDienTheoTPPTQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public CMIS_ChiTietBanDienTheoTPPTQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    } 
    public class ChiTietBanDienTheoNganhNgheQuery : QuerySearch
    {
        public int LoaiBaoCao { get; set; }
        public string SearchThang { get; set; }
        public string SearchNam { get; set; }
        public string MaDonVi { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ChiTietBanDienTheoNganhNgheQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public ChiTietBanDienTheoNganhNgheQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    } 
    public class BaoCaoSanLuongTietKiemCacDVTTQuery : QuerySearch
    {
        public int LoaiBaoCao { get; set; }
        public string SearchThang { get; set; }
        public string SearchNam { get; set; }
        public string MaDonVi { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public BaoCaoSanLuongTietKiemCacDVTTQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public BaoCaoSanLuongTietKiemCacDVTTQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    
    // Linh Vuc Quan Tri 
      public class BaoCaoTrinhDoVienChucCMNVQuery : QuerySearch
    {
        public string LoaiDV { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public BaoCaoTrinhDoVienChucCMNVQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public BaoCaoTrinhDoVienChucCMNVQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    } 
    public class BCCoCauLaoDongQuery : QuerySearch
    {
        public int LoaiBaoCao { get; set; }
        public int KyBaoCao { get; set; }
        public int BaoCaoNam { get; set; }
        public string LoaiDV { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public BCCoCauLaoDongQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public BCCoCauLaoDongQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class BaoCaoChatLuongCongNhanKTQuery : QuerySearch
    {
        public int LoaiBaoCao { get; set; }
        public int KyBaoCao { get; set; }
        public int BaoCaoNam { get; set; }
        public string LoaiDV { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public BaoCaoChatLuongCongNhanKTQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public BaoCaoChatLuongCongNhanKTQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class BaoCaoLDThuNhapQuery : QuerySearch
    {
        public int LoaiBaoCao { get; set; }
        public int KyBaoCao { get; set; }
        public int BaoCaoNam { get; set; }
        public string LoaiDV { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public BaoCaoLDThuNhapQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public BaoCaoLDThuNhapQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class BaoCaoTangGiamLaoDongQuery : QuerySearch
    {
        public int LoaiBaoCao { get; set; }
        public int KyBaoCao { get; set; }
        public int BaoCaoNam { get; set; }
        public string LoaiDV { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public BaoCaoTangGiamLaoDongQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public BaoCaoTangGiamLaoDongQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    } 
    public class BaoCaoUocLaoDongQuery : QuerySearch
    {
        public int LoaiBaoCao { get; set; }
        public int KyBaoCao { get; set; }
        public int BaoCaoNam { get; set; }
        public string maDonvi { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public BaoCaoUocLaoDongQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public BaoCaoUocLaoDongQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }

    //Bao Cao Tai Chinh
    public class BaoCaoSanLuongDienQuery : QuerySearch
    {
        public string SearchNam { get; set; }
        public string QuyBaoCao { get; set; }
        public string LoaiDV { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public BaoCaoSanLuongDienQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public BaoCaoSanLuongDienQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class BaoCaoKetQuaSanXuatKinhDoanhQuery : QuerySearch
    {
        public string SearchNam { get; set; }
        public string QuyBaoCao { get; set; }
        public string LoaiDV { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public BaoCaoKetQuaSanXuatKinhDoanhQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public BaoCaoKetQuaSanXuatKinhDoanhQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class NguonVonDauTuQuery : QuerySearch
    {
        public string SearchNam { get; set; }
        public string ThangBaoCao { get; set; }
        public string LoaiDV { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public NguonVonDauTuQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public NguonVonDauTuQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class ChiTietNguonVonDauTuXayDungQuery : QuerySearch
    {
        public string SearchNam { get; set; }
        public string QuyBaoCao { get; set; }
        public string LoaiDV { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ChiTietNguonVonDauTuXayDungQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public ChiTietNguonVonDauTuXayDungQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    } 
    public class BCCongTrinhSuaChuaLonHTQuery : QuerySearch
    {
        public string SearchNam { get; set; }
        public string ThangBaoCao { get; set; }
        public string LoaiDV { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public BCCongTrinhSuaChuaLonHTQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public BCCongTrinhSuaChuaLonHTQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class TongHopChiPhiSanXuatKinhDoanhDienQuery : QuerySearch
    {
        public string SearchNam { get; set; }
        public string QuyBaoCao { get; set; }
        public string LoaiDV { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public TongHopChiPhiSanXuatKinhDoanhDienQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public TongHopChiPhiSanXuatKinhDoanhDienQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        } 
    }
    public class BCCongTinhSuaChuaLonDoDangQuery : QuerySearch
    {
        public string QuyBaoCao { get; set; }
        public string SearchNam { get; set; }
        public string LoaiDV { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public BCCongTinhSuaChuaLonDoDangQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public BCCongTinhSuaChuaLonDoDangQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        } 
    }
    public class NguonVonVayQuery : QuerySearch
    {
        public string SearchNam { get; set; }
        public string ThangBaoCao { get; set; }
        public string LoaiDV { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public NguonVonVayQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public NguonVonVayQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class BCCongTrinhThuocNguonVonSuaChuaLonQuery : QuerySearch
    {
        public string ThangBaoCao { get; set; }
        public string SearchNam { get; set; }
        public string LoaiDV { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public BCCongTrinhThuocNguonVonSuaChuaLonQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public BCCongTrinhThuocNguonVonSuaChuaLonQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class ERP_BCCongTrinhSCLQuery : QuerySearch
    {
        public string ThangBaoCao { get; set; }
        public string SearchNam { get; set; }
        public string LoaiDV { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ERP_BCCongTrinhSCLQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public ERP_BCCongTrinhSCLQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
       public class CTHMCongTrinhHoanThanhBanGiaoQuery : QuerySearch
    {
        public string ThangBaoCao { get; set; }
        public string SearchNam { get; set; }
        public string LoaiDV { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public CTHMCongTrinhHoanThanhBanGiaoQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public CTHMCongTrinhHoanThanhBanGiaoQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class ThucHienDauTuXayDungCoBanQuery : QuerySearch
    {
        public string ThangBaoCao { get; set; }
        public string SearchNam { get; set; }
        public string LoaiDV { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ThucHienDauTuXayDungCoBanQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public ThucHienDauTuXayDungCoBanQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class ThucHienDauTuTheoCongTrinhHangMucCongTrinhQuery : QuerySearch
    {
        public string ThangBaoCao { get; set; }
        public string SearchNam { get; set; }
        public string LoaiDV { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ThucHienDauTuTheoCongTrinhHangMucCongTrinhQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public ThucHienDauTuTheoCongTrinhHangMucCongTrinhQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class NhatKyMatDienQuery : QuerySearch
    {
        /// <summary>
        /// 
        /// </summary>
        public NhatKyMatDienQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public NhatKyMatDienQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class DanhSacCongTrinhTheoKeHoachKCQuery : QuerySearch
    {
        /// <summary>
        /// 
        /// </summary>
        public string Nam { get; set; }
        public string MaDonVi { get; set; }
        public DanhSacCongTrinhTheoKeHoachKCQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public DanhSacCongTrinhTheoKeHoachKCQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class DanhSacCongTrinhTheoKeHoachDongDienQuery : QuerySearch
    {
        /// <summary>
        /// 
        /// </summary>
        public string Nam { get; set; }
        public string MaDonVi { get; set; }
        public DanhSacCongTrinhTheoKeHoachDongDienQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public DanhSacCongTrinhTheoKeHoachDongDienQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class DanhSacCongTrinhTheoThucTeKCQuery : QuerySearch
    {
        /// <summary>
        /// 
        /// </summary>
        public string Nam { get; set; }
        public DanhSacCongTrinhTheoThucTeKCQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public DanhSacCongTrinhTheoThucTeKCQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class DanhSacCongTrinhTheoThucTeDongDienQuery : QuerySearch
    {
        /// <summary>
        /// 
        /// </summary>
        public string Nam { get; set; }
        public DanhSacCongTrinhTheoThucTeDongDienQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public DanhSacCongTrinhTheoThucTeDongDienQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }

    public class TongHopDauTuXDQuery : QuerySearch
    {
        /// <summary>
        /// 
        /// </summary>
        public string Nam { get; set; }
        public TongHopDauTuXDQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public TongHopDauTuXDQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class ThuHienKeHoachDTXDQuery : QuerySearch
    {
        /// <summary>
        /// 
        /// </summary>
        public string Nam { get; set; }
        public string thang { get; set; }
        public string maDonVi { get; set; }
        public ThuHienKeHoachDTXDQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public ThuHienKeHoachDTXDQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    } 
    public class XeTaiCau_BCTHChiPhiSuDungXeTaiCauQuery : QuerySearch
    {
        /// <summary>
        /// 
        /// </summary>
        public string Nam { get; set; }
        public string Thang { get; set; }
        public string maDonVi { get; set; }
        public XeTaiCau_BCTHChiPhiSuDungXeTaiCauQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public XeTaiCau_BCTHChiPhiSuDungXeTaiCauQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
     
    public class BaoCaoSuDungXeTaiCauQuery : QuerySearch
    {
        /// <summary>
        /// 
        /// </summary>
        public string Nam { get; set; }
        public string thang { get; set; }
        public string MaDonVi { get; set; }
        public string DonViText { get; set; }
        public BaoCaoSuDungXeTaiCauQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public BaoCaoSuDungXeTaiCauQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }  
    public class ERP_BCNguonVonDauTuJQuery : QuerySearch
    {
        /// <summary>
        /// 
        /// </summary>
        public string SearchNam { get; set; }
        public string ThangBaoCao { get; set; }
        public string LoaiDV { get; set; }
        public ERP_BCNguonVonDauTuJQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public ERP_BCNguonVonDauTuJQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    } 
    public class DTXD_BaoCaoTongHopDTXDQuery : QuerySearch
    {
        /// <summary>
        /// 
        /// </summary>
        public string Nam { get; set; }
        public string thang { get; set; }
        public string maDonVi { get; set; }
        public DTXD_BaoCaoTongHopDTXDQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public DTXD_BaoCaoTongHopDTXDQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class TongHopTienDoKhoiCongDonDienQuery : QuerySearch
    {
        /// <summary>
        /// 
        /// </summary>
        public string Nam { get; set; }      
        public TongHopTienDoKhoiCongDonDienQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public TongHopTienDoKhoiCongDonDienQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class BKH_TongHopSuCoVaSuatSuCoDuongDayVaTBAQuery : QuerySearch
    {
        /// <summary>
        /// 
        /// </summary>
        public string Thang { get; set; }
        public string Nam { get; set; }
        public BKH_TongHopSuCoVaSuatSuCoDuongDayVaTBAQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public BKH_TongHopSuCoVaSuatSuCoDuongDayVaTBAQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class BangSoSanhGiaTriTonKhoDTXDQuery : QuerySearch
    {
        /// <summary>
        /// 
        /// </summary>
        public string Thang { get; set; }
        public string Nam { get; set; }
        public string MaDonVi { get; set; }
        public string DonViText { get; set; }
        public BangSoSanhGiaTriTonKhoDTXDQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public BangSoSanhGiaTriTonKhoDTXDQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class BangSoSanhGiaTriTonKhoSXKDQuery : QuerySearch
    {
        /// <summary>
        /// 
        /// </summary>
        public string Thang { get; set; }
        public string Nam { get; set; }
        public string MaDonVi { get; set; }
        public string DonViText { get; set; }
        public BangSoSanhGiaTriTonKhoSXKDQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public BangSoSanhGiaTriTonKhoSXKDQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
    public class ERP_BCTuoiThoHangTonKho_DTXDQuery : QuerySearch
    {
        /// <summary>
        /// 
        /// </summary>
        public string Thang { get; set; }
        public string Nam { get; set; }
        public string MaDonVi { get; set; }
        public ERP_BCTuoiThoHangTonKho_DTXDQuery()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public ERP_BCTuoiThoHangTonKho_DTXDQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
}
