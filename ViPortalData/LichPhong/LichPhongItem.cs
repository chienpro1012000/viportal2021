﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.JSGrid;
using ViPortal_Utils.Base;

namespace ViPortalData.LichPhong
{
	public class LichPhongItem : SPEntity
	{
		public SPFieldLookupValue fldGroup { set; get; }
		public string LichDiaDiem { set; get; }
		public string LichGhiChu { set; get; }
		public SPFieldLookupValue LichLanhDaoChuTri { set; get; }
		public string LichNoiDung { set; get; }
		public SPFieldLookupValueCollection LichThanhPhanThamGia { set; get; }
		public string NoiDungChuanBi { set; get; }
		public DateTime? LichThoiGianBatDau { set; get; }
		public DateTime? LichThoiGianKetThuc { set; get; }
		public int LichTrangThai { set; get; }
		public SPFieldLookupValueCollection LichPhongBanThamGia { set; get; }
		public string CHU_TRI { set; get; }
		public string OldID { get; set; } //luu lịch của tổng cty khi là lịch giao xuống
		public string CHUAN_BI { set; get; }
		public string THANH_PHAN { set; get; }
		public int DBPhanLoai { get; set; }
		//public int DBPhanLoai { get; set; }
		public string LogText { get; set; }
		public string LogNoiDung { get; set; }
		//lưu thông tin giao lịch của phòng nếu có
		public string TBNoiDung { get; set; }
		public LichPhongItem()
		{
			fldGroup = new SPFieldLookupValue();
			LichLanhDaoChuTri = new SPFieldLookupValue();
			LichThanhPhanThamGia = new SPFieldLookupValueCollection();
			LichPhongBanThamGia = new SPFieldLookupValueCollection();
			LichGhiChu = string.Empty;
			LichNoiDung = string.Empty;
		}
	}
	public class LichPhongJson : EntityJson
	{
		public LookupData fldGroup { set; get; }
		public string LichDiaDiem { set; get; }
		public string LichGhiChu { set; get; }
		public LookupData LichLanhDaoChuTri { set; get; }
		public int DBPhanLoai { get; set; }
		public string LichNoiDung { set; get; }
		public List<LookupData> LichThanhPhanThamGia { set; get; }
		public List<LookupData> LichPhongBanThamGia { set; get; }
		public string NoiDungChuanBi { set; get; }
		public DateTime? LichThoiGianBatDau { set; get; }
		public DateTime? LichThoiGianKetThuc { set; get; }
		//lưu thông tin giao lịch của phòng nếu có
		public string TBNoiDung { get; set; }
		public string LogText { get; set; }
		public string CHU_TRI { set; get; }
		public string THANH_PHAN { set; get; }
		//public int DBPhanLoai { get; set; }
		public string THANH_PHAN_Title
		{
			get
			{
				string _THANH_PHAN_Title = "";
				if (!string.IsNullOrEmpty(LogText) && LogText.Contains("_CreatedUser"))
				{
					//lịch tự đk
					_THANH_PHAN_Title = $"{((LichLanhDaoChuTri != null && !string.IsNullOrEmpty(LichLanhDaoChuTri.Title)) ? (" ," + LichLanhDaoChuTri.Title) : "")} {(LichPhongBanThamGia != null ? (" ," + string.Join(",", LichPhongBanThamGia.Select(x => x.Title))) : "")}{(LichPhongBanThamGia != null ? (", " + string.Join(",", LichThanhPhanThamGia.Select(x => x.Title))) : "")}{(!string.IsNullOrWhiteSpace(THANH_PHAN) ? ("," + THANH_PHAN) : "")}";
				}
				else
				{
					//lịch đồng bộ.
					_THANH_PHAN_Title = $"{((LichLanhDaoChuTri != null && !string.IsNullOrEmpty(LichLanhDaoChuTri.Title)) ? (LichLanhDaoChuTri.Title) : "")}{(LichPhongBanThamGia != null ? (", " + string.Join(",", LichThanhPhanThamGia.Select(x => x.Title))) : "")} {(LichPhongBanThamGia != null ? (" ," + string.Join(",", LichPhongBanThamGia.Select(x => x.Title))) : "")}";
				}
				if (_THANH_PHAN_Title.Trim().StartsWith(","))
				{
					_THANH_PHAN_Title = _THANH_PHAN_Title.Trim().Substring(1);
				}
				return _THANH_PHAN_Title;
			}
		}
		public string CHUAN_BI { set; get; }
		public string CHUAN_BI_Title
		{
			get
			{
				if (!string.IsNullOrEmpty(CHUAN_BI))
				{
					if (!string.IsNullOrEmpty(LogText) && LogText.Contains("_CreatedUser"))
					{
						List<LookupData> CHUAN_BIObj = clsFucUtils.GetDanhSachMutilLoookupByStringFiledData(CHUAN_BI);
						return string.Join(",", CHUAN_BIObj.Select(x => x.Title));
					}
					else
					{
						return CHUAN_BI;
					}
				}
				else return "";
			}
		}
		public string CHU_TRI_Title
		{
			get
			{
				if (!string.IsNullOrEmpty(CHU_TRI))
				{
					if (!string.IsNullOrEmpty(LogText) && LogText.Contains("_CreatedUser"))
					{
						LookupData CHU_TRILookup = clsFucUtils.GetLoookupByStringFiledData(CHU_TRI);
						return CHU_TRILookup.Title;
					}
					else
					{
						return CHU_TRI;
					}
				}
				else return "";
			}
		}
		public int LichTrangThai { set; get; }
		public LichPhongJson()
		{
			fldGroup = new LookupData();
			LichLanhDaoChuTri = new LookupData();
			LichThanhPhanThamGia = new List<LookupData>();
			LichPhongBanThamGia = new List<LookupData>();
		}
	}
}