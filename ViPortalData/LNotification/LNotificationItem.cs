using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{
    public class LNotificationItem : SPEntity
    {
        public string LNotiDaXem { set; get; }
        public int LNotiIsSentFirebase { set; get; }
        public int LNotiIsSentMail { set; get; }
        public string DoiTuongTitle { get; set; }
        public string LNotiMoTa { set; get; }
        public string LNotiNguoiGui { set; get; }
        public string LNotiNguoiNhan { set; get; }
        public string LNotiNguoiNhan_Id { set; get; }
        public string LNotiChuaXem_Id { get; set; }
        public string LNotiNoiDung { set; get; }
        public DateTime? LNotiSentTime { set; get; }
        public string LNotiData { get; set; }
        public string TitleBaiViet { get; set; }
        public string UrlLink { get; set; }

        public LNotificationItem()
        {

        }
    }

    public class LNotificationJson : EntityJson
    {
        public string LNotiDaXem { set; get; }
        public int LNotiIsSentFirebase { set; get; }
        public int LNotiIsSentMail { set; get; }
        public string LNotiMoTa { set; get; }
        public string LNotiNguoiGui { set; get; }
        public string LNotiNguoiNhan { set; get; }
        public string LNotiNguoiNhan_Id { set; get; }
        public string LNotiNoiDung { set; get; }
        public DateTime? LNotiSentTime { set; get; }
        public string DoiTuongTitle { get; set; }
        public string LNotiData { get; set; }
        public string TitleBaiViet { get; set; }

        public bool isDaXem { get; set; }
        public string UrlLink { get; set; }
        public LNotificationJson()
        {

        }
    }
}
