﻿using Microsoft.SharePoint;
using SolrNet;
using SolrNet.Commands.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public class LNotificationDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/noidung/Lists/LNotification";
        public LNotificationDA(bool isAdmin = false)
        {
            if (isAdmin)
                InitAdmin(_urlList);
            else
                Init(_urlList);
        }
        public LNotificationDA(string _urlSiteProcess, bool isAdmin = false)
        {
            _urlList = _urlSiteProcess + _urlList;
            if (isAdmin)
                InitAdmin(_urlList);
            else
                Init(_urlList);
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<LNotificationJson> GetListJson(LNotificationQuery searhOption, params string[] Fields)
        {
            List<LNotificationJson> lstReturn = new List<LNotificationJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "LNotiIsSentMail", "DoiTuongTitle", "LNotiData", "LNotiDaXem",
                    "LNotiNguoiNhan", "LNotiSentTime", "LNotiMoTa", "LNotiNguoiNhan_Id", "LNotiNguoiGui", "TitleBaiViet","UrlLink" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<LNotificationJson>(SPcoll, Fields);
            return lstReturn;
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public int GetCount(LNotificationQuery searhOption)
        {
            SPQuery oQuery = new SPQuery();
            oQuery.ViewFields = BuildViewField("ID");
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = SpListProcess.GetItems(oQuery);            
            return SPcoll.Count;
        }

        public string BuildQuery(LNotificationQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if(searhOption.LNotiChuaXem_Id > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.LNotiNguoiNhan_Id> 0)
                oBuildQuery.Append("<And>");
            if (searhOption.isSendMail)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            if(searhOption.TuNgay.HasValue)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption.TuNgay.HasValue)
            {
                oBuildQuery.Append(GetStringGeqDate("Created", searhOption.TuNgay.Value));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            if (searhOption.isSendMail)
            {
                oBuildQuery.Append(getstringNumEQ("LNotiIsSentMail", 0));

                oBuildQuery.Append("</And>");
            }
            if (searhOption.LNotiNguoiNhan_Id > 0)
            {
                oBuildQuery.Append(GetStringContainsText("LNotiNguoiNhan_Id", searhOption.LNotiNguoiNhan_Id + ""));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.LNotiChuaXem_Id > 0)
            {
                oBuildQuery.Append(GetStringContainsText("LNotiChuaXem_Id", searhOption.LNotiNguoiNhan_Id + ""));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
        public List<ISolrQuery> BuildQuerySolr(LNotificationQuery searhOption)
        {
            var Querys = new List<ISolrQuery>();
            Querys.Add(new SolrQuery("*:*"));
            if (searhOption.UrlList != null && searhOption.UrlList.Count > 0)
            {
                //UrlList:("/noidung/Lists/VanBanTaiLieuSuKien" "/noidung/Lists/VanBanTaiLieu")
                Querys.Add(new SolrQuery(string.Format("FullUrlList:({0})", string.Join(" ", searhOption.UrlList.Select(x => "\"" + x + "\"")))));
            }
            else if (SpListProcess != null)
            {
                Querys.Add(new SolrQuery(string.Format("FullUrlList:\"{0}\"", SpListProcess.ParentWeb.ServerRelativeUrl + "/" + SpListProcess.RootFolder.Url)));
            }
            Querys.AddSearchBase(searhOption);
            if (searhOption.LNotiNguoiNhan_Id > 0)
                Querys.Add(new SolrQuery(string.Format("LNotiNguoiNhan_Id:\",{0},\"", searhOption.LNotiNguoiNhan_Id + "")));
            if (searhOption.LNotiChuaXem_Id > 0)
            {
                Querys.Add(new SolrQuery(string.Format("LNotiChuaXem_Id:\"*{0}*\"", searhOption.LNotiChuaXem_Id + "")));
            }
            if (searhOption.isSendMail)
            {
                Querys.Add(new SolrQuery(string.Format("LNotiIsSentMail:\"*{0}*\"", 0)));
            }
            if (searhOption.TuNgay.HasValue)
            {
                Querys.Add(new SolrQuery(GetQueryDateTimeSolrGeq("Created", searhOption.TuNgay.Value)));
            }
            BuildQueryToString(Querys);
            return Querys;
        }
        public List<LNotificationJson> GetListJsonSolr(LNotificationQuery searhOption, params string[] Fields)
        {
            List<LNotificationJson> lstReturn = new List<LNotificationJson>();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "SP_ID", "Title", "LNotiIsSentMail", "DoiTuongTitle", "LNotiData", "LNotiDaXem",
                    "LNotiNguoiNhan", "LNotiSentTime", "LNotiMoTa", "LNotiNguoiNhan_Id", "LNotiNguoiGui", "TitleBaiViet","Url"}; //Set default
            }
            var Querys = BuildQuerySolr(searhOption);
            #region Option khởi tạo
            var oorders = new List<SortOrder>();

            if (oorders.FindIndex(x => x.FieldName == searhOption.FieldOrder) == -1)
                oorders.Add(new SortOrder(searhOption.FieldOrder == "ID" ? "id" : searhOption.FieldOrder, (searhOption.Ascending) ? Order.ASC : Order.DESC));
            QueryOptions options = new QueryOptions()
            {
                OrderBy = oorders,
                Fields = Fields,
                //StartOrCursor =  new StartOrCursor.Start((currentPage - 1) * rowPerPage)
            };
            if (searhOption.Length > 0)
            {
                options.Rows = searhOption.Length;
                options.Start = searhOption.Start; //Barn ghi dau tien
            }

            #endregion
            var result = solrWorkerDM.Query(new SolrMultipleCriteriaQuery(Querys, "AND"), options);
            TongSoBanGhiSauKhiQuery = Convert.ToInt32(result.NumFound);
            foreach (DanhMucSolr itemsolr in result)
            {
                lstReturn.Add(ConvertSolrToJson<LNotificationJson>(itemsolr, Fields));
            }
            return lstReturn;
        }
    }
}