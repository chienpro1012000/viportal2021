using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{
    public class DMCaTrucItem : SPEntity
    {
        public string ThoiGianBatDau { set; get; }
        public string ThoiGianKetThuc { set; get; }
        public int LoaiCaTruc { set; get; }
        public SPFieldLookupValue fldGroup { get; set; }

        public DMCaTrucItem()
        {
            fldGroup = new SPFieldLookupValue();
        }
    }

    public class DMCaTrucJson : EntityJson
    {
        public string ThoiGianBatDau { set; get; }
        public string ThoiGianKetThuc { set; get; }
        public int LoaiCaTruc { set; get; }
        public LookupData fldGroup { get; set; }
        public DMCaTrucJson()
        {
            fldGroup = new LookupData();
        }
    }
}
