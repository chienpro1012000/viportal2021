using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{
    public class DMTrangThaiItem : SPEntity
    {
        public string DMVietTat { set; get; }

        public DMTrangThaiItem()
        {

        }
    }

    public class DMTrangThaiJson : EntityJson
    {
        public string DMVietTat { set; get; }

        public DMTrangThaiJson()
        {

        }
    }
}
