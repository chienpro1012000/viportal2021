﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.JSGrid;
using ViPortal_Utils.Base;

namespace ViPortalData.DMHinhAnh
{
	public class DMHinhAnhItem : SPEntity
	{
		public int DMSTT { set; get; }
		public string DMMoTa { set; get; }
		public string ImageNews { set; get; }
		public string DMAnhListAnh { set; get; }
		public DateTime? TBNgayDang { set; get; }
		public SPFieldLookupValue CreatedUser { get; set; }
		public int DMSLAnh { set; get; }
		public DMHinhAnhItem()
		{
			CreatedUser = new SPFieldLookupValue();
		}
	}
	public class DMHinhAnhJson : EntityJson
	{
		public int DMSTT { set; get; }
		public string DMMoTa { set; get; }
		public string ImageNews { set; get; }
		public string DMAnhListAnh { set; get; }
		public DateTime? TBNgayDang { set; get; }
		public int DMSLAnh { set; get; }
		public LookupData CreatedUser { get; set; }
		public DMHinhAnhJson()
		{
			CreatedUser = new LookupData();
		}
	}

	public class categoryImagesEVN
    {
		public int id { get; set; }
		public int total { get; set; }
		public string name { get; set; }
		public string url { get; set; }
	}
	public class ImagesEVN
	{
		public string url { get; set; }
	}
	public class VideoEVN
    {
		public string name { get; set; }
		public string url { get; set; }
		public string thumbnail { get; set; }
		public DateTime createdDate { get; set; }

	}
}