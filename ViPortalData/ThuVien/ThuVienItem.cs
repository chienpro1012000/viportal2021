﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;


namespace ViPortalData.ThuVien
{
	public class ThuVienItem : SPEntity
	{
		public string MaSach { set; get; }
		public string NhaXB { set; get; }
		public SPFieldLookupValueCollection NguoiMuon { set; get; }
		public DateTime? NgayMuon { set; get; }
		public DateTime? NgayTra { set; get; }
		public string SoDT { set; get; }
		public string GhiChu { set; get; }
		public string TrangThai { set; get; }

		//public SPFieldLookupValue LookUp { set; get; }
		// lookup nhiều	public SPFieldLookupValueCollection BBB { set; get; }
		public ThuVienItem()
		{
			NguoiMuon = new SPFieldLookupValueCollection();
			MaSach = string.Empty;
			NhaXB = string.Empty;
			SoDT = string.Empty;	
			GhiChu = string.Empty;	
		}
	}
	public class ThuVienJson : EntityJson
	{
		public string MaSach { set; get; }
		public string NhaXB { set; get; }
		public List<LookupData> NguoiMuon { set; get; }

		public DateTime? NgayMuon { set; get; }
		public DateTime? NgayTra { set; get; }
		public string SoDT { set; get; }
		public string GhiChu { set; get; }
		public string TrangThai { set; get; }
		//public LookupData LookUp { set; get; }
		// lookup nhiều	public  List<LookupData> SSs { set; get; }
		public ThuVienJson()
		{
			MaSach = string.Empty;
			NhaXB = string.Empty;
			SoDT = string.Empty;
			GhiChu = string.Empty;
		}
	}
}
