using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortalData
{
    public class LPermissionDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/Lists/LPermission";
        public LPermissionDA()
        {
            Init(_urlList);
        }
        public LPermissionDA(string _urlListProcess)
            : base(_urlListProcess)
        {
            _urlList = _urlListProcess;
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<LPermissionJson> GetListJson(LPermissionQuery searhOption, params string[] Fields)
        {
            List<LPermissionJson> lstReturn = new List<LPermissionJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "MaQuyen", "PhanLoaiQuyen" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, 0, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<LPermissionJson>(SPcoll, Fields);
            return lstReturn;
        }

        public int GetCount(LPermissionQuery searhOption)
        {
            SPQuery oQuery = new SPQuery();
            oQuery.ViewFields = BuildViewField("ID");
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = SpListProcess.GetItems(oQuery);
            return SPcoll.Count;
        }

        public string BuildQuery(LPermissionQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            //if (searhOption.PhanLoaiQuyen > -1)
            //    oBuildQuery.Append("<And>");
            if (!string.IsNullOrEmpty(searhOption.MaQuyen))
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption.isGetBylistID)
            {
                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));
                oBuildQuery.Append("</And>");
            }
            if (!string.IsNullOrEmpty(searhOption.MaQuyen))
            {
                oBuildQuery.Append(GetStringStartWith("MaQuyen", searhOption.MaQuyen));
                oBuildQuery.Append("</And>");
            }
            //if (searhOption.PhanLoaiQuyen > -1)
            //{
            //    oBuildQuery.Append(GetStringEQNumber("PhanLoaiQuyen", searhOption.PhanLoaiQuyen));
            //    oBuildQuery.Append("</And>");
            //}
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}