using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using ViPortal_Utils.Base;

namespace ViPortalData
{
    public class LPermissionQuery : QuerySearch
    {
        public List<int> ListPermissCheck { get; set; }
        public string MaQuyen { get; set; }
        public int PhanLoaiQuyen { get; set; }
        public LPermissionQuery()
        {
            ListPermissCheck = new List<int>();
        }
        public LPermissionQuery(HttpRequest request)
            : base(request)
        {
            ListPermissCheck = new List<int>();
            DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
}