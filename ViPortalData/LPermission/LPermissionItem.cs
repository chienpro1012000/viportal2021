using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace ViPortalData
{
    public class LPermissionItem : SPEntity
    {
        public string MaQuyen { set; get; }
        //public int PhanLoaiQuyen { set; get; }

        public LPermissionItem()
        {

        }
    }

    public class LPermissionJson : EntityJson
    {
        public string MaQuyen { set; get; }
        //public int PhanLoaiQuyen { set; get; }
        public LPermissionJson()
        {

        }
    }
}
