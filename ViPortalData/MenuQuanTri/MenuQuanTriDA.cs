﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public class MenuQuanTriDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/cms/Lists/MenuQuanTri";
        public MenuQuanTriDA(bool isAdmin = false)
        {
            if (isAdmin)
                InitAdmin(_urlList);
            else
                Init(_urlList);
        }
        public MenuQuanTriDA(string _urlSiteProcess)
            : base(_urlSiteProcess + "/cms/Lists/MenuQuanTri")
        {
            _urlList = _urlSiteProcess;
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param> 
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<MenuQuanTriJson> GetListJson(MenuQuanTriQuery searhOption, params string[] Fields)
        {
            List<MenuQuanTriJson> lstReturn = new List<MenuQuanTriJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {

                Fields = new string[] { "ID", "Title", "MenuLink", "MenuShowSubPortal", "MenuParent", "MenuIcon", "MenuPublic", "_ModerationStatus", "MenuIndex" , "Permissions", "PhanLoaiMenu" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<MenuQuanTriJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(MenuQuanTriQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if (searhOption.MenuParent == -1) //MenuParent null
                oBuildQuery.Append("<And>");
            if (searhOption.MenuParent >  0)
                oBuildQuery.Append("<And>");
            if (searhOption.MenuShowPortal )
                oBuildQuery.Append("<And>");
            if (searhOption.MenuShowSubPortal )
                oBuildQuery.Append("<And>");
            if (searhOption.PhanLoaiMenu > -1)
                oBuildQuery.Append("<And>");
            if (searhOption.lstPermistion.Count > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.MenuPublic > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            if (searhOption._ModerationStatus == 1)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption._ModerationStatus == 1)
            {
                oBuildQuery.Append(GetStringModer(0)); //appro là 0
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            if (searhOption.MenuPublic > 0)
            {
                if (searhOption.MenuPublic == 1)
                    oBuildQuery.Append(GetStringBoolean("MenuPublic", 1));
                else oBuildQuery.Append(GetStringBoolean("MenuPublic", 2));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.lstPermistion.Count > 0)
            {
                oBuildQuery.Append("<Or>");
                oBuildQuery.Append(getstringNull( "Permissions"));
                oBuildQuery.Append(getStringMultiLookup(searhOption.lstPermistion, "Permissions"));
                oBuildQuery.Append("</Or>");
                oBuildQuery.Append("</And>");
            }
            if (searhOption.PhanLoaiMenu > -1)
            {
                oBuildQuery.Append(GetStringEQNumber("PhanLoaiMenu", searhOption.PhanLoaiMenu));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.MenuShowSubPortal)
            {
                oBuildQuery.Append(GetStringBoolean("MenuShowSubPortal", 1));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.MenuShowPortal)
            {
                oBuildQuery.Append(GetStringBoolean("MenuShowPortal", 1));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.MenuParent > 0)
            {
                oBuildQuery.Append(GetStringEQLookUp("MenuParent", searhOption.MenuParent));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.MenuParent == -1) //MenuParent null
            {
                oBuildQuery.Append(getstringNull("MenuParent"));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}