using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public class MenuQuanTriQuery : QuerySearch
    {
        public int PhanLoaiMenu { get; set; }
        public int MenuPublic { get; set; }
        public int MenuParent { get; set; }
        public List<int> lstPermistion { get; set; }
        public bool MenuShowSubPortal { get; set; }
        public bool MenuShowPortal { get; set; }
        public MenuQuanTriQuery()
        {
            lstPermistion = new List<int>();
        }
        public MenuQuanTriQuery(HttpRequest request)
            : base(request)
        {
            lstPermistion = new List<int>();
            DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public | 
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                if (!string.IsNullOrEmpty(request[pinfo.Name]))
                {
                    var values = request[pinfo.Name];
                    switch (pinfo.PropertyType.FullName)
                    {
                        case "System.Int32":
                            if (!string.IsNullOrEmpty(values))
                            {
                                pinfo.SetValue(this, System.Convert.ToInt32(values), null);
                            }
                            break;
                        case "System.Nullable`1[[System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]":
                        case "System.DateTime ?":
                        case "System.DateTime":
                            if (!string.IsNullOrEmpty(values))
                            {
                                dtfi.ShortDatePattern = "MM/dd/yyyy";
                                dtfi.DateSeparator = "/";
                                DateTime dtTemp = System.Convert.ToDateTime(values, dtfi);
                                if (dtTemp.Year < 1900 && dtTemp.Year > 2099)
                                    dtTemp = new DateTime(2012, 2, 30);
                                pinfo.SetValue(this, ((DateTime)dtTemp), null);
                            }
                            else
                            {
                                object dateNullValue = null;
                                pinfo.SetValue(this, dateNullValue, null);
                            }
                            break;

                        default:
                        case "System.String":
                            if (!string.IsNullOrEmpty(values))
                            {
                                pinfo.SetValue(this, (values).Trim(), null);
                            }
                            else
                            {
                                pinfo.SetValue(this, "", null);
                            }
                            break;
                    }
                }
            }
        }
    }
}