using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{
    public class MenuQuanTriItem : SPEntity
    {
        public string MenuLink { set; get; }
        public bool MenuNewWindow { set; get; }
        public int MenuIndex { set; get; }
        public int PhanLoaiMenu { set; get; }
        public bool MenuShow { set; get; }
        public bool MenuPublic { get; set; }
        public bool CheckLogin { set; get; }
        public SPFieldLookupValue MenuParent { set; get; }
        public string MenuIcon { set; get; }
        public bool MenuShowSubPortal { get; set; }
        public bool MenuShowPortal { get; set; }
        public SPFieldLookupValueCollection Permissions { set; get; }

        public MenuQuanTriItem()
        {
            MenuParent = new SPFieldLookupValue();
            Permissions = new SPFieldLookupValueCollection();

        }
    }
    public class MenuQuanTriJson : EntityJson {
        public string MenuLink { set; get; }
        public bool MenuNewWindow { set; get; }
        public int MenuIndex { set; get; }
        public int CountMenuSub { set; get; }
        public int PhanLoaiMenu { set; get; }
        public bool MenuPublic { get; set; }
        public bool MenuShow { set; get; }
        public bool CheckLogin { set; get; }
        public LookupData MenuParent { set; get; }
        public string MenuIcon { set; get; }
        public bool MenuShowSubPortal { get; set; }
        public List<LookupData> Permissions { set; get; }
        public bool MenuShowPortal { get; set; }
        public MenuQuanTriJson()
        {
            MenuParent = new LookupData();
        }
    }
}
