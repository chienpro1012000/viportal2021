﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;

namespace ViPortalData.KSBoChuDe
{
	public class KSBoChuDeItem : SPEntity
	{
		public string FAQCauHoi { set; get; }
		public DateTime? KSDenNgay { set; get; }
		public string UCThamGia { get; set; }
		public DateTime? KSTuNgay { set; get; }
		public string DMDescription { set; get; }
		public bool DMHienThi { set; get; }
		public int DMSTT { set; get; }
		public KSBoChuDeItem()
		{
			UCThamGia = string.Empty;
		}
	}
	public class KSBoChuDeJson : EntityJson
	{
		public string FAQCauHoi { set; get; }
		public string UCThamGia { get; set; }
		public DateTime? KSDenNgay { set; get; }
		public DateTime? KSTuNgay { set; get; }
		public string DMDescription { set; get; }
		public string TrangThaiText {
			get
			{
				if (KSDenNgay.Value.Date >= DateTime.Now.Date)
					return "Đang diễn ra";
				else
					return "Đã kết thúc";
			}
		}
		
		public bool DMHienThi { set; get; }
		public int DMSTT { set; get; }
		public KSBoChuDeJson()
		{
			
		}
	}
}