﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using ViPortal_Utils.Base;

namespace ViPortalData.KSBoChuDe
{
    public class KSBoChuDeQuery : QuerySearch
    {
        public DateTime? KSTuNgay { get; set; }
        public DateTime? KSDenNgay { get; set; }
        public bool DMHienThi { get; set; }
        public KSBoChuDeQuery()
        {
        }
        public KSBoChuDeQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
}