﻿using Microsoft.SharePoint;
using SolrNet;
using SolrNet.Commands.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortalData.KSBoChuDe
{
    public class KSBoChuDeDA : SPBaseDA<DataSolr>
    {
        private string _urlList = "/noidung/Lists/KSBoChuDe";
        public KSBoChuDeDA(bool isAdmin = false)
        {
            if (isAdmin)
                InitAdmin(_urlList);
            else
                Init(_urlList);
        }
        public KSBoChuDeDA(string _urlSiteProcess)
            : base(_urlSiteProcess + "/noidung/Lists/KSBoChuDe",true)
        {
            _urlList = _urlSiteProcess;
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<KSBoChuDeJson> GetListJson(KSBoChuDeQuery searhOption, params string[] Fields)
        {
            List<KSBoChuDeJson> lstReturn = new List<KSBoChuDeJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "DMSTT", "DMHienThi", "DMDescription", "KSTuNgay", "UCThamGia",
                    "KSDenNgay", "_ModerationStatus", "TrangThaiText" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<KSBoChuDeJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(KSBoChuDeQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>"); 
            if (searhOption.DMHienThi)
                oBuildQuery.Append("<And>");
            if (searhOption.KSDenNgay.HasValue)
                oBuildQuery.Append("<And>");
            if (searhOption.KSTuNgay.HasValue)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            if (searhOption._ModerationStatus == 1)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption._ModerationStatus == 1)
            {
                oBuildQuery.Append(GetStringModer(0)); //appro là 0
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetBylistID)
            {
                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.KSTuNgay.HasValue)
            {
                oBuildQuery.Append(GetStringCamlDate("KSTuNgay", searhOption.KSTuNgay.Value, "Geq"));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.KSDenNgay.HasValue)
            {
                oBuildQuery.Append(GetStringCamlDate("KSDenNgay", searhOption.KSDenNgay.Value, "Leq"));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.DMHienThi)
            {
                oBuildQuery.Append(GetStringBoolean("DMHienThi", 1));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
        public List<ISolrQuery> BuildQuerySolr(KSBoChuDeQuery searhOption)
        {
            var Querys = new List<ISolrQuery>();
            Querys.Add(new SolrQuery("*:*"));
            if (searhOption.UrlList != null && searhOption.UrlList.Count > 0)
            {
                //UrlList:("/noidung/Lists/VanBanTaiLieuSuKien" "/noidung/Lists/VanBanTaiLieu")
                Querys.Add(new SolrQuery(string.Format("FullUrlList:({0})", string.Join(" ", searhOption.UrlList.Select(x => "\"" + x + "\"")))));
            }
            else if (SpListProcess != null)
            {
                Querys.Add(new SolrQuery(string.Format("FullUrlList:\"{0}\"", SpListProcess.ParentWeb.ServerRelativeUrl + "/" + SpListProcess.RootFolder.Url)));
            }
            Querys.AddSearchBase(searhOption);
            //if (searhOption.chudee > 0)
            //    Querys.Add(new SolrQuery(string.Format("DMLoaiTaiLieu_Id:\",{0},\"", searhOption.DMLoaiTaiLieu)));
            if (searhOption.DMHienThi)
            {
                Querys.Add(new SolrQuery(string.Format("DMHienThi:\"*{0}*\"", 1)));
            }
            if (searhOption.KSDenNgay.HasValue)
            {
                Querys.Add(new SolrQuery(GetQueryDateTimeSolrLeq("KSDenNgay", searhOption.KSDenNgay.Value)));
            }
            if (searhOption.KSTuNgay.HasValue)
            {
                Querys.Add(new SolrQuery(GetQueryDateTimeSolrGeq("KSTuNgay", searhOption.KSTuNgay.Value)));
            }
            BuildQueryToString(Querys);
            return Querys;
        }
        public List<KSBoChuDeJson> GetListJsonSolr(KSBoChuDeQuery searhOption, params string[] Fields)
        {
            List<KSBoChuDeJson> lstReturn = new List<KSBoChuDeJson>();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "SP_ID", "Title", "DMSTT", "DMHienThi", "DMDescription", "KSTuNgay", "UCThamGia","KSDenNgay", "_ModerationStatus", "TrangThaiText"}; //Set default
            }
            var Querys = BuildQuerySolr(searhOption);
            #region Option khởi tạo
            var oorders = new List<SortOrder>();

            if (oorders.FindIndex(x => x.FieldName == searhOption.FieldOrder) == -1)
                oorders.Add(new SortOrder(searhOption.FieldOrder == "ID" ? "id" : searhOption.FieldOrder, (searhOption.Ascending) ? Order.ASC : Order.DESC));
            QueryOptions options = new QueryOptions()
            {
                OrderBy = oorders,
                Fields = Fields,
                //StartOrCursor =  new StartOrCursor.Start((currentPage - 1) * rowPerPage)
            };
            if (searhOption.Length > 0)
            {
                options.Rows = searhOption.Length;
                options.Start = searhOption.Start; //Barn ghi dau tien
            }

            #endregion
            var result = solrWorkerDM.Query(new SolrMultipleCriteriaQuery(Querys, "AND"), options);
            TongSoBanGhiSauKhiQuery = Convert.ToInt32(result.NumFound);
            foreach (DataSolr itemsolr in result)
            {
                lstReturn.Add(ConvertSolrToJson<KSBoChuDeJson>(itemsolr, Fields));
            }
            return lstReturn;
        }
    }
}