using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortalData.DanhSachNgonNgu
{
    public class DanhSachNgonNguDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/Lists/DanhSachNgonNgu";
        public DanhSachNgonNguDA(bool isAdmin = false)
        {
            if (isAdmin)
                InitAdmin(_urlList);
            else
            Init(_urlList);
        }
        public DanhSachNgonNguDA(string UrlSite, bool isAdmin = false)
        {
            _urlList = UrlSite + _urlList;
            if (isAdmin)
                InitAdmin(_urlList);
            else
                Init(_urlList);
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<DanhSachNgonNguJson> GetListJson(DanhSachNgonNguQuery searhOption, params string[] Fields)
        {
            List<DanhSachNgonNguJson> lstReturn = new List<DanhSachNgonNguJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "DMDescription", "DMHienThi", "DMMoTa", "DMSTT", "DMVietTat", "ColumnImage",
                    "URLWeb","_ModerationStatus", "Language" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<DanhSachNgonNguJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(DanhSachNgonNguQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if(!string.IsNullOrEmpty( searhOption.URLWeb))
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            if (searhOption._ModerationStatus == 1)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption._ModerationStatus == 1)
            {
                oBuildQuery.Append(GetStringModer(0)); //appro l� 0
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            if (!string.IsNullOrEmpty(searhOption.URLWeb))
            {
                oBuildQuery.Append(GetStringEQText("URLWeb", searhOption.URLWeb));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}