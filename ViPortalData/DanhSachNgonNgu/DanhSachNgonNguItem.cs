using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;

namespace ViPortalData.DanhSachNgonNgu
{
	public class DanhSachNgonNguItem : SPEntity
	{
		public string DMDescription { set; get; }
		public bool DMHienThi { set; get; }
		public string DMMoTa { set; get; }
		public int DMSTT { set; get; }
		public string URLWeb { set; get; }
		public string ColumnImage { get; set; }
		public string ColumnImageSrc
		{
			get
			{
				if (!string.IsNullOrEmpty(ColumnImage))
				{
					string matchString = Regex.Match(ColumnImage, "<img.+?src=[\"'](.+?)[\"'].*?>", RegexOptions.IgnoreCase).Groups[1].Value;
					return matchString;
				}
				else return "";
			}
		}
		public string DMVietTat { set; get; }
		public string Language { get; set; }
		public DanhSachNgonNguItem()
		{

		}
	}
	public class DanhSachNgonNguJson : EntityJson
	{
		public string Language { get; set; }
		public string DMDescription { set; get; }
		public bool DMHienThi { set; get; }
		public string DMMoTa { set; get; }
		public int DMSTT { set; get; }
		public string ColumnImage { get; set; }
		public string ColumnImageSrc
		{
			get
			{
				if (!string.IsNullOrEmpty(ColumnImage))
				{
					string matchString = Regex.Match(ColumnImage, "<img.+?src=[\"'](.+?)[\"'].*?>", RegexOptions.IgnoreCase).Groups[1].Value;
					return matchString;
				}
				else return "";
			}
		}
		public string URLWeb { set; get; }
		public string DMVietTat { set; get; }
		public DanhSachNgonNguJson()
		{

		}
	}
}