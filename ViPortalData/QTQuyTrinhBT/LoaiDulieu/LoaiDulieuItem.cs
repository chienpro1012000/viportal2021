using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace ViPortalData
{
    public class LoaiDulieuItem : SPEntity
    {
        public bool DMHienThi { set; get; }
        public string DMMoTa { set; get; }
        public int DMSTT { set; get; }
        public string DMVietTat { set; get; }
        public string UrlList { set; get; }

        public LoaiDulieuItem()
        {

        }
    }

    public class LoaiDulieuJson : EntityJson
    {
        public bool DMHienThi { set; get; }
        public string DMMoTa { set; get; }
        public int DMSTT { set; get; }
        public string DMVietTat { set; get; }

        public LoaiDulieuJson()
        {

        }
    }
}
