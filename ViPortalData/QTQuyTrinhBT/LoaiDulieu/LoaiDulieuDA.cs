using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortalData
{
    public class LoaiDulieuDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/Lists/LoaiDulieu";
        public LoaiDulieuDA()
        {
            Init(_urlList);
        }
        public LoaiDulieuDA(string _urlListProcess)
            : base(_urlListProcess)
        {
            _urlList = _urlListProcess;
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<LoaiDulieuJson> GetListJson(LoaiDulieuQuery searhOption, params string[] Fields)
        {
            List<LoaiDulieuJson> lstReturn = new List<LoaiDulieuJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "UrlList", "_ModerationStatus" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<LoaiDulieuJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(LoaiDulieuQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if (searhOption.UrlList.Count > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            if (searhOption.UrlList.Count > 0)
            {
                oBuildQuery.Append(getStringInListString("UrlList", searhOption.UrlList));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            Queryreturn = oBuildQuery.ToString();
            return Queryreturn;
        }
    }
}