using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace ViPortalData
{
    public class WorkflowItem : SPEntity
    {
        public SPFieldLookupValue DoiTuong { set; get; }
        public SPFieldLookupValue TrangThaiHienTai { set; get; }
        public string ThongTinChuyenTT { set; get; }

        public WorkflowItem()
        {
            DoiTuong = new SPFieldLookupValue();
            TrangThaiHienTai = new SPFieldLookupValue();
            ThongTinChuyenTT = string.Empty;

        }
    }
    public class ReNhanhItem
    {
        public string Title { get; set; }
        public string ReNhanhItemId { get; set; }
        public string MaTrangThai_Title { get; set; }
        public string MaTrangThai { get; set; }
        public string MaTrangThaiReturn { get; set; }
        public List<LookupData> lstNguoiDung { get; set; }
        public List<LookupData> Permisstions { get; set; }
        public List<LookupData> PhongBanNhom { get; set; }

        public ReNhanhItem()
        {
            ReNhanhItemId = System.Guid.NewGuid().ToString();
            lstNguoiDung = new List<LookupData>();
            Permisstions = new List<LookupData>();
            PhongBanNhom = new List<LookupData>();
        }
    }
    public class WorkflowJson : EntityJson
    {
        public LookupData DoiTuong { set; get; }
        public LookupData TrangThaiHienTai { set; get; }
        public LookupData TrangThaiHienTai_x003a_DMVietTat { get; set; }
        public string ThongTinChuyenTT { set; get; }

        public WorkflowJson()
        {
            DoiTuong = new LookupData();
            TrangThaiHienTai = new LookupData();
            TrangThaiHienTai_x003a_DMVietTat = new LookupData();
            ThongTinChuyenTT = string.Empty;
        }
    }

    public class LogTextItem
    {
        public string ParentStep { get; set; }
        public string CurStep { get; set; }
        public int UserID { get; set; }

        public LogTextItem()
        {
            
        }
        public LogTextItem(string _ItemLog)
        {
            string[] temp = _ItemLog.Split('-');
            if(temp.Length == 3)
            {
                CurStep = temp[2];
                ParentStep = temp[0];
                UserID = Convert.ToInt32(temp[1]);
            }else if (temp.Length == 2)
            {
                ParentStep = temp[1];
                UserID = Convert.ToInt32(temp[0]);
            }
        }
    }
    public class LogTextLstObj
    {
        public List<LogTextItem> LstLog { get; set; }
        public LogTextLstObj(string LogText)
        {
            //|-16-999||-16-999||-16-999||-16-999||-16-999||999-16-000||000-16-001||001-16-999||999-16-000||000-27-001|
            List<string> lstText = LogText.Split(new Char[] { '|' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            LstLog = lstText.Select(x => new LogTextItem(x)).ToList();
        }
    }
}
