using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortalData
{
    public class WorkflowDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/Lists/Workflow";
        public WorkflowDA()
        {
            Init(_urlList);
        }
        public WorkflowDA(string _urlListProcess)
            : base(_urlListProcess)
        {
            _urlList = _urlListProcess;
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<WorkflowJson> GetListJson(WorkflowQuery searhOption, params string[] Fields)
        {
            List<WorkflowJson> lstReturn = new List<WorkflowJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0) 
            {
                Fields = new string[] { "ID", "Title", "TrangThaiHienTai", "ThongTinChuyenTT", "TrangThaiHienTai_x003a_DMVietTat" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<WorkflowJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(WorkflowQuery  searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if(searhOption.DoiTuong > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            if (searhOption.DoiTuong > 0)
            {
                oBuildQuery.Append(GetStringEQLookUp("DoiTuong", searhOption.DoiTuong));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}