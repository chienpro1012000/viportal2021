using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public class LienKetWebsiteDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/noidung/Lists/LienKetWebsite";
        public LienKetWebsiteDA()
        {
            Init(_urlList,true);
        }
        public LienKetWebsiteDA(string _urlListProcess)
            : base(_urlListProcess + "/noidung/Lists/LienKetWebsite")
        {
            _urlList = _urlListProcess;
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<LienKetWebsiteJson> GetListJson(LienKetWebsiteQuery searhOption, params string[] Fields)
        {
            List<LienKetWebsiteJson> lstReturn = new List<LienKetWebsiteJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0) 
            {
                Fields = new string[] { "ID", "Title","MoTa","STT","Link" , "_ModerationStatus", "AnhDaiDien", "ParentLienKet" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<LienKetWebsiteJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(LienKetWebsiteQuery  searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if(searhOption.HasParentLienKet)
                oBuildQuery.Append("<And>");
            if (searhOption.NotHasLink)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            if (searhOption._ModerationStatus == 1)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption._ModerationStatus == 1)
            {
                oBuildQuery.Append(GetStringModer(0)); //appro l� 0
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            if (searhOption.NotHasLink)
            {
                oBuildQuery.Append(getstringNull("Link"));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.HasParentLienKet)
            {
                oBuildQuery.Append(getstringNull("ParentLienKet"));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}