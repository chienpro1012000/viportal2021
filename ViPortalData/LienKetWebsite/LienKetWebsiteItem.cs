using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{
    public class LienKetWebsiteItem : SPEntity
    {
        public string Link { set; get; }
        public int STT { set; get; }
        public string MoTa { set; get; }
        public string AnhDaiDien { get; set; }
        public SPFieldLookupValue ParentLienKet { get; set; }

        public LienKetWebsiteItem()
        {
            ParentLienKet = new SPFieldLookupValue();
        }
    }
    public class LienKetWebsiteJson : EntityJson
    {
        public string Link { set; get; }
        public int STT { set; get; }
        public string MoTa { set; get; }
        public string AnhDaiDien { get; set; }
        public LookupData ParentLienKet { get; set; }
        public LienKetWebsiteJson()
        {
            ParentLienKet = new LookupData();
        }
    }
}
