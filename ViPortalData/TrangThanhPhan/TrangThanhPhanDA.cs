using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public class TrangThanhPhanDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/noidung/Lists/TrangThanhPhan";
        public TrangThanhPhanDA()
        {
            Init(_urlList);
        }
        public TrangThanhPhanDA(string _urlListProcess)
            : base(_urlListProcess)
        {
            _urlList = _urlListProcess;
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<TrangThanhPhanJson> GetListJson(TrangThanhPhanQuery searhOption, params string[] Fields)
        {
            List<TrangThanhPhanJson> lstReturn = new List<TrangThanhPhanJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0) 
            {
                Fields = new string[] { "ID", "Title", "UrlView", "UrlQuanTri", "STT" , "_ModerationStatus" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<TrangThanhPhanJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(TrangThanhPhanQuery  searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}