using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{
    public class TrangThanhPhanItem : SPEntity
    {
        public string UrlView { set; get; }
        public string UrlQuanTri { set; get; }
        public string MoTa { set; get; }
        public int STT { set; get; }

        public TrangThanhPhanItem()
        {

        }
    }
    public class TrangThanhPhanJson : EntityJson
    {
        public string UrlView { set; get; }
        public string UrlQuanTri { set; get; }
        public string MoTa { set; get; }
        public int STT { set; get; }

        public TrangThanhPhanJson()
        {

        }
    }
}
