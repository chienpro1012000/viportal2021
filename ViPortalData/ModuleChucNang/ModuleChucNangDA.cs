using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public class ModuleChucNangDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/noidung/Lists/ModuleChucNang";
        public ModuleChucNangDA()
        {
            Init(_urlList);
        }
        public ModuleChucNangDA(string _urlSiteProcess)
        {
            _urlList = _urlSiteProcess + _urlList;
            Init(_urlList);
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<ModuleChucNangJson> GetListJson(ModuleChucNangQuery searhOption, params string[] Fields)
        {
            List<ModuleChucNangJson> lstReturn = new List<ModuleChucNangJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0) 
            {
                Fields = new string[] { "ID", "Title", "UrlCMS", "_ModerationStatus", "UrlControl", "ConfigNgonNgu" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<ModuleChucNangJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(ModuleChucNangQuery  searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if(!string.IsNullOrEmpty(searhOption.UrlControl))
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            if (!string.IsNullOrEmpty(searhOption.UrlControl))
            {
                oBuildQuery.Append(GetStringEQText("UrlControl", searhOption.UrlControl));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}