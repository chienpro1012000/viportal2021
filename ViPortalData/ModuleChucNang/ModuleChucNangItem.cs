using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{
    public class ModuleChucNangItem : SPEntity
    {
        public string UrlCMS { set; get; }
        public string UrlPortal { set; get; }
        public string ImageNews { set; get; }
        public string MoTa { set; get; }
        public string UrlControl { get; set; }
        public DateTime? NgayTao { set; get; }
        public DateTime? LastModified { get; set; }
        public string ConfigNgonNgu { get; set; }

        public ModuleChucNangItem()
        {

        }
    }
    public class ModuleChucNangJson : EntityJson
    {
        public string UrlCMS { set; get; }
        public string UrlControl { get; set; }
        public string UrlPortal { set; get; }
        public string ImageNews { set; get; }
        public string MoTa { set; get; }
        public string ConfigNgonNgu { get; set; }
        public DateTime? NgayTao { set; get; }

        public ModuleChucNangJson()
        {

        }
    }


    public class ConfigNgonngu
    {
        public int IDNgonNgu { get; set; }
        public string TitleNgonNgu { get; set; }
        public Dictionary<string, string> ConfigKeyValue = new Dictionary<string, string>();
    }
}
