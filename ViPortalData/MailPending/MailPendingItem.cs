﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;


namespace ViPortalData.MailPending
{
	public class MailPendingItem : SPEntity
	{
		public bool EmailDaGui { set; get; }
		public bool SMSDaGui { set; get; }
		public string NguoiGui { set; get; }
		public string NguoiNhan { set; get; }
		public string EmailNhan { set; get; }
		public string EmailNoiDung { set; get; }
		public DateTime? EmailThoiGianGui { set; get; }
		public string SmsNoiDung { set; get; }
		public string SMSSDT { set; get; }
		public DateTime? SMSThoiGianGui { set; get; }
		// lookup nhiều	public SPFieldLookupValueCollection BBB { set; get; }
		public MailPendingItem()
		{

			Title = string.Empty;
			EmailNhan = string.Empty;
			EmailNoiDung = string.Empty;
			SmsNoiDung = string.Empty;
			SMSSDT = string.Empty;

		}
	}
	public class MailPendingJson : EntityJson
	{
		public bool EmailDaGui { set; get; }
		public bool SMSDaGui { set; get; }
		public string NguoiGui { set; get; }
		public string NguoiNhan { set; get; }
		public string EmailNhan { set; get; }
		public string EmailNoiDung { set; get; }
		public DateTime? EmailThoiGianGui { set; get; }
		public string SmsNoiDung { set; get; }
		public string SMSSDT { set; get; }
		public DateTime? SMSThoiGianGui { set; get; }
		// lookup nhiều	public  List<LookupData> SSs { set; get; }
		public MailPendingJson()
		{
			Title = string.Empty;
			EmailNhan = string.Empty;
			EmailNoiDung = string.Empty;
			SmsNoiDung = string.Empty;
			SMSSDT = string.Empty;
		}
	}
}
