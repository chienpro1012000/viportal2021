using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{
    public class CommentMXHItem : SPEntity
    {
        public SPFieldLookupValue BaiViet { set; get; }
        public string Titles { set; get; }
        public SPFieldLookupValue CreatedUser { set; get; }

        public CommentMXHItem()
        {
            BaiViet = new SPFieldLookupValue();
            CreatedUser = new SPFieldLookupValue();

        }
    }
    public class CommentMXHJson : EntityJson
    {
        public LookupData BaiViet { set; get; }
        public string Titles { set; get; }
        public LookupData CreatedUser { set; get; }

        public CommentMXHJson()
        {
            BaiViet = new LookupData();
            CreatedUser = new LookupData();

        }
    }
}
