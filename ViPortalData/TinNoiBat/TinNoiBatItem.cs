using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.JSGrid;
using ViPortal_Utils.Base;

namespace ViPortalData.TinNoiBat
{
	public class TinNoiBatItem : SPEntity
	{
		public string DescriptionNews { set; get; }
		public DateTime? CreatedDate { set; get; }
		public string ImageNews { set; get; }
		public string ContentNews { set; get; }
		public int DMSTT { set; get; }
		public int ReadCount { set; get; }
		public int ItemTinTuc { get; set; }
		public string UrlList { get; set; }
		public TinNoiBatItem()
		{

		}
	}
	public class TinNoiBatJson : EntityJson
	{
		public string DescriptionNews { set; get; }
		public DateTime? CreatedDate { set; get; }
		public string ImageNews { set; get; }
		public string ContentNews { set; get; }
		public int DMSTT { set; get; }
		public int ReadCount { set; get; }
		public int ItemTinTuc { get; set; }
		public string Title_DanhMuc { get; set; }
		public TinNoiBatJson()
		{
		}
	}
}