﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using ViPortal_Utils.Base;

namespace ViPortalData.TinNoiBat
{
    public class TinNoiBatQuery : QuerySearch
    {
        public int ItemTinTuc { get; set; }
        public string UrlListNew { get; set; }
        public string Title { get; set; }
        /// <summary>
        /// stt lớn nhất
        /// </summary>
        public int MaxSTT { get; set; }

        /// <summary>
        /// stt nhỏ nhất.
        /// </summary>
        public int MinSTT { get; set; }
        public TinNoiBatQuery()
        {
            
        }
        public TinNoiBatQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
}