﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViPortal_Utils.Base;

namespace ViPortalData.BinhLuan
{
    public class BinhLuanItem : SPEntity
    {
        public SPFieldLookupValue BaiViet { set; get; }
        public string AnhDaiDien { get; set; }
        public SPFieldLookupValue CreatedUser { set; get; }
        /// <summary>
        /// id;#title
        /// </summary>
        public string IDChuDe { get; set; }

        public BinhLuanItem()
        {
            CreatedUser = new SPFieldLookupValue();
        }
    }
    public class BinhLuanJson : EntityJson
    {
        public LookupData BaiViet { set; get; }
        public string AnhDaiDien { get; set; }
        public LookupData IDChuDe { get; set; }
        public LookupData CreatedUser { set; get; }

        public BinhLuanJson()
        {
            CreatedUser = new LookupData();
        }
    }
}
