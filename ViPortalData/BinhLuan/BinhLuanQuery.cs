﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ViPortal_Utils.Base;

namespace ViPortalData.BinhLuan
{
    public class BinhLuanQuery : QuerySearch
    {
        public int BaiViet { get; set; }
        public int BaiVietID { get; set; }
        public int IDChuDe { get; set; }
        public BinhLuanQuery()
        {
        }
        public BinhLuanQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
}
