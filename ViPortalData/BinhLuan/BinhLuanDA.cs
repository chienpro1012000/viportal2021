﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortalData.BinhLuan
{
    public class BinhLuanDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/diendan/Lists/BinhLuan";

        public BinhLuanDA(bool isAdmin = false)
        {
            if (isAdmin)
                Init(_urlList);
            else InitAdmin(_urlList);
        }

        public BinhLuanDA(string _urlListProcess, bool isAdmin = false)
            //: base(_urlListProcess)
        {
            _urlList = _urlListProcess + _urlList;
            if (isAdmin)
                Init(_urlList);
            else InitAdmin(_urlList);
        }

        public List<BinhLuanJson> GetListJson(BinhLuanQuery searhOption, params string[] Fields)
        {
            List<BinhLuanJson> lstReturn = new List<BinhLuanJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "CreatedUser", "BaiViet", "Created", "AnhDaiDien", "_ModerationStatus", "IDChuDe" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<BinhLuanJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(BinhLuanQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();

            oBuildQuery.Append("<Where>");
            if (searhOption.IDChuDe > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.BaiViet > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            if (searhOption._ModerationStatus == 1)
                oBuildQuery.Append("<And>");
            if (searhOption.BaiVietID > 0)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption.BaiVietID > 0)
            {
                oBuildQuery.Append(GetStringIncludeLookUp("BaiViet", searhOption.BaiVietID));
                oBuildQuery.Append("</And>");
            }
            if (searhOption._ModerationStatus == 1)
            {
                oBuildQuery.Append(GetStringModer(0)); //appro là 0
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            if (searhOption.BaiViet > 0)
            {
                oBuildQuery.Append(GetStringEQLookUp("BaiViet", searhOption.BaiViet));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.IDChuDe > 0)
            {
                oBuildQuery.Append(GetStringStartWith("IDChuDe", $"{searhOption.IDChuDe};#"));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}
