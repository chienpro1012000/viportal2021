﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortalData.SPListCommon
{
    public class SPListDA<U>  : SPBaseDA<U> where U : EntitySolr, new()
    {
        public SPListDA(string _urlListProcess,bool isSolr = false)
        {
            Init(_urlListProcess, isSolr);
        }
        public string GetLogText(int ItemID)
        {
            SPListItem oSPListItem = SpListProcess.GetItemByIdSelectedFields(ItemID, "LogText");
            if (oSPListItem["LogText"] != null)
                return oSPListItem["LogText"].ToString();
            else return "";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ItemID"></param>
        /// <returns></returns>
        public  ItemContent GetInfo(int ItemID)
        {
            
            ItemContent oItemContent = new ItemContent();
            try
            {
                SPListItem oSPListItem = SpListProcess.GetItemByIdSelectedFields(ItemID, "LogText", "Title");
                if (oSPListItem["LogText"] != null)
                    oItemContent.LogText = oSPListItem["LogText"].ToString();
                if (oSPListItem["Title"] != null)
                    oItemContent.Title = oSPListItem["Title"].ToString();
            }catch (Exception ex)
            {
                throw new System.Exception(ex.Message + ". DefaultEditFormUrl:" + SpListProcess.DefaultEditFormUrl + ".ItemID:" + ItemID);
            }
            return oItemContent;
        }
    }
    public class ItemContent
    {
        public string Title { get; set; }
        public string LogText { get; set; }
        public ItemContent()
        {
            LogText = string.Empty;
            Title = string.Empty;
        }
    }
}
