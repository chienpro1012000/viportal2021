using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public class TinTucQuery : QuerySearch
    {
        public bool isTichHop { get; set; }
        public string DanhMucTin { get; set; }
        public List<string> lstUrLNewList { get; set; }
        public string LogText { get; set; }
        public TinTucQuery()
        {
        }
        public TinTucQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }

        public string UrlListTinTuc { get; set; }
    }
}