using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{
    public class TinTucItem : SPEntity
    {
        public string DescriptionNews { set; get; }
        public DateTime? CreatedDate { set; get; }
        public string ImageNews { set; get; }
        public string TacGia { set; get; }
        public bool isHotNew { set; get; }
        public string ContentNews { set; get; }
        public string UrlView { get; set; }
        public string DanhMucTin { set; get; }
        public SPFieldLookupValue NguoiTao { set; get; }
        public string LogText { get; set; }
        public SPFieldLookupValue EditUser { set; get; }
        public string Titles { set; get; }
        public int ReadCount { get; set; }

        public TinTucItem()
        {
            NguoiTao = new SPFieldLookupValue();
            EditUser = new SPFieldLookupValue();
            CreatedDate = DateTime.Now;

        }
    }
    public class TinTucJson : EntityJson
    {
        public string DescriptionNews { set; get; }
        public DateTime? CreatedDate { set; get; }
        public string ImageNews { set; get; }
        public string TacGia { set; get; }
        public bool isHotNew { set; get; }
        public string UrlView { get; set; }
        public string ContentNews { set; get; }
        public string DanhMucTin { set; get; }
        
       
        public LookupData NguoiTao { set; get; }
        public LookupData EditUser { set; get; }
        
        public string Titles { set; get; }
        public int ReadCount { get; set; }
        public int SP_ID { get; set; }

        public TinTucJson()
        {
            NguoiTao = new LookupData();
            EditUser = new LookupData();

        }
    }
}
