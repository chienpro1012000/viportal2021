﻿using CommonServiceLocator;
using Microsoft.SharePoint;
using SolrNet;
using SolrNet.Commands.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public class TinTucDA : SPBaseDA<DataSolr>
    {
        private string _urlList = "/noidung/Lists/";

        public TinTucDA(bool isAdmin = false)
        {
            if (isAdmin)
                InitAdmin(_urlList + "News", true);
            else
                Init(_urlList + "News", true);
        }
        public TinTucDA(string _urlFullListProcess, bool isAdmin = false)
        {
            if (isAdmin)
                InitAdmin(_urlFullListProcess , true);
            else
                Init(_urlFullListProcess, true);
        }
        public TinTucDA(string urlsite, string _urlListProcess, bool isAdmin = false)
        {
            _urlList = urlsite + _urlList + _urlListProcess;
            if (isAdmin)
                InitAdmin(_urlList, true);
            else
                Init(_urlList, true);
            
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<TinTucJson> GetListJson(TinTucQuery searhOption, params string[] Fields)
        {
            List<TinTucJson> lstReturn = new List<TinTucJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "CreatedDate", "Titles", "_ModerationStatus", "NguoiTao", "ImageNews", "Created", "EditUser", "WFTrangThai", "UserPhuTrach", "isHotNew", "DescriptionNews" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<TinTucJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(TinTucQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if(!string.IsNullOrEmpty(searhOption.LogText))
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            if (searhOption._ModerationStatus == 1)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption._ModerationStatus == 1)
            {
                oBuildQuery.Append(GetStringModer(0)); //appro là 0
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            if (!string.IsNullOrEmpty(searhOption.LogText))
            {
                oBuildQuery.Append(GetStringContainsText("LogText", searhOption.LogText));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }


        public List<ISolrQuery> BuildQuerySolr(TinTucQuery searhOption)
        {
            var Querys = new List<ISolrQuery>();
            Querys.Add(new SolrQuery("*:*"));
            if (searhOption.lstUrLNewList != null && searhOption.lstUrLNewList.Count > 0)
            {
                //UrlList:("/noidung/Lists/TinTucSuKien" "/noidung/Lists/TinTuc")
                Querys.Add(new SolrQuery(string.Format("FullUrlList:({0})", string.Join(" ", searhOption.lstUrLNewList.Select(x => "\"" + x + "\"")))));
            }
            else if (searhOption.UrlList != null && searhOption.UrlList.Count > 0)
            {
                //UrlList:("/noidung/Lists/TinTucSuKien" "/noidung/Lists/TinTuc")
                Querys.Add(new SolrQuery(string.Format("FullUrlList:({0})", string.Join(" ", searhOption.UrlList.Select(x => "\"" + x + "\"")))));
            }
            else if (SpListProcess != null)
            {
                Querys.Add(new SolrQuery(string.Format("FullUrlList:\"{0}\"", SpListProcess.ParentWeb.ServerRelativeUrl + "/" + SpListProcess.RootFolder.Url)));
            }
            //if (!string.IsNullOrEmpty(searhOption.Keyword))
            //{
            //    Querys.Add(new SolrQuery(string.Format("Titles:*{0}*", searhOption.Keyword)));
                
            //}
            Querys.AddSearchBase(searhOption);
            BuildQueryToString(Querys);
            return Querys;
        }
        public List<TinTucJson> GetListJsonSolr(TinTucQuery searhOption, params string[] Fields)
        {
            List<TinTucJson> lstReturn = new List<TinTucJson>();
            if (Fields.Count() == 0)
            {
                //Fields = new string[] { "id", "SP_ID", "Title", "CreatedDate", "Titles", "_ModerationStatus", "NguoiTao", "ImageNews", "UrlList" }; //Set default
                Fields = new string[] { "id", "SP_ID", "Title", "CreatedDate", "DescriptionNews", "Titles", 
                    "_ModerationStatus", "NguoiTao", "ImageNews", "Created", "EditUser", "WFTrangThai", 
                    "UserPhuTrach", "isHotNew", "FullUrlList", "UrlView" }; //Set default
            }
            var Querys = BuildQuerySolr(searhOption);
            #region Option khởi tạo
            var oorders = new List<SortOrder>();

            if (oorders.FindIndex(x => x.FieldName == searhOption.FieldOrder) == -1)
                oorders.Add(new SortOrder(searhOption.FieldOrder == "ID" ? "id" : searhOption.FieldOrder, (searhOption.Ascending) ? Order.ASC : Order.DESC));
            QueryOptions options = new QueryOptions()
            {
                OrderBy = oorders,
                Fields = Fields,
                //StartOrCursor =  new StartOrCursor.Start((currentPage - 1) * rowPerPage)
            };
            if (searhOption.Length > 0)
            {
                options.Rows = searhOption.Length;
                options.Start = searhOption.Start; //Barn ghi dau tien
            }

            #endregion
            var result = solrWorkerDM.Query(new SolrMultipleCriteriaQuery(Querys, "AND"), options);
            TongSoBanGhiSauKhiQuery = Convert.ToInt32(result.NumFound);
            foreach (DataSolr itemsolr in result)
            {
                lstReturn.Add(ConvertSolrToJson<TinTucJson>(itemsolr, Fields));
            }
            return lstReturn;
        }
    }
}