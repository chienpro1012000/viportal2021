using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortalData
{
    public class DMBaoCaoDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/htbaocao/Lists/DMBaoCao";
        public DMBaoCaoDA()
        {
            Init(_urlList);
        }
        public DMBaoCaoDA(string _urlListProcess)
            : base(_urlListProcess)
        {
            _urlList = _urlListProcess;
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<DMBaoCaoJson> GetListJson(DMBaoCaoQuery searhOption, params string[] Fields)
        {
            List<DMBaoCaoJson> lstReturn = new List<DMBaoCaoJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0) 
            {
                Fields = new string[] { "ID", "Title", "DMVietTat", "DMSTT", "_ModerationStatus", "LoaiBaoCao", "UrlFileBCChiTiet", "UrlBaoCaoTongHop" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<DMBaoCaoJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(DMBaoCaoQuery  searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if(searhOption.DonViId > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            if (searhOption._ModerationStatus == 1)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption._ModerationStatus == 1)
            {
                oBuildQuery.Append(GetStringModer(0)); //appro l� 0
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetBylistID)
            {
                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.DonViId > 0)
            {
                oBuildQuery.Append("<Or>");
                oBuildQuery.Append(GetStringContainsText("DonViSuDung", "," + searhOption.DonViId + ","));
                oBuildQuery.Append(getstringNull("DonViSuDung"));

               oBuildQuery.Append("</Or>");
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}