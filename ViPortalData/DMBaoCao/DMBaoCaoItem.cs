﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace ViPortalData
{
    public class DMBaoCaoItem : SPEntity
    {
        public string DMDescription { set; get; }
        public bool DMHienThi { set; get; }
        public string DMMoTa { set; get; }
        public int DMSTT { set; get; }
        public string DMVietTat { set; get; }
        public string UrlList { set; get; }
        public int LoaiBaoCao { get; set; }
        public string UrlFileBCChiTiet { get; set; }
        public string UrlBaoCaoTongHop { get; set; }
        public string jsonBCCT { get; set; }
        public string jsonBCTH { get; set; }
        public int PhanLoaiHinhThucTongHop { get; set; }
        public string DonViSuDung { get; set; }
        public string JsonConfig { get; set; }
        public DMBaoCaoItem()
        {

        }
    }
    
    public class DMBaoCaoJson : EntityJson
    {
        public string DMDescription { set; get; }
        public bool DMHienThi { set; get; }
        public string DMMoTa { set; get; }
        public string DonViSuDung { get; set; }
        public int DMSTT { set; get; }
        public string DMVietTat { set; get; }
        public string UrlFileBCChiTiet { set; get; }
        public string UrlBaoCaoTongHop { set; get; }
        public int LoaiBaoCao { get; set; }
        public DMBaoCaoJson()
        {

        }
    }
}
