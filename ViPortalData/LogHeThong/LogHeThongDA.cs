using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortalData.LogHeThong
{
    public class LogHeThongDA : SPBaseDA<LogHeThongSolr>
    {
        private string _urlList = "/noidung/Lists/LogHeThong";
        public LogHeThongDA()
        {
            Init(_urlList);
        }
        public LogHeThongDA(bool isAdmin)
        {
            if (isAdmin)
                InitAdmin(_urlList);
            else
                Init(_urlList);
        }
        public LogHeThongDA(string _urlListProcess)
            : base(_urlListProcess + "/noidung/Lists/LogHeThong")
        {
            _urlList = _urlListProcess;
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<LogHeThongJson> GetListJson(LogHeThongQuery searhOption, params string[] Fields)
        {
            List<LogHeThongJson> lstReturn = new List<LogHeThongJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "CreatedUser", "LogDoiTuong", "LogFunction", "LogNoiDung", "LogThaoTac", "LogTrangThai", "fldGroup", "_ModerationStatus", "Created" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<LogHeThongJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(LogHeThongQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if (!string.IsNullOrEmpty(searhOption.LogThaoTac))
                oBuildQuery.Append("<And>");
            if (searhOption.CreatedUser > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            if (searhOption.CreatedUser > 0)
            {
                oBuildQuery.Append(GetStringEQLookUp("CreatedUser", searhOption.CreatedUser));
                oBuildQuery.Append("</And>");
            }
            if (!string.IsNullOrEmpty(searhOption.LogThaoTac))
            {
                oBuildQuery.Append(GetStringEQText("LogThaoTac", searhOption.LogThaoTac));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}