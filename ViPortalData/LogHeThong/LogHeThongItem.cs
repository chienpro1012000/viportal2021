﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;

namespace ViPortalData.LogHeThong
{
	public class LogHeThongItem : SPEntity
	{
		public SPFieldLookupValue CreatedUser { set; get; }
		public string LogDoiTuong { set; get; }
		public string LogFunction { set; get; }
		public string LogNoiDung { set; get; }
		public string LogThaoTac { set; get; }
		public int LogTrangThai { set; get; }
        public int LogIDDoiTuong { get; set; }

        public SPFieldLookupValue fldGroup { set; get; }
		public LogHeThongItem()
		{
			CreatedUser = new SPFieldLookupValue();
			fldGroup = new SPFieldLookupValue();
		}
	}
	public class LogHeThongJson : EntityJson
	{
		public LookupData CreatedUser { set; get; }
        public int LogIDDoiTuong { get; set; }
        public string LogDoiTuong { set; get; }
		public string LogFunction { set; get; }
		public string LogNoiDung { set; get; }
		public string LogThaoTac { set; get; }
		public int LogTrangThai { set; get; }
        public string sTitle
        {

            get
            {
                string TitleThaoTac = "Thao tác";
                switch (LogThaoTac)
                {
                    case "QUERYDATA":
                        TitleThaoTac = "Xem danh sách";
                        break;
                    case "UPDATE":
                        TitleThaoTac = "Cập nhật";
                        break;
                    case "ADD":
                        TitleThaoTac = "Thêm mới";
                        break;
                    case "DELETE":
                        TitleThaoTac = "Xóa";
                        break;
                    case "DELETE-MULTI":
                        TitleThaoTac = "Xóa nhiều";
                        break;
                    case "APPROVED":
                        TitleThaoTac = "Duyệt";
                        break;
                    case "PENDDING":
                        TitleThaoTac = "Hủy duyệt";
                        break;
                }
                string _LogDoiTuongTitle = LogDoiTuong;
                switch (LogDoiTuong)
                {
                    case "Banner":
                        _LogDoiTuongTitle = "Banner";
                        break;

                }

                return $"{TitleThaoTac} {_LogDoiTuongTitle}";
                    
            }
        }

		public LookupData fldGroup { set; get; }
		public LogHeThongJson()
		{
			CreatedUser = new LookupData();
			fldGroup = new LookupData();
		}
	}
}