﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Newtonsoft.Json;
using ViPortal_Utils.Base;
namespace ViPortalData
{
    public class SSOInfor
    {
        public string code { get; set; }
        public string paramCode { get; set; }
        public string message { get; set; }
        public string status { get; set; }
        public SSOUserInfor data { get; set; }
        public string yourAppReturn { get; set; }
    }
    public class SSOUserInfor
    {
        public string serviceTicket { get; set; }
        public string expiresIn { get; set; }
        public ssoidentity identity { get; set; }
        public List<SSOGroup> listGroup { get; set; }
    }
    public class ssoidentity
    {
        public string username { get; set; }
        public string fullName { get; set; }
        public string userId { get; set; }
        public string appCode { get; set; }
        public string appId { get; set; }
        public string email { get; set; }
        public string authentication2Factor { get; set; }
        public string staffCode { get; set; }
        public string orgId { get; set; }
    }
    public class SSOGroup
    {
        public string groupId { get; set; }
        public string groupName { get; set; }
        public string parentGroupId { get; set; }
        public string status { get; set; }
        public string description { get; set; }
    }

    public class LUserItem : SPEntity
    {
        public SPFieldLookupValue Groups { set; get; }
        public SPFieldLookupValue UserPhongBan { set; get; }
        public string TaiKhoanTruyCap { set; get; }
        public string UserEmail { set; get; }
        public string UserSDT { set; get; }
        public string UserChucVu { set; get; }
        public string AnhDaiDien { get; set; }

        public string Connections { get; set; }
        public DateTime? UserNgaySinh { get; set; }
        public string TokenFireBase { get; set; }
        //public DateTime? CountSinhNhat { get; set; }
        public SPFieldLookupValueCollection Permissions { set; get; }
        public SPFieldLookupValueCollection Permissions_BaoCao { set; get; }
        public SPFieldLookupValueCollection Roles { get; set; }
        public string PerQuery { get; set; }
        public string PerJson { get; set; }
        public int TrangThaiComment { get; set; }
        public string staffCode { get; set; }
        public int DMSTT { get; set; }
        public string HRMSID { get; set; }

        public LUserItem()
        {
            Groups = new SPFieldLookupValue();
            UserPhongBan = new SPFieldLookupValue();
            Permissions = new SPFieldLookupValueCollection();
            Permissions_BaoCao = new SPFieldLookupValueCollection();
            Roles = new SPFieldLookupValueCollection();
        }
    }
    public class Connection
    {
        public string ConnectionID { get; set; }
        public string UserAgent { get; set; }
        public bool Connected { get; set; }
    }
    public class LUserJson : EntityJson
    {
        public LookupData Groups { set; get; }
        public DateTime? UserNgaySinh { get; set; }
        //public string CurSiteAction { get; set; }
        public int DMSTT { get; set; }
        public string UrlSite { get; set; }
        public LookupData UserPhongBan { set; get; }
        public string TaiKhoanTruyCap { set; get; }
        public string UserEmail { set; get; }
        public string UserSDT { set; get; }
        public string AnhDaiDien { get; set; }
        public string staffCode { get; set; }
        public string PerJson { get; set; }
        public string AnhDaiDienSrcImg { get; set; }
        public string UserChucVu { set; get; }
        public string Connections { get; set; }
        public string PerQuery { get; set; }
        public string PermissionsQuery
        {
            get;set;
        }
        public int fldGroup
        {
            get
            {
                return Groups.ID;
            }
        }
        public List<Connection> ListConnections { get; set; }
        public List<LookupData> Permissions { set; get; }
        public List<LookupData> Roles { set; get; }
        public int TrangThaiComment { set; get; }

        /// <summary>
        /// site đang thao tác của người dùng để check thông tin.
        /// </summary>
        public string CurSiteAction { get; set; }

        public LUserJson()
        {
            Groups = new LookupData();
            UserPhongBan = new LookupData();
            Permissions = new List<LookupData>();
            Roles = new List<LookupData>();
            ListConnections = new List<Connection>();
            PermissionsQuery = string.Empty;

        }
    }

    public class PermisonGroup
    {
        public int fldGroup { get; set; }
        public List<LookupData> Roles { get; set; }
        public List<LookupData> Permissions { get; set; }

        public PermisonGroup()
        {
            Roles = new List<LookupData>();
            Permissions = new List<LookupData>();
        }
    }
    public class PerQuery
    {
        public int fldGroup { get; set; }
        public string  MaQuyen { get; set; }
        public PerQuery(int _fldGroup, string _maQuyen)
        {
            fldGroup = _fldGroup;
            MaQuyen = _maQuyen;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="txtPer">2_003</param>
        public PerQuery(string txtPer)
        {
            if (!string.IsNullOrEmpty(txtPer))
            {
                fldGroup = Convert.ToInt32(txtPer.Split('_')[0]);
                MaQuyen = txtPer.Split('_')[1];
            }
            else
            {
                fldGroup = 0; MaQuyen = "";
            }
        }
    }
    public class LstPerQuery
    {
        public List<PerQuery> perQueries = new List<PerQuery>();
        public LstPerQuery()
        {
            perQueries = new List<PerQuery>();
        }
        /// <summary>
        /// |1_001|2_003|;
        /// </summary>
        /// <param name="PerQuery"></param>
        public LstPerQuery(string PerQuery)
        {
            if (!string.IsNullOrEmpty(PerQuery))
            {
                perQueries = PerQuery.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries).Select(x => new PerQuery(x)).ToList();
            }
            else perQueries = new List<PerQuery>();
        }
    }

    public class DanhBaRespon
    {
        public bool suc { get; set; }
        public string msg { get; set; }
        public List<DanhBaDongBo> data { get; set; }
    }
    public class DanhBaDongBo
    {
        public int donvi_id { get; set; }
        public int STT { get; set; }
        public string tendonvi { get; set; }
        public string diachi { get; set; }
        public DateTime ngaysinh { get; set; }
        public DateTime SinhNhatNamNay
        {
            get
            {
                DateTime nextBirthday = ngaysinh.AddYears(DateTime.Today.Year - ngaysinh.Year);
                if (nextBirthday < DateTime.Today)
                {
                    nextBirthday = nextBirthday.AddYears(1);
                }
                return nextBirthday;
            }
        }
        public string tenkhaisinh { get; set; }
        public bool gioitinh { get; set; }
        public string dienthoai { get; set; }
        public string email { get; set; }
        public long DEPARTMENT_ID { get; set; }
        public string DEPARTMENT_NAME { get; set; }
        public string tenphongban { get; set; }
        public string TEN_VTRICDANH { get; set; }
        public string DIENTHOAI_NOIBO { get; set; }
        public long NS_ID { get; set; }
        public string SOHIEU_NS { get; set; }
        public string ImageAvatar { get; set; }
    }

    public class SinhNhatLanhDaoResponse
    {
        public bool suc { get; set; }
        public string msg { get; set; }
        public List<SinhNhatLanhDao> data { get; set; }
    }
    public class SinhNhatLanhDao
    {
        //"ID_LLNS": "276000000009124",
        //    "TENKHAISINH": "Mã Hoài Nam",
        //    "NGAYSINH": "1970-11-10T00:00:00",
        //    "TEN_CVU": "Thành viên HĐTV",
        //    "ID_DONVI": 276,
        //    "TEN_PHONGB": "Hội đồng thành viên",
        //    "TEN_DONVI": "Tổng Công ty Điện lực thành phố Hà Nội",
        //    "BIRTHDAY": "2022-11-10T00:00:00",
        //    "STT": 1
        public long ID_LLNS { get; set; }
        public string TENKHAISINH { get; set; }
        public DateTime NGAYSINH { get; set; }
        public string TEN_CVU { get; set; }
        public int ID_DONVI { get; set; }
        public string TEN_PHONGB { get; set; }
        public string TEN_DONVI { get; set; }
        public DateTime BIRTHDAY { get; set; }
        public int STT { get; set; }
        public string ImageAvatar { get; set; }
    }

    public class AvatarUser
    {
        public string vid { get; set; }
        public long nsId { get; set; }
        public string anhNs { get; set; }
        public DateTime updateTime { get; set; }
    }
}
