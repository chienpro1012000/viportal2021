using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using ViPortal_Utils.Base;

namespace ViPortalData
{
    public class LUserQuery : QuerySearch
    {
        public bool GetUserPhongBan { get; set; }
        public int UserPhongBan { get; set; }
        public string TaiKhoanTruyCap { get; set; }
        public string UserEmail { get; set; }
        public int Roles { get; set; }
        public string UserSDT { get; set; }
        public int SoNgayBaoSinhNhat { get; set; }
        public DateTime? SapDen_SoNgayBaoSinhNhat { get; set; }
        public DateTime? DangDienRa_SoNgayBaoSinhNhat { get; set; }
        /// <summary>
        ///  search truwongf staffCode trong db luser
        /// </summary>
        public string staffCode { get; set; }
        public string HRMSID { get; set; }
        public int Groups { get; set; }
        public string PerQuery { get;  set; }
        public string KeySSO { get; set; }

        public LUserQuery()
        {
        }
        public LUserQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
}