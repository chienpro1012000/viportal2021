using Microsoft.SharePoint;
using SolrNet;
using SolrNet.Commands.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortalData
{
    public class LUserDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/Lists/LUser";
        public LUserDA(bool isAdmin = false)
        {
            /*if (isAdmin)*/ InitAdmin(_urlList);
            //else
            //    Init(_urlList);
        }
        public LUserDA(string _urlListProcess)
            : base(_urlListProcess)
        {
            _urlList = _urlListProcess + _urlList;
            Init(_urlList);
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<LUserJson> GetListJson(LUserQuery searhOption, params string[] Fields)
        {
            List<LUserJson> lstReturn = new List<LUserJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "TaiKhoanTruyCap", "UserChucVu", "UserSDT", "_ModerationStatus", "Connections", "UserNgaySinh", "AnhDaiDien", "UserPhongBan", "Permissions", "Roles", "Groups", "TrangThaiComment", "UserEmail", "PerQuery" , "staffCode", "PerJson", "CurSiteAction" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<LUserJson>(SPcoll, Fields);
            return lstReturn;
        }


        public List<LUserJson> GetListJsonSolr(LUserQuery searhOption, params string[] Fields)
        {
            List<LUserJson> lstReturn = new List<LUserJson>();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "SP_ID", "Title", "TaiKhoanTruyCap", "UserChucVu", "UserSDT", "_ModerationStatus", "Connections", "UserNgaySinh", "AnhDaiDien", "UserPhongBan", "Permissions", "Roles", "Groups", "TrangThaiComment", "UserEmail" }; //Set default
            }
            var Querys = BuildQuerySolr(searhOption);
            #region Option khởi tạo
            var oorders = new List<SortOrder>();

            if (oorders.FindIndex(x => x.FieldName == searhOption.FieldOrder) == -1)
                oorders.Add(new SortOrder(searhOption.FieldOrder == "ID" ? "SP_ID" : searhOption.FieldOrder, (searhOption.Ascending) ? Order.ASC : Order.DESC));
            QueryOptions options = new QueryOptions()
            {
                OrderBy = oorders,
                Fields = Fields,
                //StartOrCursor =  new StartOrCursor.Start((currentPage - 1) * rowPerPage)
            };
            if (searhOption.Length > 0)
            {
                options.Rows = searhOption.Length;
                options.Start = searhOption.Start; //Barn ghi dau tien
            }

            #endregion
            var result = solrWorkerDM.Query(new SolrMultipleCriteriaQuery(Querys, "AND"), options);
            TongSoBanGhiSauKhiQuery = Convert.ToInt32(result.NumFound);
            foreach (DanhMucSolr itemsolr in result)
            {
                lstReturn.Add(ConvertSolrToJson<LUserJson>(itemsolr, Fields));
            }
            return lstReturn;
        }



        public int GetCount(LUserQuery searhOption)
        {
            SPQuery oQuery = new SPQuery();
            oQuery.ViewFields = BuildViewField("ID");
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = SpListProcess.GetItems(oQuery);
            return SPcoll.Count;
        }


        public string BuildQuery(LUserQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if(!string.IsNullOrEmpty(searhOption.KeySSO))
                oBuildQuery.Append("<And>");
            if (!string.IsNullOrEmpty(searhOption.staffCode))
                oBuildQuery.Append("<And>");
            if (!string.IsNullOrEmpty(searhOption.PerQuery))
                oBuildQuery.Append("<And>");
            if (searhOption.Roles > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.Groups > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.SapDen_SoNgayBaoSinhNhat.HasValue)
                oBuildQuery.Append("<And>");
            if (searhOption.DangDienRa_SoNgayBaoSinhNhat.HasValue)
                oBuildQuery.Append("<And>");
            if (!string.IsNullOrEmpty(searhOption.TaiKhoanTruyCap))
                oBuildQuery.Append("<And>");
            if (searhOption.GetUserPhongBan)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            if (searhOption.ItemIDNotGet > 0)
                oBuildQuery.Append("<And>");
            if (!string.IsNullOrEmpty(searhOption.UserEmail))
                oBuildQuery.Append("<And>");
            if (!string.IsNullOrEmpty(searhOption.UserSDT))
                oBuildQuery.Append("<And>");
            if (searhOption._ModerationStatus == 1)
                oBuildQuery.Append("<And>");
            
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            
            if (searhOption._ModerationStatus == 1)
            {
                oBuildQuery.Append(GetStringModer(0)); //appro là 0
                oBuildQuery.Append("</And>");
            }
            if (!string.IsNullOrEmpty(searhOption.UserSDT))
            {
                oBuildQuery.Append(GetStringEQText("UserSDT", searhOption.UserSDT));
                oBuildQuery.Append("</And>");
            }
            if (!string.IsNullOrEmpty(searhOption.UserEmail))
            {
                oBuildQuery.Append(GetStringEQText("UserEmail", searhOption.UserEmail));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.ItemIDNotGet > 0)
            {
                oBuildQuery.Append(getstringNumNotEQ("ID", searhOption.ItemIDNotGet));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetBylistID)
            {
                oBuildQuery.Append(getStringByListIdOnly(searhOption.lstIDget));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.GetUserPhongBan)
            {
                if (searhOption.UserPhongBan > 0)
                    oBuildQuery.Append(GetStringEQLookUp("UserPhongBan", searhOption.UserPhongBan));
                else oBuildQuery.Append(getstringNull());
                oBuildQuery.Append("</And>");
            }
            if (!string.IsNullOrEmpty(searhOption.TaiKhoanTruyCap))
            {
                oBuildQuery.Append(GetStringEQText("TaiKhoanTruyCap", searhOption.TaiKhoanTruyCap));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.DangDienRa_SoNgayBaoSinhNhat.HasValue)
            {
                oBuildQuery.Append(GetStringEqDate("CountSinhNhat", searhOption.DangDienRa_SoNgayBaoSinhNhat.Value, false));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.SapDen_SoNgayBaoSinhNhat.HasValue)
            {
                oBuildQuery.Append(GetStringGeqDate("CountSinhNhat", searhOption.SapDen_SoNgayBaoSinhNhat.Value.AddDays(1), false));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.Groups > 0)
            {
                oBuildQuery.Append(GetStringEQLookUp("Groups", searhOption.Groups));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.Roles > 0)
            {
                oBuildQuery.Append(GetStringEQLookUp("Roles", searhOption.Roles));
                oBuildQuery.Append("</And>");
            }
            if (!string.IsNullOrEmpty(searhOption.PerQuery))
            {
                oBuildQuery.Append(GetStringContainsText("PerQuery", "|" + searhOption.PerQuery + "|"));
                oBuildQuery.Append("</And>");
            }
            if (!string.IsNullOrEmpty(searhOption.staffCode))
            {
                oBuildQuery.Append(GetStringEQText("staffCode", searhOption.staffCode));
                oBuildQuery.Append("</And>");
            }
            if (!string.IsNullOrEmpty(searhOption.KeySSO))
            {
                oBuildQuery.Append(GetStringEQText("KeySSO", searhOption.KeySSO));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }

        public List<ISolrQuery> BuildQuerySolr(LUserQuery searhOption)
        {
            var Querys = new List<ISolrQuery>();
            Querys.Add(new SolrQuery("*:*"));
            if (searhOption.UrlList != null && searhOption.UrlList.Count > 0)
            {
                //UrlList:("/noidung/Lists/VanBanTaiLieuSuKien" "/noidung/Lists/VanBanTaiLieu")
                Querys.Add(new SolrQuery(string.Format("FullUrlList:({0})", string.Join(" ", searhOption.UrlList.Select(x => "\"" + x + "\"")))));
            }
            else if (SpListProcess != null)
            {
                Querys.Add(new SolrQuery(string.Format("FullUrlList:\"{0}\"", SpListProcess.ParentWeb.ServerRelativeUrl + "/" + SpListProcess.RootFolder.Url)));
            }
            Querys.AddSearchBase(searhOption);

            //DMQuyChe
            if (!string.IsNullOrEmpty(searhOption.UserSDT))
            {
                //oBuildQuery.Append(GetStringEQText("UserSDT", searhOption.UserSDT));
                //oBuildQuery.Append("</And>");
                Querys.Add(new SolrQuery(string.Format("UserSDT:\"{0}\"", searhOption.UserSDT)));
            }
            if (!string.IsNullOrEmpty(searhOption.UserEmail))
            {
                Querys.Add(new SolrQuery(string.Format("UserEmail:\"{0}\"", searhOption.UserEmail)));
            }
            if (searhOption.ItemIDNotGet > 0)
            {
                //oBuildQuery.Append(getstringNumNotEQ("ID", searhOption.ItemIDNotGet));
                //oBuildQuery.Append("</And>");
                Querys.Add(new SolrQuery(string.Format("!SP_ID:\"{0}\"", searhOption.ItemIDNotGet)));
            }
            BuildQueryToString(Querys);
            return Querys;
        }
    }
}