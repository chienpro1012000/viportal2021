using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using ViPortal_Utils.Base;

namespace ViPortalData.LichCaNhan
{
    public class LichCaNhanQuery : QuerySearch
    {
        public DateTime? TuNgay_LichThoiGianBatDau { get; set; }
        public DateTime? DenNgay_LichThoiGianBatDau { get; set; }
        public bool isGetLichTiepTheo { get; set; }
        public bool checkTrungLich { get; set; }
        public DateTime? LichThoiGianBatDau { get; set; }
        public DateTime? LichThoiGianKetThuc { get; set; }
        public int CreatedUser { get; set; }
        public int OldID { get; set; }
        public string LogText { get; set; }
        public LichCaNhanQuery()
        {
        }
        public LichCaNhanQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
}