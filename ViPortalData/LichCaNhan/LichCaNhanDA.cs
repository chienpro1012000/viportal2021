﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using SolrNet;
using SolrNet.Commands.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortalData.LichCaNhan
{
    public class LichCaNhanDA : SPBaseDA<DataSolr>
    {
        private string _urlList = "/noidung/Lists/LichCaNhan";
        public LichCaNhanDA(bool isAdmin = false)
        {
            if (isAdmin)
                InitAdmin(_urlList, true);
            else
                Init(_urlList, true);

        }
        public LichCaNhanDA(string _urlSiteProcess)
        {
            _urlList = _urlSiteProcess + _urlList;
            Init(_urlList,true);
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<LichCaNhanJson> GetListJson(LichCaNhanQuery searhOption, params string[] Fields)
        {
            List<LichCaNhanJson> lstReturn = new List<LichCaNhanJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "fldGroup", "CreatedUser", "LichDiaDiem", "LogText",
                    "LichGhiChu", "LichLanhDaoChuTri", "LichNoiDung", "LichThanhPhanThamGia", "LichThoiGianBatDau", "LichThoiGianKetThuc", "LichTrangThai", "_ModerationStatus", "Created" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<LichCaNhanJson>(SPcoll, Fields);
            return lstReturn;
        }
        public List<ISolrQuery> BuildQuerySolr(LichCaNhanQuery searhOption)
        {
            var Querys = new List<ISolrQuery>();
            Querys.Add(new SolrQuery("*:*"));
            if (searhOption.UrlList != null && searhOption.UrlList.Count > 0)
            {
                //UrlList:("/noidung/Lists/VanBanTaiLieuSuKien" "/noidung/Lists/VanBanTaiLieu")
                Querys.Add(new SolrQuery(string.Format("FullUrlList:({0})", string.Join(" ", searhOption.UrlList.Select(x => "\"" + x + "\"")))));
            }
            else if (SpListProcess != null)
            {
                Querys.Add(new SolrQuery(string.Format("FullUrlList:\"{0}\"", SpListProcess.ParentWeb.ServerRelativeUrl + "/" + SpListProcess.RootFolder.Url)));
            }
            Querys.AddSearchBase(searhOption);
            if (searhOption.CreatedUser > 0)
                Querys.Add(new SolrQuery(string.Format("CreatedUser_Id:\",{0},\"", searhOption.CreatedUser)));
            if (searhOption.DenNgay_LichThoiGianBatDau.HasValue)
            {
                Querys.Add(new SolrQuery(GetQueryDateTimeSolrLeq("LichThoiGianBatDau", searhOption.DenNgay_LichThoiGianBatDau.Value.AddDays(1))));
            }
            if (searhOption.TuNgay_LichThoiGianBatDau.HasValue)
            {
                Querys.Add(new SolrQuery(GetQueryDateTimeSolrGeq("LichThoiGianBatDau", searhOption.TuNgay_LichThoiGianBatDau.Value)));
            }
            if (searhOption.isGetLichTiepTheo)
            {
                //oBuildQuery.Append("<Geq>");
                //oBuildQuery.Append("<FieldRef Name ='LichThoiGianBatDau' />");
                //oBuildQuery.Append("<Value IncludeTimeValue='FALSE' Type ='DateTime'><Today/></Value>");
                //oBuildQuery.Append("</Geq>");
                //oBuildQuery.Append("</And>");
            }
            if (searhOption.checkTrungLich)
            {
                Querys.Add(new SolrQuery($"(LichThoiGianBatDau:[{SPUtility.CreateISO8601DateTimeFromSystemDateTime(searhOption.LichThoiGianBatDau.Value)} TO {SPUtility.CreateISO8601DateTimeFromSystemDateTime(searhOption.LichThoiGianKetThuc.Value)}] OR LichThoiGianKetThuc:[{SPUtility.CreateISO8601DateTimeFromSystemDateTime(searhOption.LichThoiGianBatDau.Value)} TO {SPUtility.CreateISO8601DateTimeFromSystemDateTime(searhOption.LichThoiGianKetThuc.Value)}])"));
            }
            BuildQueryToString(Querys);
            return Querys;
        }
        public List<LichCaNhanJson> GetListJsonSolr(LichCaNhanQuery searhOption, params string[] Fields)
        {
            List<LichCaNhanJson> lstReturn = new List<LichCaNhanJson>();
            if (Fields.Count() == 0)
            {
                Fields = new string[] {"SP_ID", "Title", "fldGroup", "CreatedUser", "LichDiaDiem", "LichGhiChu", "LichLanhDaoChuTri", "LichNoiDung", "LichThanhPhanThamGia", "LichThoiGianBatDau", "LichThoiGianKetThuc", "LichTrangThai", "_ModerationStatus", "Created", "FullUrlList" ,"id","LogNoiDung",
                    "DBPhanLoai", "LogText", "CHU_TRI", "CHUAN_BI","THANH_PHAN","LichPhongBanThamGia"
                }; //Set default
            }
            var Querys = BuildQuerySolr(searhOption);
            #region Option khởi tạo
            var oorders = new List<SortOrder>();

            if (oorders.FindIndex(x => x.FieldName == searhOption.FieldOrder) == -1)
                oorders.Add(new SortOrder(searhOption.FieldOrder == "ID" ? "id" : searhOption.FieldOrder, (searhOption.Ascending) ? Order.ASC : Order.DESC));
            QueryOptions options = new QueryOptions()
            {
                OrderBy = oorders,
                Fields = Fields,
                //StartOrCursor =  new StartOrCursor.Start((currentPage - 1) * rowPerPage)
            };
            if (searhOption.Length > 0)
            {
                options.Rows = searhOption.Length;
                options.Start = searhOption.Start; //Barn ghi dau tien
            }

            #endregion
            var result = solrWorkerDM.Query(new SolrMultipleCriteriaQuery(Querys, "AND"), options);
            TongSoBanGhiSauKhiQuery = Convert.ToInt32(result.NumFound);
            foreach (DataSolr itemsolr in result)
            {
                lstReturn.Add(ConvertSolrToJson<LichCaNhanJson>(itemsolr, Fields));
            }
            return lstReturn;
        }

        public string BuildQuery(LichCaNhanQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if (!string.IsNullOrEmpty(searhOption.LogText))
                oBuildQuery.Append("<And>");
            if (searhOption.OldID > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.CreatedUser > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetLichTiepTheo)
                oBuildQuery.Append("<And>");
            if (searhOption.TuNgay_LichThoiGianBatDau.HasValue)
                oBuildQuery.Append("<And>");
            if (searhOption.DenNgay_LichThoiGianBatDau.HasValue)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            if (searhOption._ModerationStatus == 1)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption._ModerationStatus == 1)
            {
                oBuildQuery.Append(GetStringModer(0)); //appro là 0
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetBylistID)
            {
                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.DenNgay_LichThoiGianBatDau.HasValue)
            {
                oBuildQuery.Append(GetStringCamlDate("LichThoiGianBatDau", searhOption.DenNgay_LichThoiGianBatDau.Value, "Leq"));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.TuNgay_LichThoiGianBatDau.HasValue)
            {
                oBuildQuery.Append(GetStringCamlDate("LichThoiGianBatDau", searhOption.TuNgay_LichThoiGianBatDau.Value, "Geq"));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetLichTiepTheo)
            {
                oBuildQuery.Append("<Geq>");
                oBuildQuery.Append("<FieldRef Name ='LichThoiGianBatDau' />");
                oBuildQuery.Append("<Value IncludeTimeValue='FALSE' Type ='DateTime'><Today/></Value>");
                oBuildQuery.Append("</Geq>");
                oBuildQuery.Append("</And>");
            }
            if (searhOption.CreatedUser > 0)
            {
                oBuildQuery.Append(GetStringEQLookUp("CreatedUser", searhOption.CreatedUser));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.OldID > 0)
            {
                oBuildQuery.Append(GetStringEQText("OldID", searhOption.OldID.ToString()));
                oBuildQuery.Append("</And>");
            }
            if (!string.IsNullOrEmpty(searhOption.LogText))
            {
                oBuildQuery.Append(GetStringContainsText("LogText", searhOption.LogText));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}