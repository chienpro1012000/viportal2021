﻿using Microsoft.SharePoint;
using SolrNet;
using SolrNet.Commands.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace ViPortalData.TraLoiHoiDap
{
    public class TraLoiHoiDapDA : SPBaseDA<DataSolr>
    {
        private string _urlList = "/noidung/Lists/TraLoiHoiDap";
        public TraLoiHoiDapDA(bool isAdmin = false)
        {
            if (isAdmin)
                InitAdmin(_urlList);
            else
                Init(_urlList);
        }
        public TraLoiHoiDapDA(string _urlSiteProcess , bool isAdmin = false)
        {
            _urlList = _urlSiteProcess + "/noidung/Lists/TraLoiHoiDap";
            if (isAdmin)
                InitAdmin(_urlList);
            else
                Init(_urlList);
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<TraLoiHoiDapJson> GetListJson(TraLoiHoiDapQuery searhOption, params string[] Fields)
        {
            List<TraLoiHoiDapJson> lstReturn = new List<TraLoiHoiDapJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "FAQNgayTraLoi", "FAQNoiDungTraLoi", "FQNguoiTraLoi", "IDHoiDap", "_ModerationStatus" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<TraLoiHoiDapJson>(SPcoll, Fields);
            return lstReturn;
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<TraLoiHoiDapItem> GetListObj(TraLoiHoiDapQuery searhOption, params string[] Fields)
        {
            List<TraLoiHoiDapItem> lstReturn = new List<TraLoiHoiDapItem>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "FAQNgayTraLoi", "FAQNoiDungTraLoi", "FQNguoiTraLoi", "IDHoiDap", "_ModerationStatus", "Attachments" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListObj<TraLoiHoiDapItem>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(TraLoiHoiDapQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if (searhOption.LstIDHoiDap?.Count > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.IDHoiDap > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            if(searhOption._ModerationStatus == 1)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption._ModerationStatus == 1)
            {
                oBuildQuery.Append(GetStringModer(0));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            if (searhOption.IDHoiDap > 0)
            {
                oBuildQuery.Append(GetStringEQLookUp("IDHoiDap", searhOption.IDHoiDap));

                oBuildQuery.Append("</And>");
            }
            if (searhOption.LstIDHoiDap?.Count > 0)
            {
                oBuildQuery.Append(getStringMultiLookup(searhOption.LstIDHoiDap, "IDHoiDap"));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
        public List<ISolrQuery> BuildQuerySolr(TraLoiHoiDapQuery searhOption)
        {
            var Querys = new List<ISolrQuery>();
            Querys.Add(new SolrQuery("*:*"));
            if (searhOption.UrlList != null && searhOption.UrlList.Count > 0)
            {
                //UrlList:("/noidung/Lists/VanBanTaiLieuSuKien" "/noidung/Lists/VanBanTaiLieu")
                Querys.Add(new SolrQuery(string.Format("FullUrlList:({0})", string.Join(" ", searhOption.UrlList.Select(x => "\"" + x + "\"")))));
            }
            else if (SpListProcess != null)
            {
                Querys.Add(new SolrQuery(string.Format("FullUrlList:\"{0}\"", SpListProcess.ParentWeb.ServerRelativeUrl + "/" + SpListProcess.RootFolder.Url)));
            }
            Querys.AddSearchBase(searhOption);
            if (searhOption.IDHoiDap > 0)
                Querys.Add(new SolrQuery(string.Format("IDHoiDap_Id:\",{0},\"", searhOption.IDHoiDap)));
            BuildQueryToString(Querys);
            return Querys;
        }
        public List<TraLoiHoiDapJson> GetListJsonSolr(TraLoiHoiDapQuery searhOption, params string[] Fields)
        {
            List<TraLoiHoiDapJson> lstReturn = new List<TraLoiHoiDapJson>();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "SP_ID", "Title", "FAQNgayTraLoi", "FAQNoiDungTraLoi", "FQNguoiTraLoi", "IDHoiDap", "_ModerationStatus" }; //Set default
            }
            var Querys = BuildQuerySolr(searhOption);
            #region Option khởi tạo
            var oorders = new List<SortOrder>();

            if (oorders.FindIndex(x => x.FieldName == searhOption.FieldOrder) == -1)
                oorders.Add(new SortOrder(searhOption.FieldOrder == "ID" ? "id" : searhOption.FieldOrder, (searhOption.Ascending) ? Order.ASC : Order.DESC));
            QueryOptions options = new QueryOptions()
            {
                OrderBy = oorders,
                Fields = Fields,
                //StartOrCursor =  new StartOrCursor.Start((currentPage - 1) * rowPerPage)
            };
            if (searhOption.Length > 0)
            {
                options.Rows = searhOption.Length;
                options.Start = searhOption.Start; //Barn ghi dau tien
            }

            #endregion
            var result = solrWorkerDM.Query(new SolrMultipleCriteriaQuery(Querys, "AND"), options);
            TongSoBanGhiSauKhiQuery = Convert.ToInt32(result.NumFound);
            foreach (DataSolr itemsolr in result)
            {
                lstReturn.Add(ConvertSolrToJson<TraLoiHoiDapJson>(itemsolr, Fields));
            }
            return lstReturn;
        }
    }
}