using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;

namespace ViPortalData.TraLoiHoiDap
{
	public class TraLoiHoiDapItem : SPEntity
	{
		public string FAQNoiDungTraLoi { set; get; }
		public DateTime? FAQNgayTraLoi { set; get; }
		public SPFieldLookupValue FQNguoiTraLoi { set; get; }
		public SPFieldLookupValue IDHoiDap { set; get; }
		public TraLoiHoiDapItem()
		{
			FQNguoiTraLoi = new SPFieldLookupValue();
			IDHoiDap = new SPFieldLookupValue();
			FAQNgayTraLoi = DateTime.Now;
		}
	}
	public class TraLoiHoiDapJson : EntityJson
	{
		public string FAQNoiDungTraLoi { set; get; }
		public DateTime? FAQNgayTraLoi { set; get; }
		public LookupData FQNguoiTraLoi { set; get; }
		public LookupData IDHoiDap { set; get; }
		public TraLoiHoiDapJson()
		{
			FQNguoiTraLoi = new LookupData();
			IDHoiDap = new LookupData();
		}
	}
}