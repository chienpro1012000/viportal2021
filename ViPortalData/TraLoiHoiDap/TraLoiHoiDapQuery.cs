using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using ViPortal_Utils.Base;

namespace ViPortalData.TraLoiHoiDap
{
    public class TraLoiHoiDapQuery : QuerySearch
    {
        public int IDHoiDap { get; set; }
        public List<int> LstIDHoiDap { get; set; }
        public TraLoiHoiDapQuery()
        {
            LstIDHoiDap = new List<int>();
        }
        public TraLoiHoiDapQuery(HttpRequest request)
            : base(request)
        {
            LstIDHoiDap = new List<int>();
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
}