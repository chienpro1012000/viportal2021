using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public class CongThanhPhanDA : SPBaseDA<DanhMucSolr>
    {
        private string _urlList = "/noidung/Lists/CongThanhPhan";
        public CongThanhPhanDA()
        {
            Init(_urlList);
        }
        public CongThanhPhanDA(string _urlListProcess)
            : base(_urlListProcess + "/noidung/Lists/CongThanhPhan")
        {
            _urlList = _urlListProcess;
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<CongThanhPhanJson> GetListJson(CongThanhPhanQuery searhOption, params string[] Fields)
        {
            List<CongThanhPhanJson> lstReturn = new List<CongThanhPhanJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "UrlSite","MoTa", "_ModerationStatus","STT", "Public"  }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<CongThanhPhanJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(CongThanhPhanQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if(!string.IsNullOrEmpty(searhOption.UrlSite))
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            if (!string.IsNullOrEmpty(searhOption.UrlSite))
            {
                oBuildQuery.Append(GetStringEQText("UrlSite", searhOption.UrlSite));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}