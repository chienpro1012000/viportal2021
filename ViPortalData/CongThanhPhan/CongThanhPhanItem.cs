using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{
    public class CongThanhPhanItem : SPEntity
    {
        public string UrlSite { set; get; }
        public string MoTa { set; get; }
        public bool Public { set; get; }
        public int STT { set; get; }

        public CongThanhPhanItem()
        {

        }
    }
    public class CongThanhPhanJson : EntityJson
    {
        public string UrlSite { set; get; }
        public string MoTa { set; get; }
        public bool Public { set; get; }
        public int STT { set; get; }

        public CongThanhPhanJson()
        {

        }
    }
}
