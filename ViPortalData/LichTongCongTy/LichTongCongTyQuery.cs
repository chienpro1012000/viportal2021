using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using ViPortal_Utils.Base;

namespace ViPortalData.LichTongCongTy
{
    public class LichTongCongTyQuery : QuerySearch
    {
        public DateTime? TuNgay_LichThoiGianBatDau { get; set; }
        public DateTime? DenNgay_LichThoiGianBatDau { get; set; }
        public LichTongCongTyQuery()
        {
        }
        public LichTongCongTyQuery(HttpRequest request)
            : base(request)
        {
            PropertyInfo[] properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (PropertyInfo pinfo in properties)
            {
                pinfo.SetValue(this, RequestValue(pinfo, request), null);
            }
        }
    }
}