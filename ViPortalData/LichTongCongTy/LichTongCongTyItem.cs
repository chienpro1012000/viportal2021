using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.JSGrid;
using ViPortal_Utils.Base;

namespace ViPortalData.LichTongCongTy
{
	public class LichTongCongTyItem : SPEntity
	{
		public string LichDiaDiem { set; get; }
		public string LichGhiChu { set; get; }
		public SPFieldLookupValue LichLanhDaoChuTri { set; get; }
		public string LichNoiDung { set; get; }
		public SPFieldLookupValueCollection LichThanhPhanThamGia { set; get; }
		public string NoiDungChuanBi { set; get; }
		public DateTime? LichThoiGianBatDau { set; get; }
		public DateTime? LichThoiGianKetThuc { set; get; }
		public int LichTrangThai { set; get; }
		public string CHU_TRI { set; get; }
		public string OldID { get; set; }
		public string CHUAN_BI { set; get; }
		public string THANH_PHAN { set; get; }
		public string LogText { get; set; }
		public int DBPhanLoai { get; set; }
		public LichTongCongTyItem()
		{
			LichLanhDaoChuTri = new SPFieldLookupValue();
			LichThanhPhanThamGia = new SPFieldLookupValueCollection();
			LichGhiChu = string.Empty;
			LichNoiDung = string.Empty;
			LogText = string.Empty;
		}
	}
	public class LichTongCongTyJson : EntityJson
	{
		public string LichDiaDiem { set; get; }
		public string LichGhiChu { set; get; }
		public string LogText { get; set; }
		public LookupData LichLanhDaoChuTri { set; get; }
		public string LichNoiDung { set; get; }
		public List<LookupData> LichThanhPhanThamGia { set; get; }
		public int DBPhanLoai { get; set; }
		public DateTime? LichThoiGianBatDau { set; get; }
		public DateTime? LichThoiGianKetThuc { set; get; }
		public string THANH_PHAN { set; get; }
		public string CHU_TRI { set; get; }
		public string CHUAN_BI { set; get; }
		public int LichTrangThai { set; get; }
		public string NoiDungChuanBi { set; get; }
		public LichTongCongTyJson()
		{
			LichLanhDaoChuTri = new LookupData();
			LichThanhPhanThamGia = new List<LookupData>();
		}
	}

	public class LichTichHop
	{
		public string ID { get; set; }
		public string NOI_DUNG { get; set; }
		public string DIA_DIEM { get; set; }
		public string THANH_PHAN { get; set; }
		public string CHU_TRI { get; set; }
		public string CHUAN_BI { get; set; }
		public string tg_bat_dau { get; set; }
		public string THOI_GIAN { get; set; }
		public string NGAY_THANG { get; set; }
		public string ID_Cuoc_Hop { get; set; }
		public string ID_VPKG { get; set; }
		public string TT { get; set; }
	}
}