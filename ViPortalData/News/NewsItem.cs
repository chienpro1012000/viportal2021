using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{
    public class NewsItem : SPEntity
    {
        public string DescriptionNews { set; get; }
        public DateTime? CreatedDate { set; get; }
        public string ImageNews { set; get; }
        public string ContentNews { set; get; }
        public string TacGia { set; get; }
        public bool isHotNew { set; get; }

        public NewsItem()
        {
            CreatedDate = DateTime.Now;
        }
    }

    public class NewsJson : EntityJson
    {
        public string DescriptionNews { set; get; }
        public DateTime? CreatedDate { set; get; }
        public string ImageNews { set; get; }
        public string ContentNews { set; get; }
        public string TacGia { set; get; }
        public bool isHotNew { set; get; }

        public NewsJson()
        {
            CreatedDate = DateTime.Now;
            ImageNews = "/Content/themeV1/img/media/Mask-4@2x.png";
        }
    }
    public class ObjectShareNew
    {
        public List<NewShare> newsList { get; set; }
        public int totalPages { get; set; }
    }
    public class NewShare
    {
        public int id { get; set; }
        public string newsTitle { get; set; }
        public string newsCategoryName { get; set; }
        public string newsKey { get; set; }
        public string newsSummary { get; set; }

        public string newsPictureThumbnailUrl { get; set; }
        public string newsContent { get; set; }
        public string newsAuthor { get; set; }

        public string newsCategoryKey { get; set; }

        public DateTime newsPublishedDate { get; set; }
    }
}
