using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using ViPortal_Utils.Base;
namespace VIPortalAPP
{
    public class BirthdayItem : SPEntity
    {
        public string ID_LLNS { set; get; }
        public string TENKHAISINH { set; get; }
        public DateTime? NGAYSINH { set; get; }
        public string TEN_CVU { set; get; }
        public int ID_DONVI { set; get; }
        public string TEN_PHONGB { set; get; }
        public string TEN_DONVI { set; get; }
        public DateTime? BIRTHDAY { set; get; }

        public BirthdayItem()
        {

        }
    }
    public class BirthdayJson : EntityJson
    {
        public string ID_LLNS { set; get; }
        public string TENKHAISINH { set; get; }
        public DateTime? NGAYSINH { set; get; }
        public string TEN_CVU { set; get; }
        public int ID_DONVI { set; get; }
        public string TEN_PHONGB { set; get; }
        public string TEN_DONVI { set; get; }
        public DateTime? BIRTHDAY { set; get; }

        public BirthdayJson()
        {

        }
    }
}
