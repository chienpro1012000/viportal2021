using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;

namespace VIPortalAPP
{
    public class BirthdayDA : SPBaseDA<DataSolr>
    {
        private string _urlList = "/noidung/Lists/Birthday";
        public BirthdayDA()
        {
            Init(_urlList);
        }
        public BirthdayDA(string _urlListProcess)
            : base(_urlListProcess)
        {
            _urlList = _urlListProcess;
        }
        /// <summary>
        /// 
        /// Author      Date        Comments
        /// VINHHQ      27/03
        /// </summary>
        /// <param name="searhOption"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="currentPage"></param>
        /// <param name="FieldSort"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public List<BirthdayJson> GetListJson(BirthdayQuery searhOption, params string[] Fields)
        {
            List<BirthdayJson> lstReturn = new List<BirthdayJson>();
            SPQuery oQuery = new SPQuery();
            if (Fields.Count() == 0)
            {
                Fields = new string[] { "ID", "Title", "ID_LLNS", "TENKHAISINH", "NGAYSINH", "TEN_CVU", "ID_DONVI", "TEN_PHONGB", "TEN_DONVI", "BIRTHDAY" }; //Set default
            }
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append(StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            oBuildQuery.Append(BuildQuery(searhOption));
            oQuery.Query = oBuildQuery.ToString();
            SPListItemCollection SPcoll = getPageBySPQueryFix(oQuery, searhOption.Length, searhOption.Start, StringOrderBy(searhOption.FieldOrder, searhOption.Ascending));
            lstReturn = ConvertListJson<BirthdayJson>(SPcoll, Fields);
            return lstReturn;
        }

        public string BuildQuery(BirthdayQuery searhOption)
        {
            StringBuilder oBuildQuery = new StringBuilder();
            oBuildQuery.Append("<Where>");
            if (searhOption.ID_DONVI > 0)
                oBuildQuery.Append("<And>");
            if (searhOption.TuNgay.HasValue)
                oBuildQuery.Append("<And>");
            if (searhOption.DenNgay.HasValue)
                oBuildQuery.Append("<And>");
            if (searhOption.isGetBylistID)
                oBuildQuery.Append("<And>");
            oBuildQuery.Append(getDynamicSearch(searhOption.SearchIn, searhOption.Keyword));
            if (searhOption.isGetBylistID)
            {

                oBuildQuery.Append(getStringByListId(searhOption.lstIDget));

                oBuildQuery.Append("</And>");
            }
            if (searhOption.DenNgay.HasValue)
            {
                oBuildQuery.Append(GetStringCamlDate("BIRTHDAY", searhOption.DenNgay.Value, "Leq"));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.TuNgay.HasValue)
            {
                oBuildQuery.Append(GetStringCamlDate("BIRTHDAY", searhOption.TuNgay.Value, "Geq"));
                oBuildQuery.Append("</And>");
            }
            if (searhOption.ID_DONVI > 0)
            {
                oBuildQuery.Append(getstringNumEQ("ID_DONVI", searhOption.ID_DONVI));
                oBuildQuery.Append("</And>");
            }
            oBuildQuery.Append("</Where>");
            return oBuildQuery.ToString();
        }
    }
}