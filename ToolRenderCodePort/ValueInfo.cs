﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToolRenderCodePort
{
   public class ValueInfo
    {
        public Guid ID { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
        public Guid ParentID { get; set; }
        public List<ValueInfo> lstChild { get; set; }
    }
}
