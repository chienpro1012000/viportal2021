﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToolRenderCodePort
{
   public class FieldInfo
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public int Index { get; set; }
        public string Title { get; set; }
       public FieldInfo()
        {
            Index = 999;
        }
    }
}
