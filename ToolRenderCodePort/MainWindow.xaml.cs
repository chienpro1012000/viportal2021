﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using wform = System.Windows.Forms;
using Microsoft.SharePoint;
using System.IO;
namespace ToolRenderCodePort
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        List<FieldInfo> lsFields = new List<FieldInfo>();
        private void btnChoosePath_Click(object sender, RoutedEventArgs e)
        {
            var folderDialog = new wform.FolderBrowserDialog();
            folderDialog.SelectedPath = "C:\\";
            wform.DialogResult result = folderDialog.ShowDialog();
            if (result.ToString() == "OK")
                txtPath.Text = folderDialog.SelectedPath;
        }
        private void btnOpen_Click(object sender, RoutedEventArgs e)
        {
            GetSite();
        }
        private void txtSite_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                GetSite();
        }
        private void GetSite()
        {
            try
            {
                List<ValueInfo> webs = new List<ValueInfo>();
                if (txtSite.Text.Length == 0)
                    return;
                SPSite site = new SPSite(txtSite.Text);
                foreach (SPWeb web in site.AllWebs)
                {
                    webs.Add(new ValueInfo()
                    {
                        Name = web.Title,
                        ID = web.ID,
                        Url = web.Url,
                        ParentID = web.ParentWebId
                    });
                }
                GetChildren(webs, Guid.Empty, string.Empty);

                cboWeb.ItemsSource = webs;
                cboWeb.Items.Refresh();
                cboWeb.SelectedIndex = 0;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }


        private List<ValueInfo> GetChildren(List<ValueInfo> lst, Guid parentId, string str)
        {
            str += "--- ";
            lst = lst.Where(c => c.ParentID == parentId)
                    .Select(c => { c.lstChild = GetChildren(lst, c.ID, str); c.Name = str + c.Name; return c; })
                    .ToList();
            return lst;
        }
        private void cboWeb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var value = cboWeb.SelectedValue;
            SPSite site = new SPSite(txtSite.Text);
            SPWeb web = site.OpenWeb(Guid.Parse(value.ToString()));
            SPListCollection listcols = web.Lists;
            List<ValueInfo> lists = new List<ValueInfo>();
            foreach (SPList list in listcols)
            {
                if (list.AllowDeletion)
                    lists.Add(new ValueInfo()
                    {
                        Name = list.Title,
                        ID = list.ID,
                        Url = list.RootFolder.ServerRelativeUrl
                    });
            }
            cboLists.ItemsSource = lists;
            cboLists.Items.Refresh();

        }
        private void cboLists_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string url = cboLists.SelectedValue.ToString();
            var value = cboWeb.SelectedValue;
            SPSite site = new SPSite(txtSite.Text);
            SPWeb web = site.OpenWeb(Guid.Parse(value.ToString()));
            spList = web.GetList(url);

            txtName.Text = spList.Title;

            lsFields = new List<FieldInfo>();
            foreach (SPField f in spList.Fields)
            {
                if (!f.FromBaseType)
                {
                    lsFields.Add(new FieldInfo()
                    {
                        Name = f.EntityPropertyName,
                        Type = f.TypeAsString
                    });
                }
            }
            dtgFields.ItemsSource = lsFields;
            dtgFields.Items.Refresh();
        }

        public string ReplaceType(string outType)
        {
            switch (outType)
            {
                case "Boolean":
                    return "bool";
                case "DateTime":
                    return "DateTime?";
                case "Number":
                    return "int";
                case "Lookup":
                    return "SPFieldLookupValue";
                case "LookupMulti":
                    return "SPFieldLookupValueCollection";
                default:
                    return "string";
            }
        }
        public string ReplaceControlHTML(string outType, string name, string title)
        {
            switch (outType)
            {
                case "Note":
                    return string.Format("<textarea  name=\"{0}\" id=\"{0}\" class=\"form-control\"><%:{1}%></textarea>", name, "o" + txtName.Text + "." + name);
                case "Boolean":
                    return string.Format("<input value=\"true\" type=\"checkbox\" name=\"{0}\" id=\"{0}\" <%={1}%> />", name, string.Format("o{0}.{1}? \"checked\" : string.Empty", txtName.Text, name));
                case "DateTime":
                    return string.Format("<input type=\"text\" name=\"{0}\" id=\"{0}\" value=\"{1}\" class=\"form-control input-datetime\"/>", name, "<%:string.Format(\"{0:dd/MM/yyyy}\"," + "o" + txtName.Text + "." + name + ")%>");
                case "Lookup":
                    return string.Format("<select data-selected=\"<%:{3}%>\" data-url=\"/UserControls/{2}/pAction.ashx?do=alljson\" data-place=\"Chọn {1}\" name=\"{0}\" id=\"{0}\" class=\"form-control\"></select>",
                        name, title, txtName.Text, "o" + txtName.Text + "." + name + ".LookupId");
                case "LookupMulti":
                    return string.Format("<select data-selected=\"<%:string.Join(", ", {3}.Select(x=>x.LookupId))%>\" data-url=\"/UserControls/{2}/pAction.ashx?do=alljson\" data-place=\"Chọn {1}\" name=\"{0}\" id=\"{0}\" class=\"form-control\" multiple></select>", 
                        name, title, txtName.Text, "o" + txtName.Text + "." + name);
                case "HTML":
                    return string.Format("<textarea name=\"{0}\" id=\"{0}\" class=\"form-control\"><%={1}%></textarea>", name, "o" + txtName.Text + "." + name);
                default:
                    return string.Format("<input type=\"text\" name=\"{0}\" id=\"{0}\" placeholder=\"Nhập {1}\" value=\"<%:{2}%>\" class=\"form-control\"/>", name, title.ToLower(), "o" + txtName.Text + "." + name);
            }

        }
        private void btnRenderCode_Click(object sender, RoutedEventArgs e)
        {
            lsFields = lsFields.OrderBy(item => item.Index).ToList();
            #region Code Class
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\File\codeclass.txt";
            string codeClass = File.ReadAllText(path);
            StringBuilder sblCodeClass = new StringBuilder();
            foreach (var field in lsFields)
            {
                sblCodeClass.AppendFormat("public {0} {1} {{set;get;}}\n\t", ReplaceType(field.Type), field.Name);
            }
            string code = sblCodeClass.ToString();
            sblCodeClass = new StringBuilder();
            foreach (var field in lsFields)
            {
                if (field.Type.Equals("Lookup") || field.Type.Equals("LookupMulti"))
                    sblCodeClass.AppendFormat("{0}=new {1}();\n\t", field.Name, ReplaceType(field.Type));
            }
            string codeKhoiTao = sblCodeClass.ToString();
            string codeClassFinish = codeClass.Replace("{0}", txtNameSpace.Text).Replace("{1}", txtName.Text + "Item").Replace("{2}", code).Replace("{3}", codeKhoiTao);// string.Format(codeClass,, ,"");
            txtCodeClass.Text = codeClassFinish;
            #endregion
            #region code class DA
            string pathDA = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\File\codeclassDA.txt";
            string codeClassDA = File.ReadAllText(pathDA);
            string codeClassFinishDA = codeClassDA.Replace("{0}", txtNameSpace.Text).Replace("{1}", txtName.Text).Replace("{2}", spList.RootFolder.ServerRelativeUrl);// string.Format(codeClass,, ,"");
            txtCodeClassDA.Text = codeClassFinishDA;
            #endregion
            #region code class Query
            string pathQuery = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\File\codeClassQuery.txt";
            string codeClassQuery = File.ReadAllText(pathQuery);
            string codeClassFinishQuery = codeClassQuery.Replace("{0}", txtNameSpace.Text).Replace("{1}", txtName.Text);// string.Format(codeClass,, ,"");
            //txtCodeClassDA.Text = codeClassFinishQuery;

            #endregion
            #region Code Form ASPX
            string pathForm = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\File\codeFormAspx.txt";
            string codeForm = File.ReadAllText(pathForm);
            StringBuilder sblCodeForm = new StringBuilder();
            foreach (var field in lsFields)
            {
                sblCodeForm.AppendLine("<div class=\"form-group row\">");
                sblCodeForm.AppendLine("\t<label for=\"" + field.Name + "\" class=\"col-sm-2 control-label\">" + (!string.IsNullOrEmpty(field.Title) ? field.Title : field.Name) + "</label>");
                sblCodeForm.AppendLine("\t<div class=\"col-sm-10\">");
                sblCodeForm.AppendLine("\t\t" + ReplaceControlHTML(field.Type, field.Name, (!string.IsNullOrEmpty(field.Title) ? field.Title : field.Name)));
                sblCodeForm.AppendLine("\t</div>");
                sblCodeForm.AppendLine("</div>");
            }
            string codeFormFinish = codeForm.Replace("{0}", txtName.Text).Replace("{1}", txtNameSpace.Text).Replace("{2}", sblCodeForm.ToString());// string.Format(codeClass,, ,"");
            txtCodeForm.Text = codeFormFinish;
            #endregion
            #region Code Form ASPX.CS
            string pathFormCS = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\File\codeFormcs.txt";
            string codeFormCS = File.ReadAllText(pathFormCS);
            string codeFormCSFinish = codeFormCS.Replace("{0}", txtNameSpace.Text).Replace("{1}", txtName.Text);
            txtCodeFormCS.Text = codeFormCSFinish;
            #endregion
            #region code form aspx design
            string pathFormDesign = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\File\codeFormDesign.txt";
            string codeFormDesign = File.ReadAllText(pathFormDesign);
            string codeFormDesignFinish = codeFormDesign.Replace("{0}", txtNameSpace.Text).Replace("{1}", txtName.Text);
            #endregion
            #region Code Action ASHX
            string pathASHX = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\File\codeActionASHXCS.txt";
            string codeASHX = File.ReadAllText(pathASHX);
            string codeASHXFinish = codeASHX.Replace("{0}", txtNameSpace.Text).Replace("{1}", txtName.Text);// string.Format(codeClass,, ,"");
            txtCodeAction.Text = codeASHXFinish;

            string strASHX = string.Format("<%@ WebHandler Language=\"C#\" CodeBehind=\"pAction.ashx.cs\" Class=\"{0}.pAction\" %>", txtNameSpace.Text);
            #endregion
            #region Code View

            string pathViewASPX = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\File\codeViewaspx.txt";
            string codeViewASPX = File.ReadAllText(pathViewASPX);
            StringBuilder sblCodeView = new StringBuilder();
            foreach (var field in lsFields)
            {
                sblCodeView.AppendLine("<div class=\"form-group\">");
                sblCodeView.AppendLine("\t<label for=\"" + field.Name + "\" class=\"col-sm-2 control-label\">" + (!string.IsNullOrEmpty(field.Title) ? field.Title : field.Name) + "</label>");
                sblCodeView.AppendLine("\t<div class=\"col-sm-10\">");
                if (field.Type == "Boolean")
                {
                    sblCodeView.AppendLine("\t\t" + string.Format("<%=o{0}.{1}? \"Có\" : \"Không\"%>", txtName.Text, field.Name) + "");
                }
                else if (field.Type == "DateTime")
                    sblCodeView.AppendLine("\t\t" + string.Format("<%=string.Format(\"{{0:dd/MM/yyyy hh:mm tt}}\", o{0}.{1})%>", txtName.Text, field.Name) + "");
                else sblCodeView.AppendLine("\t\t" + string.Format("<%=o{0}.{1}%>", txtName.Text, field.Name) + "");
                sblCodeView.AppendLine("\t</div>");
                sblCodeView.AppendLine("</div>");
            }

            string codeViewASPXFinish = codeViewASPX.Replace("{0}", txtNameSpace.Text).Replace("{1}", txtName.Text).Replace("{2}", sblCodeView.ToString());
            txtCodeView.Text = codeViewASPXFinish;

            //
            string pathViewCS = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\File\codeViewcs.txt";
            string codeViewCS = File.ReadAllText(pathViewCS);
            string codeViewCSFinish = codeViewCS.Replace("{0}", txtNameSpace.Text).Replace("{1}", txtName.Text);

            //txtCodeView.Text = codeViewCSFinish;
            string pathViewDesign = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\File\codeViewDesign.txt";
            string codeViewDesign = File.ReadAllText(pathViewDesign);
            string codeViewDesignFinish = codeViewDesign.Replace("{0}", txtNameSpace.Text).Replace("{1}", txtName.Text);

            #endregion

            #region Code PList ASPX
            string pathUserControl = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\File\codeUserControl.txt";
            string codeUserControl = File.ReadAllText(pathUserControl);
            string codeUserControlFinish = codeUserControl.Replace("{0}", txtNameSpace.Text).Replace("{1}", txtName.Text).Replace("{2}", txtFormName.Text);// string.Format(codeClass,, ,"");
            txtUserCotrol.Text = codeUserControlFinish;

            //

            //
            //string strASHX = string.Format("<%@ WebHandler Language=\"C#\" CodeBehind=\"hAction{0}.ashx.cs\" Class=\"{1}.hAction{0}\" %>",txtName.Text,txtNameSpace.Text);
            //File.WriteAllText(FullPath + @"/hAction" + txtName.Text + ".ashx", strASHX);

            #endregion
            #region Code PList CS
            string pathUserControlCS = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\File\codeUserControlCS.txt";
            string codeUserControlCS = File.ReadAllText(pathUserControlCS);
            string codeUserControlFinishCS = codeUserControlCS.Replace("{0}", txtNameSpace.Text).Replace("{1}", txtName.Text);
            txtUserCotrolCS.Text = codeUserControlFinishCS;

            #endregion
            #region Code PList CS Design
            string pathUserControlCSDesign = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\File\codeUserControlCSDesign.txt";
            string codeUserControlCSDesign = File.ReadAllText(pathUserControlCSDesign);
            string codeUserControlFinishCSDesign = codeUserControlCSDesign.Replace("{0}", txtNameSpace.Text).Replace("{1}", txtName.Text);
            #endregion
            #region Code Webpart
            string pathWebpartCS = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\File\codeWebpartcs.txt";
            string codeWebpartCS = File.ReadAllText(pathWebpartCS);
            string codeWebpartFinishCS = codeWebpartCS.Replace("{0}", txtNameSpace.Text).Replace("{1}", txtName.Text);
            txtWebPARTCS.Text = codeWebpartFinishCS;
            #endregion
            #region Code Toolpart
            string pathToolpartCS = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\File\codeWebpartToolpart.txt";
            string codeToolpartCS = File.ReadAllText(pathToolpartCS);
            string codeToolpartFinishCS = codeToolpartCS.Replace("{0}", txtNameSpace.Text).Replace("{1}", txtName.Text);
            txtToolpartCS.Text = codeToolpartFinishCS;
            #endregion
            #region ghe code ra file
            if (txtPath.Text.Length > 0)
            {
                string FullPath = txtPath.Text + @"/" + txtName.Text;
                if (!Directory.Exists(FullPath))
                    Directory.CreateDirectory(FullPath);

                File.WriteAllText(FullPath + @"/" + txtName.Text + "Query.cs", codeClassFinishQuery);
                File.WriteAllText(FullPath + @"/" + txtName.Text + "Item.cs", codeClassFinish);
                File.WriteAllText(FullPath + @"/" + txtName.Text + "DA.cs", codeClassFinishDA);
                File.WriteAllText(FullPath + @"/pForm" + txtName.Text + ".aspx", codeFormFinish);
                File.WriteAllText(FullPath + @"/pForm" + txtName.Text + ".aspx.cs", codeFormCSFinish);
                File.WriteAllText(FullPath + @"/pForm" + txtName.Text + ".aspx.designer.cs", codeFormDesignFinish);
                File.WriteAllText(FullPath + @"/pAction.ashx", strASHX);
                File.WriteAllText(FullPath + @"/pAction.ashx.cs", codeASHXFinish);
                File.WriteAllText(FullPath + @"/pView" + txtName.Text + ".aspx", codeViewASPXFinish);
                File.WriteAllText(FullPath + @"/pView" + txtName.Text + ".aspx.cs", codeViewCSFinish);
                File.WriteAllText(FullPath + @"/pView" + txtName.Text + ".aspx.designer.cs", codeViewDesignFinish);
                File.WriteAllText(FullPath + @"/UC_" + txtName.Text + ".ascx", codeUserControlFinish);
                File.WriteAllText(FullPath + @"/UC_" + txtName.Text + ".ascx.cs", codeUserControlFinishCS);
                File.WriteAllText(FullPath + @"/UC_" + txtName.Text + ".ascx.designer.cs", codeUserControlFinishCSDesign);
                //File.WriteAllText(FullPath + @"/wpQuanTri" + txtName.Text + ".cs", codeWebpartFinishCS);
                //File.WriteAllText(FullPath + @"/wpQuanTri" + txtName.Text + "ToolPart.cs", codeToolpartFinishCS);
            }

            #endregion
        }


        public SPList spList { get; set; }
    }
}
