﻿using Microsoft.SharePoint;
using Newtonsoft.Json;
using SolrNet;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ViPortal_Utils;
using ViPortal_Utils.Base;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.Lconfig;
using ViPortalData.LichTongCongTy;
using ViPortalData.MailPending;

namespace ToolSendMail
{
    
    public class DanhBaRespon
    {
        public bool suc { get; set; }
        public string msg { get; set; }
        public List<DanhBaDongBo> data { get; set; }
    }
    public class DanhBaDongBo
    {
        public int donvi_id { get; set; }
        public string tendonvi { get; set; }
        public string diachi { get; set; }
        public DateTime ngaysinh { get; set; }
        public string tenkhaisinh { get; set; }
        public bool gioitinh { get; set; }
        public string dienthoai { get; set; }
        public string email { get; set; }
        public float department_id { get; set; }
        public string tenphongban { get; set; }
    }
    class Program
    {
        public static List<LNotificationJson> lstLNotification { get; set; }
        public static LconfigDA oLconfigDA { get; set; }
        public static LNotificationDA oLNotificationDA { get; set; }
        public static LNotificationItem oLNotificationItem { get; set; }
        public static LUserDA oLUserDA { get; set; }
        public static MailPendingDA oMailPendingDA { get; set; }
        public static MailPendingItem oMailPendingItem { get; set; }
        public static string UrlSite { get; set; }
        public static List<string> mailto { get; set; }
        public static DateTime firstDayv1 { get; set; }
        public static int intCurYear = System.DateTime.Today.Year;

        public static DateTime firstDay = System.DateTime.Today;
        public static int intCurWeek = 1;
        public static string strYear = "";
        public static string strlastyear = "";
        public static string strnextyear = "";

        public static string strWeek = "";
        public static string strlastweek = "";
        public static string strnextweek = "";
        static void Main(string[] args)
        {
            StartupSolr.Init<ViPortal_Utils.DataSolr>("http://solrserver2:9000/solr/Portal2021");
            StartupSolr.Init<ViPortal_Utils.DanhMucSolr>("http://solrserver2:9000/solr/Portal2021DanhMuc");
            StartupSolr.Init<ViPortal_Utils.LogHeThongSolr>("http://solrserver2:9000/solr/Portal2021Log");
            //DongBoLich();
            //DongBoLich();
            //GetNotifiAndSendMail();
            //DongBoDanhBa();
            


            Console.WriteLine("Finish!");
        }

        public static void UpdateLichDonvi()
        {

        }

        #region Đồng bộ danh bạ
        public static void DongBoDanhBa()
        {
            DanhBaItem oDanhBaItem = new DanhBaItem();
            DanhBaDA oDanhBaDA = new DanhBaDA();

            ///Xóa hết danh bạ trước khi đồng bộ lại
            Console.WriteLine("Xoa du lieu cu truoc khi dong bo......");
            DeleteAll_DanhBa(oDanhBaDA);

            ///Đồng bộ dữ liệu mới
            Console.WriteLine("Dong bo du lieu moi.....");
            //string APIEVNHanoiNew = $"https://portal.evnhanoi.vn/apigateway/HRMS_DanhBa_NhanVien/0/0";
            string APIEVNHanoiNew = $"http://10.9.125.85:8080/HRMS_DanhBa_NhanVien/0/0";
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes("EVNHANOI:Evnhanoi@123");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var root = JsonConvert.DeserializeObject<DanhBaRespon>(responseBody);
            foreach (var item in root.data)
            {
                int IDDonVi = oDanhBaDA.CheckExit(0, item.donvi_id.ToString(), "oldID");
                if (IDDonVi > 0)
                {
                    //Check phong ban thuoc don vi da ton tai hay chua
                    List<DanhBaJson> lstPhongBanDonVi = GetPhongBan(IDDonVi, item.tenphongban);
                    if (lstPhongBanDonVi.Count > 0)
                    {                        
                        oDanhBaItem = new DanhBaItem();
                        oDanhBaItem.Title = item.tenkhaisinh;
                        oDanhBaItem.DBDiaChi = item.diachi;
                        oDanhBaItem.DBSDT = item.dienthoai;
                        oDanhBaItem.DBEmail = item.email;
                        oDanhBaItem.DBDonVi = new SPFieldLookupValue(IDDonVi, item.tendonvi);
                        oDanhBaItem.DBParentPB = new SPFieldLookupValue(lstPhongBanDonVi[0].ID, lstPhongBanDonVi[0].Title);
                        oDanhBaItem.IsDonVi = false;
                        oDanhBaItem.DBNgaySinh = item.ngaysinh;
                        oDanhBaDA.UpdateObject<DanhBaItem>(oDanhBaItem);
                        oDanhBaDA.UpdateSPModerationStatus(oDanhBaItem.ID,SPModerationStatusType.Approved);
                    }
                    else
                    {
                        oDanhBaItem = new DanhBaItem();
                        oDanhBaItem.Title = item.tenphongban;
                        oDanhBaItem.DBParentPB = new SPFieldLookupValue(IDDonVi, "");
                        oDanhBaItem.isOffice = "1";
                        oDanhBaItem.IsDonVi = true;
                        oDanhBaDA.UpdateObject<DanhBaItem>(oDanhBaItem);
                        oDanhBaDA.UpdateSPModerationStatus(oDanhBaItem.ID, SPModerationStatusType.Approved);

                        /// lấy lại ID phòng ban để lưu cho người hiện tại
                        List<DanhBaJson> lstPhongBan = GetPhongBan(IDDonVi, item.tenphongban);

                        /// Lưu người hiện tại đang dùng ID đơn vị và Phòng ban để check phía trên (khởi tạo lại và lưu)
                        oDanhBaItem = new DanhBaItem();
                        oDanhBaItem.Title = item.tenkhaisinh;
                        oDanhBaItem.DBDiaChi = item.diachi;
                        oDanhBaItem.DBSDT = item.dienthoai;
                        oDanhBaItem.DBEmail = item.email;
                        oDanhBaItem.DBDonVi = new SPFieldLookupValue(IDDonVi, item.tendonvi);
                        oDanhBaItem.DBParentPB = new SPFieldLookupValue(lstPhongBan[0].ID, lstPhongBan[0].Title);
                        oDanhBaItem.IsDonVi = false;
                        oDanhBaItem.DBNgaySinh = item.ngaysinh;
                        oDanhBaDA.UpdateObject<DanhBaItem>(oDanhBaItem);
                        oDanhBaDA.UpdateSPModerationStatus(oDanhBaItem.ID, SPModerationStatusType.Approved);
                    }

                }
                else
                {
                    ///Lưu đơn vị
                    oDanhBaItem = new DanhBaItem();
                    oDanhBaItem.Title = item.tendonvi;
                    oDanhBaItem.oldID = item.donvi_id;
                    oDanhBaItem.isOffice = "1";
                    oDanhBaItem.IsDonVi = true;
                    oDanhBaDA.UpdateObject<DanhBaItem>(oDanhBaItem);
                    oDanhBaDA.UpdateSPModerationStatus(oDanhBaItem.ID, SPModerationStatusType.Approved);

                    ///sau khi cập nhật đơn vị xong dùng hàm checkexit lay ID don vi vua luu
                    int IDDonVi_update = oDanhBaDA.CheckExit(0, item.donvi_id.ToString(), "oldID");

                    ///check phong ban va luu phong ban theo don vi
                    List<DanhBaJson> lstDanhBa = GetPhongBan(IDDonVi_update, item.tenphongban);
                    if (lstDanhBa.Count <= 0)
                    {
                        oDanhBaItem = new DanhBaItem();
                        oDanhBaItem.Title = item.tenphongban;
                        oDanhBaItem.DBDonVi = new SPFieldLookupValue(IDDonVi_update, "");
                        oDanhBaItem.DBParentPB = new SPFieldLookupValue(IDDonVi_update, "");
                        //oDanhBaItem.DBPhongBan = new SPFieldLookupValue(IDDonVi_update, "");
                        oDanhBaItem.isOffice = "1";
                        oDanhBaItem.IsDonVi = true;
                        oDanhBaDA.UpdateObject<DanhBaItem>(oDanhBaItem);
                        oDanhBaDA.UpdateSPModerationStatus(oDanhBaItem.ID, SPModerationStatusType.Approved);
                        /// lấy lại ID phòng ban để lưu cho người hiện tại
                        List<DanhBaJson> lstPhongBan = GetPhongBan(IDDonVi_update, item.tenphongban);

                        /// Lưu người hiện tại đang dùng ID đơn vị và Phòng ban để check phía trên (khởi tạo lại và lưu)
                        oDanhBaItem = new DanhBaItem();
                        oDanhBaItem.Title = item.tenkhaisinh;
                        oDanhBaItem.DBDiaChi = item.diachi;
                        oDanhBaItem.DBSDT = item.dienthoai;
                        oDanhBaItem.DBEmail = item.email;
                        oDanhBaItem.DBDonVi = new SPFieldLookupValue(IDDonVi_update, "");
                        oDanhBaItem.DBParentPB = new SPFieldLookupValue(lstPhongBan[0].ID, lstPhongBan[0].Title);
                        oDanhBaItem.IsDonVi = false;
                        oDanhBaItem.DBNgaySinh = item.ngaysinh;
                        oDanhBaDA.UpdateObject<DanhBaItem>(oDanhBaItem);
                        oDanhBaDA.UpdateSPModerationStatus(oDanhBaItem.ID, SPModerationStatusType.Approved);
                    }
                    else
                    {
                        /// Lưu người hiện tại đang dùng ID đơn vị và Phòng ban để check phía trên (khởi tạo lại và lưu)
                        oDanhBaItem = new DanhBaItem();
                        oDanhBaItem.Title = item.tenkhaisinh;
                        oDanhBaItem.DBDiaChi = item.diachi;
                        oDanhBaItem.DBSDT = item.dienthoai;
                        oDanhBaItem.DBEmail = item.email;
                        oDanhBaItem.DBDonVi = new SPFieldLookupValue(IDDonVi_update, "");
                        oDanhBaItem.DBParentPB = new SPFieldLookupValue(lstDanhBa[0].ID, lstDanhBa[0].Title);
                        oDanhBaItem.IsDonVi = false;
                        oDanhBaItem.DBNgaySinh = item.ngaysinh;
                        oDanhBaDA.UpdateObject<DanhBaItem>(oDanhBaItem);
                        oDanhBaDA.UpdateSPModerationStatus(oDanhBaItem.ID, SPModerationStatusType.Approved);
                    }

                }
            }
        }
        public static List<DanhBaJson> CheckDonVi(int IDDonVi)
        {
            DanhBaDA oDanhBaDA = new DanhBaDA();
            List<DanhBaJson> lstDanhBa = oDanhBaDA.GetListJson(new DanhBaQuery() { oldID = IDDonVi });

            return lstDanhBa;
        }
        public static List<DanhBaJson> GetPhongBan(int IDDonVi, string TenPhongBan)
        {
            DanhBaDA oDanhBaDA = new DanhBaDA();
            List<DanhBaJson> lstDanhBa = oDanhBaDA.GetListJson(new DanhBaQuery() { IDParent = IDDonVi, Title = TenPhongBan });

            return lstDanhBa;
        }
        public static void DeleteAll_DanhBa(DanhBaDA oDanhBaDA)
        {
            using (SPSite mySite = new SPSite("http://localhost:6003/noidung"))
            {
                using (SPWeb myWeb = mySite.OpenWeb())
                {
                    SPList myList = myWeb.Lists["DanhBa"];

                    SPListItemCollection myItems = myList.GetItems();

                    //1st way
                    foreach (SPListItem item in myItems)
                    {
                        oDanhBaDA.DeleteObject(Convert.ToInt32(item["ID"]));
                    }
                }
            }
        }
        #endregion
        #region Lưu Sinh nhật Lãnh đạo
        public static void SaveSinhNhatLanhDao()
        {
            Console.WriteLine("Save birthday");
            BirthdayDA oBirthdayDA = new BirthdayDA();
            string APIEVNHanoiNew = $"https://portal.evnhanoi.vn/apigateway/HRMS_ChucMungSinhNhat_LanhDao";
            ///IP server nội bộ
            //string APIEVNHanoiNew = $"http://10.9.125.85:8080/HRMS_ChucMungSinhNhat_LanhDao";

            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes("EVNHANOI:Evnhanoi@123");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var root = JsonConvert.DeserializeObject<Birthday>(responseBody);
            foreach (var item in root.data)
            {
                oBirthdayDA.UpdateObject<BirthdayItem>(item);
            }
        }
        public class Birthday
        {
            public string msg { get; set; }
            public bool suc { get; set; }
            public List<BirthdayItem> data { get; set; }
        }
        public class User_Birthday
        {
            public string ID_LLNS { get; set; }
            public string TENKHAISINH { get; set; }
            public DateTime NGAYSINH { get; set; }
            public int ID_DONVI { get; set; }
            public string TEN_CVU { get; set; }
            public string TEN_PHONGB { get; set; }
            public string TEN_DONVI { get; set; }
            public DateTime BIRTHDAY { get; set; }
        }
        #endregion
        #region Đồng bộ lịch
        public static void DongBoLich()
        {

            LichTongCongTyDA lichTongCongTyDA = new LichTongCongTyDA();
            HttpClient client = new HttpClient();
            //var byteArray = Encoding.ASCII.GetBytes("username:password1234");
            client.DefaultRequestHeaders.Add("Authorization", "Basic RVZOSEFOT0k6RVZOSEFOT0lAQ3VvbmdYMTY=");
            //get lichj theo tuan hieen tai.
            firstDay = LayNgayDauVaKhoiTaoGiaTriTuan_Nam();
            //ip nội bộ
            HttpResponseMessage response = client.GetAsync("http://10.9.125.13:8074/api/Portal/get_LichTuan?TuNgay=" + firstDay.ToString("dd/MM/yyyy") + "&DenNgay=" + firstDay.AddDays(7).ToString("dd/MM/yyyy") + "&Loai_Lich=1").Result;
            //ip public
            //HttpResponseMessage response = client.GetAsync("http://42.112.213.225:8074/api/Portal/get_LichTuan?TuNgay=" + firstDay.ToString("dd/MM/yyyy") + "&DenNgay=" + firstDay.AddDays(7).ToString("dd/MM/yyyy") + "&Loai_Lich=1").Result;
            string content = response.Content.ReadAsStringAsync().Result;
            Console.WriteLine("Đang tiến hành đồng bộ lịch làm việc...");
            List<LichTichHop> lichTichHops = Newtonsoft.Json.JsonConvert.DeserializeObject<List<LichTichHop>>(content);

            foreach (LichTichHop lichTichHop in lichTichHops)
            {

                try
                {
                    int IDItem = lichTongCongTyDA.CheckExit(0, lichTichHop.ID_Cuoc_Hop, "OldID");
                    string[] objTG = lichTichHop.THOI_GIAN.Split('-');
                    DateTime NGAY_THANG;
                    if (DateTime.TryParseExact(lichTichHop.NGAY_THANG,
                                                "d/M/yyyy",
                                                CultureInfo.InvariantCulture,
                                                DateTimeStyles.None,
                        out NGAY_THANG))
                    {
                        LichTongCongTyItem lichTongCongTyItem = new LichTongCongTyItem();
                        if (IDItem > 0)
                        {
                            lichTongCongTyItem = lichTongCongTyDA.GetByIdToObject<LichTongCongTyItem>(IDItem);
                        }
                        //valid date
                        //lichTongCongTyItem = new LichTongCongTyItem()
                        //{
                        //    ID = IDItem,
                        //    Title = lichTichHop.NOI_DUNG.GetTitle255(),
                        //    LichNoiDung = lichTichHop.NOI_DUNG,
                        //    LichDiaDiem = lichTichHop.DIA_DIEM,
                        //    CHUAN_BI = lichTichHop.CHUAN_BI,
                        //    THANH_PHAN = lichTichHop.THANH_PHAN,
                        //    CHU_TRI = lichTichHop.CHU_TRI,
                        //    OldID = lichTichHop.ID
                        //};
                        lichTongCongTyItem.ID = IDItem;
                        lichTongCongTyItem.Title = lichTichHop.NOI_DUNG.GetTitle255();
                        lichTongCongTyItem.LichNoiDung = lichTichHop.NOI_DUNG;
                        lichTongCongTyItem.LichDiaDiem = lichTichHop.DIA_DIEM;
                        lichTongCongTyItem.THANH_PHAN = lichTichHop.THANH_PHAN;
                        lichTongCongTyItem.CHU_TRI = lichTichHop.CHU_TRI;
                        lichTongCongTyItem.LogText = lichTongCongTyItem.LogText.Replace("_TT-", "_DEL-");
                        lichTongCongTyItem.LogText += $"_TT-{lichTichHop.TT}_";
                        lichTongCongTyItem.OldID = lichTichHop.ID_Cuoc_Hop;
                        lichTongCongTyItem.ID = IDItem;
                        if (objTG.Length == 2)
                        {
                            string[] TimeBatdat = objTG[0].Split('h');

                            lichTongCongTyItem.LichThoiGianBatDau = new DateTime(NGAY_THANG.Year, NGAY_THANG.Month, NGAY_THANG.Day, Convert.ToInt32(TimeBatdat[0]), Convert.ToInt32(TimeBatdat[1]), 0);

                            string[] TimeKetThuc = objTG[1].Split('h');

                            lichTongCongTyItem.LichThoiGianKetThuc = new DateTime(NGAY_THANG.Year, NGAY_THANG.Month, NGAY_THANG.Day, Convert.ToInt32(TimeKetThuc[0]), Convert.ToInt32(TimeKetThuc[1]), 0);

                            lichTongCongTyDA.UpdateObject<LichTongCongTyItem>(lichTongCongTyItem);

                            lichTongCongTyDA.UpdateSPModerationStatus(lichTongCongTyItem.ID, SPModerationStatusType.Approved);
                        }

                    }
                    else
                    {
                        //invalid date
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message + "." + ex.StackTrace);
                }

            }
            //OldID

        }
        #endregion
        public static void SendSMS()
        {
            oLNotificationDA = new LNotificationDA(true);
            lstLNotification = oLNotificationDA.GetListJson(new LNotificationQuery() { isSendMail = true, TuNgay = DateTime.Now.AddHours(-1) });
        }

        static void GetNotifiAndSendMail()
        {
            oLNotificationDA = new LNotificationDA(true);
            
            oLUserDA = new LUserDA();
            //oMailPendingDA = new MailPendingDA();
            oLconfigDA = new LconfigDA();
            string Hostmail = oLconfigDA.GetValueConfigByType("Hostmail");
            string MailFrom = oLconfigDA.GetValueConfigByType("AccountMail");
            string PassEmail = oLconfigDA.GetValueConfigByType("PassEmail");
            string ContentWF = oLconfigDA.GetValueMultilConfigByType("ContentWF");
            string Port = oLconfigDA.GetValueConfigByType("Port");

            lstLNotification = oLNotificationDA.GetListJson(new LNotificationQuery() { isSendMail = true, TuNgay = DateTime.Now.AddHours(-1) });
            foreach (var item in lstLNotification)
            {
                LUserItem oLUser = new LUserItem();
                oMailPendingItem = new MailPendingItem();
                //get mail
                List<int> lstIDUser = clsFucUtils.GetDanhSachIDsQuaFormPost(item.LNotiNguoiNhan_Id);
                foreach (int itemid in lstIDUser)
                {
                    oLUser = oLUserDA.GetByIdToObject<LUserItem>(itemid);
                    mailto = new List<string>();
                    mailto.Add(oLUser.UserEmail);
                    string subject = "[Email] Thông báo về việc " + item.Title;
                    ContentWF = ContentWF.Replace("{NguoiNhan}", oLUser.Title);
                    ContentWF = ContentWF.Replace("{Title}", item.Title);
                    ContentWF = ContentWF.Replace("{TieuDeBaiViet}", item.TitleBaiViet);
                    ContentWF = ContentWF.Replace("{NgayGiao}", DateTime.Now.ToString("dd/MM/yyyy"));
                    SendMail(subject, ContentWF, MailFrom, PassEmail, mailto, Hostmail, Port);
                    //update trạng thái gửi mail trên LNotificationItem
                    oLNotificationDA.UpdateOneField(item.ID, "LNotiIsSentMail", 1);
                    //tạo bản ghi trên MailPending
                    oMailPendingItem.Title = subject;
                    oMailPendingItem.EmailNoiDung = ContentWF;
                    oMailPendingItem.NguoiGui = item.LNotiNguoiGui;
                    oMailPendingItem.NguoiNhan = oLUser.Title;
                    oMailPendingItem.EmailNhan = oLUser.UserEmail;
                    oMailPendingItem.EmailThoiGianGui = DateTime.Now;
                    oMailPendingItem.EmailDaGui = true;
                    oMailPendingDA.UpdateObject<MailPendingItem>(oMailPendingItem);
                    //item.LNotiData
                    if (!string.IsNullOrEmpty(oLUser.TokenFireBase) && !string.IsNullOrEmpty(item.LNotiData))
                    {
                        {
                            NhatKyXuLyItem oNhatKyXuLyItem = new NhatKyXuLyItem();
                            oNhatKyXuLyItem = Newtonsoft.Json.JsonConvert.DeserializeObject<NhatKyXuLyItem>(item.LNotiData);
                            clsFucUtils.SendNotiFi("EVN Portal Thông báo mới", "Thông báo về việc " + item.Title, oLUser.TokenFireBase, new
                            {
                                LogIDDoiTuong = oNhatKyXuLyItem.LogIDDoiTuong,
                                DoiTuongTitle = oNhatKyXuLyItem.DoiTuongTitle,
                                LogDoiTuong = oNhatKyXuLyItem.LogDoiTuong,
                            });
                        }
                    }
                    //AAAACLhtkSU:APA91bGjzGuk1E9YIe4v_jVCztr8QQ_mIccnFDbBiGg6ObdMQGs-PkwNJk07g-cSW3zKlh8abfy92MkFco8kyeKf5Aa14CQbxw4EfZfXmQlI-xecSQpE7fk_-In8T6ZxpRecgBXKusmo
                }

            }

        }
        static void SendMail(string subject, string ContentWF, string MailFrom, string PassEmail, List<string> mailto, string Hostmail, string Port)
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient(Hostmail);
            mail.From = new MailAddress(MailFrom);
            foreach (string item in mailto)
                mail.To.Add(item);
            mail.Subject = subject;
            mail.Body = ContentWF;
            mail.IsBodyHtml = true;
            SmtpServer.Port = Convert.ToInt32(Port);
            SmtpServer.Credentials = new System.Net.NetworkCredential(MailFrom, PassEmail);
            //SmtpServer.EnableSsl = true;
            SmtpServer.Send(mail);
        }
        /// <summary>
        /// Lay ra gia tri int cua thu - ngay dau nam
        /// </summary>
        /// <param name="nam">Nam</param>
        /// <returns>0: Thu 2; 1: thu 3; ..... 5: thu bay,6: Chu Nhat</returns>
        public static int ThuCuaNgayDauNam(int nam)
        {
            // khoi tao ngay dau tien cua nam
            DateTime startYear = new DateTime(nam, 1, 1);
            int startdayofyear = 0;

            if (startYear.DayOfWeek == DayOfWeek.Monday) startdayofyear = 0; // thu 2
            else if (startYear.DayOfWeek == DayOfWeek.Tuesday) startdayofyear = 1; // thu 3
            else if (startYear.DayOfWeek == DayOfWeek.Wednesday) startdayofyear = 2;
            else if (startYear.DayOfWeek == DayOfWeek.Thursday) startdayofyear = 3;
            else if (startYear.DayOfWeek == DayOfWeek.Friday) startdayofyear = 4;
            else if (startYear.DayOfWeek == DayOfWeek.Saturday) startdayofyear = 5;
            else startdayofyear = 6;
            return startdayofyear;
        }
        /// <summary>
        /// Lay so tuan trong 1 nam
        /// </summary>
        /// <param name="nam">Nam can lay</param>
        /// <returns>So tuan, tinh tu tuan 1</returns>
        public static int LaySoTuanTrongNam(int nam)
        {
            // kiem tra ngay dau tien cua nam vao thu nao
            int startdayofyear = ThuCuaNgayDauNam(nam);

            DateTime endYear = new DateTime(nam, 12, 31);
            // khai bao ngay dau tuan dau tien cua nam
            int SoNgayTrongNam = endYear.DayOfYear;

            // Lay so tuan trong 1 nam
            int SoTuanTrongNam = 0;
            SoTuanTrongNam = (SoNgayTrongNam + startdayofyear) / 7;
            int SoDu = (SoNgayTrongNam + startdayofyear) % 7;
            if (SoDu > 0) SoTuanTrongNam = SoTuanTrongNam + 1;

            return SoTuanTrongNam;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="CurWeek"></param>
        /// <param name="CurYear"></param>
        /// <returns></returns>
        public static DateTime NgayDauTuan(int CurWeek, int CurYear)
        {
            //Khai bao, khoi tao gia tri ngay dau tuan
            DateTime firstDate = new DateTime(CurYear, 1, 1);
            // khoi tao ngay dau tien cua nam
            DateTime startYear = new DateTime(CurYear, 1, 1);
            // khoi tao ngay cuoi cung cua nam
            DateTime endYear = new DateTime(CurYear, 12, 31);
            // khai bao ngay dau tuan dau tien cua nam
            DateTime NgayDauTuanThu1 = new DateTime(CurYear, 1, 1);
            // So ngay trong nam
            int SoNgayTrongNam = endYear.DayOfYear;

            // kiem tra ngay dau tien cua nam vao thu nao
            int startdayofyear = ThuCuaNgayDauNam(CurYear);


            // Lay so tuan trong 1 nam
            int SoTuanTrongNam = LaySoTuanTrongNam(CurYear);

            // kiem tra so tuan truyen vao co hop le hay khong
            // neu khong hop le se lay tuan hien thoi
            if (CurWeek < 1 || CurWeek > SoTuanTrongNam)
            {
                if (CurYear != DateTime.Today.Year) CurWeek = 1;
                else
                {
                    // lay tuan hien thoi
                    DateTime today = System.DateTime.Today;

                    int NgayHienthoi = today.DayOfYear + startdayofyear;
                    CurWeek = NgayHienthoi / 7;
                    if (NgayHienthoi % 7 > 0) CurWeek = CurWeek + 1;
                }
            }

            // lay ngay dau tien cua tuan dau tien
            NgayDauTuanThu1 = startYear.AddDays(startdayofyear * -1);

            int NgayDauTuan = (CurWeek - 1) * 7;
            // Lay ngay dau tien cua tuan can lay
            firstDate = NgayDauTuanThu1.AddDays(NgayDauTuan);

            // tra ve ngay dau tien cua tuan can lay
            return firstDate;
        }
        /// <summary>
        /// LayNgayDauVaKhoiTaoGiaTriTuan_Nam
        /// </summary>

        public static DateTime LayNgayDauVaKhoiTaoGiaTriTuan_Nam()
        {
            // lay nam
            try
            {
                if (HttpContext.Current.Request["p_year"] != null) strYear = HttpContext.Current.Request["p_year"].ToString().Trim();
                intCurYear = int.Parse(strYear);
                if (intCurYear > 9999 || intCurYear < 1900)
                {
                    intCurYear = System.DateTime.Today.Year;
                    strYear = intCurYear.ToString();
                }
            }
            catch
            {
                intCurYear = System.DateTime.Today.Year;
                strYear = intCurYear.ToString();
            }

            int SoTuanTrongNam = LaySoTuanTrongNam(intCurYear);

            // lay tuan
            try
            {
                if (!string.IsNullOrEmpty(HttpContext.Current.Request["p_week"]))
                    strWeek = HttpContext.Current.Request["p_week"].ToString().Trim();
                //if (!string.IsNullOrEmpty(next) && next == "next")
                //    intCurWeek = Convert.ToInt32(strWeek) + 1;
                //else if (!string.IsNullOrEmpty(last) && last == "last")
                //    intCurWeek = Convert.ToInt32(strWeek) - 1;
                //else
                intCurWeek = Convert.ToInt32(strWeek);
                //intCurWeek = int.Parse(strWeek);

                int startdayofyear = ThuCuaNgayDauNam(intCurYear);
                if (intCurWeek < 1 || intCurWeek > SoTuanTrongNam)
                {
                    if (intCurYear != DateTime.Today.Year) intCurWeek = 1;
                    else
                    {
                        // lay tuan hien thoi
                        DateTime today = System.DateTime.Today;

                        int NgayHienthoi = today.DayOfYear + startdayofyear;
                        intCurWeek = NgayHienthoi / 7;
                        if (NgayHienthoi % 7 > 0) intCurWeek = intCurWeek + 1;
                    }
                }
                strWeek = intCurWeek.ToString();

            }
            catch
            {

                int startdayofyear = ThuCuaNgayDauNam(intCurYear);

                if (intCurYear != DateTime.Today.Year) intCurWeek = 1;
                else
                {
                    // lay tuan hien thoi
                    DateTime today = System.DateTime.Today;

                    int NgayHienthoi = today.DayOfYear + startdayofyear;
                    intCurWeek = NgayHienthoi / 7;
                    if (NgayHienthoi % 7 > 0) intCurWeek = intCurWeek + 1;
                }
                strWeek = intCurWeek.ToString();
            }

            // lay gia tri cua ngay dau tuan

            firstDay = NgayDauTuan(intCurWeek, intCurYear);

            //lblNgayChon.Text = "Từ ngày: " + string.Format("{0:dd/MM/yyyy}", firstDay) + " - Đến ngày: " + string.Format("{0:dd/MM/yyyy}", firstDay.AddDays(6));

            if (intCurWeek == SoTuanTrongNam)
            {
                strnextweek = "1";
                int nam = intCurYear + 1;
                strnextyear = nam.ToString();

                int tuan = intCurWeek - 1;
                strlastweek = tuan.ToString();
                strlastyear = strYear;

            }
            else if (intCurWeek == 1)
            {


                int tuan = intCurWeek + 1;
                strnextweek = tuan.ToString();
                strnextyear = strYear;


                int nam = intCurYear - 1;
                strlastyear = nam.ToString();
                tuan = LaySoTuanTrongNam(nam);
                strlastweek = tuan.ToString();

            }
            else
            {

                strnextyear = strYear;
                strlastyear = strYear;


                int tuan = intCurWeek + 1;
                strnextweek = tuan.ToString();

                tuan = intCurWeek - 1;
                strlastweek = tuan.ToString();
            }
            return firstDay;
        }

    }
}
