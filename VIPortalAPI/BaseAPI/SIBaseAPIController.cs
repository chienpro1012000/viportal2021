﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Description;
using ViPortal_Utils;
using VIPortalAPI.Filters;
using VIPortalAPI.Models;
using ViPortalData;
using ViPortalData.LVaiTro;

namespace VIPortalAPI.BaseAPI
{
    /// <summary>
    /// Base API
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    public class SIBaseAPI : ApiController
    {
        /// <summary>
        /// Tham số check chạy query
        /// </summary>
        public string doAction { get; set; }
        /// <summary>
        /// cur UrlSite
        /// </summary>
        public string UrlSite;
        /// <summary>
        /// cur UrlSite
        /// </summary>
        public string UrlList;
        /// <summary>
        /// Thoong tin user
        /// </summary>
        public LUserJson CurentUser { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CurFldGroup { get; private set; }

        /// <summary>
        /// ham khoi tao
        /// </summary>
        /// <param name="controllerContext"></param>
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            UrlSite = GetCurentSite();

            CurentUser = FuncLogin();
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["do"]))
                doAction = HttpContext.Current.Request["do"].ToUpper();
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["UrlList"]))
                UrlList = HttpContext.Current.Request["UrlList"];
        }
        /// <summary>
        /// Ham get curentsie
        /// </summary>
        /// <returns></returns>
        public  string GetCurentSite()
        {
            try
            {
                string siteTemp = string.Empty;
                if (HttpContext.Current.Request.UrlReferrer != null)
                    siteTemp = HttpContext.Current.Request.UrlReferrer.ToString();
                else
                    siteTemp = HttpContext.Current.Request.Url.AbsoluteUri;
                string site = siteTemp.Split('/')[3];
                if (site.Equals("noidung") || site.Equals("cms") || site.Equals("Pages"))
                    return "";
                else
                    return "/" + site;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// lay thong tin user.
        /// </summary>
        /// <returns></returns>
        public LUserJson FuncLogin()
        {
            string userName = HttpContext.Current.User.Identity.Name;
            //0#.w|udtt\hoannt
            if (!string.IsNullOrEmpty(userName))
            {
                if (userName.Contains("|"))
                {
                    userName = userName.Split('|').LastOrDefault();
                }
                //0#.w|simax\maianh
                if (userName.Contains("\\"))
                {
                    userName = userName.Split('\\')[1];
                }
            }
            var Session = HttpContext.Current.Session;

            LUserJson oLUserJson = new LUserJson();
            if (Session["ObjectUser"] != null)
            {
                oLUserJson = (LUserJson)Session["ObjectUser"];
                if (oLUserJson.TaiKhoanTruyCap != userName || oLUserJson.CurSiteAction != UrlSite)
                {
                    Session["ObjectUser"] = null;
                    return FuncLogin();
                }
            }
            else
            {
                #region Lấy dữ liệu json
                LUserDA oLUserDA = new LUserDA();

                var tempuser = oLUserDA.GetListJson(new LUserQuery()
                {
                    TaiKhoanTruyCap = userName
                });
                if (tempuser.Count > 0)
                {
                    int buoc = 1;
                    string MessageLog = "";
                    oLUserJson = tempuser[0];
                    try
                    {
                       
                        oLUserJson.CurSiteAction = UrlSite; //site đang thao tác nghiệp vụ
                        MessageLog += (oLUserJson.CurSiteAction + "\r\n");
                        LGroupDA lGroupDA = new LGroupDA();
                        var temp = lGroupDA.GetListJson(new LGroupQuery() { UrlSite = UrlSite, GroupIsDonVi = 1 }).FirstOrDefault();
                        if (temp != null)
                            CurFldGroup = temp.ID;
                        MessageLog += (CurFldGroup + "\r\n");
                        buoc  = 2;
                        //chỗ này check lại chỗ này.
                        if (CurFldGroup > 0)
                        {
                            buoc = 3;
                            if (!string.IsNullOrEmpty(oLUserJson.PerJson))
                            {
                                if (temp != null)
                                    CurFldGroup = temp.ID;
                                MessageLog += (oLUserJson.PerJson + "\r\n");
                                buoc = 4;
                                List<PermisonGroup> permisonGroups = new List<PermisonGroup>();
                                permisonGroups = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PermisonGroup>>(oLUserJson.PerJson);

                                PermisonGroup oPermisonGroup = permisonGroups.FirstOrDefault(x => x.fldGroup == CurFldGroup);
                                if (oPermisonGroup == null) oPermisonGroup = new PermisonGroup();
                                oLUserJson.Roles = oPermisonGroup.Roles == null ? new List<ViPortal_Utils.Base.LookupData>() : oPermisonGroup.Roles;
                                oLUserJson.Permissions = oPermisonGroup.Permissions == null ? new List<ViPortal_Utils.Base.LookupData>() : oPermisonGroup.Permissions;
                                MessageLog += (Newtonsoft.Json.JsonConvert.SerializeObject(oPermisonGroup) + "\r\n");

                            }
                        }

                        if (oLUserJson.Roles != null && oLUserJson.Roles.Count > 0)
                        {
                            LVaiTroDA oLVaiTroDA = new LVaiTroDA();
                            List<LVaiTroJson> lstVaiTro = oLVaiTroDA.GetListJson(new LVaiTroQuery() { lstIDget = oLUserJson.Roles.Select(x => x.ID).ToList(), isGetBylistID = true });
                            foreach (var item in lstVaiTro)
                            {
                                oLUserJson.Permissions.AddRange(item.Permission);
                            }
                        }
                        oLUserJson.Permissions = oLUserJson.Permissions.DistinctBy(x => x.ID).ToList();
                        LPermissionDA lPermissionDA = new LPermissionDA();
                        List<LPermissionJson> lstPermission = lPermissionDA.GetListJson(new LPermissionQuery() { lstIDget = oLUserJson.Permissions.Select(x => x.ID).ToList(), isGetBylistID = true });
                        oLUserJson.PermissionsQuery = "|" + string.Join("|", lstPermission.Select(x => x.MaQuyen)) + "|";
                        //LGroupDA lGroupDA = new LGroupDA();
                        LGroupItem itemGroup = lGroupDA.GetByIdToObject<LGroupItem>(oLUserJson.Groups.ID);
                        oLUserJson.UrlSite = itemGroup.UrlSite;
                        Session["ObjectUser"] = oLUserJson;
                    }catch (Exception ex)
                    {
                        throw new Exception($"Erro Message: {MessageLog}");
                    }
                }
                #endregion
            }
            return oLUserJson;
        }
    }

    //[JwtAuthentication]
    /// <summary>
    /// 
    /// </summary>
    public class SIBaseAPIWeb : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        public string UserName = "";
        /// <summary>
        /// /
        /// </summary>
        /// <param name="controllerContext"></param>
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            var request = controllerContext.Request;
            var authorization = request.Headers.GetValues("Authorization1")?.FirstOrDefault(); //Authorization;
            UserName = JwtManager.ValidateToken(authorization);
            if (string.IsNullOrEmpty(UserName))
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }


        }
    }
}
