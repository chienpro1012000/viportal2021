using SolrNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using ViPortal_Utils;
using ViPortalData.Models;

namespace VIPortalAPI
{
    /// <summary>
    /// global application
    /// </summary>
    public class WebApiApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// appstart
        /// </summary>
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            StartupSolr.Init<ViPortal_Utils.DataSolr>("http://solrserver2:9000/solr/Portal2021");
            StartupSolr.Init<ViPortal_Utils.DanhMucSolr>("http://solrserver2:9000/solr/Portal2021DanhMuc");
            StartupSolr.Init<ViPortal_Utils.LogHeThongSolr>("http://solrserver2:9000/solr/Portal2021Log");
        }
        /// <summary>
        /// 
        /// </summary>
        protected void Application_PostAuthorizeRequest()
        {
            System.Web.HttpContext.Current.SetSessionStateBehavior(System.Web.SessionState.SessionStateBehavior.Required);
        }
    }
}
