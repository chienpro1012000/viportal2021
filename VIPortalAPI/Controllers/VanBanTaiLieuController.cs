﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ViPortal_Utils.Base;
using VIPortalAPI.BaseAPI;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.LoaiTaiLieu;
using ViPortalData.VanBanTaiLieu;

namespace VIPortalAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    /// 
    public class VanBanTaiLieuController : SIBaseAPI
    {
        /// <summary>
        /// /
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA()
        {
            VanBanTaiLieuDA oVanBanTaiLieuDA = new VanBanTaiLieuDA(UrlSite);
            VanBanTaiLieuQuery oVanBanTaiLieuQuery = new VanBanTaiLieuQuery(HttpContext.Current.Request);
            oVanBanTaiLieuQuery._ModerationStatus = 1;
            List<VanBanTaiLieuJson> lstquyche = oVanBanTaiLieuDA.GetListJsonSolr(oVanBanTaiLieuQuery);
                   
            DataGridRender oGrid = new DataGridRender(lstquyche, oVanBanTaiLieuQuery.Draw,
                oVanBanTaiLieuDA.TongSoBanGhiSauKhiQuery);
            oGrid.Query = oVanBanTaiLieuDA.Queryreturn;
            return oGrid;
        }
        /// <summary>
        /// /
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA_TAILIEUCHIASE()
        {
            TaiLieuChiaSeDA oTaiLieuChiaSeDA = new TaiLieuChiaSeDA();
            TaiLieuChiaSeQuery oTaiLieuChiaSeQuery = new TaiLieuChiaSeQuery(HttpContext.Current.Request);
            oTaiLieuChiaSeQuery._ModerationStatus = 1;
            List<TaiLieuChiaSeJson> lstquyche = oTaiLieuChiaSeDA.GetListJson(oTaiLieuChiaSeQuery);

            DataGridRender oGrid = new DataGridRender(lstquyche, oTaiLieuChiaSeQuery.Draw,
                oTaiLieuChiaSeDA.TongSoBanGhiSauKhiQuery);
            oGrid.Query = oTaiLieuChiaSeDA.Queryreturn;
            return oGrid;
        }
        /// <summary>
        /// THÔNG BÁO
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA_THONGBAO()
        {
            ThongBaoDA oThongBaoDA = new ThongBaoDA(UrlSite);
            ThongBaoQuery oThongBaoQuery = new ThongBaoQuery(HttpContext.Current.Request);
            oThongBaoQuery._ModerationStatus = 1;
            var oData = oThongBaoDA.GetListJson(oThongBaoQuery);
            DataGridRender oGrid = new DataGridRender(oData, oThongBaoQuery.Draw,
                oThongBaoDA.TongSoBanGhiSauKhiQuery);
            return oGrid;
        }
        /// <summary>
        /// NOTIFICATION
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA_NOTIFICATION()
        {
            LNotificationDA oLNotificationDA = new LNotificationDA();
            LNotificationQuery oLNotificationQuery = new LNotificationQuery(HttpContext.Current.Request);
            oLNotificationQuery._ModerationStatus = 1;
            var oData = oLNotificationDA.GetListJson(oLNotificationQuery);
            DataGridRender oGrid = new DataGridRender(oData, oLNotificationQuery.Draw,oLNotificationDA.TongSoBanGhiSauKhiQuery);
            return oGrid;
        }
    }
}
