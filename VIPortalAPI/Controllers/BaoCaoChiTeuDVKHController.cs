﻿using Library_Shared.Utilities;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using ViPortal_Utils.Base;
using VIPortalAPI.BaseAPI;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.Lconfig;

namespace VIPortalAPI.Controllers
{
    public class BaoCaoChiTeuDVKHController : SIBaseAPI
    {
        // GET: BaoCaoChiTeuDVKH
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_BaoCaoChiTieuDVKH()
        {
            BaoCaoChiTeuDVKHQuery oBaoCaoChiTeuDVKHQuery = new BaoCaoChiTeuDVKHQuery(HttpContext.Current.Request);

            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/CMIS_BCTHCacChiTieuChatLuongPhucVuKH";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBaoCaoChiTeuDVKHQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            var bcNam = oBaoCaoChiTeuDVKHQuery.SearchNam;
            var bcThang = oBaoCaoChiTeuDVKHQuery.SearchThang;
            var bcDV = oBaoCaoChiTeuDVKHQuery.MaDonVi;

            string APIEVNHanoiNew = oConfigAPI.UrlAPI+ "/"+ bcDV + "/" + bcThang+"/"+bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapBaoCaoChiTeuDVKHJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(BaoCaoChiTeuDVKHJson),
                    new Dictionary<string, string>
                    {
                        {"STT", "STT_HT"},
                        {"ChiTieu", "CHI_TIEU"},
                        {"DonVi", "DON_VI"},
                        {"SoLuongTBC", "GIATRI_THANG"},
                        {"SoCungKiTBC", "SO_CUNGKY"},
                        {"TiTrongTBC", "TY_TRONG"},
                        {"TThoiGianTHienTBC", "TONGTG_THANG"},
                        {"ThoiGianTBTBC", "TGIAN_TB"},
                        {"SoCungKi2TBC", "SO_CUNGKY2"},
                        {"SoLuongLK", "GIA_TRI_LK"},
                        {"SoCungKiLK", "SO_CUNGKY_LK"},
                        {"TiTrongLK", "TY_TRONG_LK"},
                        {"TThoiGianTHienLK", "TONGTG_LK"},
                        {"ThoiGianBTLK", "TGIAN_TB_LK"},
                        {"SoCungKi2LK", "SO_CUNGKY_LK2"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };


            var oData = JsonConvert.DeserializeObject<List<BaoCaoChiTeuDVKHJson>>(string1resultUP, settings); ;
            string donvi;
            if (bcDV == "all")
            {
                donvi = "";
            }
            else if (bcDV == "PD")
            {
                donvi = "Tổng Công ty Điện lực TP Hà Nội";
            }
            else if (bcDV == "PD0600")
            {
                donvi = "Công ty Điện lưc Thanh Trì";
            }
            else if (bcDV == "PD1400")
            {
                donvi = "Công ty Điện lực Long Biên";
            }
            else if (bcDV == "P")
            {
                donvi = "Tập đoàn Điện Lực Việt Nam";
            }
            else if (bcDV == "PD0700")
            {
                donvi = "Công ty Điện lực Gia Lâm";
            }
            else if (bcDV == "PD0800")
            {
                donvi = "Công ty Điện lực Đông Anh";
            }
            else if (bcDV == "PD0900")
            {
                donvi = "Công ty Điện lực Sóc Sơn";
            }
            else if (bcDV == "PD1800")
            {
                donvi = "Công ty Điện lực Chương Mỹ";
            }
            else if (bcDV == "PD2000")
            {
                donvi = "Công ty Điện lực Thường Tín";
            }
            else if (bcDV == "PD2400")
            {
                donvi = "Công ty Điện lực Mỹ Đức";
            }
            else if (bcDV == "PD2600")
            {
                donvi = "Công ty Điện lực Phúc Thọ";
            }
            else if (bcDV == "PD2500")
            {
                donvi = "Công ty Điện lực Phú Xuyên";
            }
            else if (bcDV == "PD2800")
            {
                donvi = "Công ty Điện lực Thanh Oai";
            }
            else if (bcDV == "PD2900")
            {
                donvi = "Công ty Điện lực Ứng Hòa";
            }
            else if (bcDV == "PD0100")
            {
                donvi = "Công ty Điện lực Hoàn Kiếm";
            }
            else if (bcDV == "PD1500")
            {
                donvi = "Công ty Điện lực Mê Linh";
            }
            else if (bcDV == "PD1700")
            {
                donvi = "Công ty Điện lực Sơn Tây ";
            }
            else if (bcDV == "PD1900")
            {
                donvi = "Công ty Điện lực Thạch Thất";
            }
            else if (bcDV == "PD2100")
            {
                donvi = "Công ty Điện lực Ba Vì";
            }
            else if (bcDV == "PD2200")
            {
                donvi = "Công ty Điện lực Đan Phượng";
            }
            else if (bcDV == "PD2300")
            {
                donvi = "Công ty Điện lực Hoài Đức";
            }
            else if (bcDV == "PD2700")
            {
                donvi = "Công ty Điện lực Quốc Oai ";
            }
            else if (bcDV == "PD0400")
            {
                donvi = "Công ty Điện lực Đống Đa ";
            }
            else if (bcDV == "PD0500")
            {
                donvi = "Công ty Điện lực Nam Từ Liêm ";
            }
            else if (bcDV == "PD0200")
            {
                donvi = "Công ty Điện lực Hai Bà Trưng ";
            }
            else if (bcDV == "PD1000")
            {
                donvi = "Công ty Điện lực Tây Hồ  ";
            }
            else if (bcDV == "PD1100")
            {
                donvi = "Công ty Điện lực Thanh Xuân ";
            }
            else if (bcDV == "PD1200")
            {
                donvi = "Công ty Điện lực Cầy giấy ";
            }
            else if (bcDV == "PD3000")
            {
                donvi = "Công ty Điện lực Bắc Từ Liêm ";
            }
            else if (bcDV == "PD1300")
            {
                donvi = "Công ty Điện lực Hoàng Mai ";
            }
            else if (bcDV == "PD0300")
            {
                donvi = "Công ty Điện lực Ba Đình ";
            }
            else if (bcDV == "PD1600")
            {
                donvi = "Công ty Điện lực Hà Đông ";
            }
            else
            {
                donvi = "Tổng Công ty Điện lực TP Hà Nội ";
            };

            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tổng Công ty Điện lực TP Hà Nội",
                TieuDeTrai2 = donvi,
                Title = "BÁO CÁO CÁC CHỈ TIÊU CHẤT LƯỢNG DỊCH VỤ KHÁCH HÀNG",
                LoaiBM = "Biểu KDDN2",
               // NguoiKy = "VŨ THẾ THẮNG",
                Thang = bcThang.ToString(),
                Nam = bcNam.ToString(),
                NguoiLapBieu = CurentUser.Title,
                //ToTongHop = " NGUYỄN ĐỨC AN ",

                recordsTotal = oData.Count
            };

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoChiTeuDVKHQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoChiTeuDVKHQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng: ";
                    if (bcThang!=null)
                        Tungaydenngay += bcThang;
                    Tungaydenngay += " - Năm: ";
                    if (bcNam!=null)
                        Tungaydenngay += bcNam;
                    lstValues.Add(new ClsExcel(3, 6, Tungaydenngay, StyleExcell.TextDatetime, false, 12,true,3,3,6,8));
                    int rowStart = 7;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.ChiTieu, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.DonVi, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.SoLuongTBC, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.SoCungKiTBC, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.TiTrongTBC, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.TThoiGianTHienTBC, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.ThoiGianTBTBC, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.SoCungKi2TBC, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.SoLuongLK, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.SoCungKiLK, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.TiTrongLK, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 12, item.TThoiGianTHienLK, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 13, item.ThoiGianBTLK, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 14, item.SoCungKi2LK, StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }
                   
                    lstValues.Add(new ClsExcel(4, 13, "Có tổng số: " + oData.Count + " bản ghi", StyleExcell.TextRight, false, 12,true,4,4,13,14));
                    lstValues.Add(new ClsExcel(2, 0, donvi, StyleExcell.TextCenterBold, false, 12, false,3,3,13,16));


                    lstValues.Add(new ClsExcel(++rowStart+2, 1, "TỔ TỔNG HỢP ", StyleExcell.TextCenterBold, false, 12, true, rowStart+2, rowStart+2, 1, 3));
                    lstValues.Add(new ClsExcel(++rowStart+2, 1, "", StyleExcell.TextCenterBold, false, 12, true, rowStart+2, rowStart + 6, 1, 3));
                    lstValues.Add(new ClsExcel(rowStart + 7, 1, dataGiaoDienRender.ToTongHop, StyleExcell.TextCenterBold, false, 12, true, rowStart + 7, rowStart + 7, 1, 3));

                    lstValues.Add(new ClsExcel(++rowStart, 4, "NGƯỜI LẬP BIỂU ", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart, 4, 8));
                    lstValues.Add(new ClsExcel(++rowStart, 4, "", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart + 4, 4, 8));
                    lstValues.Add(new ClsExcel(rowStart + 5, 4,CurentUser.Title, StyleExcell.TextCenterBold, false, 12, true, rowStart +5, rowStart + 5, 4, 8));

                    string ngaythangnam = "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    lstValues.Add(new ClsExcel(++rowStart-4, 9, ngaythangnam, StyleExcell.TextDatetime, false, 12, true, rowStart-4, rowStart-4, 9, 14));
                    lstValues.Add(new ClsExcel(++rowStart-4, 9, "KT.TỔNG GIÁM ĐỐC", StyleExcell.TextCenterBold, false, 12, true, rowStart-4, rowStart-4, 9, 14));
                    lstValues.Add(new ClsExcel(++rowStart-4, 9, "PHÓ TỔNG GIÁM ĐỐC", StyleExcell.TextCenterBold, false, 12, true, rowStart-4, rowStart-4, 9, 14));
                    lstValues.Add(new ClsExcel(++rowStart-4, 9, "", StyleExcell.TextCenterBold, false, 12, true, rowStart-4, rowStart , 9, 14));
                    //lstValues.Add(new ClsExcel(rowStart + 1, 2, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 2, 1, 1));
                    //lstValues.Add(new ClsExcel(rowStart + 1, 9, dataGiaoDienRender.NguoiKy, StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 1, 9, 14));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "BaoCaoChiTieuDVKH_Thang_" +bcThang+"_Nam_"+bcNam));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
    }
}