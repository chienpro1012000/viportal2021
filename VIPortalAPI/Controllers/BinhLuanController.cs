﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using ViPortal_Utils.Base;
using VIPortalAPP;
using ViPortalData.BinhLuan;

namespace VIPortalAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    public class BinhLuanController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA()
        {
            BinhLuanDA oBinhLuanDA = new BinhLuanDA();
            BinhLuanQuery oBinhLuanQuery = new BinhLuanQuery(HttpContext.Current.Request);
            oBinhLuanQuery._ModerationStatus = 1;
            var oData = oBinhLuanDA.GetListJson(oBinhLuanQuery);
            DataGridRender oGrid = new DataGridRender(oData, oBinhLuanQuery.Draw,
                oBinhLuanDA.TongSoBanGhiSauKhiQuery);
            return oGrid;

        }
        /// <summary>
        /// tằng bình luận len
        /// </summary>
        /// <returns></returns>
        public ResultAction AddComment()
        {
            ResultAction oResultAction = new ResultAction();
            try
            {

                BinhLuanItem oBinhLuanItem = new BinhLuanItem();
                oBinhLuanItem.UpdateObject(HttpContext.Current.Request);
                BinhLuanDA oBinhLuanDA = new BinhLuanDA(true);
                oBinhLuanDA.UpdateObject<BinhLuanItem>(oBinhLuanItem);
                if(oBinhLuanItem.BaiViet.LookupId > 0)
                {
                    BaiVietDA oBaiVietDA = new BaiVietDA();
                    BaiVietItem oBaiVietItem= oBaiVietDA.GetByIdToObjectSelectFields<BaiVietItem>(oBinhLuanItem.BaiViet.LookupId, "");
                    oBaiVietDA.SystemUpdateOneField(oBinhLuanItem.BaiViet.LookupId, "SoLuotComment", oBaiVietItem.SoLuotComment + 1);
                }
                oResultAction = new ResultAction()
                {
                    State = ActionState.Succeed,
                    OData = oBinhLuanItem,
                    Message = "Bình luận viết thành công. Bình luận cần được phê duyệt."
                };
            }
            catch (Exception ex)
            {
                oResultAction = new ResultAction()
                {
                    State = ActionState.Error,
                    Message = ex.Message + ex.StackTrace
                };
            }
            return oResultAction;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA_MANGXAHOI()
        {
            CommentMXHDA oBinhLuanDA = new CommentMXHDA();
            CommentMXHQuery oBinhLuanQuery = new CommentMXHQuery(HttpContext.Current.Request);
            oBinhLuanQuery._ModerationStatus = 1;
            var oData = oBinhLuanDA.GetListJsonSolr(oBinhLuanQuery);
            DataGridRender oGrid = new DataGridRender(oData, oBinhLuanQuery.Draw,
                oBinhLuanDA.TongSoBanGhiSauKhiQuery);
            return oGrid;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction AddComment_MANGXAHOI()
        {
            ResultAction oResultAction = new ResultAction();
            try
            {

                CommentMXHItem oBinhLuanItem = new CommentMXHItem();
                oBinhLuanItem.UpdateObject(HttpContext.Current.Request);
                CommentMXHDA oBinhLuanDA = new CommentMXHDA();
               string outtb =   oBinhLuanDA.UpdateObject<CommentMXHItem>(oBinhLuanItem);
                if (string.IsNullOrEmpty(outtb))
                {
                    oBinhLuanDA.UpdateSPModerationStatus(oBinhLuanItem.ID, Microsoft.SharePoint.SPModerationStatusType.Approved);
                    if (oBinhLuanItem.BaiViet.LookupId > 0)
                    {
                        MangXaHoiDA oBaiVietDA = new MangXaHoiDA();
                        MangXaHoiItem oBaiVietItem = oBaiVietDA.GetByIdToObjectSelectFields<MangXaHoiItem>(oBinhLuanItem.BaiViet.LookupId, "");
                        oBaiVietDA.SystemUpdateOneField(oBinhLuanItem.BaiViet.LookupId, "SoLuotComment", oBaiVietItem.SoLuotComment + 1);
                    }
                    oResultAction = new ResultAction()
                    {
                        State = ActionState.Succeed,
                        OData = oBinhLuanItem,
                        Message = "Thêm mới bình luận viết thành công"
                    };
                }
                else
                {
                    oResultAction = new ResultAction()
                    {
                        State = ActionState.Error,
                        Message = outtb
                    };
                }
            }
            catch (Exception ex)
            {
                oResultAction = new ResultAction()
                {
                    State = ActionState.Error,
                    Message = ex.Message + ex.StackTrace
                };
            }
            return oResultAction;
        }
    }
}
