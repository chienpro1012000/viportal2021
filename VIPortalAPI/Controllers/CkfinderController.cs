﻿using Newtonsoft.Json.Linq;
using System.IO;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace VIPortalAPI.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class CkfinderController : ApiController
    {
        // GET api/<controller>/5
        public string Get()
        {
            return "value";
        }
        [HttpGet]
        public IHttpActionResult connector(string command)
        {
            switch (command)
            {
                case "Init":
                    //read file va trả ve du lieu.

                    JObject data = JObject.Parse(File.ReadAllText(HttpContext.Current.Server.MapPath(@"\Contents\ckfinder\init.json")));
                    return Ok(data);
                case "GetFolders":
                    //read file va trả ve du lieu.
                    //{"folders":[{"name":"Simax","hasChildren":false,"acl":1023}],"currentFolder":{"path":"/","acl":1023},"resourceType":"Files"}

                    JObject dataFolders = JObject.Parse(File.ReadAllText(HttpContext.Current.Server.MapPath(@"\Contents\ckfinder\init.json")));
                    return Ok(dataFolders);
                default:
                    return Ok();
            }

        }
    }
}