﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ViPortal_Utils.Base;
using VIPortalAPI.BaseAPI;
using VIPortalAPP;

namespace VIPortalAPI.Controllers
{
    /// <summary>
    /// tin tuc
    /// </summary>
    public class LinkController : SIBaseAPI
    {
        /// <summary>
        /// get danh sach tin theo chuyen muc
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA()
        {
            
            LienKetWebsiteQuery oLienKetWebsiteQuery = new LienKetWebsiteQuery(HttpContext.Current.Request);
            LienKetWebsiteDA oLienKetWebsiteDA = new LienKetWebsiteDA(UrlSite);
            oLienKetWebsiteQuery._ModerationStatus = 1;
            var oData = oLienKetWebsiteDA.GetListJson(oLienKetWebsiteQuery);
            DataGridRender oGrid = new DataGridRender(oData, oLienKetWebsiteQuery.Draw, oLienKetWebsiteDA.TongSoBanGhiSauKhiQuery);
            return oGrid;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA_CONGTP()
        {

            CongThanhPhanQuery oCongThanhPhanQuery = new CongThanhPhanQuery(HttpContext.Current.Request);
            CongThanhPhanDA oCongThanhPhanDA = new CongThanhPhanDA(UrlSite);
            oCongThanhPhanQuery._ModerationStatus = 1;
            var oData = oCongThanhPhanDA.GetListJson(oCongThanhPhanQuery);
            DataGridRender oGrid = new DataGridRender(oData, oCongThanhPhanQuery.Draw, oCongThanhPhanDA.TongSoBanGhiSauKhiQuery);
            return oGrid;
        }
    }
}