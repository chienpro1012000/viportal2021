﻿using Library_Shared.Utilities;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using ViPortal_Utils;
using ViPortal_Utils.Base;
using VIPortalAPI.BaseAPI;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.Lconfig;
using ViPortalData.LichCaNhan;
using ViPortalData.LichDonVi;
using ViPortalData.LichPhong;
using ViPortalData.LichTapDoan;
using ViPortalData.LichTongCongTy;

namespace VIPortalAPI.Controllers
{

    /// <summary>
    /// 
    /// 
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    public class LichLamViecController : SIBaseAPI
    {

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA_LICHTAPDOAN()
        {
            LichTapDoanDA oLichTongCongTyDA = new LichTapDoanDA(true);
            LichTapDoanQuery oLichTapDoanQuery = new LichTapDoanQuery(HttpContext.Current.Request);
            oLichTapDoanQuery._ModerationStatus = 1;
            oLichTapDoanQuery.FieldOrder = "LichThoiGianBatDau";
            oLichTapDoanQuery.Ascending = true;
            var oData = oLichTongCongTyDA.GetListJsonSolr(oLichTapDoanQuery);
            DataGridRender oGrid = new DataGridRender(oData, oLichTapDoanQuery.Draw,
                oLichTongCongTyDA.TongSoBanGhiSauKhiQuery);
            return oGrid;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public LichTapDoanItem GETBYID_LICHTAPDOAN()
        {
            LichTapDoanItem lichTapDoanItem = new LichTapDoanItem();
            LichTapDoanDA oLichTongCongTyDA = new LichTapDoanDA(true);
            LichTapDoanQuery oLichTapDoanQuery = new LichTapDoanQuery(HttpContext.Current.Request);
            if (oLichTapDoanQuery.ItemID > 0)
            {
                lichTapDoanItem = oLichTongCongTyDA.GetByIdToObject<LichTapDoanItem>(oLichTapDoanQuery.ItemID);
            }
            return lichTapDoanItem;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA_LICHCANHAN()
        {
            LichCaNhanDA oLichCaNhanDA = new LichCaNhanDA(true);
            LichCaNhanQuery oLichCaNhanQuery = new LichCaNhanQuery(HttpContext.Current.Request);
            oLichCaNhanQuery._ModerationStatus = 1;
            oLichCaNhanQuery.FieldOrder = "LichThoiGianBatDau";
            oLichCaNhanQuery.Ascending = true;
            var oData = oLichCaNhanDA.GetListJsonSolr(oLichCaNhanQuery);
            DataGridRender oGrid = new DataGridRender(oData, oLichCaNhanQuery.Draw,
                oLichCaNhanDA.TongSoBanGhiSauKhiQuery);
            return oGrid;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public LichCaNhanItem GETBYID_LICHCANHAN()
        {
            LichCaNhanItem LichCaNhanItem = new LichCaNhanItem();
            LichCaNhanDA oLichTongCongTyDA = new LichCaNhanDA(true);
            LichCaNhanQuery oLichCaNhanQuery = new LichCaNhanQuery(HttpContext.Current.Request);
            if (oLichCaNhanQuery.ItemID > 0)
            {
                LichCaNhanItem = oLichTongCongTyDA.GetByIdToObject<LichCaNhanItem>(oLichCaNhanQuery.ItemID);
            }
            return LichCaNhanItem;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA_LICHTONGCTY()
        {
            LichTongCongTyDA oLichTongCongTyDA = new LichTongCongTyDA(true);
            LichTongCongTyQuery oLichTongCongTyQuery = new LichTongCongTyQuery(HttpContext.Current.Request);
            oLichTongCongTyQuery._ModerationStatus = 1;
            oLichTongCongTyQuery.FieldOrder = "LichThoiGianBatDau";
            oLichTongCongTyQuery.Ascending = true;
            var oData = oLichTongCongTyDA.GetListJsonSolr(oLichTongCongTyQuery);
            DataGridRender oGrid = new DataGridRender(oData, oLichTongCongTyQuery.Draw,
                oLichTongCongTyDA.TongSoBanGhiSauKhiQuery);
            return oGrid;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public LichTongCongTyItem GETBYID_LICHTONGCTY()
        {
            LichTongCongTyItem LichTongCongTyItem = new LichTongCongTyItem();
            LichTongCongTyDA oLichTongCongTyDA = new LichTongCongTyDA(true);
            LichTongCongTyQuery oLichTongCongTyQuery = new LichTongCongTyQuery(HttpContext.Current.Request);
            if (oLichTongCongTyQuery.ItemID > 0)
            {
                LichTongCongTyItem = oLichTongCongTyDA.GetByIdToObject<LichTongCongTyItem>(oLichTongCongTyQuery.ItemID);
            }
            return LichTongCongTyItem;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA_PHONG()
        {
            LichPhongDA oLichPhongDA = new LichPhongDA(true);
            LichPhongQuery oLichPhongQuery = new LichPhongQuery(HttpContext.Current.Request);
            oLichPhongQuery._ModerationStatus = 1;
            oLichPhongQuery.FieldOrder = "LichThoiGianBatDau";
            oLichPhongQuery.Ascending = true;
            var oData = oLichPhongDA.GetListJsonSolr(oLichPhongQuery);
            DataGridRender oGrid = new DataGridRender(oData, oLichPhongQuery.Draw,
                oLichPhongDA.TongSoBanGhiSauKhiQuery);
            return oGrid;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public LichPhongItem GETBYID_PHONG()
        {
            LichPhongItem LichPhongItem = new LichPhongItem();
            LichPhongDA oLichPhongDA = new LichPhongDA(true);
            LichPhongQuery oLichPhongQuery = new LichPhongQuery(HttpContext.Current.Request);
            if (oLichPhongQuery.ItemID > 0)
            {
                LichPhongItem = oLichPhongDA.GetByIdToObject<LichPhongItem>(oLichPhongQuery.ItemID);
            }
            return LichPhongItem;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA_DONVI()
        {
            LichDonViDA oLichDonViDA = new LichDonViDA(true);
            LichDonViQuery oLichDonViQuery = new LichDonViQuery(HttpContext.Current.Request);
            oLichDonViQuery._ModerationStatus = 1;

            oLichDonViQuery.FieldOrder = "LichThoiGianBatDau";
            oLichDonViQuery.Ascending = true;
            var oData = oLichDonViDA.GetListJsonSolr(oLichDonViQuery);
            DataGridRender oGrid = new DataGridRender(oData, oLichDonViQuery.Draw,
                oLichDonViDA.TongSoBanGhiSauKhiQuery);
            return oGrid;
        }
        // This presumes that weeks start with Monday.
        // Week 1 is the 1st week of the year with a Thursday in it.
        /// <summary>
        /// 
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public int GetIso8601WeekOfYear(DateTime time)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public LichDonViItem GETBYID_DONVI()
        {
            LichDonViItem LichDonViItem = new LichDonViItem();
            LichDonViDA oLichDonViDA = new LichDonViDA(true);
            LichDonViQuery oLichDonViQuery = new LichDonViQuery(HttpContext.Current.Request);
            if (oLichDonViQuery.ItemID > 0)
            {
                LichDonViItem = oLichDonViDA.GetByIdToObject<LichDonViItem>(oLichDonViQuery.ItemID);
            }
            return LichDonViItem;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// 
        [System.Web.Http.HttpPost]
        public DataGridRender GetListLichHienThi()
        {

            if (string.IsNullOrEmpty(UrlList))
                UrlList = "/noidung/Lists/LichHop";

            if (!string.IsNullOrEmpty(HttpContext.Current.Request["last"]))
                last = HttpContext.Current.Request["last"];
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["next"]))
                next = HttpContext.Current.Request["next"];
            LichHopQuery oLichHopQuery = new LichHopQuery(HttpContext.Current.Request);
            int SONgayFix = 7;

            if (oLichHopQuery.TuNgay_LichHop.HasValue && !oLichHopQuery.DenNgay_LichHop.HasValue)
            {
                oLichHopQuery.DenNgay_LichHop = oLichHopQuery.TuNgay_LichHop.Value.AddDays(7);
            }
            if (oLichHopQuery.DenNgay_LichHop.HasValue && !oLichHopQuery.TuNgay_LichHop.HasValue)
            {
                oLichHopQuery.TuNgay_LichHop = oLichHopQuery.DenNgay_LichHop.Value.AddDays(-7);
            }
            if (!oLichHopQuery.DenNgay_LichHop.HasValue && !oLichHopQuery.TuNgay_LichHop.HasValue)
                LayNgayDauVaKhoiTaoGiaTriTuan_Nam();
            else
            {

                firstDay = oLichHopQuery.TuNgay_LichHop.Value;
                intCurWeek = GetIso8601WeekOfYear(firstDay);
                SONgayFix = oLichHopQuery.DenNgay_LichHop.Value.Subtract(oLichHopQuery.TuNgay_LichHop.Value).Days + 1;
                //get week off day.
            }
            LichHopDA oLichHopDA = new LichHopDA(UrlList, true);


            for (int i = 0; i < SONgayFix; i++)
            {

                HienThiSuKienTrongNgay(firstDay.AddDays(i), i, oLichHopQuery);

            }
            DataGridRender oGrid = new DataGridRender(lstLichHienThi, oLichHopQuery.Draw,
                oLichHopDA.TongSoBanGhiSauKhiQuery);
            oGrid.Query = Convert.ToString(intCurWeek);
            oGrid.result = "Từ ngày: " + string.Format("{0:dd/MM/yyyy}", firstDay) + "- đến ngày: " + string.Format("{0:dd/MM/yyyy}", firstDay.AddDays(6));
            return oGrid;
        }
        int intCurYear = System.DateTime.Today.Year;

        DateTime firstDay = System.DateTime.Today;
        int intCurWeek = 1;
        List<LichHienThi> lstLichHienThi = new List<LichHienThi>();
        string strYear = "";
        string strlastyear = "";
        string strnextyear = "";

        string strWeek = "";
        string strlastweek = "";
        string strnextweek = "";
        public string last { get; set; }
        public string next { get; set; }
        public List<LUserJson> LstUserThamGia { get; set; }
        public int STTLich = 0;
        public bool CheckDateTime()
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtCurDay"></param>
        /// <param name="SoTTNgayTrongTuan"></param>
        protected void HienThiSuKienTrongNgay(DateTime dtCurDay, int SoTTNgayTrongTuan, LichHopQuery oLichHopQuery)
        {
            LichHienThi oLichHienThi = new LichHienThi();
            oLichHienThi.Thu = dtCurDay.GetThuByNgay();
            oLichHienThi.Ngay = string.Format("{0:dd/MM/yyyy}", dtCurDay);
            string meessgaeerro = "";
            try
            {
                LichHopDA objLichPhongban = new LichHopDA(UrlList, true);
                LichHopQuery lichHopQuery = new LichHopQuery()
                {
                    LichNgay = dtCurDay,
                    //DenNgay_LichHop = dtCurDay,
                    _ModerationStatus = 1,
                    FieldOrder = "LichThoiGianBatDau",
                    Ascending = true,
                };

                lichHopQuery.SearchIn = oLichHopQuery.SearchIn;
                lichHopQuery.Keyword = oLichHopQuery.Keyword;
                lichHopQuery.fldGroup = oLichHopQuery.fldGroup;
                lichHopQuery.CreatedUser = oLichHopQuery.CreatedUser;
                List<LichHopJson> lstLichHop = objLichPhongban.GetListJsonSolr(lichHopQuery);


                //nếu là là lịch công ty thì hiển htij cả lịch phòng tự đăng ký.
                if (UrlList.Contains("LichDonVi"))
                {
                    //thì sẽ lấy thêm lịch phòng tự đăng ký ko phải lịch giao về.
                    //get all phòng ban của người dùng
                    LGroupDA lGroupDA = new LGroupDA();
                    List<LGroupJson> LstPhongBan = lGroupDA.GetListJson(new LGroupQuery()
                    {
                        GroupParent = CurentUser.Groups.ID,
                        isGetByParent = true
                    });
                    lichHopQuery.fldGroup = 0;
                    lichHopQuery.UrlList = new List<string>() { "/noidung/Lists/LichPhong" };
                    lichHopQuery.LichTrangThai = 999;
                    lichHopQuery.isGetLichPhongCuadonVi = true;
                    lichHopQuery.LstfldGroup = LstPhongBan.Select(x => x.ID).ToList();
                    List<LichHopJson> lstLichHopPhong = objLichPhongban.GetListJsonSolr(lichHopQuery);
                    lstLichHop.AddRange(lstLichHopPhong);
                }



                lstLichHop = lstLichHop.OrderBy(x => x.LichThoiGianBatDau).ThenBy(y => y.LichThoiGianKetThuc).ToList();
                List<int> lstUserThamGia = new List<int>();
                lstLichHop.ForEach(x => lstUserThamGia.AddRange(x.LichThanhPhanThamGia.Select(y => y.ID)));
                //lstUserThamGia.AddRange(lstLichHop.Select(x => x.CreatedUserID));
                //lstUserThamGia = lstUserThamGia.Where(x => x > 0).Distinct().ToList();
                LUserDA lUserDA = new LUserDA();
                LstUserThamGia = lUserDA.GetListJson(new LUserQuery() { isGetBylistID = true, lstIDget = lstUserThamGia, Length = 0 });
                //SPQuery spQuery = LaySuKienTheoNgay(dtCurDay, intCurPhongban);
                //SPListItemCollection dsSuKien = objLichPhongban.LaySuKienTheoNgay(spQuery);
                if (lstLichHop != null && lstLichHop.Count > 0)
                {
                    string strKetQua = "";
                    strKetQua = strKetQua + "<table class='tblChiTietLich' cellpadding='0' cellspacing='0'>";
                    foreach (LichHopJson curitem in lstLichHop)
                    {
                        STTLich++;
                        meessgaeerro = "";
                        int isDuyetLich = 0; //default
                        int isCreated = 0;
                        int OnlyView = 0;
                        int KiemDuyetND = 0;
                        string classTr = "";
                        //Nếu là lịch tự tạo
                        if ((!string.IsNullOrEmpty(curitem.LogText) && curitem.LogText.Contains($"_CreatedUser")) && (curitem.LichTrangThai == 1 || curitem.LichTrangThai == 2 || curitem.LichTrangThai == 0))/*Trạng thái chờ duyệt*/
                        {
                            if (UrlList.Contains("LichDonVi"))
                            {
                                //if (curitem.LichTrangThai == 1) //chờ kieemr tra thông tin.
                                //{
                                //    if (CurentUser.PerQuery.Contains($"|{CurentUser.Groups.ID}_01010207|")) //quyền kiểm tra nội dung lịch đơn vị.
                                //    {
                                //        isDuyetLich = 5; //kiểm soát nội dung
                                //        classTr = " lichchoduyet";
                                //        KiemDuyetND = 1;
                                //    }
                                //    else isDuyetLich = 2;
                                //}
                                if (curitem.LichTrangThai == 1 || curitem.LichTrangThai == 0)
                                {
                                    bool isLDPHongChuTriDuyet = false;
                                    if ((curitem.LichLanhDaoChuTri == null || curitem.LichLanhDaoChuTri.ID == 0))
                                    {
                                        LookupData CHU_TRI = new LookupData();
                                        if (!string.IsNullOrEmpty(curitem.CHU_TRI))
                                        {
                                            CHU_TRI = clsFucUtils.GetLoookupByStringFiledData(curitem.CHU_TRI);
                                            if (CHU_TRI.ID > 0)
                                            {
                                                //phòng ban chủ trì sẽ lấy từ dây ra để check
                                                if (CurentUser.UserPhongBan.ID == CHU_TRI.ID && CurentUser.PerQuery.Contains($"|{CurentUser.Groups.ID}_01010305|"))
                                                {
                                                    isLDPHongChuTriDuyet = true;
                                                }
                                            }
                                        }
                                        if (CHU_TRI.ID <= 0) //khoogn co nguoi chu tri thi lay phong ban cua nguoi tao ra de check.
                                        {
                                            //tìm ra phòng tạo lịch.
                                            //nếu người dùng là lãnh đạo của phòng tạo ra lịch và có quyền duyệt lịch thì cũng ok.
                                            if (!string.IsNullOrEmpty(curitem.LogText) && curitem.LogText.Contains($"_PBCreated{CurentUser.UserPhongBan.ID}_") && CurentUser.PerQuery.Contains($"|{CurentUser.Groups.ID}_01010305|"))
                                            {
                                                isLDPHongChuTriDuyet = true;
                                            }
                                        }
                                    }

                                    if (isLDPHongChuTriDuyet || CurentUser.ID == curitem.LichLanhDaoChuTri.ID || CurentUser.PerQuery.Contains($"|{CurentUser.Groups.ID}_01010205|"))
                                    {
                                        isDuyetLich = 1;
                                        classTr = " lichchoduyet";
                                    }
                                    else isDuyetLich = 2;
                                }

                            }
                            if (UrlList.Contains("LichPhong"))
                            {
                                if (curitem.LogText.Contains("_LichDonVi-") || curitem.LogText.Contains("_LichTongCongTy-"))
                                {
                                    //neeus laf lich giao xuong thi phan cong tiep
                                }
                                else //neu ko thi moi lafm tiep
                                {
                                    if (CurentUser.ID == curitem.LichLanhDaoChuTri.ID || CurentUser.PerQuery.Contains($"|{CurentUser.Groups.ID}_01010305|")) //nếu có lịch phong.
                                    {
                                        isDuyetLich = 1;
                                        classTr = " lichchoduyet";
                                    }
                                    else isDuyetLich = 2;
                                }
                            }
                            if (curitem.LichTrangThai == 1 || curitem.LichTrangThai == 2)
                            {
                                if ((!string.IsNullOrEmpty(curitem.LogText) && curitem.LogText.Contains($"_CreatedUser{CurentUser.ID}_")) || CurentUser.PerQuery.Contains($"|{CurentUser.Groups.ID}_01010207|")) //Là người tạo
                                {
                                    //isDuyetLich = 3; //để trạng thái bằng 3 để hiển thị lên.
                                    classTr = " lichchoduyetcreater";
                                    isCreated = 1; //là người tạo
                                }
                            }
                            if (UrlList.Contains("LichDonVi"))
                            {
                                if (curitem.fldGroup.ID != oLichHopQuery.fldGroup)
                                {
                                    OnlyView = 1;
                                    //isDuyetLich = 0;
                                }
                            }

                        }
                        if (curitem.LichTrangThai == 4) //bị trả về thì chỉ hiển thị ở người tạo
                        {
                            if (!string.IsNullOrEmpty(curitem.LogText) && curitem.LogText.Contains($"_CreatedUser{CurentUser.ID}_")) //Là người tạo
                            {
                                isDuyetLich = 4; //để trạng thái bằng 3 để hiển thị lên.
                                classTr = " lichchoduyetcreater";
                            }
                            else isDuyetLich = 2; //k hiển thị ở những người còn lại.
                        }
                        meessgaeerro += "Quabuocchekc1";
                        if (isDuyetLich != 2 || curitem.LichTrangThai == 0 || isCreated == 1)
                        {
                            #region Xử lý với từng bản ghi.
                            string ChuTri = curitem.LichLanhDaoChuTri.Title;
                            if (string.IsNullOrEmpty(ChuTri)) ChuTri = curitem.CHU_TRI;
                            string TPThamGia = string.Join(", ", curitem.LichThanhPhanThamGia.Select(x => x.Title));
                            TPThamGia += curitem.THANH_PHAN;
                            TPThamGia = "Thành phần: " + TPThamGia;
                            //strKetQua = strKetQua + "<tbody>";
                            strKetQua = strKetQua + "<tr class='" + (STTLich % 2 == 0 ? "clschan" : "clsle") + ((curitem.LichTrangThai == 1 || curitem.LichTrangThai == 2) ? classTr : "") + "'>";
                            strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 7% ; text-align:center'>" +
                               (curitem.LichThoiGianBatDau.Value.AddHours(7).Date == dtCurDay ? curitem.LichThoiGianBatDau.Value.AddHours(7).ToString("HH:mm") + "-" : "8:00-") +
                                (curitem.LichThoiGianKetThuc.HasValue ? ((curitem.LichThoiGianKetThuc.HasValue && curitem.LichThoiGianKetThuc.Value.AddHours(7).Date == dtCurDay) ? curitem.LichThoiGianKetThuc.Value.AddHours(7).ToString("HH:mm") : "17:00") : "");
                            if (!string.IsNullOrEmpty(curitem.LogText))
                            {
                                if (curitem.LogText.Contains("_TT-ho_"))
                                {
                                    strKetQua = strKetQua + "<br>(<span class='LichHoan'>Hoãn</span>)";
                                }
                                else if (curitem.LogText.Contains("_TT-bs_"))
                                {
                                    strKetQua = strKetQua + "<br>(<span class='LichBoSung'>Bổ sung</span>)";
                                }
                                else if (curitem.LogText.Contains("_TT-dt_"))
                                {
                                    strKetQua = strKetQua + "<br>(<span class='LichBoSung'>Thay đổi</span>)";
                                }
                                //dt
                                else
                                {
                                    if (curitem.DBPhanLoai == 1)
                                    {
                                        strKetQua = strKetQua + "<br>(<span class='LichBoSung'>Bổ sung</span>)";
                                    }
                                    else if (curitem.DBPhanLoai == 2)
                                    {
                                        strKetQua = strKetQua + "<br>(<span class='LichHoan'>Hoãn</span>)";
                                    }
                                    else if (curitem.DBPhanLoai == 3)
                                    {
                                        strKetQua = strKetQua + "<br>(<span class='LichThayDoi'>Thay đổi</span>)";
                                    }
                                }
                            }
                            if (isDuyetLich == 4)
                            {
                                strKetQua = strKetQua + "<br>(<span class='LichTraVeTuLD'>Không Duyệt</span>)";
                            }

                            strKetQua = strKetQua + "</td>";
                            meessgaeerro += "colum2";
                            strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'>" + curitem.LichDiaDiem + "</td>";
                            string FileAttach = "";
                            if (curitem.ListFileAttach != null && curitem.ListFileAttach.Count > 0)
                            {
                                FileAttach = "<br/>Tài liệu: " + string.Join(",", curitem.ListFileAttach.Select(x => $"<a target=\"_blank\" href=\"{x.Url}\">{x.Name}</a>"));
                            }
                            if (!string.IsNullOrEmpty(curitem.LichGhiChu))
                            {
                                FileAttach = "<br/>Ghi chú: " + curitem.LichGhiChu;
                            }
                            strKetQua = strKetQua + "<td class='Lich_ChiTiet' data-ItemID=" + curitem.ID + " style=''>" + curitem.Title + FileAttach + "</td>";
                            //strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 10%'>" + ChuTri + "</td>";

                            if (!string.IsNullOrEmpty(curitem.LogText) && curitem.LogText.Contains("_CreatedUser"))
                            {

                                meessgaeerro += "_CreatedUser";
                                int CountCheckTrung = lstLichHop.Count(x => curitem.DBPhanLoai != 2 && x.LichLanhDaoChuTri != null && x.LichLanhDaoChuTri.ID > 0 &&
                                    curitem.LichLanhDaoChuTri != null && curitem.LichLanhDaoChuTri.ID > 0
                                    && x.LichLanhDaoChuTri.ID == curitem.LichLanhDaoChuTri.ID && x.ID != curitem.ID &&
                                curitem.LichThoiGianBatDau.HasValue && (!string.IsNullOrEmpty(x.LogText) && !x.LogText.Contains("_TT-ho_")) && x.DBPhanLoai != 2 &&
                                 ((curitem.LichThoiGianKetThuc.HasValue && curitem.LichThoiGianBatDau <= x.LichThoiGianBatDau && x.LichThoiGianBatDau < curitem.LichThoiGianKetThuc) ||
                                 (x.LichThoiGianKetThuc.HasValue && curitem.LichThoiGianKetThuc.HasValue && curitem.LichThoiGianBatDau < x.LichThoiGianKetThuc && x.LichThoiGianKetThuc <= curitem.LichThoiGianKetThuc) ||
                                 (x.LichThoiGianBatDau.HasValue && curitem.LichThoiGianKetThuc.HasValue && curitem.LichThoiGianBatDau >= x.LichThoiGianBatDau && x.LichThoiGianKetThuc > curitem.LichThoiGianBatDau))
                                 );
                                meessgaeerro += "CountCheckTrung";
                                //nếu là lịch do đơn vị đăng ký.
                                LookupData CHU_TRI = clsFucUtils.GetLoookupByStringFiledData(curitem.CHU_TRI);
                                List<LookupData> CHUAN_BI = clsFucUtils.GetDanhSachMutilLoookupByStringFiledData(curitem.CHUAN_BI);
                                strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'>" + "<span class='clsTipChuTri'>Chủ trì: " + CHU_TRI.Title + "</span><br/><span class='clsTipChuanBi'>Chuẩn bị: " + string.Join(",", CHUAN_BI.Select(x => x.Title)) + "</span></td>";
                                meessgaeerro += "CountCheckTrung_CHU_TRI";
                                //chỗ thành phần này sẽ điều chỉnh.
                                strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'>" + "" + "</td>";
                                // cột thành phần ở đơn vị
                                strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'>" +
                                     "<span class=\"" + (CountCheckTrung > 0 ? "clsWarningTrung" : "") + "\"><span class='clsTipLD'>Lãnh đạo: " + curitem.LichLanhDaoChuTri.Title + (CountCheckTrung > 0 ? "<span class=\"clsGhiChuTrung\">(Trùng lịch)</span>" : "") + "</span></span>" +
                                    "<br/>" + "Phòng ban: " + string.Join(", ", curitem.LichPhongBanThamGia.Select(x => x.Title)) +
                                     (curitem.LichThanhPhanThamGia.Count > 0 ? ("<br/>" + "Cán bộ: <a class=\"custom-tooltip\" title=\"" + BuildHTMLUserToolTip(curitem.LichThanhPhanThamGia.Select(x => x.ID).ToList()) + "\" data-toggle=\"tooltip\" data-html=\"true\" href=\"#\">Chi tiết</a>") : "") +
                                     (!string.IsNullOrEmpty(curitem.THANH_PHAN) ? ("<br/>" + curitem.THANH_PHAN) : "") +
                                     ((!string.IsNullOrEmpty(curitem.objGiaoLichDonVi.LichSoNguoiThamgia)) ? ("<br/>Số lượng" + curitem.objGiaoLichDonVi.LichSoNguoiThamgia) : "") +
                                     (!string.IsNullOrEmpty(curitem.objGiaoLichDonVi.LichGhiChuGiao) ? ("<br/>" + "Ghi chú: " + curitem.objGiaoLichDonVi.LichGhiChuGiao) : "") +
                                     (curitem.objGiaoLichDonVi.ListFileAttach.Count > 0 ? ("<br/>" + "Đính kèm: " + GetHtmlFile(curitem.objGiaoLichDonVi.ListFileAttach)) : "") +
                                     ((curitem.objGiaoLichDonVi.JsonKhachMoi != null && curitem.objGiaoLichDonVi.JsonKhachMoi.Count > 0 && !string.IsNullOrEmpty(curitem.objGiaoLichDonVi.JsonKhachMoi.FirstOrDefault().Hoten)) ? ("<br/>" + "Khách mời: " + string.Join(", ", curitem.objGiaoLichDonVi.JsonKhachMoi.Select(x => x.Hoten))) : "") +
                                     (!string.IsNullOrEmpty(curitem.TBNoiDung) ? $"<br/>Ghi chú phòng: {curitem.TBNoiDung}" : "") +
                                     "</td>";
                                //strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'>" + curitem.THANH_PHAN + "</td>";

                                meessgaeerro += "colum3";
                            }
                            else
                            {
                                strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'><span class='clsTipChuTri'>Chủ trì:" + curitem.CHU_TRI + "</span><br/><span class='clsTipChuanBi'>Chuẩn bị:" + curitem.CHUAN_BI + "</span></td>";
                                strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'>" + curitem.THANH_PHAN + "</td>";
                                meessgaeerro += "colum3";
                                if (UrlList.Contains("LichTongCongTy"))
                                {
                                    strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'>" + "" + "</td>";
                                }
                                else
                                {
                                    meessgaeerro += "CountCheckTrung2";
                                    //check xem lanhd đao cho bị trùng ko, để xử lý.
                                    //dùng linq tìm các lịch khác có thông tin đó.
                                    int CountCheckTrung = lstLichHop.Count(x => !curitem.LogText.Contains("_TT-ho_") && x.LichLanhDaoChuTri != null && x.LichLanhDaoChuTri.ID > 0 &&
                                    curitem.LichLanhDaoChuTri != null && curitem.LichLanhDaoChuTri.ID > 0
                                    && x.LichLanhDaoChuTri.ID == curitem.LichLanhDaoChuTri.ID && x.ID != curitem.ID &&
                                 curitem.LichThoiGianBatDau.HasValue && (!string.IsNullOrEmpty(x.LogText) && !x.LogText.Contains("_TT-ho_")) && x.DBPhanLoai != 2 &&
                                 ((curitem.LichThoiGianKetThuc.HasValue && curitem.LichThoiGianBatDau <= x.LichThoiGianBatDau && x.LichThoiGianBatDau < curitem.LichThoiGianKetThuc) ||
                                 (x.LichThoiGianKetThuc.HasValue && curitem.LichThoiGianKetThuc.HasValue && curitem.LichThoiGianBatDau < x.LichThoiGianKetThuc && x.LichThoiGianKetThuc <= curitem.LichThoiGianKetThuc) ||
                                 (x.LichThoiGianBatDau.HasValue && curitem.LichThoiGianKetThuc.HasValue && curitem.LichThoiGianBatDau >= x.LichThoiGianBatDau && x.LichThoiGianKetThuc > curitem.LichThoiGianBatDau))
                                 );
                                    meessgaeerro += "CountCheckTrung3";
                                    strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'>" + "<span class=\"" + (CountCheckTrung > 0 ? "clsWarningTrung" : "") + "\"><span class='clsTipLD'>Lãnh đạo: " + curitem.LichLanhDaoChuTri.Title  + (CountCheckTrung > 0 ? "<span class=\"clsGhiChuTrung\">(Trùng lịch)</span>" : "") + "</span></span>" +
                                        "<br/>" + "Phòng ban: " + string.Join(", ", curitem.LichPhongBanThamGia.Select(x => x.Title)) +

                                         (curitem.LichThanhPhanThamGia.Count > 0 ? ("<br/>" + "Cán bộ: <a class=\"custom-tooltip\" title=\"" + BuildHTMLUserToolTip(curitem.LichThanhPhanThamGia.Select(x => x.ID).ToList()) + "\" data-toggle=\"tooltip\" data-html=\"true\" href=\"#\">Chi tiết</a>") : "") +
                                         (!string.IsNullOrEmpty(curitem.objGiaoLichDonVi.LichGhiChuGiao) ? ("<br/>" + "Ghi chú: " + curitem.objGiaoLichDonVi.LichGhiChuGiao) : "") +
                                         (curitem.objGiaoLichDonVi.ListFileAttach.Count > 0 ? ("<br/>" + "Đính kèm: " + GetHtmlFile(curitem.objGiaoLichDonVi.ListFileAttach)) : "") +
                                         ((curitem.objGiaoLichDonVi.JsonKhachMoi != null && curitem.objGiaoLichDonVi.JsonKhachMoi.Count > 0 && !string.IsNullOrEmpty(curitem.objGiaoLichDonVi.JsonKhachMoi.FirstOrDefault().Hoten)) ? ("<br/>" + "Khách mời: " + string.Join(", ", curitem.objGiaoLichDonVi.JsonKhachMoi.Select(x => x.Hoten))) : "") +
                                         (!string.IsNullOrEmpty(curitem.TBNoiDung) ? $"<br/>Ghi chú phòng: {curitem.TBNoiDung}" : "") +
                                         "</td>";
                                    meessgaeerro += "CountCheckTrung2end";
                                }
                            }
                            string tdPhancong = "";
                            if (UrlList.Contains("LichTongCongTy"))
                            {
                                if (CurentUser.PerQuery.Contains($"{CurentUser.Groups.ID}_01010206")) //check quyền giao lịch của đơn vị
                                {
                                    tdPhancong = "<a title=\"Phân công lịch cho đơn vị\" href=\"javascript:PhanCong(" + curitem.ID + ");\"><i class=\"fa fa-users\" aria-hidden=\"true\"></i></a>";
                                    if (!string.IsNullOrEmpty(curitem.LogText) && curitem.LogText.Contains($"_${CurentUser.Groups.ID}$_")) //nếu là giao rồi thì thôi
                                    {
                                        tdPhancong = "";
                                    }
                                }
                                strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 3%'>" + tdPhancong + "</td>";
                            }
                            if (UrlList.Contains("LichPhong"))
                            {
                                if (isDuyetLich == 1)
                                {
                                    if (curitem.LichTrangThai == 1)//chờ duyệt
                                        tdPhancong = "<a href=\"javascript:;\" style=\"cursor: pointer;\" data-ItemID=\"" + curitem.ID + "\" title=\"Chờ duyệt\" class=\"CalendarCheck\"><i class=\"far fa-calendar-check\"></i></a>";
                                    else tdPhancong = "<a href=\"javascript:;\" style=\"cursor: pointer;\" data-ItemID=\"" + curitem.ID + "\" title=\"Hủy duyệt\" class=\"CalendarUnCheck\"><i class=\"fas fa-undo\"></i></a>";
                                }
                                else if (CurentUser.PerQuery.Contains("01010306") && isDuyetLich != 1)
                                {
                                    tdPhancong = "<a title=\"Phân công lịch phòng\" href=\"javascript:PhanCongLichPhong(" + curitem.ID + "," + curitem.LichTrangThai + ");\"><i class=\"fa fa-users\" aria-hidden=\"true\"></i></a>";
                                    if (!string.IsNullOrEmpty(curitem.LogText) && curitem.LogText.Contains($"_${CurentUser.UserPhongBan.ID}$_")) //nếu là giao rồi thì thôi
                                    {
                                        tdPhancong = "";
                                        tdPhancong = "<a title=\"Sửa Phân công lịch phòng\" href=\"javascript:SuaPhanCongLichPhong(" + curitem.ID + "," + curitem.LichTrangThai + ");\"><i class=\"fa fa-edit\" aria-hidden=\"true\"></i></a>";
                                        //nếu có thì cho sửa phân công.

                                    }
                                }
                                if (curitem.LichTrangThai == 1 && (isDuyetLich == 3 || isDuyetLich == 4 || isCreated == 1)) //mới tạo
                                {
                                    tdPhancong += "<a title=\"Sửa lịch phòng\" href=\"javascript:SuaLichPhong(" + curitem.ID + "," + curitem.LichTrangThai + ");\"><i class=\"fa fa-edit\" aria-hidden=\"true\"></i></a><br>";
                                    tdPhancong += "<a title=\"Xóa Lịch  phòng\" href=\"javascript:XoaLichPhong(" + curitem.ID + ");\"><i class=\"far fa-trash-alt\"></i></a>";
                                }
                                tdPhancong += "<a title=\"Copy Lịch  Phòng\" href=\"javascript:CopyLichPhong(" + curitem.ID + ");\"><i class=\"far fa-copy\"></i></a>";
                                strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 3%'>" + tdPhancong + "</td>";
                            }
                            string tddaxem = "";
                            if (UrlList.Contains("LichDonVi") && OnlyView != 1)
                            {

                                meessgaeerro += "LichDonViAction";
                                if (isDuyetLich == 1)
                                {
                                    if (curitem.LichTrangThai == 1 || curitem.LichTrangThai == 2)//chờ duyệt
                                        tddaxem = "<a href=\"javascript:;\" style=\"cursor: pointer;\" data-ItemID=\"" + curitem.ID + "\" title=\"Chờ duyệt\" class=\"CalendarCheck\"><i class=\"far fa-calendar-check\"></i></a>";
                                    else if (curitem.LichTrangThai == 0)
                                        tddaxem = "<a href=\"javascript:;\" style=\"cursor: pointer;\" data-ItemID=\"" + curitem.ID + "\" title=\"Hủy duyệt\" class=\"CalendarUnCheck\"><i class=\"fas fa-undo\"></i></a>";
                                }
                                if (isDuyetLich != 1 && CurentUser.PerQuery.Contains($"{CurentUser.Groups.ID}_01010206") && (string.IsNullOrEmpty(curitem.LogText) || (!string.IsNullOrEmpty(curitem.LogText) && !curitem.LogText.Contains("CreatedUser")))) //check quyền giao lịch của đơn vị và chỉ giao lại với các lịch từ tổng
                                {
                                    tddaxem = "<a title=\"Sửa Phân công lịch cho đơn vị\" href=\"javascript:SuaPhanCong(" + curitem.ID + ");\"><i class=\"fas fa-edit\"></i></a>";
                                    //if (!string.IsNullOrEmpty(curitem.LogText) && curitem.LogText.Contains($"_${CurentUser.Groups.ID}$_")) //nếu là giao rồi thì thôi
                                    //{
                                    //    tddaxem = "";
                                    //}
                                }
                                if (isDuyetLich == 3 || isDuyetLich == 4 || isCreated == 1) //mới tạo
                                {
                                    tddaxem += "<a title=\"Sửa lịch đơn vị\" href=\"javascript:SuaLichDonVi(" + curitem.ID + "," + curitem.LichTrangThai + ");\"><i class=\"fa fa-edit\" aria-hidden=\"true\"></i></a><br>";
                                    tddaxem += "<a title=\"Xóa Lịch  Đơn vị\" href=\"javascript:XoaLich(" + curitem.ID + ");\"><i class=\"far fa-trash-alt\"></i></a><br>";
                                }
                                //<i class="far fa-copy"></i> copy
                                tddaxem += "<a title=\"Copy Lịch  Đơn vị\" href=\"javascript:CopyLichDonvi(" + curitem.ID + ");\"><i class=\"far fa-copy\"></i></a>";
                                if (KiemDuyetND == 1) //nếu là người kiểm duyệt thì sẽ có nút abc....
                                {
                                    tddaxem += "<a title=\"Kiểm duyệt Lịch  Đơn vị\" href=\"javascript:KiemDuyetLichDonvi(" + curitem.ID + ");\"><i class=\"far fa-calendar-check\"></i></a>";
                                }
                            }
                            else if (UrlList.Contains("LichTongCongTy"))
                            {
                                if (CurentUser.PerQuery.Contains($"{CurentUser.Groups.ID}_01010206")) //check quyền giao lịch của đơn vị
                                {
                                    tddaxem = $"<a href=\"javascript:DaxemTongCTY(" + curitem.ID + ");\" data-ItemID=\"" + curitem.ID + "\" title=\"Đánh dấu đã xem\" class=\"DaxemLichTongCongTy\"><i class=\"fas fa-flag-checkered\"></i></a>";
                                    if (!string.IsNullOrEmpty(curitem.LogText) && curitem.LogText.Contains($"_${CurentUser.Groups.ID}$_")) //nếu là giao rồi thì thôi
                                    {
                                        tddaxem = "<i class=\"far fa-check-circle\"></i>";
                                    }
                                }
                            }
                            else if (UrlList.Contains("LichPhong"))
                            {
                                //phải duyệt.
                                if (isDuyetLich == 1)
                                {
                                    //tddaxem = "<a  data-ItemID=\"" + curitem.ID + "\" style=\"cursor: pointer;\" title=\"Chờ duyệt\" class=\"CalendarCheck\"><i class=\"far fa-calendar-check\"></i></a>";
                                }
                                else
                                {
                                    if (CurentUser.PerQuery.Contains("01010306"))
                                    {
                                        tddaxem = "<input type=\"checkbox\" id=\"IDLichDonVi" + curitem.ID + "\" name=\"IDLich\" value=\"" + curitem.ID + "\"><br>";
                                        if (!string.IsNullOrEmpty(curitem.LogText) && curitem.LogText.Contains($"_${CurentUser.UserPhongBan.ID}$_")) //nếu là giao rồi thì thôi
                                        {
                                            tddaxem = "<i class=\"far fa-check-circle\"></i><br>";
                                        }
                                    }
                                }
                                
                                //if (isDuyetLich == 3) //mới tạo
                                //{
                                //    tddaxem = "<a title=\"Sửa lịch phòng\" href=\"javascript:SuaLichPhong(" + curitem.ID + "," + curitem.LichTrangThai + ");\"><i class=\"fa fa-edit\" aria-hidden=\"true\"></i></a>";
                                //}
                            }
                            else if (UrlList.Contains("LichCaNhan"))
                            {
                                if (curitem.OldID == 0)
                                {
                                    tddaxem = "<a title=\"Sửa lịch cá nhân\" href=\"javascript:SuaLichCaNhan(" + curitem.ID + "," + curitem.LichTrangThai + ");\"><i class=\"fa fa-edit\" aria-hidden=\"true\"></i></a><br>";
                                    tddaxem += "<a title=\"Xóa Lịch  cá nhân\" href=\"javascript:XoaLichCaNhan(" + curitem.ID + ");\"><i class=\"far fa-trash-alt\"></i></a>";
                                }
                                tddaxem += "<a title=\"Copy Lịch  Đơn vị\" href=\"javascript:CopyLichCaNhan(" + curitem.ID + ");\"><i class=\"far fa-copy\"></i></a>";
                                //strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 3%'>" + tdPhancong + "</td>";

                            }
                            strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 3%'>" + tddaxem + "</td>";
                            strKetQua = strKetQua + "</tr>";
                            #endregion
                            //strKetQua = strKetQua + "</tbody>";
                        }
                    }
                    strKetQua = strKetQua + "</table>";
                    oLichHienThi.Content = strKetQua;
                }
                else
                {
                    string strKetQua = "";

                    strKetQua = strKetQua + "<table class='tblChiTietLich' cellpadding='0' cellspacing='0'>";
                    //strKetQua = strKetQua + "<tbody>";
                    strKetQua = strKetQua + "<tr class='clsle'>";
                    strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 7% ; text-align:center'></td>";

                    strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'></td>";
                    strKetQua = strKetQua + "<td class='Lich_ChiTiet linkview' style=''></td>";
                    strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'></td>";
                    strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'></td>";
                    strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'></td>";
                    strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 3%'></td>";
                    if (UrlList.Contains("LichTongCongTy"))
                    {
                        strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 3%'></td>";
                    }
                    if (UrlList.Contains("LichPhong"))
                    {
                        strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 3%'></td>";
                    }
                    strKetQua = strKetQua + "</tr>";
                    //strKetQua = strKetQua + "</tbody>";
                    strKetQua = strKetQua + "</table>";
                    oLichHienThi.Content = strKetQua;
                }
            }
            catch (Exception ex)
            {
                //Response.Write(ex.ToString());
                throw new Exception(meessgaeerro, ex);
            }
            lstLichHienThi.Add(oLichHienThi);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="LstUser"></param>
        /// <returns></returns>
        public string BuildHTMLUserToolTip(List<int> LstUser)
        {
            List<LUserJson> temp = LstUserThamGia.Where(x => LstUser.Contains(x.ID)).ToList();
            var results = from p in temp
                          group p by p.UserPhongBan.Title;
            string HtmlGroup = "<p>";

            foreach (var item in results)
            {
                HtmlGroup += $"Phòng {item.Key}: ";
                HtmlGroup += (string.Join(", ", item.Select(x => x.Title)) + "<br/>");
            }
            HtmlGroup += "</p>";
            return HtmlGroup;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lstFile"></param>
        /// <returns></returns>
        public string GetHtmlFile(List<BaseFileAttach> lstFile)
        {
            string htmlfile = "";
            foreach (BaseFileAttach item in lstFile)
            {
                htmlfile += $"<a href=\"{item.Url}\"  target=\"_blank\">{item.Name}</a>,";
            }
            return htmlfile;
        }

        /// <summary>
        /// Lay ra gia tri int cua thu - ngay dau nam
        /// </summary>
        /// <param name="nam">Nam</param>
        /// <returns>0: Thu 2; 1: thu 3; ..... 5: thu bay,6: Chu Nhat</returns>
        public int ThuCuaNgayDauNam(int nam)
        {
            // khoi tao ngay dau tien cua nam
            DateTime startYear = new DateTime(nam, 1, 1);
            int startdayofyear = 0;

            if (startYear.DayOfWeek == DayOfWeek.Monday) startdayofyear = 0; // thu 2
            else if (startYear.DayOfWeek == DayOfWeek.Tuesday) startdayofyear = 1; // thu 3
            else if (startYear.DayOfWeek == DayOfWeek.Wednesday) startdayofyear = 2;
            else if (startYear.DayOfWeek == DayOfWeek.Thursday) startdayofyear = 3;
            else if (startYear.DayOfWeek == DayOfWeek.Friday) startdayofyear = 4;
            else if (startYear.DayOfWeek == DayOfWeek.Saturday) startdayofyear = 5;
            else startdayofyear = 6;
            return startdayofyear;
        }
        /// <summary>
        /// Lay so tuan trong 1 nam
        /// </summary>
        /// <param name="nam">Nam can lay</param>
        /// <returns>So tuan, tinh tu tuan 1</returns>
        public int LaySoTuanTrongNam(int nam)
        {
            // kiem tra ngay dau tien cua nam vao thu nao
            int startdayofyear = ThuCuaNgayDauNam(nam);

            DateTime endYear = new DateTime(nam, 12, 31);
            // khai bao ngay dau tuan dau tien cua nam
            int SoNgayTrongNam = endYear.DayOfYear;

            // Lay so tuan trong 1 nam
            int SoTuanTrongNam = 0;
            SoTuanTrongNam = (SoNgayTrongNam + startdayofyear) / 7;
            int SoDu = (SoNgayTrongNam + startdayofyear) % 7;
            if (SoDu > 0) SoTuanTrongNam = SoTuanTrongNam + 1;

            return SoTuanTrongNam;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="CurWeek"></param>
        /// <param name="CurYear"></param>
        /// <returns></returns>
        public DateTime NgayDauTuan(int CurWeek, int CurYear)
        {
            //Khai bao, khoi tao gia tri ngay dau tuan
            DateTime firstDate = new DateTime(CurYear, 1, 1);
            // khoi tao ngay dau tien cua nam
            DateTime startYear = new DateTime(CurYear, 1, 1);
            // khoi tao ngay cuoi cung cua nam
            DateTime endYear = new DateTime(CurYear, 12, 31);
            // khai bao ngay dau tuan dau tien cua nam
            DateTime NgayDauTuanThu1 = new DateTime(CurYear, 1, 1);
            // So ngay trong nam
            int SoNgayTrongNam = endYear.DayOfYear;

            // kiem tra ngay dau tien cua nam vao thu nao
            int startdayofyear = ThuCuaNgayDauNam(CurYear);


            // Lay so tuan trong 1 nam
            int SoTuanTrongNam = LaySoTuanTrongNam(CurYear);

            // kiem tra so tuan truyen vao co hop le hay khong
            // neu khong hop le se lay tuan hien thoi
            if (CurWeek < 1 || CurWeek > SoTuanTrongNam)
            {
                if (CurYear != DateTime.Today.Year) CurWeek = 1;
                else
                {
                    // lay tuan hien thoi
                    DateTime today = System.DateTime.Today;

                    int NgayHienthoi = today.DayOfYear + startdayofyear;
                    CurWeek = NgayHienthoi / 7;
                    if (NgayHienthoi % 7 > 0) CurWeek = CurWeek + 1;
                }
            }

            // lay ngay dau tien cua tuan dau tien
            NgayDauTuanThu1 = startYear.AddDays(startdayofyear * -1);

            int NgayDauTuan = (CurWeek - 1) * 7;
            // Lay ngay dau tien cua tuan can lay
            firstDate = NgayDauTuanThu1.AddDays(NgayDauTuan);

            // tra ve ngay dau tien cua tuan can lay
            return firstDate;
        }
        /// <summary>
        /// LayNgayDauVaKhoiTaoGiaTriTuan_Nam
        /// </summary>
        protected void LayNgayDauVaKhoiTaoGiaTriTuan_Nam()
        {
            // lay nam
            try
            {
                if (HttpContext.Current.Request["p_year"] != null) strYear = HttpContext.Current.Request["p_year"].ToString().Trim();
                intCurYear = int.Parse(strYear);
                if (intCurYear > 9999 || intCurYear < 1900)
                {
                    intCurYear = System.DateTime.Today.Year;
                    strYear = intCurYear.ToString();
                }
            }
            catch
            {
                intCurYear = System.DateTime.Today.Year;
                strYear = intCurYear.ToString();
            }

            int SoTuanTrongNam = LaySoTuanTrongNam(intCurYear);

            // lay tuan
            try
            {
                if (!string.IsNullOrEmpty(HttpContext.Current.Request["p_week"]))
                {
                    strWeek = HttpContext.Current.Request["p_week"].ToString().Trim();
                    intCurWeek = Convert.ToInt32(strWeek);
                    //intCurWeek = int.Parse(strWeek);

                    int startdayofyear = ThuCuaNgayDauNam(intCurYear);
                    if (intCurWeek < 1 || intCurWeek > SoTuanTrongNam)
                    {
                        if (intCurYear != DateTime.Today.Year) intCurWeek = 1;
                        else
                        {
                            // lay tuan hien thoi
                            DateTime today = System.DateTime.Today;

                            int NgayHienthoi = today.DayOfYear + startdayofyear;
                            intCurWeek = NgayHienthoi / 7;
                            if (NgayHienthoi % 7 > 0) intCurWeek = intCurWeek + 1;
                        }
                    }
                    strWeek = intCurWeek.ToString();
                }
                else
                {
                    int startdayofyear = ThuCuaNgayDauNam(intCurYear);

                    if (intCurYear != DateTime.Today.Year) intCurWeek = 1;
                    else
                    {
                        // lay tuan hien thoi
                        DateTime today = System.DateTime.Today;

                        int NgayHienthoi = today.DayOfYear + startdayofyear;
                        intCurWeek = NgayHienthoi / 7;
                        if (NgayHienthoi % 7 > 0) intCurWeek = intCurWeek + 1;
                    }
                    strWeek = intCurWeek.ToString();
                }
            }
            catch
            {

                int startdayofyear = ThuCuaNgayDauNam(intCurYear);

                if (intCurYear != DateTime.Today.Year) intCurWeek = 1;
                else
                {
                    // lay tuan hien thoi
                    DateTime today = System.DateTime.Today;

                    int NgayHienthoi = today.DayOfYear + startdayofyear;
                    intCurWeek = NgayHienthoi / 7;
                    if (NgayHienthoi % 7 > 0) intCurWeek = intCurWeek + 1;
                }
                strWeek = intCurWeek.ToString();
            }

            // lay gia tri cua ngay dau tuan

            firstDay = NgayDauTuan(intCurWeek, intCurYear);

            //lblNgayChon.Text = "Từ ngày: " + string.Format("{0:dd/MM/yyyy}", firstDay) + " - Đến ngày: " + string.Format("{0:dd/MM/yyyy}", firstDay.AddDays(6));

            if (intCurWeek == SoTuanTrongNam)
            {
                strnextweek = "1";
                int nam = intCurYear + 1;
                strnextyear = nam.ToString();

                int tuan = intCurWeek - 1;
                strlastweek = tuan.ToString();
                strlastyear = strYear;

            }
            else if (intCurWeek == 1)
            {


                int tuan = intCurWeek + 1;
                strnextweek = tuan.ToString();
                strnextyear = strYear;


                int nam = intCurYear - 1;
                strlastyear = nam.ToString();
                tuan = LaySoTuanTrongNam(nam);
                strlastweek = tuan.ToString();

            }
            else
            {

                strnextyear = strYear;
                strlastyear = strYear;


                int tuan = intCurWeek + 1;
                strnextweek = tuan.ToString();

                tuan = intCurWeek - 1;
                strlastweek = tuan.ToString();
            }

        }
        /// <summary>
        /// Làm việc gì đó ở đây
        /// </summary>
        /// <returns></returns>
        /// 
        [System.Web.Http.HttpPost]
        public DataGridRender GetListCaTrucHienThi()
        {
            DMCaTrucDA dMCaTrucDA = new DMCaTrucDA(UrlSite);
            List<DMCaTrucJson> lstDMCaTruc = dMCaTrucDA.GetListJson(new DMCaTrucQuery() { FieldOrder = "Title", Ascending = true, });

            LichCaTrucQuery oLichCaTrucQuery = new LichCaTrucQuery(HttpContext.Current.Request);
            oLichCaTrucQuery.Length = 0;
            lstDMCaTruc = lstDMCaTruc.Where(x => x.fldGroup != null && x.fldGroup.ID == oLichCaTrucQuery.fldGroup).ToList();
            DataGridRender dataGridRender = new DataGridRender();
            LichCaTrucDA lichCaTrucDA = new LichCaTrucDA(UrlSite);
            List<LichCaTrucJson> lstLichCaTruc = lichCaTrucDA.GetListJson(oLichCaTrucQuery);
            int SONgayFix = oLichCaTrucQuery.DenNgay.Value.Subtract(oLichCaTrucQuery.TuNgay.Value).Days + 1;

            //get thông tin user.
            LUserDA lUserDA = new LUserDA();
            List<LUserJson> LstUser = lUserDA.GetListJson(new LUserQuery()
            {
                isGetBylistID = true,
                lstIDget = lstLichCaTruc.Select(x => x.CreatedUser.ID).Distinct().ToList(),
                Keyword = !string.IsNullOrEmpty(HttpContext.Current.Request["keywordUser"]) ? HttpContext.Current.Request["keywordUser"] : "",
                SearchIn = new List<string>() { "Title", "UserEmail", "UserSDT", "UserChucVu" }
            });

            LGroupDA oLGroupDA = new LGroupDA();
            List<LGroupJson> LstPhongBan = oLGroupDA.GetListJson(new LGroupQuery()
            {
                GroupParent = CurentUser.Groups.ID,
                isGetByParent = true
            });
            for (int i = 0; i < SONgayFix; i++)
            {
                DateTime curdate = oLichCaTrucQuery.TuNgay.Value.AddDays(i);


                //check xem nay là ngày gì.
                int DayOfWeek = (int)curdate.DayOfWeek;
                LichHienThi lichHienThi = new LichHienThi()
                {
                    Thu = curdate.GetThuByNgay(),
                    Ngay = string.Format("{0:dd/MM/yyyy}", curdate)
                };
                List<LichCaTrucJson> temp = lstLichCaTruc.Where(x => x.NgayTruc.Value.Date == curdate.Date).ToList();
                string strKetQua = "";
                strKetQua = strKetQua + "<table class='tblChiTietLich' cellpadding='0' cellspacing='0'>";

                //duyệt qua danh sách ca nữa là ok.
                foreach (DMCaTrucJson oDMCaTrucJson in lstDMCaTruc)
                {
                    int CountCaTruc = 0;
                    List<LichCaTrucJson> tempCaTruc = temp.Where(x => x.CaTruc.ID == oDMCaTrucJson.ID).ToList();
                    if (tempCaTruc.Count > 0)
                    {

                        tempCaTruc = tempCaTruc.OrderByDescending(x => x.DBPhanLoai).ToList();
                        for (int j = 0; j < tempCaTruc.Count; j++)
                        {

                            LichCaTrucJson tempitem = tempCaTruc[j];
                            if (LstUser.FindIndex(x => x.ID == tempitem.CreatedUser.ID) > -1)
                            {
                                #region MyRegion

                                strKetQua = strKetQua + "<tr class='clsle'>";
                                if (j == 0)
                                {
                                    strKetQua = strKetQua + "<td rowspan=\"" + tempCaTruc.Count + "\" class='Lich_ChiTiet' style='width: 13% ; text-align:center'>" + oDMCaTrucJson.Title + "<br/>" +
                                        //tempitem.ThoiGianBatDauThucTe.ToString("HH:mm") +
                                        //" - " + tempitem.ThoiGianKetThucThucTe.ToString("HH:mm") +
                                        "</td>";

                                }
                                LUserJson tempUserTruc = LstUser.FirstOrDefault(x => x.ID == tempitem.CreatedUser.ID);
                                LGroupJson PBTruc = LstPhongBan.FirstOrDefault(x => x.ID == tempUserTruc.UserPhongBan.ID);
                                strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'>" + (tempitem.DBPhanLoai == 1 ? "Trực chính" : "Trực Phụ") + "</td>";
                                strKetQua = strKetQua + "<td class='Lich_ChiTiet' style=''>" + (string.IsNullOrEmpty( PBTruc.MaDinhDanh ) ? PBTruc.Title : PBTruc.MaDinhDanh) + tempitem.CreatedUser.Title + "</td>";
                                strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'>" + LstUser.FirstOrDefault(x => x.ID == tempitem.CreatedUser.ID)?.UserSDT + "</td>";
                                strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'>" + LstUser.FirstOrDefault(x => x.ID == tempitem.CreatedUser.ID)?.UserEmail + "</td>";
                                strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'>" + tempitem.LichGhiChu + "</td>";

                                string tddaxem = "";
                                if (oLichCaTrucQuery.fldGroup == CurentUser.Groups.ID) //nếu là lịch đơn vị.
                                {
                                    if (tempitem.fldGroup.ID == oLichCaTrucQuery.fldGroup && CurentUser.PermissionsQuery.Contains("010108"))
                                    {
                                        tddaxem += "<a title=\"Sửa Lịch trực đơn vị\" href=\"javascript:SuaCaTruc(" + tempitem.ID + ");\"><i class=\"fas fa-edit\"></i></a>&nbsp;&nbsp;";
                                        tddaxem += "<a title=\"Sửa Lịch trực đơn vị\" href=\"javascript:XoaCaTruc(" + tempitem.ID + ");\"><i class=\"far fa-trash-alt\"></i></a>";
                                    }
                                }
                                else if (oLichCaTrucQuery.fldGroup == CurentUser.UserPhongBan.ID)
                                {
                                    if (tempitem.fldGroup.ID == oLichCaTrucQuery.fldGroup && CurentUser.PermissionsQuery.Contains("010107"))
                                    {
                                        tddaxem += "<a title=\"Sửa Lịch trực phòng\" href=\"javascript:SuaCaTrucPhong(" + tempitem.ID + ");\"><i class=\"fas fa-edit\"></i></a>&nbsp;&nbsp;";
                                        tddaxem += "<a title=\"Sửa Lịch trực phòng\" href=\"javascript:XoaCaTruc(" + tempitem.ID + ");\"><i class=\"far fa-trash-alt\"></i></a>";
                                    }
                                }
                                strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 3%'>" + tddaxem + "</td>";
                                strKetQua += "</tr>";
                                #endregion
                                CountCaTruc++;
                            }

                        }
                        if (CountCaTruc == 0)
                        {
                            bool isCaCuaNgay = false;
                            // curdate.DayOfWeek
                            if ((DayOfWeek == 0 || DayOfWeek == 6) && oDMCaTrucJson.LoaiCaTruc == 1)//nghỉ t7 cn
                            {
                                //là ngày nghỉ.
                                isCaCuaNgay = true;
                            }
                            if ((DayOfWeek != 0 && DayOfWeek != 6) && oDMCaTrucJson.LoaiCaTruc == 0)//nghỉ t7 cn
                            {
                                isCaCuaNgay = true;
                            }
                            if (isCaCuaNgay)
                            {
                                ////check xem nó thuộc nhóm nào thì fix vào. lịch nghỉ hay lịch gì.
                                //strKetQua = strKetQua + "<tr class='clsle'>";

                                //strKetQua = strKetQua + "<td  class='Lich_ChiTiet' style='width: 13% ; text-align:center'>" + oDMCaTrucJson.Title + "<br/>" + oDMCaTrucJson.ThoiGianBatDau +
                                //            " - " + oDMCaTrucJson.ThoiGianKetThuc + "</td>";
                                //strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'></td>";
                                //strKetQua = strKetQua + "<td class='Lich_ChiTiet' style=''></td>";
                                //strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'></td>";
                                //strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'></td>";
                                //strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'></td>";
                                //strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 3%'></td>";
                                //strKetQua += "</tr>";
                            }
                        }
                    }
                    else
                    {
                        bool isCaCuaNgay = false;
                        // curdate.DayOfWeek
                        if ((DayOfWeek == 0 || DayOfWeek == 6) && oDMCaTrucJson.LoaiCaTruc == 1)//nghỉ t7 cn
                        {
                            //là ngày nghỉ.
                            isCaCuaNgay = true;
                        }
                        if ((DayOfWeek != 0 && DayOfWeek != 6) && oDMCaTrucJson.LoaiCaTruc == 0)//nghỉ t7 cn
                        {
                            isCaCuaNgay = true;
                        }
                        if (isCaCuaNgay)
                        {
                            //check xem nó thuộc nhóm nào thì fix vào. lịch nghỉ hay lịch gì.
                            //strKetQua = strKetQua + "<tr class='clsle'>";

                            //strKetQua = strKetQua + "<td  class='Lich_ChiTiet' style='width: 13% ; text-align:center'>" + oDMCaTrucJson.Title + "<br/>" + oDMCaTrucJson.ThoiGianBatDau +
                            //            " - " + oDMCaTrucJson.ThoiGianKetThuc + "</td>";
                            //strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'></td>";
                            //strKetQua = strKetQua + "<td class='Lich_ChiTiet' style=''></td>";
                            //strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'></td>";
                            //strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'></td>";
                            //strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 15%'></td>";
                            //strKetQua = strKetQua + "<td class='Lich_ChiTiet' style='width: 3%'></td>";
                            //strKetQua += "</tr>";
                        }
                    }
                }
                strKetQua += "</table>";
                lichHienThi.Content = strKetQua;
                lstLichHienThi.Add(lichHienThi);
            }

            dataGridRender = new DataGridRender()
            {
                data = lstLichHienThi,
                draw = oLichCaTrucQuery.Draw,
                recordsTotal = lstLichHienThi.Count
            };
            return dataGridRender;
        }

        /// <summary>
        /// get file và return file
        /// </summary>
        /// <returns></returns>
        /// 
        [System.Web.Http.HttpGet]
        public IHttpActionResult ExportExcel()
        {
            ResultAction oResultAction = new ResultAction();
            LichCaTrucQuery oLichCaTrucQuery = new LichCaTrucQuery(HttpContext.Current.Request);
            oLichCaTrucQuery.Length = 0;
            string UrlFix = HttpContext.Current.Request["UrlFix"];
            LichCaTrucDA lichCaTrucDA = new LichCaTrucDA(UrlFix);
            LconfigDA oLconfigDA = new LconfigDA();
            LconfigJson oLconfigJson = oLconfigDA.GetListJson(new LconfigQuery()
            {
                ConfigType = "ExcelCaTruc"
            }).FirstOrDefault();
            List<LichCaTrucJson> lstLichCaTruc = lichCaTrucDA.GetListJson(oLichCaTrucQuery);

            LUserDA lUserDA = new LUserDA();
            List<LUserJson> LstUser = lUserDA.GetListJson(new LUserQuery()
            {
                isGetBylistID = true,
                lstIDget = lstLichCaTruc.Select(x => x.CreatedUser.ID).Distinct().ToList()
            });

            if (oLconfigJson != null)
            {
                string UrlFile = oLconfigJson.ListFileAttach[0].Url;
                var stream = oLconfigDA.SpListProcess.ParentWeb.GetFile(UrlFile).OpenBinary();

                Stream FileExcel = oLconfigDA.SpListProcess.ParentWeb.GetFile(UrlFile).OpenBinaryStream();
                XSSFWorkbook hssfworkbook = new XSSFWorkbook(FileExcel);
                ISheet oSheet = hssfworkbook.GetSheetAt(0);
                IRow oRow = oSheet.GetRow(1);
                BaseWordExcel.SetValueCellExcel(oRow, 0, $"Lịch trực  từ ngày { string.Format("{0:dd/MM/yyyy}", oLichCaTrucQuery.TuNgay.Value)} đến ngày { string.Format("{0:dd/MM/yyyy}", oLichCaTrucQuery.DenNgay.Value)}", null);

                //List<int> lstUserTruc = clsFucUtils.GetDanhSachIDsQuaFormPost(context.Request["LichThanhPhanThamGia"]);
                //oLichCaTrucItem.UpdateObject(context.Request);
                //LichCaTrucQuery oLichCaTrucQuery = new LichCaTrucQuery(context.Request);
                int SONgayFix = oLichCaTrucQuery.DenNgay.Value.Subtract(oLichCaTrucQuery.TuNgay.Value).Days + 1;
                DMCaTrucDA odMCaTrucDA = new DMCaTrucDA(UrlFix);
                List<DMCaTrucJson> lstDMCaTruc = odMCaTrucDA.GetListJson(new DMCaTrucQuery() { FieldOrder = "Title", Ascending = true, fldGroup = oLichCaTrucQuery.fldGroup });

                int startrow = 4;

                for (int i = 0; i < SONgayFix; i++)
                {
                    DateTime curdate = oLichCaTrucQuery.TuNgay.Value.AddDays(i);
                    //check xem nay là ngày gì.
                    int DayOfWeek = (int)curdate.DayOfWeek;
                    List<DMCaTrucJson> lstDMCaTrucByday = new List<DMCaTrucJson>();
                    if ((DayOfWeek == 0 || DayOfWeek == 6))//nghỉ t7 cn
                    {
                        lstDMCaTrucByday = lstDMCaTruc.Where(x => x.LoaiCaTruc == 1).ToList();
                    }
                    if ((DayOfWeek != 0 && DayOfWeek != 6))//nghỉ t7 cn
                    {
                        lstDMCaTrucByday = lstDMCaTruc.Where(x => x.LoaiCaTruc == 0).ToList();
                    }
                   

                    foreach (DMCaTrucJson oDMCaTrucJson in lstDMCaTrucByday)
                    {
                        try
                        {
                            List<LichCaTrucJson> lstLichNgay = lstLichCaTruc.Where(x => x.NgayTruc == curdate & x.CaTruc.ID == oDMCaTrucJson.ID).ToList();
                            if (lstLichNgay.Count > 0)
                            {
                                foreach (var item in lstLichNgay)
                                {
                                    IRow oRowValue = oSheet.GetRow(startrow);
                                    if (oRowValue == null) oRowValue = oSheet.CreateRow(startrow);
                                    BaseWordExcel.SetValueCellExcel(oRowValue, 0,  string.Format("{0:dd/MM/yyyy}", curdate), null);
                                    BaseWordExcel.SetValueCellExcel(oRowValue, 1, oDMCaTrucJson.Title, null);
                                    LUserJson lUserJson = LstUser.FirstOrDefault(x => x.ID == item.CreatedUser.ID);
                                    if (lUserJson != null)
                                    {
                                        BaseWordExcel.SetValueCellExcel(oRowValue, 2, lUserJson.Title, null);
                                        BaseWordExcel.SetValueCellExcel(oRowValue, 3, lUserJson.UserPhongBan.Title, null);
                                        BaseWordExcel.SetValueCellExcel(oRowValue, 4, item.DBPhanLoai == 1 ? "Trực chính" : "Trực phụ", null);
                                        BaseWordExcel.SetValueCellExcel(oRowValue, 5, lUserJson.UserSDT, null);
                                        BaseWordExcel.SetValueCellExcel(oRowValue, 6, lUserJson.UserEmail, null);
                                        BaseWordExcel.SetValueCellExcel(oRowValue, 7, "", null);
                                    }
                                    startrow++;
                                }
                            }
                            else
                            {
                                IRow oRowValue = oSheet.GetRow(startrow);
                                if (oRowValue == null) oRowValue = oSheet.CreateRow(startrow);
                                BaseWordExcel.SetValueCellExcel(oRowValue, 0, string.Format("{0:dd/MM/yyyy}", curdate), null);
                                startrow++;
                            }


                            ////tạo 1 ca trục vs 1 nguoi,
                            //sttuser++;
                            //if (sttuser >= lstUserTruc.Count)
                            //    sttuser = 0;
                            //oLichCaTrucItem = new LichCaTrucItem()
                            //{
                            //    fldGroup = new Microsoft.SharePoint.SPFieldLookupValue(oLichCaTrucQuery.fldGroup, ""),
                            //    CreatedUser = new Microsoft.SharePoint.SPFieldLookupValue(lstUserTruc[sttuser], ""),
                            //    CaTruc = new Microsoft.SharePoint.SPFieldLookupValue(oDMCaTrucJson.ID, ""),
                            //    NgayTruc = curdate,
                            //    DBPhanLoai = 0
                            //};
                            //oLichCaTrucItem.ThoiGianBatDauThucTe = DateTime.ParseExact($"{oLichCaTrucItem.NgayTruc.Value.ToString("dd/MM/yyyy")} {oDMCaTrucJson.ThoiGianBatDau}", "dd/MM/yyyy H:mm", CultureInfo.InvariantCulture);
                            //oLichCaTrucItem.ThoiGianKetThucThucTe = DateTime.ParseExact($"{oLichCaTrucItem.NgayTruc.Value.ToString("dd/MM/yyyy")} {oDMCaTrucJson.ThoiGianKetThuc}", "dd/MM/yyyy H:mm", CultureInfo.InvariantCulture);
                            //oLichCaTrucDA.UpdateObject<LichCaTrucItem>(oLichCaTrucItem);

                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Message + " + ".", ex);
                        }
                    }
                }

                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                try
                {
                    hssfworkbook.Write(bos);
                }
                finally
                {
                    bos.Close();
                }
                byte[] bytes = bos.ToByteArray();
                var result = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new ByteArrayContent(bytes)
                };
                result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = Path.GetFileName(UrlFile)
                };
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                var response = ResponseMessage(result);
                return response;

            }
            else
            {
                var result = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent("Đã có lỗi xảy ra, không có file biểu mẫu")
                };
                return ResponseMessage(result);
            }
        }
    }
}