﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using ViPortal_Utils.Base;
using ViPortalData;
using ViPortalData.Lconfig;

namespace VIPortalAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class DanhBaController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA()
        {

            DanhBaDA oDanhBaDA = new DanhBaDA();
            DanhBaQuery oDanhBaQuery = new DanhBaQuery(HttpContext.Current.Request);
            oDanhBaQuery._ModerationStatus = 1;
            oDanhBaQuery.FieldOrder = "DMSTT";
            oDanhBaQuery.Ascending = true;
            //var oData = oDanhBaDA.GetListJson(oDanhBaQuery);
            //DataGridRender oGrid = new DataGridRender(oData, oDanhBaQuery.Draw,
             //   oDanhBaDA.TongSoBanGhiSauKhiQuery);

            //xử lý lại chỗ này thôi.
            LconfigDA oLconfigDA = new LconfigDA();
            string APIEVNDanhBa = oLconfigDA.GetListJson(new LconfigQuery() { ConfigType = "APIEVNDanhBa" }).FirstOrDefault()?.ConfigValue;
            if (string.IsNullOrEmpty(APIEVNDanhBa))
                APIEVNDanhBa = "https://portal.evnhanoi.vn/apigateway";
            //lấy dữ liệu theo đơn vị.
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes("EVNHANOI:Evnhanoi@123");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.PostAsJsonAsync(APIEVNDanhBa + "/HRMS_DanhBa_NhanVien", new
            {
                MaDonVi = oDanhBaQuery.MaDonVi,
                PhongBan = oDanhBaQuery.PhongBan,
                TuKhoa = oDanhBaQuery.Keyword
            }).Result;

            //HttpResponseMessage response = client.PostAsJsonAsync(APIEVNDanhBa + "/HRMS_DanhBa_NhanVien").Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var root = JsonConvert.DeserializeObject<DanhBaRespon>(responseBody);
            List<DanhBaJson> lstDataReturn = new List<DanhBaJson>();
            DataGridRender oGrid = new DataGridRender(root.data, oDanhBaQuery.Draw,
                0);
            return oGrid;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public DataGridRender QUERYDATADONVI()
        {
            LconfigDA oLconfigDA = new LconfigDA();
            string APIEVNDanhBa = oLconfigDA.GetListJson(new LconfigQuery() { ConfigType = "APIHRMS" }).FirstOrDefault()?.ConfigValue;
            if (string.IsNullOrEmpty(APIEVNDanhBa))
                APIEVNDanhBa = "http://42.112.213.225:8074";

            DataGridRender oDataGridRender = new DataGridRender();
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Basic RVZOSEFOT0k6RVZOSEFOT0lAQ3VvbmdYMTY=");
            //HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(APIEVNDanhBa + "/api/get_HRMS_ThongTinToChuc").Result;

            string content = response.Content.ReadAsStringAsync().Result;

            List<LDonViEVN> lstDonvi = new List<LDonViEVN>();
            lstDonvi = Newtonsoft.Json.JsonConvert.DeserializeObject<List<LDonViEVN>>(content);
            List<LGroupJson> lstLGroupJson = new List<LGroupJson>();
            lstLGroupJson = lstDonvi.Select(x => new LGroupJson()
            {
                Title = x.orgName,
                ID = Convert.ToInt32(x.orgId)
            }).ToList();
            return new DataGridRender()
            {
                data = lstLGroupJson
            };
        }
        ///QUERYDATAPHONGBAN
        [HttpGet]
        public DataGridRender QUERYDATAPHONGBAN()
        {
            LconfigDA oLconfigDA = new LconfigDA();
            string APIHRMS = oLconfigDA.GetListJson(new LconfigQuery() { ConfigType = "APIHRMS" }).FirstOrDefault()?.ConfigValue;
            if (string.IsNullOrEmpty(APIHRMS))
                APIHRMS = "http://42.112.213.225:8074";

            string orgId = HttpContext.Current.Request["orgId"];
            DataGridRender oDataGridRender = new DataGridRender();

            HttpClient client = new HttpClient();
            //var byteArray = Encoding.ASCII.GetBytes("username:password1234");
            client.DefaultRequestHeaders.Add("Authorization", "Basic RVZOSEFOT0k6RVZOSEFOT0lAQ3VvbmdYMTY=");

            HttpResponseMessage response = client.GetAsync(APIHRMS + "/api/get_HRMS_ThongTinPhongBan").Result;

            string content = response.Content.ReadAsStringAsync().Result;

            List<LPhongBanEVN> lstDonvi = new List<LPhongBanEVN>();
            lstDonvi = Newtonsoft.Json.JsonConvert.DeserializeObject<List<LPhongBanEVN>>(content);
            List<LGroupJson> lstLGroupJson = new List<LGroupJson>();
            lstLGroupJson = lstDonvi.Where(y => y.orgId == orgId).Select(x => new LGroupJson()
            {
                Title = x.name,
                OldID = x.deptId
            }).ToList();
            return new DataGridRender()
            {
                data = lstLGroupJson
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DanhBaItem GETBYID_DANHBA()
        {
            DanhBaItem ODanhBaItem = new DanhBaItem();
            DanhBaDA oDanhBaDA = new DanhBaDA();
            DanhBaQuery oDanhBaQuery = new DanhBaQuery(HttpContext.Current.Request);
            if (oDanhBaQuery.ItemID > 0)
            {
                ODanhBaItem = oDanhBaDA.GetByIdToObject<DanhBaItem>(oDanhBaQuery.ItemID);
            }
            return ODanhBaItem;
        }
    }
}
