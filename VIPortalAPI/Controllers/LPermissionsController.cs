﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ViPortal_Utils.Base;
using VIPortalAPI.BaseAPI;
using ViPortalData;

namespace VIPortalAPI.Controllers
{
    public class LPermissionsController : SIBaseAPI
    {
        LPermissionQuery oLPermissionsQuery;
        [HttpGet]
        public List<TreeViewItem> QUERYTREE()
        {
            HttpResponseMessage oRespon = Request.CreateResponse(HttpStatusCode.OK, "");
            LPermissionDA oLPermissionsDA = new LPermissionDA();
            oLPermissionsQuery = new LPermissionQuery(HttpContext.Current.Request);
            oLPermissionsQuery.FieldOrder = "permistionMaPermission";
            //if ((!currentUser.permistionMaPermission.Contains(",08,")) && (string.IsNullOrEmpty(oLPermissionsQuery.permistionMaPermission) || !currentUser.permistionMaPermission.Contains("," + oLPermissionsQuery.permistionMaPermission)))
            //    oLPermissionsQuery.permistionMaPermission = "-1";
            List<LPermissionJson> LstLPermissionsJson = oLPermissionsDA.GetListJson(oLPermissionsQuery);
            
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["ListDataPer"]))
            {
                string lstper = HttpContext.Current.Request["ListDataPer"];
                var arrPer = lstper.Split(',');
                List<LPermissionJson> lstPer = new List<LPermissionJson>();
                foreach (var per in arrPer)
                {
                    LPermissionJson item = new LPermissionJson();
                    item = LstLPermissionsJson.FirstOrDefault(x => x.MaQuyen == per);
                    if (item.MaQuyen.Length > 2)
                    {
                        string perRoot = item.MaQuyen.Substring(0, 2);
                        LPermissionJson root = new LPermissionJson();
                        root = LstLPermissionsJson.FirstOrDefault(x => x.MaQuyen == perRoot);
                        if (lstPer.FindIndex(x => x.ID == root.ID) == -1)
                            lstPer.Add(root);
                        lstPer.Add(item);
                    }
                }
                LstLPermissionsJson = lstPer;
            }
            // BuildTreeRoot(LstLPermissionsJson, new TreeViewItem(), "", false);
            List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
            treeViewItems = BuildTree(LstLPermissionsJson, "", 2);
            return treeViewItems;
        }

        private List<TreeViewItem> BuildTree(List<LPermissionJson> lstLPermissionsJson, string MaQuyenParent, int maLength)
        {
            List<TreeViewItem> lsTreeViewItem = new List<TreeViewItem>();
            foreach (LPermissionJson item in lstLPermissionsJson.Where(x => x.MaQuyen != null && x.MaQuyen.Length == maLength && x.MaQuyen.StartsWith(MaQuyenParent)))
            {
                lsTreeViewItem.Add(new TreeViewItem(item.MaQuyen + "." + item.Title, Convert.ToString(item.ID),
                    BuildTree(lstLPermissionsJson, item.MaQuyen, maLength + 2), false, item.MaQuyen.Length > 2 ? false: true, oLPermissionsQuery.ListPermissCheck.Count(x => x == item.ID) > 0 ? true : false, false, item.MaQuyen));
            }
            return lsTreeViewItem;
        }
    }
}
