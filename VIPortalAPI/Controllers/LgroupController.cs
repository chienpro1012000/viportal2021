﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using ViPortal_Utils.Base;
using VIPortalAPI.BaseAPI;
using ViPortalData;

namespace VIPortalAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    public class LgroupController : SIBaseAPI
    {
        private LGroupQuery oLGroupQuery;
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public List<TreeViewItem> QUERYTREE()
        {
            HttpResponseMessage oRespon = Request.CreateResponse(HttpStatusCode.OK, "");
            LGroupDA oLGroupDA = new LGroupDA();
            oLGroupQuery = new LGroupQuery(HttpContext.Current.Request);
            oLGroupQuery.FieldOrder = "DMSTT";
            oLGroupQuery.Ascending = true;
            oLGroupQuery.Length = 0;
            //if ((!currentUser.permistionMaPermission.Contains(",08,")) && (string.IsNullOrEmpty(oLGroupQuery.permistionMaPermission) || !currentUser.permistionMaPermission.Contains("," + oLGroupQuery.permistionMaPermission)))
            //    oLGroupQuery.permistionMaPermission = "-1";
            List<LGroupJson> LstLGroupJson = oLGroupDA.GetListJson(oLGroupQuery);

            // BuildTreeRoot(LstLGroupJson, new TreeViewItem(), "", false);
            List<TreeViewItem> treeViewItems = new List<TreeViewItem>();

            List<TreeViewItem> treeViewItemsTemp = new List<TreeViewItem>();
            treeViewItemsTemp = BuildTree(LstLGroupJson, oLGroupQuery.GroupParent, true);
            if (oLGroupQuery.isGetParent)
            {
                LGroupItem LGroupJsonParent = oLGroupDA.GetByIdToObject<LGroupItem>(oLGroupQuery.GroupParent);
                treeViewItems = new List<TreeViewItem>() {
                    new TreeViewItem()
                    {
                        expanded = true,
                        selected = false,
                        folder = LGroupJsonParent.GroupIsDonVi,
                        extraClasses = string.Empty,
                        lazy = true,
                        children = treeViewItemsTemp,
                        title = LGroupJsonParent.Title,
                        key = LGroupJsonParent.ID.ToString()
                    }
                };
            }
            else treeViewItems = treeViewItemsTemp;
            return treeViewItems;
        }

        private List<TreeViewItem> BuildTree(List<LGroupJson> lstLGroupJson, int idParent, bool expanded = false)
        {
            List<TreeViewItem> lsTreeViewItem = new List<TreeViewItem>();
            foreach (LGroupJson item in lstLGroupJson.Where(x => x.GroupParent.ID == idParent))
            {
                TreeViewItem treeViewItem = new TreeViewItem(item.Title, Convert.ToString(item.ID),
                    BuildTree(lstLGroupJson, item.ID), item.GroupIsDonVi);
                treeViewItem.expanded = expanded;
                lsTreeViewItem.Add(treeViewItem);
            }
            return lsTreeViewItem;
        }
    }
}