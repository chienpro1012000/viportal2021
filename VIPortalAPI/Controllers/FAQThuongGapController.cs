﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using ViPortal_Utils.Base;
using VIPortalAPI.BaseAPI;
using ViPortalData.FAQThuongGap;

namespace VIPortalAPI.Controllers
{
    /// <summary>
    /// /
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    public class FAQThuongGapController : SIBaseAPI
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA()
        {
            FAQThuongGapDA oFAQThuongGapDA = new FAQThuongGapDA(UrlSite);
            FAQThuongGapQuery oFAQThuongGapQuery = new FAQThuongGapQuery(HttpContext.Current.Request);
            oFAQThuongGapQuery._ModerationStatus = 1;
            var oData = oFAQThuongGapDA.GetListJson(oFAQThuongGapQuery);
            DataGridRender oGrid = new DataGridRender(oData, oFAQThuongGapQuery.Draw,
                oFAQThuongGapDA.TongSoBanGhiSauKhiQuery);
            return oGrid;
        }
    }
}
