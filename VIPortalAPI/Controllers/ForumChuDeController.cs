﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using ViPortal_Utils.Base;
using VIPortalAPI.BaseAPI;
using VIPortalAPP;

namespace VIPortalAPI.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ForumChuDeController : SIBaseAPI
    {
        [HttpGet, HttpPost]
        public List<TreeViewItem> QUERYTREE()
        {
            List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
            ForumChuDeDA oForumChuDeDA = new ForumChuDeDA();
            ForumChuDeQuery oForumChuDeQuery = new ForumChuDeQuery(HttpContext.Current.Request);
            oForumChuDeQuery._ModerationStatus = 1;
            var oDataDanhMuc = oForumChuDeDA.GetListJsonSolr(oForumChuDeQuery);
            treeViewItems = BuildTree(oDataDanhMuc, 0);
            return treeViewItems;
        }

        public ResultAction AddThread()
        {
            ResultAction oResultAction = new ResultAction();
            try
            {
                
                BaiVietItem oBaiVietItem = new BaiVietItem();
                oBaiVietItem.UpdateObject(HttpContext.Current.Request);
                int ItemBaiViet = Convert.ToInt32(HttpContext.Current.Request["ItemIDBaiViet"]);
                BaiVietDA oBaiVietDA = new BaiVietDA(true);
                oBaiVietDA.UpdateObject<BaiVietItem>(oBaiVietItem);
                if (ItemBaiViet > 0)
                {
                    oResultAction = new ResultAction()
                    {
                        State = ActionState.Succeed,
                        OData = oBaiVietItem,
                        Message = "Cập nhật bài viết thành công"
                    };
                }
                else
                {
                    oResultAction = new ResultAction()
                    {
                        State = ActionState.Succeed,
                        OData = oBaiVietItem,
                        Message = "Thêm mới bài viết thành công"
                    };
                }                
            }
            catch (Exception ex)
            {
                oResultAction = new ResultAction()
                {
                    State = ActionState.Error,
                    Message = ex.Message + ex.StackTrace
                };
            }
            return oResultAction;
        }
        private List<TreeViewItem> BuildTree(List<ForumChuDeJson> lstLForumChuDeJson, int ChuDeParent = 0)
        {
            List<TreeViewItem> lsTreeViewItem = new List<TreeViewItem>();
            foreach (ForumChuDeJson item in lstLForumChuDeJson.Where(x => x.ParentChuDe.ID == ChuDeParent))
            {
                lsTreeViewItem.Add(new TreeViewItem()
                {
                    title = item.Title,
                    mota = item.MoTa,
                    key = Convert.ToString(item.ID),
                    children = BuildTree(lstLForumChuDeJson, item.ID),
                    folder = false,
                    expanded = true
                });
            }
            return lsTreeViewItem;
        }
    }
}
