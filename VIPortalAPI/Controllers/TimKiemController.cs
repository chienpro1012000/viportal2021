﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ViPortal_Utils.Base;
using ViPortalData.DMVideo;
using ViPortalData.TimKiem;

namespace VIPortalAPI.Controllers
{
    /// <summary>
    /// Tìm kiếm
    /// </summary>
    public class TimKiemController : BaseAPI.SIBaseAPI
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA()
        {
            TimKiemDA oDMVideoDA = new TimKiemDA();
            QuerySearch oQuerySearch = new QuerySearch(HttpContext.Current.Request);
            oQuerySearch._ModerationStatus = 1;
            oQuerySearch.UrlListNotGet = new List<string>() { "/noidung/Lists/LichPhong", "/noidung/Lists/LichCaNhan" };
            if (!string.IsNullOrEmpty(UrlSite))
            {
                oQuerySearch.UrlSiteStart = $"\\-{UrlSite.Substring(1)}\\-noidung-";
            }else oQuerySearch.UrlSiteStart = "\\-noidung\\-";
            var oData = oDMVideoDA.GetListJsonSolr(oQuerySearch);
            //oData = oData.Select(c => { c.urlChiTiet = value; return c; }).ToList();

            DataGridRender oGrid = new DataGridRender(oData, oQuerySearch.Draw,
                oDMVideoDA.TongSoBanGhiSauKhiQuery);
            return oGrid;
        }
    }
}