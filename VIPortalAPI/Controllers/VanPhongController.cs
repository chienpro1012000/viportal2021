﻿using Library_Shared.Utilities;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using ViPortal_Utils.Base;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.Lconfig;

namespace VIPortalAPI.Controllers
{
    public class VanPhongController : ApiController
    {
        public string TuNgay;
        public string DenNgay;
        public string Nam;
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_DanhMucHoSo()
        {
            DanhMucHoSoQuery oBaoCaoChiTeuDVKHQuery = new DanhMucHoSoQuery(HttpContext.Current.Request);
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/DOFFICE_BCDangKyMucLucHoSo/276/01-01-2021/02-02-2021";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_" + oBaoCaoChiTeuDVKHQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            string LoaiVB = oBaoCaoChiTeuDVKHQuery.LoaiDV;
            string TuNgay = oBaoCaoChiTeuDVKHQuery.TuNgay.Value.ToString("dd-MM-yyyy");
            string DenNgay = oBaoCaoChiTeuDVKHQuery.DenNgay.Value.ToString("dd-MM-yyyy");
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + LoaiVB + "/" + TuNgay + "/" + DenNgay;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapDanhMucHoSoJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(DanhMucHoSoJson),
                    new Dictionary<string, string>
                    {
                        {"KyHieuHoSo", "KY_HIEU_HS"},
                        {"TenHoSo", "TEN_HOSO"},
                        {"ThoiHanBaoQuan", "THOI_HAN_HS"},
                        {"NguoiLap", "NGUOI_LAP"},
                        {"GhiChu", "GHI_CHU"},

                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<DanhMucHoSoJson>>(model.data, settings);
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                Title = "DANH MỤC HỒ SƠ",
                recordsTotal = oData.Count
            };

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoChiTeuDVKHQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoChiTeuDVKHQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Từ ngày: ";
                    if (oBaoCaoChiTeuDVKHQuery.TuNgay.HasValue)
                        Tungaydenngay += oBaoCaoChiTeuDVKHQuery.TuNgay.Value.ToString("dd/MM/yyyy");
                    Tungaydenngay += " - đến ngày ";
                    if (oBaoCaoChiTeuDVKHQuery.DenNgay.HasValue)
                        Tungaydenngay += oBaoCaoChiTeuDVKHQuery.DenNgay.Value.ToString("dd/MM/yyyy");
                    lstValues.Add(new ClsExcel(2, 0, Tungaydenngay, StyleExcell.TextCenter));
                    lstValues.Add(new ClsExcel(3, 3, "Tổng số : " + oData.Count + " bản ghi", StyleExcell.TextCenter, false, 12, true, 3, 3, 3, 5));
                    int rowStart = 5;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.KyHieuHoSo, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.TenHoSo, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.ThoiHanBaoQuan, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.NguoiLap, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.GhiChu != "NULL" ? item.GhiChu : "", StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }

                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "DanhMucHoSo_" + DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_MucLucVanBanTaiLieu()
        {
            MucLucVanBanTaiLieuQuery oBaoCaoChiTeuDVKHQuery = new MucLucVanBanTaiLieuQuery(HttpContext.Current.Request);
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/DOFFICE_BaoCaoSoNhapTaiLieu/276/2013/IN_NGAY/2021/01-01-2021/31-12-2021/0/0/VBDE/ALL";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_" + oBaoCaoChiTeuDVKHQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            string LoaiDV = oBaoCaoChiTeuDVKHQuery.LoaiDV;
            string PhanLoaiVB = oBaoCaoChiTeuDVKHQuery.PhanLoaiVB;
            string PhanLoai = oBaoCaoChiTeuDVKHQuery.PhanLoai;
            string KieuGuiNhan = oBaoCaoChiTeuDVKHQuery.KieuGuiNhan;
            string TuSo = oBaoCaoChiTeuDVKHQuery.TuSo;
            string DenSo = oBaoCaoChiTeuDVKHQuery.DenSo;

            if (oBaoCaoChiTeuDVKHQuery.TuNgay.HasValue)
            {
                TuNgay = oBaoCaoChiTeuDVKHQuery.TuNgay.Value.ToString("dd-MM-yyyy");
                Nam = oBaoCaoChiTeuDVKHQuery.TuNgay.Value.ToString("yyyy");
            }
            else if (oBaoCaoChiTeuDVKHQuery.TuNgay.HasValue && oBaoCaoChiTeuDVKHQuery.TuNgay.Value.ToString("dd-MM-yyyy") == DateTime.Now.ToString("dd-MM-yyyy"))
            {
                TuNgay = "ALL";
                Nam = DateTime.Now.ToString("yyyy");
            }
            else
            {
                TuNgay = "ALL";
                Nam = DateTime.Now.ToString("yyyy");
            }
            if (oBaoCaoChiTeuDVKHQuery.DenNgay.HasValue)
            {
                DenNgay = oBaoCaoChiTeuDVKHQuery.DenNgay.Value.ToString("dd-MM-yyyy");
            }
            else if (oBaoCaoChiTeuDVKHQuery.DenNgay.HasValue && oBaoCaoChiTeuDVKHQuery.DenNgay.Value.ToString("dd-MM-yyyy") == DateTime.Now.ToString("dd-MM-yyyy"))
            {
                DenNgay = "ALL";
            }
            else
            {
                DenNgay = "ALL";
            }
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + LoaiDV + "/2013/" + PhanLoai + "/" + Nam + "/" + TuNgay + "/" + DenNgay + "/" + TuSo + "/" + DenSo + "/" + PhanLoaiVB + "/" + KieuGuiNhan;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapMucLucVanBanTaiLieuJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(MucLucVanBanTaiLieuJson),
                    new Dictionary<string, string>
                    {
                        {"STT", "STT"},
                        {"SoDen", "SO_DEN"},
                        {"NgayDen", "NGAY_NHAN"},
                        {"TacGiaVanBan", "NOI_BAN_HANH"},
                        {"SoKyHieu", "KY_HIEU"},
                        {"NgayVanBan", "NGAY_VB"},
                        {"TrichYeu", "TRICH_YEU"},
                        {"ToSo", "SO_TRANG"},
                        {"GhiChu", "GHI_CHU"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<MucLucVanBanTaiLieuJson>>(model.data, settings);
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                Title = "MỤC LỤC VĂN BẢN, TÀI LIỆU",
                Nam = Nam,
                recordsTotal = oData.Count
            };

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoChiTeuDVKHQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoChiTeuDVKHQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Từ ngày: ";
                    if (TuNgay == "ALL" && DenNgay == "ALL")
                    {
                        Tungaydenngay += "Từ Số: ";
                        if (TuSo != null)
                        {
                            Tungaydenngay += oBaoCaoChiTeuDVKHQuery.TuSo.ToString();
                        }
                        Tungaydenngay += " - đến số ";
                        if (DenSo != null)
                        {
                            Tungaydenngay += oBaoCaoChiTeuDVKHQuery.DenSo.ToString();
                        }

                    }
                    else
                    {
                        Tungaydenngay += "Từ ngày: ";
                        if (oBaoCaoChiTeuDVKHQuery.TuNgay.HasValue)
                            Tungaydenngay += oBaoCaoChiTeuDVKHQuery.TuNgay.Value.ToString("dd/MM/yyyy");
                        Tungaydenngay += " - đến ngày ";
                        if (oBaoCaoChiTeuDVKHQuery.DenNgay.HasValue)
                            Tungaydenngay += oBaoCaoChiTeuDVKHQuery.DenNgay.Value.ToString("dd/MM/yyyy");
                    }

                    lstValues.Add(new ClsExcel(1, 0, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 0, 0));
                    lstValues.Add(new ClsExcel(2, 0, "Đơn vị : " + oBaoCaoChiTeuDVKHQuery.DonViText, StyleExcell.TextCenter, false, 12, false, 0, 0));
                    int rowStart = 7;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.SoDen, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.NgayDen, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.TacGiaVanBan, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.SoKyHieu, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.NgayVanBan, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.TrichYeu, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.ToSo, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.GhiChu != "NULL" ? item.GhiChu : "", StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }
                    lstValues.Add(new ClsExcel(3, 7, "Có tổng số: " + oData.Count + " bản ghi", StyleExcell.TextCenter, false, 12, true, 3, 3, 7, 8));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "MucLucVanBanTaiLieu_" + DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_SoVanBanDi()
        {
            SoVanBanDiQuery oBaoCaoChiTeuDVKHQuery = new SoVanBanDiQuery(HttpContext.Current.Request);
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/DOFFICE_BaoCaoSoNhapTaiLieu/276/2013/IN_SO/2021/ALL/ALL/6000/7000/VBDI/ALL";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_" + oBaoCaoChiTeuDVKHQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            string LoaiDV = oBaoCaoChiTeuDVKHQuery.LoaiDV;
            string PhanLoaiVB = oBaoCaoChiTeuDVKHQuery.PhanLoaiVB;
            string PhanLoai = oBaoCaoChiTeuDVKHQuery.PhanLoai;
            string KieuGuiNhan = oBaoCaoChiTeuDVKHQuery.KieuGuiNhan;
            string TuSo = oBaoCaoChiTeuDVKHQuery.TuSo;
            string DenSo = oBaoCaoChiTeuDVKHQuery.DenSo;
            if (oBaoCaoChiTeuDVKHQuery.TuNgay.HasValue)
            {
                TuNgay = oBaoCaoChiTeuDVKHQuery.TuNgay.Value.ToString("dd-MM-yyyy");
                Nam = oBaoCaoChiTeuDVKHQuery.TuNgay.Value.ToString("yyyy");
            }
            else if (oBaoCaoChiTeuDVKHQuery.TuNgay.HasValue && oBaoCaoChiTeuDVKHQuery.TuNgay.Value.ToString("dd-MM-yyyy") == DateTime.Now.ToString("dd-MM-yyyy"))
            {
                TuNgay = "ALL";
                Nam = DateTime.Now.ToString("yyyy");
            }
            else
            {
                TuNgay = "ALL";
                Nam = DateTime.Now.ToString("yyyy");
            }
            if (oBaoCaoChiTeuDVKHQuery.DenNgay.HasValue)
            {
                DenNgay = oBaoCaoChiTeuDVKHQuery.DenNgay.Value.ToString("dd-MM-yyyy");
            }
            else if (oBaoCaoChiTeuDVKHQuery.DenNgay.HasValue && oBaoCaoChiTeuDVKHQuery.DenNgay.Value.ToString("dd-MM-yyyy") == DateTime.Now.ToString("dd-MM-yyyy"))
            {
                DenNgay = "ALL";
            }
            else
            {
                DenNgay = "ALL";
            }
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + LoaiDV + "/2013/" + PhanLoai + "/" + Nam + "/" + TuNgay + "/" + DenNgay + "/" + TuSo + "/" + DenSo + "/" + PhanLoaiVB + "/" + KieuGuiNhan;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapDangKyGiuSoVanBanDiJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(SoVanBanDiJson),
                    new Dictionary<string, string>
                    {
                        {"STT", "STT"},
                        {"SoKyHieuVanBan", "KY_HIEU"},
                        {"NgayThangVanBan", "NGAY_VB"},
                        {"TenLoaiVaTrichYeuVanBan", "TRICH_YEU"},
                        {"NguoiKy", "NGUOI_KY"},
                        {"NoiNhanVanBan", "DV_NHAN"},
                        {"GhiChu", "GHI_CHU"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<SoVanBanDiJson>>(model.data, settings);
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                Title = "SỔ VĂN BẢN ĐI",
                recordsTotal = oData.Count
            };

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoChiTeuDVKHQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoChiTeuDVKHQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "";
                    if (TuNgay == "ALL" && DenNgay == "ALL")
                    {
                        Tungaydenngay += "Từ Số: ";
                        if (TuSo != null)
                        {
                            Tungaydenngay += oBaoCaoChiTeuDVKHQuery.TuSo.ToString();
                        }
                        Tungaydenngay += " - đến số ";
                        if (DenSo != null)
                        {
                            Tungaydenngay += oBaoCaoChiTeuDVKHQuery.DenSo.ToString();
                        }

                    }
                    else
                    {
                        Tungaydenngay += "Từ ngày: ";
                        if (oBaoCaoChiTeuDVKHQuery.TuNgay.HasValue)
                            Tungaydenngay += oBaoCaoChiTeuDVKHQuery.TuNgay.Value.ToString("dd/MM/yyyy");
                        Tungaydenngay += " - đến ngày ";
                        if (oBaoCaoChiTeuDVKHQuery.DenNgay.HasValue)
                            Tungaydenngay += oBaoCaoChiTeuDVKHQuery.DenNgay.Value.ToString("dd/MM/yyyy");
                    }


                    lstValues.Add(new ClsExcel(2, 0, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 0, 0));
                    lstValues.Add(new ClsExcel(3, 0, "Đơn vị : " + oBaoCaoChiTeuDVKHQuery.DonViText, StyleExcell.TextDatetime, false, 12, false, 0, 0));
                    int rowStart = 7;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.SoKyHieuVanBan, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.NgayThangVanBan, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.TenLoaiVaTrichYeuVanBan, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.NguoiKy, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.NoiNhanVanBan, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.GhiChu != "NULL" ? item.GhiChu : "", StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }
                    lstValues.Add(new ClsExcel(4, 6, "Có tổng số: " + oData.Count + " bản ghi", StyleExcell.TextRight, false, 12, false, 4, 4));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "SoVanBanDi_" + DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_MucLucHoSo()
        {
            MucLucHoSoQuery oBaoCaoChiTeuDVKHQuery = new MucLucHoSoQuery(HttpContext.Current.Request);

            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/DOFFICE_ThongKeMucLucHoSo/276/01-01-2021/02-02-2021";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_" + oBaoCaoChiTeuDVKHQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            string loaidv = oBaoCaoChiTeuDVKHQuery.LoaiDV;
            string TuNgay = oBaoCaoChiTeuDVKHQuery.TuNgay.Value.ToString("dd-MM-yyyy");
            string DenNgay = oBaoCaoChiTeuDVKHQuery.DenNgay.Value.ToString("dd-MM-yyyy");
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + loaidv + "/" + TuNgay + "/" + DenNgay;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapMucLucHoSoJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(MucLucHoSoJson),
                    new Dictionary<string, string>
                    {
                        {"SoKyHieu", "KY_HIEU"},
                        {"NgayVanBan", "NGAY_VB"},
                        {"TrichYeuVanBan", "TRICH_YEU"},
                        {"TacGia", "TAC_GIA"},
                        {"NgayThemTaiLieu", "NGAY_THEM_TAILIEU"},
                        {"GhiChu", "GHI_CHU"},

                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<MucLucHoSoJson>>(model.data, settings);
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                Title = "MỤC LỤC HỒ SƠ",
                recordsTotal = oData.Count
            };

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoChiTeuDVKHQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoChiTeuDVKHQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Từ ngày: ";
                    if (oBaoCaoChiTeuDVKHQuery.TuNgay.HasValue)
                        Tungaydenngay += oBaoCaoChiTeuDVKHQuery.TuNgay.Value.ToString("dd/MM/yyyy");
                    Tungaydenngay += " - đến ngày ";
                    if (oBaoCaoChiTeuDVKHQuery.DenNgay.HasValue)
                        Tungaydenngay += oBaoCaoChiTeuDVKHQuery.DenNgay.Value.ToString("dd/MM/yyyy");
                    lstValues.Add(new ClsExcel(2, 0, Tungaydenngay, StyleExcell.TextDatetime));
                    lstValues.Add(new ClsExcel(3, 5, "Tổng số : " + oData.Count + " bản ghi", StyleExcell.TextCenter, false, 12, false, 3, 3));
                    int rowStart = 5;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.SoKyHieu, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.NgayVanBan, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.TrichYeuVanBan, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.TacGia, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.NgayThemTaiLieu, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.GhiChu != "NULL" ? item.GhiChu : "", StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }

                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "MucLucHoSo_" + DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_DangKyGiuSoVanBanDi()
        {
            DangKyGiuSoVanBanDiQuery oBaoCaoChiTeuDVKHQuery = new DangKyGiuSoVanBanDiQuery(HttpContext.Current.Request);
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/DOFFICE_BaoCaoSoXuatTamTaiLieu/276/01-01-2021/02-02-2021/ALL";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_" + oBaoCaoChiTeuDVKHQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            string LoaiDV = oBaoCaoChiTeuDVKHQuery.LoaiDV;
            string TrangThai = oBaoCaoChiTeuDVKHQuery.TrangThai;
            string TuNgay = oBaoCaoChiTeuDVKHQuery.TuNgay.Value.ToString("dd-MM-yyyy");
            string DenNgay = oBaoCaoChiTeuDVKHQuery.DenNgay.Value.ToString("dd-MM-yyyy");
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + LoaiDV + "/" + TuNgay + "/" + DenNgay + "/" + TrangThai;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapDangKyGiuSoVanBanDiJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(DangKyGiuSoVanBanDiJson),
                    new Dictionary<string, string>
                    {
                        {"SoDangKyGiu", "SO_DK"},
                        {"SoVanBan", "TEN_SO_VB"},
                        {"NguoiDangKy", "FIRSTNAME"},
                        {"BanPhong", "TEN_PB"},
                        {"ThoiGianDangKy", "TG_DKY"},
                        {"TinhTrang", "TT_CAP_SO"},
                        {"GhiChu", "GHI_CHU"},

                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<DangKyGiuSoVanBanDiJson>>(model.data, settings);
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                Title = "ĐĂNG KÝ GIỮ SỐ VĂN BẢN ĐI",
                Thang = oBaoCaoChiTeuDVKHQuery.TuNgay.Value.ToString("dd-MM-yyyy"),
                Nam = oBaoCaoChiTeuDVKHQuery.DenNgay.Value.ToString("dd-MM-yyyy"),
                recordsTotal = oData.Count
            };

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoChiTeuDVKHQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoChiTeuDVKHQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Từ ngày: ";
                    if (oBaoCaoChiTeuDVKHQuery.TuNgay.HasValue)
                        Tungaydenngay += oBaoCaoChiTeuDVKHQuery.TuNgay.Value.ToString("dd/MM/yyyy");
                    Tungaydenngay += " - đến ngày ";
                    if (oBaoCaoChiTeuDVKHQuery.DenNgay.HasValue)
                        Tungaydenngay += oBaoCaoChiTeuDVKHQuery.DenNgay.Value.ToString("dd/MM/yyyy");


                    lstValues.Add(new ClsExcel(1, 0, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 0, 0));
                    int rowStart = 3;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.SoDangKyGiu, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.SoVanBan, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.NguoiDangKy, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.BanPhong, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.ThoiGianDangKy, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.TinhTrang, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.GhiChu != "NULL" ? item.GhiChu : "", StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }

                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "DangKyGiuSoVanBanDi_" + DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
    }
}