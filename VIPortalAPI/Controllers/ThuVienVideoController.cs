﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ViPortal_Utils.Base;
using VIPortalAPP;



namespace VIPortalAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class ThuVienVideoController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA()
        {
            VideoDA oVideoDA = new VideoDA();
            VideoQuery oVideoQuery = new VideoQuery(HttpContext.Current.Request);
            oVideoQuery._ModerationStatus = 1;
            var oData = oVideoDA.GetListJson(oVideoQuery);
            DataGridRender oGrid = new DataGridRender(oData, oVideoQuery.Draw,
                oVideoDA.TongSoBanGhiSauKhiQuery);
            return oGrid;
        }
    }
}
