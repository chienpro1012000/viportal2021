﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ViPortal_Utils.Base;
using VIPortalAPI.BaseAPI;
using VIPortalAPP;
using ViPortalData.KSBoChuDe;

namespace VIPortalAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class VoteController : SIBaseAPI
    {
        /// <summary>
        /// /
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA()
        {
            KSBoChuDeDA oKSBoChuDeDA = new KSBoChuDeDA(UrlSite);
            KSBoChuDeQuery oKSBoChuDeQuery = new KSBoChuDeQuery(HttpContext.Current.Request);
            oKSBoChuDeQuery._ModerationStatus = 1;
            //oKSBoChuDeQuery.DMHienThi = true;
            var oData = oKSBoChuDeDA.GetListJsonSolr(oKSBoChuDeQuery);
            DataGridRender oGrid = new DataGridRender(oData, oKSBoChuDeQuery.Draw,
                oKSBoChuDeDA.TongSoBanGhiSauKhiQuery);
            return oGrid;
        }
    }
}
