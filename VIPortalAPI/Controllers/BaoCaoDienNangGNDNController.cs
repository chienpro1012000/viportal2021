﻿using Library_Shared.Utilities;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using ViPortal_Utils.Base;
using VIPortalAPI.BaseAPI;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.Lconfig;

namespace VIPortalAPI.Controllers
{
    public class BaoCaoDienNangGNDNController : SIBaseAPI
    {
        // GET: BaoCaoDienNangGNDN
        // GET: BaoCaoChiTeuDVKH
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_BaoCaoDienNangGNDN()
        {
            BaoCaoDienNangGNDNQuery oBaoCaoDienNangGNDNQuery = new BaoCaoDienNangGNDNQuery(HttpContext.Current.Request);

            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/CMIS_BCTHDienNangGiaoNhanDNCDV";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBaoCaoDienNangGNDNQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            var thang = oBaoCaoDienNangGNDNQuery.Thang;
            var nam = oBaoCaoDienNangGNDNQuery.Nam;
            var bcDV = oBaoCaoDienNangGNDNQuery.MaDonVi;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI+"/"+ bcDV+ "/"+thang+"/"+nam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapBaoCaoDienNangGNDNJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(BaoCaoDienNangGNDNJson),
                    new Dictionary<string, string>
                    {
                        {"DVGiaoNhan", "TEN_DVGN"},
                        {"STT", "STT"},

                        {"DNNBinhThuong", "BTN"},
                        {"DNNCaoDiem", "CDN"},
                        {"DNNThapDiem", "TDN"},
                        {"DNNCto1Gia", "KTN"},
                       // {"DNNTongCong", "BTN"+"CDN"+"TDN"+"KTN"},

                        {"DNGBinhThuong", "BTG"},
                        {"DNGCaoDiem", "CDG"},
                        {"DNGThapDiem", "TDG"},
                        {"DNGCto1Gia", "KTG"},
                       // {"DNGTongCong", "BTG"+"CDG"+"TDG"+"KTG"},
                       
                        //{"DNTNBinhThuong", "KTG"},
                        //{"DNTNCaoDiem", "KTG"},
                        //{"DNTNThapDiem", "KTG"},
                        //{"DNTNTongCong", "KTG"},
                       
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };
            string donvi = "";
            if (bcDV == "all")
            {
                donvi = "";
            }
            else if (bcDV == "PD")
            {
                donvi = "Tổng Công ty Điện lực TP Hà Nội";
            }
            else if (bcDV == "PD0600")
            {
                donvi = "Công ty Điện lưc Thanh Trì";
            }
            else if (bcDV == "PD1400")
            {
                donvi = "Công ty Điện lực Long Biên";
            }
            else if (bcDV == "P")
            {
                donvi = "Tập đoàn Điện Lực Việt Nam";
            }
            else if (bcDV == "PD0700")
            {
                donvi = "Công ty Điện lực Gia Lâm";
            }
            else if (bcDV == "PD0800")
            {
                donvi = "Công ty Điện lực Đông Anh";
            }
            else if (bcDV == "PD0900")
            {
                donvi = "Công ty Điện lực Sóc Sơn";
            }
            else if (bcDV == "PD1800")
            {
                donvi = "Công ty Điện lực Chương Mỹ";
            }
            else if (bcDV == "PD2000")
            {
                donvi = "Công ty Điện lực Thường Tín";
            }
            else if (bcDV == "PD2400")
            {
                donvi = "Công ty Điện lực Mỹ Đức";
            }
            else if (bcDV == "PD2600")
            {
                donvi = "Công ty Điện lực Phúc Thọ";
            }
            else if (bcDV == "PD2500")
            {
                donvi = "Công ty Điện lực Phú Xuyên";
            }
            else if (bcDV == "PD2800")
            {
                donvi = "Công ty Điện lực Thanh Oai";
            }
            else if (bcDV == "PD2900")
            {
                donvi = "Công ty Điện lực Ứng Hòa";
            }
            else if (bcDV == "PD0100")
            {
                donvi = "Công ty Điện lực Hoàn Kiếm";
            }
            else if (bcDV == "PD1500")
            {
                donvi = "Công ty Điện lực Mê Linh";
            }
            else if (bcDV == "PD1700")
            {
                donvi = "Công ty Điện lực Sơn Tây ";
            }
            else if (bcDV == "PD1900")
            {
                donvi = "Công ty Điện lực Thạch Thất";
            }
            else if (bcDV == "PD2100")
            {
                donvi = "Công ty Điện lực Ba Vì";
            }
            else if (bcDV == "PD2200")
            {
                donvi = "Công ty Điện lực Đan Phượng";
            }
            else if (bcDV == "PD2300")
            {
                donvi = "Công ty Điện lực Hoài Đức";
            }
            else if (bcDV == "PD2700")
            {
                donvi = "Công ty Điện lực Quốc Oai ";
            }
            else if (bcDV == "PD0400")
            {
                donvi = "Công ty Điện lực Đống Đa ";
            }
            else if (bcDV == "PD0500")
            {
                donvi = "Công ty Điện lực Nam Từ Liêm ";
            }
            else if (bcDV == "PD0200")
            {
                donvi = "Công ty Điện lực Hai Bà Trưng ";
            }
            else if (bcDV == "PD1000")
            {
                donvi = "Công ty Điện lực Tây Hồ  ";
            }
            else if (bcDV == "PD1100")
            {
                donvi = "Công ty Điện lực Thanh Xuân ";
            }
            else if (bcDV == "PD1200")
            {
                donvi = "Công ty Điện lực Cầy giấy ";
            }
            else if (bcDV == "PD3000")
            {
                donvi = "Công ty Điện lực Bắc Từ Liêm ";
            }
            else if (bcDV == "PD1300")
            {
                donvi = "Công ty Điện lực Hoàng Mai ";
            }
            else if (bcDV == "PD0300")
            {
                donvi = "Công ty Điện lực Ba Đình ";
            }
            else if (bcDV == "PD1600")
            {
                donvi = "Công ty Điện lực Hà Đông ";
            };
          
            var oData = JsonConvert.DeserializeObject<List<BaoCaoDienNangGNDNJson>>(string1resultUP, settings);
            //chua check duoc dư lieu khi mà show len man hinh cai nào là cha va con
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tổng Công ty Điện lực TP Hà Nội",
                TieuDeTrai2 = donvi,
                Title = "ĐIỆN NĂNG GIAO NHẬN ĐẦU NGUỒN ",
                
                //NguoiKy = "VŨ THẾ THẮNG",
                Thang =thang,
                Nam =nam,
                DVTinh = "Đơn vị: kWh",
                NguoiLapBieu = CurentUser.Title,
                
                recordsTotal = oData.Count
            };

            
            var recordTotal = new BaoCaoDienNangGNDNJson("Tổng cộng");
            recordTotal.Style = 1;
            recordTotal.NotHasSTT = 1;
            foreach (BaoCaoDienNangGNDNJson eachRecord in oData)
            {
                try
                {
                    recordTotal.DNNBinhThuong = (double.Parse(recordTotal.DNNBinhThuong, System.Globalization.CultureInfo.InvariantCulture)
                                                + Math.Round(double.Parse(eachRecord.DNNBinhThuong, System.Globalization.CultureInfo.InvariantCulture))
                                            ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.DNNCaoDiem = (double.Parse(recordTotal.DNNCaoDiem, System.Globalization.CultureInfo.InvariantCulture)
                                                + Math.Round(double.Parse(eachRecord.DNNCaoDiem, System.Globalization.CultureInfo.InvariantCulture))
                                            ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.DNNThapDiem = (double.Parse(recordTotal.DNNThapDiem, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.DNNThapDiem, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.DNNCto1Gia = (double.Parse(recordTotal.DNNCto1Gia, System.Globalization.CultureInfo.InvariantCulture)
                                                + Math.Round(double.Parse(eachRecord.DNNCto1Gia, System.Globalization.CultureInfo.InvariantCulture))
                                            ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.DNGBinhThuong = (double.Parse(recordTotal.DNGBinhThuong, System.Globalization.CultureInfo.InvariantCulture)
                                    + Math.Round(double.Parse(eachRecord.DNGBinhThuong, System.Globalization.CultureInfo.InvariantCulture))
                                ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.DNGCaoDiem = (double.Parse(recordTotal.DNGCaoDiem, System.Globalization.CultureInfo.InvariantCulture)
                                + Math.Round(double.Parse(eachRecord.DNGCaoDiem, System.Globalization.CultureInfo.InvariantCulture))
                            ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.DNGThapDiem = (double.Parse(recordTotal.DNGThapDiem, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.DNGThapDiem, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.DNGCto1Gia = (double.Parse(recordTotal.DNGCto1Gia, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.DNGCto1Gia, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

            }

            oData.Add(recordTotal);



            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoDienNangGNDNQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoDienNangGNDNQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng: ";
                    if (thang.ToString() != null)
                        Tungaydenngay +=thang;
                    Tungaydenngay += "  Năm: ";
                    if (nam.ToString() != null)
                        Tungaydenngay += nam;
                    lstValues.Add(new ClsExcel(2, 7, Tungaydenngay, StyleExcell.TextDatetime, false, 12, true, 2, 2, 7, 10));
                  
                    string loaiBaoCao = "Loại báo cáo: ";
                    if (bcDV == "1")
                    {
                        loaiBaoCao += "Báo cáo tháng";

                    }
                    else if (bcDV == "2")
                    {
                        loaiBaoCao += "Báo cáo quý";
                    }
                    else if (bcDV == "3")
                    {
                        loaiBaoCao += "Báo cáo 6 tháng";
                    }
                    else if (bcDV == "4")
                    {
                        loaiBaoCao += "Báo cáo năm";
                    }
                    int rowStart = 7;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        item.DNNTongCong = (Convert.ToDouble(item.DNNBinhThuong) + Convert.ToDouble(item.DNNCaoDiem) + Convert.ToDouble(item.DNNThapDiem) + Convert.ToDouble(item.DNNCto1Gia)).ToString();
                        item.DNGTongCong = (Convert.ToDouble(item.DNGBinhThuong) + Convert.ToDouble(item.DNGCaoDiem) + Convert.ToDouble(item.DNGThapDiem) + Convert.ToDouble(item.DNGCto1Gia)).ToString();

                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.DVGiaoNhan, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.DNNBinhThuong, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.DNNCaoDiem, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.DNNThapDiem, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.DNNCto1Gia, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6,item.DNNTongCong, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.DNGBinhThuong, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.DNGCaoDiem, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.DNGThapDiem, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.DNGCto1Gia, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.DNGTongCong, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 12, ((Convert.ToDouble(item.DNNBinhThuong) - Convert.ToDouble(item.DNGBinhThuong)+ Convert.ToDouble(item.DNNCto1Gia)- Convert.ToDouble(item.DNGCto1Gia)).ToString()), StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 13, ((Convert.ToDouble(item.DNNCaoDiem)- Convert.ToDouble(item.DNGCaoDiem)).ToString()), StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 14,((Convert.ToDouble(item.DNNThapDiem)- Convert.ToDouble(item.DNGThapDiem)).ToString()), StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 15, ((Convert.ToDouble(item.DNNTongCong)- Convert.ToDouble(item.DNGTongCong)).ToString()), StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }
                    lstValues.Add(new ClsExcel(3, 14, "Có tổng số: " + oData.Count + " bản ghi", StyleExcell.TextRight, false, 12, true, 3, 3, 14, 15));
                    lstValues.Add(new ClsExcel(2, 0, donvi, StyleExcell.TextCenterBold, false, 12,false,2,2,14,17));
                   // lstValues.Add(new ClsExcel(4, 14, loaiBaoCao, StyleExcell.TextRight, false, 12, true, 4, 4, 14, 17));


                    lstValues.Add(new ClsExcel(++rowStart + 2, 1, "NGƯỜI LẬP BIỂU  ", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 2, 1, 3));
                    lstValues.Add(new ClsExcel(++rowStart + 2, 1, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 6, 1, 3));
                    lstValues.Add(new ClsExcel(rowStart + 7, 1, CurentUser.Title, StyleExcell.TextCenterBold, false, 12, true, rowStart + 7, rowStart + 7, 1, 3));

               
                    string ngaythangnam = "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    lstValues.Add(new ClsExcel(++rowStart -1 , 9, ngaythangnam, StyleExcell.TextDatetime, false, 12, true, rowStart -1, rowStart -1, 9, 14));
                    lstValues.Add(new ClsExcel(++rowStart -1, 9, "KT.TRƯỞNG BAN", StyleExcell.TextCenterBold, false, 12, true, rowStart -1, rowStart -1, 9, 14));
                    lstValues.Add(new ClsExcel(++rowStart -1 , 9, "PHÓ TRƯỞNG BAN", StyleExcell.TextCenterBold, false, 12, true, rowStart -1, rowStart -1, 9, 14));
                    lstValues.Add(new ClsExcel(++rowStart -1 , 9, "", StyleExcell.TextCenterBold, false, 12, true, rowStart -1, rowStart +2, 9, 14));
                    //lstValues.Add(new ClsExcel(rowStart + 1, 2, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 2, 1, 1));
                    //lstValues.Add(new ClsExcel(rowStart + 3, 9, dataGiaoDienRender.NguoiKy, StyleExcell.TextCenterBold, false, 12, true, rowStart +3, rowStart + 3, 9, 14));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "BaoCaoDienNangGNDN_Thang_" + thang+"_Nam_"+nam));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
    }
}