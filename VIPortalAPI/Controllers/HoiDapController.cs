﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using ViPortal_Utils.Base;
using ViPortalData.FAQThuongGap;
using ViPortalData.HoiDap;
using ViPortalData.TraLoiHoiDap;

namespace VIPortalAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    public class HoiDapController : BaseAPI.SIBaseAPI
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA()
        {
            HoiDapDA oHoiDapDA = new HoiDapDA(UrlSite, true);
            HoiDapQuery oHoiDapQuery = new HoiDapQuery(HttpContext.Current.Request);
            oHoiDapQuery._ModerationStatus = 1;
            var oData = oHoiDapDA.GetListJson(oHoiDapQuery);
            List<int> LstCauHoi = oData.Select(x => x.ID).ToList();
            if (LstCauHoi.Count > 0)
            {
                TraLoiHoiDapDA oTraLoiHoiDapDA = new TraLoiHoiDapDA(UrlSite);
                List<TraLoiHoiDapJson> lstCauTraLoi = oTraLoiHoiDapDA.GetListJson(new TraLoiHoiDapQuery()
                {
                    LstIDHoiDap = LstCauHoi,
                    _ModerationStatus = 1
                });
                //oData = oData.Select(x =>
                //{
                //    x.NoiDungTraLoi = lstCauTraLoi.FirstOrDefault(y => y.IDHoiDap.ID == x.ID)?.FAQNoiDungTraLoi;
                //    x.TieuDeTraLoi = lstCauTraLoi.FirstOrDefault(y => y.IDHoiDap.ID == x.ID)?.Title;
                //    x.FQNguoiTraLoi = lstCauTraLoi.FirstOrDefault(y => y.IDHoiDap.ID == x.ID)?.FQNguoiTraLoi;
                //    return x;
                //}).ToList();
                foreach (HoiDapJson item in oData)
                {
                    if (lstCauTraLoi.FindIndex(y => y.IDHoiDap.ID == item.ID) > -1)
                    {
                        item.NoiDungTraLoi = lstCauTraLoi.FirstOrDefault(y => y.IDHoiDap.ID == item.ID)?.FAQNoiDungTraLoi;
                        item.TieuDeTraLoi = lstCauTraLoi.FirstOrDefault(y => y.IDHoiDap.ID == item.ID)?.Title;
                        item.FQNguoiTraLoi = lstCauTraLoi.FirstOrDefault(y => y.IDHoiDap.ID == item.ID)?.FQNguoiTraLoi;
                    }
                }
            }

            DataGridRender oGrid = new DataGridRender(oData, oHoiDapQuery.Draw,
                oHoiDapDA.TongSoBanGhiSauKhiQuery);
            return oGrid;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction GuiCauHoi()
        {
            HoiDapItem oHoiDapItem = new HoiDapItem();
            oHoiDapItem.UpdateObject(HttpContext.Current.Request);
            HoiDapDA oHoiDapDA = new HoiDapDA(UrlSite, true);
            ResultAction oResultAction = new ResultAction();
            oHoiDapItem.FQNguoiHoi = new SPFieldLookupValue(CurentUser.ID, "");
            oHoiDapDA.UpdateObject<HoiDapItem>(oHoiDapItem);
            oResultAction = new ResultAction()
            {
                State = ActionState.Succeed,
                Message = "Bạn đã gửi câu hỏi thành công"
            };
            return oResultAction;
        }
    }
}
