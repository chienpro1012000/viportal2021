﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using ViPortal_Utils.Base;

namespace VIPortalAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class ManagerFileController : BaseAPI.SIBaseAPI
    {
        /// GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        /// <summary>
        /// /
        /// </summary>
        /// <returns></returns>
        /// 
        [System.Web.Http.HttpGet, System.Web.Http.HttpPost]
        public IHttpActionResult DoRequest ()
        {

            ResultAction oResult = new ResultAction();
            switch (doAction)
            {
                case "QUERYDATA":
                    oResult.OData = GetAllFolder();
                    return Json(oResult.OData);
                case "QUERYFILE":
                    oResult.OData = GetAllFile();
                    return Json(oResult.OData);
                case "UPDATE":
                    oResult = UPDATEFILE();
                    break;
                case "UPLOADFILEBASE":
                    oResult = UPLOADFILEBASE();
                    break;
            }
            return Json(oResult);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Urlfile"></param>
        /// <returns></returns>
        /// 
        [System.Web.Http.HttpPost]
        public ResultAction Deletefile(string Urlfile)
        {
            ResultAction oResultAction = new ResultAction();
            try
            {
                File.Delete(Urlfile);
                oResultAction.Message = "Xóa thành công";
            }
            catch (Exception ex)
            {
                oResultAction.State = ActionState.Error;
                oResultAction.Message = ex.Message;
            }
            return oResultAction;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GenerateFile(string Urlfile)
        {
            Urlfile = Urlfile.Replace("web1.config1", "web.config");
            Urlfile = Urlfile.Replace("Web1.config1", "Web.config");
            if (!string.IsNullOrEmpty(Urlfile))
            {
                using (FileStream fileStream = new FileStream(Urlfile, FileMode.Open))
                {
                    //contentdm
                    //var stream = new MemoryStream();
                    MemoryStream stream = new MemoryStream();
                    fileStream.CopyTo(stream);
                    //return ms.ToArray();
                    // processing the stream.

                    var result = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new ByteArrayContent(stream.ToArray())
                    };
                    result.Content.Headers.ContentDisposition =
                        new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                        {
                            FileName = Path.GetFileName(Urlfile)
                        };
                    result.Content.Headers.ContentType =
                        new MediaTypeHeaderValue("application/octet-stream");
                    fileStream.Close();
                    return result;
                }
            }else
            {
                return new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent("Không xác định path file") };

            }
        }
        public ResultAction UPLOADFILEBASE()
        {
            List<UploadFilesResult> resultUpLoadItems = new List<UploadFilesResult>();
            ResultAction objMsg = new ResultAction();
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["Noidung"]))
            {
                FileAttach lstFile = Newtonsoft.Json.JsonConvert.DeserializeObject<FileAttach>(HttpContext.Current.Request["Noidung"]);
                string fileServer = Guid.NewGuid() + Path.GetExtension(lstFile.Name);
                string pathServer = string.Format(@"{0}\{1}", @"Uploads\ajaxUpload\", fileServer);
                String pathFile = string.Format(@"{0}\{1}", HttpContext.Current.Server.MapPath(@"\"), pathServer);
                UploadFilesResult resultUpLoad = new UploadFilesResult();
                File.WriteAllBytes(pathFile, lstFile.DataFile);
                resultUpLoad.pathFile = fileServer;
                resultUpLoad.name = lstFile.Name;
                resultUpLoad.url = string.Format(@"{0}/{1}", @"/Uploads/ajaxUpload/", fileServer);
                resultUpLoad.size = lstFile.DataFile.Length;
                //write file ra ổ cứng.
                resultUpLoadItems.Add(resultUpLoad);
            }
            objMsg.OData = resultUpLoadItems;
            return objMsg;
        }
        public ResultAction UPDATEFILE()
        {
            // ResultAction oResultAction = new ResultAction();
            ResultAction objMsg = new ResultAction();
            List<FileAttach> ListFileAttachAdd = new List<FileAttach>();
            string path = string.Empty;
            try
            {
                path = HttpContext.Current.Request["PathFileUpload"];
                if (!string.IsNullOrEmpty(HttpContext.Current.Request["ThuMuc"]))
                {
                    string pathlocal = path + "\\" + HttpContext.Current.Request["ThuMuc"];
                    try
                    {
                        if (!Directory.Exists(pathlocal))
                            Directory.CreateDirectory(pathlocal);
                        objMsg.Message = "<b>Thêm mới thành công</b>";
                    }
                    catch (Exception ex)
                    {
                        // objMsg.State = ActionState.Error;
                        // objMsg.Message = ex.Message;
                    }
                }
                //lấy về danh sách file đính kèm
                if (!string.IsNullOrEmpty(HttpContext.Current.Request["listValueFileAttach"])) //listValueFileAttach1
                {
                    string strListFileAttach = HttpContext.Current.Request["listValueFileAttach"];
                    System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    List<FileAttachForm> ltsFileForm = oSerializer.Deserialize<List<FileAttachForm>>(strListFileAttach);
                    FileAttach fileAttach;
                    string filePath = "/Uploads/ajaxUpload/";
                    foreach (FileAttachForm fileForm in ltsFileForm)
                    {
                        try
                        {
                            fileAttach = new FileAttach();
                            fileAttach.Name = fileForm.FileName;
                            fileAttach.DataFile = clsFucUtils.ReadFile(HttpContext.Current.Server.MapPath(filePath + fileForm.FileServer));
                            ListFileAttachAdd.Add(fileAttach);
                        }
                        catch
                        {
                            // neu co loi khi add file dinh kem, thi bo di, khoong lam gi nua
                        }
                    }
                }

                if (ListFileAttachAdd.Count > 0)
                {
                    foreach (FileAttach FileItem in ListFileAttachAdd)
                    {
                        string pathlocal = path + "\\" + FileItem.Name;
                        try
                        {
                            File.WriteAllBytes(pathlocal, FileItem.DataFile);

                            objMsg.Message = "<b>Upload thành công</b>";
                        }
                        catch (Exception ex)
                        {
                            objMsg.State = ActionState.Error;
                            objMsg.Message = ex.Message;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objMsg.State = ActionState.Error;
                objMsg.Message = ex.ToString();
            }
            return objMsg;

        }
        public DataGridRender GetAllFile()
        {
            DataGridRender oGrid = null;
            List<ObjFIle> lsttemp = new List<ObjFIle>();
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["path"]))
            {
                DirectoryInfo dirInfo = new DirectoryInfo(HttpContext.Current.Request["path"]);

                FileInfo[] fileNames = dirInfo.GetFiles();
                lsttemp = fileNames.OrderByDescending(y => y.LastWriteTime).Select(x => new ObjFIle(x.Name, x.LastWriteTime, x.Length)).ToList();

            }
            oGrid = new DataGridRender(lsttemp, Convert.ToInt32(HttpContext.Current.Request["draw"]), lsttemp.Count);
            return oGrid;
        }

        public List<TreeViewItem> lstTree = new List<TreeViewItem>();
        public List<TreeViewItem> GetAllFolder()
        {
            // lay thong tin file webconfig

            ResultAction oResult = new ResultAction();
            lstTree = new List<TreeViewItem>();
            TreeViewItem oTreeViewItem = new TreeViewItem();
            string path = ConfigurationManager.AppSettings["PortalPath"];
            string lastFolderName = "";
            if (!string.IsNullOrEmpty(path))
            {
                lastFolderName = path.Substring(path.LastIndexOf("\\") + 1); //Path.GetFileName(Path.GetDirectoryName(path));
                oTreeViewItem.title = "EVN Portal";
                oTreeViewItem.key = path;
                oTreeViewItem.expanded = true;
                BuildTree(oTreeViewItem, path);
                lstTree.Add(oTreeViewItem);
            }

            oTreeViewItem = new TreeViewItem();
            path = ConfigurationManager.AppSettings["APIPath"];
            if (!string.IsNullOrEmpty(path))
            {
                lastFolderName = path.Substring(path.LastIndexOf("\\") + 1); //Path.GetFileName(Path.GetDirectoryName(path));
                oTreeViewItem.title = "VIPortalAPI";
                oTreeViewItem.key = path;
                oTreeViewItem.expanded = true;

                BuildTree(oTreeViewItem, path);
                lstTree.Add(oTreeViewItem);
            }
            //oResult.
            oResult.OData = lstTree;
            return lstTree;
        }
        public List<string> BlockFolder = new List<string>() { "Uploads", "App_Start", "App_Data", "obj", "Properties", "_forms" };
        private void BuildTree(TreeViewItem oTreeViewItem, string targetDirectory)
        {

            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
            foreach (string foder in subdirectoryEntries)
            {
                //C:\tfs\Possi - SP2013\Ossi VinhPhuc DVC34 V2\WebDev\bin
                string lastFolderName = foder.Substring(foder.LastIndexOf("\\") + 1); // Path.GetFileName(Path.GetDirectoryName(foder));
                if (BlockFolder.FindIndex(x => foder.Contains(x)) == -1)
                {
                    TreeViewItem oTreeViewTemp = new TreeViewItem();
                    oTreeViewTemp.key = foder;
                    oTreeViewTemp.title = lastFolderName;
                    oTreeViewItem.children.Add(oTreeViewTemp);
                    BuildTree(oTreeViewTemp, foder);
                }
            }
        }

    }
}