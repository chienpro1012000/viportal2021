﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using ViPortal_Utils.Base;
using VIPortalAPI.BaseAPI;
using VIPortalAPP;

namespace VIPortalAPI.Controllers
{
    /// <summary>
    /// Menuquantri API
    /// </summary>
    public class MenuQuanTriController : SIBaseAPI
    {
        MenuQuanTriDA oMenuQuanTriDA;
        private StringBuilder oHtmlMenu;
        // GET api/<controller>

        /// <summary>
        /// Ham lay menu cua cms
        /// </summary>
        /// <returns></returns>
        public string GETMENULEFTCMS()
        {
            oHtmlMenu = new StringBuilder();
            //khởi tạo all đi xem sao đã nhé.
            oMenuQuanTriDA = new MenuQuanTriDA();
            MenuQuanTriQuery oMenuQuanTriQuery = new MenuQuanTriQuery(HttpContext.Current.Request);
            oMenuQuanTriQuery.Length = 0;
            oMenuQuanTriQuery._ModerationStatus = 1;
            oMenuQuanTriQuery.Ascending = true;
            oMenuQuanTriQuery.FieldOrder = "MenuIndex";
            List<int> lstperUser = new List<int>();
            List<MenuQuanTriJson> oDataJsonMenu = new List<MenuQuanTriJson>();
            if (CurentUser.Permissions.Count > 0)
            {
                foreach (var item in CurentUser.Permissions)
                {
                    lstperUser.Add(item.ID);
                }
                if (lstperUser.Count > 0)
                {
                    oMenuQuanTriQuery.lstPermistion = lstperUser;
                }
                oDataJsonMenu = oMenuQuanTriDA.GetListJson(oMenuQuanTriQuery);
            }
            else
            {

            }
            //xử lý url + thêm urlsite cho nó ra dữ liệu
            //oDataJsonMenu = oDataJsonMenu.Select(x=>x.)
            //oDataJsonMenu = oDataJsonMenu.Select(x => { x.MenuLink = UrlSite + x.MenuLink; return x; }).ToList();

            BuildMenuDaCap(oDataJsonMenu, 0);
            return oHtmlMenu.ToString();
        }
        private void BuildMenuDaCap(List<MenuQuanTriJson> lstMenuQuanTriJson, int rootId = 0)
        {
            IEnumerable<MenuQuanTriJson> tempMenu = lstMenuQuanTriJson.Where(x => x.MenuParent.ID == rootId);
            foreach (MenuQuanTriJson oMenuQuanTriJson in tempMenu)
            {
                if (oMenuQuanTriJson.MenuParent.ID == 0)
                {
                    //<li class="nav-header">EXAMPLES</li>
                    oHtmlMenu.AppendFormat("<li class=\"nav-header\">{0}</li>", oMenuQuanTriJson.Title);
                    BuildMenuDaCap(lstMenuQuanTriJson, oMenuQuanTriJson.ID);
                }
                else if (string.IsNullOrEmpty(oMenuQuanTriJson.MenuLink))
                {
                    int CountMenuSub = lstMenuQuanTriJson.Count(x => x.MenuParent.ID == oMenuQuanTriJson.ID);
                    oHtmlMenu.AppendFormat("<li class=\"nav-item\"><a href=\"#\" class=\"nav-link\">{2}<p>{0}<i class=\"fas fa-angle-left right\"></i><span class=\"badge badge-info right\">{1}</span></p></a>", oMenuQuanTriJson.Title, CountMenuSub, string.IsNullOrEmpty(oMenuQuanTriJson.MenuIcon) ? "<i class=\"nav-icon fas fa-copy\"></i>" : oMenuQuanTriJson.MenuIcon);
                    //check neu có menu con thì gọi tiếp đệ quy ở đây rồi đóng form.
                    if (CountMenuSub > 0)
                    {
                        oHtmlMenu.Append("<ul class=\"nav nav-treeview\">");
                        BuildMenuDaCap(lstMenuQuanTriJson, oMenuQuanTriJson.ID);
                        oHtmlMenu.Append("</ul>");
                    }
                    oHtmlMenu.Append("</li>");
                }
                else
                {
                    oHtmlMenu.AppendFormat("<li class=\"nav-item\"><a href=\"{0}\" class=\"nav-link\">{2}<p>{1}</p></a></li>", UrlSite +  oMenuQuanTriJson.MenuLink, oMenuQuanTriJson.Title, string.IsNullOrEmpty(oMenuQuanTriJson.MenuIcon) ? "<i class=\"far fa-circle nav-icon\"></i>" : oMenuQuanTriJson.MenuIcon);
                }
            }
        }
        /// <summary>
        /// menu danh sách.
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA()
        {
            MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
            MenuQuanTriQuery oMenuQuanTriQuery = new MenuQuanTriQuery(HttpContext.Current.Request);
            if (!string.IsNullOrEmpty(UrlSite))
            {
                oMenuQuanTriQuery.MenuShowSubPortal = true;
            }
            else oMenuQuanTriQuery.MenuShowPortal = true;
            var oData = oMenuQuanTriDA.GetListJson(oMenuQuanTriQuery);
            //fix + urlsite nêu có
            if (!string.IsNullOrEmpty(UrlSite))
            {
                oData = oData.Select(x => { x.MenuLink = x.MenuLink.StartsWith("http") ? x.MenuLink : UrlSite + x.MenuLink; return x; }).ToList();
            }
            oData = oData.Where(x => x.Permissions.Count == 0 ||
            (x.Permissions.Count > 0 && CurentUser.Permissions.Count(y => x.Permissions.Select(z => z.ID).Contains(y.ID)) > 0)).ToList();


            DataGridRender oGrid = new DataGridRender(oData, oMenuQuanTriQuery.Draw,
                oMenuQuanTriDA.TongSoBanGhiSauKhiQuery);
            return oGrid;

        }
        /// <summary>
        /// get menu theo cấp
        /// </summary>
        /// <returns></returns>
        public List<TreeViewItem> GetMenuTree()
        {
            List<TreeViewItem> treeViewItems = new List<TreeViewItem>();
            oHtmlMenu = new StringBuilder();
            oMenuQuanTriDA = new MenuQuanTriDA(UrlSite);
            MenuQuanTriQuery oMenuQuanTriQuery = new MenuQuanTriQuery(HttpContext.Current.Request);
            oMenuQuanTriQuery.Length = 0;
            oMenuQuanTriQuery.Ascending = true;
            oMenuQuanTriQuery.FieldOrder = "MenuIndex";
            List<int> lstperUser = new List<int>();
            foreach (var item in CurentUser.Permissions)
            {
                lstperUser.Add(item.ID);
            }
            if (lstperUser.Count > 0)
            {
                oMenuQuanTriQuery.lstPermistion = lstperUser;
            }
            var oDataJsonMenu = oMenuQuanTriDA.GetListJson(oMenuQuanTriQuery);
            treeViewItems = BuildTree(oDataJsonMenu, 0);
            return treeViewItems;
        }
        private List<TreeViewItem> BuildTree(List<MenuQuanTriJson> lstMenuQuanTriJson, int idParent)
        {
            List<TreeViewItem> lsTreeViewItem = new List<TreeViewItem>();
            foreach (MenuQuanTriJson item in lstMenuQuanTriJson.Where(x => x.MenuParent.ID == idParent))
            {
                lsTreeViewItem.Add(new TreeViewItem(item.ID + "." + item.Title, item.MenuLink,
                BuildTree(lstMenuQuanTriJson, item.ID), false, true, false, false, item.ID.ToString()));
            }
            return lsTreeViewItem;
        }
    }
}