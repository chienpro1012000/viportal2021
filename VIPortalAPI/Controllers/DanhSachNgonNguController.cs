﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using ViPortal_Utils.Base;
using VIPortalAPI.BaseAPI;
using ViPortalData.DanhSachNgonNgu;


namespace VIPortalAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    ///  [ApiExplorerSettings(IgnoreApi = true)]
    ///  
    [ApiExplorerSettings(IgnoreApi = true)]
    public class DanhSachNgonNguController : SIBaseAPI
    {
        /// <summary>
        /// /
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA()
        {

            DanhSachNgonNguQuery oDanhSachNgonNguQuery = new DanhSachNgonNguQuery(HttpContext.Current.Request);
            DanhSachNgonNguDA oDanhSachNgonNguDA = new DanhSachNgonNguDA();
            oDanhSachNgonNguQuery._ModerationStatus = 1;
            var oData = oDanhSachNgonNguDA.GetListJson(oDanhSachNgonNguQuery);
            DataGridRender oGrid = new DataGridRender(oData, oDanhSachNgonNguQuery.Draw,
                oDanhSachNgonNguDA.TongSoBanGhiSauKhiQuery);
            return oGrid;

        }
    }
}