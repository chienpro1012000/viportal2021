﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ViPortal_Utils.Base;
using VIPortalAPI.BaseAPI;
using ViPortalData;
using ViPortalData.DMHinhAnh;
using ViPortalData.Lconfig;

namespace VIPortalAPI.Controllers
{

    /// <summary>
    /// API cho hình ảnh video
    /// </summary>
    public class HinhAnhVideoController : SIBaseAPI
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA()
        {
            //
            if (string.IsNullOrEmpty(UrlSite) && false)
            {
                DMHinhAnhQuery oDMHinhAnhQuery = new DMHinhAnhQuery(HttpContext.Current.Request);
                //nếu là site root thì lấy từ API.
                //https://apicskh.evnhanoi.com.vn/api/PublicImage/GetCategoryImage?numberOfCategory=100
                LconfigDA oLconfigDA = new LconfigDA();
                string APIHinhAnhVideo = oLconfigDA.GetListJson(new LconfigQuery() { ConfigType = "APIHinhAnhVideo" }).FirstOrDefault()?.ConfigValue;
                if (string.IsNullOrEmpty(APIHinhAnhVideo))
                    APIHinhAnhVideo = "https://apicskh.evnhanoi.com.vn";
                HttpClient client = new HttpClient();
                //var byteArray = Encoding.ASCII.GetBytes("EVNHANOI:Evnhanoi@123");
                //client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
                HttpResponseMessage response = client.GetAsync(APIHinhAnhVideo + "/api/PublicImage/GetCategoryImage?numberOfCategory=100").Result;
                response.EnsureSuccessStatusCode();
                string responseBody = response.Content.ReadAsStringAsync().Result;
                JObject jObject = JObject.Parse(responseBody);

                List<categoryImagesEVN> players = jObject["categoryImagesList"].ToObject<categoryImagesEVN[]>().ToList();
                List<DMHinhAnhJson> oData = players.Select(x => new DMHinhAnhJson()
                {
                    ImageNews = x.url,
                    Title = x.name,
                    ID = x.id,
                    DMSLAnh = x.total
                }).ToList();
                DataGridRender oGrid = new DataGridRender(oData, oDMHinhAnhQuery.Draw,
                   oData.Count);
                return oGrid;
            }
            else
            {
                DMHinhAnhDA oDMHinhAnhDA = new DMHinhAnhDA(UrlSite);
                DMHinhAnhQuery oDMHinhAnhQuery = new DMHinhAnhQuery(HttpContext.Current.Request);
                oDMHinhAnhQuery._ModerationStatus = 1;
                var oData = oDMHinhAnhDA.GetListJsonSolr(oDMHinhAnhQuery);
                DataGridRender oGrid = new DataGridRender(oData, oDMHinhAnhQuery.Draw,
                    oDMHinhAnhDA.TongSoBanGhiSauKhiQuery);
                return oGrid;
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataGridRender HINHANH_QUERYDATA()
        {
            HinhAnhQuery oHinhAnhQuery = new HinhAnhQuery(HttpContext.Current.Request);
            if (string.IsNullOrEmpty(UrlSite) && false)
            {
                var oData = new List<HinhAnhJson>();


                LconfigDA oLconfigDA = new LconfigDA();
                string APIHinhAnhVideo = oLconfigDA.GetListJson(new LconfigQuery() { ConfigType = "APIHinhAnhVideo" }).FirstOrDefault()?.ConfigValue;
                if (string.IsNullOrEmpty(APIHinhAnhVideo))
                    APIHinhAnhVideo = "https://apicskh.evnhanoi.com.vn";
                HttpClient client = new HttpClient();
                //var byteArray = Encoding.ASCII.GetBytes("EVNHANOI:Evnhanoi@123");
                //client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
                HttpResponseMessage response = client.GetAsync(APIHinhAnhVideo + $"/api/PublicImage/GetImage?categoryID={oHinhAnhQuery.Album}&pageIndex=1&pageSize=10000").Result;
                response.EnsureSuccessStatusCode();
                string responseBody = response.Content.ReadAsStringAsync().Result;
                JObject jObject = JObject.Parse(responseBody);

                List<ImagesEVN> players = jObject["imagesList"].ToObject<ImagesEVN[]>().ToList();
                oData = players.Select(x => new HinhAnhJson()
                {
                    ImageNews = x.url,
                    Title = Path.GetFileName(x.url)
                }).ToList();


                DataGridRender oGrid = new DataGridRender(oData, oHinhAnhQuery.Draw,
                    oData.Count);
                oGrid.Query = "";
                return oGrid;
            }
            else {
                DMHinhAnhDA oDanhMucHinhAnhDA = new DMHinhAnhDA(UrlSite);
                DMHinhAnhItem oDMHinhAnhItem = oDanhMucHinhAnhDA.GetByIdToObject<DMHinhAnhItem>(oHinhAnhQuery.Album);
                HinhAnhDA oHinhAnhDA = new HinhAnhDA(UrlSite);
                oHinhAnhQuery._ModerationStatus = 1;
                var oData = oHinhAnhDA.GetListJsonSolr(oHinhAnhQuery);
                if(oDMHinhAnhItem.ListFileAttach.Count > 0)
                {
                    oData.AddRange(oDMHinhAnhItem.ListFileAttach.Select(x => new HinhAnhJson()
                    {
                        Title = oDMHinhAnhItem.Title,
                        ImageNews = x.Url
                    }));
                }
                DataGridRender oGrid = new DataGridRender(oData, oHinhAnhQuery.Draw,
                    oHinhAnhDA.TongSoBanGhiSauKhiQuery);
                oGrid.Query = oHinhAnhDA.Queryreturn;
                return oGrid;
            }
        }
    }
}