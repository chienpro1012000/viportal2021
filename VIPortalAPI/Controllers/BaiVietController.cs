﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description; 
using ViPortal_Utils.Base;
using VIPortalAPP;
using ViPortalData;

namespace VIPortalAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    public class BaiVietController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA()
        {
            BaiVietDA oBaiVietDA = new BaiVietDA();
            BaiVietQuery oBaiVietQuery = new BaiVietQuery(HttpContext.Current.Request);
            oBaiVietQuery._ModerationStatus = 1;
            var oData = oBaiVietDA.GetListJson(oBaiVietQuery);
            DataGridRender oGrid = new DataGridRender(oData, oBaiVietQuery.Draw,
                oBaiVietDA.TongSoBanGhiSauKhiQuery);
            return oGrid;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA_MANGXAHOI()
        {
            MangXaHoiDA oBaiVietDA = new MangXaHoiDA();
            MangXaHoiQuery oBaiVietQuery = new MangXaHoiQuery(HttpContext.Current.Request);
            oBaiVietQuery._ModerationStatus = 1;
            var oData = oBaiVietDA.GetListJson(oBaiVietQuery);
            DataGridRender oGrid = new DataGridRender(oData, oBaiVietQuery.Draw,
                oBaiVietDA.TongSoBanGhiSauKhiQuery);
            return oGrid;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction AddBaiVienMangXaHoi()
        {
            ResultAction oResultAction = new ResultAction();
            try
            {

                MangXaHoiItem oBaiVietItem = new MangXaHoiItem();
                oBaiVietItem.UpdateObject(HttpContext.Current.Request);
                MangXaHoiDA oBaiVietDA = new MangXaHoiDA();
                oBaiVietDA.UpdateObject<MangXaHoiItem>(oBaiVietItem);
                oResultAction = new ResultAction()
                {
                    State = ActionState.Succeed,
                    OData = oBaiVietItem,
                    Message = "Thêm mới bài viết thành công"
                };
            }
            catch (Exception ex)
            {
                oResultAction = new ResultAction()
                {
                    State = ActionState.Error,
                    Message = ex.Message + ex.StackTrace
                };
            }
            return oResultAction;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction Like()
        {
            ResultAction oResultAction = new ResultAction();
            try
            {
                MangXaHoiDA oBaiVietDA = new MangXaHoiDA();
                MangXaHoiItem oBaiVietItem = new MangXaHoiItem();
                //hứng và get bài viết
                int BaiViet = Convert.ToInt32(HttpContext.Current.Request["BaiViet"]);
                int CurrentUserID = Convert.ToInt32(HttpContext.Current.Request["CurrentUserID"]);
                string CurrentUser = HttpContext.Current.Request["CurrentUser"];                
                oBaiVietItem = oBaiVietDA.GetByIdToObject<MangXaHoiItem>(BaiViet);
                //change và cập nhật dữ liệu bài viết
                Dictionary<string, object> lstUpdate = new Dictionary<string, object>();    
                lstUpdate.Add("LuotLike", oBaiVietItem.LuotLike += 1); 
                if (!string.IsNullOrEmpty(oBaiVietItem.CurrentUsertLike))
                {
                    lstUpdate.Add("CurrentUsertLike", oBaiVietItem.CurrentUsertLike += "," + CurrentUser);
                }
                else
                {
                    lstUpdate.Add("CurrentUsertLike", oBaiVietItem.CurrentUsertLike += CurrentUser);
                }
                oBaiVietDA.UpdateOneOrMoreField(BaiViet, lstUpdate,true);
                //oBaiVietDA.UpdateObject<MangXaHoiItem>(oBaiVietItem);
                LUserDA oLUserDA = new LUserDA();
                Dictionary<string, object> lstUpdateLUser = new Dictionary<string, object>();
                lstUpdateLUser.Add("TrangThaiComment", 1);
                oLUserDA.UpdateOneOrMoreField(CurrentUserID, lstUpdateLUser,true);
                oResultAction = new ResultAction()
                {

                };
            }
            catch (Exception ex)
            {
            }
            return oResultAction;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction Dislike()
        {
            ResultAction oResultAction = new ResultAction();
            try
            {
                int BaiViet = Convert.ToInt32(HttpContext.Current.Request["BaiViet"]);
                int CurrentUserID = Convert.ToInt32(HttpContext.Current.Request["CurrentUserID"]);
                string CurrentUser = HttpContext.Current.Request["CurrentUser"];
                MangXaHoiDA oBaiVietDA = new MangXaHoiDA();
                MangXaHoiItem oBaiVietItem = new MangXaHoiItem();
                oBaiVietItem = oBaiVietDA.GetByIdToObject<MangXaHoiItem>(BaiViet);
                //change và save bài viết
                Dictionary<string, object> lstUpdate = new Dictionary<string, object>();
                
                lstUpdate.Add("LuotLike", oBaiVietItem.LuotLike - 1);
                //oBaiVietItem.LuotLike = oBaiVietItem.LuotLike - 1;
                List<string> list = oBaiVietItem.CurrentUsertLike.Split(',').ToList();
                list.RemoveAll(x => ((string)x) == CurrentUser);
                oBaiVietItem.CurrentUsertLike = string.Join(", ", list.Select(s => string.Format("{0}", s)));
                lstUpdate.Add("CurrentUsertLike", oBaiVietItem.CurrentUsertLike);
                oBaiVietDA.UpdateOneOrMoreField(BaiViet, lstUpdate, true);

                LUserDA oLUserDA = new LUserDA();               
                Dictionary<string, object> lstUpdateLUser = new Dictionary<string, object>();
                lstUpdateLUser.Add("TrangThaiComment", 0);
                oLUserDA.UpdateOneOrMoreField(CurrentUserID, lstUpdateLUser, true);
                oResultAction = new ResultAction()
                {

                };
            }
            catch (Exception ex)
            {
            }
            return oResultAction;
        }
    }
}
