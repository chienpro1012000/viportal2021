﻿using Library_Shared.Utilities;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using ViPortal_Utils.Base;
using VIPortalAPI.BaseAPI;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.Lconfig;

namespace VIPortalAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class BC1KHDVKHController : SIBaseAPI
    {

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// 

        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_ChiTieuKinhDoanh()
        {
            ChiTieuKinhDoanhQuery oChiTieuKinhDoanhQuery = new ChiTieuKinhDoanhQuery(HttpContext.Current.Request);
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/CMIS_BCTHCacChiTieuKinhDoanh/PD0200/11/2021";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oChiTieuKinhDoanhQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var nam = oChiTieuKinhDoanhQuery.Nam;
            var thang = oChiTieuKinhDoanhQuery.Thang;
            var bcDV = oChiTieuKinhDoanhQuery.MaDonVi;

            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/"+ bcDV + "/" + thang + "/" + nam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapChiTieuKinhDoanhJson>(responseBody);
            var string1resultUP = model.data1.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(ChiTieuKinhDoanhJson),
                    new Dictionary<string, string>
                    {
                        {"STT","STT" },
                       {"ChiTieu", "CHI_TIEU"},
                       {"DonViTinh", "DVI_TINH"},
                       {"Thang", "THANG"},
                       {"LuyKeNam", "LUY_KE"},
                       {"CungKy", "CUNG_KY"},
                       {"KHoach", "KE_HOACH"},
                    }
                }
            };
            
            string donvi;
            if (bcDV == "all")
            {
                donvi = "";
            }
            else if (bcDV == "PD")
            {
                donvi = "Tổng Công ty Điện lực TP Hà Nội";
            }
            else if (bcDV == "PD0600")
            {
                donvi = "Công ty Điện lưc Thanh Trì";
            }
            else if (bcDV == "PD1400")
            {
                donvi = "Công ty Điện lực Long Biên";
            }
            else if (bcDV == "P")
            {
                donvi = "Tập đoàn Điện Lực Việt Nam";
            }
            else if (bcDV == "PD0700")
            {
                donvi = "Công ty Điện lực Gia Lâm";
            }
            else if (bcDV == "PD0800")
            {
                donvi = "Công ty Điện lực Đông Anh";
            }
            else if (bcDV == "PD0900")
            {
                donvi = "Công ty Điện lực Sóc Sơn";
            }
            else if (bcDV == "PD1800")
            {
                donvi = "Công ty Điện lực Chương Mỹ";
            }
            else if (bcDV == "PD2000")
            {
                donvi = "Công ty Điện lực Thường Tín";
            }
            else if (bcDV == "PD2400")
            {
                donvi = "Công ty Điện lực Mỹ Đức";
            }
            else if (bcDV == "PD2600")
            {
                donvi = "Công ty Điện lực Phúc Thọ";
            }
            else if (bcDV == "PD2500")
            {
                donvi = "Công ty Điện lực Phú Xuyên";
            }
            else if (bcDV == "PD2800")
            {
                donvi = "Công ty Điện lực Thanh Oai";
            }
            else if (bcDV == "PD2900")
            {
                donvi = "Công ty Điện lực Ứng Hòa";
            }
            else if (bcDV == "PD0100")
            {
                donvi = "Công ty Điện lực Hoàn Kiếm";
            }
            else if (bcDV == "PD1500")
            {
                donvi = "Công ty Điện lực Mê Linh";
            }
            else if (bcDV == "PD1700")
            {
                donvi = "Công ty Điện lực Sơn Tây ";
            }
            else if (bcDV == "PD1900")
            {
                donvi = "Công ty Điện lực Thạch Thất";
            }
            else if (bcDV == "PD2100")
            {
                donvi = "Công ty Điện lực Ba Vì";
            }
            else if (bcDV == "PD2200")
            {
                donvi = "Công ty Điện lực Đan Phượng";
            }
            else if (bcDV == "PD2300")
            {
                donvi = "Công ty Điện lực Hoài Đức";
            }
            else if (bcDV == "PD2700")
            {
                donvi = "Công ty Điện lực Quốc Oai ";
            }
            else if (bcDV == "PD0400")
            {
                donvi = "Công ty Điện lực Đống Đa ";
            }
            else if (bcDV == "PD0500")
            {
                donvi = "Công ty Điện lực Nam Từ Liêm ";
            }
            else if (bcDV == "PD0200")
            {
                donvi = "Công ty Điện lực Hai Bà Trưng ";
            }
            else if (bcDV == "PD1000")
            {
                donvi = "Công ty Điện lực Tây Hồ  ";
            }
            else if (bcDV == "PD1100")
            {
                donvi = "Công ty Điện lực Thanh Xuân ";
            }
            else if (bcDV == "PD1200")
            {
                donvi = "Công ty Điện lực Cầy giấy ";
            }
            else if (bcDV == "PD3000")
            {
                donvi = "Công ty Điện lực Bắc Từ Liêm ";
            }
            else if (bcDV == "PD1300")
            {
                donvi = "Công ty Điện lực Hoàng Mai ";
            }
            else if (bcDV == "PD0300")
            {
                donvi = "Công ty Điện lực Ba Đình ";
            }
            else if (bcDV == "PD1600")
            {
                donvi = "Công ty Điện lực Hà Đông ";
            }
            else
            {
                donvi = "Tổng Công ty Điện lực TP Hà Nội ";
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };
            var oData = JsonConvert.DeserializeObject<List<ChiTieuKinhDoanhJson>>(string1resultUP, settings);
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1= "Tổng Công ty Điện lực TP Hà Nội",
                TieuDeTrai2= donvi,
                Title = "TỔNG HỢP KẾT QUẢ THỰC HIỆN CÁC CHỈ TIÊU KINH DOANH",
                LoaiBM = "KDDN 1",
                //NguoiKy = "Lê Ánh Dương",
                Thang = thang.ToString(),
                Nam = nam.ToString(),
                
                recordsTotal = oData.Count
            };

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oChiTieuKinhDoanhQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oChiTieuKinhDoanhQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng: ";
                    if (thang.ToString()!=null)
                        Tungaydenngay += thang;
                    Tungaydenngay += "  Năm: ";
                    if (nam.ToString()!=null)
                        Tungaydenngay += nam;
                    lstValues.Add(new ClsExcel(5, 0, Tungaydenngay, StyleExcell.TextDatetime, false, 12));
                    int rowStart = 10;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.ChiTieu, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.DonViTinh, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.Thang, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.LuyKeNam, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.CungKy, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.KHoach, StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }
                        
                    lstValues.Add(new ClsExcel(6, 6, "Có tổng số: " + oData.Count + " bản ghi", StyleExcell.TextRight, false, 12));
                    lstValues.Add(new ClsExcel(1, 0, donvi, StyleExcell.TextCenterBold, false, 12,false));

                    lstValues.Add(new ClsExcel(++rowStart, 0, "2. Phân tích đánh giá tình hình thực hiện các chỉ tiêu kinh doanh và nhiệm vụ đột xuất", StyleExcell.TextLeft, false, 12, true, rowStart, rowStart, 0, 6));
                    lstValues.Add(new ClsExcel(++rowStart, 0, "", StyleExcell.TextLeft, false, 12, true, rowStart, rowStart, 0, 6));
                    lstValues.Add(new ClsExcel(++rowStart, 0, "3. Các ý kiến đề xuất và kiến nghị với EVN", StyleExcell.TextLeft, false, 12, true, rowStart, rowStart, 0, 6));
                    lstValues.Add(new ClsExcel(++rowStart, 0, "", StyleExcell.TextLeft, false, 12, true, rowStart, rowStart, 0, 6));


                    string ngaythangnam = "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    lstValues.Add(new ClsExcel(++rowStart, 5, ngaythangnam, StyleExcell.TextDatetime, false, 12, true, rowStart, rowStart, 5, 6));
                    lstValues.Add(new ClsExcel(++rowStart, 5, "KT.TỔNG GIÁM ĐỐC", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart, 5, 6));
                    lstValues.Add(new ClsExcel(++rowStart, 5, "PHÓ TỔNG GIÁM ĐỐC", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart, 5, 6));
                    lstValues.Add(new ClsExcel(++rowStart, 5, "", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart + 4, 5, 6));
                    //lstValues.Add(new ClsExcel(rowStart + 1, 2, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 2, 1, 1));
                    //lstValues.Add(new ClsExcel(rowStart+5, 5, dataGiaoDienRender.NguoiKy, StyleExcell.TextCenterBold, false, 12, true, rowStart+5, rowStart+5, 5, 6));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "ChiTieuKinhDoanh_DV_" +bcDV+"_Thang_"+thang+"_Nam_"+nam ));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }

        //Ti le dien nang dung de truyen tai va phan phoi cac don vi
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_TTDienNangDungTTvaPPCuaDV()
        {
            TTDienNangDungTTvaPPCuaDVQuery oTTDienNangDungTTvaPPCuaDVQuery = new TTDienNangDungTTvaPPCuaDVQuery(HttpContext.Current.Request);
            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/CMIS_TLDNTruyenTaiVaPhanPhoiCuaCDV";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oTTDienNangDungTTvaPPCuaDVQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var bcNam = oTTDienNangDungTTvaPPCuaDVQuery.SearchNam;
            var bcThang = oTTDienNangDungTTvaPPCuaDVQuery.SearchThang;
            var LoaiBaoCao = oTTDienNangDungTTvaPPCuaDVQuery.LoaiBaoCao;
            var bcDV = oTTDienNangDungTTvaPPCuaDVQuery.MaDonVi;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcDV + "/" + bcThang + "/" + bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapTTDienNangDungTTvaPPCuaDVJson>(responseBody);
            var string1resultUP = model.data1.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(TTDienNangDungTTvaPPCuaDVJson),
                    new Dictionary<string, string>
                    {
                        {"TenDonVi", "TEN_DONVI"},
                        {"DNNTongNhan", "TG_NHAN"},
                        {"DNNNhanBanThang", "NHAN_BT"},
                        {"DNNNhanKV", "NHAN_KV"},
                        {"DienNangGiao", "GIAO_NB"},
                        {"DienNangGiaoNgay", "GIAO_NGAY"},
                        {"DienThuongPham", "TG_TPHAM"},
                        {"TTVaPPKWH", "TT_TTAI_PP"},
                        {"TTVaPPPhanTram", "DONG_TCONG"},  
                          // {"LuyKeNam", "DONG_TCONG"},
                       // {"SSluyKeNamvsCK", "DONG_TCONG"},
                        //{"SSluyKeNamvsKHG", "DONG_TCONG"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };
            string donvi = " ";
            if (bcDV == "all")
            {
                donvi += "";
            }
            else if (bcDV == "PD")
            {
                donvi += "Tổng Công ty Điện lực TP Hà Nội";
            }
            else if (bcDV == "PD0600")
            {
                donvi += "Công ty Điện lưc Thanh Trì";
            }
            else if (bcDV == "PD1400")
            {
                donvi += "Công ty Điện lực Long Biên";
            }
            else if (bcDV == "P")
            {
                donvi += "Tập đoàn Điện Lực Việt Nam";
            }
            else if (bcDV == "PD0700")
            {
                donvi += "Công ty Điện lực Gia Lâm";
            }
            else if (bcDV == "PD0800")
            {
                donvi += "Công ty Điện lực Đông Anh";
            }
            else if (bcDV == "PD0900")
            {
                donvi += "Công ty Điện lực Sóc Sơn";
            }
            else if (bcDV == "PD1800")
            {
                donvi += "Công ty Điện lực Chương Mỹ";
            }
            else if (bcDV == "PD2000")
            {
                donvi += "Công ty Điện lực Thường Tín";
            }
            else if (bcDV == "PD2400")
            {
                donvi += "Công ty Điện lực Mỹ Đức";
            }
            else if (bcDV == "PD2600")
            {
                donvi += "Công ty Điện lực Phúc Thọ";
            }
            else if (bcDV == "PD2500")
            {
                donvi += "Công ty Điện lực Phú Xuyên";
            }
            else if (bcDV == "PD2800")
            {
                donvi += "Công ty Điện lực Thanh Oai";
            }
            else if (bcDV == "PD2900")
            {
                donvi += "Công ty Điện lực Ứng Hòa";
            }
            else if (bcDV == "PD0100")
            {
                donvi += "Công ty Điện lực Hoàn Kiếm";
            }
            else if (bcDV == "PD1500")
            {
                donvi += "Công ty Điện lực Mê Linh";
            }
            else if (bcDV == "PD1700")
            {
                donvi += "Công ty Điện lực Sơn Tây ";
            }
            else if (bcDV == "PD1900")
            {
                donvi += "Công ty Điện lực Thạch Thất";
            }
            else if (bcDV == "PD2100")
            {
                donvi += "Công ty Điện lực Ba Vì";
            }
            else if (bcDV == "PD2200")
            {
                donvi += "Công ty Điện lực Đan Phượng";
            }
            else if (bcDV == "PD2300")
            {
                donvi += "Công ty Điện lực Hoài Đức";
            }
            else if (bcDV == "PD2700")
            {
                donvi += "Công ty Điện lực Quốc Oai ";
            }
            else if (bcDV == "PD0400")
            {
                donvi += "Công ty Điện lực Đống Đa ";
            }
            else if (bcDV == "PD0500")
            {
                donvi += "Công ty Điện lực Nam Từ Liêm ";
            }
            else if (bcDV == "PD0200")
            {
                donvi += "Công ty Điện lực Hai Bà Trưng ";
            }
            else if (bcDV == "PD1000")
            {
                donvi += "Công ty Điện lực Tây Hồ  ";
            }
            else if (bcDV == "PD1100")
            {
                donvi += "Công ty Điện lực Thanh Xuân ";
            }
            else if (bcDV == "PD1200")
            {
                donvi += "Công ty Điện lực Cầy giấy ";
            }
            else if (bcDV == "PD3000")
            {
                donvi += "Công ty Điện lực Bắc Từ Liêm ";
            }
            else if (bcDV == "PD1300")
            {
                donvi += "Công ty Điện lực Hoàng Mai ";
            }
            else if (bcDV == "PD0300")
            {
                donvi += "Công ty Điện lực Ba Đình ";
            }
            else if (bcDV == "PD1600")
            {
                donvi += "Công ty Điện lực Hà Đông ";
            }
            else
            {
                donvi += "Tổng Công ty Điện lực TP Hà Nội ";
            };
            var oData = JsonConvert.DeserializeObject<List<TTDienNangDungTTvaPPCuaDVJson>>(string1resultUP, settings);

            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tổng Công ty Điện lực TP Hà Nội",
                TieuDeTrai2 = donvi,
                Title = "TỈ LỆ ĐIỆN NĂNG DÙNG ĐỂ TRUYỀN TẢI VÀ PHÂN PHỐI CỦA CÁC ĐƠN VỊ  ",
                LoaiBM = "KDDN 5A",
                NguoiLapBieu=CurentUser.Title,
                Thang = bcThang.ToString(),
                Nam = bcNam.ToString(), 
                recordsTotal = oData.Count
            };

            var recordTotal = new TTDienNangDungTTvaPPCuaDVJson("Toàn TCT");
            recordTotal.Style = 1;
            recordTotal.NotHasSTT = 1;
            foreach (TTDienNangDungTTvaPPCuaDVJson eachRecord in oData)
            {
                recordTotal.DNNTongNhan = (double.Parse(recordTotal.DNNTongNhan, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.DNNTongNhan, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();

                recordTotal.DNNNhanBanThang = (double.Parse(recordTotal.DNNNhanBanThang, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.DNNNhanBanThang, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();

                recordTotal.DNNNhanKV = (double.Parse(recordTotal.DNNNhanKV, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.DNNNhanKV, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();

                recordTotal.DienNangGiao = (double.Parse(recordTotal.DienNangGiao, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.DienNangGiao, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();

                recordTotal.DienNangGiaoNgay = (double.Parse(recordTotal.DienNangGiaoNgay, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.DienNangGiaoNgay, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();

                recordTotal.DienThuongPham = (double.Parse(recordTotal.DienThuongPham, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.DienThuongPham, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();

                recordTotal.TTVaPPKWH = (double.Parse(recordTotal.TTVaPPKWH, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TTVaPPKWH, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();

            }

            oData.Add(recordTotal);

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oTTDienNangDungTTvaPPCuaDVQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oTTDienNangDungTTvaPPCuaDVQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng: ";
                    if (bcThang != null)
                        Tungaydenngay += bcThang.ToString();
                    Tungaydenngay += " - Năm: ";
                    if (bcNam != null)
                        Tungaydenngay += bcNam.ToString();
                   
                    string loaiBaoCao = "Loại báo cáo: ";
                    if (LoaiBaoCao == "1")
                    {
                        loaiBaoCao += "Báo cáo tháng";

                    }
                    else if (LoaiBaoCao == "2")
                    {
                        loaiBaoCao += "Báo cáo quý";
                    }
                    else if (LoaiBaoCao == "3")
                    {
                        loaiBaoCao += "Báo cáo 6 tháng";
                    }
                    else
                    {
                        loaiBaoCao += "Báo cáo năm";
                    }
                    lstValues.Add(new ClsExcel(3, 3, Tungaydenngay, StyleExcell.TextDatetime, false, 12, true, 3, 3, 3, 10));
                    lstValues.Add(new ClsExcel(2, 0, donvi, StyleExcell.TextCenterBold, false, 12, false, 4, 4, 4, 7));
                    lstValues.Add(new ClsExcel(4, 0, loaiBaoCao, StyleExcell.TextDatetime, false, 12, true, 4, 4, 0, 3));
                    int rowStart = 7;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.TenDonVi, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.DNNTongNhan, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.DNNNhanBanThang, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.DNNNhanKV, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.DienNangGiao, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.DienNangGiaoNgay, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.DienThuongPham, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.TTVaPPKWH, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.TTVaPPPhanTram, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.LuyKeNam, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.SSluyKeNamvsCK, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 12, item.SSluyKeNamvsKHG, StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }
                    lstValues.Add(new ClsExcel(4, 10, "Có tổng số: " + oData.Count + " bản ghi", StyleExcell.TextRight, false, 12, true, 4, 4, 10, 12));



                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "NGƯỜI LẬP BIỂU ", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 2, 0, 3));
                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 6, 0, 3));
                    lstValues.Add(new ClsExcel(rowStart + 7, 0, CurentUser.Title, StyleExcell.TextCenterBold, false, 12, true, rowStart + 7, rowStart + 7, 0, 3));

                    lstValues.Add(new ClsExcel(++rowStart, 4, " Tổ Tổng Hợp", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart, 4, 8));
                    lstValues.Add(new ClsExcel(++rowStart, 4, "", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart + 4, 4, 8));
                    lstValues.Add(new ClsExcel(rowStart + 5, 4, dataGiaoDienRender.ToTongHop, StyleExcell.TextCenterBold, false, 12, true, rowStart + 5, rowStart + 5, 4, 8));

                    string ngaythangnam = "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    lstValues.Add(new ClsExcel(++rowStart - 4, 9, ngaythangnam, StyleExcell.TextDatetime, false, 12, true, rowStart - 4, rowStart - 4, 9, 12));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 9, "KT.TRƯỞNG BAN", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart - 4, 9, 12));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 9, "PHÓ TRƯỞNG BAN", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart - 4, 9, 12));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 9, "", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart, 9, 12));
                    //lstValues.Add(new ClsExcel(rowStart + 1, 2, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 2, 1, 1));
                    lstValues.Add(new ClsExcel(rowStart + 1, 9, dataGiaoDienRender.NguoiKy, StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 1, 9, 12));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "TTDienNangDungTTvaPPCuaDV_DV_" + bcDV + "_Thang_" + bcThang + "_Nam_" + bcNam));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        } 
        // Tong hop so cong to cua cac don vi 
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_TongHopSoCongToCuaDV()
        {
            TongHopSoCongToCuaDVQuery oTongHopSoCongToCuaDVQuery = new TongHopSoCongToCuaDVQuery(HttpContext.Current.Request);
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/CMIS_TongHopSoCongToCuaCacDV";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oTongHopSoCongToCuaDVQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var nam = oTongHopSoCongToCuaDVQuery.Nam;
            var thang = oTongHopSoCongToCuaDVQuery.Thang;
            var maDonvi = oTongHopSoCongToCuaDVQuery.MaDonVi;

            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + maDonvi + "/" + thang + "/" + nam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapTongHopSoCongToCuaDVJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(TongHopSoCongToCuaDVJson),
                    new Dictionary<string, string>
                    {
                       {"TenDonVi", "TENDONVI"},
                       {"TSToanDonVi", "TONGCT"},
                       {"TSNoiBo1Pha", "NOIBO_1P"},
                       {"TSNoiBo3Pha", "NOIBO_3P"},
                       {"TSBanDien1Pha", "TONGCT1P"},
                       {"TSBanDien3Pha", "TONGCT3P"},
                       {"CTBanDien1PhaCoKhi", "SLCONGTOC1"},
                       {"CTBanDien1PhaDienTu1Gia", "SLCONGTOD1"},
                       {"CTBanDien1PhaDienTuNhieuGia", "SLCONGTODM"},
                       {"CTBanDien3PhaCoKhi", "SLCONGTOC3"},
                       {"CTBanDien3PhaDienTu1Gia", "SLCONGTOD3"},
                       {"CTBanDien3PhaDienTuNhieuGia", "SLCONGTODB"},
                       
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };
            string donvi;
            if (maDonvi == "all")
            {
                donvi = "";
            }
            else if (maDonvi == "PD")
            {
                donvi = "Tổng Công ty Điện lực TP Hà Nội";
            }
            else if (maDonvi == "PD0600")
            {
                donvi = "Công ty Điện lưc Thanh Trì";
            }
            else if (maDonvi == "PD1400")
            {
                donvi = "Công ty Điện lực Long Biên";
            }
            else if (maDonvi == "P")
            {
                donvi = "Tập đoàn Điện Lực Việt Nam";
            }
            else if (maDonvi == "PD0700")
            {
                donvi = "Công ty Điện lực Gia Lâm";
            }
            else if (maDonvi == "PD0800")
            {
                donvi = "Công ty Điện lực Đông Anh";
            }
            else if (maDonvi == "PD0900")
            {
                donvi = "Công ty Điện lực Sóc Sơn";
            }
            else if (maDonvi == "PD1800")
            {
                donvi = "Công ty Điện lực Chương Mỹ";
            }
            else if (maDonvi == "PD2000")
            {
                donvi = "Công ty Điện lực Thường Tín";
            }
            else if (maDonvi == "PD2400")
            {
                donvi = "Công ty Điện lực Mỹ Đức";
            }
            else if (maDonvi == "PD2600")
            {
                donvi = "Công ty Điện lực Phúc Thọ";
            }
            else if (maDonvi == "PD2500")
            {
                donvi = "Công ty Điện lực Phú Xuyên";
            }
            else if (maDonvi == "PD2800")
            {
                donvi = "Công ty Điện lực Thanh Oai";
            }
            else if (maDonvi == "PD2900")
            {
                donvi = "Công ty Điện lực Ứng Hòa";
            }
            else if (maDonvi == "PD0100")
            {
                donvi = "Công ty Điện lực Hoàn Kiếm";
            }
            else if (maDonvi == "PD1500")
            {
                donvi = "Công ty Điện lực Mê Linh";
            }
            else if (maDonvi == "PD1700")
            {
                donvi = "Công ty Điện lực Sơn Tây ";
            }
            else if (maDonvi == "PD1900")
            {
                donvi = "Công ty Điện lực Thạch Thất";
            }
            else if (maDonvi == "PD2100")
            {
                donvi = "Công ty Điện lực Ba Vì";
            }
            else if (maDonvi == "PD2200")
            {
                donvi = "Công ty Điện lực Đan Phượng";
            }
            else if (maDonvi == "PD2300")
            {
                donvi = "Công ty Điện lực Hoài Đức";
            }
            else if (maDonvi == "PD2700")
            {
                donvi = "Công ty Điện lực Quốc Oai ";
            }
            else if (maDonvi == "PD0400")
            {
                donvi = "Công ty Điện lực Đống Đa ";
            }
            else if (maDonvi == "PD0500")
            {
                donvi = "Công ty Điện lực Nam Từ Liêm ";
            }
            else if (maDonvi == "PD0200")
            {
                donvi = "Công ty Điện lực Hai Bà Trưng ";
            }
            else if (maDonvi == "PD1000")
            {
                donvi = "Công ty Điện lực Tây Hồ  ";
            }
            else if (maDonvi == "PD1100")
            {
                donvi = "Công ty Điện lực Thanh Xuân ";
            }
            else if (maDonvi == "PD1200")
            {
                donvi = "Công ty Điện lực Cầy giấy ";
            }
            else if (maDonvi == "PD3000")
            {
                donvi = "Công ty Điện lực Bắc Từ Liêm ";
            }
            else if (maDonvi == "PD1300")
            {
                donvi = "Công ty Điện lực Hoàng Mai ";
            }
            else if (maDonvi == "PD0300")
            {
                donvi = "Công ty Điện lực Ba Đình ";
            }
            else if (maDonvi == "PD1600")
            {
                donvi = "Công ty Điện lực Hà Đông ";
            }
            else
            {
                donvi = "Tổng Công ty Điện lực TP Hà Nội ";
            };
            var oData = JsonConvert.DeserializeObject<List<TongHopSoCongToCuaDVJson>>(string1resultUP, settings);
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tổng Công ty Điện lực TP Hà Nội",
                TieuDeTrai2 = donvi,
                Title = "TỔNG HỢP SỐ CÔNG TƠ CỦA CÁC ĐƠN VỊ  ",
                LoaiBM = "KDDN 6A",
                DVTinh = "Đơn vị: Công tơ",
                Thang = thang,
                Nam = nam,

                recordsTotal = oData.Count
            };

            var recordTotal = new TongHopSoCongToCuaDVJson("Toàn TCT");
            recordTotal.Style = 1;
            recordTotal.NotHasSTT = 1;

            foreach(TongHopSoCongToCuaDVJson eachRecord in oData)
            {
                // todo: tong howp du lieu
                recordTotal.TSToanDonVi = (double.Parse(recordTotal.TSToanDonVi, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TSToanDonVi, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                recordTotal.TSNoiBo1Pha = (double.Parse(recordTotal.TSNoiBo1Pha, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TSNoiBo1Pha, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                recordTotal.TSNoiBo3Pha = (double.Parse(recordTotal.TSNoiBo3Pha, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TSNoiBo3Pha, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                recordTotal.TSBanDien1Pha = (double.Parse(recordTotal.TSBanDien1Pha, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TSBanDien1Pha, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                recordTotal.TSBanDien3Pha = (double.Parse(recordTotal.TSBanDien3Pha, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TSBanDien3Pha, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                recordTotal.CTBanDien1PhaCoKhi = (double.Parse(recordTotal.CTBanDien1PhaCoKhi, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.CTBanDien1PhaCoKhi, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                recordTotal.CTBanDien1PhaDienTu1Gia = (double.Parse(recordTotal.CTBanDien1PhaDienTu1Gia, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.CTBanDien1PhaDienTu1Gia, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                recordTotal.CTBanDien1PhaDienTuNhieuGia = (double.Parse(recordTotal.CTBanDien1PhaDienTuNhieuGia, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.CTBanDien1PhaDienTuNhieuGia, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                recordTotal.CTBanDien3PhaCoKhi = (double.Parse(recordTotal.CTBanDien3PhaCoKhi, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.CTBanDien3PhaCoKhi, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                recordTotal.CTBanDien3PhaDienTu1Gia = (double.Parse(recordTotal.CTBanDien3PhaDienTu1Gia, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.CTBanDien3PhaDienTu1Gia, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                recordTotal.CTBanDien3PhaDienTuNhieuGia = (double.Parse(recordTotal.CTBanDien3PhaDienTuNhieuGia, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.CTBanDien3PhaDienTuNhieuGia, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
            }

            oData.Add(recordTotal);

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oTongHopSoCongToCuaDVQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oTongHopSoCongToCuaDVQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng: ";
                    if (thang.ToString() != null)
                        Tungaydenngay += thang;
                    Tungaydenngay += " - Năm: ";
                    if (nam.ToString()!=null)
                        Tungaydenngay += nam;
                    lstValues.Add(new ClsExcel(2, 4, Tungaydenngay, StyleExcell.TextDatetime, false, 12, true, 2, 2, 4, 9));
                    int rowStart = 9;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.TenDonVi, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.TSToanDonVi, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.TSNoiBo1Pha, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.TSNoiBo3Pha, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.TSBanDien1Pha, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.TSBanDien3Pha, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.CTBanDien1PhaCoKhi, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.CTBanDien1PhaDienTu1Gia, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.CTBanDien1PhaDienTuNhieuGia, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.CTBanDien3PhaCoKhi, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.CTBanDien3PhaDienTu1Gia, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 12, item.CTBanDien3PhaDienTuNhieuGia, StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }
                   
                    lstValues.Add(new ClsExcel(3, 11, "Có tổng số: " + oData.Count + " bản ghi", StyleExcell.TextRight, false, 12, true, 3, 3, 11, 12));
                    lstValues.Add(new ClsExcel(2, 0, donvi, StyleExcell.TextCenterBold, false, 12, false, 3, 3, 5, 10));



                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "NGƯỜI LẬP BIỂU ", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 2, 0, 2));
                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 6, 0, 2));
                    lstValues.Add(new ClsExcel(rowStart + 7, 0, CurentUser.Title, StyleExcell.TextCenterBold, false, 12, true, rowStart + 7, rowStart + 7, 0, 2));

                    lstValues.Add(new ClsExcel(++rowStart, 3, " Tổ Tổng Hợp", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart, 3, 7));
                    lstValues.Add(new ClsExcel(++rowStart, 3, "", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart + 4, 3, 7));
                    lstValues.Add(new ClsExcel(rowStart + 5, 3, dataGiaoDienRender.ToTongHop, StyleExcell.TextCenterBold, false, 12, true, rowStart + 5, rowStart + 5, 3, 7));

                    string ngaythangnam = "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    lstValues.Add(new ClsExcel(++rowStart - 4, 8, ngaythangnam, StyleExcell.TextDatetime, false, 12, true, rowStart - 4, rowStart - 4, 8, 12));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 8, "KT.TRƯỞNG BAN", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart - 4, 8, 12));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 8, "PHÓ TRƯỞNG BAN", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart - 4, 8, 12));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 8, "", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart, 8, 12));
                    //lstValues.Add(new ClsExcel(rowStart + 1, 2, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 2, 1, 1));
                    //lstValues.Add(new ClsExcel(rowStart + 1, 8, dataGiaoDienRender.NguoiKy, StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 1, 8, 12));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "TongHopSoCongToCuaDV_" +thang+"_Nam_"+nam ));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        //Tong hop so hop dong mua ban dien cua cac don vi
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_TongHopSoHDMuaBanDienCuaDV()
        {
            TongHopSoHDMuaBanDienCuaDVQuery oTongHopSoHDMuaBanDienCuaDVQuery = new TongHopSoHDMuaBanDienCuaDVQuery(HttpContext.Current.Request);

            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/CMIS_TongHopSoHopDongMBDCacDV/PD0200/5/2021";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oTongHopSoHDMuaBanDienCuaDVQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            var bcDV = oTongHopSoHDMuaBanDienCuaDVQuery.MaDonVi;
            var bcNam = oTongHopSoHDMuaBanDienCuaDVQuery.SearchNam;
            var bcThang = oTongHopSoHDMuaBanDienCuaDVQuery.SearchThang;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcDV + "/" + bcThang + "/" + bcNam ;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapTongHopSoHDMuaBanDienCuaDVJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(TongHopSoHDMuaBanDienCuaDVJson),
                    new Dictionary<string, string>
                    {
                        {"TenDonVi", "TENDONVI"},
                        {"TongSo", "TONGHD"},
                        {"HDSinhHoat", "HDDANSU"},
                        {"HDNgoaiSinhHoat", "HDKINHTE"},
                        {"ThanhPhanPTNNLNTS", "HDG_NN_LN_TS"},
                        {"ThanhPhanPTCNXD", "HDG_CN_XD"},
                        {"ThanhPhanPTKDV", "HDG_KD_DV"},
                        {"ThanhPhanPTQLTD", "HDG_QL_TD"},
                        {"ThanhPhanPTKhac", "HDG_KHAC"}
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };
            string donvi;
            if (bcDV == "all")
            {
                donvi = "";
            }
            else if (bcDV == "PD")
            {
                donvi = "Tổng Công ty Điện lực TP Hà Nội";
            }
            else if (bcDV == "PD0600")
            {
                donvi = "Công ty Điện lưc Thanh Trì";
            }
            else if (bcDV == "PD1400")
            {
                donvi = "Công ty Điện lực Long Biên";
            }
            else if (bcDV == "P")
            {
                donvi = "Tập đoàn Điện Lực Việt Nam";
            }
            else if (bcDV == "PD0700")
            {
                donvi = "Công ty Điện lực Gia Lâm";
            }
            else if (bcDV == "PD0800")
            {
                donvi = "Công ty Điện lực Đông Anh";
            }
            else if (bcDV == "PD0900")
            {
                donvi = "Công ty Điện lực Sóc Sơn";
            }
            else if (bcDV == "PD1800")
            {
                donvi = "Công ty Điện lực Chương Mỹ";
            }
            else if (bcDV == "PD2000")
            {
                donvi = "Công ty Điện lực Thường Tín";
            }
            else if (bcDV == "PD2400")
            {
                donvi = "Công ty Điện lực Mỹ Đức";
            }
            else if (bcDV == "PD2600")
            {
                donvi = "Công ty Điện lực Phúc Thọ";
            }
            else if (bcDV == "PD2500")
            {
                donvi = "Công ty Điện lực Phú Xuyên";
            }
            else if (bcDV == "PD2800")
            {
                donvi = "Công ty Điện lực Thanh Oai";
            }
            else if (bcDV == "PD2900")
            {
                donvi = "Công ty Điện lực Ứng Hòa";
            }
            else if (bcDV == "PD0100")
            {
                donvi = "Công ty Điện lực Hoàn Kiếm";
            }
            else if (bcDV == "PD1500")
            {
                donvi = "Công ty Điện lực Mê Linh";
            }
            else if (bcDV == "PD1700")
            {
                donvi = "Công ty Điện lực Sơn Tây ";
            }
            else if (bcDV == "PD1900")
            {
                donvi = "Công ty Điện lực Thạch Thất";
            }
            else if (bcDV == "PD2100")
            {
                donvi = "Công ty Điện lực Ba Vì";
            }
            else if (bcDV == "PD2200")
            {
                donvi = "Công ty Điện lực Đan Phượng";
            }
            else if (bcDV == "PD2300")
            {
                donvi = "Công ty Điện lực Hoài Đức";
            }
            else if (bcDV == "PD2700")
            {
                donvi = "Công ty Điện lực Quốc Oai ";
            }
            else if (bcDV == "PD0400")
            {
                donvi = "Công ty Điện lực Đống Đa ";
            }
            else if (bcDV == "PD0500")
            {
                donvi = "Công ty Điện lực Nam Từ Liêm ";
            }
            else if (bcDV == "PD0200")
            {
                donvi = "Công ty Điện lực Hai Bà Trưng ";
            }
            else if (bcDV == "PD1000")
            {
                donvi = "Công ty Điện lực Tây Hồ  ";
            }
            else if (bcDV == "PD1100")
            {
                donvi = "Công ty Điện lực Thanh Xuân ";
            }
            else if (bcDV == "PD1200")
            {
                donvi = "Công ty Điện lực Cầy giấy ";
            }
            else if (bcDV == "PD3000")
            {
                donvi = "Công ty Điện lực Bắc Từ Liêm ";
            }
            else if (bcDV == "PD1300")
            {
                donvi = "Công ty Điện lực Hoàng Mai ";
            }
            else if (bcDV == "PD0300")
            {
                donvi = "Công ty Điện lực Ba Đình ";
            }
            else if (bcDV == "PD1600")
            {
                donvi = "Công ty Điện lực Hà Đông ";
            }
            else
            {
                donvi = "Tổng Công ty Điện lực TP Hà Nội ";
            };
            var oData = JsonConvert.DeserializeObject<List<TongHopSoHDMuaBanDienCuaDVJson>>(string1resultUP, settings);

          
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tổng Công ty Điện lực TP Hà Nội",
                TieuDeTrai2 = donvi,
                Title = "TỔNG HỢP SỐ HỢP ĐỒNG MUA BÁN ĐIỆN CỦA CÁC ĐƠN VỊ   ",
                LoaiBM = "KDDN 7A",
                DVTinh = "Đơn vị:  Hợp đồng",
                Thang = bcThang.ToString(),
                Nam = bcNam.ToString(),

                recordsTotal = oData.Count
            };

            var recordTotal = new TongHopSoHDMuaBanDienCuaDVJson("Toàn TCT");
            recordTotal.Style = 1;
            recordTotal.NotHasSTT = 1;

            foreach (TongHopSoHDMuaBanDienCuaDVJson eachRecord in oData)
            {
                // todo: tong howp du lieu
                recordTotal.TongSo = (double.Parse(recordTotal.TongSo, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TongSo, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                recordTotal.HDSinhHoat = (double.Parse(recordTotal.HDSinhHoat, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.HDSinhHoat, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                recordTotal.HDNgoaiSinhHoat = (double.Parse(recordTotal.HDNgoaiSinhHoat, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.HDNgoaiSinhHoat, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                recordTotal.ThanhPhanPTNNLNTS = (double.Parse(recordTotal.ThanhPhanPTNNLNTS, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.ThanhPhanPTNNLNTS, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                recordTotal.ThanhPhanPTCNXD = (double.Parse(recordTotal.ThanhPhanPTCNXD, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.ThanhPhanPTCNXD, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();

                recordTotal.ThanhPhanPTKDV = (double.Parse(recordTotal.ThanhPhanPTKDV, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.ThanhPhanPTKDV, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                recordTotal.ThanhPhanPTQLTD = (double.Parse(recordTotal.ThanhPhanPTQLTD, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.ThanhPhanPTQLTD, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();

                recordTotal.ThanhPhanPTKhac = (double.Parse(recordTotal.ThanhPhanPTKhac, System.Globalization.CultureInfo.InvariantCulture)
                                           + Math.Round(double.Parse(eachRecord.ThanhPhanPTKhac, System.Globalization.CultureInfo.InvariantCulture))
                                       ).ToString();

            }
            oData.Add(recordTotal);

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oTongHopSoHDMuaBanDienCuaDVQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oTongHopSoHDMuaBanDienCuaDVQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng: ";
                    if (bcThang != null)
                        Tungaydenngay += bcThang.ToString();
                    Tungaydenngay += " - Năm ";
                    if (bcNam != null)
                        Tungaydenngay += bcNam.ToString();
                   
                    lstValues.Add(new ClsExcel(2, 0, donvi, StyleExcell.TextCenterBold, false, 12, false, 4, 4, 4, 7));
                    lstValues.Add(new ClsExcel(3, 3, Tungaydenngay, StyleExcell.TextDatetime, false, 12, true, 3, 3, 3, 7));
                    int rowStart = 8;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.TenDonVi, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.TongSo, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.HDSinhHoat, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.HDNgoaiSinhHoat, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.ThanhPhanPTNNLNTS, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.ThanhPhanPTCNXD, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.ThanhPhanPTKDV, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.ThanhPhanPTQLTD, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.ThanhPhanPTKhac, StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }
                    lstValues.Add(new ClsExcel(4, 8, "Có tổng số: " + oData.Count + " bản ghi", StyleExcell.TextRight, false, 12, true, 4, 4, 8, 9));



                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "NGƯỜI LẬP BIỂU ", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 2, 0, 2));
                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 6, 0, 2));
                    lstValues.Add(new ClsExcel(rowStart + 7, 0, CurentUser.Title, StyleExcell.TextCenterBold, false, 12, true, rowStart + 7, rowStart + 7, 0, 2));

                    lstValues.Add(new ClsExcel(++rowStart, 3, " Tổ Tổng Hợp", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart, 3, 6));
                    lstValues.Add(new ClsExcel(++rowStart, 3, "", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart + 4, 3, 6));
                    lstValues.Add(new ClsExcel(rowStart + 5, 3, dataGiaoDienRender.ToTongHop, StyleExcell.TextCenterBold, false, 12, true, rowStart + 5, rowStart + 5, 3, 6));

                    string ngaythangnam = "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    lstValues.Add(new ClsExcel(++rowStart - 4, 7, ngaythangnam, StyleExcell.TextDatetime, false, 12, true, rowStart - 4, rowStart - 4, 7, 9));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 7, "KT.TRƯỞNG BAN", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart - 4, 7, 9));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 7, "PHÓ TRƯỞNG BAN", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart - 4, 7, 9));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 7, "", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart, 7, 9));
                    //lstValues.Add(new ClsExcel(rowStart + 1, 2, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 2, 1, 1));
                    //lstValues.Add(new ClsExcel(rowStart + 1, 7, dataGiaoDienRender.NguoiKy, StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 1, 7, 9));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "TongHopSoHDMuaBanDienCuaDV_Thang_"+bcThang +"_Nam_"+bcNam));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_ChiTietBanDienTheoDTG()
        {
            ChiTietBanDienTheoDTGQuery oTongHopSoHDMuaBanDienCuaDVQuery = new ChiTietBanDienTheoDTGQuery(HttpContext.Current.Request);

            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/CMIS_ChiTietBanDienTheoDTG";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oTongHopSoHDMuaBanDienCuaDVQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            var bcDV = oTongHopSoHDMuaBanDienCuaDVQuery.MaDonVi;
            var bcNam = oTongHopSoHDMuaBanDienCuaDVQuery.SearchNam;
            var bcThang = oTongHopSoHDMuaBanDienCuaDVQuery.SearchThang;
            var bcLoaiBC = oTongHopSoHDMuaBanDienCuaDVQuery.LoaiBaoCao;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcDV + "/" + bcThang + "/" + bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapChiTietBanDienTheoDTGJson>(responseBody);
            var string1resultUP = model.data1.Replace("\\", "");    
            var map = new Dictionary<Type, Dictionary<string, string>>
            {

                {

                    typeof(ChiTietBanDienTheoDTGJson),
                    new Dictionary<string, string>
                    {
                        {"MaDT", "TENGIA"},
                        {"MucGia", "DONGIA"},
                        {"T_DienNang", "SANLUONG"},
                        {"T_TiTrong", "TYTRONG"},
                        {"T_ThanhTien", "SO_TIEN"},
                        {"LKN_DienNang", "SANLUONGLK"},
                        {"LKN_TiTrong", "TYTRONGLK"},
                        {"LKN_ThanhTien", "SOTIENLK"}
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<ChiTietBanDienTheoDTGJson>>(string1resultUP, settings);
            string donvi ;
            if (bcDV == "all")
            {
                donvi = " ";
            }
            else if (bcDV == "PD")
            {
                donvi = "Tổng Công ty Điện lực TP Hà Nội";
            }
            else if (bcDV == "PD0600")
            {
                donvi = "Công ty Điện lưc Thanh Trì";
            }
            else if (bcDV == "PD1400")
            {
                donvi = "Công ty Điện lực Long Biên";
            }
            else if (bcDV == "P")
            {
                donvi = "Tập đoàn Điện Lực Việt Nam";
            }
            else if (bcDV == "PD0700")
            {
                donvi = "Công ty Điện lực Gia Lâm";
            }
            else if (bcDV == "PD0800")
            {
                donvi = "Công ty Điện lực Đông Anh";
            }
            else if (bcDV == "PD0900")
            {
                donvi = "Công ty Điện lực Sóc Sơn";
            }
            else if (bcDV == "PD1800")
            {
                donvi = "Công ty Điện lực Chương Mỹ";
            }
            else if (bcDV == "PD2000")
            {
                donvi = "Công ty Điện lực Thường Tín";
            }
            else if (bcDV == "PD2400")
            {
                donvi = "Công ty Điện lực Mỹ Đức";
            }
            else if (bcDV == "PD2600")
            {
                donvi = "Công ty Điện lực Phúc Thọ";
            }
            else if (bcDV == "PD2500")
            {
                donvi = "Công ty Điện lực Phú Xuyên";
            }
            else if (bcDV == "PD2800")
            {
                donvi = "Công ty Điện lực Thanh Oai";
            }
            else if (bcDV == "PD2900")
            {
                donvi = "Công ty Điện lực Ứng Hòa";
            }
            else if (bcDV == "PD0100")
            {
                donvi = "Công ty Điện lực Hoàn Kiếm";
            }
            else if (bcDV == "PD1500")
            {
                donvi = "Công ty Điện lực Mê Linh";
            }
            else if (bcDV == "PD1700")
            {
                donvi = "Công ty Điện lực Sơn Tây ";
            }
            else if (bcDV == "PD1900")
            {
                donvi = "Công ty Điện lực Thạch Thất";
            }
            else if (bcDV == "PD2100")
            {
                donvi = "Công ty Điện lực Ba Vì";
            }
            else if (bcDV == "PD2200")
            {
                donvi = "Công ty Điện lực Đan Phượng";
            }
            else if (bcDV == "PD2300")
            {
                donvi = "Công ty Điện lực Hoài Đức";
            }
            else if (bcDV == "PD2700")
            {
                donvi = "Công ty Điện lực Quốc Oai ";
            }
            else if (bcDV == "PD0400")
            {
                donvi = "Công ty Điện lực Đống Đa ";
            }
            else if (bcDV == "PD0500")
            {
                donvi = "Công ty Điện lực Nam Từ Liêm ";
            }
            else if (bcDV == "PD0200")
            {
                donvi = "Công ty Điện lực Hai Bà Trưng ";
            }
            else if (bcDV == "PD1000")
            {
                donvi = "Công ty Điện lực Tây Hồ  ";
            }
            else if (bcDV == "PD1100")
            {
                donvi = "Công ty Điện lực Thanh Xuân ";
            }
            else if (bcDV == "PD1200")
            {
                donvi = "Công ty Điện lực Cầy giấy ";
            }
            else if (bcDV == "PD3000")
            {
                donvi = "Công ty Điện lực Bắc Từ Liêm ";
            }
            else if (bcDV == "PD1300")
            {
                donvi = "Công ty Điện lực Hoàng Mai ";
            }
            else if (bcDV == "PD0300")
            {
                donvi = "Công ty Điện lực Ba Đình ";
            }
            else if (bcDV == "PD1600")
            {
                donvi = "Công ty Điện lực Hà Đông ";
            }
            else
            {
                donvi = "Tổng Công ty Điện lực TP Hà Nội ";
            };

            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tổng Công ty Điện lực TP Hà Nội",
                TieuDeTrai2 = donvi,
                Title = "CHI TIẾT BÁN ĐIỆN THEO ĐỐI TƯỢNG GIÁ ",
                LoaiBM = "KDDN 9",
               // DVTinh = "Đơn vị:  Hợp đồng",
                Thang = bcThang.ToString(),
                Nam = bcNam.ToString(),

                recordsTotal = oData.Count
            };

            
            var recordTotal = new ChiTietBanDienTheoDTGJson("Cộng (A)");
            recordTotal.Style = 1;
            recordTotal.NotHasSTT = 1;

            //tong xxxx

            double tong = oData.Sum(x => double.Parse(x.T_DienNang));
            double tong1 = oData.Sum(x => double.Parse(x.LKN_DienNang));
            foreach (ChiTietBanDienTheoDTGJson eachRecord in oData)
            {
                
                
                try
                {
                    recordTotal.T_DienNang = (double.Parse(recordTotal.T_DienNang, System.Globalization.CultureInfo.InvariantCulture)
                                                + Math.Round(double.Parse(eachRecord.T_DienNang, System.Globalization.CultureInfo.InvariantCulture))
                                            ).ToString();
                }
                catch (Exception e) { }
                eachRecord.T_TiTrong = (double.Parse(eachRecord.T_DienNang)*100 / tong).ToString();

                eachRecord.LKN_TiTrong = (double.Parse(eachRecord.LKN_DienNang) * 100 / tong1).ToString();

                try
                {
                    recordTotal.T_ThanhTien = (double.Parse(recordTotal.T_ThanhTien, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.T_ThanhTien, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.LKN_DienNang = (double.Parse(recordTotal.LKN_DienNang, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.LKN_DienNang, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.LKN_ThanhTien = (double.Parse(recordTotal.LKN_ThanhTien, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.LKN_ThanhTien, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

            }
            oData.Add(recordTotal);

            var recordTotal1 = new ChiTietBanDienTheoDTGJson("Tổng (A+B+C) (cột tiền quy đổi VNĐ)");
            recordTotal1.Style = 1;
            recordTotal1.NotHasSTT = 1;

            foreach (ChiTietBanDienTheoDTGJson eachRecord1 in oData)
            {
               

                try
                {
                    recordTotal1.T_DienNang = recordTotal.T_DienNang;
                }
                catch (Exception e) { }



                try
                {
                    recordTotal1.T_TiTrong = (double.Parse(recordTotal1.T_TiTrong, System.Globalization.CultureInfo.InvariantCulture)
                                            + double.Parse(eachRecord1.T_TiTrong, System.Globalization.CultureInfo.InvariantCulture)
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal1.T_ThanhTien = recordTotal.T_ThanhTien;
                }
                catch (Exception e) { }

                try
                {
                    recordTotal1.LKN_DienNang = recordTotal.LKN_DienNang;
                }
                catch (Exception e) { }

                try
                {
                    recordTotal1.LKN_TiTrong = (double.Parse(recordTotal1.LKN_TiTrong, System.Globalization.CultureInfo.InvariantCulture)
                                            + double.Parse(eachRecord1.LKN_TiTrong, System.Globalization.CultureInfo.InvariantCulture)
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal1.LKN_ThanhTien = recordTotal.LKN_ThanhTien;
                }
                catch (Exception e) { }

            }
            oData.Add(recordTotal1);

            var recordTotal2 = new ChiTietBanDienTheoDTGJson("Giá bình quân(đ/kWh)");
            recordTotal2.Style = 1;
            recordTotal2.NotHasSTT = 1;

            foreach (ChiTietBanDienTheoDTGJson eachRecord1 in oData)
            {

                try
                {
                    recordTotal2.T_ThanhTien = (double.Parse(recordTotal1.T_ThanhTien) / double.Parse(recordTotal1.T_DienNang)).ToString();
                }
                catch (Exception e) { }

              

                try
                {
                    recordTotal2.LKN_ThanhTien = (double.Parse(recordTotal1.LKN_ThanhTien) / double.Parse(recordTotal1.LKN_DienNang)).ToString();
                    
                }
                catch (Exception e) { }

              

            }
            oData.Add(recordTotal2);


            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oTongHopSoHDMuaBanDienCuaDVQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oTongHopSoHDMuaBanDienCuaDVQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng: ";
                    if (bcThang != null)
                        Tungaydenngay += bcThang.ToString();
                    Tungaydenngay += "  Năm: ";
                    if (bcNam != null)
                        Tungaydenngay += bcNam.ToString();
                   
                    lstValues.Add(new ClsExcel(2, 0, donvi, StyleExcell.TextCenterBold, false, 12, false, 4, 4, 8, 10));
                    lstValues.Add(new ClsExcel(3, 3, Tungaydenngay, StyleExcell.TextDatetime, false, 12 ));
                    int rowStart = 12;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.MaDT, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.MucGia, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.T_DienNang, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.T_TiTrong, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.T_ThanhTien, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.LKN_DienNang, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.LKN_TiTrong, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.LKN_ThanhTien, StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }
                    lstValues.Add(new ClsExcel(5, 8, "Có tổng số: " + oData.Count + " bản ghi", StyleExcell.TextRight, false, 12, true, 5, 5, 8, 9));



                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "NGƯỜI LẬP BIỂU ", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 2, 0, 2));
                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 6, 0, 2));
                    lstValues.Add(new ClsExcel(rowStart + 7, 0, CurentUser.Title, StyleExcell.TextCenterBold, false, 12, true, rowStart + 7, rowStart + 7, 0, 2));

                    lstValues.Add(new ClsExcel(++rowStart, 3, " Tổ Tổng Hợp", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart, 3, 6));
                    lstValues.Add(new ClsExcel(++rowStart, 3, "", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart + 4, 3, 6));
                    lstValues.Add(new ClsExcel(rowStart + 5, 3, dataGiaoDienRender.ToTongHop, StyleExcell.TextCenterBold, false, 12, true, rowStart + 5, rowStart + 5, 3, 6));

                    string ngaythangnam = "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    lstValues.Add(new ClsExcel(++rowStart - 4, 7, ngaythangnam, StyleExcell.TextDatetime, false, 12, true, rowStart - 4, rowStart - 4, 7, 9));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 7, "KT.TRƯỞNG BAN", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart - 4, 7, 9));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 7, "PHÓ TRƯỞNG BAN", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart - 4, 7, 9));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 7, "", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart, 7, 9));
                    //lstValues.Add(new ClsExcel(rowStart + 1, 2, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 2, 1, 1));
                    //lstValues.Add(new ClsExcel(rowStart + 1, 7, dataGiaoDienRender.NguoiKy, StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 1, 7, 9));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "ChiTietBanDienTheoDoiTuongGia_Thang"+bcThang +"_Nam"+bcNam+"_DV:"+bcDV+"_"+ DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        } 
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        // chi tiet ban dien theo thanh phan phu tai
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_ChiTietBanDienTheoTTPhuTai()
        {
            ChiTietBanDienTheoTTPhuTaiQuery oChiTietBanDienTheoTTPhuTaiQuery = new ChiTietBanDienTheoTTPhuTaiQuery(HttpContext.Current.Request);
            
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/CMIS_ChiTietBanDienTheoTPPT";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oChiTietBanDienTheoTTPhuTaiQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var nam = oChiTietBanDienTheoTTPhuTaiQuery.Nam;
            var thang = oChiTietBanDienTheoTTPhuTaiQuery.Thang;
            var bcDV = oChiTietBanDienTheoTTPhuTaiQuery.MaDonVi;
            var LoaiBaoCao = oChiTietBanDienTheoTTPhuTaiQuery.LoaiBaoCao;

            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcDV + "/" + thang + "/" + nam ;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapChiTietBanDienTheoTTPhuTaiJson>(responseBody);
            var string2resultUP = model.data2.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(ChiTietBanDienTheoTTPhuTaiJson),
                    new Dictionary<string, string>
                    {
                        {"STT", "MA_TPPT" },
                       {"ThanhPhanPhuTai", "TEN_TPPT"},
                       {"ThangBCBieu1", "BT"},
                       {"ThangBCBieu2", "CD"},
                       {"ThangBCBieu3", "TD"},
                       {"ThangBCBanDien1LoaiG", "KT"},
                       {"ThangBCTienDien", "DTHU"},
                       {"LuyKeNBieu1", "BTLK"},
                       {"LuyKeNBieu2", "CDLK"},
                       {"LuyKeNBieu3", "TDLK"},
                       {"LuyKeNTienDien", "DTHULK"},
                       {"LuyKeNBanDien1LoaiG", "KTLK"},
                       //{"GiaTriThanhPhanPhuTai", "a"},
                       //{"TyLeThanhPhanPhuTai", "b"},
                     
                      
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var string1resultUP = model.data1.Replace("\\", "");
            List<ChiTietBanDienTheoTTPhuTaiJson> oData = new List<ChiTietBanDienTheoTTPhuTaiJson>();
            var oData1 = JsonConvert.DeserializeObject<List<ChiTietBanDienTheoTTPhuTaiJson>>(string1resultUP, settings);
            oData.AddRange(oData1);

            string TenDonVi = "Tổng cộng";
            var recordTotal = new ChiTietBanDienTheoTTPhuTaiJson(TenDonVi);
            recordTotal.Style = 1;
            recordTotal.NotHasSTT = 1;

            foreach (ChiTietBanDienTheoTTPhuTaiJson eachRecord in oData)
            {
                // todo: tong howp du lieu
                try {
                    recordTotal.ThangBCBieu1 = (double.Parse(recordTotal.ThangBCBieu1, System.Globalization.CultureInfo.InvariantCulture)
                                               + Math.Round(double.Parse(eachRecord.ThangBCBieu1, System.Globalization.CultureInfo.InvariantCulture))
                                           ).ToString();
                }
                catch(Exception e) { }
              
                try
                {
                    recordTotal.ThangBCBieu2 = (double.Parse(recordTotal.ThangBCBieu2, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.ThangBCBieu2, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }
                try
                {
                    recordTotal.ThangBCBieu3 = (double.Parse(recordTotal.ThangBCBieu3, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.ThangBCBieu3, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }
                try
                {
                    recordTotal.ThangBCBanDien1LoaiG = (double.Parse(recordTotal.ThangBCBanDien1LoaiG, System.Globalization.CultureInfo.InvariantCulture)
                                           + Math.Round(double.Parse(eachRecord.ThangBCBanDien1LoaiG, System.Globalization.CultureInfo.InvariantCulture))
                                       ).ToString();
                }
                catch (Exception e) { }
                try
                {
                    recordTotal.ThangBCTienDien = (double.Parse(recordTotal.ThangBCTienDien, System.Globalization.CultureInfo.InvariantCulture)
                                           + Math.Round(double.Parse(eachRecord.ThangBCTienDien, System.Globalization.CultureInfo.InvariantCulture))
                                       ).ToString();
                }
                catch (Exception e) { }
                try
                {
                    recordTotal.LuyKeNBieu1 = (double.Parse(recordTotal.LuyKeNBieu1, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.LuyKeNBieu1, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }
                try
                {
                    recordTotal.LuyKeNBieu2 = (double.Parse(recordTotal.LuyKeNBieu2, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.LuyKeNBieu2, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }
                try
                {
                    recordTotal.LuyKeNBieu3 = (double.Parse(recordTotal.LuyKeNBieu3, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.LuyKeNBieu3, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.LuyKeNBanDien1LoaiG = (double.Parse(recordTotal.LuyKeNBanDien1LoaiG, System.Globalization.CultureInfo.InvariantCulture)
                                           + Math.Round(double.Parse(eachRecord.LuyKeNBanDien1LoaiG, System.Globalization.CultureInfo.InvariantCulture))
                                       ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.LuyKeNTienDien = (double.Parse(recordTotal.LuyKeNTienDien, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.LuyKeNTienDien, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch(Exception e) { }

            }
            oData.Add(recordTotal);

       
            var oData2 = JsonConvert.DeserializeObject<List<ChiTietBanDienTheoTTPhuTaiJson>>(string2resultUP, settings);
            oData.AddRange(oData2);
            string donvi = "";
            if (bcDV == "all")
            {
                donvi += "";
            }
            else if (bcDV == "PD")
            {
                donvi += "Tổng Công ty Điện lực TP Hà Nội";
            }
            else if (bcDV == "PD0600")
            {
                donvi += "Công ty Điện lưc Thanh Trì";
            }
            else if (bcDV == "PD1400")
            {
                donvi += "Công ty Điện lực Long Biên";
            }
            else if (bcDV == "P")
            {
                donvi += "Tập đoàn Điện Lực Việt Nam";
            }
            else if (bcDV == "PD0700")
            {
                donvi += "Công ty Điện lực Gia Lâm";
            }
            else if (bcDV == "PD0800")
            {
                donvi += "Công ty Điện lực Đông Anh";
            }
            else if (bcDV == "PD0900")
            {
                donvi += "Công ty Điện lực Sóc Sơn";
            }
            else if (bcDV == "PD1800")
            {
                donvi += "Công ty Điện lực Chương Mỹ";
            }
            else if (bcDV == "PD2000")
            {
                donvi += "Công ty Điện lực Thường Tín";
            }
            else if (bcDV == "PD2400")
            {
                donvi += "Công ty Điện lực Mỹ Đức";
            }
            else if (bcDV == "PD2600")
            {
                donvi += "Công ty Điện lực Phúc Thọ";
            }
            else if (bcDV == "PD2500")
            {
                donvi += "Công ty Điện lực Phú Xuyên";
            }
            else if (bcDV == "PD2800")
            {
                donvi += "Công ty Điện lực Thanh Oai";
            }
            else if (bcDV == "PD2900")
            {
                donvi += "Công ty Điện lực Ứng Hòa";
            }
            else if (bcDV == "PD0100")
            {
                donvi += "Công ty Điện lực Hoàn Kiếm";
            }
            else if (bcDV == "PD1500")
            {
                donvi += "Công ty Điện lực Mê Linh";
            }
            else if (bcDV == "PD1700")
            {
                donvi += "Công ty Điện lực Sơn Tây ";
            }
            else if (bcDV == "PD1900")
            {
                donvi += "Công ty Điện lực Thạch Thất";
            }
            else if (bcDV == "PD2100")
            {
                donvi += "Công ty Điện lực Ba Vì";
            }
            else if (bcDV == "PD2200")
            {
                donvi += "Công ty Điện lực Đan Phượng";
            }
            else if (bcDV == "PD2300")
            {
                donvi += "Công ty Điện lực Hoài Đức";
            }
            else if (bcDV == "PD2700")
            {
                donvi += "Công ty Điện lực Quốc Oai ";
            }
            else if (bcDV == "PD0400")
            {
                donvi += "Công ty Điện lực Đống Đa ";
            }
            else if (bcDV == "PD0500")
            {
                donvi += "Công ty Điện lực Nam Từ Liêm ";
            }
            else if (bcDV == "PD0200")
            {
                donvi += "Công ty Điện lực Hai Bà Trưng ";
            }
            else if (bcDV == "PD1000")
            {
                donvi += "Công ty Điện lực Tây Hồ  ";
            }
            else if (bcDV == "PD1100")
            {
                donvi += "Công ty Điện lực Thanh Xuân ";
            }
            else if (bcDV == "PD1200")
            {
                donvi += "Công ty Điện lực Cầy giấy ";
            }
            else if (bcDV == "PD3000")
            {
                donvi += "Công ty Điện lực Bắc Từ Liêm ";
            }
            else if (bcDV == "PD1300")
            {
                donvi += "Công ty Điện lực Hoàng Mai ";
            }
            else if (bcDV == "PD0300")
            {
                donvi += "Công ty Điện lực Ba Đình ";
            }
            else if (bcDV == "PD1600")
            {
                donvi += "Công ty Điện lực Hà Đông ";
            }
            else
            {
                donvi += "Tổng Công ty Điện lực TP Hà Nội ";
            };
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tổng Công ty Điện lực TP Hà Nội",
                TieuDeTrai2 = donvi,
                Title = "CHI TIẾT BÁN ĐIỆN THEO THÀNH PHẦN PHỤ TẢI",
                LoaiBM = "KDDN 8A",
               // NguoiKy = "VŨ THẾ THẮNG",
                Thang = thang.ToString(),
                Nam = nam.ToString(),
                NguoiLapBieu = CurentUser.Title,
                //ToTongHop = "  NGUYỄN ĐỨC AN   ",

                recordsTotal = oData.Count
            };

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oChiTietBanDienTheoTTPhuTaiQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oChiTietBanDienTheoTTPhuTaiQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng: ";  
                    if (thang.ToString()!=null)
                        Tungaydenngay += thang;
                    Tungaydenngay += " -Năm: ";
                    if (nam.ToString()!=null)
                        Tungaydenngay +=nam;
                    lstValues.Add(new ClsExcel(2, 4, Tungaydenngay, StyleExcell.TextDatetime, false, 12, true, 2, 2, 4, 11));
                    int rowStart = 8;
                    int STT = 1;
                    string loaiBaoCao = "Loại báo cáo: ";
                    if (LoaiBaoCao == "1")
                    {
                        loaiBaoCao += "Báo cáo tháng";

                    }
                    else if (LoaiBaoCao == "2")
                    {
                        loaiBaoCao += "Báo cáo quý";
                    }
                    else if (LoaiBaoCao == "3")
                    {
                        loaiBaoCao += "Báo cáo 6 tháng";
                    }
                    else
                    {
                        loaiBaoCao += "Báo cáo năm"; 
                    }
                   
                    //set title
                    foreach (var item in oData)
                    {
                        var thangTongSL = double.Parse(item.ThangBCBieu1) + double.Parse(item.ThangBCBieu2) + double.Parse(item.ThangBCBieu3) + double.Parse(item.ThangBCBanDien1LoaiG);
                        var lkTongSL = double.Parse(item.LuyKeNBieu1) + double.Parse(item.LuyKeNBieu2) + double.Parse(item.LuyKeNBieu3) + double.Parse(item.LuyKeNBanDien1LoaiG);
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.ThanhPhanPhuTai, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, thangTongSL, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.ThangBCBieu1, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.ThangBCBieu2, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.ThangBCBieu3, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.ThangBCBanDien1LoaiG, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.ThangBCTienDien, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, lkTongSL, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.LuyKeNBieu1, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.LuyKeNBieu2, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.LuyKeNBieu3, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 12, item.LuyKeNBanDien1LoaiG, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 13, item.LuyKeNTienDien, StyleExcell.TextCenter));
                       // lstValues.Add(new ClsExcel(rowStart, 14, item.GiaTriThanhPhanPhuTai, StyleExcell.TextCenter));
                        //lstValues.Add(new ClsExcel(rowStart, 15, item.TyLeThanhPhanPhuTai, StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }
                    lstValues.Add(new ClsExcel(3, 12, "Có tổng số: " + oData.Count + " bản ghi", StyleExcell.TextRight, false, 12, true, 3, 3, 12, 13));
                    lstValues.Add(new ClsExcel(2, 0, donvi, StyleExcell.TextCenterBold, false, 12, false, 3, 3, 8,11));
                    lstValues.Add(new ClsExcel(2, 12, loaiBaoCao, StyleExcell.TextCenter, false, 12, true, 2, 2, 12, 13));


                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "NGƯỜI LẬP BIỂU ", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 2, 0, 3));
                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 6, 0, 3));
                    lstValues.Add(new ClsExcel(rowStart + 7, 0, CurentUser.Title, StyleExcell.TextCenterBold, false, 12, true, rowStart + 7, rowStart + 7, 0, 3));

                    lstValues.Add(new ClsExcel(++rowStart, 4, " Tổ Tổng Hợp", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart, 4, 8));
                    lstValues.Add(new ClsExcel(++rowStart, 4, "", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart + 4, 4, 8));
                    lstValues.Add(new ClsExcel(rowStart + 5, 4, dataGiaoDienRender.ToTongHop, StyleExcell.TextCenterBold, false, 12, true, rowStart + 5, rowStart + 5, 4, 8));

                    string ngaythangnam = "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    lstValues.Add(new ClsExcel(++rowStart - 4, 9, ngaythangnam, StyleExcell.TextDatetime, false, 12, true, rowStart - 4, rowStart - 4, 9, 13));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 9, "KT.TRƯỞNG BAN", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart - 4, 9, 13));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 9, "PHÓ TRƯỞNG BAN", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart - 4, 9, 13));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 9, "", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart, 9, 13));
                    //lstValues.Add(new ClsExcel(rowStart + 1, 2, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 2, 1, 1));
                    lstValues.Add(new ClsExcel(rowStart + 1, 9, dataGiaoDienRender.NguoiKy, StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 1, 9, 13));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "ChiTietBanDienTheoThanhPhanPhuTai_thang_" +thang+"_nam_"+nam+"_DV_"+ bcDV));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();     
                    }

                }
            }
            return dataGiaoDienRender;
        } 
        // chi tiet ban dien theo nganh nghe
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_ChiTietBanDienTheoNganhNghe()
        {
            ChiTietBanDienTheoNganhNgheQuery oChiTietBanDienTheoNganhNgheQuery = new ChiTietBanDienTheoNganhNgheQuery(HttpContext.Current.Request);
            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/CMIS_BCChiTietBanDienTheoNganhNghe";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oChiTietBanDienTheoNganhNgheQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var bcNam = oChiTietBanDienTheoNganhNgheQuery.SearchNam;
            var bcThang = oChiTietBanDienTheoNganhNgheQuery.SearchThang;
            var loaiBC = oChiTietBanDienTheoNganhNgheQuery.LoaiBaoCao;
            var bcDV = oChiTietBanDienTheoNganhNgheQuery.MaDonVi;
            
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/"+ bcDV + "/" + bcThang + "/" + bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapChiTietBanDienTheoNganhNgheJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(ChiTietBanDienTheoNganhNgheJson),
                    new Dictionary<string, string>
                    {
                        {"MaNN", "MA_NN"},
                        {"TenNN", "TEN_NN"},
                        {"SoHopDongMBD", "SO_HDONG"},
                        {"DienThuongPham", "SAN_LUONG"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<ChiTietBanDienTheoNganhNgheJson>>(string1resultUP, settings);
            /*  List<ChiTietBanDienTheoNganhNgheJson> oData = new List<ChiTietBanDienTheoNganhNgheJson>()
              {
                  new ChiTietBanDienTheoNganhNgheJson() {MaNN="00561531",TenNN = "Công ty Điện lực Hoàn Kiếm",SoHopDongMBD=984164,DienThuongPham=1556555469},
              };*/
            string donvi = "";
            if (bcDV == "all")
            {
                donvi += "";
            }
            else if (bcDV == "PD")
            {
                donvi += "Tổng Công ty Điện lực TP Hà Nội";
            }
            else if (bcDV == "PD0600")
            {
                donvi += "Công ty Điện lưc Thanh Trì";
            }
            else if (bcDV == "PD1400")
            {
                donvi += "Công ty Điện lực Long Biên";
            }
            else if (bcDV == "P")
            {
                donvi += "Tập đoàn Điện Lực Việt Nam";
            }
            else if (bcDV == "PD0700")
            {
                donvi += "Công ty Điện lực Gia Lâm";
            }
            else if (bcDV == "PD0800")
            {
                donvi += "Công ty Điện lực Đông Anh";
            }
            else if (bcDV == "PD0900")
            {
                donvi += "Công ty Điện lực Sóc Sơn";
            }
            else if (bcDV == "PD1800")
            {
                donvi += "Công ty Điện lực Chương Mỹ";
            }
            else if (bcDV == "PD2000")
            {
                donvi += "Công ty Điện lực Thường Tín";
            }
            else if (bcDV == "PD2400")
            {
                donvi += "Công ty Điện lực Mỹ Đức";
            }
            else if (bcDV == "PD2600")
            {
                donvi += "Công ty Điện lực Phúc Thọ";
            }
            else if (bcDV == "PD2500")
            {
                donvi += "Công ty Điện lực Phú Xuyên";
            }
            else if (bcDV == "PD2800")
            {
                donvi += "Công ty Điện lực Thanh Oai";
            }
            else if (bcDV == "PD2900")
            {
                donvi += "Công ty Điện lực Ứng Hòa";
            }
            else if (bcDV == "PD0100")
            {
                donvi += "Công ty Điện lực Hoàn Kiếm";
            }
            else if (bcDV == "PD1500")
            {
                donvi += "Công ty Điện lực Mê Linh";
            }
            else if (bcDV == "PD1700")
            {
                donvi += "Công ty Điện lực Sơn Tây ";
            }
            else if (bcDV == "PD1900")
            {
                donvi += "Công ty Điện lực Thạch Thất";
            }
            else if (bcDV == "PD2100")
            {
                donvi += "Công ty Điện lực Ba Vì";
            }
            else if (bcDV == "PD2200")
            {
                donvi += "Công ty Điện lực Đan Phượng";
            }
            else if (bcDV == "PD2300")
            {
                donvi += "Công ty Điện lực Hoài Đức";
            }
            else if (bcDV == "PD2700")
            {
                donvi += "Công ty Điện lực Quốc Oai ";
            }
            else if (bcDV == "PD0400")
            {
                donvi += "Công ty Điện lực Đống Đa ";
            }
            else if (bcDV == "PD0500")
            {
                donvi += "Công ty Điện lực Nam Từ Liêm ";
            }
            else if (bcDV == "PD0200")
            {
                donvi += "Công ty Điện lực Hai Bà Trưng ";
            }
            else if (bcDV == "PD1000")
            {
                donvi += "Công ty Điện lực Tây Hồ  ";
            }
            else if (bcDV == "PD1100")
            {
                donvi += "Công ty Điện lực Thanh Xuân ";
            }
            else if (bcDV == "PD1200")
            {
                donvi += "Công ty Điện lực Cầy giấy ";
            }
            else if (bcDV == "PD3000")
            {
                donvi += "Công ty Điện lực Bắc Từ Liêm ";
            }
            else if (bcDV == "PD1300")
            {
                donvi += "Công ty Điện lực Hoàng Mai ";
            }
            else if (bcDV == "PD0300")
            {
                donvi += "Công ty Điện lực Ba Đình ";
            }
            else if (bcDV == "PD1600")
            {
                donvi += "Công ty Điện lực Hà Đông ";
            }
            else
            {
                donvi += "Tổng Công ty Điện lực TP Hà Nội ";
            };
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tổng Công ty Điện lực TP Hà Nội",
                TieuDeTrai2 = donvi,
                Title = "CHI TIẾT BÁN ĐIỆN THEO NGÀNH NGHỀ",
                LoaiBM = "KDDN 12",
                //NguoiKy = "VŨ THẾ THẮNG",
                Thang = bcThang.ToString()+" ",
                Nam = bcNam.ToString(),
               // ToNangLuongTaiTao = "ĐÀM CHÍ DŨNG  ",

                recordsTotal = oData.Count
            };

            var recordTotal = new ChiTietBanDienTheoNganhNgheJson("Tổng cộng");
            recordTotal.Style = 1;
            recordTotal.NotHasSTT = 1;

            foreach (ChiTietBanDienTheoNganhNgheJson eachRecord in oData)
            {
                try {
                    recordTotal.SoHopDongMBD = (double.Parse(recordTotal.SoHopDongMBD, System.Globalization.CultureInfo.InvariantCulture)
                                                + Math.Round(double.Parse(eachRecord.SoHopDongMBD, System.Globalization.CultureInfo.InvariantCulture))
                                            ).ToString();
                }
                catch(Exception e) { }
                try
                {
                    recordTotal.DienThuongPham = (double.Parse(recordTotal.DienThuongPham, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.DienThuongPham, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch(Exception e) { }
                
                
            }
            oData.Add(recordTotal);

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oChiTietBanDienTheoNganhNgheQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oChiTietBanDienTheoNganhNgheQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng: ";
                    if (bcThang != null )
                        Tungaydenngay += bcThang.ToString();
                    Tungaydenngay += " - Năm ";
                    if (bcNam != null )
                        Tungaydenngay += bcNam.ToString();

                    lstValues.Add(new ClsExcel(4, 1, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 4, 4));
                  
                    string loaiBaoCao = "Loại báo cáo: ";
                    if (loaiBC == 1)
                    {
                        loaiBaoCao += "Báo cáo tháng";

                    }
                    else if (loaiBC == 2)
                    {
                        loaiBaoCao += "Báo cáo quý";
                    }
                    else if (loaiBC == 3)
                    {
                        loaiBaoCao += "Báo cáo 6 tháng";
                    }
                    else
                    {
                        loaiBaoCao += "Báo cáo năm";
                    }
                    lstValues.Add(new ClsExcel(6, 1, loaiBaoCao, StyleExcell.TextCenter, false, 12, true, 6, 6, 1,2));
                    int rowStart = 8;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, item.MaNN, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.TenNN, StyleExcell.TextLeft,true,12,true,rowStart,rowStart,1,2));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.SoHopDongMBD, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.DienThuongPham, StyleExcell.TextLeft));
                        rowStart++;
                        STT++;
                    }
                   
                    lstValues.Add(new ClsExcel(2, 0,donvi, StyleExcell.TextCenterBold, false, 12, false, 5, 5, 1, 3));
                    lstValues.Add(new ClsExcel(6, 3, "Có tổng số: " + oData.Count + " bản ghi", StyleExcell.TextRight, false, 12, true, 6,6 , 3, 4));



                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "TỔ NĂNG LƯỢNG TÁI TẠO", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 2, 0, 1));
                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 6, 0, 1));
                    //lstValues.Add(new ClsExcel(rowStart + 7, 0, dataGiaoDienRender.ToNangLuongTaiTao, StyleExcell.TextCenterBold, false, 12, true, rowStart + 7, rowStart + 7, 0, 1));

                    string ngaythangnam = "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    lstValues.Add(new ClsExcel(++rowStart-2, 2, ngaythangnam, StyleExcell.TextDatetime, false, 12, true, rowStart -2, rowStart -2, 2, 4));
                    lstValues.Add(new ClsExcel(++rowStart-2, 2, "KT.TRƯỞNG BAN", StyleExcell.TextCenterBold, false, 12, true, rowStart -2, rowStart -2, 2, 4));
                    lstValues.Add(new ClsExcel(++rowStart-2, 2, "PHÓ TRƯỞNG BAN", StyleExcell.TextCenterBold, false, 12, true, rowStart-2, rowStart-2, 2, 4));
                    lstValues.Add(new ClsExcel(++rowStart-2, 2, "", StyleExcell.TextCenterBold, false, 12, true, rowStart-2, rowStart + 2, 2, 4));
                    //lstValues.Add(new ClsExcel(rowStart + 3, 2, dataGiaoDienRender.NguoiKy, StyleExcell.TextCenterBold, false, 12, true, rowStart + 3, rowStart + 3, 2, 4));

                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    string loaiBaoCao1 = "LoaiBC: ";
                    if (loaiBC == 1)
                    {
                        loaiBaoCao1 += "BaoCaoThang";

                    }
                    else if (loaiBC == 2)
                    {
                        loaiBaoCao1 += "BaoCaoQuy";
                    }
                    else if (loaiBC == 3)
                    {
                        loaiBaoCao1 += "BaoCao6Thang";
                    }
                    else
                    {
                        loaiBaoCao1 += "BaoCaoNam";
                    }
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "ChiTietBanDienTheoNghanhghe_" + DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        // chi tiet ban dien theo thanh phan phu tai
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_BaoCaoSanLuongTietKiemCacDVTT() 
        {
            BaoCaoSanLuongTietKiemCacDVTTQuery oBaoCaoSanLuongTietKiemCacDVTTQuery = new BaoCaoSanLuongTietKiemCacDVTTQuery(HttpContext.Current.Request);
            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/CMIS_ChiTietSanLuongTietKiemDiemCDV";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBaoCaoSanLuongTietKiemCacDVTTQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var bcNam = oBaoCaoSanLuongTietKiemCacDVTTQuery.SearchNam;
            var bcThang = oBaoCaoSanLuongTietKiemCacDVTTQuery.SearchThang;
            var loaiBC = oBaoCaoSanLuongTietKiemCacDVTTQuery.LoaiBaoCao;
            var bcDV = oBaoCaoSanLuongTietKiemCacDVTTQuery.MaDonVi;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI+ "/"+ bcDV + "/"+bcThang+"/"+bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapTongHopBanDienCuaDVJson>(responseBody);
            var string1resultUP = model.data1.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(BaoCaoSanLuongTietKiemCacDVTTJson),
                    new Dictionary<string, string>
                    {
                        {"TenDonVi", "TEN_DVIQLY"},
                        {"HCSN", "HCSN"},
                        {"CQCS", "CQCS"},
                        {"SH", "SH"},
                        {"KDDV", "KDDV"},
                        {"SXCN", "SXCN"},
                        //{"TongCong", "HCSN"+"CQCS"+"SH"+ "KDDV"+"SXCN"},
                        {"LuyKeHCSN", "HCSNLK"},
                        {"LuyKeCQCS", "CQCSLK"},
                        {"LuyKeSH", "SHLK"},
                        {"LuyKeKDDV", "KDDVLK"},
                        {"LuyKeSXCN", "SXCNLK"}
                        //{"LuyKeTongCong",(double.Parse("HCSNLK")+double.Parse("CQCSLK")+double.Parse("SHLK")+double.Parse("KDDVLK")+double.Parse("SXCNLK")).ToString()},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<BaoCaoSanLuongTietKiemCacDVTTJson>>(string1resultUP, settings);
            //List<BaoCaoSanLuongTietKiemCacDVTTJson> oData = new List<BaoCaoSanLuongTietKiemCacDVTTJson>()
            //{
            //    new BaoCaoSanLuongTietKiemCacDVTTJson() {TenDonVi = "Công ty Điện lực Hoàn Kiếm",HCSN=984164,CQCS=1556555469,SH=19237673504,KDDV=99.99,SXCN=5904.673,TongCong=0.809857769853244,LuyKeHCSN=0.00342865044030162,LuyKeCQCS=1556555469, LuyKeSH=65616,LuyKeKDDV=164168,LuyKeSXCN=1616,LuyKeTongCong=194164616},
            //};
            string donvi = "";
            if (bcDV == "all")
            {
                donvi += "";
            }
            else if (bcDV == "PD")
            {
                donvi += "Tổng Công ty Điện lực TP Hà Nội";
            }
            else if (bcDV == "PD0600")
            {
                donvi += "Công ty Điện lưc Thanh Trì";
            }
            else if (bcDV == "PD1400")
            {
                donvi += "Công ty Điện lực Long Biên";
            }
            else if (bcDV == "P")
            {
                donvi += "Tập đoàn Điện Lực Việt Nam";
            }
            else if (bcDV == "PD0700")
            {
                donvi += "Công ty Điện lực Gia Lâm";
            }
            else if (bcDV == "PD0800")
            {
                donvi += "Công ty Điện lực Đông Anh";
            }
            else if (bcDV == "PD0900")
            {
                donvi += "Công ty Điện lực Sóc Sơn";
            }
            else if (bcDV == "PD1800")
            {
                donvi += "Công ty Điện lực Chương Mỹ";
            }
            else if (bcDV == "PD2000")
            {
                donvi += "Công ty Điện lực Thường Tín";
            }
            else if (bcDV == "PD2400")
            {
                donvi += "Công ty Điện lực Mỹ Đức";
            }
            else if (bcDV == "PD2600")
            {
                donvi += "Công ty Điện lực Phúc Thọ";
            }
            else if (bcDV == "PD2500")
            {
                donvi += "Công ty Điện lực Phú Xuyên";
            }
            else if (bcDV == "PD2800")
            {
                donvi += "Công ty Điện lực Thanh Oai";
            }
            else if (bcDV == "PD2900")
            {
                donvi += "Công ty Điện lực Ứng Hòa";
            }
            else if (bcDV == "PD0100")
            {
                donvi += "Công ty Điện lực Hoàn Kiếm";
            }
            else if (bcDV == "PD1500")
            {
                donvi += "Công ty Điện lực Mê Linh";
            }
            else if (bcDV == "PD1700")
            {
                donvi += "Công ty Điện lực Sơn Tây ";
            }
            else if (bcDV == "PD1900")
            {
                donvi += "Công ty Điện lực Thạch Thất";
            }
            else if (bcDV == "PD2100")
            {
                donvi += "Công ty Điện lực Ba Vì";
            }
            else if (bcDV == "PD2200")
            {
                donvi += "Công ty Điện lực Đan Phượng";
            }
            else if (bcDV == "PD2300")
            {
                donvi += "Công ty Điện lực Hoài Đức";
            }
            else if (bcDV == "PD2700")
            {
                donvi += "Công ty Điện lực Quốc Oai ";
            }
            else if (bcDV == "PD0400")
            {
                donvi += "Công ty Điện lực Đống Đa ";
            }
            else if (bcDV == "PD0500")
            {
                donvi += "Công ty Điện lực Nam Từ Liêm ";
            }
            else if (bcDV == "PD0200")
            {
                donvi += "Công ty Điện lực Hai Bà Trưng ";
            }
            else if (bcDV == "PD1000")
            {
                donvi += "Công ty Điện lực Tây Hồ  ";
            }
            else if (bcDV == "PD1100")
            {
                donvi += "Công ty Điện lực Thanh Xuân ";
            }
            else if (bcDV == "PD1200")
            {
                donvi += "Công ty Điện lực Cầy giấy ";
            }
            else if (bcDV == "PD3000")
            {
                donvi += "Công ty Điện lực Bắc Từ Liêm ";
            }
            else if (bcDV == "PD1300")
            {
                donvi += "Công ty Điện lực Hoàng Mai ";
            }
            else if (bcDV == "PD0300")
            {
                donvi += "Công ty Điện lực Ba Đình ";
            }
            else if (bcDV == "PD1600")
            {
                donvi += "Công ty Điện lực Hà Đông ";
            }
            else
            {
                donvi += "Tổng Công ty Điện lực TP Hà Nội ";
            };
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tổng Công ty Điện lực TP Hà Nội",
                TieuDeTrai2 = donvi,
                Title = "BÁO CÁO SẢN LƯỢNG ĐIỆN TIẾT KIỆM CÁC ĐƠN VỊ TRỰC THUỘC",
                LoaiBM = "KDDN 11",
                DVTinh = "Đơn vị: kWh",
                Thang = bcThang.ToString(),
                Nam = bcNam.ToString(),

                recordsTotal = oData.Count
            };

            string tenKm = "Tổng cộng";
            var recordTotal = new BaoCaoSanLuongTietKiemCacDVTTJson(tenKm);
            recordTotal.Style = 1;
            recordTotal.NotHasSTT = 1;
            foreach (BaoCaoSanLuongTietKiemCacDVTTJson eachRecord in oData)
            {
                try
                {
                    recordTotal.HCSN = (double.Parse(recordTotal.HCSN, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.HCSN, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch(Exception ) { }
                try
                {
                    recordTotal.CQCS = (double.Parse(recordTotal.CQCS, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.CQCS, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception ) { }
                try
                {
                    recordTotal.SH = (double.Parse(recordTotal.SH, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.SH, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception ) { }
                try
                {
                    recordTotal.KDDV = (double.Parse(recordTotal.KDDV, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.KDDV, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception ) { }
                try
                {
                    recordTotal.SXCN = (double.Parse(recordTotal.SXCN, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.SXCN, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception ) { }
                try
                {
                    recordTotal.LuyKeHCSN = (double.Parse(recordTotal.LuyKeHCSN, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.LuyKeHCSN, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception ) { }
                try
                {
                    recordTotal.LuyKeCQCS = (double.Parse(recordTotal.LuyKeCQCS, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.LuyKeCQCS, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception ) { }
                try
                {
                    recordTotal.LuyKeSH = (double.Parse(recordTotal.LuyKeSH, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.LuyKeSH, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception ) { }
                try
                {
                    recordTotal.LuyKeKDDV = (double.Parse(recordTotal.LuyKeKDDV, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.LuyKeKDDV, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception ) { }
                try
                {
                    recordTotal.LuyKeSXCN = (double.Parse(recordTotal.LuyKeSXCN, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.LuyKeSXCN, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception ) { }

            }

            oData.Add(recordTotal);

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoSanLuongTietKiemCacDVTTQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoSanLuongTietKiemCacDVTTQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng: ";
                    if (oBaoCaoSanLuongTietKiemCacDVTTQuery.SearchThang != null)
                        Tungaydenngay += oBaoCaoSanLuongTietKiemCacDVTTQuery.SearchThang.ToString();
                    Tungaydenngay += " - Năm ";
                    if (oBaoCaoSanLuongTietKiemCacDVTTQuery.SearchNam != null )
                        Tungaydenngay += oBaoCaoSanLuongTietKiemCacDVTTQuery.SearchNam.ToString();
                    lstValues.Add(new ClsExcel(3, 4, Tungaydenngay, StyleExcell.TextDatetime, false, 12, true, 3, 3, 4, 11));
                    string loaiBaoCao = "Loại báo cáo: ";
                    if (loaiBC == 1)
                    {
                        loaiBaoCao += "Báo cáo tháng";

                    }
                    else if (loaiBC == 2)
                    {
                        loaiBaoCao += "Báo cáo quý";
                    }else if (loaiBC == 3)
                    {
                        loaiBaoCao += "Báo cáo 6 tháng";
                    }
                    else
                    {
                        loaiBaoCao += "Báo cáo năm";
                    }
                    
                    lstValues.Add(new ClsExcel(2, 0,donvi, StyleExcell.TextCenterBold, false, 12, false, 4, 4, 3, 7));
                    lstValues.Add(new ClsExcel(4, 8, loaiBaoCao, StyleExcell.TextCenter, false, 12, true, 4, 4, 8, 11));

                    int rowStart = 8;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        string tongcong = item.HCSN + item.CQCS + item.SH + item.KDDV+ item.SXCN;
                        string lktongcong = item.LuyKeHCSN + item.LuyKeCQCS+ item.LuyKeSH+ item.LuyKeKDDV + item.LuyKeSXCN;
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.TenDonVi, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.HCSN, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.CQCS, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.SH, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.KDDV, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.SXCN, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, tongcong, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.LuyKeHCSN, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.LuyKeCQCS, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.LuyKeSH, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.LuyKeKDDV, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 12, item.LuyKeSXCN, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 13, lktongcong, StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }

                    lstValues.Add(new ClsExcel(4, 12, "Có tổng số: " + oData.Count + " bản ghi", StyleExcell.TextRight, false, 12, true, 4, 4, 12, 13));



                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "NGƯỜI LẬP BIỂU ", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 2, 0, 3));
                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 6, 0, 3));
                    lstValues.Add(new ClsExcel(rowStart + 7, 0,CurentUser.Title, StyleExcell.TextCenterBold, false, 12, true, rowStart + 7, rowStart + 7, 0, 3));

                   
                    string ngaythangnam = "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    lstValues.Add(new ClsExcel(++rowStart - 2, 9, ngaythangnam, StyleExcell.TextDatetime, false, 12, true, rowStart - 2, rowStart - 2, 9, 13));
                    lstValues.Add(new ClsExcel(++rowStart - 2, 9, "KT.TRƯỞNG BAN", StyleExcell.TextCenterBold, false, 12, true, rowStart - 2, rowStart - 2, 9, 13));
                    lstValues.Add(new ClsExcel(++rowStart - 2, 9, "PHÓ TRƯỞNG BAN", StyleExcell.TextCenterBold, false, 12, true, rowStart - 2, rowStart - 2, 9, 13));
                    lstValues.Add(new ClsExcel(++rowStart - 2, 9, "", StyleExcell.TextCenterBold, false, 12, true, rowStart - 2, rowStart +2, 9, 13));
                    //lstValues.Add(new ClsExcel(rowStart + 1, 2, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 2, 1, 1));
                    //lstValues.Add(new ClsExcel(rowStart + 3, 9, dataGiaoDienRender.NguoiKy, StyleExcell.TextCenterBold, false, 12, true, rowStart + 3, rowStart + 3, 9, 13));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "TongHopSoHDMuaBanDienCuaDV_" + DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_ChiTietSoThuVaSoDuCacKPT() 
        {
            ChiTietSoThuVaSoDuCacKPTQuery oBaoCaoSanLuongTietKiemCacDVTTQuery = new ChiTietSoThuVaSoDuCacKPTQuery(HttpContext.Current.Request);
            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/CMIS_ChiTietSoThuVaSoDuCacKPT";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBaoCaoSanLuongTietKiemCacDVTTQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var bcNam = oBaoCaoSanLuongTietKiemCacDVTTQuery.SearchNam;
            var bcThang = oBaoCaoSanLuongTietKiemCacDVTTQuery.SearchThang;
            var loaiBC = oBaoCaoSanLuongTietKiemCacDVTTQuery.LoaiBaoCao;
            var bcDV = oBaoCaoSanLuongTietKiemCacDVTTQuery.MaDonVi;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI+ "/"+ bcDV + "/"+bcThang+"/"+bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapChiTietSoThuVaSoDuCacKPTJson>(responseBody);
            var string1resultUP = model.data1.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(ChiTietSoThuVaSoDuCacKPTJson),
                    new Dictionary<string, string>
                    {
                        {"TEN_KM", "TEN_KM"},
                        {"PTRA_DKY", "PTRA_DKY"},
                        {"PTHU_DKY", "PTHU_DKY"},
                        {"PHATSINH", "PHATSINH"},
                        {"TTHUA_UT", "TTHUA_UT"},
                        {"TIEN_PBO", "TIEN_PBO"},
                        {"SO_THUDUOC", "SO_THUDUOC"},
                        {"PTRA_CKY", "PTRA_CKY"},
                        {"PTHU_CKY", "PTHU_CKY"},
                        {"NO_COKNTT", "NO_COKNTT"},
                        {"NO_KOKNTT", "NO_KOKNTT"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };


            var oData = JsonConvert.DeserializeObject<List<ChiTietSoThuVaSoDuCacKPTJson>>(string1resultUP, settings);
            //List<BaoCaoSanLuongTietKiemCacDVTTJson> oData = new List<BaoCaoSanLuongTietKiemCacDVTTJson>()
            //{
            //    new BaoCaoSanLuongTietKiemCacDVTTJson() {TenDonVi = "Công ty Điện lực Hoàn Kiếm",HCSN=984164,CQCS=1556555469,SH=19237673504,KDDV=99.99,SXCN=5904.673,TongCong=0.809857769853244,LuyKeHCSN=0.00342865044030162,LuyKeCQCS=1556555469, LuyKeSH=65616,LuyKeKDDV=164168,LuyKeSXCN=1616,LuyKeTongCong=194164616},
            //};
            string donvi = "";
            if (bcDV == "all")
            {
                donvi += "";
            }
            else if (bcDV == "PD")
            {
                donvi += "Tổng Công ty Điện lực TP Hà Nội";
            }
            else if (bcDV == "PD0600")
            {
                donvi += "Công ty Điện lưc Thanh Trì";
            }
            else if (bcDV == "PD1400")
            {
                donvi += "Công ty Điện lực Long Biên";
            }
            else if (bcDV == "P")
            {
                donvi += "Tập đoàn Điện Lực Việt Nam";
            }
            else if (bcDV == "PD0700")
            {
                donvi += "Công ty Điện lực Gia Lâm";
            }
            else if (bcDV == "PD0800")
            {
                donvi += "Công ty Điện lực Đông Anh";
            }
            else if (bcDV == "PD0900")
            {
                donvi += "Công ty Điện lực Sóc Sơn";
            }
            else if (bcDV == "PD1800")
            {
                donvi += "Công ty Điện lực Chương Mỹ";
            }
            else if (bcDV == "PD2000")
            {
                donvi += "Công ty Điện lực Thường Tín";
            }
            else if (bcDV == "PD2400")
            {
                donvi += "Công ty Điện lực Mỹ Đức";
            }
            else if (bcDV == "PD2600")
            {
                donvi += "Công ty Điện lực Phúc Thọ";
            }
            else if (bcDV == "PD2500")
            {
                donvi += "Công ty Điện lực Phú Xuyên";
            }
            else if (bcDV == "PD2800")
            {
                donvi += "Công ty Điện lực Thanh Oai";
            }
            else if (bcDV == "PD2900")
            {
                donvi += "Công ty Điện lực Ứng Hòa";
            }
            else if (bcDV == "PD0100")
            {
                donvi += "Công ty Điện lực Hoàn Kiếm";
            }
            else if (bcDV == "PD1500")
            {
                donvi += "Công ty Điện lực Mê Linh";
            }
            else if (bcDV == "PD1700")
            {
                donvi += "Công ty Điện lực Sơn Tây ";
            }
            else if (bcDV == "PD1900")
            {
                donvi += "Công ty Điện lực Thạch Thất";
            }
            else if (bcDV == "PD2100")
            {
                donvi += "Công ty Điện lực Ba Vì";
            }
            else if (bcDV == "PD2200")
            {
                donvi += "Công ty Điện lực Đan Phượng";
            }
            else if (bcDV == "PD2300")
            {
                donvi += "Công ty Điện lực Hoài Đức";
            }
            else if (bcDV == "PD2700")
            {
                donvi += "Công ty Điện lực Quốc Oai ";
            }
            else if (bcDV == "PD0400")
            {
                donvi += "Công ty Điện lực Đống Đa ";
            }
            else if (bcDV == "PD0500")
            {
                donvi += "Công ty Điện lực Nam Từ Liêm ";
            }
            else if (bcDV == "PD0200")
            {
                donvi += "Công ty Điện lực Hai Bà Trưng ";
            }
            else if (bcDV == "PD1000")
            {
                donvi += "Công ty Điện lực Tây Hồ  ";
            }
            else if (bcDV == "PD1100")
            {
                donvi += "Công ty Điện lực Thanh Xuân ";
            }
            else if (bcDV == "PD1200")
            {
                donvi += "Công ty Điện lực Cầy giấy ";
            }
            else if (bcDV == "PD3000")
            {
                donvi += "Công ty Điện lực Bắc Từ Liêm ";
            }
            else if (bcDV == "PD1300")
            {
                donvi += "Công ty Điện lực Hoàng Mai ";
            }
            else if (bcDV == "PD0300")
            {
                donvi += "Công ty Điện lực Ba Đình ";
            }
            else if (bcDV == "PD1600")
            {
                donvi += "Công ty Điện lực Hà Đông ";
            }
            else
            {
                donvi += "Tổng Công ty Điện lực TP Hà Nội ";
            };
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tổng Công ty Điện lực TP Hà Nội",
                TieuDeTrai2 = donvi,
                Title = "CHI TIẾT SỐ THU VÀ SỐ DƯ CÁC KHOẢN PHẢI THU",
                LoaiBM = "KDDN 10",
                DVTinh = "Đơn vị: Đồng",
               // NguoiKy = "VŨ THẾ THẮNG",
                Thang = bcThang.ToString(),
                Nam = bcNam.ToString(),
                NguoiLapBieu = CurentUser.Title,

                recordsTotal = oData.Count
            };

            string TEN_KM = "Tổng cộng";
            var recordTotal = new ChiTietSoThuVaSoDuCacKPTJson(TEN_KM);
            recordTotal.Style = 1;
            recordTotal.NotHasSTT = 1;
            foreach (ChiTietSoThuVaSoDuCacKPTJson eachRecord in oData)
            {
                recordTotal.PTRA_DKY =  ( double.Parse(recordTotal.PTRA_DKY, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.PTRA_DKY, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();

                recordTotal.PTHU_DKY = ( double.Parse(recordTotal.PTHU_DKY, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.PTHU_DKY, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                
                recordTotal.PHATSINH = ( double.Parse(recordTotal.PHATSINH, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.PHATSINH, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();

                recordTotal.TTHUA_UT = (double.Parse(recordTotal.TTHUA_UT, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TTHUA_UT, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();

                recordTotal.TIEN_PBO = (double.Parse(recordTotal.TIEN_PBO, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TIEN_PBO, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();

                recordTotal.SO_THUDUOC = (double.Parse(recordTotal.SO_THUDUOC, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.SO_THUDUOC, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();

                recordTotal.PTRA_CKY = (double.Parse(recordTotal.PTRA_CKY, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.PTRA_CKY, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();

                recordTotal.PTHU_CKY = (double.Parse(recordTotal.PTHU_CKY, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.PTHU_CKY, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();

                recordTotal.NO_COKNTT = (double.Parse(recordTotal.NO_COKNTT, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.NO_COKNTT, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();

                recordTotal.NO_KOKNTT = (double.Parse(recordTotal.NO_KOKNTT, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.NO_KOKNTT, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
            }

            oData.Add(recordTotal);

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoSanLuongTietKiemCacDVTTQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoSanLuongTietKiemCacDVTTQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng: ";
                    if (oBaoCaoSanLuongTietKiemCacDVTTQuery.SearchThang != null)
                        Tungaydenngay += oBaoCaoSanLuongTietKiemCacDVTTQuery.SearchThang.ToString();
                    Tungaydenngay += " - Năm ";
                    if (oBaoCaoSanLuongTietKiemCacDVTTQuery.SearchNam != null )
                        Tungaydenngay += oBaoCaoSanLuongTietKiemCacDVTTQuery.SearchNam.ToString();
                    lstValues.Add(new ClsExcel(4, 5, Tungaydenngay, StyleExcell.TextDatetime, false,12,true,4,4,5,7));
                    string loaiBaoCao = "Loại báo cáo: ";
                    if (loaiBC == "1")
                    {
                        loaiBaoCao += "Báo cáo tháng";

                    }
                    else if (loaiBC == "2")
                    {
                        loaiBaoCao += "Báo cáo quý";
                    }else if (loaiBC == "3")
                    {
                        loaiBaoCao += "Báo cáo 6 tháng";
                    }
                    else
                    {
                        loaiBaoCao += "Báo cáo năm";
                    }
                   
                    lstValues.Add(new ClsExcel(2, 0,donvi, StyleExcell.TextCenterBold, false, 12, false, 8, 8,11, 12));
                    lstValues.Add(new ClsExcel(7, 11, loaiBaoCao, StyleExcell.TextCenter, false, 12, true, 7, 7, 11, 12));

                    int rowStart = 13;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.TEN_KM, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.PTRA_DKY, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.PTHU_DKY, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.PHATSINH, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.TTHUA_UT, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.TIEN_PBO, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.SO_THUDUOC, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.PTRA_CKY, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.PTHU_CKY, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.NO_COKNTT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.NO_KOKNTT, StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }

                    lstValues.Add(new ClsExcel(8, 11, "Có tổng số: " + oData.Count + " bản ghi", StyleExcell.TextRight, false, 12));



                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "NGƯỜI LẬP BIỂU ", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 2, 0, 3));
                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 6, 0, 3));
                    lstValues.Add(new ClsExcel(rowStart + 7, 0, CurentUser.Title, StyleExcell.TextCenterBold, false, 12, true, rowStart + 7, rowStart + 7, 0, 3));

                   
                    string ngaythangnam = "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    lstValues.Add(new ClsExcel(++rowStart - 2, 9, ngaythangnam, StyleExcell.TextDatetime, false, 12, true, rowStart - 2, rowStart - 2, 9, 13));
                    lstValues.Add(new ClsExcel(++rowStart - 2, 9, "KT.TRƯỞNG BAN", StyleExcell.TextCenterBold, false, 12, true, rowStart - 2, rowStart - 2, 9, 13));
                    lstValues.Add(new ClsExcel(++rowStart - 2, 9, "PHÓ TRƯỞNG BAN", StyleExcell.TextCenterBold, false, 12, true, rowStart - 2, rowStart - 2, 9, 13));
                    lstValues.Add(new ClsExcel(++rowStart - 2, 9, "", StyleExcell.TextCenterBold, false, 12, true, rowStart - 2, rowStart +2, 9, 13));
                    //lstValues.Add(new ClsExcel(rowStart + 1, 2, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 2, 1, 1));
                    lstValues.Add(new ClsExcel(rowStart + 3, 9, dataGiaoDienRender.NguoiKy, StyleExcell.TextCenterBold, false, 12, true, rowStart + 3, rowStart + 3, 9, 13));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "ChiTietSoThuvaSoDuCacKhoanPhaiThu_DV_"+ bcDV + "_Thang_"+bcThang+"_Nam_"+bcNam + DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
    }
}