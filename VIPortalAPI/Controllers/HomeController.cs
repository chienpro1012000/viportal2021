﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace VIPortalAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    public class HomeController : ApiController
    {
        // GET api/<controller>
        /// <summary>
        /// Ham xin chao Viet Nam
        /// </summary>
        /// <returns></returns>
        [Route("")]
        public string Get()
        {
            return "Hello!";
        }
    }
}