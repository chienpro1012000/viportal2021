﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ViPortal_Utils.Base;
using VIPortalAPI.BaseAPI;
using VIPortalAPP;
using ViPortalData.DMHinhAnh;
using ViPortalData.DMVideo;
using ViPortalData.Lconfig;

namespace VIPortalAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class VideoController : SIBaseAPI
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA()
        {
            DMVideoDA oDMVideoDA = new DMVideoDA();
            DMVideoQuery oDMVideoQuery = new DMVideoQuery(HttpContext.Current.Request);
            oDMVideoQuery._ModerationStatus = 1;
            var oData = oDMVideoDA.GetListJson(oDMVideoQuery);
            DataGridRender oGrid = new DataGridRender(oData, oDMVideoQuery.Draw,
                oDMVideoDA.TongSoBanGhiSauKhiQuery);
            return oGrid;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>

        public DataGridRender VIDEO_QUERYDATA()
        {
            VideoQuery oVideoQuery = new VideoQuery(HttpContext.Current.Request);
            if (string.IsNullOrEmpty(UrlSite) && false)
            {
                var oData = new List<VideoJson>();


                LconfigDA oLconfigDA = new LconfigDA();
                string APIHinhAnhVideo = oLconfigDA.GetListJson(new LconfigQuery() { ConfigType = "APIHinhAnhVideo" }).FirstOrDefault()?.ConfigValue;
                if (string.IsNullOrEmpty(APIHinhAnhVideo))
                    APIHinhAnhVideo = "https://apicskh.evnhanoi.com.vn";
                HttpClient client = new HttpClient();
                //var byteArray = Encoding.ASCII.GetBytes("EVNHANOI:Evnhanoi@123");
                //client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
                int PageIndex = (oVideoQuery.Start / oVideoQuery.Length) + 1;
                HttpResponseMessage response = client.GetAsync(APIHinhAnhVideo + $"/api/PublicVideo/GetVideo?pageIndex={PageIndex}&pageSize={oVideoQuery.Length}&type=Video").Result;
                response.EnsureSuccessStatusCode();
                string responseBody = response.Content.ReadAsStringAsync().Result;
                JObject jObject = JObject.Parse(responseBody);
                int totalPages = jObject["totalPages"].ToObject<int>();
                int TongSobanGhi = totalPages * oVideoQuery.Length;
                List<VideoEVN> players = jObject["videosList"].ToObject<VideoEVN[]>().ToList();
                oData = players.Select(x => new VideoJson()
                {
                    ImageNews = x.thumbnail,
                    Title =x.name,
                    LinkVideo = x.url,
                    Created = x.createdDate
                }).ToList();


                DataGridRender oGrid = new DataGridRender(oData, oVideoQuery.Draw,
                    TongSobanGhi);
                oGrid.Query = "";
                return oGrid;
            }
            else
            {
                VideoDA oVideoDA = new VideoDA(UrlSite);
                
                oVideoQuery._ModerationStatus = 1;
                oVideoQuery.FieldOrder = "NgayDang";
                oVideoQuery.Ascending = false;
                var oData = oVideoDA.GetListJsonSolr(oVideoQuery);
                DataGridRender oGrid = new DataGridRender(oData, oVideoQuery.Draw,
                    oVideoDA.TongSoBanGhiSauKhiQuery);
                return oGrid;
            }
        }
    }
}