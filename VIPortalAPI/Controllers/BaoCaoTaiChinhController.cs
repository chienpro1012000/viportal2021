﻿using Library_Shared.Utilities;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using ViPortal_Utils.Base;
using VIPortalAPI.BaseAPI;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.Lconfig;

namespace VIPortalAPI.Controllers
{
    public class BaoCaoTaiChinhController : SIBaseAPI
    {
        //Báo cáo sản lượng điện
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_BaoCaoSanLuongDien()
        {
            BaoCaoSanLuongDienQuery oBaoCaoSanLuongDienQuery = new BaoCaoSanLuongDienQuery(HttpContext.Current.Request);

            /*   List<BaoCaoSanLuongDienJson> oData = new List<BaoCaoSanLuongDienJson>()
               {

                   new BaoCaoSanLuongDienJson() {DienGia="Điện mua của EVN",MaSo=150,DonViTinh="kwH",KeHoachQuy="",ThucHienQuy="253030400000",TyLeQuy="",KeHoachLuyKe="",ThucHienLuyKe="681132737000",TyLeLyKe=""},
                   new BaoCaoSanLuongDienJson() {DienGia="Điện mua của EVN",MaSo=150,DonViTinh="kwH",KeHoachQuy="",ThucHienQuy="253030400000",TyLeQuy="",KeHoachLuyKe="",ThucHienLuyKe="681132737000",TyLeLyKe=""},
                   new BaoCaoSanLuongDienJson() {DienGia="Điện mua của EVN",MaSo=150,DonViTinh="kwH",KeHoachQuy="",ThucHienQuy="253030400000",TyLeQuy="",KeHoachLuyKe="",ThucHienLuyKe="681132737000",TyLeLyKe=""},
                   new BaoCaoSanLuongDienJson() {DienGia="Điện mua của EVN",MaSo=150,DonViTinh="kwH",KeHoachQuy="",ThucHienQuy="253030400000",TyLeQuy="",KeHoachLuyKe="",ThucHienLuyKe="681132737000",TyLeLyKe=""}
                };*/
            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/ERP_BCSanLuongDien";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBaoCaoSanLuongDienQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var bcNam = oBaoCaoSanLuongDienQuery.SearchNam;
            var bcQuyBaoCao = oBaoCaoSanLuongDienQuery.QuyBaoCao;
            var bcDV = oBaoCaoSanLuongDienQuery.LoaiDV;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/"+ bcDV + "/Q" + bcQuyBaoCao + "_" + bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapBaoCaoSanLuongDienJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(BaoCaoSanLuongDienJson),
                    new Dictionary<string, string>
                    {
                        {"DienGia", "LINE_DESCRIPTION"},
                        {"MaSo", "STRUCT"},
                       // {"DonViTinh", "ATTRIBUTE49"},
                        {"KeHoachQuy", "NUM3"},
                        {"ThucHienQuy", "NUM4"},
                        {"TyLeQuy", "TL_QUY"},
                        {"KeHoachLuyKe", "NUM5"},
                        {"ThucHienLuyKe", "NUM6"},
                        {"TyLeLyKe", "TL_NAM"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<BaoCaoSanLuongDienJson>>(string1resultUP, settings);
            string donvi = "";
            if (bcDV == "all")
            {
                donvi += "";
            }
            else if (bcDV == "074000")
            {
                donvi += "Tổng Công ty điện lực TP Hà Nội";
            }
            else if (bcDV == "070600")
            {
                donvi += "Công ty Điện lực Thanh Trì";
            }
            else if (bcDV == "000100")
            {
                donvi += "Tập đoàn Điện lực Việt Nam - Kế toán ngành";
            }
            else if (bcDV == "070700")
            {
                donvi += "Công ty Điện lực Gia Lâm";
            }
            else if (bcDV == "070800")
            {
                donvi += "Công ty Điện lực Đông Anh ";
            }
            else if (bcDV == "071400")
            {
                donvi += "Công ty Điện lực Long Biên";
            }
            else if (bcDV == "070900")
            {
                donvi += "Công ty Điện lực Sóc Sơn";
            }
            else if (bcDV == "071800")
            {
                donvi += "Công ty Điện lực Chương Mỹ";
            }
            else if (bcDV == "072000")
            {
                donvi += "Công ty Điện lực Thường Tín";
            }
            else if (bcDV == "072400")
            {
                donvi += "Công ty Điện lực Mỹ Đức";
            }
            else if (bcDV == "072600")
            {
                donvi += "Công ty Điện lực Phúc Thọ";
            }
            else if (bcDV == "072500")
            {
                donvi += "Công ty Điện lực Phú Xuyên";
            }
            else if (bcDV == "072800")
            {
                donvi += "Công ty Điện lực Thanh Oai";
            }
            else if (bcDV == "072900")
            {
                donvi += "Công ty Điện lực Ứng Hòa";
            }
            else if (bcDV == "070100")
            {
                donvi += "Công ty Điện lực Hoàn Kiếm ";
            }
            else if (bcDV == "071500")
            {
                donvi += "Công ty Điện lực Mê Linh";
            }
            else if (bcDV == "071700")
            {
                donvi += "Công ty Điện lực Sơn Tây";
            }
            else if (bcDV == "071900")
            {
                donvi += "Công ty Điện lực Thạch Thất";
            }
            else if (bcDV == "072100")
            {
                donvi += "Công ty Điện lực Ba Vì";
            }
            else if (bcDV == "072200")
            {
                donvi += "Công ty Điện lực Đan Phượng";
            }
            else if (bcDV == "072300")
            {
                donvi += "Công ty Điện lực Hoài Đức";
            }
            else if (bcDV == "072700")
            {
                donvi += "Công ty Điện lực Quốc Oai ";
            }
            else if (bcDV == "070400")
            {
                donvi += "Công ty Điện lực Đống Đa";
            }
            else if (bcDV == "070500")
            {
                donvi += "Công ty Điện lực Nam Từ Liêm";
            }
            else if (bcDV == "070200")
            {
                donvi += "Công ty Điện lực Hai Bà Trưng";
            }
            else if (bcDV == "071000")
            {
                donvi += "Công ty Điện lực Tây Hồ";
            }
            else if (bcDV == "071100")
            {
                donvi += "Công ty Điện lực Thanh Xuân";
            }
            else if (bcDV == "071200")
            {
                donvi += "Công ty Điện lực Cầu Giấy";
            }
            else if (bcDV == "073000")
            {
                donvi += "Công ty Điện lực Bắc Từ Liêm";
            }
            else if (bcDV == "071300")
            {
                donvi += "Công ty Điện lực Hoàng Mai";
            }
            else if (bcDV == "070300")
            {
                donvi += "Công ty Điện lực Ba Đình";
            }
            else if (bcDV == "071600")
            {
                donvi += "Công ty Điện lực Hà Đông";
            }
            else if (bcDV == "073100")
            {
                donvi += "Công ty Công nghệ thông tin Điện lực Hà Nội";
            }
            else if (bcDV == "073901")
            {
                donvi += "Ban Quản lý dự án Lưới điện Hà Nội - DEP";
            }
            else if (bcDV == "073500")
            {
                donvi += "Công ty Thí nghiệm điện Điện lực Hà Nội";
            }
            else if (bcDV == "073802")
            {
                donvi += "Ban Quản lý dự án phát triển Điện lực Hà Nội - XDCB";
            }
            else if (bcDV == "073200")
            {
                donvi += "Trung tâm Điều độ Hệ thống điện TP Hà Nội";
            }
            else if (bcDV == "073902")
            {
                donvi += "Ban Quản lý dự án Lưới điện Hà Nội - XDCB";
            }
            else if (bcDV == "073801")
            {
                donvi += "Ban Quản lý dự án phát triển Điện lực Hà Nội - ADB";
            }
            else if (bcDV == "073600")
            {
                donvi += "Công ty Lưới điện cao thế TP Hà Nội";
            }
            else if (bcDV == "073700")
            {
                donvi += "Công ty Dịch vụ Điện lực Hà Nội";
            }
            else
            {
                donvi += "Tổng Công ty Điện lực TP Hà Nội";
            };

            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tổng Công ty Điện lực TP Hà Nội",
                TieuDeTrai2 = donvi,
                Title = "BÁO CÁO SẢN LƯỢNG ĐIỆN",
                Nam = bcNam.ToString(),
                LoaiBM = "Biểu 01/THKT",
                NguoiLapBieu=CurentUser.Title,
                Thang= bcQuyBaoCao.ToString(),
                recordsTotal = oData.Count
            };

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoSanLuongDienQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoSanLuongDienQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Qúy : ";
                    if (bcQuyBaoCao != null)
                        Tungaydenngay += bcQuyBaoCao.ToString();
                    Tungaydenngay += " - Năm ";
                    if (bcNam != null)
                        Tungaydenngay += bcNam.ToString();
                   
                    lstValues.Add(new ClsExcel(3, 1, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 3, 3));
                    lstValues.Add(new ClsExcel(1, 0, donvi, StyleExcell.TextCenter, false, 12, false, 1, 1));
                    lstValues.Add(new ClsExcel(4, 6, "Tổng số bản ghi:" + oData.Count, StyleExcell.TextRight, false, 12, true, 4, 4,6,8));
                    int rowStart = 9;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, item.DienGia, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.MaSo, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.DonViTinh, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.KeHoachQuy, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.ThucHienQuy, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.TyLeQuy, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.KeHoachLuyKe, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.ThucHienLuyKe, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.TyLeLyKe, StyleExcell.TextLeft));
                        rowStart++;
                    }

                    lstValues.Add(new ClsExcel(++rowStart + 1, 0, "NGƯỜI LẬP BIỂU", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 1,0,1));
                    lstValues.Add(new ClsExcel(++rowStart + 1, 0, "(Ký ,họ tên )", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 1,0,1));
                    lstValues.Add(new ClsExcel(rowStart + 2, 0, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 8,0,1));
                    lstValues.Add(new ClsExcel(++rowStart - 1, 2, "KẾ TOÁN TRƯỞNG", StyleExcell.TextCenterBold, false, 12, true, rowStart - 1, rowStart - 1, 2, 5));
                    lstValues.Add(new ClsExcel(++rowStart - 1, 2, "(Ký ,họ tên )", StyleExcell.TextCenterBold, false, 12, true, rowStart - 1, rowStart - 1, 2, 5));
                    lstValues.Add(new ClsExcel(rowStart, 1, "", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart + 6, 2, 5));

                    string ngaythangnam = "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    lstValues.Add(new ClsExcel(++rowStart - 4, 6, ngaythangnam, StyleExcell.TextDatetime, false, 12, true, rowStart - 4, rowStart - 4, 6, 8));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 6, "GIÁM ĐỐC", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart - 4, 6, 8));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 6, "(Ký ,họ tên )", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart - 4, 6, 8));
                    lstValues.Add(new ClsExcel(rowStart - 3, 6, "", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart + 3, 6, 8));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    


                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "BaoCaoSanLuongDien_" + DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        //Báo cáo ket qua san xua kinh doanh
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_BaoCaoKetQuaSanXuatKinhDoanh()
        {
            BaoCaoKetQuaSanXuatKinhDoanhQuery oBaoCaoKetQuaSanXuatKinhDoanhQuery = new BaoCaoKetQuaSanXuatKinhDoanhQuery(HttpContext.Current.Request);
            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/ERP_BCKetQuaHoatDongKinhDoanh/all/Q3_2021";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBaoCaoKetQuaSanXuatKinhDoanhQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var bcNam = oBaoCaoKetQuaSanXuatKinhDoanhQuery.SearchNam;
            var bcQuyBaoCao = oBaoCaoKetQuaSanXuatKinhDoanhQuery.QuyBaoCao;
            var bcDV = oBaoCaoKetQuaSanXuatKinhDoanhQuery.LoaiDV;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/"+ bcDV + "/Q" + bcQuyBaoCao + "_" + bcNam ;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapBaoCaoKetQuaSanXuatKinhDoanhJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(BaoCaoKetQuaSanXuatKinhDoanhJson),
                    new Dictionary<string, string>
                    {
                        {"DienGia", "LINE_DESCRIPTION"},
                        {"MaSo", "SETUP_CODE"},
                        {"ThuyetMinh", "ATTRIBUTE49"},
                        {"NamNayQuy", "NUM1"},
                        {"NamTruocQuy", "NUM2"},
                        {"NamNayLuyKe", "NUM3"},
                        {"NamTruocLuyKe", "NUM4"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<BaoCaoKetQuaSanXuatKinhDoanhJson>>(string1resultUP, settings);
            string donvi = "";
            if (bcDV == "all")
            {
                donvi += "";
            }
            else if (bcDV == "074000")
            {
                donvi += "Tổng Công ty điện lực TP Hà Nội";
            }
            else if (bcDV == "070600")
            {
                donvi += "Công ty Điện lực Thanh Trì";
            }
            else if (bcDV == "000100")
            {
                donvi += "Tập đoàn Điện lực Việt Nam - Kế toán ngành";
            }
            else if (bcDV == "070700")
            {
                donvi += "Công ty Điện lực Gia Lâm";
            }
            else if (bcDV == "070800")
            {
                donvi += "Công ty Điện lực Đông Anh ";
            }
            else if (bcDV == "071400")
            {
                donvi += "Công ty Điện lực Long Biên";
            }
            else if (bcDV == "070900")
            {
                donvi += "Công ty Điện lực Sóc Sơn";
            }
            else if (bcDV == "071800")
            {
                donvi += "Công ty Điện lực Chương Mỹ";
            }
            else if (bcDV == "072000")
            {
                donvi += "Công ty Điện lực Thường Tín";
            }
            else if (bcDV == "072400")
            {
                donvi += "Công ty Điện lực Mỹ Đức";
            }
            else if (bcDV == "072600")
            {
                donvi += "Công ty Điện lực Phúc Thọ";
            }
            else if (bcDV == "072500")
            {
                donvi += "Công ty Điện lực Phú Xuyên";
            }
            else if (bcDV == "072800")
            {
                donvi += "Công ty Điện lực Thanh Oai";
            }
            else if (bcDV == "072900")
            {
                donvi += "Công ty Điện lực Ứng Hòa";
            }
            else if (bcDV == "070100")
            {
                donvi += "Công ty Điện lực Hoàn Kiếm ";
            }
            else if (bcDV == "071500")
            {
                donvi += "Công ty Điện lực Mê Linh";
            }
            else if (bcDV == "071700")
            {
                donvi += "Công ty Điện lực Sơn Tây";
            }
            else if (bcDV == "071900")
            {
                donvi += "Công ty Điện lực Thạch Thất";
            }
            else if (bcDV == "072100")
            {
                donvi += "Công ty Điện lực Ba Vì";
            }
            else if (bcDV == "072200")
            {
                donvi += "Công ty Điện lực Đan Phượng";
            }
            else if (bcDV == "072300")
            {
                donvi += "Công ty Điện lực Hoài Đức";
            }
            else if (bcDV == "072700")
            {
                donvi += "Công ty Điện lực Quốc Oai ";
            }
            else if (bcDV == "070400")
            {
                donvi += "Công ty Điện lực Đống Đa";
            }
            else if (bcDV == "070500")
            {
                donvi += "Công ty Điện lực Nam Từ Liêm";
            }
            else if (bcDV == "070200")
            {
                donvi += "Công ty Điện lực Hai Bà Trưng";
            }
            else if (bcDV == "071000")
            {
                donvi += "Công ty Điện lực Tây Hồ";
            }
            else if (bcDV == "071100")
            {
                donvi += "Công ty Điện lực Thanh Xuân";
            }
            else if (bcDV == "071200")
            {
                donvi += "Công ty Điện lực Cầu Giấy";
            }
            else if (bcDV == "073000")
            {
                donvi += "Công ty Điện lực Bắc Từ Liêm";
            }
            else if (bcDV == "071300")
            {
                donvi += "Công ty Điện lực Hoàng Mai";
            }
            else if (bcDV == "070300")
            {
                donvi += "Công ty Điện lực Ba Đình";
            }
            else if (bcDV == "071600")
            {
                donvi += "Công ty Điện lực Hà Đông";
            }
            else if (bcDV == "073100")
            {
                donvi += "Công ty Công nghệ thông tin Điện lực Hà Nội";
            }
            else if (bcDV == "073901")
            {
                donvi += "Ban Quản lý dự án Lưới điện Hà Nội - DEP";
            }
            else if (bcDV == "073500")
            {
                donvi += "Công ty Thí nghiệm điện Điện lực Hà Nội";
            }
            else if (bcDV == "073802")
            {
                donvi += "Ban Quản lý dự án phát triển Điện lực Hà Nội - XDCB";
            }
            else if (bcDV == "073200")
            {
                donvi += "Trung tâm Điều độ Hệ thống điện TP Hà Nội";
            }
            else if (bcDV == "073902")
            {
                donvi += "Ban Quản lý dự án Lưới điện Hà Nội - XDCB";
            }
            else if (bcDV == "073801")
            {
                donvi += "Ban Quản lý dự án phát triển Điện lực Hà Nội - ADB";
            }
            else if (bcDV == "073600")
            {
                donvi += "Công ty Lưới điện cao thế TP Hà Nội";
            }
            else if (bcDV == "073700")
            {
                donvi += "Công ty Dịch vụ Điện lực Hà Nội";
            }
            else
            {
                donvi += "Tổng Công ty Điện lực TP Hà Nội";
            };

            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tổng Công ty Điện lực TP Hà Nội",
                TieuDeTrai2 = donvi,
                Title = "BÁO CÁO KẾT QUẢ SẢN XUẤT KINH DOANH",
                LoaiBM = "Mẫu số B02-DN          (Ban hành theo Thông tư số 200/2014/TT-BTC Ngày 22/12/2014 của Bộ Tài chính)",
                Nam = bcNam.ToString(),
                NguoiLapBieu=CurentUser.Title,
                Thang = bcQuyBaoCao.ToString(),
                recordsTotal = oData.Count
            };

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoKetQuaSanXuatKinhDoanhQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoKetQuaSanXuatKinhDoanhQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Qúy : ";
                    if (bcQuyBaoCao != null )
                        Tungaydenngay += bcQuyBaoCao.ToString();
                    Tungaydenngay += " - Năm ";
                    if (bcNam != null )
                        Tungaydenngay += bcNam.ToString();


                    lstValues.Add(new ClsExcel(4, 0, Tungaydenngay, StyleExcell.TextCenter, false, 12, false, 0, 0));

                    lstValues.Add(new ClsExcel(5, 5,"Tổng số bản ghi:"+ oData.Count, StyleExcell.TextRight, false, 12, false, 0, 0));
                    lstValues.Add(new ClsExcel(2, 0,donvi, StyleExcell.TextLeftBold, false, 12, false, 0, 0));
                    lstValues.Add(new ClsExcel(5, 3,"Đơn vị tính : đồng ", StyleExcell.TextRight, false, 12, true, 5, 5,3,4));
                    int rowStart = 9;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, item.DienGia, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.MaSo, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.ThuyetMinh, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.NamNayQuy, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.NamTruocQuy, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.NamNayLuyKe, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.NamTruocLuyKe, StyleExcell.TextLeft));
                        rowStart++;
                    }
                    lstValues.Add(new ClsExcel(++rowStart+1 , 0, "NGƯỜI LẬP BIỂU", StyleExcell.TextCenterBold, false, 12, false, rowStart+1 , rowStart+1 ));
                    lstValues.Add(new ClsExcel(++rowStart+1 , 0, "(Ký ,họ tên )", StyleExcell.TextCenterBold, false, 12, false, rowStart+1 , rowStart+1 ));
                    lstValues.Add(new ClsExcel(rowStart + 2, 0, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 8));
                    lstValues.Add(new ClsExcel(++rowStart-1 , 1, "KẾ TOÁN TRƯỞNG", StyleExcell.TextCenterBold, false, 12, true, rowStart-1 , rowStart-1 , 1, 3));
                    lstValues.Add(new ClsExcel(++rowStart-1 , 1, "(Ký ,họ tên )", StyleExcell.TextCenterBold, false, 12, true, rowStart-1 , rowStart-1 , 1, 3));
                    lstValues.Add(new ClsExcel(rowStart , 1, "", StyleExcell.TextCenterBold, false, 12, true, rowStart , rowStart + 6, 1, 3));

                    string ngaythangnam = "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    lstValues.Add(new ClsExcel(++rowStart - 4, 4, ngaythangnam, StyleExcell.TextDatetime, false, 12, true, rowStart - 4, rowStart - 4, 4, 6));
                    lstValues.Add(new ClsExcel(++rowStart -4, 4, "GIÁM ĐỐC", StyleExcell.TextCenterBold, false, 12, true, rowStart-4, rowStart-4, 4, 6));
                    lstValues.Add(new ClsExcel(++rowStart-4, 4, "(Ký ,họ tên )", StyleExcell.TextCenterBold, false, 12, true, rowStart-4, rowStart-4, 4, 6));
                    lstValues.Add(new ClsExcel(rowStart -3, 4, "", StyleExcell.TextCenterBold, false, 12, true, rowStart -3, rowStart + 3, 4, 6));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "BaoCaoKetQuaSanXuatKinhDoanh_" + DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        //Nguon von dau tu
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_NguonVonDauTu()
        {
           NguonVonDauTuQuery oNguonVonDauTuQuery = new NguonVonDauTuQuery(HttpContext.Current.Request);
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/ERP_BCNguonVonDauTu";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oNguonVonDauTuQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            var bcThang = oNguonVonDauTuQuery.ThangBaoCao;
            var bcNam = oNguonVonDauTuQuery.SearchNam;
            var bcDV = oNguonVonDauTuQuery.LoaiDV;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcDV + "/" + bcThang + "-" + bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapNguonVonDauTuJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(NguonVonDauTuJson),
                    new Dictionary<string, string>
                    {
                        {"NguonVon", "DESCRIPTION"},
                        {"SoDuDauNam", "NUM1"},
                        {"KyBaoCaoTang", "NUM2"},
                        {"LuyKeTuDauNamTang", "NUM3"},
                        {"LuyKeTuKhoiCongTang", "NUM4"},
                        {"KyBaoCaoGiam", "NUM5"},
                        {"LuyKeTuDauNamGiam", "NUM6"},
                        {"LuyKeTuKhoiCongGiam", "NUM7"},
                        {"SoDuCuoiKy", "NUM8"},

                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<NguonVonDauTuJson>>(string1resultUP, settings);
            string donvi = "";
            if (bcDV == "all")
            {
                donvi += "";
            }
            else if (bcDV == "074000")
            {
                donvi += "Tổng Công ty điện lực TP Hà Nội";
            }
            else if (bcDV == "070600")
            {
                donvi += "Công ty Điện lực Thanh Trì";
            }
            else if (bcDV == "000100")
            {
                donvi += "Tập đoàn Điện lực Việt Nam - Kế toán ngành";
            }
            else if (bcDV == "070700")
            {
                donvi += "Công ty Điện lực Gia Lâm";
            }
            else if (bcDV == "070800")
            {
                donvi += "Công ty Điện lực Đông Anh ";
            }
            else if (bcDV == "071400")
            {
                donvi += "Công ty Điện lực Long Biên";
            }
            else if (bcDV == "070900")
            {
                donvi += "Công ty Điện lực Sóc Sơn";
            }
            else if (bcDV == "071800")
            {
                donvi += "Công ty Điện lực Chương Mỹ";
            }
            else if (bcDV == "072000")
            {
                donvi += "Công ty Điện lực Thường Tín";
            }
            else if (bcDV == "072400")
            {
                donvi += "Công ty Điện lực Mỹ Đức";
            }
            else if (bcDV == "072600")
            {
                donvi += "Công ty Điện lực Phúc Thọ";
            }
            else if (bcDV == "072500")
            {
                donvi += "Công ty Điện lực Phú Xuyên";
            }
            else if (bcDV == "072800")
            {
                donvi += "Công ty Điện lực Thanh Oai";
            }
            else if (bcDV == "072900")
            {
                donvi += "Công ty Điện lực Ứng Hòa";
            }
            else if (bcDV == "070100")
            {
                donvi += "Công ty Điện lực Hoàn Kiếm ";
            }
            else if (bcDV == "071500")
            {
                donvi += "Công ty Điện lực Mê Linh";
            }
            else if (bcDV == "071700")
            {
                donvi += "Công ty Điện lực Sơn Tây";
            }
            else if (bcDV == "071900")
            {
                donvi += "Công ty Điện lực Thạch Thất";
            }
            else if (bcDV == "072100")
            {
                donvi += "Công ty Điện lực Ba Vì";
            }
            else if (bcDV == "072200")
            {
                donvi += "Công ty Điện lực Đan Phượng";
            }
            else if (bcDV == "072300")
            {
                donvi += "Công ty Điện lực Hoài Đức";
            }
            else if (bcDV == "072700")
            {
                donvi += "Công ty Điện lực Quốc Oai ";
            }
            else if (bcDV == "070400")
            {
                donvi += "Công ty Điện lực Đống Đa";
            }
            else if (bcDV == "070500")
            {
                donvi += "Công ty Điện lực Nam Từ Liêm";
            }
            else if (bcDV == "070200")
            {
                donvi += "Công ty Điện lực Hai Bà Trưng";
            }
            else if (bcDV == "071000")
            {
                donvi += "Công ty Điện lực Tây Hồ";
            }
            else if (bcDV == "071100")
            {
                donvi += "Công ty Điện lực Thanh Xuân";
            }
            else if (bcDV == "071200")
            {
                donvi += "Công ty Điện lực Cầu Giấy";
            }
            else if (bcDV == "073000")
            {
                donvi += "Công ty Điện lực Bắc Từ Liêm";
            }
            else if (bcDV == "071300")
            {
                donvi += "Công ty Điện lực Hoàng Mai";
            }
            else if (bcDV == "070300")
            {
                donvi += "Công ty Điện lực Ba Đình";
            }
            else if (bcDV == "071600")
            {
                donvi += "Công ty Điện lực Hà Đông";
            }
            else if (bcDV == "073100")
            {
                donvi += "Công ty Công nghệ thông tin Điện lực Hà Nội";
            }
            else if (bcDV == "073901")
            {
                donvi += "Ban Quản lý dự án Lưới điện Hà Nội - DEP";
            }
            else if (bcDV == "073500")
            {
                donvi += "Công ty Thí nghiệm điện Điện lực Hà Nội";
            }
            else if (bcDV == "073802")
            {
                donvi += "Ban Quản lý dự án phát triển Điện lực Hà Nội - XDCB";
            }
            else if (bcDV == "073200")
            {
                donvi += "Trung tâm Điều độ Hệ thống điện TP Hà Nội";
            }
            else if (bcDV == "073902")
            {
                donvi += "Ban Quản lý dự án Lưới điện Hà Nội - XDCB";
            }
            else if (bcDV == "073801")
            {
                donvi += "Ban Quản lý dự án phát triển Điện lực Hà Nội - ADB";
            }
            else if (bcDV == "073600")
            {
                donvi += "Công ty Lưới điện cao thế TP Hà Nội";
            }
            else if (bcDV == "073700")
            {
                donvi += "Công ty Dịch vụ Điện lực Hà Nội";
            }
            else
            {
                donvi += "Tổng Công ty Điện lực TP Hà Nội";
            };


            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tổng Công ty Điện lực TP Hà Nội",
                TieuDeTrai2 = donvi,
                Title = "NGUỒN VỐN ĐẦU TƯ",
                Thang = bcThang.ToString(),
                Nam = bcNam.ToString(),
                LoaiBM = "Biểu 01/ĐTXD",
                recordsTotal = oData.Count
            };

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oNguonVonDauTuQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oNguonVonDauTuQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng : ";
                    if (bcThang != null)
                        Tungaydenngay += bcThang.ToString();
                    Tungaydenngay += " - Năm ";
                    if (bcNam != null)
                        Tungaydenngay += bcNam.ToString();
                  


                    lstValues.Add(new ClsExcel(4, 0, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 4, 4));
                    lstValues.Add(new ClsExcel(1, 0, donvi, StyleExcell.TextCenter, false, 12, false, 4, 4));
                   
                    lstValues.Add(new ClsExcel(6, 7, "Tổng số: "+oData.Count+"  bản ghi", StyleExcell.TextDatetime, false, 12, true, 6, 6,7,8));
                    int rowStart = 9;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, item.NguonVon, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.SoDuDauNam, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.KyBaoCaoTang, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.LuyKeTuDauNamTang, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.LuyKeTuKhoiCongTang, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.KyBaoCaoGiam, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.LuyKeTuDauNamGiam, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.LuyKeTuKhoiCongGiam, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.SoDuCuoiKy, StyleExcell.TextLeft));
                        STT++;
                        rowStart++;
                    }
                    lstValues.Add(new ClsExcel(++rowStart + 1, 0, "NGƯỜI LẬP BIỂU", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 1, 0, 2));
                    lstValues.Add(new ClsExcel(++rowStart + 1, 0, "(Ký ,họ tên )", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 1, 0, 2));
                    lstValues.Add(new ClsExcel(rowStart + 2, 0, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 8, 0, 2));
                    lstValues.Add(new ClsExcel(++rowStart - 1, 3, "KẾ TOÁN TRƯỞNG", StyleExcell.TextCenterBold, false, 12, true, rowStart - 1, rowStart - 1, 3, 5));
                    lstValues.Add(new ClsExcel(++rowStart - 1, 3, "(Ký ,họ tên )", StyleExcell.TextCenterBold, false, 12, true, rowStart - 1, rowStart - 1, 3, 5));
                    lstValues.Add(new ClsExcel(rowStart, 3, "", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart + 6, 3, 5));

                    string ngaythangnam = "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    lstValues.Add(new ClsExcel(++rowStart - 4, 6, ngaythangnam, StyleExcell.TextDatetime, false, 12, true, rowStart - 4, rowStart - 4, 6, 8));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 6, "GIÁM ĐỐC", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart - 4, 6, 8));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 6, "(Ký ,họ tên )", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart - 4, 6, 8));
                    lstValues.Add(new ClsExcel(rowStart - 3, 6, "", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart + 3, 6, 8));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "NguonVonDauTu_"+ DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        //chi tiet nguon von dau tu xay dung
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_ChiTietNguonVonDauTuXayDung()
        {
            ChiTietNguonVonDauTuXayDungQuery oBaoCaoChiTeuDVKHQuery = new ChiTietNguonVonDauTuXayDungQuery(HttpContext.Current.Request);
            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/ERP_ChiTietNguonVonDTXD";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBaoCaoChiTeuDVKHQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var bcNam = oBaoCaoChiTeuDVKHQuery.SearchNam;
            var bcQuy = oBaoCaoChiTeuDVKHQuery.QuyBaoCao;
            var bcDV = oBaoCaoChiTeuDVKHQuery.LoaiDV;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcDV + "/Q" + bcQuy + "_" + bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapChiTietNguonVonDauTuXayDungJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(ChiTietNguonVonDauTuXayDungJson),
                    new Dictionary<string, string>
                    {
                       // {"STT","STRUCT"},
                        {"SETUP_CODE","SETUP_CODE"},
                        {"LINE_DESCRIPTION","LINE_DESCRIPTION"},
                        {"SoDuDauNam", "NUM1"},
                        {"QuyBaoCaoTang", "NUM2"},
                        {"LuyKeTuDauNamTang", "NUM3"},
                        {"LuyKeTuKhoiCongTang", "NUM4"},
                        {"QuyBaoCaoGiam", "NUM5"},
                        {"LuyKeTuDauNamGiam", "NUM6"},
                        {"LuyKeTuKhoiCongGiam", "NUM7"},
                        {"SoDuCuoiKy", "NUM8"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<ChiTietNguonVonDauTuXayDungJson>>(string1resultUP, settings);
            string donvi = "";
            if (bcDV == "all")
            {
                donvi += "";
            }
            else if (bcDV == "074000")
            {
                donvi += "Tổng Công ty điện lực TP Hà Nội";
            }
            else if (bcDV == "070600")
            {
                donvi += "Công ty Điện lực Thanh Trì";
            }
            else if (bcDV == "000100")
            {
                donvi += "Tập đoàn Điện lực Việt Nam - Kế toán ngành";
            }
            else if (bcDV == "070700")
            {
                donvi += "Công ty Điện lực Gia Lâm";
            }
            else if (bcDV == "070800")
            {
                donvi += "Công ty Điện lực Đông Anh ";
            }
            else if (bcDV == "071400")
            {
                donvi += "Công ty Điện lực Long Biên";
            }
            else if (bcDV == "070900")
            {
                donvi += "Công ty Điện lực Sóc Sơn";
            }
            else if (bcDV == "071800")
            {
                donvi += "Công ty Điện lực Chương Mỹ";
            }
            else if (bcDV == "072000")
            {
                donvi += "Công ty Điện lực Thường Tín";
            }
            else if (bcDV == "072400")
            {
                donvi += "Công ty Điện lực Mỹ Đức";
            }
            else if (bcDV == "072600")
            {
                donvi += "Công ty Điện lực Phúc Thọ";
            }
            else if (bcDV == "072500")
            {
                donvi += "Công ty Điện lực Phú Xuyên";
            }
            else if (bcDV == "072800")
            {
                donvi += "Công ty Điện lực Thanh Oai";
            }
            else if (bcDV == "072900")
            {
                donvi += "Công ty Điện lực Ứng Hòa";
            }
            else if (bcDV == "070100")
            {
                donvi += "Công ty Điện lực Hoàn Kiếm ";
            }
            else if (bcDV == "071500")
            {
                donvi += "Công ty Điện lực Mê Linh";
            }
            else if (bcDV == "071700")
            {
                donvi += "Công ty Điện lực Sơn Tây";
            }
            else if (bcDV == "071900")
            {
                donvi += "Công ty Điện lực Thạch Thất";
            }
            else if (bcDV == "072100")
            {
                donvi += "Công ty Điện lực Ba Vì";
            }
            else if (bcDV == "072200")
            {
                donvi += "Công ty Điện lực Đan Phượng";
            }
            else if (bcDV == "072300")
            {
                donvi += "Công ty Điện lực Hoài Đức";
            }
            else if (bcDV == "072700")
            {
                donvi += "Công ty Điện lực Quốc Oai ";
            }
            else if (bcDV == "070400")
            {
                donvi += "Công ty Điện lực Đống Đa";
            }
            else if (bcDV == "070500")
            {
                donvi += "Công ty Điện lực Nam Từ Liêm";
            }
            else if (bcDV == "070200")
            {
                donvi += "Công ty Điện lực Hai Bà Trưng";
            }
            else if (bcDV == "071000")
            {
                donvi += "Công ty Điện lực Tây Hồ";
            }
            else if (bcDV == "071100")
            {
                donvi += "Công ty Điện lực Thanh Xuân";
            }
            else if (bcDV == "071200")
            {
                donvi += "Công ty Điện lực Cầu Giấy";
            }
            else if (bcDV == "073000")
            {
                donvi += "Công ty Điện lực Bắc Từ Liêm";
            }
            else if (bcDV == "071300")
            {
                donvi += "Công ty Điện lực Hoàng Mai";
            }
            else if (bcDV == "070300")
            {
                donvi += "Công ty Điện lực Ba Đình";
            }
            else if (bcDV == "071600")
            {
                donvi += "Công ty Điện lực Hà Đông";
            }
            else if (bcDV == "073100")
            {
                donvi += "Công ty Công nghệ thông tin Điện lực Hà Nội";
            }
            else if (bcDV == "073901")
            {
                donvi += "Ban Quản lý dự án Lưới điện Hà Nội - DEP";
            }
            else if (bcDV == "073500")
            {
                donvi += "Công ty Thí nghiệm điện Điện lực Hà Nội";
            }
            else if (bcDV == "073802")
            {
                donvi += "Ban Quản lý dự án phát triển Điện lực Hà Nội - XDCB";
            }
            else if (bcDV == "073200")
            {
                donvi += "Trung tâm Điều độ Hệ thống điện TP Hà Nội";
            }
            else if (bcDV == "073902")
            {
                donvi += "Ban Quản lý dự án Lưới điện Hà Nội - XDCB";
            }
            else if (bcDV == "073801")
            {
                donvi += "Ban Quản lý dự án phát triển Điện lực Hà Nội - ADB";
            }
            else if (bcDV == "073600")
            {
                donvi += "Công ty Lưới điện cao thế TP Hà Nội";
            }
            else if (bcDV == "073700")
            {
                donvi += "Công ty Dịch vụ Điện lực Hà Nội";
            }
            else
            {
                donvi += "Tổng Công ty Điện lực TP Hà Nội";
            };


            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tổng Công ty Điện lực TP Hà Nội",
                TieuDeTrai2 = donvi,
                Title = "CHI TIẾT NGUỒN VỐN ĐẦU TƯ XÂY DỰNG",
                recordsTotal = oData.Count,
                LoaiBM = "Biểu 02/ĐTXD",
                Thang = bcQuy.ToString(),
                Nam = bcNam.ToString(),
            };

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoChiTeuDVKHQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoChiTeuDVKHQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Qúy: ";
                    if (bcQuy != null )
                        Tungaydenngay += bcQuy.ToString();
                    Tungaydenngay += " - Năm ";
                    if (bcNam != null)
                        Tungaydenngay += bcNam.ToString();

                   
                    lstValues.Add(new ClsExcel(5, 0, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 0, 0));
                    lstValues.Add(new ClsExcel(1, 0, donvi, StyleExcell.TextCenter, false, 12, false, 0, 0));
                  
                    lstValues.Add(new ClsExcel(7, 8, "Tổng số: " +oData.Count+" bản ghi", StyleExcell.TextDatetime, false, 12, true, 7, 7,8,9));
                    int rowStart = 10;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter,true));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.SETUP_CODE + "-" +item.LINE_DESCRIPTION, StyleExcell.TextLeft,true));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.SoDuDauNam, StyleExcell.TextLeft,true));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.QuyBaoCaoTang, StyleExcell.TextLeft,true));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.LuyKeTuDauNamTang, StyleExcell.TextLeft,true));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.LuyKeTuKhoiCongTang, StyleExcell.TextLeft,true));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.QuyBaoCaoGiam, StyleExcell.TextLeft,true));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.LuyKeTuDauNamGiam, StyleExcell.TextLeft,true));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.LuyKeTuKhoiCongGiam, StyleExcell.TextLeft,true));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.SoDuCuoiKy, StyleExcell.TextLeft,true));
                        STT++;
                        rowStart++;
                    }
                    lstValues.Add(new ClsExcel(++rowStart + 1, 0, "NGƯỜI LẬP BIỂU", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 1,0,3));
                    lstValues.Add(new ClsExcel(++rowStart + 1, 0, "(Ký ,họ tên )", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 1,0,3));
                    lstValues.Add(new ClsExcel(rowStart + 2, 0, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 8,0,3));
                    lstValues.Add(new ClsExcel(++rowStart - 1, 4, "KẾ TOÁN TRƯỞNG", StyleExcell.TextCenterBold, false, 12, true, rowStart - 1, rowStart - 1, 4, 6));
                    lstValues.Add(new ClsExcel(++rowStart - 1, 4, "(Ký ,họ tên )", StyleExcell.TextCenterBold, false, 12, true, rowStart - 1, rowStart - 1, 4, 6));
                    lstValues.Add(new ClsExcel(rowStart, 4, "", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart + 6, 4, 6));

                    string ngaythangnam = "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    lstValues.Add(new ClsExcel(++rowStart - 4, 7, ngaythangnam, StyleExcell.TextDatetime, false, 12, true, rowStart - 4, rowStart - 4, 7, 9));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 7, "GIÁM ĐỐC", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart - 4, 7, 9));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 7, "(Ký ,họ tên )", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart - 4, 7, 9));
                    lstValues.Add(new ClsExcel(rowStart - 3, 7, "", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart + 3, 7, 9));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "ChiTietNguonVonDauTuXayDung_" + DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        //bao cao cong trinh sua chua lon hoan thanh
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_BCCongTrinhSuaChuaLonHT()
        {
            BCCongTrinhSuaChuaLonHTQuery oBCCongTrinhSuaChuaLonHTQuery = new BCCongTrinhSuaChuaLonHTQuery(HttpContext.Current.Request);

            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/ERP_BCCongTrinhSCLHT";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBCCongTrinhSuaChuaLonHTQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            var bcThang = oBCCongTrinhSuaChuaLonHTQuery.ThangBaoCao;
            var bcNam = oBCCongTrinhSuaChuaLonHTQuery.SearchNam;
            var bcDV = oBCCongTrinhSuaChuaLonHTQuery.LoaiDV;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcDV + "/" + bcThang + "-" + bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapBCCongTrinhSuaChuaLonHTJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(BCCongTrinhSuaChuaLonHTJson),
                    new Dictionary<string, string>
                    {
                        {"TenCongTrinh", "LINE_DESCRIPTION"},
                        {"MaCongTrinh", "DISPLAY_ATT1"},
                        {"DuToanDuocDuyet", "NUM1"},
                        {"KHDuyetNamNay", "NUM2"},
                        {"DoDangNamTruoc", "NUM3"},
                        {"DaChiNamNay", "NUM4"},
                        {"VatLieuLKDC", "NUM5"},
                        {"NhanCongLKDC", "NUM6"},
                        {"MayThiCongLKDC", "NUM7"},
                        {"ChiPhiKhacLKDC", "NUM8"},
                        {"CongLKDC", "RRD.NUM5+RRD.NUM6+RRD.NUM7+RRD.NUM8"},
                        {"GiaTriQuyetToanDuocDuyet", "NUM9"},

                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<BCCongTrinhSuaChuaLonHTJson>>(string1resultUP, settings);
            string donvi = "";
            if (bcDV == "all")
            {
                donvi += "";
            }
            else if (bcDV == "074000")
            {
                donvi += "Tổng Công ty điện lực TP Hà Nội";
            }
            else if (bcDV == "070600")
            {
                donvi += "Công ty Điện lực Thanh Trì";
            }
            else if (bcDV == "000100")
            {
                donvi += "Tập đoàn Điện lực Việt Nam - Kế toán ngành";
            }
            else if (bcDV == "070700")
            {
                donvi += "Công ty Điện lực Gia Lâm";
            }
            else if (bcDV == "070800")
            {
                donvi += "Công ty Điện lực Đông Anh ";
            }
            else if (bcDV == "071400")
            {
                donvi += "Công ty Điện lực Long Biên";
            }
            else if (bcDV == "070900")
            {
                donvi += "Công ty Điện lực Sóc Sơn";
            }
            else if (bcDV == "071800")
            {
                donvi += "Công ty Điện lực Chương Mỹ";
            }
            else if (bcDV == "072000")
            {
                donvi += "Công ty Điện lực Thường Tín";
            }
            else if (bcDV == "072400")
            {
                donvi += "Công ty Điện lực Mỹ Đức";
            }
            else if (bcDV == "072600")
            {
                donvi += "Công ty Điện lực Phúc Thọ";
            }
            else if (bcDV == "072500")
            {
                donvi += "Công ty Điện lực Phú Xuyên";
            }
            else if (bcDV == "072800")
            {
                donvi += "Công ty Điện lực Thanh Oai";
            }
            else if (bcDV == "072900")
            {
                donvi += "Công ty Điện lực Ứng Hòa";
            }
            else if (bcDV == "070100")
            {
                donvi += "Công ty Điện lực Hoàn Kiếm ";
            }
            else if (bcDV == "071500")
            {
                donvi += "Công ty Điện lực Mê Linh";
            }
            else if (bcDV == "071700")
            {
                donvi += "Công ty Điện lực Sơn Tây";
            }
            else if (bcDV == "071900")
            {
                donvi += "Công ty Điện lực Thạch Thất";
            }
            else if (bcDV == "072100")
            {
                donvi += "Công ty Điện lực Ba Vì";
            }
            else if (bcDV == "072200")
            {
                donvi += "Công ty Điện lực Đan Phượng";
            }
            else if (bcDV == "072300")
            {
                donvi += "Công ty Điện lực Hoài Đức";
            }
            else if (bcDV == "072700")
            {
                donvi += "Công ty Điện lực Quốc Oai ";
            }
            else if (bcDV == "070400")
            {
                donvi += "Công ty Điện lực Đống Đa";
            }
            else if (bcDV == "070500")
            {
                donvi += "Công ty Điện lực Nam Từ Liêm";
            }
            else if (bcDV == "070200")
            {
                donvi += "Công ty Điện lực Hai Bà Trưng";
            }
            else if (bcDV == "071000")
            {
                donvi += "Công ty Điện lực Tây Hồ";
            }
            else if (bcDV == "071100")
            {
                donvi += "Công ty Điện lực Thanh Xuân";
            }
            else if (bcDV == "071200")
            {
                donvi += "Công ty Điện lực Cầu Giấy";
            }
            else if (bcDV == "073000")
            {
                donvi += "Công ty Điện lực Bắc Từ Liêm";
            }
            else if (bcDV == "071300")
            {
                donvi += "Công ty Điện lực Hoàng Mai";
            }
            else if (bcDV == "070300")
            {
                donvi += "Công ty Điện lực Ba Đình";
            }
            else if (bcDV == "071600")
            {
                donvi += "Công ty Điện lực Hà Đông";
            }
            else if (bcDV == "073100")
            {
                donvi += "Công ty Công nghệ thông tin Điện lực Hà Nội";
            }
            else if (bcDV == "073901")
            {
                donvi += "Ban Quản lý dự án Lưới điện Hà Nội - DEP";
            }
            else if (bcDV == "073500")
            {
                donvi += "Công ty Thí nghiệm điện Điện lực Hà Nội";
            }
            else if (bcDV == "073802")
            {
                donvi += "Ban Quản lý dự án phát triển Điện lực Hà Nội - XDCB";
            }
            else if (bcDV == "073200")
            {
                donvi += "Trung tâm Điều độ Hệ thống điện TP Hà Nội";
            }
            else if (bcDV == "073902")
            {
                donvi += "Ban Quản lý dự án Lưới điện Hà Nội - XDCB";
            }
            else if (bcDV == "073801")
            {
                donvi += "Ban Quản lý dự án phát triển Điện lực Hà Nội - ADB";
            }
            else if (bcDV == "073600")
            {
                donvi += "Công ty Lưới điện cao thế TP Hà Nội";
            }
            else if (bcDV == "073700")
            {
                donvi += "Công ty Dịch vụ Điện lực Hà Nội";
            }
            else
            {
                donvi += "Tổng Công ty Điện lực TP Hà Nội";
            };


            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tổng Công ty Điện lực TP Hà Nội",
                TieuDeTrai2 = donvi,
                Title = "BÁO CÁO CÔNG TRÌNH SỬA CHỮA LỚN HOÀN THÀNH",
                LoaiBM = "Biểu 10B/THKT",
                DVTinh = "ĐVT: Đồng",
                NguoiLapBieu = CurentUser.Title,
                Thang = bcThang.ToString(),
                Nam = bcNam.ToString(),
                recordsTotal = oData.Count
            };

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBCCongTrinhSuaChuaLonHTQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBCCongTrinhSuaChuaLonHTQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng : ";
                    if (bcThang != null)
                        Tungaydenngay += bcThang.ToString();
                    Tungaydenngay += " - Năm ";
                    if (bcNam != null)
                        Tungaydenngay += bcNam.ToString();
                   

                    lstValues.Add(new ClsExcel(2, 3, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 2, 2));
                    lstValues.Add(new ClsExcel(2, 0, donvi, StyleExcell.TextCenter, false, 12, false, 2, 2));
                  
                    int rowStart = 7;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, item.TenCongTrinh, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.MaCongTrinh, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.DuToanDuocDuyet, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.KHDuyetNamNay, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.DoDangNamTruoc, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.DaChiNamNay, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.VatLieuLKDC, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.NhanCongLKDC, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.MayThiCongLKDC, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.ChiPhiKhacLKDC, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.CongLKDC, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.GiaTriQuyetToanDuocDuyet, StyleExcell.TextLeft));
                        STT++;
                        rowStart++;
                    }

                    lstValues.Add(new ClsExcel(4, 10, "Có tổng số: " + oData.Count + " bản ghi", StyleExcell.TextRight, false, 12, false, 4, 4));



                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "NGƯỜI LẬP BIỂU ", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 2, 0, 1));
                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 6, 0, 1));
                    lstValues.Add(new ClsExcel(rowStart + 7, 0, CurentUser.Title, StyleExcell.TextCenterBold, false, 12, true, rowStart + 7, rowStart + 7, 0, 1));
                    lstValues.Add(new ClsExcel(rowStart + 8, 0, "(Ký, họ tên)", StyleExcell.TextCenterBold, false, 12, true, rowStart + 8, rowStart + 8, 0, 1));

                    lstValues.Add(new ClsExcel(++rowStart, 2, " KẾ TOÁN TRƯỞNG", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart, 2, 6));
                    lstValues.Add(new ClsExcel(++rowStart, 2, "", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart + 4, 2, 6));
                    lstValues.Add(new ClsExcel(rowStart + 5, 2, dataGiaoDienRender.KeToanTruong, StyleExcell.TextCenterBold, false, 12, true, rowStart + 5, rowStart + 5, 2, 6));
                    lstValues.Add(new ClsExcel(rowStart + 6, 2, "(Ký, họ tên)", StyleExcell.TextCenterBold, false, 12, true, rowStart + 6, rowStart + 6, 2, 6));

                    string ngaythangnam = "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    lstValues.Add(new ClsExcel(++rowStart - 3, 7, ngaythangnam, StyleExcell.TextDatetime, false, 12, true, rowStart - 3, rowStart - 3, 7, 11));
                    lstValues.Add(new ClsExcel(++rowStart - 3, 7, "GIÁM ĐỐC", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart - 3, 7, 11));
                    lstValues.Add(new ClsExcel(++rowStart - 3, 7, "", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart +1, 7 ,11));
                    //lstValues.Add(new ClsExcel(rowStart + 1, 2, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 2, 1, 1));
                    lstValues.Add(new ClsExcel(rowStart+2 , 7, dataGiaoDienRender.NguoiKy, StyleExcell.TextCenterBold, false, 12, true, rowStart+2 , rowStart+2 , 7, 11));
                    lstValues.Add(new ClsExcel(rowStart+3 , 7, "(Ký, họ tên, đóng dấu)", StyleExcell.TextCenterBold, false, 12, true, rowStart+3 , rowStart+3 , 7, 11));

                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "BCCongTrinhSuaChuaLonHT_"+ DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        //bao cao cong trinh sua chua lon Do dang
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_BCCongTinhSuaChuaLonDoDang()
        {
            BCCongTinhSuaChuaLonDoDangQuery oBCCongTinhSuaChuaLonDoDangQuery = new BCCongTinhSuaChuaLonDoDangQuery(HttpContext.Current.Request);
            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/ERP_BCCongTrinhSCLDD";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBCCongTinhSuaChuaLonDoDangQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var bcNam = oBCCongTinhSuaChuaLonDoDangQuery.SearchNam;
            var bcQuy = oBCCongTinhSuaChuaLonDoDangQuery.QuyBaoCao;
            var bcDV = oBCCongTinhSuaChuaLonDoDangQuery.LoaiDV;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcDV + "/Q" + bcQuy + "_" + bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapBCCongTinhSuaChuaLonDoDangJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(BCCongTinhSuaChuaLonDoDangJson),
                    new Dictionary<string, string>
                    {
                        {"TenCongTrinh","LINE_DESCRIPTION"},
                        {"MaCongTrinh","DISPLAY_ATT1"},
                        {"DuToanDuocDuyet","NUM1"},
                        {"KHDuyetNamNay","NUM2"},
                        {"DoDangNamTruoc","NUM3"},
                        {"DaChiNamNay","NUM4"},
                        {"VatLieuLKDC","NUM5"},
                        {"NhanCongLKDC","NUM6"},
                        {"MayThiCongLKDC","NUM7"},
                        {"ChiPhiKhacLKDC","NUM8"},
                        {"CongLKDC","NUM9"},
                       // {"GhiChu","STRUCT"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<BCCongTinhSuaChuaLonDoDangJson>>(string1resultUP, settings);
            //List<BCCongTinhSuaChuaLonDoDangJson> oData = new List<BCCongTinhSuaChuaLonDoDangJson>()
            //{

            //    new BCCongTinhSuaChuaLonDoDangJson() {TenCongTrinh=" XD hệ thống ĐKGS SCADA",MaCongTrinh="HAN.HDU.CTA53A5321208"},
            // };
            string donvi = "";
            if (bcDV == "all")
            {
                donvi += "";
            }
            else if (bcDV == "074000")
            {
                donvi += "Tổng Công ty điện lực TP Hà Nội";
            }
            else if (bcDV == "070600")
            {
                donvi += "Công ty Điện lực Thanh Trì";
            }
            else if (bcDV == "000100")
            {
                donvi += "Tập đoàn Điện lực Việt Nam - Kế toán ngành";
            }
            else if (bcDV == "070700")
            {
                donvi += "Công ty Điện lực Gia Lâm";
            }
            else if (bcDV == "070800")
            {
                donvi += "Công ty Điện lực Đông Anh ";
            }
            else if (bcDV == "071400")
            {
                donvi += "Công ty Điện lực Long Biên";
            }
            else if (bcDV == "070900")
            {
                donvi += "Công ty Điện lực Sóc Sơn";
            }
            else if (bcDV == "071800")
            {
                donvi += "Công ty Điện lực Chương Mỹ";
            }
            else if (bcDV == "072000")
            {
                donvi += "Công ty Điện lực Thường Tín";
            }
            else if (bcDV == "072400")
            {
                donvi += "Công ty Điện lực Mỹ Đức";
            }
            else if (bcDV == "072600")
            {
                donvi += "Công ty Điện lực Phúc Thọ";
            }
            else if (bcDV == "072500")
            {
                donvi += "Công ty Điện lực Phú Xuyên";
            }
            else if (bcDV == "072800")
            {
                donvi += "Công ty Điện lực Thanh Oai";
            }
            else if (bcDV == "072900")
            {
                donvi += "Công ty Điện lực Ứng Hòa";
            }
            else if (bcDV == "070100")
            {
                donvi += "Công ty Điện lực Hoàn Kiếm ";
            }
            else if (bcDV == "071500")
            {
                donvi += "Công ty Điện lực Mê Linh";
            }
            else if (bcDV == "071700")
            {
                donvi += "Công ty Điện lực Sơn Tây";
            }
            else if (bcDV == "071900")
            {
                donvi += "Công ty Điện lực Thạch Thất";
            }
            else if (bcDV == "072100")
            {
                donvi += "Công ty Điện lực Ba Vì";
            }
            else if (bcDV == "072200")
            {
                donvi += "Công ty Điện lực Đan Phượng";
            }
            else if (bcDV == "072300")
            {
                donvi += "Công ty Điện lực Hoài Đức";
            }
            else if (bcDV == "072700")
            {
                donvi += "Công ty Điện lực Quốc Oai ";
            }
            else if (bcDV == "070400")
            {
                donvi += "Công ty Điện lực Đống Đa";
            }
            else if (bcDV == "070500")
            {
                donvi += "Công ty Điện lực Nam Từ Liêm";
            }
            else if (bcDV == "070200")
            {
                donvi += "Công ty Điện lực Hai Bà Trưng";
            }
            else if (bcDV == "071000")
            {
                donvi += "Công ty Điện lực Tây Hồ";
            }
            else if (bcDV == "071100")
            {
                donvi += "Công ty Điện lực Thanh Xuân";
            }
            else if (bcDV == "071200")
            {
                donvi += "Công ty Điện lực Cầu Giấy";
            }
            else if (bcDV == "073000")
            {
                donvi += "Công ty Điện lực Bắc Từ Liêm";
            }
            else if (bcDV == "071300")
            {
                donvi += "Công ty Điện lực Hoàng Mai";
            }
            else if (bcDV == "070300")
            {
                donvi += "Công ty Điện lực Ba Đình";
            }
            else if (bcDV == "071600")
            {
                donvi += "Công ty Điện lực Hà Đông";
            }
            else if (bcDV == "073100")
            {
                donvi += "Công ty Công nghệ thông tin Điện lực Hà Nội";
            }
            else if (bcDV == "073901")
            {
                donvi += "Ban Quản lý dự án Lưới điện Hà Nội - DEP";
            }
            else if (bcDV == "073500")
            {
                donvi += "Công ty Thí nghiệm điện Điện lực Hà Nội";
            }
            else if (bcDV == "073802")
            {
                donvi += "Ban Quản lý dự án phát triển Điện lực Hà Nội - XDCB";
            }
            else if (bcDV == "073200")
            {
                donvi += "Trung tâm Điều độ Hệ thống điện TP Hà Nội";
            }
            else if (bcDV == "073902")
            {
                donvi += "Ban Quản lý dự án Lưới điện Hà Nội - XDCB";
            }
            else if (bcDV == "073801")
            {
                donvi += "Ban Quản lý dự án phát triển Điện lực Hà Nội - ADB";
            }
            else if (bcDV == "073600")
            {
                donvi += "Công ty Lưới điện cao thế TP Hà Nội";
            }
            else if (bcDV == "073700")
            {
                donvi += "Công ty Dịch vụ Điện lực Hà Nội";
            }
            else
            {
                donvi += "Tổng Công ty Điện lực TP Hà Nội";
            };

            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tổng Công ty Điện lực TP Hà Nội",
                TieuDeTrai2 = donvi,
                Title = "BÁO CÁO CÔNG TRÌNH SỬA CHỮA LỚN DỞ DANG",
                LoaiBM = "Biểu 10A/THKT",
                DVTinh = "ĐVT: Đồng",
                Thang = bcQuy.ToString(),
                Nam = bcNam.ToString(),
                recordsTotal = oData.Count,
                NguoiLapBieu = CurentUser.Title
            };

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBCCongTinhSuaChuaLonDoDangQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBCCongTinhSuaChuaLonDoDangQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Qúy: ";
                    if (bcQuy != null)
                        Tungaydenngay += bcQuy.ToString();
                    Tungaydenngay += " - Năm:  ";
                    if (bcNam != null)
                        Tungaydenngay += bcNam.ToString();
                   
                    lstValues.Add(new ClsExcel(2, 3, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 2, 2));
                    lstValues.Add(new ClsExcel(2, 0,donvi, StyleExcell.TextCenter, false, 12, false, 2, 2));
                
                    int rowStart = 7;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, item.TenCongTrinh, StyleExcell.TextLeft, true));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.MaCongTrinh, StyleExcell.TextLeft,true));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.DuToanDuocDuyet, StyleExcell.TextLeft,true));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.KHDuyetNamNay, StyleExcell.TextLeft,true));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.DoDangNamTruoc, StyleExcell.TextLeft,true));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.DaChiNamNay, StyleExcell.TextLeft,true));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.VatLieuLKDC, StyleExcell.TextLeft,true));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.NhanCongLKDC, StyleExcell.TextLeft,true));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.MayThiCongLKDC, StyleExcell.TextLeft,true));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.ChiPhiKhacLKDC, StyleExcell.TextLeft,true));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.CongLKDC, StyleExcell.TextLeft,true));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.GhiChu, StyleExcell.TextLeft,true));
                        STT++;
                        rowStart++;
                    }
                    lstValues.Add(new ClsExcel(4, 10, "Có tổng số: " + oData.Count + " bản ghi", StyleExcell.TextRight, false, 12, false, 4, 4));
                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "NGƯỜI LẬP BIỂU ", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 2, 0, 1));
                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 6, 0, 1));
                    lstValues.Add(new ClsExcel(rowStart + 7, 0,CurentUser.Title, StyleExcell.TextCenterBold, false, 12, true, rowStart + 7, rowStart + 7, 0, 1));
                    lstValues.Add(new ClsExcel(rowStart + 8, 0, "(Ký, họ tên)", StyleExcell.TextCenterBold, false, 12, true, rowStart + 8, rowStart + 8, 0, 1));

                    lstValues.Add(new ClsExcel(++rowStart, 2, " KẾ TOÁN TRƯỞNG", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart, 2, 6));
                    lstValues.Add(new ClsExcel(++rowStart, 2, "", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart + 4, 2, 6));
                    lstValues.Add(new ClsExcel(rowStart + 5, 2, dataGiaoDienRender.KeToanTruong, StyleExcell.TextCenterBold, false, 12, true, rowStart + 5, rowStart + 5, 2, 6));
                    lstValues.Add(new ClsExcel(rowStart + 6, 2, "(Ký, họ tên)", StyleExcell.TextCenterBold, false, 12, true, rowStart + 6, rowStart + 6, 2, 6));

                    string ngaythangnam = "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    lstValues.Add(new ClsExcel(++rowStart - 3, 7, ngaythangnam, StyleExcell.TextDatetime, false, 12, true, rowStart - 3, rowStart - 3, 7, 11));
                    lstValues.Add(new ClsExcel(++rowStart - 3, 7, "GIÁM ĐỐC", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart - 3, 7, 11));
                    lstValues.Add(new ClsExcel(++rowStart - 3, 7, "", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart +1, 7 ,11));
                    //lstValues.Add(new ClsExcel(rowStart + 1, 2, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 2, 1, 1));
                    lstValues.Add(new ClsExcel(rowStart+2 , 7, dataGiaoDienRender.NguoiKy, StyleExcell.TextCenterBold, false, 12, true, rowStart+2 , rowStart+2 , 7, 11));
                    lstValues.Add(new ClsExcel(rowStart+3 , 7, "(Ký, họ tên, đóng dấu)", StyleExcell.TextCenterBold, false, 12, true, rowStart+3 , rowStart+3 , 7, 11));

                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "BCCongTrinhSuaChuaLonHT_" + DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
         //bao cao cong trinh Thuoc nguon von sua chua lon 
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_BCCongTrinhThuocNguonVonSuaChuaLon()
        {
            BCCongTrinhThuocNguonVonSuaChuaLonQuery oBCCongTrinhThuocNguonVonSuaChuaLonQuery = new BCCongTrinhThuocNguonVonSuaChuaLonQuery(HttpContext.Current.Request);

            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/ERP_BCCongTrinhSCL";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBCCongTrinhThuocNguonVonSuaChuaLonQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            var bcThang = oBCCongTrinhThuocNguonVonSuaChuaLonQuery.ThangBaoCao;
            var bcNam = oBCCongTrinhThuocNguonVonSuaChuaLonQuery.SearchNam;
            var bcDV = oBCCongTrinhThuocNguonVonSuaChuaLonQuery.LoaiDV;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcDV + "/" + bcThang + "-" + bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapBCCongTrinhThuocNguonVonSuaChuaLonJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(BCCongTrinhThuocNguonVonSuaChuaLonJson),
                    new Dictionary<string, string>
                    {
                        {"TenCongTrinh", "LINE_DESCRIPTION"},
                        {"MaCongTrinh", "DISPLAY_ATT1"},
                        {"DuAnDuocDuyet", "NUM1"},
                        {"KHDuyetNamNay", "NUM2"},
                        {"SoDuDauNam", "NUM3"},
                        {"DaTriNamNayTrongKyBaoCao", "NUM4"},
                        {"VatLieuTuDauNam", "NUM5"},
                        {"NhanCongTuDauNam", "NUM6"},
                        {"MayThiCongTuDauNam", "NUM7"},
                        {"KhacTuDauNam", "NUM8"},
                        {"CongTuDauNam", "NUM9"},
                        {"VatLieuLKDC", "NUM10"},
                        {"NhanCongLKDC", "NUM11"},
                        {"MayThiCongLKDC", "NUM12"},
                        {"KhacLKDC", "NUM13"},
                        {"CongLKDC", "NUM14"},
                        //{"GhiChu", "NUM15"},
                        //{"TongHopChung", "NUM15"},
                        //{"SoSanhPhanTram", "NUM15"},

                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };
            string donvi = "";
            if (bcDV == "all")
            {
                donvi += "";
            }
            else if (bcDV == "074000")
            {
                donvi += "Tổng Công ty điện lực TP Hà Nội";
            }
            else if (bcDV == "070600")
            {
                donvi += "Công ty Điện lực Thanh Trì";
            }
            else if (bcDV == "000100")
            {
                donvi += "Tập đoàn Điện lực Việt Nam - Kế toán ngành";
            }
            else if (bcDV == "070700")
            {
                donvi += "Công ty Điện lực Gia Lâm";
            }
            else if (bcDV == "070800")
            {
                donvi += "Công ty Điện lực Đông Anh ";
            }
            else if (bcDV == "071400")
            {
                donvi += "Công ty Điện lực Long Biên";
            }
            else if (bcDV == "070900")
            {
                donvi += "Công ty Điện lực Sóc Sơn";
            }
            else if (bcDV == "071800")
            {
                donvi += "Công ty Điện lực Chương Mỹ";
            }
            else if (bcDV == "072000")
            {
                donvi += "Công ty Điện lực Thường Tín";
            }
            else if (bcDV == "072400")
            {
                donvi += "Công ty Điện lực Mỹ Đức";
            }
            else if (bcDV == "072600")
            {
                donvi += "Công ty Điện lực Phúc Thọ";
            }
            else if (bcDV == "072500")
            {
                donvi += "Công ty Điện lực Phú Xuyên";
            }
            else if (bcDV == "072800")
            {
                donvi += "Công ty Điện lực Thanh Oai";
            }
            else if (bcDV == "072900")
            {
                donvi += "Công ty Điện lực Ứng Hòa";
            }
            else if (bcDV == "070100")
            {
                donvi += "Công ty Điện lực Hoàn Kiếm ";
            }
            else if (bcDV == "071500")
            {
                donvi += "Công ty Điện lực Mê Linh";
            }
            else if (bcDV == "071700")
            {
                donvi += "Công ty Điện lực Sơn Tây";
            }
            else if (bcDV == "071900")
            {
                donvi += "Công ty Điện lực Thạch Thất";
            }
            else if (bcDV == "072100")
            {
                donvi += "Công ty Điện lực Ba Vì";
            }
            else if (bcDV == "072200")
            {
                donvi += "Công ty Điện lực Đan Phượng";
            }
            else if (bcDV == "072300")
            {
                donvi += "Công ty Điện lực Hoài Đức";
            }
            else if (bcDV == "072700")
            {
                donvi += "Công ty Điện lực Quốc Oai ";
            }
            else if (bcDV == "070400")
            {
                donvi += "Công ty Điện lực Đống Đa";
            }
            else if (bcDV == "070500")
            {
                donvi += "Công ty Điện lực Nam Từ Liêm";
            }
            else if (bcDV == "070200")
            {
                donvi += "Công ty Điện lực Hai Bà Trưng";
            }
            else if (bcDV == "071000")
            {
                donvi += "Công ty Điện lực Tây Hồ";
            }
            else if (bcDV == "071100")
            {
                donvi += "Công ty Điện lực Thanh Xuân";
            }
            else if (bcDV == "071200")
            {
                donvi += "Công ty Điện lực Cầu Giấy";
            }
            else if (bcDV == "073000")
            {
                donvi += "Công ty Điện lực Bắc Từ Liêm";
            }
            else if (bcDV == "071300")
            {
                donvi += "Công ty Điện lực Hoàng Mai";
            }
            else if (bcDV == "070300")
            {
                donvi += "Công ty Điện lực Ba Đình";
            }
            else if (bcDV == "071600")
            {
                donvi += "Công ty Điện lực Hà Đông";
            }
            else if (bcDV == "073100")
            {
                donvi += "Công ty Công nghệ thông tin Điện lực Hà Nội";
            }
            else if (bcDV == "073901")
            {
                donvi += "Ban Quản lý dự án Lưới điện Hà Nội - DEP";
            }
            else if (bcDV == "073500")
            {
                donvi += "Công ty Thí nghiệm điện Điện lực Hà Nội";
            }
            else if (bcDV == "073802")
            {
                donvi += "Ban Quản lý dự án phát triển Điện lực Hà Nội - XDCB";
            }
            else if (bcDV == "073200")
            {
                donvi += "Trung tâm Điều độ Hệ thống điện TP Hà Nội";
            }
            else if (bcDV == "073902")
            {
                donvi += "Ban Quản lý dự án Lưới điện Hà Nội - XDCB";
            }
            else if (bcDV == "073801")
            {
                donvi += "Ban Quản lý dự án phát triển Điện lực Hà Nội - ADB";
            }
            else if (bcDV == "073600")
            {
                donvi += "Công ty Lưới điện cao thế TP Hà Nội";
            }
            else if (bcDV == "073700")
            {
                donvi += "Công ty Dịch vụ Điện lực Hà Nội";
            }
            else
            {
                donvi += "Tổng Công ty Điện lực TP Hà Nội";
            };

            var oData = JsonConvert.DeserializeObject<List<BCCongTrinhThuocNguonVonSuaChuaLonJson>>(model.data, settings);
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tổng Công ty Điện lực TP Hà Nội",
                TieuDeTrai2 = donvi,
                Title = "BÁO CÁO CÁC CÔNG TRÌNH THUỘC NGUỒN VỐN SỬA CHỮA LỚN",
                LoaiBM = "Biểu 10/THKT",
                DVTinh = "ĐVT: Đồng",
                Thang = bcThang.ToString(),
                NguoiLapBieu = CurentUser.Title,
                Nam = bcNam.ToString(),
                recordsTotal = oData.Count
            }; 

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBCCongTrinhThuocNguonVonSuaChuaLonQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBCCongTrinhThuocNguonVonSuaChuaLonQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng : ";
                    if (bcThang != null)
                        Tungaydenngay += bcThang.ToString();
                    Tungaydenngay += " - Năm ";
                    if (bcNam != null)
                        Tungaydenngay += bcNam.ToString();
                   

                    lstValues.Add(new ClsExcel(2, 3, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 2, 2));
                    lstValues.Add(new ClsExcel(2, 0, donvi, StyleExcell.TextCenterBold, false, 12, false, 2, 2));
                   
                    int rowStart = 7;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, item.TenCongTrinh, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.MaCongTrinh, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.DuAnDuocDuyet, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.KHDuyetNamNay, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.SoDuDauNam, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.DaTriNamNayTrongKyBaoCao, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.VatLieuTuDauNam, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.NhanCongTuDauNam, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.MayThiCongTuDauNam, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.KhacTuDauNam, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.CongTuDauNam, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.VatLieuLKDC, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 12, item.NhanCongLKDC, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 13, item.MayThiCongLKDC, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 14, item.KhacLKDC, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 15, item.CongLKDC, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 16, item.GhiChu, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 17, item.TongHopChung, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 18, item.SoSanhPhanTram, StyleExcell.TextCenter));

                        STT++;
                        rowStart++;
                    }


                    lstValues.Add(new ClsExcel(3, 17, "Có tổng số: " + oData.Count + " bản ghi", StyleExcell.TextCenter, false, 12, true, 3, 3, 17, 18));



                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "NGƯỜI LẬP BIỂU ", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 2, 0, 3));
                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 6, 0, 3));
                    lstValues.Add(new ClsExcel(rowStart + 7, 0, CurentUser.Title, StyleExcell.TextCenterBold, false, 12, true, rowStart + 7, rowStart + 7, 0, 3));
                    lstValues.Add(new ClsExcel(rowStart + 8, 0, "(Ký, họ tên)", StyleExcell.TextCenterBold, false, 12, true, rowStart + 8, rowStart + 8, 0, 3));

                    lstValues.Add(new ClsExcel(++rowStart, 4, " KẾ TOÁN TRƯỞNG", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart, 4, 11));
                    lstValues.Add(new ClsExcel(++rowStart, 4, "", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart + 4, 4, 11));
                    //lstValues.Add(new ClsExcel(rowStart + 5, 4, dataGiaoDienRender.KeToanTruong, StyleExcell.TextCenterBold, false, 12, true, rowStart + 5, rowStart + 5, 4, 11));
                    lstValues.Add(new ClsExcel(rowStart + 6, 4, "(Ký, họ tên)", StyleExcell.TextCenterBold, false, 12, true, rowStart + 6, rowStart + 6, 4, 11));

                    string ngaythangnam = "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    lstValues.Add(new ClsExcel(++rowStart - 3, 12, ngaythangnam, StyleExcell.TextDatetime, false, 12, true, rowStart - 3, rowStart - 3, 12, 18));
                    lstValues.Add(new ClsExcel(++rowStart - 3, 12, "GIÁM ĐỐC", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart - 3, 12, 18));
                    lstValues.Add(new ClsExcel(++rowStart - 3, 12, "", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart + 1, 12, 18));
                    //lstValues.Add(new ClsExcel(rowStart + 1, 2, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 2, 1, 1));
                    //lstValues.Add(new ClsExcel(rowStart + 2, 12, dataGiaoDienRender.NguoiKy, StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 2, 12, 18));
                    lstValues.Add(new ClsExcel(rowStart + 3, 12, "(Ký, họ tên, đóng dấu)", StyleExcell.TextCenterBold, false, 12, true, rowStart + 3, rowStart + 3, 12, 18));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "BCCongTrinhThuocNguonVonSuaChuaLon_"+ DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
         // Cong Trinh hang muc cong trinh hoan thanh ban giao
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_CTHMCongTrinhHoanThanhBanGiao()
        {
            CTHMCongTrinhHoanThanhBanGiaoQuery oCTHMCongTrinhHoanThanhBanGiaoQuery = new CTHMCongTrinhHoanThanhBanGiaoQuery(HttpContext.Current.Request);
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/ERP_CongTrinhHangMucCongTrinhHTBG";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oCTHMCongTrinhHoanThanhBanGiaoQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            var bcThang = oCTHMCongTrinhHoanThanhBanGiaoQuery.ThangBaoCao;
            var bcNam = oCTHMCongTrinhHoanThanhBanGiaoQuery.SearchNam;
            var bcDV = oCTHMCongTrinhHoanThanhBanGiaoQuery.LoaiDV;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI+"/"+bcDV+"/"+bcThang+"-"+bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapCTHMCongTrinhHoanThanhBanGiaoJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(CTHMCongTrinhHoanThanhBanGiaoJson),
                    new Dictionary<string, string>
                    {
                        {"TenHangMucCongTrinh", "LINE_DESCRIPTION"},
                        {"NgayKhoiCong", "DATE1"},
                        {"NgayHoanThanhBGDV", "DATE2"},
                        {"DonViSuDungBGDV", "TEXT3"},
                        {"CoQuanQuyetDinhDauTu", "TEXT4"},
                        {"TongDuToanDuocDuyet","NUM1"},
                       {"ChiPhiDauTuDaHoanThanhChuaPheDuyetQTDN","NUM2"},
                        {"PhatSinhTrongKyChuaPheDuyetQT","NUM3"},
                        {"LuyKeTuDauNamDenCuoiKyChuaPheDuyetQT" ,"NUM4"},
                        {"PhatSinhTrongKyDaPheDuyetQT","NUM5"},
                       {"LuyKeTuDauNamDenCuoiKyDaPheDuyetQT","NUM6"},
                        {"SoDuCPTHDauTuDaHoanThanhBanGiaoChuaPheDuyetQTCK","NUM7"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<CTHMCongTrinhHoanThanhBanGiaoJson>>(model.data, settings);
            string donvi = "";
            if (bcDV == "all")
            {
                donvi += "";
            }
            else if (bcDV == "074000")
            {
                donvi += "Tổng Công ty điện lực TP Hà Nội";
            }
            else if (bcDV == "070600")
            {
                donvi += "Công ty Điện lực Thanh Trì";
            }
            else if (bcDV == "000100")
            {
                donvi += "Tập đoàn Điện lực Việt Nam - Kế toán ngành";
            }
            else if (bcDV == "070700")
            {
                donvi += "Công ty Điện lực Gia Lâm";
            }
            else if (bcDV == "070800")
            {
                donvi += "Công ty Điện lực Đông Anh ";
            }
            else if (bcDV == "071400")
            {
                donvi += "Công ty Điện lực Long Biên";
            }
            else if (bcDV == "070900")
            {
                donvi += "Công ty Điện lực Sóc Sơn";
            }
            else if (bcDV == "071800")
            {
                donvi += "Công ty Điện lực Chương Mỹ";
            }
            else if (bcDV == "072000")
            {
                donvi += "Công ty Điện lực Thường Tín";
            }
            else if (bcDV == "072400")
            {
                donvi += "Công ty Điện lực Mỹ Đức";
            }
            else if (bcDV == "072600")
            {
                donvi += "Công ty Điện lực Phúc Thọ";
            }
            else if (bcDV == "072500")
            {
                donvi += "Công ty Điện lực Phú Xuyên";
            }
            else if (bcDV == "072800")
            {
                donvi += "Công ty Điện lực Thanh Oai";
            }
            else if (bcDV == "072900")
            {
                donvi += "Công ty Điện lực Ứng Hòa";
            }
            else if (bcDV == "070100")
            {
                donvi += "Công ty Điện lực Hoàn Kiếm ";
            }
            else if (bcDV == "071500")
            {
                donvi += "Công ty Điện lực Mê Linh";
            }
            else if (bcDV == "071700")
            {
                donvi += "Công ty Điện lực Sơn Tây";
            }
            else if (bcDV == "071900")
            {
                donvi += "Công ty Điện lực Thạch Thất";
            }
            else if (bcDV == "072100")
            {
                donvi += "Công ty Điện lực Ba Vì";
            }
            else if (bcDV == "072200")
            {
                donvi += "Công ty Điện lực Đan Phượng";
            }
            else if (bcDV == "072300")
            {
                donvi += "Công ty Điện lực Hoài Đức";
            }
            else if (bcDV == "072700")
            {
                donvi += "Công ty Điện lực Quốc Oai ";
            }
            else if (bcDV == "070400")
            {
                donvi += "Công ty Điện lực Đống Đa";
            }
            else if (bcDV == "070500")
            {
                donvi += "Công ty Điện lực Nam Từ Liêm";
            }
            else if (bcDV == "070200")
            {
                donvi += "Công ty Điện lực Hai Bà Trưng";
            }
            else if (bcDV == "071000")
            {
                donvi += "Công ty Điện lực Tây Hồ";
            }
            else if (bcDV == "071100")
            {
                donvi += "Công ty Điện lực Thanh Xuân";
            }
            else if (bcDV == "071200")
            {
                donvi += "Công ty Điện lực Cầu Giấy";
            }
            else if (bcDV == "073000")
            {
                donvi += "Công ty Điện lực Bắc Từ Liêm";
            }
            else if (bcDV == "071300")
            {
                donvi += "Công ty Điện lực Hoàng Mai";
            }
            else if (bcDV == "070300")
            {
                donvi += "Công ty Điện lực Ba Đình";
            }
            else if (bcDV == "071600")
            {
                donvi += "Công ty Điện lực Hà Đông";
            }
            else if (bcDV == "073100")
            {
                donvi += "Công ty Công nghệ thông tin Điện lực Hà Nội";
            }
            else if (bcDV == "073901")
            {
                donvi += "Ban Quản lý dự án Lưới điện Hà Nội - DEP";
            }
            else if (bcDV == "073500")
            {
                donvi += "Công ty Thí nghiệm điện Điện lực Hà Nội";
            }
            else if (bcDV == "073802")
            {
                donvi += "Ban Quản lý dự án phát triển Điện lực Hà Nội - XDCB";
            }
            else if (bcDV == "073200")
            {
                donvi += "Trung tâm Điều độ Hệ thống điện TP Hà Nội";
            }
            else if (bcDV == "073902")
            {
                donvi += "Ban Quản lý dự án Lưới điện Hà Nội - XDCB";
            }
            else if (bcDV == "073801")
            {
                donvi += "Ban Quản lý dự án phát triển Điện lực Hà Nội - ADB";
            }
            else if (bcDV == "073600")
            {
                donvi += "Công ty Lưới điện cao thế TP Hà Nội";
            }
            else if (bcDV == "073700")
            {
                donvi += "Công ty Dịch vụ Điện lực Hà Nội";
            }
            else
            {
                donvi += "Tổng Công ty Điện lực TP Hà Nội";
            };

            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tổng Công ty Điện lực TP Hà Nội",
                TieuDeTrai2 = donvi,
                Title = "CÔNG TRÌNH, HẠNG MỤC CÔNG TRÌNH HOÀN THÀNH BÀN GIAO",
                LoaiBM = "Biểu 06/ĐTXD",
                DVTinh = "ĐVT: Đồng",
                Thang = bcThang.ToString(),
                NguoiLapBieu = CurentUser.Title,
                Nam = bcNam.ToString(),
                recordsTotal = oData.Count
            };

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oCTHMCongTrinhHoanThanhBanGiaoQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oCTHMCongTrinhHoanThanhBanGiaoQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng : ";
                    if (bcThang != null)
                        Tungaydenngay += bcThang.ToString();
                    Tungaydenngay += " - Năm ";
                    if (bcNam != null)
                        Tungaydenngay += bcNam.ToString();
                  



                    lstValues.Add(new ClsExcel(2, 4, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 2, 2));
                    lstValues.Add(new ClsExcel(2, 0, donvi, StyleExcell.TextCenter, false, 12, false, 2, 2));
                  

                    int rowStart = 6;
                    int STT = 1; 
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.TenHangMucCongTrinh, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.NgayKhoiCong, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.NgayHoanThanhBGDV, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.DonViSuDungBGDV, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.CoQuanQuyetDinhDauTu, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.TongDuToanDuocDuyet, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.ChiPhiDauTuDaHoanThanhChuaPheDuyetQTDN, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.PhatSinhTrongKyChuaPheDuyetQT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.LuyKeTuDauNamDenCuoiKyChuaPheDuyetQT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.PhatSinhTrongKyDaPheDuyetQT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.LuyKeTuDauNamDenCuoiKyDaPheDuyetQT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 12, item.SoDuCPTHDauTuDaHoanThanhBanGiaoChuaPheDuyetQTCK, StyleExcell.TextCenter));
                       
                        STT++;
                        rowStart++;
                    }

                    lstValues.Add(new ClsExcel(3, 11, "Có tổng số: " + oData.Count + " bản ghi", StyleExcell.TextRight, false, 12, false, 3, 3));



                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "NGƯỜI LẬP BIỂU ", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 2, 0, 3));
                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 6, 0, 3));
                    lstValues.Add(new ClsExcel(rowStart + 7, 0, CurentUser.Title, StyleExcell.TextCenterBold, false, 12, true, rowStart + 7, rowStart + 7, 0, 3));
                    lstValues.Add(new ClsExcel(rowStart + 8, 0, "(Ký, họ tên)", StyleExcell.TextCenterBold, false, 12, true, rowStart + 8, rowStart + 8, 0, 3));

                    lstValues.Add(new ClsExcel(++rowStart, 4, " KẾ TOÁN TRƯỞNG", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart, 4, 8));
                    lstValues.Add(new ClsExcel(++rowStart, 4, "", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart + 4, 4, 8));
                    lstValues.Add(new ClsExcel(rowStart + 5, 4, dataGiaoDienRender.KeToanTruong, StyleExcell.TextCenterBold, false, 12, true, rowStart + 5, rowStart + 5, 4, 8));
                    lstValues.Add(new ClsExcel(rowStart + 6, 4, "(Ký, họ tên)", StyleExcell.TextCenterBold, false, 12, true, rowStart + 6, rowStart + 6, 4, 8));

                    string ngaythangnam = "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    lstValues.Add(new ClsExcel(++rowStart - 3, 9, ngaythangnam, StyleExcell.TextDatetime, false, 12, true, rowStart - 3, rowStart - 3, 9, 12));
                    lstValues.Add(new ClsExcel(++rowStart - 3, 9, "THỦ TRƯỞNG ĐƠN VỊ", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart - 3, 9, 12));
                    lstValues.Add(new ClsExcel(++rowStart - 3, 9, "", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart +1, 9 ,12));
                    //lstValues.Add(new ClsExcel(rowStart + 1, 2, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 2, 1, 1));
                    //lstValues.Add(new ClsExcel(rowStart+2 , 9, dataGiaoDienRender.NguoiKy, StyleExcell.TextCenterBold, false, 12, true, rowStart+2 , rowStart+2 , 9, 12));
                    lstValues.Add(new ClsExcel(rowStart+3 , 9, "(Ký, họ tên, đóng dấu)", StyleExcell.TextCenterBold, false, 12, true, rowStart+3 , rowStart+3 , 9, 12));

                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "BCCongTrinhSuaChuaLonHT_"+ DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }

        //Tong hop chi phi san xuat kinh doanh dien
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_TongHopChiPhiSanXuatKinhDoanhDien()
        {
            TongHopChiPhiSanXuatKinhDoanhDienQuery oBaoCaoChiTeuDVKHQuery = new TongHopChiPhiSanXuatKinhDoanhDienQuery(HttpContext.Current.Request);
            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/ERP_BCChiPhiSanXuatKinhDoanhDien";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBaoCaoChiTeuDVKHQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var bcNam = oBaoCaoChiTeuDVKHQuery.SearchNam;
            var bcQuyBaoCao = oBaoCaoChiTeuDVKHQuery.QuyBaoCao;
            var bcDV = oBaoCaoChiTeuDVKHQuery.LoaiDV;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcDV + "/Q" + bcQuyBaoCao + "_" + bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapTongHopChiPhiSanXuatKinhDoanhDienJson>(responseBody);
            //var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(TongHopChiPhiSanXuatKinhDoanhDienJson),
                    new Dictionary<string, string>
                    {
                        {"DienGiai", "LINE_DESCRIPTION"},
                        {"MaSo", "STRUCT"},
                        {"TongQuy", "NUM3"},
                        {"GiaThanhDonViQuy", "NUM4"},
                        {"TyTrongQuy", "TL_QUY"},
                        {"TongLuyKe", "NUM5"},
                        {"GiaThanhDonViLuyKe", "NUM6"},
                        {"TyTrongLuyKe", "TL_NAM"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<TongHopChiPhiSanXuatKinhDoanhDienJson>>(model.data, settings);
            string donvi = "";
            if (bcDV == "all")
            {
                donvi += "";
            }
            else if (bcDV == "074000")
            {
                donvi += "Tổng Công ty điện lực TP Hà Nội";
            }
            else if (bcDV == "070600")
            {
                donvi += "Công ty Điện lực Thanh Trì";
            }
            else if (bcDV == "000100")
            {
                donvi += "Tập đoàn Điện lực Việt Nam - Kế toán ngành";
            }
            else if (bcDV == "070700")
            {
                donvi += "Công ty Điện lực Gia Lâm";
            }
            else if (bcDV == "070800")
            {
                donvi += "Công ty Điện lực Đông Anh ";
            }
            else if (bcDV == "071400")
            {
                donvi += "Công ty Điện lực Long Biên";
            }
            else if (bcDV == "070900")
            {
                donvi += "Công ty Điện lực Sóc Sơn";
            }
            else if (bcDV == "071800")
            {
                donvi += "Công ty Điện lực Chương Mỹ";
            }
            else if (bcDV == "072000")
            {
                donvi += "Công ty Điện lực Thường Tín";
            }
            else if (bcDV == "072400")
            {
                donvi += "Công ty Điện lực Mỹ Đức";
            }
            else if (bcDV == "072600")
            {
                donvi += "Công ty Điện lực Phúc Thọ";
            }
            else if (bcDV == "072500")
            {
                donvi += "Công ty Điện lực Phú Xuyên";
            }
            else if (bcDV == "072800")
            {
                donvi += "Công ty Điện lực Thanh Oai";
            }
            else if (bcDV == "072900")
            {
                donvi += "Công ty Điện lực Ứng Hòa";
            }
            else if (bcDV == "070100")
            {
                donvi += "Công ty Điện lực Hoàn Kiếm ";
            }
            else if (bcDV == "071500")
            {
                donvi += "Công ty Điện lực Mê Linh";
            }
            else if (bcDV == "071700")
            {
                donvi += "Công ty Điện lực Sơn Tây";
            }
            else if (bcDV == "071900")
            {
                donvi += "Công ty Điện lực Thạch Thất";
            }
            else if (bcDV == "072100")
            {
                donvi += "Công ty Điện lực Ba Vì";
            }
            else if (bcDV == "072200")
            {
                donvi += "Công ty Điện lực Đan Phượng";
            }
            else if (bcDV == "072300")
            {
                donvi += "Công ty Điện lực Hoài Đức";
            }
            else if (bcDV == "072700")
            {
                donvi += "Công ty Điện lực Quốc Oai ";
            }
            else if (bcDV == "070400")
            {
                donvi += "Công ty Điện lực Đống Đa";
            }
            else if (bcDV == "070500")
            {
                donvi += "Công ty Điện lực Nam Từ Liêm";
            }
            else if (bcDV == "070200")
            {
                donvi += "Công ty Điện lực Hai Bà Trưng";
            }
            else if (bcDV == "071000")
            {
                donvi += "Công ty Điện lực Tây Hồ";
            }
            else if (bcDV == "071100")
            {
                donvi += "Công ty Điện lực Thanh Xuân";
            }
            else if (bcDV == "071200")
            {
                donvi += "Công ty Điện lực Cầu Giấy";
            }
            else if (bcDV == "073000")
            {
                donvi += "Công ty Điện lực Bắc Từ Liêm";
            }
            else if (bcDV == "071300")
            {
                donvi += "Công ty Điện lực Hoàng Mai";
            }
            else if (bcDV == "070300")
            {
                donvi += "Công ty Điện lực Ba Đình";
            }
            else if (bcDV == "071600")
            {
                donvi += "Công ty Điện lực Hà Đông";
            }
            else if (bcDV == "073100")
            {
                donvi += "Công ty Công nghệ thông tin Điện lực Hà Nội";
            }
            else if (bcDV == "073901")
            {
                donvi += "Ban Quản lý dự án Lưới điện Hà Nội - DEP";
            }
            else if (bcDV == "073500")
            {
                donvi += "Công ty Thí nghiệm điện Điện lực Hà Nội";
            }
            else if (bcDV == "073802")
            {
                donvi += "Ban Quản lý dự án phát triển Điện lực Hà Nội - XDCB";
            }
            else if (bcDV == "073200")
            {
                donvi += "Trung tâm Điều độ Hệ thống điện TP Hà Nội";
            }
            else if (bcDV == "073902")
            {
                donvi += "Ban Quản lý dự án Lưới điện Hà Nội - XDCB";
            }
            else if (bcDV == "073801")
            {
                donvi += "Ban Quản lý dự án phát triển Điện lực Hà Nội - ADB";
            }
            else if (bcDV == "073600")
            {
                donvi += "Công ty Lưới điện cao thế TP Hà Nội";
            }
            else if (bcDV == "073700")
            {
                donvi += "Công ty Dịch vụ Điện lực Hà Nội";
            }
            else
            {
                donvi += "Tổng Công ty Điện lực TP Hà Nội";
            };

            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tổng Công ty Điện lực TP Hà Nội",
                TieuDeTrai2 = donvi,
                Title = "BẢNG TỔNG HỢP CHI PHÍ SẢN XUẤT KINH DOANH ĐIỆN",
                recordsTotal = oData.Count,
                LoaiBM = "Biểu 02/THKT",
                Nam = bcNam.ToString(),
                Thang = bcQuyBaoCao.ToString()
            };     

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoChiTeuDVKHQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoChiTeuDVKHQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Qúy : ";
                    if (bcQuyBaoCao != null)
                        Tungaydenngay += bcQuyBaoCao.ToString();
                    Tungaydenngay += " - Năm ";
                    if (bcNam != null)
                        Tungaydenngay += bcNam.ToString();
                  

                    lstValues.Add(new ClsExcel(4, 0, Tungaydenngay, StyleExcell.TextCenter, false, 12, false, 0, 0));
                    lstValues.Add(new ClsExcel(1, 0, donvi, StyleExcell.TextCenterBold, false, 12, false, 0, 0));
                  
                    lstValues.Add(new ClsExcel(6, 6, "Tổng số :" + oData.Count + "bản ghi", StyleExcell.TextCenter, false, 12, true, 6, 6,6,7));
                    int rowStart = 11;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                       
                        lstValues.Add(new ClsExcel(rowStart, 0, item.DienGiai, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.MaSo, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.TongQuy, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.GiaThanhDonViQuy, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.TyTrongQuy, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.TongLuyKe, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.GiaThanhDonViLuyKe, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.TyTrongLuyKe, StyleExcell.TextLeft));
                        STT++;
                        rowStart++;
                    }
                    lstValues.Add(new ClsExcel(++rowStart + 1, 0, "NGƯỜI LẬP BIỂU", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 1, 0, 1));
                    lstValues.Add(new ClsExcel(++rowStart + 1, 0, "(Ký ,họ tên )", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 1, 0, 1));
                    lstValues.Add(new ClsExcel(rowStart + 2, 0, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 8, 0, 1));
                    lstValues.Add(new ClsExcel(++rowStart - 1, 2, "KẾ TOÁN TRƯỞNG", StyleExcell.TextCenterBold, false, 12, true, rowStart - 1, rowStart - 1, 2, 4));
                    lstValues.Add(new ClsExcel(++rowStart - 1, 2, "(Ký ,họ tên )", StyleExcell.TextCenterBold, false, 12, true, rowStart - 1, rowStart - 1, 2, 4));
                    lstValues.Add(new ClsExcel(rowStart, 1, "", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart + 6, 2, 4));

                    string ngaythangnam = "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    lstValues.Add(new ClsExcel(++rowStart - 4, 5, ngaythangnam, StyleExcell.TextDatetime, false, 12, true, rowStart - 4, rowStart - 4, 5, 7));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 5, "GIÁM ĐỐC", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart - 4, 5, 7));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 5, "(Ký ,họ tên )", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart - 4, 5, 7));
                    lstValues.Add(new ClsExcel(rowStart - 3, 5, "", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart + 3, 5, 7));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "TongHopChiPhiSanXuatKinhDoanhDien_"+ DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        //Nguon Von Vay
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_NguonVonVay()
        {
            NguonVonVayQuery oBaoCaoChiTeuDVKHQuery = new NguonVonVayQuery(HttpContext.Current.Request);
            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/ERP_NguonVonVay";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBaoCaoChiTeuDVKHQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var bcNam = oBaoCaoChiTeuDVKHQuery.SearchNam;
            var bcThang = oBaoCaoChiTeuDVKHQuery.ThangBaoCao;
            var bcDV = oBaoCaoChiTeuDVKHQuery.LoaiDV;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcDV + "/" + bcThang + "-" + bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapNguonVonVayJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(NguonVonVayJson),
                    new Dictionary<string, string>
                    {
                        {"TenCongTrinh","ATTRIBUTE49"},
                        {"DoiTacVay","TEXT2"},
                      //  {"DonViTinh", "NUM1"},
                        {"NguyenTe", "NUM1"},
                        {"QuyDoiVND", "NUM2"},
                        {"VayBangVND", "NUM3"},
                        {"SoDuDauNam", "NUM4"},
                        {"KyBaoCaoTang", "NUM5"},
                        {"TuDauNamDenKyBaoCaoTang", "NUM6"},
                        {"LuyKeBatDauRutVonTang", "NUM7"},
                        {"KyBaoCaoGiam", "NUM8"},
                        {"TuDauNamDenKyBaoCaoGiam", "NUM9"},
                        {"LuyKeBatDauRutVonGiam", "NUM10"},
                        {"TongSo", "NUM11"},
                        {"NoDenHanPhaiTra", "NUM12"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<NguonVonVayJson>>(model.data, settings);
            string donvi = "";
            if (bcDV == "all")
            {
                donvi += "";
            }
            else if (bcDV == "074000")
            {
                donvi += "Tổng Công ty điện lực TP Hà Nội";
            }
            else if (bcDV == "070600")
            {
                donvi += "Công ty Điện lực Thanh Trì";
            }
            else if (bcDV == "000100")
            {
                donvi += "Tập đoàn Điện lực Việt Nam - Kế toán ngành";
            }
            else if (bcDV == "070700")
            {
                donvi += "Công ty Điện lực Gia Lâm";
            }
            else if (bcDV == "070800")
            {
                donvi += "Công ty Điện lực Đông Anh ";
            }
            else if (bcDV == "071400")
            {
                donvi += "Công ty Điện lực Long Biên";
            }
            else if (bcDV == "070900")
            {
                donvi += "Công ty Điện lực Sóc Sơn";
            }
            else if (bcDV == "071800")
            {
                donvi += "Công ty Điện lực Chương Mỹ";
            }
            else if (bcDV == "072000")
            {
                donvi += "Công ty Điện lực Thường Tín";
            }
            else if (bcDV == "072400")
            {
                donvi += "Công ty Điện lực Mỹ Đức";
            }
            else if (bcDV == "072600")
            {
                donvi += "Công ty Điện lực Phúc Thọ";
            }
            else if (bcDV == "072500")
            {
                donvi += "Công ty Điện lực Phú Xuyên";
            }
            else if (bcDV == "072800")
            {
                donvi += "Công ty Điện lực Thanh Oai";
            }
            else if (bcDV == "072900")
            {
                donvi += "Công ty Điện lực Ứng Hòa";
            }
            else if (bcDV == "070100")
            {
                donvi += "Công ty Điện lực Hoàn Kiếm ";
            }
            else if (bcDV == "071500")
            {
                donvi += "Công ty Điện lực Mê Linh";
            }
            else if (bcDV == "071700")
            {
                donvi += "Công ty Điện lực Sơn Tây";
            }
            else if (bcDV == "071900")
            {
                donvi += "Công ty Điện lực Thạch Thất";
            }
            else if (bcDV == "072100")
            {
                donvi += "Công ty Điện lực Ba Vì";
            }
            else if (bcDV == "072200")
            {
                donvi += "Công ty Điện lực Đan Phượng";
            }
            else if (bcDV == "072300")
            {
                donvi += "Công ty Điện lực Hoài Đức";
            }
            else if (bcDV == "072700")
            {
                donvi += "Công ty Điện lực Quốc Oai ";
            }
            else if (bcDV == "070400")
            {
                donvi += "Công ty Điện lực Đống Đa";
            }
            else if (bcDV == "070500")
            {
                donvi += "Công ty Điện lực Nam Từ Liêm";
            }
            else if (bcDV == "070200")
            {
                donvi += "Công ty Điện lực Hai Bà Trưng";
            }
            else if (bcDV == "071000")
            {
                donvi += "Công ty Điện lực Tây Hồ";
            }
            else if (bcDV == "071100")
            {
                donvi += "Công ty Điện lực Thanh Xuân";
            }
            else if (bcDV == "071200")
            {
                donvi += "Công ty Điện lực Cầu Giấy";
            }
            else if (bcDV == "073000")
            {
                donvi += "Công ty Điện lực Bắc Từ Liêm";
            }
            else if (bcDV == "071300")
            {
                donvi += "Công ty Điện lực Hoàng Mai";
            }
            else if (bcDV == "070300")
            {
                donvi += "Công ty Điện lực Ba Đình";
            }
            else if (bcDV == "071600")
            {
                donvi += "Công ty Điện lực Hà Đông";
            }
            else if (bcDV == "073100")
            {
                donvi += "Công ty Công nghệ thông tin Điện lực Hà Nội";
            }
            else if (bcDV == "073901")
            {
                donvi += "Ban Quản lý dự án Lưới điện Hà Nội - DEP";
            }
            else if (bcDV == "073500")
            {
                donvi += "Công ty Thí nghiệm điện Điện lực Hà Nội";
            }
            else if (bcDV == "073802")
            {
                donvi += "Ban Quản lý dự án phát triển Điện lực Hà Nội - XDCB";
            }
            else if (bcDV == "073200")
            {
                donvi += "Trung tâm Điều độ Hệ thống điện TP Hà Nội";
            }
            else if (bcDV == "073902")
            {
                donvi += "Ban Quản lý dự án Lưới điện Hà Nội - XDCB";
            }
            else if (bcDV == "073801")
            {
                donvi += "Ban Quản lý dự án phát triển Điện lực Hà Nội - ADB";
            }
            else if (bcDV == "073600")
            {
                donvi += "Công ty Lưới điện cao thế TP Hà Nội";
            }
            else if (bcDV == "073700")
            {
                donvi += "Công ty Dịch vụ Điện lực Hà Nội";
            }
            else
            {
                donvi += "Tổng Công ty Điện lực TP Hà Nội";
            };

            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tổng Công ty Điện lực TP Hà Nội",
                TieuDeTrai2 = donvi,
                Title = "NGUỒN VỐN VAY",
                Thang = bcThang.ToString(),
                LoaiBM = "Biểu 03A/ĐTXD",
                Nam = bcNam.ToString(),

                recordsTotal = oData.Count
            };

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoChiTeuDVKHQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoChiTeuDVKHQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng : ";
                    if (bcThang != null)
                        Tungaydenngay += bcThang.ToString();
                    Tungaydenngay += "  Năm ";
                    if (bcNam != null)
                        Tungaydenngay += bcNam.ToString();
                  


                    lstValues.Add(new ClsExcel(3, 0, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 0, 0));
                    lstValues.Add(new ClsExcel(1, 0, donvi, StyleExcell.TextCenterBold, false, 12, false, 0, 0));
                  
                    lstValues.Add(new ClsExcel(5, 13, "Tổng số : " +oData.Count + " bản ghi", StyleExcell.TextDatetime, false, 12, true, 5, 5,13,15));
                    int rowStart = 9;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.

                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.TenCongTrinh, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.DoiTacVay, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.DonViTinh, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.NguyenTe, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.QuyDoiVND, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.VayBangVND, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.SoDuDauNam, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.KyBaoCaoTang, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.TuDauNamDenKyBaoCaoTang, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.LuyKeBatDauRutVonTang, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.KyBaoCaoGiam, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 12, item.TuDauNamDenKyBaoCaoGiam, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 13, item.LuyKeBatDauRutVonGiam, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 14, item.TongSo, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 15, item.NoDenHanPhaiTra, StyleExcell.TextLeft));
                        STT++;
                        rowStart++;   
                    }

                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "NguonVonVay_"+DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        //Thuc hien dau tu xay dung co ban
         [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_ThucHienDauTuXayDungCoBan()
        {
            ThucHienDauTuXayDungCoBanQuery oBaoCaoChiTeuDVKHQuery = new ThucHienDauTuXayDungCoBanQuery(HttpContext.Current.Request);
            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/ERP_ThucHienDTXDCoBan";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBaoCaoChiTeuDVKHQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var bcNam = oBaoCaoChiTeuDVKHQuery.SearchNam;
            var bcThang = oBaoCaoChiTeuDVKHQuery.ThangBaoCao;
            var bcDV = oBaoCaoChiTeuDVKHQuery.LoaiDV;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcDV + "/" + bcThang + "-" + bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapThucHienDauTuXayDungCoBanJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(ThucHienDauTuXayDungCoBanJson),
                    new Dictionary<string, string>
                    {
                        {"STT","STRUCT"},
                        {"CoCauChiPhiDauTu","DESCRIPTION"},
                        {"KeHoachNam", "NUM1"},
                        {"ThucHienDauTuKy", "NUM2"},
                        {"KyBaoCaoThucHienDauTu", "NUM3"},
                        {"LuyKeTuDauNamThucHienDauTu", "NUM4"},
                        {"LuyKeTuKhoiCongThucHienDauTu", "NUM5"},
                        {"KyBaoCaoBanGiao", "NUM6"},
                        {"LuyKeTuDauNamBanGiao", "NUM7"},
                        {"LuyKeTuKhoiCongBanGiao", "NUM8"},
                        {"ThucHienDauTuCOnLaiCuoiKy", "NUM9"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<ThucHienDauTuXayDungCoBanJson>>(string1resultUP, settings);
            string donvi = "";
            if (bcDV == "all")
            {
                donvi += "";
            }
            else if (bcDV == "074000")
            {
                donvi += "Tổng Công ty điện lực TP Hà Nội";
            }
            else if (bcDV == "070600")
            {
                donvi += "Công ty Điện lực Thanh Trì";
            }
            else if (bcDV == "000100")
            {
                donvi += "Tập đoàn Điện lực Việt Nam - Kế toán ngành";
            }
            else if (bcDV == "070700")
            {
                donvi += "Công ty Điện lực Gia Lâm";
            }
            else if (bcDV == "070800")
            {
                donvi += "Công ty Điện lực Đông Anh ";
            }
            else if (bcDV == "071400")
            {
                donvi += "Công ty Điện lực Long Biên";
            }
            else if (bcDV == "070900")
            {
                donvi += "Công ty Điện lực Sóc Sơn";
            }
            else if (bcDV == "071800")
            {
                donvi += "Công ty Điện lực Chương Mỹ";
            }
            else if (bcDV == "072000")
            {
                donvi += "Công ty Điện lực Thường Tín";
            }
            else if (bcDV == "072400")
            {
                donvi += "Công ty Điện lực Mỹ Đức";
            }
            else if (bcDV == "072600")
            {
                donvi += "Công ty Điện lực Phúc Thọ";
            }
            else if (bcDV == "072500")
            {
                donvi += "Công ty Điện lực Phú Xuyên";
            }
            else if (bcDV == "072800")
            {
                donvi += "Công ty Điện lực Thanh Oai";
            }
            else if (bcDV == "072900")
            {
                donvi += "Công ty Điện lực Ứng Hòa";
            }
            else if (bcDV == "070100")
            {
                donvi += "Công ty Điện lực Hoàn Kiếm ";
            }
            else if (bcDV == "071500")
            {
                donvi += "Công ty Điện lực Mê Linh";
            }
            else if (bcDV == "071700")
            {
                donvi += "Công ty Điện lực Sơn Tây";
            }
            else if (bcDV == "071900")
            {
                donvi += "Công ty Điện lực Thạch Thất";
            }
            else if (bcDV == "072100")
            {
                donvi += "Công ty Điện lực Ba Vì";
            }
            else if (bcDV == "072200")
            {
                donvi += "Công ty Điện lực Đan Phượng";
            }
            else if (bcDV == "072300")
            {
                donvi += "Công ty Điện lực Hoài Đức";
            }
            else if (bcDV == "072700")
            {
                donvi += "Công ty Điện lực Quốc Oai ";
            }
            else if (bcDV == "070400")
            {
                donvi += "Công ty Điện lực Đống Đa";
            }
            else if (bcDV == "070500")
            {
                donvi += "Công ty Điện lực Nam Từ Liêm";
            }
            else if (bcDV == "070200")
            {
                donvi += "Công ty Điện lực Hai Bà Trưng";
            }
            else if (bcDV == "071000")
            {
                donvi += "Công ty Điện lực Tây Hồ";
            }
            else if (bcDV == "071100")
            {
                donvi += "Công ty Điện lực Thanh Xuân";
            }
            else if (bcDV == "071200")
            {
                donvi += "Công ty Điện lực Cầu Giấy";
            }
            else if (bcDV == "073000")
            {
                donvi += "Công ty Điện lực Bắc Từ Liêm";
            }
            else if (bcDV == "071300")
            {
                donvi += "Công ty Điện lực Hoàng Mai";
            }
            else if (bcDV == "070300")
            {
                donvi += "Công ty Điện lực Ba Đình";
            }
            else if (bcDV == "071600")
            {
                donvi += "Công ty Điện lực Hà Đông";
            }
            else if (bcDV == "073100")
            {
                donvi += "Công ty Công nghệ thông tin Điện lực Hà Nội";
            }
            else if (bcDV == "073901")
            {
                donvi += "Ban Quản lý dự án Lưới điện Hà Nội - DEP";
            }
            else if (bcDV == "073500")
            {
                donvi += "Công ty Thí nghiệm điện Điện lực Hà Nội";
            }
            else if (bcDV == "073802")
            {
                donvi += "Ban Quản lý dự án phát triển Điện lực Hà Nội - XDCB";
            }
            else if (bcDV == "073200")
            {
                donvi += "Trung tâm Điều độ Hệ thống điện TP Hà Nội";
            }
            else if (bcDV == "073902")
            {
                donvi += "Ban Quản lý dự án Lưới điện Hà Nội - XDCB";
            }
            else if (bcDV == "073801")
            {
                donvi += "Ban Quản lý dự án phát triển Điện lực Hà Nội - ADB";
            }
            else if (bcDV == "073600")
            {
                donvi += "Công ty Lưới điện cao thế TP Hà Nội";
            }
            else if (bcDV == "073700")
            {
                donvi += "Công ty Dịch vụ Điện lực Hà Nội";
            }
            else
            {
                donvi += "Tổng Công ty Điện lực TP Hà Nội";
            };

            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tổng Công ty Điện lực TP Hà Nội",
                TieuDeTrai2 = donvi,
                Title = "THỰC HIỆN ĐẦU TƯ XÂY DỰNG CƠ BẢN",
                LoaiBM = "Biểu 04/ĐTXD",
                Thang = bcThang.ToString(),
                Nam = bcNam.ToString(),

                recordsTotal = oData.Count
            };

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoChiTeuDVKHQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoChiTeuDVKHQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng : ";
                    if (bcThang != null )
                        Tungaydenngay += bcThang.ToString();
                    Tungaydenngay += " - Năm ";
                    if (bcNam != null)
                        Tungaydenngay += bcNam.ToString();
                   
                          

                    lstValues.Add(new ClsExcel(5, 0, Tungaydenngay, StyleExcell.TextCenter, false, 12, false, 0, 0));
                    lstValues.Add(new ClsExcel(1, 1, donvi, StyleExcell.TextCenterBold, false, 12, false, 0, 0));
                  
                    lstValues.Add(new ClsExcel(6, 9, "Tổng số : " + oData.Count + "bản ghi ", StyleExcell.TextLeft, false, 12, true, 6, 6,9,10));
                    int rowStart = 13;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.

                        lstValues.Add(new ClsExcel(rowStart, 0, item.STT, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.CoCauChiPhiDauTu, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.KeHoachNam, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.ThucHienDauTuKy, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.KyBaoCaoThucHienDauTu, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.LuyKeTuDauNamThucHienDauTu, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.LuyKeTuKhoiCongThucHienDauTu, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.KyBaoCaoBanGiao, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.LuyKeTuDauNamBanGiao, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.LuyKeTuKhoiCongBanGiao, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.ThucHienDauTuCOnLaiCuoiKy, StyleExcell.TextLeft));
                        STT++;
                        rowStart++;
                    }

                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "ThucHienDauTuXayDungCoBan_"+ DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        // THỰC HIỆN ĐẦU TƯ THEO CÔNG TRÌNH, HẠNG MỤC CÔNG TRÌNH
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_ThucHienDauTuTheoCongTrinhHangMucCongTrinh()
        {
            ThucHienDauTuTheoCongTrinhHangMucCongTrinhQuery oBaoCaoChiTeuDVKHQuery = new ThucHienDauTuTheoCongTrinhHangMucCongTrinhQuery(HttpContext.Current.Request);
            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/ERP_ThucHienDTXDCongTrinhHMCT";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBaoCaoChiTeuDVKHQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var bcNam = oBaoCaoChiTeuDVKHQuery.SearchNam;
            var bcThang = oBaoCaoChiTeuDVKHQuery.ThangBaoCao;
            var bcDV = oBaoCaoChiTeuDVKHQuery.LoaiDV;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcDV + "/" + bcThang + "-" + bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapThucHienDauTuTheoCongTrinhHangMucCongTrinhJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(ThucHienDauTuTheoCongTrinhHangMucCongTrinhJson),
                    new Dictionary<string, string>
                    {
                        {"DISPLAY_ATT1", "DISPLAY_ATT1"},
                        {"CongTrinhHangMucCT", "LINE_DESCRIPTION"},
                        {"TongDuDoan", "NUM1"},
                        {"SoDuDauNamThucHien", "NUM2"},
                        {"TongSo", "NUM3"},
                        {"XayDung", "NUM4"},
                        {"LapDat", "NUM5"},
                        {"ThietBi", "NUM6"},
                        {"ChiPhiBoiThuong", "NUM7"},
                        {"ChiPhiQuanLy", "NUM8"},
                        {"ChiPhiTuVanDauTuXayDung", "NUM9"},
                        {"TongSoKhac", "NUM10"},
                        {"ChiPhiLaiVay", "NUM11"},
                        {"ThucHienDauTuTang", "NUM12"},
                        {"ThucHienDauTuGiam", "NUM13"},
                        {"SoDuCuoiKyThucHien", "NUM14"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<ThucHienDauTuTheoCongTrinhHangMucCongTrinhJson>>(string1resultUP, settings);
            string donvi = "";
            if (bcDV == "all")
            {
                donvi += "";
            }
            else if (bcDV == "074000")
            {
                donvi += "Tổng Công ty điện lực TP Hà Nội";
            }
            else if (bcDV == "070600")
            {
                donvi += "Công ty Điện lực Thanh Trì";
            }
            else if (bcDV == "000100")
            {
                donvi += "Tập đoàn Điện lực Việt Nam - Kế toán ngành";
            }
            else if (bcDV == "070700")
            {
                donvi += "Công ty Điện lực Gia Lâm";
            }
            else if (bcDV == "070800")
            {
                donvi += "Công ty Điện lực Đông Anh ";
            }
            else if (bcDV == "071400")
            {
                donvi += "Công ty Điện lực Long Biên";
            }
            else if (bcDV == "070900")
            {
                donvi += "Công ty Điện lực Sóc Sơn";
            }
            else if (bcDV == "071800")
            {
                donvi += "Công ty Điện lực Chương Mỹ";
            }
            else if (bcDV == "072000")
            {
                donvi += "Công ty Điện lực Thường Tín";
            }
            else if (bcDV == "072400")
            {
                donvi += "Công ty Điện lực Mỹ Đức";
            }
            else if (bcDV == "072600")
            {
                donvi += "Công ty Điện lực Phúc Thọ";
            }
            else if (bcDV == "072500")
            {
                donvi += "Công ty Điện lực Phú Xuyên";
            }
            else if (bcDV == "072800")
            {
                donvi += "Công ty Điện lực Thanh Oai";
            }
            else if (bcDV == "072900")
            {
                donvi += "Công ty Điện lực Ứng Hòa";
            }
            else if (bcDV == "070100")
            {
                donvi += "Công ty Điện lực Hoàn Kiếm ";
            }
            else if (bcDV == "071500")
            {
                donvi += "Công ty Điện lực Mê Linh";
            }
            else if (bcDV == "071700")
            {
                donvi += "Công ty Điện lực Sơn Tây";
            }
            else if (bcDV == "071900")
            {
                donvi += "Công ty Điện lực Thạch Thất";
            }
            else if (bcDV == "072100")
            {
                donvi += "Công ty Điện lực Ba Vì";
            }
            else if (bcDV == "072200")
            {
                donvi += "Công ty Điện lực Đan Phượng";
            }
            else if (bcDV == "072300")
            {
                donvi += "Công ty Điện lực Hoài Đức";
            }
            else if (bcDV == "072700")
            {
                donvi += "Công ty Điện lực Quốc Oai ";
            }
            else if (bcDV == "070400")
            {
                donvi += "Công ty Điện lực Đống Đa";
            }
            else if (bcDV == "070500")
            {
                donvi += "Công ty Điện lực Nam Từ Liêm";
            }
            else if (bcDV == "070200")
            {
                donvi += "Công ty Điện lực Hai Bà Trưng";
            }
            else if (bcDV == "071000")
            {
                donvi += "Công ty Điện lực Tây Hồ";
            }
            else if (bcDV == "071100")
            {
                donvi += "Công ty Điện lực Thanh Xuân";
            }
            else if (bcDV == "071200")
            {
                donvi += "Công ty Điện lực Cầu Giấy";
            }
            else if (bcDV == "073000")
            {
                donvi += "Công ty Điện lực Bắc Từ Liêm";
            }
            else if (bcDV == "071300")
            {
                donvi += "Công ty Điện lực Hoàng Mai";
            }
            else if (bcDV == "070300")
            {
                donvi += "Công ty Điện lực Ba Đình";
            }
            else if (bcDV == "071600")
            {
                donvi += "Công ty Điện lực Hà Đông";
            }
            else if (bcDV == "073100")
            {
                donvi += "Công ty Công nghệ thông tin Điện lực Hà Nội";
            }
            else if (bcDV == "073901")
            {
                donvi += "Ban Quản lý dự án Lưới điện Hà Nội - DEP";
            }
            else if (bcDV == "073500")
            {
                donvi += "Công ty Thí nghiệm điện Điện lực Hà Nội";
            }
            else if (bcDV == "073802")
            {
                donvi += "Ban Quản lý dự án phát triển Điện lực Hà Nội - XDCB";
            }
            else if (bcDV == "073200")
            {
                donvi += "Trung tâm Điều độ Hệ thống điện TP Hà Nội";
            }
            else if (bcDV == "073902")
            {
                donvi += "Ban Quản lý dự án Lưới điện Hà Nội - XDCB";
            }
            else if (bcDV == "073801")
            {
                donvi += "Ban Quản lý dự án phát triển Điện lực Hà Nội - ADB";
            }
            else if (bcDV == "073600")
            {
                donvi += "Công ty Lưới điện cao thế TP Hà Nội";
            }
            else if (bcDV == "073700")
            {
                donvi += "Công ty Dịch vụ Điện lực Hà Nội";
            }
            else
            {
                donvi += "Tổng Công ty Điện lực TP Hà Nội";
            };

            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tổng Công ty Điện lực TP Hà Nội",
                TieuDeTrai2 = donvi,
                Title = "THỰC HIỆN ĐẦU TƯ THEO CÔNG TRÌNH, HẠNG MỤC CÔNG TRÌNH",
                //NguoiKy = "VŨ THẾ THẮNG",
                Thang = bcThang.ToString(),
                Nam = bcNam.ToString(),
                NguoiLapBieu = CurentUser.Title,
              //  KeToanTruong = "ĐỖ MAI NGA",
                recordsTotal = oData.Count
            };

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoChiTeuDVKHQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoChiTeuDVKHQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng: ";
                    if (bcThang != null )
                        Tungaydenngay += bcThang.ToString();
                    Tungaydenngay += " - Năm : ";
                    if (bcNam != null)
                        Tungaydenngay += bcNam.ToString();
                

                    lstValues.Add(new ClsExcel(4, 0, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 4, 4));
                    lstValues.Add(new ClsExcel(1, 0, donvi, StyleExcell.TextCenterBold, false, 12, false, 4, 4));
                  
                    int rowStart = 11;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.

                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.CongTrinhHangMucCT, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.TongDuDoan, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.SoDuDauNamThucHien, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.TongSo, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.XayDung, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.LapDat, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.ThietBi, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.ChiPhiBoiThuong, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.ChiPhiQuanLy, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.ChiPhiTuVanDauTuXayDung, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.TongSoKhac, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 12, item.ChiPhiLaiVay, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 13, item.ThucHienDauTuTang, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 14, item.ThucHienDauTuGiam, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 15, item.SoDuCuoiKyThucHien, StyleExcell.TextLeft));
                        STT++;
                        rowStart++;
                    }
                    lstValues.Add(new ClsExcel(6, 14, "Có tổng số: " + oData.Count + " bản ghi", StyleExcell.TextRight, false, 12, true, 6, 6,14,15));



                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "NGƯỜI LẬP BIỂU ", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 2, 0, 3));
                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 6, 0, 3));
                    lstValues.Add(new ClsExcel(rowStart + 7, 0, CurentUser.Title, StyleExcell.TextCenterBold, false, 12, true, rowStart + 7, rowStart + 7, 0, 3));
                    lstValues.Add(new ClsExcel(rowStart + 8, 0, "(Ký, họ tên)", StyleExcell.TextCenterBold, false, 12, true, rowStart + 8, rowStart + 8, 0, 3));

                    lstValues.Add(new ClsExcel(++rowStart, 4, " KẾ TOÁN TRƯỞNG", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart, 4, 11));
                    lstValues.Add(new ClsExcel(++rowStart, 4, "", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart + 4, 4, 11));
                    lstValues.Add(new ClsExcel(rowStart + 5, 4, dataGiaoDienRender.KeToanTruong, StyleExcell.TextCenterBold, false, 12, true, rowStart + 5, rowStart + 5, 4, 11));
                    lstValues.Add(new ClsExcel(rowStart + 6, 4, "(Ký, họ tên)", StyleExcell.TextCenterBold, false, 12, true, rowStart + 6, rowStart + 6, 4, 11));

                    string ngaythangnam = "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    lstValues.Add(new ClsExcel(++rowStart - 3, 12, ngaythangnam, StyleExcell.TextDatetime, false, 12, true, rowStart - 3, rowStart - 3, 12, 15));
                    lstValues.Add(new ClsExcel(++rowStart - 3, 12, "THỦ TRƯỞNG ĐƠN VỊ", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart - 3, 12, 15));
                    lstValues.Add(new ClsExcel(++rowStart - 3, 12, "", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart + 1, 12, 15));
                    //lstValues.Add(new ClsExcel(rowStart + 1, 2, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 2, 1, 1));
                    //lstValues.Add(new ClsExcel(rowStart + 2, 12, dataGiaoDienRender.NguoiKy, StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 2, 12, 15));
                    lstValues.Add(new ClsExcel(rowStart + 3, 12, "(Ký, họ tên, đóng dấu)", StyleExcell.TextCenterBold, false, 12, true, rowStart + 3, rowStart + 3, 12, 15));

                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "ThucHienDauTuTheoCongTrinhHangMucCongTrinh_" + DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }

    }
}