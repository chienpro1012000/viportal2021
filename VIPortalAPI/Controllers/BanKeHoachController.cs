﻿using Library_Shared.Utilities;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using ViPortal_Utils.Base;
using VIPortalAPI.BaseAPI;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.Lconfig;

namespace VIPortalAPI.Controllers
{
    public class BanKeHoachController : SIBaseAPI
    {
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_DTXD_BaoCaoTongHopDTXD()
        {
            DTXD_BaoCaoTongHopDTXDQuery oDanhSachCongTrinhTheoKeHoachKCQuery = new DTXD_BaoCaoTongHopDTXDQuery(HttpContext.Current.Request);

            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/DTXD_BaoCaoTongHopDTXD/2021";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_225", ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var bcNam = oDanhSachCongTrinhTheoKeHoachKCQuery.Nam;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(DTXD_BaoCaoTongHopDTXDJson),
                    new Dictionary<string, string>
                    {

                        {"TenDuAN", "TenDuAn"},
                        {"DonViQuanLy", "TenTat"},
                        {"QuyMo", "QuyMo"},
                        {"NguonVon", "NguonVonSuDung"},
                        {"NgayGiao", "NgayGiaoNV"},
                        {"VonKeHoach", "GiaTriGiaoNV"},
                        {"SoQuyetDinh", "TMDT_QuyetDinh"},
                        {"TongMucDauTu", "TMDT"},
                        {"KhoiCongKH", "KH_NgayKC"},
                        {"KhoiCongThucTe", "KH_NgayHT"},
                        {"HoanThanhKeHoach", "TT_NgayKC"},
                        {"HoanThanhThucTe", "TT_NgayNT"},
                        {"TinhTrangDuAn", "TenTinhTrangKh"},
                        {"VuongMacKienNghi", "TenTinhTrang"},
                        { "ThongKe_DuAn_TongSo", "ThongKe_DuAn_TongSo" },
                        { "SoDA_KhoiCong" ,"SoDA_KhoiCong" },
                        { "SoDA_GiaoKhoiCong" ,"SoDA_GiaoKhoiCong" },
                        { "SoDA_HT","SoDA_HT" },
                        { " SoDA_GiaoHT","SoDA_GiaoHT" }
            }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var root = JsonConvert.DeserializeObject<MapDTXD_BaoCaoTongHopDTXDJson>(responseBody, settings);

            //TestJson objectShareNew = Newtonsoft.Json.JsonConvert.DeserializeObject<TestJson>(responseBody);


            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tập đoàn điện lực Việt Nam",
                TieuDeTrai2 = "TỔNG CÔNG TY ĐIỆN LỰC TP HÀ NỘI",
                TieuDeGiua1 = "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM",
                TieuDeGiua2 = "Độc lập - Tự do - Hạnh Phúc",
                Title = "TỔNG HỢP BÁO CÁO ĐẦU TƯ XÂY DỰNG - BIỂU RÚT GỌN",
                Nam = "Năm: " + bcNam.ToString(),
                recordsTotal = root.data.Count
            };

            dataGiaoDienRender.data = root.data;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oDanhSachCongTrinhTheoKeHoachKCQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oDanhSachCongTrinhTheoKeHoachKCQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Năm: ";
                    if (oDanhSachCongTrinhTheoKeHoachKCQuery.Nam != null)
                        Tungaydenngay += oDanhSachCongTrinhTheoKeHoachKCQuery.Nam.ToString();


                    lstValues.Add(new ClsExcel(3, 0, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 3, 3));
                    int rowStart = 12;
                    int STT = 1;
                    int tongso = 0;
                    int SoDA_KhoiCong = 0;
                    int SoDA_GiaoKhoiCong = 0;
                    int SoDA_HT = 0;
                    int SoDA_GiaoHT = 0;
                    //set title
                    foreach (var item in root.data)
                    {
                        tongso += int.Parse(item.ThongKe_DuAn_TongSo);
                        SoDA_KhoiCong += int.Parse(item.SoDA_KhoiCong);
                        SoDA_GiaoKhoiCong += int.Parse(item.SoDA_GiaoKhoiCong);
                        SoDA_HT += int.Parse(item.SoDA_HT);
                        SoDA_GiaoHT += int.Parse(item.SoDA_GiaoHT);
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.TenDuAN, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.QuyMo, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.NguonVon, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.NgayGiao, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.VonKeHoach, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.SoQuyetDinh, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.TongMucDauTu, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.KhoiCongKH, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.KhoiCongThucTe, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.HoanThanhKeHoach, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.HoanThanhThucTe, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 12, item.TinhTrangDuAn, StyleExcell.TextCenter));

                        rowStart++;
                        STT++;

                    }
                    lstValues.Add(new ClsExcel(4, 1, "TỔNG SỐ DỰ ÁN : "+tongso.ToString(), StyleExcell.TextLeft, false, 12, true, 4, 4, 1, 2));
                    lstValues.Add(new ClsExcel(5, 1, "SỐ DỰ ÁN ĐÃ KHỞI CÔNG / SỐ DỰ ÁN GIAO KHỞI CÔNG: " + SoDA_KhoiCong.ToString() + "/" + SoDA_GiaoKhoiCong.ToString(), StyleExcell.TextLeft, false, 12, true, 5, 5, 1, 2));
                    lstValues.Add(new ClsExcel(6, 1, "SỐ DỰ ÁN ĐÃ HOÀN THÀNH / SỐ DỰ ÁN GIAO HOÀN THÀNH:" + SoDA_HT.ToString() + "/" + SoDA_GiaoHT.ToString(), StyleExcell.TextLeft, false, 12, true, 6, 6, 1, 2));
                    lstValues.Add(new ClsExcel(8, 10, "Có tổng số: " + root.data.Count + " bản ghi", StyleExcell.TextRight, false, 12, true, 8, 8, 10, 12));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "BaoCaoTongHopDauTuXayDung-BieuRutGon" + "_Nam: " + bcNam.ToString()));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        // bao cao nguon von dau tu 
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_ERP_BCNguonVonDauTu()
        {
            ERP_BCNguonVonDauTuJQuery oNguonVonDauTuQuery = new ERP_BCNguonVonDauTuJQuery(HttpContext.Current.Request);
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/ERP_BCNguonVonDauTu/all/10-2021";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_233", ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            var bcThang = oNguonVonDauTuQuery.ThangBaoCao;
            var bcNam = oNguonVonDauTuQuery.SearchNam;
            var bcDV = oNguonVonDauTuQuery.LoaiDV;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcDV + "/" + bcThang + "-" + bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapNguonVonDauTuJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(NguonVonDauTuJson),
                    new Dictionary<string, string>
                    {
                        {"NguonVon", "DESCRIPTION"},
                        {"SoDuDauNam", "NUM1"},
                        {"KyBaoCaoTang", "NUM2"},
                        {"LuyKeTuDauNamTang", "NUM3"},
                        {"LuyKeTuKhoiCongTang", "NUM4"},
                        {"KyBaoCaoGiam", "NUM5"},
                        {"LuyKeTuDauNamGiam", "NUM6"},
                        {"LuyKeTuKhoiCongGiam", "NUM7"},
                        {"SoDuCuoiKy", "NUM8"},

                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<NguonVonDauTuJson>>(string1resultUP, settings);

            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "TỔNG CÔNG TY ĐIỆN LỰC TP HÀ NỘI",
                TieuDeTrai2 = "CÔNG TY ĐIỆN LỰC HOÀI ĐỨC",
                Title = "BÁO CÁO NGUỒN VỐN ĐẦU TƯ",
                LoaiBM = "Biểu 01/ĐTXD",
                Thang = bcThang.ToString(),
                Nam = bcNam.ToString(),
                recordsTotal = oData.Count
            };

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oNguonVonDauTuQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oNguonVonDauTuQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng : ";
                    if (bcThang != null)
                        Tungaydenngay += bcThang.ToString();
                    Tungaydenngay += " - Năm ";
                    if (bcNam != null)
                        Tungaydenngay += bcNam.ToString();
                    string donvi = "Đơn Vị : ";
                    if (bcDV == "all")
                    {
                        donvi += "Tất cả";
                    }
                    else if (bcDV == "PD")
                    {
                        donvi += "Tổng Công ty Điện lực TP Hà Nội";
                    }
                    else if (bcDV == "PD0600")
                    {
                        donvi += "Công ty Điện lưc Thanh Trì";
                    }
                    else if (bcDV == "PD1400")
                    {
                        donvi += "Công ty Điện lực Long Biên";
                    }
                    else if (bcDV == "P")
                    {
                        donvi += "Tập đoàn Điện Lực Việt Nam";
                    }
                    else if (bcDV == "PD0700")
                    {
                        donvi += "Công ty Điện lực Gia Lâm";
                    }
                    else if (bcDV == "PD0800")
                    {
                        donvi += "Công ty Điện lực Đông Anh";
                    }
                    else if (bcDV == "PD0900")
                    {
                        donvi += "Công ty Điện lực Sóc Sơn";
                    }
                    else if (bcDV == "PD1800")
                    {
                        donvi += "Công ty Điện lực Chương Mỹ";
                    }
                    else if (bcDV == "PD2000")
                    {
                        donvi += "Công ty Điện lực Thường Tín";
                    }
                    else if (bcDV == "PD2400")
                    {
                        donvi += "Công ty Điện lực Mỹ Đức";
                    }
                    else if (bcDV == "PD2600")
                    {
                        donvi += "Công ty Điện lực Phúc Thọ";
                    }
                    else if (bcDV == "PD2500")
                    {
                        donvi += "Công ty Điện lực Phú Xuyên";
                    }
                    else if (bcDV == "PD2800")
                    {
                        donvi += "Công ty Điện lực Thanh Oai";
                    }
                    else if (bcDV == "PD2900")
                    {
                        donvi += "Công ty Điện lực Ứng Hòa";
                    }
                    else if (bcDV == "PD0100")
                    {
                        donvi += "Công ty Điện lực Hoàn Kiếm";
                    }
                    else if (bcDV == "PD1500")
                    {
                        donvi += "Công ty Điện lực Mê Linh";
                    }
                    else if (bcDV == "PD1700")
                    {
                        donvi += "Công ty Điện lực Sơn Tây ";
                    }
                    else if (bcDV == "PD1900")
                    {
                        donvi += "Công ty Điện lực Thạch Thất";
                    }
                    else if (bcDV == "PD2100")
                    {
                        donvi += "Công ty Điện lực Ba Vì";
                    }
                    else if (bcDV == "PD2200")
                    {
                        donvi += "Công ty Điện lực Đan Phượng";
                    }
                    else if (bcDV == "PD2300")
                    {
                        donvi += "Công ty Điện lực Hoài Đức";
                    }
                    else if (bcDV == "PD2700")
                    {
                        donvi += "Công ty Điện lực Quốc Oai ";
                    }
                    else if (bcDV == "PD0400")
                    {
                        donvi += "Công ty Điện lực Đống Đa ";
                    }
                    else if (bcDV == "PD0500")
                    {
                        donvi += "Công ty Điện lực Nam Từ Liêm ";
                    }
                    else if (bcDV == "PD0200")
                    {
                        donvi += "Công ty Điện lực Hai Bà Trưng ";
                    }
                    else if (bcDV == "PD1000")
                    {
                        donvi += "Công ty Điện lực Tây Hồ  ";
                    }
                    else if (bcDV == "PD1100")
                    {
                        donvi += "Công ty Điện lực Thanh Xuân ";
                    }
                    else if (bcDV == "PD1200")
                    {
                        donvi += "Công ty Điện lực Cầy giấy ";
                    }
                    else if (bcDV == "PD3000")
                    {
                        donvi += "Công ty Điện lực Bắc Từ Liêm ";
                    }
                    else if (bcDV == "PD1300")
                    {
                        donvi += "Công ty Điện lực Hoàng Mai ";
                    }
                    else if (bcDV == "PD0300")
                    {
                        donvi += "Công ty Điện lực Ba Đình ";
                    }
                    else if (bcDV == "PD1600")
                    {
                        donvi += "Công ty Điện lực Hà Đông ";
                    }
                    else
                    {
                        donvi += "Tổng Công ty Điện lực TP Hà Nội ";
                    };


                    lstValues.Add(new ClsExcel(4, 0, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 4, 4));
                    lstValues.Add(new ClsExcel(5, 5, donvi, StyleExcell.TextDatetime, false, 12, true, 5, 5, 5, 7));
                    lstValues.Add(new ClsExcel(6, 7, "Tổng số: " + oData.Count + "  bản ghi", StyleExcell.TextDatetime, false, 12, true, 6, 6, 7, 8));
                    int rowStart = 9;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, item.NguonVon, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.SoDuDauNam, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.KyBaoCaoTang, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.LuyKeTuDauNamTang, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.LuyKeTuKhoiCongTang, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.KyBaoCaoGiam, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.LuyKeTuDauNamGiam, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.LuyKeTuKhoiCongGiam, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.SoDuCuoiKy, StyleExcell.TextLeft));
                        STT++;
                        rowStart++;
                    }
                    lstValues.Add(new ClsExcel(++rowStart + 1, 0, "NGƯỜI LẬP BIỂU", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 1, 0, 2));
                    lstValues.Add(new ClsExcel(++rowStart + 1, 0, "(Ký ,họ tên )", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 1, 0, 2));
                    lstValues.Add(new ClsExcel(rowStart + 2, 0, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 8, 0, 2));
                    lstValues.Add(new ClsExcel(++rowStart - 1, 3, "KẾ TOÁN TRƯỞNG", StyleExcell.TextCenterBold, false, 12, true, rowStart - 1, rowStart - 1, 3, 5));
                    lstValues.Add(new ClsExcel(++rowStart - 1, 3, "(Ký ,họ tên )", StyleExcell.TextCenterBold, false, 12, true, rowStart - 1, rowStart - 1, 3, 5));
                    lstValues.Add(new ClsExcel(rowStart, 3, "", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart + 6, 3, 5));

                    string ngaythangnam = "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    lstValues.Add(new ClsExcel(++rowStart - 4, 6, ngaythangnam, StyleExcell.TextDatetime, false, 12, true, rowStart - 4, rowStart - 4, 6, 8));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 6, "GIÁM ĐỐC", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart - 4, 6, 8));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 6, "(Ký ,họ tên )", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart - 4, 6, 8));
                    lstValues.Add(new ClsExcel(rowStart - 3, 6, "", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart + 3, 6, 8));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "NguonVonDauTu_" + DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        // bao cáo các công trình thuộc nguồn vốn sửa chữa 
        //bao cao cong trinh Thuoc nguon von sua chua lon 
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_ERP_BCCongTrinhSCL()
        {
            ERP_BCCongTrinhSCLQuery oBCCongTrinhThuocNguonVonSuaChuaLonQuery = new ERP_BCCongTrinhSCLQuery(HttpContext.Current.Request);

            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/ERP_BCCongTrinhSCL/all/10-2021";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBCCongTrinhThuocNguonVonSuaChuaLonQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            var bcThang = oBCCongTrinhThuocNguonVonSuaChuaLonQuery.ThangBaoCao;
            var bcNam = oBCCongTrinhThuocNguonVonSuaChuaLonQuery.SearchNam;
            var bcDV = oBCCongTrinhThuocNguonVonSuaChuaLonQuery.LoaiDV;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcDV + "/" + bcThang + "-" + bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapERP_BCCongTrinhSCLJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(ERP_BCCongTrinhSCLJson),
                    new Dictionary<string, string>
                    {
                        {"TenCongTrinh", "LINE_DESCRIPTION"},
                        {"MaCongTrinh", "DISPLAY_ATT1"},
                        {"DuAnDuocDuyet", "NUM1"},
                        {"KHDuyetNamNay", "NUM2"},
                        {"SoDuDauNam", "NUM3"},
                        {"DaTriNamNayTrongKyBaoCao", "NUM4"},
                        {"VatLieuTuDauNam", "NUM5"},
                        {"NhanCongTuDauNam", "NUM6"},
                        {"MayThiCongTuDauNam", "NUM7"},
                        {"KhacTuDauNam", "NUM8"},
                        {"CongTuDauNam", "NUM9"},
                        {"VatLieuLKDC", "NUM10"},
                        {"NhanCongLKDC", "NUM11"},
                        {"MayThiCongLKDC", "NUM12"},
                        {"KhacLKDC", "NUM13"},
                        {"CongLKDC", "NUM14"},
                        //{"GhiChu", "NUM15"},
                        //{"TongHopChung", "NUM15"},
                        //{"SoSanhPhanTram", "NUM15"},



                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<ERP_BCCongTrinhSCLJson>>(model.data, settings);
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "TỔNG CÔNG TY ĐIỆN LỰC TP HÀ NỘI",
                TieuDeTrai2 = "CÔNG TY ĐIỆN LỰC HOÀI ĐỨC",
                Title = "BÁO CÁO CÁC CÔNG TRÌNH THUỘC NGUỒN VỐN SỬA CHỮA LỚN",
                LoaiBM = "Biểu 10/THKT",
                DVTinh = "ĐVT: Đồng",
                Thang = bcThang.ToString(),
                Nam = bcNam.ToString(),
                recordsTotal = oData.Count
            };

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBCCongTrinhThuocNguonVonSuaChuaLonQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBCCongTrinhThuocNguonVonSuaChuaLonQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng : ";
                    if (bcThang != null)
                        Tungaydenngay += bcThang.ToString();
                    Tungaydenngay += " - Năm ";
                    if (bcNam != null)
                        Tungaydenngay += bcNam.ToString();
                    string donvi = "Đơn Vị : ";
                    if (bcDV == "all")
                    {
                        donvi += "Tất cả";
                    }
                    else if (bcDV == "PD")
                    {
                        donvi += "Tổng Công ty Điện lực TP Hà Nội";
                    }
                    else if (bcDV == "PD0600")
                    {
                        donvi += "Công ty Điện lưc Thanh Trì";
                    }
                    else if (bcDV == "PD1400")
                    {
                        donvi += "Công ty Điện lực Long Biên";
                    }
                    else if (bcDV == "P")
                    {
                        donvi += "Tập đoàn Điện Lực Việt Nam";
                    }
                    else if (bcDV == "PD0700")
                    {
                        donvi += "Công ty Điện lực Gia Lâm";
                    }
                    else if (bcDV == "PD0800")
                    {
                        donvi += "Công ty Điện lực Đông Anh";
                    }
                    else if (bcDV == "PD0900")
                    {
                        donvi += "Công ty Điện lực Sóc Sơn";
                    }
                    else if (bcDV == "PD1800")
                    {
                        donvi += "Công ty Điện lực Chương Mỹ";
                    }
                    else if (bcDV == "PD2000")
                    {
                        donvi += "Công ty Điện lực Thường Tín";
                    }
                    else if (bcDV == "PD2400")
                    {
                        donvi += "Công ty Điện lực Mỹ Đức";
                    }
                    else if (bcDV == "PD2600")
                    {
                        donvi += "Công ty Điện lực Phúc Thọ";
                    }
                    else if (bcDV == "PD2500")
                    {
                        donvi += "Công ty Điện lực Phú Xuyên";
                    }
                    else if (bcDV == "PD2800")
                    {
                        donvi += "Công ty Điện lực Thanh Oai";
                    }
                    else if (bcDV == "PD2900")
                    {
                        donvi += "Công ty Điện lực Ứng Hòa";
                    }
                    else if (bcDV == "PD0100")
                    {
                        donvi += "Công ty Điện lực Hoàn Kiếm";
                    }
                    else if (bcDV == "PD1500")
                    {
                        donvi += "Công ty Điện lực Mê Linh";
                    }
                    else if (bcDV == "PD1700")
                    {
                        donvi += "Công ty Điện lực Sơn Tây ";
                    }
                    else if (bcDV == "PD1900")
                    {
                        donvi += "Công ty Điện lực Thạch Thất";
                    }
                    else if (bcDV == "PD2100")
                    {
                        donvi += "Công ty Điện lực Ba Vì";
                    }
                    else if (bcDV == "PD2200")
                    {
                        donvi += "Công ty Điện lực Đan Phượng";
                    }
                    else if (bcDV == "PD2300")
                    {
                        donvi += "Công ty Điện lực Hoài Đức";
                    }
                    else if (bcDV == "PD2700")
                    {
                        donvi += "Công ty Điện lực Quốc Oai ";
                    }
                    else if (bcDV == "PD0400")
                    {
                        donvi += "Công ty Điện lực Đống Đa ";
                    }
                    else if (bcDV == "PD0500")
                    {
                        donvi += "Công ty Điện lực Nam Từ Liêm ";
                    }
                    else if (bcDV == "PD0200")
                    {
                        donvi += "Công ty Điện lực Hai Bà Trưng ";
                    }
                    else if (bcDV == "PD1000")
                    {
                        donvi += "Công ty Điện lực Tây Hồ  ";
                    }
                    else if (bcDV == "PD1100")
                    {
                        donvi += "Công ty Điện lực Thanh Xuân ";
                    }
                    else if (bcDV == "PD1200")
                    {
                        donvi += "Công ty Điện lực Cầy giấy ";
                    }
                    else if (bcDV == "PD3000")
                    {
                        donvi += "Công ty Điện lực Bắc Từ Liêm ";
                    }
                    else if (bcDV == "PD1300")
                    {
                        donvi += "Công ty Điện lực Hoàng Mai ";
                    }
                    else if (bcDV == "PD0300")
                    {
                        donvi += "Công ty Điện lực Ba Đình ";
                    }
                    else if (bcDV == "PD1600")
                    {
                        donvi += "Công ty Điện lực Hà Đông ";
                    }
                    else
                    {
                        donvi += "Tổng Công ty Điện lực TP Hà Nội ";
                    };


                    lstValues.Add(new ClsExcel(2, 3, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 2, 2));
                    lstValues.Add(new ClsExcel(3, 9, donvi, StyleExcell.TextDatetime, false, 12, true, 3, 3, 9, 14));
                    int rowStart = 7;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, item.TenCongTrinh, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.MaCongTrinh, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.DuAnDuocDuyet, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.KHDuyetNamNay, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.SoDuDauNam, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.DaTriNamNayTrongKyBaoCao, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.VatLieuTuDauNam, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.NhanCongTuDauNam, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.MayThiCongTuDauNam, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.KhacTuDauNam, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.CongTuDauNam, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.VatLieuLKDC, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 12, item.NhanCongLKDC, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 13, item.MayThiCongLKDC, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 14, item.KhacLKDC, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 15, item.CongLKDC, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 16, item.GhiChu, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 17, item.TongHopChung, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 18, item.SoSanhPhanTram, StyleExcell.TextCenter));

                        STT++;
                        rowStart++;
                    }

                    lstValues.Add(new ClsExcel(3, 17, "Có tổng số: " + oData.Count + " bản ghi", StyleExcell.TextCenter, false, 12, true, 3, 3,17,18));



                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "NGƯỜI LẬP BIỂU ", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 2, 0, 3));
                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 6, 0, 3));
                    lstValues.Add(new ClsExcel(rowStart + 7, 0, dataGiaoDienRender.NguoiLapBieu, StyleExcell.TextCenterBold, false, 12, true, rowStart + 7, rowStart + 7, 0, 3));
                    lstValues.Add(new ClsExcel(rowStart + 8, 0, "(Ký, họ tên)", StyleExcell.TextCenterBold, false, 12, true, rowStart + 8, rowStart + 8, 0, 3));

                    lstValues.Add(new ClsExcel(++rowStart, 4, " KẾ TOÁN TRƯỞNG", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart, 4, 11));
                    lstValues.Add(new ClsExcel(++rowStart, 4, "", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart + 4, 4, 11));
                    lstValues.Add(new ClsExcel(rowStart + 5, 4, dataGiaoDienRender.KeToanTruong, StyleExcell.TextCenterBold, false, 12, true, rowStart + 5, rowStart + 5, 4, 11));
                    lstValues.Add(new ClsExcel(rowStart + 6, 4, "(Ký, họ tên)", StyleExcell.TextCenterBold, false, 12, true, rowStart + 6, rowStart + 6, 4, 11));

                    string ngaythangnam = "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    lstValues.Add(new ClsExcel(++rowStart - 3, 12, ngaythangnam, StyleExcell.TextDatetime, false, 12, true, rowStart - 3, rowStart - 3, 12, 18));
                    lstValues.Add(new ClsExcel(++rowStart - 3, 12, "GIÁM ĐỐC", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart - 3, 12, 18));
                    lstValues.Add(new ClsExcel(++rowStart - 3, 12, "", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart + 1, 12, 18));
                    //lstValues.Add(new ClsExcel(rowStart + 1, 2, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 2, 1, 1));
                    lstValues.Add(new ClsExcel(rowStart + 2, 12, dataGiaoDienRender.NguoiKy, StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 2, 12, 18));
                    lstValues.Add(new ClsExcel(rowStart + 3, 12, "(Ký, họ tên, đóng dấu)", StyleExcell.TextCenterBold, false, 12, true, rowStart + 3, rowStart + 3, 12, 18));

                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "BCCongTrinhThuocNguonVonSuaChuaLon_" + DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        //Tổng hợp độ tin cậy lưới điện phân phối
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_PMIS_GetAll_THDTCLuoiDienPhanPhoi()
        {
            PMIS_GetAll_THDTCLuoiDienPhanPhoiQuery oBaoCaoChiTeuDVKHQuery = new PMIS_GetAll_THDTCLuoiDienPhanPhoiQuery(HttpContext.Current.Request);
            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/PMIS_GetAll_THDTCLuoiDienPhanPhoi/02/2022";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBaoCaoChiTeuDVKHQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            var bcThang = oBaoCaoChiTeuDVKHQuery.Thang;
            var bcNam = oBaoCaoChiTeuDVKHQuery.Nam;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcThang + "/" + bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(PMIS_GetAll_THDTCLuoiDienPhanPhoiJson),
                    new Dictionary<string, string>
                    {
                        {"TenDonVi", "TENDONVI"},
                        {"TongSoKhachHang", "TONG_KH"},
                        {"TongSoLanMatDienThoangQua", "TQ_TONG_SL"},
                        {"TongSoKHBiMatDienThoang", "TQ_TONG_KH"},
                        {"MAIFI", "MAIFI"},
                        {"TongSoLanMatDienKeoDai", "KD_TONG_SL"},
                        {"TongSoKhachHangBiMatKeoDai", "KD_TONG_KH"},
                        {"TongThoiGianMatDienCuaKH", "KD_TONG_THOIGIAN"},
                        {"SAIDI", "SAIDI"},
                        {"SAIFI", "SAIFI"},
                       // {"CAIDI", "SAIFI"},
                       // {"SLLKN", "SAIFI"},
                      //  {"SSLKVoiKeHoachNam", "SAIFI"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var root = JsonConvert.DeserializeObject<MapPMIS_GetAll_THDTCLuoiDienPhanPhoiJson>(responseBody, settings);
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "TẬP ĐOÀN ĐIỆN LỰC VIỆT NAM",
                TieuDeTrai2 = "TỔNG CÔNG TY ĐIỆN LỰC TP HÀ NỘI",
                Title = "BÁO CÁO CHỈ SỐ ĐỘ TIN CẬY LƯỚI ĐIỆN PHÂN PHỐI",
                Nam = bcThang + "/" + bcNam,
                recordsTotal = root.data.Count
            };

            dataGiaoDienRender.data = root.data;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoChiTeuDVKHQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoChiTeuDVKHQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Thang: ";
                    if (bcThang != null)
                        Tungaydenngay += bcThang;
                    Tungaydenngay += " - Năm : ";
                    if (bcNam != null)
                        Tungaydenngay += bcNam;


                    lstValues.Add(new ClsExcel(5, 0, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 6, 6));
                    int rowStart = 9;

                    //set title
                    foreach (var item in root.data)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, item.TenDonVi, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.TongSoKhachHang, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.TongSoLanMatDienThoangQua, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.TongSoKHBiMatDienThoang, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.MAIFI, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.TongSoLanMatDienKeoDai, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.TongSoKhachHangBiMatKeoDai, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.TongThoiGianMatDienCuaKH, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.SAIDI, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.SAIFI, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.CAIDI, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.SLLKN, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 12, item.SSLKVoiKeHoachNam, StyleExcell.TextLeft));

                        rowStart++;

                    }
                    lstValues.Add(new ClsExcel(6, 11, "Có tổng số: " + root.data.Count + " bản ghi", StyleExcell.TextRight, false, 12,true,6,6,11,12));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "BaoCaoChiSoDoTinCayLuoiDienPhanPhoi_Thang" + bcThang + "_Nam_" + bcNam));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
     
        // GET: TongHopBanDienCuaDV
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_CMIS_TongHopSoHopDongMBDCacDV()
        {
            CMIS_TongHopSoHopDongMBDCacDVQuery oTongHopBanDienCuaDVQuery = new CMIS_TongHopSoHopDongMBDCacDVQuery(HttpContext.Current.Request);

            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/CMIS_TongHopBanDienCuaCacDV/PD0200/3/2022/1";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oTongHopBanDienCuaDVQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            var bcDV = oTongHopBanDienCuaDVQuery.MaDonVi;
            var bcNam = oTongHopBanDienCuaDVQuery.SearchNam;
            var bcThang = oTongHopBanDienCuaDVQuery.SearchThang;
            var LoaiBaoCao = oTongHopBanDienCuaDVQuery.LoaiBaoCao;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcDV + "/" + bcThang + "/" + bcNam + "/" + LoaiBaoCao;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapCMIS_TongHopSoHopDongMBDCacDVJson>(responseBody);
            var string1resultUP = model.data1.Replace("\\", "");
            var string2resultUP = model.data2.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(CMIS_TongHopSoHopDongMBDCacDVJson),
                    new Dictionary<string, string>
                    {
                        {"TenDonVi", "TENDONVI"},
                        {"DienNangTP", "TONGDTP"},
                        //{"DTTongCong", ""},
                        {"DTTienDien", "TIENDIEN"},
                        {"DTTienCSPK", "TIENCSPK"},
                        {"GiaBinhQuan", "GIABQKH"},
                        //{"ThueTongCong", ""},
                        {"ThueTienDien", "THUE_TD"},
                        {"ThueTienCSPK", "THUE_CSPK"},
                        {"DTvaThueGTGT", "THOIDIEM_KP_GIO"},
                        //{"LuyKeNam", "THOIDIEM_KP_GIO"},
                        //{"GiaBQ", "THOIDIEM_KP_GIO"},
                        //{"SoSanhLKVSCK", "THOIDIEM_KP_GIO"}
                       // {"SoSanhLKVSKHG", "THOIDIEM_KP_GIO"}
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<CMIS_TongHopSoHopDongMBDCacDVJson>>(string1resultUP, settings);

            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tập đoàn điện lực Việt Nam",
                TieuDeTrai2 = "TỔNG CÔNG TY ĐIỆN LỰC TP HÀ NỘI",
                Title = "TỔNG HỢP BÁN ĐIỆN CỦA CÁC ĐƠN VỊ ",
                LoaiBM = "KDDN 4A",
                Thang = bcThang.ToString(),
                Nam = bcNam.ToString(),

                recordsTotal = oData.Count
            };

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oTongHopBanDienCuaDVQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oTongHopBanDienCuaDVQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng: ";
                    if (bcThang != null)
                        Tungaydenngay += bcThang.ToString();
                    Tungaydenngay += " - Năm ";
                    if (bcNam != null)
                        Tungaydenngay += bcNam.ToString();

                    lstValues.Add(new ClsExcel(2, 3, Tungaydenngay, StyleExcell.TextCenter, false, 12, true, 2, 2, 3, 8));
                    string loaiBaoCao = "Loại báo cáo: ";
                    if (LoaiBaoCao == "1")
                    {
                        loaiBaoCao += "Báo cáo tháng";

                    }
                    else if (LoaiBaoCao == "2")
                    {
                        loaiBaoCao += "Báo cáo quý";
                    }
                    else if (LoaiBaoCao == "3")
                    {
                        loaiBaoCao += "Báo cáo 6 tháng";
                    }
                    else
                    {
                        loaiBaoCao += "Báo cáo năm";
                    }
                    string donvi = "Đơn Vị : ";
                    if (bcDV == "all")
                    {
                        donvi += "Tất cả";
                    }
                    else if (bcDV == "PD")
                    {
                        donvi += "Tổng Công ty Điện lực TP Hà Nội";
                    }
                    else if (bcDV == "PD0600")
                    {
                        donvi += "Công ty Điện lưc Thanh Trì";
                    }
                    else if (bcDV == "PD1400")
                    {
                        donvi += "Công ty Điện lực Long Biên";
                    }
                    else if (bcDV == "P")
                    {
                        donvi += "Tập đoàn Điện Lực Việt Nam";
                    }
                    else if (bcDV == "PD0700")
                    {
                        donvi += "Công ty Điện lực Gia Lâm";
                    }
                    else if (bcDV == "PD0800")
                    {
                        donvi += "Công ty Điện lực Đông Anh";
                    }
                    else if (bcDV == "PD0900")
                    {
                        donvi += "Công ty Điện lực Sóc Sơn";
                    }
                    else if (bcDV == "PD1800")
                    {
                        donvi += "Công ty Điện lực Chương Mỹ";
                    }
                    else if (bcDV == "PD2000")
                    {
                        donvi += "Công ty Điện lực Thường Tín";
                    }
                    else if (bcDV == "PD2400")
                    {
                        donvi += "Công ty Điện lực Mỹ Đức";
                    }
                    else if (bcDV == "PD2600")
                    {
                        donvi += "Công ty Điện lực Phúc Thọ";
                    }
                    else if (bcDV == "PD2500")
                    {
                        donvi += "Công ty Điện lực Phú Xuyên";
                    }
                    else if (bcDV == "PD2800")
                    {
                        donvi += "Công ty Điện lực Thanh Oai";
                    }
                    else if (bcDV == "PD2900")
                    {
                        donvi += "Công ty Điện lực Ứng Hòa";
                    }
                    else if (bcDV == "PD0100")
                    {
                        donvi += "Công ty Điện lực Hoàn Kiếm";
                    }
                    else if (bcDV == "PD1500")
                    {
                        donvi += "Công ty Điện lực Mê Linh";
                    }
                    else if (bcDV == "PD1700")
                    {
                        donvi += "Công ty Điện lực Sơn Tây ";
                    }
                    else if (bcDV == "PD1900")
                    {
                        donvi += "Công ty Điện lực Thạch Thất";
                    }
                    else if (bcDV == "PD2100")
                    {
                        donvi += "Công ty Điện lực Ba Vì";
                    }
                    else if (bcDV == "PD2200")
                    {
                        donvi += "Công ty Điện lực Đan Phượng";
                    }
                    else if (bcDV == "PD2300")
                    {
                        donvi += "Công ty Điện lực Hoài Đức";
                    }
                    else if (bcDV == "PD2700")
                    {
                        donvi += "Công ty Điện lực Quốc Oai ";
                    }
                    else if (bcDV == "PD0400")
                    {
                        donvi += "Công ty Điện lực Đống Đa ";
                    }
                    else if (bcDV == "PD0500")
                    {
                        donvi += "Công ty Điện lực Nam Từ Liêm ";
                    }
                    else if (bcDV == "PD0200")
                    {
                        donvi += "Công ty Điện lực Hai Bà Trưng ";
                    }
                    else if (bcDV == "PD1000")
                    {
                        donvi += "Công ty Điện lực Tây Hồ  ";
                    }
                    else if (bcDV == "PD1100")
                    {
                        donvi += "Công ty Điện lực Thanh Xuân ";
                    }
                    else if (bcDV == "PD1200")
                    {
                        donvi += "Công ty Điện lực Cầy giấy ";
                    }
                    else if (bcDV == "PD3000")
                    {
                        donvi += "Công ty Điện lực Bắc Từ Liêm ";
                    }
                    else if (bcDV == "PD1300")
                    {
                        donvi += "Công ty Điện lực Hoàng Mai ";
                    }
                    else if (bcDV == "PD0300")
                    {
                        donvi += "Công ty Điện lực Ba Đình ";
                    }
                    else if (bcDV == "PD1600")
                    {
                        donvi += "Công ty Điện lực Hà Đông ";
                    }
                    else
                    {
                        donvi += "Tổng Công ty Điện lực TP Hà Nội ";
                    };
                    lstValues.Add(new ClsExcel(3, 5, donvi, StyleExcell.TextCenter, false, 12, true, 3, 3, 5, 8));
                    lstValues.Add(new ClsExcel(3, 9, loaiBaoCao, StyleExcell.TextCenter, false, 12, true, 3, 3, 9, 10));
                    int rowStart = 7;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        string DTTongCong = (double.Parse(item.DTTienDien) + double.Parse(item.DTTienCSPK)).ToString();
                        string DTTongCong2 = (double.Parse(item.DTTienDien) + double.Parse(item.DTTienCSPK)).ToString();
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.TenDonVi, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.DienNangTP, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, DTTongCong, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.DTTienDien, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.DTTienCSPK, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.GiaBinhQuan, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, DTTongCong2, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.ThueTienDien, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.ThueTienCSPK, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, (double.Parse(item.DTTienDien) + double.Parse(item.DTTienCSPK) + double.Parse(item.ThueTienDien) + double.Parse(item.ThueTienCSPK)).ToString(), StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.ThueTienCSPK, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 12, item.ThueTienCSPK, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 13, item.ThueTienCSPK, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 14, item.ThueTienCSPK, StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }
                    lstValues.Add(new ClsExcel(4, 9, "Có tổng số: " + oData.Count + " bản ghi", StyleExcell.TextRight, false, 12, true, 4, 4, 9, 10));



                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "NGƯỜI LẬP BIỂU ", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 2, 0, 2));
                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 6, 0, 2));
                    lstValues.Add(new ClsExcel(rowStart + 7, 0, dataGiaoDienRender.NguoiLapBieu, StyleExcell.TextCenterBold, false, 12, true, rowStart + 7, rowStart + 7, 0, 2));

                    lstValues.Add(new ClsExcel(++rowStart, 3, " Tổ Tổng Hợp", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart, 3, 6));
                    lstValues.Add(new ClsExcel(++rowStart, 3, "", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart + 4, 3, 6));
                    lstValues.Add(new ClsExcel(rowStart + 5, 3, dataGiaoDienRender.ToTongHop, StyleExcell.TextCenterBold, false, 12, true, rowStart + 5, rowStart + 5, 3, 6));

                    string ngaythangnam = "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    lstValues.Add(new ClsExcel(++rowStart - 4, 7, ngaythangnam, StyleExcell.TextDatetime, false, 12, true, rowStart - 4, rowStart - 4, 7, 10));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 7, "KT.TRƯỞNG BAN", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart - 4, 7, 10));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 7, "PHÓ TRƯỞNG BAN", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart - 4, 7, 10));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 7, "", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart, 7, 10));
                    //lstValues.Add(new ClsExcel(rowStart + 1, 2, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 2, 1, 1));
                    lstValues.Add(new ClsExcel(rowStart + 1, 7, dataGiaoDienRender.NguoiKy, StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 1, 7, 10));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "TongHopBanDienCuaDV_DV" + bcDV + "_Thang_" + bcThang + "_Nam_" + bcNam));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        // chi tiet ban dien theo thanh phan phu tai
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_CMIS_ChiTietBanDienTheoTPPT()
        {
            CMIS_ChiTietBanDienTheoTPPTQuery oChiTietBanDienTheoTTPhuTaiQuery = new CMIS_ChiTietBanDienTheoTPPTQuery(HttpContext.Current.Request);

            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/CMIS_ChiTietBanDienTheoTPPT/PD0200/3/2021/1";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oChiTietBanDienTheoTTPhuTaiQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var nam = oChiTietBanDienTheoTTPhuTaiQuery.Nam;
            var thang = oChiTietBanDienTheoTTPhuTaiQuery.Thang;
            var bcDV = oChiTietBanDienTheoTTPhuTaiQuery.MaDonVi;
            var LoaiBaoCao = oChiTietBanDienTheoTTPhuTaiQuery.LoaiBaoCao;

            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcDV + "/" + thang + "/" + nam + "/" + LoaiBaoCao;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapCMIS_ChiTietBanDienTheoTPPTJson>(responseBody);
            var string1resultUP = model.data2.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(CMIS_ChiTietBanDienTheoTPPTJson),
                    new Dictionary<string, string>
                    {
                       {"ThanhPhanPhuTai", "TEN_TPPT"},
                       {"ThangBCBieu1", "BT"},
                       {"ThangBCBieu2", "CD"},
                       {"ThangBCBieu3", "TD"},
                       {"ThangBCBanDien1LoaiG", "KT"},
                       {"ThangBCTienDien", "DTHU"},
                      // {"ThangBCTongSanLuong", ""},

                       {"LuyKeNBieu1", "BTLK"},
                       {"LuyKeNBieu2", "CDLK"},
                       {"LuyKeNBieu3", "TDLK"},
                       {"LuyKeNTienDien", "DTHULK"},
                       {"LuyKeNBanDien1LoaiG", "KTLK"},
                      // {"LuyKeNTongSanLuong", "KTLK"},

                      // {"GTTPPTLKN", "KTLK"},
                      // {"TiLePTTPPT", "KTLK"},
                      
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<CMIS_ChiTietBanDienTheoTPPTJson>>(string1resultUP, settings);

            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tập đoàn điện lực Việt Nam",
                TieuDeTrai2 = "TỔNG CÔNG TY ĐIỆN LỰC TP HÀ NỘI",
                Title = "CHI TIẾT BÁN ĐIỆN THEO THÀNH PHẦN PHỤ TẢI",
                LoaiBM = "KDDN 8A",
                NguoiKy = "VŨ THẾ THẮNG",
                Thang = " 11",
                Nam = "2021",
                NguoiLapBieu = " TRẦN THỊ VÂN ANH ",
                ToTongHop = "  NGUYỄN ĐỨC AN ",

                recordsTotal = oData.Count
            };

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oChiTietBanDienTheoTTPhuTaiQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oChiTietBanDienTheoTTPhuTaiQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng: ";
                    if (thang.ToString() != null)
                        Tungaydenngay += thang;
                    Tungaydenngay += " -Năm: ";
                    if (nam.ToString() != null)
                        Tungaydenngay += nam;
                    lstValues.Add(new ClsExcel(2, 4, Tungaydenngay, StyleExcell.TextDatetime, false, 12, true, 2, 2, 4, 13));
                    int rowStart = 9;
                    int STT = 1;
                    string loaiBaoCao = "Loại báo cáo: ";
                    if (LoaiBaoCao == "1")
                    {
                        loaiBaoCao += "Báo cáo tháng";

                    }
                    else if (LoaiBaoCao == "2")
                    {
                        loaiBaoCao += "Báo cáo quý";
                    }
                    else if (LoaiBaoCao == "3")
                    {
                        loaiBaoCao += "Báo cáo 6 tháng";
                    }
                    else
                    {
                        loaiBaoCao += "Báo cáo năm";
                    }
                    string donvi = "Đơn Vị : ";
                    if (bcDV == "all")
                    {
                        donvi += "Tất cả";
                    }
                    else if (bcDV == "PD")
                    {
                        donvi += "Tổng Công ty Điện lực TP Hà Nội";
                    }
                    else if (bcDV == "PD0600")
                    {
                        donvi += "Công ty Điện lưc Thanh Trì";
                    }
                    else if (bcDV == "PD1400")
                    {
                        donvi += "Công ty Điện lực Long Biên";
                    }
                    else if (bcDV == "P")
                    {
                        donvi += "Tập đoàn Điện Lực Việt Nam";
                    }
                    else if (bcDV == "PD0700")
                    {
                        donvi += "Công ty Điện lực Gia Lâm";
                    }
                    else if (bcDV == "PD0800")
                    {
                        donvi += "Công ty Điện lực Đông Anh";
                    }
                    else if (bcDV == "PD0900")
                    {
                        donvi += "Công ty Điện lực Sóc Sơn";
                    }
                    else if (bcDV == "PD1800")
                    {
                        donvi += "Công ty Điện lực Chương Mỹ";
                    }
                    else if (bcDV == "PD2000")
                    {
                        donvi += "Công ty Điện lực Thường Tín";
                    }
                    else if (bcDV == "PD2400")
                    {
                        donvi += "Công ty Điện lực Mỹ Đức";
                    }
                    else if (bcDV == "PD2600")
                    {
                        donvi += "Công ty Điện lực Phúc Thọ";
                    }
                    else if (bcDV == "PD2500")
                    {
                        donvi += "Công ty Điện lực Phú Xuyên";
                    }
                    else if (bcDV == "PD2800")
                    {
                        donvi += "Công ty Điện lực Thanh Oai";
                    }
                    else if (bcDV == "PD2900")
                    {
                        donvi += "Công ty Điện lực Ứng Hòa";
                    }
                    else if (bcDV == "PD0100")
                    {
                        donvi += "Công ty Điện lực Hoàn Kiếm";
                    }
                    else if (bcDV == "PD1500")
                    {
                        donvi += "Công ty Điện lực Mê Linh";
                    }
                    else if (bcDV == "PD1700")
                    {
                        donvi += "Công ty Điện lực Sơn Tây ";
                    }
                    else if (bcDV == "PD1900")
                    {
                        donvi += "Công ty Điện lực Thạch Thất";
                    }
                    else if (bcDV == "PD2100")
                    {
                        donvi += "Công ty Điện lực Ba Vì";
                    }
                    else if (bcDV == "PD2200")
                    {
                        donvi += "Công ty Điện lực Đan Phượng";
                    }
                    else if (bcDV == "PD2300")
                    {
                        donvi += "Công ty Điện lực Hoài Đức";
                    }
                    else if (bcDV == "PD2700")
                    {
                        donvi += "Công ty Điện lực Quốc Oai ";
                    }
                    else if (bcDV == "PD0400")
                    {
                        donvi += "Công ty Điện lực Đống Đa ";
                    }
                    else if (bcDV == "PD0500")
                    {
                        donvi += "Công ty Điện lực Nam Từ Liêm ";
                    }
                    else if (bcDV == "PD0200")
                    {
                        donvi += "Công ty Điện lực Hai Bà Trưng ";
                    }
                    else if (bcDV == "PD1000")
                    {
                        donvi += "Công ty Điện lực Tây Hồ  ";
                    }
                    else if (bcDV == "PD1100")
                    {
                        donvi += "Công ty Điện lực Thanh Xuân ";
                    }
                    else if (bcDV == "PD1200")
                    {
                        donvi += "Công ty Điện lực Cầy giấy ";
                    }
                    else if (bcDV == "PD3000")
                    {
                        donvi += "Công ty Điện lực Bắc Từ Liêm ";
                    }
                    else if (bcDV == "PD1300")
                    {
                        donvi += "Công ty Điện lực Hoàng Mai ";
                    }
                    else if (bcDV == "PD0300")
                    {
                        donvi += "Công ty Điện lực Ba Đình ";
                    }
                    else if (bcDV == "PD1600")
                    {
                        donvi += "Công ty Điện lực Hà Đông ";
                    }
                    else
                    {
                        donvi += "Tổng Công ty Điện lực TP Hà Nội ";
                    };

                    //set title
                    foreach (var item in oData)
                    {
                        var thangTongSL = double.Parse(item.ThangBCBieu1) + double.Parse(item.ThangBCBieu2) + double.Parse(item.ThangBCBieu3) + double.Parse(item.ThangBCBanDien1LoaiG);
                        var lkTongSL = double.Parse(item.LuyKeNBieu1) + double.Parse(item.LuyKeNBieu2) + double.Parse(item.LuyKeNBieu3) + double.Parse(item.LuyKeNBanDien1LoaiG);
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.ThanhPhanPhuTai, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, thangTongSL, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.ThangBCBieu1, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.ThangBCBieu2, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.ThangBCBieu3, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.ThangBCBanDien1LoaiG, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.ThangBCTienDien, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, lkTongSL, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.LuyKeNBieu1, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.LuyKeNBieu2, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.LuyKeNBieu3, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 12, item.LuyKeNBanDien1LoaiG, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 13, item.LuyKeNTienDien, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 14, item.GTTPPTLKN, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 15, item.TiLePTTPPT, StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }
                    lstValues.Add(new ClsExcel(3, 14, "Có tổng số: " + oData.Count + " bản ghi", StyleExcell.TextRight, false, 12, true, 3, 3, 14, 15));
                    lstValues.Add(new ClsExcel(3, 4, donvi, StyleExcell.TextCenter, false, 12, true, 3, 3, 4, 8));
                    lstValues.Add(new ClsExcel(3, 9, loaiBaoCao, StyleExcell.TextCenter, false, 12, true, 3, 3, 9, 13));


                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "NGƯỜI LẬP BIỂU ", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 2, 0, 3));
                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 6, 0, 3));
                    lstValues.Add(new ClsExcel(rowStart + 7, 0,CurentUser.Title, StyleExcell.TextCenterBold, false, 12, true, rowStart + 7, rowStart + 7, 0, 3));

                    lstValues.Add(new ClsExcel(++rowStart, 4, " Tổ Tổng Hợp", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart, 4, 8));
                    lstValues.Add(new ClsExcel(++rowStart, 4, "", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart + 4, 4, 8));
                    lstValues.Add(new ClsExcel(rowStart + 5, 4, dataGiaoDienRender.ToTongHop, StyleExcell.TextCenterBold, false, 12, true, rowStart + 5, rowStart + 5, 4, 8));

                    string ngaythangnam = "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year; 
                    lstValues.Add(new ClsExcel(++rowStart - 4, 9, ngaythangnam, StyleExcell.TextDatetime, false, 12, true, rowStart - 4, rowStart - 4, 9, 15));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 9, "KT.TRƯỞNG BAN", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart - 4, 9, 15));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 9, "PHÓ TRƯỞNG BAN", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart - 4, 9, 15));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 9, "", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart, 9, 15));
                    //lstValues.Add(new ClsExcel(rowStart + 1, 2, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 2, 1, 1));
                    lstValues.Add(new ClsExcel(rowStart + 1, 9, dataGiaoDienRender.NguoiKy, StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 1, 9, 15));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "ChiTietBanDienTheoThanhPhanPhuTai_thang_" + thang + "_nam_" + nam ));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }


    }
}