﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ViPortal_Utils.Base;
using VIPortalAPI.BaseAPI;
using VIPortalAPP;

namespace VIPortalAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class LNotificationController : SIBaseAPI
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA()
        {
            LNotificationDA oLNotificationDA = new LNotificationDA();
            LNotificationQuery oLNotificationQuery = new LNotificationQuery(HttpContext.Current.Request);
            oLNotificationQuery._ModerationStatus = 1;
            var oData = oLNotificationDA.GetListJson(oLNotificationQuery);
            DataGridRender oGrid = new DataGridRender(oData, oLNotificationQuery.Draw,
                oLNotificationDA.TongSoBanGhiSauKhiQuery);
            return oGrid;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction CountChuaXem()
        {
            ResultAction oResultAction = new ResultAction();
            LNotificationDA lNotificationDA = new LNotificationDA(UrlSite);
            int TotalNotifi = lNotificationDA.GetCount(new LNotificationQuery() { 
                LNotiChuaXem_Id = CurentUser.ID,
                _ModerationStatus = 1, FieldOrder = "Created", Ascending = false, 
                LNotiNguoiNhan_Id = CurentUser.ID });
            oResultAction.Message = TotalNotifi.ToString();
            return oResultAction;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction UpdateDaXem(int ItemID)
        {

            ResultAction oResultAction = new ResultAction();
            if (ItemID > 0)
            {
                LNotificationDA lNotificationDA = new LNotificationDA(UrlSite);
                LNotificationItem lNotificationItem =  lNotificationDA.GetByIdToObjectSelectFields<LNotificationItem>(ItemID);
                if (string.IsNullOrEmpty(lNotificationItem.LNotiDaXem)) lNotificationItem.LNotiDaXem = ",";
                lNotificationItem.LNotiDaXem += ("" + CurentUser.ID + ",");
                if (string.IsNullOrEmpty(lNotificationItem.LNotiChuaXem_Id)) lNotificationItem.LNotiChuaXem_Id = "";
                lNotificationItem.LNotiChuaXem_Id = lNotificationItem.LNotiChuaXem_Id.Replace(("," + CurentUser.ID + ","), ",");
                lNotificationDA.UpdateOneOrMoreField(ItemID, new Dictionary<string, object>()
                {
                    {"LNotiChuaXem_Id", lNotificationItem.LNotiChuaXem_Id},{"LNotiDaXem", lNotificationItem.LNotiDaXem }
                });

                oResultAction = new ResultAction()
                {
                    State = ActionState.Succeed,
                    Message = "ok"
                };
            }
            return oResultAction;
        }
    }
}