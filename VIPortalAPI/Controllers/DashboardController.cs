﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using ViPortal_Utils.Base;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.Lconfig;

namespace VIPortalAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    public class DashboardController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public List<Dashboard_chart> QUERYDATA()
        {
            List<Dashboard_chart> oData = new List<Dashboard_chart>()
            {
                new Dashboard_chart() {country="Spain",year="Tiền điện", unit= "GWh", phaitra_dauthang= 2578,phaithu_dauthang= 26112,phaitra_cuoithang= 32203,phaithu_cuoithang= 58973},
                new Dashboard_chart() {country="Spain",year="Thuế tiền điện", unit= "GWh", phaitra_dauthang= 1000,phaithu_dauthang= 261,phaitra_cuoithang= 5000,phaithu_cuoithang= 58973},
                new Dashboard_chart() {country="Spain",year="Tiền CSPK", unit= "GWh", phaitra_dauthang= 2578,phaithu_dauthang= 26112,phaitra_cuoithang= 32203,phaithu_cuoithang= 58973},
                new Dashboard_chart() {country="Spain",year="Thuế CSPK", unit= "GWh", phaitra_dauthang= 1000,phaithu_dauthang= 261,phaitra_cuoithang= 500,phaithu_cuoithang= 58973},
                new Dashboard_chart() {country="Spain",year="Phạt vi phạm", unit= "GWh", phaitra_dauthang= 2578,phaithu_dauthang= 26112,phaitra_cuoithang= 32203,phaithu_cuoithang= 58973},
                new Dashboard_chart() {country="Spain",year="Tiền chậm trả", unit= "GWh", phaitra_dauthang= 1000,phaithu_dauthang= 1000,phaitra_cuoithang= 1000,phaithu_cuoithang= 58973}
            };
            
            return oData;

        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public List<TongHopSoHDMuaBanDienCuaDVJson> QUERYDATA_TongHopSoHDMuaBanDienCuaDV()
        {
            TongHopSoHDMuaBanDienCuaDVQuery oTongHopSoHDMuaBanDienCuaDVQuery = new TongHopSoHDMuaBanDienCuaDVQuery(HttpContext.Current.Request);

            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/CMIS_TongHopSoHopDongMBDCacDV";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_146", ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            var bcDV = oTongHopSoHDMuaBanDienCuaDVQuery.MaDonVi;
            var bcNam = oTongHopSoHDMuaBanDienCuaDVQuery.SearchNam;
            var bcThang = oTongHopSoHDMuaBanDienCuaDVQuery.SearchThang;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcDV + "/" + bcThang + "/" + bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapTongHopSoHDMuaBanDienCuaDVJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");
            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(TongHopSoHDMuaBanDienCuaDVJson),
                    new Dictionary<string, string>
                    {
                        {"TenDonVi", "TENDONVI"},
                        {"TongSo", "TONGHD"},
                        {"HDSinhHoat", "HDDANSU"},
                        {"HDNgoaiSinhHoat", "HDKINHTE"},
                        {"ThanhPhanPTNNLNTS", "HDG_NN_LN_TS"},
                        {"ThanhPhanPTCNXD", "HDG_CN_XD"},
                        {"ThanhPhanPTKDV", "HDG_KD_DV"},
                        {"ThanhPhanPTQLTD", "HDG_QL_TD"},
                        {"ThanhPhanPTKhac", "HDG_KHAC"}
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };
            var oData = JsonConvert.DeserializeObject<List<TongHopSoHDMuaBanDienCuaDVJson>>(string1resultUP, settings);
            return oData;
        }

        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public List<ChiTietSoThuVaSoDuCacKPTJson> QUERYDATA_ChiTietSoThuVaSoDuCacKPT()
        {
            ChiTietSoThuVaSoDuCacKPTQuery oBaoCaoSanLuongTietKiemCacDVTTQuery = new ChiTietSoThuVaSoDuCacKPTQuery(HttpContext.Current.Request);
            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/CMIS_ChiTietSoThuVaSoDuCacKPT/PD0200/5/2021/1";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_189", ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var bcNam = oBaoCaoSanLuongTietKiemCacDVTTQuery.SearchNam;
            var bcThang = oBaoCaoSanLuongTietKiemCacDVTTQuery.SearchThang;
            var loaiBC = oBaoCaoSanLuongTietKiemCacDVTTQuery.LoaiBaoCao;
            var bcDV = oBaoCaoSanLuongTietKiemCacDVTTQuery.MaDonVi;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcDV + "/" + bcThang + "/" + bcNam + "/" + loaiBC;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapChiTietSoThuVaSoDuCacKPTJson>(responseBody);
            var string1resultUP = model.data1.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(ChiTietSoThuVaSoDuCacKPTJson),
                    new Dictionary<string, string>
                    {
                        {"TEN_KM", "TEN_KM"},
                        {"PTRA_DKY", "PTRA_DKY"},
                        {"PTHU_DKY", "PTHU_DKY"},
                        {"PHATSINH", "PHATSINH"},
                        {"TTHUA_UT", "TTHUA_UT"},
                        {"TIEN_PBO", "TIEN_PBO"},
                        {"SO_THUDUOC", "SO_THUDUOC"},
                        {"PTRA_CKY", "PTRA_CKY"},
                        {"PTHU_CKY", "PTHU_CKY"},
                        {"NO_COKNTT", "NO_COKNTT"},
                        {"NO_KOKNTT", "NO_KOKNTT"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<ChiTietSoThuVaSoDuCacKPTJson>>(string1resultUP, settings);
            return oData;
        }

        // chi tiet ban dien theo thanh phan phu tai
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataGridRender QUERYDATA_ChiTietBanDienTheoTTPhuTai()
        {
            ChiTietBanDienTheoTTPhuTaiQuery oChiTietBanDienTheoTTPhuTaiQuery = new ChiTietBanDienTheoTTPhuTaiQuery(HttpContext.Current.Request);

            ConfigAPI oConfigAPI = new ConfigAPI();
            //oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/CMIS_ChiTietBanDienTheoTPPT/PD0200/3/2021/1";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/CMIS_ChiTietBanDienTheoTPPT";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_" + oChiTietBanDienTheoTTPhuTaiQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var nam = oChiTietBanDienTheoTTPhuTaiQuery.Nam;
            var thang = oChiTietBanDienTheoTTPhuTaiQuery.Thang;
            var bcDV = oChiTietBanDienTheoTTPhuTaiQuery.MaDonVi;
            var LoaiBaoCao = oChiTietBanDienTheoTTPhuTaiQuery.LoaiBaoCao;

            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcDV + "/" + thang + "/" + nam + "/" + LoaiBaoCao;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapChiTietBanDienTheoTTPhuTaiJson>(responseBody);
            var string1resultUP = model.data2.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(ChiTietBanDienTheoTTPhuTaiJson),
                    new Dictionary<string, string>
                    {
                       {"ThanhPhanPhuTai", "TEN_TPPT"},
                       {"ThangBCBieu1", "BT"},
                       {"ThangBCBieu2", "CD"},
                       {"ThangBCBieu3", "TD"},
                       {"ThangBCBanDien1LoaiG", "KT"},
                       {"ThangBCTienDien", "DTHU"},


                       {"LuyKeNBieu1", "BTLK"},
                       {"LuyKeNBieu2", "CDLK"},
                       {"LuyKeNBieu3", "TDLK"},
                       {"LuyKeNTienDien", "DTHULK"},
                       {"LuyKeNBanDien1LoaiG", "KTLK"},
                       {"GiaTriThanhPhanPhuTai", "a"},
                       {"TyLeThanhPhanPhuTai", "b"},


                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<ChiTietBanDienTheoTTPhuTaiJson>>(string1resultUP, settings);
            DataGridRender oGrid1 = new DataGridRender(oData, 1, oData.Count);
            
            return oGrid1;
        }
    }
    public class Dashboard_chart
    {
        public string country { get; set; }
        public string year { get; set; }
        public string unit { get; set; }
        public int phaitra_dauthang { get; set; }
        public int phaithu_dauthang { get; set; }
        public int phaitra_cuoithang { get; set; }
        public int phaithu_cuoithang { get; set; }
    }
}
