﻿using Library_Shared.Utilities;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using ViPortal_Utils.Base;
using VIPortalAPI.BaseAPI;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.Lconfig;

namespace VIPortalAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class LinhVucDauTuXayDungController : SIBaseAPI
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_TongHopTienDoTHCacCTDTXD()
        {
            DanhSacCongTrinhTheoKeHoachKCQuery oDanhSachCongTrinhTheoKeHoachKCQuery = new DanhSacCongTrinhTheoKeHoachKCQuery(HttpContext.Current.Request);

            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/DTXD_BCTHTienDoKhoiCongDongDienCCT";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oDanhSachCongTrinhTheoKeHoachKCQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var bcNam = oDanhSachCongTrinhTheoKeHoachKCQuery.Nam;
            var bcDV = oDanhSachCongTrinhTheoKeHoachKCQuery.MaDonVi;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcNam +"/"+ bcDV;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(TongHopTienDoTHCacCTDTXDJson),
                    new Dictionary<string, string>
                    {

                        {"DanhMucCongTrinh", "TenDuAn"},
                        {"LoaiHinhDuAn", "TenNhomDuAn"},
                        {"DonViQuanLy", "NangLucThietKe"},
                        {"KhoiCongKH", "NguonVonSuDung"},
                        {"HoanThanhKH", "TenTat"},
                        {"KhoiCongTT", "KC_Kehoach"},
                        {"DongDienTT", "KC_Thucte"},
                        {"TinhTrangDuAn", "TenTinhTrang"},

                    }
                }
            };
            string donvi;
            if (bcDV == "all")
            {
                donvi = "";
            }
            else if (bcDV == "PD")
            {
                donvi = "Tổng Công ty Điện lực TP Hà Nội";
            }
            else if (bcDV == "PD0600")
            {
                donvi = "Công ty Điện lưc Thanh Trì";
            }
            else if (bcDV == "PD1400")
            {
                donvi = "Công ty Điện lực Long Biên";
            }
            else if (bcDV == "P")
            {
                donvi = "Tập đoàn Điện Lực Việt Nam";
            }
            else if (bcDV == "PD0700")
            {
                donvi = "Công ty Điện lực Gia Lâm";
            }
            else if (bcDV == "PD0800")
            {
                donvi = "Công ty Điện lực Đông Anh";
            }
            else if (bcDV == "PD0900")
            {
                donvi = "Công ty Điện lực Sóc Sơn";
            }
            else if (bcDV == "PD1800")
            {
                donvi = "Công ty Điện lực Chương Mỹ";
            }
            else if (bcDV == "PD2000")
            {
                donvi = "Công ty Điện lực Thường Tín";
            }
            else if (bcDV == "PD2400")
            {
                donvi = "Công ty Điện lực Mỹ Đức";
            }
            else if (bcDV == "PD2600")
            {
                donvi = "Công ty Điện lực Phúc Thọ";
            }
            else if (bcDV == "PD2500")
            {
                donvi = "Công ty Điện lực Phú Xuyên";
            }
            else if (bcDV == "PD2800")
            {
                donvi = "Công ty Điện lực Thanh Oai";
            }
            else if (bcDV == "PD2900")
            {
                donvi = "Công ty Điện lực Ứng Hòa";
            }
            else if (bcDV == "PD0100")
            {
                donvi = "Công ty Điện lực Hoàn Kiếm";
            }
            else if (bcDV == "PD1500")
            {
                donvi = "Công ty Điện lực Mê Linh";
            }
            else if (bcDV == "PD1700")
            {
                donvi = "Công ty Điện lực Sơn Tây ";
            }
            else if (bcDV == "PD1900")
            {
                donvi = "Công ty Điện lực Thạch Thất";
            }
            else if (bcDV == "PD2100")
            {
                donvi = "Công ty Điện lực Ba Vì";
            }
            else if (bcDV == "PD2200")
            {
                donvi = "Công ty Điện lực Đan Phượng";
            }
            else if (bcDV == "PD2300")
            {
                donvi = "Công ty Điện lực Hoài Đức";
            }
            else if (bcDV == "PD2700")
            {
                donvi = "Công ty Điện lực Quốc Oai ";
            }
            else if (bcDV == "PD0400")
            {
                donvi = "Công ty Điện lực Đống Đa ";
            }
            else if (bcDV == "PD0500")
            {
                donvi = "Công ty Điện lực Nam Từ Liêm ";
            }
            else if (bcDV == "PD0200")
            {
                donvi = "Công ty Điện lực Hai Bà Trưng ";
            }
            else if (bcDV == "PD1000")
            {
                donvi = "Công ty Điện lực Tây Hồ  ";
            }
            else if (bcDV == "PD1100")
            {
                donvi = "Công ty Điện lực Thanh Xuân ";
            }
            else if (bcDV == "PD1200")
            {
                donvi = "Công ty Điện lực Cầy giấy ";
            }
            else if (bcDV == "PD3000")
            {
                donvi = "Công ty Điện lực Bắc Từ Liêm ";
            }
            else if (bcDV == "PD1300")
            {
                donvi = "Công ty Điện lực Hoàng Mai ";
            }
            else if (bcDV == "PD0300")
            {
                donvi = "Công ty Điện lực Ba Đình ";
            }
            else if (bcDV == "PD1600")
            {
                donvi = "Công ty Điện lực Hà Đông ";
            }
            else
            {
                donvi = "Tổng Công ty Điện lực TP Hà Nội ";
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var root = JsonConvert.DeserializeObject<MapTongHopTienDoTHCacCTDTXDJson>(responseBody, settings);

            //TestJson objectShareNew = Newtonsoft.Json.JsonConvert.DeserializeObject<TestJson>(responseBody);

            //double tong = root.data.Sum(x => double.Parse(x.KhoiCongTT));
            //double tong1 = root.data.Sum(x => double.Parse(x.KhoiCongKH));
            //double tong2 = root.data.Sum(x => double.Parse(x.DongDienTT));
            //double tong3 = root.data.Sum(x => double.Parse(x.HoanThanhKH));

            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tổng Công ty Điện lực TP Hà Nội",
                TieuDeTrai2 = donvi,
                TieuDeGiua1 = "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM",
                TieuDeGiua2 = "Độc lập - Tự do - Hạnh Phúc",
                Title = "BÁO CÁO TỔNG HỢP TIẾN ĐỘ THỰC HIỆN CÁC CÔNG TRÌNH XÂY DỰNG",
                //ToNangLuongTaiTao = tong.ToString() + "/" +  tong1.ToString(),
                //ToTongHop = tong2.ToString() + "/" + tong3.ToString(),


                Nam = "Năm: " + bcNam.ToString(),
                recordsTotal = root.data.Count,
        
            };

            foreach(TongHopTienDoTHCacCTDTXDJson getDetail in root.data)
            {

            }


            dataGiaoDienRender.data = root.data;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oDanhSachCongTrinhTheoKeHoachKCQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oDanhSachCongTrinhTheoKeHoachKCQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Năm: ";
                    if (oDanhSachCongTrinhTheoKeHoachKCQuery.Nam != null)
                        Tungaydenngay += oDanhSachCongTrinhTheoKeHoachKCQuery.Nam.ToString();


                    lstValues.Add(new ClsExcel(3, 4, Tungaydenngay, StyleExcell.TextDatetime, false, 12));
                    int rowStart = 10;
                    int STT = 1;
                    //set title
                    foreach (var item in root.data)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.DanhMucCongTrinh, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.LoaiHinhDuAn, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.DonViQuanLy, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.KhoiCongKH, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.HoanThanhKH, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.KhoiCongTT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.DongDienTT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.TinhTrangDuAn, StyleExcell.TextCenter));

                        rowStart++;
                        STT++;
                    }
                    lstValues.Add(new ClsExcel(6, 11, "Có tổng số: " + root.data.Count + " bản ghi", StyleExcell.TextRight, false, 12));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "DanhSachCongTrinhTheoKeHoachKhoiCong" + "_Nam: " + bcNam.ToString()));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_BaoCaoCongTrinhTheoKeHoachDongDien()
        {
            DanhSacCongTrinhTheoKeHoachDongDienQuery oDanhSachCongTrinhTheoKeHoachKCQuery = new DanhSacCongTrinhTheoKeHoachDongDienQuery(HttpContext.Current.Request);

            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/DTXD_BCTHTienDoKhoiCongDongDienCCT";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oDanhSachCongTrinhTheoKeHoachKCQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var bcNam = oDanhSachCongTrinhTheoKeHoachKCQuery.Nam;
            var bcDV = 0;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcNam+"/"+ bcDV;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(DanhSacCongTrinhTheoKeHoachDongDienJson),
                    new Dictionary<string, string>
                    {

                        {"DanhMucCongTrinh", "TenDuAn"},
                        {"NhomDuAn", "TenNhomDuAn"},
                        {"QuyMo", "NangLucThietKe"},
                        {"NguonVon", "NguonVonSuDung"},
                        {"DonViQLDA", "TenTat"},
                        {"TienDoKCKeHoach", "KC_Kehoach"},
                        {"TienDoKCThuHien", "KC_Thucte"},
                        {"TienDoDongDienKeHoach", "DD_Kehoach"},
                        {"TienDoDongDienThuHien", "DD_Thucte"},
                        {"TinhTrangDuAn", "TenTinhTrang"},
                        {"GhiChu", "GhiChu"},

                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var root = JsonConvert.DeserializeObject<MapDanhSacCongTrinhTheoKeHoachDongDienJson>(responseBody, settings);

            //TestJson objectShareNew = Newtonsoft.Json.JsonConvert.DeserializeObject<TestJson>(responseBody);


            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tập đoàn điện lực Việt Nam",
                TieuDeTrai2 = "TỔNG CÔNG TY ĐIỆN LỰC TP HÀ NỘI",
                TieuDeGiua1 = "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM",
                TieuDeGiua2 = "Độc lập - Tự do - Hạnh Phúc",
                Title = "BẢNG TỔNG HỢP TIẾN ĐỘ KHỞI CÔNG ĐÓNG ĐIỆN CÁC CÔNG TRÌNH ",
                Nam = "Năm: " + bcNam.ToString(),
                recordsTotal = root.data.Count
            };

            dataGiaoDienRender.data = root.data;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oDanhSachCongTrinhTheoKeHoachKCQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oDanhSachCongTrinhTheoKeHoachKCQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0) 
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Năm: ";
                    if (oDanhSachCongTrinhTheoKeHoachKCQuery.Nam != null)
                        Tungaydenngay += oDanhSachCongTrinhTheoKeHoachKCQuery.Nam.ToString();


                    lstValues.Add(new ClsExcel(3, 4, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 0, 0));
                    int rowStart = 10;
                    int STT = 1;
                    //set title
                    foreach (var item in root.data)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.DanhMucCongTrinh, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.NhomDuAn, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.QuyMo, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.NguonVon, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.DonViQLDA, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.TienDoKCKeHoach, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.TienDoKCThuHien, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.TienDoDongDienKeHoach, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.TienDoDongDienThuHien, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.TinhTrangDuAn, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.GhiChu, StyleExcell.TextCenter));

                        rowStart++;
                        STT++;
                    }
                    lstValues.Add(new ClsExcel(6, 11, "Có tổng số: " + root.data.Count + " bản ghi", StyleExcell.TextRight, false, 12));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "DanhSachCongTrinhTheoKeHoachDongDien" + "_Nam: " + bcNam.ToString()));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_BaoCaoCongTrinhThucTeKC()
        {
            DanhSacCongTrinhTheoThucTeKCQuery oDanhSachCongTrinhTheoKeHoachKCQuery = new DanhSacCongTrinhTheoThucTeKCQuery(HttpContext.Current.Request);

            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/DTXD_BCTHTienDoKhoiCongDongDienCCT";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oDanhSachCongTrinhTheoKeHoachKCQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var bcNam = oDanhSachCongTrinhTheoKeHoachKCQuery.Nam;
            var bcDV = 0;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcNam + "/" + bcDV;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(DanhSacCongTrinhTheoThucTeKCJson),
                    new Dictionary<string, string>
                    {

                        {"DanhMucCongTrinh", "TenLoaiHinh"},
                        {"NhomDuAn", "TenNhomDuAn"},
                        {"QuyMo", "NangLucThietKe"},
                        {"NguonVon", "NguonVonSuDung"},
                        {"DonViQLDA", "TenDonVi"},
                        {"TienDoKCKeHoach", "KC_Kehoach"},
                        {"TienDoKCThuHien", "KC_Thucte"},
                        {"TienDoDongDienKeHoach", "DD_Kehoach"},
                        {"TienDoDongDienThuHien", "DD_Thucte"},
                        {"TinhTrangDuAn", "TenTinhTrang"},
                        {"GhiChu", "GhiChu"},

                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var root = JsonConvert.DeserializeObject<MapDanhSacCongTrinhTheoThucTeKCJson>(responseBody, settings);

            //TestJson objectShareNew = Newtonsoft.Json.JsonConvert.DeserializeObject<TestJson>(responseBody);


            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tập đoàn điện lực Việt Nam",
                TieuDeTrai2 = "TỔNG CÔNG TY ĐIỆN LỰC TP HÀ NỘI",
                TieuDeGiua1 = "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM",
                TieuDeGiua2 = "Độc lập - Tự do - Hạnh Phúc",
                Title = "BẢNG TỔNG HỢP TIẾN ĐỘ KHỞI CÔNG ĐÓNG ĐIỆN CÁC CÔNG TRÌNH ",
                Nam = "Năm: " + bcNam.ToString(),
                recordsTotal = root.data.Count
            };

            dataGiaoDienRender.data = root.data;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oDanhSachCongTrinhTheoKeHoachKCQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oDanhSachCongTrinhTheoKeHoachKCQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Năm: ";
                    if (oDanhSachCongTrinhTheoKeHoachKCQuery.Nam != null)
                        Tungaydenngay += oDanhSachCongTrinhTheoKeHoachKCQuery.Nam.ToString();


                    lstValues.Add(new ClsExcel(3, 4, Tungaydenngay, StyleExcell.TextDatetime, false, 12));
                    int rowStart = 10;
                    int STT = 1;
                    //set title
                    foreach (var item in root.data)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.DanhMucCongTrinh, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.NhomDuAn, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.QuyMo, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.NguonVon, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.DonViQLDA, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.TienDoKCKeHoach, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.TienDoKCThuHien, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.TienDoDongDienKeHoach, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.TienDoDongDienThuHien, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.TinhTrangDuAn, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.GhiChu, StyleExcell.TextCenter));

                        rowStart++;
                        STT++;
                    }
                    lstValues.Add(new ClsExcel(6, 11, "Có tổng số: " + root.data.Count + " bản ghi", StyleExcell.TextRight, false, 12));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "DanhSachCongTrinhThucTeKhoiCong" + "_Nam: " + bcNam.ToString()));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_BaoCaoCongTrinhTheoThucTeDongDien()
        {
            DanhSacCongTrinhTheoThucTeDongDienQuery oDanhSachCongTrinhTheoKeHoachKCQuery = new DanhSacCongTrinhTheoThucTeDongDienQuery(HttpContext.Current.Request);

            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/DTXD_BCTHTienDoKhoiCongDongDienCCT";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oDanhSachCongTrinhTheoKeHoachKCQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var bcNam = oDanhSachCongTrinhTheoKeHoachKCQuery.Nam;
            var bcDV = 0;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcNam + "/" + bcDV;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(DanhSacCongTrinhTheoThucTeDongDienJson),
                    new Dictionary<string, string>
                    {

                        {"DanhMucCongTrinh", "TenLoaiHinh"},
                        {"NhomDuAn", "TenNhomDuAn"},
                        {"QuyMo", "NangLucThietKe"},
                        {"NguonVon", "NguonVonSuDung"},
                        {"DonViQLDA", "TenDonVi"},
                        {"TienDoKCKeHoach", "KC_Kehoach"},
                        {"TienDoKCThuHien", "KC_Thucte"},
                        {"TienDoDongDienKeHoach", "DD_Kehoach"},
                        {"TienDoDongDienThuHien", "DD_Thucte"},
                        {"TinhTrangDuAn", "TenTinhTrang"},
                        {"GhiChu", "GhiChu"},

                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var root = JsonConvert.DeserializeObject<MapDanhSacCongTrinhTheoThucTeDongDienJson>(responseBody, settings);

            //TestJson objectShareNew = Newtonsoft.Json.JsonConvert.DeserializeObject<TestJson>(responseBody);


            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tập đoàn điện lực Việt Nam",
                TieuDeTrai2 = "TỔNG CÔNG TY ĐIỆN LỰC TP HÀ NỘI",
                TieuDeGiua1 = "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM",
                TieuDeGiua2 = "Độc lập - Tự do - Hạnh Phúc",
                Title = "BẢNG TỔNG HỢP TIẾN ĐỘ KHỞI CÔNG ĐÓNG ĐIỆN CÁC CÔNG TRÌNH ",
                Nam = "Năm: " + bcNam.ToString(),
                recordsTotal = root.data.Count
            };

            dataGiaoDienRender.data = root.data;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oDanhSachCongTrinhTheoKeHoachKCQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oDanhSachCongTrinhTheoKeHoachKCQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Năm: ";
                    if (oDanhSachCongTrinhTheoKeHoachKCQuery.Nam != null)
                        Tungaydenngay += oDanhSachCongTrinhTheoKeHoachKCQuery.Nam.ToString();


                    lstValues.Add(new ClsExcel(3, 5, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 0, 0));
                    int rowStart = 10;
                    int STT = 1;
                    //set title
                    foreach (var item in root.data)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.DanhMucCongTrinh, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.NhomDuAn, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.QuyMo, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.NguonVon, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.DonViQLDA, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.TienDoKCKeHoach, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.TienDoKCThuHien, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.TienDoDongDienKeHoach, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.TienDoDongDienThuHien, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.TinhTrangDuAn, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.GhiChu, StyleExcell.TextCenter));

                        rowStart++;
                        STT++;
                    }
                    lstValues.Add(new ClsExcel(6, 11, "Có tổng số: " + root.data.Count + " bản ghi", StyleExcell.TextRight, false, 12));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "DanhSachCongTrinhThucTeDongDien" + "_Nam: " + bcNam.ToString()));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }

        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_TongHopDauTuXD()
        {
            TongHopDauTuXDQuery oDanhSachCongTrinhTheoKeHoachKCQuery = new TongHopDauTuXDQuery(HttpContext.Current.Request);

            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/DTXD_BaoCaoTongHopDTXD";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oDanhSachCongTrinhTheoKeHoachKCQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var bcNam = oDanhSachCongTrinhTheoKeHoachKCQuery.Nam;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(TongHopDauTuXDJson),
                    new Dictionary<string, string>
                    {

                        {"TenDuAN", "TenDuAn"},
                        {"DonViQuanLy", "TenTat"},
                        {"QuyMo", "QuyMo"},
                        {"NguonVon", "NguonVonSuDung"},
                        {"NgayGiao", "NgayGiaoNV"},
                        {"VonKeHoach", "GiaTriGiaoNV"},
                        {"SoQuyetDinh", "TMDT_QuyetDinh"},
                        {"TongMucDauTu", "TMDT"},
                        {"KhoiCongKH", "KH_NgayKC"},
                        {"KhoiCongThucTe", "TT_NgayKC"},
                        {"HoanThanhKeHoach", "KH_NgayHT"},
                        {"HoanThanhThucTe", "TT_NgayNT"},
                        {"TinhTrangDuAn", "TenTinhTrangKh"},
                        {"VuongMacKienNghi", "TenTinhTrang"},
                        { "ThongKe_DuAn_TongSo", "ThongKe_DuAn_TongSo" },
                        { "SoDA_KhoiCong" ,"SoDA_KhoiCong" },
                        { "SoDA_GiaoKhoiCong" ,"SoDA_GiaoKhoiCong" },
                        { "SoDA_HT","SoDA_HT" },
                        { " SoDA_GiaoHT","SoDA_GiaoHT" }
            }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var root = JsonConvert.DeserializeObject<MapTongHopDauTuXDJson>(responseBody, settings);

            //TestJson objectShareNew = Newtonsoft.Json.JsonConvert.DeserializeObject<TestJson>(responseBody);


            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tập đoàn điện lực Việt Nam",
                TieuDeTrai2 = "TỔNG CÔNG TY ĐIỆN LỰC TP HÀ NỘI",
                TieuDeGiua1 = "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM",
                TieuDeGiua2 = "Độc lập - Tự do - Hạnh Phúc",
                Title = "TỔNG HỢP BÁO CÁO ĐẦU TƯ XÂY DỰNG - BIỂU RÚT GỌN",
                Nam = "Năm: " + bcNam.ToString(),
                recordsTotal = root.data.Count
            };

            dataGiaoDienRender.data = root.data;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oDanhSachCongTrinhTheoKeHoachKCQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oDanhSachCongTrinhTheoKeHoachKCQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Năm: ";
                    if (oDanhSachCongTrinhTheoKeHoachKCQuery.Nam != null)
                        Tungaydenngay += oDanhSachCongTrinhTheoKeHoachKCQuery.Nam.ToString();


                    lstValues.Add(new ClsExcel(3, 0, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 3, 3));
                    int rowStart = 12;
                    int STT = 1;
                    int tongso = 0;
                    int SoDA_KhoiCong = 0;
                    int SoDA_GiaoKhoiCong = 0;
                    int SoDA_HT = 0;
                    int SoDA_GiaoHT = 0;
                    //set title
                    foreach (var item in root.data)
                    {
                        tongso += int.Parse(item.ThongKe_DuAn_TongSo);
                        SoDA_KhoiCong += int.Parse(item.SoDA_KhoiCong);
                        SoDA_GiaoKhoiCong += int.Parse(item.SoDA_GiaoKhoiCong);
                        SoDA_HT += int.Parse(item.SoDA_HT);
                        SoDA_GiaoHT += int.Parse(item.SoDA_GiaoHT);
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.TenDuAN, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.QuyMo, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.NguonVon, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.NgayGiao, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.VonKeHoach, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.SoQuyetDinh, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.TongMucDauTu, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.KhoiCongKH, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.KhoiCongThucTe, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.HoanThanhKeHoach, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.HoanThanhThucTe, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 12, item.TinhTrangDuAn, StyleExcell.TextCenter));

                        rowStart++;
                        STT++;

                    }
                    lstValues.Add(new ClsExcel(4, 1, "TỔNG SỐ DỰ ÁN : " + tongso.ToString(), StyleExcell.TextLeft, false, 12, true, 4, 4, 1, 2));
                    lstValues.Add(new ClsExcel(5, 1, "SỐ DỰ ÁN ĐÃ KHỞI CÔNG / SỐ DỰ ÁN GIAO KHỞI CÔNG: " + SoDA_KhoiCong.ToString() + "/" + SoDA_GiaoKhoiCong.ToString(), StyleExcell.TextLeft, false, 12, true, 5, 5, 1, 2));
                    lstValues.Add(new ClsExcel(6, 1, "SỐ DỰ ÁN ĐÃ HOÀN THÀNH / SỐ DỰ ÁN GIAO HOÀN THÀNH:" + SoDA_HT.ToString() + "/" + SoDA_GiaoHT.ToString(), StyleExcell.TextLeft, false, 12, true, 6, 6, 1, 2));
                    lstValues.Add(new ClsExcel(8, 10, "Có tổng số: " + root.data.Count + " bản ghi", StyleExcell.TextRight, false, 12, true, 8, 8, 10, 12));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "BaoCaoTongHopDauTuXayDung-BieuRutGon" + "_Nam: " + bcNam.ToString()));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_ThuHienKeHoachDTXD()
        {
            ThuHienKeHoachDTXDQuery oThucHienKeHoachDauTuXDQuery = new ThuHienKeHoachDTXDQuery(HttpContext.Current.Request);

            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/DTXD_BaoCaoThucHienKeHoachDTXD/15001/2021/12";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oThucHienKeHoachDauTuXDQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var bcNam = oThucHienKeHoachDauTuXDQuery.Nam;
            var bcThang = oThucHienKeHoachDauTuXDQuery.thang;
            var madonvi = oThucHienKeHoachDauTuXDQuery.maDonVi;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + madonvi + "/" + bcNam + "/" + bcThang;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(ThucHienKeHoachDauTuXayDungJson),
                    new Dictionary<string, string>
                    {

                        {"LoaiHinh", "TenLoaiHinh"},
                        {"TongSoKH", "KH_CC_TongSo"},
                        {"XayLapKH", "KH_CC_XayLap"},
                        {"ThietBiKH", "KH_CC_ThietBi"},
                        {"KhacKH", "KH_CC_Khac"},
                        {"TongSoTH", "tt_TongSo"},
                        {"XayLapTH", "tt_XayLap"},
                        {"ThietBiTH", "tt_ThietBi"},
                        {"KhacTH", "tt_Khac"},
                        //{"TongSoUocBC", ""},
                        //{"XayLapUocBC", ""},
                       // {"ThietBiUocBC", ""},
                       // {"KhacUocBC", ""},
                        {"TongSoLK", "LK_TongSo"},
                        {"XayLapLK", "LK_XayLap"},
                        {"ThietBiLK", "LK_ThietBi"},
                        {"KhacLK", "LK_Khac"},
                      //  {"GiaTriNghiemThu", ""},
                      //  {"GiaTriPhieu", ""},
                       // {"GiaTriGiaiNgan", ""},

                    }
                }
            };
            string donvi;
            if (madonvi == "all")
            {
                donvi = "";
            }
            else if (madonvi == "PD")
            {
                donvi = "Tổng Công ty Điện lực TP Hà Nội";
            }
            else if (madonvi == "PD0600")
            {
                donvi = "Công ty Điện lưc Thanh Trì";
            }
            else if (madonvi == "PD1400")
            {
                donvi = "Công ty Điện lực Long Biên";
            }
            else if (madonvi == "P")
            {
                donvi = "Tập đoàn Điện Lực Việt Nam";
            }
            else if (madonvi == "PD0700")
            {
                donvi = "Công ty Điện lực Gia Lâm";
            }
            else if (madonvi == "PD0800")
            {
                donvi = "Công ty Điện lực Đông Anh";
            }
            else if (madonvi == "PD0900")
            {
                donvi = "Công ty Điện lực Sóc Sơn";
            }
            else if (madonvi == "PD1800")
            {
                donvi = "Công ty Điện lực Chương Mỹ";
            }
            else if (madonvi == "PD2000")
            {
                donvi = "Công ty Điện lực Thường Tín";
            }
            else if (madonvi == "PD2400")
            {
                donvi = "Công ty Điện lực Mỹ Đức";
            }
            else if (madonvi == "PD2600")
            {
                donvi = "Công ty Điện lực Phúc Thọ";
            }
            else if (madonvi == "PD2500")
            {
                donvi = "Công ty Điện lực Phú Xuyên";
            }
            else if (madonvi == "PD2800")
            {
                donvi = "Công ty Điện lực Thanh Oai";
            }
            else if (madonvi == "PD2900")
            {
                donvi = "Công ty Điện lực Ứng Hòa";
            }
            else if (madonvi == "PD0100")
            {
                donvi = "Công ty Điện lực Hoàn Kiếm";
            }
            else if (madonvi == "PD1500")
            {
                donvi = "Công ty Điện lực Mê Linh";
            }
            else if (madonvi == "PD1700")
            {
                donvi = "Công ty Điện lực Sơn Tây ";
            }
            else if (madonvi == "PD1900")
            {
                donvi = "Công ty Điện lực Thạch Thất";
            }
            else if (madonvi == "PD2100")
            {
                donvi = "Công ty Điện lực Ba Vì";
            }
            else if (madonvi == "PD2200")
            {
                donvi = "Công ty Điện lực Đan Phượng";
            }
            else if (madonvi == "PD2300")
            {
                donvi = "Công ty Điện lực Hoài Đức";
            }
            else if (madonvi == "PD2700")
            {
                donvi = "Công ty Điện lực Quốc Oai ";
            }
            else if (madonvi == "PD0400")
            {
                donvi = "Công ty Điện lực Đống Đa ";
            }
            else if (madonvi == "PD0500")
            {
                donvi = "Công ty Điện lực Nam Từ Liêm ";
            }
            else if (madonvi == "PD0200")
            {
                donvi = "Công ty Điện lực Hai Bà Trưng ";
            }
            else if (madonvi == "PD1000")
            {
                donvi = "Công ty Điện lực Tây Hồ  ";
            }
            else if (madonvi == "PD1100")
            {
                donvi = "Công ty Điện lực Thanh Xuân ";
            }
            else if (madonvi == "PD1200")
            {
                donvi = "Công ty Điện lực Cầy giấy ";
            }
            else if (madonvi == "PD3000")
            {
                donvi = "Công ty Điện lực Bắc Từ Liêm ";
            }
            else if (madonvi == "PD1300")
            {
                donvi = "Công ty Điện lực Hoàng Mai ";
            }
            else if (madonvi == "PD0300")
            {
                donvi = "Công ty Điện lực Ba Đình ";
            }
            else if (madonvi == "PD1600")
            {
                donvi = "Công ty Điện lực Hà Đông ";
            }
            else
            {
                donvi = "Tổng Công ty Điện lực TP Hà Nội ";
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var root = JsonConvert.DeserializeObject<MapThucHienKeHoachDauTuXayDungJson>(responseBody, settings);

            //TestJson objectShareNew = Newtonsoft.Json.JsonConvert.DeserializeObject<TestJson>(responseBody);


            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tổng Công ty Điện lực TP Hà Nội",
                TieuDeTrai2 = donvi,
                TieuDeGiua1 = "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM",
                TieuDeGiua2 = "Độc lập - Tự do - Hạnh Phúc",
                Title = "BÁO CÁO THỰC HIỆN KẾ HOẠCH ĐẦU TƯ XÂY DỰNG NĂM",
                NguoiLapBieu=CurentUser.Title,
                Thang = "Tháng " + bcThang,
                Nam = " Năm " + bcNam,

                recordsTotal = root.data.Count
            };

            dataGiaoDienRender.data = root.data;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oThucHienKeHoachDauTuXDQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oThucHienKeHoachDauTuXDQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng: ";
                    if (bcThang != null)
                        Tungaydenngay += bcThang;
                    Tungaydenngay += " Năm: ";
                    if (bcNam!= null)
                        Tungaydenngay += bcNam;

                    lstValues.Add(new ClsExcel(5, 8, Tungaydenngay, StyleExcell.TextDatetime, false, 12, true, 5, 5, 8, 11));
                    int rowStart = 10;
                    int STT = 1;
                    //set title
                    foreach (var item in root.data)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.LoaiHinh, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.TongSoKH, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.XayLapKH, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.ThietBiKH, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.KhacKH, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.TongSoTH, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.XayLapTH, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.ThietBiTH, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.KhacTH, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.TongSoUocBC, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.XayLapUocBC, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 12, item.ThietBiUocBC, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 13, item.KhacUocBC, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 14, item.TongSoLK, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 15, item.XayLapLK, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 16, item.ThietBiLK, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 17, item.KhacLK, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 18, item.GiaTriNghiemThu, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 19, item.GiaTriPhieu, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 20, item.GiaTriGiaiNgan, StyleExcell.TextCenter));

                        rowStart++;
                        STT++;
                    }
                    lstValues.Add(new ClsExcel(6, 19, "Có tổng số: " + root.data.Count + " bản ghi", StyleExcell.TextRight, false, 12, true, 6, 6, 19, 20));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "BaoCaoThucHienKeHoachDauTuXayDungNam" + "_Nam: " + bcNam.ToString()));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }



        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_BaoCaoCongTrinhTheoKeHoachKC()
        {
            DanhSacCongTrinhTheoKeHoachKCQuery oDanhSachCongTrinhTheoKeHoachKCQuery = new DanhSacCongTrinhTheoKeHoachKCQuery(HttpContext.Current.Request);

            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/DTXD_BCTHTienDoKhoiCongDongDienCCT";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_" + oDanhSachCongTrinhTheoKeHoachKCQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var bcNam = oDanhSachCongTrinhTheoKeHoachKCQuery.Nam;
            var bcDV = 0;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcNam + "/" + bcDV;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(DanhSacCongTrinhTheoKeHoachKCJson),
                    new Dictionary<string, string>
                    {

                        {"DanhMucCongTrinh", "TenDuAn"},
                        {"NhomDuAn", "TenNhomDuAn"},
                        {"QuyMo", "NangLucThietKe"},
                        {"NguonVon", "NguonVonSuDung"},
                        {"DonViQLDA", "TenTat"},
                        {"TienDoKCKeHoach", "KC_Kehoach"},
                        {"TienDoKCThuHien", "KC_Thucte"},
                        {"TienDoDongDienKeHoach", "DD_Kehoach"},
                        {"TienDoDongDienThuHien", "DD_Thucte"},
                        {"TinhTrangDuAn", "TenTinhTrang"},
                        {"GhiChu", "GhiChu"},

                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var root = JsonConvert.DeserializeObject<MapDanhSacCongTrinhTheoKeHoachKCJson>(responseBody, settings);

            //TestJson objectShareNew = Newtonsoft.Json.JsonConvert.DeserializeObject<TestJson>(responseBody);
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tập đoàn điện lực Việt Nam",
                TieuDeTrai2 = "TỔNG CÔNG TY ĐIỆN LỰC TP HÀ NỘI",
                TieuDeGiua1 = "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM",
                TieuDeGiua2 = "Độc lập - Tự do - Hạnh Phúc",
                Title = "BẢNG TỔNG HỢP TIẾN ĐỘ KHỞI CÔNG ĐÓNG ĐIỆN CÁC CÔNG TRÌNH",
                Nam = "Năm: " + bcNam.ToString(),
                recordsTotal = root.data.Count
            };

            dataGiaoDienRender.data = root.data;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oDanhSachCongTrinhTheoKeHoachKCQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oDanhSachCongTrinhTheoKeHoachKCQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Năm: ";
                    if (oDanhSachCongTrinhTheoKeHoachKCQuery.Nam != null)
                        Tungaydenngay += oDanhSachCongTrinhTheoKeHoachKCQuery.Nam.ToString();


                    lstValues.Add(new ClsExcel(3, 4, Tungaydenngay, StyleExcell.TextDatetime, false, 12));
                    int rowStart = 10;
                    int STT = 1;
                    //set title
                    foreach (var item in root.data)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.DanhMucCongTrinh, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.NhomDuAn, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.QuyMo, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.NguonVon, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.DonViQLDA, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.TienDoKCKeHoach, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.TienDoKCThuHien, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.TienDoDongDienKeHoach, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.TienDoDongDienThuHien, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.TinhTrangDuAn, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.GhiChu, StyleExcell.TextCenter));

                        rowStart++;
                        STT++;
                    }
                    lstValues.Add(new ClsExcel(6, 11, "Có tổng số: " + root.data.Count + " bản ghi", StyleExcell.TextRight, false, 12));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "DanhSachCongTrinhTheoKeHoachKhoiCong" + "_Nam: " + bcNam.ToString()));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        }
    }