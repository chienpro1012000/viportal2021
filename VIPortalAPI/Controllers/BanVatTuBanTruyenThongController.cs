﻿using Library_Shared.Utilities;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using ViPortal_Utils.Base;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.Lconfig;

namespace VIPortalAPI.Controllers
{
    public class BanVatTuBanTruyenThongController : ApiController
    {
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_BaoCaoVeViecSuDungXeTaiCau()
        {
            BaoCaoSuDungXeTaiCauQuery oBaoCaoSuDungXeTaiCauQuery = new BaoCaoSuDungXeTaiCauQuery(HttpContext.Current.Request);

            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $" https://portal.evnhanoi.vn/apigateway/XeTaiCau_BaoCaoVeViecSuDungXeTaiCau/C07/3/2022";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBaoCaoSuDungXeTaiCauQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            var thang = oBaoCaoSuDungXeTaiCauQuery.thang;
            var nam = oBaoCaoSuDungXeTaiCauQuery.Nam;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/"+ oBaoCaoSuDungXeTaiCauQuery.MaDonVi + "/" + oBaoCaoSuDungXeTaiCauQuery.thang + "/" + oBaoCaoSuDungXeTaiCauQuery.Nam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(BaoCaoSuDungXeTaiCauJson),
                    new Dictionary<string, string>
                    {

                        {"ThoiGian", "Thoigian"},
                        {"MaLenh", "Macode"},
                        {"NoiDungCongViec", "Noidungcongviec"},
                        {"BKSXe", "BKSXe"},
                        //{"SoTien", "BTG"},
                        {"GhiChu", "ghichu"},



                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var root = JsonConvert.DeserializeObject<MapBaoCaoSuDungXeTaiCauJson>(responseBody, settings);
            //chua check duoc dư lieu khi mà show len man hinh cai nào là cha va con
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tập đoàn điện lực Việt Nam",
                TieuDeTrai2 = oBaoCaoSuDungXeTaiCauQuery.DonViText,
                TieuDeTrai3 = "Số:______/BC-(__________)",
                TieuDeGiua1 = "Cộng hòa xã hộ chủ nghĩa Việt Nam",
                TieuDeGiua2 = "Độc lập - Tự do - Hạnh phúc",
                Title = "Báo cáo về việc sử dụng xe tải cẩu ",
                LoaiBM = "BM: B06-7",
                Thang = thang,
                Nam = nam,
                recordsTotal = root.data.Count
            };
            dataGiaoDienRender.data = root.data;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoSuDungXeTaiCauQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoSuDungXeTaiCauQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng   ";
                    if (thang.ToString() != null)
                        Tungaydenngay += thang;
                    Tungaydenngay += "  Năm";
                    if (nam.ToString() != null)
                        Tungaydenngay += nam;
                    lstValues.Add(new ClsExcel(4, 3, Tungaydenngay, StyleExcell.TextDatetime, false, 12,false,4,4));

                    int rowStart = 9;
                    int STT = 1;
                    //set title
                    foreach (var item in root.data)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.ThoiGian, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.MaLenh, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.NoiDungCongViec, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.BKSXe, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.SoTien, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.GhiChu, StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }
                   // lstValues.Add(new ClsExcel(7, 6, "Có tổng số: " + root.data.Count + " bản ghi", StyleExcell.TextRight, false, 12, true, 7, 7, 6, 7));


                    lstValues.Add(new ClsExcel(++rowStart, 0, "Nơi nhận::", StyleExcell.TextLeft, false, 12, true, rowStart, rowStart, 0, 3));

                    lstValues.Add(new ClsExcel(++rowStart, 0, "- Như trên;", StyleExcell.TextLeft, false, 12, true, rowStart, rowStart, 0, 3));
                    lstValues.Add(new ClsExcel(++rowStart, 0, "- Lưu: VT", StyleExcell.TextLeft, false, 12, true, rowStart, rowStart, 0, 3));

                /*    lstValues.Add(new ClsExcel(++rowStart - 3, 5, "Trưởng đơn vị", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart - 3, 5, 6));
                    lstValues.Add(new ClsExcel(++rowStart - 3, 5, "(Ký tên ,đóng dấu )", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart - 3, 5, 6));
                    lstValues.Add(new ClsExcel(++rowStart - 3, 5, "", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart + 1, 5, 6));*/



                    //lstValues.Add(new ClsExcel(++rowStart + 2, 1, "NGƯỜI LẬP BIỂU  ", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 2, 1, 3));
                    //lstValues.Add(new ClsExcel(++rowStart + 2, 1, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 6, 1, 3));
                    //lstValues.Add(new ClsExcel(rowStart + 7, 1, dataGiaoDienRender.NguoiLapBieu, StyleExcell.TextCenterBold, false, 12, true, rowStart + 7, rowStart + 7, 1, 3));



                    lstValues.Add(new ClsExcel(++rowStart - 1, 5, "TRƯỞNG ĐƠN VỊ", StyleExcell.TextCenterBold, false, 12, true, rowStart - 1, rowStart - 1, 5, 6));
                    //lstValues.Add(new ClsExcel(++rowStart - 1, 9, "PHÓ TRƯỞNG BAN", StyleExcell.TextCenterBold, false, 12, true, rowStart - 1, rowStart - 1, 9, 14));
                    //lstValues.Add(new ClsExcel(++rowStart - 1, 9, "", StyleExcell.TextCenterBold, false, 12, true, rowStart - 1, rowStart + 2, 9, 14));
                    //lstValues.Add(new ClsExcel(rowStart + 1, 2, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 2, 1, 1));
                    //lstValues.Add(new ClsExcel(rowStart + 3, 9, dataGiaoDienRender.NguoiKy, StyleExcell.TextCenterBold, false, 12, true, rowStart + 3, rowStart + 3, 9, 14));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "BaoCaoSuDungXeTaiCau_Thang_" + thang + "_Nam_" + nam));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        // Báo cáo Tổng hợp chi phí sử dụng xe tải cẩu toàn Tổng công ty
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_XeTaiCau_BCTHChiPhiSuDungXeTaiCau()
        {
            XeTaiCau_BCTHChiPhiSuDungXeTaiCauQuery oXeTaiCau_BCTHChiPhiSuDungXeTaiCauQuery = new XeTaiCau_BCTHChiPhiSuDungXeTaiCauQuery(HttpContext.Current.Request);

            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/HRMS_BaoCaoTrinhDoCMNV/281/01-01-2022";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oXeTaiCau_BCTHChiPhiSuDungXeTaiCauQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            string bcNam = oXeTaiCau_BCTHChiPhiSuDungXeTaiCauQuery.Nam;
            string bcThang = oXeTaiCau_BCTHChiPhiSuDungXeTaiCauQuery.Thang;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcThang + "/" + bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(XeTaiCau_BCTHChiPhiSuDungXeTaiCauJson),
                    new Dictionary<string, string>
                    {
                        {"STT", "STT"},
                        {"donvisudung", "donvisudung"},
                        {"SoChuyen", "SoChuyen"},
                        {"SoKM", "SoKM"},
                        {"SoGioCau", "SoGioCau"},
                        {"ghichu", "ghichu"},
                        {"TenDV", "TenDV"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var root = JsonConvert.DeserializeObject<MapXeTaiCau_BCTHChiPhiSuDungXeTaiCauJson>(responseBody, settings);


            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = " TỔNG CÔNG TY ĐIỆN LỰC TP.HÀ NỘI",
                TieuDeTrai2 = "BAN VẬT TƯ",
                Title = "Báo cáo Tổng hợp chi phí sử dụng xe tải cẩu toàn Tổng công ty",
                LoaiBM = "BM:B06-8",
                TieuDeGiua1 = "CỘNG HOÀ XÃ HỘI CHỦ NGHĨA VIỆT NAM",
                TieuDeGiua2 = "Độc lập - Tự do - Hạnh phúc",
                Nam = bcNam,
                Thang = bcThang,


                recordsTotal = root.data.Count
            };

            dataGiaoDienRender.data = root.data;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oXeTaiCau_BCTHChiPhiSuDungXeTaiCauQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oXeTaiCau_BCTHChiPhiSuDungXeTaiCauQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng: ";
                    if (bcThang != null)
                        Tungaydenngay += bcThang;
                    Tungaydenngay += " - Năm : ";
                    if (bcNam != null)
                        Tungaydenngay += bcNam;
                    lstValues.Add(new ClsExcel(8, 0, Tungaydenngay, StyleExcell.TextDatetime, false, 12, true, 8, 8, 0, 6));
                    int rowStart = 12;
                    int STT = 1;
                    //set title
                    foreach (var item in root.data)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, item.STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.donvisudung, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.SoChuyen, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.SoKM, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.SoGioCau, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, "", StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.ghichu, StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }
                    lstValues.Add(new ClsExcel(9, 5, "Có tổng số: " + root.data.Count + " bản ghi", StyleExcell.TextRight, false, 12, true, 9, 9, 5, 6));

                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "XeTaiCau_BCTHChiPhiSuDungXeTaiCau_" + DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_BangSoSanhGiaTriTonKhoDTXD()
        {
            BangSoSanhGiaTriTonKhoDTXDQuery oBangSoSanhGiaTriTonKhoDTXDQuery = new BangSoSanhGiaTriTonKhoDTXDQuery(HttpContext.Current.Request);


            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/ERP_BangSoSanhGiaTriTonKho_DTXD/all/09-2021";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBangSoSanhGiaTriTonKhoDTXDQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var bcNam = oBangSoSanhGiaTriTonKhoDTXDQuery.Nam;
            var bcQuyBaoCao = oBangSoSanhGiaTriTonKhoDTXDQuery.Thang;
            var bcDV = oBangSoSanhGiaTriTonKhoDTXDQuery.MaDonVi;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/"+bcDV+ "/" + bcQuyBaoCao + "-" + bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapBangSoSanhGiaTriTonKhoDTXDJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(BangSoSanhGiaTriTonKhoDTXDJson),
                    new Dictionary<string, string>
                    {
                        {"DonVi", "LINE_DESCRIPTION"},
                        {"NUM1", "NUM1"},
                        {"NUM2", "NUM2"},
                        {"NUM3", "NUM3"},
                        {"NUM4", "NUM4"},
                        {"NUM5", "NUM5"},
                        {"NUM6", "NUM6"},
                        {"NUM7", "NUM7"},
                        {"NUM8", "NUM8"},
                        {"NUM9", "NUM9"},

                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };
            var TieuDeTrai3 = oBangSoSanhGiaTriTonKhoDTXDQuery.DonViText != "Tất cả" ? oBangSoSanhGiaTriTonKhoDTXDQuery.DonViText : "........";
            var oData = JsonConvert.DeserializeObject<List<BangSoSanhGiaTriTonKhoDTXDJson>>(string1resultUP, settings);
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "TỔNG CÔNG TY",
                TieuDeTrai2 = "ĐIỆN LỰC TP HÀ NỘI",
                TieuDeTrai3 = "Đơn vị : " + TieuDeTrai3,
                Title = "BẢNG SO SÁNH GIÁ TRỊ TỒN KHO ĐTXD ",
                TieuDeGiua1 = "CỘNG HOÀ XÃ HỘI CHỦ NGHĨA VIỆT NAM",
                TieuDeGiua2 = "Độc lập - Tự do - Hạnh phúc",
                Nam = bcNam.ToString(),
                Thang = bcQuyBaoCao.ToString(),

                recordsTotal = oData.Count
            };

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBangSoSanhGiaTriTonKhoDTXDQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBangSoSanhGiaTriTonKhoDTXDQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng : ";
                    if (bcQuyBaoCao != null)
                        Tungaydenngay += bcQuyBaoCao.ToString();
                    Tungaydenngay += " - Năm ";
                    if (bcNam != null)
                        Tungaydenngay += bcNam.ToString();

                    lstValues.Add(new ClsExcel(2, 5, Tungaydenngay, StyleExcell.TextDatetime, false, 12, true, 2, 2, 5, 6));
                    lstValues.Add(new ClsExcel(7, 9, "Tổng số bản ghi:" + oData.Count, StyleExcell.TextRight, false, 12, true, 7, 7, 9, 10));
                    lstValues.Add(new ClsExcel(2, 0, "Đơn vị:" + TieuDeTrai3, StyleExcell.TextCenterBold, false, 12, true, 2, 2, 0, 2));
                    lstValues.Add(new ClsExcel(3, 0, "Số:_____/ BC", StyleExcell.TextCenterBold, false, 12, true, 3, 3, 0, 2));
                    int rowStart = 12;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.DonVi, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.NUM1, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.NUM2, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.NUM3, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.NUM4, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.NUM5, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.NUM6, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.NUM7, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.NUM8, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.NUM9, StyleExcell.TextLeft));
                        rowStart++;
                        STT++;
                    }


                    lstValues.Add(new ClsExcel(++rowStart , 0, "Ghi chú :", StyleExcell.TextLeft, false, 12, true, rowStart , rowStart , 0,3));
                  
                    lstValues.Add(new ClsExcel(++rowStart , 0, "- Kỳ vọng dữ liệu tích hợp từ chương trình ERP là tốt nhất", StyleExcell.TextLeft, false, 12, true, rowStart , rowStart , 0,3));
                    lstValues.Add(new ClsExcel(++rowStart , 0, "- Nếu nhập tay thì vẫn có thể nhầm lần :", StyleExcell.TextLeft, false, 12, true, rowStart , rowStart , 0,3));

                    lstValues.Add(new ClsExcel(++rowStart - 3, 6, "Trưởng đơn vị", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart - 3, 6, 8));
                    lstValues.Add(new ClsExcel(++rowStart - 3, 6, "(Ký tên ,đóng dấu )", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart - 3, 6, 8));
                    lstValues.Add(new ClsExcel(++rowStart - 3, 6, "", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart +1, 6, 8));


                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);



                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "BangSoSanhGiaTriTonKhoDTXD" + DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_BangSoSanhGiaTriTonKhoSXKD()
        {
            BangSoSanhGiaTriTonKhoSXKDQuery oBangSoSanhGiaTriTonKhoSXKDQuery = new BangSoSanhGiaTriTonKhoSXKDQuery(HttpContext.Current.Request);


            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/ERP_BangSoSanhGiaTriTonKho_SXKD/all/09-2021";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBangSoSanhGiaTriTonKhoSXKDQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var bcNam = oBangSoSanhGiaTriTonKhoSXKDQuery.Nam;
            var bcQuyBaoCao = oBangSoSanhGiaTriTonKhoSXKDQuery.Thang;
            var bcDV = oBangSoSanhGiaTriTonKhoSXKDQuery.MaDonVi;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/"+bcDV + "/" + bcQuyBaoCao + "-" + bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapBangSoSanhGiaTriTonKhoSXKDJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(BangSoSanhGiaTriTonKhoSXKDJson),
                    new Dictionary<string, string>
                    {
                        {"DonVi", "LINE_DESCRIPTION"},
                        {"NUM1", "NUM1"},
                        {"NUM2", "NUM2"},
                        {"NUM3", "NUM3"},                      

                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };
            var TieuDeTrai3 = oBangSoSanhGiaTriTonKhoSXKDQuery.DonViText != "Tất cả" ? oBangSoSanhGiaTriTonKhoSXKDQuery.DonViText : "........";
            var oData = JsonConvert.DeserializeObject<List<BangSoSanhGiaTriTonKhoSXKDJson>>(string1resultUP, settings);
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "TỔNG CÔNG TY ",
                TieuDeTrai2 = "ĐIỆN LỰC TP HÀ NỘI",
                Title = "BẢNG SO SÁNH GIÁ TRỊ TỒN KHO SXKD ",
                TieuDeGiua1 = "CỘNG HOÀ XÃ HỘI CHỦ NGHĨA VIỆT NAM",
                TieuDeGiua2 = "Độc lập - Tự do - Hạnh phúc",
                TieuDeGiua3 = "Đơn vị : "+ TieuDeTrai3,
                Nam = bcNam.ToString(),
                Thang = bcQuyBaoCao.ToString(),

                recordsTotal = oData.Count
            };

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBangSoSanhGiaTriTonKhoSXKDQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBangSoSanhGiaTriTonKhoSXKDQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng : ";
                    if (bcQuyBaoCao != null)
                        Tungaydenngay += bcQuyBaoCao.ToString();
                    Tungaydenngay += " - Năm ";
                    if (bcNam != null)
                        Tungaydenngay += bcNam.ToString();

                    lstValues.Add(new ClsExcel(5, 2, Tungaydenngay, StyleExcell.TextDatetime, false, 12, true, 5, 5, 2, 3));
                    lstValues.Add(new ClsExcel(6, 4, "Tổng số bản ghi:" + oData.Count, StyleExcell.TextRight, false, 12, false, 7, 7, 9, 10));
                    //lstValues.Add(new ClsExcel(2, 0, "Đơn vị:" + TieuDeTrai3, StyleExcell.TextCenterBold, false, 12, true, 2, 2, 0, 2));
                    //lstValues.Add(new ClsExcel(3, 0, "Số:_____/ BC", StyleExcell.TextCenterBold, false, 12, true, 3, 3, 0, 2));
                    int rowStart = 10;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.DonVi, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.NUM1, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.NUM2, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.NUM3, StyleExcell.TextLeft));
                       
                        rowStart++;
                        STT++;
                    }

                    lstValues.Add(new ClsExcel(++rowStart, 0, "Ghi chú :", StyleExcell.TextLeft, false, 12, true, rowStart, rowStart, 0, 2));

                    lstValues.Add(new ClsExcel(++rowStart, 0, "- Kỳ vọng dữ liệu tích hợp từ chương trình ERP là tốt nhất", StyleExcell.TextLeft, false, 12, true, rowStart, rowStart, 0, 2));
                    lstValues.Add(new ClsExcel(++rowStart, 0, "- Nếu nhập tay thì vẫn có thể nhầm lần :", StyleExcell.TextLeft, false, 12, true, rowStart, rowStart, 0, 2));
                    lstValues.Add(new ClsExcel(++rowStart -3, 3, "Trưởng đơn vị", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart - 3, 3, 4));
                    lstValues.Add(new ClsExcel(++rowStart - 3, 3, "(Ký tên ,đóng dấu )", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart -3, 3, 4));
                    lstValues.Add(new ClsExcel(++rowStart - 3, 4, "", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart +1, 3, 4));

                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);



                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "BangSoSanhGiaTriTonKhoSXKD" + DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_ERP_BCTuoiThoHangTonKho_DTXD()
        {
            ERP_BCTuoiThoHangTonKho_DTXDQuery oBangSoSanhGiaTriTonKhoSXKDQuery = new ERP_BCTuoiThoHangTonKho_DTXDQuery(HttpContext.Current.Request);


            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/ERP_BCTuoiThoHangTonKho_DTXD/all/09-2021";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+ oBangSoSanhGiaTriTonKhoSXKDQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var bcNam = oBangSoSanhGiaTriTonKhoSXKDQuery.Nam;
            var bcThang = oBangSoSanhGiaTriTonKhoSXKDQuery.Thang;
            var bcDV = oBangSoSanhGiaTriTonKhoSXKDQuery.MaDonVi;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/"+ bcDV + "/" + bcThang + "-" + bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapERP_BCTuoiThoHangTonKho_DTXDJson>(responseBody);
            var string1resultUP = model.data.Replace("\\", "");

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(ERP_BCTuoiThoHangTonKho_DTXDJson),
                    new Dictionary<string, string>
                    {
                        {"LINE_DESCRIPTION", "LINE_DESCRIPTION"},
                        {"TONGTIEN", "TONGTIEN"},
                        {"NUM1", "NUM1"},
                        {"NUM2", "NUM2"},                      
                        {"NUM3", "NUM3"},                      
                        {"NUM4", "NUM4"},                      
                        {"NUM5", "NUM5"},                      
                        {"NUM6", "NUM6"},                      
                        {"NUM7", "NUM7"},                      
                        {"NUM8", "NUM8"},                      
                        {"NUM9", "NUM9"},                      
                        {"NUM10", "NUM10"},                      
                        {"NUM11", "NUM11"},                      
                        {"NUM12", "NUM12"},                      
                        {"NUM13", "NUM13"},                      
                        {"NUM14", "NUM14"},                      

                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<ERP_BCTuoiThoHangTonKho_DTXDJson>>(string1resultUP, settings);
            string donvi = "";
            if (bcDV == "all")
            {
                donvi += "";
            }
            else if (bcDV == "PD")
            {
                donvi += "Tổng Công ty Điện lực TP Hà Nội";
            }
            else if (bcDV == "PD0600")
            {
                donvi += "Công ty Điện lưc Thanh Trì";
            }
            else if (bcDV == "PD1400")
            {
                donvi += "Công ty Điện lực Long Biên";
            }
            else if (bcDV == "P")
            {
                donvi += "Tập đoàn Điện Lực Việt Nam";
            }
            else if (bcDV == "PD0700")
            {
                donvi += "Công ty Điện lực Gia Lâm";
            }
            else if (bcDV == "PD0800")
            {
                donvi += "Công ty Điện lực Đông Anh";
            }
            else if (bcDV == "PD0900")
            {
                donvi += "Công ty Điện lực Sóc Sơn";
            }
            else if (bcDV == "PD1800")
            {
                donvi += "Công ty Điện lực Chương Mỹ";
            }
            else if (bcDV == "PD2000")
            {
                donvi += "Công ty Điện lực Thường Tín";
            }
            else if (bcDV == "PD2400")
            {
                donvi += "Công ty Điện lực Mỹ Đức";
            }
            else if (bcDV == "PD2600")
            {
                donvi += "Công ty Điện lực Phúc Thọ";
            }
            else if (bcDV == "PD2500")
            {
                donvi += "Công ty Điện lực Phú Xuyên";
            }
            else if (bcDV == "PD2800")
            {
                donvi += "Công ty Điện lực Thanh Oai";
            }
            else if (bcDV == "PD2900")
            {
                donvi += "Công ty Điện lực Ứng Hòa";
            }
            else if (bcDV == "PD0100")
            {
                donvi += "Công ty Điện lực Hoàn Kiếm";
            }
            else if (bcDV == "PD1500")
            {
                donvi += "Công ty Điện lực Mê Linh";
            }
            else if (bcDV == "PD1700")
            {
                donvi += "Công ty Điện lực Sơn Tây ";
            }
            else if (bcDV == "PD1900")
            {
                donvi += "Công ty Điện lực Thạch Thất";
            }
            else if (bcDV == "PD2100")
            {
                donvi += "Công ty Điện lực Ba Vì";
            }
            else if (bcDV == "PD2200")
            {
                donvi += "Công ty Điện lực Đan Phượng";
            }
            else if (bcDV == "PD2300")
            {
                donvi += "Công ty Điện lực Hoài Đức";
            }
            else if (bcDV == "PD2700")
            {
                donvi += "Công ty Điện lực Quốc Oai ";
            }
            else if (bcDV == "PD0400")
            {
                donvi += "Công ty Điện lực Đống Đa ";
            }
            else if (bcDV == "PD0500")
            {
                donvi += "Công ty Điện lực Nam Từ Liêm ";
            }
            else if (bcDV == "PD0200")
            {
                donvi += "Công ty Điện lực Hai Bà Trưng ";
            }
            else if (bcDV == "PD1000")
            {
                donvi += "Công ty Điện lực Tây Hồ  ";
            }
            else if (bcDV == "PD1100")
            {
                donvi += "Công ty Điện lực Thanh Xuân ";
            }
            else if (bcDV == "PD1200")
            {
                donvi += "Công ty Điện lực Cầy giấy ";
            }
            else if (bcDV == "PD3000")
            {
                donvi += "Công ty Điện lực Bắc Từ Liêm ";
            }
            else if (bcDV == "PD1300")
            {
                donvi += "Công ty Điện lực Hoàng Mai ";
            }
            else if (bcDV == "PD0300")
            {
                donvi += "Công ty Điện lực Ba Đình ";
            }
            else if (bcDV == "PD1600")
            {
                donvi += "Công ty Điện lực Hà Đông ";
            }
            else
            {
                donvi += "Tổng Công ty Điện lực TP Hà Nội ";
            };
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "TỔNG CÔNG TY ",
                TieuDeTrai2 = "Đơn vị: "+donvi,
                Title = "BÁO CÁO TUỔI THỌ HÀNG TỒN KHO ĐTXD",
                TieuDeGiua1 = "CỘNG HOÀ XÃ HỘI CHỦ NGHĨA VIỆT NAM",
                TieuDeGiua2 = "Độc lập - Tự do - Hạnh phúc",
                Nam = bcNam.ToString(),
                Thang = bcThang.ToString(),
                recordsTotal = oData.Count
            };

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBangSoSanhGiaTriTonKhoSXKDQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBangSoSanhGiaTriTonKhoSXKDQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng : ";
                    if (bcThang != null)
                        Tungaydenngay += bcThang.ToString();
                    Tungaydenngay += " - Năm ";
                    if (bcNam != null)
                        Tungaydenngay += bcNam.ToString();

                    lstValues.Add(new ClsExcel(5, 0, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 5, 5));
                    lstValues.Add(new ClsExcel(2, 1, "Đơn vị :"+ donvi, StyleExcell.TextCenterBold, false, 12, false, 5, 5));
                    lstValues.Add(new ClsExcel(6, 15, "Tổng số bản ghi:" + oData.Count, StyleExcell.TextCenter, false, 12, true, 6, 6, 15, 16));
                    int rowStart = 11;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.LINE_DESCRIPTION, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.TONGTIEN, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.NUM1, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.NUM2, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.NUM3, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.NUM4, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.NUM5, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.NUM6, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.NUM7, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.NUM8, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.NUM9, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 12, item.NUM10, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 13, item.NUM11, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 14, item.NUM12, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 15, item.NUM13, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 16, item.NUM14, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 17, "", StyleExcell.TextLeft));
                       
                        rowStart++;
                        STT++;
                    }
                    lstValues.Add(new ClsExcel(++rowStart, 0, "Ghi chú :", StyleExcell.TextLeft, false, 12, true, rowStart, rowStart, 0, 4));

                    lstValues.Add(new ClsExcel(++rowStart, 0, "- Kỳ vọng dữ liệu tích hợp từ chương trình ERP là tốt nhất", StyleExcell.TextLeft, false, 12, true, rowStart, rowStart, 0, 4));
                    lstValues.Add(new ClsExcel(++rowStart, 0, "- Nếu nhập tay thì vẫn có thể nhầm lần :", StyleExcell.TextLeft, false, 12, true, rowStart, rowStart, 0, 4));
                    lstValues.Add(new ClsExcel(++rowStart - 3, 12, "Trưởng đơn vị", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart - 3, 12, 16));
                    lstValues.Add(new ClsExcel(++rowStart - 3, 12, "(Ký tên ,đóng dấu )", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart - 3, 12, 16));
                    lstValues.Add(new ClsExcel(++rowStart - 3, 12, "", StyleExcell.TextCenterBold, false, 12, true, rowStart - 3, rowStart + 1, 12, 16));

                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);



                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "ERP_BCTuoiThoHangTonKho_DTXD_" + DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }  
    }
}
