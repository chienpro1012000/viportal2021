﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using ViPortal_Utils.Base;
using VIPortalAPI.BaseAPI;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.Lconfig;

namespace VIPortalAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    public class LUserController : SIBaseAPI
    {
        public static DateTime firstDayv1 { get; set; }
        public static int intCurYear = System.DateTime.Today.Year;

        public static DateTime firstDay = System.DateTime.Today;
        public static int intCurWeek = 1;
        public static string strYear = "";
        public static string strlastyear = "";
        public static string strnextyear = "";

        public static string strWeek = "";
        public static string strlastweek = "";
        public static string strnextweek = "";



        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public ResultAction ResetCookie(string Url)
        {
            ResultAction oResultAction = new ResultAction();
            if (!string.IsNullOrEmpty(Url))
            {
                LUserDA lUserDA = new LUserDA(true);
                string site = Url.Split('/')[1];
                if (site.Equals("noidung") || site.Equals("cms") || site.Equals("Pages") || site.Equals(""))
                    site = "";
                else
                    site = "/" + site;
                oResultAction.Message = site;
                lUserDA.SystemUpdateOneField(CurentUser.ID, "CurSiteAction", site);
            }
            return oResultAction;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ResultAction DONGBOANH()
        {
            int ItemID; long staffCode;
            ItemID = Convert.ToInt32(HttpContext.Current.Request["ItemID"]);
            //int GroupID = Convert.ToInt32(HttpContext.Current.Request["GroupID"]);
            staffCode = Convert.ToInt64(HttpContext.Current.Request["staffCode"]);
            ResultAction oResultAction = new ResultAction();
            HttpClient client = new HttpClient();
            LUserDA lUserDA = new LUserDA();
            //lấy thông tin group của nguwfio dùng ra xem nào đã.

            HttpResponseMessage responseUser = client.PostAsJsonAsync("http://10.9.125.97:7072/StaffView/getListAnh", new
            {
                nsId = staffCode.ToString()
            }).Result;
            string responseUserImage = responseUser.Content.ReadAsStringAsync().Result;
            string fileServer = "";
            string pathFile = "";
            try
            {
                JObject oResponseUser = JObject.Parse(responseUserImage);
                string status = oResponseUser["status"].Value<string>();
                if (status == "SUCCESS")
                {
                    JArray jsonArray = (JArray)oResponseUser["data"];
                    List<AvatarUser> lstAvatarUser = jsonArray.ToObject<List<AvatarUser>>();
                    if (lstAvatarUser.Count > 0)
                    {
                        string DataAvart = lstAvatarUser[0].anhNs;
                        if (!string.IsNullOrEmpty(DataAvart))
                        {
                            string TypeFile = clsFucUtils.GetFileExtension(DataAvart);
                            fileServer = staffCode.ToString() + "_" + DateTime.Now.Millisecond + "." + TypeFile;

                            string pathServer = string.Format(@"\{0}\{1}", @"Uploads\ajaxUpload", fileServer);
                            pathFile = string.Format(@"{0}{1}", HttpContext.Current.Server.MapPath(@"\"), pathServer);
                            //write file ra thư mục upload.
                            byte[] bytes = System.Convert.FromBase64String(DataAvart);
                            File.WriteAllBytes(pathFile, bytes);
                            //lưu file vào chỗ nào đó để lấy về cái này.

                            //  Get the object used to communicate with the server.
                            //            FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://www.contoso.com/test.htm");
                            //FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://10.9.125.85:21/images/Avatar/" + fileServer);
                            //request.Method = WebRequestMethods.Ftp.UploadFile;

                            //// This example assumes the FTP site uses anonymous logon.
                            //request.Credentials = new NetworkCredential("anonymous", "password");

                            //// Copy the contents of the file to the request stream.
                            //using (FileStream fileStream = File.Open(pathFile, FileMode.Open, FileAccess.Read))
                            //{
                            //    using (Stream requestStream = request.GetRequestStream())
                            //    {
                            //        fileStream.CopyTo(requestStream);
                            //        using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
                            //        {
                            //            oResultAction.Message += ($"Upload File Complete, status {response.StatusDescription}");
                            //            lUserDA.SystemUpdateOneField(ItemID, "AnhDaiDien", "/ckfinder/userfiles/images/Avatar/" + fileServer);
                            //        }
                            //    }
                            //}
                            try
                            {
                                using (var clientftp = new WebClient())
                                {
                                    clientftp.Credentials = new NetworkCredential("anonymous", "password");
                                    //ftp://xxx.xxx.xx.xx:21//path/filename
                                    clientftp.UploadFile("ftp://10.9.125.85:21/images/Avatar/" + fileServer, WebRequestMethods.Ftp.UploadFile, pathFile);
                                    //update user vào trường file này.
                                    lUserDA.SystemUpdateOneField(ItemID, "AnhDaiDien", "/ckfinder/userfiles/images/Avatar/" + fileServer);
                                }
                            }
                            catch (WebException e)
                            {
                                String statusUpload = ((FtpWebResponse)e.Response).StatusDescription;
                                oResultAction.Message += statusUpload;
                            }
                            //item.ImageAvatar = pathServer;
                        }
                    }
                }
                oResultAction.Message += "/ckfinder/userfiles/images/Avatar/" + fileServer;
            }
            catch (Exception ex)
            {
                oResultAction = new ResultAction()
                {
                    State = ActionState.Error,
                    Message = ex.StackTrace + "." + ex.Message + "\n\n" + pathFile
                };
            }

            return oResultAction;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA()
        {
            LUserDA oLUserDA = new LUserDA();


            LUserQuery oLUserQuery = new LUserQuery(HttpContext.Current.Request);
            oLUserQuery.SapDen_SoNgayBaoSinhNhat = DateTime.Now;
            if (!string.IsNullOrEmpty(UrlSite))
            {
                oLUserQuery.Groups = CurentUser.Groups.ID; //search trong đơn vị đó.
            }
            var oData = oLUserDA.GetListJson(oLUserQuery);
            DataGridRender oGrid = new DataGridRender(oData, oLUserQuery.Draw,
                oLUserDA.TongSoBanGhiSauKhiQuery);
            return oGrid;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA_NOW()
        {
            LUserDA oLUserDA = new LUserDA();
            LUserQuery oLUserQuery = new LUserQuery(HttpContext.Current.Request);
            oLUserQuery.DangDienRa_SoNgayBaoSinhNhat = DateTime.Now;
            //oLUserQuery.FieldOrder = "CountSinhNhat";
            //oLUserQuery.Ascending = true;
            var oData = oLUserDA.GetListJson(oLUserQuery);
            DataGridRender oGrid = new DataGridRender(oData, oLUserQuery.Draw,
                oLUserDA.TongSoBanGhiSauKhiQuery);
            return oGrid;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ResultAction DeleteConfig()
        {
            ResultAction oResultAction = new ResultAction();
            LconfigDA lconfigDA = new LconfigDA();
            List<LconfigJson> lstLConfig = lconfigDA.GetListJson(new LconfigQuery() { ConfigGroup = $"Theme_{CurentUser.ID}" });
            foreach (LconfigJson item in lstLConfig)
            {
                lconfigDA.DeleteObject(item.ID);
            }
            oResultAction = new ResultAction()
            {
                Message = "Cập nhật thành công"
            };
            return oResultAction;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction UpdateConfig()
        {
            ResultAction oResultAction = new ResultAction();
            LconfigDA lconfigDA = new LconfigDA();
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["Bg_Body"]))
            {

                oResultAction.Message = HttpContext.Current.Request["Bg_Body"];

                int IDOld = lconfigDA.CheckExit(0, $"Theme_{CurentUser.ID}_Bg_Body");
                lconfigDA.UpdateObject<LconfigItem>(new LconfigItem()
                {
                    ID = IDOld,
                    ConfigValue = oResultAction.Message,
                    ConfigGroup = $"Theme_{CurentUser.ID}",
                    ConfigType = "Bg_Body",
                    Title = $"Theme_{CurentUser.ID}_Bg_Body",

                });

            }
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["Bg_Webpart"]))
            {
                oResultAction.Message = HttpContext.Current.Request["Bg_Webpart"];
                int IDOld = lconfigDA.CheckExit(0, $"Theme_{CurentUser.ID}_Bg_Webpart");
                lconfigDA.UpdateObject<LconfigItem>(new LconfigItem()
                {
                    ID = IDOld,
                    ConfigValue = oResultAction.Message,
                    ConfigGroup = $"Theme_{CurentUser.ID}",
                    ConfigType = "Bg_Webpart",
                    Title = $"Theme_{CurentUser.ID}_Bg_Webpart",
                });
            }

            if (!string.IsNullOrEmpty(HttpContext.Current.Request["font_Body"]))
            {
                oResultAction.Message = HttpContext.Current.Request["font_Body"];
                int IDOld = lconfigDA.CheckExit(0, $"Theme_{CurentUser.ID}_font_Body");
                lconfigDA.UpdateObject<LconfigItem>(new LconfigItem()
                {
                    ID = IDOld,
                    ConfigValue = oResultAction.Message,
                    ConfigGroup = $"Theme_{CurentUser.ID}",
                    ConfigType = "font_Body",
                    Title = $"Theme_{CurentUser.ID}_font_Body",
                });
            }

            return oResultAction;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataGridRender GETDATA_SINHNHAT()
        {
            BirthdayQuery oLUserQuery = new BirthdayQuery(HttpContext.Current.Request);
            int length;
            if (Convert.ToInt32(HttpContext.Current.Request["length"]) > 0)
                length = Convert.ToInt32(HttpContext.Current.Request["length"]);
            else
                length = 10;
            int SoNgayLay;
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["SoNgayLay"]))
                SoNgayLay = Convert.ToInt32(HttpContext.Current.Request["SoNgayLay"]);
            else
                SoNgayLay = 30;
            LconfigDA oLconfigDA = new LconfigDA();
            string APIEVNHanoiDanhBa = oLconfigDA.GetListJson(new LconfigQuery() { ConfigType = "APIEVNHanoiDanhBa" }).FirstOrDefault()?.ConfigValue;
            if (string.IsNullOrEmpty(APIEVNHanoiDanhBa))
                APIEVNHanoiDanhBa = "https://portal.evnhanoi.vn/apigateway";

            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes("EVNHANOI:Evnhanoi@123");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiDanhBa + "/HRMS_ChucMungSinhNhat_LanhDao").Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            SinhNhatLanhDaoResponse oSinhNhatLanhDaoResponse = JsonConvert.DeserializeObject<SinhNhatLanhDaoResponse>(responseBody);
            //get ra các người dùng có sinh nhật trong khoảng 1 tuần tới.
            List<SinhNhatLanhDao> lstSinhNhatLanhDao = oSinhNhatLanhDaoResponse.data.Where(x => (x.BIRTHDAY > DateTime.Now.Date.AddDays(-1)) && (x.BIRTHDAY < DateTime.Now.Date.AddDays(SoNgayLay))).Skip(0).ToList();
            lstSinhNhatLanhDao = lstSinhNhatLanhDao.OrderBy(x => x.BIRTHDAY).ThenBy(y => y.STT).ToList();
            HttpClient clientAnh = new HttpClient();
            string responseUserImage = "";
            if (HttpContext.Current.Request.Url.Host.Contains("evnhanoi.vn"))
            {
                foreach (var item in lstSinhNhatLanhDao)
                {
                    //get ảnh xem sao.
                    try
                    {

                        HttpResponseMessage responseUser = client.PostAsJsonAsync("http://10.9.125.97:7072/StaffView/getListAnh", new
                        {
                            nsId = item.ID_LLNS.ToString()
                        }).Result;
                        responseUserImage = responseUser.Content.ReadAsStringAsync().Result;
                        JObject oResponseUser = JObject.Parse(responseUserImage);
                        string status = oResponseUser["status"].Value<string>();
                        if (status == "SUCCESS")
                        {
                            //List<AvatarUser> lstAvatarUser = oResponseUser["data"].Value<List<AvatarUser>>();
                            JArray jsonArray = (JArray)oResponseUser["data"];
                            List<AvatarUser> lstAvatarUser = jsonArray.ToObject<List<AvatarUser>>();
                            if (lstAvatarUser.Count > 0)
                            {
                                string DataAvart = lstAvatarUser[0].anhNs;
                                if (!string.IsNullOrEmpty(DataAvart))
                                {
                                    string TypeFile = clsFucUtils.GetFileExtension(DataAvart);
                                    string fileServer = item.ID_LLNS.ToString() + "." + TypeFile;

                                    string pathServer = string.Format(@"\{0}\{1}", @"Uploads\ajaxUpload\", fileServer);
                                    string pathFile = string.Format(@"{0}\{1}", HttpContext.Current.Server.MapPath(@"\"), pathServer);
                                    //write file ra thư mục upload.
                                    byte[] bytes = System.Convert.FromBase64String(DataAvart);
                                    File.WriteAllBytes(pathFile, bytes);
                                    item.ImageAvatar = pathServer;
                                }
                            }
                        }
                        //http://10.9.125.97:7072/StaffView/getListAnh
                        //{
                        //"nsId":"277000000000164"
                        //}

                    }
                    catch (Exception ex)
                    {
                        throw new System.Exception(ex.Message + "\r\n" + responseUserImage);
                    }
                }
            }

            //List<DanhBaDongBo> lstBirthday = new List<DanhBaDongBo>();


            //BirthdayDA oBirthdayDA = new BirthdayDA();
            //BirthdayQuery oLUserQuery = new BirthdayQuery(HttpContext.Current.Request);
            //oLUserQuery.isBirthday = true;
            //oLUserQuery.FieldOrder = "BIRTHDAY";
            //oLUserQuery.Ascending = true;
            //DateTime ngaydautuan = LayNgayDauVaKhoiTaoGiaTriTuan_Nam();
            //oLUserQuery.TuNgay = ngaydautuan;
            //oLUserQuery.DenNgay = ngaydautuan.AddDays(6);
            //var oData = oBirthdayDA.GetListJson(oLUserQuery);

            //get sinh nhật lđ.


            DataGridRender oGrid = new DataGridRender(lstSinhNhatLanhDao, oLUserQuery.Draw, lstSinhNhatLanhDao.Count);
            return oGrid;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataGridRender GETDATA_SINHNHAT_CBNV()
        {
            //DanhBaDA oDanhBaDA = new DanhBaDA();
            //DanhBaQuery oDanhBaQuery = new DanhBaQuery(HttpContext.Current.Request);
            //DateTime ngaydautuan = LayNgayDauVaKhoiTaoGiaTriTuan_Nam();
            //oDanhBaQuery.TuNgay_SinhNhat = ngaydautuan;
            //oDanhBaQuery.DenNgay_SinhNhat = ngaydautuan.AddDays(6);
            //var oData = oDanhBaDA.GetListJson(oDanhBaQuery);

            //List<DanhBaDongBo> lstBirthday = root.data.Where(x => (x.ngaysinh.Day - x.ngaysinh.Month) >= (DateTime.Now.Day - DateTime.Now.Month)).OrderBy(x => x.ngaysinh.Month).Skip(0).Take(100).ToList();
            //DataGridRender oGrid = new DataGridRender(oData, oDanhBaQuery.Draw, oDanhBaDA.TongSoBanGhiSauKhiQuery);
            LconfigDA oLconfigDA = new LconfigDA();
            string APIEVNHanoiDanhBa = oLconfigDA.GetListJson(new LconfigQuery() { ConfigType = "APIEVNHanoiDanhBa" }).FirstOrDefault()?.ConfigValue;
            if (string.IsNullOrEmpty(APIEVNHanoiDanhBa))
                APIEVNHanoiDanhBa = "https://portal.evnhanoi.vn/apigateway";
            //IP nội bộ
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes("EVNHANOI:Evnhanoi@123");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiDanhBa + "/HRMS_ChucMungSinhNhat_LanhDao").Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            SinhNhatLanhDaoResponse oSinhNhatLanhDaoResponse = JsonConvert.DeserializeObject<SinhNhatLanhDaoResponse>(responseBody);


            //string APIEVNHanoiNew = $"http://10.9.125.85:8080/HRMS_DanhBa_NhanVien/0/0";




            #region lấy thông tin request
            string id_donvi;
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["ID_DONVI"]))
                id_donvi = HttpContext.Current.Request["ID_DONVI"];
            else
                id_donvi = "";

            string PhongBan;
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["PhongBan"]))
                PhongBan = HttpContext.Current.Request["PhongBan"];
            else
                PhongBan = "";


            int length;
            if (Convert.ToInt32(HttpContext.Current.Request["length"]) > 0)
                length = Convert.ToInt32(HttpContext.Current.Request["length"]);
            else
                length = 30;
            #endregion




            HttpResponseMessage responseALL = client.PostAsJsonAsync(APIEVNHanoiDanhBa + "/HRMS_DanhBa_NhanVien", new
            {
                MaDonVi = id_donvi,
                PhongBan = PhongBan,
                TuKhoa = ""
            }).Result;

            response.EnsureSuccessStatusCode();
            string responseBodyALL = responseALL.Content.ReadAsStringAsync().Result;
            var root = JsonConvert.DeserializeObject<DanhBaRespon>(responseBodyALL);
            List<DanhBaDongBo> lstBirthday = new List<DanhBaDongBo>();
            //SoNgayLay
            int SoNgayLay;
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["SoNgayLay"]))
                SoNgayLay = Convert.ToInt32(HttpContext.Current.Request["SoNgayLay"]);
            else
                SoNgayLay = 30;
            lstBirthday = root.data.Where(x => x.SinhNhatNamNay >= DateTime.Now.Date && x.SinhNhatNamNay < DateTime.Now.Date.AddDays(SoNgayLay)).ToList();
            lstBirthday = lstBirthday.Where(x => oSinhNhatLanhDaoResponse.data.Count(y => y.ID_LLNS == x.NS_ID) == 0).ToList();
            lstBirthday = lstBirthday.OrderBy(x => x.SinhNhatNamNay).ThenBy(x => x.STT).ToList();
            //lstBirthday = lstBirthday.Where(x => x.ngaysinh.Day >= DateTime.Now.Day).Skip(0).Take(length).ToList();
            if (HttpContext.Current.Request.Url.Host.Contains("evnhanoi.vn"))
            {
                foreach (var item in lstBirthday)
                {


                    //get ảnh xem sao.
                    try
                    {

                        HttpResponseMessage responseUser = client.PostAsJsonAsync("http://10.9.125.97:7072/StaffView/getListAnh", new
                        {
                            nsId = item.NS_ID.ToString()
                        }).Result;
                        string responseUserImage = responseUser.Content.ReadAsStringAsync().Result;
                        JObject oResponseUser = JObject.Parse(responseUserImage);
                        string status = oResponseUser["status"].Value<string>();
                        if (status == "SUCCESS")
                        {
                            //List<AvatarUser> lstAvatarUser = oResponseUser["data"].Value<List<AvatarUser>>();
                            JArray jsonArray = (JArray)oResponseUser["data"];
                            List<AvatarUser> lstAvatarUser = jsonArray.ToObject<List<AvatarUser>>();
                            if (lstAvatarUser.Count > 0)
                            {
                                string DataAvart = lstAvatarUser[0].anhNs;
                                if (!string.IsNullOrEmpty(DataAvart))
                                {
                                    string TypeFile = clsFucUtils.GetFileExtension(DataAvart);
                                    string fileServer = item.NS_ID.ToString() + "." + TypeFile;

                                    string pathServer = string.Format(@"\{0}\{1}", @"Uploads\ajaxUpload\", fileServer);
                                    string pathFile = string.Format(@"{0}\{1}", HttpContext.Current.Server.MapPath(@"\"), pathServer);
                                    //write file ra thư mục upload.
                                    byte[] bytes = System.Convert.FromBase64String(DataAvart);
                                    File.WriteAllBytes(pathFile, bytes);
                                    item.ImageAvatar = pathServer;
                                }
                            }
                        }
                        //http://10.9.125.97:7072/StaffView/getListAnh
                        //{
                        //"nsId":"277000000000164"
                        //}

                    }
                    catch (Exception ex)
                    {
                        //throw new System.Exception(ex.Message + "\r\n" + responseUserImage);
                    }
                }
            }
            DataGridRender oGrid = new DataGridRender(lstBirthday, 2, lstBirthday.Count);
            return oGrid;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nam"></param>
        /// <returns></returns>
        public int ThuCuaNgayDauNam(int nam)
        {
            // khoi tao ngay dau tien cua nam
            DateTime startYear = new DateTime(nam, 1, 1);
            int startdayofyear = 0;

            if (startYear.DayOfWeek == DayOfWeek.Monday) startdayofyear = 0; // thu 2
            else if (startYear.DayOfWeek == DayOfWeek.Tuesday) startdayofyear = 1; // thu 3
            else if (startYear.DayOfWeek == DayOfWeek.Wednesday) startdayofyear = 2;
            else if (startYear.DayOfWeek == DayOfWeek.Thursday) startdayofyear = 3;
            else if (startYear.DayOfWeek == DayOfWeek.Friday) startdayofyear = 4;
            else if (startYear.DayOfWeek == DayOfWeek.Saturday) startdayofyear = 5;
            else startdayofyear = 6;
            return startdayofyear;
        }
        /// <summary>
        /// Lay so tuan trong 1 nam
        /// </summary>
        /// <param name="nam">Nam can lay</param>
        /// <returns>So tuan, tinh tu tuan 1</returns>
        public int LaySoTuanTrongNam(int nam)
        {
            // kiem tra ngay dau tien cua nam vao thu nao
            int startdayofyear = ThuCuaNgayDauNam(nam);

            DateTime endYear = new DateTime(nam, 12, 31);
            // khai bao ngay dau tuan dau tien cua nam
            int SoNgayTrongNam = endYear.DayOfYear;

            // Lay so tuan trong 1 nam
            int SoTuanTrongNam = 0;
            SoTuanTrongNam = (SoNgayTrongNam + startdayofyear) / 7;
            int SoDu = (SoNgayTrongNam + startdayofyear) % 7;
            if (SoDu > 0) SoTuanTrongNam = SoTuanTrongNam + 1;

            return SoTuanTrongNam;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="CurWeek"></param>
        /// <param name="CurYear"></param>
        /// <returns></returns>
        public DateTime NgayDauTuan(int CurWeek, int CurYear)
        {
            //Khai bao, khoi tao gia tri ngay dau tuan
            DateTime firstDate = new DateTime(CurYear, 1, 1);
            // khoi tao ngay dau tien cua nam
            DateTime startYear = new DateTime(CurYear, 1, 1);
            // khoi tao ngay cuoi cung cua nam
            DateTime endYear = new DateTime(CurYear, 12, 31);
            // khai bao ngay dau tuan dau tien cua nam
            DateTime NgayDauTuanThu1 = new DateTime(CurYear, 1, 1);
            // So ngay trong nam
            int SoNgayTrongNam = endYear.DayOfYear;

            // kiem tra ngay dau tien cua nam vao thu nao
            int startdayofyear = ThuCuaNgayDauNam(CurYear);


            // Lay so tuan trong 1 nam
            int SoTuanTrongNam = LaySoTuanTrongNam(CurYear);

            // kiem tra so tuan truyen vao co hop le hay khong
            // neu khong hop le se lay tuan hien thoi
            if (CurWeek < 1 || CurWeek > SoTuanTrongNam)
            {
                if (CurYear != DateTime.Today.Year) CurWeek = 1;
                else
                {
                    // lay tuan hien thoi
                    DateTime today = System.DateTime.Today;

                    int NgayHienthoi = today.DayOfYear + startdayofyear;
                    CurWeek = NgayHienthoi / 7;
                    if (NgayHienthoi % 7 > 0) CurWeek = CurWeek + 1;
                }
            }

            // lay ngay dau tien cua tuan dau tien
            NgayDauTuanThu1 = startYear.AddDays(startdayofyear * -1);

            int NgayDauTuan = (CurWeek - 1) * 7;
            // Lay ngay dau tien cua tuan can lay
            firstDate = NgayDauTuanThu1.AddDays(NgayDauTuan);

            // tra ve ngay dau tien cua tuan can lay
            return firstDate;
        }
        /// <summary>
        /// LayNgayDauVaKhoiTaoGiaTriTuan_Nam
        /// </summary>

        public DateTime LayNgayDauVaKhoiTaoGiaTriTuan_Nam()
        {
            // lay nam
            try
            {
                if (HttpContext.Current.Request["p_year"] != null) strYear = HttpContext.Current.Request["p_year"].ToString().Trim();
                intCurYear = int.Parse(strYear);
                if (intCurYear > 9999 || intCurYear < 1900)
                {
                    intCurYear = System.DateTime.Today.Year;
                    strYear = intCurYear.ToString();
                }
            }
            catch
            {
                intCurYear = System.DateTime.Today.Year;
                strYear = intCurYear.ToString();
            }

            int SoTuanTrongNam = LaySoTuanTrongNam(intCurYear);

            // lay tuan
            try
            {
                if (!string.IsNullOrEmpty(HttpContext.Current.Request["p_week"]))
                    strWeek = HttpContext.Current.Request["p_week"].ToString().Trim();
                //if (!string.IsNullOrEmpty(next) && next == "next")
                //    intCurWeek = Convert.ToInt32(strWeek) + 1;
                //else if (!string.IsNullOrEmpty(last) && last == "last")
                //    intCurWeek = Convert.ToInt32(strWeek) - 1;
                //else
                intCurWeek = Convert.ToInt32(strWeek);
                //intCurWeek = int.Parse(strWeek);

                int startdayofyear = ThuCuaNgayDauNam(intCurYear);
                if (intCurWeek < 1 || intCurWeek > SoTuanTrongNam)
                {
                    if (intCurYear != DateTime.Today.Year) intCurWeek = 1;
                    else
                    {
                        // lay tuan hien thoi
                        DateTime today = System.DateTime.Today;

                        int NgayHienthoi = today.DayOfYear + startdayofyear;
                        intCurWeek = NgayHienthoi / 7;
                        if (NgayHienthoi % 7 > 0) intCurWeek = intCurWeek + 1;
                    }
                }
                strWeek = intCurWeek.ToString();

            }
            catch
            {

                int startdayofyear = ThuCuaNgayDauNam(intCurYear);

                if (intCurYear != DateTime.Today.Year) intCurWeek = 1;
                else
                {
                    // lay tuan hien thoi
                    DateTime today = System.DateTime.Today;

                    int NgayHienthoi = today.DayOfYear + startdayofyear;
                    intCurWeek = NgayHienthoi / 7;
                    if (NgayHienthoi % 7 > 0) intCurWeek = intCurWeek + 1;
                }
                strWeek = intCurWeek.ToString();
            }

            // lay gia tri cua ngay dau tuan

            firstDay = NgayDauTuan(intCurWeek, intCurYear);

            //lblNgayChon.Text = "Từ ngày: " + string.Format("{0:dd/MM/yyyy}", firstDay) + " - Đến ngày: " + string.Format("{0:dd/MM/yyyy}", firstDay.AddDays(6));

            if (intCurWeek == SoTuanTrongNam)
            {
                strnextweek = "1";
                int nam = intCurYear + 1;
                strnextyear = nam.ToString();

                int tuan = intCurWeek - 1;
                strlastweek = tuan.ToString();
                strlastyear = strYear;

            }
            else if (intCurWeek == 1)
            {


                int tuan = intCurWeek + 1;
                strnextweek = tuan.ToString();
                strnextyear = strYear;


                int nam = intCurYear - 1;
                strlastyear = nam.ToString();
                tuan = LaySoTuanTrongNam(nam);
                strlastweek = tuan.ToString();

            }
            else
            {

                strnextyear = strYear;
                strlastyear = strYear;


                int tuan = intCurWeek + 1;
                strnextweek = tuan.ToString();

                tuan = intCurWeek - 1;
                strlastweek = tuan.ToString();
            }
            return firstDay;
        }
    }


}