﻿using Library_Shared.Utilities;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using ViPortal_Utils.Base;
using VIPortalAPI.BaseAPI;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.Lconfig;
using ViPortal_Utils.ClassBase;

namespace VIPortalAPI.Controllers
{
    public class TongHopBanDienCuaDVController : SIBaseAPI
    {
        // GET: TongHopBanDienCuaDV
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_TongHopBanDienCuaDV()
        {
            TongHopBanDienCuaDVQuery oTongHopBanDienCuaDVQuery = new TongHopBanDienCuaDVQuery(HttpContext.Current.Request);

            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/CMIS_TongHopBanDienCuaCacDV";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oTongHopBanDienCuaDVQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            var bcDV = oTongHopBanDienCuaDVQuery.MaDonVi;
            var bcNam = oTongHopBanDienCuaDVQuery.SearchNam;
            var bcThang = oTongHopBanDienCuaDVQuery.SearchThang;
            var LoaiBaoCao = oTongHopBanDienCuaDVQuery.LoaiBaoCao;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI+"/"+bcDV+"/"+bcThang+"/"+bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<MapTongHopBanDienCuaDVJson>(responseBody);
            var string1resultUP = model.data1.Replace("\\", "");
            var string2resultUP = model.data2.Replace("\\", "");
           
            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(TongHopBanDienCuaDVJson),
                    new Dictionary<string, string>
                    {
                        {"TenDonVi", "TENDONVI"},
                        {"DienNangTP", "TONGDTP"},
                        {"DTTongCong", "TONGDTPKH"},
                        {"DTTienDien", "TIENDIEN"},
                        {"DTTienCSPK", "TIENCSPK"},
                        {"GiaBinhQuan", "GIABQKH"},
                        //{"ThueTongCong", ""},
                        {"ThueTienDien", "THUE_TD"},
                        {"ThueTienCSPK", "THUE_CSPK"},
                        //{"DTvaThueGTGT", "THOIDIEM_KP_GIO"}
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var oData = JsonConvert.DeserializeObject<List<TongHopBanDienCuaDVJson>>(string1resultUP, settings);
            string donvi;
            if (bcDV == "all")
            {
                donvi = "";
            }
            else if (bcDV == "PD")
            {
                donvi = "Tổng Công ty Điện lực TP Hà Nội";
            }
            else if (bcDV == "PD0600")
            {
                donvi = "Công ty Điện lưc Thanh Trì";
            }
            else if (bcDV == "PD1400")
            {
                donvi = "Công ty Điện lực Long Biên";
            }
            else if (bcDV == "P")
            {
                donvi = "Tập đoàn Điện Lực Việt Nam";
            }
            else if (bcDV == "PD0700")
            {
                donvi = "Công ty Điện lực Gia Lâm";
            }
            else if (bcDV == "PD0800")
            {
                donvi = "Công ty Điện lực Đông Anh";
            }
            else if (bcDV == "PD0900")
            {
                donvi = "Công ty Điện lực Sóc Sơn";
            }
            else if (bcDV == "PD1800")
            {
                donvi = "Công ty Điện lực Chương Mỹ";
            }
            else if (bcDV == "PD2000")
            {
                donvi = "Công ty Điện lực Thường Tín";
            }
            else if (bcDV == "PD2400")
            {
                donvi = "Công ty Điện lực Mỹ Đức";
            }
            else if (bcDV == "PD2600")
            {
                donvi = "Công ty Điện lực Phúc Thọ";
            }
            else if (bcDV == "PD2500")
            {
                donvi = "Công ty Điện lực Phú Xuyên";
            }
            else if (bcDV == "PD2800")
            {
                donvi = "Công ty Điện lực Thanh Oai";
            }
            else if (bcDV == "PD2900")
            {
                donvi = "Công ty Điện lực Ứng Hòa";
            }
            else if (bcDV == "PD0100")
            {
                donvi = "Công ty Điện lực Hoàn Kiếm";
            }
            else if (bcDV == "PD1500")
            {
                donvi = "Công ty Điện lực Mê Linh";
            }
            else if (bcDV == "PD1700")
            {
                donvi = "Công ty Điện lực Sơn Tây ";
            }
            else if (bcDV == "PD1900")
            {
                donvi = "Công ty Điện lực Thạch Thất";
            }
            else if (bcDV == "PD2100")
            {
                donvi = "Công ty Điện lực Ba Vì";
            }
            else if (bcDV == "PD2200")
            {
                donvi = "Công ty Điện lực Đan Phượng";
            }
            else if (bcDV == "PD2300")
            {
                donvi = "Công ty Điện lực Hoài Đức";
            }
            else if (bcDV == "PD2700")
            {
                donvi = "Công ty Điện lực Quốc Oai ";
            }
            else if (bcDV == "PD0400")
            {
                donvi = "Công ty Điện lực Đống Đa ";
            }
            else if (bcDV == "PD0500")
            {
                donvi = "Công ty Điện lực Nam Từ Liêm ";
            }
            else if (bcDV == "PD0200")
            {
                donvi = "Công ty Điện lực Hai Bà Trưng ";
            }
            else if (bcDV == "PD1000")
            {
                donvi = "Công ty Điện lực Tây Hồ  ";
            }
            else if (bcDV == "PD1100")
            {
                donvi = "Công ty Điện lực Thanh Xuân ";
            }
            else if (bcDV == "PD1200")
            {
                donvi = "Công ty Điện lực Cầy giấy ";
            }
            else if (bcDV == "PD3000")
            {
                donvi = "Công ty Điện lực Bắc Từ Liêm ";
            }
            else if (bcDV == "PD1300")
            {
                donvi = "Công ty Điện lực Hoàng Mai ";
            }
            else if (bcDV == "PD0300")
            {
                donvi = "Công ty Điện lực Ba Đình ";
            }
            else if (bcDV == "PD1600")
            {
                donvi = "Công ty Điện lực Hà Đông ";
            }
            else
            {
                donvi = "Tổng Công ty Điện lực TP Hà Nội ";
            };
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tổng Công ty Điện lực TP Hà Nội ",
                TieuDeTrai2 = donvi,
                Title = "TỔNG HỢP BÁN ĐIỆN CỦA CÁC ĐƠN VỊ ",
                NguoiLapBieu = CurentUser.Title,
                LoaiBM = "KDDN 4A",
                Thang = bcThang.ToString(),
                Nam = bcNam.ToString(),

                recordsTotal = oData.Count
            };

            string TenDonVi = "Toàn TCT";
            var recordTotal = new TongHopBanDienCuaDVJson(TenDonVi);
            recordTotal.Style = 1;
            recordTotal.NotHasSTT = 1;

            foreach (TongHopBanDienCuaDVJson eachRecord in oData)
            {
                try
                {
                    recordTotal.DienNangTP = (double.Parse(recordTotal.DienNangTP, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.DienNangTP, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch(Exception e) { }

                try {
                    recordTotal.DTTongCong = (double.Parse(recordTotal.DTTongCong, System.Globalization.CultureInfo.InvariantCulture)
                                                + Math.Round(double.Parse(eachRecord.DTTongCong, System.Globalization.CultureInfo.InvariantCulture))
                                            ).ToString();
                }
                catch(Exception e) { }

                try
                {
                    recordTotal.DTTienDien = (double.Parse(recordTotal.DTTienDien, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.DTTienDien, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch(Exception e) { }

                try
                {
                    recordTotal.DTTienCSPK = (double.Parse(recordTotal.DTTienCSPK, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.DTTienCSPK, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }catch(Exception e) { }

                try
                {
                    recordTotal.GiaBinhQuan = (double.Parse(recordTotal.GiaBinhQuan, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.GiaBinhQuan, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.ThueTienDien = (double.Parse(recordTotal.ThueTienDien, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.ThueTienDien, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }catch(Exception e) { }

                try
                {
                    recordTotal.ThueTienCSPK = (double.Parse(recordTotal.ThueTienCSPK, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.ThueTienCSPK, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }catch(Exception e) { }
               
            }
            oData.Add(recordTotal);

            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oTongHopBanDienCuaDVQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oTongHopBanDienCuaDVQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng: ";
                    if (bcThang != null)
                        Tungaydenngay += bcThang.ToString();
                    Tungaydenngay += " - Năm ";
                    if (bcNam != null)
                        Tungaydenngay += bcNam.ToString();

                    lstValues.Add(new ClsExcel(2, 3, Tungaydenngay, StyleExcell.TextCenter, false, 12, true, 2, 2, 3, 8));
                    string loaiBaoCao = "Loại báo cáo: ";
                    if (LoaiBaoCao == "1")
                    {
                        loaiBaoCao += "Báo cáo tháng";

                    }
                    else if (LoaiBaoCao == "2")
                    {
                        loaiBaoCao += "Báo cáo quý";
                    }
                    else if (LoaiBaoCao == "3")
                    {
                        loaiBaoCao += "Báo cáo 6 tháng";
                    }
                    else
                    {
                        loaiBaoCao += "Báo cáo năm";
                    }
                    
                    lstValues.Add(new ClsExcel(2, 0,donvi, StyleExcell.TextCenterBold, false, 12, false, 3, 3, 5, 8));
                    lstValues.Add(new ClsExcel(3, 9, loaiBaoCao, StyleExcell.TextCenter, false, 12, true, 3, 3, 9, 10));
                    int rowStart = 7;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        string DTTongCong = (double.Parse(item.DTTienDien) + double.Parse(item.DTTienCSPK)).ToString();
                        string DTTongCong2 = (double.Parse(item.DTTienDien) + double.Parse(item.DTTienCSPK)).ToString();
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.TenDonVi, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.DienNangTP, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, DTTongCong, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.DTTienDien, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.DTTienCSPK, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.GiaBinhQuan, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, DTTongCong2, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.ThueTienDien, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.ThueTienCSPK, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, (double.Parse(item.DTTienDien) + double.Parse(item.DTTienCSPK) + double.Parse(item.ThueTienDien) + double.Parse(item.ThueTienCSPK)).ToString(), StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }
                    lstValues.Add(new ClsExcel(4, 9, "Có tổng số: " + oData.Count + " bản ghi", StyleExcell.TextRight, false, 12, true, 4, 4, 9, 10));



                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "NGƯỜI LẬP BIỂU ", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 2, 0, 2));
                    lstValues.Add(new ClsExcel(++rowStart + 2, 0, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 2, rowStart + 6, 0, 2));
                    lstValues.Add(new ClsExcel(rowStart + 7, 0, CurentUser.Title, StyleExcell.TextCenterBold, false, 12, true, rowStart + 7, rowStart + 7, 0, 2));

                    lstValues.Add(new ClsExcel(++rowStart, 3, " Tổ Tổng Hợp", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart, 3, 6));
                    lstValues.Add(new ClsExcel(++rowStart, 3, "", StyleExcell.TextCenterBold, false, 12, true, rowStart, rowStart + 4, 3, 6));
                    lstValues.Add(new ClsExcel(rowStart + 5, 3, dataGiaoDienRender.ToTongHop, StyleExcell.TextCenterBold, false, 12, true, rowStart + 5, rowStart + 5, 3, 6));

                    string ngaythangnam = "Ngày " + DateTime.Now.Day + " Tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    lstValues.Add(new ClsExcel(++rowStart - 4, 7, ngaythangnam, StyleExcell.TextDatetime, false, 12, true, rowStart - 4, rowStart - 4, 7, 10));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 7, "KT.TRƯỞNG BAN", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart - 4, 7, 10));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 7, "PHÓ TRƯỞNG BAN", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart - 4, 7, 10));
                    lstValues.Add(new ClsExcel(++rowStart - 4, 7, "", StyleExcell.TextCenterBold, false, 12, true, rowStart - 4, rowStart, 7, 10));
                    //lstValues.Add(new ClsExcel(rowStart + 1, 2, "", StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 2, 1, 1));
                    lstValues.Add(new ClsExcel(rowStart + 1, 7, dataGiaoDienRender.NguoiKy, StyleExcell.TextCenterBold, false, 12, true, rowStart + 1, rowStart + 1, 7, 10));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "TongHopBanDienCuaDV_DV"+bcDV+"_Thang_"+bcThang+"_Nam_"+bcNam ));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
    }
}