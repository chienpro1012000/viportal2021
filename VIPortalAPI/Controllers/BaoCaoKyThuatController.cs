﻿using Library_Shared.Utilities;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using ViPortal_Utils.Base;
using VIPortalAPI.BaseAPI;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.Lconfig;

namespace VIPortalAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class BaoCaoKyThuatController : SIBaseAPI
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_BaoCaoTinhHinhSuCoDien()
        {
            BaoCaoTinhHinhSuCoLuoiDienQuery oBaoCaoChiTeuDVKHQuery = new BaoCaoTinhHinhSuCoLuoiDienQuery(HttpContext.Current.Request);
            
            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/PMIS_GetAll_TinhHinhSCLD110500kV";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBaoCaoChiTeuDVKHQuery.ItemID , ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            var bcThang = oBaoCaoChiTeuDVKHQuery.SearchThang;
            var bcNam = oBaoCaoChiTeuDVKHQuery.SearchNam;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI+"/"+bcThang+"/"+bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(BaoCaoTinhHinhSuCoLuoiDienJson),
                    new Dictionary<string, string>
                    {
                        {"STT", "STT"},
                        {"TenDonVi", "TENDONVI"},
                        {"TenThietBi", "TENTHIETBI"},
                        {"CapDienAp", "CAPDIENAP"},
                        {"NgayXHSC", "THOIDIEM_XH_NGAY"},
                        {"GioXHSC", "THOIDIEM_XH_GIrO"},
                        {"NgayKPSC", "THOIDIEM_KP_NGAY"},
                        {"GioKPSC", "THOIDIEM_KP_GIO"},
                        {"DienBienSuCo", "DIENBIENSUCO"},
                        {"NguyenNhan", "NGUYENNHAN"},
                        {"ThoiGianGianDoanCungCapDien", "TIME_GIANDOAN_PHUT"},
                        {"ThoiGianSuCo", "TIME_SUCO_PHUT"},
                        {"CongXuatTaiTruocSuCo", "P_TAI_TRUOC_SC"},
                        {"DienNangKhongCungCapDuoc", "DIENNANG_KO_CC_DUOC"},
                        {"CachDien", "SUCO_CAP_CACHDIEN"},
                        {"MoiNoi", "SUCO_CAP_MOINOI"},
                        {"TBPhu", "SUCO_CAP_TBPHU"},
                        {"KhacCap", "SUCO_CAP_KHAC"},
                        {"MayBienAp", "SUCO_TRAM_MBA"},
                        {"MayCat", "SUCO_TRAM_MC"},
                        {"BVTram", "SUCO_TRAM_BV"},
                        {"CSTram", "SUCO_TRAM_CS"},
                        {"MayBienDienApDongDien", "SUCO_TRAM_TUTI"},
                        {"KhacTram", "SUCO_TRAM_KHAC"}
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var root = JsonConvert.DeserializeObject<MapBaoCaoTinhHinhSuCoLuoiDienJson>(responseBody, settings);

            //TestJson objectShareNew = Newtonsoft.Json.JsonConvert.DeserializeObject<TestJson>(responseBody);

                
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "Tập đoàn điện lực Việt Nam",
                TieuDeTrai2 = "TỔNG CÔNG TY ĐIỆN LỰC TP HÀ NỘI",
                Title = "BÁO CÁO TÌNH HÌNH SỰ CỐ LƯỚI ĐIỆN 110kV-500kV",
                Nam = bcThang.ToString() +" Năm "+bcNam.ToString(),
                recordsTotal = root.data.Count
            };

            dataGiaoDienRender.data = root.data;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoChiTeuDVKHQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoChiTeuDVKHQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng: ";
                    if (oBaoCaoChiTeuDVKHQuery.SearchThang != null )
                        Tungaydenngay += oBaoCaoChiTeuDVKHQuery.SearchThang.ToString();
                    Tungaydenngay += " - Năm ";
                    if (oBaoCaoChiTeuDVKHQuery.SearchNam != null)
                        Tungaydenngay += oBaoCaoChiTeuDVKHQuery.SearchNam.ToString();


                    lstValues.Add(new ClsExcel(5, 0, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 0, 0));
                    int rowStart = 10;
                    int STT = 1;
                    //set title
                    foreach (var item in root.data)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.TenDonVi, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.TenThietBi, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.CapDienAp, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.NgayXHSC, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.GioXHSC, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.NgayKPSC, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.GioKPSC, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.KD, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.TQ, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.CachDien, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.MoiNoi, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 12, item.TBPhu, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 13, item.KhacCap, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 14, item.MayBienAp, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 15, item.MayCat, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 16, item.BVTram, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 17, item.CSTram, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 18, item.MayBienDienApDongDien, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 19, item.KhacTram, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 20, item.DienBienSuCo, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 21, item.NguyenNhan, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 22, item.ThoiGianGianDoanCungCapDien, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 23, item.ThoiGianSuCo, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 24, item.CongXuatTaiTruocSuCo, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 25, item.DienNangKhongCungCapDuoc, StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }
                     lstValues.Add(new ClsExcel(6, 24, "Có tổng số: " + root.data.Count + " bản ghi", StyleExcell.TextRight, false, 12,true,6,6,24,25));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "BaoCaoTinhHinhSuCoLuoiDien_Thang" + bcThang.ToString() + "_Nam: " + bcNam.ToString() +"_"+ DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }

        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_BaoCaoChiSoDoTinCayLuoiDienPhanPhoi()
        {
            BaoCaoChiSoDoTinCayLuoiDienPhanPhoiQuery oBaoCaoChiTeuDVKHQuery = new BaoCaoChiSoDoTinCayLuoiDienPhanPhoiQuery(HttpContext.Current.Request);
            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/PMIS_GetAll_THDTCLuoiDienPhanPhoi/02/2022";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBaoCaoChiTeuDVKHQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            var bcThang = oBaoCaoChiTeuDVKHQuery.Thang;
            var bcNam = oBaoCaoChiTeuDVKHQuery.Nam;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + bcThang + "/" + bcNam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(BaoCaoChiSoDoTinCayLuoiDienPhanPhoiJson),
                    new Dictionary<string, string>
                    {
                        {"TenDonVi", "TENDONVI"},
                        {"TongSoKhachHang", "TONG_KH"},
                        {"TongSoLanMatDienThoangQua", "TQ_TONG_SL"},
                        {"TongSoKHBiMatDienThoang", "TQ_TONG_KH"},
                        {"MAIFI", "MAIFI"},
                        {"TongSoLanMatDienKeoDai", "KD_TONG_SL"},
                        {"TongSoKhachHangBiMatKeoDai", "KD_TONG_KH"},
                        {"TongThoiGianMatDienCuaKH", "KD_TONG_THOIGIAN"},
                        {"SAIDI", "SAIDI"},
                        {"SAIFI", "SAIFI"},
                          // {"CAIDI", "SAIFI"},
                       // {"SLLKN", "SAIFI"},
                      //  {"SSLKVoiKeHoachNam", "SAIFI"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var root = JsonConvert.DeserializeObject<MapBaoCaoChiSoDoTinCayLuoiDienPhanPhoiJson>(responseBody, settings);
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "TẬP ĐOÀN ĐIỆN LỰC VIỆT NAM",
                TieuDeTrai2 = "TỔNG CÔNG TY ĐIỆN LỰC TP HÀ NỘI",
                Title = "BÁO CÁO CHỈ SỐ ĐỘ TIN CẬY LƯỚI ĐIỆN PHÂN PHỐI",
                Nam =bcNam,
                Thang =bcThang,
                recordsTotal = root.data.Count
            };

            dataGiaoDienRender.data = root.data;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoChiTeuDVKHQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoChiTeuDVKHQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Thang: ";
                    if (bcThang != null)
                        Tungaydenngay += bcThang;
                    Tungaydenngay += " - Năm : ";
                    if (bcNam != null)
                        Tungaydenngay += bcNam;


                    lstValues.Add(new ClsExcel(5, 0, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 6, 6));
                    int rowStart = 9;

                    //set title
                    foreach (var item in root.data)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, item.TenDonVi, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.TongSoKhachHang, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.TongSoLanMatDienThoangQua, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.TongSoKHBiMatDienThoang, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.MAIFI, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.TongSoLanMatDienKeoDai, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.TongSoKhachHangBiMatKeoDai, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.TongThoiGianMatDienCuaKH, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.SAIDI, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.SAIFI, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.CAIDI, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.SLLKN, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 12, item.SSLKVoiKeHoachNam, StyleExcell.TextLeft));

                        rowStart++;

                    }
                    lstValues.Add(new ClsExcel(6, 11, "Có tổng số: " + root.data.Count + " bản ghi", StyleExcell.TextRight, false, 12, true, 6, 6, 11, 12));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "BaoCaoChiSoDoTinCayLuoiDienPhanPhoi_Thang" + bcThang + "_Nam_" + bcNam));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_TongHopSuCoVaSuatSuCoDuongDayVaTBA()
        {
            TongHopSuCoVaSuatSuCoDuongDayVaTBAQuery oBaoCaoChiTeuDVKHQuery = new TongHopSuCoVaSuatSuCoDuongDayVaTBAQuery(HttpContext.Current.Request);
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/PMIS_GetAll_THSCVaSSCDuongDayVaTBA110500kV";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBaoCaoChiTeuDVKHQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            var nam = oBaoCaoChiTeuDVKHQuery.Nam;
            var thang = oBaoCaoChiTeuDVKHQuery.Thang;

            string APIEVNHanoiNew = oConfigAPI.UrlAPI+"/"+thang+"/"+nam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(TongHopSuCoVaSuatSuCoDuongDayVaTBAJson),
                    new Dictionary<string, string>
                    {
                        {"INDEX1", "INDEX1"},
                         {"INDEX2", "INDEX2"},
                         {"INDEX3", "INDEX3"},
                         {"INDEX4", "INDEX4"},
                         {"INDEX5", "INDEX5"},
                         {"INDEX6", "INDEX6"},
                         {"INDEX7", "INDEX7"},
                         {"INDEX8", "INDEX8"},
                         {"INDEX9", "INDEX9"},
                         {"INDEX10", "INDEX10"},
                         {"INDEX11", "INDEX11"},
                         {"INDEX12", "INDEX12"},
                         {"INDEX13", "INDEX13"},
                         {"INDEX14", "INDEX14"},
                         {"INDEX15", "INDEX15"},
                         {"INDEX16", "INDEX16"},
                         {"INDEX17", "INDEX17"},
                         {"INDEX18", "INDEX18"},
                         {"INDEX19", "INDEX19"},
                         {"INDEX20", "INDEX20"},
                         {"INDEX21", "INDEX21"},
                         {"INDEX22", "INDEX22"},
                         {"INDEX23", "INDEX23"},
                         {"INDEX24", "INDEX24"},
                         {"INDEX25", "INDEX25"},
                         {"INDEX26", "INDEX26"},
                         {"INDEX27", "INDEX27"},
                         {"INDEX28", "INDEX28"},
                         {"INDEX29", "INDEX29"},
                         {"INDEX30", "INDEX30"},
                         {"INDEX31", "INDEX31"},
                         {"INDEX32", "INDEX32"},
                         {"INDEX33", "INDEX33"},
                         {"INDEX34", "INDEX34"},
                         {"INDEX35", "INDEX35"},
                         {"INDEX36", "INDEX36"},
                         {"INDEX37", "INDEX37"},
                         {"INDEX38", "INDEX38"},
                         {"INDEX39", "INDEX39"},
                         {"INDEX40", "INDEX40"},
                         {"INDEX41", "INDEX41"},
                         {"INDEX42", "INDEX42"},
                         {"INDEX43", "INDEX43"},
                         {"INDEX44", "INDEX44"},
                         {"INDEX45", "INDEX45"},
                         {"INDEX46", "INDEX46"},
                         {"INDEX47", "INDEX47"},
                         {"INDEX48", "INDEX48"},
                         {"INDEX49", "INDEX49"},
                         {"INDEX50", "INDEX50"},
                         {"INDEX51", "INDEX51"},
                         {"INDEX52", "INDEX52"},
                         {"INDEX53", "INDEX53"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var root = JsonConvert.DeserializeObject<MapTongHopSuCoVaSuatSuCoDuongDayVaTBAJson>(responseBody, settings);
            
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "TẬP ĐOÀN ĐIỆN LỰC VIỆT NAM",
                TieuDeTrai2 = "TỔNG CÔNG TY ĐIỆN LỰC TP HÀ NỘI",
                Title = "BẢNG TỔNG HỢP SỐ SỰ CỐ VÀ SUẤT SỰ CỐ ĐƯỜNG DÂY VÀ TBA 110 - 500kV",
                Nam = thang.ToString() + "  Năm  " + nam.ToString(),
                recordsTotal = root.data.Count,
                LoaiBM = "Biểu 06_LD",
            };

            dataGiaoDienRender.data = root.data;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoChiTeuDVKHQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoChiTeuDVKHQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng: ";
                    if (thang != null )
                        Tungaydenngay += thang.ToString();
                    Tungaydenngay += " - Năm :";
                    if (nam != null)
                        Tungaydenngay += nam.ToString();


                    lstValues.Add(new ClsExcel(5, 0, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 0, 0));
                    int rowStart = 7;
                    int collStart = 3;

                    //set title
                    foreach (var item in root.data)
                    {
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX1, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX2, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX3, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX4, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX5, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX6, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX7, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX8, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX9, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX10, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX11, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX12, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX13, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX14, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX15, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX16, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX17, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX18, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX19, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX20, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX21, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX22, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX23, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX24, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX25, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX26, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX27, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX28, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX29, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX30, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX31, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX32, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX33, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX34, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX35, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX36, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX37, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX38, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX39, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX40, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX41, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX42, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX43, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX44, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX45, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX46, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX47, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX48, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX49, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX50, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX51, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX52, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX53, StyleExcell.TextCenter));
                        collStart++;
                        rowStart = rowStart -53;

                    }

                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "TongHopSuCoVaSuatSuCoDuongDayVaTBA_Thang" + thang +"-Nam:"+nam+"_"+ DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }

        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_TongHopDoTinCayLuoiDien110_550kV()
        {
            TongHopDoTinCayLuoiDien110_550kVQuery oBaoCaoChiTeuDVKHQuery = new TongHopDoTinCayLuoiDien110_550kVQuery(HttpContext.Current.Request);
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/PMIS_GetAll_THDTCLuoiDien110500kV/11/2021";  
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBaoCaoChiTeuDVKHQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            var nam = oBaoCaoChiTeuDVKHQuery.Nam;
            var thang = oBaoCaoChiTeuDVKHQuery.Thang;

            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + thang + "/" + nam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(TongHopSuCoVaSuatSuCoDuongDayVaTBAJson),
                    new Dictionary<string, string>
                    {
                        {"INDEX1 ","INDEX1"},
                        {"INDEX2 ","INDEX2"},
                        {"INDEX3 ","INDEX3"},
                        {"INDEX4 ","INDEX4"},
                        {"INDEX5 ","INDEX5"},
                        {"INDEX6 ","INDEX6"},
                        {"INDEX7 ","INDEX7"},
                        {"INDEX8 ","INDEX8"},
                        {"INDEX9 ","INDEX9"},
                        {"INDEX10","INDEX10"},
                        {"INDEX11","INDEX11"},
                        {"INDEX12","INDEX12"},
                        {"INDEX13","INDEX13"},
                        {"INDEX14","INDEX14"},
                        {"INDEX15","INDEX15"},
                        {"INDEX16","INDEX16"},
                        {"INDEX17","INDEX17"},
                        {"INDEX18","INDEX18"},
                        {"INDEX19","INDEX19"},

                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var root = JsonConvert.DeserializeObject<MapTongHopSuCoVaSuatSuCoDuongDayVaTBAJson>(responseBody, settings);

            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "TẬP ĐOÀN ĐIỆN LỰC VIỆT NAM",
                TieuDeTrai2 = "TỔNG CÔNG TY ĐIỆN LỰC TP HÀ NỘI",
                Title = "BẢNG TỔNG HỢP ĐỘ TIN CẬY LƯỚI ĐIỆN 110 - 500kV",
                Nam = "Tháng " +thang+ " Năm "+ nam ,
                recordsTotal = root.data.Count
            };

            dataGiaoDienRender.data = root.data;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoChiTeuDVKHQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoChiTeuDVKHQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng: ";
                    if (thang != null)
                        Tungaydenngay += thang;
                    Tungaydenngay += " - Năm";
                    if (nam != null )
                        Tungaydenngay += nam;


                    lstValues.Add(new ClsExcel(5, 0, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 0, 0));
                    int rowStart = 7;
                    int collStart = 3;
                    //set title
                    foreach (var item in root.data)
                    {
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX1, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX2, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX3, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX4, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX5, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX6, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX7, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX8, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX9, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX10, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX11, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX12, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX13, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX14, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX15, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX16, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX17, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX18, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX19, StyleExcell.TextCenter));
                        collStart++;
                        rowStart = rowStart - 19;

                    }
                  
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", " TongHopDoTinCayLuoiDien110_550kV_Thang:" +thang +"_Nam:"+nam+"_"+DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        } 
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_THSCVaSSCDuongDayVaTBALTHA()
        {
            THSCVaSSCDuongDayVaTBALTHAQuery oBaoCaoChiTeuDVKHQuery = new THSCVaSSCDuongDayVaTBALTHAQuery(HttpContext.Current.Request);
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/PMIS_GetAll_THSCVaSSCDuongDayVaTBALTHA/01/2021";  
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBaoCaoChiTeuDVKHQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            var nam = oBaoCaoChiTeuDVKHQuery.Nam;
            var thang = oBaoCaoChiTeuDVKHQuery.Thang;

            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + thang + "/" + nam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(THSCVaSSCDuongDayVaTBALTHAJson),
                    new Dictionary<string, string>
                    {
                        {"INDEX1 ","INDEX1"},
                        {"INDEX2 ","INDEX2"},
                        {"INDEX3 ","INDEX3"},
                        {"INDEX4 ","INDEX4"},
                        {"INDEX5 ","INDEX5"},
                        {"INDEX6 ","INDEX6"},
                        {"INDEX7 ","INDEX7"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var root = JsonConvert.DeserializeObject<MapTHSCVaSSCDuongDayVaTBALTHAJson>(responseBody, settings);

            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "TẬP ĐOÀN ĐIỆN LỰC VIỆT NAM",
                TieuDeTrai2 = "TỔNG CÔNG TY ĐIỆN LỰC TP HÀ NỘI",
                Title = "BẢNG TỔNG HỢP SỐ SỰ CỐ VÀ SUẤT SỰ CỐ ĐƯỜNG DÂY VÀ TBA LƯỚI TRUNG, HẠ ÁP",
                Nam = thang+ " Năm "+ nam ,
                LoaiBM = "Biểu 14-LD",
                recordsTotal = root.data.Count
            };

            dataGiaoDienRender.data = root.data;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoChiTeuDVKHQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoChiTeuDVKHQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng: ";
                    if (thang != null)
                        Tungaydenngay += thang;
                    Tungaydenngay += " - Năm";
                    if (nam != null )
                        Tungaydenngay += nam;


                    lstValues.Add(new ClsExcel(5, 0, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 0, 0));
                    int rowStart = 7;
                    //set title
                    foreach (var item in root.data)
                    {
                        lstValues.Add(new ClsExcel(rowStart, 3, item.INDEX1, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.INDEX2, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.INDEX3, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.INDEX4, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.INDEX5, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.INDEX6, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.INDEX7, StyleExcell.TextCenter));
                       rowStart++;

                    }
                  
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", " THSCVaSSCDuongDayVaTBALTHA_Thang:" + thang +"_Nam:"+nam+"_"+DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }

        [System.Web.Http.HttpPost, System.Web.Http.HttpGet] 
        public DataBaoCaoRender QUERYDATA_BaoCaoTinhHinhSuCoLuoiDienTrungHaAp()
        {
            BaoCaoTinhHinhSuCoLuoiDienTrungHaApQuery oBaoCaoChiTeuDVKHQuery = new BaoCaoTinhHinhSuCoLuoiDienTrungHaApQuery(HttpContext.Current.Request);
            //fix vào lấy theo id của báo cáo.
            
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/PMIS_GetAll_THSCLuoiDienTHA/5/2021";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBaoCaoChiTeuDVKHQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            var thang = oBaoCaoChiTeuDVKHQuery.Thang;
            var nam = oBaoCaoChiTeuDVKHQuery.Nam;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI+ "/"+thang+"/"+nam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(BaoCaoTinhHinhSuCoLuoiDienTrungHaApJson),
                    new Dictionary<string, string>
                    {
                        {"TenDonVi", "TENDONVI"},
                        {"TenThietBi", "TENTHIETBI"},
                        {"CapDienAp", "CAPDIENAP"},
                        {"NgayXHSC", "SC_XH_NGAY"},
                        {"GioXHSC", "SC_XH_GIO"},
                        {"NgayKPSC", "SC_KP_NGAY"},
                        {"GioKPSC", "SC_KP_GIO"},
                        {"KD", "SC_DZ_KD"},
                        {"TQ", "SC_DZ_TQ"},
                        {"TBDC", "SC_DZ_TBDC"},
                        {"CachDien", "SC_CAP_CD"},
                        {"MoiNoi", "SC_CAP_MOINOI"},
                        {"TBPhu", "SC_CAP_TBPHU"},
                        {"KhacCap", "SC_CAP_KHAC"},
                        {"MayBienAp", "SC_TRAM_MBA"},
                        {"KhacTram", "SC_TRAM_KHAC"},
                        {"DienBienSuCo", "DIENBIENSUCO"},
                        {"NguyenNhan", "NGUYENNHAN"},
                        {"ThoiGianGianDoanCungCapDien", "TIME_GIANDOAN_CC_DIEN"},
                        {"ThoiGianSuCo", "TIME_SUCO"},
                        {"KhuVucBiNgungGiamCungCapDien", "KHUVUC_NGUNG_CC_DIEN"},
                        {"SoKhachHangBiNgungGiamCungCapDien", "SL_KH_NGUNG_CC_DIEN"},
                        {"CongXuatTaiTruocSuCo", "P_TRUOC_SC"},
                        {"DienNangKhongCungCapDuoc", "DIENNANG_KO_CC_DUOC"}
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var root = JsonConvert.DeserializeObject<MapBaoCaoTinhHinhSuCoLuoiDienTrungHaApJson>(responseBody, settings);
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "TẬP ĐOÀN ĐIỆN LỰC VIỆT NAM",
                TieuDeTrai2 = "Tổng Công ty Điện lực TP Hà Nội",
                Title = "BÁO CÁO TÌNH HÌNH SỰ CỐ LƯỚI ĐIỆN TRUNG, HẠ ÁP", 
                Nam = thang.ToString() + "  Năm " + nam.ToString(),
                recordsTotal = root.data.Count
            };

            dataGiaoDienRender.data = root.data;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoChiTeuDVKHQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoChiTeuDVKHQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng: ";
                    if (thang !=null )
                        Tungaydenngay += thang.ToString();
                    Tungaydenngay += " - Năm: ";
                    if (nam != null)
                        Tungaydenngay += nam.ToString();


                    lstValues.Add(new ClsExcel(5, 0, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 0, 0));
                    int rowStart = 10;
                    int STT = 1;
                    //set title
                    foreach (var item in root.data)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.TenDonVi, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.TenThietBi, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.CapDienAp, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.NgayXHSC, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.GioXHSC, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.NgayKPSC, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.GioKPSC, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.KD, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.TQ, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.TBDC, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.CachDien, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 12, item.MoiNoi, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 13, item.TBPhu, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 14, item.KhacCap, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 15, item.MayBienAp, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 16, item.KhacTram, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 17, item.DienBienSuCo, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 18, item.NguyenNhan, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 19, item.ThoiGianGianDoanCungCapDien, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 20, item.ThoiGianSuCo, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 21, item.KhuVucBiNgungGiamCungCapDien, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 22, item.SoKhachHangBiNgungGiamCungCapDien, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 23, item.CongXuatTaiTruocSuCo, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 24, item.DienNangKhongCungCapDuoc, StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }
                    lstValues.Add(new ClsExcel(6, 23, "Có tổng số: " + root.data.Count + " bản ghi", StyleExcell.TextRight, false, 12, true, 6, 6, 23, 24));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "BaoCaoTinhHinhSuCoLuoiDienTrungHaAp_Thang" + thang+"_Nam"+nam));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_NhatKyMatDien()
        {
            NhatKyMatDienQuery oNhatKyMatDienQuery = new NhatKyMatDienQuery(HttpContext.Current.Request);
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/OMS_GetAll_TongHopNhatKyMatDien/01-01-2021/01-02-2021"; 
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oNhatKyMatDienQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            var tungay = oNhatKyMatDienQuery.TuNgay.Value.ToString("dd-MM-yyyy");
            var denngay = oNhatKyMatDienQuery.DenNgay.Value.ToString("dd-MM-yyyy");
            string APIEVNHanoiNew = oConfigAPI.UrlAPI+"/"+ tungay+"/"+ denngay;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(NhatKyMatDienJson),
                    new Dictionary<string, string>
                    {
                        {"MaLDM", "lan_matdien"},
                        {"MaLDD", "lan_dongdien"},
                        {"DonVi", "ma_dviqly"},
                        {"DonViTinhDoTinCay", "ma_dviqly_dotincay"},
                        {"DinhDanh", "dinhdanh"},
                        {"TenPhanTuCat", "TenPTCat"},
                        {"TenPhanTuDong", "TenPTDong"},
                        {"NgayMatDien", "ngay_cat"},
                        {"NgayCoDien", "ngay_dong"},
                        {"SoPhutMatDien", "SoPhutMD"},
                        {"SoKH", "so_kh"},
                        {"MaNguyenNhan", "id_nguyennhan"},
                        {"NguyenNhan", "ten_nguyennhan"},
                        {"KhuVuc", "khuvuc"},
                        {"LyDo", "lydo"},
                        {"GhiChu", "ghichu"},
                        {"NguoiCat", "nguoi_cat"},
                        {"NguoiDong", "nguoi_dong"},
                        {"NgayThaoTacCat", "ngay_thaotac_cat"},
                        {"NgayThaoTacDong", "ngay_thaotac_dong"},
                        //{"MAIFI", "KHUVUC_NGUNG_CC_DIEN"},
                        //{"SAIFI", "SL_KH_NGUNG_CC_DIEN"},
                        //{"SAIDI", "P_TRUOC_SC"},
                       
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var root = JsonConvert.DeserializeObject<MapNhatKyMatDienJson>(responseBody, settings);
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                Title = "Nhật ký mất điện",
                Nam = "Từ ngày: " + tungay.ToString()+"  Đến ngày: "+denngay.ToString(),
                recordsTotal = root.data.Count
            };

            dataGiaoDienRender.data = root.data;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oNhatKyMatDienQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oNhatKyMatDienQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Từ ngày: ";
                    if (tungay != null)
                        Tungaydenngay += tungay.ToString();
                    Tungaydenngay += " - Đến ngày: ";
                    if (denngay != null)
                        Tungaydenngay += denngay.ToString();


                    lstValues.Add(new ClsExcel(1, 9, Tungaydenngay, StyleExcell.TextDatetime, false, 12, true, 1, 1, 9, 11));
                    int rowStart = 4;
                    int STT = 1;
                    //set title
                    foreach (var item in root.data)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.MaLDM, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.MaLDD, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.DonVi, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.DonViTinhDoTinCay, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.DinhDanh, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.TenPhanTuCat, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.TenPhanTuDong, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.NgayMatDien, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.NgayCoDien, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.IDLich, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.SoPhutMatDien, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 12, item.SoKH, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 13, item.MaNguyenNhan, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 14, item.NguyenNhan, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 15, item.KhuVuc, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 16, item.LyDo, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 17, item.GhiChu, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 18, item.NguoiCat, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 19, item.NguoiDong, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 20, item.NgayThaoTacCat, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 21, item.NgayThaoTacDong, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 22, item.MAIFI, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 23, item.SAIFI, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 24, item.SAIDI, StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }
                    lstValues.Add(new ClsExcel(2, 23, "Có tổng số: " + root.data.Count + " bản ghi", StyleExcell.TextRight, false, 12, true, 2, 2, 23, 24));
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "NhatKyMatDien_Tu_" +tungay+"_Den_"+denngay ));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
    }
}