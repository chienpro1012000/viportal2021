﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ViPortal_Utils.Base;
using VIPortalAPI.BaseAPI;
using VIPortalAPP;
using ViPortalData.Lconfig;
using ViPortalData.TinNoiBat;

namespace VIPortalAPI.Controllers
{
    /// <summary>
    /// tin tuc
    /// </summary>
    public class NewsController : SIBaseAPI
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        /// <summary>
        /// GetByIdNew 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public TinTucItem GetByIdNew()
        {
            TinTucItem oTinTucItem = new TinTucItem();
            TinTucQuery oTinTucQuery = new TinTucQuery(HttpContext.Current.Request);
            TinTucDA oTinTucDA = new TinTucDA(UrlSite, oTinTucQuery.UrlListTinTuc);
            oTinTucItem = oTinTucDA.GetByIdToObject<TinTucItem>(oTinTucQuery.ItemID);
            return oTinTucItem;
        }

        /// <summary>
        /// get danh sach tin theo chuyen muc
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA()
        {
            DataGridRender oGrid = new DataGridRender();
            TinTucQuery oTinTucQuery = new TinTucQuery(HttpContext.Current.Request);
            if (!oTinTucQuery.isTichHop)
            {
                TinTucDA oTinTucDA = new TinTucDA(UrlSite, oTinTucQuery.UrlListTinTuc);
                oTinTucQuery._ModerationStatus = 1;
                var oData = oTinTucDA.GetListJsonSolr(oTinTucQuery);
                if(oTinTucQuery.lstUrLNewList != null && oTinTucQuery.lstUrLNewList.Count > 0)
                {
                    DanhMucThongTinDA oDanhMucTTDA = new DanhMucThongTinDA(UrlSite);
                    DanhMucThongTinQuery oDanhMucThongTinQuery = new DanhMucThongTinQuery() { _ModerationStatus = 1 };
                    List<DanhMucThongTinJson> lstDanhMucTin = oDanhMucTTDA.GetListJson(oDanhMucThongTinQuery);

                    //oData = oData.Select(x => { x.DanhMucTin = oTinTucQuery.DanhMucTin; return x; }).ToList();
                    foreach (TinTucJson oTinTucJson in oData)
                    {
                        //string ListName = oTinTucJson.UrlListFull
                        oTinTucJson.DanhMucTin = lstDanhMucTin.FirstOrDefault(x => x.UrlList == oTinTucJson.UrlList)?.Title;
                    }
                }
                if (!string.IsNullOrEmpty(oTinTucQuery.DanhMucTin))
                {
                    oData = oData.Select(x => { x.DanhMucTin = oTinTucQuery.DanhMucTin; return x; }).ToList();
                }
                oGrid = new DataGridRender(oData, oTinTucQuery.Draw,
                    oTinTucDA.TongSoBanGhiSauKhiQuery);
            }
            else
            {
                //https://apicskh.evnhanoi.com.vn/api/PublicNews/GetNewsByCategoryKey?key=tin-hoat-dong&pageIndex=2&pageSize=20
                LconfigDA lconfigDA = new LconfigDA();
                string APIUrlEVNHaNoiNew = lconfigDA.GetValueConfigByType("APIUrlEVNHaNoiNew");
                if (!string.IsNullOrEmpty(APIUrlEVNHaNoiNew))
                {
                    int page = (oTinTucQuery.Start / oTinTucQuery.Length) + 1;
                    string APIEVNHanoiNew = $"{APIUrlEVNHaNoiNew}/api/PublicNews/GetNewsByCategoryKey?key={oTinTucQuery.UrlListTinTuc}&pageIndex={page}&pageSize={oTinTucQuery.Length}";
                    HttpClient client = new HttpClient();
                    HttpResponseMessage response =  client.GetAsync(APIEVNHanoiNew).Result;
                    response.EnsureSuccessStatusCode();
                    string responseBody =  response.Content.ReadAsStringAsync().Result;
                    ObjectShareNew objectShareNew = Newtonsoft.Json.JsonConvert.DeserializeObject<ObjectShareNew>(responseBody);
                    List<TinTucJson> lstNews = new List<TinTucJson>();
                    lstNews = objectShareNew.newsList.Select(x => new TinTucJson()
                    {
                        ID = x.id,
                        Title = x.newsTitle,
                        DanhMucTin = x.newsCategoryName,
                        CreatedDate = x.newsPublishedDate,
                        Created = x.newsPublishedDate,
                        DescriptionNews = x.newsSummary,
                        ImageNews = x.newsPictureThumbnailUrl,
                        UrlList = x.newsCategoryKey + "&isTichHop=true&Keynew=" + x.newsKey,
                        UrlListFull = x.newsCategoryKey,
                        Titles = x.newsTitle,
                    }).ToList();
                    oGrid = new DataGridRender(lstNews, oTinTucQuery.Draw,
                     objectShareNew.totalPages * oTinTucQuery.Length);
                }
            }
            return oGrid;
        }

        /// <summary>
        /// get danh sach tin theo chuyen muc
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATAHomeSolr()
        {
            TinNoiBatDA oNewsDA = new TinNoiBatDA(UrlSite);
            TinNoiBatQuery oNewsQuery = new TinNoiBatQuery(HttpContext.Current.Request);
            oNewsQuery._ModerationStatus = 1;
            //oNewsQuery.UrlList = new List<string>() { "/noidung/Lists/TinNoiBat"};
            var oData = oNewsDA.GetListJson(oNewsQuery);
            DataGridRender oGrid = new DataGridRender(oData, oNewsQuery.Draw,
                oNewsDA.TongSoBanGhiSauKhiQuery);
            return oGrid;

        }
        /// <summary>
        /// get Tin noi bat
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA_HOTNEW()
        {
            TinNoiBatDA oNewsDA = new TinNoiBatDA(UrlSite);
            TinNoiBatQuery oNewsQuery = new TinNoiBatQuery(HttpContext.Current.Request);
            oNewsQuery._ModerationStatus = 1;
            //oNewsQuery.UrlList = new List<string>() { "/noidung/Lists/TinNoiBat"};
            var oData = oNewsDA.GetListJson(oNewsQuery);
            var oData2 = new List<TinNoiBatJson>();
            if (oData.Count > 0)
            {
                foreach (var item in oData)
                {
                    if (!string.IsNullOrEmpty(item.UrlList))
                    {
                        item.Title_DanhMuc = GetTitle(item.UrlList.Split('/').Last());
                        oData2.Add(item);
                    }

                }
            }            
            DataGridRender oGrid = new DataGridRender(oData2, oNewsQuery.Draw,oNewsDA.TongSoBanGhiSauKhiQuery);
            return oGrid;

        }
        public string GetTitle(string UrlList)
        {            
            DanhMucThongTinJson oDanhMucTinTucItem = new DanhMucThongTinJson();
            DanhMucThongTinDA oDanhMucThongTinDA = new DanhMucThongTinDA(UrlSite);
            if (oDanhMucThongTinDA.GetListJson(new DanhMucThongTinQuery() { UrlList = UrlList }).Count > 0)
            {
                oDanhMucTinTucItem = oDanhMucThongTinDA.GetListJson(new DanhMucThongTinQuery() { UrlList = UrlList })[0];
            }            
            return oDanhMucTinTucItem.Title;
        }
    }
}