﻿using Library_Shared.Utilities;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using ViPortal_Utils.Base;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.Lconfig;

namespace VIPortalAPI.Controllers
{
    public class BanKeHoach2Controller : ApiController
    {
        // GET: BanKeHoach2
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet] 
        public DataBaoCaoRender QUERYDATA_TongHopTienDoKhoiCongDonDien()
        {
            TongHopTienDoKhoiCongDonDienQuery oTongHopTienDoKhoiCongDongDienQuery = new TongHopTienDoKhoiCongDonDienQuery(HttpContext.Current.Request);

            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $" https://portal.evnhanoi.vn/apigateway//DTXD_BCTHTienDoKhoiCongDongDienCCT/2021";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oTongHopTienDoKhoiCongDongDienQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
           
            var nam = oTongHopTienDoKhoiCongDongDienQuery.Nam;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + nam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(TongHopTienDoKhoiCongDonDienJson),
                    new Dictionary<string, string>
                    {

                        {"DanhMucCongTrinh", "TenLoaiHinh"},
                        {"NhomDuAn", "TenNhomDuAn"},
                        {"QuyMo", "NangLucThietKe"},
                        {"NguonVon", "NguonVonSuDung"},
                        {"DonViQLDA", "TenTat"},
                        {"TienDoGiaoKC", "KC_Kehoach"},
                        {"TienDoGiaoDD", "DD_Kehoach"},
                        {"TienDoThucTeKC", "KC_Thucte"},
                        {"TienDoThucTeDD", "DD_Thucte"},
                        {"TinhTrangDuAn", "TenTinhTrang"},
                        {"GhiChu", "GhiChu"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var root = JsonConvert.DeserializeObject<MapTongHopTienDoKhoiCongDonDienJson>(responseBody, settings);
            //chua check duoc dư lieu khi mà show len man hinh cai nào là cha va con
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeGiua1 = "Phục Lục I",
                Title = "TỔNG HỢP TIẾN ĐỘ KHỞI CÔNG, ĐÓNG ĐIỆN CÁC DỰ ÁN ĐẦU TƯ XÂY DỰNG ",  
                Nam = nam,
                recordsTotal = root.data.Count
            };
            dataGiaoDienRender.data = root.data;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oTongHopTienDoKhoiCongDongDienQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oTongHopTienDoKhoiCongDongDienQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    
                    string Tungaydenngay = "  Năm";
                    if (nam.ToString() != null)
                        Tungaydenngay += nam;
                    lstValues.Add(new ClsExcel(4, 5, Tungaydenngay, StyleExcell.TextDatetime, false, 12,true,4,4,5,7));

                    int rowStart = 8;
                    int STT = 1;
                    //set title
                    foreach (var item in root.data)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.DanhMucCongTrinh, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.NhomDuAn, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.QuyMo, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.NguonVon, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.DonViQLDA, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.TienDoGiaoKC, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.TienDoGiaoDD, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.TienDoThucTeKC, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.TienDoThucTeDD, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.TinhTrangDuAn, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.GhiChu, StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }
                    lstValues.Add(new ClsExcel(5, 10, "Có tổng số: " + root.data.Count + " bản ghi", StyleExcell.TextRight, false, 12,true,5,5,10,11));  
                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "TongHopTienDoKhoiCongDongDien" + "_Nam_" + nam));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_BKH_TongHopSuCoVaSuatSuCoDuongDayVaTBA()
        {
            BKH_TongHopSuCoVaSuatSuCoDuongDayVaTBAQuery oBaoCaoChiTeuDVKHQuery = new BKH_TongHopSuCoVaSuatSuCoDuongDayVaTBAQuery(HttpContext.Current.Request);
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/PMIS_GetAll_THSCVaSSCDuongDayVaTBA110500kV/01/2021";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBaoCaoChiTeuDVKHQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            var nam = oBaoCaoChiTeuDVKHQuery.Nam;
            var thang = oBaoCaoChiTeuDVKHQuery.Thang;

            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + thang + "/" + nam;
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(BKH_TongHopSuCoVaSuatSuCoDuongDayVaTBAJson),
                    new Dictionary<string, string>
                    {
                        {"INDEX1", "INDEX1"},
                         {"INDEX2", "INDEX2"},
                         {"INDEX3", "INDEX3"},
                         {"INDEX4", "INDEX4"},
                         {"INDEX5", "INDEX5"},
                         {"INDEX6", "INDEX6"},
                         {"INDEX7", "INDEX7"},
                         {"INDEX8", "INDEX8"},
                         {"INDEX9", "INDEX9"},
                         {"INDEX10", "INDEX10"},
                         {"INDEX11", "INDEX11"},
                         {"INDEX12", "INDEX12"},
                         {"INDEX13", "INDEX13"},
                         {"INDEX14", "INDEX14"},
                         {"INDEX15", "INDEX15"},
                         {"INDEX16", "INDEX16"},
                         {"INDEX17", "INDEX17"},
                         {"INDEX18", "INDEX18"},
                         {"INDEX19", "INDEX19"},
                         {"INDEX20", "INDEX20"},
                         {"INDEX21", "INDEX21"},
                         {"INDEX22", "INDEX22"},
                         {"INDEX23", "INDEX23"},
                         {"INDEX24", "INDEX24"},
                         {"INDEX25", "INDEX25"},
                         {"INDEX26", "INDEX26"},
                         {"INDEX27", "INDEX27"},
                         {"INDEX28", "INDEX28"},
                         {"INDEX29", "INDEX29"},
                         {"INDEX30", "INDEX30"},
                         {"INDEX31", "INDEX31"},
                         {"INDEX32", "INDEX32"},
                         {"INDEX33", "INDEX33"},
                         {"INDEX34", "INDEX34"},
                         {"INDEX35", "INDEX35"},
                         {"INDEX36", "INDEX36"},
                         {"INDEX37", "INDEX37"},
                         {"INDEX38", "INDEX38"},
                         {"INDEX39", "INDEX39"},
                         {"INDEX40", "INDEX40"},
                         {"INDEX41", "INDEX41"},
                         {"INDEX42", "INDEX42"},
                         {"INDEX43", "INDEX43"},
                         {"INDEX44", "INDEX44"},
                         {"INDEX45", "INDEX45"},
                         {"INDEX46", "INDEX46"},
                         {"INDEX47", "INDEX47"},
                         {"INDEX48", "INDEX48"},
                         {"INDEX49", "INDEX49"},
                         {"INDEX50", "INDEX50"},
                         {"INDEX51", "INDEX51"},
                         {"INDEX52", "INDEX52"},
                         {"INDEX53", "INDEX53"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };

            var root = JsonConvert.DeserializeObject<MapBKH_TongHopSuCoVaSuatSuCoDuongDayVaTBAJson>(responseBody, settings);

            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "TẬP ĐOÀN ĐIỆN LỰC VIỆT NAM",
                TieuDeTrai2 = "TỔNG CÔNG TY ĐIỆN LỰC TP HÀ NỘI",
                Title = "BẢNG TỔNG HỢP SỐ SỰ CỐ VÀ SUẤT SỰ CỐ ĐƯỜNG DÂY VÀ TBA 110 - 500kV",
                Nam = "Tháng:" + thang.ToString() + " - Năm : " + nam.ToString(),
                recordsTotal = root.data.Count,
                LoaiBM = "Biểu 06_LD",
            };

            dataGiaoDienRender.data = root.data;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoChiTeuDVKHQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoChiTeuDVKHQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Tháng: ";
                    if (thang != null)
                        Tungaydenngay += thang.ToString();
                    Tungaydenngay += " - Năm :";
                    if (nam != null)
                        Tungaydenngay += nam.ToString();


                    lstValues.Add(new ClsExcel(5, 0, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 0, 0));
                    int rowStart = 7;
                    int collStart = 3;

                    //set title
                    foreach (var item in root.data)
                    {
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX1, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX2, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX3, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX4, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX5, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX6, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX7, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX8, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX9, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX10, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX11, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX12, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX13, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX14, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX15, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX16, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX17, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX18, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX19, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX20, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX21, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX22, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX23, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX24, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX25, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX26, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX27, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX28, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX29, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX30, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX31, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX32, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX33, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX34, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX35, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX36, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX37, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX38, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX39, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX40, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX41, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX42, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX43, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX44, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX45, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX46, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX47, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX48, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX49, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX50, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX51, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX52, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart++, collStart, item.INDEX53, StyleExcell.TextCenter));
                        collStart++;
                        rowStart = rowStart - 53;

                    }

                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "BKH_TongHopSuCoVaSuatSuCoDuongDayVaTBA_Thang" + thang + "-Nam:" + nam + "_" + DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
    }
  
}