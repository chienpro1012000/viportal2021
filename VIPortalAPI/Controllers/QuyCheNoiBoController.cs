﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ViPortal_Utils.Base;
using VIPortalAPI.BaseAPI;
using ViPortalData.DanhMucQuyChe;
using ViPortalData.QuyCheNoiBo;
using ViPortalData.ThongBaoKetLuan;

namespace VIPortalAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class QuyCheNoiBoController : SIBaseAPI
    {
        /// <summary>
        /// QUERYDATA
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA()
        {
            QuyCheNoiBoDA oQuyCheNoiBoDA = new QuyCheNoiBoDA(UrlSite);
            QuyCheNoiBoQuery oQuyCheNoiBoQuery = new QuyCheNoiBoQuery(HttpContext.Current.Request);
            oQuyCheNoiBoQuery._ModerationStatus = 1;
            oQuyCheNoiBoQuery.FieldOrder = "QCNgayDang";
            oQuyCheNoiBoQuery.Ascending = false;
            //DanhMucQuyCheDA oDanhMucDA = new DanhMucQuyCheDA();
            //var oDataDanhMuc = oDanhMucDA.GetListJson(new DanhMucQuyCheQuery() { _ModerationStatus = 1 });
            //List<QuyCheNoiBoJson> lstquyche = new List<QuyCheNoiBoJson>();
            //foreach (var item in oDataDanhMuc)
            //{
                //oQuyCheNoiBoQuery.QC_DMQuyChe = item.ID;
                var oData1 = oQuyCheNoiBoDA.GetListJsonSolr(oQuyCheNoiBoQuery);
                //lstquyche.AddRange(oData1);
            //}
            //var oData1 = oQuyCheNoiBoDA.GetListJson(oQuyCheNoiBoQuery);
            DataGridRender oGrid = new DataGridRender(oData1, oQuyCheNoiBoQuery.Draw,
                oQuyCheNoiBoDA.TongSoBanGhiSauKhiQuery);
            return oGrid;

        }
        /// <summary>
        /// QUERYDATA_THONGBAOKETLUAN
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA_THONGBAOKETLUAN()
        {
            ThongBaoKetLuanDA oThongBaoKetLuanDA = new ThongBaoKetLuanDA(UrlSite);
            ThongBaoKetLuanQuery oThongBaoKetLuanQuery = new ThongBaoKetLuanQuery(HttpContext.Current.Request);
            oThongBaoKetLuanQuery._ModerationStatus = 1;
            oThongBaoKetLuanQuery.FieldOrder = "TBNgayRaThongBao";
            oThongBaoKetLuanQuery.Ascending = false;
            var oData = oThongBaoKetLuanDA.GetListJson(oThongBaoKetLuanQuery);
            DataGridRender oGrid = new DataGridRender(oData, oThongBaoKetLuanQuery.Draw,
                oThongBaoKetLuanDA.TongSoBanGhiSauKhiQuery);
            return oGrid;

        }
    }
}
