﻿using Library_Shared.Utilities;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using ViPortal_Utils.Base;
using VIPortalAPI.BaseAPI;
using VIPortalAPP;
using ViPortalData;
using ViPortalData.Lconfig;

namespace VIPortalAPI.Controllers
{
    public class BCLinhVucQuanTriController : SIBaseAPI
    {
        // Bao cao cac trinh do vien chuc chuyen mon nghiep vu  
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_BaoCaoTrinhDoVienChucCMNV()
        {
            BaoCaoTrinhDoVienChucCMNVQuery oBaoCaoTrinhDoVienChucCMNVQuery = new BaoCaoTrinhDoVienChucCMNVQuery(HttpContext.Current.Request);

            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/HRMS_BaoCaoTrinhDoCMNV/281/01-01-2022";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+ oBaoCaoTrinhDoVienChucCMNVQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }
            string LoaiDV = oBaoCaoTrinhDoVienChucCMNVQuery.LoaiDV; 
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/"+ LoaiDV + "/" + oBaoCaoTrinhDoVienChucCMNVQuery.DenNgay.Value.ToString("dd-MM-yyyy");
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(BaoCaoTrinhDoVienChucCMNVJson),
                    new Dictionary<string, string>
                    {
                        {"STT", "stt"},
                        {"PhanTheoNhomNganh", "TEN_NNGANH_TDUONG"},
                        {"MaSo", "MASO"},
                        {"TongSo", "TONG"},
                        {"TDTienSi", "TIEN_SY"},
                        {"TDThacSi", "THAC_SY"},
                        {"TDDaiHoc", "DAI_HOC"},
                        {"TDCaoDang", "CAO_DANG"},
                        {"TDTrungCap", "TRUNG_CAP"},
                        {"DangVien", "DANGVIEN"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };
            string loaidv = " ";
            if (LoaiDV == "309")
            {
                loaidv += "Công ty Điện lực Phúc Thọ";
            }
            else if (LoaiDV == "299")
            {
                loaidv += "Công ty Điện lực Hà Đông";
            }
            else if (LoaiDV == "300")
            {
                loaidv += "Công ty Điện lực Sơn Tây";
            }
            else if (LoaiDV == "304")
            {
                loaidv += "Công ty Điện lực  Ba Vì";
            }
            else if (LoaiDV == "310")
            {
                loaidv += "Công ty Điện lực Quốc Oai";
            }
            else if (LoaiDV == "282")
            {
                loaidv += "Công ty Thí nghiệm điện Điện lực Hà Nội";
            }
            else if (LoaiDV == "285")
            {
                loaidv += "Công ty Điện lực Hai Bà Trưng";
            }
            else if (LoaiDV == "289")
            {
                loaidv += "Công ty Điện lực Cầu Giấy";
            }
            else if (LoaiDV == "297")
            {
                loaidv += "Công ty Điện lực Long Biên";
            }
            else if (LoaiDV == "301")
            {
                loaidv += "Công ty Điện lực Chương Mỹ";
            }
            else if (LoaiDV == "307")
            {
                loaidv += "Công ty Điện lực Mỹ Đức";
            }
            else if (LoaiDV == "312")
            {
                loaidv += "Công ty Điện lực Ứng Hòa";
            }
            else if (LoaiDV == "305")
            {
                loaidv += "Công ty Điện lực Đan Phượng";
            }
            else if (LoaiDV == "280")
            {
                loaidv += "Trung tâm điều độ Hệ thống điện TP Hà Nội";
            }
            else if (LoaiDV == "292")
            {
                loaidv += "Công ty Điện lực Gia Lâm";
            }
            else if (LoaiDV == "303")
            {
                loaidv += "Công ty Điện lực Thường Tín";
            }
            else if (LoaiDV == "311")
            {
                loaidv += "Công ty Điện lực Thanh Oai";
            }
            else if (LoaiDV == "288")
            {
                loaidv += "Công ty Điện lực Tây Hồ";
            }
            else if (LoaiDV == "290")
            {
                loaidv += "Công ty Điện lực Thanh Xuân";
            }
            else if (LoaiDV == "294")
            {
                loaidv += "Công ty Điện lực Sóc Sơn";
            }
            else if (LoaiDV == "295")
            {
                loaidv += "Công ty Điện lực Thanh Trì";
            }
            else if (LoaiDV == "298")
            {
                loaidv += "Công ty Điện lực Mê Linh";
            }
            else if (LoaiDV == "306")
            {
                loaidv += "Công ty Điện lực Hoài Đức";
            }
            else if (LoaiDV == "277")
            {
                loaidv += "Ban QLDA lưới điện Hà Nội";
            }
            else if (LoaiDV == "293")
            {
                loaidv += "Công ty Điện lực Đông Anh";
            }
            else if (LoaiDV == "318")
            {
                loaidv += "Công ty lưới điện cao thế Thành phố Hà Nội";
            }
            else if (LoaiDV == "287")
            {
                loaidv += "Công ty Điện lực Đống Đa";
            }
            else if (LoaiDV == "477")
            {
                loaidv += "BQLDA Phát triển điện lực Hà Nội ";
            }
            else if (LoaiDV == "281")
            {
                loaidv += "Công ty Công nghệ thông tin Điện lực Hà Nội";
            }
            else if (LoaiDV == "481")
            {
                loaidv += "Công ty Điện lực Bắc Từ Liêm";
            }
            else if (LoaiDV == "286")
            {
                loaidv += "Công ty Điện lực Ba Đình ";
            }
            else if (LoaiDV == "284")
            {
                loaidv += "Công ty Điện lực Hoàn Kiếm";
            }
            else if (LoaiDV == "313")
            {
                loaidv += "Công ty Dịch vụ Điện lực Hà Nội ";
            }
            else if (LoaiDV == "296")
            {
                loaidv += "Công ty Điện lực Hoàng Mai ";
            }
            else if (LoaiDV == "302")
            {
                loaidv += "Công ty Điện lực Thạch Thất";
            }
            else if (LoaiDV == "308")
            {
                loaidv += "Công ty Điện lực Phú Xuyên ";
            }
            else if (LoaiDV == "488")
            {
                loaidv += "Trung tâm chăm sóc khách hàng";
            }
            else if (LoaiDV == "291")
            {
                loaidv += "Công ty Điện lực Nam Từ Liêm";
            }
            else
            {
                loaidv += "Tất cả";
            }
            var root = JsonConvert.DeserializeObject<MapBaoCaoTrinhDoVienChucCMNVJson>(responseBody, settings);


            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "TỔNG CÔNG TY ĐIỆN LỰC THÀNH PHỐ HÀ NỘI",
                TieuDeTrai2 = loaidv,
                Title = "BÁO CÁO TRÌNH ĐỘ VIÊN CHỨC CHUYÊN MÔN NGHIỆP VỤ",
                LoaiBM = "EVN04a",
                SoBM = "Biểu số 4a",
                TieuDeGiua1 = "CỘNG HOÀ XÃ HỘI CHỦ NGHĨA VIỆT NAM",
                TieuDeGiua2 = "Độc lập - Tự do - Hạnh phúc",

                Nam = oBaoCaoTrinhDoVienChucCMNVQuery.DenNgay.Value.ToString("dd-MM-yyyy"),


                recordsTotal = root.data.Count
            };

            var recordTotal = new BaoCaoTrinhDoVienChucCMNVJson("Tổng số");
            recordTotal.Style = 1;
            recordTotal.NotHasSTT = 1;

            foreach (BaoCaoTrinhDoVienChucCMNVJson eachRecord in root.data)
            {
                try
                {
                    recordTotal.TongSo = (double.Parse(recordTotal.TongSo, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TongSo, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.TDTienSi = (double.Parse(recordTotal.TDTienSi, System.Globalization.CultureInfo.InvariantCulture)
                                                + Math.Round(double.Parse(eachRecord.TDTienSi, System.Globalization.CultureInfo.InvariantCulture))
                                            ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.TDThacSi = (double.Parse(recordTotal.TDThacSi, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TDThacSi, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.TDDaiHoc = (double.Parse(recordTotal.TDDaiHoc, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TDDaiHoc, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.TDCaoDang = (double.Parse(recordTotal.TDCaoDang, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TDCaoDang, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.TDTrungCap = (double.Parse(recordTotal.TDTrungCap, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TDTrungCap, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.DangVien = (double.Parse(recordTotal.DangVien, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.DangVien, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

            }
            root.data.Add(recordTotal);


            dataGiaoDienRender.data = root.data;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoTrinhDoVienChucCMNVQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoTrinhDoVienChucCMNVQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Đến ngày: ";
                    if (oBaoCaoTrinhDoVienChucCMNVQuery.DenNgay.HasValue)
                        Tungaydenngay += oBaoCaoTrinhDoVienChucCMNVQuery.DenNgay.Value.ToString("dd/MM/yyyy");
                  
                    lstValues.Add(new ClsExcel(6, 2, Tungaydenngay, StyleExcell.TextDatetime, false, 12, true, 6, 6, 2, 5));
                    int rowStart = 11;
                    int STT = 1;
                    //set title
                    foreach (var item in root.data)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.PhanTheoNhomNganh, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.MaSo, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.TongSo, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.TDTienSi, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.TDThacSi, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.TDDaiHoc, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.TDCaoDang, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.TDTrungCap, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.DangVien, StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }
                    lstValues.Add(new ClsExcel(2, 0, loaidv, StyleExcell.TextCenter, false, 12, false, 2, 2));
                    lstValues.Add(new ClsExcel(7, 8, "Có tổng số: " + root.data.Count + " bản ghi", StyleExcell.TextRight, false, 12, true, 7, 7, 8, 9));

                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "BaoCaoTrinhDoVienChucCMNV_" + DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
     
        // Bao cao cac trinh do vien chuc chuyen mon nghiep vu 
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_BCCoCauLaoDong()
        {
            BCCoCauLaoDongQuery oBCCoCauLaoDongQuery = new BCCoCauLaoDongQuery(HttpContext.Current.Request);

            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/HRMS_BaoCaoCoCauLaoDong";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBCCoCauLaoDongQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var loaiBC = oBCCoCauLaoDongQuery.LoaiBaoCao;
            var kyBC = oBCCoCauLaoDongQuery.KyBaoCao;
            var namBC = oBCCoCauLaoDongQuery.BaoCaoNam;
            var LoaiDV = oBCCoCauLaoDongQuery.LoaiDV;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + loaiBC + "/" + kyBC + "/" + namBC + "/"+ LoaiDV + "/true";
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(BCCoCauLaoDongJson),
                    new Dictionary<string, string>
                    {
                        {"STT", "stt_hienthi"},
                        {"ChucDanh", "chucdanh"},
                        {"MaSo", "maso"},
                        {"TongSoLDCMDenCuoiKiBC", "ldongTongso"},
                        {"DangVien", "ldongDangvien"},
                        {"LDNu", "ldongNu"},
                        {"LDNNuocNgoai", "ldongNuocngoai"},
                        {"DanTocItNguoi", "ldongDantoc"},
                        {"TDuoi30", "tuoiduoi30"},
                        {"Tuoi30D39", "tuoi30den39"},
                        {"Tuoi40D49", "tuoi40den49"},
                        {"Tuoi50D59", "tuoi50den59"},
                        {"TuoiTren60", "tuoitren60"},
                        {"TienSiKT", "tdoTiensiKT"},
                        {"TienSiKTXH", "tdoTiensiKte"},
                        {"ThacSiKT", "tdoThacsiKT"},
                        {"ThacSiKTXH", "tdoThacsiKte"},
                        {"DaiHocKT", "tdoDaihocKT"},
                        {"DaiHocKTXH", "tdoDaihocKte"},
                        {"CaoDangKT", "tdoCaodangKT"},
                        {"CaoDangKTXH", "tdoCaodangKte"},
                        {"TrungCapKT", "tdoTrungcapKT"},
                        {"TrungCapKTXH", "tdoTrungcapKte"},
                        {"CongNhanKT", "tdoCongnhanKT"},
                        {"DaoTaoNgheNganHan", "tdoNganhan"},
                        {"ChuaQuaDaoTao", "tdoChuaDaotao"},
                        {"TrenDaiHoc", "tdoCtriTrenDH"},
                        {"CuNhan", "tdoCtriCunhan"},
                        {"CaoCap", "tdoCtriCaocap"},
                        {"TrungCap", "tdoCtriTrungcap"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };
            string loaidv = "";
            if (LoaiDV == "309")
            {
                loaidv += "Công ty Điện lực Phúc Thọ";
            }
            else if (LoaiDV == "299")
            {
                loaidv += "Công ty Điện lực Hà Đông";
            }
            else if (LoaiDV == "300")
            {
                loaidv += "Công ty Điện lực Sơn Tây";
            }
            else if (LoaiDV == "304")
            {
                loaidv += "Công ty Điện lực  Ba Vì";
            }
            else if (LoaiDV == "310")
            {
                loaidv += "Công ty Điện lực Quốc Oai";
            }
            else if (LoaiDV == "282")
            {
                loaidv += "Công ty Thí nghiệm điện Điện lực Hà Nội";
            }
            else if (LoaiDV == "285")
            {
                loaidv += "Công ty Điện lực Hai Bà Trưng";
            }
            else if (LoaiDV == "289")
            {
                loaidv += "Công ty Điện lực Cầu Giấy";
            }
            else if (LoaiDV == "297")
            {
                loaidv += "Công ty Điện lực Long Biên";
            }
            else if (LoaiDV == "301")
            {
                loaidv += "Công ty Điện lực Chương Mỹ";
            }
            else if (LoaiDV == "307")
            {
                loaidv += "Công ty Điện lực Mỹ Đức";
            }
            else if (LoaiDV == "312")
            {
                loaidv += "Công ty Điện lực Ứng Hòa";
            }
            else if (LoaiDV == "305")
            {
                loaidv += "Công ty Điện lực Đan Phượng";
            }
            else if (LoaiDV == "280")
            {
                loaidv += "Trung tâm điều độ Hệ thống điện TP Hà Nội";
            }
            else if (LoaiDV == "292")
            {
                loaidv += "Công ty Điện lực Gia Lâm";
            }
            else if (LoaiDV == "303")
            {
                loaidv += "Công ty Điện lực Thường Tín";
            }
            else if (LoaiDV == "311")
            {
                loaidv += "Công ty Điện lực Thanh Oai";
            }
            else if (LoaiDV == "288")
            {
                loaidv += "Công ty Điện lực Tây Hồ";
            }
            else if (LoaiDV == "290")
            {
                loaidv += "Công ty Điện lực Thanh Xuân";
            }
            else if (LoaiDV == "294")
            {
                loaidv += "Công ty Điện lực Sóc Sơn";
            }
            else if (LoaiDV == "295")
            {
                loaidv += "Công ty Điện lực Thanh Trì";
            }
            else if (LoaiDV == "298")
            {
                loaidv += "Công ty Điện lực Mê Linh";
            }
            else if (LoaiDV == "306")
            {
                loaidv += "Công ty Điện lực Hoài Đức";
            }
            else if (LoaiDV == "277")
            {
                loaidv += "Ban QLDA lưới điện Hà Nội";
            }
            else if (LoaiDV == "293")
            {
                loaidv += "Công ty Điện lực Đông Anh";
            }
            else if (LoaiDV == "318")
            {
                loaidv += "Công ty lưới điện cao thế Thành phố Hà Nội";
            }
            else if (LoaiDV == "287")
            {
                loaidv += "Công ty Điện lực Đống Đa";
            }
            else if (LoaiDV == "477")
            {
                loaidv += "BQLDA Phát triển điện lực Hà Nội ";
            }
            else if (LoaiDV == "281")
            {
                loaidv += "Công ty Công nghệ thông tin Điện lực Hà Nội";
            }
            else if (LoaiDV == "481")
            {
                loaidv += "Công ty Điện lực Bắc Từ Liêm";
            }
            else if (LoaiDV == "286")
            {
                loaidv += "Công ty Điện lực Ba Đình ";
            }
            else if (LoaiDV == "284")
            {
                loaidv += "Công ty Điện lực Hoàn Kiếm";
            }
            else if (LoaiDV == "313")
            {
                loaidv += "Công ty Dịch vụ Điện lực Hà Nội ";
            }
            else if (LoaiDV == "296")
            {
                loaidv += "Công ty Điện lực Hoàng Mai ";
            }
            else if (LoaiDV == "302")
            {
                loaidv += "Công ty Điện lực Thạch Thất";
            }
            else if (LoaiDV == "308")
            {
                loaidv += "Công ty Điện lực Phú Xuyên ";
            }
            else if (LoaiDV == "488")
            {
                loaidv += "Trung tâm chăm sóc khách hàng";
            }
            else if (LoaiDV == "291")
            {
                loaidv += "Công ty Điện lực Nam Từ Liêm";
            }
            else
            {
                loaidv += "Tất cả";
            }
            var root = JsonConvert.DeserializeObject<MapBCCoCauLaoDongJson>(responseBody, settings);
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "TỔNG CÔNG TY ĐIỆN LỰC THÀNH PHỐ HÀ NỘI",
                TieuDeTrai2 = loaidv,
                Title = "BÁO CÁO CƠ CẤU LAO ĐỘNG",
                LoaiBM = "EVN03",
                SoBM = "Biểu số 3",
                TieuDeGiua1 = "CỘNG HOÀ XÃ HỘI CHỦ NGHĨA VIỆT NAM",
                TieuDeGiua2 = "Độc lập - Tự do - Hạnh phúc",

                Nam = namBC.ToString(),


                recordsTotal = root.data.Count
            };

            
            var recordTotal = new BCCoCauLaoDongJson("Tổng số");
            recordTotal.Style = 1;
            recordTotal.NotHasSTT = 1;

            foreach (BCCoCauLaoDongJson eachRecord in root.data)
            {
                try
                {
                    recordTotal.TongSoLDCMDenCuoiKiBC = (double.Parse(recordTotal.TongSoLDCMDenCuoiKiBC, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TongSoLDCMDenCuoiKiBC, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.DangVien = (double.Parse(recordTotal.DangVien, System.Globalization.CultureInfo.InvariantCulture)
                                                + Math.Round(double.Parse(eachRecord.DangVien, System.Globalization.CultureInfo.InvariantCulture))
                                            ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.LDNu = (double.Parse(recordTotal.LDNu, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.LDNu, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.LDNNuocNgoai = (double.Parse(recordTotal.LDNNuocNgoai, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.LDNNuocNgoai, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.DanTocItNguoi = (double.Parse(recordTotal.DanTocItNguoi, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.DanTocItNguoi, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.TDuoi30 = (double.Parse(recordTotal.TDuoi30, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TDuoi30, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.Tuoi30D39 = (double.Parse(recordTotal.Tuoi30D39, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.Tuoi30D39, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { } 
                try
                {
                    recordTotal.Tuoi40D49 = (double.Parse(recordTotal.Tuoi40D49, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.Tuoi40D49, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.Tuoi50D59 = (double.Parse(recordTotal.Tuoi50D59, System.Globalization.CultureInfo.InvariantCulture)
                                                + Math.Round(double.Parse(eachRecord.Tuoi50D59, System.Globalization.CultureInfo.InvariantCulture))
                                            ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.TuoiTren60 = (double.Parse(recordTotal.TuoiTren60, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TuoiTren60, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.TienSiKT = (double.Parse(recordTotal.TienSiKT, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TienSiKT, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.TienSiKTXH = (double.Parse(recordTotal.TienSiKTXH, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TienSiKTXH, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.ThacSiKT = (double.Parse(recordTotal.ThacSiKT, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.ThacSiKT, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.ThacSiKTXH = (double.Parse(recordTotal.ThacSiKTXH, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.ThacSiKTXH, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }
                try
                {
                    recordTotal.DaiHocKT = (double.Parse(recordTotal.DaiHocKT, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.DaiHocKT, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.DaiHocKTXH = (double.Parse(recordTotal.DaiHocKTXH, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.DaiHocKTXH, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { } 
                try
                {
                    recordTotal.CaoDangKT = (double.Parse(recordTotal.CaoDangKT, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.CaoDangKT, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.CaoDangKTXH = (double.Parse(recordTotal.CaoDangKTXH, System.Globalization.CultureInfo.InvariantCulture)
                                                + Math.Round(double.Parse(eachRecord.CaoDangKTXH, System.Globalization.CultureInfo.InvariantCulture))
                                            ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.TrungCapKT = (double.Parse(recordTotal.TrungCapKT, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TrungCapKT, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.TrungCapKTXH = (double.Parse(recordTotal.TrungCapKTXH, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TrungCapKTXH, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.CongNhanKT = (double.Parse(recordTotal.CongNhanKT, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.CongNhanKT, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.DaoTaoNgheNganHan = (double.Parse(recordTotal.DaoTaoNgheNganHan, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.DaoTaoNgheNganHan, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.ChuaQuaDaoTao = (double.Parse(recordTotal.ChuaQuaDaoTao, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.ChuaQuaDaoTao, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { } 
                try
                {
                    recordTotal.TrenDaiHoc = (double.Parse(recordTotal.TrenDaiHoc, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TrenDaiHoc, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.CuNhan = (double.Parse(recordTotal.CuNhan, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.CuNhan, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.CaoCap = (double.Parse(recordTotal.CaoCap, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.CaoCap, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.TrungCap = (double.Parse(recordTotal.TrungCap, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TrungCap, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

            }
            root.data.Add(recordTotal);


            dataGiaoDienRender.data = root.data;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBCCoCauLaoDongQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBCCoCauLaoDongQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string loaibaocao = "Loại báo cáo:";
                    if (loaiBC == 1)
                    {
                        loaibaocao += "Báo cáo tháng";
                    }
                    else if (loaiBC == 2)
                    {
                        loaibaocao += "Báo cáo quý";
                    }
                    else if (loaiBC == 3)
                    {
                        loaibaocao += "Báo cáo 6 tháng";
                    }
                    else
                    {
                        loaibaocao += "Báo cáo năm";
                    }
                    string kybaocao = "Kỳ báo cáo:";
                    if (kyBC == 1)
                    {
                        kybaocao += "Tháng báo cáo";
                    }
                    else
                    {
                        kybaocao += "Qúy báo cáo";
                    }
                   
                    //Tungaydenngay += " - đến ngày ";
                    //if (oBaoCaoChatLuongCongNhanKTQuery.DenNgay.HasValue)
                    //    Tungaydenngay += oBaoCaoChatLuongCongNhanKTQuery.DenNgay.Value.ToString("dd/MM/yyyy");
                    lstValues.Add(new ClsExcel(5, 10,"Năm : "+ namBC.ToString(), StyleExcell.TextDatetime, false, 12, true, 5, 5, 10, 18));

                    lstValues.Add(new ClsExcel(2, 0,loaidv, StyleExcell.TextCenter, false, 12, false, 2, 2));
                    lstValues.Add(new ClsExcel(6, 16, loaibaocao, StyleExcell.TextCenter, false, 12, true, 6, 6, 16,21));
                    lstValues.Add(new ClsExcel(6, 22, kybaocao, StyleExcell.TextCenter, false, 12, true, 6, 6, 22, 27));
                    int rowStart = 11;
                    int STT = 1;
                    //set title
                    foreach (var item in root.data)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, item.STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.ChucDanh, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.MaSo, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.TongSoLDCMDenCuoiKiBC, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.DangVien, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.LDNu, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.LDNNuocNgoai, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.DanTocItNguoi, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.TDuoi30, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.Tuoi30D39, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.Tuoi40D49, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.Tuoi50D59, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 12, item.TuoiTren60, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 13, item.TienSiKT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 14, item.TienSiKTXH, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 15, item.ThacSiKT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 16, item.ThacSiKTXH, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 17, item.DaiHocKT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 18, item.DaiHocKTXH, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 19, item.CaoDangKT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 20, item.CaoDangKTXH, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 21, item.TrungCapKT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 22, item.TrungCapKTXH, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 23, item.CongNhanKT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 24, item.DaoTaoNgheNganHan, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 25, item.ChuaQuaDaoTao, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 26, item.TrenDaiHoc, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 27, item.CuNhan, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 28, item.CaoCap, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 29, item.TrungCap, StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }
                    lstValues.Add(new ClsExcel(6, 28, "Có tổng số: " + root.data.Count + " bản ghi", StyleExcell.TextRight, false, 12, true, 6, 6, 28, 29));

                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    string _loaibaocao = "_LoaiBC:";
                    if (oBCCoCauLaoDongQuery.LoaiBaoCao == 1)
                    {
                        _loaibaocao += "BaoCaoThang";
                    }
                    else if (oBCCoCauLaoDongQuery.LoaiBaoCao == 2)
                    {
                        _loaibaocao += "BaoCaoQuy";
                    }
                    else if (oBCCoCauLaoDongQuery.LoaiBaoCao == 3)
                    {
                        _loaibaocao += "BaoCao6Thang";
                    }
                    else
                    {
                        _loaibaocao += "BaoCaoNam";
                    }
                    string _kybaocao = "_KyBC:";
                    if (oBCCoCauLaoDongQuery.KyBaoCao == 1)
                    {
                        _kybaocao += "ThangBaoCao";
                    }
                    else
                    {
                        _kybaocao += "QuyBaoCao";
                    }
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "BCCoCauLaoDongQuery"+"_"+ DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }
        // Bao cao chat luong cong nhan ki thuat 
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_BaoCaoChatLuongCongNhanKT()
        {
            BaoCaoChatLuongCongNhanKTQuery oBaoCaoChatLuongCongNhanKTQuery = new BaoCaoChatLuongCongNhanKTQuery(HttpContext.Current.Request);

            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/HRMS_BaoCaoChatLuongCongNhanKyThuat";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBaoCaoChatLuongCongNhanKTQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var loaiBC = oBaoCaoChatLuongCongNhanKTQuery.LoaiBaoCao;
            var kyBC = oBaoCaoChatLuongCongNhanKTQuery.KyBaoCao;
            var namBC = oBaoCaoChatLuongCongNhanKTQuery.BaoCaoNam;
            var LoaiDV = oBaoCaoChatLuongCongNhanKTQuery.LoaiDV;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + loaiBC + "/" + kyBC + "/" + namBC + "/"+ LoaiDV + "/true";
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(BaoCaoChatLuongCongNhanKTJson),
                    new Dictionary<string, string>
                    {
                        {"STT", "stt"},
                        {"DanhMucNghe", "nghe"},
                        {"MaSo", "maso"},
                        {"TongSoCoMat", "comatTongso"},
                        {"NuCoMat", "comatNu"},
                        {"BTThangLuongI", "thangluongbacI"},
                        {"BTThangLuongII", "thangluongbacII"},
                        {"BTThangLuongIII", "thangluongbacIII"},
                        {"BTThangLuongIV", "thangluongbacIV"},
                        {"BTThangLuongV", "thangluongbacV"},
                        {"BTThangLuongVI", "thangluongbacVI"},
                        {"BTThangLuongVII", "thangluongbacVII"},
                        {"BTBangLuongI", "bangluongbacI"},
                        {"BTBangLuongII", "bangluongbacII"},
                        {"BTBangLuongIII", "bangluongbacIII"},
                        {"BTBangLuongIV", "bangluongbacIV"},
                        {"BTBangLuongV", "bangluongbacV"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };
            string loaidv = "";
            if (LoaiDV == "309")
            {
                loaidv += "Công ty Điện lực Phúc Thọ";
            }
            else if (LoaiDV == "299")
            {
                loaidv += "Công ty Điện lực Hà Đông";
            }
            else if (LoaiDV == "300")
            {
                loaidv += "Công ty Điện lực Sơn Tây";
            }
            else if (LoaiDV == "304")
            {
                loaidv += "Công ty Điện lực  Ba Vì";
            }
            else if (LoaiDV == "310")
            {
                loaidv += "Công ty Điện lực Quốc Oai";
            }
            else if (LoaiDV == "282")
            {
                loaidv += "Công ty Thí nghiệm điện Điện lực Hà Nội";
            }
            else if (LoaiDV == "285")
            {
                loaidv += "Công ty Điện lực Hai Bà Trưng";
            }
            else if (LoaiDV == "289")
            {
                loaidv += "Công ty Điện lực Cầu Giấy";
            }
            else if (LoaiDV == "297")
            {
                loaidv += "Công ty Điện lực Long Biên";
            }
            else if (LoaiDV == "301")
            {
                loaidv += "Công ty Điện lực Chương Mỹ";
            }
            else if (LoaiDV == "307")
            {
                loaidv += "Công ty Điện lực Mỹ Đức";
            }
            else if (LoaiDV == "312")
            {
                loaidv += "Công ty Điện lực Ứng Hòa";
            }
            else if (LoaiDV == "305")
            {
                loaidv += "Công ty Điện lực Đan Phượng";
            }
            else if (LoaiDV == "280")
            {
                loaidv += "Trung tâm điều độ Hệ thống điện TP Hà Nội";
            }
            else if (LoaiDV == "292")
            {
                loaidv += "Công ty Điện lực Gia Lâm";
            }
            else if (LoaiDV == "303")
            {
                loaidv += "Công ty Điện lực Thường Tín";
            }
            else if (LoaiDV == "311")
            {
                loaidv += "Công ty Điện lực Thanh Oai";
            }
            else if (LoaiDV == "288")
            {
                loaidv += "Công ty Điện lực Tây Hồ";
            }
            else if (LoaiDV == "290")
            {
                loaidv += "Công ty Điện lực Thanh Xuân";
            }
            else if (LoaiDV == "294")
            {
                loaidv += "Công ty Điện lực Sóc Sơn";
            }
            else if (LoaiDV == "295")
            {
                loaidv += "Công ty Điện lực Thanh Trì";
            }
            else if (LoaiDV == "298")
            {
                loaidv += "Công ty Điện lực Mê Linh";
            }
            else if (LoaiDV == "306")
            {
                loaidv += "Công ty Điện lực Hoài Đức";
            }
            else if (LoaiDV == "277")
            {
                loaidv += "Ban QLDA lưới điện Hà Nội";
            }
            else if (LoaiDV == "293")
            {
                loaidv += "Công ty Điện lực Đông Anh";
            }
            else if (LoaiDV == "318")
            {
                loaidv += "Công ty lưới điện cao thế Thành phố Hà Nội";
            }
            else if (LoaiDV == "287")
            {
                loaidv += "Công ty Điện lực Đống Đa";
            }
            else if (LoaiDV == "477")
            {
                loaidv += "BQLDA Phát triển điện lực Hà Nội ";
            }
            else if (LoaiDV == "281")
            {
                loaidv += "Công ty Công nghệ thông tin Điện lực Hà Nội";
            }
            else if (LoaiDV == "481")
            {
                loaidv += "Công ty Điện lực Bắc Từ Liêm";
            }
            else if (LoaiDV == "286")
            {
                loaidv += "Công ty Điện lực Ba Đình ";
            }
            else if (LoaiDV == "284")
            {
                loaidv += "Công ty Điện lực Hoàn Kiếm";
            }
            else if (LoaiDV == "313")
            {
                loaidv += "Công ty Dịch vụ Điện lực Hà Nội ";
            }
            else if (LoaiDV == "296")
            {
                loaidv += "Công ty Điện lực Hoàng Mai ";
            }
            else if (LoaiDV == "302")
            {
                loaidv += "Công ty Điện lực Thạch Thất";
            }
            else if (LoaiDV == "308")
            {
                loaidv += "Công ty Điện lực Phú Xuyên ";
            }
            else if (LoaiDV == "488")
            {
                loaidv += "Trung tâm chăm sóc khách hàng";
            }
            else if (LoaiDV == "291")
            {
                loaidv += "Công ty Điện lực Nam Từ Liêm";
            }
            else
            {
                loaidv += "Tất cả";
            }
            var root = JsonConvert.DeserializeObject<MapBaoCaoChatLuongCongNhanKTJson>(responseBody, settings);
            List<BaoCaoChatLuongCongNhanKTJson> oData = new List<BaoCaoChatLuongCongNhanKTJson>()
            {
                new BaoCaoChatLuongCongNhanKTJson() {STT="A", DanhMucNghe = "Lao động quản lý (đơn vị cấp 2)",MaSo="CA-01"},
            };

            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "TỔNG CÔNG TY ĐIỆN LỰC THÀNH PHỐ HÀ NỘI",
                TieuDeTrai2 = loaidv,
                Title = "BÁO CÁO CHẤT LƯỢNG CÔNG NHÂN KỸ THUẬT",
                LoaiBM = "EVN04b",
                SoBM = "Biểu số 4b",
                TieuDeGiua1 = "CỘNG HOÀ XÃ HỘI CHỦ NGHĨA VIỆT NAM",
                TieuDeGiua2 = "Độc lập - Tự do - Hạnh phúc",

                Nam = namBC.ToString(),


                recordsTotal = root.data.Count
            };

            string TenDonVi = "Tổng số";
            var recordTotal = new BaoCaoChatLuongCongNhanKTJson(TenDonVi);
            recordTotal.Style = 1;
            recordTotal.NotHasSTT = 1;

            foreach (BaoCaoChatLuongCongNhanKTJson eachRecord in root.data)
            {
                try
                {
                    recordTotal.TongSoCoMat = (double.Parse(recordTotal.TongSoCoMat, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TongSoCoMat, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.NuCoMat = (double.Parse(recordTotal.NuCoMat, System.Globalization.CultureInfo.InvariantCulture)
                                                + Math.Round(double.Parse(eachRecord.NuCoMat, System.Globalization.CultureInfo.InvariantCulture))
                                            ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.BTThangLuongI = (double.Parse(recordTotal.BTThangLuongI, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.BTThangLuongI, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.BTThangLuongII = (double.Parse(recordTotal.BTThangLuongII, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.BTThangLuongII, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.BTThangLuongIII = (double.Parse(recordTotal.BTThangLuongIII, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.BTThangLuongIII, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.BTThangLuongIV = (double.Parse(recordTotal.BTThangLuongIV, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.BTThangLuongIV, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.BTThangLuongV = (double.Parse(recordTotal.BTThangLuongV, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.BTThangLuongV, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }
                try
                {
                    recordTotal.BTThangLuongVI = (double.Parse(recordTotal.BTThangLuongVI, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.BTThangLuongVI, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }
                try
                {
                    recordTotal.BTThangLuongVII = (double.Parse(recordTotal.BTThangLuongVII, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.BTThangLuongVII, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }
                try
                {
                    recordTotal.BTBangLuongI = (double.Parse(recordTotal.BTBangLuongI, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.BTBangLuongI, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }
                try
                {
                    recordTotal.BTBangLuongII = (double.Parse(recordTotal.BTBangLuongII, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.BTBangLuongII, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }
                try
                {
                    recordTotal.BTBangLuongIII = (double.Parse(recordTotal.BTBangLuongIII, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.BTBangLuongIII, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }
                try
                {
                    recordTotal.BTBangLuongIV = (double.Parse(recordTotal.BTBangLuongIV, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.BTBangLuongIV, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }
                try
                {
                    recordTotal.BTBangLuongV = (double.Parse(recordTotal.BTBangLuongV, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.BTBangLuongV, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

            }
            root.data.Add(recordTotal);


            dataGiaoDienRender.data = root.data;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoChatLuongCongNhanKTQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoChatLuongCongNhanKTQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string baoCaoNam = "Năm : ";
                    if (oBaoCaoChatLuongCongNhanKTQuery.BaoCaoNam != null)
                        baoCaoNam += oBaoCaoChatLuongCongNhanKTQuery.BaoCaoNam.ToString();
                    //Tungaydenngay += " - đến ngày ";
                    //if (oBaoCaoChatLuongCongNhanKTQuery.DenNgay.HasValue)
                    //    Tungaydenngay += oBaoCaoChatLuongCongNhanKTQuery.DenNgay.Value.ToString("dd/MM/yyyy");
                    lstValues.Add(new ClsExcel(4, 1, baoCaoNam, StyleExcell.TextDatetime, false, 12, false, 4, 4));
                    string loaibaocao = "Loại báo cáo:";
                    if (oBaoCaoChatLuongCongNhanKTQuery.LoaiBaoCao == 1)
                    {
                        loaibaocao += "Báo cáo tháng";
                    }
                    else if (oBaoCaoChatLuongCongNhanKTQuery.LoaiBaoCao == 2)
                    {
                        loaibaocao += "Báo cáo quý";
                    }
                    else if (oBaoCaoChatLuongCongNhanKTQuery.LoaiBaoCao == 3)
                    {
                        loaibaocao += "Báo cáo 6 tháng";
                    }
                    else
                    {
                        loaibaocao += "Báo cáo năm";
                    }
                    string kybaocao = "Kỳ báo cáo:";
                    if (oBaoCaoChatLuongCongNhanKTQuery.KyBaoCao == 1)
                    {
                        kybaocao += "Tháng báo cáo";
                    }
                    else
                    {
                        kybaocao += "Qúy báo cáo";
                    }
                   
                    //Tungaydenngay += " - đến ngày ";
                    //if (oBaoCaoChatLuongCongNhanKTQuery.DenNgay.HasValue)
                    //    Tungaydenngay += oBaoCaoChatLuongCongNhanKTQuery.DenNgay.Value.ToString("dd/MM/yyyy");
                    lstValues.Add(new ClsExcel(4, 1, baoCaoNam, StyleExcell.TextDatetime, false, 12, false, 4, 4));
                    lstValues.Add(new ClsExcel(1, 0,loaidv, StyleExcell.TextCenter, false, 12, false, 1, 1));
                    lstValues.Add(new ClsExcel(5, 2, loaibaocao, StyleExcell.TextCenter, false, 12, true, 5, 5, 2, 7));
                    lstValues.Add(new ClsExcel(5, 8, kybaocao, StyleExcell.TextCenter, false, 12, true, 5, 5, 8, 13));
                    int rowStart = 10;
                    int STT = 1;
                    //set title
                    foreach (var item in root.data)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, item.STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.DanhMucNghe, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.MaSo, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.TongSoCoMat, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.NuCoMat, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.BTThangLuongI, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.BTThangLuongII, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.BTThangLuongIII, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.BTThangLuongIV, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.BTThangLuongV, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.BTThangLuongVI, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.BTThangLuongVII, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 12, item.BTBangLuongI, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 13, item.BTBangLuongII, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 14, item.BTBangLuongIII, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 15, item.BTBangLuongIV, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 16, item.BTBangLuongV, StyleExcell.TextCenter));
                        rowStart++;
                        STT++;
                    }
                    lstValues.Add(new ClsExcel(6, 14, "Có tổng số: " + root.data.Count + " bản ghi", StyleExcell.TextRight, false, 12, true, 6, 6, 14, 16));

                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues, "CNKT (04B)");
                    string _loaibaocao = "_LoaiBC:";
                    if (oBaoCaoChatLuongCongNhanKTQuery.LoaiBaoCao == 1)
                    {
                        _loaibaocao += "BaoCaoThang";
                    }
                    else if (oBaoCaoChatLuongCongNhanKTQuery.LoaiBaoCao == 2)
                    {
                        _loaibaocao += "BaoCaoQuy";
                    }
                    else if (oBaoCaoChatLuongCongNhanKTQuery.LoaiBaoCao == 3)
                    {
                        _loaibaocao += "BaoCao6Thang";
                    }
                    else
                    {
                        _loaibaocao += "BaoCaoNam";
                    }
                    string _kybaocao = "_KyBC:";
                    if (oBaoCaoChatLuongCongNhanKTQuery.KyBaoCao == 1)
                    {
                        _kybaocao += "ThangBaoCao";
                    }
                    else
                    {
                        _kybaocao += "QuyBaoCao";
                    }

                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "BaoCaoChatLuongCongNhanKT"+ "_" + DateTime.Now.ToString("dd/MM/yyyy")));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }

                }
            }
            return dataGiaoDienRender;
        }

        // Bao cao Lao Dong va Thu Nhap 
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_BaoCaoLDThuNhap()
        {
            BaoCaoLDThuNhapQuery oBaoCaoLDThuNhapQuery = new BaoCaoLDThuNhapQuery(HttpContext.Current.Request);
            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/HRMS_BaoCaoChatLuongCongNhanKyThuat/4/1/2015/281/true";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBaoCaoLDThuNhapQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var loaiBC = oBaoCaoLDThuNhapQuery.LoaiBaoCao;
            var kyBC = oBaoCaoLDThuNhapQuery.KyBaoCao;
            var namBC = oBaoCaoLDThuNhapQuery.BaoCaoNam;
            var LoaiDV = oBaoCaoLDThuNhapQuery.LoaiDV;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + loaiBC + "/" + kyBC + "/" + namBC + "/"+ LoaiDV + "/true";
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(BaoCaoLDThuNhapJson),
                    new Dictionary<string, string>
                    {
                        //{ "STT", ""},
                        { "NganhNgheKT", "nganhnghe"},
                        { "MaSo", "Maso"},
                        { "TongSoLDDauKy", "DK_Tongso"},
                        { "LDNuDauKy", "DK_Tongsonu"},
                        { "LDDongBHXKDauKy", "DK_TongsoBHXH"},
                        { "LDHDDuoi6ThangDauKy", "DK_Tongsoduoi6thang"},
                        { "TongSoLDCuoiKy", "CK_Tongso"},
                        { "LDNuCuoiKy", "CK_Tongsonu"},
                        { "LDDongBHXKCuoiKy", "CK_TongsoBHXH"},
                        { "LDHDDuoi6ThangCuoiKy", "CK_Tongsoduoi6thang"},
                        { "LDTangTrongKy", "Trky_Tang"},
                        { "LDGiamTrongKy", "Trky_Giam"},
                        { "LDKhongCoNhuCauSudung", "Trky_Doidu"},
                        { "SoLDBinhQuanTrongKiBC", "Trky_BQ"},
                        { "TongThuNhap", "Tnhap_Tongthunhap"},
                        { "TienLuongVaCacKhoanTCL", "Tnhap_Luong"},
                        { "BHXTraThayLuong", "Tnhap_BHXH"},
                        { "CacThuNhapKhacKTinhVaoCPSXKD", "Tnhap_Thunhapkhac"},
                        { "ThuNhapBinhQuan", "Tnhap_ThunhapBQ"},
                        { "TienLuongBinhQuan", "Tnhap_Luongbinhquan"},
                        { "DongGopCuaDVveBHXHYTTNKPCD", "Donvi_dongBHXH"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };
            string loaidv = "";
            if (LoaiDV == "309")
            {
                loaidv += "Công ty Điện lực Phúc Thọ";
            }
            else if (LoaiDV == "299")
            {
                loaidv += "Công ty Điện lực Hà Đông";
            }
            else if (LoaiDV == "300")
            {
                loaidv += "Công ty Điện lực Sơn Tây";
            }
            else if (LoaiDV == "304")
            {
                loaidv += "Công ty Điện lực  Ba Vì";
            }
            else if (LoaiDV == "310")
            {
                loaidv += "Công ty Điện lực Quốc Oai";
            }
            else if (LoaiDV == "282")
            {
                loaidv += "Công ty Thí nghiệm điện Điện lực Hà Nội";
            }
            else if (LoaiDV == "285")
            {
                loaidv += "Công ty Điện lực Hai Bà Trưng";
            }
            else if (LoaiDV == "289")
            {
                loaidv += "Công ty Điện lực Cầu Giấy";
            }
            else if (LoaiDV == "297")
            {
                loaidv += "Công ty Điện lực Long Biên";
            }
            else if (LoaiDV == "301")
            {
                loaidv += "Công ty Điện lực Chương Mỹ";
            }
            else if (LoaiDV == "307")
            {
                loaidv += "Công ty Điện lực Mỹ Đức";
            }
            else if (LoaiDV == "312")
            {
                loaidv += "Công ty Điện lực Ứng Hòa";
            }
            else if (LoaiDV == "305")
            {
                loaidv += "Công ty Điện lực Đan Phượng";
            }
            else if (LoaiDV == "280")
            {
                loaidv += "Trung tâm điều độ Hệ thống điện TP Hà Nội";
            }
            else if (LoaiDV == "292")
            {
                loaidv += "Công ty Điện lực Gia Lâm";
            }
            else if (LoaiDV == "303")
            {
                loaidv += "Công ty Điện lực Thường Tín";
            }
            else if (LoaiDV == "311")
            {
                loaidv += "Công ty Điện lực Thanh Oai";
            }
            else if (LoaiDV == "288")
            {
                loaidv += "Công ty Điện lực Tây Hồ";
            }
            else if (LoaiDV == "290")
            {
                loaidv += "Công ty Điện lực Thanh Xuân";
            }
            else if (LoaiDV == "294")
            {
                loaidv += "Công ty Điện lực Sóc Sơn";
            }
            else if (LoaiDV == "295")
            {
                loaidv += "Công ty Điện lực Thanh Trì";
            }
            else if (LoaiDV == "298")
            {
                loaidv += "Công ty Điện lực Mê Linh";
            }
            else if (LoaiDV == "306")
            {
                loaidv += "Công ty Điện lực Hoài Đức";
            }
            else if (LoaiDV == "277")
            {
                loaidv += "Ban QLDA lưới điện Hà Nội";
            }
            else if (LoaiDV == "293")
            {
                loaidv += "Công ty Điện lực Đông Anh";
            }
            else if (LoaiDV == "318")
            {
                loaidv += "Công ty lưới điện cao thế Thành phố Hà Nội";
            }
            else if (LoaiDV == "287")
            {
                loaidv += "Công ty Điện lực Đống Đa";
            }
            else if (LoaiDV == "477")
            {
                loaidv += "BQLDA Phát triển điện lực Hà Nội ";
            }
            else if (LoaiDV == "281")
            {
                loaidv += "Công ty Công nghệ thông tin Điện lực Hà Nội";
            }
            else if (LoaiDV == "481")
            {
                loaidv += "Công ty Điện lực Bắc Từ Liêm";
            }
            else if (LoaiDV == "286")
            {
                loaidv += "Công ty Điện lực Ba Đình ";
            }
            else if (LoaiDV == "284")
            {
                loaidv += "Công ty Điện lực Hoàn Kiếm";
            }
            else if (LoaiDV == "313")
            {
                loaidv += "Công ty Dịch vụ Điện lực Hà Nội ";
            }
            else if (LoaiDV == "296")
            {
                loaidv += "Công ty Điện lực Hoàng Mai ";
            }
            else if (LoaiDV == "302")
            {
                loaidv += "Công ty Điện lực Thạch Thất";
            }
            else if (LoaiDV == "308")
            {
                loaidv += "Công ty Điện lực Phú Xuyên ";
            }
            else if (LoaiDV == "488")
            {
                loaidv += "Trung tâm chăm sóc khách hàng";
            }
            else if (LoaiDV == "291")
            {
                loaidv += "Công ty Điện lực Nam Từ Liêm";
            }
            else
            {
                loaidv += "";
            }
            var root = JsonConvert.DeserializeObject<MapBaoCaoLDThuNhapJson>(responseBody, settings);
            var oData = root.data;
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "TỔNG CÔNG TY ĐIỆN LỰC THÀNH PHỐ HÀ NỘI",
                TieuDeTrai2 = loaidv,
                Title = "BÁO CÁO LAO ĐỘNG - THU NHẬP",
                LoaiBM = "EVN02b",
                Thang = " 1",
                GiTruTieuDe = "(THEO NGÀNH KINH TẾ)",
                SoBM = "Biểu số 2b",
                TieuDeGiua1 = "CỘNG HOÀ XÃ HỘI CHỦ NGHĨA VIỆT NAM",
                TieuDeGiua2 = "Độc lập - Tự do - Hạnh phúc",

                Nam = namBC.ToString(),
                recordsTotal = oData.Count 
            };

            
            var recordTotal = new BaoCaoLDThuNhapJson(" Tổng số ");
            recordTotal.Style = 1;
            recordTotal.NotHasSTT = 1;

            foreach (BaoCaoLDThuNhapJson eachRecord in oData)
            {
                try
                {
                    recordTotal.TongSoLDDauKy = (double.Parse(recordTotal.TongSoLDDauKy, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TongSoLDDauKy, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.LDNuDauKy = (double.Parse(recordTotal.LDNuDauKy, System.Globalization.CultureInfo.InvariantCulture)
                                                + Math.Round(double.Parse(eachRecord.LDNuDauKy, System.Globalization.CultureInfo.InvariantCulture))
                                            ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.LDDongBHXKDauKy = (double.Parse(recordTotal.LDDongBHXKDauKy, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.LDDongBHXKDauKy, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.LDHDDuoi6ThangDauKy = (double.Parse(recordTotal.LDHDDuoi6ThangDauKy, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.LDHDDuoi6ThangDauKy, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.TongSoLDCuoiKy = (double.Parse(recordTotal.TongSoLDCuoiKy, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TongSoLDCuoiKy, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.LDNuCuoiKy = (double.Parse(recordTotal.LDNuCuoiKy, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.LDNuCuoiKy, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

                try
                {
                    recordTotal.LDDongBHXKCuoiKy = (double.Parse(recordTotal.LDDongBHXKCuoiKy, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.LDDongBHXKCuoiKy, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }
                
                try
                {
                    recordTotal.LDHDDuoi6ThangCuoiKy = (double.Parse(recordTotal.LDHDDuoi6ThangCuoiKy, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.LDHDDuoi6ThangCuoiKy, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }
                try
                {
                    recordTotal.LDTangTrongKy = (double.Parse(recordTotal.LDTangTrongKy, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.LDTangTrongKy, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }
                
                try
                {
                    recordTotal.LDGiamTrongKy = (double.Parse(recordTotal.LDGiamTrongKy, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.LDGiamTrongKy, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }
                try
                {
                    recordTotal.LDKhongCoNhuCauSudung = (double.Parse(recordTotal.LDKhongCoNhuCauSudung, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.LDKhongCoNhuCauSudung, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { } 
                try
                {
                    recordTotal.SoLDBinhQuanTrongKiBC = (double.Parse(recordTotal.SoLDBinhQuanTrongKiBC, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.SoLDBinhQuanTrongKiBC, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { } 
                try
                {
                    recordTotal.TongThuNhap = (double.Parse(recordTotal.TongThuNhap, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TongThuNhap, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }
                try
                {
                    recordTotal.TienLuongVaCacKhoanTCL = (double.Parse(recordTotal.TienLuongVaCacKhoanTCL, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TienLuongVaCacKhoanTCL, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }
                try
                {
                    recordTotal.BHXTraThayLuong = (double.Parse(recordTotal.BHXTraThayLuong, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.BHXTraThayLuong, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { } 
                try
                {
                    recordTotal.CacThuNhapKhacKTinhVaoCPSXKD = (double.Parse(recordTotal.CacThuNhapKhacKTinhVaoCPSXKD, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.CacThuNhapKhacKTinhVaoCPSXKD, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { } 
                try
                {
                    recordTotal.ThuNhapBinhQuan = (double.Parse(recordTotal.ThuNhapBinhQuan, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.ThuNhapBinhQuan, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }
                try
                {
                    recordTotal.TienLuongBinhQuan = (double.Parse(recordTotal.TienLuongBinhQuan, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.TienLuongBinhQuan, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }
                try
                {
                    recordTotal.DongGopCuaDVveBHXHYTTNKPCD = (double.Parse(recordTotal.DongGopCuaDVveBHXHYTTNKPCD, System.Globalization.CultureInfo.InvariantCulture)
                                            + Math.Round(double.Parse(eachRecord.DongGopCuaDVveBHXHYTTNKPCD, System.Globalization.CultureInfo.InvariantCulture))
                                        ).ToString();
                }
                catch (Exception e) { }

            }
            oData.Add(recordTotal);


            dataGiaoDienRender.data = oData;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoLDThuNhapQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoLDThuNhapQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Năm: ";
                    if (namBC != null )
                        Tungaydenngay += namBC.ToString();
                    string loaibaocao = "Loại báo cáo:";
                    if (loaiBC == 1)
                    {
                        loaibaocao += "Báo cáo tháng";
                    }
                    else if (loaiBC == 2)
                    {
                        loaibaocao += "Báo cáo quý";
                    }
                    else if (loaiBC == 3)
                    {
                        loaibaocao += "Báo cáo 6 tháng";
                    }
                    else
                    {
                        loaibaocao += "Báo cáo năm";
                    }
                    string kybaocao = "Kỳ báo cáo:";
                    if (kyBC == 1)
                    {
                        kybaocao += "Tháng báo cáo";
                    }
                    else
                    {
                        kybaocao += "Qúy báo cáo";
                    }
                  

                    lstValues.Add(new ClsExcel(4, 0, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 4, 4));
                    lstValues.Add(new ClsExcel(6, 6, loaibaocao, StyleExcell.TextDatetime, false, 12, true, 6, 6,6,10));
                    lstValues.Add(new ClsExcel(6, 11, kybaocao, StyleExcell.TextDatetime, false, 12, true, 6, 6,11,14));
                    lstValues.Add(new ClsExcel(1, 0, loaidv, StyleExcell.TextDatetime, false, 12, false, 1, 1));
                    int rowStart = 12;
                    int STT = 1;
                    //set title
                    foreach (var item in oData)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.NganhNgheKT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.MaSo, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.TongSoLDDauKy, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.LDNuDauKy, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.LDDongBHXKDauKy, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.LDHDDuoi6ThangDauKy, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.TongSoLDCuoiKy, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.LDNuCuoiKy, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.LDDongBHXKCuoiKy, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.LDHDDuoi6ThangCuoiKy, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.LDTangTrongKy, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 12, item.LDGiamTrongKy, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 13, item.LDKhongCoNhuCauSudung, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 14, item.SoLDBinhQuanTrongKiBC, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 15, item.TongThuNhap, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 16, item.TienLuongVaCacKhoanTCL, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 17, item.BHXTraThayLuong, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 18, item.CacThuNhapKhacKTinhVaoCPSXKD, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 19, item.ThuNhapBinhQuan, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 20, item.TienLuongBinhQuan, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 21, item.DongGopCuaDVveBHXHYTTNKPCD, StyleExcell.TextCenter));

                        rowStart++;
                        STT++;
                    }
                        lstValues.Add(new ClsExcel(7, 20, "Có tổng số: " + oData.Count + " bản ghi", StyleExcell.TextRight, false, 12, false, 7, 7));
                        lstValues.Add(new ClsExcel(5, 0, "(" + dataGiaoDienRender.GiTruTieuDe + ")", StyleExcell.TextCenter, false, 12, false, 5, 5));

                        hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                        using (MemoryStream exportData = new MemoryStream())
                        {
                            hssfworkbook.Write(exportData);
                            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "BaoCaoLDThuNhap_" + DateTime.Now.ToString("dd/MM/yyyy")));
                            HttpContext.Current.Response.Clear();
                            HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                            HttpContext.Current.Response.End();
                            HttpContext.Current.Response.Flush();
                        }

                    
                }

            }
            return dataGiaoDienRender;
        }
        // Bao cao tang giam lao dong 
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_BaoCaoTangGiamLaoDong()
        {
            string tendoanhnghiep = HttpContext.Current.Request["tendoanhnghiep"];
            BaoCaoTangGiamLaoDongQuery oBaoCaoTangGiamLaoDongQuery = new BaoCaoTangGiamLaoDongQuery(HttpContext.Current.Request);
            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/HRMS_BaoCaoTangGiamLaoDong/1/1/2022/281/true";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBaoCaoTangGiamLaoDongQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var loaiBC = oBaoCaoTangGiamLaoDongQuery.LoaiBaoCao;
            var kyBC = oBaoCaoTangGiamLaoDongQuery.KyBaoCao;
            var namBC = oBaoCaoTangGiamLaoDongQuery.BaoCaoNam;
            var LoaiDV = oBaoCaoTangGiamLaoDongQuery.LoaiDV;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + loaiBC + "/" + kyBC + "/" + namBC + "/"+ LoaiDV + "/true";
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(BaoCaoTangGiamLaoDongJson),
                    new Dictionary<string, string>
                    {
                        {"STT", "Sott"},
                        {"ChiTieu", "Tenchitieu"},
                        {"MaSo", "Maso"},
                        {"TongSoLDPhatSinh", "Laodongkybc"},
                        {"LDNguoiNuocNgoaiPhatSinh", "Ldnuocngoai"},
                        {"LDNuPhatSinh", "Laodongnu"},
                        {"SanXuatKinhDoanhPhatSinh", "CnvcSxkdDien"},
                        {"TongSoLDTuDauNam", "LKtudaunamdenkybc"},
                        {"LDNguoiNuocNgoaiTuDauNam", "LKldnuocngoai"},
                        {"LDNuTuDauNam", "LKlaodongnu"},
                        {"SanXuatKinhDoanhTuDauNam", "LKSxkdDien"},
                        //{"GhiChu", ""},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };
            string loaidv = "";
            if (LoaiDV == "309")
            {
                loaidv += "Công ty Điện lực Phúc Thọ";
            }
            else if (LoaiDV == "299")
            {
                loaidv += "Công ty Điện lực Hà Đông";
            }
            else if (LoaiDV == "300")
            {
                loaidv += "Công ty Điện lực Sơn Tây";
            }
            else if (LoaiDV == "304")
            {
                loaidv += "Công ty Điện lực  Ba Vì";
            }
            else if (LoaiDV == "310")
            {
                loaidv += "Công ty Điện lực Quốc Oai";
            }
            else if (LoaiDV == "282")
            {
                loaidv += "Công ty Thí nghiệm điện Điện lực Hà Nội";
            }
            else if (LoaiDV == "285")
            {
                loaidv += "Công ty Điện lực Hai Bà Trưng";
            }
            else if (LoaiDV == "289")
            {
                loaidv += "Công ty Điện lực Cầu Giấy";
            }
            else if (LoaiDV == "297")
            {
                loaidv += "Công ty Điện lực Long Biên";
            }
            else if (LoaiDV == "301")
            {
                loaidv += "Công ty Điện lực Chương Mỹ";
            }
            else if (LoaiDV == "307")
            {
                loaidv += "Công ty Điện lực Mỹ Đức";
            }
            else if (LoaiDV == "312")
            {
                loaidv += "Công ty Điện lực Ứng Hòa";
            }
            else if (LoaiDV == "305")
            {
                loaidv += "Công ty Điện lực Đan Phượng";
            }
            else if (LoaiDV == "280")
            {
                loaidv += "Trung tâm điều độ Hệ thống điện TP Hà Nội";
            }
            else if (LoaiDV == "292")
            {
                loaidv += "Công ty Điện lực Gia Lâm";
            }
            else if (LoaiDV == "303")
            {
                loaidv += "Công ty Điện lực Thường Tín";
            }
            else if (LoaiDV == "311")
            {
                loaidv += "Công ty Điện lực Thanh Oai";
            }
            else if (LoaiDV == "288")
            {
                loaidv += "Công ty Điện lực Tây Hồ";
            }
            else if (LoaiDV == "290")
            {
                loaidv += "Công ty Điện lực Thanh Xuân";
            }
            else if (LoaiDV == "294")
            {
                loaidv += "Công ty Điện lực Sóc Sơn";
            }
            else if (LoaiDV == "295")
            {
                loaidv += "Công ty Điện lực Thanh Trì";
            }
            else if (LoaiDV == "298")
            {
                loaidv += "Công ty Điện lực Mê Linh";
            }
            else if (LoaiDV == "306")
            {
                loaidv += "Công ty Điện lực Hoài Đức";
            }
            else if (LoaiDV == "277")
            {
                loaidv += "Ban QLDA lưới điện Hà Nội";
            }
            else if (LoaiDV == "293")
            {
                loaidv += "Công ty Điện lực Đông Anh";
            }
            else if (LoaiDV == "318")
            {
                loaidv += "Công ty lưới điện cao thế Thành phố Hà Nội";
            }
            else if (LoaiDV == "287")
            {
                loaidv += "Công ty Điện lực Đống Đa";
            }
            else if (LoaiDV == "477")
            {
                loaidv += "BQLDA Phát triển điện lực Hà Nội ";
            }
            else if (LoaiDV == "281")
            {
                loaidv += "Công ty Công nghệ thông tin Điện lực Hà Nội";
            }
            else if (LoaiDV == "481")
            {
                loaidv += "Công ty Điện lực Bắc Từ Liêm";
            }
            else if (LoaiDV == "286")
            {
                loaidv += "Công ty Điện lực Ba Đình ";
            }
            else if (LoaiDV == "284")
            {
                loaidv += "Công ty Điện lực Hoàn Kiếm";
            }
            else if (LoaiDV == "313")
            {
                loaidv += "Công ty Dịch vụ Điện lực Hà Nội ";
            }
            else if (LoaiDV == "296")
            {
                loaidv += "Công ty Điện lực Hoàng Mai ";
            }
            else if (LoaiDV == "302")
            {
                loaidv += "Công ty Điện lực Thạch Thất";
            }
            else if (LoaiDV == "308")
            {
                loaidv += "Công ty Điện lực Phú Xuyên ";
            }
            else if (LoaiDV == "488")
            {
                loaidv += "Trung tâm chăm sóc khách hàng";
            }
            else if (LoaiDV == "291")
            {
                loaidv += "Công ty Điện lực Nam Từ Liêm";
            }
            else
            {
                loaidv += "Tất cả";
            }
            var root = JsonConvert.DeserializeObject<MapBaoCaoTangGiamLaoDongJson>(responseBody, settings);
            List<BaoCaoTangGiamLaoDongJson> oData = new List<BaoCaoTangGiamLaoDongJson>()
            {
                new  BaoCaoTangGiamLaoDongJson() {ChiTieu = "Lao động quản lý (đơn vị cấp 2)",MaSo="CA-01"},
            };
            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "TỔNG CÔNG TY ĐIỆN LỰC THÀNH PHỐ HÀ NỘI",
                TieuDeTrai2 = loaidv,
                Title = "BÁO CÁO TĂNG GIẢM LAO ĐỘNG ",
                LoaiBM = "EVN01",
                Thang = " 1",

                TieuDeGiua1 = "CỘNG HOÀ XÃ HỘI CHỦ NGHĨA VIỆT NAM",
                TieuDeGiua2 = "Độc lập - Tự do - Hạnh phúc",

                Nam = namBC.ToString(),
                recordsTotal = root.data.Count
            };

            dataGiaoDienRender.data = root.data;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoTangGiamLaoDongQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoTangGiamLaoDongQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string loaibaocao = "Loại báo cáo:";
                    if (loaiBC == 1)
                    {
                        loaibaocao += "Báo cáo tháng";
                    }
                    else if (loaiBC == 2)
                    {
                        loaibaocao += "Báo cáo quý";
                    }
                    else if (loaiBC == 3)
                    {
                        loaibaocao += "Báo cáo 6 tháng";
                    }
                    else
                    {
                        loaibaocao += "Báo cáo năm";
                    }
                    string kybaocao = "Kỳ báo cáo:";
                    if (kyBC == 1)
                    {
                        kybaocao += "Tháng báo cáo";
                    }
                    else
                    {
                        kybaocao += "Qúy báo cáo";
                    }
                   
                    lstValues.Add(new ClsExcel(4, 0,"Năm:"+ namBC.ToString(), StyleExcell.TextDatetime, false, 12, false, 4, 4));
                    lstValues.Add(new ClsExcel(1, 0, loaidv, StyleExcell.TextDatetime, false, 12, false, 1, 1));
                    lstValues.Add(new ClsExcel(5, 4, loaibaocao, StyleExcell.TextDatetime, false, 12, true, 5, 5,4,7));
                    lstValues.Add(new ClsExcel(5, 8, kybaocao, StyleExcell.TextDatetime, false, 12, true, 5,5, 8,11));
                    int rowStart = 11;
                    int STT = 1;
                    //set title
                    foreach (var item in root.data)
                    {
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, item.STT, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.ChiTieu, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.MaSo, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.TongSoLDPhatSinh, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.LDNguoiNuocNgoaiPhatSinh, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.LDNuPhatSinh, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 6, item.SanXuatKinhDoanhPhatSinh, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 7, item.TongSoLDTuDauNam, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 8, item.LDNguoiNuocNgoaiTuDauNam, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 9, item.LDNuTuDauNam, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 10, item.SanXuatKinhDoanhTuDauNam, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 11, item.GhiChu, StyleExcell.TextCenter));

                        rowStart++;
                        STT++;
                    }

                    lstValues.Add(new ClsExcel(6, 10, "Có tổng số: " + root.data.Count + " bản ghi", StyleExcell.TextRight, false, 12, true, 6, 6, 10, 11));

                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                        string _loaibaocao = "_LoaiBC:";
                        if (loaiBC == 1)
                        {
                            _loaibaocao += "BaoCaoThang";
                        }
                        else if (loaiBC == 2)
                        {
                            _loaibaocao += "BaoCaoQuy";
                        }
                        else if (loaiBC == 3)
                        {
                            _loaibaocao += "BaoCao6Thang";
                        }
                        else
                        {
                            _loaibaocao += "BaoCaoNam";
                        }
                        string _kybaocao = "_KyBC:";
                        if (kyBC == 1)
                        {
                            _kybaocao += "ThangBaoCao";
                        }
                        else
                        {
                            _kybaocao += "QuyBaoCao";
                        }
                        using (MemoryStream exportData = new MemoryStream())
                        {
                            hssfworkbook.Write(exportData);
                            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "BaoCaoTangGiamLaoDong_"+ DateTime.Now.ToString("dd/MM/yyyy")));
                            HttpContext.Current.Response.Clear();
                            HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                            HttpContext.Current.Response.End();
                            HttpContext.Current.Response.Flush();
                        }

                }

            }
            return dataGiaoDienRender;
        }
        // Bao cao tang giam lao dong 
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataBaoCaoRender QUERYDATA_BaoCaoUocLaoDong()
        {
            BaoCaoUocLaoDongQuery oBaoCaoUocLaoDongQuery = new BaoCaoUocLaoDongQuery(HttpContext.Current.Request);
            //fix vào lấy theo id của báo cáo.
            ConfigAPI oConfigAPI = new ConfigAPI();
            oConfigAPI.UrlAPI = $"https://portal.evnhanoi.vn/apigateway/HRMS_BaoCaoUocLaoDongThuNhap";  // chỗ này 1/2021 là phải truyền giá trị và ko phải fix thế này nhé.
            oConfigAPI.UserName = "EVNHANOI";
            oConfigAPI.Password = "Evnhanoi@123";
            LconfigDA lconfigDA = new LconfigDA();
            LconfigJson lconfigJson = lconfigDA.GetListJson(new LconfigQuery() { ConfigType = "BaoCao_"+oBaoCaoUocLaoDongQuery.ItemID, ConfigGroup = "CauHinhBaoCao" }).FirstOrDefault();
            if (lconfigJson != null && !string.IsNullOrEmpty(lconfigJson.ConfigValueMulti))
            {
                oConfigAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigAPI>(lconfigJson.ConfigValueMulti);
            }

            var loaiBC = oBaoCaoUocLaoDongQuery.LoaiBaoCao;
            var kyBC = oBaoCaoUocLaoDongQuery.KyBaoCao;
            var namBC = oBaoCaoUocLaoDongQuery.BaoCaoNam;
            var madonVi = oBaoCaoUocLaoDongQuery.maDonvi;
            string APIEVNHanoiNew = oConfigAPI.UrlAPI + "/" + loaiBC + "/" + kyBC + "/" + namBC + "/"+ madonVi + "/true";
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{oConfigAPI.UserName}:{oConfigAPI.Password}");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiNew).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var map = new Dictionary<Type, Dictionary<string, string>>
            {
                {
                    typeof(BaoCaoUocLaoDongJson),
                    new Dictionary<string, string>
                    {
                        //{"STT", "machitieu"},
                        //{"TenDoanhNghiep", "Tenchitieu"},
                        //{"DiaChi", "Tenchitieu"},
                        //{"DienThoai", "Tenchitieu"},
                        //{"Email", "Tenchitieu"},
                        //{"LoaiNghanhSXKinhDoanh", "Tenchitieu"},
                        //{"LoaiHinhKinhTeDoanhNghiep", "Tenchitieu"},
                        {"TenChiTieu", "Tenchitieu"},
                        {"MaSo", "Maso"},
                        {"TongSoDauKy", "Tongso_dauky"},
                        {"LDNuDauKy", "Tongnu_dauky"},
                        {"TongSoCuoiKy", "Tongso_cuoiky"},
                        {"LDNuCuoiKy", "Tongnu_cuoiky"},
                        {"PhatSinhTrongKy", "Thunhapphatsinhtrongky"},
                    }
                }
            };
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DynamicMappingResolver(map)
            };
            string loaidv = "";
            if (madonVi == "309")
            {
                loaidv += "Công ty Điện lực Phúc Thọ";
            }
            else if (madonVi == "299")
            {
                loaidv += "Công ty Điện lực Hà Đông";
            }
            else if (madonVi == "300")
            {
                loaidv += "Công ty Điện lực Sơn Tây";
            }
            else if (madonVi == "304")
            {
                loaidv += "Công ty Điện lực  Ba Vì";
            }
            else if (madonVi == "310")
            {
                loaidv += "Công ty Điện lực Quốc Oai";
            }
            else if (madonVi == "282")
            {
                loaidv += "Công ty Thí nghiệm điện Điện lực Hà Nội";
            }
            else if (madonVi == "285")
            {
                loaidv += "Công ty Điện lực Hai Bà Trưng";
            }
            else if (madonVi == "289")
            {
                loaidv += "Công ty Điện lực Cầu Giấy";
            }
            else if (madonVi == "297")
            {
                loaidv += "Công ty Điện lực Long Biên";
            }
            else if (madonVi == "301")
            {
                loaidv += "Công ty Điện lực Chương Mỹ";
            }
            else if (madonVi == "307")
            {
                loaidv += "Công ty Điện lực Mỹ Đức";
            }
            else if (madonVi == "312")
            {
                loaidv += "Công ty Điện lực Ứng Hòa";
            }
            else if (madonVi == "305")
            {
                loaidv += "Công ty Điện lực Đan Phượng";
            }
            else if (madonVi == "280")
            {
                loaidv += "Trung tâm điều độ Hệ thống điện TP Hà Nội";
            }
            else if (madonVi == "292")
            {
                loaidv += "Công ty Điện lực Gia Lâm";
            }
            else if (madonVi == "303")
            {
                loaidv += "Công ty Điện lực Thường Tín";
            }
            else if (madonVi == "311")
            {
                loaidv += "Công ty Điện lực Thanh Oai";
            }
            else if (madonVi == "288")
            {
                loaidv += "Công ty Điện lực Tây Hồ";
            }
            else if (madonVi == "290")
            {
                loaidv += "Công ty Điện lực Thanh Xuân";
            }
            else if (madonVi == "294")
            {
                loaidv += "Công ty Điện lực Sóc Sơn";
            }
            else if (madonVi == "295")
            {
                loaidv += "Công ty Điện lực Thanh Trì";
            }
            else if (madonVi == "298")
            {
                loaidv += "Công ty Điện lực Mê Linh";
            }
            else if (madonVi == "306")
            {
                loaidv += "Công ty Điện lực Hoài Đức";
            }
            else if (madonVi == "277")
            {
                loaidv += "Ban QLDA lưới điện Hà Nội";
            }
            else if (madonVi == "293")
            {
                loaidv += "Công ty Điện lực Đông Anh";
            }
            else if (madonVi == "318")
            {
                loaidv += "Công ty lưới điện cao thế Thành phố Hà Nội";
            }
            else if (madonVi == "287")
            {
                loaidv += "Công ty Điện lực Đống Đa";
            }
            else if (madonVi == "477")
            {
                loaidv += "BQLDA Phát triển điện lực Hà Nội ";
            }
            else if (madonVi == "281")
            {
                loaidv += "Công ty Công nghệ thông tin Điện lực Hà Nội";
            }
            else if (madonVi == "481")
            {
                loaidv += "Công ty Điện lực Bắc Từ Liêm";
            }
            else if (madonVi == "286")
            {
                loaidv += "Công ty Điện lực Ba Đình ";
            }
            else if (madonVi == "284")
            {
                loaidv += "Công ty Điện lực Hoàn Kiếm";
            }
            else if (madonVi == "313")
            {
                loaidv += "Công ty Dịch vụ Điện lực Hà Nội ";
            }
            else if (madonVi == "296")
            {
                loaidv += "Công ty Điện lực Hoàng Mai ";
            }
            else if (madonVi == "302")
            {
                loaidv += "Công ty Điện lực Thạch Thất";
            }
            else if (madonVi == "308")
            {
                loaidv += "Công ty Điện lực Phú Xuyên ";
            }
            else if (madonVi == "488")
            {
                loaidv += "Trung tâm chăm sóc khách hàng";
            }
            else if (madonVi == "291")
            {
                loaidv += "Công ty Điện lực Nam Từ Liêm";
            }
            else
            {
                loaidv += "Tất cả";
            }
            var root = JsonConvert.DeserializeObject<MapBaoCaoUocLaoDongJson>(responseBody, settings);
            
            
            BaoCaoUocLaoDongJson dataBaocao = new BaoCaoUocLaoDongJson();
            dataBaocao.DiaChi = "";

            DataBaoCaoRender dataGiaoDienRender = new DataBaoCaoRender()
            {
                TieuDeTrai1 = "TỔNG CÔNG TY ĐIỆN LỰC THÀNH PHỐ HÀ NỘI",
                TieuDeTrai2 = loaidv,
                Title = "BÁO CÁO ƯỚC LAO ĐỘNG",
                LoaiBM = "EVN02a",
              

                TieuDeGiua1 = "CỘNG HOÀ XÃ HỘI CHỦ NGHĨA VIỆT NAM",
                TieuDeGiua2 = "Độc lập - Tự do - Hạnh phúc",

                Nam = namBC.ToString(),
                recordsTotal = root.data.Count
            };

            dataGiaoDienRender.data = root.data;
            //DataGridRender oGrid = new DataGridRender(oData, oChiTieuKinhDoanhQuery.Draw,
            //     oData.Count);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["excel"]) && oBaoCaoUocLaoDongQuery.ItemID > 0)
            {
                MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA();
                MenuQuanTriItem oMenuQuanTriItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oBaoCaoUocLaoDongQuery.ItemID);
                if (oMenuQuanTriItem.ListFileAttach.Count > 0)
                {
                    Stream FileExcel = oMenuQuanTriDA.SpListProcess.ParentWeb.GetFile(oMenuQuanTriItem.ListFileAttach[0].Url).OpenBinaryStream();
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileExcel);
                    List<ClsExcel> lstValues = new List<ClsExcel>();
                    string Tungaydenngay = "Năm : ";
                    if (namBC != null)
                        Tungaydenngay += namBC.ToString();
                    
                    string loaibaocao = "Loại báo cáo:";
                    if (loaiBC == 1)
                    {
                        loaibaocao += "Báo cáo tháng";
                    }
                    else if (loaiBC == 2)
                    {
                        loaibaocao += "Báo cáo quý";
                    }
                    else if (loaiBC == 3)
                    {
                        loaibaocao += "Báo cáo 6 tháng";
                    }
                    else
                    {
                        loaibaocao += "Báo cáo năm";
                    }
                    string kybaocao = "Kỳ báo cáo:";
                    if (kyBC == 1)
                    {
                        kybaocao += "Tháng báo cáo";
                    }
                    else
                    {
                        kybaocao += "Qúy báo cáo";
                    }
                    lstValues.Add(new ClsExcel(5, 0, Tungaydenngay, StyleExcell.TextDatetime, false, 12, false, 5, 5));
                    lstValues.Add(new ClsExcel(2, 0, loaidv, StyleExcell.TextDatetime, false, 12, false, 5, 5));
                    lstValues.Add(new ClsExcel(6, 1, dataBaocao.TenDoanhNghiep, StyleExcell.TextDatetime, false, 12, false, 6, 6));
                    lstValues.Add(new ClsExcel(7, 1, dataBaocao.DiaChi, StyleExcell.TextDatetime, false, 12, false, 7, 7));
                    lstValues.Add(new ClsExcel(8, 1, dataBaocao.DienThoai, StyleExcell.TextDatetime, false, 12, false, 8, 8));
                    lstValues.Add(new ClsExcel(9, 1, dataBaocao.Email, StyleExcell.TextDatetime, false, 12, false, 9, 9));
                    lstValues.Add(new ClsExcel(10, 1, dataBaocao.LoaiNghanhSXKinhDoanh, StyleExcell.TextDatetime, false, 12, false, 10, 10));
                    lstValues.Add(new ClsExcel(11, 1, dataBaocao.LoaiHinhKinhTeDoanhNghiep, StyleExcell.TextDatetime, false, 12, false, 11, 11));
                    int rowStart = 17;
                    int STT = 1;
                    int check = 1;
                    //set title
                    foreach (var item in root.data)
                    {
                        
                        //render data ở chỗ này.
                        lstValues.Add(new ClsExcel(rowStart, 0, item.TenChiTieu, StyleExcell.TextLeft));
                        lstValues.Add(new ClsExcel(rowStart, 1, item.MaSo, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 2, item.TongSoDauKy, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 3, item.LDNuDauKy, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 4, item.TongSoCuoiKy, StyleExcell.TextCenter));
                        lstValues.Add(new ClsExcel(rowStart, 5, item.LDNuCuoiKy, StyleExcell.TextCenter));

                        rowStart++;
                        STT++;
                        check++;
                        if (check == 18)
                        {
                            break;
                        }
                    }
                    lstValues.Add(new ClsExcel(rowStart, 0, "2. Thu nhập của người lao động và đóng góp của chủ doanh nghiệp về BHXH, y tế, thất nghiệp, kinh phí công đoàn", StyleExcell.TextLeft, false, 12, true, rowStart, rowStart, 0, 5));
                    lstValues.Add(new ClsExcel(rowStart+1, 1, "Đơn vị tính: Triệu đồng", StyleExcell.TextLeft, false, 12, true, rowStart+1, rowStart+1, 1,2));
                    rowStart = rowStart + 2;
                    lstValues.Add(new ClsExcel(rowStart, 0, " Tên chỉ tiêu", StyleExcell.TextCenter, true, 12, false, rowStart, rowStart));
                    lstValues.Add(new ClsExcel(rowStart, 1, "Mã số", StyleExcell.TextCenter, true, 12, false, rowStart, rowStart));
                    lstValues.Add(new ClsExcel(rowStart, 2, "Phát sinh trong kỳ", StyleExcell.TextCenter, true, 12, false, rowStart, rowStart));

                    rowStart = rowStart + 1;
                    int check2 = 1;
                    foreach (var item in root.data)
                    {
                        check2++;
                        if(check2 >= 19)
                        {
                            //render data ở chỗ này.
                            lstValues.Add(new ClsExcel(rowStart, 0, item.TenChiTieu, StyleExcell.TextLeft));
                            lstValues.Add(new ClsExcel(rowStart, 1, item.MaSo, StyleExcell.TextCenter));
                            lstValues.Add(new ClsExcel(rowStart, 2, item.PhatSinhTrongKy, StyleExcell.TextCenter));

                            rowStart++;
                            STT++;
                        }
                      
                    }


                    hssfworkbook = BaseWordExcel.ExportExcell(hssfworkbook, lstValues);
                    using (MemoryStream exportData = new MemoryStream())
                    {
                        hssfworkbook.Write(exportData);
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xls", "BaoCaoUocLaoDong_Nam" +namBC.ToString()));
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush();
                    }
                }

            }
            return dataGiaoDienRender;
        }
    }
}