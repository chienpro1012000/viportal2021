﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Description;
using System.Web.Mvc;
using ViPortal_Utils.Base;
using VIPortalAPI.BaseAPI;
using VIPortalAPI.Filters;
using VIPortalAPI.Models;
using VIPortalAPP;
using ViPortalData.DMHinhAnh;
using ViPortalData.DMVideo;
using ViPortalData.HoiDap;
using ViPortalData.LichCaNhan;
using ViPortalData.LichDonVi;
using ViPortalData.LichPhong;
using ViPortalData.LichTongCongTy;
using ViPortalData.QuyCheNoiBo;
using ViPortalData.ThongBaoKetLuan;
using ViPortalData.VanBanTaiLieu;
using ViPortalData.DanhMucQuyChe;
using ViPortalData;
using ViPortalData.FAQThuongGap;
using ViPortalData.LoaiTaiLieu;
using ViPortalData.DanhMucChucVu;
using ViPortalData.DMLinhVuc;
using ViPortalData.KSBoChuDe;
using ViPortalData.KSBoCauHoi;
using ViPortalData.KSCauTraLoi;
using ViPortalData.TimKiem;
using ViPortalData.TinNoiBat;
using ViPortalData.TraLoiHoiDap;
using Microsoft.SharePoint;
using System.IO;
using ViPortalData.LichTapDoan;
using Newtonsoft.Json.Linq;
using CommonServiceLocator;
using ViPortal_Utils;
using SolrNet;
using ViPortalData.Lconfig;
using Newtonsoft.Json;
using System.Text;
using System.Net.Mail;
using Microsoft.Exchange.WebServices.Data;

namespace VIPortalAPI.ControllersMobi
{
    /// <summary>
    /// Nhoms api chia se thong tin cho app
    /// </summary>
    public class MobiController : SIBaseAPIWeb
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="controllerContext"></param>
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
        }
        // GET: api/Mobi
        /// <summary>
        /// Ham test
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        #region API Tin tức
        /// <summary>
        /// Input 
        /// {
        ///   "draw":"1",
        ///   "start":"0",
        ///   "length":"20",
        ///   "fieldOrder":"ID",
        ///   "ascending":"false",
        ///   "do":"QUERYDATA"
        ///}
        /// </summary>
        /// <returns></returns>
        /// 
        public ResultAction TinTucQUERYDATA(TinTucQuery oNewsQuery)
        {
            TinTucDA oTinTucDA = new TinTucDA(true);
            oNewsQuery._ModerationStatus = 1;
            if (!string.IsNullOrEmpty(oNewsQuery.UrlListTinTuc))
            {
                oTinTucDA = new TinTucDA("", oNewsQuery.UrlListTinTuc, true);
            }
            if (oNewsQuery.ItemID > 0)
            {
                NewsItem oNewsItem = oTinTucDA.GetByIdToObject<NewsItem>(oNewsQuery.ItemID);
                return new ResultAction(ActionState.Succeed, oNewsItem);
            }
            else
            {
                if (!string.IsNullOrEmpty(oNewsQuery.UrlListTinTuc))
                {
                    //NewsQuery oNewsQuery = new NewsQuery(HttpContext.Current.Request);
                    var oData = oTinTucDA.GetListJson(oNewsQuery);
                    oData = oData.Select(c => { c.ImageNews = CheckImage(c.ImageNews); return c; }).ToList();
                    DataGridRender oGrid = new DataGridRender(oData, oNewsQuery.Draw,
                        oTinTucDA.TongSoBanGhiSauKhiQuery);
                    return new ResultAction(ActionState.Succeed, oGrid);
                }
                else
                {
                    DanhMucThongTinDA oDanhMucThongTinDA = new DanhMucThongTinDA(true);
                    oNewsQuery.UrlList =
                        oDanhMucThongTinDA.GetListJson(new DanhMucThongTinQuery()
                        {
                            _ModerationStatus = 1
                        }).Select(x => $"/noidung/Lists/{x.UrlList}").ToList();
                    var oData = oTinTucDA.GetListJsonSolr(oNewsQuery);

                    oData = oData.Select(c => { c.ImageNews = CheckImage(c.ImageNews); return c; }).ToList();

                    DataGridRender oGrid = new DataGridRender(oData, oNewsQuery.Draw,
                        oTinTucDA.TongSoBanGhiSauKhiQuery);
                    return new ResultAction(ActionState.Succeed, oGrid);
                }
            }
        }

        private string CheckImage(string ImageNews)
        {
            if (!ImageNews.StartsWith("http"))
            {
                ImageNews = Request.RequestUri.GetLeftPart(UriPartial.Authority) + ImageNews;
            }
            return ImageNews;
        }

        /// <summary>
        /// Ham lay thong bao
        /// </summary>
        /// <returns></returns>
        public ResultAction DANHMUCTIN_QUERYDATA(DanhMucThongTinQuery oDanhMucThongTinQuery)
        {
            DanhMucThongTinDA oDanhMucThongTinDA = new DanhMucThongTinDA(true);
            oDanhMucThongTinQuery._ModerationStatus = 1;
            if (oDanhMucThongTinQuery.ItemID > 0)
            {
                DanhMucThongTinItem oNewsItem = oDanhMucThongTinDA.GetByIdToObject<DanhMucThongTinItem>(oDanhMucThongTinQuery.ItemID);
                return new ResultAction(ActionState.Succeed, oNewsItem);
            }
            else
            {
                List<DanhMucThongTinJson> oData = new List<DanhMucThongTinJson>();

                if (string.IsNullOrEmpty(oDanhMucThongTinQuery.Urlsite))
                {
                    //add thêm ở đoạn này các danh mục thông tin.
                    LconfigDA lconfigDA = new LconfigDA(oDanhMucThongTinQuery.Urlsite, true);
                    List<LconfigJson> lconfigJsons = lconfigDA.GetListJson(new LconfigQuery() { ConfigGroup = "DanhMucTinEVNHaNoiShare", _ModerationStatus = 1 });
                    oData.AddRange(lconfigJsons.Select(x => new DanhMucThongTinJson()
                    {
                        ID = x.ID + 999,
                        Title = x.Title,
                        isTichHop = true,
                        UrlList = x.ConfigValue
                    }));
                }
                //DanhMucThongTinQuery oDanhMucThongTinQuery = new DanhMucThongTinQuery(HttpContext.Current.Request);
                oData = oDanhMucThongTinDA.GetListJson(oDanhMucThongTinQuery);
                DataGridRender oGrid = new DataGridRender(oData, oDanhMucThongTinQuery.Draw,
                    oDanhMucThongTinDA.TongSoBanGhiSauKhiQuery);
                return new ResultAction(ActionState.Succeed, oGrid);
            }

        }
        /// <summary>
        /// Ham lay thong bao
        /// </summary>
        /// <returns></returns>
        public ResultAction THONGBAO_QUERYDATA(ThongBaoQuery oThongBaoQuery)
        {
            ThongBaoDA oThongBaoDA = new ThongBaoDA(oThongBaoQuery.Urlsite, true);
            oThongBaoQuery._ModerationStatus = 1;
            if (oThongBaoQuery.ItemID > 0)
            {
                ThongBaoItem oNewsItem = oThongBaoDA.GetByIdToObject<ThongBaoItem>(oThongBaoQuery.ItemID);
                return new ResultAction(ActionState.Succeed, oNewsItem);
            }
            else
            {
                List<ThongBaoJson> oData = new List<ThongBaoJson>();

                //ThongBaoQuery oThongBaoQuery = new ThongBaoQuery(HttpContext.Current.Request);
                oData = oThongBaoDA.GetListJson(oThongBaoQuery);
                DataGridRender oGrid = new DataGridRender(oData, oThongBaoQuery.Draw,
                    oThongBaoDA.TongSoBanGhiSauKhiQuery);
                return new ResultAction(ActionState.Succeed, oGrid);
            }

        }

        /// <summary>
        /// Ham lay thong bao
        /// </summary>
        /// <returns></returns>
        public ResultAction TINNOIBAT_QUERYDATA(TinNoiBatQuery oTinNoiBatQuery)
        {
            TinNoiBatDA oTinNoiBatDA = new TinNoiBatDA(oTinNoiBatQuery.Urlsite, true);
            oTinNoiBatQuery._ModerationStatus = 1;
            if (oTinNoiBatQuery.ItemID > 0)
            {
                TinNoiBatItem oNewsItem = oTinNoiBatDA.GetByIdToObject<TinNoiBatItem>(oTinNoiBatQuery.ItemID);
                if (oNewsItem.ItemTinTuc > 0 && !string.IsNullOrEmpty(oNewsItem.UrlList))
                {
                    TinTucDA oTinTucNewDA = new TinTucDA("", oNewsItem.UrlList.Split('/').LastOrDefault(), true);
                    TinTucItem oTinTucItemNew = oTinTucNewDA.GetByIdToObject<TinTucItem>(oNewsItem.ItemTinTuc);
                    oNewsItem.ContentNews = oTinTucItemNew.ContentNews;
                }

                return new ResultAction(ActionState.Succeed, oNewsItem);
            }
            else
            {
                List<TinNoiBatJson> oData = new List<TinNoiBatJson>();

                //TinNoiBatQuery oTinNoiBatQuery = new TinNoiBatQuery(HttpContext.Current.Request);
                oData = oTinNoiBatDA.GetListJson(oTinNoiBatQuery);
                DataGridRender oGrid = new DataGridRender(oData, oTinNoiBatQuery.Draw,
                    oTinNoiBatDA.TongSoBanGhiSauKhiQuery);
                return new ResultAction(ActionState.Succeed, oGrid);
            }

        }

        /// <summary>
        /// ham lay lich ca nhan
        /// </summary>
        /// <returns></returns>
        public ResultAction LICHCANHAN_QUERYDATA(LichCaNhanQuery oLichCaNhanQuery)
        {
            LichCaNhanDA oLichCaNhanDA = new LichCaNhanDA(true);
            oLichCaNhanQuery._ModerationStatus = 1;
            if (oLichCaNhanQuery.ItemID > 0)
            {
                LichCaNhanItem oNewsItem = oLichCaNhanDA.GetByIdToObject<LichCaNhanItem>(oLichCaNhanQuery.ItemID);
                return new ResultAction(ActionState.Succeed, oNewsItem);
            }
            else
            {
                List<LichCaNhanJson> oData = new List<LichCaNhanJson>();

                //LichCaNhanQuery oLichCaNhanQuery = new LichCaNhanQuery(HttpContext.Current.Request);
                oData = oLichCaNhanDA.GetListJson(oLichCaNhanQuery);
                DataGridRender oGrid = new DataGridRender(oData, oLichCaNhanQuery.Draw,
                    oLichCaNhanDA.TongSoBanGhiSauKhiQuery);
                return new ResultAction(ActionState.Succeed, oGrid);
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="oLichDonViQuery"></param>
        /// <returns></returns>
        public ResultAction LichDonVi_QUERYDATA(LichDonViQuery oLichDonViQuery)
        {
            LichDonViDA oLichDonViDA = new LichDonViDA(true);
            oLichDonViQuery._ModerationStatus = 1;
            if (oLichDonViQuery.ItemID > 0)
            {
                LichDonViItem oNewsItem = oLichDonViDA.GetByIdToObject<LichDonViItem>(oLichDonViQuery.ItemID);
                return new ResultAction(ActionState.Succeed, oNewsItem);
            }
            else
            {
                LUserDA oLuserDA = new LUserDA(true);
                var CurentUser = oLuserDA.GetListJson(new LUserQuery() { TaiKhoanTruyCap = UserName }).FirstOrDefault();

                List<LichDonViJson> oData = new List<LichDonViJson>();
                oLichDonViQuery.fldGroup = CurentUser.Groups.ID;
                //LichDonViQuery oLichDonViQuery = new LichDonViQuery(HttpContext.Current.Request);
                oData = oLichDonViDA.GetListJson(oLichDonViQuery);
                DataGridRender oGrid = new DataGridRender(oData, oLichDonViQuery.Draw,
                    oLichDonViDA.TongSoBanGhiSauKhiQuery);
                return new ResultAction(ActionState.Succeed, oGrid);
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction LichPhong_QUERYDATA(LichPhongQuery oLichPhongQuery)
        {
            LichPhongDA oLichPhongDA = new LichPhongDA(true);
            oLichPhongQuery._ModerationStatus = 1;
            if (oLichPhongQuery.ItemID > 0)
            {
                LichPhongItem oNewsItem = oLichPhongDA.GetByIdToObject<LichPhongItem>(oLichPhongQuery.ItemID);
                return new ResultAction(ActionState.Succeed, oNewsItem);
            }
            else
            {
                LUserDA oLuserDA = new LUserDA(true);
                var CurentUser = oLuserDA.GetListJson(new LUserQuery() { TaiKhoanTruyCap = UserName }).FirstOrDefault();

                List<LichPhongJson> oData = new List<LichPhongJson>();
                oLichPhongQuery.fldGroup = CurentUser.UserPhongBan.ID;

                //LichPhongQuery oLichPhongQuery = new LichPhongQuery(HttpContext.Current.Request);
                oData = oLichPhongDA.GetListJson(oLichPhongQuery);
                DataGridRender oGrid = new DataGridRender(oData, oLichPhongQuery.Draw,
                    oLichPhongDA.TongSoBanGhiSauKhiQuery);
                return new ResultAction(ActionState.Succeed, oGrid);
            }

        }
        /// <summary>
        /// ham lay lich ca nhan
        /// </summary>
        /// <returns></returns>
        public ResultAction LICHTIMKIEM_QUERYDATA(LichCaNhanQuery oLichCaNhanQuery)
        {
            LichCaNhanDA oLichCaNhanDA = new LichCaNhanDA(true);
            oLichCaNhanQuery._ModerationStatus = 1;
            if (oLichCaNhanQuery.ItemID > 0)
            {
                LichCaNhanItem oNewsItem = oLichCaNhanDA.GetByIdToObject<LichCaNhanItem>(oLichCaNhanQuery.ItemID);
                return new ResultAction(ActionState.Succeed, oNewsItem);
            }
            else
            {
                DataGridRender oGrid = new DataGridRender();
                List<LichCaNhanJson> oData = new List<LichCaNhanJson>();
                if (oLichCaNhanQuery.SearchIndex)
                {
                    oData = oLichCaNhanDA.GetListJsonSolr(oLichCaNhanQuery);
                    oGrid = new DataGridRender(oData, oLichCaNhanQuery.Draw,
                       oLichCaNhanDA.TongSoBanGhiSauKhiQuery);
                }
                else
                {
                    //LichCaNhanQuery oLichCaNhanQuery = new LichCaNhanQuery(HttpContext.Current.Request);
                    oData = oLichCaNhanDA.GetListJson(oLichCaNhanQuery);
                    oGrid = new DataGridRender(oData, oLichCaNhanQuery.Draw,
                       oLichCaNhanDA.TongSoBanGhiSauKhiQuery);
                }
                return new ResultAction(ActionState.Succeed, oGrid);
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction LICHTONGCONGTY_QUERYDATA(LichTongCongTyQuery oLichTongCongTyQuery)
        {
            LichTongCongTyDA oLichTongCongTyDA = new LichTongCongTyDA(true);
            oLichTongCongTyQuery._ModerationStatus = 1;
            if (oLichTongCongTyQuery.ItemID > 0)
            {
                LichTongCongTyItem oNewsItem = oLichTongCongTyDA.GetByIdToObject<LichTongCongTyItem>(oLichTongCongTyQuery.ItemID);
                return new ResultAction(ActionState.Succeed, oNewsItem);
            }
            else
            {
                List<LichTongCongTyJson> oData = new List<LichTongCongTyJson>();
                //LichTongCongTyQuery oLichTongCongTyQuery = new LichTongCongTyQuery(HttpContext.Current.Request);
                oData = oLichTongCongTyDA.GetListJson(oLichTongCongTyQuery);
                DataGridRender oGrid = new DataGridRender(oData, oLichTongCongTyQuery.Draw,
                    oLichTongCongTyDA.TongSoBanGhiSauKhiQuery);
                return new ResultAction(ActionState.Succeed, oGrid);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction LichTapDoanDA_QUERYDATA(LichTapDoanQuery oLichTapDoanQuery)
        {
            LichTapDoanDA oLichTapDoanDA = new LichTapDoanDA(true);
            oLichTapDoanQuery._ModerationStatus = 1;
            if (oLichTapDoanQuery.ItemID > 0)
            {
                LichTapDoanItem oNewsItem = oLichTapDoanDA.GetByIdToObject<LichTapDoanItem>(oLichTapDoanQuery.ItemID);
                return new ResultAction(ActionState.Succeed, oNewsItem);
            }
            else
            {
                List<LichTapDoanJson> oData = new List<LichTapDoanJson>();
                //LichTapDoanQuery oLichTapDoanQuery = new LichTapDoanQuery(HttpContext.Current.Request);
                oData = oLichTapDoanDA.GetListJson(oLichTapDoanQuery);
                DataGridRender oGrid = new DataGridRender(oData, oLichTapDoanQuery.Draw,
                    oLichTapDoanDA.TongSoBanGhiSauKhiQuery);
                return new ResultAction(ActionState.Succeed, oGrid);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction LichHop_QUERYDATA(LichHopQuery oLichHopQuery)
        {
            LichHopDA oLichHopDA = new LichHopDA(true);
            oLichHopQuery._ModerationStatus = 1;
            if (oLichHopQuery.ItemID > 0)
            {
                LichHopItem oNewsItem = oLichHopDA.GetByIdToObject<LichHopItem>(oLichHopQuery.ItemID);
                return new ResultAction(ActionState.Succeed, oNewsItem);
            }
            else
            {
                List<LichHopJson> oData = new List<LichHopJson>();
                //LichHopQuery oLichHopQuery = new LichHopQuery(HttpContext.Current.Request);
                oData = oLichHopDA.GetListJson(oLichHopQuery);
                DataGridRender oGrid = new DataGridRender(oData, oLichHopQuery.Draw,
                    oLichHopDA.TongSoBanGhiSauKhiQuery);
                return new ResultAction(ActionState.Succeed, oGrid);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction LOAITAILIEU_QUERYDATA(LoaiTaiLieuQuery oLoaiTaiLieuQuery)
        {
            LoaiTaiLieuDA oLoaiTaiLieuDA = new LoaiTaiLieuDA(true);
            oLoaiTaiLieuQuery._ModerationStatus = 1;
            if (oLoaiTaiLieuQuery.ItemID > 0)
            {
                LoaiTaiLieuItem oNewsItem = oLoaiTaiLieuDA.GetByIdToObject<LoaiTaiLieuItem>(oLoaiTaiLieuQuery.ItemID);
                return new ResultAction(ActionState.Succeed, oNewsItem);
            }
            else
            {
                List<LoaiTaiLieuJson> oData = new List<LoaiTaiLieuJson>();

                //LoaiTaiLieuQuery oLoaiTaiLieuQuery = new LoaiTaiLieuQuery(HttpContext.Current.Request);
                oData = oLoaiTaiLieuDA.GetListJson(oLoaiTaiLieuQuery);
                DataGridRender oGrid = new DataGridRender(oData, oLoaiTaiLieuQuery.Draw,
                    oLoaiTaiLieuDA.TongSoBanGhiSauKhiQuery);
                return new ResultAction(ActionState.Succeed, oGrid);
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction VANBANTAILIEU_QUERYDATA(VanBanTaiLieuQuery oVanBanTaiLieuQuery)
        {
            VanBanTaiLieuDA oVanBanTaiLieuDA = new VanBanTaiLieuDA(oVanBanTaiLieuQuery.Urlsite, true);
            oVanBanTaiLieuQuery._ModerationStatus = 1;
            if (oVanBanTaiLieuQuery.ItemID > 0)
            {
                VanBanTaiLieuItem oNewsItem = oVanBanTaiLieuDA.GetByIdToObject<VanBanTaiLieuItem>(oVanBanTaiLieuQuery.ItemID);
                return new ResultAction(ActionState.Succeed, oNewsItem);
            }
            else
            {
                List<VanBanTaiLieuJson> oData = new List<VanBanTaiLieuJson>();

                //VanBanTaiLieuQuery oVanBanTaiLieuQuery = new VanBanTaiLieuQuery(HttpContext.Current.Request);
                oData = oVanBanTaiLieuDA.GetListJson(oVanBanTaiLieuQuery);
                DataGridRender oGrid = new DataGridRender(oData, oVanBanTaiLieuQuery.Draw,
                    oVanBanTaiLieuDA.TongSoBanGhiSauKhiQuery);
                return new ResultAction(ActionState.Succeed, oGrid);
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction QUYCHENOIBO_QUERYDATA(QuyCheNoiBoQuery oQuyCheNoiBoQuery)
        {
            QuyCheNoiBoDA oQuyCheNoiBoDA = new QuyCheNoiBoDA(oQuyCheNoiBoQuery.Urlsite, true);
            oQuyCheNoiBoQuery._ModerationStatus = 1;
            if (oQuyCheNoiBoQuery.ItemID > 0)
            {
                QuyCheNoiBoItem oNewsItem = oQuyCheNoiBoDA.GetByIdToObject<QuyCheNoiBoItem>(oQuyCheNoiBoQuery.ItemID);
                return new ResultAction(ActionState.Succeed, oNewsItem);
            }
            else
            {
                List<QuyCheNoiBoJson> oData = new List<QuyCheNoiBoJson>();

                //QuyCheNoiBoQuery oQuyCheNoiBoQuery = new QuyCheNoiBoQuery(HttpContext.Current.Request);
                oData = oQuyCheNoiBoDA.GetListJson(oQuyCheNoiBoQuery);
                DataGridRender oGrid = new DataGridRender(oData, oQuyCheNoiBoQuery.Draw,
                    oQuyCheNoiBoDA.TongSoBanGhiSauKhiQuery);
                return new ResultAction(ActionState.Succeed, oGrid);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction MENU_QUERYDATA(MenuQuanTriQuery oMenuQuanTriQuery)
        {
            MenuQuanTriDA oMenuQuanTriDA = new MenuQuanTriDA(true);
            oMenuQuanTriQuery._ModerationStatus = 1;
            if (oMenuQuanTriQuery.ItemID > 0)
            {
                MenuQuanTriItem oNewsItem = oMenuQuanTriDA.GetByIdToObject<MenuQuanTriItem>(oMenuQuanTriQuery.ItemID);
                return new ResultAction(ActionState.Succeed, oNewsItem);
            }
            else
            {

                //MenuQuanTriQuery oMenuQuanTriQuery = new MenuQuanTriQuery(HttpContext.Current.Request);
                var oData = oMenuQuanTriDA.GetListJson(oMenuQuanTriQuery);
                DataGridRender oGrid = new DataGridRender(oData, oMenuQuanTriQuery.Draw,
                    oMenuQuanTriDA.TongSoBanGhiSauKhiQuery);
                return new ResultAction(ActionState.Succeed, oGrid);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction DMLINHVUC_QUERYDATA(DMLinhVucQuery oDMLinhVucQuery)
        {
            DMLinhVucDA oDMLinhVucDA = new DMLinhVucDA(oDMLinhVucQuery.Urlsite, true);
            oDMLinhVucQuery._ModerationStatus = 1;
            if (oDMLinhVucQuery.ItemID > 0)
            {
                DMLinhVucItem oNewsItem = oDMLinhVucDA.GetByIdToObject<DMLinhVucItem>(oDMLinhVucQuery.ItemID);
                return new ResultAction(ActionState.Succeed, oNewsItem);
            }
            else
            {
                //DMLinhVucQuery oDMLinhVucQuery = new DMLinhVucQuery(HttpContext.Current.Request);
                var oData = oDMLinhVucDA.GetListJson(oDMLinhVucQuery);
                DataGridRender oGrid = new DataGridRender(oData, oDMLinhVucQuery.Draw,
                    oDMLinhVucDA.TongSoBanGhiSauKhiQuery);
                return new ResultAction(ActionState.Succeed, oGrid);
            }
        }

        /// <summary>
        /// HOIDAP_DELETE
        /// </summary>
        /// <param name="oHoiDapQuery"></param>
        /// <returns></returns>
        public ResultAction HOIDAP_DELETE(HoiDapQuery oHoiDapQuery)
        {
            HoiDapDA oHoiDapDA = new HoiDapDA(oHoiDapQuery.Urlsite, true);
            oHoiDapQuery._ModerationStatus = 1;
            if (oHoiDapQuery.ItemID > 0)
            {
                HoiDapItem oNewsItem = oHoiDapDA.GetByIdToObject<HoiDapItem>(oHoiDapQuery.ItemID);
                //bổ sung add log
                oHoiDapDA.DeleteObject(oHoiDapQuery.ItemID);
                return new ResultAction() { State = ActionState.Succeed, Message = "Xóa thành công câu hỏi" };
            }
            else
            {
                return new ResultAction() { State = ActionState.Error, Message = "Không xác định câu hỏi cần xóa" };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction HOIDAP_QUERYDATA(HoiDapQuery oHoiDapQuery)
        {
            HoiDapDA oHoiDapDA = new HoiDapDA(oHoiDapQuery.Urlsite, true);
            oHoiDapQuery._ModerationStatus = 1;
            if (oHoiDapQuery.ItemID > 0)
            {
                HoiDapItem oNewsItem = oHoiDapDA.GetByIdToObject<HoiDapItem>(oHoiDapQuery.ItemID);
                return new ResultAction(ActionState.Succeed, oNewsItem);
            }
            else
            {
                //HoiDapQuery oHoiDapQuery = new HoiDapQuery(HttpContext.Current.Request);
                var oData = oHoiDapDA.GetListJson(oHoiDapQuery);
                List<int> LstCauHoi = oData.Select(x => x.ID).ToList();
                if (LstCauHoi.Count > 0)
                {
                    TraLoiHoiDapDA oTraLoiHoiDapDA = new TraLoiHoiDapDA(oHoiDapQuery.Urlsite, true);
                    List<TraLoiHoiDapJson> lstCauTraLoi = oTraLoiHoiDapDA.GetListJson(new TraLoiHoiDapQuery()
                    {
                        LstIDHoiDap = LstCauHoi
                    });
                    //oData = oData.Select(x =>
                    //{
                    //    x.NoiDungTraLoi = lstCauTraLoi.FirstOrDefault(y => y.IDHoiDap.ID == x.ID)?.FAQNoiDungTraLoi;
                    //    x.TieuDeTraLoi = lstCauTraLoi.FirstOrDefault(y => y.IDHoiDap.ID == x.ID)?.Title;
                    //    x.FQNguoiTraLoi = lstCauTraLoi.FirstOrDefault(y => y.IDHoiDap.ID == x.ID)?.FQNguoiTraLoi;
                    //    return x;
                    //}).ToList();
                    foreach (HoiDapJson item in oData)
                    {
                        if (lstCauTraLoi.FindIndex(y => y.IDHoiDap.ID == item.ID) > -1)
                        {
                            item.NoiDungTraLoi = lstCauTraLoi.FirstOrDefault(y => y.IDHoiDap.ID == item.ID)?.FAQNoiDungTraLoi;
                            item.TieuDeTraLoi = lstCauTraLoi.FirstOrDefault(y => y.IDHoiDap.ID == item.ID)?.Title;
                            item.FQNguoiTraLoi = lstCauTraLoi.FirstOrDefault(y => y.IDHoiDap.ID == item.ID)?.FQNguoiTraLoi;
                        }
                    }
                }
                DataGridRender oGrid = new DataGridRender(oData, oHoiDapQuery.Draw,
                    oHoiDapDA.TongSoBanGhiSauKhiQuery);
                return new ResultAction(ActionState.Succeed, oGrid);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction HOIDAP_GUICAUHOI(HoiDapJson oHoiDapItem)
        {
            LUserDA oLuserDA = new LUserDA(true);
            var CurentUser = oLuserDA.GetListJson(new LUserQuery() { TaiKhoanTruyCap = UserName }).FirstOrDefault();
            if (CurentUser != null)
            {
                HoiDapDA oHoiDapDA = new HoiDapDA(true);
                oHoiDapItem.FQNguoiHoi = new LookupData(CurentUser.ID, CurentUser.Title);
                //stry { }
                oHoiDapDA.AddCauHoiByJson(oHoiDapItem);
                return new ResultAction()
                {
                    State = ActionState.Succeed,
                    Message = "Gửi câu hỏi thành công"
                };
            }
            else
            {
                return new ResultAction()
                {
                    State = ActionState.Error,
                    Message = "Không xác định tài khoản gửi"
                };

            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction FAQTHUONGGAP_QUERYDATA(FAQThuongGapQuery oFAQThuongGapQuery)
        {
            FAQThuongGapDA oFAQThuongGapDA = new FAQThuongGapDA(true);
            oFAQThuongGapQuery._ModerationStatus = 1;
            if (oFAQThuongGapQuery.ItemID > 0)
            {
                FAQThuongGapItem oNewsItem = oFAQThuongGapDA.GetByIdToObject<FAQThuongGapItem>(oFAQThuongGapQuery.ItemID);
                return new ResultAction(ActionState.Succeed, oNewsItem);
            }
            else
            {
                //FAQThuongGapQuery oFAQThuongGapQuery = new FAQThuongGapQuery(HttpContext.Current.Request);
                var oData = oFAQThuongGapDA.GetListJson(oFAQThuongGapQuery);
                DataGridRender oGrid = new DataGridRender(oData, oFAQThuongGapQuery.Draw,
                    oFAQThuongGapDA.TongSoBanGhiSauKhiQuery);
                return new ResultAction(ActionState.Succeed, oGrid);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction DANHMUCIMAGE_QUERYDATA(DMHinhAnhQuery oDMHinhAnhQuery)
        {
            DMHinhAnhDA oDMHinhAnhDA = new DMHinhAnhDA(oDMHinhAnhQuery.Urlsite, true);
            oDMHinhAnhQuery._ModerationStatus = 1;
            if (oDMHinhAnhQuery.ItemID > 0)
            {
                DMHinhAnhItem oNewsItem = oDMHinhAnhDA.GetByIdToObject<DMHinhAnhItem>(oDMHinhAnhQuery.ItemID);
                return new ResultAction(ActionState.Succeed, oNewsItem);
            }
            else
            {
                //DMHinhAnhQuery oDMHinhAnhQuery = new DMHinhAnhQuery(HttpContext.Current.Request);
                var oData = oDMHinhAnhDA.GetListJson(oDMHinhAnhQuery);
                DataGridRender oGrid = new DataGridRender(oData, oDMHinhAnhQuery.Draw,
                    oDMHinhAnhDA.TongSoBanGhiSauKhiQuery);
                return new ResultAction(ActionState.Succeed, oGrid);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction IMAGE_QUERYDATA(HinhAnhQuery oHinhAnhQuery)
        {
            HinhAnhDA oHinhAnhDA = new HinhAnhDA(oHinhAnhQuery.Urlsite, true);
            oHinhAnhQuery._ModerationStatus = 1;
            if (oHinhAnhQuery.ItemID > 0)
            {
                HinhAnhItem oNewsItem = oHinhAnhDA.GetByIdToObject<HinhAnhItem>(oHinhAnhQuery.ItemID);
                return new ResultAction(ActionState.Succeed, oNewsItem);
            }
            else
            {
                //HinhAnhQuery oHinhAnhQuery = new HinhAnhQuery(HttpContext.Current.Request);
                var oData = oHinhAnhDA.GetListJson(oHinhAnhQuery);
                DataGridRender oGrid = new DataGridRender(oData, oHinhAnhQuery.Draw,
                    oHinhAnhDA.TongSoBanGhiSauKhiQuery);
                return new ResultAction(ActionState.Succeed, oGrid);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction DANHMUCVIDEO_QUERYDATA(DMVideoQuery oDMVideoQuery)
        {
            DMVideoDA oDMVideoDA = new DMVideoDA(oDMVideoQuery.Urlsite, true);
            oDMVideoQuery._ModerationStatus = 1;
            if (oDMVideoQuery.ItemID > 0)
            {
                DMVideoItem oNewsItem = oDMVideoDA.GetByIdToObject<DMVideoItem>(oDMVideoQuery.ItemID);
                return new ResultAction(ActionState.Succeed, oNewsItem);
            }
            else
            {
                //DMVideoQuery oDMVideoQuery = new DMVideoQuery(HttpContext.Current.Request);
                var oData = oDMVideoDA.GetListJson(oDMVideoQuery);
                DataGridRender oGrid = new DataGridRender(oData, oDMVideoQuery.Draw,
                    oDMVideoDA.TongSoBanGhiSauKhiQuery);
                return new ResultAction(ActionState.Succeed, oGrid);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>


        public ResultAction VIDEO_QUERYDATA(VideoQuery oVideoQuery)
        {
            VideoDA oVideoDA = new VideoDA(oVideoQuery.Urlsite, true);
            oVideoQuery._ModerationStatus = 1;
            if (oVideoQuery.ItemID > 0)
            {
                VideoItem oNewsItem = oVideoDA.GetByIdToObject<VideoItem>(oVideoQuery.ItemID);
                return new ResultAction(ActionState.Succeed, oNewsItem);
            }
            else
            {
                //VideoQuery oVideoQuery = new VideoQuery(HttpContext.Current.Request);
                var oData = oVideoDA.GetListJson(oVideoQuery);
                DataGridRender oGrid = new DataGridRender(oData, oVideoQuery.Draw,
                    oVideoDA.TongSoBanGhiSauKhiQuery);
                return new ResultAction(ActionState.Succeed, oGrid);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction DANHMUCQUYCHE_QUERYDATA(DanhMucQuyCheQuery oDanhMucQuyCheQuery)
        {
            DanhMucQuyCheDA oDanhMucQuyCheDA = new DanhMucQuyCheDA(oDanhMucQuyCheQuery.Urlsite, true);
            oDanhMucQuyCheQuery._ModerationStatus = 1;
            if (oDanhMucQuyCheQuery.ItemID > 0)
            {
                DanhMucQuyCheItem oNewsItem = oDanhMucQuyCheDA.GetByIdToObject<DanhMucQuyCheItem>(oDanhMucQuyCheQuery.ItemID);
                return new ResultAction(ActionState.Succeed, oNewsItem);
            }
            else
            {
                //DanhMucQuyCheQuery oDanhMucQuyCheQuery = new DanhMucQuyCheQuery(HttpContext.Current.Request);
                var oData = oDanhMucQuyCheDA.GetListJson(oDanhMucQuyCheQuery);
                DataGridRender oGrid = new DataGridRender(oData, oDanhMucQuyCheQuery.Draw,
                    oDanhMucQuyCheDA.TongSoBanGhiSauKhiQuery);
                return new ResultAction(ActionState.Succeed, oGrid);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction DANHMUCCHUCVU_QUERYDATA(DanhMucChucVuQuery oDanhMucChucVuQuery)
        {
            DanhMucChucVuDA oDanhMucChucVuDA = new DanhMucChucVuDA(true);
            oDanhMucChucVuQuery._ModerationStatus = 1;
            if (oDanhMucChucVuQuery.ItemID > 0)
            {
                DanhMucChucVuItem oNewsItem = oDanhMucChucVuDA.GetByIdToObject<DanhMucChucVuItem>(oDanhMucChucVuQuery.ItemID);
                return new ResultAction(ActionState.Succeed, oNewsItem);
            }
            else
            {
                //DanhMucChucVuQuery oDanhMucChucVuQuery = new DanhMucChucVuQuery(HttpContext.Current.Request);
                var oData = oDanhMucChucVuDA.GetListJson(oDanhMucChucVuQuery);
                DataGridRender oGrid = new DataGridRender(oData, oDanhMucChucVuQuery.Draw,
                    oDanhMucChucVuDA.TongSoBanGhiSauKhiQuery);
                return new ResultAction(ActionState.Succeed, oGrid);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction DANHBA_QUERYDATA(DanhBaQuery oDanhBaQuery)
        {
            //DanhBaDA oDanhBaDA = new DanhBaDA(true);
            //oDanhBaQuery._ModerationStatus = 1;
            //if (oDanhBaQuery.ItemID > 0)
            //{
            //    DanhBaItem oNewsItem = oDanhBaDA.GetByIdToObject<DanhBaItem>(oDanhBaQuery.ItemID);
            //    return new ResultAction(ActionState.Succeed, oNewsItem);
            //}
            //else
            //{
            //    //DanhBaQuery oDanhBaQuery = new DanhBaQuery(HttpContext.Current.Request);
            //    var oData = oDanhBaDA.GetListJson(oDanhBaQuery);
            //    DataGridRender oGrid = new DataGridRender(oData, oDanhBaQuery.Draw,
            //    oDanhBaDA.TongSoBanGhiSauKhiQuery);
            //    return new ResultAction(ActionState.Succeed, oGrid);

            //}

            //DanhBaDA oDanhBaDA = new DanhBaDA();
            //DanhBaQuery oDanhBaQuery = new DanhBaQuery(HttpContext.Current.Request);
            oDanhBaQuery._ModerationStatus = 1;
            oDanhBaQuery.FieldOrder = "DMSTT";
            oDanhBaQuery.Ascending = true;

            if (!string.IsNullOrEmpty(oDanhBaQuery.Urlsite))
            {
                LGroupDA oLGroupDA = new LGroupDA(true);
                LGroupJson lGroupJson = oLGroupDA.GetListJson(new LGroupQuery() { UrlSite = oDanhBaQuery.Urlsite }).FirstOrDefault();
                if (lGroupJson != null)
                {
                    if (!string.IsNullOrEmpty(lGroupJson.orgCode))
                        oDanhBaQuery.MaDonVi = Convert.ToInt32(lGroupJson.OldID);
                    else
                    {
                        LGroupItem oLGroupItem = oLGroupDA.GetByIdToObject<LGroupItem>(lGroupJson.GroupParent.ID);
                        oDanhBaQuery.PhongBan = Convert.ToInt64(lGroupJson.OldID);
                        oDanhBaQuery.MaDonVi = Convert.ToInt32(oLGroupItem.OldID);
                    }
                }

            }
            else
            {
                oDanhBaQuery.MaDonVi = 276;
            }

            //var oData = oDanhBaDA.GetListJson(oDanhBaQuery);
            //DataGridRender oGrid = new DataGridRender(oData, oDanhBaQuery.Draw,
            //   oDanhBaDA.TongSoBanGhiSauKhiQuery);

            //xử lý lại chỗ này thôi.
            LconfigDA oLconfigDA = new LconfigDA(true);
            string APIEVNDanhBa = oLconfigDA.GetListJson(new LconfigQuery() { ConfigType = "APIEVNDanhBa" }).FirstOrDefault()?.ConfigValue;
            if (string.IsNullOrEmpty(APIEVNDanhBa))
                APIEVNDanhBa = "https://portal.evnhanoi.vn/apigateway";
            //lấy dữ liệu theo đơn vị.
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes("EVNHANOI:Evnhanoi@123");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.PostAsJsonAsync(APIEVNDanhBa + "/HRMS_DanhBa_NhanVien", new
            {
                MaDonVi = oDanhBaQuery.MaDonVi,
                PhongBan = oDanhBaQuery.PhongBan,
                TuKhoa = ""
            }).Result;

            //HttpResponseMessage response = client.PostAsJsonAsync(APIEVNDanhBa + "/HRMS_DanhBa_NhanVien").Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var root = JsonConvert.DeserializeObject<DanhBaRespon>(responseBody);
            List<DanhBaJson> lstDataReturn = new List<DanhBaJson>();
            DataGridRender oGrid = new DataGridRender(root.data, oDanhBaQuery.Draw,
                0);
            return new ResultAction(ActionState.Succeed, oGrid);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction NOTIFI_QUERYDATA(LNotificationQuery oLNotificationQuery)
        {
            LUserDA oLuserDA = new LUserDA(true);
            var CurentUser = oLuserDA.GetListJson(new LUserQuery() { TaiKhoanTruyCap = UserName }).FirstOrDefault();
            LNotificationDA oLNotificationDA = new LNotificationDA(true);
            oLNotificationQuery._ModerationStatus = 1;
            oLNotificationQuery.LNotiNguoiNhan_Id = CurentUser.ID;
            if (oLNotificationQuery.ItemID > 0)
            {
                LNotificationItem oNewsItem = oLNotificationDA.GetByIdToObject<LNotificationItem>(oLNotificationQuery.ItemID);
                return new ResultAction(ActionState.Succeed, oNewsItem);
            }
            else
            {
                //LNotificationQuery oLNotificationQuery = new LNotificationQuery(HttpContext.Current.Request);
                var oData = oLNotificationDA.GetListJson(oLNotificationQuery);
                DataGridRender oGrid = new DataGridRender(oData, oLNotificationQuery.Draw,
                oLNotificationDA.TongSoBanGhiSauKhiQuery);
                return new ResultAction(ActionState.Succeed, oGrid);

            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="oLNotificationQuery"></param>
        /// <returns></returns>
        public ResultAction NOTIFI_UPDATEDAXEM(LNotificationQuery oLNotificationQuery)
        {
            if (oLNotificationQuery.ItemID > 0)
            {
                LUserDA oLuserDA = new LUserDA(true);
                var CurentUser = oLuserDA.GetListJson(new LUserQuery() { TaiKhoanTruyCap = UserName }).FirstOrDefault();
                LNotificationDA oLNotificationDA = new LNotificationDA(true);
                LNotificationItem oNewsItem = oLNotificationDA.GetByIdToObject<LNotificationItem>(oLNotificationQuery.ItemID);
                oNewsItem.LNotiDaXem = string.IsNullOrEmpty(oNewsItem.LNotiDaXem) ? "," : oNewsItem.LNotiDaXem;
                oNewsItem.LNotiDaXem += (CurentUser.ID + ",");
                oLNotificationDA.UpdateOneField(oLNotificationQuery.ItemID, "LNotiDaXem", oNewsItem.LNotiDaXem);
            }
            return new ResultAction()
            {
                State = ActionState.Succeed,
                Message = "Đánh dấu đã xem thành công"
            };
        }

        /// <summary>
        /// Get thoong tin user.
        /// </summary>
        /// <param name="oLuserQuery"></param>
        /// <returns></returns>
        public ResultAction THONGTIN_USER(LUserQuery oLuserQuery)
        {
            LUserDA oLuserDA = new LUserDA(true);
            var CurentUser = oLuserDA.GetListJson(new LUserQuery() { TaiKhoanTruyCap = UserName }).FirstOrDefault();
            if (CurentUser != null)
            {
                LGroupDA lGroupDA = new LGroupDA(true);
                LGroupItem lGroupItem = lGroupDA.GetByIdToObject<LGroupItem>(CurentUser.Groups.ID);
                CurentUser.UrlSite = lGroupItem.UrlSite;
            }
            return new ResultAction(ActionState.Succeed, CurentUser);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResultAction KSBOCHUDE_QUERYDATA(KSBoChuDeQuery oKSBoChuDeQuery)
        {
            KSBoChuDeDA oKSBoChuDeDA = new KSBoChuDeDA(true);
            List<KSBoCauHoiJson> lstBoCauHoi = new List<KSBoCauHoiJson>();
            oKSBoChuDeQuery._ModerationStatus = 1;
            if (oKSBoChuDeQuery.ItemID > 0)
            {
                KSBoChuDeItem oKSBoChuDeItem = oKSBoChuDeDA.GetByIdToObject<KSBoChuDeItem>(oKSBoChuDeQuery.ItemID);
                //lst câu hỏi và câu trả lời cho từng bộ câu hỏi.
                KSBoCauHoiDA oKSBoCauHoiDA = new KSBoCauHoiDA(true);
                KSCauTraLoiDA objCauTraLoiDA = new KSCauTraLoiDA(true);
                if (oKSBoChuDeItem.ID > 0)
                {
                    lstBoCauHoi = oKSBoCauHoiDA.GetListJson(new KSBoCauHoiQuery() { IdChuDe = oKSBoChuDeItem.ID });
                    foreach (KSBoCauHoiJson oKSBoCauHoiJson in lstBoCauHoi)
                    {
                        oKSBoCauHoiJson.lstKSCauTraLoiJson = objCauTraLoiDA.GetListJson(new KSCauTraLoiQuery() { IDCauHoi = oKSBoCauHoiJson.ID });
                    }
                }
                return new ResultAction(ActionState.Succeed, new { KSBoChuDeItem = oKSBoChuDeItem, BoCauHOI = lstBoCauHoi });
            }
            else
            {
                //KSBoChuDeQuery oKSBoChuDeQuery = new KSBoChuDeQuery(HttpContext.Current.Request);
                var oData = oKSBoChuDeDA.GetListJson(oKSBoChuDeQuery);
                DataGridRender oGrid = new DataGridRender(oData, oKSBoChuDeQuery.Draw,
                oKSBoChuDeDA.TongSoBanGhiSauKhiQuery);
                return new ResultAction(ActionState.Succeed, oGrid);

            }
        }
        /// <summary>
        /// ksbo chu de.
        /// </summary>
        /// <param name="oKQKhaoSat"></param>
        /// <returns></returns>
        public ResultAction GuiKhaoSat(KQKhaoSat oKQKhaoSat)
        {
            if (oKQKhaoSat.IDBoChuDe > 0)
            {
                KSBoChuDeDA oKSBoChuDeDA = new KSBoChuDeDA(true);
                KSCauTraLoiItem oKSCauTraLoiItem = new KSCauTraLoiItem();
                KSCauTraLoiDA oKSCauTraLoiDA = new KSCauTraLoiDA(true);
                KSBoChuDeItem oKSBoChuDeItem = oKSBoChuDeDA.GetByIdToObject<KSBoChuDeItem>(oKQKhaoSat.IDBoChuDe);
                if (oKSBoChuDeItem.UCThamGia.Contains($",{oKQKhaoSat.CurentUserID},"))
                {
                    return new ResultAction()
                    {
                        State = ActionState.Error,
                        Message = "Bạn đã bình chọn trước đó, không thể bình chọn lại"
                    };
                }
                else
                {
                    if (string.IsNullOrEmpty(oKSBoChuDeItem.UCThamGia)) oKSBoChuDeItem.UCThamGia = ",";
                    oKSBoChuDeDA.SystemUpdateOneField(Convert.ToInt32(oKQKhaoSat.IDBoChuDe), "UCThamGia", oKSBoChuDeItem.UCThamGia + "," + oKQKhaoSat.CurentUserID + ",");
                    var lstidcautraloi = oKQKhaoSat.LstCauHoi_TraLoi;
                    if (oKQKhaoSat.LstCauHoi_TraLoi.Count > 0)
                    {
                        for (int i = 0; i < oKQKhaoSat.LstCauHoi_TraLoi.Count; i++)
                        {
                            int idcautraloi = Convert.ToInt32(lstidcautraloi[i]);
                            oKSCauTraLoiItem = oKSCauTraLoiDA.GetByIdToObject<KSCauTraLoiItem>(idcautraloi);
                            oKSCauTraLoiItem.SoLuotBinhChon += 1;
                            oKSCauTraLoiDA.UpdateObject<KSCauTraLoiItem>(oKSCauTraLoiItem);
                        }
                    }
                    return new ResultAction()
                    {
                        State = ActionState.Succeed,
                        Message = "Gửi khảo sát thành công"
                    };
                }
            }
            else
            {
                return new ResultAction()
                {
                    State = ActionState.Succeed,
                    Message = "Vui lòng lựa chọn chủ đề gửi khảo sát"
                };
            }
        }

        /// <summary>
        /// Tìm kiếm ful.
        /// </summary>
        /// <returns></returns>
        public DataGridRender TimKiemFUll_QUERYDATA(QuerySearch oQuerySearch)
        {
            TimKiemDA oDMVideoDA = new TimKiemDA();
            if (oQuerySearch == null) oQuerySearch = new QuerySearch();
            //QuerySearch oQuerySearch = new QuerySearch(HttpContext.Current.Request);
            oQuerySearch._ModerationStatus = 1;
            var oData = oDMVideoDA.GetListJsonSolr(oQuerySearch);
            DataGridRender oGrid = new DataGridRender(oData, oQuerySearch.Draw,
                oDMVideoDA.TongSoBanGhiSauKhiQuery);
            return oGrid;
        }
        #endregion
        /// <summary>
        /// 
        /// 
        /// </summary>
        /// <param name="LstFile"></param>
        /// <returns></returns>
        /// 

        [System.Web.Http.HttpPost]
        public IEnumerable<string> GetFilePublic(string[] LstFile)
        {
            var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);

            List<string> lstFileReturn = new List<string>();
            foreach (string urlfile in LstFile)
            {
                if (urlfile.StartsWith("http"))
                {
                    SPSecurity.RunWithElevatedPrivileges(delegate ()
                    {
                        //string urlfile = "http://server/site/documentlibrary/testdoc.docx";
                        using (SPSite site = new SPSite(urlfile))
                        {
                            using (SPWeb web = site.OpenWeb())
                            {
                                SPFile file = web.GetFile(urlfile);
                                if (file.Exists)
                                {
                                    string filename = Guid.NewGuid().ToString();
                                    string filePath = "/Uploads/ajaxUpload/";
                                    string fileout = HttpContext.Current.Server.MapPath(filePath + filename + System.IO.Path.GetExtension(urlfile));
                                    File.WriteAllBytes(fileout, file.OpenBinary());
                                    lstFileReturn.Add(baseUrl + filePath + filename + System.IO.Path.GetExtension(urlfile));
                                }
                            }
                        }
                    });
                }
                else
                {

                }
            }
            return lstFileReturn;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA_SINHNHATSAPDIENRA()
        {
            LUserDA oLUserDA = new LUserDA(true);
            LUserQuery oLUserQuery = new LUserQuery(HttpContext.Current.Request);
            oLUserQuery.SapDen_SoNgayBaoSinhNhat = DateTime.Now;
            var oData = oLUserDA.GetListJson(oLUserQuery);
            DataGridRender oGrid = new DataGridRender(oData, oLUserQuery.Draw,
                oLUserDA.TongSoBanGhiSauKhiQuery);
            return oGrid;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataGridRender QUERYDATA_SINHNHAT_NOW()
        {
            LUserDA oLUserDA = new LUserDA(true);
            LUserQuery oLUserQuery = new LUserQuery(HttpContext.Current.Request);
            oLUserQuery.DangDienRa_SoNgayBaoSinhNhat = DateTime.Now;
            //oLUserQuery.FieldOrder = "CountSinhNhat";
            //oLUserQuery.Ascending = true;
            var oData = oLUserDA.GetListJson(oLUserQuery);
            DataGridRender oGrid = new DataGridRender(oData, oLUserQuery.Draw,
                oLUserDA.TongSoBanGhiSauKhiQuery);
            return oGrid;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataGridRender GETDATA_SINHNHAT_CBNV(QuerySearch oQuerySearch)
        {
            //DanhBaDA oDanhBaDA = new DanhBaDA();
            //DanhBaQuery oDanhBaQuery = new DanhBaQuery(HttpContext.Current.Request);
            //DateTime ngaydautuan = LayNgayDauVaKhoiTaoGiaTriTuan_Nam();
            //oDanhBaQuery.TuNgay_SinhNhat = ngaydautuan;
            //oDanhBaQuery.DenNgay_SinhNhat = ngaydautuan.AddDays(6);
            //var oData = oDanhBaDA.GetListJson(oDanhBaQuery);

            //List<DanhBaDongBo> lstBirthday = root.data.Where(x => (x.ngaysinh.Day - x.ngaysinh.Month) >= (DateTime.Now.Day - DateTime.Now.Month)).OrderBy(x => x.ngaysinh.Month).Skip(0).Take(100).ToList();
            //DataGridRender oGrid = new DataGridRender(oData, oDanhBaQuery.Draw, oDanhBaDA.TongSoBanGhiSauKhiQuery);
            LconfigDA oLconfigDA = new LconfigDA(true);
            string APIEVNHanoiDanhBa = oLconfigDA.GetListJson(new LconfigQuery() { ConfigType = "APIEVNHanoiDanhBa" }).FirstOrDefault()?.ConfigValue;
            if (string.IsNullOrEmpty(APIEVNHanoiDanhBa))
                APIEVNHanoiDanhBa = "https://portal.evnhanoi.vn/apigateway";
            //IP nội bộ
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes("EVNHANOI:Evnhanoi@123");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiDanhBa + "/HRMS_ChucMungSinhNhat_LanhDao").Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            SinhNhatLanhDaoResponse oSinhNhatLanhDaoResponse = JsonConvert.DeserializeObject<SinhNhatLanhDaoResponse>(responseBody);
            LGroupDA oLGroupDA = new LGroupDA(true);

            //string APIEVNHanoiNew = $"http://10.9.125.85:8080/HRMS_DanhBa_NhanVien/0/0";
            string PhongBan = "";
            string id_donvi = "276"; ;
            if (!string.IsNullOrEmpty(oQuerySearch.Urlsite))
            {
                //LGroupDA lGroupDA = new LGroupDA();
                LGroupJson lGroupJson = oLGroupDA.GetListJson(new LGroupQuery() { UrlSite = oQuerySearch.Urlsite }).FirstOrDefault();
                if (lGroupJson != null)
                {
                    if (!string.IsNullOrEmpty(lGroupJson.orgCode))
                        id_donvi = lGroupJson.OldID;
                    else
                    {
                        LGroupItem oLGroupItem = oLGroupDA.GetByIdToObject<LGroupItem>(lGroupJson.GroupParent.ID);
                        PhongBan = lGroupJson.OldID;
                        id_donvi = oLGroupItem.OldID;
                    }
                }

            }
            else
            {
                id_donvi = "276";
            }

            #region lấy thông tin request


            #endregion




            HttpResponseMessage responseALL = client.PostAsJsonAsync(APIEVNHanoiDanhBa + "/HRMS_DanhBa_NhanVien", new
            {
                MaDonVi = id_donvi,
                PhongBan = PhongBan,
                TuKhoa = ""
            }).Result;

            response.EnsureSuccessStatusCode();
            string responseBodyALL = responseALL.Content.ReadAsStringAsync().Result;
            var root = JsonConvert.DeserializeObject<DanhBaRespon>(responseBodyALL);
            List<DanhBaDongBo> lstBirthday = new List<DanhBaDongBo>();
            //SoNgayLay
            int SoNgayLay;
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["SoNgayLay"]))
                SoNgayLay = Convert.ToInt32(HttpContext.Current.Request["SoNgayLay"]);
            else
                SoNgayLay = 30;
            lstBirthday = root.data.Where(x => x.SinhNhatNamNay >= DateTime.Now.Date && x.SinhNhatNamNay < DateTime.Now.Date.AddDays(SoNgayLay)).ToList();
            lstBirthday = lstBirthday.Where(x => oSinhNhatLanhDaoResponse.data.Count(y => y.ID_LLNS == x.NS_ID) == 0).ToList();
            lstBirthday = lstBirthday.OrderBy(x => x.SinhNhatNamNay).ThenBy(x => x.STT).ToList();
            //lstBirthday = lstBirthday.Where(x => x.ngaysinh.Day >= DateTime.Now.Day).Skip(0).Take(length).ToList();
            if (HttpContext.Current.Request.Url.Host.Contains("evnhanoi.vn"))
            {
                foreach (var item in lstBirthday)
                {


                    //get ảnh xem sao.
                    try
                    {

                        HttpResponseMessage responseUser = client.PostAsJsonAsync("http://10.9.125.97:7072/StaffView/getListAnh", new
                        {
                            nsId = item.NS_ID.ToString()
                        }).Result;
                        string responseUserImage = responseUser.Content.ReadAsStringAsync().Result;
                        JObject oResponseUser = JObject.Parse(responseUserImage);
                        string status = oResponseUser["status"].Value<string>();
                        if (status == "SUCCESS")
                        {
                            //List<AvatarUser> lstAvatarUser = oResponseUser["data"].Value<List<AvatarUser>>();
                            JArray jsonArray = (JArray)oResponseUser["data"];
                            List<AvatarUser> lstAvatarUser = jsonArray.ToObject<List<AvatarUser>>();
                            if (lstAvatarUser.Count > 0)
                            {
                                string DataAvart = lstAvatarUser[0].anhNs;
                                if (!string.IsNullOrEmpty(DataAvart))
                                {
                                    string TypeFile = clsFucUtils.GetFileExtension(DataAvart);
                                    string fileServer = item.NS_ID.ToString() + "." + TypeFile;

                                    string pathServer = string.Format(@"\{0}\{1}", @"Uploads\ajaxUpload\", fileServer);
                                    string pathFile = string.Format(@"{0}\{1}", HttpContext.Current.Server.MapPath(@"\"), pathServer);
                                    //write file ra thư mục upload.
                                    byte[] bytes = System.Convert.FromBase64String(DataAvart);
                                    File.WriteAllBytes(pathFile, bytes);
                                    item.ImageAvatar = pathServer;
                                }
                            }
                        }
                        //http://10.9.125.97:7072/StaffView/getListAnh
                        //{
                        //"nsId":"277000000000164"
                        //}

                    }
                    catch (Exception ex)
                    {
                        //throw new System.Exception(ex.Message + "\r\n" + responseUserImage);
                    }
                }
            }
            DataGridRender oGrid = new DataGridRender(lstBirthday, 2, lstBirthday.Count);
            return oGrid;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public DataGridRender GETDATA_SINHNHAT()
        {
            BirthdayQuery oLUserQuery = new BirthdayQuery(HttpContext.Current.Request);
            int length;
            if (Convert.ToInt32(HttpContext.Current.Request["length"]) > 0)
                length = Convert.ToInt32(HttpContext.Current.Request["length"]);
            else
                length = 10;
            int SoNgayLay;
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["SoNgayLay"]))
                SoNgayLay = Convert.ToInt32(HttpContext.Current.Request["SoNgayLay"]);
            else
                SoNgayLay = 30;
            LconfigDA oLconfigDA = new LconfigDA(true);
            string APIEVNHanoiDanhBa = oLconfigDA.GetListJson(new LconfigQuery() { ConfigType = "APIEVNHanoiDanhBa" }).FirstOrDefault()?.ConfigValue;
            if (string.IsNullOrEmpty(APIEVNHanoiDanhBa))
                APIEVNHanoiDanhBa = "https://portal.evnhanoi.vn/apigateway";

            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes("EVNHANOI:Evnhanoi@123");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
            HttpResponseMessage response = client.GetAsync(APIEVNHanoiDanhBa + "/HRMS_ChucMungSinhNhat_LanhDao").Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            SinhNhatLanhDaoResponse oSinhNhatLanhDaoResponse = JsonConvert.DeserializeObject<SinhNhatLanhDaoResponse>(responseBody);
            //get ra các người dùng có sinh nhật trong khoảng 1 tuần tới.
            List<SinhNhatLanhDao> lstSinhNhatLanhDao = oSinhNhatLanhDaoResponse.data.Where(x => (x.BIRTHDAY > DateTime.Now.Date.AddDays(-1)) && (x.BIRTHDAY < DateTime.Now.Date.AddDays(SoNgayLay))).Skip(0).ToList();
            lstSinhNhatLanhDao = lstSinhNhatLanhDao.OrderBy(x => x.BIRTHDAY).ThenBy(y => y.STT).ToList();
            HttpClient clientAnh = new HttpClient();
            string responseUserImage = "";
            if (HttpContext.Current.Request.Url.Host.Contains("evnhanoi.vn"))
            {
                foreach (var item in lstSinhNhatLanhDao)
                {
                    //get ảnh xem sao.
                    try
                    {

                        HttpResponseMessage responseUser = client.PostAsJsonAsync("http://10.9.125.97:7072/StaffView/getListAnh", new
                        {
                            nsId = item.ID_LLNS.ToString()
                        }).Result;
                        responseUserImage = responseUser.Content.ReadAsStringAsync().Result;
                        JObject oResponseUser = JObject.Parse(responseUserImage);
                        string status = oResponseUser["status"].Value<string>();
                        if (status == "SUCCESS")
                        {
                            //List<AvatarUser> lstAvatarUser = oResponseUser["data"].Value<List<AvatarUser>>();
                            JArray jsonArray = (JArray)oResponseUser["data"];
                            List<AvatarUser> lstAvatarUser = jsonArray.ToObject<List<AvatarUser>>();
                            if (lstAvatarUser.Count > 0)
                            {
                                string DataAvart = lstAvatarUser[0].anhNs;
                                if (!string.IsNullOrEmpty(DataAvart))
                                {
                                    string TypeFile = clsFucUtils.GetFileExtension(DataAvart);
                                    string fileServer = item.ID_LLNS.ToString() + "." + TypeFile;

                                    string pathServer = string.Format(@"\{0}\{1}", @"Uploads\ajaxUpload\", fileServer);
                                    string pathFile = string.Format(@"{0}\{1}", HttpContext.Current.Server.MapPath(@"\"), pathServer);
                                    //write file ra thư mục upload.
                                    byte[] bytes = System.Convert.FromBase64String(DataAvart);
                                    File.WriteAllBytes(pathFile, bytes);
                                    item.ImageAvatar = pathServer;
                                }
                            }
                        }
                        //http://10.9.125.97:7072/StaffView/getListAnh
                        //{
                        //"nsId":"277000000000164"
                        //}

                    }
                    catch (Exception ex)
                    {
                        throw new System.Exception(ex.Message + "\r\n" + responseUserImage);
                    }
                }
            }

            //List<DanhBaDongBo> lstBirthday = new List<DanhBaDongBo>();


            //BirthdayDA oBirthdayDA = new BirthdayDA();
            //BirthdayQuery oLUserQuery = new BirthdayQuery(HttpContext.Current.Request);
            //oLUserQuery.isBirthday = true;
            //oLUserQuery.FieldOrder = "BIRTHDAY";
            //oLUserQuery.Ascending = true;
            //DateTime ngaydautuan = LayNgayDauVaKhoiTaoGiaTriTuan_Nam();
            //oLUserQuery.TuNgay = ngaydautuan;
            //oLUserQuery.DenNgay = ngaydautuan.AddDays(6);
            //var oData = oBirthdayDA.GetListJson(oLUserQuery);

            //get sinh nhật lđ.


            DataGridRender oGrid = new DataGridRender(lstSinhNhatLanhDao, oLUserQuery.Draw, lstSinhNhatLanhDao.Count);
            return oGrid;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oLuserQuery"></param>
        /// <returns></returns>
        //Update thoong tin usser nhes.
        public ResultAction UpdateAnhDaiDien(LUserQuery oLuserQuery)
        {
            ResultAction oResultAction = new ResultAction();
            HttpClient client = new HttpClient();
            LUserDA lUserDA = new LUserDA();
            if (oLuserQuery.ItemID > 0 && !string.IsNullOrEmpty(oLuserQuery.HRMSID))
            {
                HttpResponseMessage responseUser = client.PostAsJsonAsync("http://10.9.125.97:7072/StaffView/getListAnh", new
                {
                    nsId = oLuserQuery.HRMSID
                }).Result;
                string responseUserImage = responseUser.Content.ReadAsStringAsync().Result;
                string fileServer = "";
                string pathFile = "";
                try
                {
                    JObject oResponseUser = JObject.Parse(responseUserImage);
                    string status = oResponseUser["status"].Value<string>();
                    if (status == "SUCCESS")
                    {
                        JArray jsonArray = (JArray)oResponseUser["data"];
                        List<AvatarUser> lstAvatarUser = jsonArray.ToObject<List<AvatarUser>>();
                        if (lstAvatarUser.Count > 0)
                        {
                            string DataAvart = lstAvatarUser[0].anhNs;
                            if (!string.IsNullOrEmpty(DataAvart))
                            {
                                string TypeFile = clsFucUtils.GetFileExtension(DataAvart);
                                fileServer = oLuserQuery.HRMSID.ToString() + "_" + DateTime.Now.Millisecond + "." + TypeFile;

                                string pathServer = string.Format(@"\{0}\{1}", @"Uploads\ajaxUpload", fileServer);
                                pathFile = string.Format(@"{0}{1}", HttpContext.Current.Server.MapPath(@"\"), pathServer);
                                //write file ra thư mục upload.
                                byte[] bytes = System.Convert.FromBase64String(DataAvart);
                                File.WriteAllBytes(pathFile, bytes);
                                try
                                {
                                    using (var clientftp = new WebClient())
                                    {
                                        clientftp.Credentials = new NetworkCredential("anonymous", "password");
                                        //ftp://xxx.xxx.xx.xx:21//path/filename
                                        clientftp.UploadFile("ftp://10.9.125.85:21/images/Avatar/" + fileServer, WebRequestMethods.Ftp.UploadFile, pathFile);
                                        //update user vào trường file này.
                                        lUserDA.SystemUpdateOneField(oLuserQuery.ItemID, "AnhDaiDien", "/ckfinder/userfiles/images/Avatar/" + fileServer);
                                    }
                                }
                                catch (WebException e)
                                {
                                    String statusUpload = ((FtpWebResponse)e.Response).StatusDescription;
                                    oResultAction.Message += statusUpload;
                                }
                                //item.ImageAvatar = pathServer;
                            }
                        }
                    }
                    oResultAction.Message += "/ckfinder/userfiles/images/Avatar/" + fileServer;
                }
                catch (Exception ex)
                {
                    oResultAction = new ResultAction()
                    {
                        State = ActionState.Error,
                        Message = ex.StackTrace + "." + ex.Message + "\n\n" + pathFile
                    };
                }
            }
            return oResultAction;
        }
        /// <summary>
        /// update solr theo id
        /// </summary>
        /// <param name="NewsQuery"></param>
        /// <returns></returns>
        public ResultAction UpdateItemSolrData(NewsQuery NewsQuery)
        {
            ResultAction oResultAction = new ResultAction();
            if (NewsQuery.TypeDanhMuc)
            {
                var solrWorkerStatic = ServiceLocator.Current.GetInstance<ISolrOperations<DanhMucSolr>>();

                string urlList = NewsQuery.UrlList;
                if (!string.IsNullOrEmpty(urlList) && NewsQuery.ItemID > 0)
                {
                    var fullPath = "http://localhost:6003" + urlList;
                    if (SPContext.Current != null)
                        fullPath = SPContext.Current.Site.Url + urlList;
                    SPSecurity.RunWithElevatedPrivileges(delegate ()
                    {
                        using (SPSite cSite = new SPSite(fullPath))
                        {
                            using (SPWeb cWeb = cSite.OpenWeb())
                            {
                                //Su dung ham get GetList 
                                SPList ListConvert = cWeb.GetList(urlList);
                                SPListItem item = ListConvert.GetItemById(NewsQuery.ItemID);
                                DanhMucSolr oDMSolr = new DanhMucSolr(item);
                                solrWorkerStatic.Add(oDMSolr);
                                solrWorkerStatic.Commit();
                                //Console.WriteLine(oDMSolr.Title);
                                oResultAction = new ResultAction()
                                {
                                    Message = "Ok"
                                };
                                //foreach (SPListItem item in ListConvert.Items)
                                //{
                                //    DataSolr oDMSolr = new DataSolr(item);
                                //    solrWorkerStatic.Add(oDMSolr);
                                //    solrWorkerStatic.Commit();
                                //    //Console.WriteLine(oDMSolr.Title);
                                //    oResultAction = new ResultAction()
                                //    {
                                //        Message = "Ok"
                                //    };
                                //}

                            };
                        };
                    });
                }
            }
            else
            {
                var solrWorkerStatic = ServiceLocator.Current.GetInstance<ISolrOperations<DataSolr>>();

                string urlList = NewsQuery.UrlList;
                if (!string.IsNullOrEmpty(urlList) && NewsQuery.ItemID > 0)
                {
                    var fullPath = "http://localhost:6003" + urlList;
                    if (SPContext.Current != null)
                        fullPath = SPContext.Current.Site.Url + urlList;
                    SPSecurity.RunWithElevatedPrivileges(delegate ()
                    {
                        using (SPSite cSite = new SPSite(fullPath))
                        {
                            using (SPWeb cWeb = cSite.OpenWeb())
                            {
                            //Su dung ham get GetList 
                            SPList ListConvert = cWeb.GetList(urlList);
                                SPListItem item = ListConvert.GetItemById(NewsQuery.ItemID);
                                DataSolr oDMSolr = new DataSolr(item);
                                solrWorkerStatic.Add(oDMSolr);
                                solrWorkerStatic.Commit();
                            //Console.WriteLine(oDMSolr.Title);
                            oResultAction = new ResultAction()
                                {
                                    Message = "Ok"
                                };
                            //foreach (SPListItem item in ListConvert.Items)
                            //{
                            //    DataSolr oDMSolr = new DataSolr(item);
                            //    solrWorkerStatic.Add(oDMSolr);
                            //    solrWorkerStatic.Commit();
                            //    //Console.WriteLine(oDMSolr.Title);
                            //    oResultAction = new ResultAction()
                            //    {
                            //        Message = "Ok"
                            //    };
                            //}

                        };
                        };
                    });
                }
            }
            return oResultAction;
        }

        /// <summary>
        /// hàm dùng để gửi mail
        /// 
        /// </summary>
        /// <param name="oLNotificationJson"></param>
        /// <returns></returns>
        /// 
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public ResultAction SendMail(LNotificationJson oLNotificationJson)
        {
            ResultAction oResultAction = new ResultAction();
            LconfigDA oLconfigDA = new LconfigDA(true);
            string Hostmail = oLconfigDA.GetValueConfigByType("Hostmail");
            string MailFrom = oLconfigDA.GetValueConfigByType("AccountMail");
            string PassEmail = oLconfigDA.GetValueConfigByType("PassEmail");
            string ContentWF = oLconfigDA.GetValueMultilConfigByType("ContentWF");
            string Port = oLconfigDA.GetValueConfigByType("Port");
            List<string> mailto = new List<string>() { "vinhhq.205@gmail.com" };
            ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2013_SP1);
            service.Credentials = new NetworkCredential("test01", "Admin@1234");
            service.Url = new Uri("https://mail.evnhanoi.vn/EWS/Exchange.asmx");
            EmailMessage emailMessage = new EmailMessage(service);
            emailMessage.Subject = "Thử gửi";
            emailMessage.ToRecipients.Add("vinhhq.205@gmail.com");
            emailMessage.Body = new MessageBody("Chuc mừng sinh nhat");
            emailMessage.SendAndSaveCopy();
            //MailMessage mail = new MailMessage();
            //SmtpClient SmtpServer = new SmtpClient(Hostmail);
            //mail.From = new MailAddress(MailFrom);
            //foreach (string item in mailto)
            //    mail.To.Add(item);
            //mail.Subject = "Chuc mung sinh nhat";
            //mail.Body = ContentWF;
            //mail.IsBodyHtml = true;
            //SmtpServer.Port = Convert.ToInt32(Port);
            //SmtpServer.Credentials = new System.Net.NetworkCredential(MailFrom, PassEmail);
            ////SmtpServer.EnableSsl = true;
            //SmtpServer.Send(mail);
            oResultAction.OData = oLNotificationJson;
            oResultAction.Message = "Gửi thành công";
            return oResultAction;
        }

    }
}
