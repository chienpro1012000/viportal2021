﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Security;
using VIPortalAPI.Models;
using ViPortalData;
using ViPortalData.Lconfig;

namespace VIPortalAPI.ControllersMobi
{
    /// <summary>
    /// 
    /// </summary>
    [AllowAnonymous]
    public class AccountController : ApiController
    {
        /// <summary>
        /// /
        /// </summary>
        /// <param name="objVM"></param>
        /// <returns></returns>
        [HttpPost]
        public ResponseVM UserLogin([FromBody] LoginVM objVM)
        {
            //check xác thực chỗ này thông qua AD
            LconfigDA oLconfigDA = new LconfigDA(true);
            List<LconfigJson> lstJson = oLconfigDA.GetListJson(new LconfigQuery() { ConfigGroup = "ADConfig" });
            string Domain = lstJson.FirstOrDefault(x => x.ConfigType == "Domain") != null ? lstJson.FirstOrDefault(x => x.ConfigType == "Domain").ConfigValue : "udtt";
            string LdapPath = lstJson.FirstOrDefault(x => x.ConfigType == "LdapPath") != null ? lstJson.FirstOrDefault(x => x.ConfigType == "LdapPath").ConfigValue : "LDAP://DC=ungdungtructuyen,DC=vn";
            string ErrMessage = "";
            if (ValidateUser(Domain, objVM.UserName, objVM.Passward, LdapPath, out ErrMessage))
            {
                //check tiếp vào user xem có đủ quyền đăng nhập hay ko/
                LUserDA oLUserDA = new LUserDA(true);
                List<LUserJson> CountUser = oLUserDA.GetListJson(new LUserQuery()
                {
                    TaiKhoanTruyCap = objVM.UserName,
                    _ModerationStatus = 1
                });
                if (CountUser.Count > 0)
                {
                    //update thông tin user.
                    if(!string.IsNullOrEmpty(objVM.TokenFireBase))
                    {
                        oLUserDA.SystemUpdateOneField(CountUser[0].ID, "TokenFireBase", objVM.TokenFireBase);
                    }
                    return new ResponseVM
                    {
                        Status = "Success",
                        Message = JwtManager.GenerateToken(objVM.UserName)
                    };
                }
                else
                {
                    return new ResponseVM
                    {
                        Status = "Error",
                        Message = "Tài khoản không đúng"
                    };
                }
            }
            else
            {
                return new ResponseVM
                {
                    Status = "Error",
                    Message = ErrMessage
                };
            }
        }

        /// <summary>
        /// /
        /// </summary>
        /// <param name="objVM"></param>
        /// <returns></returns>
        [HttpPost]
        public ResponseVM UserLoginCheck([FromBody] LoginVM objVM)
        {
            LUserDA oLuserDA = new LUserDA(true);
            List<LUserJson> CountUser = oLuserDA.GetListJson(new LUserQuery()
            {
                KeySSO = objVM.UserName,
                _ModerationStatus = 1
            });
            if (CountUser.Count > 0)
            {
                //update thông tin user.
                if (!string.IsNullOrEmpty(objVM.TokenFireBase))
                {
                    oLuserDA.SystemUpdateOneField(CountUser[0].ID, "TokenFireBase", objVM.TokenFireBase);
                }
                return new ResponseVM
                {
                    Status = "Success",
                    Message = JwtManager.GenerateToken(objVM.UserName)
                };
            }
            else
            {
                return new ResponseVM
                {
                    Status = "Error",
                    Message = "Tài khoản không đúng"
                };
            }
        }

        /// <summary>
        /// hàm check tài khoản pass
        /// </summary>
        /// <param name="domain"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="LdapPath"></param>
        /// <param name="Errmsg"></param>
        /// <returns></returns>
        public bool ValidateUser(string domain, string username, string password, string LdapPath, out string Errmsg)
        {
            Errmsg = "";
            try
            {
                //string domainAndUsername = domain + @"\" + username;
                //DirectoryEntry entry = new DirectoryEntry(LdapPath, domainAndUsername, password);
                //try
                //{
                //    // Bind to the native AdsObject to force authentication.
                //    Object obj = entry.NativeObject;
                //    DirectorySearcher search = new DirectorySearcher(entry);
                //    search.Filter = "(SAMAccountName=" + username + ")";
                //    search.PropertiesToLoad.Add("cn");
                //    SearchResult result = search.FindOne();
                //    if (null == result)
                //    {
                //        return false;
                //    }
                //    // Update the new path to the user in the directory
                //    LdapPath = result.Path;
                //    string _filterAttribute = (String)result.Properties["cn"][0];
                //}
                //catch (Exception ex)
                //{
                //    Errmsg = ex.Message;
                //    ///throw new Exception("Error authenticating user." + ex.Message);
                //    ///retur
                //    ///
                //    return false;

                //}
                if( Membership.ValidateUser(username, password))
                {
                    return true;
                }
                else
                {
                    Errmsg = "Tài khoản, mật khẩu không đúng";
                    return false;
                }
            }catch (Exception ex)
            {
                Errmsg = ex.Message;
            }
            return false;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ticket"></param>
        /// <param name="appCode"></param>
        /// <returns></returns>
        public IHttpActionResult serviceValidate (string ticket, string appCode)
        {
            var client = new HttpClient();
            var content = client.GetStringAsync("http://10.9.125.79:1111/sso/serviceValidate?ticket=" + ticket + "&appCode=PORTAL").Result;

            SSOInfor oSSOInfor = Newtonsoft.Json.JsonConvert.DeserializeObject<SSOInfor>(content);
            return Ok(oSSOInfor);
        }
    }
}
