﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace VIPortalAPI.Filters
{
    public class APIAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool IsAuthorized(HttpActionContext context)
        {
            // Do your stuff and determine if the request can proceed further or not
            // If not, return false
            return true;
        }
    }
}