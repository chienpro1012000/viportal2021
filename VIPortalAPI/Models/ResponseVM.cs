﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VIPortalAPI.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class ResponseVM
    {
        /// <summary>
        /// 
        /// </summary>
        public string Status { set; get; }
        /// <summary>
        /// /
        /// </summary>
        public string Message { set; get; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class LoginVM
    {
        /// <summary>
        /// 
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Passward { get; set; }
        /// <summary>
        /// lưu thông tin token user
        /// </summary>
        public string TokenFireBase { get; set; }
    }
}